unit SORFileManagerMain;

interface

uses
    // by Developer
    Registry, Util, IniFiles, EventLog, ActiveX, DateUtils, Variants, ZipMstr, Crypto,
    // by default
    Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs, DB, ADODB, ExtCtrls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, IdFTPCommon;

type
  TKapsch_SORFileManagerService = class(TService)
    BaseSOR: TADOConnection;
    BaseEventos: TADOConnection;
    tmr_EjecutarProcesos: TTimer;
    ADOStoredProc1: TADOStoredProc;
    tmr_CancelFTP: TTimer;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure tmr_EjecutarProcesosTimer(Sender: TObject);
    procedure ServiceBeforeInstall(Sender: TService);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceAfterUninstall(Sender: TService);
    procedure tmr_CancelFTPTimer(Sender: TObject);
  private
    { Private declarations }
    IdFtp1  : TIdFTP;

    FTrabajando,
    FDeteniendo : Boolean;

    FLogPath,
    FArchivoLogNombre,
    FDatabaseNameSOR, FServerSOR,
    FDatabaseNameEventos, FServerEventos : string;

    FArchivoLog: TextFile;

    FConcesionariaNativa    : Byte;

    FCanalComunicacionSOR,
    FTipoCanalComuniacionSOR : Integer;

    FUsuariosistema,
    FCanalComunicacionDescripcion,
    FTipoCanalComunicacionDescripcion : string;

    FHash,
    FEncryptedPassword : Ansistring;

    procedure LeerParametrosIni;
    function CheckDBConnections: Boolean;

    function ValidarCanalComunicacion: Boolean;
    function ValidarArchivoPorPatron(pArchivo, pPatron: String): Boolean;

    procedure ValidarXML(
                pCodigoTipoArchivo,
                pCodigoConcesionaria            : Byte;
                pCodigoArchivo                  : Int64;
                pRutaArchivoCompleta            : Ansistring);

    function ValidarArchivoDuplicado(pCodigoArchivo: Int64): Boolean;
    function ValidarDumpCaducado(pCodigoArchivo: Int64): Boolean;
    function ValidarDumpDelDia(pCodigoArchivo: Int64): Boolean;

    function MyGetFileSize(pArchivo: AnsiString): Cardinal;

    procedure ConectarFTP(pCodigoCanalComunicacion: Byte);

    procedure InsertaArchivo(
                pCodigoTipoArchivo,
                pCodigoCanalComunicacion,
                pCodigoCanalComunicacionAccion,
                pCodigoUbicacionOrigen,
                pCodigoUbicacionDestino         : Byte;

                pNombreArchivo                  : Ansistring;
                var pCodigoArchivo              : Int64;
                var pFechaGeneracion            : TDateTime;
                var pCodigoConcesionaria        : Byte);

    procedure ActualizaEstadoArchivo(pCodigoArchivo: Int64; pCodigoEstadoArchivo: Byte);

    procedure ProcesarCanalFTP(
                pCodigoTipoArchivo,
                pCodigoCanalComunicacion,
                pCodigoCanalComunicacionTipo,
                pCodigoCanalComunicacionAccion,
                pCodigoUbicacionOrigen,
                pCodigoUbicacionDestino         : Byte;

                pRutaUbicacionOrigen,
                pRutaUbicacionDestino,
                pPrefijoArchivo,
                pSufijoArchivo,
                pExtensionArchivo               : AnsiString;
                pLargoFijo                      : Byte;

                pEliminarDespuesAccion,                                         // TASK_080_MGO_20161021
                pDirectoriosPorFecha            : Boolean);                     // TASK_080_MGO_20161021

    procedure ProcesarCanalRecursoCompartido(
                pCodigoTipoArchivo,
                pCodigoCanalComunicacion,
                pCodigoCanalComunicacionTipo,
                pCodigoCanalComunicacionAccion,
                pCodigoUbicacionOrigen,
                pCodigoUbicacionDestino         : Byte;

                pRutaUbicacionOrigen,
                pRutaUbicacionDestino,
                pPrefijoArchivo,
                pSufijoArchivo,
                pExtensionArchivo               : AnsiString;
                pLargoFijo                      : Byte;

                pEliminarDespuesAccion,                                         // TASK_080_MGO_20161021
                pDirectoriosPorFecha            : Boolean);                     // TASK_080_MGO_20161021



    procedure DescomprimirZIP (
                pCodigoTipoArchivo              : Byte;
                pCodigoArchivo                  : Int64;
                pRutaArchivoCompleta            : Ansistring);

    procedure AddDebug(s: AnsiString; SoloLog: Boolean = True; Error: Boolean = False);
    procedure AddEvent(pProcedimiento, pMensaje: AnsiString; pError: Boolean = False; pInfoAdicional: AnsiString = '');

    procedure EjecutarProcesos;
    procedure ActivarDesactivarAbortFTP(pActivar: Boolean);
    procedure EliminaClienteFTP;

  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  Kapsch_SORFileManagerService: TKapsch_SORFileManagerService;


const
    cServiceDescription         = 'SOR File Manager Service';
    cModulo                     = 'SORFileManagerService';
    cCodigoSistema              = 200;

    cTipoCanalFTP               = 1;
    cTipoCanalRecursoCompartido = 2;

    cAccionUpload               = 1;
    cAccionDownload             = 2;

    cFileXML_EntradasSinProcesar    = 1;
    cFileXML_Salidas                = 2;
    cFileXML_NovedadesSinProcesar   = 3;
    cFileDUMP_RNUT                  = 4;
    cFileDUMP_SAP                   = 5;
    cNovedadesContrato              = 103;
    // INICIO : TASK_080_MGO_20161021
    cNominaServipag                 = 150;
    cRendicionServipag_CAJA         = 151;
    cRendicionServipag_CAJA_CH      = 152;
    cRendicionServipag_PORTAL       = 153;
    cRendicionServipag_EXPRESS      = 154;
    // FIN : TASK_080_MGO_20161021

    CONST_ARCHIVO_ESTADO_PENDIENTE_DESCARGA            = 0;
    CONST_ARCHIVO_ESTADO_EN_PROCESO_DESCARGA           = 1;
    CONST_ARCHIVO_ESTADO_DESCARGADO                    = 2;
    CONST_ARCHIVO_ESTADO_EN_PROCESO_DESCOMPRESION      = 10;
    CONST_ARCHIVO_ESTADO_PENDIENTE_CARGA_DUMP          = 11;
    CONST_ARCHIVO_ESTADO_EN_PROCESO_VALIDACION_XML     = 20;
    CONST_ARCHIVO_ESTADO_PENDIENTE_GENERACION_DESPACHO = 21;
    CONST_ARCHIVO_ESTADO_PENDIENTE_CARGA_RESPUESTA     = 24;
    CONST_ARCHIVO_ESTADO_PENDIENTE_ENVIO               = 30;
    CONST_ARCHIVO_ESTADO_EN_PROCESO_ENVIO              = 31;
    CONST_ARCHIVO_ESTADO_ENVIADO                       = 32;
    CONST_ARCHIVO_ESTADO_DUMP_FECHA_NO_VALIDA          = 249;
    CONST_ARCHIVO_ESTADO_DUMP_CADUCADO                 = 251;
    CONST_ARCHIVO_ESTADO_NO_EXISTE                     = 253;
    CONST_ARCHIVO_ESTADO_DUPLICADO                     = 254;
    CONST_ARCHIVO_ESTADO_CON_ERRORES                   = 255;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  Kapsch_SORFileManagerService.Controller(CtrlCode);
end;

function TKapsch_SORFileManagerService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TKapsch_SORFileManagerService.ServiceAfterInstall(Sender: TService);
    var
        Registro: TRegistry;

    const
        cServicioInstalado  = 'Servicio Instalado en %s, Canal Comunicaci�n: %s';
        cProcedimiento      = 'ServiceAfterInstall';
begin
    Registro := TRegistry.Create(KEY_READ or KEY_WRITE);

    with Registro do try
        RootKey := HKEY_LOCAL_MACHINE;
        if OpenKey('\SYSTEM\CurrentControlSet\Services\' + Name, false) then begin
            WriteString('Description', cServiceDescription);
            CloseKey;
        end;
    finally
        Free;
    end;

    if CheckDBConnections and ValidarCanalComunicacion then begin

        AddEvent(cProcedimiento, Format(cServicioInstalado, [GetMachineName, FCanalComunicacionDescripcion]));
    end;
end;

procedure TKapsch_SORFileManagerService.ServiceAfterUninstall(Sender: TService);
    const
        cServicioDesinstalado   = 'Servicio Desinstalado en %s, Canal Comunicaci�n: %s';
        cProcedimiento          = 'ServiceAfterUninstall';
begin
    if CheckDBConnections and ValidarCanalComunicacion then begin

        AddEvent(cProcedimiento, Format(cServicioDesinstalado, [GetMachineName, FCanalComunicacionDescripcion]));
    end;
end;

procedure TKapsch_SORFileManagerService.ServiceBeforeInstall(Sender: TService);
    const
        cDisplayName = '%s - Canal Comunicaci�n: %s';
begin
    LeerParametrosIni;
    if CheckDBConnections and ValidarCanalComunicacion then begin
        DisplayName := Format(cDisplayName, [DisplayName, FCanalComunicacionDescripcion])
    end
    else DisplayName := Format(cDisplayName, [DisplayName, IntToStr(FCanalComunicacionSOR)]);
end;

procedure TKapsch_SORFileManagerService.ServiceCreate(Sender: TObject);
    const
        cName = '%s_Canal_%d';
begin
    LeerParametrosIni;

    Name := Format(cName, [Name, FCanalComunicacionSOR]);
end;

procedure TKapsch_SORFileManagerService.EliminaClienteFTP;

    procedure LiberarMemoria;
    begin
        if Win32Platform = VER_PLATFORM_WIN32_NT then begin
            SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
        end;
    end;
begin
    if Assigned(IdFtp1) then begin
        FreeAndNil(IdFtp1);
        LiberarMemoria;
        AddDebug('Componente Eliminado');
    end;
end;

procedure TKapsch_SORFileManagerService.ActivarDesactivarAbortFTP(pActivar: Boolean);
begin
    tmr_CancelFTP.Enabled := False;
    if pActivar then tmr_CancelFTP.Enabled := True;
end;

procedure TKapsch_SORFileManagerService.LeerParametrosIni;
begin
    FLogPath                := GoodDir(InstallIni.ReadString('Paths', 'Log', ExtractFilePath(GetExeName) + 'Log'));

    { INICIO : TASK_080_MGO_20161021
    FDatabaseNameSOR        := InstallIni.ReadString('Database BO_SOR', 'DatabaseName', '');
    FServerSOR              := InstallIni.ReadString('Database BO_SOR', 'Server', GetMachineName);
    }
    FDatabaseNameSOR        := InstallIni.ReadString('Database BO', 'DatabaseName', '');
    FServerSOR              := InstallIni.ReadString('Database BO', 'Server', GetMachineName);
    // FIN : TASK_080_MGO_20161021
    FDatabaseNameEventos    := InstallIni.ReadString('Database BO_Eventos', 'DatabaseName', '');
    FServerEventos          := InstallIni.ReadString('Database BO_Eventos', 'Server', GetMachineName);

    FCanalComunicacionSOR   := InstallIni.ReadInteger('General', 'CanalComunicacionSOR',0);
    FConcesionariaNativa    := InstallIni.ReadInteger('General', 'ConcesionariaNativa',0);

    FUsuarioSistema         := 'usr_Canal' + IntToStr(FCanalComunicacionSOR);

    FHash                   := InstallIni.ReadString('General', 'Hash', 'Cualquiera');
    FEncryptedPassword      := InstallIni.ReadString('General', 'Password', 'Cualquiera');

    ForceDirectories(FLogPath);
end;

function TKapsch_SORFileManagerService.CheckDBConnections: Boolean;
    const
        cErrorConexion      = 'Se produjo un error al conectar con la DB. Error: %s, Server: %s, Database: %s.';
        cConnectionString   = 'Provider=SQLOLEDB.1; Workstation ID=%s; Initial Catalog=%s; Data Source=%s';
begin
    try
        Result := True;

        if BaseSOR.Connected then BaseSOR.Close;

        with BaseSOR do begin
            Connected           := False;
            ConnectionString    := Format(cConnectionString, [GetMachineName, FDatabaseNameSOR, FServerSOR]);
            KeepConnection      := True;
            LoginPrompt         := False;
        end;

        BaseSOR.Open('usr_sor', DecodePWDEx(FEncryptedPassword, FHash));

        Result := BaseSOR.Connected;
    except
        on e: Exception do begin
            Result := False;
            AddDebug(Format(cErrorConexion, [e.Message, FServerSOR, FDatabaseNameSOR]), False, True);
        end;
    end;

    if Result then try
        if BaseEventos.Connected then BaseEventos.Close;

        with BaseEventos do begin
            Connected           := False;
            ConnectionString    := Format(cConnectionString, [GetMachineName, FDatabaseNameEventos, FServerEventos]);
            KeepConnection      := True;
            LoginPrompt         := False;
        end;
        BaseEventos.Open('usr_sor', DecodePWDEx(FEncryptedPassword, FHash));

        Result := BaseEventos.Connected;
    except
        on e: Exception do begin
            Result := False;
            AddDebug(Format(cErrorConexion, [e.Message, FServerEventos, FDatabaseNameEventos]), False, True);
        end;
    end;
end;

procedure TKapsch_SORFileManagerService.AddDebug(s: AnsiString; SoloLog: Boolean = True; Error: Boolean = False);
    resourceString
        MSG_PROCESAR_FILE_OPEN_ERROR = 'Ha ocurrido el error "%s" al intentar la apertura del archivo Log %s del Servicio %s';
        MSG_PROCESAR_FILE_WRITE_ERROR = 'Ha ocurrido el error "%s" al intentar escribir el mensaje "%s" en el archivo Log %s del Servicio %s';

    var
        Mensaje: TStringList;
        ArchivoLog: TextFile;
        NombreArchivoLog: string;
        Ahora: TDateTime;

    const
        cLineaLog = '%s - %s - %s';
        cError = 'ERROR';
        cInfo = 'INFO';
begin
    try
        Ahora := Now;

        NombreArchivoLog := Format(FLogPath + 'Log_%s_%s.txt',[cModulo, FormatDateTime('yyyymmdd', Ahora)]);
        AssignFile(ArchivoLog, NombreArchivoLog);
        try
            if FileExists(NombreArchivoLog) then begin
                Append(ArchivoLog);
            end
            else begin
                Rewrite(ArchivoLog);
            end;
        except
            on e:exception do begin
                EventLogReportEvent(elError, Format(MSG_PROCESAR_FILE_OPEN_ERROR,
                  [e.Message, NombreArchivoLog, cModulo]), '');
            end;
        end;

        try
            Writeln(ArchivoLog,  Format(cLineaLog, [FormatDateTime('dd/mm/yyyy hh:nn:ss.zzz', Ahora), iif(Error, cError, cInfo), s]));
        except
            on e:exception do begin
                EventLogReportEvent(elError, Format(MSG_PROCESAR_FILE_WRITE_ERROR,
                  [e.Message, s, NombreArchivoLog, cModulo]), '');
            end;
        end;
    finally
        CloseFile(ArchivoLog);
    end;
end;

procedure TKapsch_SORFileManagerService.AddEvent(pProcedimiento, pMensaje: string; pError: Boolean = False; pInfoAdicional: AnsiString = '');
    var
        lLOG_EventoAppAgregar: TADOStoredProc;
begin
    try
        try
            lLOG_EventoAppAgregar := TADOStoredProc.Create(nil);

            with lLOG_EventoAppAgregar do begin

                Connection      := BaseEventos;
                ProcedureName   := 'LOG_EventoAppAgregar';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoSistema').Value      := cCodigoSistema;
                Parameters.ParamByName('@NombreProceso').Value      := cModulo + '.' + pProcedimiento;
                Parameters.ParamByName('@ErrorEstado').Value        := iif(pError, 1, 0);
                Parameters.ParamByName('@ErrorMensaje').Value       := pMensaje;
                Parameters.ParamByName('@ErrorInfoAdicional').Value := iif(pInfoAdicional = EmptyStr, Null, pInfoAdicional);
                ExecProc;
            end;
        except
            on E: Exception do begin

                AddDebug(e.Message, False, True);
            end;
        end;
    finally
        if Assigned(lLOG_EventoAppAgregar) then begin

            if lLOG_EventoAppAgregar.Active then lLOG_EventoAppAgregar.Close;
            FreeAndNil(lLOG_EventoAppAgregar);
        end;
    end;
end;

function TKapsch_SORFileManagerService.ValidarArchivoPorPatron(pArchivo, pPatron: String): Boolean;
    var
        lArchivo    : Array [0..255] of Char;
        lPatron     : Array [0..255] of Char;

	function CompararPatron(pElemento, pPatron: PChar): Boolean;
	begin
		if 0 = StrComp(pPatron,'*') then begin
			Result := True
        end
		else begin
			if (pElemento^ = Chr(0)) and (pPatron^ <> Chr(0)) then begin
				Result := False
            end
			else begin
				if pElemento^ = Chr(0) then begin
					Result := True
                end
				else begin
					case pPatron^ of
						'*': begin
                            if CompararPatron(pElemento,@pPatron[1]) then begin
								Result := True
                            end
							else Result := CompararPatron(@pElemento[1],pPatron);
                        end;

						'?': Result := CompararPatron(@pElemento[1],@pPatron[1]);
					else
						if pElemento^ = pPatron^ then begin
    						Result := CompararPatron(@pElemento[1],@pPatron[1])
                        end
        				else Result := False;
			        end;
        		end;
            end;
        end;
	end;

begin
    StrPCopy(lArchivo,pArchivo);
    StrPCopy(lPatron,pPatron);
    
    Result := CompararPatron(lArchivo,lPatron);
end;

procedure TKapsch_SORFileManagerService.ServiceStart(Sender: TService; var Started: Boolean);
    const
        cConnectionString       = 'Provider=SQLOLEDB.1; Workstation ID=%s; Initial Catalog=%s; Data Source=%s';
        cProcedimiento          = 'ServiceStart';
        cErrorIniciandoServicio = 'NO se pudo inciar el Servicio %s en %s, deb�do al siguiente Error: %s.';
        cServicioIniciado       = 'Servicio Iniciado en %s. Canal Comunicaci�n: %s.';
begin
    try
        CoInitialize(nil);

        LeerParametrosIni;
        AddDebug('Iniciando Servicio ...', False, False);

        FTrabajando := False;
        FDeteniendo := False;

        tmr_EjecutarProcesos.Enabled := true;
        Started := True;

        if CheckDBConnections and ValidarCanalComunicacion then begin
            AddEvent(cProcedimiento, Format(cServicioIniciado, [GetMachineName, FCanalComunicacionDescripcion]), False);
        end;

        AddDebug('Servicio Iniciado.', False, False);
    except
        on e: Exception do begin
        
            Started := False;
            AddEvent(cProcedimiento, Format(cErrorIniciandoServicio, [FCanalComunicacionDescripcion, GetMachineName, e.Message]), True);
        end;
    end;
end;

procedure TKapsch_SORFileManagerService.ServiceStop(Sender: TService; var Stopped: Boolean);
    var
        lTiempoEspera: Integer;

    const
        cProcedimiento              = 'ServiceStop';
        cErrorDeteniendoServicio    = 'Se produjo un Error deteniendo el Servicio %s en %s. Error: %s.';
        cServicioDetenido           = 'Servicio Detenido en %s. Canal Comunicaci�n: %s.';

begin
    try
        try
            AddDebug('Deteniendo Servicio ...', False, False);

            FDeteniendo   := True;
            tmr_EjecutarProcesos.Enabled  := False;
            lTiempoEspera := 0;

            while FTrabajando and (lTiempoEspera < 60000) do begin
                AddDebug('Esperando a que el Servicio finalice el trabajo en curso ...', False, False);
                Sleep(250);
                lTiempoEspera := lTiempoEspera + 250
            end;

            if CheckDBConnections then begin
                AddEvent(cProcedimiento, Format(cServicioDetenido, [GetMachineName, FCanalComunicacionDescripcion]), False);
            end;

            EliminaClienteFTP;

            AddDebug('Servicio Detenido.', False, False);
        except
            on e: Exception do begin
                AddEvent(cProcedimiento, Format(cErrorDeteniendoServicio, [FCanalComunicacionDescripcion, GetMachineName, e.Message]), True);
            end;
        end;
    finally
        CoUninitialize;
        Stopped := True;
    end;
end;

procedure TKapsch_SORFileManagerService.tmr_CancelFTPTimer(Sender: TObject);
    const
        cProcedimiento  = 'tmr_CancelFTPTimer';
        cError          = 'Enviando FTP Abort al Objeto FTP ...';
begin
    if Assigned(IdFTP1) then begin
        AddDebug(cError, False, True);
        AddEvent(cProcedimiento, cError, True);
        IdFTP1.Abort;
    end;
end;

procedure TKapsch_SORFileManagerService.tmr_EjecutarProcesosTimer(Sender: TObject);
begin
    try
        tmr_EjecutarProcesos.Enabled := False;

        FTrabajando := True;
        if not FDeteniendo then begin

            LeerParametrosIni;

            if CheckDBConnections and ValidarCanalComunicacion then begin
                EjecutarProcesos;
                tmr_EjecutarProcesos.Interval := 60000; // 1 min
            end
            else begin
                tmr_EjecutarProcesos.Interval := 300000; // 5 min
            end;
        end;

        FTrabajando := False;
    finally
        tmr_EjecutarProcesos.Enabled := not FDeteniendo;
    end;
end;

procedure TKapsch_SORFileManagerService.EjecutarProcesos;
    var
        lSORFileManager_CanalTareasObtener: TADOStoredProc;

        lCodigoTipoArchivo,
        lCodigoCanalComunicacion,
        lCodigoCanalComunicacionTipo,
        lCodigoCanalComunicacionAccion,
        lCodigoUbicacionOrigen,
        lCodigoUbicacionDestino,
        lLargoFijo                      : Byte;

        lRutaUbicacionOrigen,
        lRutaUbicacionDestino,
        lPrefijoArchivo,
        lSufijoArchivo,
        lExtensionArchivo               : AnsiString;

        lEliminarDespuesAccion,                                                 //TASK_080_MGO_20161021
        lDirectoriosPorFecha            : Boolean;                              //TASK_080_MGO_20161021

    const
        cProcedimiento = 'EjecutarProcesos';
begin
    try

        try
            lSORFileManager_CanalTareasObtener := TADOStoredProc.Create(nil);

            with lSORFileManager_CanalTareasObtener do begin

                Connection      := BaseSOR;
                ProcedureName   := 'SORFileManager_CanalTareasObtener';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoCanalComunicacion').Value := FCanalComunicacionSOR;
                Open;

                //AddDebug('SORFileManager_CanalTareasObtener');
            end;

            if (not lSORFileManager_CanalTareasObtener.Eof) and (not FDeteniendo) then begin

                while (not lSORFileManager_CanalTareasObtener.Eof) and (not FDeteniendo) do begin

                    with lSORFileManager_CanalTareasObtener do begin
                        lCodigoTipoArchivo              := FieldByName('CodigoTipoArchivo').AsInteger;
                        lCodigoCanalComunicacion        := FieldByName('CodigoCanalComunicacion').AsInteger;
                        lCodigoCanalComunicacionTipo    := FieldByName('CodigoCanalComunicacionTipo').AsInteger;
                        lCodigoCanalComunicacionAccion  := FieldByName('CodigoCanalComunicacionAccion').AsInteger;
                        lCodigoUbicacionOrigen          := FieldByName('CodigoUbicacionOrigen').AsInteger;
                        lCodigoUbicacionDestino         := FieldByName('CodigoUbicacionDestino').AsInteger;

                        lRutaUbicacionOrigen            := FieldByName('RutaUbicacionOrigen').AsString;
                        lRutaUbicacionDestino           := FieldByName('RutaUbicacionDestino').AsString;
                        lPrefijoArchivo                 := FieldByName('PrefijoArchivo').AsString;
                        lSufijoArchivo                  := FieldByName('SufijoArchivo').AsString;
                        lExtensionArchivo               := FieldByName('ExtensionArchivo').AsString;
                        lLargoFijo                      := FieldByName('LargoFijo').AsInteger;

                        lDirectoriosPorFecha            := FieldByName('DirectoriosPorFecha').AsBoolean;    // TASK_080_MGO_20161021
                        lEliminarDespuesAccion          := FieldByName('EliminarDespuesAccion').AsBoolean;

                        //AddDebug(inttostr(lCodigoTipoArchivo) + ' ' + IntToStr(lCodigoCanalComunicacionTipo));
                    end;

                    case lCodigoCanalComunicacionTipo of

                        cTipoCanalFTP: begin

                            ProcesarCanalFTP(
                                lCodigoTipoArchivo,
                                lCodigoCanalComunicacion,
                                lCodigoCanalComunicacionTipo,
                                lCodigoCanalComunicacionAccion,
                                lCodigoUbicacionOrigen,
                                lCodigoUbicacionDestino,
                                lRutaUbicacionOrigen,
                                lRutaUbicacionDestino,
                                lPrefijoArchivo,
                                lSufijoArchivo,
                                lExtensionArchivo,
                                lLargoFijo,
                                lEliminarDespuesAccion,                         // TASK_080_MGO_20161021
                                lDirectoriosPorFecha);                          // TASK_080_MGO_20161021
                        end;

                        cTipoCanalRecursoCompartido: begin

                            ProcesarCanalRecursoCompartido(
                                lCodigoTipoArchivo,
                                lCodigoCanalComunicacion,
                                lCodigoCanalComunicacionTipo,
                                lCodigoCanalComunicacionAccion,
                                lCodigoUbicacionOrigen,
                                lCodigoUbicacionDestino,
                                lRutaUbicacionOrigen,
                                lRutaUbicacionDestino,
                                lPrefijoArchivo,
                                lSufijoArchivo,
                                lExtensionArchivo,
                                lLargoFijo,
                                lEliminarDespuesAccion,                         // TASK_080_MGO_20161021
                                lDirectoriosPorFecha);                          // TASK_080_MGO_20161021
                        end;    
                    end;

                    lSORFileManager_CanalTareasObtener.Next;
                end;
            end;
        except
            on E: Exception do begin
                AddEvent(cProcedimiento, e.Message, True);
            end;
        end;
    finally
        if Assigned(lSORFileManager_CanalTareasObtener) then begin
            if lSORFileManager_CanalTareasObtener.Active then lSORFileManager_CanalTareasObtener.Close;
            FreeAndNil(lSORFileManager_CanalTareasObtener);
        end;
    end;
end;

function TKapsch_SORFileManagerService.ValidarCanalComunicacion: Boolean;
    var
        lSORFileManager_CanalValidar: TADOStoredProc;

    const
        cErrorEnCanalComunicacion   = 'El Canal de Comunicaci�n configurado NO es v�lido. C�digo Canal %d.';
        cProcedimiento              = 'ValidarCanalComunicacion';
begin
    try
        Result := False;

        try
            lSORFileManager_CanalValidar := TADOStoredProc.Create(nil);

            with lSORFileManager_CanalValidar do begin

                Connection      := BaseSOR;
                ProcedureName   := 'SORFileManager_CanalValidar';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoCanalComunicacion').Value := FCanalComunicacionSOR;
                Open;

                if Recordset.RecordCount = 1 then begin

                    FCanalComunicacionDescripcion       := FieldByName('CanalComunicacionDescripcion').AsString;
                    FTipoCanalComuniacionSOR            := FieldByName('CodigoCanalComunicacionTipo').AsInteger;
                    FTipoCanalComunicacionDescripcion   := FieldByName('TipoCanalComunicacionDescripcion').AsString;

                    Result := True;
                end
                else raise Exception.Create(Format(cErrorEnCanalComunicacion, [FCanalComunicacionSOR]));
            end;
        except
            on e: Exception do begin
                AddEvent(cProcedimiento, e.Message, True);
            end;
        end;
    finally
        if Assigned(lSORFileManager_CanalValidar) then begin
            if lSORFileManager_CanalValidar.Active then lSORFileManager_CanalValidar.Close;
            FreeAndNil(lSORFileManager_CanalValidar);
        end;
    end;
end;

procedure TKapsch_SORFileManagerService.InsertaArchivo(
                pCodigoTipoArchivo,
                pCodigoCanalComunicacion,
                pCodigoCanalComunicacionAccion,
                pCodigoUbicacionOrigen,
                pCodigoUbicacionDestino         : Byte;


                pNombreArchivo                  : Ansistring;
                var pCodigoArchivo              : Int64;
                var pFechaGeneracion            : TDateTime;
                var pCodigoConcesionaria        : Byte);

    var
        lNumeroArchivo              : Int64;
        lFechaGeneracion,
        lFechaProcesoRNUT           : TDateTime;

        lArchivosMaestro_Insertar   : TADOStoredProc;

    const
        cProcedimiento = 'InsertaArchivo';

    function ConvierteFecha(pFechaSTR: AnsiString): TDateTime;
        var
            lYear, lMonth, lDay, lHours, lMinutes, lSeconds: Word;
    begin
        lYear       := StrToInt(Copy(pFechaSTR, 1, 4));
        lMonth      := StrToInt(Copy(pFechaSTR, 5, 2));
        lDay        := StrToInt(Copy(pFechaSTR, 7, 2));
        lHours      := StrToInt(Copy(pFechaSTR, 9, 2));
        lMinutes    := StrToInt(Copy(pFechaSTR, 11, 2));
        lSeconds    := StrToInt(Copy(pFechaSTR, 13, 2));

        Result      := EncodeDateTime(lYear, lMonth, lDay, lHours, lMinutes, lSeconds, 0);
    end;

    procedure CargaDatosArchivoXML;
        var
            lCadena     : AnsiString;
            lCadena1    : AnsiString;
            lCadena2    : AnsiString;
            lCadena3    : AnsiString;
            lCadena4    : AnsiString;
            lCadena5    : AnsiString;
            lCaracter   : Char;
            lContador   : Integer;
            lSeparador  : Integer;
    begin

        lCadena1    := EmptyStr;
        lCadena2    := EmptyStr;
        lCadena3    := EmptyStr;
        lCadena4    := EmptyStr;
        lCadena5    := EmptyStr;

        lSeparador  := 0;

        for lContador := 1 to Length(pNombreArchivo) do begin

            lCaracter := pNombreArchivo[lContador];

            if lCaracter = '_' then begin

                Inc(lSeparador);

                case lSeparador of

                    1: begin
                        lCadena1    := lCadena;
                        lCadena     := EmptyStr;
                    end;
                    2: begin
                        lCadena2    := lCadena;
                        lCadena     := EmptyStr;
                    end;
                    3: begin
                        lCadena3    := lCadena;
                        lCadena     := EmptyStr;
                    end;
                    4: begin
                        lCadena4    := lCadena;
                        lCadena     := EmptyStr;
                    end;
                end;
            end
            else if lCaracter = '.' then begin
                case lSeparador of

                    3: begin
                        lCadena4    := lCadena;
                        lCadena     := EmptyStr;
                    end;
                    4: begin
                        lCadena5    := lCadena;
                        lCadena     := EmptyStr;
                    end;
                end;

                Break;
            end
            else lCadena := lCadena + lCaracter;
        end;

        try
            pCodigoConcesionaria    := StrToInt(lCadena2);
            lFechaGeneracion        := ConvierteFecha(lCadena4);
            lNumeroArchivo          := StrToInt64(lCadena3);

            case pCodigoTipoArchivo of
                cFileXML_Salidas, cFileXML_NovedadesSinProcesar: begin

                    lFechaProcesoRNUT   :=  ConvierteFecha(lCadena5);
                end;

                cFileXML_EntradasSinProcesar: begin

                    lFechaProcesoRNUT   := NullDate;
                end;
            end;

        except
            on e: Exception do begin
                pCodigoConcesionaria    := 0;
                lFechaGeneracion        := NullDate;
                lFechaProcesoRNUT       := NullDate;
                lNumeroArchivo          := 0;
            end;
        end;
    end;


begin
    try
        pCodigoArchivo          := 0;

        try
            case pCodigoTipoArchivo of
                // entrada_1_2135521_20131104164435.xml
                cNovedadesContrato, cFileXML_EntradasSinProcesar, cFileXML_Salidas, cFileXML_NovedadesSinProcesar: CargaDatosArchivoXML;
//
//                    pCodigoConcesionaria    := StrToInt(Copy(pNombreArchivo, 9, 1));
//                    lFechaGeneracion        := ConvierteFecha(Copy(pNombreArchivo, Length(pNombreArchivo) - 17, 14));
//                    lFechaProcesoRNUT       := NullDate;
//                    lNumeroArchivo          := StrToInt64(Copy(pNombreArchivo, 11, Length(pNombreArchivo) - 29));
//                except
//                    on e: Exception do begin
//                        pCodigoConcesionaria    := 0;
//                        lFechaGeneracion        := NullDate;
//                        lFechaProcesoRNUT       := NullDate;
//                        lNumeroArchivo          := 0;
//                    end;
//                end;
//                // salida_1_507008_20080319092717_20080319090106.xml
//                cFileXML_Salidas: try
//
//                    pCodigoConcesionaria    := StrToInt(Copy(pNombreArchivo, 8, 1));
//                    lFechaGeneracion        := ConvierteFecha(Copy(pNombreArchivo, Length(pNombreArchivo) - 32, 14));
//                    lFechaProcesoRNUT       := ConvierteFecha(Copy(pNombreArchivo, Length(pNombreArchivo) - 17, 14));
//                    lNumeroArchivo          := StrToInt64(Copy(pNombreArchivo, 10, Length(pNombreArchivo) - 43));
//                except
//                    on e: Exception do begin
//                        pCodigoConcesionaria    := 0;
//                        lFechaGeneracion        := NullDate;
//                        lFechaProcesoRNUT       := NullDate;
//                        lNumeroArchivo          := 0;
//                    end;
//                end;
//                // novedad_4_803535_20131104145600_20131104160240.xml
//                cFileXML_NovedadesSinProcesar: try
//
//                    pCodigoConcesionaria    := StrToInt(Copy(pNombreArchivo, 9, 1));
//                    lFechaGeneracion        := ConvierteFecha(Copy(pNombreArchivo, Length(pNombreArchivo) - 32, 14));
//                    lFechaProcesoRNUT       := ConvierteFecha(Copy(pNombreArchivo, Length(pNombreArchivo) - 17, 14));
//                    lNumeroArchivo          := StrToInt64(Copy(pNombreArchivo, 11, Length(pNombreArchivo) - 44));
//                except
//                    on e: Exception do begin
//                        pCodigoConcesionaria    := 0;
//                        lFechaGeneracion        := NullDate;
//                        lFechaProcesoRNUT       := NullDate;
//                        lNumeroArchivo          := 0;
//                    end;
//                end;
//                  DUMP_COBO_20160704085700051
                // DUMP2014070904.zip
                cFileDUMP_RNUT: try

                    pCodigoConcesionaria    := FConcesionariaNativa;
                    lFechaGeneracion        := ConvierteFecha(Copy(pNombreArchivo, 5, 8) + '000000');
                    lFechaProcesoRNUT       := NullDate;
                    lNumeroArchivo          := StrToInt64(Copy(pNombreArchivo, 5, 10));
                except
                    on e: Exception do begin
                        pCodigoConcesionaria    := 0;
                        lFechaGeneracion        := NullDate;
                        lFechaProcesoRNUT       := NullDate;
                        lNumeroArchivo          := 0;
                    end;
                end;
                // DUMP_SAP_20140805161600010.zip
                cFileDUMP_SAP: try

                    pCodigoConcesionaria    := FConcesionariaNativa;
                    lFechaGeneracion        := ConvierteFecha(Copy(pNombreArchivo, 11, 12) + '00');
                    lFechaProcesoRNUT       := NullDate;
                    lNumeroArchivo          := StrToInt64(Copy(pNombreArchivo, 11, 4) + Copy(pNombreArchivo, 23, 5));
                except
                    on e: Exception do begin
                        pCodigoConcesionaria    := 0;
                        lFechaGeneracion        := NullDate;
                        lFechaProcesoRNUT       := NullDate;
                        lNumeroArchivo          := 0;
                    end;
                end;
                // INICIO : TASK_080_MGO_20161021
                cNominaServipag ,cRendicionServipag_CAJA, cRendicionServipag_PORTAL, cRendicionServipag_EXPRESS: try
                    pCodigoConcesionaria    := FConcesionariaNativa;
                    lFechaGeneracion        := ConvierteFecha(Copy(pNombreArchivo, 6, 8) + '000000');
                    lNumeroArchivo          := StrToInt64(Copy(pNombreArchivo, 6, 8));
                except
                    on e: Exception do begin
                        pCodigoConcesionaria    := 0;
                        lFechaGeneracion        := NullDate;
                        lNumeroArchivo          := 0;
                    end;
                end;

                cRendicionServipag_CAJA_CH: try
                    pCodigoConcesionaria    := FConcesionariaNativa;
                    lFechaGeneracion        := ConvierteFecha(Copy(pNombreArchivo, 8, 8) + '000000');
                    lNumeroArchivo          := StrToInt64(Copy(pNombreArchivo, 8, 8));
                except
                    on e: Exception do begin
                        pCodigoConcesionaria    := 0;
                        lFechaGeneracion        := NullDate;
                        lNumeroArchivo          := 0;
                    end;
                end;
                // FIN : TASK_080_MGO_20161021
            end;

            pFechaGeneracion := lFechaGeneracion;

            lArchivosMaestro_Insertar := TADOStoredProc.Create(nil);

            with lArchivosMaestro_Insertar do begin
                Connection      := BaseSOR;
                ProcedureName   := 'SORFileManager_ArchivosMaestro_Insertar';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoTipoArchivo').Value              := pCodigoTipoArchivo;
                Parameters.ParamByName('@CodigoCanalComunicacion').Value        := pCodigoCanalComunicacion;
                Parameters.ParamByName('@CodigoCanalComunicacionAccion').Value  := pCodigoCanalComunicacionAccion;
                Parameters.ParamByName('@CodigoUbicacionOrigen').Value          := pCodigoUbicacionOrigen;
                Parameters.ParamByName('@CodigoUbicacionDestino').Value         := pCodigoUbicacionDestino;
                Parameters.ParamByName('@NombreArchivo').Value                  := pNombreArchivo;
                Parameters.ParamByName('@CodigoEstado').Value                   := CONST_ARCHIVO_ESTADO_PENDIENTE_DESCARGA;
                Parameters.ParamByName('@NumeroArchivo').Value                  := lNumeroArchivo;
                Parameters.ParamByName('@CodigoConcesionaria').Value            := iif(pCodigoConcesionaria = 0, Null, pCodigoConcesionaria);
                Parameters.ParamByName('@FechaHoraGeneracionArchivo').Value     := iif(lFechaGeneracion = NullDate, Null, lFechaGeneracion);;
                Parameters.ParamByName('@FechaHoraProcesoRNUT').Value           := iif(lFechaProcesoRNUT = NullDate, Null, lFechaProcesoRNUT);
                Parameters.ParamByName('@UsuarioCreacion').Value                := FUsuarioSistema;
                Parameters.ParamByName('@CodigoArchivo').Value                  := pCodigoArchivo;

                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value = 0 then begin
                    pCodigoArchivo := Parameters.ParamByName('@CodigoArchivo').Value;
                end;
            end;

        except
            on e: Exception do begin
                AddEvent(cProcedimiento, e.Message, True, pNombreArchivo);
            end;
        end;
    finally
        if Assigned(lArchivosMaestro_Insertar) then begin
            if lArchivosMaestro_Insertar.Active then lArchivosMaestro_Insertar.Close;
            FreeAndNil(lArchivosMaestro_Insertar);
        end;
    end;
end;

procedure TKapsch_SORFileManagerService.ActualizaEstadoArchivo(pCodigoArchivo: Int64; pCodigoEstadoArchivo: Byte);
    var
        lArchivosMaestro_CambiarEstado : TADOStoredProc;

    const
        cProcedimiento = 'ActualizaEstadoArchivo';

begin
    try
        try
            lArchivosMaestro_CambiarEstado := TADOStoredProc.Create(nil);

            with lArchivosMaestro_CambiarEstado do begin
                Connection      := BaseSOR;
                ProcedureName   := 'ArchivosMaestro_CambiarEstado';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoArchivo').Value      := pCodigoArchivo;
                Parameters.ParamByName('@CodigoEstadoNuevo').Value  := pCodigoEstadoArchivo;
                Parameters.ParamByName('@Usuario').Value            := FUsuariosistema;

                BaseSOR.BeginTrans;
                ExecProc;
                BaseSOR.CommitTrans;
            end;

        except
            on e: Exception do begin
                if BaseSOR.InTransaction then BaseSOR.RollbackTrans;
                
                raise Exception.Create(e.Message);
            end;
        end;
    finally
        if Assigned(lArchivosMaestro_CambiarEstado) then begin
            if lArchivosMaestro_CambiarEstado.Active then lArchivosMaestro_CambiarEstado.Close;
            FreeAndNil(lArchivosMaestro_CambiarEstado);
        end;
    end;
end;


function TKapsch_SORFileManagerService.ValidarArchivoDuplicado(pCodigoArchivo: Int64): Boolean;
    var
        lSORFileManager_ArchivoValidarDuplicado : TADOStoredProc;

    const
        cProcedimiento = 'ValidarArchivoDuplicado';

begin
    try
        try
            lSORFileManager_ArchivoValidarDuplicado := TADOStoredProc.Create(nil);
                                                                                          
            with lSORFileManager_ArchivoValidarDuplicado do begin

                Connection      := BaseSOR;
                ProcedureName   := 'SORFileManager_ArchivoValidarDuplicado';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoArchivo').Value  := pCodigoArchivo;
                ExecProc;

                Result := Parameters.ParamByName('@RETURN_VALUE').Value;
            end;

        except
            on e: Exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    finally
        if Assigned(lSORFileManager_ArchivoValidarDuplicado) then begin
            if lSORFileManager_ArchivoValidarDuplicado.Active then lSORFileManager_ArchivoValidarDuplicado.Close;
            FreeAndNil(lSORFileManager_ArchivoValidarDuplicado);
        end;
    end;
end;

function TKapsch_SORFileManagerService.ValidarDumpCaducado(pCodigoArchivo: Int64): Boolean;
    var
        lSORFileManager_ArchivoDUMPCaducado : TADOStoredProc;

    const
        cProcedimiento = 'ValidarDUMPCaducado';

begin
    try
        try
            lSORFileManager_ArchivoDUMPCaducado := TADOStoredProc.Create(nil);

            with lSORFileManager_ArchivoDUMPCaducado do begin

                Connection      := BaseSOR;
                ProcedureName   := 'SORFileManager_DUMPCaducadoValidar';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoArchivo').Value  := pCodigoArchivo;
                ExecProc;

                Result := Parameters.ParamByName('@RETURN_VALUE').Value;
            end;

        except
            on e: Exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    finally
        if Assigned(lSORFileManager_ArchivoDUMPCaducado) then begin
            if lSORFileManager_ArchivoDUMPCaducado.Active then lSORFileManager_ArchivoDUMPCaducado.Close;
            FreeAndNil(lSORFileManager_ArchivoDUMPCaducado);
        end;
    end;
end;


function TKapsch_SORFileManagerService.ValidarDumpDelDia(pCodigoArchivo: Int64): Boolean;
    var
        lSORFileManager_ArchivoDUMPDelDia : TADOStoredProc;

    const
        cProcedimiento = 'ValidarDUMPDelDia';

begin
    try
        try
            lSORFileManager_ArchivoDUMPDelDia := TADOStoredProc.Create(nil);

            with lSORFileManager_ArchivoDUMPDelDia do begin

                Connection      := BaseSOR;
                ProcedureName   := 'SORFileManager_DUMPDelDiaValidar';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoArchivo').Value  := pCodigoArchivo;
                ExecProc;

                Result := Parameters.ParamByName('@RETURN_VALUE').Value;
            end;

        except
            on e: Exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    finally
        if Assigned(lSORFileManager_ArchivoDUMPDelDia) then begin
            if lSORFileManager_ArchivoDUMPDelDia.Active then lSORFileManager_ArchivoDUMPDelDia.Close;
            FreeAndNil(lSORFileManager_ArchivoDUMPDelDia);
        end;
    end;
end;

procedure TKapsch_SORFileManagerService.ValidarXML(pCodigoTipoArchivo, pCodigoConcesionaria: Byte; pCodigoArchivo: Int64; pRutaArchivoCompleta: string);
    var
        lArchivosMaestro_CargarXML  : TADOStoredProc;

    const
        cProcedimiento              = 'ValidarXML';
        cErrorConcesionariaNoValida = 'La Concesionaria NO es v�lida para el tipo de XML. Archivo: %s';
        cErrorArchivoDuplicado      = 'El Archivo est� Duplicado. Archivo: %s.';
begin
    try
        try
            ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_EN_PROCESO_VALIDACION_XML);

            lArchivosMaestro_CargarXML := TADOStoredProc.Create(nil);

            with lArchivosMaestro_CargarXML do begin

                Connection      := BaseSOR;
                ProcedureName   := 'ArchivosMaestro_CargarXML';

                CommandTimeout := 0;

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoArchivo').Value          := pCodigoArchivo;
                Parameters.ParamByName('@RutaCompletaArchivo').Value    := pRutaArchivoCompleta;
                Parameters.ParamByName('@Usuario').Value                := FUsuariosistema;
                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value = 0 then begin

                    case pCodigoTipoArchivo of
                        cNovedadesContrato, cFileXML_EntradasSinProcesar, cFileXML_NovedadesSinProcesar: begin
                            if ((pCodigoTipoArchivo = cFileXML_EntradasSinProcesar) and (pCodigoConcesionaria = FConcesionariaNativa))
                                or ((pCodigoTipoArchivo = cFileXML_NovedadesSinProcesar) and (pCodigoConcesionaria <> FConcesionariaNativa))
                                or  ((pCodigoTipoArchivo = cNovedadesContrato))  then begin

                                if pCodigoTipoArchivo = cFileXML_NovedadesSinProcesar then begin

                                    if ValidarArchivoDuplicado(pCodigoArchivo) then begin

                                        ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_DUPLICADO);
                                        AddEvent(cProcedimiento, Format(cErrorArchivoDuplicado, [pRutaArchivoCompleta]), True, IntToStr(pCodigoArchivo));
                                    end
                                    else ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_GENERACION_DESPACHO);

                                end
                                else ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_GENERACION_DESPACHO);
                            end
                            else begin

                                ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_CON_ERRORES);
                                AddEvent(cProcedimiento, Format(cErrorConcesionariaNoValida, [pRutaArchivoCompleta]), True, IntToStr(pCodigoArchivo));
                            end;
                        end;

                        cFileXML_Salidas: begin
                            if pCodigoConcesionaria = FConcesionariaNativa then begin

                                if ValidarArchivoDuplicado(pCodigoArchivo) then begin

                                    ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_DUPLICADO);
                                    AddEvent(cProcedimiento, Format(cErrorArchivoDuplicado, [pRutaArchivoCompleta]), True, IntToStr(pCodigoArchivo));
                                end
                                else ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_CARGA_RESPUESTA);
                            end
                            else begin

                                ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_CON_ERRORES);
                                AddEvent(cProcedimiento, Format(cErrorConcesionariaNoValida, [pRutaArchivoCompleta]), True, IntToStr(pCodigoArchivo));
                            end;
                        end;
                    end;
                end
                else ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_CON_ERRORES);
            end;
        except
            on e: Exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    finally
        if Assigned(lArchivosMaestro_CargarXML) then begin
            if lArchivosMaestro_CargarXML.Active then lArchivosMaestro_CargarXML.Close;
            FreeAndNil(lArchivosMaestro_CargarXML);
        end;
    end;
end;


procedure TKapsch_SORFileManagerService.DescomprimirZIP(pCodigoTipoArchivo: Byte; pCodigoArchivo: Int64; pRutaArchivoCompleta: string);
    const
        cProcedimiento              = 'DescomprimirZIP';
        cErrorArchivoDuplicado      = 'El Archivo est� Duplicado. Archivo: %s.';
        cErrorDUMPCaducado          = 'El Archivo DUMP est� Caducado. La fecha es anterior a la fecha de hoy. Archivo: %s.';
        cErrorDUMPDelDia            = 'El Archivo DUMP No es del d�a en Curso. Archivo: %s.';
        cErrorDescompresion         = 'Error al descomprimir el Archivo: %s. Error: %s, %s.';
        cErrorTipoDesconocido       = 'Tipo Desconocido de Archivo Comprimido.';

    procedure Descomprime;
        var
            lZipMaster: TZipMaster;
    begin
        ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_EN_PROCESO_DESCOMPRESION);

        //AddDebug('-->' + pRutaArchivoCompleta);

        try
            try
                lZipMaster := TZipMaster.Create(Self);

                with lZipMaster do begin

                    Active      := True;
//                    DLL_Load    := True;
                    Unattended  := True;
                    ZipFileName := pRutaArchivoCompleta;
                    ExtrBaseDir := ExtractFilePath(pRutaArchivoCompleta);
                    Extract;
                end;

                if lZipMaster.ErrCode <> 0 then raise Exception.Create(Format(cErrorDescompresion, [pRutaArchivoCompleta, IntToStr(lZipMaster.ErrCode and $FFFF), lZipMaster.ErrMessage]));

                ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_CARGA_DUMP);
            except
                on e: Exception do begin
                    ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_CON_ERRORES);
                    AddEvent(cProcedimiento, e.Message, True, IntToStr(pCodigoArchivo));

                    raise Exception.Create(e.Message);
                end;
            end;
        finally
            if Assigned(lZipMaster) then begin

                lZipMaster.Trace     := False;
                lZipMaster.Verbose   := False;
                lZipMaster.DLL_Load  := False;

                lZipMaster.Free;
            end;
        end;
    end;
begin
    try
        case pCodigoTipoArchivo of
            cFileDUMP_RNUT, cFileDUMP_SAP: begin

                if ValidarArchivoDuplicado(pCodigoArchivo) then begin

                    ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_DUPLICADO);
                    AddEvent(cProcedimiento, Format(cErrorArchivoDuplicado, [pRutaArchivoCompleta]), True, IntToStr(pCodigoArchivo));
                end
                else if not ValidarDumpDelDia(pCodigoArchivo) then begin

                    ActualizaEstadoArchivo(pCodigoArchivo, CONST_ARCHIVO_ESTADO_DUMP_FECHA_NO_VALIDA);
                    AddEvent(cProcedimiento, Format(cErrorDUMPDelDia, [pRutaArchivoCompleta]), True, IntToStr(pCodigoArchivo));
                end
                else Descomprime;
            end;
        else
            raise Exception.Create(cErrorTipoDesconocido);
        end;
    except
        on e: Exception do begin
            raise Exception.Create(e.Message);
        end;
    end;
end;


function TKapsch_SORFileManagerService.MyGetFileSize(pArchivo: AnsiString): Cardinal;
    var
        lFile : File of byte;
begin
    AssignFile(lFile, pArchivo);

    try
        Reset(lFile);
        Result := system.FileSize(lFile);
    finally
        CloseFile(lFile);
    end;
end;



procedure TKapsch_SORFileManagerService.ProcesarCanalRecursoCompartido(
                pCodigoTipoArchivo,
                pCodigoCanalComunicacion,
                pCodigoCanalComunicacionTipo,
                pCodigoCanalComunicacionAccion,
                pCodigoUbicacionOrigen,
                pCodigoUbicacionDestino         : Byte;

                pRutaUbicacionOrigen,
                pRutaUbicacionDestino,
                pPrefijoArchivo,
                pSufijoArchivo,
                pExtensionArchivo               : AnsiString;
                pLargoFijo                      : Byte;

                pEliminarDespuesAccion,                                         // TASK_080_MGO_20161021
                pDirectoriosPorFecha            : Boolean);                     // TASK_080_MGO_20161021

    var
        lCodigoArchivo                          : Int64;
        lCodigoConcesionaria                    : Byte;
        lNombreArchivo,
        lRutaCompletaArchivosDownload,
        lRutaCompletaArchivoOrigen,
        lRutaCompletaArchivoDestino             : String;
        lFechaHoraGeneracionArchivo             : TDateTime;

        lFileSizeOrigen,
        lFileSizeDestino                        : Cardinal;

        lArchivosDownload                       : TSearchRec;

        lSORFileManager_CanalUploadsObtener,
        lSORFileManager_CanalDownloadsObtener   : TADOStoredProc;

    const
        cProcedimientoUpload    = 'ProcesarCanalRecursoCompartido.AccionUpload';
        cProcedimientoDownload  = 'ProcesarCanalRecursoCompartido.AccionDownload';
        cErrorCopyFile          = 'Error al copiar el Archivo %s a %s.';
        cErrorRenameFile        = 'Error al renombrar el Archivo de %s a %s.';
        cErrorDeleteFile        = 'Error al eliminar el Archivo %s.';
        cErrorSize              = 'Error al copiar el Archivo %s a %s. El tama�o del Archivo de Destino NO es igual al del Archivo de Origen.';
        cErrorFileNotExists     = 'Error al copiar el Archivo %s. El Archivo NO Existe.';
        cErrorFileName          = 'La Nomenclatura del Archivo NO es correcta. Archivo %s.';


    procedure DoDownload;
    begin
        try
            if FileExists(lRutaCompletaArchivoDestino) then DeleteFile(lRutaCompletaArchivoDestino);
            if FileExists(lRutaCompletaArchivoDestino + '_tmp') then DeleteFile(lRutaCompletaArchivoDestino + '_tmp');

            ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_EN_PROCESO_DESCARGA);

            if CopyFile(PChar(lRutaCompletaArchivoOrigen), PChar(lRutaCompletaArchivoDestino + '_tmp'), False) then begin

                lFileSizeOrigen     := MyGetFileSize(lRutaCompletaArchivoOrigen);
                lFileSizeDestino    := MyGetFileSize(lRutaCompletaArchivoDestino + '_tmp');

                if lFileSizeOrigen <> lFileSizeDestino then begin

                    AddEvent(
                        cProcedimientoDownload,
                        Format(cErrorSize, [lRutaCompletaArchivoOrigen, lRutaCompletaArchivoDestino + '_tmp']),
                        True,
                        IntToStr(lCodigoArchivo));

                    ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_DESCARGA);
                end
                else begin
                    if RenameFile(lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino) then begin

                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_DESCARGADO);

                        if pEliminarDespuesAccion then begin

                            if not DeleteFile(lRutaCompletaArchivoOrigen) then begin

                                AddEvent(
                                    cProcedimientoDownload,
                                    Format(cErrorDeleteFile, [lRutaCompletaArchivoOrigen]),
                                    True,
                                    IntToStr(lCodigoArchivo));
                            end;
                        end;

                        if (lCodigoConcesionaria = 0) or ((pLargoFijo > 0) and (Length(ExtractFileName(lRutaCompletaArchivoOrigen)) <> pLargoFijo)) then begin

                            ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_CON_ERRORES);
                            AddEvent(cProcedimientoDownload, Format(cErrorFileName, [lRutaCompletaArchivoOrigen]), True, IntToStr(lCodigoArchivo));

                            raise Exception.Create(Format(cErrorFileName, [lRutaCompletaArchivoOrigen]));
                        end
                        else begin
                            if (pExtensionArchivo = 'xml') then begin

                                ValidarXML(pCodigoTipoArchivo, lCodigoConcesionaria, lCodigoArchivo, lRutaCompletaArchivoDestino);
                            end;

                            if (pExtensionArchivo = 'zip') or (pExtensionArchivo = 'zip.txt') then begin

                                if (pExtensionArchivo = 'zip.txt') then begin

                                    RenameFile(lRutaCompletaArchivoDestino, StringReplace(lRutaCompletaArchivoDestino, '.txt', '', [rfReplaceAll]));
                                end;


                                DescomprimirZIP(pCodigoTipoArchivo, lCodigoArchivo, StringReplace(lRutaCompletaArchivoDestino, '.txt', '', [rfReplaceAll]));
                            end;
                        end;
                    end
                    else begin

                        AddEvent(
                            cProcedimientoDownload,
                            Format(cErrorRenameFile, [lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino]),
                            True,
                            IntToStr(lCodigoArchivo));

                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_DESCARGA);
                    end;
                end;
            end
            else begin

                AddEvent(
                    cProcedimientoDownload,
                    Format(cErrorCopyFile, [lRutaCompletaArchivoOrigen, lRutaCompletaArchivoDestino + '_tmp']),
                    True,
                    IntToStr(lCodigoArchivo));

                ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_DESCARGA);
            end;
        except
            on e: exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    end;

begin
    try
        case pCodigoCanalComunicacionAccion of

            cAccionUpload: try

                lSORFileManager_CanalUploadsObtener := TADOStoredProc.Create(nil);

                with lSORFileManager_CanalUploadsObtener do begin
                    Connection      := BaseSOR;
                    ProcedureName   := 'SORFileManager_CanalUploadsObtener';

                    Parameters.Refresh;
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoTipoArchivo').Value              := pCodigoTipoArchivo;
                    Parameters.ParamByName('@CodigoCanalComunicacion').Value        := pCodigoCanalComunicacion;
                    Parameters.ParamByName('@CodigoCanalComunicacionAccion').Value  := pCodigoCanalComunicacionAccion;
                    Parameters.ParamByName('@CodigoUbicacionOrigen').Value          := pCodigoUbicacionOrigen;
                    Parameters.ParamByName('@CodigoUbicacionDestino').Value         := pCodigoUbicacionDestino;

                    Open;

                    while not Eof do begin

                        lCodigoArchivo              := FieldByName('CodigoArchivo').AsVariant;
                        lNombreArchivo              := FieldByName('NombreArchivo').AsString;
                        lFechaHoraGeneracionArchivo := FieldByName('FechaHoraGeneracionArchivo').AsDateTime;

                        { INICIO : TASK_080_MGO_20161021
                        lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100) + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo;
                        }
                        if pDirectoriosPorFecha then
                            lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100) + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo
                        else
                            lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + lNombreArchivo;
                        // FIN : TASK_080_MGO_20161021
                        lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + lNombreArchivo;

                        if FileExists(lRutaCompletaArchivoDestino) then DeleteFile(lRutaCompletaArchivoDestino);
                        if FileExists(lRutaCompletaArchivoDestino + '_tmp') then DeleteFile(lRutaCompletaArchivoDestino + '_tmp');

                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_EN_PROCESO_ENVIO);

                        if FileExists(lRutaCompletaArchivoOrigen) then begin

                            if CopyFile(PChar(lRutaCompletaArchivoOrigen), PChar(lRutaCompletaArchivoDestino + '_tmp'), False) then begin

                                lFileSizeOrigen     := MyGetFileSize(lRutaCompletaArchivoOrigen);
                                lFileSizeDestino    := MyGetFileSize(lRutaCompletaArchivoDestino + '_tmp');

                                if lFileSizeOrigen <> lFileSizeDestino then begin

                                    AddEvent(
                                        cProcedimientoUpload,
                                        Format(cErrorSize, [lRutaCompletaArchivoOrigen, lRutaCompletaArchivoDestino + '_tmp']),
                                        True,
                                        IntToStr(lCodigoArchivo));

                                    ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_ENVIO);
                                end
                                else begin

                                    if RenameFile(lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino) then begin

                                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_ENVIADO);

                                        if pEliminarDespuesAccion then begin

                                            if not DeleteFile(lRutaCompletaArchivoOrigen) then begin

                                                AddEvent(
                                                    cProcedimientoUpload,
                                                    Format(cErrorDeleteFile, [lRutaCompletaArchivoOrigen]),
                                                    True,
                                                    IntToStr(lCodigoArchivo));
                                            end;
                                        end;
                                    end
                                    else begin

                                        AddEvent(
                                            cProcedimientoUpload,
                                            Format(cErrorRenameFile, [lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino]),
                                            True,
                                            IntToStr(lCodigoArchivo));

                                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_ENVIO);
                                    end;
                                end;
                            end
                            else begin

                                AddEvent(
                                    cProcedimientoUpload,
                                    Format(cErrorCopyFile, [lRutaCompletaArchivoOrigen, lRutaCompletaArchivoDestino + '_tmp']),
                                    True,
                                    IntToStr(lCodigoArchivo));

                                ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_ENVIO);
                            end;
                        end
                        else begin

                            AddEvent(
                                cProcedimientoUpload,
                                Format(cErrorFileNotExists, [lRutaCompletaArchivoOrigen]),
                                True,
                                IntToStr(lCodigoArchivo));

                            ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_NO_EXISTE);
                        end;

                        Next;
                    end;
                end;
            except
                on e: Exception do begin
                    AddEvent(cProcedimientoUpload, e.Message, True);
                end;
            end;

            cAccionDownload: try

                lSORFileManager_CanalDownloadsObtener := TADOStoredProc.Create(nil);

                with lSORFileManager_CanalDownloadsObtener do begin
                    Connection      := BaseSOR;
                    ProcedureName   := 'SORFileManager_CanalDownloadsObtener';

                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoTipoArchivo').Value              := pCodigoTipoArchivo;
                    Parameters.ParamByName('@CodigoCanalComunicacion').Value        := pCodigoCanalComunicacion;
                    Parameters.ParamByName('@CodigoCanalComunicacionAccion').Value  := pCodigoCanalComunicacionAccion;
                    Parameters.ParamByName('@CodigoUbicacionOrigen').Value          := pCodigoUbicacionOrigen;
                    Parameters.ParamByName('@CodigoUbicacionDestino').Value         := pCodigoUbicacionDestino;

                    Open;

                    while not Eof do begin

                        lCodigoArchivo              := FieldByName('CodigoArchivo').AsVariant;
                        lNombreArchivo              := FieldByName('NombreArchivo').AsString;
                        lFechaHoraGeneracionArchivo := FieldByName('FechaHoraGeneracionArchivo').AsDateTime;
                        lCodigoConcesionaria        := FieldByName('CodigoConcesionaria').AsInteger;

                        lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + lNombreArchivo;

                        if FileExists(lRutaCompletaArchivoOrigen) then begin
                            { INICIO : TASK_080_MGO_20161021
                            lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100);
                            ForceDirectories(lRutaCompletaArchivoDestino);
                            lRutaCompletaArchivoDestino  := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo;
                            }
                            if pDirectoriosPorFecha then begin
                                lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100);
                                ForceDirectories(lRutaCompletaArchivoDestino);
                                lRutaCompletaArchivoDestino  := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo;
                            end else begin
                                ForceDirectories(pRutaUbicacionDestino);
                                lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + lNombreArchivo;
                            end;
                            // FIN : TASK_080_MGO_20161021
                            DoDownload;
                        end
                        else begin

                            AddEvent(
                                cProcedimientoDownload,
                                Format(cErrorFileNotExists, [lRutaCompletaArchivoOrigen]),
                                True,
                                IntToStr(lCodigoArchivo));

                            ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_NO_EXISTE);
                        end;

                        Next;
                    end;
                end;

                lRutaCompletaArchivosDownload := pRutaUbicacionOrigen + '\' + pPrefijoArchivo + '*' + pSufijoArchivo + '.' + pExtensionArchivo;



                if FindFirst(lRutaCompletaArchivosDownload, integer(faReadOnly), lArchivosDownload) = 0 then begin

                    repeat

                        try
                            lFileSizeOrigen := MyGetFileSize(pRutaUbicacionOrigen + '\' + lArchivosDownload.Name);
                        except
                            on e: Exception do begin
                                lFileSizeOrigen := 0;
                                AddEvent(cProcedimientoDownload, Format('El archivo est� todav�a en Copia. Archivo: %s, Error: %s', [pRutaUbicacionOrigen + '\' + lArchivosDownload.Name, e.Message]), True);
                            end;
                        end;

                        //AddDebug('-->' + pRutaUbicacionOrigen + '\' + lArchivosDownload.Name + ' -- ' + IntToStr(lFileSizeOrigen));
                        if (lFileSizeOrigen <> 0) and (SecondsBetween(Now, FileDateToDateTime(lArchivosDownload.Time)) > 30) then begin

                            InsertaArchivo(
                                pCodigoTipoArchivo,
                                pCodigoCanalComunicacion,
                                pCodigoCanalComunicacionAccion,
                                pCodigoUbicacionOrigen,
                                pCodigoUbicacionDestino,
                                lArchivosDownload.Name,
                                lCodigoArchivo,
                                lFechaHoraGeneracionArchivo,
                                lCodigoConcesionaria);

                            if lCodigoArchivo > 0 then begin
                                lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + lArchivosDownload.Name;
                                { INICIO : TASK_080_MGO_20161021
                                lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100);
                                ForceDirectories(lRutaCompletaArchivoDestino);
                                lRutaCompletaArchivoDestino := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lArchivosDownload.Name;
                                }
                                if pDirectoriosPorFecha then begin
                                    lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100);
                                    ForceDirectories(lRutaCompletaArchivoDestino);
                                    lRutaCompletaArchivoDestino := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lArchivosDownload.Name;
                                end else begin
                                    ForceDirectories(pRutaUbicacionDestino);
                                    lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + lArchivosDownload.Name;
                                end;
                                // FIN : TASK_080_MGO_20161021

                                DoDownload;
                            end;
                        end;

                    until (FindNext(lArchivosDownload) <> 0);

                    FindClose(lArchivosDownload);
                end;
            except
                on e: Exception do begin
                    AddEvent(cProcedimientoDownload, e.Message, True);
                end;
            end;
        end;
    finally
        if Assigned(lSORFileManager_CanalUploadsObtener) then begin
            if lSORFileManager_CanalUploadsObtener.Active then lSORFileManager_CanalUploadsObtener.Close;
            FreeAndNil(lSORFileManager_CanalUploadsObtener);
        end;

        if Assigned(lSORFileManager_CanalDownloadsObtener) then begin
            if lSORFileManager_CanalDownloadsObtener.Active then lSORFileManager_CanalDownloadsObtener.Close;
            FreeAndNil(lSORFileManager_CanalDownloadsObtener);
        end;
    end;
end;

procedure TKapsch_SORFileManagerService.ProcesarCanalFTP(
                pCodigoTipoArchivo,
                pCodigoCanalComunicacion,
                pCodigoCanalComunicacionTipo,
                pCodigoCanalComunicacionAccion,
                pCodigoUbicacionOrigen,
                pCodigoUbicacionDestino         : Byte;

                pRutaUbicacionOrigen,
                pRutaUbicacionDestino,
                pPrefijoArchivo,
                pSufijoArchivo,
                pExtensionArchivo               : AnsiString;
                pLargoFijo                      : Byte;

                pEliminarDespuesAccion,                                         // TASK_080_MGO_20161021
                pDirectoriosPorFecha            : Boolean);                     // TASK_080_MGO_20161021

    var
        lCodigoArchivo                          : Int64;
        lCodigoConcesionaria                    : Byte;
        lNombreArchivo,
        lRutaCompletaArchivosDownload,
        lPatronArchivosDownload,
        lRutaCompletaArchivoOrigen,
        lRutaCompletaArchivoDestino             : String;
        lFechaHoraGeneracionArchivo             : TDateTime;

        lFileSizeOrigen,
        lFileSizeDestino                        : Cardinal;
    	lFileFTP                                : TFileStream;

        lSORFileManager_CanalUploadsObtener,
        lSORFileManager_CanalDownloadsObtener   : TADOStoredProc;

        lFilesInFTP                             : TStringList;
        lVeces                                  : Integer;

    const
        cProcedimientoUpload    = 'ProcesarCanalFTP.AccionUpload';
        cProcedimientoDownload  = 'ProcesarCanalFTP.AccionDownload';

        cErrorCopyFile          = 'Error al copiar el Archivo %s a %s.';
        cErrorRenameFile        = 'Error al renombrar el Archivo de %s a %s.';
        cErrorDeleteFile        = 'Error al eliminar el Archivo %s.';
        cErrorSize              = 'Error al copiar el Archivo %s a %s. El tama�o del Archivo de Destino NO es igual al del Archivo de Origen.';
        cErrorFileNotExists     = 'Error al copiar el Archivo %s. El Archivo NO Existe.';

        cErrorFileNomenclature  = 'El Largo de la Nomenclatura del Archivo NO';

    procedure DoDownload;
    begin
        try
            if FileExists(lRutaCompletaArchivoDestino) then DeleteFile(lRutaCompletaArchivoDestino);
            if FileExists(lRutaCompletaArchivoDestino + '_tmp') then DeleteFile(lRutaCompletaArchivoDestino + '_tmp');

            ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_EN_PROCESO_DESCARGA);

            IdFTP1.Get(lRutaCompletaArchivoOrigen, lRutaCompletaArchivoDestino + '_tmp', True, False );

            lFileSizeOrigen     := IdFTP1.Size(lRutaCompletaArchivoOrigen);
            lFileSizeDestino    := MyGetFileSize(lRutaCompletaArchivoDestino + '_tmp');

            if lFileSizeOrigen <> lFileSizeDestino then begin

                AddEvent(
                    cProcedimientoDownload,
                    Format(cErrorSize, [lRutaCompletaArchivoOrigen, lRutaCompletaArchivoDestino + '_tmp']),
                    True,
                    IntToStr(lCodigoArchivo));

                ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_DESCARGA);
            end
            else begin
                if RenameFile(lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino) then begin

                    ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_DESCARGADO);

                    if pEliminarDespuesAccion then try

                        IdFTP1.Delete(lRutaCompletaArchivoOrigen);
                    except
                        on e: Exception do begin

                            AddEvent(
                                cProcedimientoDownload,
                                Format(cErrorDeleteFile, [lRutaCompletaArchivoOrigen]),
                                True,
                                IntToStr(lCodigoArchivo));
                        end;
                    end;

                    if (pExtensionArchivo = 'xml') then begin

                        ValidarXML(pCodigoTipoArchivo, lCodigoConcesionaria, lCodigoArchivo, lRutaCompletaArchivoDestino);
                    end;

                    if (pExtensionArchivo = 'zip') or (pExtensionArchivo = 'zip.txt') then begin

                        if (pExtensionArchivo = 'zip.txt') then begin

                            RenameFile(lRutaCompletaArchivoDestino, StringReplace(lRutaCompletaArchivoDestino, '.txt', '', [rfReplaceAll]));
                        end;

//                        DescomprimirZIP(pCodigoTipoArchivo, lCodigoArchivo, lRutaCompletaArchivoDestino);
                        DescomprimirZIP(pCodigoTipoArchivo, lCodigoArchivo, StringReplace(lRutaCompletaArchivoDestino, '.txt', '', [rfReplaceAll]));
                    end;
                end
                else begin

                    AddEvent(
                        cProcedimientoDownload,
                        Format(cErrorRenameFile, [lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino]),
                        True,
                        IntToStr(lCodigoArchivo));

                    ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_DESCARGA);
                end;
            end;
        except
            on e: exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    end;

begin
    try
        case pCodigoCanalComunicacionAccion of

            cAccionUpload: try

                lSORFileManager_CanalUploadsObtener := TADOStoredProc.Create(nil);

                with lSORFileManager_CanalUploadsObtener do begin
                    Connection      := BaseSOR;
                    ProcedureName   := 'SORFileManager_CanalUploadsObtener';

                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoTipoArchivo').Value              := pCodigoTipoArchivo;
                    Parameters.ParamByName('@CodigoCanalComunicacion').Value        := pCodigoCanalComunicacion;
                    Parameters.ParamByName('@CodigoCanalComunicacionAccion').Value  := pCodigoCanalComunicacionAccion;
                    Parameters.ParamByName('@CodigoUbicacionOrigen').Value          := pCodigoUbicacionOrigen;
                    Parameters.ParamByName('@CodigoUbicacionDestino').Value         := pCodigoUbicacionDestino;

                    Open;

                    try
                        //ActivarDesactivarAbortFTP(True);
                        if not eof then ConectarFTP(pCodigoCanalComunicacion);
                    finally
                        //ActivarDesactivarAbortFTP(False);
                    end;

                    while not Eof do begin

                        lCodigoArchivo              := FieldByName('CodigoArchivo').AsVariant;
                        lNombreArchivo              := FieldByName('NombreArchivo').AsString;
                        lFechaHoraGeneracionArchivo := FieldByName('FechaHoraGeneracionArchivo').AsDateTime;

                        { INICIO : TASK_080_MGO_20161021
                        lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100) + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo;
                        }
                        if pDirectoriosPorFecha then
                            lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100) + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo
                        else
                            lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '\' + lNombreArchivo;
                        // FIN : TASK_080_MGO_20161021
                        
                        lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '/' + lNombreArchivo;

                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_EN_PROCESO_ENVIO);

                        if FileExists(lRutaCompletaArchivoOrigen) then begin

                            try
                                //ActivarDesactivarAbortFTP(True);

                                if IdFTP1.Size(lRutaCompletaArchivoDestino) <> -1 then IdFTP1.Delete(lRutaCompletaArchivoDestino);
                                if IdFTP1.Size(lRutaCompletaArchivoDestino + '_tmp') <> -1 then IdFTP1.Delete(lRutaCompletaArchivoDestino + '_tmp');

                                try
                                    lFileFTP := TFileStream.Create(lRutaCompletaArchivoOrigen, fmOpenRead);
                                    IdFTP1.Put(lFileFTP, lRutaCompletaArchivoDestino + '_tmp', False);
                                finally
                                    if Assigned(lFileFTP) then FreeAndNil(lFileFTP);
                                end;

                                lFileSizeOrigen     := MyGetFileSize(lRutaCompletaArchivoOrigen);
                                lFileSizeDestino    := IdFTP1.Size(lRutaCompletaArchivoDestino + '_tmp');

                                if lFileSizeOrigen <> lFileSizeDestino then begin

                                    AddEvent(
                                        cProcedimientoUpload,
                                        Format(cErrorSize, [lRutaCompletaArchivoOrigen, lRutaCompletaArchivoDestino + '_tmp']),
                                        True,
                                        IntToStr(lCodigoArchivo));

                                    ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_ENVIO);

                                    IdFTP1.Delete(lRutaCompletaArchivoDestino + '_tmp');
                                end
                                else begin

                                    IdFTP1.Rename(lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino);
                                    lFileSizeDestino    := IdFTP1.Size(lRutaCompletaArchivoDestino);

                                    if lFileSizeOrigen = lFileSizeDestino then begin

                                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_ENVIADO);

                                        if pEliminarDespuesAccion then begin

                                            if not DeleteFile(lRutaCompletaArchivoOrigen) then begin

                                                AddEvent(
                                                    cProcedimientoUpload,
                                                    Format(cErrorDeleteFile, [lRutaCompletaArchivoOrigen]),
                                                    True,
                                                    IntToStr(lCodigoArchivo));
                                            end;
                                        end;
                                    end
                                    else begin

                                        AddEvent(
                                            cProcedimientoUpload,
                                            Format(cErrorRenameFile, [lRutaCompletaArchivoDestino + '_tmp', lRutaCompletaArchivoDestino]),
                                            True,
                                            IntToStr(lCodigoArchivo));

                                        ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_PENDIENTE_ENVIO);

                                        IdFTP1.Delete(lRutaCompletaArchivoDestino + '_tmp');
                                    end;
                                end;
                            finally
                                //ActivarDesactivarAbortFTP(False);
                            end;
                        end
                        else begin

                            AddEvent(
                                cProcedimientoUpload,
                                Format(cErrorFileNotExists, [lRutaCompletaArchivoOrigen]),
                                True,
                                IntToStr(lCodigoArchivo));

                            ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_NO_EXISTE);
                        end;

                        Next;
                    end;
                end;
            except
                on e: Exception do begin
                    //ActivarDesactivarAbortFTP(False);
                    AddEvent(cProcedimientoUpload, e.Message, True);
                end;
            end;

            cAccionDownload: try

                //AddDebug('cAccionDownload');

                lSORFileManager_CanalDownloadsObtener := TADOStoredProc.Create(nil);

                with lSORFileManager_CanalDownloadsObtener do begin
                    Connection      := BaseSOR;
                    ProcedureName   := 'SORFileManager_CanalDownloadsObtener';

                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoTipoArchivo').Value              := pCodigoTipoArchivo;
                    Parameters.ParamByName('@CodigoCanalComunicacion').Value        := pCodigoCanalComunicacion;
                    Parameters.ParamByName('@CodigoCanalComunicacionAccion').Value  := pCodigoCanalComunicacionAccion;
                    Parameters.ParamByName('@CodigoUbicacionOrigen').Value          := pCodigoUbicacionOrigen;
                    Parameters.ParamByName('@CodigoUbicacionDestino').Value         := pCodigoUbicacionDestino;

                    Open;

                    try
                        //ActivarDesactivarAbortFTP(True);
                        if not eof then ConectarFTP(pCodigoCanalComunicacion);
                    finally
                        //ActivarDesactivarAbortFTP(False);
                    end;

                    while not Eof do begin

                        lCodigoArchivo              := FieldByName('CodigoArchivo').AsVariant;
                        lNombreArchivo              := FieldByName('NombreArchivo').AsString;
                        lFechaHoraGeneracionArchivo := FieldByName('FechaHoraGeneracionArchivo').AsDateTime;
                        lCodigoConcesionaria        := FieldByName('CodigoConcesionaria').AsInteger;

                        lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '/' + lNombreArchivo;

                        try
                            //ActivarDesactivarAbortFTP(True);

                            if IdFTP1.Size(lRutaCompletaArchivoOrigen) <> -1 then begin
                                { INICIO : TASK_080_MGO_20161021
                                lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100);
                                ForceDirectories(lRutaCompletaArchivoDestino);
                                lRutaCompletaArchivoDestino  := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo;
                                }
                                if pDirectoriosPorFecha then begin
                                    lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo) + '\' + IntToStr(lCodigoArchivo mod 100);
                                    ForceDirectories(lRutaCompletaArchivoDestino);
                                    lRutaCompletaArchivoDestino  := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lNombreArchivo;
                                end else begin
                                    ForceDirectories(pRutaUbicacionDestino);
                                    lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + lNombreArchivo;
                                end;
                                // FIN : TASK_080_MGO_20161021

                                DoDownload;
                            end
                            else begin

                                AddEvent(
                                    cProcedimientoDownload,
                                    Format(cErrorFileNotExists, [lRutaCompletaArchivoOrigen]),
                                    True,
                                    IntToStr(lCodigoArchivo));

                                ActualizaEstadoArchivo(lCodigoArchivo, CONST_ARCHIVO_ESTADO_NO_EXISTE);
                            end;
                        finally
                            //ActivarDesactivarAbortFTP(False);
                        end;

                        Next;
                    end;
                end;

                try
                    //ActivarDesactivarAbortFTP(True);

                    if (not Assigned(IdFTP1)) or (Assigned(IdFTP1) and (not IdFTP1.Connected)) then ConectarFTP(pCodigoCanalComunicacion);

                    lPatronArchivosDownload         := pPrefijoArchivo + '*' + pSufijoArchivo + '.' + pExtensionArchivo;
                    lRutaCompletaArchivosDownload   := pRutaUbicacionOrigen + '/';

                    lFilesInFTP := TStringList.Create;

                    AddDebug('Listando FTP');
                    IdFTP1.List(lFilesInFTP, lRutaCompletaArchivosDownload, False);
                finally
                    //ActivarDesactivarAbortFTP(False);
                end;


                if lFilesInFTP.Count > 0 then begin

                    AddDebug(IntToStr(lFilesInFTP.Count) + '  Archivos Encontrados. Patr�n: ' + lPatronArchivosDownload);

                    for lVeces := 0 to lFilesInFTP.Count - 1 do begin

                        lFilesInFTP.Strings[lVeces] := Copy(lFilesInFTP.Strings[lVeces], Length(lRutaCompletaArchivosDownload) + 1, Length(lFilesInFTP.Strings[lVeces]) - Length(lRutaCompletaArchivosDownload)); 

                        if ValidarArchivoPorPatron(lFilesInFTP.Strings[lVeces], lPatronArchivosDownload) then begin

                            InsertaArchivo(
                                pCodigoTipoArchivo,
                                pCodigoCanalComunicacion,
                                pCodigoCanalComunicacionAccion,
                                pCodigoUbicacionOrigen,
                                pCodigoUbicacionDestino,
                                lFilesInFTP.Strings[lVeces],
                                lCodigoArchivo,
                                lFechaHoraGeneracionArchivo,
                                lCodigoConcesionaria);

                            if lCodigoArchivo > 0 then begin
                                lRutaCompletaArchivoOrigen  := pRutaUbicacionOrigen + '/' + lFilesInFTP.Strings[lVeces];
                                { INICIO : TASK_080_MGO_20161021
                                lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo)+ '\' + IntToStr(lCodigoArchivo mod 100);
                                ForceDirectories(lRutaCompletaArchivoDestino);
                                lRutaCompletaArchivoDestino := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lFilesInFTP.Strings[lVeces];
                                }
                                if pDirectoriosPorFecha then begin
                                    lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + FormatDateTime('YYYY\MM\DD', lFechaHoraGeneracionArchivo)+ '\' + IntToStr(lCodigoArchivo mod 100);
                                    ForceDirectories(lRutaCompletaArchivoDestino);
                                    lRutaCompletaArchivoDestino := lRutaCompletaArchivoDestino + '\' + IntToStr(lCodigoArchivo) + '_' + lFilesInFTP.Strings[lVeces];
                                end else begin
                                    ForceDirectories(pRutaUbicacionDestino);
                                    lRutaCompletaArchivoDestino := pRutaUbicacionDestino + '\' + lFilesInFTP.Strings[lVeces];
                                end;
                                // FIN : TASK_080_MGO_20161021

                                try
                                    //ActivarDesactivarAbortFTP(True);
                                    DoDownload;
                                finally
                                    //ActivarDesactivarAbortFTP(False);
                                end;
                            end;
                        end;
                    end;
                end;
            except
                on e: Exception do begin
                    //ActivarDesactivarAbortFTP(False);
                    AddEvent(cProcedimientoDownload, e.Message, True);
                end;
            end;
        end;

    finally
        //ActivarDesactivarAbortFTP(False);

        if Assigned(IdFTP1) then begin
            //if IdFTP1.Connected then IdFTP1.Disconnect;
            EliminaClienteFTP;
        end;
        if Assigned(lFilesInFTP) then FreeAndNil(lFilesInFTP);

        if Assigned(lSORFileManager_CanalUploadsObtener) then begin
            if lSORFileManager_CanalUploadsObtener.Active then lSORFileManager_CanalUploadsObtener.Close;
            FreeAndNil(lSORFileManager_CanalUploadsObtener);
        end;

        if Assigned(lSORFileManager_CanalDownloadsObtener) then begin
            if lSORFileManager_CanalDownloadsObtener.Active then lSORFileManager_CanalDownloadsObtener.Close;
            FreeAndNil(lSORFileManager_CanalDownloadsObtener);
        end;
    end;
end;

procedure TKapsch_SORFileManagerService.ConectarFTP(pCodigoCanalComunicacion: Byte);
    var
        lSORFileManager_CanalParametrosFTPObtener: TADOStoredProc;

    const
        cErrorParametrosFTPNOExisten = 'NO exiten Par�metros FTP para el Canal de Comunicaci�n. C�digo Canal %d.';
begin
    try
        try
            if Assigned(IdFTP1) then EliminaClienteFTP;
            IdFTP1 := TIdFTP.Create(nil);
            AddDebug('Componente Creado');
            IdFTP1.ReadTimeout      := 30000;
            IdFTP1.TransferTimeout  := 30000;

            lSORFileManager_CanalParametrosFTPObtener := TADOStoredProc.Create(nil);

            with lSORFileManager_CanalParametrosFTPObtener do begin
                Connection      := BaseSOR;
                ProcedureName   := 'SORFileManager_CanalParametrosFTPObtener';

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoCanalComunicacion').Value := pCodigoCanalComunicacion;
                Open;

                if Recordset.RecordCount = 1 then begin
                    with IdFTP1 do begin

                        Host        := FieldByName('FTPServer').AsString;
                        Username    := FieldByName('FTPUser').AsString;
                        Password    := FieldByName('FTPPassword').AsString;
                        Port        := FieldByName('FTPPort').AsInteger;
                        Passive     := FieldByName('FTPPassive').AsBoolean;

                        if FieldByName('FTPBinary').AsBoolean then begin
                            TransferType := ftBinary
                        end
                        else TransferType := ftASCII;

                        Connect;
                        AddDebug('Conectado al FTP ' + Host + ':' + IntToStr(Port) + '@' + Username);
                    end;
                end
                else raise Exception.Create(Format(cErrorParametrosFTPNOExisten, [pCodigoCanalComunicacion]));
            end;
        except
            on e: exception do begin
                AddDebug('Error en ConectarFTP');
                IdFtp1.Abort;
                raise Exception.Create(e.Message)
            end;
        end;
    finally
        if Assigned(lSORFileManager_CanalParametrosFTPObtener) then begin
            if lSORFileManager_CanalParametrosFTPObtener.Active then lSORFileManager_CanalParametrosFTPObtener.Close;
            FreeAndNil(lSORFileManager_CanalParametrosFTPObtener);
        end;
    end;
end;

end.
