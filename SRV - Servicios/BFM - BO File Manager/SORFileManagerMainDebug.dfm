object SORFileManagerMainDebug1: TSORFileManagerMainDebug1
  Left = 0
  Top = 0
  ClientHeight = 390
  ClientWidth = 702
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 408
    Top = 248
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object BaseSOR: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=Kapsch2016;Persist Security Info=Tr' +
      'ue;User ID=sa;Initial Catalog=BO_SOR;Data Source=172.20.23.15\bo' +
      '_desarrollo'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 80
    Top = 100
  end
  object BaseEventos: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=Kapsch2016;Persist Security Info=Tr' +
      'ue;User ID=sa;Initial Catalog=BO_Eventos;Data Source=172.20.23.1' +
      '5\bo_desarrollo'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 152
    Top = 100
  end
  object tmr_EjecutarProcesos: TTimer
    Enabled = False
    OnTimer = tmr_EjecutarProcesosTimer
    Left = 160
    Top = 24
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 312
    Top = 96
  end
  object tmr_CancelFTP: TTimer
    Enabled = False
    Interval = 300000
    Left = 160
    Top = 184
  end
end
