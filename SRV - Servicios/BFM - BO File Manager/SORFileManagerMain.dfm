object Kapsch_SORFileManagerService: TKapsch_SORFileManagerService
  OldCreateOrder = False
  OnCreate = ServiceCreate
  DisplayName = 'Kapsch - SOR File Manager'
  StartType = stManual
  BeforeInstall = ServiceBeforeInstall
  AfterInstall = ServiceAfterInstall
  AfterUninstall = ServiceAfterUninstall
  OnStart = ServiceStart
  OnStop = ServiceStop
  Height = 291
  Width = 414
  object BaseSOR: TADOConnection
    LoginPrompt = False
    Left = 80
    Top = 100
  end
  object BaseEventos: TADOConnection
    LoginPrompt = False
    Left = 152
    Top = 100
  end
  object tmr_EjecutarProcesos: TTimer
    Enabled = False
    OnTimer = tmr_EjecutarProcesosTimer
    Left = 160
    Top = 24
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 288
    Top = 216
  end
  object tmr_CancelFTP: TTimer
    Enabled = False
    Interval = 300000
    OnTimer = tmr_CancelFTPTimer
    Left = 160
    Top = 184
  end
end
