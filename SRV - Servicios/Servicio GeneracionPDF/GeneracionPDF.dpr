program GeneracionPDF;

uses
  Forms,
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  ReporteNC in '..\..\Comunes\ReporteNC.pas',
  ReporteFactura in '..\..\Comunes\ReporteFactura.pas' {frmReporteFactura},
  ImprimirWO in '..\..\Comunes\ImprimirWO.pas' {frmImprimirWO},
  PeaTypes in '..\..\Comunes\PeaTypes.pas',
  frmMain in 'frmMain.pas' {frmMainForm},
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  ReporteCK in '..\..\FAC - Facturacion\ReporteCK.pas' {frmReporteCK},
  MensajesEmail in '..\..\Comunes\MensajesEmail.pas',
  dmMensaje in '..\..\Comunes\dmMensaje.pas' {dmMensajes: TDataModule},
  frmReporteBoletaFacturaElectronica in '..\..\Comunes\frmReporteBoletaFacturaElectronica.pas' {ReporteBoletaFacturaElectronicaForm},
  DTEControlDLL in '..\..\FAC - Facturacion\DBNet\DTEControlDLL.pas',
  frmReporteNotaCreditoElectronica in '..\..\Comunes\frmReporteNotaCreditoElectronica.pas' {ReporteNotaCreditoElectronicaForm},
  frmReporteContratoAdhesionPA in '..\..\Comunes\frmReporteContratoAdhesionPA.pas' {ReporteContratoAdhesionPAForm},
  PeaProcsCN in '..\..\Comunes\PeaProcsCN.pas',
  SysUtilsCN in '..\..\Comunes\SysUtilsCN.pas',
  frmMuestraMensaje in '..\..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  MsgBoxCN in '..\..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  frmBloqueosSistema in '..\..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  RStrings in '..\..\Comunes\RStrings.pas',
  ImgTypes in '..\..\Comunes\ImgTypes.pas',
  PeaProcs in '..\..\Comunes\PeaProcs.pas',
  MaskCombo in '..\..\Componentes\MaskCombo\MaskCombo.pas',
  Aviso in '..\..\Comunes\Avisos\Aviso.pas',
  AvisoTagVencidos in '..\..\Comunes\Avisos\AvisoTagVencidos.pas',
  FactoryINotificacion in '..\..\Comunes\Avisos\FactoryINotificacion.pas',
  frmVentanaAviso in '..\..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  Notificacion in '..\..\Comunes\Avisos\Notificacion.pas',
  NotificacionImpresionDoc in '..\..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  NotificacionImpresionReporte in '..\..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  TipoFormaAtencion in '..\..\Comunes\Avisos\TipoFormaAtencion.pas',
  RVMBind in '..\..\Comunes\RVMBind.pas',
  RVMClient in '..\..\Comunes\RVMClient.pas',
  RVMLogin in '..\..\Comunes\RVMLogin.pas' {frmLogin},
  RVMTypes in '..\..\Comunes\RVMTypes.pas',
  DMComunicacion in '..\..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  EncriptaRijandel in '..\..\Comunes\EncriptaRijandel.pas',
  AES in '..\..\Comunes\AES.pas',
  base64 in '..\..\Comunes\base64.pas',
  ClaveValor in '..\..\Comunes\ClaveValor.pas',
  Diccionario in '..\..\Comunes\Diccionario.pas',
  frmMuestraPDF in '..\..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  FrmRptInformeUsuario in '..\..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  FrmRptInformeUsuarioConfig in '..\..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  UtilReportes in '..\..\Comunes\UtilReportes.pas',
  frmMensajeACliente in '..\..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente},
  Crypto in '..\..\Comunes\Crypto.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDMConnections, DMConnections);
  Application.CreateForm(TfrmMainForm, frmMainForm);
  if not frmMainForm.Inicializar then frmMainform.Close;
  Application.Run;
end.
