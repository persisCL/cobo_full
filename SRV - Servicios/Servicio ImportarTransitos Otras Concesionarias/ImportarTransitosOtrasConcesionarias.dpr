program ImportarTransitosOtrasConcesionarias;

uses
  Forms,
  frmMain in 'frmMain.pas' {MainForm},
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  Crypto in '..\..\Comunes\Crypto.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDMConnections, DMConnections);
  if not MainForm.Inicializar then MainForm.Close;
  
  Application.Run;
end.
