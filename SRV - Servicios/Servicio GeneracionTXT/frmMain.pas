{********************************** File Header ********************************
Author      : Nelson Droguett Sierra
Date        : 05-Febrero-2010
Description : Servicio de creacion de archivos TXT con los detalles de los
                comprobantes para ser enviados por el ServicioEnvioMail

Revision  :2
Author      : Nelson Droguett Sierra
Date        : 22-Febrero-2010
Description : Se modifica la carga del flag de LOG, para que solo lea el .INI al
              momento de iniciar el servicio y no cada vez que se quiera escribir
              en el log, esto para disminuir el acceso a la unidad de red donde se
             guarda el LOG.
             Se agrega ademas el evento ServiceStop , para realizar tareas en la
             detencion del servicio. (Cerrar el archivo de log)

Revision  :3
Author      : Nelson Droguett Sierra
Date        : 17-Abril-2010
Description : Se obtiene el intervalo en segundos para la ejecucion desde el
        	parametro general GENERACIONTXT_INTERVALOSEGUNDOS

Revision  	: 4
Author      : Nelson Droguett Sierra
Date        : 28-Abril-2010
Description : Informar en el EVENT VIEWER no solo los errores sino tambien los
                exitos (ej, cuando parte el servicio)

Revision	: 5
Author		: Nelson Droguett Sierra
Date		: 07-Junio-2010
Description	: Se agrega registro en el log de eventos cada vez que se inicia y
            	se detiene el servicio por parte del usuario.
                Se registra en el visor de eventos el inicio del servicio cuando
                se ejecuta el evento ServiceStart

Revision	: 6
Author		: Nelson Droguett Sierra
Date		: 16-Noviembre-2010
Description	: Se registra en el visor de eventos los errores que caen en el Except
Firma		: SS-601-NDR-20101116
*******************************************************************************}
unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  DpsServ, DMConnection, UtilProc, Util, EventLog, ExtCtrls, ADODB,DB, PeaTypes,
  ConstParametrosGenerales, UtilDB, StrUtils ;
type
  TfrmMainForm = class(TForm)
    Service: TDPSService;
    tmrReporte: TTimer;
    spReporte: TADOStoredProc;
    BaseAUTORIZADA: TADOConnection;
    procedure tmrReporteTimer(Sender: TObject);
    procedure ServiceStart(Sender: TObject; var Started: Boolean; var ExitCode,
      SpecificExitCode: Cardinal);
    procedure ServiceAfterInstall(Sender: TObject);
    procedure ServiceAfterUninstall(Sender: TObject);
    procedure ServiceStop(Sender: TObject);
  private
    FServiceName: String;
    FIntervalo:  Integer;
    FDirectorio : String;
    FMensajeActual: Integer;
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra -------------------------------
    bDebeEscribirLog : Boolean;
    LogFile: TextFile;
    // --------------------------------------------------------------------------------
    function ConectarBase: Boolean;
    function ConfigurarParametros: Boolean;
    function ProcesarReportes: Boolean;
    procedure LogAction(Mensaje: string);
  public
    function Inicializar: Boolean;
  end;

var
  frmMainform: TfrmMainForm;

implementation

{$R *.dfm}



function TfrmMainForm.ConectarBase: Boolean;
resourcestring
    MSG_CONNECT_ERROR = 'Ha ocurrido un error conectando a la base de datos. Detalle: %s';
    MSG_ERROR_LOADING_PARAMETERS    = 'Error cargando los par�metros generales.';
    MSG_ERROR_USER_AND_PASSWORD_DOES_NOT_EXISTS = 'Los parametros USUARIO_BD_JORDAN y PASSWORD_BD_JORDAN no existen en la base de datos.';
var
    sUsuarioBDJordan    : String;
    sPasswordBDJordan   : String;
    bUserOK,bPasswordOK  : Boolean;
begin
    Result      := False;
    bUserOK     := ObtenerParametroGeneral(DMConnections.BaseCAC, 'USUARIO_BD_JORDAN', sUsuarioBDJordan);
    bPasswordOK := ObtenerParametroGeneral(DMConnections.BaseCAC, 'PASSWORD_BD_JORDAN', sPasswordBDJordan);
    if bUserOK AND bPasswordOK then begin
        try
            BaseAUTORIZADA.Connected:=False;
            BaseAUTORIZADA.ConnectionString := DMConnections.BaseCAC.ConnectionString;
            BaseAUTORIZADA.ConnectionString := AnsiReplaceStr(BaseAUTORIZADA.ConnectionString,'User ID=usr_op','User ID='+Trim(sUsuarioBDJordan));
            BaseAUTORIZADA.ConnectionString := AnsiReplaceStr(BaseAUTORIZADA.ConnectionString,'Password=usr_op','Password='+Trim(sPasswordBDJordan));
            if Pos('Password',BaseAUTORIZADA.ConnectionString) = 0 then                                                             // SS_1041_PDO_20120424
            begin                                                                                                                   // SS_1041_PDO_20120424
                BaseAUTORIZADA.ConnectionString := BaseAUTORIZADA.ConnectionString + ';' + 'Password='+Trim(sPasswordBDJordan);     // SS_1041_PDO_20120424
            end;                                                                                                                    // SS_1041_PDO_20120424
            BaseAUTORIZADA.Connected:=True;
            Result := True;
        except
            on e: Exception do begin
                LogAction(Format(MSG_CONNECT_ERROR, [e.Message]));
                EventLogReportEvent(elError, Format(MSG_CONNECT_ERROR, [e.Message]), '');
            end;
        end;
    end
    else
    begin
      LogAction(MSG_ERROR_LOADING_PARAMETERS);
      //Rev.6 / 16-Noviembre-2010 / Nelson Droguett Sierra --------------------------------------------------
      EventLogReportEvent(elError, Format(MSG_CONNECT_ERROR, [DMConnections.BaseCAC.ConnectionString]), ''); 			//SS-601-NDR-20101116
      msgBoxErr(MSG_ERROR_LOADING_PARAMETERS , MSG_ERROR_USER_AND_PASSWORD_DOES_NOT_EXISTS, caption, MB_ICONERROR);
      exit;
    end;
end;


function TfrmMainForm.ConfigurarParametros: Boolean;
begin
	//Rev.6 / 16-Noviembre-2010 / Nelson Droguett Sierra---------------------------------------------------------------------------
	try                                                                                                                            //SS-601-NDR-20101116
      Result := False;
      // Rev.3 / 16-Abril-2010 / Nelson Droguett Sierra
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'GENERACIONTXT_INTERVALOSEGUNDOS', FIntervalo) then Exit;
      if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_EMAILS_ADJUNTOS_TXT', FDirectorio) then Exit;      // SS_1041_PDO_20120424
      if FDirectorio = ''  then FDirectorio := '.';
      FDirectorio := GoodDir(FDirectorio);
      if not (DirectoryExists(FDirectorio)) then begin
          EventLogReportEvent(elError, 'El Directorio ' + FDirectorio + ' que se va a utlizar para gurdar los TXT no se encuentra o no est� accesible', '');
          LogAction('El Directorio ' + FDirectorio + ' que se va a utlizar para gurdar los TXT no se encuentra o no est� accesible');
          Exit;
      end;
      Result := True;
    except                                                                                                                         //SS-601-NDR-20101116
      on e: Exception do begin                                                                                                     //SS-601-NDR-20101116
          EventLogReportEvent(elError, 'El Directorio ' + FDirectorio +                                                            //SS-601-NDR-20101116
                              ' que se va a utlizar para gurdar los TXT no se encuentra o no est� accesible. Detalle : ' +         //SS-601-NDR-20101116
                              e.Message, '');                                                                                      //SS-601-NDR-20101116
      end;                                                                                                                         //SS-601-NDR-20101116
    end;                                                                                                                           //SS-601-NDR-20101116
end;

function TfrmMainForm.Inicializar: Boolean;
resourcestring
  MSG_INSTALL = 'Instalar';
  MSG_UNINSTALL = 'Desinstalar';
  MSG_SERVICE_SUCCESS_INSTALLING = 'Servicio instalado con �xito';
  MSG_SERVICE_ERROR_INSTALLING = 'Error al instalar el Servicio %s:' + CRLF + '%s';
  MSG_SERVICE_SUCCESS_UNINSTALLING = 'Servicio desinstalado con �xito';
  MSG_SERVICE_ERROR_UNINSTALLING = 'Error al desinstalar el Servicio %s:' + CRLF + '%s';
  MSG_SERVICE_CANNOT_START = 'El servicio no pudo iniciarse debido al siguiente Error: %s';
var
	ErrorCode: DWORD;
    NumeroComprobante: Int64;
    DescriErr: string;
begin
    DMConnections.SetUp;
    FServiceName := Service.ServiceName;
	Result := False;
	if FindCmdLineSwitch('install', ['/', '-'], True) then begin
		// Instalaci�n
		if InstallServices(ErrorCode, FServiceName) then begin
			MsgBox(MSG_SERVICE_SUCCESS_INSTALLING, MSG_INSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_INSTALLING , '');
		end else begin
			MsgBox(Format(MSG_SERVICE_ERROR_INSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]),
			  MSG_INSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_INSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]) , '');
		end;
	end else if FindCmdLineSwitch('uninstall', ['/', '-'], True) then begin
		// Desinstalaci�n
		if UninstallServices(ErrorCode, FServiceName) then begin
			MsgBox(MSG_SERVICE_SUCCESS_UNINSTALLING, MSG_UNINSTALL, MB_ICONINFORMATION);
            EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_UNINSTALLING , '');
		end else begin
			MsgBox(Format(MSG_SERVICE_ERROR_UNINSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]),
			  MSG_UNINSTALL, MB_ICONSTOP);
            EventLogReportEvent(elError, Format(MSG_SERVICE_ERROR_UNINSTALLING,
			  [FServiceName, SysErrorMessage(ErrorCode)]) , '');
		end;
	end else begin
		Result := StartServices;
		if Result then begin
			if not IsServiceApplication then Show;
		end else begin
			EventLogReportEvent(elError,
              Format(MSG_SERVICE_CANNOT_START, [SysErrorMessage(GetLastError())]), '');
		end;
    end;
end;


function TfrmMainForm.ProcesarReportes: Boolean;
resourcestring
    MSG_GET_REPORT_JOB_ERROR = 'Ha ocurrido un error obteniendo reportes de detalles de comprobantes';
begin
    Result := False;
    try
        spReporte.Close;
        spReporte.Connection := BaseAUTORIZADA;
        spReporte.ProcedureName := 'GeneraDetalleTransitosXemail';
        spReporte.ExecProc;
        Result := True;
    except
        on e: Exception do begin
            EventLogReportEvent(elError, MSG_GET_REPORT_JOB_ERROR + ' : ' + e.Message, '');
        end;
    end;
end;

procedure TfrmMainForm.ServiceAfterInstall(Sender: TObject);
begin
    EventLogRegisterSource;
end;

procedure TfrmMainForm.ServiceAfterUninstall(Sender: TObject);
begin
    EventLogUnregisterSource;
end;

procedure TfrmMainForm.ServiceStart(Sender: TObject; var Started: Boolean;
  var ExitCode, SpecificExitCode: Cardinal);
//Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
resourcestring
    MSG_SERVICE_SUCCESS_STARTED = 'Servicio iniciado con �xito';
//FinRev.5-------------------------------------------------------------------------
begin
    MoveToMyOwnDir;
    FMensajeActual := 0;
    tmrReporte.Enabled := True;
    Started := True;
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra -------------------------------
    bDebeEscribirLog:=InstallIni.ReadInteger('LogFile', 'LogToFile', 1) = 1;
    AssignFile( LogFile , 'GeneracionTXT.Log');
    if not FileExists('GeneracionTXT.Log') then ReWrite(LogFile);
    Append(LogFile);
    //---------------------------------------------------------------------------------
    //Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
    LogAction(MSG_SERVICE_SUCCESS_STARTED);
	EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_STARTED , '');
    //FinRev.5-------------------------------------------------------------------------
end;


procedure TfrmMainForm.ServiceStop(Sender: TObject);
//Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
resourcestring
    MSG_SERVICE_SUCCESS_STOPED = 'Servicio detenido con �xito';
//FinRev.5-------------------------------------------------------------------------
begin
    //Rev.5 / 07-Junio-2010 / Nelson Droguett Sierra-----------------------------------
    LogAction(MSG_SERVICE_SUCCESS_STOPED);
	EventLogReportEvent(elInformation, MSG_SERVICE_SUCCESS_STOPED , '');
    //FinRev.5-------------------------------------------------------------------------
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra -------------------------------
    CloseFile(LogFile);
    //---------------------------------------------------------------------------------
end;

procedure TfrmMainForm.tmrReporteTimer(Sender: TObject);
begin
    LogAction('Inicio de la generaci�n de TXT');
    tmrReporte.Enabled  := False;
    try
        if not ConectarBase then Exit;
        if not ConfigurarParametros then Exit;
        if Fintervalo <> 0 then tmrReporte.Interval := FIntervalo * 1000;
        if not ProcesarReportes() then begin
        	LogAction('ProcesarReportes retorn� Falso');
        end;
    finally
        tmrReporte.Enabled := True;
    end;
end;


procedure TfrmMainForm.LogAction(Mensaje:string);
var
    HoraYMensaje : string;
begin
    // Rev.2 / 22-Febrero-2010 / Nelson Droguett Sierra.-------------------------------
    // Solo se pregunta si escribe o no el Log, consultando una variable, que toma
    // valor en el inicio del servicio.
    if bDebeEscribirLog then begin
      HoraYMensaje := DateTimeToStr(now) + Mensaje ;
      WriteLn( LogFile , Pchar(HoraYMensaje));
    end;
end;



end.


