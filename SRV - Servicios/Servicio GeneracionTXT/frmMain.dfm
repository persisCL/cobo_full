object frmMainForm: TfrmMainForm
  Left = 0
  Top = 0
  Caption = 'Servicio de Generacion de archivos TXT'
  ClientHeight = 86
  ClientWidth = 327
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Service: TDPSService
    ServiceName = 'KTCGeneracionTXT'
    DisplayName = 'KAPSCH TraffiCom SCL - Servicio de Generaci'#243'n de archivos TXT'
    OnStart = ServiceStart
    OnStop = ServiceStop
    OnAfterInstall = ServiceAfterInstall
    OnAfterUninstall = ServiceAfterUninstall
    Left = 20
    Top = 32
  end
  object tmrReporte: TTimer
    Enabled = False
    OnTimer = tmrReporteTimer
    Left = 148
    Top = 32
  end
  object spReporte: TADOStoredProc
    Connection = BaseAUTORIZADA
    CommandTimeout = 6000
    Parameters = <>
    Left = 184
    Top = 32
  end
  object BaseAUTORIZADA: TADOConnection
    CommandTimeout = 6000
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=123456;Persist Security Info=True;U' +
      'ser ID=usr_jordan;Initial Catalog=OP_CAC_NEW2;Data Source=172.16' +
      '.3.32'
    ConnectionTimeout = 10000
    CursorLocation = clUseServer
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 232
    Top = 32
  end
end
