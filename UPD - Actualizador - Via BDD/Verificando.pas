unit Verificando;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Forms,
  Dialogs,
  ExtCtrls,
  StdCtrls,
  Controls;


type
  TfrmVerificando = class(TForm)
    Image1: TImage;
    lblAccion: TLabel;
    Image2: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Actualiza;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TStatusThread = class(TThread)
    Count : Integer;
    Crec  : Boolean;
    constructor Create; overload;
    procedure Execute(); override;
  end;


var
  frmVerificando: TfrmVerificando;

implementation

uses Actualizando;

{$R *.DFM}



constructor TStatusThread.Create;
begin
  inherited Create(True);
  Count := 0;
  Crec  := True;
end;

procedure TStatusThread.Execute();
begin
  inherited;
   repeat
     Synchronize(frmActualizando.Actualiza);
     Sleep(100)
   until false;

  //Sleep(10);

end;


procedure TfrmVerificando.Actualiza;
begin
  Refresh;
end;

procedure TfrmVerificando.FormCreate(Sender: TObject);
begin
  with TStatusThread.Create do begin
//    Priority := tpHigher;
    Resume;
  end;
end;

end.
