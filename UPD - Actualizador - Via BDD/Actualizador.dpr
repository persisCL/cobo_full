library Actualizador;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
//  Consts,
  Forms,
//  Dialogs,
  ShellApi,
  Windows,
  IdHash,
  IdHashMessageDigest,
  DB,
  ADODB,
  StrUtils,
  Actualizando in 'Actualizando.pas' {frmActualizando},
  Verificando in 'Verificando.pas' {frmVerificando};

Type
  TCBProgress = Function(const iLectura,iTotalFichero: Integer): Boolean;


{$R *.RES}


function GetContext(pFile: string): string;
begin
  Result := '';
  if ContainsText(pFile,'ADM') then Result := 'ADM';
  if ContainsText(pFile,'CRM') then Result := 'CRM';
  if ContainsText(pFile,'FAC') then Result := 'FAC';
  if ContainsText(pFile,'INT') then Result := 'INT';
  if ContainsText(pFile,'REC') then Result := 'REC';
  if ContainsText(pFile,'REP') then Result := 'REP';
  if ContainsText(pFile,'SOR') then Result := 'SOR';
  if ContainsText(pFile,'TAG') then Result := 'TAG';
end;


function TraeArchivoDesdeBDD(Archivo: string) : Boolean;
  var
    ADO       : TAdoConnection;
    qry       : TADOQuery;
    Conexion  : string;
    Hash      : string;

begin
  Conexion := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=Kapsch2016;Initial Catalog=_UTILS;Data Source=172.20.23.15\bo_desarrollo';
  ADO := TADOConnection.Create(nil);
  with ADO do begin
    ConnectionString := Conexion;
    Open;
  end;

  qry := TADOQuery.Create(nil);

  try
    with qry do begin
      Connection := ADO;
//      SQL.Text                                := 'select top 1  * from Ejecutables where FileName = ''' + ExtractFileName(Archivo) + ''' and FullFileName like ''%' + GetContext(Archivo) + '%'' order by Date desc';
      SQL.Text                                 := 'select top 1  * from Ejecutables where FileName = :Archivo and FullFileName like :Contexto order by Date desc';
      Parameters.ParamByName('Archivo').Value  := ExtractFileName(Archivo);
      Parameters.ParamByName('Contexto').Value := '%' + GetContext(Archivo) + '%';
      Open;
      Hash                                     := FieldByName('Hash').AsString;
      ForceDirectories(ExcludeTrailingPathDelimiter(ExtractFilePath(Archivo)));
      TBlobField(FieldByName('FileContent')).SaveToFile(Archivo);
      Close;
    end;
    Result := True;
  except
    on e:Exception do begin
     // ShowMessage(e.Message);
     // showmessage(qry.SQL.Text);
      Result := False;
    end;

  end;

  ADO.Connected := False;
  FreeAndNil(qry);
  FreeAndNil(ADO);

end;


function MD5(const fileName : string) : string;
var
    idmd5   : TIdHashMessageDigest5;
    fs      : TFileStream;


begin
    if FileExists(fileName) then begin
        idmd5   := TIdHashMessageDigest5.Create;
        try
            fs     := TFileStream.Create(fileName, fmOpenRead OR fmShareDenyWrite) ;
            result := idmd5.AsHex(idmd5.HashValue(fs)) ;
        finally
           fs.Free;
           idmd5.Free;
        end;
    end else
        Result  := '';
end;


function ExecAndWait(const ExecuteFile, ParamString : string): boolean;
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;

begin
    FillChar(SEInfo, SizeOf(SEInfo), 0);
    SEInfo.cbSize := SizeOf(TShellExecuteInfo);

    with SEInfo do begin
        fMask := SEE_MASK_NOCLOSEPROCESS;
        Wnd := Application.Handle;
        lpFile := PChar(ExecuteFile);
        lpParameters := PChar(ParamString);
        nShow := SW_HIDE;
    end;

    if ShellExecuteEx(@SEInfo) then
    begin
        repeat
            Application.ProcessMessages;
            GetExitCodeProcess(SEInfo.hProcess, ExitCode);
        until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
        Result:=True;
    end
    else Result := False;
end;

procedure RegisterOCXorDLL(pLibrary: AnsiString);
  type
    TRegFunc = function : HResult; stdcall;
  var
    ARegFunc : TRegFunc;
    aHandle  : THandle;

begin
    try
        aHandle := LoadLibrary(PChar(pLibrary));
        if aHandle <> 0 then try
            ARegFunc := GetProcAddress(aHandle,'DllRegisterServer');

            if Assigned(ARegFunc) then begin
                ExecAndWait('regsvr32','/s ' + pLibrary);
            end;
        finally
            FreeLibrary(aHandle);
        end;
    except
        on e: Exception do begin
            raise Exception.Create((Format('Unable to register %s. Error: %s', [pLibrary, e.Message])));
        end;
    end;
end;


function HayQueActualizar(pModuloActual,pModuloActualizado: String):  Boolean; Stdcall; Export;
var
  ADO       : TAdoConnection;
  qry       : TADOQuery;
  Conexion  : string;
  Disco,
  Hash,
  HashNew   : string;


begin

  //ShowMessage(pModuloActualizado);

  Application.CreateForm(TfrmVerificando, frmVerificando);
  with frmVerificando do begin
    lblAccion.Caption := Format(lblAccion.Caption,[ExtractFileName(pModuloActual)]);
    Show;
    Update;
  end;

  if  (pModuloActualizado = 'Actualizador.dll') then begin
    Result := False;
  end else
  begin
      try
          Disco := 'C:';
          Conexion := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=Kapsch2016;Initial Catalog=_UTILS;Data Source=172.20.23.15\bo_desarrollo';
          ADO := TADOConnection.Create(nil);
          with ADO do begin
            ConnectionString := Conexion;
            Open;
          end;
          qry := TADOQuery.Create(nil);
          qry.Connection := ADO;
          pModuloActualizado := ExtractFileName(pModuloActualizado);
          qry.SQL.Text                                  := 'select ID, Hash, FileName, FullFileName, Getdate() as Hora ' +
                                                           ' from                                      ' +
                                                           ' (select                                   ' +
                                                           '     id,                                   ' +
                                                           '     Hash,                                 ' +
                                                           '     FileName,                             ' +
                                                           '     Date,                                 ' +
                                                           '     FullFileName,                         ' +
                                                           '     TypeOfRelease,                        ' +
                                                           '     ROW_NUMBER() over (partition by TypeOfRelease, FullFileName order by Date desc) as NumRec ' +
                                                           '     from Ejecutables) a                   ' +
                                                           ' where                                     ' +
                                                           '       NumRec=1 and TypeOfRelease=:Rel     ' +
                                                           ' and a.FileName = :Archivo                 ' +
                                                           ' and a.FullFileName like :Contexto         ';

          qry.Parameters.ParamByName('Rel').Value        := 3;
          qry.Parameters.ParamByName('Archivo').Value    := pModuloActualizado;
          qry.Parameters.ParamByName('Contexto').Value   := '%' + GetContext(pModuloActual) + '%';
          qry.Open;
          Hash          := qry.FieldByName('Hash').AsString;
          HashNew       := MD5(pModuloActualizado);
          qry.Close;
          if (Hash <> HashNew) then
            Result := True
          else
            Result := False;
      except
        on e:exception do begin
 //         ShowMessage(e.Message);
          Result := False;
        end;
      end;

      ADO.Connected := False;
      FreeAndNil(qry);
      FreeAndNil(ADO);
      if Assigned(frmVerificando) then FreeAndNil(frmVerificando);

  end;
end;

procedure _ActualizaModulo(pModuloActual,pModuloActualizado: String);

begin
    Application.CreateForm(TfrmActualizando, frmActualizando);
  case FileExists(pModuloActual) Of
        True:
            begin
              with frmActualizando do begin
                lblAccion.Caption := Format(lblAccion.Caption,[ExtractFileName(pModuloActual)]);
                Show;
                Update;
              end;
              DeleteFile(PChar(pModuloActual));
              TraeArchivoDesdeBDD(pModuloActual);
            end;
        False:
            begin
              with frmActualizando do begin
                lblAccion.Caption := Format(lblAccion.Caption,[ExtractFileName(pModuloActual)]);
                Show;
                Update;
              end;
              DeleteFile(PChar(pModuloActual));
              TraeArchivoDesdeBDD(pModuloActual);
            end;
    end;
    if Assigned(frmActualizando) then FreeAndNil(frmActualizando);

end;


procedure ActualizaModulo(pModuloActual,pModuloActualizado: String); Stdcall; Export;
begin
    _ActualizaModulo(pModuloActual, pModuloActualizado);
end;

procedure ActualizaModulo2; Stdcall; Export;
    var
        pModuloActual,
        pModuloActualizado: String;

begin
    pModuloActual       := ParamStr(3);
    pModuloActualizado  := ParamStr(4);

    _ActualizaModulo(pModuloActual, pModuloActualizado);

    ShellExecute(Application.Handle,'OPEN',PChar(pModuloActual),'','',SW_SHOWNORMAL);
End;

procedure ActualizaModulo3; Stdcall; Export;
    var
        pModuloActual,
        pModuloActualizado,
        pModuloALanzar: String;
Begin

    pModuloActual       := ParamStr(3);
    pModuloActualizado  := ParamStr(4);
    pModuloALanzar      := ParamStr(5);

    _ActualizaModulo(pModuloActual, pModuloActualizado);

    ShellExecute(Application.Handle,'OPEN',PChar(pModuloALanzar),'','',SW_SHOWNORMAL);
End;

procedure ActualizaDLLorOCX; Stdcall; Export;
    var
        pModuloActual,
        pModuloActualizado,
        pModuloALanzar: String;
        pRegistrar: Boolean;
Begin

    pModuloActual       := ParamStr(3);
    pModuloActualizado  := ParamStr(4);
    pModuloALanzar      := ParamStr(5);
    pRegistrar          := StrToBool(ParamStr(6));

    _ActualizaModulo(pModuloActual, pModuloActualizado);

    if pRegistrar then RegisterOCXorDLL(pModuloActual);

    ShellExecute(Application.Handle,'OPEN',PChar(pModuloALanzar),'','',SW_SHOWNORMAL);
End;

Exports ActualizaModulo, ActualizaModulo2, ActualizaModulo3, HayQueActualizar, ActualizaDLLorOCX;

begin

end.
