unit ReporteNotaCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppStrtch, ppRegion, ppClass, ppVar,
  ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppDBBDE, ppComm, ppRelatv,
  ppDBPipe, DB, ADODB, peaTypes, DMConnection, RBSetup,
  pptypes, peaprocs, ExtCtrls, jpeg, ppBarCod, ppParameter, ppSubRpt,
  ppModule, raCodMod, UtilProc, RStrings;

type
  TformReporteNotaCredito = class(TForm)
    qryObtenerTarjetaCredito: TADOQuery;
    qryObtenerCuentaBancaria: TADOQuery;
    ObtenerDatosNotaCredito: TADOStoredProc;
    dsDatosCliente: TDataSource;
    dsDatosComprobantes: TDataSource;
    dsCuentaDebito: TDataSource;
    ppCuentaDebito: TppDBPipeline;
    ActualizarComprobanteImpreso: TADOQuery;
    rbi_NotaCredito: TRBInterface;
    ppDatosComprobantes: TppDBPipeline;
    ppDatosCliente: TppDBPipeline;
    ObtenerDatosCliente: TADOStoredProc;
    rp_NotaCredito: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppShape21: TppShape;
    ppDBText18: TppDBText;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel29: TppLabel;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppLabel31: TppLabel;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppDetailBand2: TppDetailBand;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppLabel39: TppLabel;
    ppSummaryBand2: TppSummaryBand;
    ppParameterList2: TppParameterList;
    ppShape33: TppShape;
    ppShape34: TppShape;
    ppLabel34: TppLabel;
    ppShape17: TppShape;
    ppLabel47: TppLabel;
    ObtenerDomicilioConvenio: TADOStoredProc;
    ppDomicilioFacturacion: TppDBPipeline;
    dsDomicilioFacturacion: TDataSource;
    ppLabel42: TppLabel;
    ppDBText29: TppDBText;
    ppShape18: TppShape;
    ppShape23: TppShape;
    ObtenerConceptosNotaCredito: TADOStoredProc;
    dsObtenerCargosComprobante: TDataSource;
    ppCargosComprobante: TppDBPipeline;
    ppsbCargos: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppDBText30: TppDBText;
    ObtenerConceptosNotaCreditoCodigoConcepto: TWordField;
    ObtenerConceptosNotaCreditoDescripcion: TStringField;
    ObtenerConceptosNotaCreditoImporte: TBCDField;
    ObtenerConceptosNotaCreditoDescriImporte: TStringField;
    ppDBText32: TppDBText;
    raCodeModule1: TraCodeModule;
    raCodeModule2: TraCodeModule;
    ppImage3: TppImage;
    ppDBText19: TppDBText;
    ppDBText33: TppDBText;
    ObtenerDatosNotaCreditoCodigoTipoMedioPago: TWordField;
    ObtenerDatosNotaCreditoNumeroConvenio: TStringField;
    ObtenerDatosNotaCreditoNumeroConvenioFormateado: TStringField;
    ObtenerDatosNotaCreditoPAC_CodigoBanco: TIntegerField;
    ObtenerDatosNotaCreditoPAC_CodigoTipoCuentaBancaria: TWordField;
    ObtenerDatosNotaCreditoPAC_NroCuentaBancaria: TStringField;
    ObtenerDatosNotaCreditoPAC_Sucursal: TStringField;
    ObtenerDatosNotaCreditoPAT_CodigoTipoTarjetaCredito: TWordField;
    ObtenerDatosNotaCreditoPAT_NumeroTarjetaCredito: TStringField;
    ObtenerDatosNotaCreditoPAT_FechaVencimiento: TStringField;
    ObtenerDatosNotaCreditoPAT_CodigoEmisorTarjetaCredito: TWordField;
    ObtenerDatosNotaCreditoTipoComprobante: TStringField;
    ObtenerDatosNotaCreditoNumeroComprobante: TBCDField;
    ObtenerDatosNotaCreditoFecha: TDateTimeField;
    ObtenerDatosNotaCreditoVencimiento: TDateTimeField;
    ObtenerDatosNotaCreditoConcesionaria: TStringField;
    ObtenerDatosNotaCreditoCodigoConvenio: TIntegerField;
    ObtenerDatosNotaCreditoNumeroMovimiento: TAutoIncField;
    ObtenerDatosNotaCreditoIndiceVehiculo: TIntegerField;
    ObtenerDatosNotaCreditoCodigoConcepto: TWordField;
    ObtenerDatosNotaCreditoDescripcion: TStringField;
    ObtenerDatosNotaCreditoImporte: TBCDField;
    ObtenerDatosNotaCreditoImporteTotal: TBCDField;
    ObtenerDatosNotaCreditoDescriImporte: TStringField;
    ObtenerDatosNotaCreditoPatente: TStringField;
    ppLabel1: TppLabel;
    procedure ppGroup1GetBreakValue(Sender: TObject;
      var aBreakValue: String);
    procedure ppLTituloCargosGetText(Sender: TObject; var Text: String);
    procedure pplSubtotalGetText(Sender: TObject; var Text: String);
    procedure ppDBText18GetText(Sender: TObject; var Text: String);
    procedure ppDBText28GetText(Sender: TObject; var Text: String);
  private
    { Private declarations }
  public
    { Public declarations }
     function ConfigurarImpresora: AnsiString;
     function getImpresora:AnsiString;
     Function Execute(TipoComprobante: Char; CodigoCliente: Integer; NumeroComprobante: Double;
       ShowInterface: Boolean; titulo: AnsiString): Boolean;
  end;

implementation

{$R *.dfm}


function TformReporteNotaCredito.ConfigurarImpresora: AnsiString;
begin
    rbi_NotaCredito.Configure;
    result := getImpresora;
end;

Function TformReporteNotaCredito.Execute(TipoComprobante: Char; CodigoCliente: Integer; NumeroComprobante: Double; showInterface: Boolean; titulo: AnsiString):Boolean;
resourcestring
    MSG_FACTURA_TARJETA_CREDITO = 'Factura debitada en la Tarjeta de Credito %s n�mero %s';
    MSG_FACTURA_CUENTA_BANCARIA = 'Factura debitada en la Cuenta Bancaria %s n�mero %s';

var
	Config, ConfigOriginal: TRBConfig;
    Cpostal, FileTemp: AnsiString;
begin
    Result := False;
    // Control que exista el directorio donde se guardaran los rpt
    FileTemp := ObtenerNombreArchivoFactura(DMConnections.BaseCAC,'C', NumeroComprobante);

    if (not DirectoryExists(ExtractFilePath(FileTemp))) then begin
            MsgBox(MSG_ERROR_DIRECTORIO_ACCESO,Caption, MB_ICONSTOP);
            Exit;
    end;


// Obtener datos del cliente
    ObtenerDatosCliente.close;
    ObtenerDatosCliente.Parameters.ParamByName('@CodigoCliente').value := CodigoCliente;
    ObtenerDatosCliente.open;

// Obtener datos a imprimir
    ObtenerDatosNotaCredito.Close;
    ObtenerDatosNotaCredito.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
    ObtenerDatosNotaCredito.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    ObtenerDatosNotaCredito.open;
// Obtener informacion de cuenta de debito
    if assigned(dsCuentaDebito.DataSet) then
        dsCuentaDebito.DataSet.close;
    if ObtenerDatosNotaCredito.FieldByName('CodigoTipoMedioPago').value = TPA_NINGUNO then begin
//        lCuentaDebito.Caption := MSG_FACTURA_PAGO_MANUAL;
    end else begin
        if ObtenerDatosNotaCredito.FieldByName('CodigoTipoMedioPago').value = TPA_PAT then begin
            dsCuentaDebito.DataSet := qryObtenerTarjetaCredito;
            qryObtenerTarjetaCredito.parameters.paramByName('CodigoTipoTarjetaCredito').value :=
                ObtenerDatosNotaCredito.FieldByName('PAT_CodigoTipoTarjetaCredito').value;
            qryObtenerTarjetaCredito.parameters.paramByName('CodigoConvenio').value := ObtenerDatosNotaCredito.FieldByName('CodigoConvenio').AsInteger;
            qryObtenerTarjetaCredito.open;
        end else begin
            dsCuentaDebito.DataSet := qryObtenerCuentaBancaria;
            qryObtenerCuentaBancaria.parameters.paramByName('CodigoBanco').value :=
                ObtenerDatosNotaCredito.FieldByName('PAC_CodigoBanco').value;
            qryObtenerTarjetaCredito.parameters.paramByName('CodigoConvenio').value := ObtenerDatosNotaCredito.FieldByName('CodigoConvenio').AsInteger;
            qryObtenerCuentaBancaria.open;
        end;
    end;

    if trim(ObtenerDatosCliente.fieldByName('CodigoPostal').asString) <> '' then
        Cpostal := '  (' + trim(ObtenerDatosCliente.fieldByName('CodigoPostal').asString) + ') ';

    ObtenerDomicilioConvenio.Close;
    ObtenerDomicilioConvenio.Parameters.ParamByName('@CodigoConvenio').Value := ObtenerDatosNotaCredito.FieldByName('CodigoConvenio').AsInteger;
    ObtenerDomicilioConvenio.Open;


    ObtenerConceptosNotaCredito.Close;
    ObtenerConceptosNotaCredito.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
    ObtenerConceptosNotaCredito.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    ObtenerConceptosNotaCredito.open;

 	//Grabar Factura a Disco
	//FileTemp := ObtenerNombreArchivoFactura(DMConnections.BaseCAC,'F', NumeroComprobante);
    ConfigOriginal		:= rbi_NotaCredito.GetConfig;
    Config 				:= ConfigOriginal;
    Config.FileName		:= FileTemp;
    Config.DeviceType	:= dtArchive;
    rbi_NotaCredito.SetConfig(Config);
    rbi_NotaCredito.Execute(False);
//    ConfigOriginal.DeviceType := dtScreen + dtPrinter; // Para que la segunda vez imprima
    rbi_NotaCredito.SetConfig(ConfigOriginal);

    //Ahora me fijo que habia configurado el Usuario
    if ConfigOriginal.DeviceType <> dtArchive then
        rbi_NotaCredito.Execute(ShowInterface);

    //	Marcar el comprobante como impreso
    ActualizarComprobanteImpreso.Close;
    ActualizarComprobanteImpreso.Parameters.ParamByName('FechaHora').value := NowBase(DMConnections.BaseCAC);
    ActualizarComprobanteImpreso.Parameters.ParamByName('TipoComprobante').value := TipoComprobante;
    ActualizarComprobanteImpreso.Parameters.ParamByName('NumeroComprobante').value := NumeroComprobante;
    ActualizarComprobanteImpreso.ExecSQL;
    Result := True;
end;


function TformReporteNotaCredito.getImpresora: AnsiString;
begin
    result := rbi_NotaCredito.GetConfig.PrinterName;
end;

procedure TformReporteNotaCredito.ppGroup1GetBreakValue(Sender: TObject;
  var aBreakValue: String);
begin
    aBreakValue := ObtenerDatosNotaCredito.FieldByName('Concesionaria').asString;
end;

procedure TformReporteNotaCredito.ppLTituloCargosGetText(Sender: TObject;
  var Text: String);
resourcestring
    MSG_CONCEPTOS_COMUNES = 'Conceptos comunes';
    MSG_CONCESIONARIA = 'Concesionaria %s';
begin
    if ObtenerDatosNotaCredito.FieldByName('Concesionaria').asString = '' then
        text := MSG_CONCEPTOS_COMUNES
    else
        text := Format(MSG_CONCESIONARIA, [Trim(ObtenerDatosNotaCredito.FieldByName('Concesionaria').asString)]);
end;

procedure TformReporteNotaCredito.pplSubtotalGetText(Sender: TObject;
  var Text: String);
resourcestring
    MSG_SUBTOTAL_CONCEPTOS_COMUNES = 'Subtotal Conceptos comunes';
    MSG_SUBTOTAL_CONCESIONARIA = 'Subtotal Concesionaria %s';
begin
    if ObtenerDatosNotaCredito.FieldByName('Concesionaria').asString = '' then
        text := MSG_SUBTOTAL_CONCEPTOS_COMUNES
    else
        text := Format(MSG_SUBTOTAL_CONCESIONARIA, [Trim(ObtenerDatosNotaCredito.FieldByName('Concesionaria').asString)]);
end;

procedure TformReporteNotaCredito.ppDBText18GetText(Sender: TObject;
  var Text: String);
begin
    Text := Trim(Text) + ' ' + Trim(ObtenerDomicilioConvenio.fieldbyname('Numero').AsString);
end;

procedure TformReporteNotaCredito.ppDBText28GetText(Sender: TObject;
  var Text: String);
begin
    Text := '$ ' + Trim(Text); 
end;

end.
