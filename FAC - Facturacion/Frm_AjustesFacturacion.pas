{-----------------------------------------------------------------------------
 File Name: Frm_AjustesFacturacion.pas
 Author:    FLamas
 Date Created: 24/01/2005
 Language: ES-AR
 Description:   Permite ingresar Facturas e items a facturar

 Revision 1:
    Author: dAllegretti
    Date: 22/05/2008
    Description:    Se le agreg� el parametro @UsuarioCreacion del Stored spAgregarMovimientoCuenta
                    y la correspondiente asignaci�n en el llamado al stored.

 Revision 2:
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura


Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia el llamado a llenar el combo de convenios para que utilicen el procedimiento CargarConveniosRUT.
                Se crea el procedimiento OnDrawItem para que llame al procedimiento ItemComboBoxColor.
-----------------------------------------------------------------------------}
unit Frm_AjustesFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Dateedit, ExtCtrls, utilProc, DmiCtrls, util, DBCtrls,
  ImgList, ADODB, DMConnection, ComCtrls, BuscaClientes, Utildb, DB,
  DPSControls, VariantComboBox, constParametrosGenerales, Validate,
  peaTypes, PeaProcs, dateUtils, ListBoxEx, DBListEx, DBClient,
  Frm_AgregarItemFacturacion, TimeEdit;

type
  TfrmAjusteFacturacion = class(TForm)
    gbDatosConvenio: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    peRUTCliente: TPickEdit;
    cbConveniosCliente: TVariantComboBox;
    Label7: TLabel;
    Label8: TLabel;
    NumeroConvenio: TEdit;
    gbDatosCliente: TGroupBox;
    lNombreCompleto: TLabel;
    lDomicilio: TLabel;
    lComuna: TLabel;
    lRegion: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    ObtenerCLiente: TADOStoredProc;
    spAgregarMovimientoCuenta: TADOStoredProc;
    gbAjuste: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    edFechaAjuste: TDateEdit;
    Label4: TLabel;
    edMotivoAjustePeaje: TEdit;
    Label5: TLabel;
    edMotivoAjusteIntereses: TEdit;
    edAjustePeaje: TEdit;
    edAjusteIntereses: TEdit;
    spObtenerConveniosCliente: TADOStoredProc;
    tmrConsulta: TTimer;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    btnSalir: TButton;
    btnAjustar: TButton;
    procedure btnSalirClick(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure NumeroConvenioChange(Sender: TObject);
    procedure btnAjustarClick(Sender: TObject);
    procedure edAjustePeajeChange(Sender: TObject);
    procedure edAjustePeajeKeyPress(Sender: TObject; var Key: Char);
    procedure tmrConsultaTimer(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure cbConveniosClienteDrawItem(Control: TWinControl; Index: Integer;                        //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                                           //SS_1120_MVI_20130820
  private
    { Private declarations }
	FUltimaBusqueda : TBusquedaCliente;
    procedure ClearCaptions;
	function  AgregarMovimientosCuentaAjustes : boolean;
	procedure MostrarDomicilioFacturacion;
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;


implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author       : flamas
Date Created : 24-01-2005
Description  : Iniciliza el form.
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TfrmAjusteFacturacion.Inicializar: Boolean;
var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	CenterForm(Self);
    edFechaAjuste.Date := Date;
	ClearCaptions;
	Result := True;
end;

procedure TfrmAjusteFacturacion.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: peRUTClienteButtonClick
Author       : fmalisia
Date Created : 24-11-2004
Description  : Permite buscar un cliente por medio del form FormBuscaClientes.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAjusteFacturacion.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    application.createForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peRUTCliente.Text :=  F.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
            tmrConsultaTimer(nil);
        end;
    end;
    F.free;
end;


{******************************** Function Header ******************************
Function Name: peRUTClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia el RUT del cliente se cargan todos sus datos asociados
               (Lista de convenios, nombre, domicilio y detalle de la factura a realizar).
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAjusteFacturacion.peRUTClienteChange(Sender: TObject);
begin
	if (activeControl <> sender) then Exit;
    tmrConsulta.Enabled := False;
    tmrConsulta.Enabled := True;
end;

procedure TfrmAjusteFacturacion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: ClearCaptions
Author       : fmalisia
Date Created : 24-11-2004
Description  : Borra los datos del cliente
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAjusteFacturacion.ClearCaptions;
begin
    lNombreCompleto.Caption	:= '';
    lDomicilio.Caption      := '';
    lComuna.Caption         := '';
    lRegion.Caption 		:= '';
end;

{******************************** Function Header ******************************
Function Name: peRUTClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia el RUT del cliente se cargan todos sus datos asociados
               (Lista de convenios, nombre, domicilio y detalle de la factura a realizar).
Parameters   : Sender: TObject
Return Value : None


Revision 1:
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}
procedure TfrmAjusteFacturacion.NumeroConvenioChange(Sender: TObject);
var
	Documento : String;
begin
	if (activeControl = sender) then begin
		// voy buscando el cliente que tenga este convenio, si existe pongo su RUT en el buscador
		Documento := QueryGetValue (DMConnections.BaseCAC, 'SELECT ISNULL (NumeroDocumento, '''') FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = (SELECT CodigoCliente FROM Convenio  WITH (NOLOCK) WHERE NumeroConvenio = ' + QuotedStr(NumeroConvenio.Text) + ')');

		if Documento <> '' then peRUTCliente.setFocus;
		peRUTCliente.Text := Documento
	end
end;

{******************************** Function Header ******************************
Function Name: btnAjustarClick
Author       : flamas
Date Created : 24-01-2005
Description  : Genera el ajuste de Facturaci�n
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAjusteFacturacion.btnAjustarClick(Sender: TObject);
resourcestring
    MSG_ADJUSTMENT_GENERATED_OK = 'El ajuste se gener� correctamente.';
begin
	Screen.Cursor := crHourglass;
    try
    	DMConnections.BaseCAC.BeginTrans;
    	if AgregarMovimientosCuentaAjustes then begin
				DMConnections.BaseCAC.CommitTrans;
                ShowMessage( MSG_ADJUSTMENT_GENERATED_OK );
    			Screen.Cursor := crDefault;
                Close;
        end else
        	DMConnections.BaseCAC.RollbackTrans;
    finally
    	Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: AgregarMovimientosCuentaAjustes
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega los Movimientos de cuenta correspondientes al Ajuste
Parameters   :
Return Value : boolean
*******************************************************************************}
function TfrmAjusteFacturacion.AgregarMovimientosCuentaAjustes : boolean;
resourcestring
	MSG_ERROR_UPDATING_TOLL_ADJUSTMENT = 'Error actualizando el ajuste de peaje.';
	MSG_ERROR_UPDATING_INTERESTS_ADJUSTMENT = 'Error actualizando el ajuste de intereses.';

var
    AjustePeaje, AjusteIntereses : Int64;

begin
    AjustePeaje := StrToIntDef( edAjustePeaje.Text, 0 );
    AjusteIntereses := StrToIntDef( edAjusteIntereses.Text, 0 );
	result := True;
	// Carga el movimiento de Cuenta para el ajuste de Peaje
    if ( edAjustePeaje.Text <> '' ) then begin
    	with spAgregarMovimientoCuenta, Parameters do begin
    		try
            	ParamByName( '@CodigoConvenio' ).Value 			:= cbConveniosCliente.Value;
                ParamByName( '@IndiceVehiculo' ).Value 			:= NULL;
                ParamByName( '@FechaHora' ).Value				:= edFechaAjuste.Date;
                ParamByName( '@CodigoConcepto' ).Value			:= CONCEPTO_AJUSTE_PEAJE;
                ParamByName( '@Importe' ).Value					:= AjustePeaje * 100;
                ParamByName( '@LoteFacturacion' ).Value			:= NULL;
                ParamByName( '@EsPago' ).Value					:= False;
                ParamByName( '@Observaciones' ).Value			:= edMotivoAjustePeaje.Text;
                ParamByName( '@NumeroPromocion' ).Value			:= NULL;
                ParamByName( '@NumeroFinanciamiento' ).Value	:= NULL;
                ParamByName( '@UsuarioCreacion' ).Value	        := UsuarioSistema;
                ExecProc;
            except
            	on E : exception do begin
                	MsgBoxErr(MSG_ERROR_UPDATING_TOLL_ADJUSTMENT, e.message, Caption, MB_ICONWARNING);
                    result := False;
                end;
            end;
        end;
    end;

	// Carga el movimiento de Cuenta para el ajuste de Intereses
    if ( edAjusteIntereses.Text <> '' ) then begin
    	with spAgregarMovimientoCuenta, Parameters do begin
    		try
            	ParamByName( '@CodigoConvenio' ).Value 			:= cbConveniosCliente.Value;
                ParamByName( '@IndiceVehiculo' ).Value 			:= NULL;
                ParamByName( '@FechaHora' ).Value				:= edFechaAjuste.Date;
                ParamByName( '@CodigoConcepto' ).Value			:= CONCEPTO_AJUSTE_INTERESES;
                ParamByName( '@Importe' ).Value					:= AjusteIntereses * 100;
                ParamByName( '@LoteFacturacion' ).Value			:= NULL;
                ParamByName( '@EsPago' ).Value					:= False;
                ParamByName( '@Observaciones' ).Value			:= edMotivoAjusteIntereses.Text;
                ParamByName( '@NumeroPromocion' ).Value			:= NULL;
                ParamByName( '@NumeroFinanciamiento' ).Value	:= NULL;
                ExecProc;
            except
            	on E : exception do begin
                	MsgBoxErr(MSG_ERROR_UPDATING_INTERESTS_ADJUSTMENT, e.message, Caption, MB_ICONWARNING);
                    result := False;
                end;
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: edAjustePeajeChange
Author       : FLamas
Date Created : 24-01-2005
Description  : Habilita o deshabilita el bot�n de Ajustar
Parameters   : Sender: TObject
Return Value :
*******************************************************************************}
procedure TfrmAjusteFacturacion.edAjustePeajeChange(Sender: TObject);
begin
	btnAjustar.Enabled :=   ( cbConveniosCliente.ItemIndex >= 0 ) and
    						( 	edFechaAjuste.Date <> nulldate ) and
    						(( 	edAjustePeaje.Text <> '' ) or
                             ( edAjusteIntereses.Text <> '' ));
end;

procedure TfrmAjusteFacturacion.edAjustePeajeKeyPress(Sender: TObject; var Key: Char);
begin
	if not( Key In [#8, #9, #13, '-', '0'..'9'] ) then Key := #0;
    if (Key = '-')  and
    	((Pos( '-', TEdit(Sender).Text) <> 0) or
        	(TEdit(Sender).SelStart <> 0)) then Key :=#0;
end;

procedure TfrmAjusteFacturacion.tmrConsultaTimer(Sender: TObject);
var
    CodigoConvenio : Variant;
begin
	Screen.Cursor := crHourglass;
    try
       	tmrConsulta.Enabled := False;
		ClearCaptions;
		cbConveniosCliente.Items.clear;

        // Obtenemos el cliente seleccionado
        with obtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value := NULL;
            Parameters.ParamByName('@CodigoDocumento').value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').value := iif(peRUTCLiente.Text <> '',
      															PadL(Trim(peRUTCLiente.Text), 9, '0'), '');
            Open;
        end;

        // Obtenemos la lista de convenios para el cliente seleccionado
        CodigoConvenio := Null;
//        With spObtenerConveniosCliente do begin                                                                               //SS_1120_MVI_20130820
//            Parameters.ParamByName('@CodigoCliente').Value := obtenerCliente.fieldbyname('CodigoCliente').AsInteger;          //SS_1120_MVI_20130820
//			Parameters.ParamByName('@IncluirConveniosDeBaja').Value := False;                                                   //SS_1120_MVI_20130820
//            open;                                                                                                             //SS_1120_MVI_20130820
//            First;                                                                                                            //SS_1120_MVI_20130820
//            while not spObtenerConveniosCliente.Eof do begin                                                                  //SS_1120_MVI_20130820
//                cbConveniosCliente.Items.Add(spObtenerConveniosCliente.fieldbyname('NumeroConvenioFormateado').AsString,      //SS_1120_MVI_20130820
//                  trim(spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString));                                    //SS_1120_MVI_20130820
//                if Trim(spObtenerConveniosCliente.fieldbyname('NumeroConvenio').AsString) = Trim(NumeroConvenio.Text) then	//SS_1120_MVI_20130820
//                    CodigoConvenio := spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString;                       //SS_1120_MVI_20130820
//                spObtenerConveniosCliente.Next;                                                                               //SS_1120_MVI_20130820
//            end;                                                                                                              //SS_1120_MVI_20130820
//            close                                                                                                             //SS_1120_MVI_20130820
//        end;                                                                                                                  //SS_1120_MVI_20130820
//                                                                                                                              //SS_1120_MVI_20130820
//		if (CodigoConvenio <> null ) then cbConveniosCliente.Value := CodigoConvenio                                            //SS_1120_MVI_20130820
//        else cbConveniosCliente.ItemIndex := 0;                                                                               //SS_1120_MVI_20130820
                                                                                                                                //SS_1120_MVI_20130820
        CargarConveniosRUT(DMCOnnections.BaseCAC,cbConveniosCliente, 'RUT',                                         			//SS_1120_MVI_20130820
        PadL(peRUTCLiente.Text, 9, '0' ), True, 0, False, True );                                                   			//SS_1120_MVI_20130820

        NumeroConvenio.Text := '';

        with ObtenerCliente do begin
            // Mostramos el nombre completo del cliente
            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString), Trim(FieldByName('Nombre').asString))
            else
                lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString));

            if (cbConveniosCliente.ItemIndex > 0) then MostrarDomicilioFacturacion;

        end;
    finally
		Screen.Cursor := crDefault;
    end;
end;

procedure TfrmAjusteFacturacion.MostrarDomicilioFacturacion;
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := cbConveniosCliente.Value;
        try
        	ExecProc;
    		lDomicilio.Caption	:= ParamByName('@Domicilio').Value;
 			lComuna.Caption		:= ParamByName('@Comuna').Value;
            lRegion.Caption 	:= ParamByName('@Region').Value;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

procedure TfrmAjusteFacturacion.cbConveniosClienteChange(Sender: TObject);
begin
    if Sender <> ActiveControl then exit;
    if  cbConveniosCliente.ItemIndex >= 0 then  MostrarDomicilioFacturacion;

end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TfrmAjusteFacturacion.cbConveniosClienteDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TfrmAjusteFacturacion.cbConveniosClienteDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConveniosCliente.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
//END:SS_1120_MVI_20130820 -----------------------------------------------------

end.



