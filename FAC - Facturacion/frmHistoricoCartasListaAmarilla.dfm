object HistoricoCartasListaAmarillaForm: THistoricoCartasListaAmarillaForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Reenviar Carta por Lista Amarilla'
  ClientHeight = 122
  ClientWidth = 417
  Color = clBtnFace
  Constraints.MaxHeight = 150
  Constraints.MaxWidth = 423
  Constraints.MinHeight = 150
  Constraints.MinWidth = 423
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object bvlFondo: TBevel
    Left = 8
    Top = 8
    Width = 401
    Height = 73
  end
  object lblEmpresaCorreo: TLabel
    Left = 27
    Top = 20
    Width = 111
    Height = 13
    Caption = 'Empresa de Mensajer'#237'a'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblFechaEnvio: TLabel
    Left = 28
    Top = 53
    Width = 73
    Height = 13
    Caption = 'Fecha de Env'#237'o'
  end
  object btnGrabar: TButton
    Left = 211
    Top = 89
    Width = 75
    Height = 25
    Caption = 'Grabar'
    TabOrder = 0
    OnClick = btnGrabarClick
  end
  object btnSalir: TButton
    Left = 313
    Top = 89
    Width = 75
    Height = 25
    Caption = 'Salir'
    TabOrder = 1
    OnClick = btnSalirClick
  end
  object cboEmpresaCorreo: TVariantComboBox
    Left = 162
    Top = 17
    Width = 231
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 2
    Items = <>
  end
  object deFechaEnvio: TDateEdit
    Left = 162
    Top = 49
    Width = 231
    Height = 21
    AutoSelect = False
    Color = 16444382
    TabOrder = 3
    Date = -693594.000000000000000000
  end
  object spGuardarHistoricoCartasListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GuardarHistoricoCartasListaAmarilla'
    Parameters = <>
    Left = 16
    Top = 85
  end
end
