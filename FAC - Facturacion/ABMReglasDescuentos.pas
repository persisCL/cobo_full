unit ABMReglasDescuentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ExtCtrls, ImgList, Grids, DBGrids, StdCtrls,
  Validate, DateEdit, DmiCtrls, VariantComboBox, DMConnection, DB, ADODB,
  Provider, DBClient, UtilProc, Util, PeaTypes, Categoria, PeaProcs;

type
  TfrmABMReglasDescuentos = class(TForm)
    ilImagenes: TImageList;
    pnlSuperior: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btn3: TToolButton;
    btnBuscar: TToolButton;
    pnlBotones: TPanel;
    Notebook: TNotebook;
    btnSalir1: TButton;
    btnBtnAceptar: TButton;
    btnBtnCancelar: TButton;
    pnlk: TPanel;
    dbgrdReglas: TDBGrid;
    grp1: TGroupBox;
    edtCodigoRegla: TEdit;
    edtDescripcion: TEdit;
    vcbTipoCalculo: TVariantComboBox;
    txtPrioridad: TNumericEdit;
    grp2: TGroupBox;
    dateFechaDesde: TDateEdit;
    dateFechaHasta: TDateEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    vcbTipoAsignacion: TVariantComboBox;
    lbl8: TLabel;
    grp3: TGroupBox;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    vcbClaseCategoria: TVariantComboBox;
    vcbCategoria: TVariantComboBox;
    txtCantidadTransitos: TNumericEdit;
    txtMontoTransitos: TNumericEdit;
    grp4: TGroupBox;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    vcbConcesionaria: TVariantComboBox;
    vcbRegion: TVariantComboBox;
    vcbTipoMedioPago: TVariantComboBox;
    vcbPersoneria: TVariantComboBox;
    vcbComuna: TVariantComboBox;
    txtCantidadVehiculos: TNumericEdit;
    txtMontoFacturacion: TNumericEdit;
    spReglasDescuentos_SELECT: TADOStoredProc;
    dtstprReglas: TDataSetProvider;
    cdsReglas: TClientDataSet;
    dsReglas: TDataSource;
    spReglasDescuentos_INSERT: TADOStoredProc;
    spReglasDescuentos_UPDATE: TADOStoredProc;
    spReglasDescuentos_DELETE: TADOStoredProc;
    btnCopiar: TButton;
    txtValor: TNumericEdit;
    procedure vcbRegionChange(Sender: TObject);
    procedure vcbClaseCategoriaChange(Sender: TObject);
    procedure cdsReglasAfterScroll(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnSalir1Click(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnBtnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnCopiarClick(Sender: TObject);
    procedure btnBtnAceptarClick(Sender: TObject);
    procedure vcbTipoCalculoChange(Sender: TObject);
    procedure txtValorChange(Sender: TObject);
    procedure dbgrdReglasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    Accion: Integer;    // 0: Consultar; 1: Insertar; 2: Editar
    FGuardandoDatos: Boolean;

    procedure Limpiar_Campos;
    procedure CargarRegistros;
    procedure HabilitarDeshabilitarControles(Estado: Boolean);
    procedure CargarDatosRegistro;
    procedure CargarCategorias(ClaseCategoria: Integer);
    procedure CargarComunas(CodigoRegion: String);
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

resourcestring
  STR_TODAS = 'Todas';
  STR_TODOS = 'Todos';
  STR_SELECCIONE = '(Seleccione)';

var
  frmABMReglasDescuentos: TfrmABMReglasDescuentos;
  Ahora: TDateTime;

implementation

{$R *.dfm}

function TfrmABMReglasDescuentos.Inicializar:Boolean;
resourcestring
    MSG_ERROR_CATEGORIA_CLASE = 'Ha ocurrido un error al obtener las Clases de Categor�as';
    MSG_ERROR_CONCESIONARIAS = 'Ha ocurrido un error al obtener las Concesionarias';
    MSG_ERROR_MEDIOS_PAGO = 'Ha ocurrido un error al obtener los Medios de Pago';
    MSG_ERROR_REGION = 'Ha ocurrido un error al obtener las Regiones';
var
    sp: TADOStoredProc;
begin
    Result := True;
    Accion := 0;

    Screen.Cursor := crHourGlass;

    sp := TADOStoredProc.Create(nil);
    try
        Ahora := NowBase(DMConnections.BaseCAC);

        sp.Connection := DMConnections.BaseCAC;
        sp.CommandTimeout := 60;

        vcbTipoCalculo.Clear;
        vcbTipoCalculo.Items.Add(STR_SELECCIONE, '');
        vcbTipoCalculo.Items.Add(TD_PORCENTUAL_DESC, TD_PORCENTUAL);
        vcbTipoCalculo.Items.Add(TD_IMPORTE_DESC, TD_IMPORTE);
        vcbTipoCalculo.ItemIndex := 0;

        vcbTipoAsignacion.Clear;
        vcbTipoAsignacion.Items.Add(STR_SELECCIONE, '');
        vcbTipoAsignacion.Items.Add(TA_AUTOMATICA_DESC, TA_AUTOMATICA);
        vcbTipoAsignacion.Items.Add(TA_MANUAL_DESC, TA_MANUAL);
        vcbTipoAsignacion.ItemIndex := 0;

        vcbPersoneria.Clear;
        vcbPersoneria.Items.Add(PERSONERIA_AMBAS_DESC, PERSONERIA_AMBAS);   
        vcbPersoneria.Items.Add(PERSONERIA_JURIDICA_DESC, PERSONERIA_JURIDICA);
        vcbPersoneria.Items.Add(PERSONERIA_FISICA_DESC, PERSONERIA_FISICA);   
        vcbPersoneria.ItemIndex := 0;

        vcbCategoria.Clear;
        vcbCategoria.Items.Add(STR_TODAS, 0);
        vcbCategoria.ItemIndex := 0;

        vcbComuna.Clear;
        vcbComuna.Items.Add(STR_TODAS, '');
        vcbComuna.ItemIndex := 0;

        try
            sp.ProcedureName := 'Categorias_CategoriasClases_SELECT';
            sp.Parameters.Refresh;
            sp.Open;

            vcbClaseCategoria.Items.Clear;
            vcbClaseCategoria.Items.Add(STR_TODAS, 0);
            while not sp.Eof do begin
                vcbClaseCategoria.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('ID_CategoriasClases').AsInteger);
                sp.Next;
            end;
            vcbClaseCategoria.ItemIndex := 0;
            sp.Close;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_CATEGORIA_CLASE, e.Message, 'Error', MB_ICONERROR);
                Result := False;
                Exit;
            end;
        end;

        try
            sp.ProcedureName := 'ObtenerConcesionarias';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@FiltraPorConcepto').Value := 0;
            sp.Open;

            vcbConcesionaria.Items.Clear;
            vcbConcesionaria.Items.Add(STR_TODAS, 0);
            while not sp.Eof do begin
                vcbConcesionaria.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('CodigoConcesionaria').AsInteger);
                sp.Next;
            end;
            vcbConcesionaria.ItemIndex := 0;
            sp.Close;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_CONCESIONARIAS, e.Message, 'Error', MB_ICONERROR);
                Result := False;
                Exit;
            end;
        end;  

        try
            sp.ProcedureName := 'ObtenerTipoMedioPago';
            sp.Parameters.Refresh;
            sp.Open;

            vcbTipoMedioPago.Items.Clear;
            vcbTipoMedioPago.Items.Add(STR_TODOS, '');
            vcbTipoMedioPago.Items.Add(TPA_NINGUNO_DESC, 0);
            while not sp.Eof do begin
                vcbTipoMedioPago.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('CodigoTipoMedioPago').AsInteger);
                sp.Next;
            end;
            vcbTipoMedioPago.ItemIndex := 0;
            sp.Close;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_MEDIOS_PAGO, e.Message, 'Error', MB_ICONERROR);
                Result := False;
                Exit;
            end;
        end;       

        try
            sp.ProcedureName := 'ObtenerRegiones';
            sp.Parameters.Refresh;
            sp.Open;

            vcbRegion.Items.Clear;
            vcbRegion.Items.Add(STR_TODAS, '');
            while not sp.Eof do begin
                vcbRegion.Items.Add(
                    sp.FieldByName('Region').AsString,
                    sp.FieldByName('CodigoRegion').AsString);
                sp.Next;
            end;
            vcbRegion.ItemIndex := 0;
            sp.Close;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_REGION, e.Message, 'Error', MB_ICONERROR);
                Result := False;
                Exit;
            end;
        end;

        CargarRegistros;
    finally
        sp.Free;
        Screen.Cursor := crDefault;
        HabilitarDeshabilitarControles(False);
    end;
end;

procedure TfrmABMReglasDescuentos.btnAgregarClick(Sender: TObject);
begin
    Accion := 1;
    Limpiar_Campos;
    HabilitarDeshabilitarControles(True);
    edtDescripcion.SetFocus;
end;

procedure TfrmABMReglasDescuentos.btnBtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CAPTION = 'Guardar Regla de Descuento';
    MSG_ERROR_INSERT = 'Error al insertar Regla de Descuento';
    MSG_ERROR_UPDATE = 'Error al actualizar Regla de Descuento';
    MSG_VAL_DESCRIPCION = 'Debe ingresar una descripci�n';
    MSG_VAL_TIPO_CALCULO = 'Debe seleccionar un tipo de c�lculo';
    MSG_VAL_VALOR = 'Debe ingresar el valor del descuento';
    MSG_VAL_TIPO_ASIGNACION = 'Debe seleccionar el tipo de asignaci�n';
    MSG_VAL_FECHA_DESDE = 'La fecha de inicio debe ser superior a la actual';
    MSG_VAL_FECHA_HASTA = 'La fecha de fin debe ser superior a la de inicio';
var
    I: Integer;
    Guardado: Boolean;
begin
    Guardado := False;

    if not ValidateControls([
        edtDescripcion,
        vcbTipoCalculo,
        txtValor,
        vcbTipoAsignacion,
        dateFechaDesde,
        dateFechaHasta],
            [Trim(edtDescripcion.Text) <> EmptyStr,
            vcbTipoCalculo.ItemIndex > 0,
            txtValor.ValueInt > 0,
            vcbTipoAsignacion.ItemIndex > 0,
            dateFechaDesde.Date > Ahora,
            (dateFechaHasta.Date = NullDate) or
            (dateFechaHasta.Date > dateFechaDesde.Date)],
                MSG_CAPTION,
                    [MSG_VAL_DESCRIPCION,
                    MSG_VAL_TIPO_CALCULO,
                    MSG_VAL_VALOR,
                    MSG_VAL_TIPO_ASIGNACION,
                    MSG_VAL_FECHA_DESDE,
                    MSG_VAL_FECHA_HASTA]) then
        Exit;

    try
        Screen.Cursor := crHourGlass;
        FGuardandoDatos := True;
        cdsReglas.DisableControls;

        if Accion = 1 then begin
            try
                spReglasDescuentos_INSERT.Parameters.Refresh;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CodigoRegla').Value              := Null;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@Descripcion').Value              := Trim(edtDescripcion.Text);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@TipoCalculo').Value              := vcbTipoCalculo.Value;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@Valor').Value                    := IIf(vcbTipoCalculo.Value = TD_PORCENTUAL, txtValor.ValueInt, txtValor.ValueInt * 100);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@FechaDesde').Value               := dateFechaDesde.Date;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@FechaHasta').Value               := IIf(dateFechaHasta.Date <> NullDate, dateFechaHasta.Date, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@TipoAsignacion').Value           := vcbTipoAsignacion.Value;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@Prioridad').Value                := txtPrioridad.ValueInt;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@IDCategoriaClase').Value         := IIf(vcbClaseCategoria.ItemIndex > 0, vcbClaseCategoria.Value, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CodigoCategoria').Value          := IIf(vcbCategoria.ItemIndex > 0, vcbCategoria.Value, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CantidadTransitos').Value        := txtCantidadTransitos.ValueInt;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@MontoTransitos').Value           := (txtMontoTransitos.ValueInt * 100);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CodigoConcesionaria').Value      := IIf(vcbConcesionaria.ItemIndex > 0, vcbConcesionaria.Value, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@Personeria').Value               := IIf(vcbPersoneria.ItemIndex > 0, vcbPersoneria.Value, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@TipoMedioPagoAutomatico').Value  := IIf(vcbTipoMedioPago.ItemIndex > 0, vcbTipoMedioPago.Value, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CodigoPais').Value               := PAIS_CHILE;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CodigoRegion').Value             := IIf(vcbRegion.ItemIndex > 0, vcbRegion.Value, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CodigoComuna').Value             := IIf(vcbComuna.ItemIndex > 0, vcbComuna.Value, Null);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@CantidadVehiculos').Value        := txtCantidadVehiculos.ValueInt;
                spReglasDescuentos_INSERT.Parameters.ParamByName('@MontoFacturacion').Value         := (txtMontoFacturacion.ValueInt * 100);
                spReglasDescuentos_INSERT.Parameters.ParamByName('@UsuarioCreacion').Value          := UsuarioSistema;
                spReglasDescuentos_INSERT.ExecProc;

                if spReglasDescuentos_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spReglasDescuentos_INSERT.Parameters.ParamByName('@ErrorDescription').Value);

                Guardado := True;

                edtCodigoRegla.Text := spReglasDescuentos_INSERT.Parameters.ParamByName('@CodigoRegla').Value;

                cdsReglas.Append;

                for I := 0 to cdsReglas.Fields.Count - 1 do
                    cdsReglas.Fields[I].ReadOnly := False;

                cdsReglas.FieldByName('CodigoRegla').Value                  := edtCodigoRegla.Text;
                cdsReglas.FieldByName('Descripcion').Value                  := Trim(edtDescripcion.Text);
                cdsReglas.FieldByName('TipoCalculo').Value                  := vcbTipoCalculo.Value;
                cdsReglas.FieldByName('TipoCalculoDesc').Value              := vcbTipoCalculo.Text;
                cdsReglas.FieldByName('Valor').Value                        := txtValor.ValueInt;
                cdsReglas.FieldByName('FechaDesde').Value                   := dateFechaDesde.Date;
                cdsReglas.FieldByName('FechaHasta').Value                   := IIf(dateFechaHasta.Date <> NullDate, dateFechaHasta.Date, 0);
                cdsReglas.FieldByName('TipoAsignacion').Value               := vcbTipoAsignacion.Value;
                cdsReglas.FieldByName('TipoAsignacionDesc').Value           := vcbTipoAsignacion.Text;
                cdsReglas.FieldByName('Prioridad').Value                    := txtPrioridad.ValueInt;
                cdsReglas.FieldByName('IDCategoriaClase').Value             := vcbClaseCategoria.Value;
                cdsReglas.FieldByName('CategoriaClaseDesc').Value           := vcbClaseCategoria.Text;
                cdsReglas.FieldByName('CodigoCategoria').Value              := vcbCategoria.Value;
                cdsReglas.FieldByName('CategoriaDesc').Value                := vcbCategoria.Text;
                cdsReglas.FieldByName('CantidadTransitos').Value            := txtCantidadTransitos.ValueInt;
                cdsReglas.FieldByName('MontoTransitos').Value               := txtMontoTransitos.ValueInt;
                cdsReglas.FieldByName('CodigoConcesionaria').Value          := vcbConcesionaria.Value;
                cdsReglas.FieldByName('ConcesionariaDesc').Value            := vcbConcesionaria.Text;
                cdsReglas.FieldByName('Personeria').Value                   := vcbPersoneria.Value;
                cdsReglas.FieldByName('PersoneriaDesc').Value               := vcbPersoneria.Text;
                cdsReglas.FieldByName('TipoMedioPagoAutomatico').Value      := IIf(vcbTipoMedioPago.ItemIndex > 0, vcbTipoMedioPago.Value, Null);
                cdsReglas.FieldByName('TipoMedioPagoAutomaticoDesc').Value  := vcbTipoMedioPago.Text;
                cdsReglas.FieldByName('CodigoPais').Value                   := PAIS_CHILE;
                cdsReglas.FieldByName('CodigoRegion').Value                 := vcbRegion.Value;
                cdsReglas.FieldByName('RegionDesc').Value                   := vcbRegion.Text;
                cdsReglas.FieldByName('CodigoComuna').Value                 := vcbComuna.Value;
                cdsReglas.FieldByName('ComunaDesc').Value                   := vcbComuna.Text;
                cdsReglas.FieldByName('CantidadVehiculos').Value            := txtCantidadVehiculos.ValueInt;
                cdsReglas.FieldByName('MontoFacturacion').Value             := txtMontoFacturacion.ValueInt;
                cdsReglas.FieldByName('FechaHoraCreacion').Value            := Now;
                cdsReglas.FieldByName('UsuarioCreacion').Value              := UsuarioSistema;
                cdsReglas.Post;
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_INSERT, e.Message, 'Error', MB_ICONERROR);
                    If not Guardado then Exit;
                end;
            end;
        end;

        if Accion = 2 then begin
            try
                spReglasDescuentos_UPDATE.Parameters.Refresh;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CodigoRegla').Value              := edtCodigoRegla.Text;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@Descripcion').Value              := Trim(edtDescripcion.Text);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@TipoCalculo').Value              := vcbTipoCalculo.Value;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@Valor').Value                    := IIf(vcbTipoCalculo.Value = TD_PORCENTUAL, txtValor.ValueInt, txtValor.ValueInt * 100);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@FechaDesde').Value               := dateFechaDesde.Date;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@FechaHasta').Value               := IIf(dateFechaHasta.Date <> NullDate, dateFechaHasta.Date, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@TipoAsignacion').Value           := vcbTipoAsignacion.Value;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@Prioridad').Value                := txtPrioridad.ValueInt;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@IDCategoriaClase').Value         := IIf(vcbClaseCategoria.ItemIndex > 0, vcbClaseCategoria.Value, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CodigoCategoria').Value          := IIf(vcbCategoria.ItemIndex > 0, vcbCategoria.Value, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CantidadTransitos').Value        := txtCantidadTransitos.ValueInt;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@MontoTransitos').Value           := (txtMontoTransitos.ValueInt * 100);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CodigoConcesionaria').Value      := IIf(vcbConcesionaria.ItemIndex > 0, vcbConcesionaria.Value, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@Personeria').Value               := IIf(vcbPersoneria.ItemIndex > 0, vcbPersoneria.Value, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@TipoMedioPagoAutomatico').Value  := IIf(vcbTipoMedioPago.ItemIndex > 0, vcbTipoMedioPago.Value, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CodigoPais').Value               := PAIS_CHILE;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CodigoRegion').Value             := IIf(vcbRegion.ItemIndex > 0, vcbRegion.Value, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CodigoComuna').Value             := IIf(vcbComuna.ItemIndex > 0, vcbComuna.Value, Null);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@CantidadVehiculos').Value        := txtCantidadVehiculos.ValueInt;
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@MontoFacturacion').Value         := (txtMontoFacturacion.ValueInt * 100);
                spReglasDescuentos_UPDATE.Parameters.ParamByName('@UsuarioModificacion').Value      := UsuarioSistema;
                spReglasDescuentos_UPDATE.ExecProc;

                if spReglasDescuentos_UPDATE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spReglasDescuentos_UPDATE.Parameters.ParamByName('@ErrorDescription').Value);

                Guardado := True;

                cdsReglas.Edit;

                for I := 0 to cdsReglas.Fields.Count - 1 do
                    cdsReglas.Fields[I].ReadOnly := False;

                cdsReglas.FieldByName('Descripcion').Value                  := Trim(edtDescripcion.Text);
                cdsReglas.FieldByName('TipoCalculo').Value                  := vcbTipoCalculo.Value;
                cdsReglas.FieldByName('TipoCalculoDesc').Value              := vcbTipoCalculo.Text;
                cdsReglas.FieldByName('Valor').Value                        := txtValor.ValueInt;
                cdsReglas.FieldByName('FechaDesde').Value                   := dateFechaDesde.Date;
                cdsReglas.FieldByName('FechaHasta').Value                   := IIf(dateFechaHasta.Date <> NullDate, dateFechaHasta.Date, 0);
                cdsReglas.FieldByName('TipoAsignacion').Value               := vcbTipoAsignacion.Value;
                cdsReglas.FieldByName('TipoAsignacionDesc').Value           := vcbTipoAsignacion.Text;
                cdsReglas.FieldByName('Prioridad').Value                    := txtPrioridad.ValueInt;
                cdsReglas.FieldByName('IDCategoriaClase').Value             := vcbClaseCategoria.Value;
                cdsReglas.FieldByName('CategoriaClaseDesc').Value           := vcbClaseCategoria.Text;
                cdsReglas.FieldByName('CodigoCategoria').Value              := vcbCategoria.Value;
                cdsReglas.FieldByName('CategoriaDesc').Value                := vcbCategoria.Text;
                cdsReglas.FieldByName('CantidadTransitos').Value            := txtCantidadTransitos.ValueInt;
                cdsReglas.FieldByName('MontoTransitos').Value               := txtMontoTransitos.ValueInt;
                cdsReglas.FieldByName('CodigoConcesionaria').Value          := vcbConcesionaria.Value;
                cdsReglas.FieldByName('ConcesionariaDesc').Value            := vcbConcesionaria.Text;
                cdsReglas.FieldByName('Personeria').Value                   := vcbPersoneria.Value;
                cdsReglas.FieldByName('PersoneriaDesc').Value               := vcbPersoneria.Text;
                cdsReglas.FieldByName('TipoMedioPagoAutomatico').Value      := vcbTipoMedioPago.Value;
                cdsReglas.FieldByName('TipoMedioPagoAutomaticoDesc').Value  := vcbTipoMedioPago.Text;
                cdsReglas.FieldByName('CodigoPais').Value                   := PAIS_CHILE;
                cdsReglas.FieldByName('CodigoRegion').Value                 := vcbRegion.Value;
                cdsReglas.FieldByName('RegionDesc').Value                   := vcbRegion.Text;
                cdsReglas.FieldByName('CodigoComuna').Value                 := vcbComuna.Value;
                cdsReglas.FieldByName('ComunaDesc').Value                   := vcbComuna.Text;
                cdsReglas.FieldByName('CantidadVehiculos').Value            := txtCantidadVehiculos.ValueInt;
                cdsReglas.FieldByName('MontoFacturacion').Value             := txtMontoFacturacion.ValueInt;
                cdsReglas.FieldByName('FechaHoraModificacion').Value        := Now;
                cdsReglas.FieldByName('UsuarioModificacion').Value          := UsuarioSistema;
                cdsReglas.Post;
            except
                 on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_INSERT, e.Message, 'Error', MB_ICONERROR);
                    If not Guardado then Exit;
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
        FGuardandoDatos := False;
        cdsReglas.EnableControls;
    end;    

    Accion := 0;
    HabilitarDeshabilitarControles(False);
end;

procedure TfrmABMReglasDescuentos.btnBtnCancelarClick(Sender: TObject);
begin
    Accion := 0;
    Limpiar_Campos;
    HabilitarDeshabilitarControles(False);     
    CargarDatosRegistro;
end;

procedure TfrmABMReglasDescuentos.btnCopiarClick(Sender: TObject);
begin
    Accion := 1;
    dateFechaDesde.Clear;
    dateFechaHasta.Clear;
    edtDescripcion.Text := edtDescripcion.Text + ' - Copia';
    HabilitarDeshabilitarControles(True);
    edtDescripcion.SetFocus;
end;

procedure TfrmABMReglasDescuentos.btnEditarClick(Sender: TObject);
begin
    Accion := 2;
    
    if cdsReglas.FieldByName('FechaDesde').AsDateTime > Ahora then
        HabilitarDeshabilitarControles(True)
    else begin                         
        edtDescripcion.Enabled := True;
        dateFechaHasta.Enabled := True;
    end;

    edtDescripcion.SetFocus;
end;

procedure TfrmABMReglasDescuentos.btnEliminarClick(Sender: TObject);
resourcestring
    MSG_TITLE = 'Eliminar Regla de Descuento';
    MSG_CONFIRM = '�Desea eliminar la Regla de Descuento seleccionada?';
    MSG_ERROR = 'Error eliminando la Regla de Descuento';
    MSG_ELIMINADO = 'La Regla de Descuento ha sido eliminada';
begin
    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then
        Exit;

    try
        spReglasDescuentos_DELETE.Parameters.Refresh;
        spReglasDescuentos_DELETE.Parameters.ParamByName('@CodigoRegla').Value := cdsReglas.FieldByName('CodigoRegla').AsInteger;
        spReglasDescuentos_DELETE.Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
        spReglasDescuentos_DELETE.ExecProc;

        if spReglasDescuentos_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
            raise Exception.Create(spReglasDescuentos_DELETE.Parameters.ParamByName('@ErrorDescription').Value);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;

    MsgBox(MSG_ELIMINADO, MSG_TITLE, MB_ICONINFORMATION);

    CargarRegistros;
end;

procedure TfrmABMReglasDescuentos.btnSalir1Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMReglasDescuentos.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMReglasDescuentos.CargarCategorias(ClaseCategoria: Integer);
resourcestring
    MSG_ERROR_CATEGORIAS = 'Ha ocurrido un error al obtener las Categor�as';
var
    FCategoria: TCategoria;
begin
    vcbCategoria.Items.Clear;
    vcbCategoria.Items.Add(STR_TODAS, 0);
    vcbCategoria.ItemIndex := 0;

    if ClaseCategoria <= 0 then Exit;
    
    FCategoria := TCategoria.Create;
    try
        try
            FCategoria.ObtenerCategorias(0, '', ClaseCategoria);
            while not FCategoria.ClientDataSet.Eof do begin
                vcbCategoria.Items.Add(
                    FCategoria.Descripcion,
                    FCategoria.CodigoCategoria);
                FCategoria.ClientDataSet.Next;
            end;                                
            vcbCategoria.ItemIndex := 0;
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR_CATEGORIAS, e.Message, 'Error', MB_ICONERROR);
        end;
    finally
        FCategoria.Free;
    end;
end;

procedure TfrmABMReglasDescuentos.CargarComunas(CodigoRegion: string);
resourcestring
    MSG_ERROR_COMUNAS = 'Ha ocurrido un error al obtener las Comunas';
var
    sp: TADOStoredProc;
begin
    vcbComuna.Items.Clear;
    vcbComuna.Items.Add(STR_TODAS,'');
    vcbComuna.ItemIndex := 0;

    if CodigoRegion = '' then Exit;

    sp := TADOStoredProc.Create(Self);
    try
        try                                                   
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'ObtenerComunasChileRegion';
            sp.CommandTimeout := 60;
            sp.Parameters.Refresh;                           
            sp.Parameters.ParamByName('@CodigoRegion').Value := CodigoRegion;
            sp.Open;

            while not sp.Eof do begin
                vcbComuna.Items.Add(
                    sp.FieldByName('Descripcion').AsString,
                    sp.FieldByName('CodigoComuna').AsString);
                sp.Next;
            end;
            sp.Close;  
            vcbComuna.ItemIndex := 0;
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR_COMUNAS, e.Message, 'Error', MB_ICONERROR);
        end;
    finally
        sp.Free;
    end;
end;

procedure TfrmABMReglasDescuentos.vcbClaseCategoriaChange(Sender: TObject);
begin
    CargarCategorias(vcbClaseCategoria.Value);
end;

procedure TfrmABMReglasDescuentos.vcbRegionChange(Sender: TObject);
begin
    CargarComunas(vcbRegion.Value);
end;

procedure TfrmABMReglasDescuentos.vcbTipoCalculoChange(Sender: TObject);
begin
    txtValor.Clear;
    txtValor.MaxLength := 0;
    
    if vcbTipoCalculo.Value = TD_PORCENTUAL then
        txtValor.MaxLength := 3;    
end;

procedure TfrmABMReglasDescuentos.CargarRegistros;
begin
    spReglasDescuentos_SELECT.Parameters.Refresh;
    spReglasDescuentos_SELECT.Open;

    if spReglasDescuentos_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
        raise Exception.Create(spReglasDescuentos_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

    cdsReglas.Data := dtstprReglas.Data;
end;

procedure TfrmABMReglasDescuentos.CargarDatosRegistro;
var
    RegionActual, ClaseActual: Variant;
begin                               
    ClaseActual := vcbClaseCategoria.Value;
    RegionActual := vcbRegion.Value;

    edtCodigoRegla.Text         := cdsReglas.FieldByName('CodigoRegla').AsString;
    edtDescripcion.Text         := cdsReglas.FieldByName('Descripcion').AsString;
    vcbTipoCalculo.Value        := cdsReglas.FieldByName('TipoCalculo').AsString;
    txtValor.Value              := cdsReglas.FieldByName('Valor').AsInteger;
    vcbTipoAsignacion.Value     := cdsReglas.FieldByName('TipoAsignacion').AsString;
    txtPrioridad.Value          := cdsReglas.FieldByName('Prioridad').AsInteger;
    dateFechaDesde.Date         := cdsReglas.FieldByName('FechaDesde').AsDateTime;
    if cdsReglas.FieldByName('FechaHasta').AsDateTime > 0 then
        dateFechaHasta.Date         := cdsReglas.FieldByName('FechaHasta').AsDateTime;
    vcbClaseCategoria.Value     := cdsReglas.FieldByName('IDCategoriaClase').AsInteger;
    txtCantidadTransitos.Value  := cdsReglas.FieldByName('CantidadTransitos').AsInteger;
    txtMontoTransitos.Value     := cdsReglas.FieldByName('MontoTransitos').AsInteger;
    vcbConcesionaria.Value      := cdsReglas.FieldByName('CodigoConcesionaria').AsInteger;
    vcbRegion.Value             := cdsReglas.FieldByName('CodigoRegion').AsString;
    vcbPersoneria.Value         := cdsReglas.FieldByName('Personeria').AsString;
    vcbTipoMedioPago.Value      := cdsReglas.FieldByName('TipoMedioPagoAutomatico').AsString;
    txtCantidadVehiculos.Value  := cdsReglas.FieldByName('CantidadVehiculos').AsInteger;
    txtMontoFacturacion.Value   := cdsReglas.FieldByName('MontoFacturacion').AsInteger;

    if ClaseActual <> vcbClaseCategoria.Value then
        CargarCategorias(vcbClaseCategoria.Value);

    if RegionActual <> vcbRegion.Value then
        CargarComunas(vcbRegion.Value);

    vcbCategoria.Value          := cdsReglas.FieldByName('CodigoCategoria').AsInteger;
    vcbComuna.Value             := cdsReglas.FieldByName('CodigoComuna').AsString;
end;

procedure TfrmABMReglasDescuentos.cdsReglasAfterScroll(DataSet: TDataSet);
begin
    if FGuardandoDatos then Exit;

    btnEliminar.Enabled := True;
    btnEditar.Enabled := True;

    if (cdsReglas.FieldByName('FechaHasta').AsDateTime <= Ahora) and
        (cdsReglas.FieldByName('FechaHasta').AsDateTime > 0) then begin
        btnEliminar.Enabled := False;
        btnEditar.Enabled := False;
    end;

    CargarDatosRegistro;
end;

procedure TfrmABMReglasDescuentos.dbgrdReglasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
resourcestring
    STR_COLUMN_ESTADO = 'Estado';
    STR_ESTADO_PENDIENTE = ' Pendiente';
    STR_ESTADO_EN_CURSO = ' En Curso';
    STR_ESTADO_FINALIZADO = ' Finalizado';
var
    aRect: TRect;
    Estado: String;
begin
    aRect := Rect;
    Estado := STR_ESTADO_PENDIENTE;

    if (cdsReglas.FieldByName('FechaHasta').AsDateTime <= Ahora) and
        (cdsReglas.FieldByName('FechaHasta').AsDateTime > 0) then begin
        dbgrdReglas.Canvas.Font.Color := clRed;
        dbgrdReglas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
        Estado := STR_ESTADO_FINALIZADO;
    end else if cdsReglas.FieldByName('FechaDesde').AsDateTime <= Ahora then begin
        dbgrdReglas.Canvas.Font.Color := clGreen;
        dbgrdReglas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
        Estado := STR_ESTADO_EN_CURSO;
    end;

    if Column.Title.Caption = STR_COLUMN_ESTADO then
        DrawText(dbgrdReglas.Canvas.Handle, PChar(Estado), Length(Estado), aRect, DT_SINGLELINE or DT_LEFT or DT_VCENTER);
end;

procedure TfrmABMReglasDescuentos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmABMReglasDescuentos.HabilitarDeshabilitarControles(Estado: Boolean);
begin
    btnSalir.Enabled        := not Estado;
    btnAgregar.Enabled      := not Estado;
    btnEliminar.Enabled     := (not Estado) and (cdsReglas.RecordCount > 0);
    btnEditar.Enabled       := (not Estado) and (cdsReglas.RecordCount > 0);
    btnCopiar.Enabled       := (not Estado) and (cdsReglas.RecordCount > 0);  
    dbgrdReglas.Enabled     := (not Estado) and (cdsReglas.RecordCount > 0);

    edtDescripcion.Enabled          := Estado;
    dateFechaDesde.Enabled          := Estado and ((dateFechaDesde.Date > Ahora) or (dateFechaDesde.Date <= 0));
    dateFechaHasta.Enabled          := Estado and ((dateFechaHasta.Date > Ahora) or (dateFechaHasta.Date <= 0));

    if (dateFechaDesde.Date > Ahora) or (dateFechaDesde.Date <= 0) then begin
        vcbTipoCalculo.Enabled          := Estado;
        txtValor.Enabled                := Estado and (vcbTipoCalculo.ItemIndex > 0);
        vcbTipoAsignacion.Enabled       := Estado;
        txtPrioridad.Enabled            := Estado;
        vcbClaseCategoria.Enabled       := Estado;
        vcbCategoria.Enabled            := Estado;
        txtCantidadTransitos.Enabled    := Estado;
        txtMontoTransitos.Enabled       := Estado;
        vcbConcesionaria.Enabled        := Estado;
        vcbRegion.Enabled               := Estado;
        vcbComuna.Enabled               := Estado;
        vcbPersoneria.Enabled           := Estado;
        vcbTipoMedioPago.Enabled        := Estado;
        txtCantidadVehiculos.Enabled    := Estado;
        txtMontoFacturacion.Enabled     := Estado;
    end;

    if Estado then
        Notebook.ActivePage := 'PageModi'
    else
        Notebook.ActivePage := 'PageSalir';
end;

procedure TfrmABMReglasDescuentos.Limpiar_Campos;
begin
    edtDescripcion.Clear;
    vcbTipoCalculo.ItemIndex := 0;
    txtValor.Clear;
    vcbTipoAsignacion.ItemIndex := 0;
    txtPrioridad.Value := 0;
    dateFechaDesde.Clear;
    dateFechaHasta.Clear;
    vcbClaseCategoria.ItemIndex := 0;
    vcbCategoria.ItemIndex := 0;
    txtCantidadTransitos.Clear; 
    txtMontoTransitos.Clear;
    vcbConcesionaria.ItemIndex := 0;
    vcbRegion.ItemIndex := 0;
    vcbComuna.ItemIndex := 0;
    vcbPersoneria.ItemIndex := 0;
    vcbTipoMedioPago.ItemIndex := 0;
    txtCantidadVehiculos.Clear;
    txtMontoFacturacion.Clear;
end;

procedure TfrmABMReglasDescuentos.txtValorChange(Sender: TObject);
resourcestring
    MSG_ERROR_PORCENTAJE = 'El porcentaje no puede ser mayor a 100';
begin
    if vcbTipoCalculo.ItemIndex = 0 then begin
        txtValor.Clear;
        txtValor.Enabled := False;
    end else
        txtValor.Enabled := (Accion > 0);

    if (vcbTipoCalculo.Value = TD_PORCENTUAL) and (txtValor.ValueInt > 100) then begin
        txtValor.Value := 100;
        txtValor.SetFocus;
        MsgBoxBalloon(MSG_ERROR_PORCENTAJE, 'Error', MB_ICONERROR, txtValor);
    end;
end;

end.
