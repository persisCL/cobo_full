{-------------------------------------------------------------------------------
 File Name: frm_ReporteNotaCobroDirecta.pas
 Author:  mlopez
 Date Created: 01/06/2005
 Language: ES-AR
 Description: Nota de Cobro directa y Detalle de la nota de cobro



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

-------------------------------------------------------------------------------}
unit frm_ReporteNotaCobroDirecta;

interface

uses
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
   Dialogs, ppBands, ppReport, ppSubRpt, {ppChrt, ppChrtDP,} ppCtrls,
   ppStrtch, ppMemo, ppParameter, ppBarCod, jpeg, ppPrnabl, ppClass, UtilRB,
   ppCache, ppComm, ppRelatv, ppProd, ppModule, raCodMod, DB,
   ADODB, ppDB, ppDBPipe,util,utilproc, ppVar, StdCtrls, daDataModule,
   ppRegion, ppCTMain, ppTxPipe, rbSetup, TeEngine, Series, ExtCtrls,
   TeeProcs, Chart, DbChart, TXComp, ppDevice, ConstParametrosGenerales, Printers, PeaTypes, UtilDb,
   ImprimirWO, TXRB, DMConnection;

type
  TTipoPago = (tpNinguno, tpPAT, tpPAC);               
  Tform_ReporteNotaCobroDirecta = class(TForm)
    rpt_ReporteNotaCobroDirecta: TppReport;
    rb_ReporteNotaCobroDirecta: TRBInterface;
    ppParameterList1: TppParameterList;
    Label1: TLabel;
    dbpl_ObtenerMontosPercibidosDayPass: TppDBPipeline;
    sp_ObtenerMontosPercibidosDayPass: TADOStoredProc;
    ds_sp_ObtenerMontosPercibidosDayPass: TDataSource;
    sp_ObtenerNotasCobroNQ: TADOStoredProc;
    sp_ObtenerCodigoLineaComprobanteNQ: TADOStoredProc;
    rb_ReporteDetalleDayPass: TRBInterface;
    rpt_DetalleDayPass: TppReport;
    ppHeaderBand3: TppHeaderBand;
    ppDetailBand5: TppDetailBand;
    ppFooterBand3: TppFooterBand;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    sp_ObtenerDetalleNotaCobroNQ: TADOStoredProc;
    ds_ObtenerDetalleNotaCobroNQ: TDataSource;
    dbpl_ObtenerDetalle: TppDBPipeline;
    ppTitleBand2: TppTitleBand;
    ppLabel11: TppLabel;
    ppImage2: TppImage;
    lblTitulo: TppLabel;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine2: TppLine;
    ppLine5: TppLine;
    ppDBCalc2: TppDBCalc;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel12: TppLabel;
    ppHeaderBand1: TppHeaderBand;
    ppRegion1: TppRegion;
    ppShape4: TppShape;
    pplblNumero: TppLabel;
    ppShape6: TppShape;
    ppShape5: TppShape;
    ppShape3: TppShape;
    lblComprobanteNro: TppLabel;
    ppLabel15: TppLabel;
    ppShape7: TppShape;
    ppShape8: TppShape;
    ppShape9: TppShape;
    ppLine4: TppLine;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLine9: TppLine;
    pplblFechaVencimiento: TppLabel;
    pplblVencimiento: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppShape18: TppShape;
    ppShape17: TppShape;
    ppLabel25: TppLabel;
    ppLabel13: TppLabel;
    ppLabel28: TppLabel;
    ppLabel34: TppLabel;
    pplblPeajesDelPeriodo: TppLabel;
    pplblTotalNotaCobro: TppLabel;
    pplblTotalAPagar: TppLabel;
    ppLabel1: TppLabel;
    ppsb_MontosPercibidosDayPass: TppSubReport;
    ppChildReport1: TppChildReport;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLine1: TppLine;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLine3: TppLine;
    pplCantidad: TppLabel;
    pplImporte: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppSummaryBand3: TppSummaryBand;
    pplblNumero2: TppLabel;
    pplblFechaVencimiento2: TppLabel;
    pplblTotalAPagar2: TppLabel;
    lblComprobanteNroPie: TppLabel;
    raCodeModule3: TraCodeModule;
    ppHeaderBand2: TppHeaderBand;
    pplTotalIMporte: TppLabel;
    raCodeModule1: TraCodeModule;
    pplblDatosLinea1: TppLabel;
    pplblDatosLinea2: TppLabel;
    pplblDatosLinea3: TppLabel;
    ADOStoredProc1: TADOStoredProc;
    ppImageFondo: TppImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
   private
    { Private declarations }

    function GetCodigoLineaComprobante(TipoComprobante: AnsiString; NumeroComprobante: Longint): Integer;
  public
    { Public declarations }
    function Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Integer): Boolean;
    function ImprimirComprobante(TipoComprobante: AnsiString; NumeroComprobante: Integer): Boolean;
    function ImprimirDetalle(TipoComprobante: AnsiString; NumeroComprobante: Integer): Boolean;
  end;

var
  form_ReporteNotaCobroDirecta: Tform_ReporteNotaCobroDirecta;

implementation

{$R *.dfm}

resourcestring
    CAPTION_TITLE_DETAIL_PDU      = 'Detalle de Consumo (Nota Cobro PDU N�: %d)';
    CAPTION_TITLE_DETAIL_BHTU     = 'Detalle de Consumo (Nota Cobro BHTU N�: %d)';


{******************************** Function Header ******************************
Function Name: Inicializar
Author :  mlopez
Date Created :  01-06-2005
Description : Inicilizaci�n de este formulario
Parameters :
Return Value : Boolean
********************************************************************************
Revision 1:
    Author : ggomez
    Date : 16/03/2006
    Description : Agregu� el par�metro TipoComprobante
*******************************************************************************}
function Tform_ReporteNotaCobroDirecta.Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Integer): Boolean;
begin
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: GetCodigoLineaComprobante
Author : mlopez
Date Created : 01-06-2005
Description : Obtenengo el codigo linea comprobante
Parameters : NumeroComprobante: Integer
Return Value : Boolean
********************************************************************************
Revision 1:
    Author : ggomez
    Date : 16/03/2006
    Description : Agregu� el par�metro TipoComprobante
*******************************************************************************}
function Tform_ReporteNotaCobroDirecta.GetCodigoLineaComprobante(TipoComprobante : AnsiString; NumeroComprobante : Longint): Integer;
begin
    with sp_ObtenerCodigoLineaComprobanteNQ do begin
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@TipoComprobante').Value    := TipoComprobante;
        Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
        Parameters.ParamByName('@CodigoLineaComprobante').Value := -1;
        CommandTimeOut := 500;
        ExecProc;
        Result := Parameters.ParamByName('@CodigoLineaComprobante').Value;
        Close;
    end;
end;

{******************************** Function Header ******************************
Function Name: ImprimirComprobante
Author :  mlopez
Date Created :  01-06-05
Description : Imprimo la nota de cobro directa
Parameters :
Return Value : Boolean
********************************************************************************
Revision 1:
    Author : ggomez
    Date : 16/03/2006
    Description : Agregu� el par�metro TipoComprobante

Revision 2:
    Author : ggomez
    Date : 22/06/2006
    Description :
        - Se realizo la carga dinamica de la imagen de fondo del reporte de
        acuerdo de la fecha de la info a mostrar ya q la empresa cambio de el
        rut y en los informes, tiene que estar el rut acorde de acuerdo al
        momento de la creacion de los comprobantes y no en el momento de la
        impresion.

        - Agregu� comentarios.
        - Indent� como se debe.
        - Correg� el if que comparaba la fecha de creacion con la fecha de corte RUT.
        - Agregu� resourcestring

*******************************************************************************}
function Tform_ReporteNotaCobroDirecta.ImprimirComprobante(TipoComprobante: AnsiString; NumeroComprobante: Integer): Boolean;
resourcestring
    ERROR_TITLE = 'Error de Impresi�n';
    ERROR_MESAGE = 'No se encuentra el Comprobante';
    ERROR_DESCRIPTION = 'Se ha producido un error al ejecutar el procedimiento ObtenerNotasCobroNQ';
    MSG_INVOICE_NOT_FOUND = 'No se encuentra el Comprobante';
    MSG_LINE_CODE_NOT_FOUND = 'No se encuentra el Codigo de L�nea';
    CAPTION_NRO_INVOICE_PDU     = 'N. COB. DIREC. PDU N�';
    CAPTION_NRO_INVOICE_BHTU    = 'N. COB. DIREC. BHTU N�';

    MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE = 'Error al obtener datos de configuraci�n del reporte.'
                                                + CRLF + 'Consulte al Administrador de Sistemas';
    MSG_ERROR_OBTENER_FECHA_CORTE_RUT = 'Ha ocurrido un error al obtener la fecha desde la cual la concesionaria cambio el RUT.';
    MSG_ERROR_OBTENER_DATOS_IMG_FONDO = 'Ha ocurrido un error al obtener los datos de la imagen de fondo.';
    MSG_ERROR_FALTAN_IMAGENES = 'No existen todas las im�genes requeridas para'
        + CRLF + 'poder imprimir una Nota de Cobro a Infracciones en el sistema.';
    MSG_ERROR_OBTENER_IMG_FONDO = 'Ha ocurrido un error al obtener la imagen de fondo.';

var
    CodigoLineaComprobante: Integer;
    ImporteAPagar : AnsiString;

    FechaCreacion, FechaDeCorte: TDateTime;
    FDirImagenFondo, FNombreImagenFondo: string;
begin
    Result := False;
    // Revision 2: Levantar la imagen para el fondo de la Nota de Cobro Directa.
    try
        ObtenerParametroGeneral(sp_ObtenerNotasCobroNQ.Connection, 'FECHA_DE_CORTE_RUT', FechaDeCorte);
    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_FECHA_CORTE_RUT, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end;
    try
        FechaCreacion := QueryGetValueDateTime(sp_ObtenerNotasCobroNQ.Connection,
            ' select FechaCreacion '+
            ' from Comprobantes with (nolock) ' +
            ' where (NumeroComprobante = ''' + IntToStr(NumeroComprobante) + ''')' +
                                  ' and(TipoComprobante = ''' + TipoComprobante + ''')');
    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end;

    // Si la Fecha de Creaci�n del Recibo es anterior a la Fecha desde cuando la
    // concesionaria tiene nuevo RUT, entonces imprimir el RUT Anterior, sino
    // imprimir el RUT Nuevo.
    try
        if ( FechaCreacion < FechaDeCorte )then begin
            ObtenerParametroGeneral(sp_ObtenerNotasCobroNQ.Connection, 'IMG_FONDO_NOTA_COBRO_DIRECTA_ANT', FNombreImagenFondo);
        end else begin
            ObtenerParametroGeneral(sp_ObtenerNotasCobroNQ.Connection, 'IMG_FONDO_NOTA_COBRO_DIRECTA', FNombreImagenFondo);
        end;

        ObtenerParametroGeneral(sp_ObtenerNotasCobroNQ.Connection, 'DIR_IMG_FONDO_NOTA_COBRO_DIRECTA', FDirImagenFondo);
    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_DATOS_IMG_FONDO, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end; // except

    FDirImagenFondo := GoodDir(FDirImagenFondo);
    FDirImagenFondo := FDirImagenFondo + FNombreImagenFondo;

    try
        if (FileExists(FDirImagenFondo)) then begin
            ppImageFondo.Picture.LoadFromFile(FDirImagenFondo);
        end else begin
            raise Exception.Create(MSG_ERROR_FALTAN_IMAGENES);
        end;
    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_IMG_FONDO, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end; // except
    // Fin Revision 2

    try
        with sp_ObtenerNotasCobroNQ do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@TipoComprobante').Value    := TipoComprobante;
            Parameters.ParamByName('@NumeroComprobante').Value  := NumeroComprobante;
            Parameters.ParamByName('@RutProveedorDayPass').Value := NULL;
            CommandTimeOut := 500;
            Open;
            if not IsEmpty then begin
                if TipoComprobante = TC_NOTA_COBRO_DIRECTA then begin
                    // Setear el label con el Tipo de Comprobante PDU
                    lblComprobanteNro.Caption       := CAPTION_NRO_INVOICE_PDU;
                    lblComprobanteNroPie.Caption    := lblComprobanteNro.Caption;
                end else begin
                    // Setear el label con el Tipo de Comprobante BHTU
                    lblComprobanteNro.Caption       := CAPTION_NRO_INVOICE_BHTU;
                    lblComprobanteNroPie.Caption    := lblComprobanteNro.Caption;
                end;

                pplblNumero.Caption := FieldByName('NumeroComprobante').AsString;
                pplblNumero2.Caption := FieldByName('NumeroComprobante').AsString;
                pplblVencimiento.Caption := DateToStr(FieldByName('FechaVencimiento').AsDateTime);
                pplblFechaVencimiento.Caption := DateToStr(FieldByName('FechaVencimiento').AsDateTime);
                pplblFechaVencimiento2.Caption := DateToStr(FieldByName('FechaVencimiento').AsDateTime);
                pplblPeajesDelPeriodo.Caption := FieldByName('DescriAPagar').AsString;
                pplblTotalNotaCobro.Caption := FieldByName('DescriAPagar').AsString;
                pplblTotalAPagar.Caption := FieldByName('DescriAPagar').AsString;
                pplblTotalAPagar2.Caption := FieldByName('DescriAPagar').AsString;
                ImporteAPagar := FieldByName('DescriAPagar').AsString;
                // Datos del Cliente
                pplblDatosLinea1.Caption := FieldByName('Apellido').AsString + ' ' + FieldByName('ApellidoMaterno').AsString + ' ' + FieldByName('Nombre').AsString;
                pplblDatosLinea2.Caption := FieldByName('Calle').AsString + ' ' + FieldByName('Numero').AsString + ' ' + FieldByName('DetalleDomicilio').AsString;
                pplblDatosLinea3.Caption := FieldByName('Comuna').AsString + ' - ' + FieldByName('Region').AsString;
            end else begin
                MsgBox(MSG_INVOICE_NOT_FOUND, ERROR_TITLE, MB_ICONERROR);
                Exit;
            end;
        end;

        CodigoLineaComprobante := GetCodigoLineaComprobante(TipoComprobante, NumeroComprobante);

        if CodigoLineaComprobante <> -1 then begin

            with sp_ObtenerDetalleNotaCobroNQ do begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoLineaComprobante').Value := CodigoLineaComprobante;
                Parameters.ParamByName('@CantidadDayPass').Value        := -1;
                Parameters.ParamByName('@TotalDayPass').Value           := EmptyStr;
                CommandTimeOut := 500;
                Open;

                if TipoComprobante = TC_NOTA_COBRO_DIRECTA then begin
                    // Setear el label del T�tulo el Tipo de Comprobante PDU
                    lblTitulo.Caption := Format(CAPTION_TITLE_DETAIL_PDU, [NumeroComprobante]);
                end else begin
                    // Setear el label del T�tulo el Tipo de Comprobante BHTU
                    lblTitulo.Caption := Format(CAPTION_TITLE_DETAIL_BHTU, [NumeroComprobante]);
                end;

                pplCantidad.Caption := Parameters.ParamByName('@CantidadDayPass').Value;
                pplImporte.Caption := iif(Parameters.ParamByName('@TotalDayPass').Value = Null, EmptyStr, ImporteAPagar); {Parameters.ParamByName('@TotalDayPass').Value}
            end;
        end else begin
            MsgBox(MSG_LINE_CODE_NOT_FOUND, ERROR_TITLE, MB_ICONERROR);
            Exit;
        end;
        Result := rb_ReporteNotaCobroDirecta.Execute;
    except
        on E : Exception do begin
            Result := False;
            MsgBoxErr(ERROR_MESAGE, ERROR_DESCRIPTION + CRLF + E.Message, ERROR_TITLE, MB_ICONERROR);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ImprimirDetalle
Author :  mlopez
Date Created :  01-06-2005
Description :  Imprimo el detalle de la nota de cobro
Parameters :
Return Value : Boolean
********************************************************************************
Revision 1:
    Author : ggomez
    Date : 16/03/2006                                    
    Description : Agregu� el par�metro TipoComprobante
*******************************************************************************}
function Tform_ReporteNotaCobroDirecta.ImprimirDetalle(TipoComprobante: AnsiString; NumeroComprobante: Integer): Boolean;
resourcestring
    ERROR_TITLE = 'Error de Impresi�n';
    ERROR_MENSAJE = 'No se encuentra el Comprobante';
    ERROR_DESCRIPTION = 'Se ha producido un error al ejecutar el procedimiento ObtenerDetalleNotasCobroNQ';
    MSG_COMPROBANTE_NOT_FOUND = 'No se encuentra el Comprobante';
    MSG_LINE_CODE_NOT_FOUND = 'No se encuentra el Codigo de L�nea';
var
    CodigoLineaComprobante: Integer;
    RutaLogo: AnsiString;                                                                                 //SS_1147_NDR_20140710
begin
    Result := false;
    try
        ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
        try                                                                                                 //SS_1147_NDR_20140710
          ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
        except                                                                                              //SS_1147_NDR_20140710
          On E: Exception do begin                                                                          //SS_1147_NDR_20140710
            Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
            MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
            Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
          end;                                                                                              //SS_1147_NDR_20140710
        end;                                                                                                //SS_1147_NDR_20140710


        CodigoLineaComprobante := GetCodigoLineaComprobante(TipoComprobante, NumeroComprobante);

        if CodigoLineaComprobante <> -1 then begin

            with sp_ObtenerDetalleNotaCobroNQ do begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoLineaComprobante').Value := CodigoLineaComprobante;
                Parameters.ParamByName('@CantidadDayPass').Value        := -1;
                Parameters.ParamByName('@TotalDayPass').Value           := EmptyStr;
                CommandTimeOut := 500;
                Open;

                if TipoComprobante = TC_NOTA_COBRO_DIRECTA then begin
                    // Setear el label del T�tulo el Tipo de Comprobante PDU
                    lblTitulo.Caption := Format(CAPTION_TITLE_DETAIL_PDU, [NumeroComprobante]);
                end else begin
                    // Setear el label del T�tulo el Tipo de Comprobante BHTU
                    lblTitulo.Caption := Format(CAPTION_TITLE_DETAIL_BHTU, [NumeroComprobante]);
                end;

                pplTotalImporte.Caption := iif(Parameters.ParamByName('@TotalDayPass').Value = Null, EmptyStr, Parameters.ParamByName('@TotalDayPass').Value);
            end;
        end else begin
            MsgBox(MSG_LINE_CODE_NOT_FOUND, ERROR_TITLE, MB_ICONERROR);
            Exit;
        end;
        Result := rb_ReporteDetalleDayPass.Execute;
    except
        on E : Exception do begin
            Result := False;
            MsgBoxErr(ERROR_MENSAJE, ERROR_DESCRIPTION, ERROR_TITLE, MB_ICONERROR);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author :  lgisuk
Date Created :  20/03/06
Description :   Libero el formulario  de memoria
Parameters :
Return Value : Boolean
********************************************************************************}
procedure Tform_ReporteNotaCobroDirecta.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    sp_ObtenerNotasCobroNQ.Close;
    sp_ObtenerMontosPercibidosDayPass.Close;
    Action := caFree;
end;

end.
