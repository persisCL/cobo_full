{********************************** File Header ********************************
File Name       : FrmConsultaInfraccionesComprobanteAnular.pas
Author          : CQuezada
Date Created    : 05-08-2013
Language        : ES-CL
Firma           : SS_1081_CQU_20130805
Description     : Seleccion las infracciones de un Comprobante para pasarlos a la nota de credito

*******************************************************************************}
unit FrmConsultaInfraccionesComprobanteAnular;

interface

uses
  DmConnection,
  UtilProc,
  PeaProcs,
  Util,
  UtilDB,
  PeaTypes,
  MenuesContextuales,
  ConstParametrosGenerales,
  SysUtilsCN,
  // General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB,
  DmiCtrls, VariantComboBox, Validate, DateEdit,Abm_obj,RStrings,StrUtils,
  ImgList;

type
  TFormConsultaInfraccionesComprobanteAnular = class(TForm)
	Panel2: TPanel;
	Panel3: TPanel;
	lnCheck: TImageList;
    spObtenerInfraccionesPorMovimiento : TADOStoredProc;
    dblInfracciones : TDBListEx;
    lblTotales: TLabel;
    lblTotSeleccionados: TLabel;
    btnSelectAll: TButton;
    btnDeselectAll: TButton;
    btnInvertirSeleccion: TButton;
    btnBtnSalir: TButton;
	procedure dblInfraccionesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
	procedure btnInvertirSeleccionClick(Sender: TObject);
	procedure btnDeselectAllClick(Sender: TObject);
	procedure btnSelectAllClick(Sender: TObject);
	procedure dblInfraccionesLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
	procedure btn_FiltrarClick(Sender: TObject);
	procedure btn_LimpiarClick(Sender: TObject);
	procedure dblInfraccionesDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
	procedure btnBtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure ActualizarImporteTotalSeleccionados;
    procedure dblInfraccionesColumnsHeaderClick(Sender: TObject);
  private
	FListaInfracciones: TStringList;
	FImporteDeInfraccionesSeleccionadas: Int64;
	FNumeroMovimiento : int64;
	FCantidadTotalDeInfracciones: integer;
	Function  ListaInfraccionesInclude(valor:string):boolean;
	Function  ListaInfraccionesExclude(valor:string):boolean;
	Function  ListaInfraccionesIn(valor:string):boolean;
	{ Private declarations }
  public
	{ Public declarations }
	function Inicializar(aNumeroMovimiento: int64; MDIChild: Boolean; var FListaDeInfracciones:TStringList): Boolean;
	function ImporteTotal: Int64;
	function SelectedAll: boolean;
  end;

var
  FormConsultaInfraccionesComprobanteAnular: TFormConsultaInfraccionesComprobanteAnular;


implementation

{$R *.dfm}

function TFormConsultaInfraccionesComprobanteAnular.Inicializar( aNumeroMovimiento: int64; MDIChild: Boolean; var FListaDeInfracciones:TStringList) : Boolean;
Var S: TSize;
begin
	Result := True;
	try
		CenterForm(Self);
	except
		result := False;
	end;
	FListaInfracciones := FListaDeInfracciones;
	FImporteDeInfraccionesSeleccionadas := 0;
	FCantidadTotalDeInfracciones := 0;
	FNumeroMovimiento := aNumeroMovimiento;
	btn_FiltrarClick(Self);
end;

Function TFormConsultaInfraccionesComprobanteAnular.ListaInfraccionesInclude(valor:string):boolean;
begin
	result:=false;
	try
		if not ListaInfraccionesIn(valor) then FListaInfracciones.Add(valor+'='+IntToStr(FNumeroMovimiento));
		result:=true;
	except
	end;
end;

Function TFormConsultaInfraccionesComprobanteAnular.ListaInfraccionesExclude(valor:string):boolean;
var index:integer;
begin
	result:=false;
	try
		index:=FListaInfracciones.IndexOf(valor+'='+IntToStr(FNumeroMovimiento));
		if index <> -1 then FListaInfracciones.Delete(index);
		result:=true;
	except
	end;
end;

Function TFormConsultaInfraccionesComprobanteAnular.ListaInfraccionesIn(valor:string):boolean;
begin
	result := not (FListaInfracciones.IndexOf(valor+'='+IntToStr(FNumeroMovimiento)) = -1);
end;

procedure TFormConsultaInfraccionesComprobanteAnular.dblInfraccionesColumnsHeaderClick(Sender: TObject);
begin
    SysUtilsCN.DBListExColumnHeaderClickGenerico(Sender);
end;

procedure TFormConsultaInfraccionesComprobanteAnular.dblInfraccionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	i: Integer;
begin
	//muestra si esta seleccionado para realizar un reclamo o no
	if Column = dblInfracciones.Columns[0] then begin
		i:=iif(not ListaInfraccionesIn(spObtenerInfraccionesPorMovimiento.FieldByName('CodigoInfraccion').Asstring),0,1);
		//verifica si esta seleccionado o no
		lnCheck.Draw(dblInfracciones.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.dblInfraccionesLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
	CodigoInfraccion: string;
begin
	Screen.Cursor := crHourGlass ;
	//selecciono/desselecciono transito
	if Column = dblInfracciones.Columns[0] then begin
		CodigoInfraccion := spObtenerInfraccionesPorMovimiento.FieldByName('CodigoInfraccion').Asstring;
		if ListaInfraccionesIn(CodigoInfraccion) then begin
			ListaInfraccionesExclude(CodigoInfraccion);
			FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas - spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
		end else begin
			ListaInfraccionesInclude(CodigoInfraccion);
			FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas + spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
		end;
		//dibuja el cambio
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC, RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeInfraccionesSeleccionadas) div 100);
		Sender.Invalidate;
		Sender.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.dblInfraccionesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  //si presiona spaceBar, genera lo mismo que el click de seleccion / deselecci�n
  if Key = VK_SPACE then
	dblInfraccionesLinkClick(dblInfracciones , dblInfracciones.Columns[0]);

end;

procedure TFormConsultaInfraccionesComprobanteAnular.btnBtnSalirClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerInfraccionesPorMovimiento.Active then begin
		FImporteDeInfraccionesSeleccionadas := 0;
		spObtenerInfraccionesPorMovimiento.DisableControls ;
		spObtenerInfraccionesPorMovimiento.First ;
		while not spObtenerInfraccionesPorMovimiento.eof do begin
			if ListaInfraccionesIn(spObtenerInfraccionesPorMovimiento.FieldByName('CodigoInfraccion').Asstring) then
				FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas + spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
			spObtenerInfraccionesPorMovimiento.Next ;
		end;
		spObtenerInfraccionesPorMovimiento.EnableControls ;
		spObtenerInfraccionesPorMovimiento.Close;
	end;
	Close;
	Screen.Cursor := crDefault ;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.btn_FiltrarClick(Sender: TObject);
begin
	try
        try
            spObtenerInfraccionesPorMovimiento.DisableControls;
            spObtenerInfraccionesPorMovimiento.Close;
            spObtenerInfraccionesPorMovimiento.CommandTimeout:=500;
            spObtenerInfraccionesPorMovimiento.Parameters.Refresh;
            spObtenerInfraccionesPorMovimiento.Parameters.ParamByName('@NumeroMovimiento').Value := FNumeroMovimiento;
            spObtenerInfraccionesPorMovimiento.Open;
            FCantidadTotalDeInfracciones := spObtenerInfraccionesPorMovimiento.RecordCount;
        except
            on e: exception do begin
                MsgBoxErr('ERROR', e.Message, caption, MB_ICONERROR);
                Exit;
            end;
        end;
	finally
		spObtenerInfraccionesPorMovimiento.EnableControls;
	end;
	ActualizarImporteTotalSeleccionados;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.btn_LimpiarClick(Sender: TObject);
begin
	spObtenerInfraccionesPorMovimiento.close;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.btnSelectAllClick(Sender: TObject);
Var
	CodigoInfraccion: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerInfraccionesPorMovimiento.Active then begin
		spObtenerInfraccionesPorMovimiento.DisableControls;
		spObtenerInfraccionesPorMovimiento.First;
		while not spObtenerInfraccionesPorMovimiento.eof do begin
			CodigoInfraccion := spObtenerInfraccionesPorMovimiento.FieldByName('CodigoInfraccion').Asstring;
			if not ListaInfraccionesIn(CodigoInfraccion) then begin
				ListaInfraccionesInclude(CodigoInfraccion);
				FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas + spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
			end;
			spObtenerInfraccionesPorMovimiento.Next;
		end;
		spObtenerInfraccionesPorMovimiento.EnableControls;
		spObtenerInfraccionesPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeInfraccionesSeleccionadas) div 100);
		dblInfracciones.Invalidate;
		dblInfracciones.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.btnDeselectAllClick(Sender: TObject);
Var
	CodigoInfraccion: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerInfraccionesPorMovimiento.Active then begin
		spObtenerInfraccionesPorMovimiento.DisableControls;
		spObtenerInfraccionesPorMovimiento.First;
		while not spObtenerInfraccionesPorMovimiento.eof do begin
			CodigoInfraccion := spObtenerInfraccionesPorMovimiento.FieldByName('CodigoInfraccion').Asstring;
			if ListaInfraccionesIn(CodigoInfraccion) then begin
				ListaInfraccionesExclude(CodigoInfraccion);
				FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas - spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
			end;
			spObtenerInfraccionesPorMovimiento.Next;
		end;
		spObtenerInfraccionesPorMovimiento.EnableControls;
		spObtenerInfraccionesPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeInfraccionesSeleccionadas) div 100 );
		dblInfracciones.Invalidate;
		dblInfracciones.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.btnInvertirSeleccionClick(Sender: TObject);
Var
	CodigoInfraccion: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerInfraccionesPorMovimiento.Active then begin
		spObtenerInfraccionesPorMovimiento.DisableControls;
		spObtenerInfraccionesPorMovimiento.First;
		while not spObtenerInfraccionesPorMovimiento.eof do begin
			CodigoInfraccion := spObtenerInfraccionesPorMovimiento.FieldByName('CodigoInfraccion').AsString;
			if ListaInfraccionesIn(CodigoInfraccion) then begin
				ListaInfraccionesExclude(CodigoInfraccion);
				FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas - spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
			end else begin
				ListaInfraccionesInclude(CodigoInfraccion);
				FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas + spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
			end;
			spObtenerInfraccionesPorMovimiento.Next;
		end;
		spObtenerInfraccionesPorMovimiento.EnableControls;
		spObtenerInfraccionesPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeInfraccionesSeleccionadas) div 100);
		dblInfracciones.Invalidate;
		dblInfracciones.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

function TFormConsultaInfraccionesComprobanteAnular.ImporteTotal: Int64;
begin
	Result := RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeInfraccionesSeleccionadas) div 100 ;
end;

function TFormConsultaInfraccionesComprobanteAnular.SelectedAll: boolean;
var
  xIndex : integer;
  xCantidad : integer;
begin
	xCantidad := 0;
	//Como en la lista se carg� un string del tipo 'CodigoInfraccion=NumeroMovimiento', el ValueFromIndex nos devuelve el NumeroMovimiento
	for xIndex := 0 to FListaInfracciones.Count -1 do
		if FListaInfracciones.ValueFromIndex[xIndex]  = IntToStr(FNumeroMovimiento) then Inc(xCantidad);

	Result := xCantidad = FCantidadTotalDeInfracciones ;
end;

procedure TFormConsultaInfraccionesComprobanteAnular.ActualizarImporteTotalSeleccionados;
Var
	CodigoInfraccion: string;
begin
	Screen.Cursor := crHourGlass ;
	if (spObtenerInfraccionesPorMovimiento.Active) and  (FListaInfracciones.Count > 0) then begin
		spObtenerInfraccionesPorMovimiento.DisableControls;
		spObtenerInfraccionesPorMovimiento.First;
		FImporteDeInfraccionesSeleccionadas := 0;
		while not spObtenerInfraccionesPorMovimiento.eof do begin
			CodigoInfraccion := spObtenerInfraccionesPorMovimiento.FieldByName('CodigoInfraccion').Asstring;
			if ListaInfraccionesIn(CodigoInfraccion) then
				FImporteDeInfraccionesSeleccionadas := FImporteDeInfraccionesSeleccionadas + spObtenerInfraccionesPorMovimiento.FieldByName('Importe').AsVariant;
			spObtenerInfraccionesPorMovimiento.Next;
		end;
		spObtenerInfraccionesPorMovimiento.EnableControls;
		spObtenerInfraccionesPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeInfraccionesSeleccionadas) div 100 );
		dblInfracciones.Invalidate;
		dblInfracciones.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

end.
