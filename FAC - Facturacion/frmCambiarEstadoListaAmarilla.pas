{------------------------------------------------------------------------
        frmCambiarEstadoListaAmarilla

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description :       (Ref SS 660)
                    Permite cambiar el estado de un convenio en lista amarilla
                    Si pasa a estado Alta de lista amarilla, pone en lista amarilla
                    todos sus tags

    Author      :   CQuezadaI
    Date        :   19-Febrero-2013
    Firma       :   SS_660_CQU_20121010
    Description :   Se Modifica el manejo de estados.

    Author      :   MDiazV
    Date        :   27-Febrero-2013
    Firma       :   SS_660_MDI_20130227
    Description :   Se verifica que convenio sea eliminado de legales si es pasado a lista amarilla
                    Se modifica problema con el texto de nuevo estado, para el cambio masivo.

    Author      :   MDiazV
    Date        :   24-Abril-2013
    Firma       :   SS_660_MDI_20130227
    Description :   Se suprimi� el cambio masivo de estado, se dej� funci�n �nica para el cambio de estado, sea masivo o unitario.
                    Se unific� la funci�n Inicializa, tomando como par�metro siempre el listado de convenios enviado del formulario anterior.

    Author      :   CquezadaI
    Date        :   14-Junio-2013
    Firma       :   SS_660_CQU_20130604
    Descripction:   Se realizan correcciones menores a algunas validaciones y mensajes

    Author      :   CquezadaI
    Date        :   28-Junio-2013
    Firma       :   SS_660_CQU_20130628
    Descripction:   Se agrega el reloj cuando tiene que esperar el paso a Lista Amarilla

    Author      :   CquezadaI
    Date        :   19-Julio-2013
    Firma       :   SS_660_CQU_20130711
    Descripction:   Se agrega Label con la concesionaria
                    Se agrega SP para verificar con la regla de validaci�n si es que se puede pasar a LA
					Se agrega funci�n que valida el estado antes de Inhabilitar
					Se Modifica el t�tulo de la ventana
                    Se cambia de lugar la validacion de fecha envio carta

    Author      :   CquezadaI
    Date        :   21-Octubre-2013
    Firma       :   SS_660_CQU_20130822
    Descripction:   Se modifica el despliegue de un mensaje cuando existe error.
                    antes aarec�a el de CarpetasLegales aunque se cayera el proceso.

    Author      :   CquezadaI
    Date        :   22-Noviembre-2013
    Firma       :   SS_660_CQU_20131122
    Descripction:   Se cambia de lugar una validaci�n y se captura la fecha de envio
					de la carta ya que faltaba en el proceso.
-------------------------------------------------------------------------}
unit frmCambiarEstadoListaAmarilla;

interface

uses
    constParametrosGenerales,
    frmVentanaConveniosEnLegales,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, ListBoxEx, DBListEx, ExtCtrls, DmConnection,
  DB, ADODB, VariantComboBox, UtilProc, Util, PeaProcs,
  DmiCtrls, SysUtilsCN, PeaTypes, DBClient, MidasLib, Grids, DBGrids;

type
  TCambiarEstadoListaAmarillaForm = class(TForm)
    btnCambiar: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    deFechaCarta: TDateEdit;
    lblFechaCarta: TLabel;
    Label2: TLabel;
    vcbNuevoEstado: TVariantComboBox;
    spCambiarEstadoConvenioListaAmarilla: TADOStoredProc;
    Label3: TLabel;
    Label4: TLabel;
    lblEstadoActual: TLabel;
    lblConvenio: TLabel;
    lblEmpresaCorreo: TLabel;                       // SS_660_CQU_20121010
    cboEmpresaCorreo: TVariantComboBox;             // SS_660_MDI_20130227
    dsConveniosEnCarpeta: TDataSource;              // SS_660_MDI_20130227
    cdsConveniosEnCarpeta: TClientDataSet;          // SS_660_MDI_20130227
    cdsIdConvenioCarpetaLegal: TIntegerField;       // SS_660_MDI_20130227
    cdsCodigoConvenio: TIntegerField;               // SS_660_MDI_20130227
    cdsApellido: TStringField;                      // SS_660_MDI_20130227
    cdsApellidoMaterno: TStringField;               // SS_660_MDI_20130227
    cdsNombre: TStringField;                        // SS_660_MDI_20130227
    cdsFechaHoraImpresion: TDateTimeField;          // SS_660_MDI_20130227
    cdsUsuarioImpresion: TStringField;              // SS_660_MDI_20130227
    cdsFechaHoraEnvioALegales: TDateTimeField;      // SS_660_MDI_20130227
    cdsUsuarioEnvioLegales: TStringField;           // SS_660_MDI_20130227
    cdsFechaHoraSalidaLegales: TDateTimeField;      // SS_660_MDI_20130227
    cdsFechaHoraCreacion: TDateTimeField;           // SS_660_MDI_20130227
    cdsUsuarioCreacion: TStringField;               // SS_660_MDI_20130227
    cdsFechaHoraActualizacion: TDateTimeField;      // SS_660_MDI_20130227
    cdsUsuarioActualizacion: TStringField;          // SS_660_MDI_20130227
    cdsNumeroDocumento: TStringField;               // SS_660_MDI_20130227
    cdsNumeroConvenio: TStringField;                // SS_660_MDI_20130227
    cdsSeleccionado: TBooleanField;                 // SS_660_MDI_20130227
    cdsApellidoMostrar: TStringField;               // SS_660_MDI_20130227
    cdsDias: TIntegerField;                         // SS_660_MDI_20130227
    cdsDeudaStr: TCurrencyField;                    // SS_660_MDI_20130227
    cdsUsuarioSalidaLegales: TStringField;          // SS_660_MDI_20130227
    cdsDiasDeudaTotal: TIntegerField;               // SS_660_MDI_20130227
    cdsImporteDeudaActualizada: TCurrencyField;     // SS_660_MDI_20130227
    cdsAliasEmpresaRecaudadora: TStringField;       // SS_660_MDI_20130227
    cdsDescripcionTipoDeuda: TStringField;          // SS_660_MDI_20130227
    cdsCodigoEmpresasRecaudadoras: TIntegerField;   // SS_660_MDI_20130227
    cdsCodigoTipoDeuda: TIntegerField;              // SS_660_MDI_20130227
    cdsDescripcionConcesionaria: TStringField;
    spConveniosEnCarpetaLegal: TADOStoredProc;      // SS_660_MDI_20130227
    spActualizarConvenio: TADOStoredProc;           // SS_660_MDI_20130227
    cdsExcluidos: TClientDataSet;                   // SS_660_MDI_20130227
    cdsExcluidosNumeroConvenio: TStringField;       // SS_660_MDI_20130227
    cdsSacadosLegales : TClientDataSet;             // SS_660_MDI_20130227
    cdsSacadosLegalesNumeroConvenio: TStringField;  // SS_660_MDI_20130227
    lblConcesionaria: TLabel;						// SS_660_CQU_20130711
    lblConcesionariaDato: TLabel;					// SS_660_CQU_20130711
    spReglaValidacionListaAmarilla: TADOStoredProc;	// SS_660_CQU_20130711
    procedure CargarEmpresas;                       // SS_660_CQU_20121010
    procedure vcbNuevoEstadoChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnCambiarClick(Sender: TObject);	                        // SS_1036_PDO_20120418
    function CambiaEstado( IDConvenioInhabilitado : Integer;            // SS_660_MDI_20130227
                                  CodigoConvenio : Integer;             // SS_660_MDI_20130227
                                  Estado : Integer;                     // SS_660_MDI_20130227
                                  NumeroConvenio : string;              // SS_660_MDI_20130227
                                  Descripcion : string;                 // SS_660_MDI_20130227
                                  FechaEnvioCarta : TDateTime;          // SS_660_MDI_20130227
                                  EstaEnLegales : Boolean;              // SS_660_MDI_20130227
                                  NumeroDocumento : string): string;    // SS_660_MDI_20130227



  private
    { Private declarations }
    FIDConvenio,
    FDiasEnvioCarta,                    //d�as que deben transcurrir antes del envio de carta
    FCodigoConvenio,                    // SS_660_MDI_20130227
    FEstadoActual : Integer;            // SS_660_MDI_20130227
    FNumeroConvenio : string;           // SS_660_MDI_20130227
    FDescripcion : string;              // SS_660_MDI_20130227
    FFechaEnvioCarta : TDateTime;       // SS_660_MDI_20130227
    FEstaEnLegales : Boolean;           // SS_660_CQU_20121010
    FNumeroDocumento: string;           // SS_660_MDI_20130227
    FAutorizaFiscalia: Boolean;         // SS_660_MDI_20130227
    FAutorizaGerencia: Boolean;         // SS_660_MDI_20130227
    Resultado : boolean;                // SS_660_MDI_20130227
    ResultadoCambio   : string;         // SS_660_MDI_20130227
    FListadoConvenio : TClientDataSet;  // SS_660_MDI_20130227
    FTipoCambioEstado : string;         // SS_660_MDI_20130227
    FControl : Boolean;                 // SS_660_MDI_20130227
    FMensaje : string;                  // SS_660_MDI_20130227
    FMensajeLegales : string;           // SS_660_MDI_20130227
    FCargando : Boolean;                				// SS_660_CQU_20130711
    function PuedeInhabilitar : Boolean;                // SS_660_CQU_20130711
    function TieneDistintasConcesionarias : Boolean;    // SS_660_CQU_20130711

  public
    { Public declarations }
    //function Inicializar(   IDConvenio, CodigoConvenio, Estado : integer;     // SS_660_CQU_20121010
    //                        NumeroConvenio, GlosaEstadoActual : string;       // SS_660_CQU_20121010
    //                        FEchaEnvioCarta : TDateTime) : boolean;           // SS_660_CQU_20121010
    function Inicializar(   ListadoConvenios  : TClientDataSet;                 // SS_660_MDI_20130227
                            EstadoActual      : Integer;                        // SS_660_MDI_20130227
                            GlosaActual       : string) : Boolean;              // SS_660_MDI_20130227
    function Validaciones : boolean;
    procedure LlenaComboConEstadoPosibles;
    procedure FiltrarConveniosEnCarpeta;                                        // SS_660_MDI_20130227
    procedure FiltrarConveniosEnCarpetaMasivo;                                  // SS_660_MDI_20130227
    procedure ActualizarConvenios();                                            // SS_660_MDI_20130227
    function SacarConvenioCarpetasLegales(CodigoConvenio : Integer) : string;   // SS_660_MDI_20130227
  end;

var
  CambiarEstadoListaAmarillaForm: TCambiarEstadoListaAmarillaForm;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    MDI
  Date Created: 04/03/2013
  Description: Inicializa el formulario y sus objetos para el cambio masivo de convenio
  Parameters: ListadoConvenios : TClientDataSet; EstadoActual : Integer; GlosaActual : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function TCambiarEstadoListaAmarillaForm.Inicializar( ListadoConvenios : TClientDataSet;   // SS_660_MDI_20130227
                            EstadoActual : Integer;                                        // SS_660_MDI_20130227
                            GlosaActual : string) : boolean;                               // SS_660_MDI_20130227
resourcestring
    MSG_NOMBRE_PARAMETRO = 'CANT_DIAS_CARTA_LISTA_AMARILLA';
    //MSG_CAPTION = 'Cambiar estado Convenio en Lista Amarilla';                           // SS_660_CQU_20130711
    MSG_CAPTION = 'Cambiar Estado Convenio en Lista Amarilla';                             // SS_660_CQU_20130711
    MSG_PARAMETRO = 'No se pudo obtener el par�metro general "%s"';
var                                                                                        // SS_660_CQU_20130711
    CantConcesionarias : Integer;                                                          // SS_660_CQU_20130711
begin
    FCargando := True;                                                                    // SS_660_CQU_20130711
    CenterForm(Self);                                                                     
    Caption := MSG_CAPTION;
    FListadoConvenio := TClientDataSet.Create(Self);                                      // SS_660_MDI_20130227
    FListadoConvenio := ListadoConvenios;                                                 // SS_660_MDI_20130227

    if FListadoConvenio.RecordCount = 1 then begin
        Label3.Caption := 'Convenio:';                                                    // SS_660_MDI_20130227
        lblConvenio.Caption := FListadoConvenio.FieldByName('NumeroConvenio').AsString;   // SS_660_MDI_20130227
        lblEstadoActual.Caption :=  FListadoConvenio.FieldByName('Descripcion').AsString; // SS_660_MDI_20130227
        FTipoCambioEstado := 'Unitario';                                                  // SS_660_CQU_20130604
        FEstaEnLegales := FListadoConvenio.FieldByName('EstaEnLegales').AsBoolean;        // SS_660_CQU_20130604
        lblConcesionariaDato.Caption := FListadoConvenio.FieldByName('Concesionaria').AsString; // SS_660_CQU_20130711
    end                                                                                   // SS_660_MDI_20130227
    else begin                                                                            // SS_660_MDI_20130227
        Label3.Caption := 'Convenios:';                                                   // SS_660_MDI_20130227
        lblConvenio.Caption := IntToStr(ListadoConvenios.RecordCount);                   // SS_660_MDI_20130227
        lblEstadoActual.Caption := GlosaActual;                                          // SS_660_MDI_20130227
        FTipoCambioEstado := 'Masivo';                                                    // SS_660_CQU_20130604
        // Si es masivo y tiene varias concesionarias, no mostrar la concesionaria                      // SS_660_CQU_20130711
        if TieneDistintasConcesionarias then lblConcesionariaDato.Caption := 'Varias Concesionarias'    // SS_660_CQU_20130711
        else lblConcesionariaDato.Caption := FListadoConvenio.FieldByName('Concesionaria').AsString;    // SS_660_CQU_20130711
    end;                                                                                 // SS_660_MDI_20130227

    FEstadoActual := EstadoActual;                                                       // SS_660_MDI_20130227

    if FEstadoActual = -1 then begin                                                     // SS_660_MDI_20130227
        FListadoConvenio.First;                                                          // SS_660_MDI_20130227
        while (not FListadoConvenio.Eof) and (FEstadoActual = -1) do begin               // SS_660_MDI_20130227
           FEstadoActual := FListadoConvenio.FieldByName('Estado').AsInteger;            // SS_660_MDI_20130227
           lblEstadoActual.Caption :=  FListadoConvenio.FieldByName('Descripcion').AsString; // SS_660_MDI_20130227
           FListadoConvenio.Next;                                                        // SS_660_MDI_20130227
        end;                                                                             // SS_660_MDI_20130227

    end;                                                                                 // SS_660_MDI_20130227
    LlenaComboConEstadoPosibles();
    if vcbNuevoEstado.Text = '' then begin                                               // SS_660_MDI_20130227
        btnCambiar.Enabled := False;                                                     // SS_660_MDI_20130227
    end;

    CargarEmpresas();
    FiltrarConveniosEnCarpetaMasivo();                                                   // SS_660_MDI_20130227
    //FTipoCambioEstado := 'Masivo';                                                     // SS_660_CQU_20130604
    FCargando := False;                                                                  // SS_660_CQU_20130711
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, MSG_NOMBRE_PARAMETRO, FDiasEnvioCarta) then begin
        MsgBox(Format(MSG_PARAMETRO, [MSG_NOMBRE_PARAMETRO]), Caption, MB_ICONERROR);
        Result := False;
    end
    else Result := True;
end;


{------------------------------------------------------------------------
        Validaciones

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Realiza las validaciones necesaris antes de cambiar de estado.

-------------------------------------------------------------------------}
function TCambiarEstadoListaAmarillaForm.Validaciones;
resourcestring
    MSG_FECHA_CARTA           = 'Por favor ingrese la fecha de env�o de la carta de notificaci�n';
    MSG_DIAS_CARTA_NO         = 'Deben transcurrir m�nimo %d d�as desde el env�o de la carta de notificaci�n';
    MSG_DIAS_CARTA_NO_MASIVO  = 'Deben transcurrir m�nimo %d d�as desde el env�o de la carta de notificaci�n para todos los convenios a procesar';
    MSG_FALTA_EMPRESA         = 'Debe seleccionar una empresa de correos';                                      // SS_660_CQU_20121010
    MSG_CONVENIO_LEGALES      = 'El convenio se encuentra en carpetas legales, al pasarlo a Lista Amarilla se dar� de baja de carpetas legales.' + CRLF + '�Est� seguro de continuar?'; // SS_660_MDV_20130227
    MSG_AUTORIZACIONES        = 'El convenio debe contar con las Autorizaciones por Fiscal�a y Gerencia para el cambio de estado';   // SS_660_MDI_20130227
    MSG_AUTORIZACIONES_MASIVO = 'Todos los convenios a procesar deben estar Autorizados por Fiscal�a y Gerencia';                    // SS_660_MDI_20130227
    MSG_NO_INHABILITAR        = 'Al menos uno de los convenios que ha seleccionado no cumple'                                        // SS_660_CQU_20130711
                                + CRLF +'con la regla m�nima para pasar al nuevo estado %s';                                         // SS_660_CQU_20130711
var
    FechaDeHoy : TDateTime;
    Control : TControl;                                                                                                              // SS_660_CQU_20130711
begin
    Result := True;
    FechaDeHoy := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);                                              // SS_660_CQU_20121010
    //exigir la fecha de la carta
    if vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA then begin                 // SS_660_CQU_20121010

        if cboEmpresaCorreo.Value <= 0 then begin
            MsgBoxBalloon(MSG_FALTA_EMPRESA, Caption, MB_ICONEXCLAMATION, deFechaCarta);
            Result := False;
            Exit;                                                                                           // SS_660_MDI_20130227
        end;

        if deFechaCarta.Date = NullDate then begin
            MsgBoxBalloon(MSG_FECHA_CARTA, Caption, MB_ICONEXCLAMATION, deFechaCarta);
            Result := False;
            Exit;                                                                                           // SS_660_MDI_20130227
        end;

    end;

    // Se debe validar que a�n cumpla con la Regla de Validaci�n
    if ((vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_INHABILITADO) or               // SS_660_CQU_20130711
        (vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA))				// SS_660_CQU_20130711
    then begin																						// SS_660_CQU_20130711
        if not PuedeInhabilitar then begin                                                          // SS_660_CQU_20130711
            btnCambiar.Enabled := False;                                                            // SS_660_CQU_20130711
            //Result := False;                                                                      // SS_660_CQU_20131122  // SS_660_CQU_20130711
            if FCargando then Control := btnCambiar                                                 // SS_660_CQU_20130711
            else Control := vcbNuevoEstado;                                                         // SS_660_CQU_20130711
            MsgBoxBalloon(                                                                          // SS_660_CQU_20130711
                Format(MSG_NO_INHABILITAR, [vcbNuevoEstado.Text]),                                  // SS_660_CQU_20130711
                Caption, MB_ICONINFORMATION, Control);                                              // SS_660_CQU_20130711
            Result := False;                                                                        // SS_660_CQU_20131122
            Exit;                                                                                   // SS_660_CQU_20130711
        end else btnCambiar.Enabled := True;														// SS_660_CQU_20130711
    end;                                                                                            // SS_660_CQU_20130711

    //Debe validarse que al dar de alta, se haya enviado la carta
    if FTipoCambioEstado = 'Unitario'  then begin                                                                  // SS_660_MDI_20130227

        if vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA then begin

            FAutorizaFiscalia := FListadoConvenio.FieldByName('AutorizaFiscalia').AsBoolean;        // SS_660_CQU_20130604
            FAutorizaGerencia := FListadoConvenio.FieldByName('AutorizaGerencia').AsBoolean;        // SS_660_CQU_20130604
            if (not FAutorizaFiscalia) or (not FAutorizaGerencia) then begin
                MsgBoxBalloon(MSG_AUTORIZACIONES, Caption, MB_ICONEXCLAMATION, vcbNuevoEstado);
                Result := False;
                Exit;
            end;

        end;

        if vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_INHABILITADO then begin                      // SS_660_CQU_20121010

            if FFechaEnvioCarta = 0 then FFechaEnvioCarta := FListadoConvenio.FieldByName('FechaEnvioCarta').AsDateTime;    // SS_660_CQU_20131122
            if (FechaDeHoy - FFechaEnvioCarta) < FDiasEnvioCarta then begin													// SS_660_CQU_20131122
                MsgBoxBalloon(Format(MSG_DIAS_CARTA_NO, [FDiasEnvioCarta]), Caption, MB_ICONEXCLAMATION, vcbNuevoEstado);	// SS_660_CQU_20131122
                Result := False;																							// SS_660_CQU_20131122
                Exit;																										// SS_660_CQU_20131122
            end;																											// SS_660_CQU_20131122

            if (FEstaEnLegales) then begin                                                                          // SS_660_MDI_20130227
                  Resultado := (MsgBox(MSG_CONVENIO_LEGALES, 'Atenci�n', MB_YESNO + MB_ICONQUESTION) = ID_YES);     // SS_660_MDI_20130227
                  Result := Resultado;                                                                              // SS_660_MDI_20130227
                  //Exit;                                                                                           // SS_660_CQU_20130711  // SS_660_MDI_20130227
                  if not Result then Exit;                                                                          // SS_660_CQU_20130711
                  
            end;

            //if (FechaDeHoy - FFechaEnvioCarta) < FDiasEnvioCarta then begin                                         		// SS_660_CQU_20131122	// SS_660_MDI_20130227
            //    MsgBoxBalloon(Format(MSG_DIAS_CARTA_NO, [FDiasEnvioCarta]), Caption, MB_ICONEXCLAMATION, vcbNuevoEstado);	// SS_660_CQU_20131122
            //    Result := False;																							// SS_660_CQU_20131122
            //    Exit;                                        																// SS_660_CQU_20131122	// SS_660_MDI_20130227
            //end;																											// SS_660_CQU_20131122
        end;

    end
    else begin                                                                                                      // SS_660_MDI_20130227

    // Verifico si alguno de los convenios que tiene estado carta enviada no cumple cantidad de dias envio carta
        if vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_INHABILITADO then begin
            FListadoConvenio.First;
            while not FListadoConvenio.Eof do begin
                FFechaEnvioCarta := FListadoConvenio.FieldByName('FechaEnvioCarta').AsDateTime;
                if (FechaDeHoy - FFechaEnvioCarta) < FDiasEnvioCarta then begin
                    MsgBoxBalloon(Format(MSG_DIAS_CARTA_NO_MASIVO, [FDiasEnvioCarta]), Caption, MB_ICONEXCLAMATION, vcbNuevoEstado);
                    Result := False;
                    Exit;                                                                                           // SS_660_MDI_20130227
                end;
                FListadoConvenio.Next;
            end;
        end;

        if vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA then begin                     // SS_660_MDI_20130227
            FListadoConvenio.First;                                                                                 // SS_660_MDI_20130227
            while not FListadoConvenio.Eof do begin                                                                 // SS_660_MDI_20130227
                if (not FListadoConvenio.FieldByName('AutorizaFiscalia').AsBoolean) or                              // SS_660_MDI_20130227
                    (not FListadoConvenio.FieldByName('AutorizaGerencia').AsBoolean) then begin                     // SS_660_MDI_20130227
                    MsgBoxBalloon(MSG_AUTORIZACIONES_MASIVO, Caption, MB_ICONEXCLAMATION, vcbNuevoEstado);          // SS_660_MDI_20130227
                    Result := False;                                                                                // SS_660_MDI_20130227
                    Exit;                                                                                           // SS_660_MDI_20130227
                end;                                                                                                // SS_660_MDI_20130227
                FListadoConvenio.Next;                                                                              // SS_660_MDI_20130227
            end;                                                                                                    // SS_660_MDI_20130227
        end;                                                                                                        // SS_660_MDI_20130227

    end;
end;


{-----------------------------------------------------------------------------
  Function Name: FiltrarConveniosEnCarpeta
  Author      : MDiazV
  Date        : 27-Febrero-2013
  Description : Realiza las validaciones necesaris antes de cambiar de estado.
-----------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.FiltrarConveniosEnCarpeta;
resourcestring
    ERROR_OBT_CONVENIOS         = 'Error obteniendo convenio';
var
    NombreAMostrar : string;
begin
    Screen.Cursor := crHourGlass;
    try
        if FEstaEnLegales then begin
            try
                cdsConveniosEnCarpeta.EmptydataSet;
                cdsConveniosEnCarpeta.DisableControls;
                spConveniosEnCarpetaLegal.Close;
                spConveniosEnCarpetaLegal.CommandTimeout := 1000;
                spConveniosEnCarpetaLegal.Parameters.ParamByName('@CodigoConvenio').Value               := FCodigoConvenio;   //SS_660_MDI_20130227
                spConveniosEnCarpetaLegal.Open;
                while not spConveniosEnCarpetaLegal.eof do begin
                    cdsConveniosEnCarpeta.Append;
                    cdsConveniosEnCarpeta.FieldByName('IdConvenioCarpetaLegal').AsInteger  := spConveniosEnCarpetaLegal.FieldByName('IdConvenioCarpetaLegal').AsInteger ;
                    cdsConveniosEnCarpeta.FieldByName('CodigoConvenio'        ).AsInteger  := spConveniosEnCarpetaLegal.FieldByName('CodigoConvenio'        ).AsInteger ;

                    cdsConveniosEnCarpeta.FieldByName('Apellido').AsString         := spConveniosEnCarpetaLegal.FieldByName('Apellido').AsString;
                    cdsConveniosEnCarpeta.FieldByName('ApellidoMaterno').AsString  := spConveniosEnCarpetaLegal.FieldByName('ApellidoMaterno').AsString;
                    cdsConveniosEnCarpeta.FieldByName('Nombre').AsString           := spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString;

                    NombreAMostrar := '';
                    NombreAMostrar :=   trim(spConveniosEnCarpetaLegal.FieldByName('Apellido').AsString) + ' ' + trim(spConveniosEnCarpetaLegal.FieldByName('ApellidoMaterno').AsString);

                    if trim(spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString) <> '' then NombreAMostrar := NombreAMostrar + ', ' + trim(spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString);

                    cdsConveniosEnCarpeta.FieldByName('ApellidoMostrar').AsString := NombreAMostrar;

                    if spConveniosEnCarpetaLegal.FieldByName('FechaHoraImpresion'    ).AsDateTime > 0 then
                        cdsConveniosEnCarpeta.FieldByName('FechaHoraImpresion'    ).AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraImpresion'    ).AsDateTime ;

                    cdsConveniosEnCarpeta.FieldByName('UsuarioImpresion'      ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioImpresion'      ).AsString ;
                    if spConveniosEnCarpetaLegal.FieldByName('FechaHoraEnvioLegales' ).AsDateTime > 0 then
                        cdsConveniosEnCarpeta.FieldByName('FechaHoraEnvioALegales').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraEnvioLegales' ).AsDateTime ;

                    cdsConveniosEnCarpeta.FieldByName('UsuarioEnvioLegales'   ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioEnvioLegales'   ).AsString ;

                    if spConveniosEnCarpetaLegal.FieldByName('FechaHoraSalidaLegales').AsDateTime > 0 then
                        cdsConveniosEnCarpeta.FieldByName('FechaHoraSalidaLegales').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraSalidaLegales').AsDateTime ;


                    cdsConveniosEnCarpeta.FieldByName('UsuarioSalidaLegales'  ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioSalidaLegales'  ).AsString ;


                    if spConveniosEnCarpetaLegal.FieldByName('FechaHoraCreacion'     ).AsDateTime > 0 then
                        cdsConveniosEnCarpeta.FieldByName('FechaHoraCreacion'     ).AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraCreacion'     ).AsDateTime ;

                    cdsConveniosEnCarpeta.FieldByName('UsuarioCreacion'       ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioCreacion'       ).AsString ;

                    if spConveniosEnCarpetaLegal.FieldByName('FechaHoraActualizacion').AsDateTime > 0 then
                        cdsConveniosEnCarpeta.FieldByName('FechaHoraActualizacion').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraActualizacion').AsDateTime ;

                    cdsConveniosEnCarpeta.FieldByName('UsuarioActualizacion'  ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioActualizacion'  ).AsString ;
                    cdsConveniosEnCarpeta.FieldByName('NumeroDocumento'       ).AsString   := spConveniosEnCarpetaLegal.FieldByName('NumeroDocumento'       ).AsString ;
                    cdsConveniosEnCarpeta.FieldByName('NumeroConvenio'        ).AsString   := spConveniosEnCarpetaLegal.FieldByName('NumeroConvenio'        ).AsString ;
                    cdsConveniosEnCarpeta.FieldByName('Seleccionado'          ).AsBoolean  := False;

                    cdsConveniosEnCarpeta.FieldByName('Dias'                  ).AsInteger  := spConveniosEnCarpetaLegal.FieldByName('DiasDeuda'             ).AsInteger ;
                    cdsConveniosEnCarpeta.FieldByName('DeudaStr'              ).AsCurrency   := spConveniosEnCarpetaLegal.FieldByName('ImporteDeuda' ).AsFloat;
                    cdsConveniosEnCarpeta.FieldByName('DiasDeudaTotal').AsInteger  := spConveniosEnCarpetaLegal.FieldByName('DiasDeudaTotal').AsInteger ;
                    cdsConveniosEnCarpeta.FieldByName('ImporteDeudaActualizada').AsCurrency   := spConveniosEnCarpetaLegal.FieldByName('ImporteDeudaActualizada').AsFloat;
                    cdsConveniosEnCarpeta.FieldByName('AliasEmpresaRecaudadora').AsString   := spConveniosEnCarpetaLegal.FieldByName('AliasEmpresaRecaudadora').AsString ;
                    cdsConveniosEnCarpeta.FieldByName('DescripcionTipoDeuda').AsString   := spConveniosEnCarpetaLegal.FieldByName('DescripcionTipoDeuda').AsString ;

                    cdsConveniosEnCarpeta.FieldByName('CodigoEmpresasRecaudadoras').AsInteger   := spConveniosEnCarpetaLegal.FieldByName('CodigoEmpresasRecaudadoras').AsInteger ;
                    cdsConveniosEnCarpeta.FieldByName('CodigoTipoDeuda').AsInteger   := spConveniosEnCarpetaLegal.FieldByName('CodigoTipoDeuda').AsInteger ;

                    cdsConveniosEnCarpeta.FieldByName('DescripcionConcesionaria').AsString := spConveniosEnCarpetaLegal.FieldByName('DescripcionConcesionaria').AsString;   // SS_1036_PDO_20120418

                    spConveniosEnCarpetaLegal.Next;
                end;
                cdsConveniosEnCarpeta.First;
                cdsConveniosEnCarpeta.EnableControls;
            except
                on E: Exception do begin
                          screen.Cursor := crDefault;
                      MsgBoxErr(ERROR_OBT_CONVENIOS, e.message, caption, MB_ICONSTOP);
                end;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: FiltrarConveniosEnCarpetaMasivo
  Author:  MDI
  Date Created:  04/ 03 / 2013
  Description: Filtra cada convenio del listado de convenios masivos, y obtiene los convenios que est�n en carpeta legal
  Parameters: N/A
  Return Value: N/A

-----------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.FiltrarConveniosEnCarpetaMasivo;
resourcestring
    ERROR_OBT_CONVENIOS         = 'Error obteniendo convenio';
var
    NombreAMostrar : string;
begin
    Screen.Cursor := crHourGlass;
    try
        FListadoConvenio.Open;
        FListadoConvenio.First;
        cdsConveniosEnCarpeta.EmptydataSet;
        cdsConveniosEnCarpeta.DisableControls;
        while not FListadoConvenio.Eof do begin
            if FListadoConvenio.FieldByName('EstaEnLegales').AsBoolean then begin
                try
                    spConveniosEnCarpetaLegal.Close;
                    spConveniosEnCarpetaLegal.CommandTimeout := 1000;
                    spConveniosEnCarpetaLegal.Parameters.ParamByName('@CodigoConvenio').Value               := FListadoConvenio.FieldByName('CodigoConvenio').AsInteger;   //SS_660_MDI_20130227
                    spConveniosEnCarpetaLegal.Open;
                    if not spConveniosEnCarpetaLegal.eof then begin

                        cdsConveniosEnCarpeta.Append;
                        cdsConveniosEnCarpeta.FieldByName('IdConvenioCarpetaLegal').AsInteger  := spConveniosEnCarpetaLegal.FieldByName('IdConvenioCarpetaLegal').AsInteger ;
                        cdsConveniosEnCarpeta.FieldByName('CodigoConvenio'        ).AsInteger  := spConveniosEnCarpetaLegal.FieldByName('CodigoConvenio'        ).AsInteger ;

                        cdsConveniosEnCarpeta.FieldByName('Apellido').AsString         := spConveniosEnCarpetaLegal.FieldByName('Apellido').AsString;
                        cdsConveniosEnCarpeta.FieldByName('ApellidoMaterno').AsString  := spConveniosEnCarpetaLegal.FieldByName('ApellidoMaterno').AsString;
                        cdsConveniosEnCarpeta.FieldByName('Nombre').AsString           := spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString;

                        NombreAMostrar := '';
                        NombreAMostrar :=   trim(spConveniosEnCarpetaLegal.FieldByName('Apellido').AsString) + ' ' + trim(spConveniosEnCarpetaLegal.FieldByName('ApellidoMaterno').AsString);

                        if trim(spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString) <> '' then NombreAMostrar := NombreAMostrar + ', ' + trim(spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString);

                        cdsConveniosEnCarpeta.FieldByName('ApellidoMostrar').AsString := NombreAMostrar;

                        if spConveniosEnCarpetaLegal.FieldByName('FechaHoraImpresion'    ).AsDateTime > 0 then
                            cdsConveniosEnCarpeta.FieldByName('FechaHoraImpresion'    ).AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraImpresion'    ).AsDateTime ;

                        cdsConveniosEnCarpeta.FieldByName('UsuarioImpresion'      ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioImpresion'      ).AsString ;
                        if spConveniosEnCarpetaLegal.FieldByName('FechaHoraEnvioLegales' ).AsDateTime > 0 then
                            cdsConveniosEnCarpeta.FieldByName('FechaHoraEnvioALegales').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraEnvioLegales' ).AsDateTime ;

                        cdsConveniosEnCarpeta.FieldByName('UsuarioEnvioLegales'   ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioEnvioLegales'   ).AsString ;

                        if spConveniosEnCarpetaLegal.FieldByName('FechaHoraSalidaLegales').AsDateTime > 0 then
                            cdsConveniosEnCarpeta.FieldByName('FechaHoraSalidaLegales').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraSalidaLegales').AsDateTime ;


                        cdsConveniosEnCarpeta.FieldByName('UsuarioSalidaLegales'  ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioSalidaLegales'  ).AsString ;


                        if spConveniosEnCarpetaLegal.FieldByName('FechaHoraCreacion'     ).AsDateTime > 0 then
                            cdsConveniosEnCarpeta.FieldByName('FechaHoraCreacion'     ).AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraCreacion'     ).AsDateTime ;

                        cdsConveniosEnCarpeta.FieldByName('UsuarioCreacion'       ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioCreacion'       ).AsString ;

                        if spConveniosEnCarpetaLegal.FieldByName('FechaHoraActualizacion').AsDateTime > 0 then
                            cdsConveniosEnCarpeta.FieldByName('FechaHoraActualizacion').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraActualizacion').AsDateTime ;

                        cdsConveniosEnCarpeta.FieldByName('UsuarioActualizacion'  ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioActualizacion'  ).AsString ;
                        cdsConveniosEnCarpeta.FieldByName('NumeroDocumento'       ).AsString   := spConveniosEnCarpetaLegal.FieldByName('NumeroDocumento'       ).AsString ;
                        cdsConveniosEnCarpeta.FieldByName('NumeroConvenio'        ).AsString   := spConveniosEnCarpetaLegal.FieldByName('NumeroConvenio'        ).AsString ;
                        cdsConveniosEnCarpeta.FieldByName('Seleccionado'          ).AsBoolean  := False;

                        cdsConveniosEnCarpeta.FieldByName('Dias'                  ).AsInteger  := spConveniosEnCarpetaLegal.FieldByName('DiasDeuda'             ).AsInteger ;
                        cdsConveniosEnCarpeta.FieldByName('DeudaStr'              ).AsCurrency   := spConveniosEnCarpetaLegal.FieldByName('ImporteDeuda' ).AsFloat;
                        cdsConveniosEnCarpeta.FieldByName('DiasDeudaTotal').AsInteger  := spConveniosEnCarpetaLegal.FieldByName('DiasDeudaTotal').AsInteger ;
                        cdsConveniosEnCarpeta.FieldByName('ImporteDeudaActualizada').AsCurrency   := spConveniosEnCarpetaLegal.FieldByName('ImporteDeudaActualizada').AsFloat;
                        cdsConveniosEnCarpeta.FieldByName('AliasEmpresaRecaudadora').AsString   := spConveniosEnCarpetaLegal.FieldByName('AliasEmpresaRecaudadora').AsString ;
                        cdsConveniosEnCarpeta.FieldByName('DescripcionTipoDeuda').AsString   := spConveniosEnCarpetaLegal.FieldByName('DescripcionTipoDeuda').AsString ;

                        cdsConveniosEnCarpeta.FieldByName('CodigoEmpresasRecaudadoras').AsInteger   := spConveniosEnCarpetaLegal.FieldByName('CodigoEmpresasRecaudadoras').AsInteger ;
                        cdsConveniosEnCarpeta.FieldByName('CodigoTipoDeuda').AsInteger   := spConveniosEnCarpetaLegal.FieldByName('CodigoTipoDeuda').AsInteger ;

                        cdsConveniosEnCarpeta.FieldByName('DescripcionConcesionaria').AsString := spConveniosEnCarpetaLegal.FieldByName('DescripcionConcesionaria').AsString;   // SS_1036_PDO_20120418
                        cdsConveniosEnCarpeta.Post;
                        spConveniosEnCarpetaLegal.Next;
                        cdsConveniosEnCarpeta.First;
                        cdsConveniosEnCarpeta.EnableControls;
                    end;
                    except
                        on E: Exception do begin
                                  screen.Cursor := crDefault;
                              MsgBoxErr(ERROR_OBT_CONVENIOS, e.message, caption, MB_ICONSTOP);
                        end;
                    end;
            end;
            FListadoConvenio.Next;
        end;

    finally
        Screen.Cursor := crDefault;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarConvenios
  Author: ???????
  Date Created: ??/??/????
  Description: Actualiza los convenios de acuerdo al tipo de accion y
  actualiza los datos del dataset

    Revision 1
    Author: mpiazza
    Date: 04/02/2009
    Description: SS-146 se agrego dos campos en la actualizacion de la grilla,
    y dos parametros en la actualizacion del sp, empresarecaudadora y tipodeuda
    Revision :2
        Author : vpaszkowicz
        Date : 02/03/2009
        Description : Agrego el codigo de empresa recaudadora y el del tipo
        deuda al client dataset durante el envio, para que este en la tabla
        temporal y cause errores si despu�s se quiere sacar de legales o imprimir
-----------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.ActualizarConvenios();
resourcestring
MSG_ERROR_SQL       = 'Ocurri� un error al intentar sacar el convenio de carpetas legales';
var
    FechaProceso: TDateTime;
begin
    FechaProceso:= NowBase(spActualizarConvenio.Connection);
    cdsConveniosEnCarpeta.first;
    while not cdsConveniosEnCarpeta.eof do begin
            spActualizarConvenio.Parameters.ParamByName('@Accion'                 ).Value  := 1 ; //alta
            spActualizarConvenio.Parameters.ParamByName('@IdConvenioCarpetaLegal' ).Value  := cdsConveniosEnCarpeta.FieldByName('IdConvenioCarpetaLegal').AsInteger;
            spActualizarConvenio.Parameters.ParamByName('@CodigoConvenio'         ).Value  := cdsConveniosEnCarpeta.FieldByName('CodigoConvenio'        ).AsInteger;
            spActualizarConvenio.Parameters.ParamByName('@NumeroDocumento'        ).Value  := cdsConveniosEnCarpeta.FieldByName('NumeroDocumento'       ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@Apellido'               ).Value  := cdsConveniosEnCarpeta.FieldByName('Apellido'              ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@ApellidoMaterno'        ).Value  := cdsConveniosEnCarpeta.FieldByName('ApellidoMaterno'       ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@Nombre'                 ).Value  := cdsConveniosEnCarpeta.FieldByName('Nombre'                ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@UsuarioCreacion'        ).Value  := cdsConveniosEnCarpeta.FieldByName('UsuarioCreacion'       ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@NumeroConvenio'         ).Value  := cdsConveniosEnCarpeta.FieldByName('NumeroConvenio'        ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@DiasDeuda'              ).Value  := 0; //no se actualizan nunca
            spActualizarConvenio.Parameters.ParamByName('@ImporteDeuda'           ).Value  := 0; //no se actualizan nunca

            spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion'     ).Value  := cdsConveniosEnCarpeta.FieldByName('FechaHoraImpresion'    ).AsDateTime;
            if spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion'  ).Value  = 0 then
                spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion' ).Value  := null ;

            spActualizarConvenio.Parameters.ParamByName('@UsuarioImpresion'       ).Value  := cdsConveniosEnCarpeta.FieldByName('UsuarioImpresion'      ).AsString  ;

            spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales'  ).Value := cdsConveniosEnCarpeta.FieldByName('FechaHoraEnvioALegales').AsDateTime ;
            spActualizarConvenio.Parameters.ParamByName('@UsuarioEnvioLegales'    ).Value := cdsConveniosEnCarpeta.FieldByName('UsuarioEnvioLegales'   ).AsString   ;

            if spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales'  ).Value  = 0 then
                spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales' ).Value  := null ;

            cdsConveniosEnCarpeta.Edit;
            cdsConveniosEnCarpeta.FieldByName('FechaHoraSalidaLegales').AsDateTime := FechaProceso ;
            cdsConveniosEnCarpeta.FieldByName('UsuarioSalidaLegales').AsString   := UsuarioSistema ;
            cdsConveniosEnCarpeta.Post;


            spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales' ).Value   := cdsConveniosEnCarpeta.FieldByName('FechaHoraSalidaLegales').AsDateTime ;
            spActualizarConvenio.Parameters.ParamByName('@UsuarioSalidaLegales' ).Value     := cdsConveniosEnCarpeta.FieldByName('UsuarioSalidaLegales').AsString ;


            spActualizarConvenio.Parameters.ParamByName('@UsuarioActualizacion'   ).Value := UsuarioSistema;

            if spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales'  ).Value  = 0 then
                spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales' ).Value  := null ;
            //Si esta en la tabla es por Envio o porque ya estaba grabado
            if cdsConveniosEnCarpeta.FieldByName('CodigoEmpresasRecaudadoras').AsInteger = 0 then
                spActualizarConvenio.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value  := NULL
            else    spActualizarConvenio.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value  := cdsConveniosEnCarpeta.FieldByName('CodigoEmpresasRecaudadoras').AsInteger;
            //Si esta en la tabla es por Envio o porque ya estaba grabado            
            if cdsConveniosEnCarpeta.FieldByName('CodigoTipoDeuda').AsInteger = 0 then
                spActualizarConvenio.Parameters.ParamByName('@CodigoTipoDeuda').Value := NULL
            else spActualizarConvenio.Parameters.ParamByName('@CodigoTipoDeuda').Value := cdsConveniosEnCarpeta.FieldByName('CodigoTipoDeuda').AsInteger;


            try
                DMConnections.BaseCAC.BeginTrans;
                spActualizarConvenio.Execproc;
                DMConnections.BaseCAC.CommitTrans;
                ModalResult := mrOk;
            except on e:exception do begin
                if DMConnections.BaseCAC.InTransaction then begin
                    DMConnections.BaseCAC.RollbackTrans;
                end;

                MsgBoxErr(MSG_ERROR_SQL, e.Message, Caption, MB_ICONERROR);
            end;
        end;
        cdsConveniosEnCarpeta.Next;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SacarConvenioCarpetasLegales
  Author:  MDI
  Date Created:  04/ 03 / 2013
  Description: Quita de carpeta legal el convenio enviado como par�metro
  Parameters: CodigoConvenio : Integer
  Return Value: N/A

-----------------------------------------------------------------------------}
function TCambiarEstadoListaAmarillaForm.SacarConvenioCarpetasLegales(CodigoConvenio : Integer):string;
resourcestring
MSG_ERROR_SQL       = 'Ocurri� un error al intentar sacar el convenio de carpetas legales';
var
    FechaProceso: TDateTime;
begin
    FechaProceso:= NowBase(spActualizarConvenio.Connection);

    //cdsConveniosEnCarpeta.Filter := 'CodigoConvenio = ' + IntToStr(CodigoConvenio);   //SS_660_MDI_20130227
    //cdsConveniosEnCarpeta.Filtered := True;                                           //SS_660_MDI_20130227
    //cdsConveniosEnCarpeta.First;                                                      //SS_660_MDI_20130227

    if not cdsConveniosEnCarpeta.Eof then begin

        spActualizarConvenio.Parameters.ParamByName('@Accion'                 ).Value  := 1 ; //alta
        spActualizarConvenio.Parameters.ParamByName('@IdConvenioCarpetaLegal' ).Value  := cdsConveniosEnCarpeta.FieldByName('IdConvenioCarpetaLegal').AsInteger;
        spActualizarConvenio.Parameters.ParamByName('@CodigoConvenio'         ).Value  := cdsConveniosEnCarpeta.FieldByName('CodigoConvenio'        ).AsInteger;
        spActualizarConvenio.Parameters.ParamByName('@NumeroDocumento'        ).Value  := cdsConveniosEnCarpeta.FieldByName('NumeroDocumento'       ).AsString ;
        spActualizarConvenio.Parameters.ParamByName('@Apellido'               ).Value  := cdsConveniosEnCarpeta.FieldByName('Apellido'              ).AsString ;
        spActualizarConvenio.Parameters.ParamByName('@ApellidoMaterno'        ).Value  := cdsConveniosEnCarpeta.FieldByName('ApellidoMaterno'       ).AsString ;
        spActualizarConvenio.Parameters.ParamByName('@Nombre'                 ).Value  := cdsConveniosEnCarpeta.FieldByName('Nombre'                ).AsString ;
        spActualizarConvenio.Parameters.ParamByName('@UsuarioCreacion'        ).Value  := cdsConveniosEnCarpeta.FieldByName('UsuarioCreacion'       ).AsString ;
        spActualizarConvenio.Parameters.ParamByName('@NumeroConvenio'         ).Value  := cdsConveniosEnCarpeta.FieldByName('NumeroConvenio'        ).AsString ;
        spActualizarConvenio.Parameters.ParamByName('@DiasDeuda'              ).Value  := 0; //no se actualizan nunca
        spActualizarConvenio.Parameters.ParamByName('@ImporteDeuda'           ).Value  := 0; //no se actualizan nunca

        spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion'     ).Value  := cdsConveniosEnCarpeta.FieldByName('FechaHoraImpresion'    ).AsDateTime;
        if spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion'  ).Value  = 0 then
            spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion' ).Value  := null ;

        spActualizarConvenio.Parameters.ParamByName('@UsuarioImpresion'       ).Value  := cdsConveniosEnCarpeta.FieldByName('UsuarioImpresion'      ).AsString  ;

        spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales'  ).Value := cdsConveniosEnCarpeta.FieldByName('FechaHoraEnvioALegales').AsDateTime ;
        spActualizarConvenio.Parameters.ParamByName('@UsuarioEnvioLegales'    ).Value := cdsConveniosEnCarpeta.FieldByName('UsuarioEnvioLegales'   ).AsString   ;

        if spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales'  ).Value  = 0 then
            spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales' ).Value  := null ;

        cdsConveniosEnCarpeta.Edit;
        cdsConveniosEnCarpeta.FieldByName('FechaHoraSalidaLegales').AsDateTime := FechaProceso ;
        cdsConveniosEnCarpeta.FieldByName('UsuarioSalidaLegales').AsString   := UsuarioSistema ;
        cdsConveniosEnCarpeta.Post;


        spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales' ).Value   := cdsConveniosEnCarpeta.FieldByName('FechaHoraSalidaLegales').AsDateTime ;
        spActualizarConvenio.Parameters.ParamByName('@UsuarioSalidaLegales' ).Value     := cdsConveniosEnCarpeta.FieldByName('UsuarioSalidaLegales').AsString ;


        spActualizarConvenio.Parameters.ParamByName('@UsuarioActualizacion'   ).Value := UsuarioSistema;

        if spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales'  ).Value  = 0 then
            spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales' ).Value  := null ;
        //Si esta en la tabla es por Envio o porque ya estaba grabado
        if cdsConveniosEnCarpeta.FieldByName('CodigoEmpresasRecaudadoras').AsInteger = 0 then
            spActualizarConvenio.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value  := NULL
        else    spActualizarConvenio.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value  := cdsConveniosEnCarpeta.FieldByName('CodigoEmpresasRecaudadoras').AsInteger;
        //Si esta en la tabla es por Envio o porque ya estaba grabado            
        if cdsConveniosEnCarpeta.FieldByName('CodigoTipoDeuda').AsInteger = 0 then
            spActualizarConvenio.Parameters.ParamByName('@CodigoTipoDeuda').Value := NULL
        else spActualizarConvenio.Parameters.ParamByName('@CodigoTipoDeuda').Value := cdsConveniosEnCarpeta.FieldByName('CodigoTipoDeuda').AsInteger;


        try
//            DMConnections.BaseCAC.BeginTrans;                             //SS_660_MDI_20130227
            spActualizarConvenio.Execproc;
//            DMConnections.BaseCAC.CommitTrans;                            //SS_660_MDI_20130227
            ModalResult := mrOk;
        except on e:exception do begin
//            if DMConnections.BaseCAC.InTransaction then begin             //SS_660_MDI_20130227
//                DMConnections.BaseCAC.RollbackTrans;                      //SS_660_MDI_20130227
//            end;                                                          //SS_660_MDI_20130227

            //MsgBoxErr(MSG_ERROR_SQL, e.Message, Caption, MB_ICONERROR);   //SS_660_MDI_20130227
            FMensajeLegales := e.Message;
            end;
        end;
        cdsConveniosEnCarpeta.Next;
    end;
    Result :=  FMensajeLegales;                                             //SS_660_MDI_20130227
end;

{------------------------------------------------------------------------
        LlenaComboConEstadoPosibles

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Determina los estados posibles para el convenio en
                    lista amarilla.

-------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.LlenaComboConEstadoPosibles;
resourcestring
    MSG_ERROR_CARGA_ESTADOS = 'Ocurri� el siguiente error al intentar obtener los Estados para lista amarilla';
var
    SP: TAdoStoredProc;
begin
    try
        vcbNuevoEstado.Clear;
        try
            SP := TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'ObtenerEstadosPosiblesListaAmarilla';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@Estado').Value := FEstadoActual;
            SP.Open;
            while not sp.Eof do begin
                vcbNuevoEstado.Items.Add(   SP.FieldByName('Descripcion').AsString,
                                            SP.FieldByName('Estado').AsInteger);
                SP.Next;
            end;
            SP.Close;

            if not ExisteAcceso('ELIMINAR_CONVENIO_LISTA_AMARILLA') then                                                        // SS_660_CQU_20121010
                vcbNuevoEstado.Items.Delete(vcbNuevoEstado.Items.IndexOfValue(CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO)); // SS_660_CQU_20121010

            vcbNuevoEstado.ItemIndex := 0;
            vcbNuevoEstadoChange(nil);
        except on e:exception do begin
                MsgBoxErr(MSG_ERROR_CARGA_ESTADOS, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        FreeAndNil(SP);
    end;

end;

{------------------------------------------------------------------------
        vcbNuevoEstadoChange

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Oculta la solicitud de fecha de la carta, si el estado nuevo no es 2.

-------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.vcbNuevoEstadoChange(Sender: TObject);
var
    PonerVisible : boolean;
    NuevoEstado : integer;
begin
    if vcbNuevoEstado.ItemIndex >= 0 then begin
        NuevoEstado := vcbNuevoEstado.Value;
        //PonerVisible := (NuevoEstado = 2);                                                // SS_660_CQU_20121010
        PonerVisible := (NuevoEstado = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA); // SS_660_CQU_20121010
        lblFechaCarta.Visible := PonerVisible;
        deFechaCarta.Visible := PonerVisible;
        lblEmpresaCorreo.Visible := PonerVisible;                                           // SS_660_CQU_20121010
        cboEmpresaCorreo.Visible := PonerVisible;                                           // SS_660_CQU_20121010
        btnCambiar.Enabled := True;                                                         // SS_660_CQU_20130711
    end;

end;

{------------------------------------------------------------------------
        btnCambiarClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Cambia de estado el Convenio.

    Revisi�n 1
    Author      : MDI
    Date        : 07-Marzo-2013
    Description : Se modific� el m�todo de cambio de estado, separ�ndolos en cambio unitario y masivo


-------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.btnCambiarClick(Sender: TObject);
resourcestring
    MSG_EXITO           = 'Convenios han sido cambiados de estado exitosamente';
    MSG_ERROR_SQL       = 'Ocurri� un error al intentar cambiar de estado a los convenios';
begin
    if not Validaciones() then Exit;
    try
        Screen.Cursor := crHourglass; // SS_660_CQU_20130628
        FListadoConvenio.First;

         while not FListadoConvenio.Eof do  begin
            FIDConvenio := FListadoConvenio.FieldByName('IDConvenioInhabilitado').AsInteger;
            FCodigoConvenio := FListadoConvenio.FieldByName('CodigoConvenio').AsInteger;
            FEstadoActual := FListadoConvenio.FieldByName('Estado').AsInteger;
            FNumeroConvenio := FListadoConvenio.FieldByName('NumeroConvenio').AsString;
            FDescripcion := FListadoConvenio.FieldByName('Descripcion').AsString;
            FFechaEnvioCarta := FListadoConvenio.FieldByName('FechaEnvioCarta').AsDateTime;
            FEstaEnLegales := FListadoConvenio.FieldByName('EstaEnLegales').AsBoolean;
            FNumeroDocumento := FListadoConvenio.FieldByName('NumeroDocumento').AsString;
            // INICIO SS_660_MDI_20130227
            try
                CambiaEstado( FIDConvenio,
                                    FCodigoConvenio,
                                    FEstadoActual,
                                    FNumeroConvenio,
                                    FDescripcion,
                                    FFechaEnvioCarta,
                                    FEstaEnLegales,
                                    FNumeroDocumento);
                except on e:Exception do begin
                    MsgBoxErr(MSG_ERROR_SQL, FMensaje, Caption, MB_ICONERROR);
                    exit;
                end;
            end;
            FListadoConvenio.Next;
            // FINAL SS_660_MDI_20130227
        end;
    finally
        //if (vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_INHABILITADO) and (cdsConveniosEnCarpeta.RecordCount > 0) then begin  // SS_660_CQU_20130822
        if (vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_INHABILITADO)                                                           // SS_660_CQU_20130822
            and (cdsConveniosEnCarpeta.RecordCount > 0) and (FMensaje = EmptyStr)                                                               // SS_660_CQU_20130822
        then begin                                                                                                                              // SS_660_CQU_20130822
            Application.CreateForm(TVentanaConveniosEnLegales, VentanaConveniosEnLegales);
            if VentanaConveniosEnLegales.Inicializar(cdsSacadosLegales) then begin
                if VentanaConveniosEnLegales.ShowModal = mrOk then begin
                    ModalResult := mrOk;
                end;
            end;
        end
        else begin
            if (ResultadoCambio = '') and (FMensaje = '') then begin                    // SS_660_MDI_20130227
                MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);                         // SS_660_MDI_20130227
                ModalResult := mrOk;                                                    // SS_660_MDI_20130227
            end                                                                         // SS_660_MDI_20130227
            else begin                                                                  // SS_660_MDI_20130227
                if ResultadoCambio <> '' then begin                                     // SS_660_MDI_20130227

                    MsgBoxErr(MSG_ERROR_SQL, ResultadoCambio, Caption, MB_ICONERROR);   // SS_660_MDI_20130227
                    ModalResult := mrCancel;                                            // SS_660_MDI_20130227

                end
                else begin

                    MsgBoxErr(MSG_ERROR_SQL, FMensaje, Caption, MB_ICONERROR);          // SS_660_MDI_20130227
                    ModalResult := mrCancel;                                            // SS_660_MDI_20130227

                end;

            end;
        end;
        Screen.Cursor := crDefault; // SS_660_CQU_20130628
    end

end;

{-----------------------------------------------------------------------------
  Function Name: CambiaEstado
  Author:  MDI
  Date Created:  04/ 03 / 2013
  Description: Realiza el cambio de estado para los datos enviados como par�metro.
              Este procedimiento es llamado n veces en la ejecuci�n, dependiendo de la cantidad de convenios.
  Parameters: IDConvenioInhabilitado : Integer;
              CodigoConvenio : Integer;
              Estado : Integer;
              NumeroConvenio : string;
              Descripcion : string;
              FechaEnvioCarta : TDateTime;
              EstaEnLegales : Boolean;
              NumeroDocumento : string
  Return Value: N/A

-----------------------------------------------------------------------------}
function TCambiarEstadoListaAmarillaForm.CambiaEstado( IDConvenioInhabilitado : Integer;
                                                              CodigoConvenio : Integer;
                                                              Estado : Integer;
                                                              NumeroConvenio : string;
                                                              Descripcion : string;
                                                              FechaEnvioCarta : TDateTime;
                                                              EstaEnLegales : Boolean;
                                                              NumeroDocumento : string):string;
resourcestring
    MSG_ERROR_SQL       = 'Ocurri� un error al intentar cambiar de estado al convenio';
var
    FechaDeHoy : TDateTime;
    CodConvenio : string;
begin

    FControl := False;                                                                              // SS_660_MDI_20130227
    CodConvenio := IntToStr(CodigoConvenio);

    cdsExcluidos.Active := True;                                                                    // SS_660_MDI_20130227
    FechaDeHoy := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);
    FFechaEnvioCarta := FListadoConvenio.FieldByName('FechaEnvioCarta').AsDateTime;

    with spCambiarEstadoConvenioListaAmarilla do begin

        FMensaje := '';                                                                             // SS_660_MDI_20130227

        Parameters.Refresh;
        Parameters.ParamByName('@CodigoConvenio').Value             := CodigoConvenio;
        Parameters.ParamByName('@IDConvenioInhabilitado').Value     := IDConvenioInhabilitado;

        if deFechaCarta.Visible then Parameters.ParamByName('@FechaEnvioCarta').Value := deFechaCarta.date
        else Parameters.ParamByName('@FechaEnvioCarta').Value := null;

        if cboEmpresaCorreo.Visible then Parameters.ParamByName('@IDEmpresaCorreo').Value := cboEmpresaCorreo.Value // SS_660_CQU_20121010
        else Parameters.ParamByName('@IDEmpresaCorreo').Value := null;                                              // SS_660_CQU_20121010

        Parameters.ParamByName('@NuevoEstado').Value                := vcbNuevoEstado.Value;
        Parameters.ParamByName('@CodigoUsuario').Value              := UsuarioSistema;
        try
            DMConnections.BaseCAC.BeginTrans;
            ExecProc;

            if vcbNuevoEstado.Value = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_INHABILITADO then begin  // SS_660_MDI_20130227
                cdsConveniosEnCarpeta.Filter := 'CodigoConvenio = '+CodConvenio;                    // SS_660_MDI_20130227
                cdsConveniosEnCarpeta.Filtered:= True;                                              // SS_660_MDI_20130227
                cdsConveniosEnCarpeta.First;                                                        // SS_660_MDI_20130227

                while not cdsConveniosEnCarpeta.Eof do begin                                        // SS_660_MDI_20130227
                    ResultadoCambio := '';                                                          // SS_660_MDI_20130227
                    ResultadoCambio := SacarConvenioCarpetasLegales(CodigoConvenio);                // SS_660_MDI_20130227
                    if ResultadoCambio = '' then begin                                              // SS_660_MDI_20130227
                        cdsSacadosLegales.Append;                                                   // SS_660_MDI_20130227
                        cdsSacadosLegales.FieldByName('NumeroConvenio').AsString := NumeroConvenio; // SS_660_MDI_20130227
                        cdsSacadosLegales.Post;                                                     // SS_660_MDI_20130227
                    end                                                                             // SS_660_MDI_20130227
                    else begin
                        raise Exception.Create(ResultadoCambio);                                    // SS_660_MDI_20130227
                    end;
                    cdsConveniosEnCarpeta.Next;
                end;
                cdsConveniosEnCarpeta.Filter := EmptyStr;
                cdsConveniosEnCarpeta.Filtered := False;
            end;

            DMConnections.BaseCAC.CommitTrans;

            except on e:exception do begin
                if DMConnections.BaseCAC.InTransaction then begin
                    DMConnections.BaseCAC.RollbackTrans;
                    FMensaje := e.Message;
                    //MsgBoxErr(MSG_ERROR_SQL, e.Message, Caption, MB_ICONERROR);
                    Result := FMensaje;
                end;
            end;
        end;
    end;



end;

{------------------------------------------------------------------------
        btnCancelarClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Cierra el formulario.

-------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

{-----------------------------------------------------------------------------
 File Name      : CargarEmpresas
 Author         : Claudio Quezada Ib��ez
 Date Created   : 26-Febrero-2013
 Language       : ES-CL
 Firma          : SS_660_CQU_20121010
 Description    : Obtiene las Empresas de Correo
-----------------------------------------------------------------------------}
procedure TCambiarEstadoListaAmarillaForm.CargarEmpresas;
resourcestring
    MSG_CARGAR_EMPRESAS =   'Error al cargar las empresas de correo, mensaje del sistema: ';
var
    SP: TAdoStoredProc;
begin
    // Creo el SP
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    try
        // Obtener Empresas de Correo
        SP.ProcedureName := 'ObtenerEmpresaCorreo';
        SP.Open;
        while not SP.Eof do begin
            cboEmpresaCorreo.Items.Add( SP.FieldByName('Nombre').AsString,
                                        SP.FieldByName('CodigoEmpresaCorreo').AsInteger);
            SP.Next;
        end;
        SP.Close;
        cboEmpresaCorreo.ItemIndex := 0;
    except
        on e:exception do begin
            MsgBoxErr(MSG_CARGAR_EMPRESAS, e.Message, Caption, MB_ICONERROR);
        end;
    end;
end;

{---------------------------------------------------------------------------------
function Name   : PuedeInhabilitar
Author          : CQuezadaI
Date Created    : 19-07-2013
Description     : Verifica que el Convenio cumpla con la Regla de Validaci�n
                  para la concesionaria que se indique.
Parameters      : CodigoConvenio, CodigoConcesionaria : Integer
Return Value    : True / False
Firma           : SS_660_CQU_20130711
----------------------------------------------------------------------------------}
function TCambiarEstadoListaAmarillaForm.PuedeInhabilitar;
resourcestring
    MSG_ERROR_REGLA     =   'Error al cargar la regla de validaci�n';
var
    Inhabilitar : Boolean;
    CodigoConvenio, CodigoConcesionaria : Integer;
begin
    try
        FListadoConvenio.First;
        while not FListadoConvenio.Eof do begin
            CodigoConvenio := FListadoConvenio.FieldByName('CodigoConvenio').Value;
            CodigoConcesionaria := FListadoConvenio.FieldByName('CodigoConcesionaria').Value;
            try
                if spReglaValidacionListaAmarilla.Active then spReglaValidacionListaAmarilla.Close;
                // Verifico si se puede Inhabilitar para la concesionaria indicada
                with spReglaValidacionListaAmarilla do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoConvenio').Value      := CodigoConvenio;
                    Parameters.ParamByName('@Fecha').Value               := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);
                    Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
                    Open;
                    if not IsEmpty then begin
                        // Si DebeEstar, est� EnProceso y est� en Estado CartaEnviada, puede InHabilitar
                        if  FieldByName('DebeEstar').AsBoolean and FieldByName('EnProceso').AsBoolean and
                            ( (FieldByName('UltimoEstado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA) or
                              (FieldByName('UltimoEstado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION) or
                              (FieldByName('UltimoEstado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_RE_EVALUA)
                            )
                        then Inhabilitar := True
                        // Si DebeEstar y no est� EnProceso, puede InHabilitar
                        else if  FieldByName('DebeEstar').AsBoolean and not FieldByName('EnProceso').AsBoolean
                        then Inhabilitar := True
                        else Inhabilitar := False;
                    end;
                    Close;
                end;
            except
                on e:exception do begin
                    MsgBoxErr(MSG_ERROR_REGLA, e.Message, Caption, MB_ICONERROR);
                end;
            end;
            if (FTipoCambioEstado = 'Masivo') and (not Inhabilitar) then Exit;
            FListadoConvenio.Next;
        end;
    finally
        Result := Inhabilitar;
    end;
end;

{---------------------------------------------------------------------------------
function Name   : TieneDistintasConcesionarias
Author          : CQuezadaI
Date Created    : 19-07-2013
Description     : Verifica que el Convenio pueda ser dado de alta en Lista Amarilla (Estado 3)
                  para la concesionaria que se indique.
Parameters      : None
Return Value    : True / False
Firma           : SS_660_CQU_20130711
----------------------------------------------------------------------------------}
function TCambiarEstadoListaAmarillaForm.TieneDistintasConcesionarias : Boolean;
var
    Puntero : TBookmark;
begin
    Puntero := FListadoConvenio.GetBookmark;

    FListadoConvenio.DisableControls;
    FListadoConvenio.First;

    FListadoConvenio.Filter := 'CodigoConcesionaria <> ' + FListadoConvenio.FieldByName('CodigoConcesionaria').AsString;;
    FListadoConvenio.Filtered := True;
    // Evaluo
    if not FListadoConvenio.Eof then Result := True
    else Result := False;

    FListadoConvenio.Filter := EmptyStr;
    FListadoConvenio.Filtered := False;

    if FListadoConvenio.BookmarkValid(Puntero) then FListadoConvenio.GotoBookmark(Puntero);
    FListadoConvenio.FreeBookmark(Puntero);
    FListadoConvenio.EnableControls;
end;

end.
