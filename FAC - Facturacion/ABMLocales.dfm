object FormLocales: TFormLocales
  Left = 241
  Top = 174
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Locales'
  ClientHeight = 493
  ClientWidth = 838
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 454
    Width = 838
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 516
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          ExplicitWidth = 0
          ExplicitHeight = 0
          object BtnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object Lista: TAbmList
    Left = 0
    Top = 33
    Width = 838
    Height = 148
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'80'#0'Numero Local  '
      
        #0'292'#0'Empresa                                                    ' +
        '                             ')
    HScrollBar = True
    RefreshTime = 10
    Table = Locales
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaClick
    OnProcess = ListaProcess
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
    OnInsert = ListaInsert
    OnDelete = ListaDelete
    OnEdit = ListaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 838
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar, btImprimir]
    OnClose = AbmToolbar1Close
  end
  object GroupB: TPanel
    Left = 0
    Top = 181
    Width = 838
    Height = 273
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object lblEmpresa: TLabel
      Left = 16
      Top = 50
      Width = 53
      Height = 13
      Caption = '&Empresa:'
      FocusControl = txtEmpresa
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigo: TLabel
      Left = 17
      Top = 26
      Width = 48
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txtNumeroLocal
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtEmpresa: TEdit
      Left = 73
      Top = 46
      Width = 315
      Height = 21
      Color = 16444382
      MaxLength = 100
      TabOrder = 1
    end
    object txtNumeroLocal: TEdit
      Left = 75
      Top = 22
      Width = 55
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 2
      TabOrder = 0
    end
    inline FrameDomicilio: TFrameDomicilio
      Left = 4
      Top = 76
      Width = 831
      Height = 107
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      TabOrder = 2
      TabStop = True
      ExplicitLeft = 4
      ExplicitTop = 76
      ExplicitWidth = 831
      ExplicitHeight = 107
      inherited Pnl_Arriba: TPanel
        Width = 831
        ExplicitWidth = 831
      end
      inherited Pnl_Abajo: TPanel
        Width = 831
        ExplicitWidth = 831
      end
      inherited Pnl_Medio: TPanel
        Width = 831
        ExplicitWidth = 831
      end
    end
    inline FrameTelefono: TFrameTelefono
      Left = 14
      Top = 155
      Width = 617
      Height = 25
      TabOrder = 3
      TabStop = True
      ExplicitLeft = 14
      ExplicitTop = 155
      ExplicitWidth = 617
      inherited lblTelefono: TLabel
        Left = 3
        ExplicitLeft = 3
      end
    end
  end
  object Locales: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'Locales'
    Left = 324
    Top = 75
    object LocalesNumeroLocal: TAutoIncField
      FieldName = 'NumeroLocal'
      ReadOnly = True
    end
    object LocalesEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 100
    end
    object LocalesCodigoMedioComunicacion: TIntegerField
      FieldName = 'CodigoMedioComunicacion'
    end
    object LocalesCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object LocalesFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object LocalesUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object LocalesFechaHoraActualizacion: TDateTimeField
      FieldName = 'FechaHoraActualizacion'
    end
    object LocalesUsuarioActualizacion: TStringField
      FieldName = 'UsuarioActualizacion'
      FixedChar = True
    end
  end
  object spObtenerDomicilioLocal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioLocal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroLocal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 144
  end
  object spObtenerMedioComunicacionLocal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMedioComunicacionLocal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 272
    Top = 144
  end
  object spActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 328
    Top = 144
  end
  object spActualizarMedioComunicacionLocal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionLocal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Anexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@EstadoVerificacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoMedioComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 376
    Top = 144
  end
end
