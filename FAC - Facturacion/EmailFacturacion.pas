unit EmailFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Util, UtilProc, UtilDB, StdCtrls, ExtCtrls, jpeg, DMConnection, DB,
  ADODB, DmiCtrls, BuscaTab, ListBoxEx, DBListEx, CollPnl,
  ConfirmarAccionEMailsFacturacion, ExplorarEmails;

type
  TfrmMailsFacturacion = class(TForm)
    nbWizard: TNotebook;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    spObtenerProcesos: TADOStoredProc;
    btProcesos: TBuscaTabla;
    Panel2: TPanel;
    btnAnterior: TButton;
    btnFinalizar: TButton;
    Panel3: TPanel;
    btnCerrar: TButton;
    btnSiguiente: TButton;
    dblComprobantesPendientes: TDBListEx;
    spPreview: TADOStoredProc;
    DataSource1: TDataSource;
    cpBusqueda: TCollapsablePanel;
    Label4: TLabel;
    bteProcesos: TBuscaTabEdit;
    cbSoloPendientes: TCheckBox;
    btnObtenerPreview: TButton;
    lblDetalle: TLabel;
    spContarNotificaciones: TADOStoredProc;
    Label5: TLabel;
    lblAca: TLabel;
    spVerificar: TADOStoredProc;
    spCrearMails: TADOStoredProc;
    procedure lblAcaClick(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure btnObtenerPreviewClick(Sender: TObject);
    function btProcesosProcess(Tabla: TDataSet; var Texto: string): Boolean;
    procedure btProcesosSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FNumeroProceso, FCantidadPendientes: Integer;
    FFechaInicio, FFechaFin: TDateTime;
    FOperador: String;
    function VerificarProceso(NumeroProceso: Integer = 0): Boolean;
    procedure CrearEMails(NumeroProcesoFacturacion: Integer);
    procedure Limpiar;
  public
    function Inicializar: Boolean;
  end;

var
  frmMailsFacturacion: TfrmMailsFacturacion;

implementation

{$R *.dfm}

{ TfrmMailsFacturacion }

function TfrmMailsFacturacion.Inicializar: Boolean;
resourcestring
    MSG_ERROR = 'Error';
    MSG_INIT_ERROR = 'Error al inicializar';
var
	SZ: TSize;
begin
	SZ := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, SZ.cx, sz.cy);
    nbWizard.PageIndex := 0;
    lblDetalle.Visible := False;
    cpBusqueda.Open := True;
    try
        Result := OpenTables([spObtenerProcesos]);
    except
        on e: Exception do begin
           MsgBoxErr(MSG_ERROR, e.Message, MSG_INIT_ERROR, MB_ICONERROR);
           Result := False;
        end;
    end;
end;

procedure TfrmMailsFacturacion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmMailsFacturacion.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmMailsFacturacion.btnSiguienteClick(Sender: TObject);
begin
    nbWizard.PageIndex := nbWizard.PageIndex + 1;
end;

procedure TfrmMailsFacturacion.btnAnteriorClick(Sender: TObject);
begin
    nbWizard.PageIndex := nbWizard.PageIndex - 1;
end;

procedure TfrmMailsFacturacion.btProcesosSelect(Sender: TObject;
  Tabla: TDataSet);
begin
    FNumeroProceso  := Tabla.FieldByName('NumeroProcesoFacturacion').AsInteger;
    FFechaInicio    := Tabla.FieldByName('FechaHoraInicio').AsDateTime;
    FFechaFin       := Tabla.FieldByName('FechaHoraFin').AsDateTime;
    FOperador       := Trim(Tabla.FieldByName('Operador').AsString);
    bteProcesos.Text := IntToStr(FNumeroProceso);
end;

function TfrmMailsFacturacion.btProcesosProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
    Texto := Tabla.FieldByName('NumeroProcesoFacturacion').AsString + Space(1) +
      '-' + Space(1) + Trim(Tabla.FieldByName('Operador').AsString) +
      '-' + Space(1) + Tabla.FieldByName('FechaHoraInicio').AsString;
    Result := True;
end;

procedure TfrmMailsFacturacion.btnObtenerPreviewClick(Sender: TObject);
begin
    Cursor := crHourGlass;
    try
        lblDetalle.Visible := False;
        FCantidadPendientes := 0;
        spPreview.Close;
        spContarNotificaciones.Close;
        if cbSoloPendientes.Checked then begin
            spPreview.Parameters.ParamByName('@EnvioElectronico').Value := 1;
            spContarNotificaciones.Parameters.ParamByName('@EnvioElectronico').Value := 1;
        end else begin
            spPreview.Parameters.ParamByName('@EnvioElectronico').Value := 0;
            spContarNotificaciones.Parameters.ParamByName('@EnvioElectronico').Value := 1;
        end;
        spPreview.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
        spContarNotificaciones.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
        try
            spPreview.Open;
            spContarNotificaciones.Open;
        except
            on e: Exception do begin
                //MsgBox();
                Exit;
            end;
        end;
        FCantidadPendientes := spContarNotificaciones.FieldByName('Cantidad').AsInteger;
        lblDetalle.Caption := Format(lblDetalle.Caption, [FCantidadPendientes]);
        lblDetalle.Visible := True;
    finally
        cursor := crDefault;
        btnFinalizar.Enabled := (spPreview.RecordCount > 0);
    end;
end;

procedure TfrmMailsFacturacion.btnFinalizarClick(Sender: TObject);
var
    f: TfrmAccionesEmail;
begin
   if VerificarProceso(FNumeroProceso) then begin
        // El proceso ya tiene al menos un mail, que hacemos?
        Application.CreateForm(TfrmAccionesEmail, f);
        if f.Inicializar(FNumeroProceso) then begin
            f.ShowModal;
            if f.ModalResult = mrOK then begin
                f.Release;
                CrearEmails(FNumeroProceso);
            end;
        end else begin
            f.Release;
        end;
   end else begin
        // Todo Ok, le damos nom�s..
        CrearEmails(FNumeroProceso);
   end;
end;

function TfrmMailsFacturacion.VerificarProceso(NumeroProceso: Integer = 0): Boolean;
resourcestring
    MSG_ERROR = 'Error';
    MSG_CHECK = 'Verificar Proceso';
begin
    Result := False;
    if NumeroProceso <= 0 then begin
        raise Exception.Create('Debe especificarse un n�mero de proceso');
        Exit;
    end;
    spVerificar.Close;
    spVerificar.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProceso;
    try
        spVerificar.Open;
        if spVerificar.Fields[0].Value = 1 then begin
            Result := True;
        end else begin
            Result := False;
        end;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, MSG_CHECK, MB_ICONERROR);
            Exit;
        end;
    end;
end;

procedure TfrmMailsFacturacion.CrearEMails(NumeroProcesoFacturacion: Integer);
resourcestring
    MSG_TITLE = 'Creaci�n de e-mails de Notificaci�n de Facturaci�n';
    MSG_MESSAGE = 'Desea crear los e-mails del proceso %d ?';
    MSG_OK = 'Proceso Finalizado';
    MSG_ERROR = 'Error';
begin
    if MsgBox(Format(MSG_MESSAGE, [FNumeroProceso]), MSG_TITLE,
      MB_ICONWARNING or MB_YESNO) = IDYES then begin
        Application.ProcessMessages;
        spCrearMails.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
        try
            spCrearMails.ExecProc;
            MsgBox(MSG_OK);
            Limpiar;
        except
            on e: exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
            end;
        end;
    end;
end;
procedure TfrmMailsFacturacion.Limpiar;
begin
    bteProcesos.Clear;
    spPreview.Close;
    FNumeroProceso := 0;
    btnFinalizar.Enabled := False;
end;

procedure TfrmMailsFacturacion.lblAcaClick(Sender: TObject);
var
    f: TfrmExplorarMails;
begin
    if FindFormOrCreate(TfrmExplorarMails, f) then begin
        f.Show;
    end else begin
        if FNumeroProceso > 0 then begin
            if not f.Inicializar(FNumeroProceso) then f.Release;
        end else begin
            if not f.Inicializar then f.Release;
        end;
    end;
end;

end.

