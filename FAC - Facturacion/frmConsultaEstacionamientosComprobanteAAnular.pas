{------------------------------------------------------------------------------
                frmConsultaEstacionamientosComprobanteAAnular

    Author      : mbecerra
    Date        : 29-Marzo-2011
    Description :
                    Permite la selecci�n de las transacciones de
                    estacionamiento a incluir en la Nota de Cr�dito

-------------------------------------------------------------------------------}
unit frmConsultaEstacionamientosComprobanteAAnular;

interface

uses
  PeaProcs,
  util,
  MenuesContextuales,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ListBoxEx, DBListEx, StdCtrls, DB, ADODB,
  DMConnection,StrUtils,
  UtilProc, ImgList;

type
  TConsultaEstacionamientosComprobanteAAnular = class(TForm)
    dblEstacionamientos: TDBListEx;
    Panel4: TPanel;
    lblTotales: TLabel;
    lblTotSeleccionados: TLabel;
    BtnSalir: TButton;
    btnSelectAll: TButton;
    btnDeselectAll: TButton;
    btnInvertirSeleccion: TButton;
    spObtenerEstacionamientosComprobante: TADOStoredProc;
    dsEstacionamientos: TDataSource;
    lnCheck: TImageList;
    spObtenerEstacionamientosPorMovimientoCuenta: TADOStoredProc;
    procedure btnSelectAllClick(Sender: TObject);
	procedure dblEstacionamientosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure dblEstacionamientosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
	procedure btn_FiltrarClick(Sender: TObject);
	procedure ActualizarImporteTotalSeleccionados;
    procedure dblEstacionamientosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dblEstacionamientosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnDeselectAllClick(Sender: TObject);
    procedure btnInvertirSeleccionClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure dblEstacionamientosCheckLink(Sender: TCustomDBListEx;
      Column: TDBListExColumn; var IsLink: Boolean);

  private
    { Private declarations }
	FListaEstacionamientos: TStringList;
	FImporteDeEstacionamientosSeleccionados: Int64;
	FNumeroMovimiento : int64;
	FCantidadTotalDeEstacionamientos: integer;
	Function  ListaEstacionamientosInclude(valor:string):boolean;
	Function  ListaEstacionamientosExclude(valor:string):boolean;
	Function  ListaEstacionamientosIn(valor:string):boolean;
  public
    { Public declarations }
    function Inicializar(aNumeroMovimiento: int64; MDIChild: Boolean; var FListaDeEstacionamientos:TStringList) : Boolean;
	function ImporteTotal: Int64;
	function SelectedAll: boolean;
  end;

var
  ConsultaEstacionamientosComprobanteAAnular: TConsultaEstacionamientosComprobanteAAnular;

implementation

{$R *.dfm}

const
	CONST_COLUMNA_IMAGEN = 9;

{------------------------------------------------------------------------------
                Inicializar

    Author      : mbecerra
    Date        : 29-Marzo-2011
    Description :
                    Inicializa el formulario y carga las transacciones
                    de estacionamiento facturadas en el comprobante.

-------------------------------------------------------------------------------}
function TConsultaEstacionamientosComprobanteAAnular.Inicializar( aNumeroMovimiento: int64; MDIChild: Boolean; var FListaDeEstacionamientos:TStringList) : Boolean;
Var S: TSize;
begin
	Result := True;
    try
		if MDIChild then begin
			S := GetFormClientSize(Application.MainForm);
			SetBounds(0, 0, S.cx, S.cy);
		end else begin
			FormStyle := fsNormal;
			Visible := False;
		end;
		CenterForm(Self);
    except
            Result := False;
    end;
    FListaEstacionamientos:=FListaDeEstacionamientos;
    FImporteDeEstacionamientosSeleccionados:=0;
    FCantidadTotalDeEstacionamientos:=0;
    FNumeroMovimiento:=aNumeroMovimiento;
   	btn_FiltrarClick(Self);
end;

function TConsultaEstacionamientosComprobanteAAnular.ListaEstacionamientosExclude(
  valor: string): boolean;
var index:integer;
begin
	result:=false;
	try
		index:=FListaEstacionamientos.IndexOf(valor+'='+IntToStr(FNumeroMovimiento));
		if index <> -1 then FListaEstacionamientos.Delete(index);
		result:=true;
	except
	end;
end;

function TConsultaEstacionamientosComprobanteAAnular.ListaEstacionamientosIn(
  valor: string): boolean;
begin
	result := not (FListaEstacionamientos.IndexOf(valor+'='+IntToStr(FNumeroMovimiento)) = -1);
end;

function TConsultaEstacionamientosComprobanteAAnular.ListaEstacionamientosInclude(valor: string): boolean;
begin
	result:=false;
	try
		//Se hace necesario cargar el NumCorrCA y el NumeroMovimiento porque  es una lista que va a contener los
		//NumCorrCA de todos los movimientos del comprobante, y como es necesario saber si se seleccionaron TODOS los de
		//un movimiento, necesitamos tener la informacion del numerode movimiento.
		//Cargamos el string de forma NumCorrCA=NumeroMovimiento. Ver Metodo SelectedAll
		if not ListaEstacionamientosIn(valor) then FListaEstacionamientos.Add(valor+'='+IntToStr(FNumeroMovimiento));
		result:=true;
	except
	end;
end;

procedure TConsultaEstacionamientosComprobanteAAnular.btn_FiltrarClick(
  Sender: TObject);
begin
	try
		spObtenerEstacionamientosPorMovimientoCuenta.DisableControls;
		with spObtenerEstacionamientosPorMovimientoCuenta do begin
			close;
			commandtimeout:=500;
			Parameters.Refresh;
			Parameters.ParamByName('@NumeroMovimiento').Value:= FNumeroMovimiento;
		end;
		spObtenerEstacionamientosPorMovimientoCuenta.Open;
		FCantidadTotalDeEstacionamientos := spObtenerEstacionamientosPorMovimientoCuenta.RecordCount ;
	finally
		spObtenerEstacionamientosPorMovimientoCuenta.EnableControls;
	end;
	ActualizarImporteTotalSeleccionados;
end;

procedure TConsultaEstacionamientosComprobanteAAnular.dblEstacionamientosCheckLink(
  Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
	IsLink := column = dblEstacionamientos.Columns[0];
end;

procedure TConsultaEstacionamientosComprobanteAAnular.dblEstacionamientosContextPopup(
  Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
	if not spObtenerEstacionamientosPorMovimientoCuenta.IsEmpty then Handled :=
	//MostrarMenuContextualMultiplesEstacionamientosNoCliente(dblEstacionamientos.ClientToScreen(MousePos),FListaEstacionamientos);
	MostrarMenuContextualMultiplesEstacionamientos(dblEstacionamientos.ClientToScreen(MousePos),FListaEstacionamientos);
end;

procedure TConsultaEstacionamientosComprobanteAAnular.dblEstacionamientosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	i: Integer;
begin

	//muestra si esta seleccionado para realizar un reclamo o no
	if Column = dblEstacionamientos.Columns[0] then begin
		i:=iif(not ListaEstacionamientosIn(spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('NumCorrEstacionamiento').Asstring),0,1);
		//verifica si esta seleccionado o no
		lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;




end;

procedure TConsultaEstacionamientosComprobanteAAnular.dblEstacionamientosKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  //si presiona spaceBar, genera lo mismo que el click de seleccion / deselecci�n
  if Key = VK_SPACE then
	dblEstacionamientosLinkClick(dblEstacionamientos , dblEstacionamientos.Columns[0]);
end;

procedure TConsultaEstacionamientosComprobanteAAnular.dblEstacionamientosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
	NumCorrEstacionamiento: string;
begin
	Screen.Cursor := crHourGlass ;
	//selecciono/desselecciono transito
	if Column = dblEstacionamientos.Columns[0] then begin
		NumCorrEstacionamiento  := spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('NumCorrEstacionamiento').Asstring;
		if ListaEstacionamientosIn(NumCorrEstacionamiento ) then begin
			ListaEstacionamientosExclude(NumCorrEstacionamiento );
			//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
			FImporteDeEstacionamientosSeleccionados := FImporteDeEstacionamientosSeleccionados - spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant; //Revision 4
		end else begin
			ListaEstacionamientosInclude(NumCorrEstacionamiento );
			//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
			FImporteDeEstacionamientosSeleccionados := FImporteDeEstacionamientosSeleccionados + spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant; //Revision 4
		end;
		//dibuja el cambio
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC, RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeEstacionamientosSeleccionados) div 100);
		Sender.Invalidate;
		Sender.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

function TConsultaEstacionamientosComprobanteAAnular.ImporteTotal: Int64;
begin
	Result := RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeEstacionamientosSeleccionados) div 100 ;
end;

function TConsultaEstacionamientosComprobanteAAnular.SelectedAll: boolean;
var
  xIndex : integer;
  xCantidad : integer;
begin
	xCantidad := 0;
	//Como en la lista se carg� un string del tipo 'NumCorrCA=NumeroMovimiento', el ValueFromIndex nos devuelve el NumeroMovimiento
	for xIndex := 0 to FListaEstacionamientos.Count -1 do
		if FListaEstacionamientos.ValueFromIndex[xIndex]  = IntToStr(1) then Inc(xCantidad);

	Result := xCantidad = FCantidadTotalDeEstacionamientos ;
end;




procedure TConsultaEstacionamientosComprobanteAAnular.ActualizarImporteTotalSeleccionados;
Var
	NumcorrEstacionamiento: string;
begin
	Screen.Cursor := crHourGlass ;
	if (spObtenerEstacionamientosPorMovimientoCuenta.Active) and  (FListaEstacionamientos.Count > 0) then begin
		spObtenerEstacionamientosPorMovimientoCuenta.DisableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		FImporteDeEstacionamientosSeleccionados := 0;
		while not spObtenerEstacionamientosPorMovimientoCuenta.eof do begin
			NumcorrEstacionamiento := spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('NumcorrEstacionamiento').Asstring;
			if ListaEstacionamientosIn(NumcorrEstacionamiento) then
				FImporteDeEstacionamientosSeleccionados := FImporteDeEstacionamientosSeleccionados + spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant;
			spObtenerEstacionamientosPorMovimientoCuenta.Next;
		end;
		spObtenerEstacionamientosPorMovimientoCuenta.EnableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeEstacionamientosSeleccionados) div 100 );
		dblEstacionamientos.Invalidate;
		dblEstacionamientos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;



procedure TConsultaEstacionamientosComprobanteAAnular.btnDeselectAllClick(
  Sender: TObject);
Var
	NumCorrEstacionamiento: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerEstacionamientosPorMovimientoCuenta.Active then begin
		spObtenerEstacionamientosPorMovimientoCuenta.DisableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		while not spObtenerEstacionamientosPorMovimientoCuenta.eof do begin
			NumCorrEstacionamiento := spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('NumCorrEstacionamiento').Asstring;
			if ListaEstacionamientosIn(NumCorrEstacionamiento) then begin
				ListaEstacionamientosExclude(NumCorrEstacionamiento);
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeEstacionamientosSeleccionados := FImporteDeEstacionamientosSeleccionados - spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant;   //Revision 4
			end;
			spObtenerEstacionamientosPorMovimientoCuenta.Next;
		end;
		spObtenerEstacionamientosPorMovimientoCuenta.EnableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeEstacionamientosSeleccionados) div 100 );
		dblEstacionamientos.Invalidate;
		dblEstacionamientos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;
procedure TConsultaEstacionamientosComprobanteAAnular.btnInvertirSeleccionClick(
  Sender: TObject);
Var
	NumCorrEstacionamiento: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerEstacionamientosPorMovimientoCuenta.Active then begin
		spObtenerEstacionamientosPorMovimientoCuenta.DisableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		while not spObtenerEstacionamientosPorMovimientoCuenta.eof do begin
			NumCorrEstacionamiento := spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('NumCorrEstacionamiento').Asstring;
			if ListaEstacionamientosIn(NumCorrEstacionamiento) then begin
				ListaEstacionamientosExclude(NumCorrEstacionamiento);
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeEstacionamientosSeleccionados := FImporteDeEstacionamientosSeleccionados - spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant;  //Revision 4
			end else begin
				ListaEstacionamientosInclude(NumCorrEstacionamiento);
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeEstacionamientosSeleccionados := FImporteDeEstacionamientosSeleccionados + spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant;   //Revision 4
			end;
			spObtenerEstacionamientosPorMovimientoCuenta.Next;
		end;
		spObtenerEstacionamientosPorMovimientoCuenta.EnableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeEstacionamientosSeleccionados) div 100);
		dblEstacionamientos.Invalidate;
		dblEstacionamientos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

procedure TConsultaEstacionamientosComprobanteAAnular.BtnSalirClick(
  Sender: TObject);
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerEstacionamientosPorMovimientoCuenta.Active then begin
		FImporteDeEstacionamientosSeleccionados := 0;
		spObtenerEstacionamientosPorMovimientoCuenta.DisableControls ;
		spObtenerEstacionamientosPorMovimientoCuenta.First ;
		while not spObtenerEstacionamientosPorMovimientoCuenta.eof do begin
			if ListaEstacionamientosIn(spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('NumCorrEstacionamiento').Asstring) then
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeEstacionamientosSeleccionados:= FImporteDeEstacionamientosSeleccionados + spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant; //Revision 4
			spObtenerEstacionamientosPorMovimientoCuenta.Next ;
		end;
		spObtenerEstacionamientosPorMovimientoCuenta.EnableControls ;
		spObtenerEstacionamientosPorMovimientoCuenta.Close;
	end;
	Close;
	Screen.Cursor := crDefault ;
end;

procedure TConsultaEstacionamientosComprobanteAAnular.btnSelectAllClick(
  Sender: TObject);
Var
	NumCorrEstacionamiento: string;
begin
    Screen.Cursor := crHourGlass ;
	if spObtenerEstacionamientosPorMovimientoCuenta.Active then begin
		spObtenerEstacionamientosPorMovimientoCuenta.DisableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		while not spObtenerEstacionamientosPorMovimientoCuenta.eof do begin
			NumCorrEstacionamiento := spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('NumCorrEstacionamiento').Asstring;
			if not ListaEstacionamientosIn(NumCorrEstacionamiento) then begin
				ListaEstacionamientosInclude(NumCorrEstacionamiento);
				FImporteDeEstacionamientosSeleccionados := FImporteDeEstacionamientosSeleccionados + spObtenerEstacionamientosPorMovimientoCuenta.FieldByName('Importe').AsVariant;
			end;
			spObtenerEstacionamientosPorMovimientoCuenta.Next;
		end;
		spObtenerEstacionamientosPorMovimientoCuenta.EnableControls;
		spObtenerEstacionamientosPorMovimientoCuenta.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeEstacionamientosSeleccionados) div 100);
		dblEstacionamientos.Invalidate;
		dblEstacionamientos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

end.
