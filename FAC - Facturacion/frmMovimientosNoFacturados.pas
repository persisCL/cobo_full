{-------------------------------------------------------------------------------
 Revision       : 1
 Author         : Nelson Droguett Sierra
 Date           : 05-08-2009
 Description    : Muestra los movimientos no facturados sin agruparlos.

 Firma       :   SS_1120_MVI_20130820
 Descripcion :  Se cambia combo de Convenio del tipo TComboBox a TVariantComboBox.
                Se crea llamado a DrawItem para verificar si el combo del convenio
                posee alg�n convenio con el texto "(Baja)" y en caso de encontrarlo
                llamar a la funci�n que lo colorear� en rojo.
               Se cambia el llamado a llenar el combo de convenios para que utilice el procedimiento CargarConveniosRUT.
-------------------------------------------------------------------------------}

unit frmMovimientosNoFacturados;

interface

uses
	PeaProcs,
	UtilProc,
    DMConnection,
    Util,
    UtilDB,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ListBoxEx, DBListEx, ComCtrls, DmiCtrls, DB,
  ADODB, VariantComboBox, PeaTypes;                                                           //SS_1120_MVI_20130820

type
  TMovimientosNoFacturadosForm = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    btnBuscar: TButton;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    btnSalir: TButton;
    btnEliminar: TButton;
    lvMovimientos: TListView;
    chkConveniosBaja: TCheckBox;
    spObtenerConveniosClientes: TADOStoredProc;
    spObtenerMovimientosNoFacturados: TADOStoredProc;
    spEliminarMovimientosNoFacturados: TADOStoredProc;
    btnConvenios: TButton;
    edtRut: TEdit;
    cbConvenios: TVariantComboBox;                                                    //SS_1120_MVI_20130820
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
    procedure cbConveniosKeyPress(Sender: TObject; var Key: Char);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnConveniosClick(Sender: TObject);
    procedure edtRutKeyPress(Sender: TObject; var Key: Char);
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer;               //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                           //SS_1120_MVI_20130820
  private
    { Private declarations }
    FCodigoConvenio : int64;
  public
    { Public declarations }
    function Inicializar(sCaption : string) : boolean;
    function HaySeleccionados : boolean;
    function EliminarSelecionados : boolean;
    procedure BuscarConvenios;
    procedure MuestraConvenio;
  end;

var
  MovimientosNoFacturadosForm: TMovimientosNoFacturadosForm;

implementation

{$R *.dfm}

{-------------------------------------------------------------------------------
 Function GetItem
 Author : mbecerra
 Date : 08-04-2008
 Description : 	Function que recibe un string de la forma <campo1>sep<campo2>sep<campo3>
 				y devuelve el <campo i> indicado en el par�metro, o un string vac�o
                si no lo encuentra
-------------------------------------------------------------------------------}
function GetItem(Texto : string; NumeroItem : integer; Separador : Char) : string;
var
    Elemento : string;
    i, Cual : integer;
begin
    i := 1;
    Elemento := '';
    Cual := 0;
    Texto := Texto + Separador;  {para asegurarnos de encontrar el separador en el �ltimo elemento}
    while i <= Length(Texto) do begin
        if Texto[i] = Separador then begin
            Inc(Cual);
            if Cual = NumeroItem then   {es el elemento que se debe retornar}
            	i := Length(Texto) + 1
            else
                Elemento := '';         {no es el elemento que se debe retornar}

        end
        else
          Elemento := Elemento + Texto[i];

    	Inc(i);
    end;

    Result := Elemento;
end;

{ ----------------------------------------------------------------
        Inicializar
Author: mbecerra
Date: 05-Sept-2008
Description:	Inicializa el formulario
-------------------------------------------------------------------}
function TMovimientosNoFacturadosForm.Inicializar;
begin
	Result := True;
    Caption := sCaption;
    edtRut.Text := '';
    cbConvenios.Text := '';
    CenterForm(Self);
    edtRut.SetFocus;
end;

{ ----------------------------------------------------------------
        BuscarConvenios
Author: mbecerra
Date: 05-Sept-2008
Description:	Obtiene todos los convenios asociados al Rut Ingresado
-------------------------------------------------------------------}
procedure TMovimientosNoFacturadosForm.BuscarConvenios;
resourcestring
    MSG_NOHAYCODIGO = 'No se encontr� el cliente con Rut = %s';
    MSG_NOSEE = 'No se encontraron convenios asociados al Rut ingresado';
var
    CodigoCliente : integer;
begin
	{primero obtenemos el c�digo del cliente}
    CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
    					'SELECT dbo.ObtenerCodigoPersonaRUT(''' + edtRut.Text + ''')' );
    if CodigoCliente > 0 then begin
     // with spObtenerConveniosClientes do begin                                                           //SS_1120_MVI_20130820
		 //	Close;                                                                                         //SS_1120_MVI_20130820
		 //	Parameters.ParamByName('@CodigoCliente').Value := CodigoCliente;                               //SS_1120_MVI_20130820
     //		Parameters.ParamByName('@IncluirConveniosDeBaja').Value := chkConveniosBaja.Checked;           //SS_1120_MVI_20130820
     //		Open;                                                                                          //SS_1120_MVI_20130820
     //		cbConvenios.Clear;                                                                             //SS_1120_MVI_20130820
     //		while not Eof do begin                                                                         //SS_1120_MVI_20130820
     //			cbConvenios.Items.Add(FieldByName('NumeroConvenio').AsString);                             //SS_1120_MVI_20130820
     //       	Next;                                                                                      //SS_1120_MVI_20130820
     //   	end;                                                                                           //SS_1120_MVI_20130820
                                                                                                           //SS_1120_MVI_20130820
     //   	if cbConvenios.Items.Count > 0 then begin                                                      //SS_1120_MVI_20130820
     //       	cbConvenios.ItemIndex := 0;                                                                //SS_1120_MVI_20130820
     //   		cbConvenios.SetFocus;                                                                      //SS_1120_MVI_20130820
     //   	end                                                                                            //SS_1120_MVI_20130820
     //       else                                                                                         //SS_1120_MVI_20130820
     //   		MsgBox(MSG_NOSEE, Caption, MB_ICONEXCLAMATION);                                            //SS_1120_MVI_20130820
                                                                                                           //SS_1120_MVI_20130820
     //   	Close;                                                                                         //SS_1120_MVI_20130820
     //	end;                                                                                               //SS_1120_MVI_20130820
                                                                                                           //SS_1120_MVI_20130820
      CargarConveniosRUT(DMCOnnections.BaseCAC,                                                            //SS_1120_MVI_20130820
      cbConvenios, 'RUT', PadL(edtRut.Text, 9, '0' ),not(chkConveniosBaja.Checked));                       //SS_1120_MVI_20130820
    end
    else
    	MsgBox(Format(MSG_NOHAYCODIGO,[edtRut.Text]), Caption, MB_ICONEXCLAMATION );

end;

{ ----------------------------------------------------------------
        HaySeleccionados
Author: mbecerra
Date: 08-Sept-2008
Description:	Retorna verdadero si existe alg�n item seleccionado
-------------------------------------------------------------------}
function TMovimientosNoFacturadosForm.HaySeleccionados;
var
    i : integer;
begin
	Result := False;
    i := 0;
    while (not Result) and (i < lvMovimientos.Items.Count) do begin
        Result := lvMovimientos.Items[i].Checked;
        Inc(i);
    end;

end;

{ ----------------------------------------------------------------
        EliminarSeleccionados
Author: mbecerra
Date: 08-Sept-2008
Description:	Elimina los movimientos seleccionados por el usuario
-------------------------------------------------------------------}
function TMovimientosNoFacturadosForm.EliminarSelecionados;
resourcestring
    MSG_FALLO = 'No se pudo eliminar el movimiento' + CRLF + 'Operaci�n Cancelada';
    MSG_DATOS = 'Concepto [%s] NumeroMovimiento [%s]';
var
    i, j, NumeroMovimiento : integer;
    ListaDeMovimientos, Item : string;
begin
    // Rev.1 / 05-08-2009 / Nelson Droguett Sierra
    Result := True;
    i := 0;
    while Result and (i < lvMovimientos.Items.Count) do begin
        if lvMovimientos.Items[i].Checked then begin
            //ListaDeMovimientos := lvMovimientos.Items[i].SubItems[1];
            //j := 1;
            //Item := GetItem(ListaDeMovimientos, j, ',');
            //while Result and (Item <> '') do begin
                with spEliminarMovimientosNoFacturados do begin
                	Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
                    Parameters.ParamByName('@NumeroMovimiento').Value := lvMovimientos.Items[i].SubItems[2];
                    Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
                    ExecProc;
                    Result := (Parameters.ParamByName('@RETURN_VALUE').Value = 0);
                    if not Result then begin
                    	MsgBoxErr(MSG_FALLO, Format(MSG_DATOS, [lvMovimientos.Items[i].Caption, Item]), Caption, MB_ICONERROR);
                    end;
            	end;
            //	Inc(j);
            //	Item := GetItem(ListaDeMovimientos, j, ',');
            //end;
        end;
        Inc(i);
    end;
end;

{ ----------------------------------------------------------------
        cbConveniosKeyPress
Author: mbecerra
Date: 08-Sept-2008
Description:	Fuerza el ingreso de s�lo n�meros en el campo convenio
-------------------------------------------------------------------}
procedure TMovimientosNoFacturadosForm.cbConveniosKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key in ['0'..'9', Char(VK_BACK), 'k', 'K'] then begin
    	lvMovimientos.Clear;
        edtRut.Text := '';
    end
    else
        Key := #0;

end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TMovimientosNoFacturadosForm.cbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TMovimientosNoFacturadosForm.cbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;
end;
//END:SS_1120_MVI_20130820 -----------------------------------------------------

procedure TMovimientosNoFacturadosForm.edtRutKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key = #13 then begin
    	Key := #0;
        btnConvenios.Click;
    end
    else if not (Key in ['0'..'9', Char(VK_BACK), 'k', 'K']) then Key := #0;

end;

{ ----------------------------------------------------------------
        BuscarConvenios
Author: mbecerra
Date: 05-Sept-2008
Description:	Obtiene todos los convenios asociados al Rut Ingresado
-------------------------------------------------------------------}
procedure TMovimientosNoFacturadosForm.MuestraConvenio;
var
	SumaConcepto : extended;
    GlosaConcepto, NumerosMovimiento,FechaHora : string;
    CodigoConcepto,NumeroMovimiento : integer;
    Item : TListItem;
begin
    // Rev.1 / 05-08-2009 / Nelson Droguet Sierra
	lvMovimientos.Clear;
    SumaConcepto := 0;
    CodigoConcepto := -1;
    while not spObtenerMovimientosNoFacturados.Eof do begin
//        if (CodigoConcepto = spObtenerMovimientosNoFacturados.FieldByName('CodigoConcepto').AsInteger) AND
//           (NumerosMovimiento = spObtenerMovimientosNoFacturados.FieldByName('CodigoConcepto').AsString) then begin
//        	SumaConcepto := SumaConcepto + spObtenerMovimientosNoFacturados.FieldByName('Importe').AsInteger;
//            NumerosMovimiento := NumerosMovimiento + spObtenerMovimientosNoFacturados.FieldByName('NumeroMovimiento').AsString + ',';
//        end
//        else begin
//        	if CodigoConcepto >= 0 then begin  //mostrar la sumatoria
            	Item := lvMovimientos.Items.Add;
//                Item.Caption := GlosaConcepto;
//                Item.SubItems.Add(Format('%.0n', [SumaConcepto / 100]));
                Item.Caption := spObtenerMovimientosNoFacturados.FieldByName('Descripcion').AsString;
                Item.SubItems.Add(Format('%.0n', [spObtenerMovimientosNoFacturados.FieldByName('Importe').AsInteger / 100]));
                Item.SubItems.Add(spObtenerMovimientosNoFacturados.FieldByName('FechaHora').AsString);
                Item.SubItems.Add(spObtenerMovimientosNoFacturados.FieldByName('NumeroMovimiento').AsString);
                Item.SubItems.Add(NumerosMovimiento);
//            end;
//            GlosaConcepto  := spObtenerMovimientosNoFacturados.FieldByName('Descripcion').AsString;
//            CodigoConcepto := spObtenerMovimientosNoFacturados.FieldByName('CodigoConcepto').AsInteger;
//            SumaConcepto   := spObtenerMovimientosNoFacturados.FieldByName('Importe').AsInteger;
//            NumerosMovimiento := spObtenerMovimientosNoFacturados.FieldByName('NumeroMovimiento').AsString + ',';
//            NumeroMovimiento := spObtenerMovimientosNoFacturados.FieldByName('NumeroMovimiento').AsInteger;
//            FechaHora:= spObtenerMovimientosNoFacturados.FieldByName('FechaHora').AsString;
//        end;
        spObtenerMovimientosNoFacturados.Next;
    end;

    {al final, siempre queda al menos un registro}
//    Item := lvMovimientos.Items.Add;
//    Item.Caption := GlosaConcepto;
//    Item.SubItems.Add(Format('%.0n', [SumaConcepto /100]));
//    Item.Caption := spObtenerMovimientosNoFacturados.FieldByName('Descripcion').AsString;
//    Item.SubItems.Add(Format('%.0n', [spObtenerMovimientosNoFacturados.FieldByName('Importe').AsInteger / 100]));
//    Item.SubItems.Add(spObtenerMovimientosNoFacturados.FieldByName('FechaHora').AsString);
//    Item.SubItems.Add(spObtenerMovimientosNoFacturados.FieldByName('NumeroMovimiento').AsString);
//    Item.SubItems.Add(NumerosMovimiento);
end;


{ ----------------------------------------------------------------
        btnBuscarClick
Author: mbecerra
Date: 05-Sept-2008
Description:	Obtiene los movimientos asociados al Convenio ingresado
-------------------------------------------------------------------}
procedure TMovimientosNoFacturadosForm.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_CONVENIO	= 'Debe ingresar un convenio';
    MSG_SINCONVENIO = 'No existe un convenio con el n�mero de convenio indicado';
    MSG_SINMOVI		= 'No se encontraron movimientos No Facturados para el convenio indicado';
    MSG_ERROR		= 'Ocurri� un error al obtener los movimientos';

begin
	if ValidateControls([cbConvenios], [cbConvenios.Text <> ''], Caption, [MSG_CONVENIO]) then begin
        lvMovimientos.Clear;
        spObtenerMovimientosNoFacturados.Close;
        try
        	{primero buscar el c�digo del convenio}
//            FCodigoconvenio := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCodigoConvenio(''' + cbConvenios.Text + ''' )');      //SS_1120_MVI_20130820
            FCodigoconvenio := cbConvenios.Value;                                                                                                 //SS_1120_MVI_20130820
            if FCodigoConvenio > 0 then begin
        		spObtenerMovimientosNoFacturados.Close;
        		spObtenerMovimientosNoFacturados.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        		spObtenerMovimientosNoFacturados.Open;
        		if not spObtenerMovimientosNoFacturados.IsEmpty then begin
        			Screen.Cursor := crHourGlass;
            		MuestraConvenio();
            		Screen.Cursor := crDefault;
        		end
        		else
                    MsgBox(MSG_SINMOVI, Caption, MB_ICONEXCLAMATION);
            end
            else
            	MsgBox(MSG_SINCONVENIO, Caption, MB_ICONEXCLAMATION);

        except on e:exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
        	end;

        end;
    end;

end;

procedure TMovimientosNoFacturadosForm.btnConveniosClick(Sender: TObject);
resourcestring
	MSG_NORUT = 'Debe ingresar un Rut para buscar los convenios asociados';

begin
	if ValidateControls([edtRut],[edtRut.Text <> ''], Caption, [MSG_NORUT]) then begin
        lvMovimientos.Clear;
        cbConvenios.Clear;
    	BuscarConvenios();
    end;
end;

procedure TMovimientosNoFacturadosForm.btnEliminarClick(Sender: TObject);
resourcestring
	MSG_NOHAY = 'No hay movimientos seleccionados para eliminar';
    MSG_ERROR = 'Ocurri� un error al intentar eliminar los movimientos no facturados';
    MSG_EXITO = 'Movimientos eliminados exitosamente';

begin
    if HaySeleccionados() then begin
    	try
        	DMConnections.BaseCAC.BeginTrans;
        	if EliminarSelecionados() then begin
            	DMConnections.BaseCAC.CommitTrans;
                MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
                {refrescamos los datos}
                lvMovimientos.Clear;
                spObtenerMovimientosNoFacturados.Close;
                spObtenerMovimientosNoFacturados.Open;
                if not spObtenerMovimientosNoFacturados.IsEmpty then MuestraConvenio();
                
            end
            else DMConnections.BaseCAC.RollbackTrans;

        except on e:exception do begin
            	if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
        	end;

        end;
    end
    else MsgBox(MSG_NOHAY, Caption, MB_ICONEXCLAMATION);

end;

procedure TMovimientosNoFacturadosForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TMovimientosNoFacturadosForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

end.
