object FormSeleccionarMes: TFormSeleccionarMes
  Left = 300
  Top = 215
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Seleccionar Mes'
  ClientHeight = 193
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 4
    Top = 4
    Width = 393
    Height = 153
  end
  object lbl_Titulo: TLabel
    Left = 16
    Top = 21
    Width = 370
    Height = 20
    Alignment = taCenter
    AutoSize = False
    Caption = 
      'Por favor, seleccione el mes a partir del cual generar los ciclo' +
      's de facturaci'#243'n.'
    WordWrap = True
  end
  object cb_Mes: TVariantComboBox
    Left = 128
    Top = 56
    Width = 145
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  object btn_Aceptar: TButton
    Left = 246
    Top = 162
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    TabOrder = 1
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TButton
    Left = 322
    Top = 162
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = btn_CancelarClick
  end
end
