object IncrementoCuotaArriendoForm: TIncrementoCuotaArriendoForm
  Left = 0
  Top = 0
  Caption = 'Incremento de Cuotas en Documentos Arriendo'
  ClientHeight = 613
  ClientWidth = 691
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 691
    Height = 201
    Align = alTop
    TabOrder = 0
    object lblImpresoraFiscal: TLabel
      Left = 260
      Top = 14
      Width = 94
      Height = 13
      Caption = 'Impresora Fiscal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNumeroBoleta: TLabel
      Left = 55
      Top = 14
      Width = 100
      Height = 13
      Caption = 'Numero de Boleta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCantCuotasAgrega: TLabel
      Left = 211
      Top = 167
      Width = 145
      Height = 13
      Caption = 'Cantidad de Cuotas a agregar'
    end
    object lblApellido: TLabel
      Left = 17
      Top = 47
      Width = 45
      Height = 13
      Caption = 'Apellido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 17
      Top = 61
      Width = 44
      Height = 13
      Caption = 'Nombre'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNumeroConvenio: TLabel
      Left = 17
      Top = 91
      Width = 52
      Height = 13
      Caption = 'Convenio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRUT: TLabel
      Left = 17
      Top = 76
      Width = 23
      Height = 13
      Caption = 'RUT'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTotalComprobante: TLabel
      Left = 400
      Top = 51
      Width = 109
      Height = 13
      Caption = 'Total Comprobante'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCantCuotasTotales: TLabel
      Left = 400
      Top = 66
      Width = 71
      Height = 13
      Caption = 'Cuotas Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCuotasGeneradas: TLabel
      Left = 400
      Top = 81
      Width = 102
      Height = 13
      Caption = 'Cuotas generadas'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTag: TLabel
      Left = 400
      Top = 96
      Width = 41
      Height = 13
      Caption = 'Telev'#237'a'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblUsuario: TLabel
      Left = 400
      Top = 111
      Width = 46
      Height = 13
      Caption = 'Usuario:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblApellidoTxt: TLabel
      Left = 95
      Top = 47
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblNombreTxt: TLabel
      Left = 95
      Top = 61
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblConvenioTxt: TLabel
      Left = 93
      Top = 91
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblRutTxt: TLabel
      Left = 95
      Top = 76
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblTotalComprobanteTxt: TLabel
      Left = 523
      Top = 51
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblCuotasTotalTxt: TLabel
      Left = 523
      Top = 66
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblCuotasGeneradasTxt: TLabel
      Left = 523
      Top = 81
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblTeleviaTxt: TLabel
      Left = 523
      Top = 96
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblUsuarioTxt: TLabel
      Left = 473
      Top = 111
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblFechaCreacionTxt: TLabel
      Left = 554
      Top = 111
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblFechaEmision: TLabel
      Left = 17
      Top = 106
      Width = 43
      Height = 13
      Caption = 'Emisi'#243'n'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFechaVencimiento: TLabel
      Left = 17
      Top = 122
      Width = 70
      Height = 13
      Caption = 'Vencimiento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFechaEmisionTxt: TLabel
      Left = 94
      Top = 106
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object lblFechaVencimientoTxt: TLabel
      Left = 94
      Top = 122
      Width = 63
      Height = 13
      Caption = 'lblApellidoTxt'
    end
    object cbImpresorasFiscales: TVariantComboBox
      Left = 367
      Top = 11
      Width = 169
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object edNumeroFiscalBoleta: TNumericEdit
      Left = 160
      Top = 11
      Width = 86
      Height = 21
      TabOrder = 0
    end
    object btnBuscar: TButton
      Left = 572
      Top = 9
      Width = 97
      Height = 25
      Caption = 'Buscar'
      TabOrder = 2
      OnClick = btnBuscarClick
    end
    object edCantidadCuotasAgregar: TNumericEdit
      Left = 367
      Top = 164
      Width = 34
      Height = 21
      MaxLength = 2
      TabOrder = 3
    end
    object btnAgregarCuotas: TButton
      Left = 440
      Top = 162
      Width = 137
      Height = 25
      Caption = 'Agregar Cuotas'
      TabOrder = 4
      OnClick = btnAgregarCuotasClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 201
    Width = 691
    Height = 370
    Align = alClient
    TabOrder = 1
    object dblDetalleCuotas: TDBListEx
      Left = 1
      Top = 1
      Width = 689
      Height = 368
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'N'#250'mero Cuota'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'NumeroCuota'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Fecha Emisi'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaEmisionCuota'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Fecha Vencimiento'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaVencimientoCuota'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 140
          Header.Caption = 'Fecha Generaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaGeneracionCuota'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Usuario'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'UsuarioGeneracionCuota'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Importe Cuota'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'ImporteCuota'
        end>
      DataSource = dsDetalleCuotas
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 571
    Width = 691
    Height = 42
    Align = alBottom
    TabOrder = 2
    object btnSalir: TButton
      Left = 582
      Top = 8
      Width = 96
      Height = 25
      Cancel = True
      Caption = '&Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object spObtenerDetalleComprobanteCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleCuotas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@IdComprobanteCuota'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 6
      end>
    Left = 520
    Top = 392
    object spObtenerDetalleComprobanteCuotasIdComprobanteCuota: TLargeintField
      FieldName = 'IdComprobanteCuota'
    end
    object spObtenerDetalleComprobanteCuotasNumeroCuota: TIntegerField
      FieldName = 'NumeroCuota'
    end
    object spObtenerDetalleComprobanteCuotasTipoComprobanteCuota: TStringField
      FieldName = 'TipoComprobanteCuota'
      FixedChar = True
      Size = 2
    end
    object spObtenerDetalleComprobanteCuotasNumeroComprobanteCuota: TLargeintField
      FieldName = 'NumeroComprobanteCuota'
    end
    object spObtenerDetalleComprobanteCuotasFechaEmisionCuota: TDateTimeField
      FieldName = 'FechaEmisionCuota'
    end
    object spObtenerDetalleComprobanteCuotasFechaVencimientoCuota: TDateTimeField
      FieldName = 'FechaVencimientoCuota'
    end
    object spObtenerDetalleComprobanteCuotasFechaGeneracionCuota: TDateTimeField
      FieldName = 'FechaGeneracionCuota'
    end
    object spObtenerDetalleComprobanteCuotasUsuarioGeneracionCuota: TStringField
      FieldName = 'UsuarioGeneracionCuota'
      FixedChar = True
    end
    object spObtenerDetalleComprobanteCuotasModoGeneracionCuota: TIntegerField
      FieldName = 'ModoGeneracionCuota'
    end
    object spObtenerDetalleComprobanteCuotasImporteCuota: TStringField
      FieldName = 'ImporteCuota'
    end
    object spObtenerDetalleComprobanteCuotasFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object spObtenerDetalleComprobanteCuotasUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object spObtenerDetalleComprobanteCuotasFechaHoraModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
    object spObtenerDetalleComprobanteCuotasUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
      FixedChar = True
    end
  end
  object spObtenerCabeceraComprobanteCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCabeceraComprobanteCuotas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoImpresoraFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 520
    Top = 352
  end
  object spComprobantesCuotasCrearCuota: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ComprobantesCuotasCrearCuota;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobanteDeuda'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobanteDeuda'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CrearObligatorio'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioCreacionCuota'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 520
    Top = 312
  end
  object dsDetalleCuotas: TDataSource
    DataSet = spObtenerDetalleComprobanteCuotas
    Left = 304
    Top = 264
  end
end
