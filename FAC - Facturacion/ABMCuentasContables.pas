{

Etiqueta    :   TASK_049_MGO_20160719
Descripci�n :   Se muestra mensaje cuando ya existe la cuenta contable

}

unit ABMCuentasContables;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB,DMConnection,
  DPSControls;

type
  TFormCuentasContables = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    CuentasContables: TADOTable;
    GroupB: TPanel;
    lblNombre: TLabel;
    Label15: TLabel;
    edNombre: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
    Label1: TLabel;
    neCodigoCuentaContable: TNumericEdit;
    neCodigoExternoCuentaContable: TNumericEdit;
    CuentasContablesCodigoCuentaContable: TAutoIncField;
    CuentasContablesCodigoExternoCuentaContable: TLargeintField;
    CuentasContablesNombre: TStringField;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    CuentasContablesFechaHoraCreacion: TDateTimeField;
    CuentasContablesUsuarioCreacion: TStringField;
    CuentasContablesFechaHoraActualizacion: TDateTimeField;
    CuentasContablesUsuarioActualizacion: TStringField;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializar : boolean;
  end;

var
  FormCuentasContables  : TFormCuentasContables;

implementation

resourcestring
	MSG_DELETE_QUESTION		= 'Est seguro de querer eliminar la Cuenta Contable?';
    MSG_DELETE_ERROR		= 'No se puede eliminar la Cuenta Contable porque hay datos que dependendientes.';
    MSG_DELETE_CAPTION 		= 'Eliminar la Cuenta Contable';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar la Cuenta Contable.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar la Cuenta Contable';
    MSG_DESCRIPCION         = 'Debe ingresar un nombre de cuenta';

{$R *.DFM}

function TFormCuentasContables.Inicializar: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([CuentasContables]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;

end;

procedure TFormCuentasContables.Limpiar_Campos();
begin
    neCodigoCuentaContable.Value := 0;
    neCodigoExternoCuentaContable.Value := 0;
    edNombre.Clear;

end;

function TFormCuentasContables.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
    //INICIO	: 20160614 CFU
	//Texto :=  Tabla.FieldByName('CodigoCuentaContable').AsString ;
	Texto	:=  PadR(Trim(Tabla.FieldByName('CodigoCuentaContable').AsString), 4, ' ' ) + ' '+
	  			Tabla.FieldByName('Nombre').AsString;
    //TERMINO   : 20160614 CFU
end;

procedure TFormCuentasContables.BtSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormCuentasContables.ListaInsert(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Alta;
	Limpiar_Campos;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
    neCodigoExternoCuentaContable.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormCuentasContables.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
    neCodigoExternoCuentaContable.SetFocus;
	Screen.Cursor    := crDefault;

end;

procedure TFormCuentasContables.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			CuentasContables.Delete;
		Except
			On E: EDataBaseError do begin
				CuentasContables.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormCuentasContables.ListaClick(Sender: TObject);
begin
     neCodigoCuentaContable.Value        := CuentasContables.FieldByName('CodigoCuentaContable').AsInteger;
     neCodigoExternoCuentaContable.Value := CuentasContables.FieldByName('CodigoExternoCuentaContable').AsInteger;
     edNombre.Text                       := CuentasContables.FieldByName('Nombre').AsString;
end;

procedure TFormCuentasContables.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormCuentasContables.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormCuentasContables.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, CuentasContables.FieldByName('CodigoCuentaContable').AsString);
		TextOut(Cols[1], Rect.Top, CuentasContables.FieldByName('CodigoExternoCuentaContable').AsString);
		TextOut(Cols[2], Rect.Top, CuentasContables.FieldByName('Nombre').AsString);
	end;
end;

procedure TFormCuentasContables.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormCuentasContables.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CUENTA_VACIA = 'N�mero de cuenta vac�o';
    MSG_CUENTA_EXTERNA_VACIA = 'N�mero de cuenta externa vac�o';
    MSG_CUENTA_EXISTENTE = 'Ya existe una cuenta contable con ese c�digo o n�mero de cuenta externa';     // TASK_049_MGO_20160719
begin
    if not ValidateControls([edNombre,  neCodigoExternoCuentaContable],
                [trim(edNombre.text) <> '', neCodigoExternoCuentaContable.ValueInt > 0],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION, MSG_CUENTA_EXTERNA_VACIA]) then exit;
	Screen.Cursor := crHourGlass;
	With CuentasContables do begin
		Try
			if Lista.Estado = Alta then Append else Edit;

			FieldByName('CodigoExternoCuentaContable').AsInteger  := neCodigoExternoCuentaContable.ValueInt;
			FieldByName('Nombre').AsString 	  := edNombre.Text;

            if Lista.Estado = Alta then begin
              FieldByName('UsuarioCreacion').AsString 	  := UsuarioSistema;
              FieldByName('FechaHoraCreacion').AsDateTime := NowBase(CuentasContables.Connection);
            end;

			FieldByName('UsuarioActualizacion').AsString 	  := UsuarioSistema;
			FieldByName('FechaHoraActualizacion').AsDateTime := NowBase(CuentasContables.Connection);
			Post;
            neCodigoCuentaContable.ValueInt := FieldByName('CodigoCuentaContable').AsInteger;
		except
			On E: EDataBaseError do begin
				Cancel;
                { INICIO : TASK_049_MGO_20160719
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                }
                MsgBox(MSG_CUENTA_EXISTENTE, MSG_ACTUALIZAR_CAPTION, MB_ICONEXCLAMATION+MB_OK);
                // FIN : TASK_049_MGO_20160719
			end;
		end;
	end;
	Lista.Estado     	:= Normal;
	Lista.Enabled    	:= True;
	GroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormCuentasContables.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormCuentasContables.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormCuentasContables.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
