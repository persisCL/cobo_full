object frm_AnulacionNotasCobro: Tfrm_AnulacionNotasCobro
  Left = 199
  Top = 214
  BorderStyle = bsDialog
  Caption = 'Generar Nota de Cr'#233'dito  (Anular Nota de Cobro o Boleta)'
  ClientHeight = 594
  ClientWidth = 876
  Color = clBtnFace
  Constraints.MinHeight = 409
  Constraints.MinWidth = 671
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  DesignSize = (
    876
    594)
  PixelsPerInch = 96
  TextHeight = 13
  object gbBusqueda: TGroupBox
    Left = 3
    Top = 2
    Width = 865
    Height = 103
    Anchors = [akLeft, akTop, akRight]
    Caption = ' B'#250'squeda Comprobante '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lblNumeroComprobante: TLabel
      Left = 201
      Top = 16
      Width = 118
      Height = 13
      Caption = 'N'#250'mero de Comprobante'
    end
    object lblTipoComprobante: TLabel
      Left = 18
      Top = 16
      Width = 102
      Height = 13
      Caption = 'Tipo de Comprobante'
    end
    object lblImpresoraFiscal: TLabel
      Left = 537
      Top = 16
      Width = 76
      Height = 13
      Caption = 'Impresora Fiscal'
    end
    object lblNumeroComprobanteFiscal: TLabel
      Left = 370
      Top = 16
      Width = 148
      Height = 13
      Caption = 'N'#250'mero de Comprobante Fiscal'
    end
    object lblNumeroNotaCreditoFiscal: TLabel
      Left = 369
      Top = 57
      Width = 201
      Height = 13
      Caption = 'N'#250'mero Fiscal de Nota de Cr'#233'dito a Boleta'
      Visible = False
    end
    object lblMotivoAnulacion: TLabel
      Left = 18
      Top = 57
      Width = 94
      Height = 13
      Caption = 'Motivo Nota Cr'#233'dito'
    end
    object edNumeroComprobante: TNumericEdit
      Left = 199
      Top = 30
      Width = 122
      Height = 21
      TabOrder = 1
    end
    object cbTipoComprobante: TVariantComboBox
      Left = 17
      Top = 30
      Width = 169
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTipoComprobanteChange
      Items = <>
    end
    object btnBuscar: TButton
      Left = 748
      Top = 28
      Width = 89
      Height = 25
      Caption = 'Buscar'
      TabOrder = 5
      OnClick = btnBuscarClick
    end
    object cbImpresorasFiscales: TVariantComboBox
      Left = 537
      Top = 30
      Width = 169
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 3
      Items = <>
    end
    object edNumeroComprobanteFiscal: TNumericEdit
      Left = 369
      Top = 30
      Width = 149
      Height = 21
      TabOrder = 2
    end
    object edNumeroNotaCreditoFiscal: TNumericEdit
      Left = 370
      Top = 76
      Width = 149
      Height = 21
      TabOrder = 4
      Visible = False
    end
    object cbbMotivoNotaCredito: TVariantComboBox
      Left = 17
      Top = 76
      Width = 169
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 6
      Items = <>
    end
  end
  object gbDatosComprobante: TGroupBox
    Left = 3
    Top = 111
    Width = 865
    Height = 86
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos del Comprobante Seleccionado'
    TabOrder = 1
    DesignSize = (
      865
      86)
    object Label6: TLabel
      Left = 16
      Top = 18
      Width = 48
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 73
      Top = 18
      Width = 506
      Height = 13
      AutoSize = False
      Caption = '*-----------------'
    end
    object lblDomicilio: TLabel
      Left = 78
      Top = 33
      Width = 501
      Height = 13
      AutoSize = False
      Caption = '*--------'
    end
    object Label10: TLabel
      Left = 16
      Top = 33
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 49
      Width = 105
      Height = 13
      Caption = 'Fecha de Emisi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 63
      Width = 131
      Height = 13
      Caption = 'Fecha de Vencimiento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 292
      Top = 49
      Width = 112
      Height = 13
      Caption = 'Total Comprobante:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTotalAPagar: TLabel
      Left = 419
      Top = 49
      Width = 77
      Height = 13
      AutoSize = False
      Caption = '*-------------'
    end
    object Label11: TLabel
      Left = 292
      Top = 63
      Width = 122
      Height = 13
      Caption = 'Estado Comprobante:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblEstado: TLabel
      Left = 419
      Top = 63
      Width = 43
      Height = 13
      Caption = '*-------------'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblFechaVencimiento: TLabel
      Left = 153
      Top = 63
      Width = 52
      Height = 13
      Caption = '*----------------'
    end
    object lblFecha: TLabel
      Left = 153
      Top = 49
      Width = 43
      Height = 13
      Caption = '*-------------'
    end
    object lblTotalCKsPreviasANKText: TLabel
      Left = 595
      Top = 49
      Width = 129
      Height = 13
      Caption = 'Total Cr'#233'ditos previos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblTotalCKsPreviasANK: TLabel
      Left = 772
      Top = 49
      Width = 28
      Height = 13
      Caption = '*--------'
      Visible = False
    end
    object lblMaximoTotalComprobanteCKText: TLabel
      Left = 595
      Top = 63
      Width = 124
      Height = 13
      Caption = 'M'#225'ximo Total Cr'#233'dito:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblMaximoTotalComprobanteCK: TLabel
      Left = 772
      Top = 63
      Width = 28
      Height = 13
      Caption = '*--------'
      Visible = False
    end
    object Label1: TLabel
      Left = 595
      Top = 18
      Width = 31
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblValorRUT: TLabel
      Left = 669
      Top = 18
      Width = 43
      Height = 13
      Caption = '*-------------'
    end
    object Label4: TLabel
      Left = 595
      Top = 33
      Width = 58
      Height = 13
      Caption = 'Convenio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblValorNumeroConvenio: TLabel
      Left = 669
      Top = 33
      Width = 185
      Height = 13
      AutoSize = False
      Caption = '*-------------'
    end
    object lblEnListaAmarilla: TLabel
      Left = 576
      Top = 0
      Width = 200
      Height = 14
      Alignment = taCenter
      AutoSize = False
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
  end
  object btnGenerarOAnular: TButton
    Left = 8
    Top = 561
    Width = 196
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Generar Nota Cr'#233'dito (F9)'
    Default = True
    TabOrder = 6
    OnClick = btnGenerarOAnularClick
  end
  object btnSalir: TButton
    Left = 781
    Top = 557
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 7
    OnClick = btnSalirClick
  end
  object gbDocumentoOriginal: TGroupBox
    Left = 3
    Top = 203
    Width = 865
    Height = 158
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Documento Original'
    TabOrder = 2
    object dblDocumentoOriginal: TDBListEx
      Left = 8
      Top = 16
      Width = 753
      Height = 129
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Transferir'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Seleccionado'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -13
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Concepto'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescriImporte'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Valor Neto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescriValorNeto'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Importe IVA'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescriImporteIVA'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 180
          Header.Caption = 'Descripci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Observaciones'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Observaciones'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Afecto IVA'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'AfectoIVA'
        end>
      DataSource = dsOriginal
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dblDocumentoOriginalDblClick
      OnDrawText = dblDocumentoOriginalDrawText
    end
  end
  object gbNotaCredito: TGroupBox
    Left = 3
    Top = 362
    Width = 865
    Height = 183
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Nueva Nota de Cr'#233'dito'
    TabOrder = 5
    object lblTotalNotaCredito: TLabel
      Left = 72
      Top = 161
      Width = 135
      Height = 13
      Caption = 'Total Nota de Cr'#233'dito : '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblImporteTotalNotaCredito: TLabel
      Left = 210
      Top = 161
      Width = 6
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dblNotaCredito: TDBListEx
      Left = 8
      Top = 16
      Width = 753
      Height = 141
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'CodigoConcepto'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescriImporte'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Valor Neto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescriValorNeto'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Importe IVA'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescriImporteIVA'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 180
          Header.Caption = 'Descripci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Observaciones'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Observaciones'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Acredita Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'NroOriginal'
        end>
      DataSource = dsCredito
      DragReorder = True
      ParentColor = False
      TabOrder = 3
      TabStop = True
    end
    object btnModificarConcepto: TButton
      Left = 767
      Top = 83
      Width = 89
      Height = 29
      Caption = 'Modificar'
      TabOrder = 2
      OnClick = btnModificarConceptoClick
    end
    object btnEliminarConcepto: TButton
      Left = 766
      Top = 48
      Width = 89
      Height = 29
      Caption = 'Eliminar'
      TabOrder = 1
      OnClick = btnEliminarConceptoClick
    end
    object btnAgregarNuevoConcepto: TButton
      Left = 766
      Top = 16
      Width = 89
      Height = 29
      Caption = 'Agregar Nuevo'
      TabOrder = 0
      OnClick = btnAgregarNuevoConceptoClick
    end
    object chkPasarTransitoEstadoEspecial: TCheckBox
      Left = 424
      Top = 159
      Width = 313
      Height = 17
      Caption = 'Pasar los tr'#225'nsitos a estado especial por error en Patente'
      Enabled = False
      TabOrder = 4
      Visible = False
    end
  end
  object btnPasarConcepto: TButton
    Left = 770
    Top = 245
    Width = 89
    Height = 29
    Caption = 'Pasar Concepto'
    TabOrder = 3
    OnClick = btnPasarConceptoClick
  end
  object btnPasarTodos: TButton
    Left = 770
    Top = 280
    Width = 89
    Height = 29
    Caption = 'Pasar Todos'
    TabOrder = 4
    OnClick = btnPasarTodosClick
  end
  object spAnularNotaCobro: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'AnularComprobante;1'
    Parameters = <>
    Left = 443
    Top = 490
  end
  object spComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEncabezadoComprobanteParaCredito;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoImpresoraFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 448
    object spComprobantesNumeroConvenioFormateado: TStringField
      FieldName = 'NumeroConvenioFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object spComprobantesNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object spComprobantesCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object spComprobantesTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object spComprobantesNumeroComprobante: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object spComprobantesPeriodoInicial: TDateTimeField
      FieldName = 'PeriodoInicial'
      ReadOnly = True
    end
    object spComprobantesPeriodoFinal: TDateTimeField
      FieldName = 'PeriodoFinal'
      ReadOnly = True
    end
    object spComprobantesFechaEmision: TDateTimeField
      FieldName = 'FechaEmision'
      ReadOnly = True
    end
    object spComprobantesFechaVencimiento: TDateTimeField
      FieldName = 'FechaVencimiento'
      ReadOnly = True
    end
    object spComprobantesNombreCliente: TStringField
      FieldName = 'NombreCliente'
      ReadOnly = True
      Size = 152
    end
    object spComprobantesDomicilio: TStringField
      FieldName = 'Domicilio'
      ReadOnly = True
      Size = 162
    end
    object spComprobantesComuna: TStringField
      FieldName = 'Comuna'
      ReadOnly = True
      Size = 203
    end
    object spComprobantesComentarios: TStringField
      FieldName = 'Comentarios'
      Size = 100
    end
    object spComprobantesCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      ReadOnly = True
      Size = 10
    end
    object spComprobantesSaldoPendiente: TStringField
      FieldName = 'SaldoPendiente'
      ReadOnly = True
    end
    object spComprobantesAjusteSencilloAnterior: TStringField
      FieldName = 'AjusteSencilloAnterior'
      ReadOnly = True
    end
    object spComprobantesAjusteSencilloActual: TStringField
      FieldName = 'AjusteSencilloActual'
      ReadOnly = True
    end
    object spComprobantesTotalAPagar: TStringField
      FieldName = 'TotalAPagar'
      ReadOnly = True
    end
    object spComprobantesCodigoTipoMedioPago: TWordField
      FieldName = 'CodigoTipoMedioPago'
    end
    object spComprobantesEstadoPago: TStringField
      FieldName = 'EstadoPago'
      FixedChar = True
      Size = 1
    end
    object spComprobantesNumeroComprobanteFiscal: TLargeintField
      FieldName = 'NumeroComprobanteFiscal'
    end
    object spComprobantesNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object spComprobantesTieneConceptosInfractor: TWordField
      FieldName = 'TieneConceptosInfractor'
      ReadOnly = True
    end
  end
  object cdsOriginal: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Concepto'
        DataType = ftInteger
      end
      item
        Name = 'Importe'
        DataType = ftFloat
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescriImporte'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroLinea'
        DataType = ftInteger
      end
      item
        Name = 'Pasado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'NumeroMovimiento'
        DataType = ftInteger
      end
      item
        Name = 'IndiceVehiculo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ConceptoEspecial'
        DataType = ftInteger
      end
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'AfectoIVA'
        DataType = ftBoolean
      end
      item
        Name = 'ValorNeto'
        DataType = ftFloat
      end
      item
        Name = 'ImporteIVA'
        DataType = ftFloat
      end
      item
        Name = 'DescriValorNeto'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescriImporteIVA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PorcentajeIVA'
        DataType = ftFloat
      end
      item
        Name = 'DescriPorcentajeIVA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    IndexFieldNames = 'NumeroLinea'
    Params = <>
    StoreDefs = True
    Left = 169
    Top = 240
    Data = {
      280200009619E0BD010000001800000013000000000003000000280208436F6E
      636570746F040001000000000007496D706F72746508000400000000000B4465
      736372697063696F6E0100490000000100055749445448020002003C000D4465
      73637269496D706F72746501004900000001000557494454480200020014000B
      4E756D65726F4C696E656104000100000000000650617361646F010049000000
      01000557494454480200020014000D4F62736572766163696F6E657301004900
      00000100055749445448020002003C00104E756D65726F4D6F76696D69656E74
      6F04000100000000000E496E646963655665686963756C6F0100490000000100
      05574944544802000200140010436F6E636570746F457370656369616C040001
      00000000000C53656C656363696F6E61646F0200030000000000094166656374
      6F49564102000300000000000956616C6F724E65746F08000400000000000A49
      6D706F72746549564108000400000000000F44657363726956616C6F724E6574
      6F010049000000010005574944544802000200140010446573637269496D706F
      72746549564101004900000001000557494454480200020014000D506F726365
      6E74616A65495641080004000000000013446573637269506F7263656E74616A
      65495641010049000000010005574944544802000200140013436F6469676F43
      6F6E636573696F6E61726961020001000000000001000D44454641554C545F4F
      524445520200820000000000}
    object cdsOriginalConcepto: TIntegerField
      FieldName = 'Concepto'
    end
    object cdsOriginalImporte: TFloatField
      FieldName = 'Importe'
    end
    object cdsOriginalDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 60
    end
    object cdsOriginalDescriImporte: TStringField
      FieldName = 'DescriImporte'
    end
    object cdsOriginalNumeroLinea: TIntegerField
      FieldName = 'NumeroLinea'
    end
    object cdsOriginalPasado: TStringField
      FieldName = 'Pasado'
    end
    object cdsOriginalObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 60
    end
    object cdsOriginalNumeroMovimiento: TIntegerField
      FieldName = 'NumeroMovimiento'
    end
    object cdsOriginalIndiceVehiculo: TStringField
      FieldName = 'IndiceVehiculo'
    end
    object cdsOriginalConceptoEspecial: TIntegerField
      FieldName = 'ConceptoEspecial'
    end
    object cdsOriginalSeleccionado: TBooleanField
      FieldName = 'Seleccionado'
    end
    object cdsOriginalAfectoIVA: TBooleanField
      FieldName = 'AfectoIVA'
    end
    object cdsOriginalValorNeto: TFloatField
      FieldName = 'ValorNeto'
    end
    object cdsOriginalImporteIVA: TFloatField
      FieldName = 'ImporteIVA'
    end
    object cdsOriginalDescriValorNeto: TStringField
      FieldName = 'DescriValorNeto'
    end
    object cdsOriginalDescriImporteIVA: TStringField
      FieldName = 'DescriImporteIVA'
    end
    object cdsOriginalPorcentajeIVA: TFloatField
      FieldName = 'PorcentajeIVA'
    end
    object cdsOriginalDescriPorcentajeIVA: TStringField
      FieldName = 'DescriPorcentajeIVA'
    end
    object cdsOriginalCodigoConcesionaria: TSmallintField
      FieldName = 'CodigoConcesionaria'
    end
  end
  object cdsCredito: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'Importe'
        DataType = ftFloat
      end
      item
        Name = 'ValorNeto'
        DataType = ftFloat
      end
      item
        Name = 'ImporteIVA'
        DataType = ftFloat
      end
      item
        Name = 'PorcentajeIVA'
        DataType = ftFloat
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'NroOriginal'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescriImporte'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescriValorNeto'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescriImporteIVA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ImporteOriginal'
        DataType = ftFloat
      end
      item
        Name = 'NumeroMovimiento'
        DataType = ftInteger
      end
      item
        Name = 'CodigoConceptoOriginal'
        DataType = ftInteger
      end
      item
        Name = 'IndiceVehiculo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescriPorcentajeIVA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftSmallint
      end
      item
        Name = 'SaldoDisponible'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    IndexFieldNames = 'NroOriginal'
    Params = <>
    StoreDefs = True
    Left = 312
    Top = 440
    Data = {
      390200009619E0BD01000000180000001300000000000300000039020E436F64
      69676F436F6E636570746F04000100000000000E436F6469676F436F6E76656E
      696F040001000000000007496D706F72746508000400000000000956616C6F72
      4E65746F08000400000000000A496D706F72746549564108000400000000000D
      506F7263656E74616A6549564108000400000000000D4F62736572766163696F
      6E65730100490000000100055749445448020002003C000B4E726F4F72696769
      6E616C04000100000000000B4465736372697063696F6E010049000000010005
      5749445448020002003C000D446573637269496D706F72746501004900000001
      000557494454480200020014000F44657363726956616C6F724E65746F010049
      000000010005574944544802000200140010446573637269496D706F72746549
      564101004900000001000557494454480200020014000F496D706F7274654F72
      6967696E616C0800040000000000104E756D65726F4D6F76696D69656E746F04
      0001000000000016436F6469676F436F6E636570746F4F726967696E616C0400
      0100000000000E496E646963655665686963756C6F0100490000000100055749
      44544802000200140013446573637269506F7263656E74616A65495641010049
      000000010005574944544802000200140013436F6469676F436F6E636573696F
      6E6172696102000100000000000F53616C646F446973706F6E69626C65080004
      000000000001000D44454641554C545F4F524445520200820000000000}
    object cdsCreditoCodigoConcepto: TIntegerField
      FieldName = 'CodigoConcepto'
    end
    object cdsCreditoCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsCreditoImporte: TFloatField
      FieldName = 'Importe'
    end
    object cdsCreditoObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 60
    end
    object cdsCreditoNroOriginal: TIntegerField
      FieldName = 'NroOriginal'
    end
    object cdsCreditoDescriImporte: TStringField
      FieldName = 'DescriImporte'
    end
    object cdsCreditoDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 60
    end
    object cdsCreditoImporteOriginal: TFloatField
      FieldName = 'ImporteOriginal'
    end
    object cdsCreditoNumeroMovimiento: TIntegerField
      FieldName = 'NumeroMovimiento'
    end
    object cdsCreditoCodigoConceptoOriginal: TIntegerField
      FieldName = 'CodigoConceptoOriginal'
    end
    object cdsCreditoIndiceVehiculo: TStringField
      FieldName = 'IndiceVehiculo'
    end
    object cdsCreditoImporteIVA: TFloatField
      FieldName = 'ImporteIVA'
    end
    object cdsCreditoPorcentajeIVA: TFloatField
      FieldName = 'PorcentajeIVA'
    end
    object cdsCreditoValorNeto: TFloatField
      FieldName = 'ValorNeto'
    end
    object cdsCreditoDescriValorNeto: TStringField
      FieldName = 'DescriValorNeto'
    end
    object cdsCreditoDescriImporteIVA: TStringField
      FieldName = 'DescriImporteIVA'
    end
    object cdsCreditoDescriPorcentajeIVA: TStringField
      FieldName = 'DescriPorcentajeIVA'
    end
    object cdsCreditoCodigoConcesionaria: TSmallintField
      FieldName = 'CodigoConcesionaria'
    end
    object cdsCreditoSaldoDisponible: TFloatField
      FieldName = 'SaldoDisponible'
    end
  end
  object dsOriginal: TDataSource
    DataSet = cdsOriginal
    Left = 200
    Top = 240
  end
  object dsCredito: TDataSource
    DataSet = cdsCredito
    Left = 352
    Top = 440
  end
  object spObtenerCargosComprobanteAAnular: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerCargosComprobanteAAnular;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 235
    Top = 242
  end
  object spObtenerConceptoQueAnula: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerConceptoQueAnula;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 283
    Top = 242
  end
  object spAgregarMovimientoCuentaTemporal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'AgregarMovimientoCuentaTemporal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdSesion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaProcesamiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Seleccionado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteIVA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcentajeIVA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 483
    Top = 490
  end
  object spCargarTransitoEnTablaTemporal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'CargarTransitoEnTablaTemporal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdSesion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NumCorrCA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaProcesamiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 347
    Top = 242
  end
  object ppLineasCKDBPipeline: TppDBPipeline
    DataSource = dsLineasImpresionCK
    UserName = 'LineasCKDBPipeline'
    Left = 616
    Top = 488
    object ppLineasCKDBPipelineppField1: TppField
      FieldAlias = 'Descripcion'
      FieldName = 'Descripcion'
      FieldLength = 0
      DisplayWidth = 0
      Position = 0
    end
    object ppLineasCKDBPipelineppField2: TppField
      FieldAlias = 'Importe'
      FieldName = 'Importe'
      FieldLength = 0
      DataType = dtLargeInt
      DisplayWidth = 15
      Position = 1
    end
    object ppLineasCKDBPipelineppField3: TppField
      FieldAlias = 'DescImporte'
      FieldName = 'DescImporte'
      FieldLength = 50
      DisplayWidth = 50
      Position = 2
    end
    object ppLineasCKDBPipelineppField4: TppField
      Alignment = taRightJustify
      FieldAlias = 'CodigoConcepto'
      FieldName = 'CodigoConcepto'
      FieldLength = 0
      DataType = dtLongint
      DisplayWidth = 10
      Position = 3
    end
  end
  object dsLineasImpresionCK: TDataSource
    DataSet = spObtenerLineasImpresionCK
    Left = 584
    Top = 488
  end
  object spObtenerLineasImpresionCK: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'ObtenerLineasImpresionCK;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = 'CK'
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 659
    Top = 490
    object spObtenerLineasImpresionCKDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object spObtenerLineasImpresionCKImporte: TLargeintField
      FieldName = 'Importe'
    end
    object spObtenerLineasImpresionCKDescImporte: TStringField
      FieldName = 'DescImporte'
      Size = 50
    end
    object spObtenerLineasImpresionCKCodigoConcepto: TWordField
      FieldName = 'CodigoConcepto'
    end
  end
  object spLimpiarDatosTemporales: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'LimpiarDatosTemporalesAnulacionNotaDeCobro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdSesion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 403
    Top = 242
  end
  object spObtenerImporteTransitosAcreditados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerImporteTransitosAcreditados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 443
    Top = 242
  end
  object Img_Tilde: TImageList
    Left = 200
    Top = 438
    Bitmap = {
      494C010102002400F80010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object spChequearUsuarioConcurrenteAcreditando: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'ChequearUsuarioConcurrenteAcreditando;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroDeSesion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OtroUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaOtroUsuario'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 491
    Top = 242
  end
  object spAnularBoleta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'AnularComprobante'
    Parameters = <>
    Left = 403
    Top = 490
  end
  object spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'InsertarNumeroProcesoFacturacionEnColaEnvioDBNet'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 413
    Top = 536
    object StringField15: TStringField
      FieldName = 'NumeroConvenioFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object StringField16: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object StringField17: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object LargeintField3: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'PeriodoInicial'
      ReadOnly = True
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'PeriodoFinal'
      ReadOnly = True
    end
    object DateTimeField7: TDateTimeField
      FieldName = 'FechaEmision'
      ReadOnly = True
    end
    object DateTimeField8: TDateTimeField
      FieldName = 'FechaVencimiento'
      ReadOnly = True
    end
    object StringField18: TStringField
      FieldName = 'NombreCliente'
      ReadOnly = True
      Size = 152
    end
    object StringField19: TStringField
      FieldName = 'Domicilio'
      ReadOnly = True
      Size = 162
    end
    object StringField20: TStringField
      FieldName = 'Comuna'
      ReadOnly = True
      Size = 203
    end
    object StringField21: TStringField
      FieldName = 'Comentarios'
      Size = 100
    end
    object StringField22: TStringField
      FieldName = 'CodigoPostal'
      ReadOnly = True
      Size = 10
    end
    object StringField23: TStringField
      FieldName = 'SaldoPendiente'
      ReadOnly = True
    end
    object StringField24: TStringField
      FieldName = 'AjusteSencilloAnterior'
      ReadOnly = True
    end
    object StringField25: TStringField
      FieldName = 'AjusteSencilloActual'
      ReadOnly = True
    end
    object StringField26: TStringField
      FieldName = 'TotalAPagar'
      ReadOnly = True
    end
    object WordField2: TWordField
      FieldName = 'CodigoTipoMedioPago'
    end
    object StringField27: TStringField
      FieldName = 'EstadoPago'
      FixedChar = True
      Size = 1
    end
    object LargeintField4: TLargeintField
      FieldName = 'NumeroComprobanteFiscal'
    end
    object StringField28: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
  end
  object spCrearProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'CrearProcesoFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FacturacionManual'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 449
    Top = 536
  end
  object spObtenerNumeroTipoInternoComprobanteFiscal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'ObtenerNumeroTipoInternoComprobanteFiscal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 2
        Value = Null
      end>
    Left = 489
    Top = 536
  end
  object spObtenerImporteAnuladoConceptoComprobanteAAnular: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerImporteAnuladoConceptoComprobanteAAnular'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ImporteConcepto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 534
    Top = 242
  end
  object spObtenerTodosLosCodigoConcepto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTodosLosCodigoConcepto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoConceptoADevolver'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 584
    Top = 240
  end
  object spActualizarInfraccionesComprobanteAnulado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesComprobanteAnulado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 560
    Top = 424
  end
  object spCargarEstacionamientoEnTablaTemporal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'CargarEstacionamientoEnTablaTemporal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdSesion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NumCorrTransaccion'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaProcesamiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 347
    Top = 274
  end
  object cdsResumenConcesionarias: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoConcesionaria'
        DataType = ftSmallint
      end
      item
        Name = 'Importe'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 25
    Top = 256
    Data = {
      460000009619E0BD010000001800000002000000000003000000460013436F64
      69676F436F6E636573696F6E61726961020001000000000007496D706F727465
      08000400000000000000}
    object cdsResumenConcesionariasCodigoConcesionaria: TSmallintField
      FieldName = 'CodigoConcesionaria'
    end
    object cdsResumenConcesionariasImporte: TFloatField
      FieldName = 'Importe'
    end
  end
  object spObtenerTodosLosCodigoConceptoInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTodosLosCodigoConceptoInfractores'
    Parameters = <>
    Left = 616
    Top = 240
  end
  object spCargarInfraccionesEnTablaTemporal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    LockType = ltBatchOptimistic
    ProcedureName = 'CargarInfraccionesEnTablaTemporal;1'
    Parameters = <>
    Left = 347
    Top = 306
  end
  object spObtenerTotalCKporConcepto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCKporConcepto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 88
    Top = 256
  end
  object cdsTotalCKporConcepto: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 56
    Top = 256
    Data = {
      450000009619E0BD010000001800000002000000000003000000450008436F6E
      636570746F010049000000010005574944544802000200140005546F74616C08
      000400000000000000}
    object cdsTotalCKporConceptoConcepto: TStringField
      FieldName = 'Concepto'
    end
    object cdsTotalCKporConceptoTotal: TFloatField
      FieldName = 'Total'
    end
  end
end
