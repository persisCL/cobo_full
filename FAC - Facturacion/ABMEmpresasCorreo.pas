{-----------------------------------------------------------------------------
 File Name      : ABMEmpresasCorreo
 Author         : Miguel Diaz Valenzuela
 Date Created   : 20-Febrero-2013
 Language       : ES-CL
 Firma          : SS_660_MDV_20130220
 Description    : Alta-Baja-Modificaci�n de la tabla EmpresasCorreo
-----------------------------------------------------------------------------}
unit ABMEmpresasCorreo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, VariantComboBox, Variants,
  BuscaTab;

type
  TFormEmpresasCorreo = class(TForm)
    AbmToolbar1: TAbmToolbar;
    dblEmpresasCorreo: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txtNombreEmpresa: TEdit;
    Notebook: TNotebook;
    tblEmpresasCorreo: TADOTable;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    lblCodigoEstado: TLabel;
    txtCodigoEmpresa: TNumericEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure dblEmpresasCorreoClick(Sender: TObject);
    procedure dblEmpresasCorreoDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblEmpresasCorreoEdit(Sender: TObject);
    procedure dblEmpresasCorreoRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure dblEmpresasCorreoDelete(Sender: TObject);
    procedure dblEmpresasCorreoInsert(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure tblEmpresasCorreoAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
	function Inicializa: boolean;
  end;

var
  FormEmpresasCorreo: TFormEmpresasCorreo;

implementation

resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar la Empresa seleccionada?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar la Empresa seleccionada.';
    MSG_DELETE_CAPTION		= 'Eliminar Empresa';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos de la Empresa seleccionada.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Empresa';
    MSG_AGREGAR_CAPTION	    = 'Agregar Empresa';
    MSG_DESCRIPCION         = 'Debe ingresar la Descripci�n';
    MSG_DATOS_RELACIONADOS  = 'La Empresa no puede ser eliminada ya que se encuentra relacionada';
    MSG_CODIGO_VACIO        = 'Debe ingresar un C�digo';
    MSG_CODIGO_RANGO        = 'El c�digo ingresado est� fuera del rango permitido';
{$R *.DFM}
            
function TFormEmpresasCorreo.Inicializa(): boolean;
begin
    FormStyle                   := fsMDIChild;
	if not OpenTables([tblEmpresasCorreo]) then
		Result                  := False
	else begin
       	Notebook.PageIndex      := 0;
		Result                  := True;
        tblEmpresasCorreo.Sort  := 'CodigoEmpresaCorreo';
		dblEmpresasCorreo.Reload;
	end;
end;

procedure TFormEmpresasCorreo.btnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormEmpresasCorreo.dblEmpresasCorreoClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
        txtCodigoEmpresa.Value  := FieldByName('CodigoEmpresaCorreo').AsInteger;
		txtNombreEmpresa.text	:= FieldByName('Nombre').AsString;
	end;
end;

procedure TFormEmpresasCorreo.dblEmpresasCorreoDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoEmpresaCorreo').AsString);
   		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Nombre').AsString));
	end;
end;

procedure TFormEmpresasCorreo.dblEmpresasCorreoEdit(Sender: TObject);
begin
	dblEmpresasCorreo.Enabled   := False;
    dblEmpresasCorreo.Estado    := modi;
	Notebook.PageIndex 		    := 1;
    groupb.Enabled     		    := True;
    txtNombreEmpresa.setFocus;
end;

procedure TFormEmpresasCorreo.dblEmpresasCorreoRefresh(Sender: TObject);
begin
	 if dblEmpresasCorreo.Empty then Limpiar_Campos();
end;

procedure TFormEmpresasCorreo.Limpiar_Campos();
begin
	txtNombreEmpresa.Clear;
    txtCodigoEmpresa.Clear;
end;

procedure TFormEmpresasCorreo.AbmToolbar1Close(Sender: TObject);
begin
    Close;
end;

procedure TFormEmpresasCorreo.dblEmpresasCorreoDelete(Sender: TObject);
var
    Consulta            : string;
    Estado, Resultado   : Integer;
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            Estado      := (Sender AS TDbList).Table.FieldByName('CodigoEmpresaCorreo').AsInteger;
            Consulta    := 'SELECT COUNT(CodigoEmpresaCorreo) FROM HistoricoCartasListaAmarilla WHERE CodigoEmpresaCorreo = ' + IntToStr(Estado);
            Resultado   := QueryGetValueInt(DMConnections.BaseCAC, Consulta);

            if (Resultado = 0) then
    			(Sender AS TDbList).Table.Delete
            else
                MsgBoxErr(MSG_DELETE_ERROR, MSG_DATOS_RELACIONADOS, MSG_DELETE_CAPTION, MB_ICONSTOP);
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		dblEmpresasCorreo.Reload;
	end;
	dblEmpresasCorreo.Estado    := Normal;
	dblEmpresasCorreo.Enabled   := True;
 	Notebook.PageIndex          := 0;
	Screen.Cursor               := crDefault;
end;

procedure TFormEmpresasCorreo.dblEmpresasCorreoInsert(Sender: TObject);
begin
    Limpiar_Campos;
    groupb.Enabled              := True;
	dblEmpresasCorreo.Enabled   := False;
	Notebook.PageIndex          := 1;
    txtNombreEmpresa.Enabled    := True;
    txtNombreEmpresa.Color      := $00FAEBDE;
    txtNombreEmpresa.SetFocus;
end;

procedure TFormEmpresasCorreo.btnAceptarClick(Sender: TObject);
var
    MensajeError,
    TituloMensaje   : string;
begin
    if dblEmpresasCorreo.Estado = Alta then begin
        dblEmpresasCorreo.Table.Append;
        TituloMensaje := MSG_AGREGAR_CAPTION;
    end else begin
        dblEmpresasCorreo.Table.Edit;
        TituloMensaje := MSG_ACTUALIZAR_CAPTION
    end;

    if not ValidateControls(
        [txtNombreEmpresa],
        [Trim(txtNombreEmpresa.text) <> ''],
        TituloMensaje,
        [MSG_DESCRIPCION]) then exit;
    Screen.Cursor := crHourGlass;
	With dblEmpresasCorreo.Table do begin
		Try
			if Trim(txtNombreEmpresa.text) = '' then
            	FieldByName('Nombre').Clear
            else
				FieldByName('Nombre').AsString	 := Trim(txtNombreEmpresa.text);
			Post;
            txtCodigoEmpresa.Color   := clBtnFace;
            txtCodigoEmpresa.Enabled := False;
		except
			On E: Exception do begin
                Screen.Cursor := crDefault;
				Cancel;
                MensajeError := E.Message;
                if (AnsiPos('UNIQUE KEY', MensajeError) <> 0) then
                    MensajeError := 'El Estado ingresado ya existe';
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, MensajeError, TituloMensaje, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor := crDefault;
end;

procedure TFormEmpresasCorreo.FormShow(Sender: TObject);
begin
   	dblEmpresasCorreo.Reload;
end;

procedure TFormEmpresasCorreo.tblEmpresasCorreoAfterOpen(DataSet: TDataSet);
begin
    tblEmpresasCorreo.DisableControls;
    tblEmpresasCorreo.Sort := 'CodigoEmpresaCorreo';
    tblEmpresasCorreo.EnableControls
end;

procedure TFormEmpresasCorreo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormEmpresasCorreo.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormEmpresasCorreo.Volver_Campos;
begin
	dblEmpresasCorreo.Estado    := Normal;
	dblEmpresasCorreo.Enabled   := True;
    dblEmpresasCorreo.Reload;
	dblEmpresasCorreo.SetFocus;
	Notebook.PageIndex          := 0;
    groupb.Enabled              := False;
end;

end.
