unit frmSaltosNumeracionComprobantes;

{
    Autor       : mcabello
    Descripcion : formulario que permite definir rangos de saltos de numeraci�n
                para los distintos tipos de comprobantes.
    SS          : SS_1233_MCA_20141201
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, DmiCtrls, Abm_obj, DbList, UtilProc,
  UtilDB, PeaProcs, PeaProcsCN, SysUtilsCN, DMConnection, Util, VariantComboBox;

type
  TFormSaltosNumeracion = class(TForm)
    pnl1: TPanel;
    dblSaltosNumeracion: TAbmList;
    abmtlbr1: TAbmToolbar;
    pnlGroupB: TPanel;
    lblNombre: TLabel;
    lbl1: TLabel;
    lbl2: TLabel;
    txt_folioDesde: TNumericEdit;
    tblSaltosComprobantes: TADOTable;
    Notebook: TNotebook;
    btnBtnSalir: TButton;
    btnBtnAceptar: TButton;
    btnBtnCancelar: TButton;
    txt_foliohasta: TNumericEdit;
    cboTipoComprobante: TVariantComboBox;
    strngfldSaltosComprobantesTipoComprobante: TStringField;
    lrgntfldSaltosComprobantesNumeroFolioDesde: TLargeintField;
    lrgntfldSaltosComprobantesNumeroFolioHasta: TLargeintField;
    strngfldSaltosComprobantesUsuarioCreacion: TStringField;
    dtmfldSaltosComprobantesFechaHoraCreacion: TDateTimeField;
    strngfldSaltosComprobantesUsuarioModificacion: TStringField;
    dtmfldSaltosComprobantesFechaHoraModificacion: TDateTimeField;
    blnfldSaltosComprobantesEliminado: TBooleanField;
    dtmfldSaltosComprobantesFechaHoraEliminacion: TDateTimeField;
    strngfldSaltosComprobantesUsuarioEliminacion: TStringField;
    spObtenerTiposComprobantesSaltosFolios: TADOStoredProc;
    chkOcultarEliminados: TCheckBox;
    procedure abmtlbr1Close(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBtnSalirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dblSaltosNumeracionClick(Sender: TObject);
    procedure dblSaltosNumeracionInsert(Sender: TObject);
    procedure btnBtnCancelarClick(Sender: TObject);
    procedure dblSaltosNumeracionEdit(Sender: TObject);
    procedure dblSaltosNumeracionDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblSaltosNumeracionDelete(Sender: TObject);
    procedure btnBtnAceptarClick(Sender: TObject);
    procedure dblSaltosNumeracionRefresh(Sender: TObject);
    function dblSaltosNumeracionProcess(Tabla: TDataSet;
      var Texto: string): Boolean;
    procedure chkOcultarEliminadosClick(Sender: TObject);
    procedure dblSaltosNumeracionDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos();
    procedure Volver_Campos;
    procedure CargarTiposComprobantes;
  public
    { Public declarations }
    function Inicializa(): boolean;
  end;

var
  FormSaltosNumeracion: TFormSaltosNumeracion;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el salto de numeraci�n de comprobante?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el salto de numeraci�n de comprobantes porque hay datos que dependendientes.';
    MSG_DELETE_CAPTION 		= 'Eliminar el Salto de numeraci�n de comprobante';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el salto de numeraci�n de comprobante.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Saltos de numeraci�n de comprobante';
    MSG_ERROR_FOLIO_USADO = 'El folio ingresado ya est� utilizado, por lo que no puede ser ingresado.';
    MSG_SQL_NUMEROCOMPROBANTE = 'SELECT NumeroComprobante FROM NumerosComprobantes WITH (NOLOCK) WHERE TipoComprobante = ''%s''';

{$R *.dfm}

procedure TFormSaltosNumeracion.abmtlbr1Close(Sender: TObject);
begin
	Close;
end;

procedure TFormSaltosNumeracion.btnBtnAceptarClick(Sender: TObject);
resourcestring
    MSG_YA_EXISTE   = 'Ya existe este Tipo Comprobante';
    MSG_DESCRIPCION = 'Folio Hasta debe ser mayor que Folio Desde';
    MSG_DESCRIPCION_FOLIO_ERROR = 'Folio debe Ser Mayor que 0';
    MSG_SQL_VALIDAR_FOLIO  = 'SELECT TOP 1 1 FROM SaltosNumerosComprobantes WITH (NOLOCK) WHERE TipoComprobante = ''%s'' AND (NumeroFolioDesde BETWEEN %d AND %d OR NumeroFolioHasta BETWEEN %d AND %d)';
    //INICIO	: CFU 20170224 TASK_115_CFU_20170224-Administrar_Folios_De_Facturacion_Electronica
    MSG_SQL_VALIDAR_FOLIO_SUPERPUESTO  = 'SELECT TOP 1 1 FROM SaltosNumerosComprobantes WITH (NOLOCK) WHERE TipoComprobante = ''%s'' AND ((NumeroFolioDesde <= %d and NumeroFolioHasta >= %d) OR (NumeroFolioDesde >= %d) and (NumeroFolioHasta <= %d))';
    MSG_FOLIO_SUPERPUESTO = 'No debe existir Folios superpuestos';
    //TERMINO	: CFU 20170224 TASK_115_CFU_20170224-Administrar_Folios_De_Facturacion_Electronica
    MSG_SQL_VALIDAR_NUEVO_FOLIO = 'SELECT TOP 1 1 FROM SaltosNumerosComprobantes WITH (NOLOCK) WHERE TipoComprobante = ''%s'' AND NumeroFolioHasta + 1 = %d  AND Eliminado = 0';
    MSG_YA_EXISTE_FOLIO = 'Los folios no deben contenerse unos a otros.';
    MSG_ERROR_FOLIO_NUEVO_DESDE = 'Folio Desde NO puede ser igual a Folio Hasta + 1 de otro rango anterior.';
    MSG_ERROR_FOLIO_NUEVO_HASTA = 'Folio Hasta NO puede ser igual a Folio Hasta + 1 de otro rango anterior.';
    MSG_SQL_VALIDAR_ACTUALIZACION_FOLIO = ' AND NOT (TipoComprobante =''%s'' AND NumeroFolioDesde=%d)';
    MSG_SIN_TIPO_COMPROBANTE = 'Debe seleccionar alg�n tipo de comprobante';
var
    vSQL : string;
    vExiste : Integer;
    NumeroComprobanteActual: Int64;
begin

    if not ValidateControls([cboTipoComprobante, txt_foliohasta, txt_folioDesde],
                            [cboTipoComprobante.ItemIndex <> -1, txt_folioDesde.ValueInt < txt_foliohasta.ValueInt,
                            txt_folioDesde.ValueInt > 0],
                            MSG_ACTUALIZAR_CAPTION,
                            [MSG_SIN_TIPO_COMPROBANTE, MSG_DESCRIPCION, MSG_DESCRIPCION_FOLIO_ERROR]) then Exit;

    if dblSaltosNumeracion.Estado = Alta then begin

        //verifico que el numero de comprobante no est� utilizado
        vSQL := Format(MSG_SQL_NUMEROCOMPROBANTE, [cboTipoComprobante.Value]);
        NumeroComprobanteActual := QueryGetBigintValue(DMConnections.BaseCAC, vSQL);

        if NumeroComprobanteActual >= txt_folioDesde.ValueInt then begin
            MsgBoxBalloon(MSG_ERROR_FOLIO_USADO, Caption, MB_ICONEXCLAMATION, txt_folioDesde);
            Exit;
        end;

        //verifico que el folio no este contenido en otro
        vSQL := Format(MSG_SQL_VALIDAR_FOLIO, [cboTipoComprobante.Value, txt_folioDesde.ValueInt, txt_foliohasta.ValueInt, txt_folioDesde.ValueInt, txt_foliohasta.ValueInt]);
        vExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
        if vExiste = 1 then begin
           	MsgBoxBalloon(MSG_YA_EXISTE_FOLIO, Caption, MB_ICONEXCLAMATION, txt_folioDesde);
            Exit;
        end;

        //INICIO	: CFU 20170224 TASK_115_CFU_20170224-Administrar_Folios_De_Facturacion_Electronica
        vSQL := Format(MSG_SQL_VALIDAR_FOLIO_SUPERPUESTO, [cboTipoComprobante.Value, txt_folioDesde.ValueInt, txt_folioDesde.ValueInt, txt_foliohasta.ValueInt, txt_foliohasta.ValueInt]);
        vExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
        if vExiste = 1 then begin
        	MsgBoxBalloon(MSG_FOLIO_SUPERPUESTO, Caption, MB_ICONEXCLAMATION, txt_folioDesde);
            Exit;
        end;
        //TERMINO	: CFU 20170224 TASK_115_CFU_20170224-Administrar_Folios_De_Facturacion_Electronica

        //validar si el nuevo folio desde no puede ser igual a folio hasta+ 1 de otro rango anterior
        vSQL := Format(MSG_SQL_VALIDAR_NUEVO_FOLIO, [cboTipoComprobante.Value, txt_folioDesde.ValueInt]);
        VExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
        if vExiste = 1 then begin
           MsgBoxBalloon(MSG_ERROR_FOLIO_NUEVO_DESDE, Caption, MB_ICONEXCLAMATION, txt_folioDesde);
            Exit;
        end;
    end
    else begin
        if dblSaltosNumeracion.Estado = Modi then begin

            //verifico que el folio no este contenido en otro y que no sea el mismo (modificacion)
            vSQL := Format(MSG_SQL_VALIDAR_FOLIO, [cboTipoComprobante.Value, txt_folioDesde.ValueInt, txt_foliohasta.ValueInt, txt_folioDesde.ValueInt, txt_foliohasta.ValueInt]);
            vSQL := vSQL + Format(MSG_SQL_VALIDAR_ACTUALIZACION_FOLIO, [tblSaltosComprobantes.FieldByName('TipoComprobante').AsString, tblSaltosComprobantes.FieldByName('NumeroFolioDesde').AsInteger]);
            VExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
            if vExiste = 1 then begin
                MsgBoxBalloon(MSG_YA_EXISTE_FOLIO, Caption, MB_ICONEXCLAMATION, txt_folioDesde);
                Exit;
            end;

            //validar si el nuevo folio desde no puede ser igual a folio desde+ 1 de otro rango anterior
            vSQL := Format(MSG_SQL_VALIDAR_NUEVO_FOLIO, [cboTipoComprobante.Value, txt_folioDesde.ValueInt]);
            VExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
            if vExiste = 1 then begin
                MsgBoxBalloon(MSG_ERROR_FOLIO_NUEVO_DESDE, Caption, MB_ICONEXCLAMATION, txt_folioDesde);
                Exit;
            end;

            //validar si el nuevo folio hasta no puede ser igual a folio hasta+ 1 de otro rango anterior
             vSQL := Format(MSG_SQL_VALIDAR_NUEVO_FOLIO, [cboTipoComprobante.Value, txt_folioHasta.ValueInt]);
            VExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
            if vExiste = 1 then begin
                MsgBoxBalloon(MSG_ERROR_FOLIO_NUEVO_HASTA, Caption, MB_ICONEXCLAMATION, txt_folioHasta);
                Exit;
            end;
        end;

    end;

	Screen.Cursor := crHourGlass;
	With tblSaltosComprobantes do begin
		Try
			if dblSaltosNumeracion.Estado = Alta then Append else Edit;

			FieldByName('TipoComprobante').AsString 	  := cboTipoComprobante.Value;
            FieldByName('NumeroFolioDesde').AsInteger     := txt_folioDesde.ValueInt;
            FieldByName('NumeroFolioHasta').AsInteger 	  := txt_foliohasta.ValueInt;
            FieldByName('Eliminado').AsBoolean := False;
            
            if dblSaltosNumeracion.Estado = Alta then begin
              FieldByName('UsuarioCreacion').AsString 	  := UsuarioSistema;
              FieldByName('FechaHoraCreacion').AsDateTime := NowBase(tblSaltosComprobantes.Connection);
            end;

			FieldByName('UsuarioModificacion').AsString 	  := UsuarioSistema;
			FieldByName('FechaHoraModificacion').AsDateTime := NowBase(tblSaltosComprobantes.Connection);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	dblSaltosNumeracion.Estado     	:= Normal;
	dblSaltosNumeracion.Enabled    	:= True;
	pnlGroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
	dblSaltosNumeracion.Reload;
	dblSaltosNumeracion.SetFocus;
   	Screen.Cursor 	   := crDefault;

end;

procedure TFormSaltosNumeracion.btnBtnCancelarClick(Sender: TObject);
begin
    Volver_Campos;
end;

procedure TFormSaltosNumeracion.btnBtnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TFormSaltosNumeracion.dblSaltosNumeracionClick(Sender: TObject);
var
    vSQL : string;
    NumeroComprobanteActual: Int64;
    index: integer;
begin
    try
        CambiarEstadoCursor(crHourGlass);

        with (Sender AS TDbList).Table do begin

            for index := 0 to cboTipoComprobante.Items.Count - 1 do begin
                if cboTipoComprobante.Items[index].Value =  FieldByName('TipoComprobante').AsString then begin
                    cboTipoComprobante.ItemIndex := index;
                    Break;
                end;
            end;
            txt_folioDesde.Value := FieldByName('NumeroFolioDesde').AsInteger;
            txt_folioHasta.Value := FieldByName('NumeroFolioHasta').AsInteger;

             vSQL := Format(MSG_SQL_NUMEROCOMPROBANTE, [FieldByName('TipoComprobante').AsString]);
            NumeroComprobanteActual := QueryGetBigintValue(DMConnections.BaseCAC, vSQL);

            if (FieldByName('Eliminado').AsBoolean) or (NumeroComprobanteActual > FieldByName('NumeroFolioHasta').AsFloat) then
                abmtlbr1.Habilitados := abmtlbr1.Habilitados - [btModi] - [btBaja]
            else
                abmtlbr1.Habilitados := abmtlbr1.Habilitados + [btModi] + [btBaja];

        end;
    finally
        CambiarEstadoCursor(crDefault);
    end;
end;

procedure TFormSaltosNumeracion.dblSaltosNumeracionDblClick(Sender: TObject);
begin
    dblSaltosNumeracion.Estado := Normal;
end;

procedure TFormSaltosNumeracion.dblSaltosNumeracionDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
        With tblSaltosComprobantes do begin
            Try
                Edit;
                FieldByName('Eliminado').AsBoolean := True;
                FieldByName('UsuarioEliminacion').AsString 	  := UsuarioSistema;
                FieldByName('FechaHoraEliminacion').AsDateTime := NowBase(tblSaltosComprobantes.Connection);
                Post;
            except
                On E: EDataBaseError do begin
                    Cancel;
                    MsgBoxErr( MSG_DELETE_CAPTION, E.message,
                    MSG_DELETE_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
        dblSaltosNumeracion.Reload;
	end;
    Limpiar_Campos;
	dblSaltosNumeracion.Estado       := Normal;
	dblSaltosNumeracion.Enabled      := True;
	pnlGroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormSaltosNumeracion.dblSaltosNumeracionDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
    With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        if FieldByName('Eliminado').AsBoolean then
            Font.Color := clRed
        else
            Font.Color := clBlack;
        
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('TipoComprobante').AsString));
		TextOut(Cols[1], Rect.Top, IntToStr(Tabla.FieldbyName('NumeroFolioDesde').AsInteger));
        TextOut(Cols[2], Rect.Top, IntToStr(Tabla.FieldbyName('NumeroFolioHasta').AsInteger));
        TextOut(Cols[3], Rect.Top, Trim(Tabla.FieldbyName('UsuarioCreacion').AsString));
        TextOut(Cols[4], Rect.Top, Tabla.FieldbyName('FechaHoraCreacion').AsString);
        TextOut(Cols[5], Rect.Top, Trim(Tabla.FieldbyName('UsuarioModificacion').AsString));
        TextOut(Cols[6], Rect.Top, Tabla.FieldbyName('FechaHoraModificacion').AsString);
        TextOut(Cols[7], Rect.Top, Trim(Tabla.FieldbyName('UsuarioEliminacion').AsString));
        TextOut(Cols[8], Rect.Top, Tabla.FieldbyName('FechaHoraEliminacion').AsString);
	end;
end;

procedure TFormSaltosNumeracion.dblSaltosNumeracionEdit(Sender: TObject);
var
    vSQL : string;
    NumeroComprobanteActual: Int64;
begin
    Screen.Cursor    := crHourGlass;
    dblSaltosNumeracion.Enabled	:= False;
    with (Sender AS TDbList).Table do begin
        vSQL := Format(MSG_SQL_NUMEROCOMPROBANTE, [FieldByName('TipoComprobante').AsString]);
        NumeroComprobanteActual := QueryGetBigintValue(DMConnections.BaseCAC, vSQL);

        if (FieldByName('Eliminado').AsBoolean) or (NumeroComprobanteActual > FieldByName('NumeroFolioHasta').AsFloat) then
        begin
            Volver_Campos;
        end
        else begin
            dblSaltosNumeracion.Estado     := Modi;
            pnlGroupB.Enabled   := True;
            dblSaltosNumeracion.Enabled    := False;
            Notebook.PageIndex := 1;
            chkOcultarEliminados.Enabled := False;
            cboTipoComprobante.SetFocus;
        end;
    end;



    Screen.Cursor    := crDefault;
end;

procedure TFormSaltosNumeracion.dblSaltosNumeracionInsert(Sender: TObject);
begin
    Screen.Cursor    := crHourGlass;
    pnlGroupB.Enabled := True;
    Limpiar_Campos;
    dblSaltosNumeracion.Enabled := False;
    dblSaltosNumeracion.Estado := Alta;
    Notebook.PageIndex   := 1;
    cboTipoComprobante.SetFocus;
    Screen.Cursor    := crDefault;
end;

function TFormSaltosNumeracion.dblSaltosNumeracionProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
    Result := True;
    Texto  := tblSaltosComprobantes.FieldByName('TipoComprobante').AsString;
end;

procedure TFormSaltosNumeracion.dblSaltosNumeracionRefresh(Sender: TObject);
begin
    if dblSaltosNumeracion.Empty then Limpiar_Campos();
end;

procedure TFormSaltosNumeracion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    action := caFree;
end;

procedure TFormSaltosNumeracion.FormShow(Sender: TObject);
begin
    dblSaltosNumeracion.Reload;
end;

function TFormSaltosNumeracion.Inicializa(): boolean;
resourcestring
    MSG_ERROR = 'Error buscando datos de los saltos de numeraci�n en los comprobantes';
    TXT_ERROR = 'Error';
Var
	S: TSize;
begin
    Result := False;
	S := GetFormClientSize(Application.MainForm);
    CenterForm(self);

	if not OpenTables([tblSaltosComprobantes]) then begin
        ShowMsgBoxCN(TXT_ERROR, MSG_ERROR, MB_ICONERROR, Application.MainForm);
        exit;
    end;

    CargarTiposComprobantes;
    Notebook.PageIndex := 0;
   	Result := True;
end;

procedure TFormSaltosNumeracion.Limpiar_Campos();
begin
    cboTipoComprobante.ItemIndex := -1;
    txt_folioDesde.Clear;
    txt_foliohasta.Clear;
end;

procedure TFormSaltosNumeracion.Volver_Campos;
begin
	dblSaltosNumeracion.Estado  := Normal;
	dblSaltosNumeracion.Enabled := True;
    dblSaltosNumeracion.Reload;
	dblSaltosNumeracion.SetFocus;
    cboTipoComprobante.Color := clSkyBlue;
	Notebook.PageIndex   := 0;
    pnlGroupB.Enabled       := False;
    chkOcultarEliminados.Enabled := True;
end;

procedure TFormSaltosNumeracion.CargarTiposComprobantes;
begin
    spObtenerTiposComprobantesSaltosFolios.Open;
    while not spObtenerTiposComprobantesSaltosFolios.eof do begin
        cboTipoComprobante.Items.Add(spObtenerTiposComprobantesSaltosFolios.FieldByName('Descripcion').Value, trim(spObtenerTiposComprobantesSaltosFolios.FieldByName('TipoComprobante').Value));
        spObtenerTiposComprobantesSaltosFolios.Next;
    end;
    spObtenerTiposComprobantesSaltosFolios.Close;
    cboTipoComprobante.ItemIndex := -1;

end;

procedure TFormSaltosNumeracion.chkOcultarEliminadosClick(Sender: TObject);
begin
    if chkOcultarEliminados.Checked then
    begin
        tblSaltosComprobantes.Filtered := False;
        tblSaltosComprobantes.Filter := 'Eliminado = 0';
        tblSaltosComprobantes.Filtered := True;
    end else
        tblSaltosComprobantes.Filtered   := False;

    dblSaltosNumeracion.ReLoad;
end;

end.
