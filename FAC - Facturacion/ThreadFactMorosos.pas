{ *******************************************************
    Author      : CQuezadaI
    Date        : 07-01-2013
    Description : Hilo con la ejecución del proceso de facturación masiva de morosos.
    Firma       : SS_660_20121010
******************************************************* }
unit ThreadFactMorosos;

interface

uses
  Classes, Windows, SysUtils, DM_ThreadFactMorosos, UtilProc, Variants, Util;

type
  TThreadFacturacionGrupoMorosos = class(TThread)
  private
	{ Private declarations }
	FGrupo: Integer;
	FAnio: Integer;
	FCiclo: Integer;
	FTerminar: ^Boolean;
	DM: TDMThreadFacturacionMorosos;
	FThreadsCorriendo: ^Integer;
	FConveniosProcesados: ^Integer;
    FErrorFacturacion: ^Boolean;
	FNumeroProcesoFacturacion: Integer;
	FFechaCorte, FFechaEmision, FFechaVencimiento: TDateTime;
  protected
	procedure Execute; override;
	function FacturarConvenios(var ConvenioAnterior: Integer): Integer;
  public
	constructor Create(Grupo, Anio, Ciclo: Integer; FechaCorte, FechaEmision,
	  FechaVencimiento: TDateTime; NumeroProcesoFacturacion: Integer;
	  var ConveniosProcesados, ThreadsCorriendo: Integer; var ErrorFacturacion, Terminar: Boolean);
	destructor Destroy; override;
  end;

implementation

constructor TThreadFacturacionGrupoMorosos.Create(Grupo, Anio, Ciclo: Integer;
  FechaCorte, FechaEmision, FechaVencimiento: TDateTime;
  NumeroProcesoFacturacion: Integer; var ConveniosProcesados,
  ThreadsCorriendo: Integer; var ErrorFacturacion, Terminar: Boolean);
begin
	Inherited Create(True);
	FreeOnTerminate := True;
	FGrupo := Grupo;
	FAnio  := Anio;
    FCiclo := Ciclo;
	FFechaCorte := FechaCorte;
	FFechaEmision := FechaEmision;
    FFechaVencimiento := FechaVencimiento;
	FNumeroProcesoFacturacion := NumeroProcesoFacturacion;
	FTerminar := @Terminar;
	FErrorFacturacion := @ErrorFacturacion;
	FConveniosProcesados := @ConveniosProcesados;
	FThreadsCorriendo := @ThreadsCorriendo;
	Inc(FThreadsCorriendo^);
	DM := TDMThreadFacturacionMorosos.Create(nil);
	Resume;
end;

destructor TThreadFacturacionGrupoMorosos.Destroy;
begin
	DM.Free;
	Dec(FThreadsCorriendo^);
	inherited;
end;

procedure TThreadFacturacionGrupoMorosos.Execute;
Var
	CodigoConvenio, Procesados: Integer;
begin
	try
		CodigoConvenio := 0;
		While not Terminated and (not FTerminar^) do begin
			Procesados := FacturarConvenios(CodigoConvenio);
			if Procesados = 0 then Break;
			if Procesados < 0 then begin
            	FErrorFacturacion^ := True;
			  	raise Exception.Create('Error al generar Facturación');
            end;
			Inc(FConveniosProcesados^, Procesados);
        end;
	except
		on e: exception do MsgBox(e.message);
	end;
end;

function TThreadFacturacionGrupoMorosos.FacturarConvenios(var ConvenioAnterior: Integer): Integer;
var
    Reintentos: Integer;
    mensajeDeadlock: AnsiString;
    DescripcionError: string;
    ConvenioHasta: integer;
begin
    // Intentamos facturar un grupo de convenios.
    Reintentos  := 0;
    Result      := -1;
    while Reintentos < 5 do begin
        try
		    with DM.FacturacionMasivaConveniosMorosos do begin
                Parameters.Refresh;
			    Parameters.ParamByname('@CodigoConvenio').Value             := 0;
			    Parameters.ParamByname('@ConvenioAnterior').Value           := ConvenioAnterior;
			    Parameters.ParamByname('@GrupoFacturacion').Value           := FGrupo;
			    Parameters.ParamByname('@FechaCorte').Value                 := FFechaCorte;
			    Parameters.ParamByname('@FechaEmision').Value               := FFechaEmision;
			    Parameters.ParamByname('@FechaVencimiento').Value           := FFechaVencimiento;
			    Parameters.ParamByname('@NumeroProcesoFacturacion').Value   := FNumeroProcesoFacturacion;
			    Parameters.ParamByname('@NoGenerarAjusteSencillo').Value    := False;
			    Parameters.ParamByname('@ConveniosProcesados').Value        := 0;
				Parameters.ParamByname('@DescripcionError').Value           := '' ;
			    Parameters.ParamByname('@CodigoUsuario').Value              := UsuarioSistema;
			    ExecProc;

                DescripcionError := Parameters.ParamByname('@DescripcionError').Value;
                ConvenioHasta    := Parameters.ParamByname('@ConvenioAnterior').Value;

                if DescripcionError <> ''  then MsgBox('Error en la facturación ' + DescripcionError);

			    ConvenioAnterior := Parameters.ParamByname('@ConvenioAnterior').Value;
			    Result           := Parameters.ParamByname('@ConveniosProcesados').Value;
                Break;
		    end;
	    except
		    on e: exception do begin
                ConvenioHasta    := DM.FacturacionMasivaConveniosMorosos.Parameters.ParamByname('@ConvenioAnterior').Value;
                DescripcionError := DM.FacturacionMasivaConveniosMorosos.Parameters.ParamByname('@DescripcionError').Value;
                if (pos('deadlock', e.Message) > 0) then begin
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(1000);
                end else begin
			        //MsgBox( format('Error en la facturación rango convenios %d  %d  ',[ConvenioAnterior ,ConvenioHasta ])            // SS_660_CQU_20130822
			        MsgBox( format('Error en la facturación de morosos, rango convenios %d  %d  ',[ConvenioAnterior ,ConvenioHasta ])  // SS_660_CQU_20130822
                    +  ' ' + DescripcionError + ' ' + e.message);
                    break;
                end;
		    end;
        end;
    end;
    // Si se ejecuto 3 veces, es por que las tres veces dio deadlock, entonces
    // mostramos el mensaje y retonramos -1 por que hubo error
    if Reintentos = 3 then begin
        MsgBox('Error en la facturación ' + mensajeDeadLock);
    end;
end;

end.
