{------------------------------------------------------------------------
                      frmAnulacionNotaCobroDirecta

Author: mbecerra
Date: 27-Agosto-2009
Description: (Ref. SS 784)
        	Permite la generaci�n de notas de cr�dito para anular NQ (Notas
            de Cobro Directa)

Revisi�n        : 1
Author          : Nelson Droguett Sierra
Date            : 25-Septiembre-2009
Descripction    : 1.Corregir la insercion de registros en la tabla temporal

Revisi�n        : 2
Author          : Nelson Droguett Sierra
Date            : 02-Octubre-2009
Descripction    : 1.Se cambia la interfaz para que permita hacer las notas de
                    credito solo a una NQ o NB por documento.

Revisi�n        : 3
Author          : Nelson Droguett Sierra
Date            : 10-Diciembre-2009
Descripction    : 1.Se limpian los controles luego de una generacion exitosa.

Revisi�n        : 4
Author          : Nelson Droguett Sierra
Date            : 7-Octubre-2010
Descripction    : (Ref. SS-784) Se imprime en el formato de la CK Electr�nica
Firma			: SS-784-NDR-20101007


Revisi�n        : 5
Author          : Nelson Droguett Sierra
Date            : 14-Octubre-2010
Descripction    : (Ref. SS-784) Se Refrescan los parametros del AnularNotaCobroNQ
Firma			: SS-784-NDR-20101014

Revision		: 6
Author			: Nelson Droguett Sierra
Date			: 08-Noviembre-2010
Description		: (Ref.SS-784)
				Cambio al t�tulo de la ventana.
                Cuando se busque un documento ya anulado, que el sistema lo indique.
Firma			: SS-784-NDR-20101108
----------------------------------------------------------------------------}
unit frmAnulacionNotaCobroDirecta;

interface

uses
	DMConnection,
    UtilProc,
    UtilDB,
    Util,
    PeaProcs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DmiCtrls, DB, ADODB,
  //frmReporteNotaDebitoElectronica,		//SS-784-NDR-20101007
  frmReporteNotaCreditoElectronica;			//SS-784-NDR-20101007

type
  TAnulacionNotaCobroDirectaForm = class(TForm)
    gbDocumento: TGroupBox;
    Label1: TLabel;
    rbtnPDU: TRadioButton;
    rbtnBHTU: TRadioButton;
    btnAgregar: TButton;
    btnSalir: TButton;
    dbDetalles: TGroupBox;
    GroupBox1: TGroupBox;
    btnGenerar: TButton;
    lstvDetalle: TListView;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    lblTotal: TLabel;
    neDocumento: TNumericEdit;
    spAnularNotaCobroNQ: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
    procedure rbtnPDUClick(Sender: TObject);
    procedure rbtnBHTUClick(Sender: TObject);
  private
    { Private declarations }
    FDetalleNotaCredito : TList;
  public
    { Public declarations }
    function Inicializar : boolean;
    procedure LimpiarDatos(CualCampo : byte);
    procedure RefrescarLista;
    function InsertarComprobantesAAnular : boolean;
    procedure BorrarTablaTemporal;
  end;

  TDatosLista = record
        Documento : string;
        Numero : int64;
        Importe : int64;
  end;

var
  AnulacionNotaCobroDirectaForm: TAnulacionNotaCobroDirectaForm;

implementation

{$R *.dfm}

const
	cLimpiarEdit	= $1;
    cLimpiarLista	= $2;
    cLimpiarTotal	= $4;
    TablaTemporalNQ = '#tmp_NQAAnular';		//tabla temporal que almacena las NQ a anular


{----------------------------------------------------------
        	InsertarComprobantesAAnular

Author: mbecerra
Date: 28-Agosto-2009
Description:	Crea una tabla temporal para insertar en ella los comprobantes
            	que se van a incluir en la Nota de Cr�dito.
-----------------------------------------------------------------------------}
function TAnulacionNotaCobroDirectaForm.InsertarComprobantesAAnular;
resourcestring
    // Rev.1 / 2-Septiembre-2009 / Nelson Droguett Sierra-----------------------------------------------------------
	//MSG_SQL_TABLA	= 'CREATE TABLE %s (TipoComprobante CHAR(2) NOT NULL, NumeroComprobante BIGINT NOT NULL)';
    //MSG_SQL_INSERT	= 'INSERT INTO %s (TipoComprobante, NumeroComprobante, Importe) VALUES (''%s'', %d)';
	MSG_SQL_TABLA	= 'CREATE TABLE %s (TipoComprobante CHAR(2) NOT NULL, NumeroComprobante BIGINT NOT NULL, Importe BIGINT NOT NULL)';
    MSG_SQL_INSERT	= 'INSERT INTO %s (TipoComprobante, NumeroComprobante, Importe) VALUES (''%s'', %d,0)';
	MSG_ERROR		= 'Ocurri� el siguiente error al crear/insertar en tabla temporal';
    MSG_NO_SQL		= 'No se insert� en la tabla temporal el comprobante %s n�mero %d';

var
	i, Insercion : integer;
    PDatosLista : ^TDatosLista;
begin
    Result := False;

    try
        DMConnections.BaseCAC.Execute(Format(MSG_SQL_TABLA, [TablaTemporalNQ]));
        i := 0;
        while i < FDetalleNotaCredito.Count do begin
        	PDatosLista := FDetalleNotaCredito[i];
            DMConnections.BaseCAC.Execute(Format(MSG_SQL_INSERT, [TablaTemporalNQ, PDatosLista^.Documento, PDatosLista^.Numero]),Insercion);
            if Insercion <> 1 then begin
            	i := FDetalleNotaCredito.Count + 1;
                MsgBox(Format(MSG_NO_SQL, [PDatosLista^.Documento, PDatosLista^.Numero]), Caption, MB_ICONERROR);
            end;

            Inc(i);
        end;

    	Result := True;
    except on e:exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
		end;
    end;

end;

{----------------------------------------------------------
        	BorrarTablaTemporal

Author: mbecerra
Date: 28-Agosto-2009
Description:	Borra la tabla temporal creada con la funci�n
				InsertarComprobantesAAnular.
-----------------------------------------------------------------------------}
procedure TAnulacionNotaCobroDirectaForm.BorrarTablaTemporal;
resourcestring
	MSG_SQL = 'DROP TABLE %s';

begin
	try
		DMConnections.BaseCAC.Execute(Format(MSG_SQL, [TablaTemporalNQ]));
    finally
        {si hay error, no importa}
    end;
end;

{----------------------------------------------------------
        	LimpiarDatos

Author: mbecerra
Date: 27-Agosto-2009
Description:	Limpia los datos del formulario, indicado en los par�metros
-----------------------------------------------------------------------------}
procedure TAnulacionNotaCobroDirectaForm.LimpiarDatos(CualCampo: Byte);
begin
    if (CualCampo and cLimpiarEdit) = cLimpiarEdit then neDocumento.Value := 0;
    if (CualCampo and cLimpiarLista) = cLimpiarLista then begin
        lstvDetalle.Clear;
        FDetalleNotaCredito.Clear;
        // Rev.1 / 29-Septiembre-2009 / Nelson Droguett Sierra ----------------------------
        btnGenerar.Enabled:=False;
    end;

    if (CualCampo and cLimpiarTotal) = cLimpiarTotal then lblTotal.Caption := '';

end;

procedure TAnulacionNotaCobroDirectaForm.rbtnBHTUClick(Sender: TObject);
begin
    //Cuando Cambia de PDU a BHTU se limpia la grilla, ya que no puede mezclar para una
    //sola Nota de Credito
    // Rev.1 / 29-Septiembre-2009 / Nelson Droguett Sierra ---------------------------
    LimpiarDatos(cLimpiarEdit + cLimpiarLista + cLimpiarTotal);
end;

procedure TAnulacionNotaCobroDirectaForm.rbtnPDUClick(Sender: TObject);
begin
    //Cuando Cambia de BHTU a PDU se limpia la grilla, ya que no puede mezclar para una
    //sola Nota de Credito
    // Rev.1 / 29-Septiembre-2009 / Nelson Droguett Sierra ---------------------------
    LimpiarDatos(cLimpiarEdit + cLimpiarLista + cLimpiarTotal);
end;

{----------------------------------------------------------
        	RefrescarLista

Author: mbecerra
Date: 27-Agosto-2009
Description:	Despliega el listado de comprobantes a anular, su n�mero y
            	el total del comprobante. Adem�s indica la suma total del monto
                por el cual se va a emitir la Nota de cr�dito electr�nica
-----------------------------------------------------------------------------}
procedure TAnulacionNotaCobroDirectaForm.RefrescarLista;
var
	i : integer;
    SumaTotal : extended;
    Item : TListItem;
    PDatosLista : ^TDatosLista;
begin
    lstvDetalle.Clear;
    SumaTotal := 0;
    for I := 0 to FDetalleNotaCredito.Count - 1 do begin
        PDatosLista := FDetalleNotaCredito[i];
    	Item := lstvDetalle.Items.Add;
        Item.Caption :=  PDatosLista^.Documento;
        Item.SubItems.Add(IntToStr(PDatosLista^.Numero));
        Item.SubItems.Add(Format('%.0n', [1.0 * PDatosLista^.Importe]));
        SumaTotal := SumaTotal + PDatosLista^.Importe;
    end;

    lblTotal.Caption := Format('%.0n', [SumaTotal]);
    // Rev.1 / 29-Septiembre-2009 / Nelson Droguett Sierra ----------------------------
    btnGenerar.Enabled := ( FDetalleNotaCredito.Count>0 );
end;


{----------------------------------------------------------
        	btnAgregarClick

Author: mbecerra
Date: 28-Agosto-2009
Description:	Despliega el listado de comprobantes a anular, su n�mero y
            	el total del comprobante. Adem�s indica la suma total del monto
                por el cual se va a emitir la Nota de cr�dito electr�nica
-----------------------------------------------------------------------------}
procedure TAnulacionNotaCobroDirectaForm.btnAgregarClick(Sender: TObject);
resourcestring
    MSG_FALTA_DOC	= 'Falta el N�mero del documento';
    MSG_NO_HAY_DOC	= 'No se encontr� el Comprobante indicado, o no est� tipificado como documento electr�nico ';
    //Rev.6 / 08-Noviembre-2010 / Nelson Droguett Sierra
    MSG_DOC_ANULADO	= 'El documento se encuentra anulado ';				//SS-784-NDR-20101108
    MSG_ERROR		= 'Ocurri� un Error al agregar el comprobante';

    //Rev.6 / 08-Noviembre-2010 / Nelson Droguett Sierra --------------------------------------------
    MSG_SQL_EP_DOC    =	'SELECT EstadoPago ' +                           		//SS-784-NDR-20101108
    					'FROM	Comprobantes (NOLOCK) ' +                       //SS-784-NDR-20101108
                    	'WHERE	TipoComprobante = ''%s'' ' +                    //SS-784-NDR-20101108
                        'AND	NumeroComprobante = %d ' +                      //SS-784-NDR-20101108
                        'AND	TipoComprobanteFiscal IN (''BA'', ''BE'', ''FA'', ''FE'' ) ';
    MSG_SQL_MONTO_DOC=	'SELECT TotalComprobante / 100 ' +                           			//SS-784-NDR-20101108
    					'FROM	Comprobantes (NOLOCK) ' +                       //SS-784-NDR-20101108
                    	'WHERE	TipoComprobante = ''%s'' ' +                    //SS-784-NDR-20101108
                        'AND	NumeroComprobante = %d ' +                      //SS-784-NDR-20101108
                        'AND	EstadoPago <> ''A'' ' +                         //SS-784-NDR-20101108
                        'AND	TipoComprobanteFiscal IN (''BA'', ''BE'', ''FA'', ''FE'' ) ';
var
    //MontoDoc, TipoDoc, ConsultaSql : string;                         			//SS-784-NDR-20101108
    EstadoPago,MontoDoc, TipoDoc, ConsultaSql : string;                         //SS-784-NDR-20101108
    //FinRev.6----------------------------------------------------------------------------------------
    PDetalleDoc : ^TDatosLista;
begin
    if not ValidateControls([neDocumento], [neDocumento.Value > 0], Caption, [MSG_FALTA_DOC]) then Exit;

    {buscar el comprobante, si existe}
    if rbtnPDU.Checked then TipoDoc := 'NQ'
    else TipoDoc := 'NB';

    try
        // Rev.2 / 02-Octubre-2009 / Nelson Droguett Sierra ---------------------------
        LimpiarDatos(cLimpiarLista + cLimpiarTotal);
        // ---------------------------------------------------------------------------.

	    //Rev.6 / 08-Noviembre-2010 / Nelson Droguett Sierra --------------------------------------------------------
    	//ConsultaSql	:= Format(MSG_SQL_DOC, [TipoDoc, neDocumento.ValueInt]);                //SS-784-NDR-20101108
    	//MontoDoc	:= QueryGetValue(DMConnections.BaseCAC, ConsultaSql);						//SS-784-NDR-20101108
    	//if MontoDoc = '' then MsgBox(MSG_NO_HAY_DOC, Caption, MB_ICONEXCLAMATION)     		//SS-784-NDR-20101108
    	ConsultaSql	:= Format(MSG_SQL_EP_DOC, [TipoDoc, neDocumento.ValueInt]);                 //SS-784-NDR-20101108
    	EstadoPago	:= QueryGetValue(DMConnections.BaseCAC, ConsultaSql);						//SS-784-NDR-20101108
    	if EstadoPago = '' then MsgBox(MSG_NO_HAY_DOC, Caption, MB_ICONEXCLAMATION)     		//SS-784-NDR-20101108
    	else if EstadoPago = 'A' then MsgBox(MSG_DOC_ANULADO, Caption, MB_ICONEXCLAMATION)		//SS-784-NDR-20101108
    	else begin                                                                              //SS-784-NDR-20101108
            ConsultaSql	:= Format(MSG_SQL_MONTO_DOC, [TipoDoc, neDocumento.ValueInt]);          //SS-784-NDR-20101108
            MontoDoc	:= QueryGetValue(DMConnections.BaseCAC, ConsultaSql);					//SS-784-NDR-20101108
        	New (PDetalleDoc);
            PDetalleDoc^.Documento	:= TipoDoc;
            PDetalleDoc^.Numero		:= neDocumento.ValueInt;
            PDetalleDoc^.Importe 	:= StrToInt(MontoDoc);
        	FDetalleNotaCredito.Add(PDetalleDoc);
            RefrescarLista();
    	end;

    except on e:exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

procedure TAnulacionNotaCobroDirectaForm.btnGenerarClick(Sender: TObject);
resourcestring
	MSG_NO_HAY_DATOS	= 'No hay documentos ingresados (NQ, NB) para anular';
    MSG_CONFIRMAR		= 'A continuaci�n se anular�n los documentos ingresados. �Desea continuar?';
    MSG_ERROR			= 'Ocurri� un error al generar la nota de cr�dito';
    MSG_EXITO			= 'Se ha generado exitosamente la nota de cr�dito electr�nica n�mero %d. �Desea Imprimirla ahora?';
var
    //Rev.4 / 7-Octubre-2010 / Nelson Droguett Sierra -----------------------
    //f: TReporteNotaDebitoElectronicaForm;				//SS-784-NDR-20101007
    f: TReporteNotaCreditoElectronicaForm;				//SS-784-NDR-20101007
    //FinRev.4---------------------------------------------------------------


    MensajeError, TipoDoc : string;
    NumeroComprobante, NumeroComprobanteFiscal : int64;
begin
    if not ValidateControls([lstvDetalle], [FDetalleNotaCredito.Count > 0], Caption, [MSG_NO_HAY_DATOS]) then Exit;

	if MsgBox(MSG_CONFIRMAR, Caption, MB_ICONQUESTION + MB_YESNO) = IDNO then Exit;

	try
        if InsertarComprobantesAAnular() then begin
        	spAnularNotaCobroNQ.Close;
            //Rev.5 / 14-Octubre-2010 / Nelson Droguett Sierra--------------------------------------------------------------------------------
            spAnularNotaCobroNQ.Parameters.Refresh;   //SS-784-NDR-20101014
            //spAnularNotaCobroNQ.Parameters.ParamByName('@DescripcionError').Value			:= NULL;
            //spAnularNotaCobroNQ.Parameters.ParamByName('@NumeroComprobanteFiscal').Value	:= NULL;
            //spAnularNotaCobroNQ.Parameters.ParamByName('@NumeroComprobante').Value			:= NULL;
            spAnularNotaCobroNQ.Parameters.ParamByName('@Usuario').Value					:= UsuarioSistema;
            spAnularNotaCobroNQ.ExecProc;
            MensajeError := spAnularNotaCobroNQ.Parameters.ParamByName('@DescripcionError').Value;
            if MensajeError <> '' then MsgBoxErr(MSG_ERROR, MensajeError, Caption, MB_ICONERROR)
            else begin
                if rbtnPDU.Checked then TipoDoc := 'CQ'
                else TipoDoc := 'CB';
                NumeroComprobante		:= spAnularNotaCobroNQ.Parameters.ParamByName('@NumeroComprobante').Value;
            	NumeroComprobanteFiscal	:= spAnularNotaCobroNQ.Parameters.ParamByName('@NumeroComprobanteFiscal').Value;
            	if MsgBox(Format(MSG_EXITO, [NumeroComprobanteFiscal]), Caption, MB_ICONQUESTION + MB_YESNO) = IDYES then begin

                	//Rev.4 / 07-Octubre-2010 / Nelson Droguett Sierra -----------------------------------------
                  	f:= TReporteNotaCreditoElectronicaForm.Create(nil);                                                   //SS-784-NDR-20101007
                    try                                                                                                   //SS-784-NDR-20101007
                    	if not f.Inicializar(DMConnections.BaseCAC,TipoDoc,NumeroComprobante,MensajeError) then           //SS-784-NDR-20101007
                           ShowMessage( MensajeError )                                                                    //SS-784-NDR-20101007
                      	else                                                                                              //SS-784-NDR-20101007
                            f.ppDBText1.Visible:=False;                                                                   //SS-784-NDR-20101007
                            //f.ppDBText13.Visible:=False;                                                                  //SS-784-NDR-20101007
                            //f.ppLabel6.Visible:=False;                                                                    //SS-784-NDR-20101007
                               //no deben ir los campos de �ltimo ajuste del convenio
                            f.pplblUltimoAjuste.Visible := False;
                            f.ppDBText2.Visible := False;
                          	f.Ejecutar;                                                                                   //SS-784-NDR-20101007
                    finally                                                                                               //SS-784-NDR-20101007
                    	if Assigned(f) then f.Release;                                                                    //SS-784-NDR-20101007
                    end;                                                                                                  //SS-784-NDR-20101007

                    //Application.CreateForm(TReporteNotaDebitoElectronicaForm,f);
                    //try
                    //    if f.Inicializar(DMConnections.BaseCAC,
                    //                     TipoDoc,
                    //                     NumeroComprobante,
                    //                     True,
                    //                     False) then begin
                    //       f.Imprimir(True);
                    //    end;
                    //finally
                    //    f.Release;
                    //end;
                    //FinRev.4-----------------------------------------------------------------------------------
                end;
            end;
        end;

        BorrarTablaTemporal();		//trata de borrar, a�n cuando no est� creada

        // Rev.3 / 10-Diciembre-2009 / Nelson Droguett Sierra -------------------------------
        LimpiarDatos(cLimpiarEdit + cLimpiarLista + cLimpiarTotal);
        // ----------------------------------------------------------------------------------

    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
	    end;
    end;

end;

procedure TAnulacionNotaCobroDirectaForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TAnulacionNotaCobroDirectaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
    FDetalleNotaCredito.Free;
end;

{----------------------------------------------------------
        	Inicializar

Author: mbecerra
Date: 28-Agosto-2009
Description:	Inicializa el formulario de anulaci�n de Notas de
                Cobro Directas Electr�nicas.
-----------------------------------------------------------------------------}
function TAnulacionNotaCobroDirectaForm.Inicializar;
resourcestring
	//Rev.6 / 08-Noviembre-2010 / Nelson Droguett Sierra
    //MSG_CAPTION	= 'Anulaci�n Notas de Cobro Directa Electr�nicas';
    MSG_CAPTION	= 'Generar Nota de Cr�dito a Nota de Cobro Directa';  //SS-784-NDR-20101108
begin
    FDetalleNotaCredito := TList.Create;
    Caption := MSG_CAPTION;
	CenterForm(Self);
    LimpiarDatos(cLimpiarEdit + cLimpiarLista + cLimpiarTotal);
end;

end.
