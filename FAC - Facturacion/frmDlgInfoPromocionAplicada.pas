unit frmDlgInfoPromocionAplicada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, DB, ADODB, DMConnection, DBCtrls;

type
  TfrmDialogInfoPromocionAplicada = class(TForm)
    spObtenerInfoPromocion: TADOStoredProc;
    RotatedLabel1: TRotatedLabel;
    spObtenerInfoPromocionNumeroPromocion: TIntegerField;
    spObtenerInfoPromocionDescripcion: TStringField;
    spObtenerInfoPromocionValorIni: TBCDField;
    spObtenerInfoPromocionValorFin: TBCDField;
    spObtenerInfoPromocionTipoDescuento: TStringField;
    spObtenerInfoPromocionDescuento: TBCDField;
    spObtenerInfoPromocionFechaHoraActivacion: TDateTimeField;
    spObtenerInfoPromocionFechaHoraBaja: TDateTimeField;
    spObtenerInfoPromocionObservacion: TMemoField;
    Label1: TLabel;
    dsObtenerInfoPromocion: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    dbtNumeroPromocion: TDBText;
    dbtDescripcion: TDBText;
    dbtValorFin: TDBText;
    dbtValorIni: TDBText;
    dbtTipoDesc: TDBText;
    dbtDescuento: TDBText;
    dbtFHBaja: TDBText;
    dbFHActivacion: TDBText;
    dbtObservacion: TDBText;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Inicializa(NroPromo: Integer) ;
  end;

var
  frmDialogInfoPromocionAplicada: TfrmDialogInfoPromocionAplicada;

implementation

{$R *.dfm}

procedure TfrmDialogInfoPromocionAplicada.Inicializa(NroPromo: Integer);
begin
     spObtenerInfoPromocion.Close;
     spObtenerInfoPromocion.Parameters.ParamByName('@NumeroPromocion').Value := NroPromo;
     spObtenerInfoPromocion.Open;
end;

end.
