object FormABMCotizacionDiariaUF: TFormABMCotizacionDiariaUF
  Left = 241
  Top = 174
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Cotizaciones de Valores Financieros'
  ClientHeight = 520
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 481
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 270
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object Lista: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 314
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'117'#0'Fecha Cotizaci'#243'n         '
      #0'81'#0'Cotizaci'#243'n        '
      #0'71'#0'Moneda        ')
    HScrollBar = True
    RefreshTime = 10
    Table = CotizacionesDiariasUF
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaClick
    OnProcess = ListaProcess
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
    OnInsert = ListaInsert
    OnDelete = ListaDelete
    OnEdit = ListaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object GroupB: TPanel
    Left = 0
    Top = 347
    Width = 592
    Height = 134
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object Label15: TLabel
      Left = 9
      Top = 22
      Width = 107
      Height = 13
      Caption = '&Fecha Cotizaci'#243'n: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 10
      Top = 54
      Width = 68
      Height = 13
      Caption = 'Cotizaci'#243'n :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMoneda: TLabel
      Left = 10
      Top = 83
      Width = 54
      Height = 13
      Caption = 'Moneda :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object neCotizacion: TNumericEdit
      Left = 126
      Top = 51
      Width = 121
      Height = 21
      MaxLength = 12
      TabOrder = 1
      Decimals = 2
    end
    object deFechaVigencia: TDateEdit
      Left = 126
      Top = 19
      Width = 121
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object cbMoneda: TComboBox
      Left = 126
      Top = 78
      Width = 121
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      OnKeyPress = cbMonedaKeyPress
    end
  end
  object CotizacionesDiariasUF: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'FechaVigencia DESC'
    TableName = 'CotizacionesDiariasUF'
    Left = 316
    Top = 75
    object CotizacionesDiariasUFFechaVigencia: TDateTimeField
      FieldName = 'FechaVigencia'
    end
    object CotizacionesDiariasUFCotizacion: TLargeintField
      FieldName = 'Cotizacion'
    end
    object CotizacionesDiariasUFFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object CotizacionesDiariasUFUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object CotizacionesDiariasUFFechaHoraModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
    object CotizacionesDiariasUFUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
      FixedChar = True
    end
    object CotizacionesDiariasUFMoneda: TStringField
      FieldName = 'Moneda'
      FixedChar = True
      Size = 3
    end
  end
  object MainMenu1: TMainMenu
    Left = 296
    Top = 2
  end
  object spObtenerListaMonedas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListaMonedas'
    Parameters = <>
    Left = 376
    Top = 272
  end
  object spObtenerCotizacionFechaMoneda: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCotizacionFechaMoneda'
    Parameters = <>
    Left = 192
    Top = 184
  end
end
