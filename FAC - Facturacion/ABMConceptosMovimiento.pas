{-----------------------------------------------------------------------------
 Unit Name: ABMConceptosMovimiento
 Author: jconcheyro
 Purpose: Sirve de ABM de registros en ConceptosMovimiento
 Date: 19/12/2005
 Description:                                                                                
 History:

 Revision: 1
 Author: jconcheyro
 Date: 12/09/2006
 Description: Se agregan campos precio defalt y concepto que anula. Se saca
 			la limitaci�n de no poder modificar conceptos marcados como Internos
 			porque ahora es necesario poder cargarles el precio default.

 Revision: 2
 Author: jconcheyro
 Date: 19/12/2006
 Description: Se agregan campos y combo de monedas.

  Revision: 3
 Author: FSandi
 Date: 19-07-2007
 Description: Se agregan los campos Libro Contable y Columna Libro Contable (SS 512)


 Revision 5
 Author: mbecerra
 Date: 24-Abril-2009
 Description:	(Ref.Facturaci�n Electr�nica)
                Se agregan dos campos a la tabla ConceptosMovimiento
                (y por ende al ABM): EsDescuentoRecargo (tinyint), y
                AfectaFechaCorte (BIT)

    	14-Mayo-2009
            	Se agrega el campo: Incluir en Nota de Cobro Infractor.


 Revision 6
 Author: jjofre
 Date: 19-Febrero-2010
 Description: (Ref. SS 708 LibroAUxPeaje - ObtenerMovimientos No Facturados)
              Se agrega el campo de Tipo Bit 'EsEliminable', en donde su estado
              define si aparecera o no en la opcion 'Eliminar Movimientos No Facturados'
              desde la facturacion.

 Revision 7
 Author: plaza
 Date: 22/02/2010
 Description: SS 802. Se agrega la opcion 'NINGUNO' al combo de libro contable
              y al combo columna contable, para poder aceptar sin especificar
              estos campos. (Antes habia que seleccionar uno si o si).

 Revision		: 8
 Author			: Nelson Droguett Sierra
 Date			: 12-Mayo-2010
 Description	: (Ref. Fase 2)
                  Agregar Campos : CodigoConcesionaria, Incluir NotaCobroDirecta,
                  UsaDescripcionTipoConcepto, Activo, CodigoTipoConcepto.
                  Ademas agregar campos de auditoria :
                  FechaHoraCreacion,UsuarioCreacion,FechaHoraModificacion,UsuarioModificacion,
                  FechaHoraDesactivacion,UsuarioDesactivacion
                  Ordenar los controles en la interfaz y reasignar los TabOrder de los mismos.

 Revision		: 9
 Author			: Nelson Droguett Sierra
 Date			: 02-Junio-2010
 Description	: (Ref. Fase 2)
            	Al agregar un concepto nuevo, el check activo siempre se presentara
                marcado y deshabilitado para modificarlo.

 Revision		: 10
 Author			: Nelson Droguett Sierra
 Date			: 02-Junio-2010
 Description	: (Ref. Fase 2)
                Al desactivar un concepto, se debe mostrar un cartel de confirmacion
                segun caso de prueba ref. a FAC.CU.103 Conceptos Movimientos, Escenario
                alternativo N� 6.

 Revision		: 11
 Author			: Nelson Droguett Sierra
 Date			: 05-Agosto-2010
 Description	: (Ref. Fase 2)
                De estar asociado a facturacion el concepto debe :
                    Exigir Libro Contable y Columna de ese libro
                    Exigir definir un Tipo de Concepto que no este asociado a otro concepto
                    activo para la misma concesionaria.
                    Exigir Concesionaria
                    Exigir GrupoImprenta
                De No estar asociado a facturacion :
                    No debe pedir GrupoImprenta
                    No debe Pedir TipoConcepto
                    No debe pedir concesionaria
                    No debe pedir LibroContable ni columna contable.

                Los valores de estos datos que esten vacios seran grabados con NULL
                	(a excepcion de la concesionaria que sera por default CN)
                El concepto que anula NUNCA es obligatorio.
                Los campos obligatorios son de color celeste los opcionales son de color blanco.

    06-01-2011 : mbecerra
                Se permite la inserci�n de c�digo intermedio

 Revision		  : 12
 Author			  : Nelson Droguett Sierra
 Date			    : 19-Enero-2011
 Description	: (Ref.Fase2.SF016)
                Se modifica la validacion para que al grabar una modificacion
                valide la existencia de un conceptomovimiento asociado al
                tipoconceptomovimiento pero ignorando el concepto que esta siendo
                modificado.
Firma         : SF016-NDR-20110119

 Revision		  : 13
 Author			  : Nelson Droguett Sierra
 Date			    : 28-Enero-2011
 Description	: (Ref.Fase2.SF046)
                El precio no es obligatorio, debe salir en color blanco.
                El Concepto que Anula, no debe ser obligatorio.
                Concesionaria el combobox debe salir ordenado alfab�ticamente.
Firma         : SF046-NDR-20110128

    Firma: SS_964_PDO_20110531

    Descripci�n: SS 964 - Despliegue de Ventana de Facturaci�n
        - Se a�ade la gesti�n del nuevo campo FacturacionInmediata

Revision        : 14
Author          : Nelson Droguett Sierra
Date            : 17-Junio-2011
Description     : (Ref.Fase2)
Firma           : FASE2-NDR-20110617
-----------------------------------------------------------------------------}
unit ABMConceptosMovimiento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask, ComCtrls, PeaProcs, validate,
  Dateedit, Util, ADODB, DMConnection, DPSControls, Variants, PeaTypes,
  VariantComboBox,Math, StrUtils, sysUtilsCN, PeaProcsCN, frmMuestraMensaje;

type
  TFormConceptosMovimientos = class(TForm)
    dblConceptos: TAbmList;
    pnlCampos: TPanel;
    lblDescripcion: TLabel;
    lblGrupoImprenta: TLabel;
    lblPrecio: TLabel;
    lblLibroContable: TLabel;
    lblColumnaContable: TLabel;
    lblDescuentoRecargo: TLabel;
    lblCodigoConcepto: TLabel;
    lblConceptoAnula: TLabel;
    lblMoneda: TLabel;
    lblCodigoConceptoMaestro: TLabel;
    lblConcesionaria: TLabel;       
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    chkIncFactura: TCheckBox;
    chkIncNotaCredito: TCheckBox;
    chkIncDebito: TCheckBox;
    chkIncBoleta: TCheckBox;
    chkAfectoIVA: TCheckBox;
    chkAfectaFechaCorte: TCheckBox;
    chkFacturacionInmediata: TCheckBox;
    chkIncNotaCobroDirecta: TCheckBox;
    neGrupoImprenta: TNumericEdit;
    nePrecio: TNumericEdit;
    txt_CodigoConcepto: TNumericEdit;
    cbMoneda: TComboBox;
    cbColumna: TVariantComboBox;
    cbLibro: TVariantComboBox;
    vcbEsDesctoRecargo: TVariantComboBox;
    cbConcesionaria: TVariantComboBox;
    tblConceptosMovimiento: TADOTable;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    AbmToolbar1: TAbmToolbar;
    tblConceptosMovimientoMaestro: TADOTable;
    pnlMaestro: TPanel;
    cbbConceptoMaestro: TVariantComboBox;
    cbbConceptoAnula: TVariantComboBox;
    procedure dblConceptosDblClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure dblConceptosClick(Sender: TObject);
    procedure dblConceptosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblConceptosEdit(Sender: TObject);
    procedure dblConceptosRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure dblConceptosDelete(Sender: TObject);
    procedure dblConceptosInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
{INICIO: TASK_004_JMA_20160412 - Para Eliminar
    procedure FormShow(Sender: TObject);}
{TERMINO: TASK_004_JMA_20160412}
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function dblConceptosProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure cbLibroChange(Sender: TObject);
    procedure chkClicked(Sender: TObject);
    procedure cbConcesionariaChange(Sender: TObject);								
    procedure dblConceptosSectionClick(Sender: TObject; SectionIndex: Integer);    
    procedure cbbConceptoMaestroChange(Sender: TObject);
  private    
    FTipoComprobante: string;    
    bDesactivoAhora:Boolean;
    bEditarConcesionaria:Boolean;
    Filtro: String;
    cargando: Boolean;        
    procedure Limpiar_Campos;
    procedure PermitirEditar(Permitir : Boolean);  
{INICIO: TASK_004_JMA_20160412 - Para Eliminar
    function ConceptoQueAnulaEsValido(aCodigoConcepto: integer): boolean;
{TERMINO: TASK_004_JMA_20160412}
    procedure ClickEffect(Clicked: Boolean);
    procedure ObtenerConceptosMaestroPorConcesionaria;
    procedure ObtenerConceptosQuePuedenAnular(CodigoConcepto, ConceptoAnula: Integer);
    function ValidarCodigoConceptoRepetido():Boolean;
  public
    function Inicializa(TipoComprobante: String = ''; EditarConcesionaria: Boolean = True): boolean;        
  end;

var
  FormConceptosMovimientos: TFormConceptosMovimientos;

implementation

{$R *.DFM}

function TFormConceptosMovimientos.Inicializa(TipoComprobante: String = ''; EditarConcesionaria: Boolean = True): boolean;
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_CARGANDO_CONCEPTOS = 'Error buscando datos de los conceptos de movimientos';
    MSG_ERROR_CARGANDO_MONEDAS = 'Se ha producido un error cargando la lista de monedas';
    MSG_ERROR_CARGANDO_LIBROS_CONTABLES = 'Se ha producido un error cargando la lista de Libros Contables';
    MSG_ERROR_CARGANDO_CONCESIONARIAS = 'Se ha producido un error cargando la lista de concesionarias';
    MSG_ERROR_CARGANDO_TIPOS_CONCEPTOS_MOVIMIENTOS = 'Se ha producido un error cargando la lista de Tipos Conceptos Movimientos';
    MSG_ERROR_CARGANDO_CONCEPTOS_MAESTROS = 'Se ha producido un error cargando la lista de Conceptos Maestros';    
var 
    sp: TADOStoredProc;       
begin
    Result := False;
    cargando:= True;
    try
        TPanelMensajesForm.MuestraMensaje('Acci�n: Cargando Datos.',  True);
        TPanelMensajesForm.MuestraMensaje('Espere Por Favor ...');
    
        //Centrando el Formulario
        CenterForm(self);

        //Inicfializo variable de objeto
        FTipoComprobante := TipoComprobante;    
        bDesactivoAhora:=False;
        bEditarConcesionaria := EditarConcesionaria;

        //Selecciono tipo de filtro
        Filtro := 'CodigoConcesionaria = %s';
        if FTipoComprobante = 'NK' then begin
            Filtro := 'CodigoConcesionaria = %s AND IncluirNotaCobro = 1';
            Caption := Caption + ' - [Filtro: Incluir en NK]';
        end;

        //Verifico si estoy trabajando con Conceptos Maestros o para Concesionarias
        if bEditarConcesionaria = True then
        begin
            Self.Caption := 'Mantenimiento de Conceptos de Movimientos por Concesionaria';
            lblConcesionaria.visible := True;
            cbConcesionaria.visible := True;
            pnlMaestro.Visible := True;

            {CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria,False);
            if cbConcesionaria.Items.Count > 0 then
                cbConcesionaria.ItemIndex := 0;}

            try    
                try
                    TPanelMensajesForm.MuestraMensaje('Cargando Concesionarias. Espere Por Favor ...');
                    sp := TADOStoredProc.Create(nil);
                    sp.Connection := DMConnections.BaseCAC;
                    sp.ProcedureName := 'COMM_Concesionarias_SELECT';
                    sp.Parameters.Refresh;
                    sp.Parameters.ParamByName('@OrderingField').Value := 'Descripcion';
                    sp.Parameters.ParamByName('@Order').Value := 'ASC';
                    sp.Open;
                    while not sp.eof do begin
                        cbConcesionaria.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('CodigoConcesionaria').AsInteger);
                        sp.Next;
                    end;
                except 
                    on E: Exception do begin
                        MsgBoxErr(MSG_ERROR_CARGANDO_CONCESIONARIAS, Caption, Caption, MB_ICONSTOP);
                        Exit;
                    end;
                end;
            finally
                if (sp <> nil) and (sp.State = dsBrowse) then
                   sp.Close;
                FreeAndNil(sp);
            end;
            if cbConcesionaria.Items.Count > 0 then
                cbConcesionaria.ItemIndex := 0; 

            dblConceptos.Table := tblConceptosMovimiento;           
            if not OpenTables([tblConceptosMovimiento]) then begin
                ShowMsgBoxCN(MSG_ERROR, MSG_ERROR_CARGANDO_CONCEPTOS, MB_ICONERROR, Application.MainForm);
                exit;
            end;
            tblConceptosMovimiento.Filter := Format(Filtro, [cbConcesionaria.Value]);
            tblConceptosMovimiento.Filtered := True;
            
            TPanelMensajesForm.MuestraMensaje('Cargando Conceptos. Espere Por Favor ...');
            ObtenerConceptosMaestroPorConcesionaria();     
        
        end
        else begin
            Self.Caption := 'Mantenimiento de Conceptos de Movimientos Maestro';
            dblConceptos.Table := tblConceptosMovimientoMaestro;
            if not OpenTables([tblConceptosMovimientoMaestro]) then begin
                ShowMsgBoxCN(MSG_ERROR, MSG_ERROR_CARGANDO_CONCEPTOS, MB_ICONERROR, Application.MainForm);
                exit;
            end;
            tblConceptosMovimientoMaestro.IndexFieldNames := 'Descripcion';
            tblConceptosMovimientoMaestro.Tag := 2;
        end;
        dblConceptos.Reload;

        //Cargo Combo de Moneda
        try    
            try                 
                cbMoneda.Items.Add(MN_MONEDA_PESO);
                sp := TADOStoredProc.Create(nil);
                sp.Connection := DMConnections.BaseCAC;
                sp.ProcedureName := 'ObtenerListaMonedas';
                sp.Parameters.Refresh;
                sp.Open;
                while not sp.eof do begin
                    if Length(Trim(sp.FieldByName('Moneda').AsString)) > 0 then
                       cbMoneda.Items.Add(sp.FieldByName('Moneda').AsString);
                    sp.Next;
                end;          
            except 
                on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_CARGANDO_MONEDAS, Caption, Caption, MB_ICONSTOP );
                    Exit;
                end;
            end;
        finally
            if (sp <> nil) and (sp.State = dsBrowse) then
               sp.Close;
            FreeAndNil(sp);
        end;
    
        // me fijo si este usuario tiene permiso de agregar nuevos conceptos
        if ExisteAcceso('AGREGAR_CONCEPTOS_MOVIMIENTOS') then
            ABMToolBar1.Habilitados := ABMToolBar1.Habilitados + [btAlta]
        else
            ABMToolBar1.Habilitados := ABMToolBar1.Habilitados - [btAlta];

        //Cargo combo libros contables
        cblibro.Items.Clear;
        cblibro.Items.Add('Ninguno',0);
        try
            try
                sp := TADOStoredProc.Create(nil);
                sp.Connection := DMConnections.BaseCAC;
                sp.ProcedureName := 'ObtenerListadoLibrosContables';
                sp.Parameters.Refresh;
                sp.Open;
                while not sp.eof do begin
                    cblibro.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('IdLibroContable').AsInteger);
                    sp.Next;
                end;
            except on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_CARGANDO_LIBROS_CONTABLES, Caption, Caption, MB_ICONSTOP );
                    Exit;
                end;
            end;
        finally
            if (sp <> nil) and (sp.State = dsBrowse) then
                sp.Close;
            FreeAndNil(sp);
        end;
        
        Notebook.PageIndex := 0;
        cargando:= False;
    finally
        TPanelMensajesForm.OcultaPanel;
    end;
    if dblConceptos.Table.RecordCount > 0 then
       dblConceptosClick(nil);
   	Result := True;
end;   

procedure TFormConceptosMovimientos.ObtenerConceptosMaestroPorConcesionaria;
resourcestring
    MSG_ERROR_CARGANDO_CONCEPTOS_MAESTROS = 'Se ha producido un error cargando la lista de Conceptos Maestros Por Concesionaria';
var
    sp: TADOStoredProc;
    
begin
    try    
        try
            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'FACT_ConceptosMovimientoMaestroPorConcesionaria_SELECT';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoConcesionaria').Value := cbConcesionaria.Value;
            sp.Parameters.ParamByName('@Order').Value := 'ASC';
            sp.Open;
            cbbConceptoMaestro.Clear;
            while not sp.eof do begin                    
                cbbConceptoMaestro.Items.Add('(' + sp.FieldByName('CodigoConcepto').AsString + ') ' + sp.FieldByName('Descripcion').AsString,
                                                    sp.FieldByName('CodigoConcepto').AsInteger);
                sp.Next;
            end;
        except 
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_CARGANDO_CONCEPTOS_MAESTROS, Caption, Caption, MB_ICONSTOP );
                Exit;
            end;
        end;
    finally
        if (sp <> nil) and (sp.State = dsBrowse) then
           sp.Close;
        FreeAndNil(sp);
    end; 
end;

procedure TFormConceptosMovimientos.dblConceptosClick(Sender: TObject);
resourcestring
    MSG_ERROR_CARGANDO_DATOS_CONTABLES = 'Ocurrio un error al cargar los datos contables del Concepto';
var                         
    tabla : TADOTable;
    sp : TADOStoredProc;

begin
    if not cargando then
    begin
        try
            Cursor := crHourGlass;
            Limpiar_Campos();
            tabla := dblConceptos.Table as TADOTable;

            if bEditarConcesionaria then  
                cbbConceptoMaestro.ItemIndex := cbbConceptoMaestro.Items.IndexOfValue(tabla.FieldByName('CodigoConceptoMaestro').AsInteger);               

            txt_CodigoConcepto.Value	    := tabla.FieldByName('CodigoConcepto').AsInteger;
            txt_Descripcion.text		    := Trim(tabla.FieldByName('Descripcion').AsString);
            nePrecio.Value                  := iif(tabla.FieldByName('Precio').AsVariant = Null,0,tabla.FieldByName('Precio').AsVariant / 100);
            cbMoneda.ItemIndex              := cbMoneda.Items.IndexOf(tabla.FieldByName('Moneda').AsString);
            neGrupoImprenta.Value		    := tabla.FieldByName('CodigoGrupoImprenta').AsInteger;
            ObtenerConceptosQuePuedenAnular(tabla.FieldByName('CodigoConcepto').Value, IIf(tabla.FieldByName('ConceptoAnula').Value=Null, 0, tabla.FieldByName('ConceptoAnula').Value));
            cbbConceptoAnula.ItemIndex      := cbbConceptoAnula.Items.IndexOfValue(IIf(tabla.FieldByName('ConceptoAnula').Value=null,0,tabla.FieldByName('ConceptoAnula').Value));
            vcbEsDesctoRecargo.ItemIndex	:= vcbEsDesctoRecargo.Items.IndexOfValue(tabla.FieldByName('EsDescuentoRecargo').AsInteger);
            //chkAjuste.Checked 			    := tabla.FieldByName('EsAjuste').AsBoolean;                 // TASK_141_MGO_20170216
            chkAfectoIVA.Checked 		    := tabla.FieldByName('AfectoIva').AsBoolean;
            //chkIncNotaCobro.Checked 	    := tabla.FieldByName('IncluirNotaCobro').AsBoolean;             // TASK_141_MGO_20170216
            chkIncFactura.Checked 		    := tabla.FieldByName('IncluirFactura').AsBoolean;
            chkIncNotaCredito.Checked 	    := tabla.FieldByName('IncluirNotaCredito').AsBoolean;
            chkIncDebito.Checked 		    := tabla.FieldByName('IncluirNotaDebito').AsBoolean;
            chkIncBoleta.Checked 		    := tabla.FieldByName('IncluirBoleta').AsBoolean;
            //chkIncluirNotaInfractor.Checked	:= tabla.FieldByName('IncluirNotaInfractor').AsBoolean;     // TASK_141_MGO_20170216
            chkIncNotaCobroDirecta.Checked  := tabla.FieldByName('IncluirNotaCobroDirecta').AsBoolean;
            chkFacturacionInmediata.Checked	:= tabla.FieldByName('FacturacionInmediata').AsBoolean;
            chkAfectaFechaCorte.Checked		:= tabla.FieldByName('AfectaFechaCorte').AsBoolean;
{INICIO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro
            chkEsEliminable.Checked		    := tabla.FieldByName('EsEliminable').AsBoolean;
TERMINO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro}

            if bEditarConcesionaria then
            begin
                try
                    try
                        if QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ConceptoAsociadoAFacturacion(' + Trim(txt_CodigoConcepto.Text) + ' )')>0 then begin

                            sp := TADOStoredProc.Create(nil);
                            sp.Connection := DMConnections.BaseCAC;
                            sp.ProcedureName := 'ObtenerDatosContablesConcepto';
                            sp.Parameters.Refresh;
                            sp.Parameters.ParamByName('@CodigoConcepto').Value := txt_CodigoConcepto.Value;
                            sp.Open;

                            if Not sp.IsEmpty then begin
                                cbLibro.ItemIndex  := cbLibro.Items.IndexOfValue(sp.FieldByName('IdLibroContable').AsInteger);
                                cbLibroChange(self);                                
                                cbColumna.ItemIndex := cbColumna.Items.IndexOfValue(sp.FieldByName('IdColumnaLibroContable').AsInteger);                                                                   
                            end;
                        end;
                    except on E: Exception do begin
                            MsgBoxErr(MSG_ERROR_CARGANDO_DATOS_CONTABLES, Caption, Caption, MB_ICONSTOP );
                            Exit;
                        end;
                    end;
                finally
                    if (sp <> nil) and (sp.State = dsBrowse) then
                        sp.Close;
                    FreeAndNil(sp);
                end;
            end
            else begin
                cbLibro.ItemIndex  := cbLibro.Items.IndexOfValue(tabla.FieldByName('CodigoLibroContable').AsInteger);
                cbLibroChange(self);                                
                cbColumna.ItemIndex := cbColumna.Items.IndexOfValue(tabla.FieldByName('CodigoColumnaLibroContable').AsInteger);                
            end;

            ClickEffect(False); //Necesario para actualizar los enables de los libros
            
            
            if ExisteAcceso('MODIFICACION_CONCEPTOS_MOVIMIENTOS') then
                ABMToolBar1.Habilitados := ABMToolBar1.Habilitados + [btModi]
            else
                ABMToolBar1.Habilitados := ABMToolBar1.Habilitados - [btModi];

            if ExisteAcceso('ELIMINACION_CONCEPTOS_MOVIMIENTOS') and (not dblConceptos.Table.FieldByName('Interno').AsBoolean) then
                ABMToolBar1.Habilitados := ABMToolBar1.Habilitados + [btBaja]
            else
                ABMToolBar1.Habilitados := ABMToolBar1.Habilitados - [btBaja];
{INICIO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro}
            if not tabla.FieldByName('EsEliminable').AsBoolean then
                AbmToolbar1.Habilitados := ABMToolBar1.Habilitados - [btBaja];
{TERMINO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro}
        finally
            Cursor := crDefault;
        end;
    end;
end;      

procedure TFormConceptosMovimientos.BtnAceptarClick(Sender: TObject);
resourcestring
    CAPTION_VALIDAR_CONCEPTO_MOVIMIENTO     = 'Validar datos del Concepto de Movimiento';
    CAPTION_ACTUALIZAR_CONCEPTO_MOVIMIENTO  = 'Actualizar Concepto de Movimiento';
    MSG_CODIGO_CONCEPTO_MOVIMIENTO_MAESTRO  = 'Seleccione un C�digo de Concepto Maestro';
    MSG_DESCRIPCION                         = 'El Concepto de Movimiento debe contener una descripci�n.';
    MSG_PRECIO                              = 'El precio debe ser mayor o igual a 0.';
    MSG_ERROR_LIBRO_CONTABLE                = 'El c�digo de Libro Contable es inv�lido';
    MSG_ACTUALIZAR_CONCEPTO_MOVIMIENTO      = 'No se pudieron actualizar los datos del Concepto de Movimiento';
    MSG_ERROR_GRUPO_IMPRENTA                = 'El n�mero de Grupo de Imprenta debe tener un valor';
    MSG_ERROR_CONCEPTO_ANULA                = 'El c�digo del concepto que anula es inv�lido';
    MSG_ERROR_COLUMNA_LIBRO_CONTABLE        = 'El c�digo de columna de Libro Contable es inv�lido';
    MSG_SQL_CODIGO                          =  'SELECT IsNull(MAX(CodigoConcepto), 0) FROM ConceptosMovimiento (NOLOCK)';
var 
    sp: TADOStoredProc;
begin

    if ValidarCodigoConceptoRepetido() = False then Exit;

    if (bEditarConcesionaria = True) and (ValidateControls([cbbConceptoMaestro],
                                                           [cbbConceptoMaestro.ItemIndex >= 0],
                                                           CAPTION_VALIDAR_CONCEPTO_MOVIMIENTO,
                                                           [MSG_CODIGO_CONCEPTO_MOVIMIENTO_MAESTRO]) = False) then Exit;



    if ValidateControls([txt_Descripcion, nePrecio, neGrupoImprenta, cblibro, cbcolumna],
                       [Trim(txt_Descripcion.Text) <> '',
                        nePrecio.Value >= 0,
                        (Not neGrupoImprenta.Enabled) or (neGrupoImprenta.Enabled AND (neGrupoImprenta.Value > 0)),
{INICIO: TASK_074_JMA_20161116
                        not(cblibro.Enabled) or (cblibro.Enabled AND cblibro.value > 0),
                        not(cbColumna.Enabled) or (cbColumna.Enabled AND cbColumna.value > 0)],
}
                        not(cblibro.Enabled) or (cblibro.Enabled AND cblibro.value >= 0),
                        not(cbColumna.Enabled) or (cbColumna.Enabled AND cbColumna.value >= 0)],
{TERMINO: TASK_074_JMA_20161116}
                        CAPTION_VALIDAR_CONCEPTO_MOVIMIENTO,
                       [MSG_DESCRIPCION, MSG_PRECIO, MSG_ERROR_GRUPO_IMPRENTA, MSG_ERROR_LIBRO_CONTABLE,
                        MSG_ERROR_COLUMNA_LIBRO_CONTABLE]) = False then Exit;

    Cursor := crHourGlass;
    try
        
        Try

            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseCAC;
            if bEditarConcesionaria then
            begin
                if dblConceptos.Estado = Modi then
                    sp.ProcedureName := 'FACT_ConceptosMovimientoConcesionaria_UPDATE'
                else
                    sp.ProcedureName := 'FACT_ConceptosMovimientoConcesionaria_INSERT';
            end
            else
            begin
                if dblConceptos.Estado = Modi then
                    sp.ProcedureName := 'FACT_ConceptosMovimientoMaestro_UPDATE'
                else
                    sp.ProcedureName := 'FACT_ConceptosMovimientoMaestro_INSERT';
            end;
            sp.Parameters.Refresh;

            sp.Parameters.ParamByName('@CodigoConcepto').Value := Trunc(txt_CodigoConcepto.Value);
            sp.Parameters.ParamByName('@Descripcion').Value := Trim(txt_Descripcion.text);
            sp.Parameters.ParamByName('@Interno').Value := False;
            //sp.Parameters.ParamByName('@EsAjuste').Value := chkAjuste.Checked;                // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@EsAjuste').Value := (vcbEsDesctoRecargo.ItemIndex > 0); // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@CodigoGrupoImprenta').Value := neGrupoImprenta.ValueInt;
            //sp.Parameters.ParamByName('@IncluirNotaCobro').Value := chkIncNotaCobro.Checked;                          // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@IncluirNotaCobro').Value := (chkIncBoleta.Checked or chkIncFactura.Checked);    // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@IncluirFactura').Value := chkIncFactura.Checked;
            sp.Parameters.ParamByName('@IncluirNotaCredito').Value := chkIncNotaCredito.Checked;
            sp.Parameters.ParamByName('@IncluirNotaDebito').Value := chkIncDebito.Checked;
            //sp.Parameters.ParamByName('@IncluirBoleta').Value := False;               // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@IncluirBoleta').Value := chkIncBoleta.Checked;  // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@AfectoIva').Value := chkAfectoIVA.Checked;
            sp.Parameters.ParamByName('@ConceptoAnula').Value := cbbConceptoAnula.Value;
            sp.Parameters.ParamByName('@Precio').Value := Round(nePrecio.Value * 100);
            sp.Parameters.ParamByName('@Moneda').Value := cbMoneda.Text;
            sp.Parameters.ParamByName('@EsDescuentoRecargo').Value := vcbEsDesctoRecargo.Value;
            sp.Parameters.ParamByName('@AfectaFechaCorte').Value := chkAfectaFechaCorte.Checked;
            //sp.Parameters.ParamByName('@IncluirNotaInfractor').Value := chkIncluirNotaInfractor.Checked;  // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@IncluirNotaInfractor').Value := False;                              // TASK_141_MGO_20170216
            sp.Parameters.ParamByName('@EsEliminable').Value := True;
            sp.Parameters.ParamByName('@FacturacionInmediata').Value := chkFacturacionInmediata.Checked;
            sp.Parameters.ParamByName('@IncluirNotaCobroDirecta').Value := chkIncNotaCobroDirecta.Checked;

            if dblConceptos.Estado = Alta then begin
                sp.Parameters.ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
                sp.Parameters.ParamByName('@FechaHoraCreacion').Value := NowBase(DMConnections.BaseCAC);
                sp.Parameters.ParamByName('@UsuarioModificacion').Value := Null;
                sp.Parameters.ParamByName('@FechaHoraModificacion').Value := Null;
            end else begin
                sp.Parameters.ParamByName('@UsuarioCreacion').Value := Null;
                sp.Parameters.ParamByName('@FechaHoraCreacion').Value := Null;
                sp.Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
                sp.Parameters.ParamByName('@FechaHoraModificacion').Value := NowBase(DMConnections.BaseCAC);
            end;

            if cbLibro.Enabled and cbColumna.Enabled then begin
              sp.Parameters.ParamByName('@CodigoLibroContable').Value := cblibro.Value;
              sp.Parameters.ParamByName('@CodigoColumnaLibroContable').Value := cbColumna.Value;
            end
            else begin
              sp.Parameters.ParamByName('@CodigoLibroContable').Value := 0;
              sp.Parameters.ParamByName('@CodigoColumnaLibroContable').Value := 0;
            end;

            if bEditarConcesionaria then  
            begin          
                sp.Parameters.ParamByName('@CodigoConcesionaria').Value := cbConcesionaria.Value;
                sp.Parameters.ParamByName('@CodigoConceptoMaestro').Value := cbbConceptoMaestro.Value;
            end;

            sp.ExecProc;

        except
            On E: Exception do begin                              
               MsgBoxErr(MSG_ACTUALIZAR_CONCEPTO_MOVIMIENTO, E.message, CAPTION_ACTUALIZAR_CONCEPTO_MOVIMIENTO, MB_ICONSTOP);
            end;
        end;

    finally
        PermitirEditar(False);   
        Cursor := crDefault;
        FreeAndNil(sp);
    end;
end;

procedure TFormConceptosMovimientos.BtnCancelarClick(Sender: TObject);
begin
   	PermitirEditar(False);
end;

procedure TFormConceptosMovimientos.dblConceptosInsert(Sender: TObject);
begin              
    dblConceptos.Estado 	:= Alta;      
   	Limpiar_Campos;                                        
    PermitirEditar(True);     
end;

procedure TFormConceptosMovimientos.dblConceptosEdit(Sender: TObject);
begin
    dblConceptos.Estado 	:= modi;    
    PermitirEditar(True);
end;

procedure TFormConceptosMovimientos.dblConceptosDelete(Sender: TObject);
resourcestring
    CAPTION_ELIMINAR_CONCEPTO_MOVIMIENTO = 'Eliminar Concepto de Movimiento';
    MSG_ELIMINAR_CONCEPTO_MOVIMIENTO = '�Est� seguro de querer eliminar el Concepto de Movimiento seleccionado?';
    MSG_ERROR_ELIMINAR_CONCEPTO_MOVIMIENTO = 'No se pudo eliminar el Concepto de Movimiento seleccionado.';
    MSG_ERROR_GRUPO_ASOCIADO_A_CICLOS_FACTURACION = 'Puede ser que el Concepto de Movimiento est� asociado a Movimientos de Cuenta.';
    MSG_ERROR_O = ' O ';
    MSG_ERROR_GRUPO_ASOCIADO_A_CONVENIO = 'Puede ser que el Concepto de Movimiento est� asociado a Lineas de Comprobantes.';
    MSG_ERROR_CONCEPTO_ASOCIADO_CONCESIONARIA = 'Puede ser que el Concepto de Movimiento est� asociado un Concepto de MOvimiento de Concesionaria.';
var
    tabla : TADOTable; 
    sp: TADOStoredProc;  
begin          

    Screen.Cursor := crHourGlass;
    try
    If MsgBox(MSG_ELIMINAR_CONCEPTO_MOVIMIENTO, CAPTION_ELIMINAR_CONCEPTO_MOVIMIENTO, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try
            tabla := dblConceptos.Table as TADOTable;   
            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseCAC;
            if bEditarConcesionaria then
                sp.ProcedureName := 'FACT_ConceptosMovimientoConcesionaria_DELETE'
            else
                sp.ProcedureName := 'FACT_ConceptosMovimientoMaestro_DELETE';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoConcepto').Value := tabla.FieldByName('CodigoConcepto').AsInteger;
            sp.ExecProc;
{INICIO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro}
            if sp.Parameters.ParamByName('@RETURN_VALUE').Value = -1 then
            begin
                if ContainsStr(sp.Parameters.ParamByName('@ErrorDescription').value,'FK_ConceptosMovimiento_ConceptosMovimientoMaestro') then
                    MsgBox('No se puede eliminar el concepto, porque esta siendo utilizado por una concesionaria', 'Eliminar Conceptos', MB_OK);
            end;
{TERMINO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro}
            tabla.Close;
            tabla.Open;
        except
            on E: EDatabaseError do begin
                if bEditarConcesionaria then
                    MsgBoxErr(MSG_ERROR_ELIMINAR_CONCEPTO_MOVIMIENTO + CRLF
                        + MSG_ERROR_GRUPO_ASOCIADO_A_CONVENIO + MSG_ERROR_O + CRLF
                        + MSG_ERROR_GRUPO_ASOCIADO_A_CICLOS_FACTURACION, E.Message,
                        CAPTION_ELIMINAR_CONCEPTO_MOVIMIENTO, MB_ICONSTOP)
                else
                    MsgBoxErr(MSG_ERROR_ELIMINAR_CONCEPTO_MOVIMIENTO + CRLF
                        + MSG_ERROR_CONCEPTO_ASOCIADO_CONCESIONARIA, E.Message,
                        CAPTION_ELIMINAR_CONCEPTO_MOVIMIENTO, MB_ICONSTOP)
            end;
            On E: Exception do begin
                MsgBoxErr(MSG_ERROR_ELIMINAR_CONCEPTO_MOVIMIENTO, e.message, CAPTION_ELIMINAR_CONCEPTO_MOVIMIENTO, MB_ICONSTOP);
            end;
        end;
    end;    
    finally
        if (sp <> nil) and (sp.State = dsBrowse) then
           sp.Close;
        FreeAndNil(sp);
        AbmToolbar1.Refresh;
                Screen.Cursor      := crDefault;
    end;
end;

procedure TFormConceptosMovimientos.Limpiar_Campos();
begin
	txt_CodigoConcepto.Clear;
	txt_Descripcion.Clear;
    nePrecio.Value                  := 0;
    neGrupoImprenta.Value		    := 0;
    cbMoneda.ItemIndex              := 0;   
    cbbConceptoAnula.ItemIndex      := 0;
    vcbEsDesctoRecargo.ItemIndex    := 0;
    cbLibro.ItemIndex               := 0;
    cbLibroChange(self);
    cbColumna.ItemIndex             := 0;
    
	//chkAjuste.Checked 			:= False;   // TASK_141_MGO_20170216
    chkAfectoIVA.Checked 		    := False;
	//chkIncNotaCobro.Checked 	    := False;   // TASK_141_MGO_20170216
	chkIncFactura.Checked 		    := False;    
	chkIncNotaCredito.Checked 	    := False;
	chkIncDebito.Checked 		    := False;     
	chkIncBoleta.Checked 		    := False;
	//chkIncluirNotaInfractor.Checked := False; // TASK_141_MGO_20170216
    chkIncNotaCobroDirecta.Checked	:= False;
    chkFacturacionInmediata.Checked := False;	
    chkAfectaFechaCorte.Checked     := False;
end;

procedure TFormConceptosMovimientos.dblConceptosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoConcepto').AsInteger, 15));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFormConceptosMovimientos.dblConceptosRefresh(Sender: TObject);
begin
	 if dblConceptos.Empty then Limpiar_Campos();
end;

procedure TFormConceptosMovimientos.dblConceptosSectionClick(Sender: TObject;  SectionIndex: Integer);
var
    Tabla : TADOTable;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        Tabla := dblConceptos.Table as TADOTable;
        case SectionIndex of
            0: begin
                case Tabla.Tag of
                    1: begin
                        Tabla.IndexFieldNames := 'CodigoConcepto DESC';
                        Tabla.Tag := -1;
                    end;
                else
                    Tabla.IndexFieldNames := 'CodigoConcepto';
                    Tabla.Tag := 1;
                end;
            end;

            1: begin
                case Tabla.Tag of
                    2: begin
                        Tabla.IndexFieldNames := 'Descripcion DESC';
                        Tabla.Tag := -2;
                    end;
                else
                    Tabla.IndexFieldNames := 'Descripcion';
                    Tabla.Tag := 2;
                end;
            end;
        end;

        dblConceptos.ReLoad;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TFormConceptosMovimientos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormConceptosMovimientos.AbmToolbar1Close(Sender: TObject);
begin
	Close;
end;

procedure TFormConceptosMovimientos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormConceptosMovimientos.cbbConceptoMaestroChange(Sender: TObject);
  var 
   CodigoConceptoMaestro : Integer;
   CodigoConcesionaria :Integer;
   sp : TADOStoredProc;
begin
    CodigoConcesionaria := cbConcesionaria.Value;
    CodigoConceptoMaestro := cbbConceptoMaestro.Value;   
    if (CodigoConcesionaria > 0) and (CodigoConceptoMaestro > 0) then
    begin
        txt_CodigoConcepto.Value := StrToInt(IntToStr(CodigoConcesionaria) + IntToStr(CodigoConceptoMaestro));
        if dblConceptos.Estado = Alta then
        begin

            try
                try
                    sp := TADOStoredProc.Create(nil);
                    sp.Connection := DMConnections.BaseCAC;
                    sp.ProcedureName := 'FACT_ConceptosMovimientoMaestro_SELECT';
                    sp.Parameters.Refresh;
                    sp.Parameters.ParamByName('@CodigoConcepto').value := CodigoConceptoMaestro;            
                    sp.Open;

                    txt_Descripcion.text		    := Trim(sp.FieldByName('Descripcion').AsString);
                    nePrecio.Value                  := iif(sp.FieldByName('Precio').AsVariant = Null,0,sp.FieldByName('Precio').AsVariant / 100);
                    cbMoneda.ItemIndex              := cbMoneda.Items.IndexOf(sp.FieldByName('Moneda').AsString);
                    neGrupoImprenta.Value		    := sp.FieldByName('CodigoGrupoImprenta').AsInteger;
                    ObtenerConceptosQuePuedenAnular (sp.FieldByName('CodigoConcepto').Value, IIf(sp.FieldByName('ConceptoAnula').Value=null,0,sp.FieldByName('ConceptoAnula').Value));
                    if IIf(sp.FieldByName('ConceptoAnula').Value=null,0,sp.FieldByName('ConceptoAnula').Value) <> 0 then
                        cbbConceptoAnula.ItemIndex  := cbbConceptoAnula.Items.IndexOfValue(sp.FieldByName('ConceptoAnula').Value);
                    vcbEsDesctoRecargo.ItemIndex	:= vcbEsDesctoRecargo.Items.IndexOfValue(sp.FieldByName('EsDescuentoRecargo').AsInteger);
                    //chkAjuste.Checked 			    := sp.FieldByName('EsAjuste').AsBoolean;                // TASK_141_MGO_20170216
                    chkAfectoIVA.Checked 		    := sp.FieldByName('AfectoIva').AsBoolean;
                    //chkIncNotaCobro.Checked 	    := sp.FieldByName('IncluirNotaCobro').AsBoolean;            // TASK_141_MGO_20170216
                    chkIncFactura.Checked 		    := sp.FieldByName('IncluirFactura').AsBoolean;
                    chkIncNotaCredito.Checked 	    := sp.FieldByName('IncluirNotaCredito').AsBoolean;
                    chkIncDebito.Checked 		    := sp.FieldByName('IncluirNotaDebito').AsBoolean;
                    chkIncBoleta.Checked 		    := sp.FieldByName('IncluirBoleta').AsBoolean;
                    //chkIncluirNotaInfractor.Checked	:= sp.FieldByName('IncluirNotaInfractor').AsBoolean;    // TASK_141_MGO_20170216
                    chkIncNotaCobroDirecta.Checked  := sp.FieldByName('IncluirNotaCobroDirecta').AsBoolean;
                    chkFacturacionInmediata.Checked	:= sp.FieldByName('FacturacionInmediata').AsBoolean;
                    chkAfectaFechaCorte.Checked		:= sp.FieldByName('AfectaFechaCorte').AsBoolean;
{INICIO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro
                    chkEsEliminable.Checked		    := sp.FieldByName('EsEliminable').AsBoolean;
TERMINO: TASK_026_JMA_20160621_Incidencias Concepto Movimiento Maestro}

                    cbLibro.ItemIndex := cbLibro.Items.IndexOfValue(sp.FieldByName('CodigoLibroContable').AsInteger);
                    cbLibroChange(self);
                    cbColumna.ItemIndex := cbColumna.Items.IndexOfValue(sp.FieldByName('CodigoColumnaLibroContable').AsInteger);
                                       
                    ClickEffect(False);   //Necesario para Cambiar los enables de los libros
                except
                    On E: Exception do begin
                        MsgBoxErr('Error cargando parametros del ConceptoMaestro', e.message, 'Error!', MB_ICONSTOP);
                    end;    
                end;
            finally
                if (sp <> nil) and (sp.State = dsBrowse) then
                   sp.Close;
                FreeAndNil(sp);
            end;
                         
        end;
    end;
end;

procedure TFormConceptosMovimientos.cbConcesionariaChange(Sender: TObject);
begin
    try
        TPanelMensajesForm.MuestraMensaje('Acci�n: Filtrar Conceptos por Concesionaria.',  True);
        TPanelMensajesForm.MuestraMensaje('Filtrando Conceptos por Concesionaria. Espere Por Favor ...');

            //if tblConceptosMovimiento.Active then Close; Eliminar esta linea  TASK_004_JMA_20160412

            tblConceptosMovimiento.Filter := Format(Filtro, [cbConcesionaria.Value]);
            tblConceptosMovimiento.Filtered := True;
            OpenTables([tblConceptosMovimiento]);
            (dblConceptos.Table as TADOTable).IndexFieldNames := 'Descripcion';
            (dblConceptos.Table as TADOTable).Tag := 2;
            dblConceptos.ReLoad;

            ObtenerConceptosMaestroPorConcesionaria();
        
    finally
        TPanelMensajesForm.OcultaPanel;
    end;
end;

procedure TFormConceptosMovimientos.cbLibroChange(Sender: TObject);
resourcestring
    MSG_ERROR_CARGANDO_COLUMNAS_CONTABLES = 'Se ha producido un error cargando la lista de Columnas Contables';
var 
    sp: TADOStoredProc;
begin
    cbColumna.Items.Clear;
    cbColumna.Items.Add('Ninguna',0);    // TASK_074_JMA_20161116

    if cbLibro.Value > 0 then begin
        try
            try
                sp := TADOStoredProc.Create(nil);
                sp.Connection := DMConnections.BaseCAC;
                sp.ProcedureName := 'ObtenerListadoColumnasContables';
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@CodigoLibroContable').Value := cbLibro.Value;
                sp.Open;
                while not sp.eof do begin
                    cbColumna.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('IdColumnaLibroContable').AsInteger);
                    sp.Next;
                end;            
            except on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_CARGANDO_COLUMNAS_CONTABLES, Caption, Caption, MB_ICONSTOP );
                    Exit;
                end;
            end;
        finally
            if (sp <> nil) and (sp.State = dsBrowse) then
               sp.Close;
            FreeAndNil(sp);
        end;
    //end else begin                            // TASK_074_JMA_20161116
    //    cbColumna.Items.Add('Ninguna',0);     // TASK_074_JMA_20161116
    end;

    {Revision 1}
    cbColumna.ItemIndex := 0;
    if pnlCampos.Enabled then
    begin
        if (cbLibro.ItemIndex > 0)then 
        begin
            cbColumna.Enabled := True;
            cbColumna.Color := clWindow;
        end
        else 
        begin
            cbColumna.Enabled := False;
            cbColumna.Color := clBtnFace;
        end;
    end;
end;

function TFormConceptosMovimientos.dblConceptosProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
    if bEditarConcesionaria then
        Texto  := tblConceptosMovimiento.FieldByName('Descripcion').AsString
    else
        Texto  := tblConceptosMovimientoMaestro.FieldByName('Descripcion').AsString
end;

procedure TFormConceptosMovimientos.dblConceptosDblClick(Sender: TObject);
begin
    //dblConceptos.Estado := Normal;
    dblConceptos.Estado := Modi;
end;

procedure TFormConceptosMovimientos.chkClicked(Sender: TObject);
begin
    if pnlCampos.Enabled then
    begin
        ClickEffect(True);
    end;
end;

procedure TFormConceptosMovimientos.ClickEffect(Clicked: Boolean);
begin
    //if (chkIncNotaCobro.Checked OR chkIncFactura.Checked OR chkIncNotaCredito.Checked OR chkIncDebito.Checked OR  // TASK_141_MGO_20170216
    //         chkIncBoleta.Checked OR chkIncluirNotaInfractor.Checked OR chkIncNotaCobroDirecta.Checked)   then    // TASK_141_MGO_20170216
    if (chkIncFactura.Checked OR chkIncNotaCredito.Checked OR chkIncDebito.Checked OR                               // TASK_141_MGO_20170216
             chkIncBoleta.Checked OR chkIncNotaCobroDirecta.Checked)   then                                         // TASK_141_MGO_20170216
    begin
        if Not (cbLibro.Enabled OR cbColumna.Enabled) then begin
            if (dblConceptos.Estado = modi) and Clicked then
                ShowMessage('Esta cambiando este concepto de Est�tico a Din�mico'+Chr(13)+Chr(10)+
                            'Debe establecer los siguientes valores :'+Chr(13)+Chr(10)+
                            ' - C�digoGrupoImprenta '+Chr(13)+Chr(10)+
                            ' - Libro Contable '+Chr(13)+Chr(10)+
                            ' - Columna Contable');

            cbLibro.Color			:=	$00FAEBDE;
            cbColumna.Color			:=	$00FAEBDE;
            neGrupoImprenta.Color	:=  $00FAEBDE;
            cbLibro.Enabled			:= True;
            neGrupoImprenta.Enabled := True;
        end;
        cbColumna.Enabled		:= False;
        if (cbLibro.Enabled = True) and (cbLibro.ItemIndex >0) then
            cbColumna.Enabled		:= True;
    end
    else begin

        if (dblConceptos.Estado = modi) and Clicked then
            ShowMessage('Esta cambiando este concepto de Din�mico a Est�tico');

        cbLibro.ItemIndex			:= 0;
        cbLibroChange(self);
        cbColumna.ItemIndex		    := 0;
        neGrupoImprenta.Value		:= 0;
        cbLibro.Color				:= clBtnFace;
        cbColumna.Color			    := clBtnFace;
        neGrupoImprenta.Color		:= clBtnFace;
        cbLibro.Enabled			    :=	False;
        cbColumna.Enabled			:=	False;
        neGrupoImprenta.Enabled	    := 	False;
    end;
end;

procedure TFormConceptosMovimientos.PermitirEditar(Permitir: Boolean);
begin    
    if Permitir then
    begin
        Notebook.PageIndex   := 1;        
        dblConceptos.Enabled := False; 

        pnlCampos.Enabled := True;
        if bEditarConcesionaria then
        begin
            cbConcesionaria.Enabled := False;
            pnlMaestro.Enabled := True;
        end;
        if dblConceptos.Estado = Alta then
        begin
            txt_CodigoConcepto.ReadOnly := False;
            txt_CodigoConcepto.Color := clWindow;
            txt_CodigoConcepto.SetFocus;
        end
        else begin
            txt_CodigoConcepto.ReadOnly := True;
            txt_CodigoConcepto.Color := clBtnFace;
            txt_Descripcion.SetFocus;
        end;
	    txt_Descripcion.Color       := clWindow;
        nePrecio.Color              := clWindow;           
        cbMoneda.Color              := clWindow;   
        cbbConceptoAnula.Color       := clWindow;
        vcbEsDesctoRecargo.Color    := clWindow;
        ClickEffect(False);

        { INICIO : TASK_141_MGO_20170216
        if dblConceptos.Estado = Alta then  //Solo cuando voy a crear uno nuevo
        begin
            chkIncNotaCobro.Checked := FTipoComprobante = 'NK';
            chkIncNotaCobro.Enabled := not (FTipoComprobante = 'NK');
        end;
        } // FIN : TASK_141_MGO_20170216            
    end
    else begin
        Notebook.PageIndex          := 0;
        
        pnlCampos.Enabled           := False; //Esto debe ante de dblConceptos.Enabled := True; para evitar que se dispare el evento chkIncNotaCobroClick
        dblConceptos.Enabled        := True;
        dblConceptos.Estado         := Normal;
        dblConceptos.Reload;
	    dblConceptos.SetFocus;  

        if bEditarConcesionaria then
        begin
            cbConcesionaria.Enabled := True;
            pnlMaestro.Enabled      := False;
        end;                       

        txt_CodigoConcepto.Color    := clBtnFace;
        txt_Descripcion.Color       := clBtnFace;
        nePrecio.Color              := clBtnFace;
        neGrupoImprenta.Color       := clBtnFace;
        cbMoneda.Color              := clBtnFace;
        cbbConceptoAnula.Color       := clBtnFace;
        vcbEsDesctoRecargo.Color    := clBtnFace;
        cbLibro.Color               := clBtnFace;
        cbColumna.Color             := clBtnFace;
    end;
end;

procedure TFormConceptosMovimientos.ObtenerConceptosQuePuedenAnular(CodigoConcepto, ConceptoAnula: Integer);
resourcestring
    ERROR = 'Error cargando Conceptos Anulables';
var 
   CodigoConcesionaria :Integer;
   sp : TADOStoredProc;   
begin

    try
        try
            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'FACT_ConceptosQuePuedenAnular_SELECT';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoConcepto').value := CodigoConcepto;
            sp.Parameters.ParamByName('@ConceptoAnula').value := ConceptoAnula;
            if bEditarConcesionaria then
            begin
                CodigoConcesionaria := cbConcesionaria.Value;
                if (CodigoConcesionaria < 0 ) then
                begin
                     MsgBoxErr(ERROR, Caption, Caption, MB_ICONSTOP );
                     Exit;
                end;
                sp.Parameters.ParamByName('@CodigoConcesionaria').value := CodigoConcesionaria;
            end;
            sp.Open;
            cbbConceptoAnula.Clear;
            cbbConceptoAnula.Items.Add('NINGUNO', 0);
            while not sp.eof do begin                    
                cbbConceptoAnula.Items.Add('(' + sp.FieldByName('CodigoConcepto').AsString + ') ' + sp.FieldByName('Descripcion').AsString,
                                                    sp.FieldByName('CodigoConcepto').AsInteger);
                sp.Next;
            end;
            except
                on E: Exception do begin
                    MsgBoxErr(ERROR, Caption, Caption, MB_ICONSTOP );
                    Exit;
                end;
            end;
    finally
        if (sp <> nil) and (sp.State = dsBrowse) then
           sp.Close;
        FreeAndNil(sp);
    end;                 
    
end;

function TFormConceptosMovimientos.ValidarCodigoConceptoRepetido():Boolean;
resourcestring
    CODIGO_REPETIDO                         = 'C�digo Concepto ya utilizado';
    CAPTION_VALIDAR_CONCEPTO_MOVIMIENTO     = 'Validar datos del Concepto de Movimiento';
var
    i: integer;
begin
    Result:= True;
{INICIO: TASK_004_JMA_20160412 - Para Eliminar
    if not txt_CodigoConcepto.Enabled and (Length(Trim(txt_CodigoConcepto.Text))>0)then
TERMINO: TASK_004_JMA_20160412 - Para Eliminar}
    if not txt_CodigoConcepto.ReadOnly and (Length(Trim(txt_CodigoConcepto.Text))>0)then
    begin
        dblConceptos.table.First;
        for I := 0 to dblConceptos.table.recordcount - 1 do
        begin
            if IntToStr(dblConceptos.table.FieldByName('CodigoConcepto').Value) = Trim(txt_CodigoConcepto.text) then
             begin
                ValidateControls([txt_CodigoConcepto],
                        [False],
                        CAPTION_VALIDAR_CONCEPTO_MOVIMIENTO,
                        [CODIGO_REPETIDO]);
                Result:= False;
                Break;
            end;
            dblConceptos.table.Next;
        end;
    end;
end;

end.
