object frmGeneracionGastosGestionCobranza: TfrmGeneracionGastosGestionCobranza
  Left = 177
  Top = 156
  Caption = 'frmGeneracionGastosGestionCobranza'
  ClientHeight = 362
  ClientWidth = 724
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 740
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object pnlFiltros: TPanel
    Left = 0
    Top = 0
    Width = 724
    Height = 113
    Align = alTop
    TabOrder = 0
    DesignSize = (
      724
      113)
    object lblNKEspecifica: TLabel
      Left = 470
      Top = 35
      Width = 126
      Height = 39
      Caption = 'Para generar gastos para una Nota de Cobro espec'#237'fica haga click'
      WordWrap = True
    end
    object lblAqui: TLabel
      Left = 569
      Top = 60
      Width = 30
      Height = 13
      Cursor = crHandPoint
      AutoSize = False
      Caption = 'aqu'#237
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = lblAquiClick
    end
    object Label8: TLabel
      Left = 468
      Top = 12
      Width = 37
      Height = 13
      Caption = 'Mostrar'
    end
    object Label9: TLabel
      Left = 560
      Top = 12
      Width = 68
      Height = 13
      Caption = 'comprobantes'
    end
    object gbConvenio: TGroupBox
      Left = 3
      Top = 58
      Width = 455
      Height = 51
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 73
        Height = 13
        Caption = 'RUT del Cliente'
      end
      object Label2: TLabel
        Left = 249
        Top = 8
        Width = 103
        Height = 13
        Caption = 'Convenios del Cliente'
      end
      object peRUTCliente: TPickEdit
        Left = 8
        Top = 24
        Width = 201
        Height = 21
        Enabled = True
        TabOrder = 0
        OnChange = peRUTClienteChange
        EditorStyle = bteTextEdit
        OnButtonClick = peRUTClienteButtonClick
      end
      object cbConvenio: TVariantComboBox
        Left = 249
        Top = 24
        Width = 201
        Height = 19
        Style = vcsOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 1
        OnDrawItem = cbConvenioDrawItem
        Items = <>
      end
    end
    object btnFiltrar: TButton
      Left = 635
      Top = 6
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Aplicar Filtros'
      Default = True
      TabOrder = 4
      OnClick = btnFiltrarClick
    end
    object chkSoloEnviadosSerbanc: TCheckBox
      Left = 468
      Top = 76
      Width = 154
      Height = 25
      Caption = 'Solo Enviados a Serbanc'
      TabOrder = 3
      WordWrap = True
    end
    object gbComprobantes: TGroupBox
      Left = 3
      Top = 0
      Width = 455
      Height = 62
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 188
        Height = 13
        Caption = 'Ver Comprobantes vencidos a la fecha:'
      end
      object Label4: TLabel
        Left = 264
        Top = 6
        Width = 168
        Height = 13
        Caption = 'Total a Pagar de los Comprobantes'
      end
      object Label5: TLabel
        Left = 232
        Top = 25
        Width = 30
        Height = 13
        Caption = 'Desde'
      end
      object Label6: TLabel
        Left = 344
        Top = 25
        Width = 28
        Height = 13
        Caption = 'Hasta'
      end
      object Label7: TLabel
        Left = 16
        Top = 25
        Width = 28
        Height = 13
        Caption = 'Hasta'
      end
      object lblFechaVencimiento: TLabel
        Left = 15
        Top = 45
        Width = 215
        Height = 13
        Caption = 'Comprobantes vencidos hasta el 99/99/9999'
      end
      object deFechaFiltro: TDateEdit
        Left = 72
        Top = 21
        Width = 122
        Height = 21
        AutoSelect = False
        TabOrder = 0
        OnExit = deFechaFiltroExit
        Date = -693594.000000000000000000
      end
      object neImporteHasta: TNumericEdit
        Left = 376
        Top = 21
        Width = 73
        Height = 21
        MaxLength = 10
        TabOrder = 2
        Value = 9999999999.000000000000000000
      end
      object neImporteDesde: TNumericEdit
        Left = 264
        Top = 21
        Width = 73
        Height = 21
        TabOrder = 1
      end
    end
    object btnLimpiarFiltros: TButton
      Left = 635
      Top = 32
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Limpiar Filtros'
      TabOrder = 5
      OnClick = btnLimpiarFiltrosClick
    end
    object neCantidad: TNumericEdit
      Left = 507
      Top = 8
      Width = 47
      Height = 21
      TabOrder = 2
      Value = 999999.000000000000000000
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 316
    Width = 724
    Height = 46
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      724
      46)
    object lblCantidadMostrada: TLabel
      Left = 8
      Top = 24
      Width = 98
      Height = 13
      Caption = 'lblCantidadMostrada'
    end
    object btnGenerar: TButton
      Left = 545
      Top = 11
      Width = 89
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Generar Gastos'
      TabOrder = 0
      OnClick = btnGenerarClick
    end
    object btnSalir: TButton
      Left = 638
      Top = 11
      Width = 89
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btnSalirClick
    end
    object pbProgreso: TProgressBar
      Left = 8
      Top = 5
      Width = 513
      Height = 17
      TabOrder = 2
      Visible = False
    end
  end
  object dblComprobantesVEncidos: TDBListEx
    Left = 0
    Top = 113
    Width = 590
    Height = 203
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 25
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'Marcado'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 35
        Header.Caption = 'Tipo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'TipoComprobante'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 85
        Header.Caption = 'N'#250'mero'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'NumeroComprobante'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Emisi'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'FechaEmision'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Vencimiento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'FechaVencimiento'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Total Comp.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'TotalComprobante'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Total a Pagar'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'TotalAPagar'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 85
        Header.Caption = 'Env. Serbanc'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'EnviadoSerbanc'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'F.Envio Serbanc'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblComprobantesVEncidosColumns0HeaderClick
        FieldName = 'FechaActualizadoSerbanc'
      end>
    DataSource = dsComprobantesVencidos
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = dblComprobantesVEncidosDblClick
    OnDrawText = dblComprobantesVEncidosDrawText
    OnKeyPress = dblComprobantesVEncidosKeyPress
  end
  object pnlSeleccion: TPanel
    Left = 590
    Top = 113
    Width = 134
    Height = 203
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      134
      203)
    object btnInvertirSeleccion: TButton
      Left = 5
      Top = 85
      Width = 125
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Invertir Seleccion Actual'
      TabOrder = 3
      OnClick = btnInvertirSeleccionClick
    end
    object btnDeselectAll: TButton
      Left = 5
      Top = 58
      Width = 125
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Deseleccionar Todos'
      TabOrder = 2
      OnClick = btnDeselectAllClick
    end
    object btnSelectAll: TButton
      Left = 5
      Top = 31
      Width = 125
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Seleccionar Todos'
      TabOrder = 1
      OnClick = btnSelectAllClick
    end
    object btnMarcar: TButton
      Left = 5
      Top = 4
      Width = 125
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Marcar / Desmarcar'
      TabOrder = 0
      OnClick = btnMarcarClick
    end
  end
  object spObtenerListaComprobantesVencidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerListaComprobantesVencidos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ImporteDesde'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ImporteHasta'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@SoloEnviadosSerbanc'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 6350
      end>
    Left = 376
    Top = 288
  end
  object tmrConsulta: TTimer
    OnTimer = tmrConsultaTimer
    Left = 392
    Top = 140
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 358
    Top = 139
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 326
    Top = 139
  end
  object dsComprobantesVencidos: TDataSource
    DataSet = cdsComprobantesVencidos
    Left = 408
    Top = 288
  end
  object cdsComprobantesVencidos: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 440
    Top = 288
    object cdsComprobantesVencidosTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object cdsComprobantesVencidosNumeroComprobante: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object cdsComprobantesVencidosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 30
    end
    object cdsComprobantesVencidosFechaEmision: TDateTimeField
      FieldName = 'FechaEmision'
    end
    object cdsComprobantesVencidosFechaVencimiento: TDateTimeField
      FieldName = 'FechaVencimiento'
    end
    object cdsComprobantesVencidosTotalComprobante: TIntegerField
      FieldName = 'TotalComprobante'
    end
    object cdsComprobantesVencidosTotalAPagar: TIntegerField
      FieldName = 'TotalAPagar'
    end
    object cdsComprobantesVencidosEnviadoSerbanc: TBooleanField
      FieldName = 'EnviadoSerbanc'
    end
    object cdsComprobantesVencidosFechaActualizadooSerbanc: TDateTimeField
      FieldName = 'FechaActualizadoSerbanc'
    end
    object cdsComprobantesVencidosMarcado: TBooleanField
      FieldName = 'Marcado'
    end
    object cdsComprobantesVencidosCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
  end
  object Imagenes: TImageList
    Left = 480
    Top = 288
    Bitmap = {
      494C010102000400100010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object spAgregarGastoGestionCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarGastoGestionCobranza'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalEnviado'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@FechaGasto'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoCalculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@ImporteGasto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 232
    Top = 232
  end
end
