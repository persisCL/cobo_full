object frmParametrosProcesoFacturacion: TfrmParametrosProcesoFacturacion
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Par'#225'metros para el Proceso de Facturaci'#243'n'
  ClientHeight = 301
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  DesignSize = (
    438
    301)
  PixelsPerInch = 96
  TextHeight = 13
  object lbl10: TLabel
    Left = 8
    Top = 70
    Width = 197
    Height = 13
    Caption = 'Valor M'#237'nimo de la Nota de Cobro:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl13: TLabel
    Left = 8
    Top = 97
    Width = 326
    Height = 13
    Caption = 'Valor M'#237'nimo de la Nota de Cobro para Pago Autom'#225'tico:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl11: TLabel
    Left = 8
    Top = 124
    Width = 230
    Height = 13
    Caption = 'Valor M'#237'nimo de la Deuda del Convenio:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl12: TLabel
    Left = 8
    Top = 151
    Width = 336
    Height = 13
    Caption = 'Valor M'#237'nimo de la Nota de Cobro para Convenios de Baja:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl14: TLabel
    Left = 8
    Top = 205
    Width = 195
    Height = 13
    Caption = 'Meses para Forzar Nota de Cobro:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl17: TLabel
    Left = 8
    Top = 232
    Width = 208
    Height = 13
    Caption = 'D'#237'as M'#237'nimos entre Notas de Cobro:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl1: TLabel
    Left = 8
    Top = 8
    Width = 416
    Height = 39
    Caption = 
      'Ingrese los par'#225'metros de facturaci'#243'n. Tenga en cuenta que una v' +
      'ez modificados, es obligatorio ejecutar una Pre-Facturaci'#243'n para' +
      ' el Grupo de Facturaci'#243'n deseado, antes de poder realizar una Fa' +
      'cturaci'#243'n.'
    WordWrap = True
  end
  object lbl2: TLabel
    Left = 8
    Top = 178
    Width = 166
    Height = 13
    Caption = 'Valor M'#237'nimo Importe Afecto:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object txtValMinNK: TNumericEdit
    Left = 365
    Top = 67
    Width = 65
    Height = 21
    TabOrder = 0
  end
  object txtValMinNKPagoAuto: TNumericEdit
    Left = 365
    Top = 94
    Width = 65
    Height = 21
    TabOrder = 1
  end
  object txtValMinDeuda: TNumericEdit
    Left = 365
    Top = 121
    Width = 65
    Height = 21
    TabOrder = 2
  end
  object txtValMinNKBaja: TNumericEdit
    Left = 365
    Top = 148
    Width = 65
    Height = 21
    TabOrder = 3
  end
  object txtMesesForzarNK: TNumericEdit
    Left = 365
    Top = 202
    Width = 65
    Height = 21
    TabOrder = 5
  end
  object txtDiasMinNK: TNumericEdit
    Left = 365
    Top = 229
    Width = 65
    Height = 21
    TabOrder = 6
  end
  object btnGuardar: TButton
    Left = 274
    Top = 268
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Guardar'
    Default = True
    TabOrder = 7
    OnClick = btnGuardarClick
    ExplicitTop = 243
  end
  object btnCancelar: TButton
    Left = 355
    Top = 268
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancelar'
    TabOrder = 8
    OnClick = btnCancelarClick
    ExplicitTop = 243
  end
  object txtValMinImpAfe: TNumericEdit
    Left = 365
    Top = 175
    Width = 65
    Height = 21
    TabOrder = 4
  end
end
