unit Financiamientos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, validate,
  Dateedit, Buttons, utilProc, DmiCtrls, util, ADODB, DMConnection, ComCtrls, BuscaClientes, navigator,
  DPSGrid, ImgList, Grids, FinanciamientoDeDueda, DB, DBGrids, ExtCtrls, peaTypes,
  peaprocs, DPSControls;

type
  TFrmFinanciamientos = class(TForm)
	btnSalir: TDPSButton;
	Panel1: TPanel;
	gbCuentasMorosas: TGroupBox;
	GroupBox4: TGroupBox;
    btnCaducar: TDPSButton;
	Label3: TLabel;
	peCodigoCliente: TPickEdit;
	ObtenerFinanciamientos: TADOStoredProc;
	Imagenes: TImageList;
	StatusBar: TStatusBar;
    ObtenerCliente: TADOStoredProc;
    dbgFinanciamientos: TDPSGrid;
    ProgressBar: TProgressBar;
    Label4: TLabel;
    deDesdeFecha: TDateEdit;
	dsFinanciamientos: TDataSource;
    gbCliente: TGroupBox;
    Label6: TLabel;
    lApellidoNombre: TLabel;
    LDomicilio: TLabel;
	btnGenerar: TDPSButton;
    CaducarFinanciamiento: TADOStoredProc;
    Label1: TLabel;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnSalirClick(Sender: TObject);
	procedure peCodigoClienteButtonClick(Sender: TObject);
	procedure peCodigoClienteChange(Sender: TObject);
	procedure dbgCuentasMorosasWillClickTitle(Sender: TDPSGrid;
	  Column: TColumn; var ExecuteAction: Boolean);
	procedure StatusBarDrawPanel(StatusBar: TStatusBar;
	  Panel: TStatusPanel; const Rect: TRect);
	procedure deDesdeFechaChange(Sender: TObject);
	procedure cbEstadoFinanciamientoChange(Sender: TObject);
	procedure dbgFinanciamientosTitleClick(Column: TColumn);
	procedure dbgFinanciamientosOrderChange(Sender: TDPSGrid;
	  var ACol: Integer; var OrderIndicator: TOrderIndicator);
	procedure dbgFinanciamientosWillClickTitle(Sender: TDPSGrid;
	  Column: TColumn; var ExecuteAction: Boolean);
	procedure dbgFinanciamientosDrawColumnCell(Sender: TObject;
	  const Rect: TRect; DataCol: Integer; Column: TColumn;
	  State: TGridDrawState);
	procedure btnGenerarClick(Sender: TObject);
	procedure btnCaducarClick(Sender: TObject);
    procedure dsFinanciamientosDataChange(Sender: TObject; Field: TField);
  private
	{ Private declarations }
	FApellido,
    FApellidoMaterno,
    FNombre: AnsiString;
	FIndexOrderly: Integer;
	FOrderIndicator: TOrderIndicator;
	procedure CargarFinanciamientos(aCamposOrden: AnsiString);

  public
	{ Public declarations }
	function inicializa(): Boolean;
  end;


implementation

{$R *.dfm}
function TFrmFinanciamientos.inicializa(): Boolean;
var i: Integer;
begin
	progressBar.parent := statusBar;

    with dbgFinanciamientos do begin
        Columns[0].Title.Caption := MSG_COL_TITLE_ESTADO;
        Columns[1].Title.Caption := MSG_COL_TITLE_FECHA;
        Columns[2].Title.Caption := MSG_COL_TITLE_CUENTA;
        Columns[3].Title.Caption := MSG_COL_TITLE_IMPORTE_FINANCIADO;
        Columns[4].Title.Caption := MSG_COL_TITLE_ENTREGA_INICIAL;
        Columns[5].Title.Caption := MSG_COL_TITLE_INTERES;
        Columns[6].Title.Caption := MSG_COL_TITLE_VIGENCIA;
        Columns[7].Title.Caption := MSG_COL_TITLE_IMPORTE_CUOTA;
        Columns[8].Title.Caption := MSG_COL_TITLE_CUOTAS;
        Columns[9].Title.Caption := MSG_COL_TITLE_CUOTAS_IMPAGAS;
        Columns[10].Title.Caption := MSG_COL_TITLE_CUOTAS_PAGAS;
        Columns[11].Title.Caption := MSG_COL_TITLE_PROXIMA_FACTURACION;
        Columns[12].Title.Caption := MSG_COL_TITLE_PROXIMO_VENCIMIENTO;
    end;

	Width  := GetFormClientSize(Application.MainForm).cx;
	Height := GetFormClientSize(Application.MainForm).cy;
	CenterForm(Self);

	for i:= 0 to StatusBar.Panels.Count - 1 do
		StatusBar.Panels[i].Text := '';
	deDesdeFecha.Date := NowBase(DMConnections.BaseCAC) - 365;
	btnGenerar.Enabled := false;
	btnCaducar.Enabled := false;

	Result := True;
end;

procedure TFrmFinanciamientos.CargarFinanciamientos(aCamposOrden: AnsiString);
resourcestring
    CAPTION_TOTAL_FINANCIAMIENTO = 'Total de Financiamientos: %d';
begin
	screen.Cursor := crHourGlass;

	with ObtenerFinanciamientos do begin
		try
			Close;
			Parameters.ParamByName('@CodigoPersona').value := peCodigoCliente.ValueInt;
			Parameters.ParamByName('@DesdeFecha').value := deDesdeFecha.Date;
			open;
			StatusBar.Panels[0].Text := format(CAPTION_TOTAL_FINANCIAMIENTO, [recordCount]);
			sort := aCamposOrden;
			btnCaducar.Enabled := (fieldByName('Estado').asString = 'Vigente') and (not isEmpty);
		finally
			screen.Cursor := crDefault;
		end;
	end;
end;

procedure TFrmFinanciamientos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFrmFinanciamientos.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFrmFinanciamientos.peCodigoClienteButtonClick(Sender: TObject);
var f: TFormBuscaClientes;
begin
	application.createForm(TFormBuscaClientes,F);
(*
	if F.Inicializa('', '', Fapellido, FApellidoMaterno, FNombre, 0) then begin
		F.ShowModal;
		if F.ModalResult = idOk then begin
			peCodigoCliente.value := F.Persona.CodigoPersona;
			peCodigoClienteChange(peCodigoCliente);
		end;
	end;
*)
	F.free;
end;

procedure TFrmFinanciamientos.peCodigoClienteChange(Sender: TObject);
begin
	if activeControl = sender then begin
		obtenerCliente.Parameters.paramByName('@CodigoCliente').Value := peCodigoCLiente.valueInt;
		obtenerCliente.open;
		if not ObtenerCliente.eof then begin
			FApellido := trim(obtenerCliente.FieldByName('Apellido').asString);
                        FApellidoMaterno := trim(obtenerCliente.FieldByName('ApellidoMaterno').asString);
			FNombre := trim(obtenerCliente.FieldByName('Nombre').asString);

                        if (ObtenerCliente.FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                            lApellidoNombre.Caption :=  ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre)
                        else
                            lApellidoNombre.Caption :=  ArmaRazonSocial(FApellido, FNombre);

                        if not (ObtenerCliente.FieldByName('CodigoDomicilio').IsNull) then
                            lDomicilio.Caption := ArmarDomicilioCompletoPorCodigo(ObtenerCliente.Connection,
                                                ObtenerCliente.FieldByName('CodigoPersona').Value,ObtenerCliente.FieldByName('CodigoDomicilio').Value)
                        else
                            lDomicilio.Caption := MSG_NO_ENCONTRO_DOMICILIO;

			btnGenerar.Enabled := true;
		end else begin
			FApellido := '';
			FNombre := '';
			lApellidoNombre.Caption := '';
			lDomicilio.Caption := '';
			btnGenerar.Enabled := False;
		end;
		obtenerCliente.close;
		CargarFinanciamientos('FechaAlta');
	end;
end;

procedure TFrmFinanciamientos.dbgCuentasMorosasWillClickTitle(
  Sender: TDPSGrid; Column: TColumn; var ExecuteAction: Boolean);
begin
	ExecuteAction := Column.FieldName <> 'Estado';
end;

procedure TFrmFinanciamientos.StatusBarDrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin
	Panel.Width := statusBar.width - 172;
	progressBar.BoundsRect := Classes.Rect(Rect.left - 1, Rect.top - 1, Rect.Left + Panel.Width - 1, Rect.bottom + 1);
end;


procedure TFrmFinanciamientos.deDesdeFechaChange(Sender: TObject);
begin
	if activeControl = sender then begin
        try
			DateToStr((Sender as TDateEdit).Date);
			CargarFinanciamientos('FechaAlta');
		except;
		end;
	end;
end;

procedure TFrmFinanciamientos.cbEstadoFinanciamientoChange(
  Sender: TObject);
begin
	CargarFinanciamientos('FechaAlta');
end;

procedure TFrmFinanciamientos.dbgFinanciamientosTitleClick(Column: TColumn);
begin
	FIndexOrderly := Column.Index;
	with dbgFinanciamientos do begin
		// Esta ordenado por un campo y ese campo es el presionado ahora cambiamos el orden
		// Si esta para ariba asignamos orden descendiente
		// Caso contrario asignamos el orden del campo presionado y por default ascendente
		if (ColumnOrderly > -1) and
		  (Column.FieldName = Columns.Items[ColumnOrderly].FieldName) and
		  (OrderIndicator = oiUp) then begin
			FOrderIndicator := oiDown;
			CargarFinanciamientos(Column.FieldName + ' DESC');
		end else begin
			FOrderIndicator := oiUp;
			CargarFinanciamientos(Column.FieldName);
		end;
	end;
end;

procedure TFrmFinanciamientos.dbgFinanciamientosOrderChange(
  Sender: TDPSGrid; var ACol: Integer;
  var OrderIndicator: TOrderIndicator);
begin
	OrderIndicator := FOrderIndicator;
	ACol := FIndexOrderly;
end;

procedure TFrmFinanciamientos.dbgFinanciamientosWillClickTitle(
  Sender: TDPSGrid; Column: TColumn; var ExecuteAction: Boolean);
begin
	ExecuteAction := Column.FieldName <> 'Estado';
end;

procedure TFrmFinanciamientos.dbgFinanciamientosDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var bmp: TBitMap;
begin
	if dbgFinanciamientos.DataSource.dataset.IsEmpty then exit;
	with (Sender as TDBGrid) do begin
		if (Column.FieldName = 'Estado') then begin
			canvas.FillRect(rect);
			bmp := TBitMap.Create;
			Imagenes.GetBitmap(1, Bmp);
			if Rect.Right <= bmp.Width then
				canvas.StretchDraw(Rect, bmp)
			else
				canvas.Draw(Rect.left + ((Rect.Right - Rect.left) - bmp.Width) div 2, Rect.top, bmp);
			bmp.Free;
		end;
	end;
end;

procedure TFrmFinanciamientos.btnGenerarClick(Sender: TObject);
begin
	if GenerarFinanciamiento(peCodigoCliente.ValueInt,
        ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre)) then
		CargarFinanciamientos('FechaAlta');
end;

procedure TFrmFinanciamientos.btnCaducarClick(Sender: TObject);
resourcestring
    MSG_CADUCAR_FINANCIAMIENTO = '�Est� seguro de caducar el financiamiento seleccionado?';
    MSG_ERROR_CADUCAR_FINANCIAMIENTO = 'No se pudo caducar el financiamiento seleccionado.';
    CAPTION_CADUCAR_FINANCIAMIENTO = 'Caducar Financiamientos';
begin
	if MsgBox(MSG_CADUCAR_FINANCIAMIENTO, CAPTION_CADUCAR_FINANCIAMIENTO, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			try
				CaducarFinanciamiento.Parameters.ParamByName('@CodigoCuenta').Value :=
				  ObtenerFinanciamientos.FieldByName('CodigoCuenta').asInteger;
				CaducarFinanciamiento.Parameters.ParamByName('@NumeroFinanciamiento').Value :=
				  ObtenerFinanciamientos.FieldByName('NumeroFinanciamiento').asInteger;
				CaducarFinanciamiento.ExecProc;
				CargarFinanciamientos('FechaAlta');
			except
				On E: Exception do begin
					MsgBoxErr(MSG_ERROR_CADUCAR_FINANCIAMIENTO, e.message, CAPTION_CADUCAR_FINANCIAMIENTO, MB_ICONSTOP);
				end;
			end;
		finally
			screen.Cursor := crDefault;
		end;
	end;
end;

procedure TFrmFinanciamientos.dsFinanciamientosDataChange(Sender: TObject;
  Field: TField);
begin
	if Field <> nil then
		btnCaducar.Enabled := (Sender as TDataSource).dataset.fieldByName('Estado').asString = 'Vigente';
end;

end.
