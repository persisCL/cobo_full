object ListaDeHistoricoCartasListaAmarillaForm: TListaDeHistoricoCartasListaAmarillaForm
  Left = 0
  Top = 55
  BorderStyle = bsDialog
  Caption = 
    'Listado de Convenios en Lista Amarilla - Hist'#243'rico de Env'#237'o de C' +
    'artas'
  ClientHeight = 317
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlFiltro: TPanel
    Left = 0
    Top = 0
    Width = 750
    Height = 57
    Align = alTop
    TabOrder = 0
    object lblConvenio: TLabel
      Left = 12
      Top = 12
      Width = 52
      Height = 13
      Caption = 'Convenio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblEstadoConvenio: TLabel
      Left = 12
      Top = 32
      Width = 38
      Height = 13
      Caption = 'Estado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDatoConvenio: TLabel
      Left = 80
      Top = 12
      Width = 45
      Height = 13
      Caption = 'Convenio'
    end
    object lblDatoEstadoConvenio: TLabel
      Left = 80
      Top = 32
      Width = 33
      Height = 13
      Caption = 'Estado'
    end
  end
  object grpHistorico: TGroupBox
    Left = 0
    Top = 57
    Width = 750
    Height = 233
    Align = alClient
    Caption = 'Hist'#243'rico de Cartas Enviadas'
    DockSite = True
    TabOrder = 1
    object dblHistoricoCartas: TDBListEx
      Left = 2
      Top = 15
      Width = 746
      Height = 216
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 100
          MaxWidth = 270
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Concesionaria'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          MinWidth = 100
          MaxWidth = 270
          Header.Caption = 'Fecha de Env'#237'o'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dblHistoricoCartasColumnsHeaderClick
          FieldName = 'FechaEnvio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 100
          MaxWidth = 200
          Header.Caption = 'Empresa de Correos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 100
          MaxWidth = 270
          Header.Caption = 'Motivo del Rechazo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Detalle'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 200
          MinWidth = 100
          MaxWidth = 500
          Header.Caption = 'Observaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Observacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          MinWidth = 100
          MaxWidth = 270
          Header.Caption = 'Fecha Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dblHistoricoCartasColumnsHeaderClick
          FieldName = 'FechaCreacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 100
          MaxWidth = 200
          Header.Caption = 'Usuario de Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'UsuarioCreacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 100
          MaxWidth = 270
          Header.Caption = 'Fecha Rechazo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dblHistoricoCartasColumnsHeaderClick
          FieldName = 'FechaModificacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 100
          MaxWidth = 200
          Header.Caption = 'Usuario Rechazo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'UsuarioModificacion'
        end>
      DataSource = dsHistoricoCartas
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dblHistoricoCartasDrawText
    end
  end
  object pnlPie: TPanel
    Left = 0
    Top = 290
    Width = 750
    Height = 27
    Align = alBottom
    AutoSize = True
    Constraints.MaxHeight = 30
    TabOrder = 2
    DesignSize = (
      750
      27)
    object btnSalir: TButton
      Left = 659
      Top = 1
      Width = 81
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Salir'
      ModalResult = 1
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object dsHistoricoCartas: TDataSource
    DataSet = spHistoricoCartas
    Left = 605
    Top = 15
  end
  object spHistoricoCartas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistoricoCartasListaAmarilla;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@IDConvenioInhabilitado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 637
    Top = 15
  end
end
