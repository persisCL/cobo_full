{********************************** Unit Header ********************************
File Name: ImprimirCarpetaLegalForm.pas
Author: nefernandez
Date Created: 12/03/2007
Language: ES-AR
Description: Formulario para presentar la opci�n de imprimir o no los Detalles
de Tr�nsitos en el Reporte de Convenios en Carpeta Legal
*******************************************************************************}

unit ImprimirCarpetaLegalForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmImprimirCarpetaLegal = class(TForm)
    lblMensaje: TLabel;
    chkImprimirDetalle: TCheckBox;
    btnAceptar: TButton;
    Button2: TButton;
    procedure chkImprimirDetalleClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FImprimirDetalleTransito: Integer;
    function Inicializar(MSG_IMPRIMIR: String; var ImprimirDetalleTransito: Integer): Boolean;
  end;

var
  frmImprimirCarpetaLegal: TfrmImprimirCarpetaLegal;

implementation

{$R *.dfm}

procedure TfrmImprimirCarpetaLegal.chkImprimirDetalleClick(Sender: TObject);
begin
  if chkImprimirDetalle.Checked = True then
      FImprimirDetalleTransito := 1
  else
      FImprimirDetalleTransito := 0;
end;

function TfrmImprimirCarpetaLegal.Inicializar(MSG_IMPRIMIR: String; var ImprimirDetalleTransito: Integer): Boolean;
begin
  lblMensaje.Caption := MSG_IMPRIMIR;
  if ImprimirDetalleTransito = 1 then
    chkImprimirDetalle.Checked := True
  else
    chkImprimirDetalle.Checked := False;

  Result := True;
end;

end.
