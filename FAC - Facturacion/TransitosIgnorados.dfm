object FrmTransitosIgnorados: TFrmTransitosIgnorados
  Left = 48
  Top = 198
  BorderStyle = bsDialog
  Caption = 'Tr'#225'nsitos Ignorados'
  ClientHeight = 373
  ClientWidth = 875
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Lista: TDBList
    Left = 0
    Top = 45
    Width = 529
    Height = 262
    TabStop = True
    TabOrder = 0
    Align = alLeft
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'109'#0'Punto Cobro'
      #0'103'#0'Fecha y Hora'
      #0'121'#0'Numero de Tag'
      #0'88'#0'Cant. Ignorado'
      #0'90'#0'ReValidaci'#243'n')
    RefreshTime = 0
    Table = ObtenerTransitosIgnorados
    Style = lbOwnerDrawFixed
    OnClick = ListaClick
    OnDblClick = ListaDblClick
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
  end
  object Panel3: TPanel
    Left = 529
    Top = 45
    Width = 346
    Height = 262
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel4: TPanel
      Left = 1
      Top = 3
      Width = 345
      Height = 258
      Hint = 'Carga el tr'#225'nsito para su validaci'#243'n'
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(Sin Imagen)'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      object Imagen: TImagePlus
        Left = 0
        Top = 0
        Width = 341
        Height = 254
        HorzScrollBar.Increment = 1
        VertScrollBar.Increment = 1
        Align = alClient
        AutoScroll = False
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        PorcentajeAcumulado = 100.000000000000000000
      end
    end
  end
  object spTotal: TStatusBar
    Left = 0
    Top = 349
    Width = 875
    Height = 24
    Panels = <>
    SimplePanel = True
  end
  object Panel7: TPanel
    Left = 0
    Top = 0
    Width = 875
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object Label1: TLabel
      Left = 15
      Top = 18
      Width = 173
      Height = 13
      Caption = 'Transacciones ignorados por: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -10
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbValidador: TComboBox
      Left = 194
      Top = 13
      Width = 231
      Height = 21
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -10
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnChange = cbValidadorChange
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 309
    Width = 875
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Panel6: TPanel
      Left = 641
      Top = 0
      Width = 234
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnAceptar: TDPSButton
        Left = 56
        Top = 10
        Width = 81
        Height = 24
        Caption = '&Validar'
        Default = True
        ModalResult = 1
        TabOrder = 0
        OnClick = btnAceptarClick
      end
      object btnCancelar: TDPSButton
        Left = 152
        Top = 10
        Width = 74
        Height = 24
        Cancel = True
        Caption = '&Cancelar'
        ModalResult = 2
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 307
    Width = 875
    Height = 2
    Align = alBottom
    TabOrder = 5
  end
  object ObtenerUsuariosConPermiso: TADOStoredProc
    Connection = DMConnections.BaseCOP
    ProcedureName = 'ObtenerUsuariosConPermiso'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@Funcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 24
    Top = 232
  end
  object ObtenerTransitosIgnorados: TADOStoredProc
    Connection = DMConnections.BaseCOP
    ProcedureName = 'ObtenerTransitosIgnorados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Validador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@IncluirReValidacion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 64
    Top = 232
  end
end
