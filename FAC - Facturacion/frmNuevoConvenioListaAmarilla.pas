{------------------------------------------------------------------------
        frmNuevoConvenioListaAmarilla

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description :       (Ref SS 660)
                    Permite ingresar un nuevo convenio a lista amarilla

    Author      : CQuezadaI
    Date        : 10-10-2012
    Firma       : SS_660_CQU_20121010
    Description : Modifica el mensaje cuando el convenio est� en lista amarilla

    Author      : CQuezadaI
    Date        : 11-07-2013
    Firma       : SS_660_CQU_20130711
    Description : Se hacen las modificaciones para considerar la Concesionaria

    Author		: MVillarroel
    Date		: 30/09/2013
	Firma       : SS_1120_MVI_20130930
	Descripcion : Se cambia la forma en la que se llena el combo cbConveniosCliente, para utilizar el procedimeinto generico CargarConveniosRUT.
              	  Se modifica el procedimiento cbConveniosClienteDrawItem para que llame al procedimiento generico ItemComboBoxColor.
                  Se comenta el procedimiento ItemComboBoxColor, ya que con el cambio no se utilizara.

	Author		: Mcabello
    Date		: 04/11/2013
    Firma		: SS_660_MCA_20131104
    Description	: cuando se ha buscado por un Rut y se escribe otro, se limpian los campos
                  se comenta la invocacion al SP ObtenerCliente ya que es invocado en la
                  funcion CargarConveniosRUT
-------------------------------------------------------------------------}
unit frmNuevoConvenioListaAmarilla;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BuscaClientes, StdCtrls, VariantComboBox, DmiCtrls, ExtCtrls, PeaProcs,
  DMConnection, DB, ADODB, PeaTypes, Util, UtilProc, SysUtilsCN;	// SS_660_CQU_20130711

type
  TNuevoConvenioListaAmarillaForm = class(TForm)
    peRUTCliente: TPickEdit;
    Label1: TLabel;
    vcbConveniosCliente: TVariantComboBox;
    btnBuscar: TButton;
    spObtenerCliente: TADOStoredProc;
    spObtenerConveniosCliente: TADOStoredProc;
    Label3: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    spAgregarConvenioAListaAmarilla: TADOStoredProc;
    lblConcesionaria: TLabel;							    // SS_660_CQU_20130711
    vcbConcesionarias: TVariantComboBox;					// SS_660_CQU_20130711
    spReglaValidacionListaAmarilla: TADOStoredProc;			// SS_660_CQU_20130711
    spObtenerConcesionarias: TADOStoredProc;				// SS_660_CQU_20130711
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure peRUTClienteKeyPress(Sender: TObject; var Key: Char);
    procedure btnBuscarClick(Sender: TObject);
    procedure vcbConveniosClienteDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure vcbConveniosClienteChange(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);   // SS_660_CQU_20130711
  private
    FUltimaBusqueda: TBusquedaCliente;
    FListaConveniosBaja: TStringList;
    procedure CargarConcesionariasDisponibles(CodigoConvenio : Integer);              // SS_660_CQU_20130711
    { Private declarations }
  public
    { Public declarations }
    function Inicializar() : Boolean;
  end;

var
  NuevoConvenioListaAmarillaForm: TNuevoConvenioListaAmarillaForm;

implementation

{$R *.dfm}

{------------------------------------------------------------------------
        ItemCBColor

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : procedimiento que permite cambiar el color de un elemento
                    de una combobox.

-------------------------------------------------------------------------}
procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor);
begin
    with (cmbBox as TVariantComboBox) do begin
        Canvas.Brush.color := ColorFondo;
        Canvas.FillRect(R);
        Canvas.Font.Color := ColorTexto;
        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption);
   end;
end;


{------------------------------------------------------------------------
        Inicializar

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Inicializa el formulario: variables, objetos, etc.

-------------------------------------------------------------------------}
function TNuevoConvenioListaAmarillaForm.Inicializar() : boolean;
resourcestring
    //MSG_CAPTION = 'Nuevo Convenio a Lista Amarilla';                                                  // SS_660_CQU_20130711
    MSG_CAPTION = 'Nuevo Convenio a Seguimiento en Lista Amarilla';                                     // SS_660_CQU_20130711
    ERROR_CONCESIONES   = 'No hay concesionarias definidas.';
begin
    FListaConveniosBaja := TStringList.Create;
    Self.Caption := MSG_CAPTION;
    CenterForm(Self);
    try                                                                                                 // SS_660_CQU_20130711
        // Cargo las Concesionarias                                                                     // SS_660_CQU_20130711
        spObtenerConcesionarias.Parameters.Refresh;                                                     // SS_660_CQU_20130711
        spObtenerConcesionarias.Parameters.ParamByName('@CodigoTipoConcesionaria').Value    := Null;    // SS_660_CQU_20130711
        spObtenerConcesionarias.Parameters.ParamByName('@FiltraPorConcepto').Value          := 0;       // SS_660_CQU_20130711
        spObtenerConcesionarias.Open;                                                                   // SS_660_CQU_20130711
        if spObtenerConcesionarias.IsEmpty then raise exception.Create(ERROR_CONCESIONES);              // SS_660_CQU_20130711
    except                                                                                              // SS_660_CQU_20130711
        on e:exception do begin                                                                         // SS_660_CQU_20130711
            MsgBoxErr('Problema al inicializar', e.Message, Caption, MB_ICONERROR);                     // SS_660_CQU_20130711
        end;                                                                                            // SS_660_CQU_20130711
    end;                                                                                                // SS_660_CQU_20130711
    Result:= True;
end;

{------------------------------------------------------------------------
        FormClose

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Limpia las variables utilizadas al cerrar el formulario.

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    FListaConveniosBaja.Free;
end;

{------------------------------------------------------------------------
        btnBuscarClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Obtiene el listado de convenios del cliente

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.btnBuscarClick(Sender: TObject);
resourcestring                                                                          // SS_660_CQU_20130711
    MSG_ERROR_RUT   =   'El Rut ingresado no es v�lido';                                // SS_660_CQU_20130711
    MSG_SIN_CONVENIO=   'No se encontraron Convenios para el Rut ingresado';            // SS_660_CQU_20130711
var                                                                                     // SS_660_CQU_20130711
    Rut : string;                                                                       // SS_660_CQU_20130711
    val : Integer;                                                                      //SS_1120_MVI_20130930
begin
    FListaConveniosBaja.Clear;
    vcbConveniosCliente.Clear;

    try
		Screen.Cursor := crHourglass;

        Rut := Trim(PadL(peRUTCLiente.Text, 9, '0'));                                   // SS_660_CQU_20130711
        if not ValidarRUT(DMConnections.BaseCAC, Rut) then begin                        // SS_660_CQU_20130711
            MsgBoxBalloon(MSG_ERROR_RUT, Caption, MB_ICONINFORMATION, peRUTCliente);    // SS_660_CQU_20130711
            Exit;                                                                       // SS_660_CQU_20130711
        end;                                                                            // SS_660_CQU_20130711

        //BEGIN:SS_660_MCA_20131104
	        // Obtenemos el cliente seleccionado
    	    //with spObtenerCliente do begin
        	//    Close;
        	//    Parameters.ParamByName('@CodigoCliente').Value      := NULL;
	        //    Parameters.ParamByName('@CodigoDocumento').Value    := TIPO_DOCUMENTO_RUT;
            //	  Parameters.ParamByName('@NumeroDocumento').Value    := Trim(PadL(peRUTCLiente.Text, 9, '0')); // SS_660_CQU_20130711
	        //    Parameters.ParamByName('@NumeroDocumento').Value    := Rut;                                     // SS_660_CQU_20130711
    	    //    Open;
        	//    if IsEmpty then begin
        	//        Close;
        	//        if not (peRUTCLiente.Text = EmptyStr) then MsgBox(MSG_SIN_CONVENIO, Caption, MB_ICONINFORMATION + MB_OK);   // SS_660_CQU_20130711
        	//	        Exit;
	        //    end;
        	//end;
        //END:SS_660_MCA_20131104

//BEGIN:SS_1120_MVI_20130930 --------------------------------------------------------------------------------------------------------------------
        //obtenemos los convenios del cliente
//        spObtenerConveniosCliente.Parameters.ParamByName('@CodigoCliente').Value := spObtenerCliente.FieldByName('CodigoCliente').AsInteger;
//        spObtenerConveniosCliente.Open;
//        while not spObtenerConveniosCliente.Eof do begin
//            vcbConveniosCliente.Items.Add(  spObtenerConveniosCliente.FieldByName('NumeroConvenioFormateado').AsString,
//                                            spObtenerConveniosCliente.FieldByName('CodigoConvenio').AsInteger);
//
//            // Si el convenio esta dado de baja, almacenarlo en la lista de
//            // convenios dados de baja.
//            if spObtenerConveniosCliente.FieldByName('CodigoEstadoConvenio').Value = ESTADO_CONVENIO_BAJA then begin
//                    FListaConveniosBaja.Add(Trim(spObtenerConveniosCliente.FieldByName('CodigoConvenio').AsString));
//            end;
//
//            spObtenerConveniosCliente.Next;
//        end;

        CargarConveniosRUT(DMCOnnections.BaseCAC,vcbConveniosCliente, 'RUT',
        PadL(peRUTCLiente.Text, 9, '0' ), False, 0 );
        FListaConveniosBaja.Clear;
        for  val := 0 to vcbConveniosCliente.Items.Count - 1 do begin

          if Pos( TXT_CONVENIO_DE_BAJA, vcbConveniosCliente.Items[val].Caption ) > 0 then  begin

             FListaConveniosBaja.Add(Trim(IntToStr(vcbConveniosCliente.Items[val].value)));

          end
        end;

//        spObtenerConveniosCliente.Close;
//END:SS_1120_MVI_20130930 --------------------------------------------------------------------------------------------------------------------
        //if vcbConveniosCliente.Items.Count > 0 then vcbConveniosCliente.ItemIndex := 0;   // SS_660_CQU_20130711
        if vcbConveniosCliente.Items.Count > 0 then begin                                   // SS_660_CQU_20130711
            vcbConveniosCliente.ItemIndex := 0;                                             // SS_660_CQU_20130711
            vcbConveniosClienteChange(vcbConveniosCliente);                                 // SS_660_CQU_20130711
        end;                                                                                // SS_660_CQU_20130711


    finally
		Screen.Cursor := crDefault;
    end;
end;

{------------------------------------------------------------------------
        btnCancelarClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Cierra el formulario

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

{------------------------------------------------------------------------
        btnAceptarClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Permite ingresar un nuevo convenio a Lista amarilla

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_CONVENIO_NO_SELEC   = 'No ha seleccionado el convenio !';
    MSG_CONVENIO_DE_BAJA    = 'No puede ingresar un convenio de baja a lista amarilla';
    MSG_ERROR_AL_GRABAR     = 'Ocurri� un error al grabar';
    MSG_GRABADO             = 'El convenio se ha agregado a seguimiento de lista amarilla';
    MSG_CONCESIO_NO_SELEC   = 'No ha seleccionado una concesionaria';          // SS_660_CQU_20130711
const                                                                          // SS_660_CQU_20121010
    CONST_CONVENIO_EN_LISTA_AMARILLA = 'El convenio ya est� Inhabilitado.';    // SS_660_CQU_20121010
var
    CodigoConvenio, Mensaje : string;
    Retorno : integer;
    CodigoConcesionaria : string;                                               // SS_660_CQU_20130711
begin
    if vcbConveniosCliente.ItemIndex < 0 then begin
        MsgBoxBalloon(MSG_CONVENIO_NO_SELEC, Caption,MB_ICONEXCLAMATION, vcbConveniosCliente);
        Exit;
    end;

    CodigoConvenio := vcbConveniosCliente.Value;
    if FListaConveniosBaja.IndexOf(CodigoConvenio) >= 0 then begin
        MsgBoxBalloon(MSG_CONVENIO_DE_BAJA, Caption, MB_ICONWARNING, vcbConveniosCliente);
        Exit;
    end;

    if vcbConcesionarias.Value <= 0 then begin                                                  // SS_660_CQU_20130711
        MsgBoxBalloon(MSG_CONCESIO_NO_SELEC, Caption, MB_ICONEXCLAMATION, vcbConcesionarias);   // SS_660_CQU_20130711
        Exit;                                                                                   // SS_660_CQU_20130711
    end else                                                                                    // SS_660_CQU_20130711
        CodigoConcesionaria := vcbConcesionarias.Value;                                         // SS_660_CQU_20130711

    spAgregarConvenioAListaAmarilla.Close;
    with spAgregarConvenioAListaAmarilla do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        Parameters.ParamByName('@CodigoUsuario').Value  := UsuarioSistema;
        Parameters.ParamByName('@MensajeError').Value   := null;
        Parameters.ParamByName('@EstadoInicial').Value  := null;                                // SS_660_CQU_20130711
        Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;            // SS_660_CQU_20130711
        try
            ExecProc;
            Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
            if Retorno < 0 then begin
                Mensaje := iif(Parameters.ParamByName('@MensajeError').Value = null, '', Parameters.ParamByName('@MensajeError').Value);
                //MsgBoxErr(MSG_ERROR_AL_GRABAR, Mensaje, Caption, MB_ICONERROR);                           // SS_660_CQU_20121010
                if Mensaje = CONST_CONVENIO_EN_LISTA_AMARILLA then MsgBox(Mensaje, Caption, MB_ICONWARNING) // SS_660_CQU_20121010
                else MsgBoxErr(MSG_ERROR_AL_GRABAR, Mensaje, Caption, MB_ICONERROR);                        // SS_660_CQU_20121010
            end
            else begin
                MsgBox(MSG_GRABADO, Caption, MB_ICONINFORMATION);
                ModalResult := mrOk;
            end;
        except on e:exception do begin
                MsgBoxErr(MSG_ERROR_AL_GRABAR, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    end;


end;

{------------------------------------------------------------------------
        peRUTClienteButtonClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Permite invocar la ventana de b�squeda por Rut.

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.peRUTClienteButtonClick(
  Sender: TObject);
var
    fbc : TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, fbc);
    if fbc.Inicializa(FUltimaBusqueda)then begin
    	if (fbc.ShowModal = mrok) then begin
            FUltimaBusqueda := fbc.UltimaBusqueda;
			peRUTCliente.Text := Trim(fbc.Persona.NumeroDocumento);
	   	end;

	end;

    fbc.Release;

end;

procedure TNuevoConvenioListaAmarillaForm.peRUTClienteChange(Sender: TObject);
begin
	vcbConveniosCliente.Items.Clear;				//SS_660_MCA_20131104
	vcbConcesionarias.Items.Clear;                  //SS_660_MCA_20131104
end;

{------------------------------------------------------------------------
        peRUTClienteKeyPress

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Simula un ENTER o Limpia la combo de convenios.

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.peRUTClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key = #13 then begin
        Key := #0;
        btnBuscarClick(nil);
    end
    //else vcbConveniosCliente.Clear;   // SS_660_CQU_20130711
    else begin                          // SS_660_CQU_20130711
        vcbConveniosCliente.Clear;      // SS_660_CQU_20130711
        vcbConcesionarias.Clear;        // SS_660_CQU_20130711
    end;                                // SS_660_CQU_20130711
end;

{------------------------------------------------------------------------
        vcbConveniosClienteDrawItem

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Invoca al procedimiento de cambio de color dependiendo
                    del estado del convenio.

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.vcbConveniosClienteDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
//BEGIN:SS_1120_MVI_20130820 ------------------------------------------------------------
//var
//    CodigoConvenio: AnsiString;
//begin
//    CodigoConvenio := vcbConveniosCliente.Items[Index].Value;
//    // Si el Convenio est� en la lista de los convenios dados de baja, entonces
//    // colorearlo de manera diferente a los que NO est�n de baja.
//    if FListaConveniosBaja.IndexOf(CodigoConvenio) <> -1 then begin
//        ItemCBColor(Control, Index, Rect, cl3DLight, clRed)
//    end else begin
//        ItemCBColor(Control, Index, Rect, $00FAEBDE, clBlack);
//    end;
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConveniosCliente.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

 //END:SS_1120_MVI_20130820 ------------------------------------------------------------

end;

{------------------------------------------------------------------------
        vcbConveniosClienteChange

    Author      : CQuezadaI
    Date        : 18-Julio-2013
	Firma		: SS_660_CQU_20130711
    Description : Carga las concesionarias permitidas seg�n el Convenio

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.vcbConveniosClienteChange(Sender: TObject);
begin
    CargarConcesionariasDisponibles(vcbConveniosCliente.Value);
end;

{------------------------------------------------------------------------
        CargarConcesionariasDisponibles

    Author      : CQuezadaI
    Date        : 18-Julio-2013
	Firma		: SS_660_CQU_20130711
    Description : Carga las concesionarias permitidas seg�n el Convenio indicado

    Author      : CQuezadaI
    Date        : 01-Agosto-2013
	Firma		: SS_660_CQU_20130711
    Description : Se agrega mensaje cuando no cumple con la regla.

-------------------------------------------------------------------------}
procedure TNuevoConvenioListaAmarillaForm.CargarConcesionariasDisponibles;
resourcestring
    ERROR_CARGA_CONCESIONARIAS  = 'Fall� la carga de concesionarias habilitadas';
    MSG_NO_CUMPLE_REGLA         = 'No se encontraron Concesionarias para inhabilitar este convenio';
var
    CodigoConcesionaria : Integer;
    Concesionaria       : String;
begin
    try
        try
            // Limpio el Combo de Concesionarias
            vcbConcesionarias.Items.Clear;
            // Cargo la regla de validaci�n
            if spReglaValidacionListaAmarilla.Active then spReglaValidacionListaAmarilla.Close;
            spReglaValidacionListaAmarilla.Parameters.Refresh;
            spReglaValidacionListaAmarilla.Parameters.ParamByName('@CodigoConvenio').Value      := CodigoConvenio;
            spReglaValidacionListaAmarilla.Parameters.ParamByName('@Fecha').Value               := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);
            spReglaValidacionListaAmarilla.Parameters.ParamByName('@CodigoConcesionaria').Value := Null;
            spReglaValidacionListaAmarilla.Open;

            // Cargo el Combo con las concesionarias permitidas
            if not spReglaValidacionListaAmarilla.Eof then begin
                vcbConcesionarias.Items.Add('Seleccione Concesionaria', 0);
                while not spReglaValidacionListaAmarilla.Eof do begin
                    if spReglaValidacionListaAmarilla.FieldByName('DebeEstar').AsBoolean and
                        not spReglaValidacionListaAmarilla.FieldByName('EnProceso').AsBoolean
                    then begin
                        spObtenerConcesionarias.Filter :=  'CodigoConcesionaria = ' + spReglaValidacionListaAmarilla.FieldByName('CodigoConcesionaria').AsString
                                                            + ' AND PermiteInhabilitarListaAmarilla = 1';
                        spObtenerConcesionarias.Filtered := True;
                        if not spObtenerConcesionarias.Eof then begin
                            CodigoConcesionaria := spObtenerConcesionarias.FieldByName('CodigoConcesionaria').AsInteger;
                            Concesionaria       := spObtenerConcesionarias.FieldByName('Descripcion').AsString;
                            // Agrego la Concesionaria al combo
                            vcbConcesionarias.Items.Add(Concesionaria, CodigoConcesionaria);
                        end;
                    end;
                    spReglaValidacionListaAmarilla.Next;
                end;
            end;
        except
            on e:exception do begin
                MsgBoxErr(ERROR_CARGA_CONCESIONARIAS, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        if vcbConcesionarias.Items.Count <= 1 then begin
            btnAceptar.Enabled := False;
            vcbConcesionarias.Clear;
            vcbConcesionarias.Items.Add('Sin Concesionaria Habilitada', -1);
            vcbConcesionarias.ItemIndex := vcbConcesionarias.Items.IndexOfValue(-1);
            MsgBoxBalloon(MSG_NO_CUMPLE_REGLA, Caption, MB_ICONWARNING, vcbConveniosCliente);
        end else begin
            btnAceptar.Enabled := True;
            vcbConcesionarias.ItemIndex := vcbConcesionarias.Items.IndexOfValue(0);
        end;
    end;
end;

end.
