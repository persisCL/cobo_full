{----------------------------------------------------------------------------------
                                 frmIngresoEspecialSD

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	SS_1268_MBE_20150511
            	Ventana de Ingreso especial de comprobante SD
                
-----------------------------------------------------------------------------------}
unit frmIngresoEspecialSD;

interface

uses
  DMConnection,
  PeaTypes,
  PeaProcs,
  BuscaClientes,
  Util,
  UtilDB,
  UtilProc,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DmiCtrls, DB, ADODB;

type
  TIngresoEspecialSDForm = class(TForm)
    lblTipoFiscal: TLabel;
    lblNumeroFiscal: TLabel;
    Label1: TLabel;
    vcbTipoComprobanteFiscal: TVariantComboBox;
    lblObservacion: TLabel;
    edtObservacion: TEdit;
    lblAfecto: TLabel;
    lblExento: TLabel;
    neNumeroComprobanteFiscal: TNumericEdit;
    neMontoExento: TNumericEdit;
    neMontoAfecto: TNumericEdit;
    btnCancelar: TButton;
    btnAceptar: TButton;
    lbl1: TLabel;
    peRUTCliente: TPickEdit;
    btnBuscar: TButton;
    lbl2: TLabel;
    vcbConveniosCliente: TVariantComboBox;
    lblIVA: TLabel;
    neMontoIVA: TNumericEdit;
    Label2: TLabel;
    spGeneracionEspecialComprobanteSaldoInicialDeudor: TADOStoredProc;
    lblSaldoLabel: TLabel;
    lblSaldoConvenio: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure vcbConveniosClienteDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure btnAceptarClick(Sender: TObject);
    procedure neMontoAfectoChange(Sender: TObject);
    procedure vcbConveniosClienteChange(Sender: TObject);
  private
    { Private declarations }
    FUltimaBusqueda : TBusquedaCliente;

  public
    { Public declarations }
    function Inicializar : boolean;
  end;

var
  IngresoEspecialSDForm: TIngresoEspecialSDForm;

implementation

{$R *.dfm}


{----------------------------------------------------------------------------------
                                 Inicializar

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Inicializa el formulario

-----------------------------------------------------------------------------------}
function TIngresoEspecialSDForm.Inicializar;
resourcestring
    TC_BOLETA_EXENTA_STR	= 'Boleta Exenta';
    TC_BOLETA_AFECTA_STR    = 'Boleta Afecta';
    TC_FACTURA_EXENTA_STR	= 'Factura Exenta';
    TC_FACTURA_AFECTA_STR	= 'Factura Afecta';

    MSG_CAPTION				= 'Ingreso comprobante SD';

begin
    Caption := MSG_CAPTION;
    Position := poScreenCenter;
    ActiveControl := vcbTipoComprobanteFiscal;
    
    {cargar los tipos de comprobantes}
    vcbTipoComprobanteFiscal.Clear;
    vcbTipoComprobanteFiscal.Items.Add(TC_BOLETA_EXENTA_STR, TC_BOLETA_EXENTA);
    vcbTipoComprobanteFiscal.Items.Add(TC_BOLETA_AFECTA_STR, TC_BOLETA_AFECTA);
    vcbTipoComprobanteFiscal.Items.Add(TC_FACTURA_EXENTA_STR, TC_FACTURA_EXENTA);
    vcbTipoComprobanteFiscal.Items.Add(TC_FACTURA_AFECTA_STR, TC_FACTURA_AFECTA);
    vcbTipoComprobanteFiscal.ItemIndex := 0;

    neNumeroComprobanteFiscal.Clear;
    neMontoExento.Clear;
    neMontoAfecto.Clear;
    edtObservacion.Clear;
    vcbConveniosCliente.OnChange(nil);

    spGeneracionEspecialComprobanteSaldoInicialDeudor.Connection := DMConnections.BaseCAC;
    
    Result := True;
end;

{----------------------------------------------------------------------------------
                                 btnAceptarClick

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Valida los datos ingresados y crea el comprobante y el pago.
-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_NO_CONVENIO		= 'No ha seleccionado el convenio del cliente';
    MSG_NO_TIPO_FIS		= 'No ha indicado el tipo de comprobante Fiscal';
    MSG_NO_NUMERO		= 'No ha ingresado el N�mero de comprobante Fiscal';
    MSG_NO_MONTO		= 'Debe indicar un Importe Exento o Afecto';
    MSG_NO_IVA			= 'Ha indicado monto afecto, pero no ha ingresado el IVA';
    MSG_NO_AFECTO		= 'Ha ingresado el monto IVA, pero no ha ingresado el Importe Afecto'; 
    MSG_NO_OBS			= 'Debe ingresar una Observaci�n';

    MSG_PREGUNTA		= 'A continuaci�n se generar� un comprobante SD, por un Importe Total de: %.0n ' + #13#10 + 
    					  ' para el convenio indicado. �Desea proceder?';

	MSG_EXITO			= 'Se ha generado exitosamente el comprobante SD %d, el pago %d y la aplicaci�n de ambos';
    MSG_ERROR_SD		= 'No se pudo generar el comprobante SD';

var
    vMsg, vPregunta : string;
    vComproSD, vPago : int64;
    
begin
    if not ValidateControls (
                              [	vcbConveniosCliente,
                                vcbTipoComprobanteFiscal,
                                neNumeroComprobanteFiscal,
                                neMontoExento,
                                edtObservacion
                              ],
                              [	vcbConveniosCliente.ItemIndex >= 0,
                                vcbTipoComprobanteFiscal.ItemIndex >= 0,
                                neNumeroComprobanteFiscal.ValueInt > 0,
                                (neMontoExento.ValueInt > 0) or (neMontoAfecto.ValueInt > 0),
                                Trim(edtObservacion.Text) <> ''
                              ],
                              Caption,
                              [	MSG_NO_CONVENIO,
                                MSG_NO_TIPO_FIS,
                                MSG_NO_NUMERO,
                                MSG_NO_MONTO,
                                MSG_NO_OBS
                              ]
                            ) then begin
        Exit;
	end;

    if (neMontoAfecto.ValueInt > 0) and (neMontoIVA.ValueInt = 0) then begin
    	MsgBoxBalloon(MSG_NO_IVA, Caption, MB_ICONEXCLAMATION, neMontoIVA);
        Exit;
    end;

    if (neMontoIVA.ValueInt > 0) and (neMontoAfecto.ValueInt = 0) then begin
        MsgBoxBalloon( MSG_NO_AFECTO, Caption, MB_ICONEXCLAMATION, neMontoAfecto);
        Exit;
    end;
    

    vPregunta := Format(MSG_PREGUNTA, [1.0 * neMontoExento.ValueInt + neMontoAfecto.ValueInt + neMontoIVA.ValueInt]);
    if MsgBox(vPregunta, Caption, MB_ICONQUESTION + MB_YESNO) = IDYES then begin
    	try
            with spGeneracionEspecialComprobanteSaldoInicialDeudor do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value				:= vcbConveniosCliente.Value;
                Parameters.ParamByName('@TipoComprobanteFiscal').Value		:= vcbTipoComprobanteFiscal.Value;
                Parameters.ParamByName('@NumeroComprobanteFiscal').Value	:= neNumeroComprobanteFiscal.ValueInt;
                Parameters.ParamByName('@MontoExento').Value				:= neMontoExento.ValueInt * 100;
                Parameters.ParamByName('@MontoAfecto').Value				:= neMontoAfecto.ValueInt * 100;
                Parameters.ParamByName('@MontoIVA').Value					:= neMontoIVA.ValueInt * 100;
                Parameters.ParamByName('@Observaciones').Value				:= edtObservacion.Text;
                Parameters.ParamByName('@CodigoUsuario').Value				:= UsuarioSistema;
                Parameters.ParamByName('@MensajeError').Value				:= null;
                Parameters.ParamByName('@NumeroComprobante').Value			:= null;
                Parameters.ParamByName('@NumeroPago').Value					:= null;
                ExecProc;
                vMsg := Parameters.ParamByName('@MensajeError').Value;
                if vMsg = '' then begin
                	vComproSD 	:= Parameters.ParamByName('@NumeroComprobante').Value;
                    vPago		:= Parameters.ParamByName('@NumeroPago').Value;

                    vMsg := Format(MSG_EXITO, [ vComproSD, vPago ]);
                    MsgBox(vMsg, Caption, MB_ICONINFORMATION);
                end
                else begin
                    MsgBoxErr(MSG_ERROR_SD, vMsg, Caption, MB_ICONERROR);
                end;
            end;

        except on e:Exception do begin
                MsgBoxErr(MSG_ERROR_SD, e.Message, Caption, MB_ICONERROR);
        	end;
        end;

    end;
    
end;

{----------------------------------------------------------------------------------
                                 btnBuscarClick

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Busca los convenios asociados al RUT

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.btnBuscarClick(Sender: TObject);
resourcestring
	MSG_NO_CONVENIOS	= 'No se encontraron convenios para el RUT %s'; 
begin
    if peRUTCliente.Text <> '' then begin
		CargarConveniosRUT( DMConnections.BaseCAC, vcbConveniosCliente, 'RUT',
    						PadL(peRUTCLiente.Text, 9, '0' ), False );

    	if vcbConveniosCliente.Items.Count = 0 then begin
        	MsgBox(Format(MSG_NO_CONVENIOS, [peRUTCliente.Text]), Caption, MB_ICONEXCLAMATION);
        end
        else begin
            vcbConveniosCliente.OnChange(nil);
        end;
    end;
    
end;

{----------------------------------------------------------------------------------
                                 btnCancelarClick

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Cancela el ingreso del comprobante SD

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{----------------------------------------------------------------------------------
                                 FormClose

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Libera la memoria de la ventana de ingreso del comprobante SD

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	action := caFree;
end;

{----------------------------------------------------------------------------------
                                 peRUTClienteButtonClick

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Invoca la ventana de b�squeda por RUT

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.peRUTClienteButtonClick(Sender: TObject);
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    Application.CreateForm(TFormBuscaClientes,FormBuscaClientes);
	if FormBuscaClientes.Inicializa(FUltimaBusqueda) then begin
        FormBuscaClientes.ShowModal;
        if FormBuscaClientes.ModalResult = idOk then begin
            FUltimaBusqueda := FormBuscaClientes.UltimaBusqueda;
            peRUTCliente.Text :=  FormBuscaClientes.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
            btnBuscar.Click;
        end;

        if FormBuscaClientes.Persona.CodigoPersona <> 0 then begin
            peRUTCliente.OnChange(nil);
        end ;
    end;
    
    FormBuscaClientes.Free;

end;

{----------------------------------------------------------------------------------
                                 peRUTClienteChange

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Limpia los campos para permitir un nuevo ingreso.

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.peRUTClienteChange(Sender: TObject);
begin
    vcbConveniosCliente.Clear;
    vcbConveniosCliente.OnChange(nil);
end;

{----------------------------------------------------------------------------------
                                 vcbConveniosClienteChange

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	despliega el saldo del convenio

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.vcbConveniosClienteChange(Sender: TObject);
resourcestring
	MSG_SQL_SALDO	= 'SELECT dbo.ObtenerSaldoDelConvenio( %d )';

var
	vSaldo : longint;
begin
    lblSaldoLabel.Visible := False;
    lblSaldoConvenio.Caption := '';
    if vcbConveniosCliente.ItemIndex >= 0 then begin
        lblSaldoLabel.Visible := True;
    	vSaldo := vcbConveniosCliente.Value;
        vSaldo := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_SALDO, [vSaldo]));
        lblSaldoConvenio.Caption := Format('$ %0.n', [vSaldo / 100.0]);
    end;

end;

{----------------------------------------------------------------------------------
                                 vcbConveniosClienteDrawItem

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Dibuja los convenios acorde a su estado: activo, de baja, sin cuenta

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.vcbConveniosClienteDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConveniosCliente.Items[Index].Caption ) > 0 then  begin
     ItemComboBoxColor(Control, Index, Rect, State, clred, true);
  end
  else begin
     ItemComboBoxColor(Control, Index, Rect, State);
  end;
end;

{----------------------------------------------------------------------------------
                                 neMontoAfectoChange

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	Activa o desactiva el ingreso de IVA

-----------------------------------------------------------------------------------}
procedure TIngresoEspecialSDForm.neMontoAfectoChange(Sender: TObject);
begin
    neMontoIVA.Enabled := (neMontoAfecto.ValueInt > 0);
    neMontoIVA.ValueInt := 0;
end;

end.
