{********************************** Unit Header ********************************
File Name : InvoicingExplorer.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
    Author : ggomez
    Date : 06/09/2006
    Description : cambi� el tipo de datos de la propiedad spObtenerComprobantesFacturadosTotalAPagar
        ahora es Largeint, antes era Integer. Este cambio se debe a que el
        campo TotalAPagar de la BD ahora es un Bigint.

*******************************************************************************}

unit InvoicingExplorer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Util, UtilProc, UtilDB, ListBoxEx, DBListEx, ExtCtrls,
  DmiCtrls, BuscaTab, ComCtrls, frmReporteFacturacion, DMConnection, DB,
  ADODB, PeaProcs;

type
  TfrmInvoicing = class(TForm)
    Panel1: TPanel;
    BuscaTabEdit1: TBuscaTabEdit;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    TabSheet4: TTabSheet;
    btProcesosFacturacion: TBuscaTabla;
    dsProcesosFacturacion: TDataSource;
    spProcesosFacturacion: TADOStoredProc;
    spProcesosFacturacionNumeroProcesoFacturacion: TAutoIncField;
    spProcesosFacturacionFechaHoraInicio: TDateTimeField;
    spProcesosFacturacionOperador: TStringField;
    spProcesosFacturacionFechaHoraFin: TDateTimeField;
    spReporte: TADOStoredProc;
    dsReporte: TDataSource;
    Panel2: TPanel;
    DBListEx1: TDBListEx;
    Panel3: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Bevel1: TBevel;
    lblPrimeraNC: TLabel;
    lblUltimaNC: TLabel;
    lblCantidadNC: TLabel;
    Panel4: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Bevel2: TBevel;
    Label13: TLabel;
    lblPeajesPer: TLabel;
    lblPeajesAnt: TLabel;
    lblFactVencida: TLabel;
    lblIntereses: TLabel;
    lblTotalAPagar: TLabel;
    spObtenerComprobantesFacturados: TADOStoredProc;
    dsComprobantesFacturados: TDataSource;
    spObtenerComprobantesFacturadosTipoComprobante: TStringField;
    spObtenerComprobantesFacturadosNumeroComprobante: TLargeintField;
    spObtenerComprobantesFacturadosCodigoConvenio: TIntegerField;
    spObtenerComprobantesFacturadosNumeroConvenio: TStringField;
    spObtenerComprobantesFacturadosNumeroDocumento: TStringField;
    spObtenerComprobantesFacturadosNombre: TStringField;
    spObtenerComprobantesFacturadosdesc_TotalComprobante: TStringField;
    spObtenerComprobantesFacturadosdesc_TotalAPagar: TStringField;
    Panel5: TPanel;
    Label14: TLabel;
    neCantidadComprobantes: TNumericEdit;
    Panel6: TPanel;
    Label15: TLabel;
    neCantidadConveniosFacturados: TNumericEdit;
    DBListEx2: TDBListEx;
    Label16: TLabel;
    Label17: TLabel;
    lblConveniosFacturados: TLabel;
    lblConveniosAFacturar: TLabel;
    spObtenerConvenios: TADOStoredProc;
    dsConvenios: TDataSource;
    spObtenerConvenioscodigoconvenio: TIntegerField;
    spObtenerConveniosNumeroConvenio: TStringField;
    spObtenerConveniosCodigoCliente: TIntegerField;
    spObtenerConveniosNombreCliente: TStringField;
    spObtenerConveniosCodigoEstadoConvenio: TWordField;
    spObtenerConveniosCodigoConcesionaria: TWordField;
    spObtenerConveniosFechaAlta: TDateTimeField;
    spObtenerConveniosTipoMedioPagoAutomatico: TWordField;
    spObtenerConveniosdesc_Concesionaria: TStringField;
    spObtenerConveniosdesc_EstadoConvenio: TStringField;
    spObtenerConveniosdesc_MedioPago: TStringField;
    Label18: TLabel;
    btnActualizarCF: TButton;
    btnActualizarCO: TButton;
    Panel7: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    lblConveniosNOFacturados: TLabel;
    lblConveniosAFacturar2: TLabel;
    neCantidadConveniosNoFacturados: TNumericEdit;
    btnActualizarNF: TButton;
    DBListEx3: TDBListEx;
    spConveniosNOFacturados: TADOStoredProc;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField2: TIntegerField;
    WordField1: TWordField;
    WordField2: TWordField;
    DateTimeField1: TDateTimeField;
    WordField3: TWordField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    dsConveniosNOFacturados: TDataSource;
    Panel8: TPanel;
    Label22: TLabel;
    neMovimientos: TNumericEdit;
    btnActualizarMO: TButton;
    DBListEx4: TDBListEx;
    Panel9: TPanel;
    Label23: TLabel;
    Bevel3: TBevel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    lblTotalMov: TLabel;
    lblMovFact: TLabel;
    lblMovNOFact: TLabel;
    Label27: TLabel;
    spMovimientos: TADOStoredProc;
    dsMovimientos: TDataSource;
    spMovimientoscodigoconvenio: TIntegerField;
    spMovimientosNumeroMovimiento: TAutoIncField;
    spMovimientosNumeroConvenio: TStringField;
    spMovimientosIndiceVehiculo: TIntegerField;
    spMovimientosCodigoCliente: TIntegerField;
    spMovimientosNombreCliente: TStringField;
    spMovimientosFechaHora: TDateTimeField;
    spMovimientosConceptoMovimiento: TStringField;
    spMovimientosObservaciones: TStringField;
    spMovimientosImporte: TStringField;
    spMovimientosFacturado: TStringField;
    lblCantmov: TLabel;
    spReportePrimeraNotaCobro: TLargeintField;
    spReporteUltimaNotaCobro: TLargeintField;
    spReporteCantidadNotasCobro: TLargeintField;
    spReporteTotalAPagar: TBCDField;
    spReportedesc_TotalAPagar: TStringField;
    spReporteTotalPeajesPeriodo: TBCDField;
    spReportedesc_TotalPeajesPeriodo: TStringField;
    spReporteTotalPeajesPerAnterior: TBCDField;
    spReportedesc_TotalPeajesPerAnterior: TStringField;
    spReporteTotalIntereses: TBCDField;
    spReportedesc_TotalIntereses: TStringField;
    spReporteTotalFacturacionVencida: TBCDField;
    spReportedesc_TotalFacturacionVencida: TStringField;
    spReporteCodigoUsuario: TStringField;
    spReporteNombreUsuario: TStringField;
    spReporteNumeroProcesoFacturacion: TIntegerField;
    spObtenerComprobantesFacturadosTotalAPagar: TLargeintField;
    spObtenerComprobantesFacturadosTotalComprobante: TLargeintField;
    procedure btnCerrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Label3Click(Sender: TObject);
    procedure btProcesosFacturacionSelect(Sender: TObject;
      Tabla: TDataSet);
    procedure DBListEx1Columns5HeaderClick(Sender: TObject);
    procedure btnActualizarCOClick(Sender: TObject);
    procedure DBListEx2Columns0HeaderClick(Sender: TObject);
    procedure DBListEx3Columns0HeaderClick(Sender: TObject);
    procedure DBListEx4Columns0HeaderClick(Sender: TObject);
    procedure Label3MouseEnter(Sender: TObject);
    procedure Label3MouseLeave(Sender: TObject);
  private
    FNumeroProcesoFacturacion: integer;
    procedure MostrarReporte;
    procedure LimpiarTablas;
    procedure ActualizarTablas;
    function FormatCantidad(valor: int64):string;
  public
    function Inicializar: Boolean;
  end;

var
  frmInvoicing: TfrmInvoicing;

implementation

{$R *.dfm}

procedure TfrmInvoicing.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

function TfrmInvoicing.Inicializar: Boolean;
Resourcestring
    MSG_ERR_TABLE_OPEN = 'No se pudo acceder a la lista de Procesos de Facturaci�n';
Var
	Sz: TSize;
begin
    SZ := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, SZ.cx, sz.cy);
    Result := True;
    try
        lblCantidadNC.Caption := '';
        lblPrimeraNC.Caption := '';
        lblUltimaNC.Caption := '';
        lblPeajesPer.Caption := '';
        lblPeajesAnt.Caption := '';
        lblFactVencida.Caption := '';
        lblIntereses.Caption := '';
        lblTotalAPagar.Caption := '';
        lblConveniosFacturados.Caption := '';
        lblConveniosAFacturar.Caption := '';
        Pagecontrol1.ActivePageIndex := 0;
        lblConveniosNOFacturados.Caption := '';
        lblConveniosAFacturar2.Caption := '';
        lblTotalMov.Caption := '';
        lblcantMov.Caption := '';
        lblMovFact.Caption := '';
        lblMovNOFact.Caption := '';
        neMovimientos.ValueInt := 100;
        necantidadComprobantes.ValueInt := 100;
        neCantidadConveniosFacturados.ValueInt := 100;
        neCantidadConveniosNOFacturados.ValueInt := 100;
        spProcesosFacturacion.Open;
    except
        on e:exception do begin
            MsgBoxErr(MSG_ERR_TABLE_OPEN,e.Message,'ERROR',MB_ICONSTOP)     ;
            Result := False
        end;
    end;
end;

procedure TfrmInvoicing.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action :=caFree;
end;

procedure TfrmInvoicing.Label3Click(Sender: TObject);
resourcestring
     ERROR_NO_PROCESS_SELECTED = 'No hay ning�n proceso de facturaci�n seleccionado.';
begin
    if FNumeroProcesoFacturacion > 0 then
        MostrarReporte
    else
        MsgBox(ERROR_NO_PROCESS_SELECTED, 'ERROR',MB_ICONERROR + MB_OK) ;
end;


procedure TfrmInvoicing.MostrarReporte;
var
    f: TformReporteFacturacion;
    FTiempo: TDateTime;
begin
    try
        FTiempo := ( spProcesosfacturacion.fieldByName('FechaHoraFin').asDateTime -
                     spProcesosfacturacion.fieldByName('FechaHoraInicio').asDateTime);

        if findFormOrCreate(TFormReporteFacturacion,f) then f.free;
             findFormOrCreate(TFormReporteFacturacion,f)  ;

        if f.Inicializa(FNumeroProcesoFacturacion, FTiempo) then f.Ejecutar
        else f.Free;
    except
        on e:Exception do begin
            MsgBoxErr('Error al intentar crear el Reporte',e.Message,'ERROR',MB_ICONSTOP)
        end;
    end;
end;


procedure TfrmInvoicing.btProcesosFacturacionSelect(Sender: TObject;
  Tabla: TDataSet);
begin
    LimpiarTablas;
    FNumeroProcesoFacturacion := spProcesosfacturacion.fieldByName('NumeroProcesoFacturacion').asInteger;
    BuscaTabEdit1.Value := FNumeroProcesoFacturacion;
    BuscaTabEdit1.Text := 'Proc. Fact. N� ' + spProcesosfacturacion.fieldByName('NumeroProcesoFacturacion').asString + ' del ' +spProcesosfacturacion.fieldByName('FechaHoraInicio').asString;
    BuscaTabEdit1.Refresh;
    ActualizarTablas
end;

procedure TfrmInvoicing.LimpiarTablas;
begin

    if spObtenerComprobantesFacturados.State <> dsInactive then
        spObtenerComprobantesFacturados.Close;

    if spObtenerConvenios.State <> dsInactive then
        spObtenerConvenios.Close;

    if spConveniosNOFacturados.State <> dsInactive then
        spConveniosNOFacturados.Close;

    if spMovimientos.State <> dsInactive then
        spMovimientos.Close;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarTablas
  Author:
  Date Created:  /  /
  Description:
  Parameters: N/A
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TfrmInvoicing.ActualizarTablas;
resourcestring
    ERROR_QTY_INVOICES          = 'La Cantidad de Comprobantes a mostrar debe ser mayor a cero';
    ERROR_QTY_CONVENIO_FACT     = 'La Cantidad de Convenios Facturados a mostrar debe ser mayor a cero';
    ERROR_QTY_CONVENIO_NOFACT   = 'La Cantidad de Convenios NO Facturados a mostrar debe ser mayor a cero';
    ERROR_QTY_MOVS              = 'La Cantidad de Movimientos a mostrar debe ser mayor a cero';
begin
// Pongo el relojito a esperar
    Screen.Cursor := crHourGlass;
    try
    // Primero valido los filtros...
        if (FNumeroProcesoFacturacion > 0) AND
            ValidateControls([neCantidadComprobantes, neCantidadConveniosFacturados,
                              neCantidadConveniosNOFacturados, neMovimientos],
                             [(neCantidadComprobantes.ValueInt > 0),
                              (neCantidadConveniosFacturados.ValueInt > 0),
                              (neCantidadConveniosNOFacturados.ValueInt > 0),
                              (neMovimientos.ValueInt > 0) ],'Cantidad a Mostrar Erronea',
                              [ERROR_QTY_INVOICES, ERROR_QTY_CONVENIO_FACT,
                               ERROR_QTY_CONVENIO_NOFACT, ERROR_QTY_MOVS]) 
           then begin
        // Actualizo los datos que tomo del reporte y los muestro
            try
                if spReporte.State <> dsInactive then
                    spReporte.Close;
                with spReporte do begin
                    Parameters.ParamByName('@NUMPROCFACT').Value := FNumeroProcesoFacturacion;
                    Open;

                    lblPrimeraNC.Caption := FieldByName('PrimeraNotaCobro').AsString;
                    lblUltimaNC.Caption := FieldByName('UltimaNotaCobro').AsString;
                    lblcantidadNC.Caption := FieldByName('CantidadNotasCobro').AsString;
                    lblPeajesPer.Caption := FieldByName('desc_TotalPeajesPeriodo').AsString;
                    lblPeajesAnt.Caption := FieldByName('desc_TotalPeajesPerAnterior').AsString;
                    lblIntereses.Caption := FieldByName('desc_totalIntereses').AsString;
                    lblFactVencida.Caption := FieldByName('desc_TotalFacturacionVencida').asString;
                    lblTotalAPagar.Caption := FieldByName('desc_TotalAPagar').AsString;
                    Close;
                end;

                // abro el sp de comprobantes facturados...
                With spObtenerComprobantesFacturados do begin
                    if State <> dsInactive then Close;
                    Parameters.ParamByName('@NumProcFact').Value := FNumeroProcesoFacturacion;
                    Parameters.ParamByName('@Cantidad').Value := neCantidadComprobantes.ValueInt ;
                    Open;
                end;

                // abro el sp de los convenios
                With spObtenerConvenios do begin
                    if State <> dsInactive then Close;
                    Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
                    Parameters.ParamByName('@Cantidad').Value := neCantidadConveniosFacturados.ValueInt ;
                    Open;
                    lblConveniosFacturados.caption := FormatCantidad(Parameters.ParamByName('@Cantidad').Value);
                end;
                // Calculo cuantos convenios se deberian haber facturado...
                lblConveniosAFacturar.Caption := QueryGetValue( DMConnections.BaseCAC ,
                        Format('SELECT dbo.FLOATTOSTRD( COUNT(CODIGOCONVENIO) ,0,0,1) FROM CONVENIO C  WITH (NOLOCK) '+
                               ' INNER JOIN AGENDAFACTURACION A  WITH (NOLOCK) ON C.CODIGOGRUPOFACTURACION = A.CODIGOGRUPOFACTURACION '+
                               ' AND A.NUMEROPROCESOFACTURACION = %d',[FNumeroProcesoFacturacion]));
                lblConveniosAFacturar2.Caption :=   lblConveniosAFacturar.Caption;

                // abo el sp de convenios NO facturados
                With spConveniosNoFacturados do begin
                    if State <> dsInactive then Close;
                    Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
                    Parameters.ParamByName('@Cantidad').Value := neCantidadConveniosNOFacturados.ValueInt ;
                    Open;
                    lblConveniosNOFacturados.Caption := FormatCantidad(Parameters.ParamByName('@Cantidad').Value);
                end;

                With spMovimientos do begin
                    if State <> dsInactive then Close;
                    Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
                    Parameters.ParamByName('@Cantidad').Value := neMovimientos.ValueInt;
                    Open;
                    lblcantMov.Caption := FormatCantidad(Parameters.ParamByName('@Cantidad').Value);
                    lbltotalMov.Caption := TRIM(Parameters.ParamByName('@TotalMovimientos').Value);
                    lblmovFact.caption := TRIM(Parameters.ParamByName('@TotalFacturados').Value);
                    lblMovNOFact.Caption := TRIM(Parameters.ParamByName('@TotalNoFacturados').Value);
                end;
            except
                on e:exception do begin
                    MsgBoxErr('Error al recuperar los datos.',e.Message,'ERROR',MB_ICONSTOP);
                end;
            end;
        end;

    finally
        Screen.Cursor := crDefault;
    end;
end;


procedure TfrmInvoicing.DBListEx1Columns5HeaderClick(Sender: TObject);
var
    SortString: string;
begin
    if NOT spObtenerComprobantesFacturados.Active then Exit;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        (Sender as TDBListExColumn).Sorting :=  csAscending
    else
        if (Sender as TDBListExColumn).Sorting =  csDescending  then
            (Sender as TDBListExColumn).Sorting :=  csAscending
        else (Sender as TDBListExColumn).Sorting :=  csDescending;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        spObtenerComprobantesFacturados.Sort := ''
    else begin
          SortString := (Sender as TDBListExColumn).FieldName;
          if (Sender as TDBListExColumn).Sorting =  csDescending  then
            SortString := SortString +  ' DESC'
          else SortString := SortString +  ' ASC';
    end;

    spObtenerComprobantesFacturados.Sort := SortString;

end;

procedure TfrmInvoicing.btnActualizarCOClick(Sender: TObject);
begin
    ActualizarTablas;
end;

Function  TfrmInvoicing.FormatCantidad(Valor:Int64):String;
begin
    Result := queryGetValue(DMConnections.BaseCAC, FORMAT('SELECT dbo.FloatToStrD( %d ,0,0,1)', [Valor]));
end;

procedure TfrmInvoicing.DBListEx2Columns0HeaderClick(Sender: TObject);
var
    SortString: string;
begin

    if NOT spObtenerConvenios.Active then exit;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        (Sender as TDBListExColumn).Sorting :=  csAscending
    else
        if (Sender as TDBListExColumn).Sorting =  csDescending  then
            (Sender as TDBListExColumn).Sorting :=  csAscending
        else (Sender as TDBListExColumn).Sorting :=  csDescending;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        spObtenerComprobantesFacturados.Sort := ''
    else begin
          SortString := (Sender as TDBListExColumn).FieldName;
          if (Sender as TDBListExColumn).Sorting =  csDescending  then
            SortString := SortString +  ' DESC'
          else SortString := SortString +  ' ASC';
    end;
    spObtenerConvenios.Sort := SortString;

end;

procedure TfrmInvoicing.DBListEx3Columns0HeaderClick(Sender: TObject);
var
    SortString: string;
begin
    if not spConveniosNOFacturados.Active then exit;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        (Sender as TDBListExColumn).Sorting :=  csAscending
    else
        if (Sender as TDBListExColumn).Sorting =  csDescending  then
            (Sender as TDBListExColumn).Sorting :=  csAscending
        else (Sender as TDBListExColumn).Sorting :=  csDescending;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        spObtenerComprobantesFacturados.Sort := ''
    else begin
          SortString := (Sender as TDBListExColumn).FieldName;
          if (Sender as TDBListExColumn).Sorting =  csDescending  then
            SortString := SortString +  ' DESC'
          else SortString := SortString +  ' ASC';
    end;
    spConveniosNOFacturados.Sort := SortString;

end;

procedure TfrmInvoicing.DBListEx4Columns0HeaderClick(Sender: TObject);
var
    SortString: string;
begin

    if NOT spMovimientos.Active then exit;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        (Sender as TDBListExColumn).Sorting :=  csAscending
    else
        if (Sender as TDBListExColumn).Sorting =  csDescending  then
            (Sender as TDBListExColumn).Sorting :=  csAscending
        else (Sender as TDBListExColumn).Sorting :=  csDescending;

    if (Sender as TDBListExColumn).Sorting =  csNone then
        spObtenerComprobantesFacturados.Sort := ''
    else begin
          SortString := (Sender as TDBListExColumn).FieldName;
          if (Sender as TDBListExColumn).Sorting =  csDescending  then
            SortString := SortString +  ' DESC'
          else SortString := SortString +  ' ASC';
    end;
    spMovimientos.Sort := SortString;

end;

procedure TfrmInvoicing.Label3MouseEnter(Sender: TObject);
begin
    screen.Cursor := crHandPoint;
end;

procedure TfrmInvoicing.Label3MouseLeave(Sender: TObject);
begin
    screen.Cursor := crDefault;
end;

end.
