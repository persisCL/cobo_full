unit PreFacturaciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util, Validate, DateEdit, ListBoxEx, DBListEx,
  ComprobantesEmitidosPrefac, DmiCtrls, VariantComboBox, DMConnection;

type
  TfrmPreFacturaciones = class(TForm)
    lstProcesos: TDBListEx;
    lbl1: TLabel;
    lbl2: TLabel;
    btnFiltrar: TButton;
    btnSalir: TButton;
    btnConsultar: TButton;
    spPREFAC_ProcesosFacturacion_SELECT: TADOStoredProc;
    dsProcesos: TDataSource;
    dateDesde: TDateEdit;
    dateHasta: TDateEdit;
    vcbGrupoFacturacion: TVariantComboBox;
    lbl3: TLabel;
    lbl4: TLabel;
    txtAno: TNumericEdit;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFiltrarClick(Sender: TObject);
    procedure CargarProcesosPreFacturacion;
    procedure btnConsultarClick(Sender: TObject);
    function Inicializar: Boolean;
    procedure lstProcesosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure lstProcesosDblClick(Sender: TObject);
    procedure ConsultarPreFacturacion;
    procedure vcbGrupoFacturacionChange(Sender: TObject);	// TASK_095_MGO_20161220
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPreFacturaciones: TfrmPreFacturaciones;

implementation

{$R *.dfm}

function TfrmPreFacturaciones.Inicializar: Boolean;
var
    spObtenerGruposFacturacion: TADOStoredProc;
begin
    Result := True;

    txtAno.Value := Year(Now);

    vcbGrupoFacturacion.Clear;
    spObtenerGruposFacturacion := TADOStoredProc.Create(Self);
    try
        spObtenerGruposFacturacion.Connection := DMConnections.BaseCAC;
        spObtenerGruposFacturacion.ProcedureName := 'ObtenerGruposFacturacion';
        spObtenerGruposFacturacion.CommandTimeout := 30;
        spObtenerGruposFacturacion.Open;

        while not spObtenerGruposFacturacion.Eof do begin
            vcbGrupoFacturacion.Items.Add(spObtenerGruposFacturacion.FieldByName('Descripcion').AsString,
                                            spObtenerGruposFacturacion.FieldByName('CodigoGrupoFacturacion').AsInteger);

            spObtenerGruposFacturacion.Next;
        end;
    finally
        spObtenerGruposFacturacion.Free;
    end;
    vcbGrupoFacturacion.Items.Add('INFRACTORES', 0);                            // TASK_142_MGO_20170220
    vcbGrupoFacturacion.ItemIndex := 0;

    try
        CargarProcesosPreFacturacion;
    except
        Result := False;
    end;
end;

procedure TfrmPreFacturaciones.lstProcesosDblClick(Sender: TObject);
begin
    ConsultarPreFacturacion;
end;

procedure TfrmPreFacturaciones.lstProcesosDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    if spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Editable').AsBoolean then
        Sender.Canvas.Font.Style := [fsBold];

    if Column = lstProcesos.Columns[8] then
        Text := IIf(spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Editable').AsBoolean, 'SI', 'NO');    

    if Column = lstProcesos.Columns[9] then                                                                 // TASK_095_MGO_20161220
        Text := IIf(spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Transitos').AsBoolean, 'SI', 'NO');    // TASK_095_MGO_20161220
end;

// INICIO : TASK_095_MGO_20161220
procedure TfrmPreFacturaciones.vcbGrupoFacturacionChange(Sender: TObject);
begin
    CargarProcesosPreFacturacion;
end;
// FIN : TASK_095_MGO_20161220

procedure TfrmPreFacturaciones.ConsultarPreFacturacion;
var
    FormComprobantesEmitidosPrefac : TFormComprobantesEmitidosPrefac;
begin
    if not spPREFAC_ProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsInteger > 0 then   // TASK_094_MGO_20161219
        Exit;                                                                                               // TASK_094_MGO_20161219

    Application.CreateForm(TFormComprobantesEmitidosPrefac, FormComprobantesEmitidosPrefac);
    if FormComprobantesEmitidosPrefac.Inicializar(
            spPREFAC_ProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsInteger) then begin
        FormComprobantesEmitidosPrefac.Show;
    end else
        FormComprobantesEmitidosPrefac.Release;
end;

procedure TfrmPreFacturaciones.btnConsultarClick(Sender: TObject);
begin
    ConsultarPreFacturacion;
end;

procedure TfrmPreFacturaciones.btnFiltrarClick(Sender: TObject);
begin
    CargarProcesosPreFacturacion;
end;

procedure TfrmPreFacturaciones.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmPreFacturaciones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmPreFacturaciones.CargarProcesosPreFacturacion;
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_SELECT = 'Error al obtener procesos de Pre Facturación';
begin
    if (dateDesde.Date > dateHasta.Date) and (not dateDesde.IsEmpty) and (not dateHasta.IsEmpty) then begin
        MsgBox('La fecha desde no puede ser mayor a la fecha hasta','Error',MB_ICONSTOP);
        Exit;
    end;

    try
        spPREFAC_ProcesosFacturacion_SELECT.Close;
        spPREFAC_ProcesosFacturacion_SELECT.Parameters.Refresh;
        if not dateDesde.IsEmpty then
            spPREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@FechaDesde').Value := dateDesde.Date;
        if not dateHasta.IsEmpty then
            spPREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@FechaHasta').Value := dateHasta.Date;
        if vcbGrupoFacturacion.Value > -1 then
            spPREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := vcbGrupoFacturacion.Value;
        if txtAno.ValueInt > 0 then
            spPREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@Ano').Value := txtAno.ValueInt;

        spPREFAC_ProcesosFacturacion_SELECT.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_SELECT, e.Message, MSG_ERROR, MB_ICONSTOP);
        end;
    end;
end;

end.
