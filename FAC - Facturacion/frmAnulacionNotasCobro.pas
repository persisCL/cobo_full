{-----------------------------------------------------------------------------
 File Name: frmAnulacionNotasCredito.pas
 Author:    flamas
 Date Created: 07/06/2005
 Language: ES-AR
 Description:  Proceso de Anulaci�n de Notas de Cobro

 Revision 1:
     Author : ggomez
     Date : 23/01/2006
     Description : Al SP spAnularNotaCobro le sub� el CommandTimeOut a 180
        pues para una NK con muchos tr�nsitos daba TimeOut. ESTA ES UNA SOLUCI�N
        PROVISORIA.

 Revision 1
 Author:    jconcheyro
 Date Created: 27/07/2006
 Language: ES-AR
 Description:  Se le agregan decimales a los importes ya que son importantes
 para mantener el balance de la cuenta corriente

 Revision 2
 Author:    jconcheyro
 Date Created: 09/08/2006
 Language: ES-AR
 Description:  Se le agrega un checkbox que comanda un nuevo parametro de
 Anular comprobante que determinar el estado a pasar a los transitos

 Revision 3
 Author:    jconcheyro
 Date Created: 22/08/2006
 Language: ES-AR
 Description:  Se agrega como tipo de comprobante posible de anulaci�n las boletas

 Revision 4
 Author:    jconcheyro
 Date Created: 05/11/2006
 Language: ES-AR
 Description:  Se agrega funcionalidad de no poder acreditar el IVA mas all� de X dias
 dados por el parametro DIAS_ACREDITAR_IVA


 Revision 5
 Author:    jconcheyro
 Date Created: 21/12/2006
 Language: ES-AR
 Description:  Se pasa todo lo que era decimal a integer, se redondea el precio
 de los transitos seleccionados para el credito hacia arriba. Se elimina toda 
 posibilidad de que existan o procesen decimales.

 Revision 6
 Author:    nefernandez
 Date Created: 23/02/2007
 Language: ES-AR
 Description: Se pasa el parametro CodigoUsuario al SP AnularBoleta y
              AnularNotaCobro (para Auditoria)

 Revision 7
 Author:    jconcheyro
 Date Created: 09/04/2007
 Language: ES-AR
 Description:  Se pueden hacer ahora creditos a boletas de arriendo.

 Revision 8
 Author:    rharris
 Date Created: 08/04/2009
 Language: ES-CL
 Description:
    1.- Se agregan el NumeroProcesoFacturacion a los Comprobantes
    de Notas de Credito
    2.- Se inserta en NumeroProcesoFacturacion en la cola de env�o a DBNET

 Revision 9
 Author:    rharris
 Date Created: 18/06/2009
 Language: ES-CL
 Description: (Ref.:Facturacion Electronica)
    1.- Si se eligen todos los transitos para anular un concepto, y si la suma
        de los importes de esos transitos seleccionados da m�s que el total del
        MC anulado, entonces se deja el importe original del MC.

 Revision 10
 Author: rharris
 Date Created: 19/06/2009
 Language: ES-CL
 Description: (Ref.:Facturacion Electronica)
    1.- Se agrega validaci�n para para que el m�ximo importe anulado por concepto
        no supere el importe total para concepto del comprobante que se est� anulando

 Revision 11
 Author: rharris
 Date Created: 23/06/2009
 Language: ES-CL
 Description: (Ref.:Facturacion Electronica)
    1.- Se modifica el AnularComprobante(spAnularNotaCobro, spAnularBoleta)
        para que si se anula un comprobante no electr�nico imprima en el formato
        original.

 Revision 12
 Author: mbecerra
 Date: 11-Agosto-2009
 Description:	(Ref. SS 784)
        Se modifica para validar que la inserci�n en la Cola de DBNet fue
        exitosso.


 Revision 13
 Author: Nelson Droguett Sierra
 Date: 17-Noviembre-2009
 Description:	(Ref. SS 847)
        Al pasar un concepto de peajes del periodo, modificar y seleccionarlos
        todos, llena una lista para almacenar ahi los peajes. Luego, si eliminan
        el concepto, esta lista no se limpiaba, por lo que a pesar de decir que
        no acreditan los peajes, los rebaja igual, permitiendo facturarlos
        nuevamente. Lo que se hizo es que al eliminar un concepto que es de peajes,
        se limpia esta lista.

 Revision 14
 Author: vpaszkowicz
 Date: 17-Diciembre-2009
 Description:	(Ref. SS 847)
        Cuando se eliminan movimientos de peaje y existen mas de 1 en la tabla
        de memoria, Si se elimina solo uno de los movimientos agregardos para
        anular, se deberan borrar solo los transitos de ese movimiento.

 Revision		: 15
 Author			: mbecerra
 Date			: 01-Junio-2010
 Description	:		(Ref. Fase II)
                    Los conceptos de peajes y peajes del per�odo se traen para
                    Multiconcesionaria.

 Revision       : 16
 Author         : pdominguez
 Date           : 05/06/2010
 Description    : Infractores Fase 2
    - Se a�aden los siguientes objetos/variables/constantes:
        spActualizarInfraccionesComprobanteAnulado: TADOStoredProc.

    - Se modifican los siguientes procedimientos/funciones:
        AnularNotaCobro,
        CargarComprobantes

 Revision       : 17
 Author         : mbecerra
 Date           : 28-03-2011
 Description    :   (Ref Fase 2)
                    Se agregan los conceptos de Peajes y de Estacionamientos,
                    ambos se toman desde la tabla de Concesionarias

 Revision       : 18
 Author         : Nelson Droguett Sierra
 Date           : 31-03-2011
 Description    :   (Ref Fase 2)
                    Se agregan los conceptos de Peajes y de Estacionamientos,
                    ambos se toman desde la tabla de Concesionarias
Firma           : PAR00133-NDR-20110331


Firma: SS_637_PDO_20110808

Description: Se a�ade el control para evitar que a un comprobante refinanciado,
    se el pueda hacer una nota de cr�dito.

Firma       : SS-985-NDR-20111025
Description : Que no  pueda hacer una nota de credito normal a un documento
              electronico que sea una nota de cobro directa.

Firma       : SS_1012_ALA_20111118
Description : Se incluye modificaci�n del Store ObtenerImporteAnuladoConceptoComprobanteAAnular
             con el cual se verfica el monto hasta el cual se puede anular el concepto.
             Se crea funcion ObtenerTotalConceptosPasados que obtiene el monto que se
             est� eliminando para un concepto en particular (grilla cdsCredito).

Firma       : SS_1012_ALA_20111128
Descripcion : Si se supera el importe del concepto, se agregar� el concepto hasta
             el monto que este disponible.
             Se soluciona transaccionalidad cuando se realiza rollback al momento de anular la Nota de cobro.
             Se soluciona bug encontrado cuando se intenta obtener el concepto que anula.

Firma       : SS-1006-NDR-20120817
Description : Validar que los montos devueltos por concesionaria no excedan a los montos originales por concesionaria

Firma       : SS_1006_10015_NDR_20120928
Description : Si el comprobante tiene conceptos de otras concesionarias que informan folios y pagos, no puede agregar conceptos manualmente

Firma       : SS_1006_10015_NDR_20121112
Description : Cambiar la funcion SQL ComprobanteTienePagos() por ComprobanteTieneRecibos()

Firma       : SS_1006_10015_NDR_20121015
Description : Eliminar Revision anterior

Firma       : SS_1006_10015_NDR_20121126
Description : Que se puedan acreditar conceptos negativos para anular descuentos facturados.

Firma       : SS_1080_CQU_20130111
Descripcion : Al pasar conceptos para anular, no permite hacerlo
              ya que indica que la suma de los conceptos super� el total de conceptos.
              Esto fue corregido inplementando una funci�n que sume los conceptos sin pasar.

Firma       : SS_1081_CQU_20130201
Descripcion : Si el comprobante tiene ConceptoInfractor asociado,
              no se permitir� realizar Nota de Cr�dito Parcial.

Firma       : SS_1080_CQU_20130523
Descripcion : Al pasar conceptos para anular, se estaban considerando los conceptos
              por notas parciales en SQL m�s el concepto a rebajar
              pero por ac� tambi�n se estaba considerando por lo que repet�a el concepto
              y entregaba un valor erroneo.

Firma       : SS_660_MVI_20130718
Description	:	(Ref: SS 660)
              Se agrega validaci�n para indicar si un Convenio est� en Lista Amarilla

Fecha       : 22-07-2013
Firma       : SS_660_CQU_20130711
Description	: Se agrega el par�metro CodigoConcesionaria a la funci�n EstaConvenioEnListaAmarilla
              Se modifica la posici�n del Label que indica si estpa en ListaAmarilla

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

Firma          :   SS_1081_CQU_20130826
Descripci�n    :   Se replica la anulaci�n de tr�nsitos para anular infracciones.
                   Se quita la restricci�n que impide hacer CK parciales a Infractores.
                   Se habilitan/deshabilitan los botones seg�n corresponda.
                   Se agrega el EstadoPago como variable general.


Firma       : SS_1120_MVI_20130820
Descripcion : Se alarga el label lblValorNumeroConvenio para incluir el texto "(Sin Cuenta)".
              Se agregan llamados a funciones para saber si un convenio esta de baja o si no posee cuentas.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.

Firma       :   SS_1006_MBE_20130911
Description :   Se corrige lo siguiente:
                Si la Nk tiene conceptos de PAK, se inhabilita el boton "Agregar Nuevo"
                Sin embargo si se busca por comprobante fiscal, el bot�n se habilita
                provocando inconsistencia.

Firma		:	SS_1144_MCA_20131118
Description	:	se agrega campo saldo disponible que es el importe - ck parcial por concepto que lo anula
            	los importe de los transitos/estacionamientos/infractores seleccionados es mayor que el saldo disponible este ultimo se agrega al importe de la nota de credito.
                se bloquea los conceptos que anulan peajes, Estacionamientos e infractores cuando se agrega un nuevo concepto a anular

Firma       :   SS_1229_MCA_20141117
Descripcion :   si el saldo total del comprobante es menor que el saldo de algun concepto y es la ultima ck (saldo 0)
                entonces hago la ck con el saldo del comprobante.

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar
----------------------------------------------------------------------------}
unit frmAnulacionNotasCobro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, Validate,
  DateEdit, ExtCtrls, ppCtrls, ppStrtch, ppRichTx, ppPrnabl, ppClass, ppBands,
  ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppDB, ppDBPipe,
  DBClient, Frm_CargoNotaCredito, FrmConsultaTransitosComprobanteAnular,
  ppParameter, ppModule, raCodMod, ppSubRpt, ReporteCK, ReporteNC, ImgList,
  ConstParametrosGenerales, frmReporteNotaCreditoElectronica,
  frmConsultaEstacionamientosComprobanteAAnular,        //REV.17
  FrmConsultaInfraccionesComprobanteAnular,             // SS_1081_CQU_20130826
  PeaProcsCN,
  SysUtilsCN;                                           //SS_1120_MVI_20130820

const
	KEY_F9 = VK_F9;
type
    Tfrm_AnulacionNotasCobro = class(TForm)
    gbBusqueda: TGroupBox;
    gbDatosComprobante: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    Label10: TLabel;
    lblNumeroComprobante: TLabel;
    edNumeroComprobante: TNumericEdit;
	btnGenerarOAnular: TButton;
	btnSalir: TButton;
	gbDocumentoOriginal: TGroupBox;
	spComprobantes: TADOStoredProc;
	spComprobantesNumeroConvenioFormateado: TStringField;
	spComprobantesNumeroConvenio: TStringField;
	spComprobantesCodigoConvenio: TIntegerField;
	spComprobantesTipoComprobante: TStringField;
    spComprobantesNumeroComprobante: TLargeintField;
    spComprobantesPeriodoInicial: TDateTimeField;
    spComprobantesPeriodoFinal: TDateTimeField;
	spComprobantesFechaEmision: TDateTimeField;
    spComprobantesFechaVencimiento: TDateTimeField;
    spComprobantesNombreCliente: TStringField;
    spComprobantesDomicilio: TStringField;
    spComprobantesComuna: TStringField;
	spComprobantesComentarios: TStringField;
    spComprobantesCodigoPostal: TStringField;
    spComprobantesSaldoPendiente: TStringField;
    spComprobantesAjusteSencilloAnterior: TStringField;
    spComprobantesAjusteSencilloActual: TStringField;
    spComprobantesTotalAPagar: TStringField;
    spComprobantesCodigoTipoMedioPago: TWordField;
    spComprobantesEstadoPago: TStringField;
    dblDocumentoOriginal: TDBListEx;
    gbNotaCredito: TGroupBox;
	btnModificarConcepto: TButton;
    btnEliminarConcepto: TButton;
    btnAgregarNuevoConcepto: TButton;
    Label2: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    lblTotalAPagar: TLabel;
	Label11: TLabel;
    lblEstado: TLabel;
	lblTotalNotaCredito: TLabel;
	lblImporteTotalNotaCredito: TLabel;
	lblTipoComprobante: TLabel;
	cbTipoComprobante: TVariantComboBox;
	dblNotaCredito: TDBListEx;
	btnBuscar: TButton;
	lblFechaVencimiento: TLabel;
	lblFecha: TLabel;
	cdsOriginal: TClientDataSet;
	dsOriginal: TDataSource;
	dsCredito: TDataSource;
	spObtenerCargosComprobanteAAnular: TADOStoredProc;
    //datos del clientDataSet
	cdsCredito: TClientDataSet;
	cdsCreditoCodigoConcepto: TIntegerField;
	cdsCreditoCodigoConvenio: TIntegerField;
	cdsCreditoImporte: TFloatField;
	cdsCreditoObservaciones: TStringField;
	cdsCreditoNroOriginal: TIntegerField;
	cdsCreditoDescriImporte: TStringField;
	cdsCreditoDescripcion: TStringField;
	cdsCreditoImporteOriginal: TFloatField;
	cdsCreditoNumeroMovimiento: TIntegerField;
	cdsCreditoCodigoConceptoOriginal: TIntegerField;
    cdsCreditoIndiceVehiculo: TStringField;
    //
	spObtenerConceptoQueAnula: TADOStoredProc;
    spAgregarMovimientoCuentaTemporal: TADOStoredProc;
    btnPasarConcepto: TButton;
	btnPasarTodos: TButton;
	spCargarTransitoEnTablaTemporal: TADOStoredProc;
    ppLineasCKDBPipeline: TppDBPipeline;
    dsLineasImpresionCK: TDataSource;
	spObtenerLineasImpresionCK: TADOStoredProc;
    spObtenerLineasImpresionCKDescripcion: TStringField;
    spObtenerLineasImpresionCKImporte: TLargeintField;
    spObtenerLineasImpresionCKDescImporte: TStringField;
    spObtenerLineasImpresionCKCodigoConcepto: TWordField;
    lblTotalCKsPreviasANKText: TLabel;
    lblTotalCKsPreviasANK: TLabel;
    lblMaximoTotalComprobanteCKText: TLabel;
    lblMaximoTotalComprobanteCK: TLabel;
    spLimpiarDatosTemporales: TADOStoredProc;
    spObtenerImporteTransitosAcreditados: TADOStoredProc;
    Img_Tilde: TImageList;
    chkPasarTransitoEstadoEspecial: TCheckBox;
    spAnularNotaCobro: TADOStoredProc;
    cbImpresorasFiscales: TVariantComboBox;
    lblImpresoraFiscal: TLabel;
    lblNumeroComprobanteFiscal: TLabel;
    edNumeroComprobanteFiscal: TNumericEdit;
    spChequearUsuarioConcurrenteAcreditando: TADOStoredProc;
    cdsCreditoImporteIVA: TFloatField;
    cdsCreditoPorcentajeIVA: TFloatField;
    spAnularBoleta: TADOStoredProc;
    cdsCreditoValorNeto: TFloatField;
    cdsCreditoDescriImporteIVA: TStringField;
    cdsOriginalConcepto: TIntegerField;
    cdsOriginalImporte: TFloatField;
    cdsOriginalDescripcion: TStringField;
    cdsOriginalDescriImporte: TStringField;
    cdsOriginalNumeroLinea: TIntegerField;
    cdsOriginalPasado: TStringField;
    cdsOriginalObservaciones: TStringField;
    cdsOriginalNumeroMovimiento: TIntegerField;
    cdsOriginalIndiceVehiculo: TStringField;
    cdsOriginalConceptoEspecial: TIntegerField;
    cdsOriginalSeleccionado: TBooleanField;
    cdsOriginalAfectoIVA: TBooleanField;
    cdsOriginalValorNeto: TFloatField;
    cdsOriginalImporteIVA: TFloatField;
    cdsOriginalDescriValorNeto: TStringField;
    cdsOriginalDescriImporteIVA: TStringField;
    cdsOriginalPorcentajeIVA: TFloatField;
    cdsOriginalDescriPorcentajeIVA: TStringField;
    cdsCreditoDescriPorcentajeIVA: TStringField;
    Label1: TLabel;
    lblValorRUT: TLabel;
    Label4: TLabel;
    lblValorNumeroConvenio: TLabel;
    edNumeroNotaCreditoFiscal: TNumericEdit;
    lblNumeroNotaCreditoFiscal: TLabel;
    spComprobantesNumeroComprobanteFiscal: TLargeintField;
    spComprobantesNumeroDocumento: TStringField;
    spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet: TADOStoredProc;
    StringField15: TStringField;
    StringField16: TStringField;
    IntegerField2: TIntegerField;
    StringField17: TStringField;
    LargeintField3: TLargeintField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    DateTimeField7: TDateTimeField;
    DateTimeField8: TDateTimeField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    WordField2: TWordField;
    StringField27: TStringField;
    LargeintField4: TLargeintField;
    StringField28: TStringField;
    spCrearProcesoFacturacion: TADOStoredProc;
    spObtenerNumeroTipoInternoComprobanteFiscal: TADOStoredProc;
    spObtenerImporteAnuladoConceptoComprobanteAAnular: TADOStoredProc;
    spObtenerTodosLosCodigoConcepto: TADOStoredProc;
    spActualizarInfraccionesComprobanteAnulado: TADOStoredProc;
    spComprobantesTieneConceptosInfractor: TWordField;
    spCargarEstacionamientoEnTablaTemporal: TADOStoredProc;
    cdsOriginalCodigoConcesionaria: TSmallintField;                             //SS-1006-NDR-20120817
    cdsCreditoCodigoConcesionaria: TSmallintField;                              //SS-1006-NDR-20120817
    cdsResumenConcesionarias: TClientDataSet;                                   //SS-1006-NDR-20120817
    cdsResumenConcesionariasCodigoConcesionaria: TSmallintField;                //SS-1006-NDR-20120817
    cdsResumenConcesionariasImporte: TFloatField;                               //SS-1006-NDR-20120817
    lblEnListaAmarilla: TLabel;                                                 //SS_660_MVI_20130718
    spObtenerTodosLosCodigoConceptoInfractores: TADOStoredProc;          		// SS_1081_CQU_20130826
    spCargarInfraccionesEnTablaTemporal: TADOStoredProc;                 		// SS_1081_CQU_20130826
	cdsCreditoSaldoDisponible: TFloatField;                                     //SS_1144_MCA_20131118
    spObtenerTotalCKporConcepto: TADOStoredProc;                                //SS_1144_MCA_20131118
    cdsTotalCKporConcepto: TClientDataSet;                                      //SS_1144_MCA_20131118
    cdsTotalCKporConceptoConcepto: TStringField;                                //SS_1144_MCA_20131118
    cdsTotalCKporConceptoTotal: TFloatField;
    cbbMotivoNotaCredito: TVariantComboBox;
    lblMotivoAnulacion: TLabel;                                  	//SS_1144_MCA_20131118
    procedure cbTipoComprobanteChange(Sender: TObject);
    procedure dblDocumentoOriginalDblClick(Sender: TObject);
	procedure dblDocumentoOriginalDrawText(Sender: TCustomDBListEx;
	  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
	  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
	  var DefaultDraw: Boolean);
	procedure btnModificarConceptoClick(Sender: TObject);
	procedure btnAgregarNuevoConceptoClick(Sender: TObject);
	procedure btnEliminarConceptoClick(Sender: TObject);
	procedure btnPasarTodosClick(Sender: TObject);
	procedure btnPasarConceptoClick(Sender: TObject);
	procedure btnBuscarClick(Sender: TObject);
	procedure btnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
	procedure btnGenerarOAnularClick(Sender: TObject);
	procedure CargarTiposComprobantes;
	procedure PasarConcepto;
    function  ChequearConcurrencia : boolean;
    function ObtenerTotalConceptosPasados(NumeroConcepto : Integer) : Extended;   //SS_1012_ALA_20111118
    function ObtenerTotalConceptosSinPasar(NumeroConcepto : Integer) : Integer;   // SS_1080_CQU_20130111
  private
    { Private declarations }
    FNumeroComprobante: Int64;
	FBotonBloqueadoPorError: boolean;
    FListaCodigoConceptoInfraBoletoPrejudicial,     // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat1,   // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat2,   // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat3,   // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat4,   // SS_1081_CQU_20130826
    FListaCodConceptoEstacionamiento,                       //REV.17
    FListaCodConceptoEstacionaAnterior,                     //REV.17
    FListaCodigoConceptoPeaje,
    FListaCodigosConceptosMovimiento,						//SS_1144_MCA_20131118
    FListaCodigosConceptosAnulacion,						//SS_1144_MCA_20131118
    FListaCodigoConceptoPeajeAnterior : TStringList;
	FTotalComprobante:double;
	FTotalComprobanteNeto:double; //precio sin IVA de movimientos en boletas
    FMaximoTotalComprobanteCK: double;
    TotalCKsPreviasANK: double;         //TASK_096_JMA_20160116
    CantidadCKsPreviasANK: integer;     //TASK_096_JMA_20160116
    ImporteTotal: double;               //TASK_096_JMA_20160116
	FCodigoConvenio: integer;
    FListaInfraccionesAlCredito,                     // SS_1081_CQU_20130826
    FListaEstacionamientosAlCredito,                        //REV.17
	FListaTransitosAlCredito: TStringList;
    FIdSesion : integer;
    FFechaProceso: TDateTime ;
    FCantidadDiasCreditosConIVA: integer;
    FNoSePuedeAcreditarIVA: boolean;
    FImporteTotalNotaCredito    : Double;   // SS_1081_CQU_20130201
    FTieneConceptoInfractor     : Boolean;  // SS_1081_CQU_20130201
    FEstadoPago                 : string;   // SS_1081_CQU_20130826
	function  CargarComprobantes: Boolean;
	procedure LimpiarDatosCliente;
	procedure LimpiarDatosComprobante;
	procedure LimpiarDetalles;
	procedure AnularNotaCobro;
    procedure AnularBoleta;
	procedure CargarDetalleComprobante;
	procedure UpdateImporteTotalCredito;
	function SeleccionarTransitosAAnular(aNumeroMovimiento: integer; var SelectedAll:boolean): Int64;
	function SeleccionarEstacionamientosAAnular(aNumeroMovimiento: integer; var SelectedAll:boolean): Int64;
    procedure LimpiarDatosTemporales;
    function ObtenerImporteRemanente(aCodigoConcepto: integer): double;
    //function LineaEsSeleccionable: boolean;
    function ObtenerIVA: Double;
    function ValidarNumeroFiscalNotaCredito:boolean;
    function SeleccionarInfraccionesAAnular(aNumeroMovimiento : integer; var SelectedAll:boolean): int64;   // SS_1081_CQU_20130826
    procedure LlenarComboMotivoAnulacion();      //TASK_058_JMA_20161014
 public
	{ Public declarations }
	function Inicializar : Boolean; //overload; mbecerra---> no tiene sentido
    function LlenarCodigosConceptosDePeajes : boolean;		//REV.15
    function LlenarCodigosConceptosInfractores : boolean;   // SS_1081_CQU_20130826
    function LlenarCodigosConceptosAnulacion : boolean;   // SS_1144_MCA_20131118
  end;

var
  frm_AnulacionNotasCobro: Tfrm_AnulacionNotasCobro;
  NumeroProcesoFacturacion : integer;
  NumeroComprobante: Integer;
  TipoComprobante: string;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  					LlenarCodigosConceptosDePeajes
  Author		: mbecerra
  Date			: 07/06/2005
  Description	: 			(Ref Fase II)
                	Llena las listas con todos los c�digos de conceptos de
                    peajes, peajes anteriores, estacionamientos y
                    estacionamientos anteriores para todas las concesionarias

-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobro.LlenarCodigosConceptosDePeajes;
resourcestring
    SQL_TIPO_AUTOPISTA      = 'SELECT dbo.CONST_TIPO_CONCESIONARIA_AUTOPISTA()';
    SQL_TIPO_ESTACIONA      = 'SELECT dbo.CONST_TIPO_CONCESIONARIA_ESTACIONAMIENTO()';

var
    TipoAutopista, TipoEstaciona : Integer;
begin
    TipoAutopista   := QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_AUTOPISTA);
    TipoEstaciona   := QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_ESTACIONA);
    
    FListaCodigoConceptoPeaje.Clear;
    FListaCodigoConceptoPeajeAnterior.Clear;
    FListaCodConceptoEstacionamiento.Clear;
    FListaCodConceptoEstacionaAnterior.Clear;
    spObtenerTodosLosCodigoConcepto.Close;
    with spObtenerTodosLosCodigoConcepto do begin
        Parameters.Refresh;
        Parameters.ParamByName('@TipoConceptoADevolver').Value	:= 3;
        Open;
        while not Eof do begin
            if FieldByName('CodigoTipoConcesionaria').AsInteger = TipoAutopista then begin
                FListaCodigoConceptoPeaje.Add(FieldByName('CodigoConceptoPeriodo').AsString);
                FListaCodigoConceptoPeajeAnterior.Add(FieldByName('CodigoConceptoPeriodoAnterior').AsString);
                FListaCodigosConceptosMovimiento.Add(FieldByName('CodigoConceptoPeriodo').AsString);			 //SS_1144_MCA_20131118
                FListaCodigosConceptosMovimiento.Add(FieldByName('CodigoConceptoPeriodoAnterior').AsString);     //SS_1144_MCA_20131118
            end;

            if FieldByName('CodigoTipoConcesionaria').AsInteger = TipoEstaciona then begin
                FListaCodConceptoEstacionamiento.Add(FieldByName('CodigoConceptoPeriodo').AsString);
                FListaCodConceptoEstacionaAnterior.Add(FieldByName('CodigoConceptoPeriodoAnterior').AsString);
                FListaCodigosConceptosMovimiento.Add(FieldByName('CodigoConceptoPeriodo').AsString);             //SS_1144_MCA_20131118
                FListaCodigosConceptosMovimiento.Add(FieldByName('CodigoConceptoPeriodoAnterior').AsString);     //SS_1144_MCA_20131118
            end;

            Next;
        end;
    end;

    spObtenerTodosLosCodigoConcepto.Close;

end;

{-----------------------------------------------------------------------------
  					LlenarCodigosConceptosAnulacion
  Author		: mcabello
  Date			: 18/11/2013
  Description	: 			(Ref Fase II)
                	Llena las listas con todos los c�digos de conceptos de
                    anulacion de peajes, peajes anteriores, estacionamientos y
                    estacionamientos anteriores para todas las concesionarias

-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobro.LlenarCodigosConceptosAnulacion;                         //SS_1144_MCA_20131118
var                                                                                        //SS_1144_MCA_20131118
	i:Integer;                                                                             //SS_1144_MCA_20131118
begin                                                                                      //SS_1144_MCA_20131118
	FListaCodigosConceptosAnulacion.Clear;                                                 //SS_1144_MCA_20131118
    i:=0;                                                                                  //SS_1144_MCA_20131118
    while i < FListaCodigosConceptosMovimiento.Count do begin                              //SS_1144_MCA_20131118
		spObtenerConceptoQueAnula.Close;                                                   //SS_1144_MCA_20131118
		spObtenerConceptoQueAnula.Parameters.ParamByName('@CodigoConcepto').Value := IIf(FListaCodigosConceptosMovimiento[i] = EmptyStr, 0, FListaCodigosConceptosMovimiento[i]);	//SS_1144_MCA_20131118
		spObtenerConceptoQueAnula.Open ;                                                   //SS_1144_MCA_20131118
    	if not spObtenerConceptoQueAnula.Eof then begin                                    //SS_1144_MCA_20131118
        	if FListaCodigosConceptosAnulacion.IndexOf(spObtenerConceptoQueAnula.FieldByName('CodigoConcepto').AsString)<0 then //SS_1144_MCA_20131118
        		FListaCodigosConceptosAnulacion.Add(spObtenerConceptoQueAnula.FieldByName('CodigoConcepto').AsString);          //SS_1144_MCA_20131118
		end;                                                                               //SS_1144_MCA_20131118
        Inc(i);                                                                            //SS_1144_MCA_20131118
    end;                                                                                   //SS_1144_MCA_20131118
    spObtenerConceptoQueAnula.Close;                                                       //SS_1144_MCA_20131118
end;                                                                                       //SS_1144_MCA_20131118


{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 07/06/2005
  Description:  Inicializaci�n de Este Formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobro.Inicializar: Boolean;
resourcestring
    MSG_ERROR_OBTENIENDO_PARAMETRO_DIAS_IVA     = 'Error obteniendo par�metro que indica cantidad de d�as que se puede acreditar IVA';
    MSG_ERROR_EN_PARAMETRO_DIAS_IVA             = 'El par�metro que indica cantidad de d�as que se puede acreditar IVA es inv�lido';

begin
    Result := False;
	CenterForm(Self);
	LimpiarDatosCliente;
	lblEstado.Caption := '';
	CargarTiposComprobantes;
    cbTipoComprobanteChange(self);
    CargarImpresorasFiscales(DMConnections.BaseCAC, cbImpresorasFiscales, 0);
	LimpiarDetalles;

    FListaCodigoConceptoPeaje		 	:= TStringList.Create;
    FListaCodigoConceptoPeajeAnterior	:= TStringList.Create;
    FListaCodConceptoEstacionamiento    := TStringList.Create;
    FListaCodConceptoEstacionaAnterior  := TStringList.Create;
	FListaTransitosAlCredito            := TStringList.Create;      //REV.17
    FListaEstacionamientosAlCredito     := TStringList.Create;      //REV.17
    FListaCodigosConceptosMovimiento	:= TStringList.Create;		//SS_1144_MCA_20131118
    FListaCodigosConceptosAnulacion		:= TStringList.Create;		//SS_1144_MCA_20131118

    FListaInfraccionesAlCredito                     :=  TStringList.Create; // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoPrejudicial      :=  TStringList.Create; // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat1    :=  TStringList.Create; // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat2    :=  TStringList.Create; // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat3    :=  TStringList.Create; // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat4    :=  TStringList.Create; // SS_1081_CQU_20130826
    // Deshabilito los botones porque no hay nada cargado                   // SS_1081_CQU_20130826
    btnPasarConcepto.Enabled                := False;                       // SS_1081_CQU_20130826
    btnAgregarNuevoConcepto.Enabled         := btnPasarConcepto.Enabled;    // SS_1081_CQU_20130826
    btnEliminarConcepto.Enabled             := btnPasarConcepto.Enabled;    // SS_1081_CQU_20130826
    btnModificarConcepto.Enabled            := btnPasarConcepto.Enabled;    // SS_1081_CQU_20130826
    btnPasarTodos.Enabled                   := btnPasarConcepto.Enabled;    // SS_1081_CQU_20130826
    chkPasarTransitoEstadoEspecial.Enabled  := btnPasarConcepto.Enabled;    // SS_1081_CQU_20130826

    LlenarCodigosConceptosDePeajes();
    LlenarCodigosConceptosInfractores();                                    // SS_1081_CQU_20130826
	//FIN REV.15
    LlenarCodigosConceptosAnulacion();                                          //SS_1144_MCA_20131118

   lblEnListaAmarilla.Visible:=False;                                                    //SS_660_MVI_20130718

    LlenarComboMotivoAnulacion();       //TASK_058_JMA_20161014


    FIdSesion :=  QueryGetValueInt(DMConnections.BaseCAC,'SELECT @@SPID');
    FFechaProceso := NowBase( DMConnections.BaseCAC ) ;
	FBotonBloqueadoPorError := False;
	FCodigoConvenio := 0;
    try
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_ACREDITAR_IVA', FCantidadDiasCreditosConIVA);
        if FCantidadDiasCreditosConIVA <= 0 then begin
            MsgBoxErr(MSG_ERROR_EN_PARAMETRO_DIAS_IVA, caption, caption, MB_ICONERROR);
            Exit;
        end;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_OBTENIENDO_PARAMETRO_DIAS_IVA, e.Message, caption, MB_ICONERROR);
            Exit;
        end;
    end;
  	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobro.LimpiarDetalles;
begin

	LimpiarDatosComprobante;
	btnGenerarOAnular.Enabled	:= False;
	//limpiamos la grilla inferior tambien
	cdsCredito.EmptyDataSet;
end;

{
    Function Name: CargarComprobantes
    Parameters : None
    Return Value: Boolean
    Author : flamas
    Date Created : 20/12/2004

    Description : Carga los datos del comprobante a anular.

    Revision : 16
        Author: pdominguez
        Date: 05/06/2010
        Description: Infractores Fase 2
            - Se deshabilita los botones btnPasarConcepto, btnAgregarNuevoConcepto, btnEliminarConcepto,
            btnModificarConcepto y el CheckBox chkPasarTransitoEstadoEspecial, en el caso de que el comprobante
            sea un comprobante con Conceptos Infractores.
}
function Tfrm_AnulacionNotasCobro.CargarComprobantes : boolean;
resourcestring
	MSG_YES 				        = 'Si';
	MSG_NO 					        = 'No';
	ERROR_GETTING_INVOICE	        = 'Error obteniendo el Comprobante';
	MSG_NOT_PAID 			        = 'No';
	MSG_INVOICE_VOIDED		        = 'El comprobante se encuentra Anulado.'
								        + crlf + 'No se puede anular.';
	MSG_COMPROBANTE_INEXISTENTE		= 'El comprobante no existe.';
    MSG_IVA_VENCIDO                 = 'Han transcurrido m�s de %d d�as desde la emisi�n del documento de d�bito' + CRLF +
                                     'El IVA de esta Nota de Cr�dito no es acreditable para el SII';
	MSG_TIPOCOMPROBANTE_INVALIDO	= 'El comprobante seleccionado es una Nota de Cobro Directa.' + CRLF +                                           //SS-985-NDR-20111025
                                  'En el men� PROCESOS debe seleccionar la opci�n : Generar Nota de Cr�dito a Nota de Cobro Directa ';           //SS-985-NDR-20111025
  //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(''%s'',GETDATE() )';                                                 //SS_660_MVI_20130718
  //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(''%s'',GETDATE(), NULL )';                                     //SS_660_MVI_20130729     // SS_660_CQU_20130711 //SS_660_MVI_20130718
    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%s) ';                                                //SS_660_MVI_20130729
   	cSQLEsConvenioDeBaja = 'SELECT dbo.EsConvenioDeBaja(%d)';                                                                                //SS_1120_MVI_20130820
    cSQLEsConvenioSinCuenta = 'SELECT dbo.EsConvenioSinCuenta(%d)';                                                                          //SS_1120_MVI_20130820
var
	EstadoPago, EstadoListaAmarilla: string;                                                                                                   //SS_660_MVI_20130729

begin
	FListaTransitosAlCredito.Clear;
    FListaEstacionamientosAlCredito.Clear;          //REV.17
    FListaInfraccionesAlCredito.Clear;              // SS_1081_CQU_20130826
	Result := False;
    TipoComprobante := cbTipoComprobante.Value;

    if	(cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA) OR
    	(cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) then
    begin
        with spObtenerNumeroTipoInternoComprobanteFiscal do begin
            Parameters.ParamByName('@NumeroComprobanteFiscal').Value := edNumeroComprobanteFiscal.Value;
            Parameters.ParamByName('@TipoComprobanteFiscal').Value := TipoComprobante;
            ExecProc;
            NumeroComprobante := Parameters.ParamByName('@NumeroComprobante').Value;
            TipoComprobante := Parameters.ParamByName('@TipoComprobante').Value;
        end;
        if ((TipoComprobante = TC_NOTA_COBRO_DIRECTA) or (TipoComprobante = TC_NOTA_COBRO_DIRECTA_BHTU)) then                 //SS-985-NDR-20111025
        begin                                                                                                                 //SS-985-NDR-20111025
          MsgBox(MSG_TIPOCOMPROBANTE_INVALIDO, Caption, MB_ICONERROR);                                                        //SS-985-NDR-20111025
          Exit;                                                                                                               //SS-985-NDR-20111025
        end;                                                                                                                  //SS-985-NDR-20111025
    end;

	spComprobantes.Close;
	spComprobantes.Parameters.ParamByName('@TipoComprobante').Value   := TipoComprobante;//cbTipoComprobante.Value;

    if cbTipoComprobante.Value = TC_BOLETA then begin
		spComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := edNumeroComprobanteFiscal.Text;
		spComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := cbImpresorasFiscales.Value;
		spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := null;
    end;

    if (cbTipoComprobante.Value = TC_NOTA_COBRO) then
    begin
        NumeroComprobante := edNumeroComprobante.ValueInt;
        spComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;//edNumeroComprobante.Text;
    end;

    if	(cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA) OR
    	(cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) then
    begin
        spComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    end;


    if ChequearConcurrencia then Exit; //si otro usuario esta haciendo un credito de este comprobante

	try
		spComprobantes.Open;
		LimpiarDetalles;
		if not spComprobantes.IsEmpty then begin
			EstadoPago                  := spComprobantes.FieldByName('EstadoPago').AsString;
			lblNombre.Caption           := spComprobantes.FieldByName('NombreCliente').AsString ;
			lblDomicilio.Caption        := spComprobantes.FieldByName('Domicilio').AsString + ' ' + spComprobantes.FieldByName('Comuna').AsString ;
			lblFecha.Caption            := FormatDateTime('dd/mm/yyyy', spComprobantes.FieldByName('FechaEmision').AsDateTime);
			lblFechaVencimiento.Caption := FormatDateTime('dd/mm/yyyy', spComprobantes.FieldByName('FechaVencimiento').AsDateTime);
			lblEstado.Caption 		    := QueryGetValue(DMConnections.BaseCAC,Format('SELECT dbo.ObtenerDescripcionEstadoComprobante(%s)',
											[QuotedStr(EstadoPago)]));
			FNumeroComprobante 		    := spComprobantes.FieldByName('NumeroComprobante').AsInteger;
			FCodigoConvenio 			:= spComprobantes.FieldByName('CodigoConvenio').AsInteger;
            lblValorRUT.Caption         := spComprobantes.FieldByName('NumeroDocumento').AsString;
            lblValorNumeroConvenio.Caption := spComprobantes.FieldByName('NumeroConvenioFormateado').AsString;
                                                                                                                     //SS_1120_MVI_20130820
       if SysUtilsCN.QueryGetBooleanValue(DMConnections.BaseCAC,                                                     //SS_1120_MVI_20130820
                            Format(cSQLEsConvenioDeBaja,                                                             //SS_1120_MVI_20130820
                                    [FCodigoConvenio])) then begin                                                   //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
            lblValorNumeroConvenio.Font.Color := clRed;                                                              //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
      end                                                                                                            //SS_1120_MVI_20130820
      else begin                                                                                                     //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
        lblValorNumeroConvenio.Font.Color := clBlack;                                                                //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
      end;                                                                                                           //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
      if SysUtilsCN.QueryGetBooleanValue(DMConnections.BaseCAC,                                                      //SS_1120_MVI_20130820
                        Format(cSQLEsConvenioSinCuenta,                                                              //SS_1120_MVI_20130820
                                [FCodigoConvenio])) then begin                                                       //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
        lblValorNumeroConvenio.Caption := lblValorNumeroConvenio.Caption + TXT_CONVENIO_SIN_CUENTA;                  //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
      end;                                                                                                           //SS_1120_MVI_20130820
                                                                                                                     //SS_1120_MVI_20130820
			FBotonBloqueadoPorError:= False;
            if ((cbTipoComprobante.Value = TC_BOLETA) OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA)) and
                (NowBase(DMConnections.BaseCAC) - spComprobantes.FieldByName('FechaEmision').AsDateTime > FCantidadDiasCreditosConIVA) then
            begin
                FNoSePuedeAcreditarIVA := True; //no se puede acreditar el IVA
                //MsgBox(format(MSG_IVA_VENCIDO , [FCantidadDiasCreditosConIVA]));
            end else
                    FNoSePuedeAcreditarIVA := False;

            // Rev. 16 (Infractores Fase 2)
            //btnPasarConcepto.Enabled               := spComprobantes.FieldByName('TieneConceptosInfractor').AsInteger = 0;    // SS_1081_CQU_20130826
            FEstadoPago                            := EstadoPago;                                                               // SS_1081_CQU_20130826
            btnPasarConcepto.Enabled               := not (FEstadoPago = COMPROBANTE_ANULADO);                                  // SS_1081_CQU_20130826
            btnAgregarNuevoConcepto.Enabled        := btnPasarConcepto.Enabled;
            btnEliminarConcepto.Enabled            := btnPasarConcepto.Enabled;
            btnModificarConcepto.Enabled           := btnPasarConcepto.Enabled;
            chkPasarTransitoEstadoEspecial.Enabled := btnPasarConcepto.Enabled;
            btnPasarTodos.Enabled                  := btnPasarConcepto.Enabled;                                                 // SS_1081_CQU_20130826
            // Fin Rev. 16 (Infractores Fase 2)
            FTieneConceptoInfractor                 := spComprobantes.FieldByName('TieneConceptosInfractor').AsInteger = 1;     // SS_1081_CQU_20130201
            // por ahora no se pueden hacer creditos a boletas de arriendo
            // ahora se puede. rev7
//            if (cbTipoComprobante.Value = TC_BOLETA ) then begin
//                if QueryGetValueInt(DMConnections.BaseCAC,format('Select dbo.ComprobanteTieneCuotas(''%s'',%d)',[ cbTipoComprobante.Value , FNumeroComprobante ])) > 0 then begin
//        			MsgBox(MSG_COMPROBANTE_CON_CUOTAS, Caption, MB_ICONERROR);
//                    Exit;
//                end;
//            end;
			Result := True;

            EstaClienteConMensajeEspecial(DMConnections.BaseCAC,spComprobantes.FieldByName('NumeroDocumento').AsString);  //SS_1408_MCA_20151027

			CargarDetalleComprobante;
			if (EstadoPago = COMPROBANTE_ANULADO) then begin
				MsgBox(MSG_INVOICE_VOIDED, Caption, MB_ICONINFORMATION);
				FBotonBloqueadoPorError:= true;
                Exit;
			end; // else if

          //Verifica si Convenio est� en Lista Amarilla                                                                                     //SS_660_MVI_20130729     //SS_660_MVI_20130718
          //lblEnListaAmarilla.Color:=clYellow;                                                                                             //SS_660_MVI_20130729    //SS_660_MVI_20130718
          //lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                                                          //SS_660_MVI_20130729    //SS_660_MVI_20130718
          //                                              Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                           //SS_660_MVI_20130729    //SS_660_MVI_20130718
          //                                                      [   spComprobantes.FieldByName('CodigoConvenio').AsString                 //SS_660_MVI_20130729    //SS_660_MVI_20130718
          //                                                      ]                                                                         //SS_660_MVI_20130729    //SS_660_MVI_20130718
          //                                                    )                                                                           //SS_660_MVI_20130729    //SS_660_MVI_20130718
          //                                           )='True';                                                                            //SS_660_MVI_20130729    //SS_660_MVI_20130718

        EstadoListaAmarilla:= QueryGetValue(   DMConnections.BaseCAC,                                                                         //SS_660_MVI_20130729
                                                 Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                         [   spComprobantes.FieldByName('CodigoConvenio').AsString                                                     //SS_660_MVI_20130729
                                                         ]                                                                                   //SS_660_MVI_20130729
                                                       )                                                                                     //SS_660_MVI_20130729
                                                       );                                                                                    //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
//        if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
        if EstadoListaAmarilla <> '' then begin                                                                                              //SS_660_MVI_20130909
                                                                                                                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
        lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end                                                                                                                                  //SS_660_MVI_20130729
        else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end;                                                                                                                                 //SS_660_MVI_20130729


		end else begin
			MsgBox(MSG_COMPROBANTE_INEXISTENTE, Caption, MB_ICONERROR);
        end;
	except
		on e: exception do begin
			MsgBoxErr(ERROR_GETTING_INVOICE, e.message, caption, MB_ICONERROR);
		end;
	end;
end;



{-----------------------------------------------------------------------------
  Function Name: btnGenerarOAnularClick
  Author:    mlopez
  Date Created: 06/10/2005
  Description: Se amplio la funcionalidad para que permita tanto la anulacion
  de una Nota de Cobro o la mera Generacion de una Nota de Credito (CK)
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobro.btnGenerarOAnularClick(Sender: TObject);
resourcestring                                                                                                                                // SS_1081_CQU_20130826  // SS_1081_CQU_20130201
//    TIENE_CONCEPTO_INFRACTOR_CREDITO_PARCIAL        = 'No se puede emitir Nota de Cr�dito parcial a Comprobantes con Conceptos Infractores';  // SS_1081_CQU_20130826  // SS_1081_CQU_20130201
    MSG_CAPTION = 'MOTIVO';
    MSG_MOTIVO_NO_SELECCIONADO = 'Seleccione un motivo de nota de credito';
    MSG_ANULACION_INVALIDA1 = 'La anulaci�n debe ser por el monto Total Comprobante';         //TASK_096_JMA_20160116
    MSG_ANULACION_INVALIDA2 = 'La anulaci�n debe ser por el monto M�ximo Total Cr�dito';             //TASK_096_JMA_20160116
    MSG_CORRECCION_INVALIDA1 = 'La correcci�n no puede ser por el monto Total Comprobante';        //TASK_096_JMA_20160116
    MSG_CORRECCION_INVALIDA2 = 'La correcci�n no puede ser por el monto M�ximo Total Cr�dito';        //TASK_096_JMA_20160116
var                                                                                                  //TASK_096_JMA_20160116
    idMotivo: integer;                                                                               //TASK_096_JMA_20160116
begin
    // Verifico que no se trate de una CK parcial
    //if FTieneConceptoInfractor and not (FImporteTotalNotaCredito = FMaximoTotalComprobanteCK) then begin    // SS_1081_CQU_20130826 // SS_1081_CQU_20130201
    //    MsgBox(TIENE_CONCEPTO_INFRACTOR_CREDITO_PARCIAL, Caption, MB_ICONWARNING);                          // SS_1081_CQU_20130826 // SS_1081_CQU_20130201
    //    Exit;                                                                                               // SS_1081_CQU_20130826 // SS_1081_CQU_20130201
    //end;                                                                                                    // SS_1081_CQU_20130826 // SS_1081_CQU_20130201

//TASK_058_JMA_20161014
    if cbbMotivoNotaCredito.ItemIndex = -1 then
    begin
       MsgBox(MSG_MOTIVO_NO_SELECCIONADO, MSG_CAPTION, MB_ICONEXCLAMATION or MB_DEFBUTTON1);
       Exit;
    end;
//TASK_058_JMA_20161014


{INICIO: TASK_096_JMA_20160116}
    idMotivo := StrToInt(Split(';',cbbMotivoNotaCredito.Value)[0]);
    if idMotivo = 1 Then
    begin
        if FMaximoTotalComprobanteCK <> ImporteTotal then
        begin
            if TotalCKsPreviasANK = 0 then
                MsgBox(MSG_ANULACION_INVALIDA1)
            else
                MsgBox(MSG_ANULACION_INVALIDA2);
            Exit;
        end;
    end
    else
    begin
        if FMaximoTotalComprobanteCK = ImporteTotal then
        begin
            if TotalCKsPreviasANK = 0 then
                MsgBox(MSG_CORRECCION_INVALIDA1)
            else
                MsgBox(MSG_CORRECCION_INVALIDA2);
            Exit;
        end;
    end;
{TERMINO: TASK_096_JMA_20160116}

    if (cbTipoComprobante.Value = TC_NOTA_COBRO)
      OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA)
      OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA)
      OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA)
      OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA)
    then
        AnularNotaCobro();

    if cbTipoComprobante.Value = TC_BOLETA then
        AnularBoleta();
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    flamas
  Date Created: 21/12/2004
  Description: si tocan la tecla F9 permito anular tambien
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobro.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
    if (Key = KEY_F9) and btnGenerarOAnular.Enabled then btnGenerarOAnular.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 08/08/2005
  Description: Permito salir del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobro.btnSalirClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 08/08/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobro.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos
	FListaTransitosAlCredito.Free;
    FListaEstacionamientosAlCredito.Free;       //REV.17
    FListaCodigoConceptoPeaje.Free;    			//REV.15
    FListaCodigoConceptoPeajeAnterior.Free;     //REV.15
    FListaCodConceptoEstacionamiento.Free;      //REV.17
    FListaCodConceptoEstacionaAnterior.Free;    //REV.17

    FListaInfraccionesAlCredito.Free;                   // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoPrejudicial.Free;    // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat1.Free;  // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat2.Free;  // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat3.Free;  // SS_1081_CQU_20130826
    FListaCodigoConceptoInfraBoletoMorosidadCat4.Free;  // SS_1081_CQU_20130826

	Action := caFree;
end;


procedure Tfrm_AnulacionNotasCobro.AnularBoleta;
resourcestring
	MSG_ERROR 				  = 'Error';
	MSG_SUCCESS				  = 'El comprobante %d se proces� correctamente.'#10#13 + 'Se gener� la Nota de Cr�dito %d ';
	MSG_ERROR_VOIDING_INVOICE = 'Error generando la Nota de Cr�dito';
	MSG_CONFIRM				  = 'Desea generar la Nota de Cr�dito ?';
	MSG_CAPTION 			  = 'Generar Nota de Cr�dito a Boleta';
	MSG_IMPRIMIR              = 'Desea imprimir la Nota de Cr�dito ?';
var
	sDescError : string;
	nNotaCredito : Int64;
    NumeroFiscalCredito: int64;
    frmReporteNotaCredito: TReporteNotaCreditoElectronicaForm;
    //REV 11
	frmReporteNC: TfrmReporteNC;

begin
 // procedimiento para generar una NC en base a la boleta
	//consulto al operador si esta seguro de realizar la operacion

    //if not ValidarNumeroFiscalNotaCredito then Exit;

	if MsgBox(MSG_CONFIRM, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;

	try
		//mbecerra 24-Junio-2009 DMConnections.BaseCAC.BeginTrans;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spAnulacionNotaCobro');

		//ahora hay que crear los movimientos con los cargos que estan en el cdsCredito
		cdsCredito.First;
		while not cdsCredito.Eof do begin
			spAgregarMovimientoCuentaTemporal.Parameters.Refresh;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConvenio'      ).Value := FCodigoConvenio;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IndiceVehiculo'      ).Value := IIF(cdsCredito.FieldByName('IndiceVehiculo').Value = '' , null, cdsCredito.FieldByName('IndiceVehiculo').Value);
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaHora'           ).Value := FFechaProceso;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConcepto'      ).Value := cdsCredito.FieldByName('CodigoConcepto').AsInteger;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Importe'             ).Value := cdsCredito.FieldByName('Importe').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@ImporteIVA'          ).Value := cdsCredito.FieldByName('ImporteIVA').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@PorcentajeIVA'       ).Value := cdsCredito.FieldByName('PorcentajeIVA').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@EsPago'              ).Value := False;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Observaciones'       ).Value := cdsCredito.FieldByName('Observaciones').AsString;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroPromocion'     ).Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroFinanciamiento').Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IdSesion'            ).Value := FIdSesion ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@TipoComprobante'     ).Value := TipoComprobante; //cbTipoComprobante.Value;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroComprobante'   ).Value := FNumeroComprobante;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaProcesamiento'  ).Value := FFechaProceso;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Usuario'             ).Value := UsuarioSistema;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Seleccionado'        ).Value := False;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroMovimiento'    ).Value := 0;
			spAgregarMovimientoCuentaTemporal.ExecProc;
			cdsCredito.Next;
		end;

        // REV 8: agrego generacion de proceso de facturacion

        with spCrearProcesoFacturacion do begin
        	Parameters.ParamByName('@Operador').Value := UsuarioSistema;
			Parameters.ParamByName('@FacturacionManual').Value := True;
			ExecProc;
        	NumeroProcesoFacturacion := Parameters.ParamByName('@NumeroProcesoFacturacion').Value;
        end;


        spAnularBoleta.Parameters.Refresh;
        spAnularBoleta.Parameters.ParamByName('@TipoComprobante').Value := TC_BOLETA;
        spAnularBoleta.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        //Parameters.ParamByName('@NumeroFiscalCredito').Value := NULL;//edNumeroNotaCreditoFiscal.ValueInt;
        spAnularBoleta.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        spAnularBoleta.Parameters.ParamByName('@PasarAEstadoEspecial').Value := false;
        // REV 8
        spAnularBoleta.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
        spAnularBoleta.Parameters.ParamByName('@NumeroNotaCredito').Value := NULL;
        spAnularBoleta.Parameters.ParamByName('@DescripcionError').Value := NULL;
        spAnularBoleta.Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value := NULL;
        //REV 11
        spAnularBoleta.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value := NULL;
        spAnularBoleta.Parameters.ParamByName('@MotivoCK').Value := cbbMotivoNotaCredito.Text;       //TASK_058_JMA_20161014
        spAnularBoleta.ExecProc;
        sDescError := Trim(spAnularBoleta.Parameters.ParamByName('@DescripcionError').Value);

        LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos

		//Verifico si hubo error al anular
		if (sDescError <> '') then begin
			//informo que hubo un error al anular
			MsgBoxErr(MSG_ERROR, sDescError, MSG_ERROR, MB_ICONERROR);
		end else begin
            //REV 11: lo dejo en la cola de envio a DBNET si el comprobante es electronico
            if spAnularBoleta.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then begin
                //REV 8
				with spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet do begin
					Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
					ExecProc;
                end;
            end;

            //mbecerra: DMConnections.BaseCAC.CommitTrans;
            DMConnections.BaseCAC.Execute('COMMIT TRAN spAnulacionNotaCobro');

            nNotaCredito := spAnularBoleta.Parameters.ParamByName('@NumeroNotaCredito').Value;
            NumeroFiscalCredito := spAnularBoleta.Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value;//edNumeroNotaCreditoFiscal.ValueInt;
			//informo que se anulo con exito
			//MsgBox( Format(MSG_SUCCESS, [FNumeroComprobante, NumeroFiscalCredito]), Caption, MB_ICONINFORMATION);
            MsgBox( Format(MSG_SUCCESS, [edNumeroComprobanteFiscal.ValueInt, NumeroFiscalCredito]), Caption, MB_ICONINFORMATION);
            if (MsgBox( Format(MSG_IMPRIMIR,[FNumeroComprobante, NumeroFiscalCredito]), Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin
                //REV 11: lo dejo en la cola de envio a DBNET si el comprobante es electronico
                if spAnularBoleta.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then begin
                	frmReporteNotaCredito:= TReporteNotaCreditoElectronicaForm.Create(self);
                    try
                    	if not frmReporteNotaCredito.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO,nNotaCredito,sDescError) then
                        	ShowMessage( sDescError )
                        else
                              frmReporteNotaCredito.Ejecutar;
                    finally
                          if Assigned(frmReporteNotaCredito) then
                            frmReporteNotaCredito.Release;
                    end;
                end
                else begin
                	frmReporteNC:= TfrmReporteNC.Create(self);
                    try
						if not frmReporteNC.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO,nNotaCredito,sDescError) then
								              ShowMessage( sDescError )
                        else
                              frmReporteNC.Ejecutar;
                    finally
                    	if Assigned(frmReporteNC) then
                            frmReporteNC.Release;
                    end;

                end;
            end;
		end;

	except on E: Exception do begin
    		MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
			//mbecerra 24-Junio-2009 DMConnections.BaseCAC.RollbackTrans;
            DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobro');
    	end;
  	end;

	LimpiarDetalles;
	LimpiarDatosCliente;
	LimpiarDatosComprobante;
	edNumeroComprobante.Clear;
    cbbMotivoNotaCredito.ItemIndex := -1;     //TASK_058_JMA_20161014

end;

{
    Procedure Name: AnularNotaCobro
    Parameters : None
    Author :
    Date Created :

    Description : Anula una Nota de Cobro

    Revision : 16
        Author: pdominguez
        Date: 05/06/2010
        Description: Infractores Fase 2
            - Se actualiza el estado y datos de nexo con su movimiento cuenta asociado,
            para las infracciones facturadas en el comprobante que ha sido anulado.
}
procedure Tfrm_AnulacionNotasCobro.AnularNotaCobro;
  {-----------------------------------------------------------------------------
	Function Name: TieneNKMedioPagoAutomatico
	Author:    lgisuk
	Date Created: 09/08/2005
	Description: Verifico si la nota de cobro tiene medio de pago autormatico
	Parameters: Sender: TObject
	Return Value: None
  -----------------------------------------------------------------------------}
  function TieneNKMedioPagoAutomatico(NumeroComprobante : int64) : Boolean;
  begin
	  Result := IIF( QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.TieneNKMedioPagoAutomatico ('+ inttostr(NumeroComprobante) +')') = 'True', True, False);
  end;

  {-----------------------------------------------------------------------------
	Function Name: TieneNKMedioPagoAutomatico
	Author:    lgisuk
	Date Created: 09/08/2005
	Description: Muestro un cartel con el estado de la nota de cobro
	Parameters: Sender: TObject
	Return Value: None
  -----------------------------------------------------------------------------}
  function MostrarEstadoNK (NumeroComprobante : int64) : Boolean;
  resourcestring
	  MSG_ERROR_SHOW_STATE = 'No se pudo mostrar el estado de la nota de cobro';
	  MSG_ERROR = 'Error';
  const
	  THE_DOCUMENT_HAS_AUTOMATIC_PAYMENT = 'El Comprobante Tiene Medio Pago Automatico!';
	  STATE_SENDING      = 'Estado Envio : ';
	  STATE_PAYMENT      = 'Estado Pago : ';
	  DATE_SENDING       = 'Fecha Envio : ';
	  FILE_NAME          = 'Nombre Archivo : ';
	  DESCRIPTION_REJECT = 'Descripci�n Rechazo : ';
	  DATE_REJECT        = 'Fecha Rechazo : ';
	  OBSERVATION_REJECT = 'Observaci�n Rechazo : ';
  var
	  SP: TAdoStoredProc;
	  FechaEnvio, FechaRechazo : AnsiString;
  begin
	  Result := False;
	  try
		  try
			  SP := TADOStoredProc.Create(nil);
			  SP.Connection := DMConnections.BaseCAC;
			  SP.ProcedureName := 'ObtenerEstadoNK';
			  SP.Parameters.Refresh;
			  SP.Parameters.ParamByName('@NumeroComprobante').Value:= NumeroComprobante;
			  SP.Open;
			  if not SP.Eof then begin
				  //Obtengo las fechas
				  FechaEnvio :=  sp.FieldByName('FechaEnvio').AsString;
				  FechaRechazo := sp.FieldByName('FechaRechazo').AsString;
				  //Informo el mensaje
				  MsgBox(THE_DOCUMENT_HAS_AUTOMATIC_PAYMENT + CRLF +
						' ' + CRLF +
						STATE_SENDING         + sp.FieldByName('EstadoEnvio').AsString   + CRLF +
						STATE_PAYMENT         + sp.FieldByName('EstadoPago').AsString    + CRLF +
						DATE_SENDING          + IIF(FechaEnvio = '', 'N/A', FechaEnvio)  + CRLF +
						FILE_NAME             + sp.FieldByName('NombreArchivo').AsString + CRLF +
						DESCRIPTION_REJECT    + sp.FieldByName('DescripcionRechazo').AsString + CRLF +
						DATE_REJECT           + IIF(FechaRechazo = '', 'N/A', FechaRechazo)   + CRLF +
						OBSERVATION_REJECT    + sp.FieldByName('ObservacionRechazo').AsString + CRLF +
						' ',
						Caption,
						MB_ICONWARNING);
			  end;
			  SP.Close;
			  result := true;
		  except
			  on E: Exception do begin
					MsgBoxErr(MSG_ERROR_SHOW_STATE, e.message, caption, MB_ICONERROR);
			  end;
		  end;
	  finally
		  FreeAndNil(SP);
	  end;
  end;

resourcestring
	MSG_ERROR 				  = 'Error';
	MSG_SUCCESS				  = 'El comprobante %d se proces� correctamente.'#10#13 + 'Se gener� la Nota de Cr�dito %d a Nota de Cobro';
	MSG_ERROR_VOIDING_INVOICE = 'Error generando la nota de cr�dito';
	MSG_CONFIRM				  = 'Desea generar la nota de cr�dito ?';
	MSG_CAPTION 			  = 'Anular Nota de Cobro';
	MSG_IMPRIMIR              = 'Desea imprimir la N�ta de Cr�dito ?';
    MSG_DBNET					= 'Error al insertar registro en la Cola de Env�o a DBNet';
var
	sDescError : string;
	nNotaCredito : Int64;
	index: integer;
    frmReporteNKE: TReporteNotaCreditoElectronicaForm;
	frmReporteCK: TfrmReporteCK;
    NotaCreditoFiscal: int64;
    idMotivo: Integer;              //TASK_096_JMA_20160116
begin
	//Verifico si la nota de cobro tenia medio de pago automatico vigente confirmado
	if TieneNKMedioPagoAutomatico(FNumeroComprobante) then
    begin
		//si es asi informo un cartel de advertencia
		MostrarEstadoNK (FNumeroComprobante);
	end;
	//consulto al operador si esta seguro de realizar la operacion
	if MsgBox(MSG_CONFIRM, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then
        Exit;

    if ChequearConcurrencia then
        Exit; //si otro usuario esta haciendo una CK de esta NK

	try
		//Anulo la nota de cobro
		//mbecerra 24-Junio-2009 DMConnections.BaseCAC.BeginTrans;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spAnulacionNotaCobro');
		//Cargar tabla temporal de transitos
		for index := 0 to FListaTransitosAlCredito.Count -1 do begin
			spCargarTransitoEnTablaTemporal.Parameters.Refresh;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@IdSesion'          ).Value := FIdSesion ;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@TipoComprobante'   ).Value := TipoComprobante;//cbTipoComprobante.Value;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@NumeroComprobante' ).Value := NumeroComprobante;//edNumeroComprobante.Text;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@FechaProcesamiento').Value := FFechaProceso;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@Usuario'           ).Value := UsuarioSistema;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@NumCorrCA'         ).Value := StrToInt(FListaTransitosAlCredito.Names[index]);
			spCargarTransitoEnTablaTemporal.ExecProc;
		end;

        //Rev.18 / 04-Abril-2011 / Nelson Droguett Sierra ------------------------------------------------------------------------------------------------------------------------------------------
		for index := 0 to FListaEstacionamientosAlCredito.Count -1 do begin                                                                                                  //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.Parameters.Refresh;                                                                                                       //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.Parameters.ParamByName('@IdSesion'              ).Value := FIdSesion ;                                                        //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.Parameters.ParamByName('@TipoComprobante'       ).Value := TipoComprobante;                                                   //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.Parameters.ParamByName('@NumeroComprobante'     ).Value := NumeroComprobante;                                                 //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.Parameters.ParamByName('@FechaProcesamiento'    ).Value := FFechaProceso;                                                     //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.Parameters.ParamByName('@Usuario'               ).Value := UsuarioSistema;                                                    //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.Parameters.ParamByName('@NumCorrEstacionamiento').Value := StrToInt(FListaEstacionamientosAlCredito.Names[index]);            //PAR00133-NDR-20110331
			spCargarEstacionamientoEnTablaTemporal.ExecProc;                                                                                                                 //PAR00133-NDR-20110331
		end;                                                                                                                                                                 //PAR00133-NDR-20110331
        //FinRev.18----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        for index := 0 to FListaInfraccionesAlCredito.Count - 1 do begin                                                                                    // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.Parameters.Refresh;                                                                                         // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.Parameters.ParamByName('@IdSesion'          ).Value := FIdSesion ;                                          // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.Parameters.ParamByName('@TipoComprobante'   ).Value := TipoComprobante;                                     // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.Parameters.ParamByName('@NumeroComprobante' ).Value := NumeroComprobante;                                   // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.Parameters.ParamByName('@FechaProcesamiento').Value := FFechaProceso;                                       // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.Parameters.ParamByName('@Usuario'           ).Value := UsuarioSistema;                                      // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.Parameters.ParamByName('@CodigoInfraccion'  ).Value := StrToInt(FListaInfraccionesAlCredito.Names[index]);  // SS_1081_CQU_20130826
            spCargarInfraccionesEnTablaTemporal.ExecProc;                                                                                                   // SS_1081_CQU_20130826
        end;                                                                                                                                                // SS_1081_CQU_20130826

		//ahora hay que crear los movimientos con los cargos que estan en el cdsCredito
		cdsCredito.First;
		while not cdsCredito.Eof do begin
			spAgregarMovimientoCuentaTemporal.Parameters.Refresh;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConvenio'      ).Value := FCodigoConvenio;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IndiceVehiculo'      ).Value := IIF(cdsCredito.FieldByName('IndiceVehiculo').Value = '' , null, cdsCredito.FieldByName('IndiceVehiculo').Value);
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaHora'           ).Value := NowBase(DMConnections.BaseCAC);
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConcepto'      ).Value := cdsCredito.FieldByName('CodigoConcepto').AsInteger;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Importe'             ).Value := cdsCredito.FieldByName('Importe').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@ImporteIVA'          ).Value := null ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@PorcentajeIVA'       ).Value := null ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@EsPago'              ).Value := False;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Observaciones'       ).Value := cdsCredito.FieldByName('Observaciones').AsString;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroPromocion'     ).Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroFinanciamiento').Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IdSesion'            ).Value := FIdSesion ;
 			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@TipoComprobante'     ).Value := TipoComprobante;//cbTipoComprobante.Value;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroComprobante'   ).Value := NumeroComprobante;//edNumeroComprobante.Text;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaProcesamiento'  ).Value := FFechaProceso;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Usuario'             ).Value := UsuarioSistema;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Seleccionado'        ).Value := False;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroMovimiento'    ).Value := cdsCredito.FieldByName('NumeroMovimiento').AsInteger;            //SS_1006_10015_NDR_20121126
			spAgregarMovimientoCuentaTemporal.ExecProc;
			cdsCredito.Next;
		end;

        //ahora se cargan los movimientos Seleccionados para volver a cargar al convenio para proxima facturacion
        cdsOriginal.First;
        while not cdsOriginal.Eof do begin
            if (cdsOriginal.FieldByName('Seleccionado').Value = True) then begin
				spAgregarMovimientoCuentaTemporal.Parameters.Refresh;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConvenio'      ).Value := FCodigoConvenio;
				spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IndiceVehiculo'      ).Value := IIF(cdsOriginal.FieldByName('IndiceVehiculo').Value = '' , null, cdsOriginal.FieldByName('IndiceVehiculo').Value);
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaHora'           ).Value := NowBase(DMConnections.BaseCAC);
    			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConcepto'      ).Value := cdsOriginal.FieldByName('Concepto').AsInteger;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Importe'             ).Value := cdsOriginal.FieldByName('Importe').AsFloat * 100 ;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@EsPago'              ).Value := False;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Observaciones'       ).Value := cdsOriginal.FieldByName('Observaciones').AsString;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroPromocion'     ).Value := null;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroFinanciamiento').Value := null;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IdSesion'            ).Value := FIdSesion ;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@TipoComprobante'     ).Value := TipoComprobante; //cbTipoComprobante.Value;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroComprobante'   ).Value := NumeroComprobante; //edNumeroComprobante.Text;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaProcesamiento'  ).Value := FFechaProceso;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Usuario'             ).Value := UsuarioSistema;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Seleccionado'        ).Value := True;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroMovimiento'    ).Value := cdsOriginal.FieldByName('NumeroMovimiento').AsInteger;
                spAgregarMovimientoCuentaTemporal.ExecProc;
            end;
			cdsOriginal.Next;
        end;
        // agrego generacion de proceso de facturacion

        spCrearProcesoFacturacion.Parameters.ParamByName('@Operador').Value := UsuarioSistema;
        spCrearProcesoFacturacion.Parameters.ParamByName('@FacturacionManual').Value := True;
        spCrearProcesoFacturacion.ExecProc;
        NumeroProcesoFacturacion := spCrearProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value;


        spAnularNotaCobro.Parameters.Refresh;
        spAnularNotaCobro.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante; //TC_NOTA_COBRO;
        spAnularNotaCobro.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante; //FNumeroComprobante;
//INICIO: TASK_059_JMA_20161019
        spAnularNotaCobro.Parameters.ParamByName('@PasarAEstadoEspecial').Value := False; //chkPasarTransitoEstadoEspecial.Checked;     TASK_059_JMA_20161019
//TERMINO: TASK_059_JMA_20161019
        spAnularNotaCobro.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        spAnularNotaCobro.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
        spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCredito').Value := NULL;
        spAnularNotaCobro.Parameters.ParamByName('@DescripcionError').Value := NULL;
        spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value := NULL;
        spAnularNotaCobro.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value := NULL;
{INICIO: TASK_096_JMA_20160116
        spAnularNotaCobro.Parameters.ParamByName('@MotivoNC').Value := cbbMotivoNotaCredito.Text;     //TASK_058_JMA_20161014
}
        idMotivo := StrToInt(Split(';',cbbMotivoNotaCredito.Value)[0]);
        spAnularNotaCobro.Parameters.ParamByName('@Id_MotivoNC').Value := idMotivo;
{TERMINO: TASK_096_JMA_20160116}
        spAnularNotaCobro.ExecProc;
        sDescError := Trim(spAnularNotaCobro.Parameters.ParamByName('@DescripcionError').Value);


        // Rev. 16 (Infractores Fase 2)
        //with spActualizarInfraccionesComprobanteAnulado, spActualizarInfraccionesComprobanteAnulado.Parameters do begin   // SS_1080_CQU_20130826
        //    ParamByName('@TipoComprobante').Value := TipoComprobante;                                                     // SS_1081_CQU_20130826
        //    ParamByName('@NumeroComprobante').Value := NumeroComprobante;                                                 // SS_1081_CQU_20130826
        //    ExecProc;                                                                                                     // SS_1081_CQU_20130826
        //end;                                                                                                              // SS_1081_CQU_20130826
        // Fin Rev. 16 (Infractores Fase 2)

    	LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos

		//Verifico si hubo error al anular
		if (sDescError <> '') then begin
        	//informo que hubo un error al anular
			MsgBoxErr(MSG_ERROR, sDescError, MSG_ERROR, MB_ICONERROR);
        	// si hay un error en el SP hace rollback
        	//mbecerra: 24-Junio-2009 DMConnections.BaseCAC.RollbackTrans;
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT = 1 BEGIN ROLLBACK TRAN spAnulacionNotaCobro END');
		end
        else
        begin
        	//REV 11: inserto en la cola si la Nota credito es fiscal (electronica)
            if spAnularNotaCobro.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then
            begin
                spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
                spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet.ExecProc;
                Index := spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet.Parameters.ParamByName('@RETURN_VALUE').Value;
                if Index < 0 then
                begin
                    MsgBox(MSG_DBNET, Caption, MB_ICONERROR);
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT = 1 BEGIN ROLLBACK TRAN spAnulacionNotaCobro END');
                    Exit;
                end;
         	end;

			//mbecerra 24-Junio-2009 DMConnections.BaseCAC.CommitTrans;
            DMConnections.BaseCAC.Execute ('IF @@TRANCOUNT = 1 BEGIN COMMIT TRAN spAnulacionNotaCobro END');

        	nNotaCredito := spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCredito').Value;
        	NotaCreditoFiscal := spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value;

			 //informo que se anulo con exito
       		if (edNumeroComprobante.Enabled = true) then
            	MsgBox( Format(MSG_SUCCESS, [edNumeroComprobante.ValueInt, NotaCreditoFiscal]), Caption, MB_ICONINFORMATION)
       		else
            	MsgBox( Format(MSG_SUCCESS, [edNumeroComprobanteFiscal.ValueInt, NotaCreditoFiscal]), Caption, MB_ICONINFORMATION);

			if (MsgBox( Format(MSG_IMPRIMIR,[FNumeroComprobante, nNotaCredito]), Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin
            	//REV 11: si la nota de credito es fiscal (electronica), imprimo con el formulario para comprobantes electronicos
				if spAnularNotaCobro.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then
                begin
                	frmReporteNKE:= TReporteNotaCreditoElectronicaForm.Create(nil);
                    try
                    	if not frmReporteNKE.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO_A_COBRO,nNotaCredito,sDescError) then
							ShowMessage( sDescError )
                      	else
                          	frmReporteNKE.Ejecutar;
                    finally
                    	if Assigned(frmReporteNKE) then
                            frmReporteNKE.Release;     
                    end;      
            	end
                else
                begin
					//Imprimo con el formulario de comprobantes no electr�nicos
                    frmReporteCK:= TfrmReporteCK.Create(nil);
                  	try
						if not frmReporteCK.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO_A_COBRO,nNotaCredito,sDescError) then
                    		ShowMessage( sDescError )
                    	else
                    		frmReporteCK.Ejecutar;
                  	finally
                    	if Assigned(frmReporteCK) then
                            frmReporteCK.Release;
                    end;
            	end;
			end;
		end;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
			      //mbecerra 24-Junio-2009 DMConnections.BaseCAC.RollbackTrans;
                  //DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobro');
                  DMConnections.BaseCAC.Execute('IF @@TRANCOUNT = 1 BEGIN ROLLBACK TRAN spAnulacionNotaCobro END');                                       //SS_1012_ALA_20111128
        end;
    end;

	LimpiarDetalles;
	LimpiarDatosCliente;
    cbbMotivoNotaCredito.ItemIndex := -1;   //TASK_058_JMA_20161014
	//LimpiarDatosComprobante;                                                  //SS_1144_MCA_20131118
    if edNumeroComprobante.Enabled = true then
	    edNumeroComprobante.Clear
    else
        edNumeroComprobanteFiscal.Clear;
    chkPasarTransitoEstadoEspecial.Checked := false;
end;

procedure Tfrm_AnulacionNotasCobro.LimpiarDatosCliente;
begin
	lblNombre.Caption       := EmptyStr;
	lblDomicilio.Caption    := EmptyStr;
end;

procedure Tfrm_AnulacionNotasCobro.LimpiarDatosComprobante;
begin
	lblFecha.Caption            := EmptyStr;
	lblTotalAPagar.Caption      := EmptyStr;
	lblFechaVencimiento.Caption := EmptyStr;
	lblEstado.Caption           := EmptyStr;
    lblValorNumeroConvenio.Caption  := EmptyStr;
    lblValorRut.Caption             := EmptyStr;
    cdsOriginal.EmptyDataSet ;
    cdsResumenConcesionarias.EmptyDataSet;                //SS-1006-NDR-20120817
    lblImporteTotalNotaCredito.Caption := EmptyStr;
    FImporteTotalNotaCredito                := 0;         // SS_1081_CQU_20130201
    lblTotalCKsPreviasANK.Visible           := False;
    lblTotalCKsPreviasANKText.Visible       := False;
    lblMaximoTotalComprobanteCKText.Visible := False;
    lblMaximoTotalComprobanteCK.Visible     := False;
    lblEnListaAmarilla.Visible  := False;                                               //SS_660_MVI_20130729

    btnPasarConcepto.Enabled                := False;		// SS_1144_MCA_20131118
    btnAgregarNuevoConcepto.Enabled         := False;		// SS_1144_MCA_20131118
    btnEliminarConcepto.Enabled             := False;    	// SS_1144_MCA_20131118
    btnModificarConcepto.Enabled            := False;    	// SS_1144_MCA_20131118
    btnPasarTodos.Enabled                   := False;    	// SS_1144_MCA_20131118
    chkPasarTransitoEstadoEspecial.Enabled  := False;    	// SS_1144_MCA_20131118
    cdsTotalCKporConcepto.EmptyDataSet;                     // SS_1144_MCA_20131118
end;

procedure Tfrm_AnulacionNotasCobro.CargarTiposComprobantes;
begin
	cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_COBRO),TC_NOTA_COBRO);
	cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_BOLETA),TC_BOLETA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_FACTURA_EXENTA),TC_FACTURA_EXENTA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_FACTURA_AFECTA),TC_FACTURA_AFECTA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_BOLETA_EXENTA),TC_BOLETA_EXENTA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_BOLETA_AFECTA),TC_BOLETA_AFECTA);
	cbTipoComprobante.ItemIndex := 0;
    cbImpresorasFiscales.Enabled := False;
    edNumeroComprobanteFiscal.Enabled := False ;
end;

procedure Tfrm_AnulacionNotasCobro.btnBuscarClick(Sender: TObject);
resourcestring
	MSG_NUMERO_INVALIDO = 'N�mero de comprobante inv�lido';
    MSG_CAPTION_CONTROL_REFINAN = 'Control Refinanciaci�n';
    MSG_MESSAGE_CONTROL_REFINAN = 'El Comprobante indicado, esta incluido en una Refinanciaci�n. No es posible hacer una Nota de Cr�dito al Comprobante.';

    MSG_CAPTION_COMPROBANTE_PAGADO = 'Comprobante tiene Pagos';
    MSG_MESSAGE_COMPROBANTE_PAGADO = 'El Comprobante indicado, tiene pagos registrados. Para hacer la Nota de Cr�dito es recomendable anular los pagos.';

    SQL_COMPROBANTE_TIENE_CONCEPTOS_OC	= 'SELECT dbo.VerificarComprobanteConceptosOtrasConcesionarias(''%s'',%d,1)';
    //SQL_COMPROBANTE_TIENE_PAGOS     	= 'SELECT dbo.ComprobanteTienePagos(''%s'',%d)';            //SS_1006_10015_NDR_20121112
    SQL_COMPROBANTE_TIENE_PAGOS     	= 'SELECT dbo.ComprobanteTieneRecibos(''%s'',%d)';            //SS_1006_10015_NDR_20121112

var
    bComprobanteTienePagos : Boolean;
begin
    bComprobanteTienePagos          := False;
    try
        if FNumeroComprobante <> 0 then LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos

        if cbTipoComprobante.Value = TC_NOTA_COBRO then
            if not ValidateControls([edNumeroComprobante],[edNumeroComprobante.Text<>''],caption,[MSG_NUMERO_INVALIDO]) then  Exit;
        if cbTipoComprobante.Value = TC_BOLETA then
            if not ValidateControls([edNumeroComprobanteFiscal],[edNumeroComprobanteFiscal.Text<>''],caption,[MSG_NUMERO_INVALIDO]) then  Exit;

        if (cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA)
            OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) then
            if not ValidateControls([edNumeroComprobanteFiscal],[edNumeroComprobanteFiscal.Text<>''],caption,[MSG_NUMERO_INVALIDO]) then  Exit;

        if True then
        if ChequearConcurrencia then Exit;

        CargarComprobantes;     // SS_637_PDO_20120205

        //if (QueryGetValue(   DMCOnnections.BaseCAC,                                                                                          //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //                     Format(SQL_COMPROBANTE_TIENE_PAGOS,[cbTipoComprobante.Value,                                                    //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //                                                         FNumeroComprobante]                                                         //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //                           )                                                                                                         //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //                 )='True') then                                                                                                      //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //begin                                                                                                                                //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //    bComprobanteTienePagos:= True;                                                                                                   //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //    ShowMsgBoxCN(MSG_CAPTION_COMPROBANTE_PAGADO, MSG_MESSAGE_COMPROBANTE_PAGADO, MB_ICONWARNING, Self);                              //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928 //SS_1012_ALA_20111128
        //end;                                                                                                                                 //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //
        //btnPasarConcepto.Enabled        := not bComprobanteTienePagos;                                                                       //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //btnPasarTodos.Enabled           := not bComprobanteTienePagos;                                                                       //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //btnAgregarNuevoConcepto.Enabled := not bComprobanteTienePagos;                                                                       //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //btnModificarConcepto.Enabled    := not bComprobanteTienePagos;                                                                       //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928
        //btnEliminarConcepto.Enabled     := not bComprobanteTienePagos;                                                                       //SS_1006_10015_NDR_20121015//SS_1006_10015_NDR_20120928

        btnAgregarNuevoConcepto.Enabled := (QueryGetValue(  DMCOnnections.BaseCAC,                                                          //SS_1006_10015_NDR_20120928
                                                            Format(SQL_COMPROBANTE_TIENE_CONCEPTOS_OC,[ TipoComprobante,        //SS_1006_MBE_20130911      cbTipoComprobante.Value,            //SS_1006_10015_NDR_20120928
                                                                                                        FNumeroComprobante]                 //SS_1006_10015_NDR_20120928
                                                                  )                                                                         //SS_1006_10015_NDR_20120928
                                                        //)='False');                                                                       // SS_1081_CQU_20130826  //SS_1006_10015_NDR_20120928
                                                        )='False') and not (FEstadoPago = COMPROBANTE_ANULADO);                             // SS_1081_CQU_20130826

        {     // Inicio Bloque: SS_637_PDO_20120205
        if CargarComprobantes then                                              //SS-985-NDR-20111025
        begin                                                                   //SS-985-NDR-20111025
          with fnEsComprobanteRefinanciado do begin                                                                                                                                                               // SS_637_PDO_20110808
              if Active then Close;                                                                                                                                                                               // SS_637_PDO_20110808
              Parameters.Refresh;                                                                                                                                                                                 // SS_637_PDO_20110808
                                                                                                                                                                                                                  // SS_637_PDO_20110808
              fnEsComprobanteRefinanciado.Parameters.ParamByName('@TipoComprobante').Value   := cbTipoComprobante.Value;                                                                                          // SS_637_PDO_20110808
              fnEsComprobanteRefinanciado.Parameters.ParamByName('@NumeroComprobante').Value := iif(Trim(edNumeroComprobante.Text) <> '', Trim(edNumeroComprobante.Text), Trim(edNumeroComprobanteFiscal.Text));  // SS_637_PDO_20110808
              ExecProc;                                                                                                                                                                                           // SS_637_PDO_20110808
                                                                                                                                                                                                                  // SS_637_PDO_20110808
              if fnEsComprobanteRefinanciado.Parameters.ParamByName('@RETURN_VALUE').Value = True then begin                                                                                                      // SS_637_PDO_20110808
                  btnPasarConcepto.Enabled  := False;                                                                                                                                                             // SS_637_PDO_20110808
                  btnPasarTodos.Enabled     := False;                                                                                                                                                             // SS_637_PDO_20110808
                  btnGenerarOAnular.Enabled := False;                                                                                                                                                             // SS_637_PDO_20110808
                                                                                                                                                                                                                  // SS_637_PDO_20110808
                  ShowMsgBoxCN(MSG_CAPTION_CONTROL_REFINAN, MSG_MESSAGE_CONTROL_REFINAN, MB_ICONWARNING, Self);                                                                                                   // SS_637_PDO_20110808
              end;                                                                                                                                                                                                // SS_637_PDO_20110808
          end;                                                                                                                                                                                                    // SS_637_PDO_20110808
        end;                                                                    //SS-985-NDR-20111025
        }     // Fin Bloque: SS_637_PDO_20120205
    except                                                                                                                                   // SS_637_PDO_20110808
        on E:Exception do begin                                                                                                              // SS_637_PDO_20110808
            ShowMsgBoxCN(e, Self);                                                                                                           // SS_637_PDO_20110808
        end;                                                                                                                                 // SS_637_PDO_20110808
    end;                                                                                                                                     // SS_637_PDO_20110808
end;

procedure Tfrm_AnulacionNotasCobro.CargarDetalleComprobante;
resourcestring
    MSG_IVA_VENCIDO_MAXIMO_CREDITO = 'El importe m�ximo del cr�dito deber� ser de %s por estar vencido el IVA' +  CRLF +
                                     'ya que han transcurrido mas de %d d�as desde la emisi�n de la boleta el d�a %s';

    MSG_IVA_VENCIDO_CREDITO_NEGADO = 'Han transcurrido mas de %d d�as desde la emisi�n de la boleta el d�a %s' +  CRLF +
                                     'y los cr�ditos previos a la boleta superan el neto. No se puede realizar otro cr�dito';
    SQL_FLOATTOSTR 				   = 'SELECT dbo.FloatToStrD(%s / 100, 1, 0, 1 )';                                            //SS_1144_MCA_20131118
var
  xIndex: integer;
  //TotalCKsPreviasANK: double;         TASK_096_JMA_20160116
  //CantidadCKsPreviasANK: integer;     TASK_096_JMA_20160116
  Importe: Double;                                                              //SS_1144_MCA_20131118
begin
	spObtenerCargosComprobanteAAnular.Close;
	spObtenerCargosComprobanteAAnular.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;//cbTipoComprobante.Value;
	spObtenerCargosComprobanteAAnular.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
	spObtenerCargosComprobanteAAnular.Open;
	xIndex := 1;
	FTotalComprobante := 0 ;
    FTotalComprobanteNeto := 0 ;
	cdsOriginal.EmptyDataSet;
	cdsTotalCKporConcepto.EmptyDataSet;                                         				//SS_1144_MCA_20131118
    if not spObtenerCargosComprobanteAAnular.Eof then begin                     				//SS_1144_MCA_20131118
    	with spObtenerTotalCKporConcepto do begin                               				//SS_1144_MCA_20131118
        	Close;                                                              				//SS_1144_MCA_20131118
        	Parameters.Refresh;                                                 				//SS_1144_MCA_20131118
        	Parameters.ParamByName('@NumeroComprobante').Value := edNumeroComprobante.Value;    //SS_1144_MCA_20131118
            Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;				//SS_1144_MCA_20131118
            Open;                                                               				//SS_1144_MCA_20131118
        end;                                                                    				//SS_1144_MCA_20131118
    end;                                                                        				//SS_1144_MCA_20131118
    while not spObtenerTotalCKporConcepto.eof do begin                                                               //SS_1144_MCA_20131118
        cdsTotalCKporConcepto.AppendRecord([spObtenerTotalCKporConcepto.FieldByName('CodigoConcepto').AsInteger,     //SS_1144_MCA_20131118
                                                 spObtenerTotalCKporConcepto.FieldByName('TotalCK').AsFloat]);       //SS_1144_MCA_20131118
        spObtenerTotalCKporConcepto.Next;                                                                            //SS_1144_MCA_20131118
    end;                                                                                                             //SS_1144_MCA_20131118

	while not spObtenerCargosComprobanteAAnular.Eof do begin
		cdsOriginal.Append;
        cdsOriginal.FieldByName('Concepto').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('CodigoConcepto').AsString ;
        //cdsOriginal.FieldByName('Importe').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat ;																											//SS_1144_MCA_20131118
		//cdsOriginal.FieldByName('Importe').Value 		:=	spObtenerCargosComprobanteAAnular.FieldByName('SaldoDisponible').AsFloat ;																									//SS_1144_MCA_20131118
        //cdsOriginal.FieldByName('DescriImporte').Value := 	spObtenerCargosComprobanteAAnular.FieldByName('DescriImporte').AsString ;																								//SS_1144_MCA_20131118
		//cdsOriginal.FieldByName('DescriImporte').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('DescriSaldoDisponible').AsString;																						//SS_1144_MCA_20131118


        if cdsTotalCKporConcepto.Locate('Concepto',                                                                              //SS_1144_MCA_20131118
                                            spObtenerCargosComprobanteAAnular.FieldByName('CodigoConcepto').AsInteger, []) then  //SS_1144_MCA_20131118
        begin                                                                                                                    //SS_1144_MCA_20131118
        	Importe := IIF(spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat + cdsTotalCKporConceptoTotal.Value < 0, 0, spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat + cdsTotalCKporConceptoTotal.Value);	//SS_1144_MCA_20131118
        	cdsOriginal.FieldByName('Importe').Value := IIF(Importe > 0, Importe, 0);                                                                                                                                                           //SS_1144_MCA_20131118
            cdsOriginal.FieldByName('DescriImporte').Value 	:= QueryGetStringValue(DMCOnnections.BaseCAC, Format(SQL_FLOATTOSTR, [FloatToStr(Importe*100)]));																					//SS_1144_MCA_20131118
            if cdsTotalCKporConceptoTotal.Value < 0 then begin                                                                                                                                                                                  //SS_1144_MCA_20131118
                cdsTotalCKporConcepto.Edit;                                                                                                                                                                                                     //SS_1144_MCA_20131118
	            cdsTotalCKporConceptoTotal.Value := cdsTotalCKporConceptoTotal.Value + spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat;                                                                                        //SS_1144_MCA_20131118
                if cdsTotalCKporConceptoTotal.Value>0 then                                                                                                                                                                                      //SS_1144_MCA_20131118
					cdsTotalCKporConceptoTotal.Value := 0;                                                                                                                                                                                      //SS_1144_MCA_20131118
            	cdsTotalCKporConcepto.Post;                                                                                                                                                                                                     //SS_1144_MCA_20131118
            end;                                                                                                                                                                                                                                //SS_1144_MCA_20131118
        end                                                                                                                                                                                                                                     //SS_1144_MCA_20131118
        else begin                                                                                                                                                                                                                              //SS_1144_MCA_20131118
        	if FListaCodigoConceptoPeaje.IndexOf(cdsOriginal.FieldByName('Concepto').Value) >= 0 then      																																		//SS_1144_MCA_20131118
            begin                                                                                          																																		//SS_1144_MCA_20131118
				cdsOriginal.FieldByName('Importe').Value 		:=	IIF(spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat + spObtenerCargosComprobanteAAnular.FieldByName('SaldoDisponible').AsFloat<0, 0, spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat + spObtenerCargosComprobanteAAnular.FieldByName('SaldoDisponible').AsFloat);				//SS_1144_MCA_20131118
    	        cdsOriginal.FieldByName('DescriImporte').Value 	:= 	QueryGetStringValue(DMCOnnections.BaseCAC, Format(SQL_FLOATTOSTR, [FloatToStr(cdsOriginal.FieldByName('Importe').Value*100)]));												//SS_1144_MCA_20131118
            end                                                                                            																																		//SS_1144_MCA_20131118
            else begin                                                                                     																																		//SS_1144_MCA_20131118
            	cdsOriginal.FieldByName('Importe').Value 		:=	spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat; 																											//SS_1144_MCA_20131118
    	        cdsOriginal.FieldByName('DescriImporte').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('DescriSaldoDisponible').AsString; 																							//SS_1144_MCA_20131118
            end;                                                                                                                                                                                                                                //SS_1144_MCA_20131118
        end;                                                                                                                                                                                                                                    //SS_1144_MCA_20131118

        //cdsOriginal.FieldByName('DescriImporte').Value := 	spObtenerCargosComprobanteAAnular.FieldByName('DescriImporte').AsString ;																										//SS_1144_MCA_20131118


        if (cbTipoComprobante.Value = TC_BOLETA) OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR  (cbTipoComprobante.Value = TC_BOLETA_AFECTA) then begin
        	cdsOriginal.FieldByName('ValorNeto').Value 			:= 	spObtenerCargosComprobanteAAnular.FieldByName('ValorNeto').AsFloat ;
            cdsOriginal.FieldByName('DescriValorNeto').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('DescriValorNeto').AsString ;
            cdsOriginal.FieldByName('ImporteIVA').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('ImporteIVA').AsFloat ;
            cdsOriginal.FieldByName('DescriImporteIVA').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('DescriImporteIVA').AsString ;

            cdsOriginal.FieldByName('PorcentajeIVA').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('PorcentajeIVA').AsFloat ;
              //el porcentaje es el que se levanta, si no se edita el registro sigue siendo el mismo.
              //esto puede ser malo o bueno al momento del cambio del IVA
              //si hiciera falta hacer hoy un credito con el mismo porcentaje de IVA del pasado, entonces esta bueno
              //si el cr�dito siempre debiera hacerse con el IVA nuevo, entonces los cargos deberian editarse para que tomen el nuevo IVA
        end;

        cdsOriginal.FieldByName('Descripcion').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('Descripcion').AsString ;
        cdsOriginal.FieldByName('Observaciones').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('Observaciones').AsString ;
        cdsOriginal.FieldByName('NumeroMovimiento').Value := spObtenerCargosComprobanteAAnular.FieldByName('NumeroMovimiento').AsInteger ;
        cdsOriginal.FieldByName('IndiceVehiculo').Value := IIF(spObtenerCargosComprobanteAAnular.FieldByName('IndiceVehiculo').IsNull, ' ' ,spObtenerCargosComprobanteAAnular.FieldByName('IndiceVehiculo').AsString ) ;
        cdsOriginal.FieldByName('NumeroLinea').Value 	:= 	xIndex ;
        cdsOriginal.FieldByName('Pasado').Value := 	'N';
        cdsOriginal.FieldByName('CodigoConcesionaria').Value := spObtenerCargosComprobanteAAnular.FieldByName('CodigoConcesionaria').AsInteger;     //SS-1006-NDR-20120817


        if cdsResumenConcesionarias.Locate('CodigoConcesionaria',                                                                                                      //SS-1006-NDR-20120817
                                            spObtenerCargosComprobanteAAnular.FieldByName('CodigoConcesionaria').AsInteger, [loPartialKey]) then                       //SS-1006-NDR-20120817
        begin                                                                                                                                                          //SS-1006-NDR-20120817
          cdsResumenConcesionarias.Edit;                                                                                                                               //SS-1006-NDR-20120817
          cdsResumenConcesionariasImporte.Value := cdsResumenConcesionariasImporte.Value + spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat;           //SS-1006-NDR-20120817
          cdsResumenConcesionarias.Post;                                                                                                                               //SS-1006-NDR-20120817
        end                                                                                                                                                            //SS-1006-NDR-20120817
        else                                                                                                                                                           //SS-1006-NDR-20120817
        begin                                                                                                                                                          //SS-1006-NDR-20120817
          cdsResumenConcesionarias.AppendRecord([spObtenerCargosComprobanteAAnular.FieldByName('CodigoConcesionaria').AsInteger,                                       //SS-1006-NDR-20120817
                                                 spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat]);                                                   //SS-1006-NDR-20120817
        end;                                                                                                                                                           //SS-1006-NDR-20120817
        

        FTotalComprobante := FTotalComprobante + spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat;
        //if (cbTipoComprobante.Value = TC_BOLETA) and FNoSePuedeAcreditarIVA
        //then
        //FTotalComprobanteNeto := FTotalComprobanteNeto + spObtenerCargosComprobanteAAnular.FieldByName('ValorNeto').AsFloat;

        Inc(xIndex);
		spObtenerCargosComprobanteAAnular.Next;
	end;
    cdsOriginal.First;  //porque si no queda en eof a pesar de haber cargado filas, y los botones de agregar y modificar no funcionan bien

    TotalCKsPreviasANK      := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerImporteCanceladoDeCKaUnaNK(''%s'',%d)',[TipoComprobante,FNumeroComprobante])) / 100 ;
    CantidadCKsPreviasANK   := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerCantidadDeCKaUnaNK(''%s'',%d)',[TipoComprobante,FNumeroComprobante])) ;
    //if (cbTipoComprobante.Value = TC_BOLETA) and FNoSePuedeAcreditarIVA
    //then
    //    FMaximoTotalComprobanteCK  := FTotalComprobanteNeto - TotalCKsPreviasANK
    //else
    FMaximoTotalComprobanteCK  := FTotalComprobante - TotalCKsPreviasANK ;


    if FMaximoTotalComprobanteCK <= 0 then  //si ya no se puede acreditar el IVA y se habi�n hecho cr�ditos por un importe superior al neto de la boleta, quedar�a negativo
        FMaximoTotalComprobanteCK := 0;

    lblTotalCKsPreviasANK.Caption           := FormatearImporteConDecimales(DMConnections.BaseCAC, TotalCKsPreviasANK, True, False, True);
    lblTotalCKsPreviasANKText.Caption       := 'Total ' + IntToStr(CantidadCKsPreviasANK) + ' Cr�ditos previos :' ;
	lblTotalAPagar.Caption                  := FormatearImporteConDecimales(DMConnections.BaseCAC, FTotalComprobante, True, False, True);
    lblMaximoTotalComprobanteCK.Caption     := FormatearImporte(DMConnections.BaseCAC, Trunc(FMaximoTotalComprobanteCK));
    lblTotalCKsPreviasANK.Visible           := (CantidadCKsPreviasANK > 0);
    lblTotalCKsPreviasANKText.Visible       := (CantidadCKsPreviasANK > 0);
    lblMaximoTotalComprobanteCKText.Visible := (CantidadCKsPreviasANK > 0);
    lblMaximoTotalComprobanteCK.Visible     := (CantidadCKsPreviasANK > 0);
    {if FNoSePuedeAcreditarIVA then begin
        lblMaximoTotalComprobanteCKText.Visible := True;
        lblMaximoTotalComprobanteCK.Visible     := True;
        if FMaximoTotalComprobanteCK = 0 then MsgBox(format(MSG_IVA_VENCIDO_CREDITO_NEGADO , [FCantidadDiasCreditosConIVA, lblFecha.Caption]))
            else MsgBox(format(MSG_IVA_VENCIDO_MAXIMO_CREDITO , [lblMaximoTotalComprobanteCK.Caption, FCantidadDiasCreditosConIVA, lblFecha.Caption]));
    end;}
end;


procedure Tfrm_AnulacionNotasCobro.btnPasarConceptoClick(Sender: TObject);
begin
	PasarConcepto;
	UpdateImporteTotalCredito;
end;

procedure Tfrm_AnulacionNotasCobro.btnPasarTodosClick(Sender: TObject);
begin
	cdsOriginal.First;
	while not cdsOriginal.Eof do begin
		PasarConcepto;
		cdsOriginal.Next;
	end;
	cdsOriginal.First;
	UpdateImporteTotalCredito;
end;

procedure Tfrm_AnulacionNotasCobro.btnEliminarConceptoClick(Sender: TObject);
var
    Posicion, indice: integer;
    CodigoConceptoStr : string;			//REV.15
begin
    if cdsCredito.Eof then Exit;

    CodigoConceptoStr := cdsCredito.FieldByName('CodigoConceptoOriginal').AsString;  	//REV.15
    
    // Rev.13 / 17-Noviembre-2009 / Nelson Droguett Sierra. --------------------------------
    // Limpia la lista de los Transitos seleccionados, si el concepto eliminado es de peajes.
    {----------------REV.15
	if (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeaje) or
		(cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeajePeriodoAnterior) then
    ------------------}
    if	(FListaCodigoConceptoPeaje.IndexOf(CodigoConceptoStr) >= 0) or
    	(FListaCodigoConceptoPeajeAnterior.IndexOf(CodigoConceptoStr) >= 0) then
    begin
    // Rev.14 / 17-Diciembre-2009 /  vpaszkowicz. --------------------------------
    // Borra de la lista de los Transitos seleccionados los numcorrCa asociados al numero de movimiento
    // si el concepto eliminado es de peajes. El mov ya se estaba guardando en el StringList separado del NumCorrCa por el signo =
        //FListaTransitosAlCredito.Clear;
        for Indice := FListaTransitosAlCredito.Count - 1 downto 0 do begin
            Posicion := Pos('=', FListaTransitosAlCredito[indice]);
            If StrToInt(Copy(FListaTransitosAlCredito[indice], Posicion + 1, Length(FListaTransitosAlCredito[indice]) - Posicion)) = cdsCredito.FieldByName('NumeroMovimiento').Value then
                FListaTransitosAlCredito.Delete(Indice);
        end;
    end
    //Rev.18 / 31-Marzo-2011 / Nelson Droguett Sierra------------------------------------------------------------------------------
    else if	(FListaCodConceptoEstacionamiento.IndexOf(CodigoConceptoStr) >= 0) or                   //PAR00133-NDR-20110331
    	(FListaCodConceptoEstacionaAnterior.IndexOf(CodigoConceptoStr) >= 0) then                   //PAR00133-NDR-20110331
    begin                                                                                           //PAR00133-NDR-20110331
        for Indice := FListaEstacionamientosAlCredito.Count - 1 downto 0 do begin                   //PAR00133-NDR-20110331
            Posicion := Pos('=', FListaEstacionamientosAlCredito[indice]);                          //PAR00133-NDR-20110331
            If StrToInt(Copy(   FListaEstacionamientosAlCredito[indice],                            //PAR00133-NDR-20110331
                                Posicion + 1,                                                       //PAR00133-NDR-20110331
                                Length(FListaEstacionamientosAlCredito[indice]) - Posicion          //PAR00133-NDR-20110331
                            )                                                                       //PAR00133-NDR-20110331
                       ) = cdsCredito.FieldByName('NumeroMovimiento').Value then                    //PAR00133-NDR-20110331
                FListaEstacionamientosAlCredito.Delete(Indice);                                     //PAR00133-NDR-20110331
        end;                                                                                        //PAR00133-NDR-20110331
    //end;                                                                                          // SS_1081_CQU_20130826  //PAR00133-NDR-20110331
    //FinRev.18--------------------------------------------------------------------------------------------------------------------
    end                                                                                             // SS_1081_CQU_20130826
    else if FTieneConceptoInfractor and (                                                           // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoPrejudicial.IndexOf(CodigoConceptoStr) >= 0) or             // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat1.IndexOf(CodigoConceptoStr) >= 0) or           // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat2.IndexOf(CodigoConceptoStr) >= 0) or           // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat3.IndexOf(CodigoConceptoStr) >= 0) or           // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat4.IndexOf(CodigoConceptoStr) >= 0)              // SS_1081_CQU_20130826
    ) then begin                                                                                    // SS_1081_CQU_20130826
        for Indice := FListaInfraccionesAlCredito.Count - 1 downto 0 do begin                       // SS_1081_CQU_20130826
            Posicion := Pos('=', FListaInfraccionesAlCredito[indice]);                              // SS_1081_CQU_20130826
            If StrToInt(Copy(   FListaInfraccionesAlCredito[indice],                                // SS_1081_CQU_20130826
                                Posicion + 1,                                                       // SS_1081_CQU_20130826
                                Length(FListaInfraccionesAlCredito[indice]) - Posicion              // SS_1081_CQU_20130826
                            )                                                                       // SS_1081_CQU_20130826
                    ) = cdsCredito.FieldByName('NumeroMovimiento').Value                            // SS_1081_CQU_20130826
            then FListaInfraccionesAlCredito.Delete(Indice);                                        // SS_1081_CQU_20130826
        end;                                                                                        // SS_1081_CQU_20130826
    end;                                                                                            // SS_1081_CQU_20130826

    //--------------------------------------------------------------------------------------
	if cdsCredito.FieldByName('NroOriginal').Value <> null then
		if cdsOriginal.FindKey([cdsCredito.FieldByName('NroOriginal').Value]) then begin
		cdsOriginal.Edit;
		cdsOriginal.FieldByName('Pasado').Value := 'N';
		cdsOriginal.Post;
	end;
	cdsCredito.Delete;
	UpdateImporteTotalCredito;
end;

{
BEGIN SS-1006-NDR-20120817
}
procedure Tfrm_AnulacionNotasCobro.btnAgregarNuevoConceptoClick(Sender: TObject);
resourcestring                                                                                                                                    //SS-1006-NDR-20120817
    MSG_ERROR_CONCESIONARIA='El concepto agregado esta asociado a una concesionaria que no esta incluida en el comprobante que se esta anulando'; //SS-1006-NDR-20120817
var
  f: TfrmCargoNotaCredito;
  CodigoConcesionaria:SmallInt;                                                 //SS-1006-NDR-20120817
  bError:Boolean;                                                               //SS-1006-NDR-20120817
begin
    bError:=False;
    try
        if cdsOriginal.eof then Exit;
        f := TfrmCargoNotaCredito.Create(nil);
        f.Visible := False;
        //if f.Inicializar(false ,false , false , QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO()') , ObtenerIVA , FFechaProceso, FNoSePuedeAcreditarIVA) then								//SS_1144_MCA_20131118
        if f.Inicializar(false ,false , false , QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO()') , ObtenerIVA , FFechaProceso, FNoSePuedeAcreditarIVA, FListaCodigosConceptosAnulacion) then	//SS_1144_MCA_20131118
        begin
            if f.ShowModal = mrOK then
            begin
                CodigoConcesionaria := QueryGetValueInt(DMConnections.BaseCAC,'SELECT CodigoConcesionaria FROM ConceptosMovimiento WITH (NOLOCK) WHERE CodigoConcepto='+f.cbConcepto.Value);//SS-1006-NDR-20120817
                if cdsResumenConcesionarias.Locate('CodigoConcesionaria',CodigoConcesionaria,[loPartialKey]) then                                                                           //SS-1006-NDR-20120817
                begin                                                                                                                                                                       //SS-1006-NDR-20120817
                    cdsCredito.Append;
                    cdsCredito.FieldByName('CodigoConcepto').Value 	:= f.cbConcepto.Value;
                    cdsCredito.FieldByName('Descripcion').Value 	:= f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
                    cdsCredito.FieldByName('DescriImporte').Value 	:= FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value + f.edImporteIVA.Value, True, False, True);
                    cdsCredito.FieldByName('Importe').Value 		:= f.edImporte.Value + f.edImporteIVA.Value;
                    cdsCredito.FieldByName('NroOriginal').Value 	:= 0;
                    cdsCredito.FieldByName('Observaciones').Value 	:= f.edDetalle.Text;
                    cdsCredito.FieldByName('ImporteOriginal').Value := 0; //el nuevo valor no puede superar el original
                    cdsCredito.FieldByName('CodigoConceptoOriginal').Value 	:= 0;
                    cdsCredito.FieldByName('ImporteIVA').Value      := f.edImporteIVA.Value;
                    cdsCredito.FieldByName('ValorNeto').Value       := f.edImporte.Value ;
                    cdsCredito.FieldByName('PorcentajeIVA').Value   := f.edPorcentajeIVA.Value;
                    cdsCredito.FieldByName('DescriImporteIVA').Value      := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporteIVA.Value, True, False, True);
                    cdsCredito.FieldByName('DescriValorNeto').Value       := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value , True, False, True);
                    cdsCredito.FieldByName('DescriPorcentajeIVA').Value   := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edPorcentajeIVA.Value, True, False, True);
                    cdsCredito.FieldByName('CodigoConcesionaria').Value   := CodigoConcesionaria; //SS-1006-NDR-20120817
                    cdsCredito.Post;
                end                                                                                                                                                                       //SS-1006-NDR-20120817
                else                                                                                                                                                                      //SS-1006-NDR-20120817
                begin                                                                                                                                                                     //SS-1006-NDR-20120817
                    bError:=True;                                                                                                                                                         //SS-1006-NDR-20120817
                end;                                                                                                                                                                      //SS-1006-NDR-20120817
            end;
        end;
    finally
        f.Release;
        if bError then  ShowMsgBoxCN(Self.caption,MSG_ERROR_CONCESIONARIA,MB_ICONERROR,Self);                                                                                             //SS-1006-NDR-20120817
    	UpdateImporteTotalCredito;                                                                                                                                                        //SS-1006-NDR-20120817
    end;
end;
{
END SS-1006-NDR-20120817
}


procedure Tfrm_AnulacionNotasCobro.btnModificarConceptoClick(Sender: TObject);
var
  f: TfrmCargoNotaCredito;
  ImporteTransitosSeleccionados: int64;
  ImporteEstacionamientosSeleccionados: int64;                                         //PAR00133-NDR-20110331
  ImporteInfraccionesSeleccionadas : Int64; // SS_1081_CQU_20130826
  SelectedAll: boolean;
  CodigoConceptoStr : string;		//REV.15
begin

    if cdsCredito.Eof then Exit;

    CodigoConceptoStr := cdsCredito.FieldByName('CodigoConceptoOriginal').AsString;  	//REV.15
    {---------REV.15
	if (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeaje) or
		(cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeajePeriodoAnterior) then begin
    -----------}
    if	(FListaCodigoConceptoPeaje.IndexOf(CodigoConceptoStr) >= 0) or
    	(FListaCodigoConceptoPeajeAnterior.IndexOf(CodigoConceptoStr) >= 0) then begin
		cdsCredito.Edit;
		SelectedAll := False;
		//si de un movimientoCuenta se seleccionan todo los transitos, el importe del movimiento no va a coincidir con la
		//sumatoria de los importes de los transitos, por cuestiones de redondeo. Si se detecta que son todos los transitos,
        //entonces no se usa la suma de importes.
        //Hay que calcular la diferencia entre el importe del Movimiento de la NK y los Movimientos de las CK que lo fueron anulando
		ImporteTransitosSeleccionados:= SeleccionarTransitosAAnular(cdsCredito.FieldByName('NumeroMovimiento').Value , SelectedAll);
    	//REV 9
		//if SelectedAll then
		//	cdsCredito.FieldByName('Importe').Value :=  cdsCredito.FieldByName('ImporteOriginal').Value - ObtenerImporteRemanente(cdsCredito.FieldByName('CodigoConceptoOriginal').Value)
		//else
	    //if ImporteTransitosSeleccionados <= cdsCredito.FieldByName('ImporteOriginal').Value then     		//SS_1144_MCA_20131118

        if SelectedAll then	begin																			//SS_1144_MCA_20131118
            if ImporteTransitosSeleccionados <= cdsCredito.FieldByName('SaldoDisponible').Value then       	//SS_1144_MCA_20131118
                cdsCredito.FieldByName('Importe').Value := ImporteTransitosSeleccionados
            else
        	//cdsCredito.FieldByName('Importe').Value := cdsCredito.FieldByName('ImporteOriginal').Value;	//SS_1144_MCA_20131118
            cdsCredito.FieldByName('Importe').Value := cdsCredito.FieldByName('SaldoDisponible').Value		//SS_1144_MCA_20131118
    	//Fin REV 9
        end                                                                                            		//SS_1144_MCA_20131118
        else                                                                                           		//SS_1144_MCA_20131118
             cdsCredito.FieldByName('Importe').Value := ImporteTransitosSeleccionados;                 		//SS_1144_MCA_20131118


        if (FMaximoTotalComprobanteCK - cdsCredito.FieldByName('Importe').Value)  < 0 then                  //SS_1229_MCA_20141117
            if cdsCredito.FieldByName('Importe').Value > FMaximoTotalComprobanteCK then                     //SS_1229_MCA_20141117
                cdsCredito.FieldByName('Importe').Value := FMaximoTotalComprobanteCK;                       //SS_1229_MCA_20141117


		cdsCredito.FieldByName('DescriImporte').Value := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('Importe').Value, True, False, True);
		cdsCredito.Post;
    //Rev.18 / 31-Marzo-2011 / Nelson Droguett Sierra -------------------------------------------------------------------------------
	end else if	(FListaCodConceptoEstacionamiento.IndexOf(CodigoConceptoStr) >= 0) or
    	(FListaCodConceptoEstacionaAnterior.IndexOf(CodigoConceptoStr) >= 0) then begin
		cdsCredito.Edit;
		SelectedAll := False;
		//si de un movimientoCuenta se seleccionan todo los estacionamientos, el importe del movimiento no va a coincidir con la
		//sumatoria de los importes de los estacionamientos, por cuestiones de redondeo. Si se detecta que son todos los estacionamientos,
        //entonces no se usa la suma de importes.
        //Hay que calcular la diferencia entre el importe del Movimiento de la NK y los Movimientos de las CK que lo fueron anulando
		ImporteEstacionamientosSeleccionados:= SeleccionarEstacionamientosAAnular(cdsCredito.FieldByName('NumeroMovimiento').Value , SelectedAll);
    	//REV 9
		//if SelectedAll then
		//	cdsCredito.FieldByName('Importe').Value :=  cdsCredito.FieldByName('ImporteOriginal').Value - ObtenerImporteRemanente(cdsCredito.FieldByName('CodigoConceptoOriginal').Value)
		//else
	    //if ImporteEstacionamientosSeleccionados <= cdsCredito.FieldByName('ImporteOriginal').Value then   	//SS_1144_MCA_20131118
        if SelectedAll then	begin																				//SS_1144_MCA_20131118
        if ImporteEstacionamientosSeleccionados <= cdsCredito.FieldByName('SaldoDisponible').Value then         //SS_1144_MCA_20131118
    	    cdsCredito.FieldByName('Importe').Value := ImporteEstacionamientosSeleccionados
    	else
        	//cdsCredito.FieldByName('Importe').Value := cdsCredito.FieldByName('ImporteOriginal').Value;       //SS_1144_MCA_20131118
            cdsCredito.FieldByName('Importe').Value := cdsCredito.FieldByName('SaldoDisponible').Value;    		//SS_1144_MCA_20131118
		end                                                                                            			//SS_1144_MCA_20131118
        else                                                                                           			//SS_1144_MCA_20131118
             cdsCredito.FieldByName('Importe').Value := ImporteEstacionamientosSeleccionados;                 	//SS_1144_MCA_20131118

    	//Fin REV 9
        if (FMaximoTotalComprobanteCK - cdsCredito.FieldByName('Importe').Value)  < 0 then                  //SS_1229_MCA_20141117
            if cdsCredito.FieldByName('Importe').Value > FMaximoTotalComprobanteCK then                     //SS_1229_MCA_20141117
                cdsCredito.FieldByName('Importe').Value := FMaximoTotalComprobanteCK;                       //SS_1229_MCA_20141117


		cdsCredito.FieldByName('DescriImporte').Value := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('Importe').Value, True, False, True);
		cdsCredito.Post;
	end else
    //FinRev.18--------------------------------------------------------------------------------------------------------------------
    if FTieneConceptoInfractor and (                                                                            // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoPrejudicial.IndexOf(CodigoConceptoStr) >= 0) or                         // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat1.IndexOf(CodigoConceptoStr) >= 0) or                       // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat2.IndexOf(CodigoConceptoStr) >= 0) or                       // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat3.IndexOf(CodigoConceptoStr) >= 0) or                       // SS_1081_CQU_20130826
        (FListaCodigoConceptoInfraBoletoMorosidadCat4.IndexOf(CodigoConceptoStr) >= 0)                          // SS_1081_CQU_20130826
    ) then begin                                                                                                // SS_1081_CQU_20130826
		cdsCredito.Edit;                                                                                        // SS_1081_CQU_20130826
		SelectedAll := False;                                                                                   // SS_1081_CQU_20130826
                                                                                                                // SS_1081_CQU_20130826
        ImporteInfraccionesSeleccionadas := SeleccionarInfraccionesAAnular(                                     // SS_1081_CQU_20130826
                                                    cdsCredito.FieldByName('NumeroMovimiento').Value,           // SS_1081_CQU_20130826
                                                    SelectedAll);                                               // SS_1081_CQU_20130826
                                                                                                                // SS_1081_CQU_20130826
	    //if ImporteInfraccionesSeleccionadas <= cdsCredito.FieldByName('ImporteOriginal').Value thn            //SS_1144_MCA_20131118  // SS_1081_CQU_20130826
        if SelectedAll then	begin																				//SS_1144_MCA_20131118
		if ImporteInfraccionesSeleccionadas <= cdsCredito.FieldByName('SaldoDisponible').Value then
    	    cdsCredito.FieldByName('Importe').Value := ImporteInfraccionesSeleccionadas                         // SS_1081_CQU_20130826
    	else                                                                                                    // SS_1081_CQU_20130826
        	//cdsCredito.FieldByName('Importe').Value := cdsCredito.FieldByName('ImporteOriginal').Value;       //SS_1144_MCA_20131118  // SS_1081_CQU_20130826
            cdsCredito.FieldByName('Importe').Value := cdsCredito.FieldByName('SaldoDisponible').Value;         //SS_1144_MCA_20131118
        end                                                                                            			//SS_1144_MCA_20131118
        else                                                                                           			//SS_1144_MCA_20131118
             cdsCredito.FieldByName('Importe').Value := ImporteInfraccionesSeleccionadas;                 		//SS_1144_MCA_20131118

        if (FMaximoTotalComprobanteCK - cdsCredito.FieldByName('Importe').Value)  < 0 then                  //SS_1229_MCA_20141117
            if cdsCredito.FieldByName('Importe').Value > FMaximoTotalComprobanteCK then                     //SS_1229_MCA_20141117
                cdsCredito.FieldByName('Importe').Value := FMaximoTotalComprobanteCK;                       //SS_1229_MCA_20141117


		cdsCredito.FieldByName('DescriImporte').Value := FormatearImporteConDecimales(                          // SS_1081_CQU_20130826
                                                                    DMConnections.BaseCAC ,                     // SS_1081_CQU_20130826
                                                                    cdsCredito.FieldByName('Importe').Value,    // SS_1081_CQU_20130826
                                                                    True, False, True);                         // SS_1081_CQU_20130826
		cdsCredito.Post;                                                                                        // SS_1081_CQU_20130826
    end else                                                                                                    // SS_1081_CQU_20130826
	begin
		f := TfrmCargoNotaCredito.Create(Application);
		f.Visible := False;
		if f.Inicializar(false ,
                        false,
                        false,
                        cdsCredito.FieldByName('CodigoConceptoOriginal').Value = 0,
                        cdsCredito.FieldByName('CodigoConcepto').Value,
                        cdsCredito.FieldByName('Importe').Value,
                        cdsCredito.FieldByName('ImporteOriginal').Value,
                        cdsCredito.FieldByName('Observaciones').Value,
                        cdsCredito.FieldByName('Descripcion').Value,
                        QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO()') ,
                        ObtenerIVA ,
                        FFechaProceso,
                        FNoSePuedeAcreditarIVA) then begin
			if f.ShowModal = mrOK then begin
				cdsCredito.Edit;
				cdsCredito.FieldByName('DescriImporte').Value 	:= FormatearImporteConDecimales(DMConnections.BaseCAC,(f.edImporte.Value + f.edImporteIVA.Value)*f.FSignoMovimiento, True, False, True);  //SS_1006_1015_NDR_20121126
				cdsCredito.FieldByName('Importe').Value 		:= (f.edImporte.Value + f.edImporteIVA.Value)*f.FSignoMovimiento;                                                                         //SS_1006_1015_NDR_20121126
				cdsCredito.FieldByName('Observaciones').Value 	:= f.edDetalle.Text;
                cdsCredito.FieldByName('ImporteIVA').Value      := f.edImporteIVA.Value*f.FSignoMovimiento;                                                                                               //SS_1006_1015_NDR_20121126
                cdsCredito.FieldByName('ValorNeto').Value       := f.edImporte.Value*f.FSignoMovimiento  ;                                                                                                //SS_1006_1015_NDR_20121126
                cdsCredito.FieldByName('PorcentajeIVA').Value   := f.edPorcentajeIVA.Value;
                cdsCredito.FieldByName('DescriImporteIVA').Value      := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporteIVA.Value*f.FSignoMovimiento, True, False, True);                 //SS_1006_1015_NDR_20121126
                cdsCredito.FieldByName('DescriValorNeto').Value       := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value*f.FSignoMovimiento , True, False, True);                   //SS_1006_1015_NDR_20121126
                cdsCredito.FieldByName('DescriPorcentajeIVA').Value   := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edPorcentajeIVA.Value, True, False, True);
				cdsCredito.Post;
			end;
		end;
        f.Release;
	end;
	UpdateImporteTotalCredito;
end;

function Tfrm_AnulacionNotasCobro.ObtenerImporteRemanente( aCodigoConcepto:integer ): double;
begin
//si se seleccionaron todos los transitos, entonces el precio del movimiento cuenta ser� la diferencia
//del movimiento de la NK menos la suma de los movimientos de las CK que los anularon.

    //se le pasa el Nro de NK y por ejemplo 1 por Peajes. el SP buscar el inverso
    spObtenerImporteTransitosAcreditados.Parameters.Refresh;
    spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;//edNumeroComprobante.Text;
    spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@CodigoConcepto').Value := aCodigoConcepto;
    spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@Importe').Value := null;
    spObtenerImporteTransitosAcreditados.ExecProc ;
    Result := spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@Importe').Value ;
    Result := Result * (-1) / 100 ;
    //el store los devuelve como negativos, pero queda mas claro devolverlos como positivos, as� despues cuando se usa
    //la funcion se debe una resta del importe original del Movimiento y el resultado de esta funcion

end;


//Rev.18 / 31-Marzo-2011 / Nelson Droguett Sierra ---------------------------------------------------------------------------------
function Tfrm_AnulacionNotasCobro.SeleccionarEstacionamientosAAnular( aNumeroMovimiento: integer; var SelectedAll:boolean): Int64;
var
	f2: TConsultaEstacionamientosComprobanteAAnular;
	ImporteTotal: integer;
begin
	ImporteTotal := 0;
	Result := ImporteTotal;
	f2 := TConsultaEstacionamientosComprobanteAAnular.Create(Application);
	f2.Visible := False;
	if f2.Inicializar(aNumeroMovimiento, True, FListaEstacionamientosAlCredito) then begin
		f2.ShowModal;
		Result := f2.ImporteTotal;
		SelectedAll := f2.SelectedAll;
		f2.Release;
	end;
	//la lista contiene ahora los numCorrCA de los tr�nsitos a meter en el credito
end;
//FinRev.18------------------------------------------------------------------------------------------------------------------------



function Tfrm_AnulacionNotasCobro.SeleccionarTransitosAAnular( aNumeroMovimiento : integer; var SelectedAll:boolean): int64;
var
	f2: TFormConsultaTransitosComprobanteAnular;
	ImporteTotal: integer;
begin
	ImporteTotal := 0;
	Result := ImporteTotal;
	f2 := TFormConsultaTransitosComprobanteAnular.Create(Application);
	f2.Visible := False;
	if f2.Inicializar(aNumeroMovimiento, True, FListaTransitosAlCredito) then begin
		f2.ShowModal;
		Result := f2.ImporteTotal;
		SelectedAll := f2.SelectedAll;
		f2.Release;
	end;
	//la lista contiene ahora los numCorrCA de los tr�nsitos a meter en el credito
end;
{
    funcion que obtiene el total de importe existente en la grilla de credito.
}
function Tfrm_AnulacionNotasCobro.ObtenerTotalConceptosPasados(
  NumeroConcepto: Integer): Extended;
var
    Bookmark        : TBookmark;
    ImporteTotal    : Extended;
begin
    Bookmark := cdsCredito.GetBookmark;
	cdsCredito.DisableControls;
	cdsCredito.First;
	ImporteTotal := 0 ;
	while not cdsCredito.eof do begin
        if cdsCredito.FieldByName('CodigoConcepto').AsInteger = NumeroConcepto then begin
            ImporteTotal := ImporteTotal + cdsCredito.FieldByName('Importe').AsFloat;
        end;
		cdsCredito.Next;
	end;
	cdsCredito.GotoBookmark(Bookmark);
	cdsCredito.EnableControls;
	cdsCredito.FreeBookmark(BookMark);
    Result := ImporteTotal;
end;

{
BEGIN SS-1006-NDR-20120817
}
procedure Tfrm_AnulacionNotasCobro.UpdateImporteTotalCredito;
resourcestring
	ERROR_IMPORTE_CREDITO_MAYOR_A_ORIGINAL = 'El importe del cr�dito a generar supera al importe del comprobante original (%0f)';
	ERROR_IMPORTE_CONCEPTO_MAYOR_A_ORIGINAL = 'El importe devuelto para una concesionaria espec�fica es mayor al importe original asociado a esa concesionaria (%s)';
var
  Bookmark: TBookmark;
  //ImporteTotal: double;                                                       //TASK_096_JMA_20160116
  var CodigoConcesionaria:Integer;                                              //SS-1006-NDR-20120817
  var sumaCreditos:Double;                                                      //SS-1006-NDR-20120817
  var bErrorMontos:Boolean;                                                     //SS-1006-NDR-20120817
begin
    bErrorMontos:=False;                                                        //SS-1006-NDR-20120817
	Bookmark := cdsCredito.GetBookmark;
	cdsCredito.DisableControls;
	cdsCredito.First;
	ImporteTotal := 0 ;
	while not cdsCredito.eof do begin
		ImporteTotal := ImporteTotal + cdsCredito.FieldByName('Importe').AsFloat  ;
		cdsCredito.Next;
	end;
	lblImporteTotalNotaCredito.Caption := FormatearImporteConDecimales(DMConnections.BaseCAC, ImporteTotal, True, False, True);
    FImporteTotalNotaCredito           := ImporteTotal;                         // SS_1081_CQU_20130201

    // BEGIN : SS_1006_10015_NDR_20121126 ------------------------------------------------------------------------------------
    cdsResumenConcesionarias.First;
    while (not cdsResumenConcesionarias.Eof) do
    begin
        sumaCreditos:=0;
        cdsCredito.First;
        while ((not cdsCredito.eof)) do //and (not bErrorMontos)) do
        begin
            if cdsCredito.FieldByName('CodigoConcesionaria').AsInteger = cdsResumenConcesionariasCodigoConcesionaria.AsInteger then
            begin
                sumaCreditos := sumaCreditos + cdsCredito.FieldByName('Importe').AsFloat;
                //if sumaCreditos>cdsResumenConcesionariasImporte.AsFloat then
                //begin
                //    bErrorMontos:=True;
                //    Break;
                //end;
            end;
            cdsCredito.Next;
        end;
        if sumaCreditos>cdsResumenConcesionariasImporte.AsFloat then
        begin
            bErrorMontos:=True;
            Break;
        end;
        //if bErrorMontos then Break;
        cdsResumenConcesionarias.Next;
    end;
    // END : SS_1006_10015_NDR_20121126 ------------------------------------------------------------------------------------


	btnGenerarOAnular.Enabled := (ImporteTotal <= FMaximoTotalComprobanteCK) and (ImporteTotal > 0) ;

	if FBotonBloqueadoPorError or not(ValidateControls(     [   dblNotaCredito,
                                                                lblImporteTotalNotaCredito

                                                            ],
                                                            [   not bErrorMontos,             // Si no llego al final, se salio antes
                                                                ImporteTotal <= FMaximoTotalComprobanteCK
                                                            ],
                                                            caption,
                                                            [   Format(ERROR_IMPORTE_CONCEPTO_MAYOR_A_ORIGINAL,[cdsCredito.FieldByName('Descripcion').AsString]),
                                                                Format(ERROR_IMPORTE_CREDITO_MAYOR_A_ORIGINAL,[FMaximoTotalComprobanteCK])
                                                            ]
                                                        )
                                    ) then
		btnGenerarOAnular.Enabled := False;

	cdsCredito.GotoBookmark(Bookmark);
	cdsCredito.EnableControls;
	cdsCredito.FreeBookmark(BookMark);
end;
{
END SS-1006-NDR-20120817
}


procedure Tfrm_AnulacionNotasCobro.PasarConcepto;
resourcestring
	ERROR_GETTING_CHARGE =  'Error grave. No se ha encontrado Concepto que anula al Concepto %s' + CRLF +
							            'Deber� configurarse el concepto con otro que lo anule.' + CRLF +
							            'Ya no podr� seguir trabajando con esta Nota de Cobro' ;
  MSG_ERROR         = 'Ha ocurrido un error ejecutado el SP ObtenerImporteAnuladoConceptoComprobanteAAnular';
//  MSG_WNG_CONCEPTO  = 'El concepto ya fue anulado en su totalidad, no puede ser utilizado en una Nota de Cr�dito para este Comprobante';      //SS_1012_ALA_20111118
  MSG_WNG_CONCEPTO  = 'El concepto que desea anular sobrepasa el monto para este Comprobante.';          //SS_1012_ALA_20111118    //SS_1012_ALA_20111128

var
  ImporteConceptoRestante   : Extended;                                              //SS_1012_ALA_20111118
  CodigoConceptoStr         : string;			//REV.15
  Iva                       : Double;                                               //SS_1012_ALA_20111128
  ConceptosSinPasar       : Integer;     // SS_1080_CQU_20130111
begin
	if cdsOriginal.FieldByName('Pasado').Value = 'N' then begin
		//En base al concepto original, encontrar su contraparte.
        spObtenerConceptoQueAnula.Close;                                        //SS_1012_ALA_20111128
		spObtenerConceptoQueAnula.Parameters.ParamByName('@CodigoConcepto').Value := cdsOriginal.FieldByName('Concepto').Value;
		spObtenerConceptoQueAnula.Open ;
        if spObtenerConceptoQueAnula.Eof then begin
			MsgBoxErr(Format(ERROR_GETTING_CHARGE,[cdsOriginal.FieldByName('Descripcion').AsString] ), caption, caption, MB_ICONERROR);
			spObtenerConceptoQueAnula.Close ;
			FBotonBloqueadoPorError:= true;
			btnGenerarOAnular.Enabled := False;
			exit;
		end;
        //REV 10
        try
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.Refresh;
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@TipoComprobante').Value        := TipoComprobante;
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@NumeroComprobante').Value      := NumeroComprobante;
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@CodigoConcepto').Value         := cdsOriginal.FieldByName('Concepto').Value;                           //SS_1012_ALA_20111118
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@CodigoConceptoAnula').Value    := spObtenerConceptoQueAnula.FieldByName('CodigoConcepto').Value;       //SS_1012_ALA_20111118
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@NumeroMovimiento').Value       := cdsOriginal.FieldByName('NumeroMovimiento').Value;                   // SS_1006_10015_NDR_20121126
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@ImporteConcepto').Value        := null;
            spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@EsDescuentoRecargo').Value     := null;                                                                //SS_1006_1015_NDR_20121126

            spObtenerImporteAnuladoConceptoComprobanteAAnular.ExecProc;
            ImporteConceptoRestante := spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@ImporteConcepto').Value;                                                    //SS_1012_ALA_20111118
        except
            on E: Exception do begin
					MsgBoxErr(MSG_ERROR, e.message, caption, MB_ICONERROR);
			end;
        end;
        //REV 10
        ImporteConceptoRestante := ImporteConceptoRestante - ObtenerTotalConceptosPasados(spObtenerConceptoQueAnula.FieldByName('CodigoConcepto').AsInteger);                                 //SS_1012_ALA_20111118
        { INICIO BLOQUE SS_1012_ALA_20111128
        if (cdsOriginal.FieldByName('Importe').Value > ImporteConceptoRestante) then begin                                                                                                    //SS_1012_ALA_20111118
			  MsgBox(Format(MSG_WNG_CONCEPTO, [FormatFloat(FORMATO_IMPORTE, ImporteConceptoRestante)]), caption, MB_ICONWARNING);                                                             //SS_1012_ALA_20111118
			  spObtenerConceptoQueAnula.Close ;
			  btnGenerarOAnular.Enabled := False;
        end
        else begin
        FIN BLOQUE SS_1012_ALA_20111128
        }
        ConceptosSinPasar := ObtenerTotalConceptosSinPasar(cdsOriginal.FieldByName('Concepto').AsInteger);  // SS_1080_CQU_20130111
        if spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@EsDescuentoRecargo').Value = True then    // SS_1080_CQU_20130523
            ConceptosSinPasar := ConceptosSinPasar * -1;                                                                        // SS_1080_CQU_20130523
        if not (ConceptosSinPasar = ImporteConceptoRestante) then                                           // SS_1080_CQU_20130111
            ImporteConceptoRestante := ConceptosSinPasar - ImporteConceptoRestante;                         // SS_1080_CQU_20130111

        if ((ImporteConceptoRestante > 0) or                                                                                                                                                //SS_1006_1015_NDR_20121126
            (   (ImporteConceptoRestante < 0) and                                                                                                                                           //SS_1006_1015_NDR_20121126
                (spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@EsDescuentoRecargo').Value=True                                                                 //SS_1006_1015_NDR_20121126
                )                                                                                                                                                                           //SS_1006_1015_NDR_20121126
            )                                                                                                                                                                               //SS_1006_1015_NDR_20121126
           )  then                                                                                                                                                                          //SS_1006_1015_NDR_20121126
        begin
            cdsOriginal.Edit;
            cdsOriginal.FieldByName('Pasado').Value := 'S';
            cdsOriginal.Post;

            cdsCredito.Append;
            cdsCredito.FieldByName('CodigoConcepto').Value 		    := spObtenerConceptoQueAnula.FieldByName('CodigoConcepto').AsString ;
            cdsCredito.FieldByName('Descripcion').Value 		    := spObtenerConceptoQueAnula.FieldByName('Descripcion').Value;
            cdsCredito.FieldByName('CodigoConceptoOriginal').Value 	:= cdsOriginal.FieldByName('Concepto').Value;

            cdsCredito.FieldByName('SaldoDisponible').Value			:= cdsOriginal.FieldByName('Importe').Value;	//SS_1144_MCA_20131118

            {-----------REV.15
            if (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeaje) or
                  (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeajePeriodoAnterior) then begin
            }

            CodigoConceptoStr := cdsCredito.FieldByName('CodigoConceptoOriginal').AsString;  	//REV.15
            if	(FListaCodigoConceptoPeaje.IndexOf(CodigoConceptoStr) >= 0) or
                (FListaCodigoConceptoPeajeAnterior.IndexOf(CodigoConceptoStr) >= 0) then begin

                  //los conceptos de peaje los paso con importe cero, porque el importe depende de los transitos que se seleccionen
                  cdsCredito.FieldByName('DescriImporte').Value := '';
                  cdsCredito.FieldByName('Importe').Value 		:= 0;
                //Rev.18 / 31-Marzo-2011 / Nelson Droguett Sierra --------------------------------------------------------------------
            end
            else if	(FListaCodConceptoEstacionamiento.IndexOf(CodigoConceptoStr) >= 0) or                                                                       //PAR00133-NDR-20110331
                (FListaCodConceptoEstacionaAnterior.IndexOf(CodigoConceptoStr) >= 0) then begin                                                                     //PAR00133-NDR-20110331
                  //los conceptos de estacionamiento los paso con importe cero, porque el importe depende de los estacionamientos que se seleccionen                //PAR00133-NDR-20110331
                  cdsCredito.FieldByName('DescriImporte').Value := '';                                                                                      //PAR00133-NDR-20110331
                  cdsCredito.FieldByName('Importe').Value       := 0;                                                                                           //PAR00133-NDR-20110331
            end
            else if FTieneConceptoInfractor and (                                                                       // SS_1081_CQU_20130826
                (FListaCodigoConceptoInfraBoletoPrejudicial.IndexOf(CodigoConceptoStr) >= 0) or                         // SS_1081_CQU_20130826
                (FListaCodigoConceptoInfraBoletoMorosidadCat1.IndexOf(CodigoConceptoStr) >= 0) or                       // SS_1081_CQU_20130826
                (FListaCodigoConceptoInfraBoletoMorosidadCat2.IndexOf(CodigoConceptoStr) >= 0) or                       // SS_1081_CQU_20130826
                (FListaCodigoConceptoInfraBoletoMorosidadCat3.IndexOf(CodigoConceptoStr) >= 0) or                       // SS_1081_CQU_20130826
                (FListaCodigoConceptoInfraBoletoMorosidadCat4.IndexOf(CodigoConceptoStr) >= 0)                          // SS_1081_CQU_20130826
            ) then begin                                                                                                // SS_1081_CQU_20130826
                // los conceptos de Infracciones los paso con importe cero, porque el importe depende                   // SS_1081_CQU_20130826
                // de las Infracciones que se seleccionen                                                               // SS_1081_CQU_20130826
                cdsCredito.FieldByName('DescriImporte').Value := '';                                                    // SS_1081_CQU_20130826
                cdsCredito.FieldByName('Importe').Value       := 0;                                                     // SS_1081_CQU_20130826
            end                                                                                                         // SS_1081_CQU_20130826
            else begin                                                                                                                                          //PAR00133-NDR-20110331
                //FinRev.18------------------------------------------------------------------------------------------------------------
                    //cdsCredito.FieldByName('DescriImporte').Value 		:= cdsOriginal.FieldByName('DescriImporte').Value;
                    //cdsCredito.FieldByName('Importe').Value 			:= cdsOriginal.FieldByName('Importe').Value;

                // REV 10: dejo como importe la diferencia entre el importe del concepto y lo anulado hasta ahora
                //SS_1012_ALA_20111118: Dejo el importe como el valor del concepto que se est� anulando
                //if (cdsOriginal.FieldByName('Importe').Value > ImporteAnuladoConcepto) then begin                                                                                              //SS_1012_ALA_20111118
                    //cdsCredito.FieldByName('Importe').Value         := cdsOriginal.FieldByName('Importe').Value - ImporteAnuladoConcepto; //el nuevo valor no puede superar el original        //SS_1012_ALA_20111118
                //INICIO BOQUE SS_1012_ALA_20111128
                if cdsOriginal.FieldByName('Importe').Value > ImporteConceptoRestante then begin
                    if cdsOriginal.FieldByName('ImporteIVA').Value > 0 then begin
                        Iva                                                 :=  ObtenerIVA;
                        cdsCredito.FieldByName('Importe').Value             := ImporteConceptoRestante;
                        cdsCredito.FieldByName('DescriImporte').Value       := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('Importe').Value, True, False, True);
                        cdsCredito.FieldByName('ValorNeto').Value           := CalcularImporteNetoDeImporte(Iva, ImporteConceptoRestante);
                        cdsCredito.FieldByName('ImporteIVA').Value          := CalcularIVADeImporte(Iva, ImporteConceptoRestante);
                        cdsCredito.FieldByName('PorcentajeIVA').Value       := Iva;
                        cdsCredito.FieldByName('DescriImporteIVA').Value    := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('ImporteIVA').Value, True, False, True);
                        cdsCredito.FieldByName('DescriValorNeto').Value     := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('ValorNeto').Value, True, False, True);
                        cdsCredito.FieldByName('DescriPorcentajeIVA').Value := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('PorcentajeIVA').Value, True, False, True);
                    end
                    else begin
                        cdsCredito.FieldByName('Importe').Value             := ImporteConceptoRestante;
                        cdsCredito.FieldByName('DescriImporte').Value       := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('Importe').Value, True, False, True);
                        cdsCredito.FieldByName('ImporteIVA').Value          := cdsOriginal.FieldByName('ImporteIVA').Value;
                        cdsCredito.FieldByName('ValorNeto').Value           := cdsOriginal.FieldByName('ValorNeto').Value;
                        cdsCredito.FieldByName('PorcentajeIVA').Value       := cdsOriginal.FieldByName('PorcentajeIVA').Value;
                        cdsCredito.FieldByName('DescriImporteIVA').Value    := cdsOriginal.FieldByName('DescriImporteIVA').Value;
                        cdsCredito.FieldByName('DescriValorNeto').Value     := cdsOriginal.FieldByName('DescriValorNeto').Value;
                        cdsCredito.FieldByName('DescriPorcentajeIVA').Value := cdsOriginal.FieldByName('DescriPorcentajeIVA').Value;                                                                 
                    end;
                end
                else begin //TERMINO BLOQUE SS_1012_ALA_20111128
                    cdsCredito.FieldByName('Importe').Value             := cdsOriginal.FieldByName('Importe').Value;
                    cdsCredito.FieldByName('DescriImporte').Value       := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('Importe').Value, True, False, True);
                    cdsCredito.FieldByName('ImporteIVA').Value          := cdsOriginal.FieldByName('ImporteIVA').Value;                                                                          //SS_1012_ALA_20111128
                    cdsCredito.FieldByName('ValorNeto').Value           := cdsOriginal.FieldByName('ValorNeto').Value;                                                                           //SS_1012_ALA_20111128
                    cdsCredito.FieldByName('PorcentajeIVA').Value       := cdsOriginal.FieldByName('PorcentajeIVA').Value;                                                                       //SS_1012_ALA_20111128
                    cdsCredito.FieldByName('DescriImporteIVA').Value    := cdsOriginal.FieldByName('DescriImporteIVA').Value;                                                                    //SS_1012_ALA_20111128
                    cdsCredito.FieldByName('DescriValorNeto').Value     := cdsOriginal.FieldByName('DescriValorNeto').Value;                                                                     //SS_1012_ALA_20111128
                    cdsCredito.FieldByName('DescriPorcentajeIVA').Value := cdsOriginal.FieldByName('DescriPorcentajeIVA').Value;                                                                 //SS_1012_ALA_20111128
                end;                                                                                                                                                                             //SS_1012_ALA_20111128
                //end                                                                                                                                                                            //SS_1012_ALA_20111118
                //else begin                                                                                                                                                                     //SS_1012_ALA_20111118
                //    cdsCredito.FieldByName('Importe').Value         := 0;                                                                                                                      //SS_1012_ALA_20111118
                //    cdsCredito.FieldByName('DescriImporte').Value   := '';                                                                                                                     //SS_1012_ALA_20111118
                //end;                                                                                                                                                                           //SS_1012_ALA_20111118
            end;
            cdsCredito.FieldByName('NroOriginal').Value 		:= cdsOriginal.FieldByName('NumeroLinea').Value;
            cdsCredito.FieldByName('Observaciones').Value 		:= cdsOriginal.FieldByName('Observaciones').Value;
            cdsCredito.FieldByName('NumeroMovimiento').Value 	:= cdsOriginal.FieldByName('NumeroMovimiento').Value;
            // REV 10: dejo como importe original la diferencia entre el importe del concepto y lo anulado hasta ahora
            //SS_1012_ALA_20111118: Dejo el importe como el valor del concepto que se est� anulando
            //if cdsOriginal.FieldByName('Importe').Value > ImporteAnuladoConcepto then
                //cdsCredito.FieldByName('ImporteOriginal').Value := cdsOriginal.FieldByName('Importe').Value - ImporteAnuladoConcepto //el nuevo valor no puede superar el original             //SS_1012_ALA_20111118
                //cdsCredito.FieldByName('ImporteOriginal').Value := cdsOriginal.FieldByName('Importe').Value;                                                                                   //SS_1144_MCA_20131118 //SS_1012_ALA_20111118
            cdsCredito.FieldByName('ImporteOriginal').Value := ImporteConceptoRestante;                                                                                                          //SS_1144_MCA_20131118
            //else                                                                                                                                                                               //SS_1012_ALA_20111118
            //    cdsCredito.FieldByName('ImporteOriginal').Value := 0;                                                                                                                          //SS_1012_ALA_20111118

            cdsCredito.FieldByName('IndiceVehiculo').Value 		:= cdsOriginal.FieldByName('IndiceVehiculo').Value;
            {INICIO BLOQUE SS_1012_ALA_20111128
            cdsCredito.FieldByName('ImporteIVA').Value          := cdsOriginal.FieldByName('ImporteIVA').Value;
            cdsCredito.FieldByName('ValorNeto').Value           := cdsOriginal.FieldByName('ValorNeto').Value;
            cdsCredito.FieldByName('PorcentajeIVA').Value       := cdsOriginal.FieldByName('PorcentajeIVA').Value;
            cdsCredito.FieldByName('DescriImporteIVA').Value    := cdsOriginal.FieldByName('DescriImporteIVA').Value;
            cdsCredito.FieldByName('DescriValorNeto').Value     := cdsOriginal.FieldByName('DescriValorNeto').Value;
            cdsCredito.FieldByName('DescriPorcentajeIVA').Value := cdsOriginal.FieldByName('DescriPorcentajeIVA').Value;
            TERMINO BLOQUE SS_1012_ALA_20111128
            }
            {if FNoSePuedeAcreditarIVA then begin
                      cdsCredito.FieldByName('DescriImporte').Value 		    := cdsOriginal.FieldByName('DescriValorNeto').Value;
                      cdsCredito.FieldByName('Importe').Value 			    := cdsOriginal.FieldByName('ValorNeto').Value;
                cdsCredito.FieldByName('ImporteOriginal').Value 	    := cdsOriginal.FieldByName('ValorNeto').Value;
                cdsCredito.FieldByName('ImporteIVA').Value              := 0;
                cdsCredito.FieldByName('ValorNeto').Value               := cdsOriginal.FieldByName('ValorNeto').Value;
                cdsCredito.FieldByName('PorcentajeIVA').Value           := 0;
                cdsCredito.FieldByName('DescriImporteIVA').Value        := '0';
                cdsCredito.FieldByName('DescriValorNeto').Value         := cdsOriginal.FieldByName('DescriValorNeto').Value;
                cdsCredito.FieldByName('DescriPorcentajeIVA').Value     := '0';
            end;}
            cdsCredito.FieldByName('CodigoConcesionaria').Value 		:= cdsOriginal.FieldByName('CodigoConcesionaria').Value; //SS-1006-NDR-20120817


            cdsCredito.Post;
            spObtenerConceptoQueAnula.Close;
            UpdateImporteTotalCredito;
        end                                                                                                                                                                                 //SS_1012_ALA_20111128
        else begin                                                                                                                                                                          //SS_1012_ALA_20111128
            MsgBox(MSG_WNG_CONCEPTO, caption, MB_ICONWARNING);                                                                                                                              //SS_1012_ALA_20111128
        end;                                                                                                                                                                                

	end;
end;

function Tfrm_AnulacionNotasCobro.ChequearConcurrencia: boolean;
resourcestring
	MSG_NUMERO_INVALIDO = 'N�mero de comprobante inv�lido';
    ERROR_COMPROBANTE_SIENDO_ANULADO = 'El comprobante est� siendo anulado por el Usuario %s el d�a %s';
var
    OtroUsuario: string;
    FechaOtroUsuario: TDateTime;
begin
    //Chequeamos si otro usuario est� modificando este comprobante en la tabla de MovimientosTemporales

    spChequearUsuarioConcurrenteAcreditando.Parameters.Refresh;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@TipoComprobante').Value    := cbTipoComprobante.Value ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@NumeroComprobante').Value  := edNumeroComprobante.ValueInt ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@NumeroDeSesion').Value     := FIdSesion ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value        := null ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value   := null ;
    spChequearUsuarioConcurrenteAcreditando.ExecProc;


    if spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value <> null then
        OtroUsuario := spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value
    else OtroUsuario := '';

    if spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value <> null then
        FechaOtroUsuario := spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value
    else FechaOtroUsuario := 0;

    Result := False;
    if OtroUsuario <> '' then begin
        MsgBoxErr(Format(ERROR_COMPROBANTE_SIENDO_ANULADO,[OtroUsuario,DateTimeToStr(FechaOtroUsuario)] ), caption, caption, MB_ICONERROR);
        Result := True;
        Exit;
    end;

end;

procedure Tfrm_AnulacionNotasCobro.LimpiarDatosTemporales;
begin
    spLimpiarDatosTemporales.Parameters.ParamByName('@IdSesion').Value := FIdSesion ;
    spLimpiarDatosTemporales.ExecProc;
end;

procedure Tfrm_AnulacionNotasCobro.cbTipoComprobanteChange(Sender: TObject);
var
  index: integer;
begin


    if (cbTipoComprobante.Value = TC_BOLETA) then begin
        edNumeroComprobanteFiscal.Enabled       := true;
        cbImpresorasFiscales.Enabled            := true;
        lblNumeroComprobanteFiscal.Enabled      := true;
        lblImpresoraFiscal.Enabled              := true;
//INICIO: TASK_059_JMA_20161019
        //chkPasarTransitoEstadoEspecial.Visible  := false;
//TERMINO: TASK_059_JMA_20161019

        edNumeroComprobante.Enabled             := false;
        lblNumeroComprobante.Enabled            := false;
        //lblNumeroNotaCreditoFiscal.Enabled  := cbTipoComprobante.Value = TC_BOLETA;
        //edNumeroNotaCreditoFiscal.Enabled   := cbTipoComprobante.Value = TC_BOLETA;
    end;

    //edNumeroComprobante.Enabled     := cbTipoComprobante.Value = TC_NOTA_COBRO;
    //lblNumeroComprobante.Enabled    := cbTipoComprobante.Value = TC_NOTA_COBRO;
    if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
        edNumeroComprobante.Enabled             := true;
        lblNumeroComprobante.Enabled            := true;
//INICIO: TASK_059_JMA_20161019
        //chkPasarTransitoEstadoEspecial.Visible  := true;
//TERMINO: TASK_059_JMA_20161019

        edNumeroComprobanteFiscal.Enabled       := false;
        lblNumeroComprobanteFiscal.Enabled      := false;
        cbImpresorasFiscales.Enabled            := false;
        lblImpresoraFiscal.Enabled              := false;
    end;

    if (cbTipoComprobante.Value = TC_FACTURA_EXENTA) OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA)
        OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA)
    then begin
        edNumeroComprobanteFiscal.Enabled       := true;
        lblNumeroComprobanteFiscal.Enabled      := true;
//INICIO: TASK_059_JMA_20161019
        //chkPasarTransitoEstadoEspecial.Visible  := true;
//TERMINO: TASK_059_JMA_20161019

        edNumeroComprobante.Enabled             := false;
        lblNumeroComprobante.Enabled            := false;
        cbImpresorasFiscales.Enabled            := false;
        lblImpresoraFiscal.Enabled              := false;
    end;

    // Mostrar u ocultar la columna de Transferir y AfectoIVA
    for index := 0 to dblDocumentoOriginal.Columns.Count - 1 do begin

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'Seleccionado' then begin
            dblDocumentoOriginal.Columns.Items[index].Width := 0;
            dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
        end;
//          No existe mas la columna seleccionado
//            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
//                dblDocumentoOriginal.Columns.Items[index].Width := 80;
//                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Transferir';
//            end else begin
//                dblDocumentoOriginal.Columns.Items[index].Width := 0;
//                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
//            end;

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'AfectoIVA' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblDocumentoOriginal.Columns.Items[index].Width := 0;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
            end else begin
                dblDocumentoOriginal.Columns.Items[index].Width := 80;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Afecto IVA';
            end;

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'DescriValorNeto' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblDocumentoOriginal.Columns.Items[index].Width := 0;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
            end else begin
                dblDocumentoOriginal.Columns.Items[index].Width := 90;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Valor Neto';
            end;

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'DescriImporteIVA' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblDocumentoOriginal.Columns.Items[index].Width := 0;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
            end else begin
                dblDocumentoOriginal.Columns.Items[index].Width := 90;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Importe IVA';
            end;

    end;

    for index := 0 to dblNotaCredito.Columns.Count - 1 do begin
        if dblNotaCredito.Columns.Items[index].FieldName = 'DescriValorNeto' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblNotaCredito.Columns.Items[index].Width := 0;
                dblNotaCredito.Columns.Items[index].Header.Caption := '';
            end else begin
                dblNotaCredito.Columns.Items[index].Width := 90;
                dblNotaCredito.Columns.Items[index].Header.Caption := 'Valor Neto';
            end;

        if dblNotaCredito.Columns.Items[index].FieldName = 'DescriImporteIVA' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblNotaCredito.Columns.Items[index].Width := 0 ;
                dblNotaCredito.Columns.Items[index].Header.Caption := '';
            end else begin
                dblNotaCredito.Columns.Items[index].Width := 90;
                dblNotaCredito.Columns.Items[index].Header.Caption := 'Importe IVA';
             end;
    end;

    //chkPasarTransitoEstadoEspecial.Visible := cbTipoComprobante.Value = TC_NOTA_COBRO;

    if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
        edNumeroComprobanteFiscal.Value := 0;
        cbImpresorasFiscales.ItemIndex := -1;
    end;
    if (cbTipoComprobante.Value = TC_BOLETA) then
        edNumeroComprobante.Value := 0;

    if (cbTipoComprobante.Value = TC_FACTURA_EXENTA)
        OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA)
        OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA)
        OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA) then begin
        edNumeroComprobante.Value := 0;
        cbImpresorasFiscales.ItemIndex := -1;
    end;


	LimpiarDatosCliente;
	LimpiarDatosComprobante;
	LimpiarDetalles;

end;

function Tfrm_AnulacionNotasCobro.ObtenerIVA: Double;
begin
    //se usa now porque la emision de los creditos en la base es getdate
    Result := QueryGetValueInt(DMConnections.BaseCAC,
                                'SELECT dbo.ObtenerIVA(''' + FormatDateTime('yyyymmdd',
                                NowBase(spComprobantes.Connection)) + ''')');
    Result := Result / 100;       // esto devuelve un 0.19 porque en la base dice 1900

end;

procedure Tfrm_AnulacionNotasCobro.dblDocumentoOriginalDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
//var
//  OldColor: TColor;
//  bmp: TBitMap;
begin
	//estos codigos de cargos se van a crear de todas maneras como movimientos en el convenio sin numero de lote
	//les cambio el color para que el usuario lo sepa
    //Ahora le agregamos un check para marcar los que se pasan de nuevo al convenio sin numero de lote, o no


//    No existe mas la seleccion de cargos a devolver al convenio
//	OldColor := Sender.Canvas.Font.color ;
//	if LineaEsSeleccionable and (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
//		Sender.Canvas.Font.color := clred;
//	    with Sender do begin
//    		if (Column.FieldName = 'Seleccionado') then begin
//				Text := '';
//				Canvas.FillRect(Rect);
//				bmp := TBitMap.Create;
//				try
//					DefaultDraw := False;
//					if (cdsOriginal.FieldByName('Seleccionado').AsBoolean ) then begin
//						Img_Tilde.GetBitmap(0, Bmp);
//					end else begin
//	                    Img_Tilde.GetBitmap(1, Bmp);
//	                end;
//                    Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
//	            finally
//                    bmp.Free;
//                end;
//            end;
//        end;
//    end
//	else
//		Sender.Canvas.Font.color := OldColor;

end;

function Tfrm_AnulacionNotasCobro.ValidarNumeroFiscalNotaCredito:boolean;
resourcestring
	MSG_ERROR_IMPRESORA_MANUAL_NO_ENCONTRADA = 'No se encontro la impresora manual en la n�mina de impresoras registradas';
    MSG_ERROR_NUMERO_FISCAL_CREDITO_REPETIDO = 'Ya existe una Nota de Cr�dito con ese N�mero Fiscal';
    MSG_ERROR_NUMERO_FISCAL_CREDITO_ERRONEO = 'Valor err�neo para N�mero Fiscal de Nota de Cr�dito';
var
    CodigoImpresoraFiscalManual: integer;
begin
    CodigoImpresoraFiscalManual := QueryGetValueInt(DMConnections.BaseCAC,'select dbo.ObtenerCodigoImpresoraManual()');
    Result := False;
    if CodigoImpresoraFiscalManual = 0 then begin
        MsgBoxErr(MSG_ERROR_IMPRESORA_MANUAL_NO_ENCONTRADA, Caption, Caption, 0);
        Exit;
    end;

    //if edNumeroNotaCreditoFiscal.ValueInt <= 0 then begin
    //    MsgBoxBalloon(MSG_ERROR_NUMERO_FISCAL_CREDITO_ERRONEO, Caption, MB_ICONSTOP, edNumeroNotaCreditoFiscal);
    //    Result := False;
    //    Exit;
    //end;

    //CodigoImpresoraFiscal puede ser la manual o una que exista o ninguna, en ese caso el parametro es cero
    //Si Result queda True es que no existe el comprobante
    //Result := QueryGetValue( DMConnections.BaseCAC, Format('SELECT dbo.ExisteComprobanteFiscal (''%s'', %d , %d )',
    //            	[TC_NOTA_CREDITO, edNumeroNotaCreditoFiscal.ValueInt, CodigoImpresoraFiscalManual] )) = 'False';

    //if not Result then MsgBoxBalloon(MSG_ERROR_NUMERO_FISCAL_CREDITO_REPETIDO, Caption, MB_ICONSTOP, edNumeroNotaCreditoFiscal);


end;

procedure Tfrm_AnulacionNotasCobro.dblDocumentoOriginalDblClick(
  Sender: TObject);
begin
//    No existe mas seleccion de cargos para devolver al convenio
//	if cdsOriginal.IsEmpty then Exit;
//	dblDocumentoOriginal.Invalidate;
//	if not LineaEsSeleccionable then Exit;
//	with cdsOriginal do begin
//    	  Edit;
//        FieldByName('Seleccionado').AsBoolean := not FieldByName( 'Seleccionado' ).AsBoolean;
//        Post;
//	end;
//	dblDocumentoOriginal.Invalidate;
end;

{*******************************************************************************
    Procedure Name  : ObtenerTotalConceptosSinPasar
    Author          : CQuezadaI
    Date            : 11-01-2013
    Firma           : SS_1080_CQU_20130111
    Description     : Obtiene el total de Conceptos sin pasar a anulaci�n.
*******************************************************************************}
function Tfrm_AnulacionNotasCobro.ObtenerTotalConceptosSinPasar(NumeroConcepto: Integer): Integer;
var
    Bookmark        : TBookmark;
    ImporteTotal    : Integer;
begin
    Bookmark := cdsOriginal.GetBookmark;
	cdsOriginal.DisableControls;
	cdsOriginal.First;
	ImporteTotal := 0;
    cdsOriginal.Filter := 'Concepto = ' + IntToStr(NumeroConcepto) + ' AND Pasado = ''N''';
    cdsOriginal.Filtered := True;
	while not cdsOriginal.eof do begin
        ImporteTotal := ImporteTotal + cdsOriginal.FieldByName('Importe').AsInteger;
		cdsOriginal.Next;
	end;
    cdsOriginal.Filtered := False;
    cdsOriginal.Filter := '';
	cdsOriginal.GotoBookmark(Bookmark);
	cdsOriginal.EnableControls;
	cdsOriginal.FreeBookmark(BookMark);
    Result := ImporteTotal;
end;

{-----------------------------------------------------------------------------
  					LlenarCodigosConceptosInfractores
  Author		: CQuezadaI
  Date			: 27/08/2013
  Firma         : SS_1081_CQU_20130826
  Description	: Llena las listas con todos los c�digos de conceptos
                  infractores para todas las concesionarias.
                  Se basa en LlenarCodigosConceptosDePeajes
-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobro.LlenarCodigosConceptosInfractores;
resourcestring
    MSG_ERROR_OBTENIENDO_CONCEPTOS_INFRACTORES  =   'Ocurri� un error cargando los conceptos infractores';
begin
    // Limpio los StringList
    FListaCodigoConceptoInfraBoletoPrejudicial.Clear;
    FListaCodigoConceptoInfraBoletoMorosidadCat1.Clear;
    FListaCodigoConceptoInfraBoletoMorosidadCat2.Clear;
    FListaCodigoConceptoInfraBoletoMorosidadCat3.Clear;
    FListaCodigoConceptoInfraBoletoMorosidadCat4.Clear;

    spObtenerTodosLosCodigoConceptoInfractores.Close;

    try
        with spObtenerTodosLosCodigoConceptoInfractores do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConcesionaria').Value	:= Null;
            Open;
            // Cargo los StringList
            while not Eof do begin
                FListaCodigoConceptoInfraBoletoPrejudicial.Add(FieldByName('CodigoConceptoInfraBoletoPrejudicial').AsString);
                FListaCodigosConceptosMovimiento.Add(FieldByName('CodigoConceptoInfraBoletoPrejudicial').AsString); 				//SS_1144_MCA_20131118
                FListaCodigoConceptoInfraBoletoMorosidadCat1.Add(FieldByName('CodigoConceptoInfraBoletoMorosidadCat1').AsString);
                FListaCodigoConceptoInfraBoletoMorosidadCat2.Add(FieldByName('CodigoConceptoInfraBoletoMorosidadCat2').AsString);
                FListaCodigoConceptoInfraBoletoMorosidadCat3.Add(FieldByName('CodigoConceptoInfraBoletoMorosidadCat3').AsString);
                FListaCodigoConceptoInfraBoletoMorosidadCat4.Add(FieldByName('CodigoConceptoInfraBoletoMorosidadCat4').AsString);
                Next;
            end;
        end;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_OBTENIENDO_CONCEPTOS_INFRACTORES, e.Message, caption, MB_ICONERROR);
            Exit;
        end;
    end;
    spObtenerTodosLosCodigoConceptoInfractores.Close;

end;

{-----------------------------------------------------------------------------
  					SeleccionarInfraccionesAAnular
  Author		: CQuezadaI
  Date			: 27/08/2013
  Firma         : SS_1081_CQU_20130826
  Description	: Abre una ventana en la cual se podr�n seleccionar
                  las infracciones a anular.
                  Se basa en SeleccionarTransitosAAnular
-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobro.SeleccionarInfraccionesAAnular(aNumeroMovimiento : integer; var SelectedAll : boolean): int64;
var
	fInfraccionesAnular: TFormConsultaInfraccionesComprobanteAnular;
	ImporteTotal: integer;
begin
	ImporteTotal := 0;
	Result := ImporteTotal;
    
    Application.CreateForm(TFormConsultaInfraccionesComprobanteAnular, fInfraccionesAnular);

	if fInfraccionesAnular.Inicializar(aNumeroMovimiento, False, FListaInfraccionesAlCredito) then begin
		fInfraccionesAnular.ShowModal;
        Result := fInfraccionesAnular.ImporteTotal;
        SelectedAll := fInfraccionesAnular.SelectedAll;
        fInfraccionesAnular.Release;
	end;
end;

{INICIO: TASK_058_JMA_20161014}
procedure Tfrm_AnulacionNotasCobro.LlenarComboMotivoAnulacion();
resourcestring
    MSG_ERROR_OBTENIENDO_MOTIVOS_NOTA_CREDITO = 'Error obteniendo motivos nota credito';
var
    SpMotivos : TADOStoredProc;
    clave: string;
begin

    try
        try
            SpMotivos := TADOStoredProc.Create(nil);
            SpMotivos.Connection := DMConnections.BaseCAC;
            SpMotivos.ProcedureName := 'NC_SELECT_MotivosNotaCredito';
            SpMotivos.Parameters.Refresh;
            SpMotivos.Open;
            SpMotivos.First;

            while not SpMotivos.Eof do
            begin
                {INICIO: TASK_096_JMA_20160116
                cbbMotivoNotaCredito.Items.Add(SpMotivos.Fields.FieldByName('Descripcion').Value,SpMotivos.Fields.FieldByName('Id').Value);
                }
                clave := IntToStr(SpMotivos.Fields.FieldByName('Id_Motivo').Value) + ';' + IIf(SpMotivos.Fields.FieldByName('AnulacionCompleta').Value,'1','0');
                cbbMotivoNotaCredito.Items.Add(SpMotivos.Fields.FieldByName('Descripcion').Value,clave);
                {TERMINO: TASK_096_JMA_20160116}
                SpMotivos.Next;
            end;

            cbbMotivoNotaCredito.ItemIndex := -1;

        except
            on ex: Exception do
            begin
               MsgBoxErr(MSG_ERROR_OBTENIENDO_MOTIVOS_NOTA_CREDITO, ex.Message, caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        if Assigned(SpMotivos) then
            SpMotivos.Close;
    end;
end;
{TERMINO: TASK_058_JMA_20161014}


end.