object ReservaComprobanteDevDineroForm: TReservaComprobanteDevDineroForm
  Left = 0
  Top = 0
  Caption = 'ReservaComprobanteDevDineroForm'
  ClientHeight = 547
  ClientWidth = 1083
  Color = clBtnFace
  Constraints.MinHeight = 450
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 1083
    Height = 506
    ActivePage = tbsListado
    Align = alClient
    TabOrder = 0
    object tbsListado: TTabSheet
      Caption = 'Listado/B'#250'squeda de Reservas'
      DesignSize = (
        1075
        478)
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1075
        Height = 129
        Align = alTop
        TabOrder = 0
        DesignSize = (
          1075
          129)
        object lblRutClienteFiltro: TLabel
          Left = 63
          Top = 49
          Width = 67
          Height = 16
          Caption = 'Rut Cliente:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblNumeroReservaDVFiltro: TLabel
          Left = 13
          Top = 74
          Width = 120
          Height = 16
          Caption = 'N'#250'mero Reserva DV:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblNumeroComprobanteFiltro: TLabel
          Left = 73
          Top = 99
          Width = 57
          Height = 16
          Caption = 'Convenio:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblNumeroReciboFiltro: TLabel
          Left = 368
          Top = 50
          Width = 110
          Height = 16
          Caption = 'N'#250'mero de Recibo:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 44
          Top = 14
          Width = 232
          Height = 18
          Caption = 'Listado/B'#250'squeda de Reservas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edRutClienteFiltro: TEdit
          Left = 136
          Top = 48
          Width = 198
          Height = 21
          MaxLength = 11
          TabOrder = 0
        end
        object edConvenioFiltro: TEdit
          Left = 136
          Top = 98
          Width = 198
          Height = 21
          MaxLength = 30
          TabOrder = 2
        end
        object btnFiltrar: TButton
          Left = 821
          Top = 46
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Buscar'
          TabOrder = 5
          OnClick = btnFiltrarClick
        end
        object edNumeroReciboFiltro: TNumericEdit
          Left = 484
          Top = 48
          Width = 159
          Height = 21
          TabOrder = 3
        end
        object edNumeroReservaDVFiltro: TNumericEdit
          Left = 136
          Top = 73
          Width = 198
          Height = 21
          TabOrder = 1
        end
        object chkMostrarComprobantesNOVigentes: TCheckBox
          Left = 484
          Top = 106
          Width = 194
          Height = 17
          Caption = 'Mostrar Reservas NO Vigentes'
          TabOrder = 4
        end
        object btnImprimir: TButton
          Left = 821
          Top = 98
          Width = 107
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Imprimir Reserva'
          TabOrder = 6
          OnClick = btnImprimirClick
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 129
        Width = 1075
        Height = 349
        Align = alClient
        Caption = 'Listado Comprobantes'
        TabOrder = 1
        object dblComprobantes: TDBListEx
          Left = 2
          Top = 15
          Width = 1071
          Height = 332
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 70
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'FechaSolicitud'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 95
              Header.Caption = 'N'#176' Reserva DV'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'IdReservaComprobanteDV'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Rut Cliente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'NumeroDocumento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'N'#250'mero Recibo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'NumeroRecibo'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Convenio'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'NumeroConvenio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Monto'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'Importe'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 110
              Header.Caption = 'Usuario Creaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'UsuarioCreador'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Usuario Impresi'#243'n Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'UsuarioImpresionDV'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha Impresion Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'FechaHoraImpresionDV'
            end
            item
              Alignment = taCenter
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Eliminada'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = OrdenarColumna
              FieldName = 'Eliminada'
            end>
          DataSource = dsObtenerReservaComprobantesDV
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDrawText = dblComprobantesDrawText
        end
      end
      object btnEliminar: TButton
        Left = 944
        Top = 98
        Width = 113
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Eliminar Reserva'
        TabOrder = 2
        OnClick = btnEliminarClick
      end
    end
    object tbsReserva: TTabSheet
      Caption = 'Generar una nueva Reserva'
      ImageIndex = 1
      object lbl1: TLabel
        Left = 110
        Top = 54
        Width = 67
        Height = 16
        Caption = 'Rut Cliente:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl5: TLabel
        Left = 500
        Top = 60
        Width = 37
        Height = 16
        Caption = 'Saldo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblSaldoConvenio: TLabel
        Left = 500
        Top = 82
        Width = 97
        Height = 16
        Caption = 'lblSaldoConvenio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl2: TLabel
        Left = 120
        Top = 82
        Width = 57
        Height = 16
        Caption = 'Convenio:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl3: TLabel
        Left = 138
        Top = 106
        Width = 39
        Height = 16
        Caption = 'Fecha:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblRecibo: TLabel
        Left = 67
        Top = 172
        Width = 110
        Height = 16
        Alignment = taRightJustify
        Caption = 'N'#250'mero de Recibo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl4: TLabel
        Left = 112
        Top = 204
        Width = 65
        Height = 16
        Caption = 'Importe  $:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 44
        Top = 14
        Width = 119
        Height = 18
        Caption = 'Nueva Reserva:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEnListaAmarilla: TLabel
        Left = 378
        Top = 107
        Width = 200
        Height = 14
        Alignment = taCenter
        AutoSize = False
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = False
        Visible = False
      end
      object peRutCliente: TPickEdit
        Left = 192
        Top = 51
        Width = 161
        Height = 24
        Enabled = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnKeyPress = ValidaBusca
        EditorStyle = bteTextEdit
        OnButtonClick = peRutClienteButtonClick
      end
      object vcbConvenios: TVariantComboBox
        Tag = 10
        Left = 192
        Top = 79
        Width = 281
        Height = 22
        Style = vcsOwnerDrawFixed
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 1
        OnChange = vcbConveniosChange
        OnDrawItem = vcbConveniosDrawItem
        Items = <>
      end
      object deFechaEmision: TDateEdit
        Left = 192
        Top = 105
        Width = 119
        Height = 21
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
      object edNumeroRecibo: TNumericEdit
        Left = 192
        Top = 170
        Width = 121
        Height = 21
        TabOrder = 5
      end
      object edImporte: TNumericEdit
        Left = 192
        Top = 205
        Width = 121
        Height = 21
        TabOrder = 6
      end
      object btnLimpiar: TButton
        Left = 192
        Top = 254
        Width = 102
        Height = 25
        Caption = 'Limpiar'
        TabOrder = 7
        OnClick = btnLimpiarClick
      end
      object btnGrabar: TButton
        Left = 347
        Top = 254
        Width = 102
        Height = 25
        Caption = 'Grabar'
        TabOrder = 8
        OnClick = btnGrabarClick
      end
      object rbRecibo: TRadioButton
        Left = 184
        Top = 144
        Width = 65
        Height = 17
        Caption = 'Recibo'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        TabStop = True
        OnClick = rbReciboClick
      end
      object rbSaldoAcreedor: TRadioButton
        Left = 264
        Top = 144
        Width = 113
        Height = 17
        Caption = 'Saldo Acreedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = rbSaldoAcreedorClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1083
    Height = 41
    Align = alTop
    TabOrder = 1
    DesignSize = (
      1083
      41)
    object btnSalir: TButton
      Left = 978
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object spObtenerConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@SoloActivos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SoloContanera'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 737
    Top = 325
  end
  object spObtenerReservaComprobantesDV: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerReservaComprobantesDV'
    Parameters = <>
    Left = 768
    Top = 264
  end
  object dsObtenerReservaComprobantesDV: TDataSource
    DataSet = spObtenerReservaComprobantesDV
    Left = 736
    Top = 264
  end
  object spInsertarReservaComprobantesDV: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarReservaComprobantesDV'
    Parameters = <>
    Left = 776
    Top = 328
  end
end
