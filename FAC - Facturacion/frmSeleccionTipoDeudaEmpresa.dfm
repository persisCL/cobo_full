object SeleccionTipoDeudaEmpresaForm: TSeleccionTipoDeudaEmpresaForm
  Left = 0
  Top = 0
  Caption = 'Selecci'#243'n Tipo Deuda Empresa Recaudadora'
  ClientHeight = 257
  ClientWidth = 462
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 14
    Top = 13
    Width = 432
    Height = 188
  end
  object lblMensaje: TLabel
    Left = 54
    Top = 40
    Width = 331
    Height = 26
    Caption = 
      'seleccione la empresa de cobranza y tipo de deuda a categorizar ' +
      ':'
    WordWrap = True
  end
  object lblEmpresaRecaudadora: TLabel
    Left = 86
    Top = 88
    Width = 112
    Height = 13
    Caption = 'Empresa Recaudadora:'
  end
  object lbltipodeuda: TLabel
    Left = 125
    Top = 136
    Width = 72
    Height = 13
    Caption = 'Tipo de deuda:'
  end
  object btnAceptar: TButton
    Left = 125
    Top = 220
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    Default = True
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 230
    Top = 220
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancelar'
    TabOrder = 1
    OnClick = btnCancelarClick
  end
  object cbEmpresaRecaudadora: TVariantComboBox
    Left = 208
    Top = 85
    Width = 217
    Height = 21
    Style = vcsDropDownList
    DroppedWidth = 0
    ItemHeight = 13
    TabOrder = 2
    Items = <>
  end
  object cbTipoDeuda: TVariantComboBox
    Left = 208
    Top = 133
    Width = 217
    Height = 21
    Style = vcsDropDownList
    DroppedWidth = 0
    ItemHeight = 13
    TabOrder = 3
    Items = <>
  end
end
