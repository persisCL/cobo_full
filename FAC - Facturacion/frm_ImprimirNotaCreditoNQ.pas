{-------------------------------------------------------------------------------
 File Name      : frm_ImprimirNotaCreditoNQ.pas
 Author         : Nelson Droguett Sierra
 Date Created   : 29-Septiembre-2009
 Language       : ES-AR
 Description    : Permite imprimir la nota nota de credito a una nota de cobro directa

 Revision       :   1
 Author         : Nelson Droguett Sierra
 Date           : 15-Diciembre-2009
 Description    : Validacion de Fechas, y habilitacion del boton de impresion
                luego de que se existan datos validos.

 Revision       :   2
 Author         : Nelson Droguett Sierra
 Date           : 07-Octubre-2010
 Description    : Se imprime en el formato de la CK electronica
 Firma			: SS-784-NDR-20101007
-------------------------------------------------------------------------------}
unit frm_ImprimirNotaCreditoNQ;

interface

uses
  //Imprimir nota de cobro directa
  DMConnection,
  PeaTypes,
  PeaProcs,
  Util,
  UtilProc,
  //frmReporteNotaDebitoElectronica,		//SS-784-NDR-20101007
  frmReporteNotaCreditoElectronica,			//SS-784-NDR-20101007
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, DB, ADODB, VariantComboBox, Validate,
  DateEdit, DateUtils, StrUtils;

type
  Tform_ImprimirNotaCreditoNQ = class(TForm)
    dbl_NotasCobro: TDBListEx;
    Label1: TLabel;
    btn_ImprimirNotaCredito: TButton;
    btn_Cerrar: TButton;
    sp_ObtenerNotasCreditoNQ: TADOStoredProc;
    ds_ObtenerNotasCreditoNQ: TDataSource;
    btnBuscar: TButton;
    Label2: TLabel;
    cbProveedorDayPass: TVariantComboBox;
    Label3: TLabel;
    deFDesde: TDateEdit;
    Label4: TLabel;
    deFHasta: TDateEdit;
    rbNotasCobroPDU: TRadioButton;
    rbNotasCobroBHTU: TRadioButton;
    procedure btnBuscarClick(Sender: TObject);
    procedure sp_ObtenerNotasCreditoNQAfterOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_CerrarClick(Sender: TObject);
    procedure btn_ImprimirNotaCreditoClick(Sender: TObject);
    procedure OrdenarPorColumna(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar : Boolean;
    procedure CargarProveedoresDayPass;
  end;

var
  form_ImprimirNotaCreditoNQ: Tform_ImprimirNotaCreditoNQ;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author: mlopez
  Date Created: 01/06/2005
  Description: Inicializo este formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tform_ImprimirNotaCreditoNQ.Inicializar: Boolean;
begin
    Result := True;
    {Revision 1:  cargar los proveedores DayPass}
    CargarProveedoresDayPass();
end;

{-----------------------------------------------------------------------------
  Function Name: OrdenarPorColumna
  Author: mbecerra
  Date Created: 03-Octubre-2008
  Description: Ordena los datos de la TDBListEx de acuerdo a la columna que seleccione el usuario
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCreditoNQ.OrdenarPorColumna(Sender: TObject);
begin
	if not sp_ObtenerNotasCreditoNQ.Active then Exit
    else
        OrdenarGridPorColumna(	TDBListExColumn(Sender).Columns.List,
                                TDBListExColumn(Sender),
                                TDBListExColumn(Sender).FieldName);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarProveedoresDayPass
  Author: mbecerra
  Date Created: 04-Sept-2008
  Description: carga los proveedores DayPass
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCreditoNQ.CargarProveedoresDayPass;
var
  	I : Integer;
  	Qry : TADOQuery;
    S : AnsiString;
begin
    Qry := TAdoQuery.Create(nil);
    try
        Qry.Connection := DMConnections.BaseCAC;
        S := 'SELECT * FROM ProveedoresDayPass WITH (nolock) ORDER BY Descripcion';
        Qry.SQL.Text := S;
        Qry.Open;
        cbProveedorDayPass.Clear;
        I := -1;
        while not Qry.Eof do begin
          cbProveedorDayPass.Items.Add(	Qry.FieldByName('Descripcion').AsString + Space(200) +  {descripci�n}
          								Qry.FieldByName('NumeroDocumento').AsString,            {Rut}
                            			Qry.FieldByName('CodigoProveedorDayPass').AsInteger);   {codigoProveedor}

          Qry.Next;
        end;
    finally
        Qry.Close;
        Qry.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarClick
  Author: mlopez
  Date Created: 01/06/2005
  Description: Permito obtener las Notas de Cobro directas a imprimir
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCreditoNQ.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_ERROR_EMPTY_DATA 	= 'No hay datos de acuerdo a las opciones seleccionadas';
	MSG_SIN_PROV 			= 'Falta seleccionar el proveedor DayPass';
	MSG_RANGO_FECHAS_VALIDO = 'La fecha de inicio del rango debe ser menor o igual a la fecha de t�rmino';

begin
	if cbProveedorDayPass.Text = '' then begin
    	MsgBox(MSG_SIN_PROV, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    // Rev.1 / 15-Diciembre-2009 / Nelson Droguett Sierra.
    if deFDesde.Date>deFHasta.Date then begin
    	MsgBox(MSG_RANGO_FECHAS_VALIDO, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;
    //------------------------------------------------------------------------


    sp_ObtenerNotasCreditoNQ.Close;
    //sp_ObtenerNotasCreditoNQ.Parameters.Refresh;
//    if rbNotasCobroPDU.Checked then begin	// Buscar las Notas de Cobro Directas por PDU
//        sp_ObtenerNotasCobroNQ.Parameters.ParamByName('@TipoComprobante').Value := TC_NOTA_COBRO_DIRECTA;
//    end else begin							// Buscar las Notas de Cobro Directas por BHTU
//        sp_ObtenerNotasCobroNQ.Parameters.ParamByName('@TipoComprobante').Value := TC_NOTA_COBRO_DIRECTA_BHTU;
//    end;

    sp_ObtenerNotasCreditoNQ.Parameters.Refresh;
    sp_ObtenerNotasCreditoNQ.Parameters.ParamByName('@FechaDesde').Value := deFDesde.Date;
    sp_ObtenerNotasCreditoNQ.Parameters.ParamByName('@FechaHasta').Value := deFHasta.Date;
    sp_ObtenerNotasCreditoNQ.Parameters.ParamByName('@RutProveedorDayPass').Value := Trim(StrRight(cbProveedorDayPass.Text,200));
    sp_ObtenerNotasCreditoNQ.Parameters.ParamByName('@TipoComprobante').Value := IfThen(rbNotasCobroPDU.Checked,TC_NOTA_CREDITO_A_NQ,TC_NOTA_CREDITO_A_NB);
    sp_ObtenerNotasCreditoNQ.CommandTimeout := 500;
    sp_ObtenerNotasCreditoNQ.Open;

    if sp_ObtenerNotasCreditoNQ.IsEmpty then begin
        MsgBox(MSG_ERROR_EMPTY_DATA, Caption, MB_ICONINFORMATION);
    end
    else begin
        // Rev.1 / 15-Diciembre-2009 / Nelson Droguett Sierra -----------------
        btn_ImprimirNotaCredito.Enabled:=True;
        // --------------------------------------------------------------------
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: sp_ObtenerNotasCobroNQAfterOpen
  Author: mlopez
  Date Created: 01/06/2005
  Description: Habilito / Deshabilito botones para imprimir
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCreditoNQ.sp_ObtenerNotasCreditoNQAfterOpen(DataSet: TDataSet);
begin
    btn_ImprimirNotaCredito.Enabled := not sp_ObtenerNotasCreditoNQ.IsEmpty;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_ImprimirComprobanteClick
  Author: mlopez
  Date Created: 01/06/2005
  Description: Permito imprimir la nota de cobro
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
Revision 1:
    Author : ggomez
    Date : 16/03/2006
    Description : Agregu� el par�metro TipoComprobante en la llamada al m�todo
          ImprimirComprobante.
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCreditoNQ.btn_ImprimirNotaCreditoClick(Sender: TObject);
var
    //f : TReporteNotaDebitoElectronicaForm;			//SS-784-NDR-20101007
    f : TReporteNotaCreditoElectronicaForm;				//SS-784-NDR-20101007

    TipoComprobante, sError : string;
begin
    Screen.Cursor := crHourGlass;
    //REV.3
    with sp_ObtenerNotasCreditoNQ do begin
    	//Rev.2 / 07-Octubre-2010 / Nelson Droguett Sierra
    	f:= TReporteNotaCreditoElectronicaForm.Create(nil);                     //SS-784-NDR-20101007
        try                                                                     //SS-784-NDR-20101007
            if not f.Inicializar(DMConnections.BaseCAC,                         //SS-784-NDR-20101007
                                 FieldByName('TipoComprobante').AsString,       //SS-784-NDR-20101007
                                 FieldByName('NumeroComprobante').AsInteger,    //SS-784-NDR-20101007
                                 sError) then                                   //SS-784-NDR-20101007
                ShowMessage( sError )                                           //SS-784-NDR-20101007
            else begin                                                          //SS-784-NDR-20101007
                f.ppDBText1.Visible:=False;                                     //SS-784-NDR-20101007
                //f.ppDBText13.Visible:=False;                                    //SS-784-NDR-20101007
                //f.ppLabel6.Visible:=False;                                      //SS-784-NDR-20101007
                //no deben ir los campos de �ltimo ajuste del convenio
                f.pplblUltimoAjuste.Visible := False;
                f.ppDBText2.Visible := False;
                f.Ejecutar;                                                     //SS-784-NDR-20101007
            end;                                                                //SS-784-NDR-20101007
        finally                                                                 //SS-784-NDR-20101007
            if Assigned(f) then f.Release;                                      //SS-784-NDR-20101007
        end;                                                                    //SS-784-NDR-20101007
        //try
        //	Application.CreateForm(TReporteNotaDebitoElectronicaForm, f);
        //	if f.Inicializar(	DMConnections.BaseCAC,
        //                       FieldByName('TipoComprobante').AsString,
        //						FieldByName('NumeroComprobante').AsInteger, True,False) then begin
        //   	F.Imprimir(True);
        //	end
        //	else ShowMessage(sError);
        //finally
        //    f.Release;
        //end;
        //FinRev.2------------------------------------------------------------------------------------

    end; {with}


    Screen.Cursor := crDefault;

end;


{-----------------------------------------------------------------------------
  Function Name: btn_CerrarClick
  Author: lgisuk
  Date Created:  20/03/06
  Description:  Permito cerrar el formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCreditoNQ.btn_CerrarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author: lgisuk
  Date Created: 20/03/06
  Description: Libero el formulario de memoria
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCreditoNQ.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    sp_ObtenerNotasCreditoNQ.Close;
    Action := caFree;
end;

procedure Tform_ImprimirNotaCreditoNQ.FormShow(Sender: TObject);
begin
    // seteo fechas por defecto...
    // Hasta HOY
    deFHasta.Date := NowBase(DMConnections.BaseCAC);
    //si estamos a mitad de mes o mas... muestro el mes en curso...DESDE = 01 - mes en curso
    //sino, muestro el mes anterior y lo que va de este..01-mes anterior
    if DaysBetween(deFHasta.Date,StartOfTheMonth(deFHAsta.date)) >= 15 then
        deFDesde.Date := StartOfTheMonth(deFHasta.Date)
    else deFDesde.Date := StartOfTheMonth(deFHasta.Date-15); //como estamos a menos del dia 15, (hoy-15) es el mes anterior...
end;

end.
