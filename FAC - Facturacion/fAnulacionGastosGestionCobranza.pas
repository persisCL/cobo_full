unit fAnulacionGastosGestionCobranza;

interface

uses
  // Utiles varios
  Util, UtilProc, PeaTypes, PeaProcs, BuscaClientes,
  // Parametros generales y base de datos
  ConstParametrosGenerales, UtilDB, DMConnection,
  // generales
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, ExtCtrls, DB, ADODB, Validate, DateEdit, VariantComboBox,
  DmiCtrls, DBClient, ImgList, ComCtrls;

type
  TfrmAnulacionGastosGestionCobranza = class(TForm)
    neNumeroComprobante: TNumericEdit;
    Label1: TLabel;
    btnSalir: TButton;
    btnAnular: TButton;
    Bevel1: TBevel;
    spAnularGastoGestionCobranza: TADOStoredProc;
    procedure btnAnularClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FProcesando : boolean;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

var
  frmAnulacionGastosGestionCobranza: TfrmAnulacionGastosGestionCobranza;

implementation

{$R *.dfm}

procedure TfrmAnulacionGastosGestionCobranza.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmAnulacionGastosGestionCobranza.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 20/10/2005
Description : Incializa el formulario
Parameters : Titulo: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmAnulacionGastosGestionCobranza.Inicializar(Titulo: AnsiString): boolean;
begin
    Caption := Titulo;
    CenterForm(Self);
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 20/10/2005
Description :   sale del form
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAnulacionGastosGestionCobranza.btnSalirClick(Sender: TObject);
begin
    if (not FProcesando) then Close;
end;

{******************************** Function Header ******************************
Function Name: btnAnularClick
Author : ndonadio
Date Created : 20/10/2005
Description :   Anula el gasto para el Comprobante seleccionado si lo hay
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAnulacionGastosGestionCobranza.btnAnularClick(Sender: TObject);
resourcestring
    WARN_THERE_IS_NO_CHARGE = 'No hay Gastos de Cobranza "anulables" para el Comprobante Nota de Cobro N� %d';
    ERROR_INVALID_NUMBER    = 'El valor ingresado no es v�lido.';
    ERROR_CANT_ANULATE      = 'No se puede anular el gasto.';
    PROCESS_ENDS_OK         = 'El proceso finaliz� con �xito.';
const
    SQLStr  = 'SELECT 1 FROM GastosGestionCobranzas (NOLOCK) ' +
      ' WHERE (TipoComprobante = ''NK'' AND NumeroComprobante = %d ) ' +
      ' AND (FechaAnulacion IS NULL) AND (NumeroMovimiento IS NULL)';
var
    ExisteGasto: Boolean;
    StrSQL: AnsiString;
begin
    // valido que el num edit tenga algo coherente
    if not ValidateControls( [neNumeroComprobante], [neNumeroComprobante.ValueInt > 0], Caption, [ERROR_INVALID_NUMBER]) then Exit;

    // Antes de anular verifico que para ese comprobante haya un Gasto Generado.
    StrSQL := Format(SQLStr, [neNumeroComprobante.ValueInt]);
    ExisteGasto := QueryGetValueInt( DMConnections.BaseCAC, StrSQL) = 1;
    if not ExisteGasto then begin
        MsgBox(Format(WARN_THERE_IS_NO_CHARGE, [neNumeroComprobante.ValueInt]), Caption, MB_ICONWARNING);
        Exit;
    end;

    Cursor := crHourglass;
    try
        // Ahora si, trato de anular...
        try
            DMConnections.BaseCAC.BeginTrans;
            spAnularGastoGestionCobranza.Parameters.ParamByName('@TipoComprobante').Value := 'NK';
            spAnularGastoGestionCobranza.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            spAnularGastoGestionCobranza.Parameters.ParamByName('@NumeroComprobante').Value := neNumeroComprobante.ValueInt;
            spAnularGastoGestionCobranza.ExecProc;
            DMConnections.BaseCAC.CommitTrans;
        except
            on e:exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(ERROR_CANT_ANULATE, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
        MsgBox(PROCESS_ENDS_OK, Caption, MB_ICONINFORMATION);
    finally
        Cursor := crDefault;
        neNumeroComprobante.Clear;
    end;
end;

end.
