object frmFacturaciones: TfrmFacturaciones
  Left = 0
  Top = 0
  Caption = 'Listado de Facturaciones'
  ClientHeight = 378
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    766
    378)
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 322
    Top = 13
    Width = 65
    Height = 13
    Caption = 'Fecha desde:'
  end
  object lbl2: TLabel
    Left = 502
    Top = 13
    Width = 63
    Height = 13
    Caption = 'Fecha hasta:'
  end
  object lbl3: TLabel
    Left = 20
    Top = 13
    Width = 33
    Height = 13
    Caption = 'Grupo:'
  end
  object lbl4: TLabel
    Left = 225
    Top = 13
    Width = 23
    Height = 13
    Caption = 'A'#241'o:'
  end
  object lstProcesos: TDBListEx
    Left = 8
    Top = 39
    Width = 750
    Height = 302
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'C'#243'digo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroProcesoFacturacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Grupo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'GrupoFacturacionDescri'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'Ciclo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Ciclo'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'A'#241'o'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Ano'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Inicio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraInicio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Finalizaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraFin'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Operador'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Operador'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Comprobantes'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Comprobantes'
      end>
    DataSource = dsProcesos
    DragReorder = True
    ParentColor = False
    TabOrder = 5
    TabStop = True
    OnDblClick = lstProcesosDblClick
  end
  object btnFiltrar: TButton
    Left = 683
    Top = 8
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Filtrar'
    TabOrder = 4
    OnClick = btnFiltrarClick
  end
  object btnSalir: TButton
    Left = 683
    Top = 347
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Salir'
    TabOrder = 7
    OnClick = btnSalirClick
  end
  object btnConsultar: TButton
    Left = 535
    Top = 347
    Width = 142
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Consultar Facturaci'#243'n'
    TabOrder = 6
    OnClick = btnConsultarClick
  end
  object dateDesde: TDateEdit
    Left = 393
    Top = 10
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 2
    Date = -693594.000000000000000000
  end
  object dateHasta: TDateEdit
    Left = 571
    Top = 10
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 3
    Date = -693594.000000000000000000
  end
  object vcbGrupoFacturacion: TVariantComboBox
    Left = 59
    Top = 10
    Width = 145
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = vcbGrupoFacturacionChange
    Items = <>
  end
  object txtAno: TNumericEdit
    Left = 253
    Top = 10
    Width = 45
    Height = 21
    TabOrder = 1
  end
  object spProcesosFacturacion_SELECT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ProcesosFacturacion_SELECT'
    Parameters = <>
    Left = 504
    Top = 152
  end
  object dsProcesos: TDataSource
    DataSet = spProcesosFacturacion_SELECT
    Left = 504
    Top = 208
  end
end
