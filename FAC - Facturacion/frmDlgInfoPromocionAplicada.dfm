object frmDialogInfoPromocionAplicada: TfrmDialogInfoPromocionAplicada
  Left = 441
  Top = 243
  AlphaBlend = True
  AlphaBlendValue = 225
  BorderStyle = bsNone
  Caption = 'Informaci'#243'n de la Promoci'#243'n'
  ClientHeight = 274
  ClientWidth = 324
  Color = clInfoBk
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object RotatedLabel1: TRotatedLabel
    Left = 0
    Top = 80
    Width = 23
    Height = 187
    Caption = 'Promoci'#243'n Aplicada'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Angle = 90
  end
  object Label1: TLabel
    Left = 32
    Top = 8
    Width = 65
    Height = 13
    Caption = 'Promoci'#243'n N'#186
  end
  object Label2: TLabel
    Left = 32
    Top = 32
    Width = 56
    Height = 13
    Caption = 'Descripci'#243'n'
  end
  object Label3: TLabel
    Left = 32
    Top = 88
    Width = 54
    Height = 13
    Caption = 'Valor Inicial'
  end
  object Label4: TLabel
    Left = 184
    Top = 88
    Width = 49
    Height = 13
    Caption = 'Valor Final'
  end
  object Label5: TLabel
    Left = 32
    Top = 120
    Width = 73
    Height = 13
    Caption = 'TipoDescuento'
  end
  object Label6: TLabel
    Left = 184
    Top = 120
    Width = 52
    Height = 13
    Caption = 'Descuento'
  end
  object Label7: TLabel
    Left = 32
    Top = 184
    Width = 21
    Height = 13
    Caption = 'Baja'
  end
  object Label8: TLabel
    Left = 32
    Top = 152
    Width = 50
    Height = 13
    Caption = 'Activaci'#243'n'
  end
  object Label9: TLabel
    Left = 32
    Top = 216
    Width = 60
    Height = 13
    Caption = 'Observacion'
  end
  object dbtNumeroPromocion: TDBText
    Left = 104
    Top = 8
    Width = 49
    Height = 17
    Alignment = taRightJustify
    DataField = 'NumeroPromocion'
    DataSource = dsObtenerInfoPromocion
  end
  object dbtDescripcion: TDBText
    Left = 104
    Top = 32
    Width = 209
    Height = 49
    DataField = 'Descripcion'
    DataSource = dsObtenerInfoPromocion
    WordWrap = True
  end
  object dbtValorFin: TDBText
    Left = 248
    Top = 88
    Width = 65
    Height = 17
    Alignment = taRightJustify
    DataField = 'ValorFin'
    DataSource = dsObtenerInfoPromocion
  end
  object dbtValorIni: TDBText
    Left = 104
    Top = 88
    Width = 65
    Height = 17
    Alignment = taRightJustify
    DataField = 'ValorIni'
    DataSource = dsObtenerInfoPromocion
  end
  object dbtTipoDesc: TDBText
    Left = 112
    Top = 120
    Width = 25
    Height = 17
    DataField = 'TipoDescuento'
    DataSource = dsObtenerInfoPromocion
  end
  object dbtDescuento: TDBText
    Left = 248
    Top = 120
    Width = 65
    Height = 17
    Alignment = taRightJustify
    DataField = 'Descuento'
    DataSource = dsObtenerInfoPromocion
  end
  object dbtFHBaja: TDBText
    Left = 104
    Top = 184
    Width = 129
    Height = 17
    DataField = 'FechaHoraBaja'
    DataSource = dsObtenerInfoPromocion
  end
  object dbFHActivacion: TDBText
    Left = 104
    Top = 152
    Width = 129
    Height = 17
    DataField = 'FechaHoraActivacion'
    DataSource = dsObtenerInfoPromocion
  end
  object dbtObservacion: TDBText
    Left = 104
    Top = 216
    Width = 209
    Height = 49
    DataField = 'Observacion'
    DataSource = dsObtenerInfoPromocion
    WordWrap = True
  end
  object spObtenerInfoPromocion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfoPromocion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 296
    object spObtenerInfoPromocionNumeroPromocion: TIntegerField
      FieldName = 'NumeroPromocion'
    end
    object spObtenerInfoPromocionDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 80
    end
    object spObtenerInfoPromocionValorIni: TBCDField
      FieldName = 'ValorIni'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
    object spObtenerInfoPromocionValorFin: TBCDField
      FieldName = 'ValorFin'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
    object spObtenerInfoPromocionTipoDescuento: TStringField
      FieldName = 'TipoDescuento'
      FixedChar = True
      Size = 1
    end
    object spObtenerInfoPromocionDescuento: TBCDField
      FieldName = 'Descuento'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
    object spObtenerInfoPromocionFechaHoraActivacion: TDateTimeField
      FieldName = 'FechaHoraActivacion'
    end
    object spObtenerInfoPromocionFechaHoraBaja: TDateTimeField
      FieldName = 'FechaHoraBaja'
    end
    object spObtenerInfoPromocionObservacion: TMemoField
      FieldName = 'Observacion'
      ReadOnly = True
      BlobType = ftMemo
    end
  end
  object dsObtenerInfoPromocion: TDataSource
    DataSet = spObtenerInfoPromocion
    Left = 296
    Top = 32
  end
end
