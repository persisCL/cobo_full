object frmCarpetasMorosos: TfrmCarpetasMorosos
  Left = 0
  Top = 0
  Caption = 'frmCarpetasMorosos'
  ClientHeight = 335
  ClientWidth = 489
  Color = clBtnFace
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dbcUltimosDoce: TDBChart
    Left = 8
    Top = 136
    Width = 337
    Height = 161
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    BackWall.Pen.Visible = False
    BottomWall.Pen.Visible = False
    Foot.AdjustFrame = False
    Foot.Frame.Style = psDash
    Foot.Visible = False
    LeftWall.Brush.Color = clWhite
    LeftWall.Brush.Style = bsClear
    LeftWall.Pen.Visible = False
    PrintProportional = False
    Title.AdjustFrame = False
    Title.Text.Strings = (
      '        ')
    BottomAxis.Axis.Width = 1
    BottomAxis.DateTimeFormat = 'dd/MM/yyyy'
    BottomAxis.Grid.Style = psSolid
    BottomAxis.Grid.Visible = False
    BottomAxis.Increment = 30.000000000000000000
    BottomAxis.LabelsFont.Charset = ANSI_CHARSET
    BottomAxis.LabelsFont.Height = -9
    BottomAxis.LabelsSeparation = 0
    BottomAxis.MinorTicks.Visible = False
    BottomAxis.RoundFirstLabel = False
    BottomAxis.TickOnLabelsOnly = False
    BottomAxis.Title.Caption = 'Meses'
    BottomAxis.Title.Font.Charset = ANSI_CHARSET
    BottomAxis.Title.Font.Height = -9
    Frame.Visible = False
    LeftAxis.Axis.Width = 1
    LeftAxis.Grid.Style = psSolid
    LeftAxis.Grid.Visible = False
    LeftAxis.LabelsFont.Charset = ANSI_CHARSET
    LeftAxis.LabelsFont.Height = -8
    LeftAxis.LabelsSeparation = 50
    LeftAxis.MinorTicks.Visible = False
    LeftAxis.RoundFirstLabel = False
    LeftAxis.TicksInner.Visible = False
    LeftAxis.TickOnLabelsOnly = False
    LeftAxis.Title.Caption = 'Pesos'
    LeftAxis.Title.Font.Charset = ANSI_CHARSET
    Legend.Frame.Visible = False
    Legend.ResizeChart = False
    Legend.Shadow.HorizSize = 0
    Legend.Shadow.VertSize = 0
    Legend.Visible = False
    RightAxis.Labels = False
    RightAxis.LabelsOnAxis = False
    RightAxis.LabelStyle = talNone
    RightAxis.RoundFirstLabel = False
    RightAxis.Visible = False
    TopAxis.Axis.Width = 1
    TopAxis.Axis.Visible = False
    TopAxis.Grid.Style = psDash
    TopAxis.Grid.Visible = False
    TopAxis.Labels = False
    TopAxis.LabelsOnAxis = False
    TopAxis.LabelStyle = talNone
    TopAxis.RoundFirstLabel = False
    TopAxis.Ticks.Visible = False
    TopAxis.TicksInner.Visible = False
    TopAxis.Visible = False
    View3D = False
    View3DWalls = False
    Zoom.Allow = False
    BevelOuter = bvNone
    BevelWidth = 0
    Color = clWhite
    TabOrder = 0
    object Series1: TBarSeries
      Marks.Callout.Brush.Color = clBlack
      Marks.Clip = True
      Marks.Style = smsLabelPercentTotal
      Marks.Transparent = True
      Marks.Visible = False
      DataSource = spUltimosDoce
      SeriesColor = clBlack
      ShowInLegend = False
      XLabelsSource = 'Mes'
      AutoMarkPosition = False
      Dark3D = False
      Gradient.Direction = gdTopBottom
      MultiBar = mbNone
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      XValues.ValueSource = 'IDh'
      YValues.Name = 'Bar'
      YValues.Order = loNone
      YValues.ValueSource = 'TotalAPagar'
    end
  end
  object plConvenios: TppDBPipeline
    DataSource = dsConvenios
    OpenDataSource = False
    SkipWhenNoRecords = False
    UserName = 'plConvenios'
    Left = 184
    Top = 40
    object plConveniosppField1: TppField
      Alignment = taRightJustify
      FieldAlias = 'cdsConveniosEnCarpetaIdConvenioCarpetaLegal'
      FieldName = 'cdsConveniosEnCarpetaIdConvenioCarpetaLegal'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 0
    end
    object plConveniosppField2: TppField
      Alignment = taRightJustify
      FieldAlias = 'cdsConveniosEnCarpetaCodigoConvenio'
      FieldName = 'cdsConveniosEnCarpetaCodigoConvenio'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 1
    end
    object plConveniosppField3: TppField
      FieldAlias = 'cdsConveniosEnCarpetaApellido'
      FieldName = 'cdsConveniosEnCarpetaApellido'
      FieldLength = 60
      DisplayWidth = 60
      Position = 2
    end
    object plConveniosppField4: TppField
      FieldAlias = 'cdsConveniosEnCarpetaApellidoMaterno'
      FieldName = 'cdsConveniosEnCarpetaApellidoMaterno'
      FieldLength = 60
      DisplayWidth = 60
      Position = 3
    end
    object plConveniosppField5: TppField
      FieldAlias = 'cdsConveniosEnCarpetaNombre'
      FieldName = 'cdsConveniosEnCarpetaNombre'
      FieldLength = 30
      DisplayWidth = 30
      Position = 4
    end
    object plConveniosppField6: TppField
      FieldAlias = 'cdsConveniosEnCarpetaFechaHoraImpresion'
      FieldName = 'cdsConveniosEnCarpetaFechaHoraImpresion'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 5
    end
    object plConveniosppField7: TppField
      FieldAlias = 'cdsConveniosEnCarpetaUsuarioImpresion'
      FieldName = 'cdsConveniosEnCarpetaUsuarioImpresion'
      FieldLength = 20
      DisplayWidth = 20
      Position = 6
    end
    object plConveniosppField8: TppField
      FieldAlias = 'cdsConveniosEnCarpetaFechaHoraEnvioALegales'
      FieldName = 'cdsConveniosEnCarpetaFechaHoraEnvioALegales'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 7
    end
    object plConveniosppField9: TppField
      FieldAlias = 'cdsConveniosEnCarpetaUsuarioEnvioLegales'
      FieldName = 'cdsConveniosEnCarpetaUsuarioEnvioLegales'
      FieldLength = 20
      DisplayWidth = 20
      Position = 8
    end
    object plConveniosppField10: TppField
      FieldAlias = 'cdsConveniosEnCarpetaFechaHoraSalidaLegales'
      FieldName = 'cdsConveniosEnCarpetaFechaHoraSalidaLegales'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 9
    end
    object plConveniosppField11: TppField
      FieldAlias = 'cdsConveniosEnCarpetaFechaHoraCreacion'
      FieldName = 'cdsConveniosEnCarpetaFechaHoraCreacion'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 10
    end
    object plConveniosppField12: TppField
      FieldAlias = 'cdsConveniosEnCarpetaUsuarioCreacion'
      FieldName = 'cdsConveniosEnCarpetaUsuarioCreacion'
      FieldLength = 20
      DisplayWidth = 20
      Position = 11
    end
    object plConveniosppField13: TppField
      FieldAlias = 'cdsConveniosEnCarpetaFechaHoraActualizacion'
      FieldName = 'cdsConveniosEnCarpetaFechaHoraActualizacion'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 12
    end
    object plConveniosppField14: TppField
      FieldAlias = 'cdsConveniosEnCarpetaUsuarioActualizacion'
      FieldName = 'cdsConveniosEnCarpetaUsuarioActualizacion'
      FieldLength = 20
      DisplayWidth = 20
      Position = 13
    end
    object plConveniosppField15: TppField
      FieldAlias = 'cdsConveniosEnCarpetaNumeroDocumento'
      FieldName = 'cdsConveniosEnCarpetaNumeroDocumento'
      FieldLength = 20
      DisplayWidth = 20
      Position = 14
    end
    object plConveniosppField16: TppField
      FieldAlias = 'cdsConveniosEnCarpetaNumeroConvenio'
      FieldName = 'cdsConveniosEnCarpetaNumeroConvenio'
      FieldLength = 20
      DisplayWidth = 20
      Position = 15
    end
    object plConveniosppField17: TppField
      FieldAlias = 'cdsConveniosEnCarpetaSeleccionado'
      FieldName = 'cdsConveniosEnCarpetaSeleccionado'
      FieldLength = 0
      DataType = dtBoolean
      DisplayWidth = 5
      Position = 16
    end
    object plConveniosppField18: TppField
      FieldAlias = 'cdsConveniosEnCarpetaApellidoMostrar'
      FieldName = 'cdsConveniosEnCarpetaApellidoMostrar'
      FieldLength = 155
      DisplayWidth = 155
      Position = 17
    end
  end
  object dsComprobantesImpagos: TDataSource
    DataSet = spObtenerComprobantes
    Left = 152
    Top = 72
  end
  object dsConvenios: TDataSource
    DataSet = cdsConvenios
    Left = 152
    Top = 40
  end
  object plComprobantesImpagos: TppDBPipeline
    DataSource = dsComprobantesImpagos
    OpenDataSource = False
    SkipWhenNoRecords = False
    UserName = 'plComprobantesImpagos'
    Left = 184
    Top = 72
    MasterDataPipelineName = 'plConvenios'
    object plComprobantesImpagosppField58: TppField
      FieldAlias = 'Ordenamiento'
      FieldName = 'Ordenamiento'
      FieldLength = 10
      DisplayWidth = 10
      Position = 0
    end
    object plComprobantesImpagosppField1: TppField
      FieldAlias = 'IdSesion'
      FieldName = 'IdSesion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField2: TppField
      FieldAlias = 'TipoComprobante'
      FieldName = 'TipoComprobante'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField3: TppField
      FieldAlias = 'NumeroComprobante'
      FieldName = 'NumeroComprobante'
      FieldLength = 0
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField59: TppField
      FieldAlias = 'NumeroComprobanteStr'
      FieldName = 'NumeroComprobanteStr'
      FieldLength = 10
      DisplayWidth = 10
      Position = 4
    end
    object plComprobantesImpagosppField4: TppField
      FieldAlias = 'FechaCreacion'
      FieldName = 'FechaCreacion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField5: TppField
      FieldAlias = 'PeriodoInicial'
      FieldName = 'PeriodoInicial'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField6: TppField
      FieldAlias = 'PeriodoFinal'
      FieldName = 'PeriodoFinal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField7: TppField
      FieldAlias = 'FechaEmision'
      FieldName = 'FechaEmision'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField8: TppField
      FieldAlias = 'FechaVencimiento'
      FieldName = 'FechaVencimiento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField9: TppField
      FieldAlias = 'CodigoConvenio'
      FieldName = 'CodigoConvenio'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField10: TppField
      FieldAlias = 'Personeria'
      FieldName = 'Personeria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField11: TppField
      FieldAlias = 'Apellido'
      FieldName = 'Apellido'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 12
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField12: TppField
      FieldAlias = 'ApellidoMaterno'
      FieldName = 'ApellidoMaterno'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 13
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField13: TppField
      FieldAlias = 'Nombre'
      FieldName = 'Nombre'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 14
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField60: TppField
      FieldAlias = 'NombreCliente'
      FieldName = 'NombreCliente'
      FieldLength = 10
      DisplayWidth = 10
      Position = 15
    end
    object plComprobantesImpagosppField14: TppField
      FieldAlias = 'TipoDocumento'
      FieldName = 'TipoDocumento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 16
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField15: TppField
      FieldAlias = 'NumeroDocumento'
      FieldName = 'NumeroDocumento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 17
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField16: TppField
      FieldAlias = 'Sexo'
      FieldName = 'Sexo'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 18
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField17: TppField
      FieldAlias = 'Calle'
      FieldName = 'Calle'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 19
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField18: TppField
      FieldAlias = 'Numero'
      FieldName = 'Numero'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 20
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField19: TppField
      FieldAlias = 'DetalleDomicilio'
      FieldName = 'DetalleDomicilio'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 21
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField20: TppField
      FieldAlias = 'CodigoComuna'
      FieldName = 'CodigoComuna'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 22
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField21: TppField
      FieldAlias = 'CodigoRegion'
      FieldName = 'CodigoRegion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 23
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField22: TppField
      FieldAlias = 'TotalComprobante'
      FieldName = 'TotalComprobante'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 24
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField23: TppField
      FieldAlias = 'SaldoAnterior'
      FieldName = 'SaldoAnterior'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 25
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField24: TppField
      FieldAlias = 'TotalAPagar'
      FieldName = 'TotalAPagar'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 26
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField25: TppField
      FieldAlias = 'AjusteSencilloAnterior'
      FieldName = 'AjusteSencilloAnterior'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 27
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField26: TppField
      FieldAlias = 'AjusteSencilloActual'
      FieldName = 'AjusteSencilloActual'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 28
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField27: TppField
      FieldAlias = 'EstadoPago'
      FieldName = 'EstadoPago'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 29
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField28: TppField
      FieldAlias = 'EstadoDebito'
      FieldName = 'EstadoDebito'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 30
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField29: TppField
      FieldAlias = 'FechaHoraImpreso'
      FieldName = 'FechaHoraImpreso'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 31
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField30: TppField
      FieldAlias = 'TipoComprobanteAjustado'
      FieldName = 'TipoComprobanteAjustado'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 32
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField31: TppField
      FieldAlias = 'NumeroComprobanteAjustado'
      FieldName = 'NumeroComprobanteAjustado'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 33
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField32: TppField
      FieldAlias = 'IDRegistroOperacion'
      FieldName = 'IDRegistroOperacion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 34
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField33: TppField
      FieldAlias = 'CodigoTipoMedioPago'
      FieldName = 'CodigoTipoMedioPago'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 35
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField34: TppField
      FieldAlias = 'PAC_CodigoBanco'
      FieldName = 'PAC_CodigoBanco'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 36
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField35: TppField
      FieldAlias = 'PAC_CodigoTipoCuentaBancaria'
      FieldName = 'PAC_CodigoTipoCuentaBancaria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 37
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField36: TppField
      FieldAlias = 'PAC_NroCuentaBancaria'
      FieldName = 'PAC_NroCuentaBancaria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 38
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField37: TppField
      FieldAlias = 'PAC_Sucursal'
      FieldName = 'PAC_Sucursal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 39
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField38: TppField
      FieldAlias = 'PAT_CodigoTipoTarjetaCredito'
      FieldName = 'PAT_CodigoTipoTarjetaCredito'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 40
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField39: TppField
      FieldAlias = 'PAT_NumeroTarjetaCredito'
      FieldName = 'PAT_NumeroTarjetaCredito'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 41
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField40: TppField
      FieldAlias = 'PAT_FechaVencimiento'
      FieldName = 'PAT_FechaVencimiento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 42
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField41: TppField
      FieldAlias = 'PAT_CodigoEmisorTarjetaCredito'
      FieldName = 'PAT_CodigoEmisorTarjetaCredito'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 43
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField42: TppField
      FieldAlias = 'FechaHoraPago'
      FieldName = 'FechaHoraPago'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 44
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField43: TppField
      FieldAlias = 'CodigoPersona'
      FieldName = 'CodigoPersona'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 45
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField44: TppField
      FieldAlias = 'CodigoPostal'
      FieldName = 'CodigoPostal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 46
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField45: TppField
      FieldAlias = 'Inserto1'
      FieldName = 'Inserto1'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 47
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField46: TppField
      FieldAlias = 'Inserto2'
      FieldName = 'Inserto2'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 48
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField47: TppField
      FieldAlias = 'Inserto3'
      FieldName = 'Inserto3'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 49
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField48: TppField
      FieldAlias = 'Inserto4'
      FieldName = 'Inserto4'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 50
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField49: TppField
      FieldAlias = 'NumeroProcesoFacturacion'
      FieldName = 'NumeroProcesoFacturacion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 51
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField50: TppField
      FieldAlias = 'ImpresionDetallada'
      FieldName = 'ImpresionDetallada'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 52
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField51: TppField
      FieldAlias = 'CantidadReimpresiones'
      FieldName = 'CantidadReimpresiones'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 53
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField52: TppField
      FieldAlias = 'InteresesBonificados'
      FieldName = 'InteresesBonificados'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 54
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField53: TppField
      FieldAlias = 'EnvioElectronico'
      FieldName = 'EnvioElectronico'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 55
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField54: TppField
      FieldAlias = 'NumeroProcesoContabilizacion'
      FieldName = 'NumeroProcesoContabilizacion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 56
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField55: TppField
      FieldAlias = 'NumeroComprobanteFiscal'
      FieldName = 'NumeroComprobanteFiscal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 57
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField56: TppField
      FieldAlias = 'CodigoImpresoraFiscal'
      FieldName = 'CodigoImpresoraFiscal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 58
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppField57: TppField
      FieldAlias = 'Saldo'
      FieldName = 'Saldo'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 59
      Searchable = False
      Sortable = False
    end
    object plComprobantesImpagosppMasterFieldLink2: TppMasterFieldLink
      MasterFieldName = 'cdsConveniosEnCarpetaCodigoConvenio'
      GuidCollationType = gcString
      DetailFieldName = 'CodigoConvenio'
      DetailSortOrder = soAscending
    end
  end
  object spCargarComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CarpetasMorosos_CargarComprobantes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 72
  end
  object spObtenerComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctDynamic
    ProcedureName = 'CarpetasMorosos_ObtenerComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@IdSesion'
        DataType = ftSmallint
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        DataType = ftInteger
        Value = Null
      end>
    Left = 216
    Top = 72
  end
  object pprCaratula: TppReport
    NoDataBehaviors = [ndBlankReport]
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Caratula (Cartpeta Legales)'
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 10000
    PrinterSetup.mmMarginLeft = 12000
    PrinterSetup.mmMarginRight = 10000
    PrinterSetup.mmMarginTop = 10000
    PrinterSetup.mmPaperHeight = 279000
    PrinterSetup.mmPaperWidth = 216000
    PrinterSetup.PaperSize = 1
    Template.FileName = 'C:\Documents and Settings\ddiaz\My Documents\Caratula.rtm'
    Units = utMillimeters
    BeforePrint = pprCaratulaBeforePrint
    DeviceType = 'Printer'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    ModalPreview = False
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    SavePrinterSetup = True
    ShowPrintDialog = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 44
    Top = 8
    Version = '12.04'
    mmColumnWidth = 0
    object ppTitleBand2: TppTitleBand
      mmBottomOffset = 0
      mmHeight = 78052
      mmPrintPosition = 0
      object ppLabel7: TppLabel
        UserName = 'Label9'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'CARPETA LEGALES'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 20
        Font.Style = [fsBold, fsItalic, fsUnderline]
        Transparent = True
        mmHeight = 8202
        mmLeft = 65617
        mmTop = 25929
        mmWidth = 69586
        BandType = 1
      end
      object ppImgLogoNK: TppImage
        UserName = 'ImgLogoNK'
        AlignHorizontal = ahCenter
        AlignVertical = avCenter
        MaintainAspectRatio = True
        Stretch = True
        Anchors = [atLeft, atBottom]
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Picture.Data = {
          07544269746D6170F2870000424DF28700000000000036000000280000009E00
          0000490000000100180000000000BC870000C40E0000C40E0000000000000000
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
          FEFEFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE
          FFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171712828283A3A3A
          E6E6E6FFFFFFFFFFFFA8A8A8272727282828818181FFFFFFFFFFFF7B7B7BF0F0
          F0FFFFFFE4E4E4515151FFFFFFFFFFFF9A9A9A2727272F2F2FC3C3C3FFFFFFFC
          FCFC3F3F3F3F3F3F3F3F3F2E2E2EF4F4F4FFFFFF5F5F5F313131323232CACACA
          FFFFFFC5C5C5A1A1A1FFFFFFFFFFFF5B5B5B262626323232D0D0D0FFFFFFF9F9
          F9666666FFFFFFFFFFFF878787ACACACE8E8E8787878FFFFFFFFFFFFE9E9E968
          6868FFFFFF535353FFFFFFFFFFFFDFDFDF696969FFFFFF939393D4D4D4C2C2C2
          9F9F9FFFFFFFFFFFFFC8C8C88C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF616161757575FFFFFFF9F9F9000000FF
          FFFFB4B4B4222222FFFFFFFFFFFF6D6D6D696969FFFFFF333333E9E9E9FFFFFF
          111111060606FFFFFFA6A6A6313131FFFFFFFFFFFF000000DDDDDDF8F8F81818
          18FFFFFFFFFFFFFFFFFFFFFFFF6C6C6C868686FFFFFFFFFFFF000000FFFFFFA7
          A7A7707070FFFFFF262626A7A7A7FFFFFFFFFFFF000000E8E8E8FBFBFB0F0F0F
          FFFFFFCCCCCC000000838383FFFFFF000000E8E8E8F0F0F05858589D9D9DFFFF
          FF000000FFFFFFFFFFFFC1C1C15C5C5CFFFFFF5B5B5BB5B5B5FFFFFF000000F9
          F9F9F4F4F4252525D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE1E1E1FFFFFF4242
          42D2D2D2FFFFFFFFFFFFFFFFFF000000FFFFFF3A3A3AFFFFFF4D4D4DC3C3C313
          1313FFFFFF373737DADADAFFFFFFFFFFFFEFEFEFECECECF9F9F9030303ABABAB
          9C9C9CAAAAAAFFFFFFFFFFFFE9E9E99191913D3D3D323232FFFFFFAAAAAA7575
          75FFFFFF000000FFFFFFFFFFFFFFFFFF9D9D9D787878FFFFFF1B1B1BFFFFFF00
          0000BCBCBC787878FFFFFF9D9D9D202020868686000000FFFFFFFFFFFF000000
          636363616161111111C0C0C0FFFFFF616161B8B8B8FFFFFF6767674242426A6A
          6A0C0C0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFEFEFFFFFFF535353B9B9B9
          FFFFFFFFFFFFFFFFFF000000FFFFFF464646959595424242FFFFFF040404FFFF
          FF464646CACACAFFFFFFFFFFFFFBFBFBF8F8F8F8F8F80E0E0ED4D4D4C3C3C3CE
          CECEFFFFFFC3C3C3111111ADADADF3F3F3F8F8F8FFFFFFAAAAAA757575FFFFFF
          000000FFFFFFFFFFFFFFFFFF9393937E7E7EFFFFFF323232343434D6D6D6BBBB
          BB767676FFFFFFFFFFFF1B1B1BC6C6C66A6A6AFFFFFFFFFFFF000000FFFFFFFC
          FCFC959595707070FFFFFF616161B7B7B7FFFFFFF2F2F23E3E3E909090A1A1A1
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFA9A9A9151515DBDBDBB5B5B5000000FFFFFFECECEC000000BABABAD4
          D4D41B1B1B959595FFFFFF434343000000FFFFFFFFFFFF000000FFFFFFDCDCDC
          000000CCCCCCCFCFCF000000E9E9E9F9F9F9000000B8B8B8ABABAB9F9F9FFFFF
          FFC6C6C62E2E2EEBEBEB969696181818FFFFFFA3A3A36A6A6AFFFFFF7D7D7D34
          3434D9D9D9B1B1B1000000FBFBFBFFFFFF000000777777FFFFFFAAAAAA6B6B6B
          FFFFFFFFFFFF707070000000F7F7F7FFFFFFFFFFFF000000C8C8C8C0C0C05454
          546D6D6DFFFFFF565656B1B1B1FFFFFFFFFFFF3C3C3C000000FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
          FFFFFFD3D3D3737373868686FFFFFFFFFFFFFFFFFFF8F8F87F7F7F757575D0D0
          D0FFFFFFFFFFFFC5C5C5E7E7E7FFFFFFFFFFFFB5B5B5FFFFFFFFFFFFEEEEEE79
          7979787878F0F0F0FFFFFFFEFEFEA6A6A6A0A0A0A0A0A0979797FFFFFFFFFFFF
          CECECE707070959595FFFFFFFFFFFFE3E3E3D3D3D3FFFFFFFFFFFFC2C2C27171
          71878787FFFFFFFFFFFFFEFEFEA4A4A4FFFFFFFFFFFFE5E5E5D4D4D4FFFFFFFF
          FFFFF8F8F8B5B5B5FFFFFFFFFFFFFFFFFFB2B2B29E9E9E9C9C9CBFBFBFFFFFFF
          FFFFFFCDCDCDE7E7E7FFFFFFFFFFFFE7E7E7C5C5C5FFFFFFFEFEFEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFEFEFEFDFDFDFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFF
          FFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
          FDFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFDFDFDF6767671A1A1A0000000000000E0E0E515151B8B8B8FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C1A1A1A000000000000363636B0B0B0
          FFFFFFFFFFFFFFFFFFFFFFFFD9D9D94D4D4D0606060000000303033B3B3BB7B7
          B7FFFFFFFFFFFFFFFFFFF4F4F43C3C3C000000000000B7B7B7FFFFFFE0E0E044
          4444000000000000393939C3C3C3FFFFFF5151519D9D9DFFFFFF474747B3B3B3
          FFFFFFFFFFFFFFFFFFFFFFFFE1E1E1464646C5C5C5FFFFFFFFFFFFF5F5F56565
          650E0E0E000000000000373737B2B2B2FFFFFFFFFFFFA3A3A34A4A4AFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCD393939000000000000434343D3D3D3
          FFFFFF494949B2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF545454868686FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C4F4F4FCBCBCBFF
          FFFFFFFFFFFAFAFA6B6B6B0808080000000606064E4E4ED4D4D4FFFFFFFFFFFF
          FFFFFF4D4D4DA0A0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F94040
          40000000000000B1B1B1FFFFFFFFFFFFEEEEEE6161610C0C0C00000003030338
          3838B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3B3B3B
          000000353535A4A4A4D5D5D5E0E0E0B9B9B96D6D6D000000000000D1D1D1FFFF
          FFFFFFFFF9F9F9000000000000BEBEBEF9F9F9F6F6F6A5A5A50000001E1E1EFF
          FFFFFFFFFFB2B2B2000000787878F9F9F9FFFFFFEFEFEF8D8D8D0000007F7F7F
          FFFFFFFFFFFF5C5C5C000000EBEBEBDCDCDCD0D0D0DEDEDE000000323232E6E6
          E6FCFCFCD4D4D4424242060606000000C2C2C2FFFFFF000000878787FFFFFFFF
          FFFFFFFFFFFFFFFFD1D1D1000000A1A1A1FFFFFFBDBDBD000000444444E5E5E5
          FFFFFFFBFBFB9F9F9F000000353535FFFFFF6A6A6A000000FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFB8B8B80000004B4B4BEDEDEDFAFAFACBCBCB2B2B2B06060600
          0000DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003A3A3AFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000000000ACACACFFFFFFCBCB
          CB0000002F2F2FD6D6D6FBFBFBEEEEEE7D7D7D0000005F5F5FFFFFFFFFFFFF00
          0000676767FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF696969000000E8E8E8
          E1E1E1C7C7C7FFFFFFB5B5B50000004C4C4CE9E9E9FFFFFFF8F8F89C9C9C0000
          00434343FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF040404000000D2D2D2FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF313131000000D4D4D4FFFFFF
          232323000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9000000565656FFFF
          FF8A8A8A7D7D7DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000292929FFFFFFFF
          FFFF555555000000FFFFFFFFFFFFFFFFFF989898000000D4D4D4FFFFFFFFFFFF
          FFFFFFFFFFFF717171000000D0D0D0FFFFFF0000008C8C8CFFFFFFFFFFFFFFFF
          FFFFFFFFD4D4D4000000A7A7A7FFFFFF0000004F4F4FFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFF3F3F3B2B2B2FFFFFF717171000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF737373000000F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF4D4D4D000000EDED
          EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF5050500000005C5C5C000000B2B2B2FFFFFF0000003A3A3A
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1000000A7A7A7FFFFFF0000006E6E
          6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF626262000000FFFFFFFFFFFFFF
          FFFFFFFFFF0000005D5D5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB0B0B0
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFEFEFEFFFFFF5B5B5B000000C4C4C4FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFF1B1B1B656565FFFFFF0000007E
          7E7EFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFF353535000000FFFFFFFFFFFF
          FFFFFF9090905C5C5C464646161616000000080808DEDEDEFFFFFFFFFFFF5757
          57000000FFFFFFFFFFFFFFFFFFFFFFFF1818180000005F5F5F9B9B9BC5C5C5FF
          FFFFC4C4C4000000D0D0D0FFFFFF0000008D8D8DFFFFFFFFFFFFFFFFFFFFFFFF
          D4D4D4000000ADADADD4D4D40000006767679393938B8B8B8B8B8B8B8B8B8C8C
          8C9999999B9B9BFFFFFF747474000000FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFF5
          F5F50000000000006B6B6B9F9F9FCBCBCBFFFFFF9D9D9D000000EDEDEDFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFDFDFDFFFFFFFFFF
          FF393939000000B0B0B0E0E0E0000000B8B8B8C3C3C3000000C5C5C5FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004B4B4BFFFFFF0000006F6F6FFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFBEBE
          BE0000007171719191918B8B8B8B8B8B8B8B8B8B8B8B9999999A9A9AF6F6F6FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF000000070707FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000005A5A5AFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF0000001E1E1EFFFFFFC7C7C700000011
          1111787878979797C1C1C1F3F3F3FFFFFFFFFFFFFEFEFEFFFFFF575757000000
          FFFFFFFFFFFFFDFDFDFFFFFFFFFFFFE9E9E99797977777775858583030301313
          13000000CFCFCFFFFFFF0000007A7A7AFFFFFFFFFFFFFFFFFFFFFFFFD9D9D900
          0000A7A7A7EEEEEE0000004E4E4E8787877B7B7B7B7B7B7B7B7B8787873B3B3B
          000000F4F4F4767676000000FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFF
          FFDEDEDE939393737373535353262626111111000000ECECECFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFFFFFF232323000000
          BFBFBFFFFFFFC8C8C8000000B7B7B7D9D9D9000000A9A9A9FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF000000727272FFFFFF0000005E5E5EFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFD4D4D4000000
          5858588686867B7B7B7B7B7B7B7B7B878787363636000000EFEFEFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF4F4F4000000474747FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF707070000000CFCFCFFFFFFF
          FFFFFFFFFFFFFFFFFF7A7A7A000000C5C5C5FFFFFF686868000000FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF000000727272FFFFFFFFFFFF606060000000FFFFFFFF
          FFFFFFFFFFFFFFFF222222848484FFFFFFFFFFFFFFFFFFFFFFFFAFAFAF000000
          D5D5D5FEFEFE000000000000E1E1E1FFFFFFFFFFFFFFFFFFA3A3A3000000C6C6
          C6FFFFFF323232000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF161616080808FF
          FFFF7272720000002C2C2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0CA5A5A5
          FFFFFFFFFFFFFFFFFFFFFFFF878787000000F4F4F4FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF000000444444FFFFFFFFFFFFFFFFFF0D0D0D000000D5D5D5FFFFFFFF
          FFFFCACACA000000B1B1B1FFFFFF1D1D1D000000FEFEFEFFFFFFFFFFFFFFFFFF
          FFFFFF303030000000F9F9F9FFFFFF000000000000B8B8B8FFFFFFFFFFFFFFFF
          FFFEFEFEFFFFFF6F6F6F000000FFFFFFFFFFFFFFFFFFFFFFFF1D1D1D000000FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0C141414FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF0000001A1A1AFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF7A7A7A00000036363682828278
          7878070707000000B6B6B6FFFFFFFFFFFFF8F8F80E0E0E0000005D5D5D7A7A7A
          616161000000222222FFFFFFFFFFFF0000000404040000001313130000009999
          99FFFFFFC1C1C10000002A2A2A787878818181535353000000353535FFFFFFF6
          F6F60000006464640606063232327676765252520000005C5C5CFFFFFFFFFFFF
          FFFFFF4242420000005A5A5A878787666666000000131313F9F9F9FFFFFF6363
          63000000C0C0C00000002C2C2C515151C6C6C6FFFFFFA3A3A30000003C3C3C7A
          7A7A7F7F7F4949490000005A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          000000444444FFFFFFFFFFFF000000000000E7E7E7FFFFFFFFFFFFFFFFFFCACA
          CA000000B0B0B0FFFFFFFFFFFF4040400000005252528686866B6B6B00000002
          0202E8E8E8FFFFFFFFFFFF000000686868444444000000474747626262FFFFFF
          000000030303000000131313000000939393FFFFFFFFFFFF3A3A3A0000005E5E
          5E8787876262620000001F1F1FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF55
          5555000000D8D8D8FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFD7D7D7E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFB0B0B0878787888888BFBF
          BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD9F9F9F959595A1A1A1E2
          E2E2FFFFFFFFFFFFFFFFFFFAFAFA5151510C0C0CFFFFFFEDEDEDF8F8F8FFFFFF
          FFFFFFFFFFFFB8B8B89393938D8D8DA0A0A0DDDDDDFFFFFFFFFFFFFFFFFFEEEE
          EEFEFEFEFFFFFFC5C5C59494949D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEEEEEEA0A0A08888889B9B9BDEDEDEFFFFFFFFFFFFFFFFFFF6F6F6EEEEEE
          FFFFFFFFFFFFC7C7C7909090DDDDDDFFFFFFFFFFFFFFFFFFB0B0B09191918F8F
          8FA2A2A2EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004D
          4D4DFFFFFF000000000000F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFCACACA000000
          B0B0B0FFFFFFFFFFFFFFFFFFF0F0F0A3A3A37F7F7F929292D4D4D4FFFFFFFFFF
          FFFFFFFFFFFFFFEFEFEFF7F7F7FFFFFFF1F1F1A5A5A59A9A9AFFFFFFFAFAFA5F
          5F5F000000FFFFFFEEEEEEF5F5F5FFFFFFFFFFFFFFFFFFEAEAEA9E9E9E898989
          9D9D9DE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A00
          0000D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
          FEFFFFFFFEFEFEFFFFFF474747000000FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFF
          FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFDFDFDFEFEFEFFFFFFFDFDFDFE
          FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003030300000
          00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACACA000000B0B0B0FF
          FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FEFEFEFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF5757570000
          00FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A
          000000515151C1C1C1EEEEEEF6F6F6DDDDDD8C8C8C000000000000BCBCBCFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFDADADACACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000001A1A1AFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C6000000ACACACFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDC6C6C6FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E169
          6969161616000000000000000000333333999999FFFFFFFFFFFFFEFEFEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6464646C6C6CFFFFFFFFFFFFFDFDFDFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF545454CECECEFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFE
          FEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFFFFFEFFFFFAFEFFFEFEFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
          FEFCFFFEFEFFFEFDFFFEFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFEFFFFFDFFFFFFFFFEFFFFFEFFFCFFFEFEFFFEFFFFFEFF
          FEFFFFFEFFFCFFFFFDFFFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFE
          FFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
          FEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFAFEFF
          FFFDFFFEFEFFFDFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          F9FFFFFAFFFEFCFEFEFDFFFCFEFFFEFEFEFFFDFEFFFEFEFFFDFEFFFCFFFFFFFE
          FFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFE
          FEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFFFFFF
          FEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFCFFFE
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFCFDFBFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFEFC
          FEFFFEFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFEFDFFFFFB
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9F765B994
          71C19F71C39C71C29C71C29C71C39C71C29C71C29C70C39C65C19782BCA2FFFF
          FFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFBFFFEE8F3EECDE8DAB5E0CD9FD4C18DCFB280C9A671C19E70C29D71
          C39C71C39C71C29C71C39C71C29C71C29C71C39C71C29C71C39C71C29C71C29C
          71C39C71C29C71C39C71C39C71C29C6EC29A7DC2A4FFFFFFFDFFFFFEFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF7F10EA15A28AA6A26
          AA6B27A96B24AA6B25AA6B26AA6B28A96B1EA96829A46BEEFDF5FFFFFFFBFFFF
          FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFFFFFBFFFEFEFDFEFFFEFEFEFFFEFFFDFEFFFFFEFDFEFFFDFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F3EBB8E0CE8BD1B06BC1984AB08132AA70
          23A1641EA3641EA66422A86523A86526A86728AA692AAA6928AA6B26AA6B26AA
          6B26A96B26AA6B26AA6B26AA6B26AA6B26A96B26AA6B26AA6B26AA6B26AA6B26
          A96B26AA6B26AA6B26A96B22A9683AAA76FFFFFFFEFFFEFEFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7132AE7232AE
          7230AE7233AE7230AD712AAF7027A16AEAF9F3FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFEFFFFFEFFFEFEFFFEFFFCFEFFFFFFFFFFFFFFFFFFFFFFE9F4F4B2DC
          CC7CC5A349B37F27A5691FA26220A86225A96828AB6C2BAC6E2FAD7030AE7131
          AE7131AD7231AD7231AE7231AE7231AD7231AE7231AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AE7233AD72
          33AD7131AE7219A362BFE4D8FFFFFFFEFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFA
          FEFEFEFEFFFFFFFFFFFFFFF7FFFABBDFCF6DC39E38A7741DA46422A96224AB69
          28AD6F2CAE702CAD712BAD7331AD7131AD7132AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD722EAE712E
          A96B71C199FFFFFFFAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FF
          FCABD8C555B78823A1671AA7662CA96B31AC7034AC7231AE7232AE7132AE7032
          AD7233AD7234AD7132AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
          AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD7229AE7025A267FCFF
          FFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFEFEFFFC
          FFFFFFFEFEFEFFFEFFFEFDFDFEFEFFFFFFFFFFFFCDEADD68BE901F9F641BA767
          2AAB6D31AE7130AD7230AE7231AE7131AE7133AD7132AD7231AD7232AD7131AF
          702FAF7031AD7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
          7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEDF8F31AA36533AE7131AE7231AD7234AD7125AA668CCAB1FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFEFFFFFFFF
          FEFDFFFEFFFFFFFFFFFFB5DECA3EAA7922A3602AA96D2BAF7236AC7331AE7131
          AE7132AE7132AD7132AE7231AE7231AE7231AE7232AE7131AE7232AD7232AD72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
          FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
          F31AA36533AE7131AE7232AD722FAE711BA260F2FBFBFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFDFEFFFFFFFFFFFF
          BEE1D339AA751DA6642EAC6F33AC7230AE732BAD722EAD7332AE7132AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
          33AE7131AE7231AE712BAB6E52B587FFFFFFFFFDFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F7F757B2891BA66024
          AE6B31AF6F35AE6F30AE7131AD7332AC7331AD7332AE7132AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
          AE7231AD7125A96995D3B5FFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FEFFFFFAFEFFFEFEFFFFFBFEFFFFFFA7DAC522A16328AB6C32AE7131AD7232AE
          7131AE7232AD7231AE7132AE7132AD7131AE7132AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AD
          711EA766C4E5D4FFFFFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF
          FEFFFEFFFFFFFFFF7AC0A316A45E2AAF6E32AD7231AD7232AE7231AD7232AD72
          32AE7132AE7131AE7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE711AA564
          E1F0EAFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
          FF68BD951CA66332AE7133AE6F31AD7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7119A463E6F4F1FF
          FFFFFEFFFDFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFCFFFFFFFFFF84C8AC18A663
          35AE712EAE722FAF7032AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
          AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AD721CA661E1F0EEFFFFFFFEFF
          FCFFFFFFFFFFFFFDFFFFFCFFFFFCFFFFFFFFFFCDE9D915A15F31AE7132AD7232
          AD7132AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
          7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEDF8F31AA36533AE7131AE7232AD721DA863C8E3D7FFFFFFFDFFFDFFFFFF
          FFFFFFFFFFFFFDFFFFFFFEFFFFFFFF3BAB772AAC6F33AF6D31AD7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
          FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
          F31AA36533AE7131AE7232AD7223A96897D3B7FFFFFFFEFFFEFFFFFFFFFFFFFD
          FFFFF9FFFFFFFFFFCAE9DF1BA66032AD7233AE6F31AD7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
          33AE7131AE7231AE712CAC6E53B587FFFFFFFEFEFEFEFFFFFEFFFFFDFFFEFFFF
          FEFFFFFF80C8AC28A96932AE7137AB7431AE7132AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
          AE7231AE7132AD721BA161F1F9F7FFFDFFFEFFFFFBFFFFFCFFFEFFFFFDFFFFFF
          61BC9029AB6C31AE7231AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE
          7231AD7024A96B7ACAA3FFFFFFFFFFFFFCFFFFFBFDFFFBFFFFFFFFFF5ABA8E2A
          AB6D32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD72
          2FAD721AA264E9F9F0FFFFFFFFFEFFFEFDFFFCFFFEFFFFFF75C49D27AA6B32AE
          7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD7230AD722C
          AA6D54B084FFFFFFFFFDFFFFFDFFFFFFFCFFFFFFABD9C21EA8682EAD7232AD72
          31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
          AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7232AE7132AE7131AE7120A7
          6880C7A7FFFFFFFFFFFCFFFEFEFFFEFFF7FFFD1C9F6534AE6E31AD7232AE7132
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
          7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEDF8F31AA36533AE7131AE7232AE7232AE7232AE7231AE7132AE711CA165
          98D2B6FFFFFFFCFEFFFEFDFFFFFFFF7FC5A61EAC6432AF7132AD7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
          FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
          F31AA36533AE7131AE7232AE7232AE7232AE7232AE712FAC742EAE7116A4638E
          CDB2FFFFFFFFFFFFFFFFFDFEFFFF34A47128AB6E32AE7131AE7132AD7232AD72
          31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
          33AE7131AE7232AE7232AE7232AE7232AE7131AD7234AD7231AD721BA6606ABD
          97FFFFFFFFFFFFFFFFFFE8F8EE23A26A27AC7035AB7431AF7031AE7034AD7231
          AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
          AE7232AE7232AE7232AE7232AE7232AE7131AE7132AE7130AD7125A9663AA977
          D4EEE6FFFFFFFFFFFFE7F7EF36A5741DAC6536AC7233AC7431AD7232AE7131AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
          32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
          7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
          AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F319A36433AE7131AE7131AE
          7131AE7131AE7131AE7131AE7131AE7131AD7231AF7038AC7429AB7116A45D7A
          C3A0FDFFFFFFFFFFFFFFFF69BC9612A2622BAE6C33AF6C2EAD7332AE7131AE71
          31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
          7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131
          AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE71
          31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
          7131AE7131AE712CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F1069D5820A6651EA6651EA6651EA665
          1EA6651EA6651EA6651EA6651EA66522A5651CA76424A66326A66213A25E0F9C
          5883C9A8F3FCF6FFFFFFC8EADD45AB7A15A65F1FA6661EA6651EA6651EA6651E
          A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
          1EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6
          651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651E
          A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
          1EA6651AA56234A770FFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF9FDFDBEE1D5C3E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
          E4DAC5E4DAC5E4DAC5E4DAC6E4DAC6E4D9C5E4DAC5E4DAC5E3DBC5E5D9B6E0D1
          BDD9D3FDFEFFFFFFFFFFFFFFCDE7DFC0E3D8C6E4DAC5E4DAC5E4DAC5E4DAC5E4
          DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
          E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DA
          C5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4
          DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
          E3D9CAE4DCFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFDFEFEFCFEFEFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFBFFFFFEFFFFFFFF
          FFFEFFFFFCFFFFFCFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFF
          FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
          FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
          FDFFFFFCFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
          FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
          FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
          FDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
          FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFEFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
        mmHeight = 19000
        mmLeft = 2117
        mmTop = 8467
        mmWidth = 42000
        BandType = 1
      end
      object ppLabel1: TppLabel
        UserName = 'Label10'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Fecha:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 6615
        mmLeft = 137584
        mmTop = 6350
        mmWidth = 20373
        BandType = 1
      end
      object ppSystemVariable2: TppSystemVariable
        UserName = 'SystemVariable2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DisplayFormat = 'dd/mm/yyyy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 6615
        mmLeft = 162984
        mmTop = 6350
        mmWidth = 30163
        BandType = 1
      end
      object ppLine6: TppLine
        UserName = 'Line2'
        Border.BorderPositions = []
        Border.Color = clWindowText
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Pen.Color = clWindowText
        Pen.Width = 3
        Weight = 2.250000000000000000
        mmHeight = 1058
        mmLeft = 2117
        mmTop = 40217
        mmWidth = 190765
        BandType = 1
      end
      object ppNombre: TppLabel
        UserName = 'ppNombre'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'ppNombre'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 6615
        mmLeft = 31750
        mmTop = 55563
        mmWidth = 59002
        BandType = 1
      end
      object ppApellido: TppLabel
        UserName = 'ppApellido'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'ppApellido'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 6615
        mmLeft = 31750
        mmTop = 49213
        mmWidth = 59267
        BandType = 1
      end
      object ppConvenio: TppLabel
        UserName = 'ppConvenio'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'ppConvenio'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 6615
        mmLeft = 31750
        mmTop = 61913
        mmWidth = 59267
        BandType = 1
      end
      object ppLine7: TppLine
        UserName = 'Line1'
        Border.BorderPositions = []
        Border.Color = clWindowText
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Pen.Color = clWindowText
        Pen.Width = 0
        Weight = 0.125000000000000000
        mmHeight = 529
        mmLeft = 2117
        mmTop = 71967
        mmWidth = 190236
        BandType = 1
      end
      object ppLabel8: TppLabel
        UserName = 'Label8'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Convenio'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 6350
        mmTop = 62971
        mmWidth = 16140
        BandType = 1
      end
      object ppLabel32: TppLabel
        UserName = 'Label2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Nombre:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 6350
        mmTop = 56621
        mmWidth = 14552
        BandType = 1
      end
      object ppLabel33: TppLabel
        UserName = 'Label6'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Apellido:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 6350
        mmTop = 50271
        mmWidth = 14817
        BandType = 1
      end
      object lblUsuario: TppLabel
        UserName = 'lblUsuario'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lblUsuario'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 137584
        mmTop = 15081
        mmWidth = 15610
        BandType = 1
      end
    end
    object ppDetailTransitos: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 17992
      mmPrintPosition = 0
      object subRptNK: TppSubReport
        UserName = 'subRptNK'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ParentPrinterSetup = False
        ShiftRelativeTo = pbSaltoComprobante
        TraverseAllData = False
        DataPipelineName = 'plComprobantesImpagos'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 3704
        mmWidth = 194000
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object crComprobantesImpagos: TppChildReport
          AutoStop = False
          DataPipeline = plComprobantesImpagos
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Caratula (Cartpeta Legales)'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 2000
          PrinterSetup.mmMarginLeft = 2000
          PrinterSetup.mmMarginRight = 2000
          PrinterSetup.mmMarginTop = 2000
          PrinterSetup.mmPaperHeight = 279000
          PrinterSetup.mmPaperWidth = 216000
          PrinterSetup.PaperSize = 1
          Units = utMillimeters
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'plComprobantesImpagos'
          object ppDetailNK: TppDetailBand
            BeforePrint = ppDetailNKBeforePrint
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 187590
            mmPrintPosition = 0
            object ppLblCopiaFiel: TppLabel
              UserName = 'LblCopiaFiel'
              Angle = 35
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Copia Fiel del Original'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Name = 'Arial Black'
              Font.Size = 34
              Font.Style = []
              Transparent = True
              mmHeight = 96838
              mmLeft = 50800
              mmTop = 30427
              mmWidth = 135467
              BandType = 4
              RotatedOriginLeft = 0
              RotatedOriginTop = 82753
            end
            object ppSubReport2: TppSubReport
              UserName = 'SubReport2'
              ExpandAll = True
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ParentPrinterSetup = False
              ParentWidth = False
              PrintBehavior = pbFixed
              TraverseAllData = False
              DataPipelineName = 'plConsumos'
              mmHeight = 58208
              mmLeft = 9790
              mmTop = 57944
              mmWidth = 96309
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object rptReporteConsumo: TppChildReport
                AutoStop = False
                DataPipeline = plConsumos
                NoDataBehaviors = [ndBlankReport]
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'NOTA DE COBRO'
                PrinterSetup.PaperName = 'Letter'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 2540
                PrinterSetup.mmMarginLeft = 2540
                PrinterSetup.mmMarginRight = 2540
                PrinterSetup.mmMarginTop = 2540
                PrinterSetup.mmPaperHeight = 279000
                PrinterSetup.mmPaperWidth = 216000
                PrinterSetup.PaperSize = 1
                Units = utMillimeters
                Version = '12.04'
                mmColumnWidth = 0
                DataPipelineName = 'plConsumos'
                object ppTitleBand3: TppTitleBand
                  mmBottomOffset = 0
                  mmHeight = 6879
                  mmPrintPosition = 0
                  object ppLabel5: TppLabel
                    UserName = 'Label1'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'N'#186' de Matr'#237'cula'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = [fsBold]
                    TextAlignment = taCentered
                    Transparent = True
                    WordWrap = True
                    mmHeight = 5842
                    mmLeft = 2243
                    mmTop = 529
                    mmWidth = 10753
                    BandType = 1
                  end
                  object ppLabel6: TppLabel
                    UserName = 'Label2'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Kil'#243'metros'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 2910
                    mmLeft = 16933
                    mmTop = 2117
                    mmWidth = 12965
                    BandType = 1
                  end
                  object ppLabel9: TppLabel
                    UserName = 'Label3'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Normal'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 2910
                    mmLeft = 30692
                    mmTop = 2117
                    mmWidth = 14023
                    BandType = 1
                  end
                  object ppLabel10: TppLabel
                    UserName = 'Label8'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Punta'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 2910
                    mmLeft = 45244
                    mmTop = 2117
                    mmWidth = 14023
                    BandType = 1
                  end
                  object ppLabel11: TppLabel
                    UserName = 'Label10'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Congesti'#243'n'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 2910
                    mmLeft = 60325
                    mmTop = 2117
                    mmWidth = 14023
                    BandType = 1
                  end
                  object ppLabel12: TppLabel
                    UserName = 'Label4'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Peaje'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    WordWrap = True
                    mmHeight = 2910
                    mmLeft = 86519
                    mmTop = 2117
                    mmWidth = 6350
                    BandType = 1
                  end
                  object ppLine1: TppLine
                    UserName = 'Line3'
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Weight = 0.750000000000000000
                    mmHeight = 3969
                    mmLeft = 254
                    mmTop = 265
                    mmWidth = 92710
                    BandType = 1
                  end
                  object ppLine2: TppLine
                    UserName = 'Line1'
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Position = lpBottom
                    Weight = 0.750000000000000000
                    mmHeight = 1588
                    mmLeft = 254
                    mmTop = 5291
                    mmWidth = 92710
                    BandType = 1
                  end
                end
                object ppDetailBand3: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 56091
                  mmPrintPosition = 0
                  object dbtDetalleConsumoNroMatricula: TppDBText
                    UserName = 'DBText7'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'NroMatricula'
                    DataPipeline = plConsumos
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taCentered
                    Transparent = True
                    DataPipelineName = 'plConsumos'
                    mmHeight = 2879
                    mmLeft = 265
                    mmTop = 254
                    mmWidth = 15240
                    BandType = 4
                  end
                  object dbtDetalleConsumoKilometros: TppDBText
                    UserName = 'DBText1'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'Kilometros'
                    DataPipeline = plConsumos
                    DisplayFormat = '.00'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'plConsumos'
                    mmHeight = 2910
                    mmLeft = 15875
                    mmTop = 265
                    mmWidth = 14023
                    BandType = 4
                  end
                  object dbtDetalleConsumoImporteNormal: TppDBText
                    UserName = 'DBText2'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'DImporteNormal'
                    DataPipeline = plConsumos
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'plConsumos'
                    mmHeight = 2910
                    mmLeft = 30692
                    mmTop = 265
                    mmWidth = 14023
                    BandType = 4
                  end
                  object dbtDetalleConsumoImportePunta: TppDBText
                    UserName = 'DBText3'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'DImportePunta'
                    DataPipeline = plConsumos
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'plConsumos'
                    mmHeight = 2910
                    mmLeft = 45244
                    mmTop = 265
                    mmWidth = 14023
                    BandType = 4
                  end
                  object dbtDetalleConsumoImporteCongestion: TppDBText
                    UserName = 'DBText4'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'DImporteCongestion'
                    DataPipeline = plConsumos
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'plConsumos'
                    mmHeight = 2910
                    mmLeft = 60325
                    mmTop = 265
                    mmWidth = 14288
                    BandType = 4
                  end
                  object dbtDetalleConsumoImporte: TppDBText
                    UserName = 'DBText6'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'DImporte'
                    DataPipeline = plConsumos
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Arial'
                    Font.Size = 7
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'plConsumos'
                    mmHeight = 2879
                    mmLeft = 75142
                    mmTop = 265
                    mmWidth = 17727
                    BandType = 4
                  end
                  object ppLblAnexo: TppLabel
                    UserName = 'LblAnexo'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'DETALLE DE CONSUMO EN ANEXO'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clMenuText
                    Font.Name = 'Arial'
                    Font.Size = 11
                    Font.Style = []
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 14023
                    mmTop = 23283
                    mmWidth = 66411
                    BandType = 4
                  end
                end
                object raCodeModule1: TraCodeModule
                  ProgramStream = {00}
                end
              end
            end
            object ppSubReport3: TppSubReport
              UserName = 'SubReport3'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ParentPrinterSetup = False
              ParentWidth = False
              TraverseAllData = False
              DataPipelineName = 'plObtenerCargos'
              mmHeight = 102129
              mmLeft = 110596
              mmTop = 13229
              mmWidth = 87313
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReport3: TppChildReport
                AutoStop = False
                DataPipeline = plObtenerCargos
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'NOTA DE COBRO'
                PrinterSetup.PaperName = 'Letter'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 2540
                PrinterSetup.mmMarginLeft = 2540
                PrinterSetup.mmMarginRight = 2540
                PrinterSetup.mmMarginTop = 2540
                PrinterSetup.mmPaperHeight = 279000
                PrinterSetup.mmPaperWidth = 216000
                PrinterSetup.PaperSize = 1
                Units = utMillimeters
                Version = '12.04'
                mmColumnWidth = 197379
                DataPipelineName = 'plObtenerCargos'
                object ppDetailBand4: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  mmBottomOffset = 0
                  mmHeight = 4498
                  mmPrintPosition = 0
                  object ppDBText9: TppDBText
                    UserName = 'DBText8'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'Descripcion'
                    DataPipeline = plObtenerCargos
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 9
                    Font.Style = []
                    ParentDataPipeline = False
                    Transparent = True
                    DataPipelineName = 'plObtenerCargos'
                    mmHeight = 3704
                    mmLeft = 0
                    mmTop = 0
                    mmWidth = 65088
                    BandType = 4
                  end
                  object ppDBText10: TppDBText
                    UserName = 'DBText9'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'DescImporte'
                    DataPipeline = plObtenerCargos
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 9
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'plObtenerCargos'
                    mmHeight = 3704
                    mmLeft = 66411
                    mmTop = 0
                    mmWidth = 20373
                    BandType = 4
                  end
                end
                object raCodeModule4: TraCodeModule
                  ProgramStream = {00}
                end
              end
            end
            object ppShape18: TppShape
              UserName = 'Shape18'
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              Visible = False
              mmHeight = 5556
              mmLeft = 161661
              mmTop = 116152
              mmWidth = 35983
              BandType = 4
            end
            object ppdbtNombre: TppDBText
              UserName = 'dbtNombre'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'NombreCliente'
              DataPipeline = plComprobantes
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'plComprobantes'
              mmHeight = 4233
              mmLeft = 22754
              mmTop = 7673
              mmWidth = 83079
              BandType = 4
            end
            object ppdbtDir1: TppDBText
              UserName = 'dbtDir1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'Domicilio'
              DataPipeline = plComprobantes
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              ParentDataPipeline = False
              Transparent = True
              WordWrap = True
              DataPipelineName = 'plComprobantes'
              mmHeight = 9790
              mmLeft = 22754
              mmTop = 12435
              mmWidth = 83079
              BandType = 4
            end
            object ppdbtDir2: TppDBText
              UserName = 'dbtDir2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'Comuna'
              DataPipeline = plComprobantes
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'plComprobantes'
              mmHeight = 4233
              mmLeft = 22754
              mmTop = 22490
              mmWidth = 83344
              BandType = 4
            end
            object ppdbtTotalAPagar: TppDBText
              UserName = 'dbtTotalAPagar'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'TotalAPagar'
              DataPipeline = plComprobantes
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsBold]
              ParentDataPipeline = False
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'plComprobantes'
              mmHeight = 3704
              mmLeft = 165100
              mmTop = 117211
              mmWidth = 31750
              BandType = 4
            end
            object ppLabel20: TppLabel
              UserName = 'Label20'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'N'#250'mero de Convenio:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              mmHeight = 3704
              mmLeft = 22754
              mmTop = 43392
              mmWidth = 31221
              BandType = 4
            end
            object ppdbtNumConvenioFormat: TppDBText
              UserName = 'dbtNumConvenioFormat'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'NumeroConvenioFormateado'
              DataPipeline = plComprobantes
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'plComprobantes'
              mmHeight = 3704
              mmLeft = 60061
              mmTop = 43392
              mmWidth = 45508
              BandType = 4
            end
            object ppLine3: TppLine
              UserName = 'Line3'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Weight = 0.750000000000000000
              mmHeight = 2117
              mmLeft = 10848
              mmTop = 116152
              mmWidth = 95515
              BandType = 4
            end
            object ppLabel26: TppLabel
              UserName = 'Label26'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'TOTAL'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 2910
              mmLeft = 11906
              mmTop = 117211
              mmWidth = 14023
              BandType = 4
            end
            object pplblTotal: TppLabel
              UserName = 'lblTotal'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Total'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              WordWrap = True
              mmHeight = 2910
              mmLeft = 90488
              mmTop = 117211
              mmWidth = 14023
              BandType = 4
            end
            object pplblCongestion: TppLabel
              UserName = 'Label101'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Congesti'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 2910
              mmLeft = 75142
              mmTop = 117211
              mmWidth = 14023
              BandType = 4
            end
            object pplblPunta: TppLabel
              UserName = 'lblPunta'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Punta'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 2910
              mmLeft = 59531
              mmTop = 117211
              mmWidth = 14023
              BandType = 4
            end
            object pplblNormal: TppLabel
              UserName = 'lblNormal'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Normal'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 2910
              mmLeft = 44450
              mmTop = 117211
              mmWidth = 14023
              BandType = 4
            end
            object pplblKm: TppLabel
              UserName = 'lblKm'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Kilometros'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 7
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 2910
              mmLeft = 27517
              mmTop = 117211
              mmWidth = 14023
              BandType = 4
            end
            object ppLine10: TppLine
              UserName = 'Line10'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Position = lpBottom
              Weight = 0.750000000000000000
              mmHeight = 2117
              mmLeft = 10848
              mmTop = 118798
              mmWidth = 95515
              BandType = 4
            end
            object lblTestLegth: TppLabel
              UserName = 'lblTestLegth'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Calle Nombre de Calle Largo123455 Piso 18 Dto Z'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 23283
              mmTop = 2910
              mmWidth = 83079
              BandType = 4
            end
            object ppdbbcConvenio: TppDBBarCode
              UserName = 'dbbcConvenio'
              AlignBarCode = ahLeft
              BarCodeType = bcCode39
              BarColor = clWindowText
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'CodigoPostal'
              DataPipeline = plComprobantes
              PrintHumanReadable = False
              Alignment = taCenter
              AutoSize = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Name = 'Courier New'
              Font.Size = 6
              Font.Style = []
              DataPipelineName = 'plComprobantes'
              mmHeight = 11642
              mmLeft = 22754
              mmTop = 27781
              mmWidth = 80963
              BandType = 4
              mmBarWidth = 254
              mmWideBarRatio = 127000
            end
            object ppShape17: TppShape
              UserName = 'Shape17'
              Brush.Color = clBlack
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              mmHeight = 5556
              mmLeft = 110067
              mmTop = 116152
              mmWidth = 61913
              BandType = 4
            end
            object ppLabel25: TppLabel
              UserName = 'Label25'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'TOTAL A PAGAR AL'
              Color = clBlack
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWhite
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsBold]
              mmHeight = 3704
              mmLeft = 116946
              mmTop = 116946
              mmWidth = 29633
              BandType = 4
            end
            object ppSubReport4: TppSubReport
              UserName = 'SubReport4'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ParentPrinterSetup = False
              ParentWidth = False
              PrintBehavior = pbFixed
              TraverseAllData = False
              DataPipelineName = 'plMensaje'
              mmHeight = 44979
              mmLeft = 10848
              mmTop = 130175
              mmWidth = 87313
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReport4: TppChildReport
                AutoStop = False
                DataPipeline = plMensaje
                PrinterSetup.BinName = 'Default'
                PrinterSetup.DocumentName = 'NOTA DE COBRO'
                PrinterSetup.PaperName = 'Letter'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 2540
                PrinterSetup.mmMarginLeft = 2540
                PrinterSetup.mmMarginRight = 2540
                PrinterSetup.mmMarginTop = 2540
                PrinterSetup.mmPaperHeight = 279401
                PrinterSetup.mmPaperWidth = 215900
                PrinterSetup.PaperSize = 1
                Version = '12.04'
                mmColumnWidth = 0
                DataPipelineName = 'plMensaje'
                object ppTitleBand4: TppTitleBand
                  Visible = False
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object ppDetailBand5: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 8996
                  mmPrintPosition = 0
                  object ppDBMemo1: TppDBMemo
                    UserName = 'DBMemo1'
                    KeepTogether = True
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    CharWrap = False
                    DataField = 'Texto'
                    DataPipeline = plMensaje
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Name = 'Courier New'
                    Font.Size = 8
                    Font.Style = []
                    ParentDataPipeline = False
                    Stretch = True
                    Transparent = True
                    DataPipelineName = 'plMensaje'
                    mmHeight = 8202
                    mmLeft = 265
                    mmTop = 0
                    mmWidth = 88900
                    BandType = 4
                    mmBottomOffset = 0
                    mmOverFlowOffset = 0
                    mmStopPosition = 0
                    mmMinHeight = 0
                    mmLeading = 0
                  end
                end
                object ppSummaryBand2: TppSummaryBand
                  Visible = False
                  AlignToBottom = False
                  mmBottomOffset = 0
                  mmHeight = 0
                  mmPrintPosition = 0
                end
                object raCodeModule2: TraCodeModule
                  ProgramStream = {00}
                end
              end
            end
            object imgGraficoUltimosDoce: TppImage
              UserName = 'imgGraficoUltimosDoce'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              mmHeight = 45244
              mmLeft = 107686
              mmTop = 129911
              mmWidth = 98161
              BandType = 4
            end
            object ppLabel13: TppLabel
              UserName = 'Label13'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 
                'V'#225'lido como documento tributario seg'#250'n Resoluci'#243'n Ex. N'#186' 6080/19' +
                '99 SII, autorizada seg'#250'n Ord. 857 del 6/10/2004 XV DR-SII'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 6
              Font.Style = []
              Transparent = True
              Visible = False
              mmHeight = 2381
              mmLeft = 10583
              mmTop = 178594
              mmWidth = 119856
              BandType = 4
            end
            object ppDBText24: TppDBText
              UserName = 'DBText203'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Color = clBlack
              DataField = 'FechaHoy'
              DataPipeline = plComprobantes
              DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWhite
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = [fsBold]
              ParentDataPipeline = False
              TextAlignment = taRightJustified
              DataPipelineName = 'plComprobantes'
              mmHeight = 3704
              mmLeft = 147638
              mmTop = 116946
              mmWidth = 23548
              BandType = 4
            end
          end
          object ppFooterBand1: TppFooterBand
            Visible = False
            mmBottomOffset = 0
            mmHeight = 1058
            mmPrintPosition = 0
          end
          object ppGroup2: TppGroup
            BreakName = 'NumeroComprobante'
            DataPipeline = plComprobantesImpagos
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            OutlineSettings.CreateNode = True
            NewPage = True
            ReprintOnSubsequentPage = False
            StartOnOddPage = False
            UserName = 'Group2'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = 'plComprobantesImpagos'
            NewFile = False
            object ppGroupHeaderBand2: TppGroupHeaderBand
              mmBottomOffset = 0
              mmHeight = 37306
              mmPrintPosition = 0
              object ppRegion1: TppRegion
                UserName = 'Region1'
                Brush.Style = bsClear
                Caption = 'Region1'
                Pen.Style = psClear
                Transparent = True
                mmHeight = 32808
                mmLeft = 0
                mmTop = 529
                mmWidth = 204259
                BandType = 3
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppImage1: TppImage
                  UserName = 'ImagenFondo'
                  AlignHorizontal = ahLeft
                  AlignVertical = avTop
                  MaintainAspectRatio = False
                  Stretch = True
                  Transparent = True
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  mmHeight = 271992
                  mmLeft = 0
                  mmTop = 0
                  mmWidth = 210873
                  BandType = 3
                  GroupNo = 0
                end
                object ppShape1: TppShape
                  UserName = 'Shape101'
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 6085
                  mmLeft = 160338
                  mmTop = 10055
                  mmWidth = 43127
                  BandType = 3
                  GroupNo = 0
                end
                object ppShape2: TppShape
                  UserName = 'Shape2'
                  Brush.Color = clSilver
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 6085
                  mmLeft = 110332
                  mmTop = 10054
                  mmWidth = 50271
                  BandType = 3
                  GroupNo = 0
                end
                object ppShape3: TppShape
                  UserName = 'Shape3'
                  Brush.Color = clBlack
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 8467
                  mmLeft = 110332
                  mmTop = 1323
                  mmWidth = 50271
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel4: TppLabel
                  UserName = 'Label2'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Caption = 'NOTA DE COBRO N'#186'   '
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWhite
                  Font.Name = 'Arial Black'
                  Font.Size = 11
                  Font.Style = []
                  Transparent = True
                  mmHeight = 5461
                  mmLeft = 111919
                  mmTop = 2381
                  mmWidth = 47625
                  BandType = 3
                  GroupNo = 0
                end
                object ppShape4: TppShape
                  UserName = 'Shape4'
                  Brush.Color = clSilver
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 8467
                  mmLeft = 160338
                  mmTop = 1323
                  mmWidth = 43127
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel15: TppLabel
                  UserName = 'Label15'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Caption = 'VENCIMIENTO'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 10
                  Font.Style = [fsBold]
                  Transparent = True
                  mmHeight = 4233
                  mmLeft = 112448
                  mmTop = 11113
                  mmWidth = 24606
                  BandType = 3
                  GroupNo = 0
                end
                object ppShape5: TppShape
                  UserName = 'Shape5'
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 6615
                  mmLeft = 110332
                  mmTop = 23548
                  mmWidth = 46831
                  BandType = 3
                  GroupNo = 0
                end
                object ppShape6: TppShape
                  UserName = 'Shape6'
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 6615
                  mmLeft = 156898
                  mmTop = 23548
                  mmWidth = 46302
                  BandType = 3
                  GroupNo = 0
                end
                object ppShape7: TppShape
                  UserName = 'Shape7'
                  Brush.Color = clBlack
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 5292
                  mmLeft = 110332
                  mmTop = 18521
                  mmWidth = 92869
                  BandType = 3
                  GroupNo = 0
                end
                object ppLine4: TppLine
                  UserName = 'Line1'
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Pen.Color = clWhite
                  Pen.Width = 2
                  Position = lpLeft
                  Weight = 1.500000000000000000
                  mmHeight = 5292
                  mmLeft = 156634
                  mmTop = 18521
                  mmWidth = 3969
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel16: TppLabel
                  UserName = 'Label16'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Caption = ' Fecha de Emisi'#243'n '
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWhite
                  Font.Name = 'Arial'
                  Font.Size = 9
                  Font.Style = [fsBold]
                  TextAlignment = taCentered
                  Transparent = True
                  mmHeight = 3895
                  mmLeft = 111125
                  mmTop = 19315
                  mmWidth = 43656
                  BandType = 3
                  GroupNo = 0
                end
                object ppLabel17: TppLabel
                  UserName = 'Label17'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Caption = ' Periodo de Cobro '
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWhite
                  Font.Name = 'Arial'
                  Font.Size = 9
                  Font.Style = [fsBold]
                  TextAlignment = taCentered
                  Transparent = True
                  mmHeight = 3895
                  mmLeft = 158486
                  mmTop = 19315
                  mmWidth = 43656
                  BandType = 3
                  GroupNo = 0
                end
                object ppDBText22: TppDBText
                  UserName = 'DBText22'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  DataField = 'PeriodoFinal'
                  DataPipeline = plComprobantesImpagos
                  DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 11
                  Font.Style = []
                  ParentDataPipeline = False
                  TextAlignment = taCentered
                  Transparent = True
                  DataPipelineName = 'plComprobantesImpagos'
                  mmHeight = 4498
                  mmLeft = 181240
                  mmTop = 24871
                  mmWidth = 21431
                  BandType = 3
                  GroupNo = 0
                end
                object ppDBText25: TppDBText
                  UserName = 'DBText25'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  DataField = 'FechaEmision'
                  DataPipeline = plComprobantesImpagos
                  DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 11
                  Font.Style = []
                  ParentDataPipeline = False
                  TextAlignment = taCentered
                  Transparent = True
                  DataPipelineName = 'plComprobantesImpagos'
                  mmHeight = 4487
                  mmLeft = 111655
                  mmTop = 24607
                  mmWidth = 43392
                  BandType = 3
                  GroupNo = 0
                end
                object ppDBText26: TppDBText
                  UserName = 'DBText26'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  DataField = 'PeriodoInicial'
                  DataPipeline = plComprobantesImpagos
                  DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 11
                  Font.Style = []
                  ParentDataPipeline = False
                  TextAlignment = taCentered
                  Transparent = True
                  DataPipelineName = 'plComprobantesImpagos'
                  mmHeight = 4498
                  mmLeft = 158750
                  mmTop = 24606
                  mmWidth = 20373
                  BandType = 3
                  GroupNo = 0
                end
                object ppDBText27: TppDBText
                  UserName = 'DBText27'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  DataField = 'NumeroComprobante'
                  DataPipeline = plComprobantesImpagos
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 14
                  Font.Style = []
                  ParentDataPipeline = False
                  TextAlignment = taRightJustified
                  Transparent = True
                  DataPipelineName = 'plComprobantesImpagos'
                  mmHeight = 5821
                  mmLeft = 162190
                  mmTop = 2381
                  mmWidth = 39688
                  BandType = 3
                  GroupNo = 0
                end
                object ppDBText28: TppDBText
                  UserName = 'DBText28'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  DataField = 'FechaVencimiento'
                  DataPipeline = plComprobantesImpagos
                  DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Name = 'Arial'
                  Font.Size = 11
                  Font.Style = []
                  ParentDataPipeline = False
                  TextAlignment = taCentered
                  Transparent = True
                  DataPipelineName = 'plComprobantesImpagos'
                  mmHeight = 4498
                  mmLeft = 162190
                  mmTop = 11112
                  mmWidth = 40746
                  BandType = 3
                  GroupNo = 0
                end
                object ppLine9: TppLine
                  UserName = 'Line9'
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Position = lpLeft
                  Weight = 0.750000000000000000
                  mmHeight = 6879
                  mmLeft = 179917
                  mmTop = 23019
                  mmWidth = 2646
                  BandType = 3
                  GroupNo = 0
                end
              end
            end
            object ppGroupFooterBand2: TppGroupFooterBand
              PrintHeight = phDynamic
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 94721
              mmPrintPosition = 0
              object ppdbtTotalAPagar2: TppDBText
                UserName = 'dbtTotalAPagar2'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'TotalAPagar'
                DataPipeline = plComprobantes
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 11
                Font.Style = [fsBold]
                ParentDataPipeline = False
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'plComprobantes'
                mmHeight = 4763
                mmLeft = 156369
                mmTop = 24077
                mmWidth = 39952
                BandType = 5
                GroupNo = 0
              end
              object ppdbtVencimiento2: TppDBText
                UserName = 'dbtVencimiento1'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'FechaVencimiento'
                DataPipeline = plComprobantes
                DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 11
                Font.Style = [fsBold]
                ParentDataPipeline = False
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'plComprobantes'
                mmHeight = 4763
                mmLeft = 156634
                mmTop = 30956
                mmWidth = 40217
                BandType = 5
                GroupNo = 0
              end
              object ppdbtConvenio2: TppDBText
                UserName = 'dbtConvenio2'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'NumeroConvenioFormateado'
                DataPipeline = plComprobantes
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'plComprobantes'
                mmHeight = 3175
                mmLeft = 156369
                mmTop = 13758
                mmWidth = 39952
                BandType = 5
                GroupNo = 0
              end
              object ppdbtNroNK2: TppDBText
                UserName = 'dbtNroNK2'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'NumeroComprobante'
                DataPipeline = plComprobantes
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'plComprobantes'
                mmHeight = 3175
                mmLeft = 156369
                mmTop = 17463
                mmWidth = 39952
                BandType = 5
                GroupNo = 0
              end
              object ppLabel28: TppLabel
                UserName = 'Label28'
                HyperlinkColor = clBlue
                AutoSize = False
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'NUMERO DE CONVENIO'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3175
                mmLeft = 121709
                mmTop = 13758
                mmWidth = 38365
                BandType = 5
                GroupNo = 0
              end
              object imgPagada: TppImage
                UserName = 'imgPagada'
                AlignHorizontal = ahCenter
                AlignVertical = avCenter
                MaintainAspectRatio = False
                Stretch = True
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                mmHeight = 9260
                mmLeft = 167217
                mmTop = 0
                mmWidth = 29104
                BandType = 5
                GroupNo = 0
              end
              object imgPACPAT: TppImage
                UserName = 'ImagePACPAT'
                AlignHorizontal = ahCenter
                AlignVertical = avCenter
                MaintainAspectRatio = False
                Stretch = True
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                mmHeight = 21431
                mmLeft = 151077
                mmTop = 0
                mmWidth = 23813
                BandType = 5
                GroupNo = 0
              end
              object imgPublicidad: TppImage
                UserName = 'imgPublicidad'
                AlignHorizontal = ahCenter
                AlignVertical = avCenter
                MaintainAspectRatio = False
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                mmHeight = 52123
                mmLeft = 2117
                mmTop = 1323
                mmWidth = 77258
                BandType = 5
                GroupNo = 0
              end
              object ppdbbcConvenio2: TppDBBarCode
                UserName = 'dbbcConvenio2'
                AlignBarCode = ahLeft
                AutoEncode = True
                AutoSizeFont = False
                BarCodeType = bcCode128
                BarColor = clWindowText
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'CodigoBarras'
                DataPipeline = plComprobantes
                PrintHumanReadable = False
                Alignment = taCenter
                AutoSize = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Courier New'
                Font.Size = 8
                Font.Style = []
                DataPipelineName = 'plComprobantes'
                mmHeight = 10848
                mmLeft = 122767
                mmTop = 37571
                mmWidth = 73290
                BandType = 5
                GroupNo = 0
                mmBarWidth = 254
                mmWideBarRatio = 81280
              end
              object ppDBText23: TppDBText
                UserName = 'DBText23'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'CodigoBarras'
                DataPipeline = plComprobantes
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taCentered
                Transparent = True
                DataPipelineName = 'plComprobantes'
                mmHeight = 3175
                mmLeft = 122767
                mmTop = 50271
                mmWidth = 56092
                BandType = 5
                GroupNo = 0
              end
              object pbSaltoDetalleConsumos: TppPageBreak
                UserName = 'pbSaltoDetalleConsumos'
                Visible = False
                mmHeight = 3440
                mmLeft = 0
                mmTop = 78581
                mmWidth = 212000
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
              end
              object ppSubRptDetConsumos: TppSubReport
                UserName = 'SubRptDetConsumos'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                ParentPrinterSetup = False
                ShiftRelativeTo = pbSaltoDetalleConsumos
                TraverseAllData = False
                DataPipelineName = 'plConsumos'
                mmHeight = 5027
                mmLeft = 0
                mmTop = 73290
                mmWidth = 212000
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport2: TppChildReport
                  AutoStop = False
                  Columns = 2
                  ColumnPositions.Strings = (
                    '6350'
                    '105000')
                  DataPipeline = plConsumos
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.DocumentName = 'Caratula (Cartpeta Legales)'
                  PrinterSetup.PaperName = 'Letter'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 2540
                  PrinterSetup.mmMarginLeft = 2540
                  PrinterSetup.mmMarginRight = 2540
                  PrinterSetup.mmMarginTop = 2540
                  PrinterSetup.mmPaperHeight = 279000
                  PrinterSetup.mmPaperWidth = 216000
                  PrinterSetup.PaperSize = 1
                  Units = utMillimeters
                  Version = '12.04'
                  mmColumnWidth = 98650
                  DataPipelineName = 'plConsumos'
                  object ppTitleBand1: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 39158
                    mmPrintPosition = 0
                    object ppRegion2: TppRegion
                      UserName = 'Region2'
                      Brush.Style = bsClear
                      Caption = 'Region2'
                      Pen.Style = psClear
                      Transparent = True
                      mmHeight = 38365
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 204523
                      BandType = 1
                      mmBottomOffset = 0
                      mmOverFlowOffset = 0
                      mmStopPosition = 0
                      mmMinHeight = 0
                      object ppShape10: TppShape
                        UserName = 'Shape10'
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        mmHeight = 6085
                        mmLeft = 152136
                        mmTop = 9525
                        mmWidth = 43127
                        BandType = 1
                      end
                      object ppShape11: TppShape
                        UserName = 'Shape11'
                        Brush.Color = clSilver
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        mmHeight = 6085
                        mmLeft = 102129
                        mmTop = 9525
                        mmWidth = 50271
                        BandType = 1
                      end
                      object ppShape12: TppShape
                        UserName = 'Shape12'
                        Brush.Color = clBlack
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        mmHeight = 8467
                        mmLeft = 102129
                        mmTop = 794
                        mmWidth = 50271
                        BandType = 1
                      end
                      object ppLabel14: TppLabel
                        UserName = 'Label14'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'NOTA DE COBRO N'#186'   '
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWhite
                        Font.Name = 'Arial Black'
                        Font.Size = 11
                        Font.Style = []
                        Transparent = True
                        mmHeight = 5556
                        mmLeft = 103717
                        mmTop = 1852
                        mmWidth = 47625
                        BandType = 1
                      end
                      object ppShape13: TppShape
                        UserName = 'Shape13'
                        Brush.Color = clSilver
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        mmHeight = 8467
                        mmLeft = 152136
                        mmTop = 794
                        mmWidth = 43127
                        BandType = 1
                      end
                      object ppLabel18: TppLabel
                        UserName = 'Label18'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'VENCIMIENTO'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 10
                        Font.Style = [fsBold]
                        Transparent = True
                        mmHeight = 4233
                        mmLeft = 104246
                        mmTop = 10583
                        mmWidth = 24606
                        BandType = 1
                      end
                      object ppShape14: TppShape
                        UserName = 'Shape14'
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        mmHeight = 6615
                        mmLeft = 102129
                        mmTop = 23019
                        mmWidth = 46831
                        BandType = 1
                      end
                      object ppShape15: TppShape
                        UserName = 'Shape15'
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        mmHeight = 6615
                        mmLeft = 148696
                        mmTop = 23019
                        mmWidth = 46302
                        BandType = 1
                      end
                      object ppShape16: TppShape
                        UserName = 'Shape16'
                        Brush.Color = clBlack
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        mmHeight = 5292
                        mmLeft = 102129
                        mmTop = 17992
                        mmWidth = 92869
                        BandType = 1
                      end
                      object ppLine5: TppLine
                        UserName = 'Line6'
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Pen.Color = clWhite
                        Pen.Width = 2
                        Position = lpLeft
                        Weight = 1.500000000000000000
                        mmHeight = 5292
                        mmLeft = 148432
                        mmTop = 17992
                        mmWidth = 3969
                        BandType = 1
                      end
                      object ppLabel19: TppLabel
                        UserName = 'Label19'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = ' Fecha de Emisi'#243'n '
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWhite
                        Font.Name = 'Arial'
                        Font.Size = 9
                        Font.Style = [fsBold]
                        TextAlignment = taCentered
                        Transparent = True
                        mmHeight = 3969
                        mmLeft = 102923
                        mmTop = 18785
                        mmWidth = 43656
                        BandType = 1
                      end
                      object ppLabel21: TppLabel
                        UserName = 'Label11'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = ' Periodo de Cobro '
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWhite
                        Font.Name = 'Arial'
                        Font.Size = 9
                        Font.Style = [fsBold]
                        TextAlignment = taCentered
                        Transparent = True
                        mmHeight = 3969
                        mmLeft = 150284
                        mmTop = 18785
                        mmWidth = 43656
                        BandType = 1
                      end
                      object ppDBText15: TppDBText
                        UserName = 'dbtPerFin1'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        DataField = 'PeriodoFinal'
                        DataPipeline = plComprobantes
                        DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 11
                        Font.Style = []
                        ParentDataPipeline = False
                        TextAlignment = taCentered
                        Transparent = True
                        DataPipelineName = 'plComprobantes'
                        mmHeight = 4498
                        mmLeft = 171450
                        mmTop = 23019
                        mmWidth = 21431
                        BandType = 1
                      end
                      object ppDBText16: TppDBText
                        UserName = 'dbtFEmision1'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        DataField = 'FechaEmision'
                        DataPipeline = plComprobantes
                        DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 11
                        Font.Style = []
                        ParentDataPipeline = False
                        TextAlignment = taCentered
                        Transparent = True
                        DataPipelineName = 'plComprobantes'
                        mmHeight = 4498
                        mmLeft = 103452
                        mmTop = 24077
                        mmWidth = 43392
                        BandType = 1
                      end
                      object ppDBText17: TppDBText
                        UserName = 'dbtPerInicio1'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        DataField = 'PeriodoInicial'
                        DataPipeline = plComprobantes
                        DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 11
                        Font.Style = []
                        ParentDataPipeline = False
                        TextAlignment = taCentered
                        Transparent = True
                        DataPipelineName = 'plComprobantes'
                        mmHeight = 4498
                        mmLeft = 150284
                        mmTop = 23019
                        mmWidth = 20373
                        BandType = 1
                      end
                      object ppDBText18: TppDBText
                        UserName = 'dbtNroNK1'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        DataField = 'NumeroComprobante'
                        DataPipeline = plComprobantes
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 14
                        Font.Style = []
                        ParentDataPipeline = False
                        TextAlignment = taRightJustified
                        Transparent = True
                        DataPipelineName = 'plComprobantes'
                        mmHeight = 5821
                        mmLeft = 153988
                        mmTop = 1852
                        mmWidth = 39688
                        BandType = 1
                      end
                      object ppDBText19: TppDBText
                        UserName = 'dbtVencimiento2'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        DataField = 'FechaVencimiento'
                        DataPipeline = plComprobantes
                        DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 11
                        Font.Style = []
                        ParentDataPipeline = False
                        TextAlignment = taCentered
                        Transparent = True
                        DataPipelineName = 'plComprobantes'
                        mmHeight = 4498
                        mmLeft = 153988
                        mmTop = 10583
                        mmWidth = 40746
                        BandType = 1
                      end
                      object ppShape19: TppShape
                        UserName = 'Shape19'
                        Brush.Color = clBlack
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        Pen.Color = clGray
                        Shape = stRoundRect
                        mmHeight = 5556
                        mmLeft = 2117
                        mmTop = 31485
                        mmWidth = 194734
                        BandType = 1
                      end
                      object ppLabel22: TppLabel
                        UserName = 'Label22'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'DETALLE DE CONSUMOS POR CUENTA'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWhite
                        Font.Name = 'Arial'
                        Font.Size = 10
                        Font.Style = [fsBold]
                        Transparent = True
                        mmHeight = 4233
                        mmLeft = 4233
                        mmTop = 31485
                        mmWidth = 67733
                        BandType = 1
                      end
                      object ppLine11: TppLine
                        UserName = 'Line11'
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Position = lpLeft
                        Weight = 0.750000000000000000
                        mmHeight = 6879
                        mmLeft = 171715
                        mmTop = 22490
                        mmWidth = 2646
                        BandType = 1
                      end
                      object ppLabel38: TppLabel
                        UserName = 'Label3'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Copia Fiel del Original'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clSilver
                        Font.Name = 'Arial Black'
                        Font.Size = 20
                        Font.Style = []
                        Transparent = True
                        mmHeight = 10054
                        mmLeft = 4233
                        mmTop = 6350
                        mmWidth = 86519
                        BandType = 1
                      end
                    end
                  end
                  object ppHeaderBand2: TppHeaderBand
                    mmBottomOffset = 0
                    mmHeight = 0
                    mmPrintPosition = 0
                  end
                  object ppColumnHeaderBand2: TppColumnHeaderBand
                    mmBottomOffset = 0
                    mmHeight = 7938
                    mmPrintPosition = 0
                    object ppLine8: TppLine
                      UserName = 'Line8'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1588
                      mmLeft = 0
                      mmTop = 6350
                      mmWidth = 94721
                      BandType = 2
                    end
                    object ppLabel27: TppLabel
                      UserName = 'Label27'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Importe'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      WordWrap = True
                      mmHeight = 2910
                      mmLeft = 79640
                      mmTop = 3440
                      mmWidth = 14023
                      BandType = 2
                    end
                    object ppLabel24: TppLabel
                      UserName = 'Label1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Congesti'#243'n'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 2910
                      mmLeft = 63236
                      mmTop = 3440
                      mmWidth = 14023
                      BandType = 2
                    end
                    object ppLabel23: TppLabel
                      UserName = 'Label23'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Punta'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 2910
                      mmLeft = 47625
                      mmTop = 3440
                      mmWidth = 14023
                      BandType = 2
                    end
                    object ppLabel30: TppLabel
                      UserName = 'Label30'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Normal'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 2910
                      mmLeft = 31485
                      mmTop = 3440
                      mmWidth = 14023
                      BandType = 2
                    end
                    object ppLabel31: TppLabel
                      UserName = 'Label21'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Kil'#243'metros'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 2963
                      mmLeft = 16140
                      mmTop = 3440
                      mmWidth = 12869
                      BandType = 2
                    end
                    object ppLabel34: TppLabel
                      UserName = 'Label34'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'N'#186' de Matr'#237'cula'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      WordWrap = True
                      mmHeight = 5821
                      mmLeft = 1852
                      mmTop = 1323
                      mmWidth = 10848
                      BandType = 2
                    end
                    object ppLine12: TppLine
                      UserName = 'Line5'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Weight = 0.750000000000000000
                      mmHeight = 3969
                      mmLeft = 0
                      mmTop = 1323
                      mmWidth = 94721
                      BandType = 2
                    end
                  end
                  object ppDetailBand1: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 3969
                    mmPrintPosition = 0
                    object ppDBText11: TppDBText
                      UserName = 'DBText11'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'NroMatricula'
                      DataPipeline = plConsumos
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'plConsumos'
                      mmHeight = 2879
                      mmLeft = 265
                      mmTop = 265
                      mmWidth = 15240
                      BandType = 4
                    end
                    object ppDBText12: TppDBText
                      UserName = 'DBText10'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Kilometros'
                      DataPipeline = plConsumos
                      DisplayFormat = '.00'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'plConsumos'
                      mmHeight = 2910
                      mmLeft = 15875
                      mmTop = 265
                      mmWidth = 14023
                      BandType = 4
                    end
                    object ppDBText13: TppDBText
                      UserName = 'DBText13'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'DImporteNormal'
                      DataPipeline = plConsumos
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'plConsumos'
                      mmHeight = 2910
                      mmLeft = 30692
                      mmTop = 265
                      mmWidth = 14023
                      BandType = 4
                    end
                    object ppDBText14: TppDBText
                      UserName = 'DBText12'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'DImportePunta'
                      DataPipeline = plConsumos
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'plConsumos'
                      mmHeight = 2910
                      mmLeft = 46302
                      mmTop = 265
                      mmWidth = 14023
                      BandType = 4
                    end
                    object ppDBText20: TppDBText
                      UserName = 'DBText20'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'DImporteCongestion'
                      DataPipeline = plConsumos
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'plConsumos'
                      mmHeight = 2910
                      mmLeft = 61119
                      mmTop = 529
                      mmWidth = 14288
                      BandType = 4
                    end
                    object ppDBText21: TppDBText
                      UserName = 'DBText14'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'DImporte'
                      DataPipeline = plConsumos
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Name = 'Arial'
                      Font.Size = 7
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'plConsumos'
                      mmHeight = 2910
                      mmLeft = 78581
                      mmTop = 529
                      mmWidth = 15081
                      BandType = 4
                    end
                  end
                  object ppColumnFooterBand2: TppColumnFooterBand
                    mmBottomOffset = 0
                    mmHeight = 0
                    mmPrintPosition = 0
                  end
                  object ppGroup4: TppGroup
                    BreakName = 'ppDBText18'
                    BreakType = btCustomField
                    GroupFileSettings.NewFile = False
                    GroupFileSettings.EmailFile = False
                    OutlineSettings.CreateNode = True
                    NewPage = True
                    ReprintOnSubsequentColumn = False
                    ReprintOnSubsequentPage = False
                    StartOnOddPage = False
                    UserName = 'Group4'
                    mmNewColumnThreshold = 0
                    mmNewPageThreshold = 0
                    DataPipelineName = ''
                    NewFile = False
                    object ppGroupHeaderBand4: TppGroupHeaderBand
                      mmBottomOffset = 0
                      mmHeight = 0
                      mmPrintPosition = 0
                    end
                    object ppGroupFooterBand4: TppGroupFooterBand
                      HideWhenOneDetail = False
                      mmBottomOffset = 0
                      mmHeight = 7673
                      mmPrintPosition = 0
                      object ppLine14: TppLine
                        UserName = 'Line7'
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Weight = 0.750000000000000000
                        mmHeight = 2117
                        mmLeft = 0
                        mmTop = 529
                        mmWidth = 94456
                        BandType = 5
                        GroupNo = 0
                      end
                      object ppLabel35: TppLabel
                        UserName = 'Label35'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'TOTAL'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 7
                        Font.Style = [fsBold]
                        TextAlignment = taCentered
                        Transparent = True
                        mmHeight = 2910
                        mmLeft = 794
                        mmTop = 2117
                        mmWidth = 14023
                        BandType = 5
                        GroupNo = 0
                      end
                      object ppLblCongestion17: TppLabel
                        UserName = 'LblCongestion17'
                        HyperlinkColor = clBlue
                        AutoSize = False
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Congesti'#243'n'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Name = 'Arial'
                        Font.Size = 7
                        Font.Style = [fsBold]
                        TextAlignment = taRightJustified
                        Transparent = True
                        mmHeight = 2910
                        mmLeft = 60854
                        mmTop = 2117
                        mmWidth = 14023
                        BandType = 5
                        GroupNo = 0
                      end
                      object ppLblPunta17: TppLabel
                        UserName = 'lblPunta1'
                        HyperlinkColor = clBlue
                        AutoSize = False
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Punta'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Name = 'Arial'
                        Font.Size = 7
                        Font.Style = [fsBold]
                        TextAlignment = taRightJustified
                        Transparent = True
                        mmHeight = 2910
                        mmLeft = 45508
                        mmTop = 2117
                        mmWidth = 14552
                        BandType = 5
                        GroupNo = 0
                      end
                      object pplblNormal17: TppLabel
                        UserName = 'lblNormal1'
                        HyperlinkColor = clBlue
                        AutoSize = False
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Normal'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Name = 'Arial'
                        Font.Size = 7
                        Font.Style = [fsBold]
                        TextAlignment = taRightJustified
                        Transparent = True
                        mmHeight = 2910
                        mmLeft = 30427
                        mmTop = 2117
                        mmWidth = 14023
                        BandType = 5
                        GroupNo = 0
                      end
                      object pplblKm17: TppLabel
                        UserName = 'lblKm1'
                        HyperlinkColor = clBlue
                        AutoSize = False
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Kilometros'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Name = 'Arial'
                        Font.Size = 7
                        Font.Style = [fsBold]
                        TextAlignment = taRightJustified
                        Transparent = True
                        mmHeight = 2910
                        mmLeft = 15610
                        mmTop = 2117
                        mmWidth = 14288
                        BandType = 5
                        GroupNo = 0
                      end
                      object ppLbltotal17: TppLabel
                        UserName = 'Lbltotal17'
                        HyperlinkColor = clBlue
                        AutoSize = False
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Importe'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Name = 'Arial'
                        Font.Size = 7
                        Font.Style = [fsBold]
                        TextAlignment = taRightJustified
                        Transparent = True
                        mmHeight = 2910
                        mmLeft = 75142
                        mmTop = 2117
                        mmWidth = 18521
                        BandType = 5
                        GroupNo = 0
                      end
                      object ppLine15: TppLine
                        UserName = 'Line12'
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Position = lpBottom
                        Weight = 0.750000000000000000
                        mmHeight = 2117
                        mmLeft = 0
                        mmTop = 3969
                        mmWidth = 94721
                        BandType = 5
                        GroupNo = 0
                      end
                    end
                  end
                end
              end
              object ppLabel29: TppLabel
                UserName = 'Label29'
                HyperlinkColor = clBlue
                AutoSize = False
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'NOTA DE COBRO N'#186
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 8
                Font.Style = []
                Transparent = True
                mmHeight = 3175
                mmLeft = 121709
                mmTop = 17463
                mmWidth = 39158
                BandType = 5
                GroupNo = 0
              end
            end
          end
          object raCodeModule3: TraCodeModule
            ProgramStream = {00}
          end
        end
      end
      object pbSaltoComprobante: TppPageBreak
        UserName = 'pbSaltoComprobante'
        mmHeight = 3440
        mmLeft = 0
        mmTop = 794
        mmWidth = 194000
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
      end
      object srComprobantesImpagosNoHay: TppSubReport
        UserName = 'srComprobantesImpagosNoHay'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ShiftRelativeTo = pbSaltoComprobante
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 9525
        mmWidth = 194000
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport5: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Caratula (Cartpeta Legales)'
          PrinterSetup.PaperName = 'Letter'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 10000
          PrinterSetup.mmMarginLeft = 12000
          PrinterSetup.mmMarginRight = 10000
          PrinterSetup.mmMarginTop = 10000
          PrinterSetup.mmPaperHeight = 279000
          PrinterSetup.mmPaperWidth = 216000
          PrinterSetup.PaperSize = 1
          Units = utMillimeters
          Version = '12.04'
          mmColumnWidth = 0
          object ppTitleBand8: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 64558
            mmPrintPosition = 0
            object lblComprobantesImpagosNoHayFecha: TppLabel
              UserName = 'Label1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Fecha:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 16
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 6615
              mmLeft = 141817
              mmTop = 8467
              mmWidth = 20373
              BandType = 1
            end
            object svComprobantesImpagosNoHayFecha: TppSystemVariable
              UserName = 'svTransitosNoFacturadosNoHayFecha1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DisplayFormat = 'dd/mm/yyyy'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 16
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 6615
              mmLeft = 166688
              mmTop = 8467
              mmWidth = 30956
              BandType = 1
            end
            object lblComprobantesImpagosNoHayTit: TppLabel
              UserName = 'Label2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Comprobantes Impagos'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 16
              Font.Style = [fsBold, fsItalic, fsUnderline]
              Transparent = True
              mmHeight = 6615
              mmLeft = 61383
              mmTop = 27517
              mmWidth = 68263
              BandType = 1
            end
            object lineComprobantesImpagosNoHay: TppLine
              UserName = 'lineComprobantesImpagosNoHay'
              Border.BorderPositions = []
              Border.Color = clWindowText
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Pen.Color = clWindowText
              Pen.Width = 8
              Weight = 6.000000000000000000
              mmHeight = 2910
              mmLeft = 0
              mmTop = 35983
              mmWidth = 205317
              BandType = 1
            end
            object lblComprobantesImpagosNoHayMensaje: TppLabel
              UserName = 'Label3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'A la fecha no hay comprobantes impagos'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 16
              Font.Style = [fsBold, fsItalic]
              Transparent = True
              mmHeight = 6615
              mmLeft = 38365
              mmTop = 41275
              mmWidth = 114829
              BandType = 1
            end
            object ppImage4: TppImage
              UserName = 'ImgLogoNK1'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = True
              Stretch = True
              Anchors = [atLeft, atBottom]
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Picture.Data = {
                07544269746D6170F2870000424DF28700000000000036000000280000009E00
                0000490000000100180000000000BC870000C40E0000C40E0000000000000000
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
                FEFEFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE
                FFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171712828283A3A3A
                E6E6E6FFFFFFFFFFFFA8A8A8272727282828818181FFFFFFFFFFFF7B7B7BF0F0
                F0FFFFFFE4E4E4515151FFFFFFFFFFFF9A9A9A2727272F2F2FC3C3C3FFFFFFFC
                FCFC3F3F3F3F3F3F3F3F3F2E2E2EF4F4F4FFFFFF5F5F5F313131323232CACACA
                FFFFFFC5C5C5A1A1A1FFFFFFFFFFFF5B5B5B262626323232D0D0D0FFFFFFF9F9
                F9666666FFFFFFFFFFFF878787ACACACE8E8E8787878FFFFFFFFFFFFE9E9E968
                6868FFFFFF535353FFFFFFFFFFFFDFDFDF696969FFFFFF939393D4D4D4C2C2C2
                9F9F9FFFFFFFFFFFFFC8C8C88C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF616161757575FFFFFFF9F9F9000000FF
                FFFFB4B4B4222222FFFFFFFFFFFF6D6D6D696969FFFFFF333333E9E9E9FFFFFF
                111111060606FFFFFFA6A6A6313131FFFFFFFFFFFF000000DDDDDDF8F8F81818
                18FFFFFFFFFFFFFFFFFFFFFFFF6C6C6C868686FFFFFFFFFFFF000000FFFFFFA7
                A7A7707070FFFFFF262626A7A7A7FFFFFFFFFFFF000000E8E8E8FBFBFB0F0F0F
                FFFFFFCCCCCC000000838383FFFFFF000000E8E8E8F0F0F05858589D9D9DFFFF
                FF000000FFFFFFFFFFFFC1C1C15C5C5CFFFFFF5B5B5BB5B5B5FFFFFF000000F9
                F9F9F4F4F4252525D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE1E1E1FFFFFF4242
                42D2D2D2FFFFFFFFFFFFFFFFFF000000FFFFFF3A3A3AFFFFFF4D4D4DC3C3C313
                1313FFFFFF373737DADADAFFFFFFFFFFFFEFEFEFECECECF9F9F9030303ABABAB
                9C9C9CAAAAAAFFFFFFFFFFFFE9E9E99191913D3D3D323232FFFFFFAAAAAA7575
                75FFFFFF000000FFFFFFFFFFFFFFFFFF9D9D9D787878FFFFFF1B1B1BFFFFFF00
                0000BCBCBC787878FFFFFF9D9D9D202020868686000000FFFFFFFFFFFF000000
                636363616161111111C0C0C0FFFFFF616161B8B8B8FFFFFF6767674242426A6A
                6A0C0C0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFEFEFFFFFFF535353B9B9B9
                FFFFFFFFFFFFFFFFFF000000FFFFFF464646959595424242FFFFFF040404FFFF
                FF464646CACACAFFFFFFFFFFFFFBFBFBF8F8F8F8F8F80E0E0ED4D4D4C3C3C3CE
                CECEFFFFFFC3C3C3111111ADADADF3F3F3F8F8F8FFFFFFAAAAAA757575FFFFFF
                000000FFFFFFFFFFFFFFFFFF9393937E7E7EFFFFFF323232343434D6D6D6BBBB
                BB767676FFFFFFFFFFFF1B1B1BC6C6C66A6A6AFFFFFFFFFFFF000000FFFFFFFC
                FCFC959595707070FFFFFF616161B7B7B7FFFFFFF2F2F23E3E3E909090A1A1A1
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFA9A9A9151515DBDBDBB5B5B5000000FFFFFFECECEC000000BABABAD4
                D4D41B1B1B959595FFFFFF434343000000FFFFFFFFFFFF000000FFFFFFDCDCDC
                000000CCCCCCCFCFCF000000E9E9E9F9F9F9000000B8B8B8ABABAB9F9F9FFFFF
                FFC6C6C62E2E2EEBEBEB969696181818FFFFFFA3A3A36A6A6AFFFFFF7D7D7D34
                3434D9D9D9B1B1B1000000FBFBFBFFFFFF000000777777FFFFFFAAAAAA6B6B6B
                FFFFFFFFFFFF707070000000F7F7F7FFFFFFFFFFFF000000C8C8C8C0C0C05454
                546D6D6DFFFFFF565656B1B1B1FFFFFFFFFFFF3C3C3C000000FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
                FFFFFFD3D3D3737373868686FFFFFFFFFFFFFFFFFFF8F8F87F7F7F757575D0D0
                D0FFFFFFFFFFFFC5C5C5E7E7E7FFFFFFFFFFFFB5B5B5FFFFFFFFFFFFEEEEEE79
                7979787878F0F0F0FFFFFFFEFEFEA6A6A6A0A0A0A0A0A0979797FFFFFFFFFFFF
                CECECE707070959595FFFFFFFFFFFFE3E3E3D3D3D3FFFFFFFFFFFFC2C2C27171
                71878787FFFFFFFFFFFFFEFEFEA4A4A4FFFFFFFFFFFFE5E5E5D4D4D4FFFFFFFF
                FFFFF8F8F8B5B5B5FFFFFFFFFFFFFFFFFFB2B2B29E9E9E9C9C9CBFBFBFFFFFFF
                FFFFFFCDCDCDE7E7E7FFFFFFFFFFFFE7E7E7C5C5C5FFFFFFFEFEFEFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFEFEFDFDFDFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFF
                FFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
                FDFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFDFDFDF6767671A1A1A0000000000000E0E0E515151B8B8B8FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C1A1A1A000000000000363636B0B0B0
                FFFFFFFFFFFFFFFFFFFFFFFFD9D9D94D4D4D0606060000000303033B3B3BB7B7
                B7FFFFFFFFFFFFFFFFFFF4F4F43C3C3C000000000000B7B7B7FFFFFFE0E0E044
                4444000000000000393939C3C3C3FFFFFF5151519D9D9DFFFFFF474747B3B3B3
                FFFFFFFFFFFFFFFFFFFFFFFFE1E1E1464646C5C5C5FFFFFFFFFFFFF5F5F56565
                650E0E0E000000000000373737B2B2B2FFFFFFFFFFFFA3A3A34A4A4AFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCD393939000000000000434343D3D3D3
                FFFFFF494949B2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF545454868686FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C4F4F4FCBCBCBFF
                FFFFFFFFFFFAFAFA6B6B6B0808080000000606064E4E4ED4D4D4FFFFFFFFFFFF
                FFFFFF4D4D4DA0A0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F94040
                40000000000000B1B1B1FFFFFFFFFFFFEEEEEE6161610C0C0C00000003030338
                3838B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3B3B3B
                000000353535A4A4A4D5D5D5E0E0E0B9B9B96D6D6D000000000000D1D1D1FFFF
                FFFFFFFFF9F9F9000000000000BEBEBEF9F9F9F6F6F6A5A5A50000001E1E1EFF
                FFFFFFFFFFB2B2B2000000787878F9F9F9FFFFFFEFEFEF8D8D8D0000007F7F7F
                FFFFFFFFFFFF5C5C5C000000EBEBEBDCDCDCD0D0D0DEDEDE000000323232E6E6
                E6FCFCFCD4D4D4424242060606000000C2C2C2FFFFFF000000878787FFFFFFFF
                FFFFFFFFFFFFFFFFD1D1D1000000A1A1A1FFFFFFBDBDBD000000444444E5E5E5
                FFFFFFFBFBFB9F9F9F000000353535FFFFFF6A6A6A000000FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFB8B8B80000004B4B4BEDEDEDFAFAFACBCBCB2B2B2B06060600
                0000DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003A3A3AFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000000000ACACACFFFFFFCBCB
                CB0000002F2F2FD6D6D6FBFBFBEEEEEE7D7D7D0000005F5F5FFFFFFFFFFFFF00
                0000676767FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF696969000000E8E8E8
                E1E1E1C7C7C7FFFFFFB5B5B50000004C4C4CE9E9E9FFFFFFF8F8F89C9C9C0000
                00434343FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF040404000000D2D2D2FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF313131000000D4D4D4FFFFFF
                232323000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9000000565656FFFF
                FF8A8A8A7D7D7DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000292929FFFFFFFF
                FFFF555555000000FFFFFFFFFFFFFFFFFF989898000000D4D4D4FFFFFFFFFFFF
                FFFFFFFFFFFF717171000000D0D0D0FFFFFF0000008C8C8CFFFFFFFFFFFFFFFF
                FFFFFFFFD4D4D4000000A7A7A7FFFFFF0000004F4F4FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF3F3F3B2B2B2FFFFFF717171000000FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF737373000000F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF4D4D4D000000EDED
                EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF5050500000005C5C5C000000B2B2B2FFFFFF0000003A3A3A
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1000000A7A7A7FFFFFF0000006E6E
                6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF626262000000FFFFFFFFFFFFFF
                FFFFFFFFFF0000005D5D5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB0B0B0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFEFEFEFFFFFF5B5B5B000000C4C4C4FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFF1B1B1B656565FFFFFF0000007E
                7E7EFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFF353535000000FFFFFFFFFFFF
                FFFFFF9090905C5C5C464646161616000000080808DEDEDEFFFFFFFFFFFF5757
                57000000FFFFFFFFFFFFFFFFFFFFFFFF1818180000005F5F5F9B9B9BC5C5C5FF
                FFFFC4C4C4000000D0D0D0FFFFFF0000008D8D8DFFFFFFFFFFFFFFFFFFFFFFFF
                D4D4D4000000ADADADD4D4D40000006767679393938B8B8B8B8B8B8B8B8B8C8C
                8C9999999B9B9BFFFFFF747474000000FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFF5
                F5F50000000000006B6B6B9F9F9FCBCBCBFFFFFF9D9D9D000000EDEDEDFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFDFDFDFFFFFFFFFF
                FF393939000000B0B0B0E0E0E0000000B8B8B8C3C3C3000000C5C5C5FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004B4B4BFFFFFF0000006F6F6FFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFBEBE
                BE0000007171719191918B8B8B8B8B8B8B8B8B8B8B8B9999999A9A9AF6F6F6FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000070707FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000005A5A5AFFFF
                FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF0000001E1E1EFFFFFFC7C7C700000011
                1111787878979797C1C1C1F3F3F3FFFFFFFFFFFFFEFEFEFFFFFF575757000000
                FFFFFFFFFFFFFDFDFDFFFFFFFFFFFFE9E9E99797977777775858583030301313
                13000000CFCFCFFFFFFF0000007A7A7AFFFFFFFFFFFFFFFFFFFFFFFFD9D9D900
                0000A7A7A7EEEEEE0000004E4E4E8787877B7B7B7B7B7B7B7B7B8787873B3B3B
                000000F4F4F4767676000000FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFF
                FFDEDEDE939393737373535353262626111111000000ECECECFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFFFFFF232323000000
                BFBFBFFFFFFFC8C8C8000000B7B7B7D9D9D9000000A9A9A9FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000727272FFFFFF0000005E5E5EFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFD4D4D4000000
                5858588686867B7B7B7B7B7B7B7B7B878787363636000000EFEFEFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF4F4F4000000474747FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF707070000000CFCFCFFFFFFF
                FFFFFFFFFFFFFFFFFF7A7A7A000000C5C5C5FFFFFF686868000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000727272FFFFFFFFFFFF606060000000FFFFFFFF
                FFFFFFFFFFFFFFFF222222848484FFFFFFFFFFFFFFFFFFFFFFFFAFAFAF000000
                D5D5D5FEFEFE000000000000E1E1E1FFFFFFFFFFFFFFFFFFA3A3A3000000C6C6
                C6FFFFFF323232000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF161616080808FF
                FFFF7272720000002C2C2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0CA5A5A5
                FFFFFFFFFFFFFFFFFFFFFFFF878787000000F4F4F4FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF000000444444FFFFFFFFFFFFFFFFFF0D0D0D000000D5D5D5FFFFFFFF
                FFFFCACACA000000B1B1B1FFFFFF1D1D1D000000FEFEFEFFFFFFFFFFFFFFFFFF
                FFFFFF303030000000F9F9F9FFFFFF000000000000B8B8B8FFFFFFFFFFFFFFFF
                FFFEFEFEFFFFFF6F6F6F000000FFFFFFFFFFFFFFFFFFFFFFFF1D1D1D000000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0C141414FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF0000001A1A1AFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF7A7A7A00000036363682828278
                7878070707000000B6B6B6FFFFFFFFFFFFF8F8F80E0E0E0000005D5D5D7A7A7A
                616161000000222222FFFFFFFFFFFF0000000404040000001313130000009999
                99FFFFFFC1C1C10000002A2A2A787878818181535353000000353535FFFFFFF6
                F6F60000006464640606063232327676765252520000005C5C5CFFFFFFFFFFFF
                FFFFFF4242420000005A5A5A878787666666000000131313F9F9F9FFFFFF6363
                63000000C0C0C00000002C2C2C515151C6C6C6FFFFFFA3A3A30000003C3C3C7A
                7A7A7F7F7F4949490000005A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                000000444444FFFFFFFFFFFF000000000000E7E7E7FFFFFFFFFFFFFFFFFFCACA
                CA000000B0B0B0FFFFFFFFFFFF4040400000005252528686866B6B6B00000002
                0202E8E8E8FFFFFFFFFFFF000000686868444444000000474747626262FFFFFF
                000000030303000000131313000000939393FFFFFFFFFFFF3A3A3A0000005E5E
                5E8787876262620000001F1F1FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF55
                5555000000D8D8D8FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFD7D7D7E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFB0B0B0878787888888BFBF
                BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD9F9F9F959595A1A1A1E2
                E2E2FFFFFFFFFFFFFFFFFFFAFAFA5151510C0C0CFFFFFFEDEDEDF8F8F8FFFFFF
                FFFFFFFFFFFFB8B8B89393938D8D8DA0A0A0DDDDDDFFFFFFFFFFFFFFFFFFEEEE
                EEFEFEFEFFFFFFC5C5C59494949D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEEEEEEA0A0A08888889B9B9BDEDEDEFFFFFFFFFFFFFFFFFFF6F6F6EEEEEE
                FFFFFFFFFFFFC7C7C7909090DDDDDDFFFFFFFFFFFFFFFFFFB0B0B09191918F8F
                8FA2A2A2EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004D
                4D4DFFFFFF000000000000F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFCACACA000000
                B0B0B0FFFFFFFFFFFFFFFFFFF0F0F0A3A3A37F7F7F929292D4D4D4FFFFFFFFFF
                FFFFFFFFFFFFFFEFEFEFF7F7F7FFFFFFF1F1F1A5A5A59A9A9AFFFFFFFAFAFA5F
                5F5F000000FFFFFFEEEEEEF5F5F5FFFFFFFFFFFFFFFFFFEAEAEA9E9E9E898989
                9D9D9DE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
                00000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A00
                0000D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                FEFFFFFFFEFEFEFFFFFF474747000000FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFDFDFDFEFEFEFFFFFFFDFDFDFE
                FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003030300000
                00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACACA000000B0B0B0FF
                FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF5757570000
                00FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A
                000000515151C1C1C1EEEEEEF6F6F6DDDDDD8C8C8C000000000000BCBCBCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFDADADACACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000001A1A1AFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C6000000ACACACFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDC6C6C6FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E169
                6969161616000000000000000000333333999999FFFFFFFFFFFFFEFEFEFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6464646C6C6CFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF545454CECECEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFE
                FEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFFFFFEFFFFFAFEFFFEFEFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                FEFCFFFEFEFFFEFDFFFEFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFEFFFFFDFFFFFFFFFEFFFFFEFFFCFFFEFEFFFEFFFFFEFF
                FEFFFFFEFFFCFFFFFDFFFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFE
                FFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                FEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFAFEFF
                FFFDFFFEFEFFFDFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F9FFFFFAFFFEFCFEFEFDFFFCFEFFFEFEFEFFFDFEFFFEFEFFFDFEFFFCFFFFFFFE
                FFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFE
                FEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFFFFFF
                FEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFCFFFE
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFCFDFBFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFEFC
                FEFFFEFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFEFDFFFFFB
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9F765B994
                71C19F71C39C71C29C71C29C71C39C71C29C71C29C70C39C65C19782BCA2FFFF
                FFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFBFFFEE8F3EECDE8DAB5E0CD9FD4C18DCFB280C9A671C19E70C29D71
                C39C71C39C71C29C71C39C71C29C71C29C71C39C71C29C71C39C71C29C71C29C
                71C39C71C29C71C39C71C39C71C29C6EC29A7DC2A4FFFFFFFDFFFFFEFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF7F10EA15A28AA6A26
                AA6B27A96B24AA6B25AA6B26AA6B28A96B1EA96829A46BEEFDF5FFFFFFFBFFFF
                FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFEFFFFFBFFFEFEFDFEFFFEFEFEFFFEFFFDFEFFFFFEFDFEFFFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F3EBB8E0CE8BD1B06BC1984AB08132AA70
                23A1641EA3641EA66422A86523A86526A86728AA692AAA6928AA6B26AA6B26AA
                6B26A96B26AA6B26AA6B26AA6B26AA6B26A96B26AA6B26AA6B26AA6B26AA6B26
                A96B26AA6B26AA6B26A96B22A9683AAA76FFFFFFFEFFFEFEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7132AE7232AE
                7230AE7233AE7230AD712AAF7027A16AEAF9F3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFFFFFEFFFEFEFFFEFFFCFEFFFFFFFFFFFFFFFFFFFFFFE9F4F4B2DC
                CC7CC5A349B37F27A5691FA26220A86225A96828AB6C2BAC6E2FAD7030AE7131
                AE7131AD7231AD7231AE7231AE7231AD7231AE7231AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AE7233AD72
                33AD7131AE7219A362BFE4D8FFFFFFFEFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFA
                FEFEFEFEFFFFFFFFFFFFFFF7FFFABBDFCF6DC39E38A7741DA46422A96224AB69
                28AD6F2CAE702CAD712BAD7331AD7131AD7132AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD722EAE712E
                A96B71C199FFFFFFFAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FF
                FCABD8C555B78823A1671AA7662CA96B31AC7034AC7231AE7232AE7132AE7032
                AD7233AD7234AD7132AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD7229AE7025A267FCFF
                FFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFEFEFFFC
                FFFFFFFEFEFEFFFEFFFEFDFDFEFEFFFFFFFFFFFFCDEADD68BE901F9F641BA767
                2AAB6D31AE7130AD7230AE7231AE7131AE7133AD7132AD7231AD7232AD7131AF
                702FAF7031AD7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7231AD7234AD7125AA668CCAB1FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFEFFFFFFFF
                FEFDFFFEFFFFFFFFFFFFB5DECA3EAA7922A3602AA96D2BAF7236AC7331AE7131
                AE7132AE7132AD7132AE7231AE7231AE7231AE7232AE7131AE7232AD7232AD72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AD722FAE711BA260F2FBFBFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFDFEFFFFFFFFFFFF
                BEE1D339AA751DA6642EAC6F33AC7230AE732BAD722EAD7332AE7132AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7231AE712BAB6E52B587FFFFFFFFFDFEFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F7F757B2891BA66024
                AE6B31AF6F35AE6F30AE7131AD7332AC7331AD7332AE7132AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7231AD7125A96995D3B5FFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFFFFFAFEFFFEFEFFFFFBFEFFFFFFA7DAC522A16328AB6C32AE7131AD7232AE
                7131AE7232AD7231AE7132AE7132AD7131AE7132AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AD
                711EA766C4E5D4FFFFFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF
                FEFFFEFFFFFFFFFF7AC0A316A45E2AAF6E32AD7231AD7232AE7231AD7232AD72
                32AE7132AE7131AE7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE711AA564
                E1F0EAFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
                FF68BD951CA66332AE7133AE6F31AD7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7119A463E6F4F1FF
                FFFFFEFFFDFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFCFFFFFFFFFF84C8AC18A663
                35AE712EAE722FAF7032AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AD721CA661E1F0EEFFFFFFFEFF
                FCFFFFFFFFFFFFFDFFFFFCFFFFFCFFFFFFFFFFCDE9D915A15F31AE7132AD7232
                AD7132AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7232AD721DA863C8E3D7FFFFFFFDFFFDFFFFFF
                FFFFFFFFFFFFFDFFFFFFFEFFFFFFFF3BAB772AAC6F33AF6D31AD7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AD7223A96897D3B7FFFFFFFEFFFEFFFFFFFFFFFFFD
                FFFFF9FFFFFFFFFFCAE9DF1BA66032AD7233AE6F31AD7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7231AE712CAC6E53B587FFFFFFFEFEFEFEFFFFFEFFFFFDFFFEFFFF
                FEFFFFFF80C8AC28A96932AE7137AB7431AE7132AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7231AE7132AD721BA161F1F9F7FFFDFFFEFFFFFBFFFFFCFFFEFFFFFDFFFFFF
                61BC9029AB6C31AE7231AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE
                7231AD7024A96B7ACAA3FFFFFFFFFFFFFCFFFFFBFDFFFBFFFFFFFFFF5ABA8E2A
                AB6D32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD72
                2FAD721AA264E9F9F0FFFFFFFFFEFFFEFDFFFCFFFEFFFFFF75C49D27AA6B32AE
                7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD7230AD722C
                AA6D54B084FFFFFFFFFDFFFFFDFFFFFFFCFFFFFFABD9C21EA8682EAD7232AD72
                31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7232AE7132AE7131AE7120A7
                6880C7A7FFFFFFFFFFFCFFFEFEFFFEFFF7FFFD1C9F6534AE6E31AD7232AE7132
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7232AE7232AE7232AE7231AE7132AE711CA165
                98D2B6FFFFFFFCFEFFFEFDFFFFFFFF7FC5A61EAC6432AF7132AD7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AE7232AE7232AE7232AE712FAC742EAE7116A4638E
                CDB2FFFFFFFFFFFFFFFFFDFEFFFF34A47128AB6E32AE7131AE7132AD7232AD72
                31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7232AE7232AE7232AE7232AE7131AD7234AD7231AD721BA6606ABD
                97FFFFFFFFFFFFFFFFFFE8F8EE23A26A27AC7035AB7431AF7031AE7034AD7231
                AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7232AE7232AE7232AE7232AE7232AE7131AE7132AE7130AD7125A9663AA977
                D4EEE6FFFFFFFFFFFFE7F7EF36A5741DAC6536AC7233AC7431AD7232AE7131AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F319A36433AE7131AE7131AE
                7131AE7131AE7131AE7131AE7131AE7131AD7231AF7038AC7429AB7116A45D7A
                C3A0FDFFFFFFFFFFFFFFFF69BC9612A2622BAE6C33AF6C2EAD7332AE7131AE71
                31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131
                AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE71
                31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                7131AE7131AE712CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F1069D5820A6651EA6651EA6651EA665
                1EA6651EA6651EA6651EA6651EA66522A5651CA76424A66326A66213A25E0F9C
                5883C9A8F3FCF6FFFFFFC8EADD45AB7A15A65F1FA6661EA6651EA6651EA6651E
                A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                1EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6
                651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651E
                A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                1EA6651AA56234A770FFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFF9FDFDBEE1D5C3E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E4DAC5E4DAC5E4DAC5E4DAC6E4DAC6E4D9C5E4DAC5E4DAC5E3DBC5E5D9B6E0D1
                BDD9D3FDFEFFFFFFFFFFFFFFCDE7DFC0E3D8C6E4DAC5E4DAC5E4DAC5E4DAC5E4
                DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DA
                C5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4
                DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E3D9CAE4DCFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFEFEFCFEFEFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFBFFFFFEFFFFFFFF
                FFFEFFFFFCFFFFFCFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                FDFFFFFCFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                FDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
              mmHeight = 19000
              mmLeft = 2117
              mmTop = 8467
              mmWidth = 42000
              BandType = 1
            end
          end
          object ppDetailBand8: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppSummaryBand3: TppSummaryBand
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
        end
      end
    end
    object ppFooterBand2: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 2910
      mmPrintPosition = 0
    end
    object ppGroup3: TppGroup
      BreakName = 'ppConvenio'
      BreakType = btCustomField
      GroupFileSettings.NewFile = False
      GroupFileSettings.EmailFile = False
      OutlineSettings.CreateNode = True
      NewPage = True
      ReprintOnSubsequentPage = False
      StartOnOddPage = False
      UserName = 'Group3'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = ''
      NewFile = False
      object ppGroupHeaderBand3: TppGroupHeaderBand
        mmBottomOffset = 0
        mmHeight = 0
        mmPrintPosition = 0
      end
      object gfConvenio: TppGroupFooterBand
        PrintHeight = phDynamic
        HideWhenOneDetail = False
        mmBottomOffset = 0
        mmHeight = 17727
        mmPrintPosition = 0
        object pbSaltoTransitosNoFacturados: TppPageBreak
          UserName = 'pbSaltoTransitosNoFacturados'
          mmHeight = 3440
          mmLeft = 0
          mmTop = 1323
          mmWidth = 194000
          BandType = 5
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
        end
        object srTransitosNoFacturados: TppSubReport
          UserName = 'srTransitosNoFacturados'
          ExpandAll = False
          NewPrintJob = False
          OutlineSettings.CreateNode = True
          ParentPrinterSetup = False
          ShiftRelativeTo = pbSaltoTransitosNoFacturados
          TraverseAllData = False
          DataPipelineName = 'plTransitosNoFacturados'
          mmHeight = 4498
          mmLeft = 0
          mmTop = 5821
          mmWidth = 194000
          BandType = 5
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          object ppChildReport6: TppChildReport
            AutoStop = False
            Columns = 2
            ColumnPositions.Strings = (
              '6350'
              '105000')
            DataPipeline = plTransitosNoFacturados
            PrinterSetup.BinName = 'Default'
            PrinterSetup.DocumentName = 'Caratula (Cartpeta Legales)'
            PrinterSetup.PaperName = 'Letter'
            PrinterSetup.PrinterName = 'Default'
            PrinterSetup.SaveDeviceSettings = False
            PrinterSetup.mmMarginBottom = 2540
            PrinterSetup.mmMarginLeft = 2540
            PrinterSetup.mmMarginRight = 2540
            PrinterSetup.mmMarginTop = 6350
            PrinterSetup.mmPaperHeight = 279000
            PrinterSetup.mmPaperWidth = 216000
            PrinterSetup.PaperSize = 1
            Units = utMillimeters
            Version = '12.04'
            mmColumnWidth = 98650
            DataPipelineName = 'plTransitosNoFacturados'
            object ppTitleBand7: TppTitleBand
              mmBottomOffset = 0
              mmHeight = 53975
              mmPrintPosition = 0
              object ppSystemVariable4: TppSystemVariable
                UserName = 'SystemVariable2'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 16
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 6615
                mmLeft = 162984
                mmTop = 4233
                mmWidth = 30956
                BandType = 1
              end
              object ppLabel61: TppLabel
                UserName = 'Label36'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Fecha:'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 16
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 6615
                mmLeft = 137584
                mmTop = 4233
                mmWidth = 20373
                BandType = 1
              end
              object ppLabel70: TppLabel
                UserName = 'Label37'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Tr'#225'nsitos no facturados'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 16
                Font.Style = [fsBold, fsItalic, fsUnderline]
                Transparent = True
                mmHeight = 6562
                mmLeft = 57150
                mmTop = 23283
                mmWidth = 64050
                BandType = 1
              end
              object ppLine24: TppLine
                UserName = 'Line17'
                Border.BorderPositions = []
                Border.Color = clWindowText
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Pen.Color = clWindowText
                Pen.Width = 8
                Weight = 6.000000000000000000
                mmHeight = 2910
                mmLeft = 0
                mmTop = 31750
                mmWidth = 205317
                BandType = 1
              end
              object ppLabel71: TppLabel
                UserName = 'Label38'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Cliente:'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 10
                Font.Style = [fsBold, fsUnderline]
                Transparent = True
                mmHeight = 4233
                mmLeft = 4233
                mmTop = 38100
                mmWidth = 12965
                BandType = 1
              end
              object ppDBText45: TppDBText
                UserName = 'DBText29'
                CharWrap = True
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'cdsConveniosEnCarpetaApellidoMostrar'
                DataPipeline = plConvenios
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 10
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'plConvenios'
                mmHeight = 3969
                mmLeft = 23283
                mmTop = 38100
                mmWidth = 95779
                BandType = 1
              end
              object ppImage3: TppImage
                UserName = 'ImgLogoNK3'
                AlignHorizontal = ahCenter
                AlignVertical = avCenter
                MaintainAspectRatio = True
                Stretch = True
                Anchors = [atLeft, atBottom]
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Picture.Data = {
                  07544269746D6170F2870000424DF28700000000000036000000280000009E00
                  0000490000000100180000000000BC870000C40E0000C40E0000000000000000
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
                  FEFEFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE
                  FFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171712828283A3A3A
                  E6E6E6FFFFFFFFFFFFA8A8A8272727282828818181FFFFFFFFFFFF7B7B7BF0F0
                  F0FFFFFFE4E4E4515151FFFFFFFFFFFF9A9A9A2727272F2F2FC3C3C3FFFFFFFC
                  FCFC3F3F3F3F3F3F3F3F3F2E2E2EF4F4F4FFFFFF5F5F5F313131323232CACACA
                  FFFFFFC5C5C5A1A1A1FFFFFFFFFFFF5B5B5B262626323232D0D0D0FFFFFFF9F9
                  F9666666FFFFFFFFFFFF878787ACACACE8E8E8787878FFFFFFFFFFFFE9E9E968
                  6868FFFFFF535353FFFFFFFFFFFFDFDFDF696969FFFFFF939393D4D4D4C2C2C2
                  9F9F9FFFFFFFFFFFFFC8C8C88C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF616161757575FFFFFFF9F9F9000000FF
                  FFFFB4B4B4222222FFFFFFFFFFFF6D6D6D696969FFFFFF333333E9E9E9FFFFFF
                  111111060606FFFFFFA6A6A6313131FFFFFFFFFFFF000000DDDDDDF8F8F81818
                  18FFFFFFFFFFFFFFFFFFFFFFFF6C6C6C868686FFFFFFFFFFFF000000FFFFFFA7
                  A7A7707070FFFFFF262626A7A7A7FFFFFFFFFFFF000000E8E8E8FBFBFB0F0F0F
                  FFFFFFCCCCCC000000838383FFFFFF000000E8E8E8F0F0F05858589D9D9DFFFF
                  FF000000FFFFFFFFFFFFC1C1C15C5C5CFFFFFF5B5B5BB5B5B5FFFFFF000000F9
                  F9F9F4F4F4252525D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE1E1E1FFFFFF4242
                  42D2D2D2FFFFFFFFFFFFFFFFFF000000FFFFFF3A3A3AFFFFFF4D4D4DC3C3C313
                  1313FFFFFF373737DADADAFFFFFFFFFFFFEFEFEFECECECF9F9F9030303ABABAB
                  9C9C9CAAAAAAFFFFFFFFFFFFE9E9E99191913D3D3D323232FFFFFFAAAAAA7575
                  75FFFFFF000000FFFFFFFFFFFFFFFFFF9D9D9D787878FFFFFF1B1B1BFFFFFF00
                  0000BCBCBC787878FFFFFF9D9D9D202020868686000000FFFFFFFFFFFF000000
                  636363616161111111C0C0C0FFFFFF616161B8B8B8FFFFFF6767674242426A6A
                  6A0C0C0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFEFEFFFFFFF535353B9B9B9
                  FFFFFFFFFFFFFFFFFF000000FFFFFF464646959595424242FFFFFF040404FFFF
                  FF464646CACACAFFFFFFFFFFFFFBFBFBF8F8F8F8F8F80E0E0ED4D4D4C3C3C3CE
                  CECEFFFFFFC3C3C3111111ADADADF3F3F3F8F8F8FFFFFFAAAAAA757575FFFFFF
                  000000FFFFFFFFFFFFFFFFFF9393937E7E7EFFFFFF323232343434D6D6D6BBBB
                  BB767676FFFFFFFFFFFF1B1B1BC6C6C66A6A6AFFFFFFFFFFFF000000FFFFFFFC
                  FCFC959595707070FFFFFF616161B7B7B7FFFFFFF2F2F23E3E3E909090A1A1A1
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFA9A9A9151515DBDBDBB5B5B5000000FFFFFFECECEC000000BABABAD4
                  D4D41B1B1B959595FFFFFF434343000000FFFFFFFFFFFF000000FFFFFFDCDCDC
                  000000CCCCCCCFCFCF000000E9E9E9F9F9F9000000B8B8B8ABABAB9F9F9FFFFF
                  FFC6C6C62E2E2EEBEBEB969696181818FFFFFFA3A3A36A6A6AFFFFFF7D7D7D34
                  3434D9D9D9B1B1B1000000FBFBFBFFFFFF000000777777FFFFFFAAAAAA6B6B6B
                  FFFFFFFFFFFF707070000000F7F7F7FFFFFFFFFFFF000000C8C8C8C0C0C05454
                  546D6D6DFFFFFF565656B1B1B1FFFFFFFFFFFF3C3C3C000000FFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
                  FFFFFFD3D3D3737373868686FFFFFFFFFFFFFFFFFFF8F8F87F7F7F757575D0D0
                  D0FFFFFFFFFFFFC5C5C5E7E7E7FFFFFFFFFFFFB5B5B5FFFFFFFFFFFFEEEEEE79
                  7979787878F0F0F0FFFFFFFEFEFEA6A6A6A0A0A0A0A0A0979797FFFFFFFFFFFF
                  CECECE707070959595FFFFFFFFFFFFE3E3E3D3D3D3FFFFFFFFFFFFC2C2C27171
                  71878787FFFFFFFFFFFFFEFEFEA4A4A4FFFFFFFFFFFFE5E5E5D4D4D4FFFFFFFF
                  FFFFF8F8F8B5B5B5FFFFFFFFFFFFFFFFFFB2B2B29E9E9E9C9C9CBFBFBFFFFFFF
                  FFFFFFCDCDCDE7E7E7FFFFFFFFFFFFE7E7E7C5C5C5FFFFFFFEFEFEFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFEFEFEFDFDFDFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFF
                  FFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
                  FDFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFDFDFDF6767671A1A1A0000000000000E0E0E515151B8B8B8FFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C1A1A1A000000000000363636B0B0B0
                  FFFFFFFFFFFFFFFFFFFFFFFFD9D9D94D4D4D0606060000000303033B3B3BB7B7
                  B7FFFFFFFFFFFFFFFFFFF4F4F43C3C3C000000000000B7B7B7FFFFFFE0E0E044
                  4444000000000000393939C3C3C3FFFFFF5151519D9D9DFFFFFF474747B3B3B3
                  FFFFFFFFFFFFFFFFFFFFFFFFE1E1E1464646C5C5C5FFFFFFFFFFFFF5F5F56565
                  650E0E0E000000000000373737B2B2B2FFFFFFFFFFFFA3A3A34A4A4AFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCD393939000000000000434343D3D3D3
                  FFFFFF494949B2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF545454868686FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C4F4F4FCBCBCBFF
                  FFFFFFFFFFFAFAFA6B6B6B0808080000000606064E4E4ED4D4D4FFFFFFFFFFFF
                  FFFFFF4D4D4DA0A0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F94040
                  40000000000000B1B1B1FFFFFFFFFFFFEEEEEE6161610C0C0C00000003030338
                  3838B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3B3B3B
                  000000353535A4A4A4D5D5D5E0E0E0B9B9B96D6D6D000000000000D1D1D1FFFF
                  FFFFFFFFF9F9F9000000000000BEBEBEF9F9F9F6F6F6A5A5A50000001E1E1EFF
                  FFFFFFFFFFB2B2B2000000787878F9F9F9FFFFFFEFEFEF8D8D8D0000007F7F7F
                  FFFFFFFFFFFF5C5C5C000000EBEBEBDCDCDCD0D0D0DEDEDE000000323232E6E6
                  E6FCFCFCD4D4D4424242060606000000C2C2C2FFFFFF000000878787FFFFFFFF
                  FFFFFFFFFFFFFFFFD1D1D1000000A1A1A1FFFFFFBDBDBD000000444444E5E5E5
                  FFFFFFFBFBFB9F9F9F000000353535FFFFFF6A6A6A000000FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFB8B8B80000004B4B4BEDEDEDFAFAFACBCBCB2B2B2B06060600
                  0000DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003A3A3AFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000000000ACACACFFFFFFCBCB
                  CB0000002F2F2FD6D6D6FBFBFBEEEEEE7D7D7D0000005F5F5FFFFFFFFFFFFF00
                  0000676767FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF696969000000E8E8E8
                  E1E1E1C7C7C7FFFFFFB5B5B50000004C4C4CE9E9E9FFFFFFF8F8F89C9C9C0000
                  00434343FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF040404000000D2D2D2FF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF313131000000D4D4D4FFFFFF
                  232323000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9000000565656FFFF
                  FF8A8A8A7D7D7DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000292929FFFFFFFF
                  FFFF555555000000FFFFFFFFFFFFFFFFFF989898000000D4D4D4FFFFFFFFFFFF
                  FFFFFFFFFFFF717171000000D0D0D0FFFFFF0000008C8C8CFFFFFFFFFFFFFFFF
                  FFFFFFFFD4D4D4000000A7A7A7FFFFFF0000004F4F4FFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFF3F3F3B2B2B2FFFFFF717171000000FFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF737373000000F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF4D4D4D000000EDED
                  EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF5050500000005C5C5C000000B2B2B2FFFFFF0000003A3A3A
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1000000A7A7A7FFFFFF0000006E6E
                  6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF626262000000FFFFFFFFFFFFFF
                  FFFFFFFFFF0000005D5D5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB0B0B0
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFEFEFEFFFFFF5B5B5B000000C4C4C4FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFF1B1B1B656565FFFFFF0000007E
                  7E7EFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFF353535000000FFFFFFFFFFFF
                  FFFFFF9090905C5C5C464646161616000000080808DEDEDEFFFFFFFFFFFF5757
                  57000000FFFFFFFFFFFFFFFFFFFFFFFF1818180000005F5F5F9B9B9BC5C5C5FF
                  FFFFC4C4C4000000D0D0D0FFFFFF0000008D8D8DFFFFFFFFFFFFFFFFFFFFFFFF
                  D4D4D4000000ADADADD4D4D40000006767679393938B8B8B8B8B8B8B8B8B8C8C
                  8C9999999B9B9BFFFFFF747474000000FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFF5
                  F5F50000000000006B6B6B9F9F9FCBCBCBFFFFFF9D9D9D000000EDEDEDFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFDFDFDFFFFFFFFFF
                  FF393939000000B0B0B0E0E0E0000000B8B8B8C3C3C3000000C5C5C5FFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004B4B4BFFFFFF0000006F6F6FFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFBEBE
                  BE0000007171719191918B8B8B8B8B8B8B8B8B8B8B8B9999999A9A9AF6F6F6FF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFF000000070707FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000005A5A5AFFFF
                  FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF0000001E1E1EFFFFFFC7C7C700000011
                  1111787878979797C1C1C1F3F3F3FFFFFFFFFFFFFEFEFEFFFFFF575757000000
                  FFFFFFFFFFFFFDFDFDFFFFFFFFFFFFE9E9E99797977777775858583030301313
                  13000000CFCFCFFFFFFF0000007A7A7AFFFFFFFFFFFFFFFFFFFFFFFFD9D9D900
                  0000A7A7A7EEEEEE0000004E4E4E8787877B7B7B7B7B7B7B7B7B8787873B3B3B
                  000000F4F4F4767676000000FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFF
                  FFDEDEDE939393737373535353262626111111000000ECECECFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFFFFFF232323000000
                  BFBFBFFFFFFFC8C8C8000000B7B7B7D9D9D9000000A9A9A9FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFF000000727272FFFFFF0000005E5E5EFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFD4D4D4000000
                  5858588686867B7B7B7B7B7B7B7B7B878787363636000000EFEFEFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFF4F4F4000000474747FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF707070000000CFCFCFFFFFFF
                  FFFFFFFFFFFFFFFFFF7A7A7A000000C5C5C5FFFFFF686868000000FFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFF000000727272FFFFFFFFFFFF606060000000FFFFFFFF
                  FFFFFFFFFFFFFFFF222222848484FFFFFFFFFFFFFFFFFFFFFFFFAFAFAF000000
                  D5D5D5FEFEFE000000000000E1E1E1FFFFFFFFFFFFFFFFFFA3A3A3000000C6C6
                  C6FFFFFF323232000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF161616080808FF
                  FFFF7272720000002C2C2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0CA5A5A5
                  FFFFFFFFFFFFFFFFFFFFFFFF878787000000F4F4F4FFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF000000444444FFFFFFFFFFFFFFFFFF0D0D0D000000D5D5D5FFFFFFFF
                  FFFFCACACA000000B1B1B1FFFFFF1D1D1D000000FEFEFEFFFFFFFFFFFFFFFFFF
                  FFFFFF303030000000F9F9F9FFFFFF000000000000B8B8B8FFFFFFFFFFFFFFFF
                  FFFEFEFEFFFFFF6F6F6F000000FFFFFFFFFFFFFFFFFFFFFFFF1D1D1D000000FF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0C141414FFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF0000001A1A1AFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF7A7A7A00000036363682828278
                  7878070707000000B6B6B6FFFFFFFFFFFFF8F8F80E0E0E0000005D5D5D7A7A7A
                  616161000000222222FFFFFFFFFFFF0000000404040000001313130000009999
                  99FFFFFFC1C1C10000002A2A2A787878818181535353000000353535FFFFFFF6
                  F6F60000006464640606063232327676765252520000005C5C5CFFFFFFFFFFFF
                  FFFFFF4242420000005A5A5A878787666666000000131313F9F9F9FFFFFF6363
                  63000000C0C0C00000002C2C2C515151C6C6C6FFFFFFA3A3A30000003C3C3C7A
                  7A7A7F7F7F4949490000005A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  000000444444FFFFFFFFFFFF000000000000E7E7E7FFFFFFFFFFFFFFFFFFCACA
                  CA000000B0B0B0FFFFFFFFFFFF4040400000005252528686866B6B6B00000002
                  0202E8E8E8FFFFFFFFFFFF000000686868444444000000474747626262FFFFFF
                  000000030303000000131313000000939393FFFFFFFFFFFF3A3A3A0000005E5E
                  5E8787876262620000001F1F1FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF55
                  5555000000D8D8D8FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFD7D7D7E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFB0B0B0878787888888BFBF
                  BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD9F9F9F959595A1A1A1E2
                  E2E2FFFFFFFFFFFFFFFFFFFAFAFA5151510C0C0CFFFFFFEDEDEDF8F8F8FFFFFF
                  FFFFFFFFFFFFB8B8B89393938D8D8DA0A0A0DDDDDDFFFFFFFFFFFFFFFFFFEEEE
                  EEFEFEFEFFFFFFC5C5C59494949D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEEEEEEA0A0A08888889B9B9BDEDEDEFFFFFFFFFFFFFFFFFFF6F6F6EEEEEE
                  FFFFFFFFFFFFC7C7C7909090DDDDDDFFFFFFFFFFFFFFFFFFB0B0B09191918F8F
                  8FA2A2A2EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004D
                  4D4DFFFFFF000000000000F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFCACACA000000
                  B0B0B0FFFFFFFFFFFFFFFFFFF0F0F0A3A3A37F7F7F929292D4D4D4FFFFFFFFFF
                  FFFFFFFFFFFFFFEFEFEFF7F7F7FFFFFFF1F1F1A5A5A59A9A9AFFFFFFFAFAFA5F
                  5F5F000000FFFFFFEEEEEEF5F5F5FFFFFFFFFFFFFFFFFFEAEAEA9E9E9E898989
                  9D9D9DE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
                  00000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A00
                  0000D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                  FEFFFFFFFEFEFEFFFFFF474747000000FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFF
                  FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFDFDFDFEFEFEFFFFFFFDFDFDFE
                  FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003030300000
                  00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACACA000000B0B0B0FF
                  FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FEFEFEFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF5757570000
                  00FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A
                  000000515151C1C1C1EEEEEEF6F6F6DDDDDD8C8C8C000000000000BCBCBCFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFDADADACACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000001A1A1AFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C6000000ACACACFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDC6C6C6FFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E169
                  6969161616000000000000000000333333999999FFFFFFFFFFFFFEFEFEFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6464646C6C6CFFFFFFFFFFFFFDFDFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF545454CECECEFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFE
                  FEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFFFFFEFFFFFAFEFFFEFEFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                  FEFCFFFEFEFFFEFDFFFEFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFEFFFFFDFFFFFFFFFEFFFFFEFFFCFFFEFEFFFEFFFFFEFF
                  FEFFFFFEFFFCFFFFFDFFFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFE
                  FFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                  FEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFAFEFF
                  FFFDFFFEFEFFFDFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  F9FFFFFAFFFEFCFEFEFDFFFCFEFFFEFEFEFFFDFEFFFEFEFFFDFEFFFCFFFFFFFE
                  FFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFE
                  FEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFFFFFF
                  FEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFCFFFE
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFEFCFDFBFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFEFC
                  FEFFFEFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFEFDFFFFFB
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9F765B994
                  71C19F71C39C71C29C71C29C71C39C71C29C71C29C70C39C65C19782BCA2FFFF
                  FFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFBFFFEE8F3EECDE8DAB5E0CD9FD4C18DCFB280C9A671C19E70C29D71
                  C39C71C39C71C29C71C39C71C29C71C29C71C39C71C29C71C39C71C29C71C29C
                  71C39C71C29C71C39C71C39C71C29C6EC29A7DC2A4FFFFFFFDFFFFFEFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF7F10EA15A28AA6A26
                  AA6B27A96B24AA6B25AA6B26AA6B28A96B1EA96829A46BEEFDF5FFFFFFFBFFFF
                  FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFEFFFFFBFFFEFEFDFEFFFEFEFEFFFEFFFDFEFFFFFEFDFEFFFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F3EBB8E0CE8BD1B06BC1984AB08132AA70
                  23A1641EA3641EA66422A86523A86526A86728AA692AAA6928AA6B26AA6B26AA
                  6B26A96B26AA6B26AA6B26AA6B26AA6B26A96B26AA6B26AA6B26AA6B26AA6B26
                  A96B26AA6B26AA6B26A96B22A9683AAA76FFFFFFFEFFFEFEFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7132AE7232AE
                  7230AE7233AE7230AD712AAF7027A16AEAF9F3FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFEFFFFFEFFFEFEFFFEFFFCFEFFFFFFFFFFFFFFFFFFFFFFE9F4F4B2DC
                  CC7CC5A349B37F27A5691FA26220A86225A96828AB6C2BAC6E2FAD7030AE7131
                  AE7131AD7231AD7231AE7231AE7231AD7231AE7231AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AE7233AD72
                  33AD7131AE7219A362BFE4D8FFFFFFFEFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFA
                  FEFEFEFEFFFFFFFFFFFFFFF7FFFABBDFCF6DC39E38A7741DA46422A96224AB69
                  28AD6F2CAE702CAD712BAD7331AD7131AD7132AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD722EAE712E
                  A96B71C199FFFFFFFAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FF
                  FCABD8C555B78823A1671AA7662CA96B31AC7034AC7231AE7232AE7132AE7032
                  AD7233AD7234AD7132AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                  AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD7229AE7025A267FCFF
                  FFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFEFEFFFC
                  FFFFFFFEFEFEFFFEFFFEFDFDFEFEFFFFFFFFFFFFCDEADD68BE901F9F641BA767
                  2AAB6D31AE7130AD7230AE7231AE7131AE7133AD7132AD7231AD7232AD7131AF
                  702FAF7031AD7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                  7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEDF8F31AA36533AE7131AE7231AD7234AD7125AA668CCAB1FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFEFFFFFFFF
                  FEFDFFFEFFFFFFFFFFFFB5DECA3EAA7922A3602AA96D2BAF7236AC7331AE7131
                  AE7132AE7132AD7132AE7231AE7231AE7231AE7232AE7131AE7232AD7232AD72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                  FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                  F31AA36533AE7131AE7232AD722FAE711BA260F2FBFBFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFDFEFFFFFFFFFFFF
                  BEE1D339AA751DA6642EAC6F33AC7230AE732BAD722EAD7332AE7132AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                  33AE7131AE7231AE712BAB6E52B587FFFFFFFFFDFEFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F7F757B2891BA66024
                  AE6B31AF6F35AE6F30AE7131AD7332AC7331AD7332AE7132AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                  AE7231AD7125A96995D3B5FFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FEFFFFFAFEFFFEFEFFFFFBFEFFFFFFA7DAC522A16328AB6C32AE7131AD7232AE
                  7131AE7232AD7231AE7132AE7132AD7131AE7132AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AD
                  711EA766C4E5D4FFFFFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF
                  FEFFFEFFFFFFFFFF7AC0A316A45E2AAF6E32AD7231AD7232AE7231AD7232AD72
                  32AE7132AE7131AE7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE711AA564
                  E1F0EAFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
                  FF68BD951CA66332AE7133AE6F31AD7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7119A463E6F4F1FF
                  FFFFFEFFFDFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFCFFFFFFFFFF84C8AC18A663
                  35AE712EAE722FAF7032AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                  AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AD721CA661E1F0EEFFFFFFFEFF
                  FCFFFFFFFFFFFFFDFFFFFCFFFFFCFFFFFFFFFFCDE9D915A15F31AE7132AD7232
                  AD7132AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                  7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEDF8F31AA36533AE7131AE7232AD721DA863C8E3D7FFFFFFFDFFFDFFFFFF
                  FFFFFFFFFFFFFDFFFFFFFEFFFFFFFF3BAB772AAC6F33AF6D31AD7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                  FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                  F31AA36533AE7131AE7232AD7223A96897D3B7FFFFFFFEFFFEFFFFFFFFFFFFFD
                  FFFFF9FFFFFFFFFFCAE9DF1BA66032AD7233AE6F31AD7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                  33AE7131AE7231AE712CAC6E53B587FFFFFFFEFEFEFEFFFFFEFFFFFDFFFEFFFF
                  FEFFFFFF80C8AC28A96932AE7137AB7431AE7132AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                  AE7231AE7132AD721BA161F1F9F7FFFDFFFEFFFFFBFFFFFCFFFEFFFFFDFFFFFF
                  61BC9029AB6C31AE7231AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE
                  7231AD7024A96B7ACAA3FFFFFFFFFFFFFCFFFFFBFDFFFBFFFFFFFFFF5ABA8E2A
                  AB6D32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD72
                  2FAD721AA264E9F9F0FFFFFFFFFEFFFEFDFFFCFFFEFFFFFF75C49D27AA6B32AE
                  7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD7230AD722C
                  AA6D54B084FFFFFFFFFDFFFFFDFFFFFFFCFFFFFFABD9C21EA8682EAD7232AD72
                  31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                  AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7232AE7132AE7131AE7120A7
                  6880C7A7FFFFFFFFFFFCFFFEFEFFFEFFF7FFFD1C9F6534AE6E31AD7232AE7132
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                  7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEDF8F31AA36533AE7131AE7232AE7232AE7232AE7231AE7132AE711CA165
                  98D2B6FFFFFFFCFEFFFEFDFFFFFFFF7FC5A61EAC6432AF7132AD7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                  FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                  F31AA36533AE7131AE7232AE7232AE7232AE7232AE712FAC742EAE7116A4638E
                  CDB2FFFFFFFFFFFFFFFFFDFEFFFF34A47128AB6E32AE7131AE7132AD7232AD72
                  31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                  33AE7131AE7232AE7232AE7232AE7232AE7131AD7234AD7231AD721BA6606ABD
                  97FFFFFFFFFFFFFFFFFFE8F8EE23A26A27AC7035AB7431AF7031AE7034AD7231
                  AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                  AE7232AE7232AE7232AE7232AE7232AE7131AE7132AE7130AD7125A9663AA977
                  D4EEE6FFFFFFFFFFFFE7F7EF36A5741DAC6536AC7233AC7431AD7232AE7131AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F319A36433AE7131AE7131AE
                  7131AE7131AE7131AE7131AE7131AE7131AD7231AF7038AC7429AB7116A45D7A
                  C3A0FDFFFFFFFFFFFFFFFF69BC9612A2622BAE6C33AF6C2EAD7332AE7131AE71
                  31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                  7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131
                  AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE71
                  31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                  7131AE7131AE712CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F1069D5820A6651EA6651EA6651EA665
                  1EA6651EA6651EA6651EA6651EA66522A5651CA76424A66326A66213A25E0F9C
                  5883C9A8F3FCF6FFFFFFC8EADD45AB7A15A65F1FA6661EA6651EA6651EA6651E
                  A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                  1EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6
                  651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651E
                  A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                  1EA6651AA56234A770FFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFF9FDFDBEE1D5C3E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                  E4DAC5E4DAC5E4DAC5E4DAC6E4DAC6E4D9C5E4DAC5E4DAC5E3DBC5E5D9B6E0D1
                  BDD9D3FDFEFFFFFFFFFFFFFFCDE7DFC0E3D8C6E4DAC5E4DAC5E4DAC5E4DAC5E4
                  DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                  E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DA
                  C5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4
                  DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                  E3D9CAE4DCFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFDFEFEFCFEFEFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFBFFFFFEFFFFFFFF
                  FFFEFFFFFCFFFFFCFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFF
                  FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                  FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                  FDFFFFFCFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                  FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                  FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                  FDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                  FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFEFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
                mmHeight = 19000
                mmLeft = 2117
                mmTop = 9790
                mmWidth = 42000
                BandType = 1
              end
            end
            object ppColumnHeaderBand5: TppColumnHeaderBand
              mmBottomOffset = 0
              mmHeight = 7938
              mmPrintPosition = 0
              object ppLine25: TppLine
                UserName = 'Line19'
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Weight = 0.750000000000000000
                mmHeight = 3969
                mmLeft = 1058
                mmTop = 529
                mmWidth = 92604
                BandType = 2
              end
              object ppLabel72: TppLabel
                UserName = 'Label41'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'N'#186' de Matr'#237'cula'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = [fsBold]
                TextAlignment = taCentered
                Transparent = True
                WordWrap = True
                mmHeight = 5821
                mmLeft = 2646
                mmTop = 1323
                mmWidth = 10848
                BandType = 2
              end
              object ppLabel73: TppLabel
                UserName = 'Label42'
                HyperlinkColor = clBlue
                AutoSize = False
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = '  Fecha     Hora'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 2910
                mmLeft = 15875
                mmTop = 3440
                mmWidth = 22225
                BandType = 2
              end
              object ppLabel74: TppLabel
                UserName = 'Label44'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'P'#243'rtico'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = [fsBold]
                TextAlignment = taCentered
                Transparent = True
                mmHeight = 2910
                mmLeft = 46567
                mmTop = 3440
                mmWidth = 8467
                BandType = 2
              end
              object ppLabel75: TppLabel
                UserName = 'Label45'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Tipo Tarifa'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = [fsBold]
                TextAlignment = taCentered
                Transparent = True
                mmHeight = 2910
                mmLeft = 64823
                mmTop = 3440
                mmWidth = 12435
                BandType = 2
              end
              object ppLabel76: TppLabel
                UserName = 'Label47'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Importe'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = [fsBold]
                TextAlignment = taCentered
                Transparent = True
                WordWrap = True
                mmHeight = 2910
                mmLeft = 78581
                mmTop = 3440
                mmWidth = 14023
                BandType = 2
              end
              object ppLine26: TppLine
                UserName = 'Line20'
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Position = lpBottom
                Weight = 0.750000000000000000
                mmHeight = 2910
                mmLeft = 1058
                mmTop = 5027
                mmWidth = 93927
                BandType = 2
              end
            end
            object ppDetailBand7: TppDetailBand
              Background1.Brush.Style = bsClear
              Background1.Gradient.EndColor = clWhite
              Background1.Gradient.StartColor = clWhite
              Background1.Gradient.Style = gsNone
              Background2.Brush.Style = bsClear
              Background2.Gradient.EndColor = clWhite
              Background2.Gradient.StartColor = clWhite
              Background2.Gradient.Style = gsNone
              mmBottomOffset = 0
              mmHeight = 3440
              mmPrintPosition = 0
              object ppDBText52: TppDBText
                UserName = 'DBText34'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Color = clWindowText
                DataField = 'DescriTipoHorario'
                DataPipeline = plTransitosNoFacturados
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taCentered
                Transparent = True
                DataPipelineName = 'plTransitosNoFacturados'
                mmHeight = 3175
                mmLeft = 63236
                mmTop = 265
                mmWidth = 15610
                BandType = 4
              end
              object ppDBText53: TppDBText
                UserName = 'DBText35'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Color = clNavy
                DataField = 'DescriPtoCobro'
                DataPipeline = plTransitosNoFacturados
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taCentered
                Transparent = True
                DataPipelineName = 'plTransitosNoFacturados'
                mmHeight = 2910
                mmLeft = 38629
                mmTop = 529
                mmWidth = 24342
                BandType = 4
              end
              object ppDBText54: TppDBText
                UserName = 'DBText36'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'DescriImporte'
                DataPipeline = plTransitosNoFacturados
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taRightJustified
                Transparent = True
                DataPipelineName = 'plTransitosNoFacturados'
                mmHeight = 2910
                mmLeft = 79375
                mmTop = 265
                mmWidth = 14023
                BandType = 4
              end
              object ppDBText55: TppDBText
                UserName = 'DBText101'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'FechaHora'
                DataPipeline = plTransitosNoFacturados
                DisplayFormat = 'dd'#39'/'#39'mm'#39'/'#39'yyyy  hh'#39':'#39'nn'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = []
                ParentDataPipeline = False
                Transparent = True
                DataPipelineName = 'plTransitosNoFacturados'
                mmHeight = 2910
                mmLeft = 15875
                mmTop = 265
                mmWidth = 21696
                BandType = 4
              end
              object ppDBText56: TppDBText
                UserName = 'DBText43'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DataField = 'Patente'
                DataPipeline = plTransitosNoFacturados
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Name = 'Arial'
                Font.Size = 7
                Font.Style = []
                ParentDataPipeline = False
                TextAlignment = taCentered
                Transparent = True
                DataPipelineName = 'plTransitosNoFacturados'
                mmHeight = 2879
                mmLeft = 265
                mmTop = 265
                mmWidth = 15240
                BandType = 4
              end
            end
            object ppColumnFooterBand5: TppColumnFooterBand
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
            object ppFooterBand6: TppFooterBand
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
            object ppGroup8: TppGroup
              BreakName = 'CodigoConvenio'
              DataPipeline = plTransitosNoFacturados
              GroupFileSettings.NewFile = False
              GroupFileSettings.EmailFile = False
              KeepTogether = True
              OutlineSettings.CreateNode = True
              ReprintOnSubsequentColumn = False
              ReprintOnSubsequentPage = False
              StartOnOddPage = False
              UserName = 'Group5'
              mmNewColumnThreshold = 0
              mmNewPageThreshold = 0
              DataPipelineName = 'plTransitosNoFacturados'
              NewFile = False
              object ppGroupHeaderBand8: TppGroupHeaderBand
                mmBottomOffset = 0
                mmHeight = 0
                mmPrintPosition = 0
              end
              object ppGroupFooterBand3: TppGroupFooterBand
                HideWhenOneDetail = False
                mmBottomOffset = 0
                mmHeight = 8467
                mmPrintPosition = 0
                object ppShape9: TppShape
                  UserName = 'Shape2'
                  Brush.Color = clBlack
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 5821
                  mmLeft = 54240
                  mmTop = 1588
                  mmWidth = 42333
                  BandType = 5
                  GroupNo = 0
                end
                object ppShape8: TppShape
                  UserName = 'Shape1'
                  Brush.Color = clBlack
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 5821
                  mmLeft = 2381
                  mmTop = 1588
                  mmWidth = 52123
                  BandType = 5
                  GroupNo = 0
                end
                object lblTransitosNoFacturadosTotal: TppLabel
                  UserName = 'Label1'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Caption = 'TOTAL'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWhite
                  Font.Name = 'Arial'
                  Font.Size = 10
                  Font.Style = [fsBold]
                  Transparent = True
                  mmHeight = 4191
                  mmLeft = 30163
                  mmTop = 2117
                  mmWidth = 11726
                  BandType = 5
                  GroupNo = 0
                end
                object dbcTransitosnoFacturadosTotal: TppDBCalc
                  UserName = 'dbcTransitosnoFacturadosTotal'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  DataField = 'Importe'
                  DataPipeline = plTransitosNoFacturados
                  DisplayFormat = '$.00'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWhite
                  Font.Name = 'Arial'
                  Font.Size = 10
                  Font.Style = [fsBold]
                  TextAlignment = taRightJustified
                  Transparent = True
                  DataPipelineName = 'plTransitosNoFacturados'
                  mmHeight = 4233
                  mmLeft = 61648
                  mmTop = 2117
                  mmWidth = 32279
                  BandType = 5
                  GroupNo = 0
                end
              end
            end
            object ppGroup1: TppGroup
              BreakName = 'Patente'
              DataPipeline = plTransitosNoFacturados
              GroupFileSettings.NewFile = False
              GroupFileSettings.EmailFile = False
              KeepTogether = True
              OutlineSettings.CreateNode = True
              StartOnOddPage = False
              UserName = 'Group1'
              mmNewColumnThreshold = 0
              mmNewPageThreshold = 0
              DataPipelineName = 'plTransitosNoFacturados'
              NewFile = False
              object ppGroupHeaderBand1: TppGroupHeaderBand
                mmBottomOffset = 0
                mmHeight = 0
                mmPrintPosition = 0
              end
              object ppGroupFooterBand1: TppGroupFooterBand
                HideWhenOneDetail = False
                mmBottomOffset = 0
                mmHeight = 7673
                mmPrintPosition = 0
                object ppShape41: TppShape
                  UserName = 'Shape9'
                  Brush.Color = clGray
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 5821
                  mmLeft = 2381
                  mmTop = 794
                  mmWidth = 52123
                  BandType = 5
                  GroupNo = 1
                end
                object ppLabel80: TppLabel
                  UserName = 'Label48'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  Caption = 'TOTAL PATENTE'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWhite
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = [fsBold]
                  Transparent = True
                  mmHeight = 3440
                  mmLeft = 29898
                  mmTop = 2117
                  mmWidth = 23019
                  BandType = 5
                  GroupNo = 1
                end
                object ppShape40: TppShape
                  UserName = 'Shape8'
                  Gradient.EndColor = clWhite
                  Gradient.StartColor = clWhite
                  Gradient.Style = gsNone
                  mmHeight = 5821
                  mmLeft = 54240
                  mmTop = 794
                  mmWidth = 42333
                  BandType = 5
                  GroupNo = 1
                end
                object ppDBCalc8: TppDBCalc
                  UserName = 'DBCalc3'
                  HyperlinkColor = clBlue
                  Border.BorderPositions = []
                  Border.Color = clBlack
                  Border.Style = psSolid
                  Border.Visible = False
                  Border.Weight = 1.000000000000000000
                  DataField = 'Importe'
                  DataPipeline = plTransitosNoFacturados
                  DisplayFormat = '$.00'
                  Ellipsis = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Name = 'Arial'
                  Font.Size = 8
                  Font.Style = [fsBold]
                  ParentDataPipeline = False
                  ResetGroup = ppGroup1
                  TextAlignment = taRightJustified
                  Transparent = True
                  DataPipelineName = 'plTransitosNoFacturados'
                  mmHeight = 3440
                  mmLeft = 61648
                  mmTop = 2117
                  mmWidth = 32279
                  BandType = 5
                  GroupNo = 1
                end
              end
            end
          end
        end
        object srTransitosNoFacturadosNoHay: TppSubReport
          UserName = 'srTransitosNoFacturadosNoHay'
          ExpandAll = False
          NewPrintJob = False
          OutlineSettings.CreateNode = True
          ShiftRelativeTo = pbSaltoTransitosNoFacturados
          TraverseAllData = False
          mmHeight = 5027
          mmLeft = 0
          mmTop = 10583
          mmWidth = 194000
          BandType = 5
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          mmMinHeight = 0
          object ppChildReport1: TppChildReport
            PrinterSetup.BinName = 'Default'
            PrinterSetup.DocumentName = 'Caratula (Cartpeta Legales)'
            PrinterSetup.PaperName = 'Letter'
            PrinterSetup.PrinterName = 'Default'
            PrinterSetup.SaveDeviceSettings = False
            PrinterSetup.mmMarginBottom = 10000
            PrinterSetup.mmMarginLeft = 12000
            PrinterSetup.mmMarginRight = 10000
            PrinterSetup.mmMarginTop = 10000
            PrinterSetup.mmPaperHeight = 279000
            PrinterSetup.mmPaperWidth = 216000
            PrinterSetup.PaperSize = 1
            Units = utMillimeters
            Version = '12.04'
            mmColumnWidth = 0
            object ppTitleBand5: TppTitleBand
              mmBottomOffset = 0
              mmHeight = 50800
              mmPrintPosition = 0
              object ppImage2: TppImage
                UserName = 'Image1'
                AlignHorizontal = ahCenter
                AlignVertical = avCenter
                AutoSize = True
                MaintainAspectRatio = True
                Stretch = True
                Anchors = [atLeft, atBottom]
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Picture.Data = {
                  07544269746D6170F2870000424DF28700000000000036000000280000009E00
                  0000490000000100180000000000BC870000C40E0000C40E0000000000000000
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
                  FEFEFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE
                  FFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171712828283A3A3A
                  E6E6E6FFFFFFFFFFFFA8A8A8272727282828818181FFFFFFFFFFFF7B7B7BF0F0
                  F0FFFFFFE4E4E4515151FFFFFFFFFFFF9A9A9A2727272F2F2FC3C3C3FFFFFFFC
                  FCFC3F3F3F3F3F3F3F3F3F2E2E2EF4F4F4FFFFFF5F5F5F313131323232CACACA
                  FFFFFFC5C5C5A1A1A1FFFFFFFFFFFF5B5B5B262626323232D0D0D0FFFFFFF9F9
                  F9666666FFFFFFFFFFFF878787ACACACE8E8E8787878FFFFFFFFFFFFE9E9E968
                  6868FFFFFF535353FFFFFFFFFFFFDFDFDF696969FFFFFF939393D4D4D4C2C2C2
                  9F9F9FFFFFFFFFFFFFC8C8C88C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF616161757575FFFFFFF9F9F9000000FF
                  FFFFB4B4B4222222FFFFFFFFFFFF6D6D6D696969FFFFFF333333E9E9E9FFFFFF
                  111111060606FFFFFFA6A6A6313131FFFFFFFFFFFF000000DDDDDDF8F8F81818
                  18FFFFFFFFFFFFFFFFFFFFFFFF6C6C6C868686FFFFFFFFFFFF000000FFFFFFA7
                  A7A7707070FFFFFF262626A7A7A7FFFFFFFFFFFF000000E8E8E8FBFBFB0F0F0F
                  FFFFFFCCCCCC000000838383FFFFFF000000E8E8E8F0F0F05858589D9D9DFFFF
                  FF000000FFFFFFFFFFFFC1C1C15C5C5CFFFFFF5B5B5BB5B5B5FFFFFF000000F9
                  F9F9F4F4F4252525D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE1E1E1FFFFFF4242
                  42D2D2D2FFFFFFFFFFFFFFFFFF000000FFFFFF3A3A3AFFFFFF4D4D4DC3C3C313
                  1313FFFFFF373737DADADAFFFFFFFFFFFFEFEFEFECECECF9F9F9030303ABABAB
                  9C9C9CAAAAAAFFFFFFFFFFFFE9E9E99191913D3D3D323232FFFFFFAAAAAA7575
                  75FFFFFF000000FFFFFFFFFFFFFFFFFF9D9D9D787878FFFFFF1B1B1BFFFFFF00
                  0000BCBCBC787878FFFFFF9D9D9D202020868686000000FFFFFFFFFFFF000000
                  636363616161111111C0C0C0FFFFFF616161B8B8B8FFFFFF6767674242426A6A
                  6A0C0C0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFEFEFFFFFFF535353B9B9B9
                  FFFFFFFFFFFFFFFFFF000000FFFFFF464646959595424242FFFFFF040404FFFF
                  FF464646CACACAFFFFFFFFFFFFFBFBFBF8F8F8F8F8F80E0E0ED4D4D4C3C3C3CE
                  CECEFFFFFFC3C3C3111111ADADADF3F3F3F8F8F8FFFFFFAAAAAA757575FFFFFF
                  000000FFFFFFFFFFFFFFFFFF9393937E7E7EFFFFFF323232343434D6D6D6BBBB
                  BB767676FFFFFFFFFFFF1B1B1BC6C6C66A6A6AFFFFFFFFFFFF000000FFFFFFFC
                  FCFC959595707070FFFFFF616161B7B7B7FFFFFFF2F2F23E3E3E909090A1A1A1
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFA9A9A9151515DBDBDBB5B5B5000000FFFFFFECECEC000000BABABAD4
                  D4D41B1B1B959595FFFFFF434343000000FFFFFFFFFFFF000000FFFFFFDCDCDC
                  000000CCCCCCCFCFCF000000E9E9E9F9F9F9000000B8B8B8ABABAB9F9F9FFFFF
                  FFC6C6C62E2E2EEBEBEB969696181818FFFFFFA3A3A36A6A6AFFFFFF7D7D7D34
                  3434D9D9D9B1B1B1000000FBFBFBFFFFFF000000777777FFFFFFAAAAAA6B6B6B
                  FFFFFFFFFFFF707070000000F7F7F7FFFFFFFFFFFF000000C8C8C8C0C0C05454
                  546D6D6DFFFFFF565656B1B1B1FFFFFFFFFFFF3C3C3C000000FFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
                  FFFFFFD3D3D3737373868686FFFFFFFFFFFFFFFFFFF8F8F87F7F7F757575D0D0
                  D0FFFFFFFFFFFFC5C5C5E7E7E7FFFFFFFFFFFFB5B5B5FFFFFFFFFFFFEEEEEE79
                  7979787878F0F0F0FFFFFFFEFEFEA6A6A6A0A0A0A0A0A0979797FFFFFFFFFFFF
                  CECECE707070959595FFFFFFFFFFFFE3E3E3D3D3D3FFFFFFFFFFFFC2C2C27171
                  71878787FFFFFFFFFFFFFEFEFEA4A4A4FFFFFFFFFFFFE5E5E5D4D4D4FFFFFFFF
                  FFFFF8F8F8B5B5B5FFFFFFFFFFFFFFFFFFB2B2B29E9E9E9C9C9CBFBFBFFFFFFF
                  FFFFFFCDCDCDE7E7E7FFFFFFFFFFFFE7E7E7C5C5C5FFFFFFFEFEFEFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFEFEFEFDFDFDFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFF
                  FFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
                  FDFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFDFDFDF6767671A1A1A0000000000000E0E0E515151B8B8B8FFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C1A1A1A000000000000363636B0B0B0
                  FFFFFFFFFFFFFFFFFFFFFFFFD9D9D94D4D4D0606060000000303033B3B3BB7B7
                  B7FFFFFFFFFFFFFFFFFFF4F4F43C3C3C000000000000B7B7B7FFFFFFE0E0E044
                  4444000000000000393939C3C3C3FFFFFF5151519D9D9DFFFFFF474747B3B3B3
                  FFFFFFFFFFFFFFFFFFFFFFFFE1E1E1464646C5C5C5FFFFFFFFFFFFF5F5F56565
                  650E0E0E000000000000373737B2B2B2FFFFFFFFFFFFA3A3A34A4A4AFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCD393939000000000000434343D3D3D3
                  FFFFFF494949B2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF545454868686FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C4F4F4FCBCBCBFF
                  FFFFFFFFFFFAFAFA6B6B6B0808080000000606064E4E4ED4D4D4FFFFFFFFFFFF
                  FFFFFF4D4D4DA0A0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F94040
                  40000000000000B1B1B1FFFFFFFFFFFFEEEEEE6161610C0C0C00000003030338
                  3838B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3B3B3B
                  000000353535A4A4A4D5D5D5E0E0E0B9B9B96D6D6D000000000000D1D1D1FFFF
                  FFFFFFFFF9F9F9000000000000BEBEBEF9F9F9F6F6F6A5A5A50000001E1E1EFF
                  FFFFFFFFFFB2B2B2000000787878F9F9F9FFFFFFEFEFEF8D8D8D0000007F7F7F
                  FFFFFFFFFFFF5C5C5C000000EBEBEBDCDCDCD0D0D0DEDEDE000000323232E6E6
                  E6FCFCFCD4D4D4424242060606000000C2C2C2FFFFFF000000878787FFFFFFFF
                  FFFFFFFFFFFFFFFFD1D1D1000000A1A1A1FFFFFFBDBDBD000000444444E5E5E5
                  FFFFFFFBFBFB9F9F9F000000353535FFFFFF6A6A6A000000FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFB8B8B80000004B4B4BEDEDEDFAFAFACBCBCB2B2B2B06060600
                  0000DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003A3A3AFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000000000ACACACFFFFFFCBCB
                  CB0000002F2F2FD6D6D6FBFBFBEEEEEE7D7D7D0000005F5F5FFFFFFFFFFFFF00
                  0000676767FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF696969000000E8E8E8
                  E1E1E1C7C7C7FFFFFFB5B5B50000004C4C4CE9E9E9FFFFFFF8F8F89C9C9C0000
                  00434343FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF040404000000D2D2D2FF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF313131000000D4D4D4FFFFFF
                  232323000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9000000565656FFFF
                  FF8A8A8A7D7D7DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000292929FFFFFFFF
                  FFFF555555000000FFFFFFFFFFFFFFFFFF989898000000D4D4D4FFFFFFFFFFFF
                  FFFFFFFFFFFF717171000000D0D0D0FFFFFF0000008C8C8CFFFFFFFFFFFFFFFF
                  FFFFFFFFD4D4D4000000A7A7A7FFFFFF0000004F4F4FFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFF3F3F3B2B2B2FFFFFF717171000000FFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF737373000000F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF4D4D4D000000EDED
                  EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF5050500000005C5C5C000000B2B2B2FFFFFF0000003A3A3A
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1000000A7A7A7FFFFFF0000006E6E
                  6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF626262000000FFFFFFFFFFFFFF
                  FFFFFFFFFF0000005D5D5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB0B0B0
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFEFEFEFFFFFF5B5B5B000000C4C4C4FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFF1B1B1B656565FFFFFF0000007E
                  7E7EFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFF353535000000FFFFFFFFFFFF
                  FFFFFF9090905C5C5C464646161616000000080808DEDEDEFFFFFFFFFFFF5757
                  57000000FFFFFFFFFFFFFFFFFFFFFFFF1818180000005F5F5F9B9B9BC5C5C5FF
                  FFFFC4C4C4000000D0D0D0FFFFFF0000008D8D8DFFFFFFFFFFFFFFFFFFFFFFFF
                  D4D4D4000000ADADADD4D4D40000006767679393938B8B8B8B8B8B8B8B8B8C8C
                  8C9999999B9B9BFFFFFF747474000000FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFF5
                  F5F50000000000006B6B6B9F9F9FCBCBCBFFFFFF9D9D9D000000EDEDEDFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFDFDFDFFFFFFFFFF
                  FF393939000000B0B0B0E0E0E0000000B8B8B8C3C3C3000000C5C5C5FFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004B4B4BFFFFFF0000006F6F6FFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFBEBE
                  BE0000007171719191918B8B8B8B8B8B8B8B8B8B8B8B9999999A9A9AF6F6F6FF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFF000000070707FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000005A5A5AFFFF
                  FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF0000001E1E1EFFFFFFC7C7C700000011
                  1111787878979797C1C1C1F3F3F3FFFFFFFFFFFFFEFEFEFFFFFF575757000000
                  FFFFFFFFFFFFFDFDFDFFFFFFFFFFFFE9E9E99797977777775858583030301313
                  13000000CFCFCFFFFFFF0000007A7A7AFFFFFFFFFFFFFFFFFFFFFFFFD9D9D900
                  0000A7A7A7EEEEEE0000004E4E4E8787877B7B7B7B7B7B7B7B7B8787873B3B3B
                  000000F4F4F4767676000000FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFF
                  FFDEDEDE939393737373535353262626111111000000ECECECFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFFFFFF232323000000
                  BFBFBFFFFFFFC8C8C8000000B7B7B7D9D9D9000000A9A9A9FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFF000000727272FFFFFF0000005E5E5EFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFD4D4D4000000
                  5858588686867B7B7B7B7B7B7B7B7B878787363636000000EFEFEFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFF4F4F4000000474747FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF707070000000CFCFCFFFFFFF
                  FFFFFFFFFFFFFFFFFF7A7A7A000000C5C5C5FFFFFF686868000000FFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFF000000727272FFFFFFFFFFFF606060000000FFFFFFFF
                  FFFFFFFFFFFFFFFF222222848484FFFFFFFFFFFFFFFFFFFFFFFFAFAFAF000000
                  D5D5D5FEFEFE000000000000E1E1E1FFFFFFFFFFFFFFFFFFA3A3A3000000C6C6
                  C6FFFFFF323232000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF161616080808FF
                  FFFF7272720000002C2C2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0CA5A5A5
                  FFFFFFFFFFFFFFFFFFFFFFFF878787000000F4F4F4FFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF000000444444FFFFFFFFFFFFFFFFFF0D0D0D000000D5D5D5FFFFFFFF
                  FFFFCACACA000000B1B1B1FFFFFF1D1D1D000000FEFEFEFFFFFFFFFFFFFFFFFF
                  FFFFFF303030000000F9F9F9FFFFFF000000000000B8B8B8FFFFFFFFFFFFFFFF
                  FFFEFEFEFFFFFF6F6F6F000000FFFFFFFFFFFFFFFFFFFFFFFF1D1D1D000000FF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0C141414FFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF0000001A1A1AFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF7A7A7A00000036363682828278
                  7878070707000000B6B6B6FFFFFFFFFFFFF8F8F80E0E0E0000005D5D5D7A7A7A
                  616161000000222222FFFFFFFFFFFF0000000404040000001313130000009999
                  99FFFFFFC1C1C10000002A2A2A787878818181535353000000353535FFFFFFF6
                  F6F60000006464640606063232327676765252520000005C5C5CFFFFFFFFFFFF
                  FFFFFF4242420000005A5A5A878787666666000000131313F9F9F9FFFFFF6363
                  63000000C0C0C00000002C2C2C515151C6C6C6FFFFFFA3A3A30000003C3C3C7A
                  7A7A7F7F7F4949490000005A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  000000444444FFFFFFFFFFFF000000000000E7E7E7FFFFFFFFFFFFFFFFFFCACA
                  CA000000B0B0B0FFFFFFFFFFFF4040400000005252528686866B6B6B00000002
                  0202E8E8E8FFFFFFFFFFFF000000686868444444000000474747626262FFFFFF
                  000000030303000000131313000000939393FFFFFFFFFFFF3A3A3A0000005E5E
                  5E8787876262620000001F1F1FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF55
                  5555000000D8D8D8FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFD7D7D7E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFB0B0B0878787888888BFBF
                  BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD9F9F9F959595A1A1A1E2
                  E2E2FFFFFFFFFFFFFFFFFFFAFAFA5151510C0C0CFFFFFFEDEDEDF8F8F8FFFFFF
                  FFFFFFFFFFFFB8B8B89393938D8D8DA0A0A0DDDDDDFFFFFFFFFFFFFFFFFFEEEE
                  EEFEFEFEFFFFFFC5C5C59494949D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEEEEEEA0A0A08888889B9B9BDEDEDEFFFFFFFFFFFFFFFFFFF6F6F6EEEEEE
                  FFFFFFFFFFFFC7C7C7909090DDDDDDFFFFFFFFFFFFFFFFFFB0B0B09191918F8F
                  8FA2A2A2EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004D
                  4D4DFFFFFF000000000000F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFCACACA000000
                  B0B0B0FFFFFFFFFFFFFFFFFFF0F0F0A3A3A37F7F7F929292D4D4D4FFFFFFFFFF
                  FFFFFFFFFFFFFFEFEFEFF7F7F7FFFFFFF1F1F1A5A5A59A9A9AFFFFFFFAFAFA5F
                  5F5F000000FFFFFFEEEEEEF5F5F5FFFFFFFFFFFFFFFFFFEAEAEA9E9E9E898989
                  9D9D9DE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
                  00000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A00
                  0000D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                  FEFFFFFFFEFEFEFFFFFF474747000000FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFF
                  FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFDFDFDFEFEFEFFFFFFFDFDFDFE
                  FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003030300000
                  00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACACA000000B0B0B0FF
                  FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FEFEFEFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF5757570000
                  00FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A
                  000000515151C1C1C1EEEEEEF6F6F6DDDDDD8C8C8C000000000000BCBCBCFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFDADADACACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000001A1A1AFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C6000000ACACACFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDC6C6C6FFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E169
                  6969161616000000000000000000333333999999FFFFFFFFFFFFFEFEFEFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6464646C6C6CFFFFFFFFFFFFFDFDFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF545454CECECEFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFE
                  FEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFFFFFEFFFFFAFEFFFEFEFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                  FEFCFFFEFEFFFEFDFFFEFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFEFFFFFDFFFFFFFFFEFFFFFEFFFCFFFEFEFFFEFFFFFEFF
                  FEFFFFFEFFFCFFFFFDFFFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFE
                  FFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                  FEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFAFEFF
                  FFFDFFFEFEFFFDFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  F9FFFFFAFFFEFCFEFEFDFFFCFEFFFEFEFEFFFDFEFFFEFEFFFDFEFFFCFFFFFFFE
                  FFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFE
                  FEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFFFFFF
                  FEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFCFFFE
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFEFCFDFBFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFEFC
                  FEFFFEFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFEFDFFFFFB
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9F765B994
                  71C19F71C39C71C29C71C29C71C39C71C29C71C29C70C39C65C19782BCA2FFFF
                  FFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFBFFFEE8F3EECDE8DAB5E0CD9FD4C18DCFB280C9A671C19E70C29D71
                  C39C71C39C71C29C71C39C71C29C71C29C71C39C71C29C71C39C71C29C71C29C
                  71C39C71C29C71C39C71C39C71C29C6EC29A7DC2A4FFFFFFFDFFFFFEFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF7F10EA15A28AA6A26
                  AA6B27A96B24AA6B25AA6B26AA6B28A96B1EA96829A46BEEFDF5FFFFFFFBFFFF
                  FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFEFFFFFBFFFEFEFDFEFFFEFEFEFFFEFFFDFEFFFFFEFDFEFFFDFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F3EBB8E0CE8BD1B06BC1984AB08132AA70
                  23A1641EA3641EA66422A86523A86526A86728AA692AAA6928AA6B26AA6B26AA
                  6B26A96B26AA6B26AA6B26AA6B26AA6B26A96B26AA6B26AA6B26AA6B26AA6B26
                  A96B26AA6B26AA6B26A96B22A9683AAA76FFFFFFFEFFFEFEFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7132AE7232AE
                  7230AE7233AE7230AD712AAF7027A16AEAF9F3FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFEFFFFFEFFFEFEFFFEFFFCFEFFFFFFFFFFFFFFFFFFFFFFE9F4F4B2DC
                  CC7CC5A349B37F27A5691FA26220A86225A96828AB6C2BAC6E2FAD7030AE7131
                  AE7131AD7231AD7231AE7231AE7231AD7231AE7231AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AE7233AD72
                  33AD7131AE7219A362BFE4D8FFFFFFFEFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFA
                  FEFEFEFEFFFFFFFFFFFFFFF7FFFABBDFCF6DC39E38A7741DA46422A96224AB69
                  28AD6F2CAE702CAD712BAD7331AD7131AD7132AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD722EAE712E
                  A96B71C199FFFFFFFAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FF
                  FCABD8C555B78823A1671AA7662CA96B31AC7034AC7231AE7232AE7132AE7032
                  AD7233AD7234AD7132AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                  AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD7229AE7025A267FCFF
                  FFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFEFEFFFC
                  FFFFFFFEFEFEFFFEFFFEFDFDFEFEFFFFFFFFFFFFCDEADD68BE901F9F641BA767
                  2AAB6D31AE7130AD7230AE7231AE7131AE7133AD7132AD7231AD7232AD7131AF
                  702FAF7031AD7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                  7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEDF8F31AA36533AE7131AE7231AD7234AD7125AA668CCAB1FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFEFFFFFFFF
                  FEFDFFFEFFFFFFFFFFFFB5DECA3EAA7922A3602AA96D2BAF7236AC7331AE7131
                  AE7132AE7132AD7132AE7231AE7231AE7231AE7232AE7131AE7232AD7232AD72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                  FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                  F31AA36533AE7131AE7232AD722FAE711BA260F2FBFBFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFDFEFFFFFFFFFFFF
                  BEE1D339AA751DA6642EAC6F33AC7230AE732BAD722EAD7332AE7132AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                  33AE7131AE7231AE712BAB6E52B587FFFFFFFFFDFEFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F7F757B2891BA66024
                  AE6B31AF6F35AE6F30AE7131AD7332AC7331AD7332AE7132AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                  AE7231AD7125A96995D3B5FFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FEFFFFFAFEFFFEFEFFFFFBFEFFFFFFA7DAC522A16328AB6C32AE7131AD7232AE
                  7131AE7232AD7231AE7132AE7132AD7131AE7132AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AD
                  711EA766C4E5D4FFFFFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF
                  FEFFFEFFFFFFFFFF7AC0A316A45E2AAF6E32AD7231AD7232AE7231AD7232AD72
                  32AE7132AE7131AE7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE711AA564
                  E1F0EAFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
                  FF68BD951CA66332AE7133AE6F31AD7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7119A463E6F4F1FF
                  FFFFFEFFFDFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFCFFFFFFFFFF84C8AC18A663
                  35AE712EAE722FAF7032AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                  AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AD721CA661E1F0EEFFFFFFFEFF
                  FCFFFFFFFFFFFFFDFFFFFCFFFFFCFFFFFFFFFFCDE9D915A15F31AE7132AD7232
                  AD7132AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                  7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEDF8F31AA36533AE7131AE7232AD721DA863C8E3D7FFFFFFFDFFFDFFFFFF
                  FFFFFFFFFFFFFDFFFFFFFEFFFFFFFF3BAB772AAC6F33AF6D31AD7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                  FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                  F31AA36533AE7131AE7232AD7223A96897D3B7FFFFFFFEFFFEFFFFFFFFFFFFFD
                  FFFFF9FFFFFFFFFFCAE9DF1BA66032AD7233AE6F31AD7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                  33AE7131AE7231AE712CAC6E53B587FFFFFFFEFEFEFEFFFFFEFFFFFDFFFEFFFF
                  FEFFFFFF80C8AC28A96932AE7137AB7431AE7132AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                  AE7231AE7132AD721BA161F1F9F7FFFDFFFEFFFFFBFFFFFCFFFEFFFFFDFFFFFF
                  61BC9029AB6C31AE7231AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE
                  7231AD7024A96B7ACAA3FFFFFFFFFFFFFCFFFFFBFDFFFBFFFFFFFFFF5ABA8E2A
                  AB6D32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD72
                  2FAD721AA264E9F9F0FFFFFFFFFEFFFEFDFFFCFFFEFFFFFF75C49D27AA6B32AE
                  7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD7230AD722C
                  AA6D54B084FFFFFFFFFDFFFFFDFFFFFFFCFFFFFFABD9C21EA8682EAD7232AD72
                  31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                  AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7232AE7132AE7131AE7120A7
                  6880C7A7FFFFFFFFFFFCFFFEFEFFFEFFF7FFFD1C9F6534AE6E31AD7232AE7132
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                  7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFEDF8F31AA36533AE7131AE7232AE7232AE7232AE7231AE7132AE711CA165
                  98D2B6FFFFFFFCFEFFFEFDFFFFFFFF7FC5A61EAC6432AF7132AD7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                  FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                  F31AA36533AE7131AE7232AE7232AE7232AE7232AE712FAC742EAE7116A4638E
                  CDB2FFFFFFFFFFFFFFFFFDFEFFFF34A47128AB6E32AE7131AE7132AD7232AD72
                  31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                  33AE7131AE7232AE7232AE7232AE7232AE7131AD7234AD7231AD721BA6606ABD
                  97FFFFFFFFFFFFFFFFFFE8F8EE23A26A27AC7035AB7431AF7031AE7034AD7231
                  AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                  AE7232AE7232AE7232AE7232AE7232AE7131AE7132AE7130AD7125A9663AA977
                  D4EEE6FFFFFFFFFFFFE7F7EF36A5741DAC6536AC7233AC7431AD7232AE7131AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                  32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                  7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                  AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F319A36433AE7131AE7131AE
                  7131AE7131AE7131AE7131AE7131AE7131AD7231AF7038AC7429AB7116A45D7A
                  C3A0FDFFFFFFFFFFFFFFFF69BC9612A2622BAE6C33AF6C2EAD7332AE7131AE71
                  31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                  7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131
                  AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE71
                  31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                  7131AE7131AE712CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F1069D5820A6651EA6651EA6651EA665
                  1EA6651EA6651EA6651EA6651EA66522A5651CA76424A66326A66213A25E0F9C
                  5883C9A8F3FCF6FFFFFFC8EADD45AB7A15A65F1FA6661EA6651EA6651EA6651E
                  A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                  1EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6
                  651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651E
                  A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                  1EA6651AA56234A770FFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFF9FDFDBEE1D5C3E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                  E4DAC5E4DAC5E4DAC5E4DAC6E4DAC6E4D9C5E4DAC5E4DAC5E3DBC5E5D9B6E0D1
                  BDD9D3FDFEFFFFFFFFFFFFFFCDE7DFC0E3D8C6E4DAC5E4DAC5E4DAC5E4DAC5E4
                  DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                  E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DA
                  C5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4
                  DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                  E3D9CAE4DCFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFDFEFEFCFEFEFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFBFFFFFEFFFFFFFF
                  FFFEFFFFFCFFFFFCFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFF
                  FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                  FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                  FDFFFFFCFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                  FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                  FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                  FDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                  FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFEFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
                mmHeight = 24342
                mmLeft = 2117
                mmTop = 9790
                mmWidth = 42333
                BandType = 1
              end
              object lbllblTransitosNoFacturadosNoHayTit: TppLabel
                UserName = 'Label1'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Tr'#225'nsitos no facturados'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 16
                Font.Style = [fsBold, fsItalic, fsUnderline]
                Transparent = True
                mmHeight = 6562
                mmLeft = 59267
                mmTop = 25400
                mmWidth = 64050
                BandType = 1
              end
              object lblTransitosNoFacturadosNoHayFecha: TppLabel
                UserName = 'Label2'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Fecha:'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 16
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 6615
                mmLeft = 139700
                mmTop = 6350
                mmWidth = 20373
                BandType = 1
              end
              object svTransitosNoFacturadosNoHayFecha: TppSystemVariable
                UserName = 'svTransitosNoFacturadosNoHayFecha'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                DisplayFormat = 'dd/mm/yyyy'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 16
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 6615
                mmLeft = 165100
                mmTop = 6350
                mmWidth = 30956
                BandType = 1
              end
              object lblTransitosNoFacturadosNoHayMensaje: TppLabel
                UserName = 'Label3'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'A la fecha no hay tr'#225'nsitos no facturados'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 16
                Font.Style = [fsBold, fsItalic]
                Transparent = True
                mmHeight = 6615
                mmLeft = 36248
                mmTop = 39158
                mmWidth = 119592
                BandType = 1
              end
              object ppLine13: TppLine
                UserName = 'Line13'
                Border.BorderPositions = []
                Border.Color = clWindowText
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Pen.Color = clWindowText
                Pen.Width = 8
                Weight = 6.000000000000000000
                mmHeight = 2910
                mmLeft = 0
                mmTop = 33867
                mmWidth = 205317
                BandType = 1
              end
            end
            object ppDetailBand2: TppDetailBand
              Background1.Brush.Style = bsClear
              Background1.Gradient.EndColor = clWhite
              Background1.Gradient.StartColor = clWhite
              Background1.Gradient.Style = gsNone
              Background2.Brush.Style = bsClear
              Background2.Gradient.EndColor = clWhite
              Background2.Gradient.StartColor = clWhite
              Background2.Gradient.Style = gsNone
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
            object ppSummaryBand1: TppSummaryBand
              AlignToBottom = False
              mmBottomOffset = 0
              mmHeight = 0
              mmPrintPosition = 0
            end
          end
        end
      end
    end
    object raCodeModule5: TraCodeModule
      ProgramStream = {00}
    end
    object ppParameterList2: TppParameterList
    end
  end
  object rbiCaratula: TRBInterface
    Report = pprCaratula
    OrderIndex = 0
    Left = 8
    Top = 8
  end
  object spBorrarTmps: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CarpetasMorosos_BorrarTmps;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdSesion'
        DataType = ftSmallint
        Value = Null
      end>
    Left = 216
    Top = 104
  end
  object spComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 500
    ProcedureName = 'ObtenerEncabezadoFacturaAImprimir;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 432
    Top = 40
    object spComprobantesNumeroConvenioFormateado: TStringField
      FieldName = 'NumeroConvenioFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object spComprobantesNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object spComprobantesCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object spComprobantesTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object spComprobantesNumeroComprobante: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object spComprobantesPeriodoInicial: TDateTimeField
      FieldName = 'PeriodoInicial'
      ReadOnly = True
    end
    object spComprobantesPeriodoFinal: TDateTimeField
      FieldName = 'PeriodoFinal'
      ReadOnly = True
    end
    object spComprobantesFechaEmision: TDateTimeField
      FieldName = 'FechaEmision'
      ReadOnly = True
    end
    object spComprobantesFechaVencimiento: TDateTimeField
      FieldName = 'FechaVencimiento'
    end
    object spComprobantesNombreCliente: TStringField
      FieldName = 'NombreCliente'
      ReadOnly = True
      Size = 152
    end
    object spComprobantesDomicilio: TStringField
      FieldName = 'Domicilio'
      ReadOnly = True
      Size = 61
    end
    object spComprobantesComuna: TStringField
      FieldName = 'Comuna'
      ReadOnly = True
      FixedChar = True
      Size = 100
    end
    object spComprobantesComentarios: TStringField
      FieldName = 'Comentarios'
      Size = 100
    end
    object spComprobantesCodigoBarras: TStringField
      FieldName = 'CodigoBarras'
      ReadOnly = True
      Size = 113
    end
    object spComprobantesCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      ReadOnly = True
      Size = 10
    end
    object spComprobantesTotalAPagar: TStringField
      FieldName = 'TotalAPagar'
      ReadOnly = True
    end
    object spComprobantesCodigoTipoMedioPago: TWordField
      FieldName = 'CodigoTipoMedioPago'
    end
    object spComprobantesEstadoPago: TStringField
      FieldName = 'EstadoPago'
      FixedChar = True
      Size = 1
    end
    object spComprobantesSaldoPendiente: TStringField
      FieldName = 'SaldoPendiente'
      ReadOnly = True
    end
    object spComprobantesAjusteSencilloAnterior: TStringField
      FieldName = 'AjusteSencilloAnterior'
      ReadOnly = True
    end
    object spComprobantesAjusteSencilloActual: TStringField
      FieldName = 'AjusteSencilloActual'
      ReadOnly = True
    end
    object spComprobantesFechaHoy: TDateTimeField
      FieldName = 'FechaHoy'
      ReadOnly = True
    end
  end
  object spObtenerCargos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 500
    ProcedureName = 'ObtenerListadoCargosFacturaAImprimir;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 431
    Top = 72
    object spObtenerCargosDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object spObtenerCargosImporte: TLargeintField
      FieldName = 'Importe'
    end
    object spObtenerCargosDescImporte: TStringField
      FieldName = 'DescImporte'
      Size = 50
    end
  end
  object spUltimosDoce: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 500
    ProcedureName = 'ObtenerUltimosDoceMeses;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = -6
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 432
    Top = 105
    object spUltimosDoceIDh: TDateTimeField
      FieldName = 'IDh'
    end
    object spUltimosDoceMes: TStringField
      FieldName = 'Mes'
      FixedChar = True
      Size = 3
    end
    object spUltimosDoceTotalAPagar: TBCDField
      FieldName = 'TotalAPagar'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
  end
  object spObtenerConsumos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 500
    ProcedureName = 'ImprimirNK_ObtenerConsumosDeNotaCobro;1'
    Parameters = <
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalKm'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@TotalN'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@TotalP'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@TotalC'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@TotalTotal'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end>
    Left = 431
    Top = 136
    object spObtenerConsumosNroMatricula: TStringField
      FieldName = 'NroMatricula'
      FixedChar = True
      Size = 10
    end
    object spObtenerConsumosKilometros: TBCDField
      FieldName = 'Kilometros'
      Precision = 32
      Size = 2
    end
    object spObtenerConsumosDImporteCongestion: TStringField
      FieldName = 'DImporteCongestion'
    end
    object spObtenerConsumosDImportePunta: TStringField
      FieldName = 'DImportePunta'
    end
    object spObtenerConsumosDImporteNormal: TStringField
      FieldName = 'DImporteNormal'
    end
    object spObtenerConsumosDImporte: TStringField
      FieldName = 'DImporte'
    end
  end
  object spMensaje: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 500
    ProcedureName = 'ObtenerMensajeComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 432
    Top = 168
    object spMensajeTexto: TStringField
      FieldName = 'Texto'
      Size = 255
    end
  end
  object plComprobantes: TppDBPipeline
    DataSource = dsComprobantes
    UserName = 'Comprobantes'
    BeforeClose = plComprobantesBeforeClose
    Left = 352
    Top = 40
    object plComprobantesppField1: TppField
      FieldAlias = 'NumeroConvenioFormateado'
      FieldName = 'NumeroConvenioFormateado'
      FieldLength = 0
      DisplayWidth = 0
      Position = 0
    end
    object plComprobantesppField2: TppField
      FieldAlias = 'NumeroConvenio'
      FieldName = 'NumeroConvenio'
      FieldLength = 30
      DisplayWidth = 30
      Position = 1
    end
    object plComprobantesppField3: TppField
      Alignment = taRightJustify
      FieldAlias = 'CodigoConvenio'
      FieldName = 'CodigoConvenio'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 2
    end
    object plComprobantesppField4: TppField
      FieldAlias = 'TipoComprobante'
      FieldName = 'TipoComprobante'
      FieldLength = 2
      DisplayWidth = 2
      Position = 3
    end
    object plComprobantesppField5: TppField
      FieldAlias = 'NumeroComprobante'
      FieldName = 'NumeroComprobante'
      FieldLength = 0
      DataType = dtLargeInt
      DisplayWidth = 15
      Position = 4
    end
    object plComprobantesppField6: TppField
      FieldAlias = 'PeriodoInicial'
      FieldName = 'PeriodoInicial'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 5
    end
    object plComprobantesppField7: TppField
      FieldAlias = 'PeriodoFinal'
      FieldName = 'PeriodoFinal'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 6
    end
    object plComprobantesppField8: TppField
      FieldAlias = 'FechaEmision'
      FieldName = 'FechaEmision'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 7
    end
    object plComprobantesppField9: TppField
      FieldAlias = 'FechaVencimiento'
      FieldName = 'FechaVencimiento'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 8
    end
    object plComprobantesppField10: TppField
      FieldAlias = 'NombreCliente'
      FieldName = 'NombreCliente'
      FieldLength = 152
      DisplayWidth = 152
      Position = 9
    end
    object plComprobantesppField11: TppField
      FieldAlias = 'Domicilio'
      FieldName = 'Domicilio'
      FieldLength = 61
      DisplayWidth = 61
      Position = 10
    end
    object plComprobantesppField12: TppField
      FieldAlias = 'Comuna'
      FieldName = 'Comuna'
      FieldLength = 100
      DisplayWidth = 100
      Position = 11
    end
    object plComprobantesppField13: TppField
      FieldAlias = 'Comentarios'
      FieldName = 'Comentarios'
      FieldLength = 100
      DisplayWidth = 100
      Position = 12
    end
    object plComprobantesppField14: TppField
      FieldAlias = 'CodigoBarras'
      FieldName = 'CodigoBarras'
      FieldLength = 113
      DisplayWidth = 113
      Position = 13
    end
    object plComprobantesppField15: TppField
      FieldAlias = 'CodigoPostal'
      FieldName = 'CodigoPostal'
      FieldLength = 10
      DisplayWidth = 10
      Position = 14
    end
    object plComprobantesppField16: TppField
      FieldAlias = 'TotalAPagar'
      FieldName = 'TotalAPagar'
      FieldLength = 20
      DisplayWidth = 20
      Position = 15
    end
    object plComprobantesppField17: TppField
      Alignment = taRightJustify
      FieldAlias = 'CodigoTipoMedioPago'
      FieldName = 'CodigoTipoMedioPago'
      FieldLength = 0
      DataType = dtLongint
      DisplayWidth = 10
      Position = 16
    end
    object plComprobantesppField18: TppField
      FieldAlias = 'EstadoPago'
      FieldName = 'EstadoPago'
      FieldLength = 1
      DisplayWidth = 1
      Position = 17
    end
    object plComprobantesppField19: TppField
      FieldAlias = 'SaldoPendiente'
      FieldName = 'SaldoPendiente'
      FieldLength = 20
      DisplayWidth = 20
      Position = 18
    end
    object plComprobantesppField20: TppField
      FieldAlias = 'AjusteSencilloAnterior'
      FieldName = 'AjusteSencilloAnterior'
      FieldLength = 20
      DisplayWidth = 20
      Position = 19
    end
    object plComprobantesppField21: TppField
      FieldAlias = 'AjusteSencilloActual'
      FieldName = 'AjusteSencilloActual'
      FieldLength = 20
      DisplayWidth = 20
      Position = 20
    end
    object plComprobantesppField22: TppField
      FieldAlias = 'FechaHoy'
      FieldName = 'FechaHoy'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 21
    end
  end
  object dsComprobantes: TDataSource
    DataSet = spComprobantes
    Left = 392
    Top = 40
  end
  object plObtenerCargos: TppDBPipeline
    DataSource = dsObtenerCargos
    UserName = 'ObtenerCargos'
    Left = 352
    Top = 72
    object plObtenerCargosppField1: TppField
      FieldAlias = 'Descripcion'
      FieldName = 'Descripcion'
      FieldLength = 0
      DisplayWidth = 0
      Position = 0
    end
    object plObtenerCargosppField2: TppField
      FieldAlias = 'Importe'
      FieldName = 'Importe'
      FieldLength = 0
      DataType = dtLargeInt
      DisplayWidth = 15
      Position = 1
    end
    object plObtenerCargosppField3: TppField
      FieldAlias = 'DescImporte'
      FieldName = 'DescImporte'
      FieldLength = 50
      DisplayWidth = 50
      Position = 2
    end
  end
  object plUltimosDoce: TppDBPipeline
    DataSource = dsUltimosDoce
    UserName = 'UltimosDoce'
    Left = 352
    Top = 104
    object plUltimosDoceppField1: TppField
      FieldAlias = 'IDh'
      FieldName = 'IDh'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object plUltimosDoceppField2: TppField
      FieldAlias = 'Mes'
      FieldName = 'Mes'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object plUltimosDoceppField3: TppField
      FieldAlias = 'TotalAPagar'
      FieldName = 'TotalAPagar'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
  end
  object plConsumos: TppDBPipeline
    DataSource = dsObtenerConsumos
    UserName = 'Consumos'
    Left = 352
    Top = 136
    object plObtnerConsumosppField1: TppField
      FieldAlias = 'NroMatricula'
      FieldName = 'NroMatricula'
      FieldLength = 0
      DisplayWidth = 0
      Position = 0
    end
    object plObtnerConsumosppField2: TppField
      Alignment = taRightJustify
      FieldAlias = 'Kilometros'
      FieldName = 'Kilometros'
      FieldLength = 2
      DataType = dtDouble
      DisplayWidth = 33
      Position = 1
    end
    object plObtnerConsumosppField3: TppField
      FieldAlias = 'DImporteCongestion'
      FieldName = 'DImporteCongestion'
      FieldLength = 20
      DisplayWidth = 20
      Position = 2
    end
    object plObtnerConsumosppField4: TppField
      FieldAlias = 'DImportePunta'
      FieldName = 'DImportePunta'
      FieldLength = 20
      DisplayWidth = 20
      Position = 3
    end
    object plObtnerConsumosppField5: TppField
      FieldAlias = 'DImporteNormal'
      FieldName = 'DImporteNormal'
      FieldLength = 20
      DisplayWidth = 20
      Position = 4
    end
    object plObtnerConsumosppField6: TppField
      FieldAlias = 'DImporte'
      FieldName = 'DImporte'
      FieldLength = 20
      DisplayWidth = 20
      Position = 5
    end
  end
  object plMensaje: TppDBPipeline
    DataSource = dsMensaje
    UserName = 'Mensaje'
    Left = 352
    Top = 168
    object plMensajeppField1: TppField
      FieldAlias = 'Texto'
      FieldName = 'Texto'
      FieldLength = 0
      DisplayWidth = 0
      Position = 0
    end
  end
  object dsObtenerCargos: TDataSource
    DataSet = spObtenerCargos
    Left = 392
    Top = 72
  end
  object dsUltimosDoce: TDataSource
    DataSet = spUltimosDoce
    Left = 392
    Top = 104
  end
  object dsObtenerConsumos: TDataSource
    DataSet = spObtenerConsumos
    Left = 392
    Top = 136
  end
  object dsMensaje: TDataSource
    DataSet = spMensaje
    Left = 392
    Top = 168
  end
  object spObtenerTransitosNoFacturadosDeConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTransitosNoFacturadosDeConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 432
    Top = 256
  end
  object dsspObtenerTransitosNoFacturadosDeConvenio: TDataSource
    DataSet = spObtenerTransitosNoFacturadosDeConvenio
    Left = 396
    Top = 256
  end
  object plTransitosNoFacturados: TppDBPipeline
    DataSource = dsspObtenerTransitosNoFacturadosDeConvenio
    UserName = 'plTransitosNoFacturados'
    Left = 356
    Top = 256
    object plTransitosNoFacturadosppField1: TppField
      FieldAlias = 'NumCorrCA'
      FieldName = 'NumCorrCA'
      FieldLength = 0
      DataType = dtLargeInt
      DisplayWidth = 15
      Position = 0
    end
    object plTransitosNoFacturadosppField2: TppField
      FieldAlias = 'FechaHora'
      FieldName = 'FechaHora'
      FieldLength = 0
      DataType = dtDateTime
      DisplayWidth = 18
      Position = 1
    end
    object plTransitosNoFacturadosppField3: TppField
      FieldAlias = 'TipoHorarioPrimerTramo'
      FieldName = 'TipoHorarioPrimerTramo'
      FieldLength = 1
      DisplayWidth = 1
      Position = 2
    end
    object plTransitosNoFacturadosppField4: TppField
      Alignment = taRightJustify
      FieldAlias = 'Importe'
      FieldName = 'Importe'
      FieldLength = 6
      DataType = dtDouble
      DisplayWidth = 27
      Position = 3
    end
    object plTransitosNoFacturadosppField5: TppField
      FieldAlias = 'DescriImporte'
      FieldName = 'DescriImporte'
      FieldLength = 20
      DisplayWidth = 20
      Position = 4
    end
    object plTransitosNoFacturadosppField6: TppField
      FieldAlias = 'DescriPtoCobro'
      FieldName = 'DescriPtoCobro'
      FieldLength = 60
      DisplayWidth = 60
      Position = 5
    end
    object plTransitosNoFacturadosppField7: TppField
      FieldAlias = 'Patente'
      FieldName = 'Patente'
      FieldLength = 10
      DisplayWidth = 10
      Position = 6
    end
    object plTransitosNoFacturadosppField8: TppField
      FieldAlias = 'DescriTipoHorario'
      FieldName = 'DescriTipoHorario'
      FieldLength = 40
      DisplayWidth = 40
      Position = 7
    end
    object plTransitosNoFacturadosppField9: TppField
      Alignment = taRightJustify
      FieldAlias = 'CodigoConvenio'
      FieldName = 'CodigoConvenio'
      FieldLength = 0
      DataType = dtInteger
      DisplayWidth = 10
      Position = 8
    end
  end
  object cdsConvenios: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'cdsConveniosEnCarpetaIdConvenioCarpetaLegal'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaCodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaApellido'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosEnCarpetaApellidoMaterno'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosEnCarpetaNombre'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraImpresion'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioImpresion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraEnvioALegales'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioEnvioLegales'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraSalidaLegales'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraActualizacion'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioActualizacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaNumeroDocumento'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaNumeroConvenio'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaSeleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'cdsConveniosEnCarpetaApellidoMostrar'
        DataType = ftString
        Size = 155
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 96
    Top = 40
    Data = {
      B20600009619E0BD010000001800000012000000000003000000B2061F636473
      436F6E76656E696F73456E436172706574614964436F6E76656E696F04000100
      00000100094649454C444E414D452C000980636473436F6E76656E696F73456E
      436172706574614964436F6E76656E696F436172706574614C6567616C001F63
      6473436F6E76656E696F73456E43617270657461436F6469676F436F6E760400
      010000000100094649454C444E414D4524000980636473436F6E76656E696F73
      456E43617270657461436F6469676F436F6E76656E696F001D636473436F6E76
      656E696F73456E436172706574614170656C6C69646F01004900000001000557
      49445448020002003C001F636473436F6E76656E696F73456E43617270657461
      4170656C6C69646F4D610100490000000200055749445448020002003C000946
      49454C444E414D4525000980636473436F6E76656E696F73456E436172706574
      614170656C6C69646F4D617465726E6F001B636473436F6E76656E696F73456E
      436172706574614E6F6D6272650100490000000100055749445448020002001E
      001F636473436F6E76656E696F73456E436172706574614665636861486F7261
      490800080000000100094649454C444E414D4528000980636473436F6E76656E
      696F73456E436172706574614665636861486F7261496D70726573696F6E001F
      636473436F6E76656E696F73456E436172706574615573756172696F496D7001
      00490000000200055749445448020002001400094649454C444E414D45260009
      80636473436F6E76656E696F73456E436172706574615573756172696F496D70
      726573696F6E001F636473436F6E76656E696F73456E43617270657461466563
      6861486F7261450800080000000100094649454C444E414D452C000980636473
      436F6E76656E696F73456E436172706574614665636861486F7261456E76696F
      414C6567616C6573001F636473436F6E76656E696F73456E4361727065746155
      73756172696F456E760100490000000200055749445448020002001400094649
      454C444E414D4529000980636473436F6E76656E696F73456E43617270657461
      5573756172696F456E76696F4C6567616C6573001F636473436F6E76656E696F
      73456E436172706574614665636861486F726153080008000000010009464945
      4C444E414D452C000980636473436F6E76656E696F73456E4361727065746146
      65636861486F726153616C6964614C6567616C6573001F636473436F6E76656E
      696F73456E436172706574614665636861486F72614308000800000001000946
      49454C444E414D4527000980636473436F6E76656E696F73456E436172706574
      614665636861486F72614372656163696F6E001F636473436F6E76656E696F73
      456E436172706574615573756172696F43726501004900000002000557494454
      48020002001400094649454C444E414D4525000980636473436F6E76656E696F
      73456E436172706574615573756172696F4372656163696F6E001F636473436F
      6E76656E696F73456E436172706574614665636861486F726141080008000000
      0100094649454C444E414D452C000980636473436F6E76656E696F73456E4361
      72706574614665636861486F726141637475616C697A6163696F6E001F636473
      436F6E76656E696F73456E436172706574615573756172696F41637401004900
      00000200055749445448020002001400094649454C444E414D452A0009806364
      73436F6E76656E696F73456E436172706574615573756172696F41637475616C
      697A6163696F6E001F636473436F6E76656E696F73456E436172706574614E75
      6D65726F446F6375010049000000020005574944544802000200140009464945
      4C444E414D4525000980636473436F6E76656E696F73456E436172706574614E
      756D65726F446F63756D656E746F001F636473436F6E76656E696F73456E4361
      72706574614E756D65726F436F6E760100490000000200055749445448020002
      001400094649454C444E414D4524000980636473436F6E76656E696F73456E43
      6172706574614E756D65726F436F6E76656E696F001F636473436F6E76656E69
      6F73456E4361727065746153656C656363696F6E610200030000000100094649
      454C444E414D4522000980636473436F6E76656E696F73456E43617270657461
      53656C656363696F6E61646F001F636473436F6E76656E696F73456E43617270
      6574614170656C6C69646F4D6F0100490000000200055749445448020002009B
      00094649454C444E414D4525000980636473436F6E76656E696F73456E436172
      706574614170656C6C69646F4D6F7374726172000000}
  end
end
