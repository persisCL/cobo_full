unit FrmDatosContactoComunicacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, MaskCombo, peatypes, util, utilproc;

type
  TFormDatosContactoComunicacion = class(TForm)
    Label4: TLabel;
    lblApellido: TLabel;
    lblNombre: TLabel;
    Label1: TLabel;
    txt_nombre: TEdit;
    txtDomicilio: TEdit;
    txt_apellido: TEdit;
    mcTipoNumeroDocumento: TMaskCombo;
    lblApellidoMaterno: TLabel;
    txt_apellidoMaterno: TEdit;
    Label2: TLabel;
    txtNumeroCalle: TEdit;
    Panel10: TPanel;
    Label12: TLabel;
    txtTelefonoParticular: TEdit;
    Label21: TLabel;
    txtEmailParticular: TEdit;
    btnAceptar: TDPSButton;
    btnSalir: TDPSButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
    function GetContactoComunicacion: TDatosContactoComunicacion;
    procedure SetContactoComunicacion(contactoComu: TDatosContactoComunicacion);
  public
    { Public declarations }
    function Inicializar(): Boolean;
    function InicializarModificacion(cont: TDatosContactoComunicacion): boolean;
    property ContactoComunicacion: TDatosContactoComunicacion read GetContactoComunicacion write SetContactoComunicacion;

  end;

var
  FormDatosContactoComunicacion: TFormDatosContactoComunicacion;

implementation

uses DMConnection, Peaprocs;

{$R *.dfm}

function TFormDatosContactoComunicacion.Inicializar(): Boolean;
begin
    CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento);
    Result := True;
end;

function TFormDatosContactoComunicacion.GetContactoComunicacion: TDatosContactoComunicacion;
var
    Tmp: TDatosContactoComunicacion;
begin
    //Guardo en la Variable los datos de los Campos
    with Tmp do begin
        Persona.CodigoPersona   := 0;
        Persona.TipoDocumento   := Trim(StrRight(mcTipoNumeroDocumento.ComboText, 10));
        Persona.NumeroDocumento := Trim(mcTipoNumeroDocumento.MaskText);
        Persona.Apellido        := Trim(txt_Apellido.Text);
        Persona.ApellidoMaterno := Trim(txt_ApellidoMaterno.Text);
        Persona.Nombre          := Trim(txt_Nombre.Text);
        Domicilio               := Trim(txtDomicilio.Text);
        NumeroCalle             := Trim(txtNumeroCalle.Text);
        Telefono                := Trim(txtTelefonoParticular.Text);
        Email                   := Trim(txtEmailParticular.Text);
    end;
    Result := tmp;
end;


procedure TFormDatosContactoComunicacion.btnAceptarClick(Sender: TObject);
ResourceString
    CAPTION_VALIDAR_DATOS_CONTACTO  = 'Validar datos del Contacto';
    MSG_VALIDAR_EMAIL               = 'La direcci�n de correo especificada es err�nea';
    MSG_VALIDAR_RUT                 = 'El N�mero de documento es incorrecto';
    MSG_VALIDAR_APELLIDO            = 'Debe ingresar el apellido.';
    MSG_VALIDAR_NOMBRE              = 'Debe ingresar el nombre.';
begin
    if not mcTipoNumeroDocumento.ValidateMask  then begin
        MsgBoxBalloon(MSG_VALIDAR_RUT, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, mcTipoNumeroDocumento);
        Exit;
    end;
    if  (trim(txt_apellido.Text) = '') then begin
        MsgBoxBalloon(MSG_VALIDAR_APELLIDO, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, txt_apellido);
        Exit;
    end;
    if  (trim(txt_nombre.Text) = '') then begin
        MsgBoxBalloon(MSG_VALIDAR_APELLIDO, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, txt_nombre);
        Exit;
    end;
    if (Trim(txtemailParticular.Text) <> '') and not IsValidEmailList(Trim(txtemailParticular.Text)) then begin
        MsgBoxBalloon(MSG_VALIDAR_EMAIL, CAPTION_VALIDAR_DATOS_CONTACTO, MB_ICONSTOP, txtemailParticular);
        Exit;
    end;
    ModalResult := mrOK;
end;

procedure TFormDatosContactoComunicacion.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormDatosContactoComunicacion.SetContactoComunicacion(
  contactoComu: TDatosContactoComunicacion);
begin
    with ContactoComunicacion do begin
        Persona.CodigoPersona   := 0;
        Persona.TipoDocumento   := contactoComu.Persona.TipoDocumento;
        Persona.NumeroDocumento := contactoComu.Persona.NumeroDocumento;
        Persona.Apellido        := contactoComu.Persona.Apellido;
        Persona.ApellidoMaterno := contactoComu.Persona.ApellidoMaterno;
        Persona.Nombre          := contactoComu.Persona.Nombre;
        Domicilio               := contactoComu.Domicilio;
        NumeroCalle             := contactoComu.NumeroCalle;
        Telefono                := contactoComu.Telefono;
        Email                   := contactoComu.Email;
    end;
end;

function TFormDatosContactoComunicacion.InicializarModificacion(
  cont: TDatosContactoComunicacion): boolean;
begin
    CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento);
    mcTipoNumeroDocumento.MaskText := trim(Cont.Persona.NumeroDocumento);
    txt_apellido.Text           := Cont.Persona.Apellido;
    txt_apellidoMaterno.Text    := Cont.Persona.ApellidoMaterno;
    txt_nombre.Text             := Cont.Persona.Nombre;
    txtDomicilio.Text           := Cont.Domicilio;
    txtNumeroCalle.Text         := Cont.NumeroCalle;
    txtTelefonoParticular.Text  := Cont.Telefono;
    txtEmailParticular.Text     := Cont.Email;
    Result := True;
end;

end.
