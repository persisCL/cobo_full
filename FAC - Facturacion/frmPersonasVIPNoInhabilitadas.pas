{
Firma       :   SS_1006_1015_NDR_20121015
Descripton  :   Personas que no son tomadas en cuenta en el JOB que inhabilita
                por morosidad.

--------------------------------------------------------------------------------------

Revision		:	2
Author			:	Miguel Villarroel
Date			:	12-Junio-2013
Description		:	Valida que cliente no est� en Lista Amarilla.
Firma           :   SS-660-MVI-20130610
--------------------------------------------------------------------------------------

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla
--------------------------------------------------------------------------------------

Firma       : SS_1006_MVI_20130814
Descripcion : Se realizan correcciones para que al buscar un Rut se habilite inmediatamente
              el bot�n (Agregar, Quitar).
              Se adiciona el campo Rut en la tabla de datos.
              Se cambia el MsgBox por el MsgBoxCN

Firma       :   SS_1006D_MBE_20140311
Description :   Se hacen algunas mejoras al flujo.
--------------------------------------------------------------------------------------
}
unit frmPersonasVIPNoInhabilitadas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, frmDatoPesona,BuscaClientes, DmiCtrls,strutils,
  DB, ADODB, Util,UtilDB,DMConnection, DBClient, ListBoxEx, DBListEx,PeaProcsCN,PeaProcs,UtilProc;

type
  TformPersonasVIPNoInhabilitadas = class(TForm)
    pnlBottom: TPanel;
    pnlCentral: TPanel;
    pnlButtons: TPanel;
    btnGrabar: TButton;
    btnSalir: TButton;
    Panel1: TPanel;
    peNumeroDocumento: TPickEdit;
    Label4: TLabel;
    lblNombre: TLabel;
    spObtenerDatosCliente: TADOStoredProc;
    DBLEPersonasVIP: TDBListEx;
    spObtenerPersonaVIPNoInhabilitada: TADOStoredProc;
    dsObtenerPersonaVIPNoInhabilitada: TDataSource;
    spConveniosEnListaAmarillaPorRut: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure ordenarColumna(Sender: TObject);
    procedure peNumeroDocumentoEnter(Sender: TObject);
    procedure peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoPersona: Integer;
    FCodigoConcesionaria: SmallInt;
  public
    { Public declarations }
	function Inicializar(): Boolean;
    procedure ObtenerPersonaVIPNoInhabilitada;
  end;

var
  formPersonasVIPNoInhabilitadas: TformPersonasVIPNoInhabilitadas;

implementation

{$R *.dfm}

procedure TformPersonasVIPNoInhabilitadas.btnGrabarClick(Sender: TObject);
resourcestring
    MSG_TITULO = 'Clientes VIP No Inhabilitados';
    MSG_DESEA_ACTUALIZAR = '�Est� seguro que desea %s a %s como cliente VIP?';
var
    spActualizarPersonaVIPNoInhabilitada:TADOStoredProc;
begin
//    if MsgBox(Format(MSG_DESEA_ACTUALIZAR,[UpperCase(btnGrabar.Caption),lblNombre.Caption]),  MSG_TITULO,     //SS_1006_MVI_20130814
//                MB_ICONWARNING + MB_YESNO + MB_DEFBUTTON2) = IDYES then                                       //SS_1006_MVI_20130814
    if ShowMsgBoxCN(MSG_TITULO, Format(MSG_DESEA_ACTUALIZAR,[UpperCase(btnGrabar.Caption),lblNombre.Caption]),  //SS_1006_MVI_20130814
                     MB_ICONQUESTION, Self) = mrOk then                                                         //SS_1006_MVI_20130814
    begin
        try
            spActualizarPersonaVIPNoInhabilitada := TADOStoredProc.Create(nil);
            spActualizarPersonaVIPNoInhabilitada.Connection := DMConnections.BaseCAC;
            spActualizarPersonaVIPNoInhabilitada.ProcedureName := 'ActualizarPersonaVIPNoInhabilitada';
            With spActualizarPersonaVIPNoInhabilitada do
            begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value          := FCodigoPersona;
                Parameters.ParamByName('@CodigoConcesionaria').Value    := FCOdigoConcesionaria;
                Parameters.ParamByName('@CodigoUsuario').Value          := UsuarioSistema;
                if QueryGetValueInt(DMConnections.BaseCAC,
                                    Format('SELECT dbo.PersonaVIPNoInhabilitada(%d,%d)',
                                            [FCodigoPersona,FCodigoConcesionaria
                                            ]
                                          ))=0 then
                begin
                    Parameters.ParamByName('@Accion').Value          := 'A';
                end
                else
                begin
                    Parameters.ParamByName('@Accion').Value          := 'B';
                end;
                Parameters.ParamByName('@DescripcionError').Value       := '';
                ExecProc;
            end;
            Inicializar;
        finally
            spActualizarPersonaVIPNoInhabilitada.Close;
            FreeAndNil(spActualizarPersonaVIPNoInhabilitada);
        end;
    end;
end;

procedure TformPersonasVIPNoInhabilitadas.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TformPersonasVIPNoInhabilitadas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

function TformPersonasVIPNoInhabilitadas.Inicializar: Boolean;
begin
  spObtenerPersonaVIPNoInhabilitada.Close;
  peNumeroDocumento.Text:='';
  lblNombre.Caption:='';
  FCodigoPersona:=0;
  FCodigoConcesionaria:=8;
  btnGrabar.Caption:='Grabar';
  ObtenerPersonaVIPNoInhabilitada;
  btnGrabar.Caption := 'Grabar';
  btnGrabar.Enabled:=False;
  Result := True;                   //SS_1006D_MBE_20140311
end;

procedure TformPersonasVIPNoInhabilitadas.ObtenerPersonaVIPNoInhabilitada;
resourcestring
    rsTitulo    ='Convenios VIP No Inhabilitados';
    rsMensaje   ='El RUT Ingresado no tiene convenios';
begin
    with spObtenerPersonaVIPNoInhabilitada do
    begin
        Close;
        Parameters.Refresh;
        If FCodigoPersona=0 then
            Parameters.ParamByName('@CodigoPersona').Value :=Null
        else
            Parameters.ParamByName('@CodigoPersona').Value :=FCodigoPersona;

        Parameters.ParamByName('@CodigoConcesionaria').Value :=FCodigoConcesionaria;
        Open;
    end;
    if QueryGetValueInt(DMConnections.BaseCAC,
                        Format('SELECT dbo.PersonaVIPNoInhabilitada(%d,%d)',
                                [FCodigoPersona,FCodigoConcesionaria
                                ]
                              ))=0 then
    begin
        btnGrabar.Caption:='AGREGAR';
    end
    else
    begin
        btnGrabar.Caption:='QUITAR';
    end;
end;

procedure TformPersonasVIPNoInhabilitadas.peNumeroDocumentoButtonClick(
  Sender: TObject);
var
	f: TFormBuscaClientes;
//  letra: Word;                                                                    //SS_1006_MVI_20130814  //SS_1006D_MBE_20140311
    letra : Char;                                                                   //SS_1006D_MBE_20140311
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
            FUltimaBusqueda := F.UltimaBusqueda;
			peNumeroDocumento.Text := Trim(f.Persona.NumeroDocumento);
            lblNombre.Caption:=IfThen(f.Persona.Personeria='J',f.Persona.RazonSocial,Trim(f.Persona.Apellido)+' '+Trim(f.Persona.ApellidoMaterno)+', '+Trim(f.Persona.Nombre));
            FCodigoPersona := F.Persona.CodigoPersona;
            ObtenerPersonaVIPNoInhabilitada;
//            letra := 13;                                                          //SS_1006_MVI_20130814         //SS_1006D_MBE_20140311
            letra := #13;                                                         //SS_1006D_MBE_20140311
//           peNumeroDocumento.OnKeyUp( nil, letra, [ ssShift ] );                 //SS_1006_MVI_20130814          //SS_1006D_MBE_20140311
            peNumeroDocumento.OnKeyPress(nil, letra);                               //SS_1006D_MBE_20140311
	   	end;
	end;
    f.Release;
end;

procedure TformPersonasVIPNoInhabilitadas.peNumeroDocumentoEnter(
  Sender: TObject);
begin
  Inicializar;
end;

procedure TformPersonasVIPNoInhabilitadas.peNumeroDocumentoKeyPress(
  Sender: TObject; var Key: Char);
 resourcestring                                                                                                             //SS-660-MVI-20130610
    MSG_TITULO = 'Clientes VIP No Inhabilitados';                                                                           //SS-660-MVI-20130610
    MSG_CLIENTE_EN_LISTA_AMARILLA = 'El cliente posee convenios en Lista Amarilla';                                         //SS-660-MVI-20130610
    MSG_NO_EXISTE_CLIENTE   = 'No se encontr� el RUT %s';                                                                   //SS_1006D_MBE_20140311
const                                                                                                                       //SS-660-MVI-20130610
    SQLValidarClienteListaAmarilla = 'SELECT 1 FROM Convenio WITH(NOLOCK) WHERE CodigoCliente = %d'                         //SS-660-MVI-20130610
                                      //+' AND dbo.EstaConvenioEnListaAmarilla(Convenio.CodigoConvenio, GETDATE()) = 1';        // SS_660_CQU_20130711  //SS-660-MVI-20130610
                                      +' AND dbo.EstaConvenioEnListaAmarilla(Convenio.CodigoConvenio, GETDATE(), NULL) = 1';    // SS_660_CQU_20130711  //SS-660-MVI-20130610
var
	f: TFormBuscaClientes;
  Convenios: String;																									  //SS-660-MVI-20130610
begin
    if Key = #13 then
    begin
        Key := #0;
        if Trim(peNumeroDocumento.text) = '' then begin
            peNumeroDocumento.OnButtonClick(nil);
        end
        else
        begin
            FCodigoPersona := QueryGetValueInt(DMConnections.BaseCAC, Format(
                  'SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s''',
                  [PadL(Trim(peNumeroDocumento.Text), 9, '0')]));
            if FCodigoPersona = 0 then begin                                                                          //SS_1006D_MBE_20140311
                MsgBox(Format(MSG_NO_EXISTE_CLIENTE, [peNumeroDocumento.Text]),Caption, MB_ICONEXCLAMATION);          //SS_1006D_MBE_20140311
                Exit;                                                                                                 //SS-660-MVI-20130610
            end;                                                                                                      //SS_1006D_MBE_20140311

            spObtenerDatosCliente.Close;
            spObtenerDatosCliente.Parameters.ParamByName('@CodigoCliente').Value := FCodigoPersona;
            spObtenerDatosCliente.Open;
            lblNombre.Caption:= Trim(spObtenerDatosCliente.FieldByName('Nombre').AsString);
            ObtenerPersonaVIPNoInhabilitada;
            if ( (QueryGetValueInt(DMConnections.BaseCAC,                                                                                   //SS-660-MVI-20130610
                                Format(SQLValidarClienteListaAmarilla,                                                                      //SS-660-MVI-20130610
                                       [FCodigoPersona]                                                                                     //SS-660-MVI-20130610
                                       )) = 1)                                                                                              //SS-660-MVI-20130610
                 and (QueryGetValueInt(DMConnections.BaseCAC,                                                                               //SS-660-MVI-20130610
                                Format('SELECT dbo.PersonaVIPNoInhabilitada(%d,%d)',                                                        //SS-660-MVI-20130610
                                      [FCodigoPersona,FCodigoConcesionaria]                                                                 //SS-660-MVI-20130610
                                      )) = 0 )                                                                                              //SS-660-MVI-20130610
              )then begin                                                                                                                   //SS-660-MVI-20130610
                                                                                                                                             //SS-660-MVI-20130610
            Convenios := ' ' + #13;                                                                                                          //SS-660-MVI-20130610
            with spConveniosEnListaAmarillaPorRut do begin                                                                                   //SS-660-MVI-20130610
              Parameters.Refresh;                                                                                                            //SS-660-MVI-20130610
              Parameters.ParamByName('@Rut').Value := peNumeroDocumento.text;                                                                //SS-660-MVI-20130610
                                                                                                                                             //SS-660-MVI-20130610
              Open;                                                                                                                          //SS-660-MVI-20130610
              while not spConveniosEnListaAmarillaPorRut.Eof  do begin                                                                     //SS_1006D_MBE_20140311
                Convenios:= Convenios + ' ' + spConveniosEnListaAmarillaPorRut.FieldByName('NumeroConvenio').AsString + ' ' +              //SS_1006D_MBE_20140311
                spConveniosEnListaAmarillaPorRut.FieldByName('EstadoListaAmarilla').AsString + #13;                                        //SS_1006D_MBE_20140311
                spConveniosEnListaAmarillaPorRut.Next;                                                                                     //SS_1006D_MBE_20140311
              end;                                                                                                                           //SS-660-MVI-20130610
              Close;                                                                                                                         //SS-660-MVI-20130610
                                                                                                                                             //SS-660-MVI-20130610
              //MsgBoxErr( MSG_CLIENTE_EN_LISTA_AMARILLA + Convenios, '',MSG_TITULO, 0);                    // SS_1006D_MBE_20140311
              MsgBox(MSG_CLIENTE_EN_LISTA_AMARILLA + Convenios, MSG_TITULO, MB_ICONEXCLAMATION);            // SS_1006D_MBE_20140311                                                //SS-660-MVI-20130610
                                                                                                                                             //SS-660-MVI-20130610
            end;                                                                                                                             //SS-660-MVI-20130610
            end                                                                                                                              //SS-660-MVI-20130610
            else                                                                                                                             //SS-660-MVI-20130610
            begin                                                                                                                            //SS-660-MVI-20130610
                 btnGrabar.Enabled:=True;                                                                                                    //SS-660-MVI-20130610
            end;                                                                                                                             //SS-660-MVI-20130610
            DBLEPersonasVIP.SetFocus;
        end;
    end;

end;

procedure TformPersonasVIPNoInhabilitadas.ordenarColumna(Sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;


end.
