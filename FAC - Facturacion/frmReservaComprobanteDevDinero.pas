{----------------------------------------------------------------------------
                	frmComprobanteDevolucionDinero

Author		: mbecerra
Date		: 04-Marzo-2013
Description	:		(Ref: SS 959)
                Ventana que permite la pre-generaci�n del comprobante
                de devoluci�n de dinero

				Los comprobantes pregenerados, podr�n luego se generados e
                impresos desde la ventana de Cobranzas

     Author		: mvillarroel
    Date		: 07-Julio-2013
    Firma   : SS_660_MVI_20130711
    Description	:		(Ref: SS 660)
                    Se agrega validaci�n para indicar si un Convenio est� en Lista Amarilla

    Autor       :   CQuezadaI
    Fecha       :   23-07-2013
    Firma       :   SS_660_CQU_20130711
    Description	:   Se agrega el par�metro CodigoConcesionaria a la funci�n EstaConvenioEnListaAmarilla
                    Se modifica la posici�n del Label que indica si estpa en ListaAmarilla

    Firma       : SS_660_MVI_20130729
    Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

    Firma       :   SS_1120_MVI_20130820
    Descripcion :   Se cambia el llamado a llenar el combo de convenios para que utilice el procedimiento
                    CargarConveniosRUT.
                    Se modifica el procedimiento OnDrawItem para que llame al procedimeinto ItemComboBoxColor.
                    Adem�s se coment� el procedimeinto ItemCBColor ya que el nuevo procedimeinto realizar�
                    la tarea de colorear los conveniso de baja.

    Firma       :   SS_660_MVI_20130909
    Descripcion :   Se realiza la limpieza del label lista amarilla, dentro de la limpieza de los convenios.
                    Y Se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                    Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.


    Firma       :   SS_1147_MBE_20150427
    Description :   Se agrega a las reglas que tenga o un Recibo o un Saldo Acreedor

    Firma       : SS_1408_MCA_20151027
    Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

-----------------------------------------------------------------------------}
unit frmReservaComprobanteDevDinero;

interface

uses
  DMConnection,
  PeaProcs,
  UtilProc,
  UtilDB,
  Util,
  BuscaClientes,
  frmReporteCompDevDinero,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DmiCtrls, Validate, DateEdit, DB, ADODB,
  ExtCtrls, ListBoxEx, DBListEx, ComCtrls, PeaTypes;                              //SS_1120_MVI_20130820

type
  TReservaComprobanteDevDineroForm = class(TForm)
    spObtenerConvenios: TADOStoredProc;
    spObtenerReservaComprobantesDV: TADOStoredProc;
    dsObtenerReservaComprobantesDV: TDataSource;
    spInsertarReservaComprobantesDV: TADOStoredProc;
    PageControl1: TPageControl;
    tbsListado: TTabSheet;
    tbsReserva: TTabSheet;
    Panel1: TPanel;
    lblRutClienteFiltro: TLabel;
    lblNumeroReservaDVFiltro: TLabel;
    lblNumeroComprobanteFiltro: TLabel;
    lblNumeroReciboFiltro: TLabel;
    edRutClienteFiltro: TEdit;
    edConvenioFiltro: TEdit;
    btnFiltrar: TButton;
    edNumeroReciboFiltro: TNumericEdit;
    edNumeroReservaDVFiltro: TNumericEdit;
    chkMostrarComprobantesNOVigentes: TCheckBox;
    GroupBox1: TGroupBox;
    dblComprobantes: TDBListEx;
    lbl1: TLabel;
    peRutCliente: TPickEdit;
    lbl5: TLabel;
    lblSaldoConvenio: TLabel;
    vcbConvenios: TVariantComboBox;
    lbl2: TLabel;
    lbl3: TLabel;
    deFechaEmision: TDateEdit;
    lblRecibo: TLabel;
    edNumeroRecibo: TNumericEdit;
    lbl4: TLabel;
    edImporte: TNumericEdit;
    btnLimpiar: TButton;
    btnGrabar: TButton;
    Label1: TLabel;
    Panel2: TPanel;
    btnSalir: TButton;
    Label2: TLabel;
    btnImprimir: TButton;
    btnEliminar: TButton;
    lblEnListaAmarilla: TLabel;
    rbRecibo: TRadioButton;
    rbSaldoAcreedor: TRadioButton;                                           //SS_660_MVI_20130711
    procedure peRutClienteButtonClick(Sender: TObject);
    procedure ValidaBusca(Sender: TObject; var Key: Char);
    procedure vcbConveniosChange(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    function ObtenerRutClienteFiltro    : string;
    function ObtenerConvenioFiltro      : string;
    function ObtenerNumeroReciboFiltro  : Integer;
    function ObtenerNumeroReservaFiltro : Integer;
    function ObtenerIncluyeInactivosFiltro : Boolean;
    procedure FiltrarDatosBusqueda;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLimpiarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure OrdenarColumna(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure dblComprobantesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure rbReciboClick(Sender: TObject);
    procedure rbSaldoAcreedorClick(Sender: TObject);
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
  public
    { Public declarations }
    function Inicializar : Boolean;
    procedure BuscarConvenios;
    procedure MuestraError(CodigoError : Integer);
    procedure LimpiarDatos;
    procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor; Bold: Boolean);
    
  end;

var
  ReservaComprobanteDevDineroForm: TReservaComprobanteDevDineroForm;

implementation

{$R *.dfm}

{----------------------------------------------------------------------------
                	QueryGetValueBool

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Devuelve un valor booleano de la consulta
-----------------------------------------------------------------------------}
function QueryGetValueBool(Conn : TADOConnection; cSQL : string) : Boolean;
var
    aQry : TADOQuery;
begin
	Result := False;
    aQry := TADOQuery.Create(nil);
    aQry.SQL.Text := cSQL;
    aQry.Connection := Conn;
    aQry.Open;
    if not aQry.Eof then begin
       	Result := aQry.Fields[0].AsBoolean;
    end;
    aQry.Close;
    aQry.Free;
end;

{----------------------------------------------------------------------------
                	Inicializar

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Inicializa la ventana que permite la creaci�n e
                impresi�n del comprobante de devoluci�n de dinero
-----------------------------------------------------------------------------}
function TReservaComprobanteDevDineroForm.Inicializar;
resourcestring
    MSG_CAPTION		= 'Reserva de Devoluci�n de Dinero';
begin
	Result := True;
    CenterForm(Self);
    Caption := MSG_CAPTION;
    LimpiarDatos;
    PageControl1.ActivePage := tbsListado;
    deFechaEmision.Enabled  := ExisteAcceso('FECHA_EMISION_DEVOLUCION_DINERO');
    btnEliminar.Enabled     := ExisteAcceso('PUEDE_ELIMINAR_RESERVA_DEV_DINERO');
    lblEnListaAmarilla.Visible:=False;                                                    //SS_660_MVI_20130711

    rbRecibo.Checked := True;                                                             // SS_1147_MBE_20150427
    rbRecibo.OnClick(nil);                                                                // SS_1147_MBE_20150427
end;

{----------------------------------------------------------------------------
                	MuestraError

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Despliega un mensaje acorde al c�digo de error retornado por
              el stored InsertarReservaComprobanteDV
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.MuestraError(CodigoError : Integer);
resourcestring
//	MSG_MENOS_UNO		= 'Debe ingresar el n�mero de recibo.';                                            // SS_1147_MBE_20150427
    MSG_MENOS_UNO		= 'Debe ingresar el n�mero de recibo o Comprobante SA';                            // SS_1147_MBE_20150427
    MSG_MENOS_DOS       = 'Debe ingresar el importe.';
    MSG_MENOS_TRES      = 'Debe ingresar el convenio.';
    MSG_MENOS_CUATRO    = 'Debe ingresar la fecha de solicitud';
    MSG_MENOS_CINCO		= 'Interno: no se ha indicado el Usuario Creaci�n';
    MSG_MENOS_SEIS      = 'La fecha de Emisi�n debe ser mayor o igual que hoy.';
    MSG_MENOS_SIETE		= 'No puede hacer una reserva si el convenio est� dado de baja y no hay saldo a favor del cliente';
    MSG_MENOS_OCHO	    = 'El cliente no posee saldo a favor para la devoluci�n de dinero';  //NK pagada y no vencida
    MSG_MENOS_NUEVE		= 'El monto indicado excede el saldo del convenio';
//    MSG_MENOS_DIEZ		= 'El recibo se encuentra anulado';                                           // SS_1147_MBE_20150427
    MSG_MENOS_DIEZ		= 'El recibo o comprobante SA se encuentra anulado';                              // SS_1147_MBE_20150427
//    MSG_MENOS_ONCE		= 'El Recibo indicado no pertenece al convenio';                              // SS_1147_MBE_20150427
    MSG_MENOS_ONCE		= 'El Recibo o comprobante SA indicado no pertenece al convenio';                 // SS_1147_MBE_20150427
    MSG_MENOS_DOCE		= 'La suma de las reservas excede el saldo disponible del convenio';
    MSG_MENOS_TRECE		= 'Error al crear la reserva.';
    
begin
	case CodigoError of
    	-1 : MsgBoxBalloon(MSG_MENOS_UNO, Caption, MB_ICONERROR, edNumeroRecibo);
        -2 : MsgBoxBalloon(MSG_MENOS_DOS, Caption, MB_ICONERROR, edImporte);
        -3 : MsgBoxBalloon(MSG_MENOS_TRES, Caption, MB_ICONERROR, vcbConvenios);
        -4 : MsgBoxBalloon(MSG_MENOS_CUATRO, Caption, MB_ICONERROR, deFechaEmision);
        -5 : MsgBox(MSG_MENOS_CINCO, Caption, MB_ICONERROR);
        -6 : MsgBoxBalloon(MSG_MENOS_SEIS, Caption, MB_ICONERROR, deFechaEmision);
        -7 : MsgBoxBalloon(MSG_MENOS_SIETE, Caption, MB_ICONERROR, vcbConvenios);
        -8 : MsgBox(MSG_MENOS_OCHO, Caption, MB_ICONERROR);
        -9 : MsgBoxBalloon(MSG_MENOS_NUEVE, Caption, MB_ICONERROR, edImporte);
        -10 : MsgBoxBalloon(MSG_MENOS_DIEZ, Caption, MB_ICONERROR, edNumeroRecibo);
        -11 : MsgBoxBalloon(MSG_MENOS_ONCE, Caption, MB_ICONERROR, edNumeroRecibo);
        -12 : MsgBox(MSG_MENOS_DOCE, Caption, MB_ICONERROR);
        -13 : MsgBox(MSG_MENOS_TRECE, Caption, MB_ICONERROR);
    end;
end;

{----------------------------------------------------------------------------
                	ItemCBColor

Author		: mbecerra
Date		: 04-Marzo-2013
Description	:		(Ref: SS 959)
              Colorea los datos de una variant ComboBox
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor; Bold: Boolean);
begin
    with (cmbBox as TVariantComboBox) do begin
        Canvas.Brush.color := ColorFondo;
        Canvas.FillRect(R);
        Canvas.Font.Color := ColorTexto;
        if Bold then begin
        	Canvas.Font.Style := Canvas.Font.Style + [fsBold]
        end
        else begin
        	Canvas.Font.Style := Canvas.Font.Style - [fsBold]
        end;

        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption + Util.iif(Bold, ' (Baja)', ''));
   end;
end;

{---------------------------------------------------
            OrdenarColumna

Author		: mbecerra
Date		: 04-Marzo-2013
Description	:		(Ref: SS 959)
    			Permite ordenarla columnas de una grilla
-----------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.OrdenarColumna(Sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;                                                                                                                            

{----------------------------------------------------------------------------
                	ObtenerConvenioFiltro

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Devuelve el convenio seleccionado  en la comboBox
-----------------------------------------------------------------------------}
function TReservaComprobanteDevDineroForm.ObtenerConvenioFiltro: string;
begin
    Result := edConvenioFiltro.Text;
end;

{----------------------------------------------------------------------------
                	ObtenerIncluyeInactivosFiltro

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Indica si el checkbox est� marcado o no
-----------------------------------------------------------------------------}
function TReservaComprobanteDevDineroForm.ObtenerIncluyeInactivosFiltro: Boolean;
begin
    Result := chkMostrarComprobantesNOVigentes.Checked;
end;

{----------------------------------------------------------------------------
                	ObtenerNumeroReciboFiltro

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Devuelve el valor del n�mero de recibo
-----------------------------------------------------------------------------}
function TReservaComprobanteDevDineroForm.ObtenerNumeroReciboFiltro: Integer;
begin
    Result := edNumeroReciboFiltro.ValueInt;                                                                                    
end;

{----------------------------------------------------------------------------
                	ObtenerNumeroReservaFiltro

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Devuelve el valor del n�mero de reserva
-----------------------------------------------------------------------------}
function TReservaComprobanteDevDineroForm.ObtenerNumeroReservaFiltro: Integer;
begin
    Result := edNumeroReservaDVFiltro.ValueInt;                                                                                      
end;

{----------------------------------------------------------------------------
                	ObtenerRutClienteFiltro

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Devuelve el valor del Rut de cliente
-----------------------------------------------------------------------------}
function TReservaComprobanteDevDineroForm.ObtenerRutClienteFiltro: string;
begin
    Result := edRutClienteFiltro.Text;
end;

{----------------------------------------------------------------------------
                	LimpiarDatos

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Limpia los campos de la pantalla
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.LimpiarDatos;
begin
    peRutCliente.Text := '';
    vcbConvenios.Clear;
    deFechaEmision.Date := NowBase(DMConnections.BaseCAC);
    edNumeroRecibo.Clear;
    edImporte.Clear;                                                            
    lblSaldoConvenio.Caption := '';
    lblEnListaAmarilla.Visible  := False;                                               //SS_660_MVI_20130729
end;

{----------------------------------------------------------------------------
                	BuscarConvenio

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Busca los convenios asociados al Rut ingresado en el campo
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.BuscarConvenios;
resourcestring
	MSG_NO_HAY_DATOS = 'No se encontraron convenios para el %s Indicado';
    MSG_ERROR = 'Error al obtener los convenios asociados al %s';

begin
	try
    	vcbConvenios.Clear;
  //  	spObtenerConvenios.Close;                                                                              //SS_1120_MVI_20130820
  //      spObtenerConvenios.Parameters.Refresh;                                                               //SS_1120_MVI_20130820
  //      spObtenerConvenios.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';                        //SS_1120_MVI_20130820
  //  	spObtenerConvenios.Parameters.ParamByName('@NumeroDocumento').Value := peRutCliente.Text;              //SS_1120_MVI_20130820
  //      spObtenerConvenios.Parameters.ParamByName('@SoloActivos').Value := False;                            //SS_1120_MVI_20130820
  //  	spObtenerConvenios.Open;                                                                               //SS_1120_MVI_20130820
  //  	if not spObtenerConvenios.IsEmpty then begin                                                           //SS_1120_MVI_20130820
  //      	while not spObtenerConvenios.Eof do begin                                                          //SS_1120_MVI_20130820
	//            vcbConvenios.Items.Add(	spObtenerConvenios.FieldByName('NumeroConvenio').AsString,         //SS_1120_MVI_20130820
  //                          			spObtenerConvenios.FieldByName('CodigoConvenio').AsString );           //SS_1120_MVI_20130820
  //      		spObtenerConvenios.Next;                                                                       //SS_1120_MVI_20130820
  //      	end;                                                                                               //SS_1120_MVI_20130820
  //       spObtenerConvenios.Close;                                                                           //SS_1120_MVI_20130820

         EstaClienteConMensajeEspecial(DMCOnnections.BaseCAC, PadL(peRutCliente.Text, 9, '0' ));               //SS_1408_MCA_20151027
                                                                                                               //SS_1120_MVI_20130820
         CargarConveniosRUT(DMCOnnections.BaseCAC,vcbConvenios, 'RUT',                                         //SS_1120_MVI_20130820
            PadL(peRutCliente.Text, 9, '0' ));                                                                 //SS_1120_MVI_20130820
                                                                                                               //SS_1120_MVI_20130820
            if vcbConvenios.Items.Count > 0 then begin
            	vcbConvenios.SetFocus;
                vcbConvenios.ItemIndex := 0;
                if Assigned(vcbConvenios.OnChange) then vcbConvenios.OnChange(nil);                            //SS_1120_MVI_20130820
            end;

  //  	end                                                                                                  	//SS_1120_MVI_20130820
  //  	else MsgBox(Format(MSG_NO_HAY_DATOS,[peRutCliente.Text]), Caption, MB_ICONEXCLAMATION);              	//SS_1120_MVI_20130820
    except on e:exception do begin
        	MsgBoxErr(Format(MSG_ERROR,[peRutCliente.Text]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

procedure TReservaComprobanteDevDineroForm.FiltrarDatosBusqueda;
resourcestring
	MSG_NO_HAY_REGISTROS = 'No se encontraron registros para los criterios de b�squeda indicados';
    
begin
    if spObtenerReservaComprobantesDV.Active then spObtenerReservaComprobantesDV.Close;

    with spObtenerReservaComprobantesDV.Parameters do begin
        Refresh;
        ParamByName('@IdReservaComprobanteDV').Value	  	:= iif(ObtenerNumeroReservaFiltro = 0 , Null, ObtenerNumeroReservaFiltro);
        ParamByName('@NumeroConvenio').Value                := iif(ObtenerConvenioFiltro = '' , Null, ObtenerConvenioFiltro);
        ParamByName('@RutCliente').Value                    := iif(ObtenerRutClienteFiltro = '' , Null, ObtenerRutClienteFiltro);
        ParamByName('@NumeroRecibo').Value                  := iif(ObtenerNumeroReciboFiltro = 0, Null, ObtenerNumeroReciboFiltro);
        ParamByName('@FechaSolicitud').Value                := Null;
        ParamByName('@FechaHoraImpresionDV').Value          := Null;
        ParamByName('@IncluyeComprobanteInactivo').Value    := ObtenerIncluyeInactivosFiltro();
        ParamByName('@IncluyeReservasEliminadas').Value     := 1;													
    end;
    
    spObtenerReservaComprobantesDV.Open;
    if spObtenerReservaComprobantesDV.IsEmpty then MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONEXCLAMATION);
    
end;

procedure TReservaComprobanteDevDineroForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

{----------------------------------------------------------------------------
                	btnAceptarClick

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Genera e imprime el comprobanted e devoluci�n de dinero
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.btnGrabarClick(Sender: TObject);
resourcestring
	MSG_CAPTION     = 'Validaciones';
    MSG_CONTINUAR	= 'Se va a crear una Reserva de Devoluci�n de dinero para este convenio, �Desea Continuar?';
    MSG_ERROR		= 'Ocurri� un error al Crear la Reserva de Devoluci�n de Dinero';
    MSG_EXITO		= 'Se ha creado exitosamente la Reserva de devoluci�n de dinero n�mero %d';
    MSG_RECIBO      = 'Recibo';																		   // SS_1147_MBE_20150427
    MSG_SALDOACRE	= 'Saldo Acreedor';																   // SS_1147_MBE_20150427

    //mensajes de validaciones
    MSG_NO_ESTA_RUT			= 'Falta el Rut del cliente';
    MSG_NO_ESTA_CONVENIO	= 'Falta el convenio del cliente';
    MSG_NO_ESTA_FECHA		= 'Falta la Fecha de Emisi�n';
//    MSG_NO_ESTA_NRO_RECIBO	= 'Debe indicar el n�mero de recibo';                                  // SS_1147_MBE_20150427
    MSG_NO_ESTA_NRO_RECIBO	= 'Debe indicar el n�mero de ';                     						// SS_1147_MBE_20150427
    MSG_NO_ESTA_IMPORTE		= 'Debe ingresar un Importe mayor que cero';
    MSG_FECHA_MAYOR_HOY     = 'La fecha de Emisi�n debe ser mayor o igual que hoy.';

var
    Resultado           : Integer;
    NumeroComprobante   : Integer;                                                                         
    FechaHoy            : TDate;
    Mensaje				: string;																	   // SS_1147_MBE_20150427
    
begin
    FechaHoy := Trunc(NowBase(DMConnections.BaseCAC));
    
    if rbRecibo.Checked then Mensaje := MSG_NO_ESTA_NRO_RECIBO + MSG_RECIBO                            // SS_1147_MBE_20150427
    else Mensaje := MSG_NO_ESTA_NRO_RECIBO + MSG_SALDOACRE;                                            // SS_1147_MBE_20150427
    
	//Validar que se hayan ingresado todos los datos
    if not ValidateControls(
    						[	peRutCliente, vcbConvenios, deFechaEmision,
    							edNumeroRecibo, edImporte, deFechaEmision
    						],
                            [	peRutCliente.Text <> '', vcbConvenios.ItemIndex >= 0,
    							deFechaEmision.Date <> NullDate,
                                edNumeroRecibo.ValueInt > 0, edImporte.ValueInt > 0,
                                deFechaEmision.Date >= FechaHoy                                            
                            ],
                            MSG_CAPTION,
                            [	MSG_NO_ESTA_RUT, MSG_NO_ESTA_CONVENIO,
//                            	MSG_NO_ESTA_FECHA, MSG_NO_ESTA_NRO_RECIBO,								// SS_1147_MBE_20150427
                                MSG_NO_ESTA_FECHA, Mensaje,												// SS_1147_MBE_20150427
                                MSG_NO_ESTA_IMPORTE, MSG_FECHA_MAYOR_HOY                                    
                            ]
    					) then Exit;

    if MsgBox(MSG_CONTINUAR, Caption, MB_ICONQUESTION + MB_YESNO) = IDNO then Exit;

    try
      //Crear el comprobante
      //el stored tiene transaccionalidad
    	spInsertarReservaComprobantesDV.Close;
        with spInsertarReservaComprobantesDV do begin
        	Parameters.Refresh;
        	Parameters.ParamByName('@CodigoConvenio').Value					:= vcbConvenios.Value;
			Parameters.ParamByName('@NumeroRecibo').Value				    := IIf(edNumeroRecibo.ValueInt > 0, edNumeroRecibo.ValueInt, null);
            Parameters.ParamByName('@TipoRecibo').Value						:= IIf(rbRecibo.Checked, 'RC', 'SA'); 		// SS_1147_MBE_20150427
			Parameters.ParamByName('@FechaSolicitud').Value					:= deFechaEmision.Date;
			Parameters.ParamByName('@Importe').Value				        := edImporte.ValueInt * 100;
			Parameters.ParamByName('@UsuarioCreador').Value					:= UsuarioSistema;
            Parameters.ParamByName('@IdReservaComprobanteDV').Value         := Null;
            ExecProc;
            Resultado := Parameters.ParamByName('@RETURN_VALUE').Value;
            if Resultado < 0 then MuestraError(Resultado)
            else begin
            	NumeroComprobante := Parameters.ParamByName('@IdReservaComprobanteDV').Value;
            	//Lanzar la impresi�n
                Application.CreateForm(TReporteCompDevDineroForm, ReporteCompDevDineroForm);
                ReporteCompDevDineroForm.InicializarReserva(NumeroComprobante);
                MsgBox(Format(MSG_EXITO, [NumeroComprobante]), Caption, MB_ICONINFORMATION);
                ReporteCompDevDineroForm.Release;
                LimpiarDatos();
                FiltrarDatosBusqueda();                                                                                                            
            end;
        end;
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

{----------------------------------------------------------------------------
                	btnSalirClick

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Cierra la ventana actual
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{----------------------------------------------------------------------------
                	btnEliminarClick

Author		: mbecerra
Date		: 01-Marzo-2013
Description	: (Ref: SS 959)
             marca una reserva como eliminada
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.btnEliminarClick(Sender: TObject);
resourcestring
    MSG_RESERVA_YA_ELIMINADA        = 'No puede eliminar la reserva. La reserva ya se encuentra eliminada';
    MSG_RESERVA_USADA               = 'No puede eliminar la reserva. La reserva fue utilizada para generar un comprobante de devoluci�n de dinero';
    MSG_ESTA_SEGURO                 = '�Est� seguro de eliminar esta reserva [n�mero %d]?';
    MSG_RESERVA_HA_SIDO_ELIMINADA   = 'La reserva ha sido eliminada';

    MSG_SQL_ELIMINAR =  'UPDATE ReservaComprobantesDV SET Eliminada = 1, ' +
                        'UsuarioModificacion = ''%s'', FechaHoramodificacion = GETDATE() ' +
                        'WHERE IdReservaComprobanteDV = %d';

var
    IDReserva : Longint;
    
begin
    if spObtenerReservaComprobantesDV.IsEmpty or spObtenerReservaComprobantesDV.Eof then Exit;
    if spObtenerReservaComprobantesDV.FieldByName('Eliminada').AsBoolean then begin
        MsgBox(MSG_RESERVA_YA_ELIMINADA, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    if not spObtenerReservaComprobantesDV.FieldByName('NumeroComprobanteDVGenerado').IsNull then begin
        MsgBox(MSG_RESERVA_USADA, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    IDReserva := spObtenerReservaComprobantesDV.FieldByName('IdReservaComprobanteDV').AsInteger;
    if MsgBox(Format(MSG_ESTA_SEGURO, [IDReserva]), Caption, MB_YESNO + MB_ICONQUESTION) = IDNO then Exit;

    //Eliminar
    DMConnections.BaseCAC.Execute(Format(MSG_SQL_ELIMINAR, [UsuarioSistema, IDReserva]));
    MsgBox(MSG_RESERVA_HA_SIDO_ELIMINADA, Caption, MB_ICONINFORMATION);
    btnFiltrar.OnClick(nil);

    
end;

{----------------------------------------------------------------------------
                	btnFiltrarClick

Author		: alabra
Date		: 03-Agosto-2011
Description	: (Ref: SS 959)
             Obtiene las Reserva Comprobantes DV seg�n los filtros ingresados
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.btnFiltrarClick(Sender: TObject);
begin
    EstaClienteConMensajeEspecial(DMConnections.BaseCAC, PadL(Trim(edRutClienteFiltro.Text), 9, '0'));			//SS_1408_MCA_20151027
    FiltrarDatosBusqueda();
end;

{----------------------------------------------------------------------------
                	peRutClienteButtonClick

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Cierra la ventana actual 
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.peRutClienteButtonClick(
  Sender: TObject);
var
	f : TFormBuscaClientes;
    TeclaEnter : Char;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda) then begin
        f.ShowModal;
        if f.ModalResult = IDOK then begin
            FUltimaBusqueda := f.UltimaBusqueda;
            TPickEdit(Sender).Text := f.Persona.NumeroDocumento;
            TeclaEnter := #13;
            ValidaBusca(Sender, TeclaEnter);
        end;
    end;

    f.Free;

end;


{----------------------------------------------------------------------------
                	rbReciboClick

Author		: mbecerra
Date		: 27-Abril-2015
Description	:		(Ref: SS 1147)

              coloca la leyenda asociada al tipo de comprobante a buscar 
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.rbReciboClick(Sender: TObject);
begin
    lblRecibo.Caption := 'N�mero de Recibo:';
end;

{----------------------------------------------------------------------------
                	rbSaldoAcreedorClick

Author		: mbecerra
Date		: 27-Abril-2015
Description	:		(Ref: SS 1147)

              coloca la leyenda asociada al tipo de comprobante a buscar 
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.rbSaldoAcreedorClick(
  Sender: TObject);
begin
    lblRecibo.Caption := 'N�mero de Saldo Acreedor:';
end;

{----------------------------------------------------------------------------
                	ValidaBusca

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Cierra la ventana actual 
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.ValidaBusca(Sender: TObject;
  var Key: Char);
resourcestring
	MSG_RUT = 'El Rut %s ingresado es inv�lido';

begin
    if Key = #13 then begin
    	Key := #0;
        if ValidarRUT(DMConnections.BaseCAC, TPickEdit(Sender).Text) then begin
        	BuscarConvenios();
        end
        else begin
            MsgBox(Format(MSG_RUT,[TPickEdit(Sender).Text]), Caption, MB_ICONEXCLAMATION);
        end;
    end
    else begin
    	if not (Key in ['0'..'9', 'K', 'k', Char(VK_BACK)]) then Key := #0
        else begin
        	vcbConvenios.Clear;
            lblSaldoConvenio.Caption := '';
            lblEnListaAmarilla.Visible  := False;                                         //SS_660_MVI_20130909
            lblEnListaAmarilla.Caption  := EmptyStr;                                      //SS_660_MVI_20130909

        end;

    end;
end;

{----------------------------------------------------------------------------
                	vcbConveniosChange

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Muestra el Saldo del convenio. 
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.vcbConveniosChange(Sender: TObject);
resourcestring
	MSG_SQL_SALDO	= 'SELECT dbo.ObtenerSaldoDelConvenio( %d )';
  //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(''%s'',GETDATE() )';          // SS_660_CQU_20130711  //SS_660_MVI_20130711
  //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(''%s'',GETDATE(), NULL )';    //SS_660_MVI_20130729  // SS_660_CQU_20130711
   SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%s) ';                                                //SS_660_MVI_20130729
var
	Saldo : LongInt;
  EstadoListaAmarilla : String;                                                                                                           //SS_660_MVI_20130729
begin

   // lblEnListaAmarilla.Color:=clYellow;                                                                                       //SS_660_MVI_20130729            //SS_660_MVI_20130711
   // lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                                                    //SS_660_MVI_20130729           //SS_660_MVI_20130711
   //                                                     Format(SQL_ESTACONVENIOENLISTAAMARILLA,                               //SS_660_MVI_20130729              //SS_660_MVI_20130711
   //                                                             [   vcbConvenios.Value                                        //SS_660_MVI_20130729               //SS_660_MVI_20130711
   //                                                             ]                                                             //SS_660_MVI_20130729                //SS_660_MVI_20130711
   //                                                           )                                                               //SS_660_MVI_20130729              //SS_660_MVI_20130711
   //                                                  )='True';                                                                //SS_660_MVI_20130729              //SS_660_MVI_20130711

        EstadoListaAmarilla:= QueryGetValue(   DMConnections.BaseCAC,                                                                         //SS_660_MVI_20130729
                                                 Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                         [   IntToStr(vcbConvenios.Value)                                                    //SS_660_MVI_20130729
                                                         ]                                                                                   //SS_660_MVI_20130729
                                                       )                                                                                     //SS_660_MVI_20130729
                                                       );                                                                                    //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
//        if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
        if EstadoListaAmarilla <> '' then begin                                                                                              //SS_660_MVI_20130909
                                                                                                                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
        lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end                                                                                                                                  //SS_660_MVI_20130729
        else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end;                                                                                                                                 //SS_660_MVI_20130729
   

    lblSaldoConvenio.Caption := '';
    if vcbConvenios.ItemIndex >= 0 then begin
    	Saldo := vcbConvenios.Value;
        Saldo := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_SALDO, [Saldo]));
        lblSaldoConvenio.Caption := Format('$ %0.n', [Saldo / 100.0]);
    end;

end;

{----------------------------------------------------------------------------
                	btnLimpiarClick

Author		: mbecerra
Date		: 25-Mayo-2012
Description	:		(Ref: SS 959)
              Invoca a la funci�n para limpiar los datos de la reserva en pantalla.
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.btnLimpiarClick(Sender: TObject);
begin
    LimpiarDatos;
end;

{----------------------------------------------------------------------------
                	btnImprimirClick

Author		: mbecerra
Date		: 25-Mayo-2012
Description	:		(Ref: SS 959)
              Permite imprimir la reserva seleccionada en pantalla.
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.btnImprimirClick(Sender: TObject);
resourcestring
    MSG_NO_EXISTE_RESERVA				= 'No se puede imprimir una reserva que NO es del d�a actual';
    MSG_NO_SE_PUEDE_IMPRIMIR_ELIMINADA	= 'No se puede Imprimir una reserva eliminada';
    MSG_NO_SE_PUEDE_IMPRIMIR_YA_USADA	= 'No se puede imprimir una reserva ya utilizada en un comprobante Devoluci�n de Dinero';
begin
    if spObtenerReservaComprobantesDV.IsEmpty or spObtenerReservaComprobantesDV.Eof then Exit;

    if spObtenerReservaComprobantesDV.FieldByName('Eliminada').AsBoolean then begin
        MsgBox(MSG_NO_SE_PUEDE_IMPRIMIR_ELIMINADA, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    if not spObtenerReservaComprobantesDV.FieldByName('NumeroComprobanteDVGenerado').IsNull then begin
        MsgBox(MSG_NO_SE_PUEDE_IMPRIMIR_YA_USADA, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    if  (not spObtenerReservaComprobantesDV.FieldByName('IdReservaComprobanteDV').IsNull) and
        (spObtenerReservaComprobantesDV.FieldByName('FechaSolicitud').AsDateTime =  spObtenerReservaComprobantesDV.FieldByName('FechaDeHoy').AsDateTime) then begin
        Application.CreateForm(TReporteCompDevDineroForm, ReporteCompDevDineroForm);
        ReporteCompDevDineroForm.InicializarReserva(spObtenerReservaComprobantesDV.FieldByName('IdReservaComprobanteDV').AsInteger);
        ReporteCompDevDineroForm.Release;
    end
    else MsgBox(MSG_NO_EXISTE_RESERVA,Caption, MB_ICONEXCLAMATION);
end;


{----------------------------------------------------------------------------
                	dblComprobantesDrawText

Author		: mbecerra
Date		: 28-Febrero-2013
Description	:		(Ref: SS 959)
              Modifica la visualizaci�n de las columnas de la grilla
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.dblComprobantesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Eliminada' then begin
        if spObtenerReservaComprobantesDV.FieldByName('Eliminada').AsBoolean then Text := 'S�'
        else Text := '';
    end;

end;

{----------------------------------------------------------------------------
                	vcbConveniosDrawItem

Author		: mbecerra
Date		: 28-Febrero-2013
Description	:		(Ref: SS 959)
              Pone de color rojo los convenios de baja
-----------------------------------------------------------------------------}
procedure TReservaComprobanteDevDineroForm.vcbConveniosDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
//const                                                                                                         //SS_1120_MVI_20130820
//    	cSQLEsConvenioDeBaja = 'SELECT dbo.EsConvenioDeBaja(%d)';                                               //SS_1120_MVI_20130820
//    var                                                                                                       //SS_1120_MVI_20130820
//    	CodigoConvenio: Integer;                                                                                //SS_1120_MVI_20130820
//        FontColor,                                                                                            //SS_1120_MVI_20130820
//        BackGroundColor: TColor;                                                                              //SS_1120_MVI_20130820
//        FontBold : Boolean;                                                                                   //SS_1120_MVI_20130820
begin                                                                                                           //SS_1120_MVI_20130820
//	CodigoConvenio := vcbConvenios.Items[Index].Value;                                                          //SS_1120_MVI_20130820
                                                                                                                //SS_1120_MVI_20130820
//	if QueryGetValueBool(DMConnections.BaseCAC, Format(cSQLEsConvenioDeBaja,[CodigoConvenio])) then begin       //SS_1120_MVI_20130820
                                                                                                                //SS_1120_MVI_20130820
//        BackGroundColor := clWhite;                                                                           //SS_1120_MVI_20130820
//        FontColor 		:= clRed;                                                                           //SS_1120_MVI_20130820
//	    FontBold 		:= True;                                                                                //SS_1120_MVI_20130820
                                                                                                                //SS_1120_MVI_20130820
//    	if (Control as TVariantComboBox).DroppedDown then begin                                                 //SS_1120_MVI_20130820
//            if (odselected in State) then begin                                                               //SS_1120_MVI_20130820
//	        	BackGroundColor := clHighlight;                                                                 //SS_1120_MVI_20130820
//		        FontColor 		:= clWhite;                                                                     //SS_1120_MVI_20130820
//            end;                                                                                              //SS_1120_MVI_20130820
//        end;                                                                                                  //SS_1120_MVI_20130820
//    end                                                                                                       //SS_1120_MVI_20130820
//    else begin                                                                                                //SS_1120_MVI_20130820
                                                                                                                //SS_1120_MVI_20130820
//        BackGroundColor := clWhite;                                                                           //SS_1120_MVI_20130820
//        FontColor		:= clBlack;                                                                             //SS_1120_MVI_20130820
//        FontBold 		:= False;                                                                               //SS_1120_MVI_20130820
                                                                                                                //SS_1120_MVI_20130820
//    	if (Control as TVariantComboBox).DroppedDown then begin                                                 //SS_1120_MVI_20130820
//            if (odselected in State) then begin                                                               //SS_1120_MVI_20130820
//	        	BackGroundColor := clHighlight;                                                                 //SS_1120_MVI_20130820
//		        FontColor 		:= clWhite;                                                                     //SS_1120_MVI_20130820
//	        end;                                                                                                //SS_1120_MVI_20130820
//        end;                                                                                                  //SS_1120_MVI_20130820
//    end;                                                                                                      //SS_1120_MVI_20130820
                                                                                                                //SS_1120_MVI_20130820
//	ItemCBColor(Control, Index, Rect, BackGroundColor, FontColor, FontBold);                                    //SS_1120_MVI_20130820
                                                                                                                //SS_1120_MVI_20130820
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin             				//SS_1120_MVI_20130820
                                                                                              					//SS_1120_MVI_20130820
     ItemComboBoxColor(Control, Index, Rect, State, clred, true);                    							//SS_1120_MVI_20130820
                                                                                              					//SS_1120_MVI_20130820
  end                                                                                         					//SS_1120_MVI_20130820
  else begin                                                                                  					//SS_1120_MVI_20130820
                                                                                              					//SS_1120_MVI_20130820
     ItemComboBoxColor(Control, Index, Rect, State);                                          					//SS_1120_MVI_20130820
                                                                                              					//SS_1120_MVI_20130820
  end;                                                                                        					//SS_1120_MVI_20130820
                                                                                              					//SS_1120_MVI_20130820
end;

end.
