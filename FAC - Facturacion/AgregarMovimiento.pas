unit AgregarMovimiento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DmiCtrls, Grids, DBGrids, DPSGrid,
  ImgList, DB, ADODB, validate, Dateedit, DBClient, util, PeaProcs, utilProc,
  DMConnection, peaTypes, DPSControls, UtilFacturacion;

type
  TfrmAgregarMovimientoAjuste = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cbCuentas: TComboBox;
    dbgTransitos: TDPSGrid;
    Label4: TLabel;
    lTransitosSeleccionados: TLabel;
    rgTipoAjuste: TRadioGroup;
    rbMovimientoPersonal: TRadioButton;
    neImporte: TNumericEdit;
    cbConceptosMovimiento: TComboBox;
    Label2: TLabel;
    Label5: TLabel;
    rbMovimientoPorTransito: TRadioButton;
    Imagenes: TImageList;
    Label6: TLabel;
    eObservaciones: TEdit;
    neImporteTransitos: TNumericEdit;
	Label7: TLabel;
    deFechaMovimiento: TDateEdit;
    Label3: TLabel;
    ObtenerTransitosComprobante: TADOStoredProc;
    cdsTransitos: TClientDataSet;
    dsTransitos: TDataSource;
    btnMarcarTodo: TDPSButton;
    btnInvertir: TDPSButton;
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
    ObtenerCuentasAjuste: TADOStoredProc;
    procedure cbCuentasChange(Sender: TObject);
    procedure dbgTransitosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgTransitosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgTransitosColEnter(Sender: TObject);
	procedure dbgTransitosColExit(Sender: TObject);
	procedure dbgTransitosDblClick(Sender: TObject);
	procedure btnMarcarTodoClick(Sender: TObject);
	procedure btnInvertirClick(Sender: TObject);
	procedure dbgTransitosExit(Sender: TObject);
	procedure rbMovimientoPersonalClick(Sender: TObject);
	procedure rbMovimientoPorTransitoClick(Sender: TObject);
	procedure cdsTransitosBeforePost(DataSet: TDataSet);
	procedure cdsTransitosAfterPost(DataSet: TDataSet);
	procedure cdsTransitosAfterEdit(DataSet: TDataSet);
	procedure FormActivate(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
	{ Private declarations }
	FImporteAnterior: Double;
	FCodigoConcepto, FTransitosSeleccionados: Integer;
	FEstadoCambio, FCargando: Boolean;
	FFechaHora: TDateTime;
    FNumeroComprobanteAAjustar : Int64;

	procedure MarcarTransito(MarcarTransito: Boolean);
  public
	{ Public declarations }
    function Inicializa(CodigoCLiente, CodigoConvenio, IndiceVehiculo : Integer;
      cdsTransitosAjustados: TClientDataSet; ModoPago: AnsiString; TipoAjuste, NumeroMovimiento: Integer;
      Fecha: TDateTime; Observaciones: AnsiString; CodigoConcepto: Integer; Importe: Double; NumeroComprobanteAAjustar : Int64): Boolean;
  end;

var
  frmAgregarMovimientoAjuste: TfrmAgregarMovimientoAjuste;

    function AgregarMovimientoAjuste(
      CodigoCLiente, CodigoConvenio: Integer;
      ModoPago: AnsiString;
      TipoAjuste: Integer;
      cdsTransitosAjustados: TClientDataSet;
      NumeroMovimiento: Integer;
      NumeroComprobanteAAjustar : Int64;
      var IndiceVehiculo: Integer; var Fecha: TDateTime; var Observaciones: AnsiString;
      var Importe: Double; var CodigoConcepto: Integer; var Patente : String): Boolean;
implementation

const CM_AJUSTE_FACTURACION = 9;
resourcestring
    CAPTION_VALIDAR_IMPORTE = 'Validar Importe del Movimiento';
{$R *.dfm}


function AgregarMovimientoAjuste(
  CodigoCLiente, CodigoConvenio: Integer;
  ModoPago: AnsiString;
  TipoAjuste: Integer;
  cdsTransitosAjustados: TClientDataSet;
  NumeroMovimiento: Integer;
  NumeroComprobanteAAjustar : Int64;
  var IndiceVehiculo: Integer; var Fecha: TDateTime; var Observaciones: AnsiString;
  var Importe: Double; var CodigoConcepto: Integer; var Patente : String): Boolean;
var
  F: TFrmAgregarMovimientoAjuste;
begin
	result := false;
	application.CreateForm(TFrmAgregarMovimientoAjuste,F);
	try
		with F do begin
			if Inicializa(CodigoCLiente, CodigoConvenio, IndiceVehiculo, CdsTransitosAjustados, ModoPago, TipoAjuste,
						  NumeroMovimiento, Fecha, Observaciones, CodigoConcepto, Importe, NumeroComprobanteAAjustar) then begin
				ShowModal;
				if ModalResult = idOk then begin
					btnAceptar.cursor := crHourGlass;
					cdsTransitosAjustados.Filter := 'IndiceVehiculo = ' + intToStr(IndiceVehiculo) +
					  ' and NumeroMovimiento = ' + IntToStr(NumeroMovimiento);
					cdsTransitosAjustados.first;
					while not cdsTransitosAjustados.eof do
						cdsTransitosAjustados.delete;
					if FFechaHora = 0 then
						Fecha := deFechaMovimiento.date
					else
						Fecha := FFechaHora;
					IndiceVehiculo := IVal(StrRight(cbCuentas.Items[cbCuentas.ItemIndex], 20));
                    Patente := Trim(StrLeft(StrRight(cbCuentas.Items[cbCuentas.ItemIndex], 150),100));
					if rbMovimientoPorTransito.Checked then begin
						CodigoConcepto := CM_AJUSTE_FACTURACION;
						Importe := -1 * neImporteTransitos.Value;
						cdsTransitos.Filter := 'Marcado = true';
						cdsTransitos.First;
						while not cdsTransitos.eof do begin
							cdsTransitosAjustados.appendRecord([
												   cdsTransitos.FieldByName('IndiceVehiculo').asInteger,
												   NumeroMovimiento,
												   cdsTransitos.FieldByName('CodigoConcesionaria').asInteger,
												   cdsTransitos.FieldByName('NumeroViaje').asInteger,
												   cdsTransitos.FieldByName('NumeroPuntoCobro').asInteger,
												   cdsTransitos.FieldByName('ImporteAAjustar').asFloat,
												   cdsTransitos.FieldByName('FechaHora').asString,
												   cdsTransitos.FieldByName('DescriConcesionaria').asString,
												   cdsTransitos.FieldByName('DescriPuntoCobro').asString,
												   cdsTransitos.FieldByName('Categoria').asString,
												   cdsTransitos.FieldByName('DescriImporte').AsString]);
							cdsTransitos.next;
                        end;
					end else begin
						CodigoConcepto := IVal(StrRight(cbConceptosMovimiento.Items[cbConceptosMovimiento.ItemIndex], 20));
						if (rgTipoAjuste.ItemIndex = 1) then
							Importe := -1 * neImporte.Value
						else
							Importe := Abs(neImporte.Value);
					end;
					Observaciones := eObservaciones.text;
					btnAceptar.cursor := crDefault;
					result := true;
				end else begin
					result := false;
					btnAceptar.cursor := crDefault;
				end;
			end;
		end;
	finally
		F.free;
	end;
end;


Function TfrmAgregarMovimientoAjuste.Inicializa(CodigoCLiente, CodigoConvenio, IndiceVehiculo : Integer;
  cdsTransitosAjustados: TClientDataSet; ModoPago: AnsiString; TipoAjuste, NumeroMovimiento: Integer;
  Fecha: TDateTime; Observaciones: AnsiString; CodigoConcepto: Integer; Importe: Double; NumeroComprobanteAAjustar : Int64): Boolean;

resourcestring
    CAPTION_MOVIMIENTOS  = 'Agregar Movimiento a Facturar Proximamente';
    CAPTION_NOTA_CREDITO = 'Agregar Movimiento para Nota de Cr�dito';
    CAPTION_NOTA_DEBITO  = 'Agregar Movimiento para Nota de D�bito';
    MSG_COL_TITLE_SALDO  = 'Saldo';
    MSG_COL_TITLE_ITE_A_AJUSTAR = 'Ite. a Ajustar';
    MSG_TODAS_CUENTAS    = 'TODAS LAS CUENTAS';



  procedure CargarCuentasSegunAjuste(IndiceVehiculo : Integer);
  var
    auxIndice : Integer;
    PatenteAnterior: string;
  begin
	  // Si existe un comprobante seleccionado
//	  if (Trim(TipoComprobante) <> '') then begin
        auxIndice := 0;
		  with ObtenerCuentasAjuste do begin
			  Parameters.ParamByName('@CodigoConvenio').value := CodigoConvenio;
			  Parameters.ParamByName('@TraerBaja').value := True;
			  open;
             cbCuentas.Items.Add(
                      MSG_TODAS_CUENTAS+
                      space(200) +
                      MSG_TODAS_CUENTAS +
                      space(100) +
                      '-1');

              PatenteAnterior := '';
			  while not eof do begin
                  if (PatenteAnterior <> Trim(FieldByName('Patente').asString)) then begin
    				  cbCuentas.Items.Add(trim(FieldByName('DescripcionMarca').asString) + ' - ' +
    									  Trim(FieldByName('Patente').asString ) +
                                          ' ('+Trim(SerialNumberToEtiqueta(FieldByName('ContractSerialNumber').AsString))+ ')' + space(200) +
                                          FieldByName('Patente').asString + space(100) +
    									  (trim(FieldByName('IndiceVehiculo').asString)));
                      if IndiceVehiculo = FieldByName('IndiceVehiculo').AsInteger then
                        auxIndice := cbCuentas.Items.Count - 1;
                  end;
                  PatenteAnterior := Trim(FieldByName('Patente').asString);
    			  next;
			  end;
			  close;
              cbCuentas.ItemIndex := auxIndice;
		  end;
//	  end else begin
//		  // Si no existe un comprobante seleccionado preguntamos por el Tipo de Ajuste
//		  // Si es una nota de cr�dito o una nota de debito cargamos segun el debito especificado
//		  if (TipoAjuste = 2) then begin
//			  if (ModoPago = TP_POSPAGO_MANUAL) then
//				  CargarCuentas(DMConnections.BaseCAC, cbCuentas, CodigoCliente, TP_POSPAGO_MANUAL, 0, 0)
//			  else
//				  CargarCuentas(DMConnections.BaseCAC, cbCuentas, CodigoCliente, TP_DEBITO_AUTOMATICO,
//					CodigoDebitoAutomatico, 0);
//		  end else begin
//			  // Si no hay un comprobante asociado y el ajuste se hace agregando movimientos
//			  // cargamos todas las cuentas no prepagas
//			  with QryCuentas do begin
//				  Parameters.ParamByName('CodigoCliente').value := CodigoCliente;
//				  open;
//				  while not eof do begin
//					  DatosVehiculo := BuscarDatosVehiculo(DMConnections.BaseCAC,FieldByName('CodigoCuenta').value);
//					  cbCuentas.Items.Add(trim(FieldByName('CodigoCuenta').asString) + ' - ' +
//										  DatosVehiculo + space(200) +
//										  trim(FieldByName('CodigoCuenta').asString));
//					  next;
//				  end;
//				  close;
//			  end;
//		  end;
//	  end;
  end;

var ImporteAAjustar, importeAjustado, Saldo: Double;
	i: Integer;
begin
	result := true;

    with dbgTransitos do begin
        Columns[1].Title.Caption := MSG_COL_TITLE_HORA;
        Columns[2].Title.Caption := MSG_COL_TITLE_PUNTO_COBRO;
        Columns[3].Title.Caption := MSG_COL_TITLE_CATEGORIA;
        Columns[4].Title.Caption := MSG_COL_TITLE_IMPORTE;
        Columns[5].Title.Caption := MSG_COL_TITLE_SALDO;
        Columns[6].Title.Caption := MSG_COL_TITLE_ITE_A_AJUSTAR;
    end;


	try
		FTransitosSeleccionados := 0;
		neImporteTransitos.Value := 0;
        FNumeroComprobanteAAjustar := NumeroComprobanteAAjustar;
		// Cargamos los conceptos
		CargarConceptosExternos(DMConnections.BaseCAC, cbConceptosMovimiento, 0);
		cbConceptosMovimiento.ItemIndex := 0;
		// Cargamos las cuentas segun el tipo de ajuste
		CargarCuentasSegunAjuste(IndiceVehiculo);


		// Asignamos los campos generales (Fecha, Caption, Observaciones y Codigo de Concepto)
		if TipoAjuste = 0 then begin
			deFechaMovimiento.enabled := True;
			deFechaMovimiento.date := fecha;
			FFechaHora := 0;
			Caption := CAPTION_MOVIMIENTOS;
		end else if TipoAjuste = 1 then begin
			deFechaMovimiento.enabled := false;
			FFechaHora := NowBase(DMConnections.BaseCAC);
			deFechaMovimiento.date := FFechaHora;
			Caption := CAPTION_NOTA_CREDITO;
		end else begin
			deFechaMovimiento.enabled := false;
			FFechaHora := NowBase(DMConnections.BaseCAC);
			deFechaMovimiento.date := FFechaHora;
			Caption := CAPTION_NOTA_DEBITO;
		end;
		eObservaciones.Text := Observaciones;
		FCodigoConcepto := CodigoConcepto;

		// Cargamos las concesionarias seg�n los viajes del comprobante asociado
//		with qryConcesionarias do begin
//			Parameters.ParamByName('TipoComprobante').value := TipoComprobante;
//			Parameters.ParamByName('NumeroComprobante').value := NumeroComprobante;
//			Open;
//			first;
//			while not eof do begin
//				cbConcesionarias    .Items.Add(Trim(FieldByName('Descripcion').asString) + space(200) +
//				  FieldByName('CodigoConcesionaria').asString);
//				next;
//			end;
//			first;
//			if not eof then
//				cbConcesionarias.ItemIndex := 0;
//			close;
//		end;

		// Si estamos modificando un movimiento generado por transitos habilitamos la lista de transitos
		// y deshabilitamos el concepto y el importe
		if CodigoConcepto = CM_AJUSTE_FACTURACION then begin
//			cbConcesionarias.Enabled := True;
			dbgTransitos.Enabled := True;
			btnMarcarTodo.Enabled := True;
			btnInvertir.Enabled := True;
			neImporte.value := 0;
			neImporte.enabled := False;
			cbConceptosMovimiento.ItemIndex := -1;
			cbConceptosMovimiento.enabled := False;
			rgTipoAjuste.Enabled := False;
		end else begin
//			cbConcesionarias.Enabled := False;
			dbgTransitos.Enabled := False;
			btnMarcarTodo.Enabled := False;
			btnInvertir.Enabled := False;
			cbConceptosMovimiento.enabled := True;
			i := 0;
			while CodigoConcepto = IVal(StrRight(cbConceptosMovimiento.Items[i], 20)) do
				inc(i);
			cbConceptosMovimiento.ItemIndex := i;
			neImporte.enabled := True;
			rgTipoAjuste.Enabled := True;
			rgTipoAjuste.ItemIndex := iif(Importe < 0, 1, 0);
			neImporte.value := iif(Importe < 0, -1 * Importe, Importe);
		end;

		// Si se trata de ajustar un comprobante especifico cargamos los transitos facturados
		// en el mismo
		with ObtenerTransitosComprobante do begin
			FCargando := True;
			cdsTransitos.CreateDataSet;
			Parameters.ParamByName('@CodigoConvenio').value := CodigoConvenio;
            Parameters.ParamByName('@NumeroCoprobante').Value := FNumeroComprobanteAAjustar;
			open;
			first;
			cdsTransitos.emptyDataSet;
			while not eof do begin
				// Busco si el transito ya esta ajustado por otros movimientos (En el comprobante)
				cdsTransitosAjustados.filter := 'IndiceVehiculo = ' + FieldByName('IndiceVehiculo').asString +
				  ' and CodigoConcesionaria = ' + FieldByName('CodigoConcesionaria').asString +
				  ' and NumeroViaje = ' + FieldByName('NumeroViaje').asString +
				  ' and NumeroPuntoCobro = ' + FieldByName('NumeroPuntoCobro').AsString;
				cdsTransitosAjustados.First;
				importeAjustado := 0;
				while not cdsTransitosAjustados.eof do begin
					importeAjustado := importeAjustado + cdsTransitosAjustados.fieldByName('ImporteAjustado').asFloat;
					cdsTransitosAjustados.next;
				end;

				// Busco si el transito existe en este movimiento
				cdsTransitosAjustados.filter := 'IndiceVehiculo = ' + FieldByName('IndiceVehiculo').asString +
				  ' and NumeroMovimiento = ' + intToStr(NumeroMovimiento) +
				  ' and CodigoConcesionaria = ' + FieldByName('CodigoConcesionaria').asString +
				  ' and NumeroViaje = ' + FieldByName('NumeroViaje').asString +
				  ' and NumeroPuntoCobro = ' + FieldByName('NumeroPuntoCobro').asString;
				ImporteAAjustar := 0;
				if not cdsTransitosAjustados.IsEmpty then
					ImporteAAjustar := cdsTransitosAjustados.FieldByName('ImporteAjustado').asFloat;

				// Si el transito esta totalmemnte ajustado por otro movimiento no lo muestro
				saldo := FieldByName('Saldo').asFloat - importeAjustado + ImporteAAjustar;
				if saldo > 0 then begin
					cdsTransitos.appendRecord([(ImporteAAjustar <> 0),
											   FieldByName('IndiceVehiculo').asInteger,
											   FieldByName('CodigoConcesionaria').asInteger,
											   FieldByName('DescriConcesionaria').asString,
											   FieldByName('NumeroViaje').asInteger,
											   FieldByName('NumeroPuntoCobro').asInteger,
											   FieldByName('DescriPuntoCobro').asString,
											   FieldByName('FechaHora').asString,
											   FieldByName('Categoria').asString,
											   FieldByName('DescriImporte').AsString,
											   Saldo,
											   formatFloat(FORMATO_IMPORTE, saldo),
											   importeAAjustar,
                                               FieldByName('NumeroComprobante').asInteger]);
					if (ImporteAAjustar <> 0) then begin
						inc(FTransitosSeleccionados);
						neImporteTransitos.Value := neImporteTransitos.Value + importeAAjustar;
					end;
				end;
				next;
			end;
			cdsTransitos.first;
            rbMovimientoPorTransito.enabled := (not cdsTransitos.isEmpty) and (FNumeroComprobanteAAjustar > 0);

            cbCuentas.OnChange(cbCuentas);
			close;
			FCargando := False;
			LTransitosSeleccionados.Caption := intToStr(FTransitosSeleccionados);
		end;
    except
        on e: Exception do begin
            MsgBoxErr('Error al inicializar', e.Message, Caption, MB_ICONSTOP);
	    	result := false;
        end;
	end;
end;


procedure TFrmAgregarMovimientoAjuste.MarcarTransito(MarcarTransito: Boolean);
begin
    // Marcar el transito manualmente (Con la barra o doble click)
    cdsTransitos.Edit;
    if MarcarTransito then
        cdsTransitos.FieldByName('ImporteAAjustar').AsFloat := cdsTransitos.FieldByName('Saldo').AsFloat
    else
        cdsTransitos.FieldByName('ImporteAAjustar').AsFloat := 0;
    cdsTransitos.post;
end;


procedure TFrmAgregarMovimientoAjuste.cbCuentasChange(Sender: TObject);
begin
    (Sender as TComboBox).cursor := crHourGlass;
    FTransitosSeleccionados := 0;
    with cdsTransitos do begin
        // Cargamos los transitos para la nueva cuenta
        DisableControls;
        Filter := 'IndiceVehiculo = ' + StrRight(cbCuentas.Items[cbCuentas.ItemIndex], 20);
        first;
        while not eof do begin
            if FieldByName('Marcado').AsBoolean then
                inc(FTransitosSeleccionados);
            next;
            lTransitosSeleccionados.Caption := intToStr(FTransitosSeleccionados);
        end;
        first;
        EnableControls;
    end;
    rbMovimientoPorTransito.Enabled := cdsTransitos.RecordCount > 0;
    (Sender as TComboBox).cursor := crDefault;
end;

procedure TFrmAgregarMovimientoAjuste.dbgTransitosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (key = VK_SPACE) and ((Sender as TDBGrid).SelectedIndex <> 7) then
        MarcarTransito(not cdsTransitos.FieldByName('Marcado').AsBoolean);
end;

procedure TfrmAgregarMovimientoAjuste.dbgTransitosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var bmp: TBitMap;
begin
    with (Sender as TDBGrid) do begin
        if (datacol = 0) then begin
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            if (Columns[0].Field.asBoolean) then
          	    Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);
            canvas.Draw(rect.Left, rect.Top,bmp);
            bmp.Free;
        end;
    end;
end;

procedure TfrmAgregarMovimientoAjuste.dbgTransitosColEnter(Sender: TObject);
begin
(*
    if (Sender as TDBGrid).SelectedIndex = 7 then
         (Sender as TDBGrid).Options := (Sender as TDBGrid).Options + [dgEditing]
    else
         (Sender as TDBGrid).Options := (Sender as TDBGrid).Options - [dgEditing];
*)
end;

procedure TfrmAgregarMovimientoAjuste.dbgTransitosColExit(Sender: TObject);
begin
    if (ActiveControl = sender) and ((Sender as TDBGrid).SelectedIndex = 7)
      and ((Sender as TDBGrid).DataSource.dataSet.state = dsEdit) then
        (Sender as TDBGrid).DataSource.dataset.Post;
end;

procedure TfrmAgregarMovimientoAjuste.dbgTransitosDblClick(Sender: TObject);
begin
    if (Sender as TDBGrid).SelectedIndex <> 7 then
        MarcarTransito(not cdsTransitos.FieldByName('Marcado').AsBoolean);
end;

procedure TfrmAgregarMovimientoAjuste.btnMarcarTodoClick(Sender: TObject);
var bm: TBookMark;
begin
    (sender as TDPSButton).cursor := crHourGlass;
    with cdsTransitos do begin
        disableControls;
        bm := GetBookMark;
        first;
        while not eof do begin
            MarcarTransito(true);
            next;
        end;
        gotoBookMark(bm);
        freeBookMark(bm);
        EnableControls;
    end;
    (sender as TDPSButton).cursor := crDefault;
end;

procedure TfrmAgregarMovimientoAjuste.btnInvertirClick(Sender: TObject);
var bm: TBookMark;
begin
    (sender as TDPSButton).cursor := crHourGlass;
    with cdsTransitos do begin
        disableControls;
        bm := GetBookMark;
        first;
        while not eof do begin
            MarcarTransito(not FieldByName('Marcado').asBoolean);
            next;
        end;
        gotoBookMark(bm);
        freeBookMark(bm);
        EnableControls;
    end;
  (sender as TDPSButton).cursor := crDefault;
end;

procedure TfrmAgregarMovimientoAjuste.dbgTransitosExit(Sender: TObject);
begin
    if (cdsTransitos.state = dsEdit) then
        cdsTransitos.Post;
end;

procedure TfrmAgregarMovimientoAjuste.rbMovimientoPersonalClick(
  Sender: TObject);
begin
    if (sender as TRadioButton).Checked then begin
        dbgTransitos.Enabled := false;
        btnMarcarTodo.Enabled := false;
        btnInvertir.Enabled := false;
//        cbConcesionarias.Enabled := False;
        cbConceptosMovimiento.enabled := true;
        neImporte.enabled := true;
        cbConceptosMovimiento.setFocus;
        rgTipoAjuste.Enabled := True;
    end;
end;

procedure TfrmAgregarMovimientoAjuste.rbMovimientoPorTransitoClick(
  Sender: TObject);
begin
    if (sender as TRadioButton).Checked then begin
        rgTipoAjuste.Enabled := False;
        cbConceptosMovimiento.enabled := false;
        neImporte.enabled := false;
        dbgTransitos.Enabled := true;
        btnMarcarTodo.Enabled := true;
        btnInvertir.Enabled := true;
//        cbConcesionarias.Enabled := True;
        dbgTransitos.Refresh;
        dbgTransitos.setFocus;
    end;
end;

procedure TfrmAgregarMovimientoAjuste.cdsTransitosBeforePost(
  DataSet: TDataSet);
resourcestring
    MSG_IMPORTE_AJUSTE = 'El importe de ajuste debe ser menor o igual al saldo de la transacci�n.';
begin
    with DataSet do begin
        if (fieldByName('ImporteAAjustar').asFloat < 0) or
           (FieldByName('ImporteAAjustar').asFloat > FieldByName('Saldo').asFloat) then begin
            msgBox(MSG_IMPORTE_AJUSTE, CAPTION_VALIDAR_IMPORTE, MB_OK);
            dbgTransitos.SetFocus;
            abort;
        end;
        FEstadoCambio := FieldByName('Marcado').asBoolean <> (FieldByName('ImporteAAjustar').AsFloat <> 0);
        FieldByName('Marcado').asBoolean := FieldByName('ImporteAAjustar').AsFloat <> 0;
    end;
end;


procedure TfrmAgregarMovimientoAjuste.cdsTransitosAfterPost(
  DataSet: TDataSet);
begin
    if not FCargando then begin
        // Actualizo el contador de tr�nsitos Marcados y el importe total sumado
        neImporteTransitos.value := neImporteTransitos.value + (cdsTransitos.FieldByName('ImporteAAjustar').asFloat - FImporteAnterior);
        if (FEstadoCambio) then begin
            if cdsTransitos.FieldByName('Marcado').AsBoolean then
                inc(FTransitosSeleccionados)
            else
                dec(FTransitosSeleccionados);
            lTransitosSeleccionados.Caption := intToStr(FTransitosSeleccionados);
        end;
    end;
end;

procedure TfrmAgregarMovimientoAjuste.cdsTransitosAfterEdit(
  DataSet: TDataSet);
begin
    FImporteAnterior := dataset.FieldByName('ImporteAAjustar').asFloat;
end;

procedure TfrmAgregarMovimientoAjuste.FormActivate(Sender: TObject);
begin
    if FCodigoConcepto = CM_AJUSTE_FACTURACION then
        rbMovimientoPorTransito.Checked := true
    else
        rbMovimientoPersonal.Checked := true;
end;

procedure TfrmAgregarMovimientoAjuste.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
resourcestring
    MSG_IMPORTE_CERO = 'El importe del movimiento no puede ser cero.';
begin
    CanClose := True;
    if (ModalResult = idOk) and
       ((rbMovimientoPersonal.Checked and (neImporte.Value = 0)) or
        (rbMovimientoPorTransito.Checked and (neImporteTransitos.Value = 0))) then begin
        msgBox(MSG_IMPORTE_CERO, CAPTION_VALIDAR_IMPORTE, MB_OK);
        CanClose := False;
    end;
end;

end.
