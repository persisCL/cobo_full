{-----------------------------------------------------------------------------
 File Name      : frmRechazarCartaListaAmarilla
 Author         : Claudio Quezada Ib��ez
 Date Created   : 12-Junio-2013
 Language       : ES-CL
 Firma          : SS_660_CQU_20130604
 Description    : Rechaza una carta enviada por Lista Amarilla

 Author         : Claudio Quezada Ib��ez
 Date Created   : 05-Julio-2013
 Language       : ES-CL
 Firma          : SS_660_CQU_20130628
 Description    : Formatea el Numero de Convenio
                  Se modifican las dimensiones y ubicaci�n de algunos controles

 Author         : Miguel Villarroel
 Date Created   : 11-Julio-2013
 Language       : ES-CL
 Firma          : SS_660_MVI_20130711
 Description    : Se agrega la concesionaria en el mensaje superior de la ventana.

 Author         : Claudio Quezada Ib��ez
 Date Created   : 22-Julio-2013
 Language       : ES-CL
 Firma          : SS_660_CQU_20130711
 Description    : Se quita el formato del Numero de Convenio ya que viene formateado
-----------------------------------------------------------------------------}
unit frmRechazarCartaListaAmarilla;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, Validate, DateEdit, ExtCtrls, DmConnection, DB, ADODB,
    VariantComboBox, UtilProc, Util, SysUtilsCN, PeaProcs;

type
  TRechazarCartaListaAmarillaForm = class(TForm)
    btnGrabar: TButton;
    btnSalir: TButton;
    bvlFondo: TBevel;
    spRechazarCartaListaAmarilla: TADOStoredProc;
    lblMotivoDevolucion: TLabel;
    cboMotivoRechazo: TVariantComboBox;
    lblObservacion: TLabel;
    txtObservacion: TMemo;
    spObtenerMotivosDevolucionComprobantesCorreo: TADOStoredProc;
    lblDatoCarta: TLabel;
    qryObtenerIDCarta: TADOQuery;
    procedure btnSalirClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure cboMotivoRechazoChange(Sender: TObject);
  private
    { Private declarations }
    FIDConvenioInhabilitado,
    FCodigoConvenio,
    FIDCarta,
    FCodigoMotivoDevolucion :   Integer;
    FFechaEnvioAnterior     :   TDateTime;
    FNumeroConvenio, FConcecionaria  :   string;                                                            //SS_660_MVI_20130711

    function RechazarCarta : Boolean;
    procedure LlenaCombos;
    function Validaciones : boolean;
    function ObtenerIDCarta : Boolean;
  public
    { Public declarations }
    function Inicializar(IDConvenioInhabilitado, CodigoConvenio : integer; FechaAnterior : TDateTime; NumeroConvenio : String; Concesionaria : String) : boolean; //SS_660_MVI_20130711
  end;

var
  RechazarCartaListaAmarillaForm: TRechazarCartaListaAmarillaForm;

implementation

{$R *.dfm}

function TRechazarCartaListaAmarillaForm.Inicializar;
resourcestring
    MSG_ERROR_INICIALIZA    =   'No se puede cargar la ventana de Rechazo de Cartas';
    MSG_CARTA_A_RECHAZAR    =   'Rechazando carta enviada el %s'#10#13'para el convenio %s, de la concesionaria %s';                                                //SS_660_MVI_20130711
    MSG_ERROR_CARGAR_CARTA  =   'Error al cargar la carta a rechazar';
    MSG_CARTA_RECHAZADA     =   'Carta rechazada con anterioridad';
begin
    try
        CenterForm(Self);
        FIDConvenioInhabilitado :=  IDConvenioInhabilitado;
        FCodigoConvenio         :=  CodigoConvenio;
        FFechaEnvioAnterior     :=  FechaAnterior;
        FNumeroConvenio         :=  NumeroConvenio;
        //lblDatoCarta.Caption    :=  Format(MSG_CARTA_A_RECHAZAR, [FormatDateTime('dd/mm/yyyy', FFechaEnvioAnterior), FNumeroConvenio]);                         //  SS_660_CQU_20130628
        FConcecionaria          :=  Concesionaria;                                                                                                                //SS_660_MVI_20130711
        //lblDatoCarta.Caption    :=  Format(MSG_CARTA_A_RECHAZAR, [FormatDateTime('dd/mm/yyyy', FFechaEnvioAnterior), FormatearNumeroConvenio(FNumeroConvenio), FConcecionaria]);  //  SS_660_CQU_20130628       //SS_660_MVI_20130711
        lblDatoCarta.Caption    :=  Format(MSG_CARTA_A_RECHAZAR, [FormatDateTime('dd/mm/yyyy', FFechaEnvioAnterior), FNumeroConvenio, FConcecionaria]);                             // SS_660_CQU_20130711

        if not ObtenerIDCarta() then begin
            if FIDCarta = -1 then raise Exception.Create(MSG_CARTA_RECHAZADA)
            else raise Exception.Create(MSG_ERROR_CARGAR_CARTA);
        end;

        LlenaCombos();

        Result := True;
    except
        on e:exception do begin
            MsgBoxErr(MSG_ERROR_INICIALIZA, e.Message, Caption, MB_ICONERROR);
            Result := False;
        end;
    end;
end;

{---------------------------------------------------------------------------------
function Name   : ObtenerIDCarta
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Obtiene el IDHistoricoCartasListaAmarilla de la tabla HistoricoCartasListaAmarilla.
Parameters      : None
Return Value    : True / False
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
function TRechazarCartaListaAmarillaForm.ObtenerIDCarta;
begin
    try
        with qryObtenerIDCarta do begin
            Parameters.ParamByName('IDConvenioInhabilitado').Value := FIDConvenioInhabilitado;
            Parameters.ParamByName('CodigoConvenio').Value := FCodigoConvenio;
            Active := True;
            if (RecordCount > 0) then begin
                if FieldByName('CodigoMotivoDevolucion').Value > 0 then
                    FIDCarta := -1
                else
                    FIDCarta := FieldByName('IDHistoricoCartasListaAmarilla').AsInteger;
            end;
            if FIDCarta > 0 then Result := True
            else Result := False;
        end;
    except
        Result := False
    end;
end;

{---------------------------------------------------------------------------------
function Name   : RechazarCarta
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Graba en la tabla HistoricoCartasListaAmarilla el motivo del rechazo de la carta
Parameters      : IDHistoricoCartaListaAmarilla : Integer
Return Value    : True / False
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
function TRechazarCartaListaAmarillaForm.RechazarCarta;
begin
    if FIDCarta > 0 then begin
        try
            with spRechazarCartaListaAmarilla do begin
                Parameters.Refresh;
                Parameters.ParamByName('@IDHistoricoCartasListaAmarilla').Value :=  FIDCarta;
                Parameters.ParamByName('@CodigoMotivoDevolucion').Value         :=  FCodigoMotivoDevolucion;
                Parameters.ParamByName('@Observacion').Value                    :=  Trim(txtObservacion.Text);
                Parameters.ParamByName('@UsuarioCreacion').Value                :=  UsuarioSistema;
            end;
            DMConnections.BaseCAC.BeginTrans;
            spRechazarCartaListaAmarilla.ExecProc;
            DMConnections.BaseCAC.CommitTrans;
            Result := True;
        except
            if DMConnections.BaseCAC.InTransaction then begin
                DMConnections.BaseCAC.RollbackTrans;
            end;
            Result := False;
        end;  
    end else
        Result := False;

end;

{---------------------------------------------------------------------------------
Procedure Name  : LlenaCombos
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Carga el combo con los motivos de rechazo
Parameters      : None
Return Value    : None
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
procedure TRechazarCartaListaAmarillaForm.LlenaCombos;
resourcestring
    MSG_CARGAR_MOTIVOS  =   'Error al cargar los motivos de devoluci�n de correspondencia, mensaje del sistema: ';
begin
    try
        // agrego un "Seleccione"
        cboMotivoRechazo.Items.Add(' - Seleccione Motivo - ', -1);
        // Obtener Motivos de Devolucion
        with spObtenerMotivosDevolucionComprobantesCorreo do begin
            Open;
            while not Eof do begin
                cboMotivoRechazo.Items.Add( FieldByName('Detalle').AsString,
                                            FieldByName('CodigoMotivoDevolucion').AsInteger);
                Next;
            end;
            Close;
        end;
        // Dejo seleccionado el primero
        cboMotivoRechazo.ItemIndex := 0;
    except
        on e:exception do begin
            raise Exception.Create(MSG_CARGAR_MOTIVOS + e.Message);
        end;
    end;
end;

{---------------------------------------------------------------------------------
Procedure Name  : btnGrabarClick
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Manda a grabar el rechazo
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
procedure TRechazarCartaListaAmarillaForm.btnGrabarClick(Sender: TObject);
resourcestring
    MSG_EXITO       = 'La carta ha sido rechazada';
    MSG_ERROR       = 'Ocurri� un error al intentar grabar el Hist�rico de Cartas';
begin
    // Valido los controles
    if not Validaciones then Exit;

    if not RechazarCarta then ModalResult := mrCancel
    else begin
        MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
        ModalResult := mrOk;
    end;
end;

{---------------------------------------------------------------------------------
Procedure Name  : btnSalirClick
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Retorna Cancel
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
procedure TRechazarCartaListaAmarillaForm.btnSalirClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

{---------------------------------------------------------------------------------
Procedure Name  : cboMotivoRechazoChange
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Captura el valor del Combo de Motivos
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
procedure TRechazarCartaListaAmarillaForm.cboMotivoRechazoChange(Sender: TObject);
begin
    FCodigoMotivoDevolucion := cboMotivoRechazo.Value;
end;

{---------------------------------------------------------------------------------
function Name   : Validaciones
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Valida que lo ingresado por el usuario sea lo correcto
Parameters      : None
Return Value    : True / False
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
function TRechazarCartaListaAmarillaForm.Validaciones;
resourcestring
    MSG_GRABAR              =   'Problema al rechazar la carta';
    MSG_SELECCIONE_MOTIVO   =   'Debe seleccionar un motivo de rechazo';
    MSG_FALTA_OBSERVACION   =   'Debe ingresar una observaci�n';
begin
    Result := False;

    if not ValidateControls(
            [cboMotivoRechazo],
            [cboMotivoRechazo.ItemIndex <> 0],
            MSG_GRABAR,
            [MSG_SELECCIONE_MOTIVO])
    then Exit
    else Result := True;
end;

end.
