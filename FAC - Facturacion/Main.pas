{********************************** File Header ********************************
File Name   : Main.pas
Author      :
Date Created:
Language    : ES-AR
Description : Facturaci�n
    - Para la emisi�n de comprobantes en la impresora fiscal se utiliza una dll
    que debe estar ubicada en el mismo lugar que el ejecutable.

Revision 1:
    Author : ggomez
    Date : 04/08/2006
    Description :
        - Agregu� la funci�n adicional PERMISOS_DATOS_CLIENTE.
        - Agregu� la funci�n adicional EDICION_ACTIONLIST_TAG_TERCEROS.
        - Modifiqu� el nombre de la funci�n adicional EDICION_ACTIONLIST_TAG,
        ahora es EDICION_ACTIONLIST_TAG_PROPIOS.

 Revision 2:
    Author : jconcheyro
    Date : 17/11/2006
    Description : En inicializar se agrega VerificarExisteCotizacionUFDelDia
    Que muestra un mensaje de aviso si no existe la Cotizacion de UF del d�a
    que se usa en Arriendos de Telev�a.

 Revision 3:
    Author : jconcheyro
    Date : 18/12/2006
    Description : VerificarExisteCotizacionUFDelDia ahora es VerificarExistenCotizacionesDelDia

 Revision 4:
    Author : nefernandez
    Date : 29/01/2009
    Description : SS 782: Se agrega el permiso que permite editar o no la "Fecha de Emisi�n" en la facturaci�n manual

  Revision 5:
    Author : mpiazza
    Date : 03-02-2009
    Description : SS 146: Se agrega Llamada a ABMEmpresasRecaudadoras

	Revision 6
    Author: mbecerra
    Date: 12-Febrero-2009
    Description: 	(Ref: Facturaci�n Electr�nica)
    				Se agrega una nueva opci�n de men� y la invocaci�n al
                    nuevo formulario: frmTipificaci�nMasiva

    		13-Marzo-2009
                    Se agrega la llamada a la configuraci�n de la variable
                    EGATE_HOME.

    		15-Abril-2009
                    Se agrega el nuevo formulario ImpresionDesdeHastaDeNKForm para
                    impresi�n masiva de NK

            17-Abril-2009
                    Se renombra el formulario frm_facturacionUnConvenio por
                    frmFacturacionManual, y se coloca en Comunes, pues ahora ser�
                    invocado en el CAC
	Revision 7
    Author: Nelson Droguett Sierra
    Date: 01-Septiembre-2009
    Description: 	(Ref: Facturaci�n Electr�nica)
                    Se crea una nueva opcion de menu : Nota de Credito por cierre
                    de convenio

	Revision 8
    Author: Nelson Droguett Sierra
    Date: 29-Septiembre-2009
    Description: 	(Ref: Facturaci�n Electr�nica)
                    Se habilita la anulacion de NQ (Nota de cobro directa), y la
                    impresion de las notas de credito a notas de cobro directas
	Revision	: 10
    Author		: Nelson Droguett Sierra
	Date		: 22-Julio-2010
    Description	: Se agrega la opcion de Tabla de Tasas de Interes que estana en Administracion

    Rev.: SS_963_PDO_20110428
    Description: Se cambia el nombre de Menu Tasas de Inter�s por Tasas de Inter�s NO Reajustables.
        Se A�ade la nuava opci�n de men� Tasas de Inter�s Reajustables.

    Firma       : SS_959_ALA_20111003
    Description : Se agrega acci�n actPregeneracionDevolucionDinero al men� mnuPregeneracionDevDinero,
                  el cual en el evento Execute llama al formulario frmComprobanteDevolucionDinero

    Firma       : SS_959_MBE_20120528
    Description : Se agrega el permiso para editar la fecha de emisi�n del comprobante de reserva

    Firma       : SS_959_MBE_20130226
    Description : Se agrega el permiso para el bot�n Eliminar en la ventana de reserva


    Firma       : SS_660_MBE_20121002
    Description : Se agrega el nuevo men� de Inhabilitaci�n por Lista amarilla
                    y la invocaci�n de la respectiva ventana.

    Firma       : SS_1006_1015_NDR_20121015
    Description : Personas VIP no inhabilitadas.

    Firma       : SS_660_CQU_20121010
    Description : Se agrega el formulario para facturar los moroso por lista amarilla
                  Se agrega formulario para imprimir las NC Infractores, su permiso y su men�
                  Se modifica el nombre del men� que carga el formulario para imprimir
                  las facturas infractoras, se cambia de "Imprimir Notas de Cobro Infractores"
                  por "Imprimir Notas de Cobro Morosos"
                  Se agregan los ABM de Estado de Lista Amarilla y el
                  ABM de Empresas de Correos.
                  Se agrega Permiso para el estado "Eliminado"
                  Se agregan los siguientes permisos:
                    -   bntNuevoConvenio = AGREGAR_CONVENIO_LISTA_AMARILLA
                    -   btnCambiarEstado = CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA
                    -   btnReEnviarCarta = REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA
                  En la dblListaAmarilla existen dos chkBox, �stos deben tener permisos independientes
                  y son los siguientes:
                    -   chkAutorizaFiscalia = AUTORIZA_FISCALIA_CONVENIO_LISTA_AMARILLA
                    -   chkAutorizaGerencia = AUTORIZA_GERENCIA_CONVENIO_LISTA_AMARILLA
    Firma       : SS_1100_NDR_20130417
    Description : Permiso al boton "Pasar a Carpetas Legales"

    Firma       : SS_660_CQU_20130604
    Description : Permiso al bot�n "Rechazar Carta" de Lista Amarilla

    Firma       :   SS_925_NDR_20131122
    Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)

	Firma       :   SS_925_MBE_20131223
	Description :   Se cambia la ventana de Acerca de.

    Firma       :   SS_1233_MCA_20141201
    Descripcion :   se agrega nuevo formulario que permite ingresar saltos en la numeracion de los tipos comprobantes


Firma       : SS_1147_NDR_20141216
Descripcion : Color de menues segun concesionaria nativa

Firma       : SS_1147AAA_NDR_20150414
Descripcion : Correccion a observaciones de Refinanciaci�n (Caption y StatusBar)

Firma		:	SS_1268_MBE_20150511
Description	:	Se crea el men�: mnu_crearComprobanteSD

  Firma       : SS_1436_NDR_20151230
  Descripcion : Dejar un registro en RegistroOperaciones cuando el usuario sale del sistema  o cuando trata de ingresar con password erronea

  Firma       : SS_1437_NDR_20151230
  Descripcion : Que los menus de los informes se carguen segun los permisos del usuario, y que dependan del GenerarMenues del Sistema
*******************************************************************************}
unit Main;

interface

uses
  //Facturacion
  DmConnection,                 //conexion a base de datos OP_CAC
  util,
  UtilDB,
  UtilProc,
  PeaTypes,
  peaprocs,
  cacProcs,
  RStrings,
  MenuesContextuales,           //Menues de Reclamos
  Navigator,                    //Lista de Tareas
  FrmReclamos,                  //Reclamos
  FrmOrdenesServicioBloqueadas, //Tareas Bloqueadas
  FrmRptEstadisticaReclamos,    //Informe de Estadistica de Reclamos
  FrmInicioConsultaConvenio,    //Cartola
  Frm_FacturacionMasiva,
  frmFacturacionManual,
  frm_IngresarComprobantes,
  Frm_AjustesFacturacion,
  frmReporteFacturacion,
  frmUltimosConsumos,
  AgendaFacturacion,
  VerificacionManualFacturacion,
  frmIngresoEspecialSD,									//  SS_1268_MBE_20150511
  ComprobantesEmitidos,
  ABMGrupoFacturacion,
  frmAnulacionNotasCobro,
  ABMDimensiones,               // PAN_CU.COBO.ADM.CNT.002_20170411
  // Rev.7 / 01-Septiembre-2009 / Nelson Droguett Sierra----------------------------
  //frmAnulacionNotasCobroCierreConvenio, 						--SS-877-NDR-20100915
  // Rev.8 / 01-Septiembre-2009 / Nelson Droguett Sierra----------------------------
  frmReporteNotaDebitoElectronica,
  frm_ImprimirNotaCreditoNQ,
  //--------------------------------------------------------------------------------
  frm_ImprimirNotaCobroDirecta,
  fGeneracionGastosGestionCobranza,
  fAnulacionGastosGestionCobranza,
  ABMConceptosMovimiento,
  frm_IngresarMovimientos,
  frmMovimientosNofacturados,   //SS 732
  DPSAbout,                     //Acerca de..
  frmABMTiposDeudas,
  frmTipificacionMasiva, 			//Revision 6
{  frmImpresionDesdeHastaDeNK,	//Revision 6}
  frmAnulacionNotaCobroDirecta,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, IconBars, ImgList, Buttons, StdCtrls, ComCtrls,
  ToolWin, ActnList, AppEvnts, login, DB, ADODB, ActnMan,CambioPassword, jpeg,
  InvoicingExplorer, Comm, DeclHard, DeclPrn, XPMan, XPStyleActnCtrls,
  EmailFacturacion, frm_NotaDeCobroDirecta, CustomAssistance, frm_CarpetaLegales,
  frmReaplicarConvenio, ABMTiposComprobante, ABMLocales, ABMTalonariosComprobantesLocales, ABMCuentasContables,
  frmIncrementoCuotaArriendo, frmABMCotizacionDiariaUF, frmControlIndemnizaciones,
  frmABMEmpresasRecaudadoras,
  //Rev.10 / 22-Julio-2010 / Nelson Droguett Sierra--------------------------------------------
  frmAcercaDe,                                          //SS_925_MBE_20131223
  ABMTasasInteres,
  ABMTasasInteresReajustables,
  frmPersonasVIPNoInhabilitadas,        //SS_1006_1015_NDR_20121015
  frmReservaComprobanteDevDinero,       //SS_959_ALA_20111003
  frmListaDeConveniosEnListaAmarilla,   //SS_660_MBE_20121002
  frm_SeleccionarProcesosMorosos,       //SS_660_CQU_20121010
  ABMEstadosListaAmarilla,              //SS_660_CQU_20121010
  ABMEmpresasCorreo,                    //SS_660_MDV_20121010
  ABMMotivosNoFacturar,                 //TASK_086_MGO_20161121
  PreFacturaciones,                     //TASK_086_MGO_20161121
  Facturaciones,                        //TASK_100_MGO_20161223
  ABMReglasDescuentos,                  //TASK_146_MGO_20170303
  frmSaltosNumeracionComprobantes,      //SS_1233_MCA_20141201	//TASK_108_JMA_20170214
  Categoria,                            //TASK_108_JMA_20170214
  ClaseBase,                              //TASK_108_JMA_20170214
  ABMConceptosProyecto,						// PAN_CU.COBO.ADM.CNT.002_20170411
  FrameDatosVehiculos;                      //TASK_108_JMA_20170214

type
  TMainForm = class(TForm)
    StatusBar: TStatusBar;
    Menu: TMainMenu;
    mnu_Archivo: TMenuItem;
	menuSalir: TMenuItem;
    mnu_tablas: TMenuItem;
    mnu_GruposFacturacion: TMenuItem;
    mnu_procesos: TMenuItem;
    mnu_Facturacion: TMenuItem;
    mnu_AjusteFacturacion: TMenuItem;
    mnu_consultas: TMenuItem;
    mnu_ComprobantesEmitidos: TMenuItem;
    mnu_UltimosConsumos: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambioPassword: TMenuItem;
    mnu_Ventana: TMenuItem;
    mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    N2: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Ayuda: TMenuItem;
    mnu_acercade: TMenuItem;
    ActionManager1: TActionManager;
    actArchivos: TAction;
    actTablas: TAction;
    actProcesos: TAction;
    actConsultas: TAction;
    actMantenimiento: TAction;
    actVentana: TAction;
    actAyuda: TAction;
	actSalir: TAction;
    actGruposDeFacturacion: TAction;
    actAsistenteFacturacion: TAction;
    actUltimosConsumos: TAction;
    actComprobantesEmitidos: TAction;
    actCambiarPassword: TAction;
    actAjusteFacturacion: TAction;
    Imagenes: TImageList;
    ImagenFondo: TImage;
    actCalendario: TAction;
    mnu_Calendario: TMenuItem;
    mnu_FacturacioManual: TMenuItem;
    actVentanas: TAction;
    actCascada: TAction;
    actTile: TAction;
    actSiguiente: TAction;
    actAnterior: TAction;
    MenuImagenes: TImageList;
    mnu_Ver: TMenuItem;
    mnu_IconBar: TMenuItem;
    actIconBar: TAction;
    actAcercaDe: TAction;
    mnu_Informes: TMenuItem;
    N1: TMenuItem;
    mnu_ConsultainicialConvenios: TMenuItem;
    mnu_Reclamos: TMenuItem;
    mnu_ReclamosGenerales: TMenuItem;
    actFacturacionUnCOnvenio: TAction;
    mnu_IngresoComprobantes: TMenuItem;
    mnu_ExploradorProcesosdeFacturacion: TMenuItem;
    N4: TMenuItem;
    mnu_EditarReclamos: TMenuItem;
    mnu_EstadisticaReclamos: TMenuItem;
    N5: TMenuItem;
    mnu_OrdenesServicioBloquadas: TMenuItem;
    actIngresoComprobantes: TAction;
    actConsultaInicialConvenio: TAction;
    actExploradorProcesosFacturacion: TAction;
    N6: TMenuItem;
    mnu_AnulaciondeNotasdeCobro: TMenuItem;
    actAnulacionNotasCobro: TAction;
    mnu_BarraDeNavegacion: TMenuItem;
    BarradeHotLinks1: TMenuItem;
    act_BarraDeNavegacion: TAction;
    act_BarraDeHotLinks: TAction;
    XPManifest1: TXPManifest;
    N7: TMenuItem;
    GenerarEmails: TMenuItem;
    N8: TMenuItem;
    mnu_GenerarNotadeCobroDirecta: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    mnu_ImprimirNotaCobroDirecta: TMenuItem;
    GenerarGastosporGestindeCombranzas1: TMenuItem;
    AnulacindeGastosporGestindeCobranzas1: TMenuItem;
    N11: TMenuItem;
    mnu_ConceptosMovimiento: TMenuItem;
    actConceptosMovimientos: TAction;
    mnu_IngresodeMovimientosCuentas: TMenuItem;
    act_IngresodeMovimientosCuenta: TAction;
    mnu_CarpetaDeudores: TMenuItem;
    actCarpetasMorosos: TAction;
    mnu_ReaplicarConvenio: TMenuItem;
    actReaplicarConvenio: TAction;
    actABMTiposComprobantes: TAction;
    mnu_TiposComprobantes: TMenuItem;
    actLocales: TAction;
    mnu_Locales: TMenuItem;
    actTalonarios: TAction;
    mnu_talonarios: TMenuItem;
    actCuentasContables: TAction;
    mnu_CuentasContables: TMenuItem;
    AgregarCuotadeComprobante1: TMenuItem;
    actAgregarCuotaComprobante: TAction;
    actABMCotizacionesUF: TAction;
    ABMCotizacionesDiariasUF1: TMenuItem;
    mnu_ControlIndemnizaciones: TMenuItem;
    mnuMovimientosNoFacturados: TMenuItem;
    mnu_CarpetaLegal: TMenuItem;
    mnu_ABMTiposdeDeudas: TMenuItem;
    mnu_ABMEmpresasRecaudadoras: TMenuItem;
    mnuFacturacionElectronica: TMenuItem;
    mnuTipificacionyNumeracionMasiva: TMenuItem;
    frmAnulacionNotaCobroDirecta: TMenuItem;
    mnuImprimirNCaNQ: TMenuItem;
    mnuTasasInteres: TMenuItem;
    actTasasInteres: TAction;
    actTasasInteresReajustables: TAction;
    asasdeIntersReajustables1: TMenuItem;
    mnuPregeneracionDevDinero: TMenuItem;					//SS_959_ALA_20111003
    actPregeneracionDevolucionDinero: TAction;                                     // SS_660_MBE_20121002
    mnuInhabilitacionPorListaAmarilla: TMenuItem;       // SS_660_MBE_20121002
    mnuImprimirNCInfractores: TMenuItem;                // SS_660_CQU_20121010
    mnuABMEmpresasCorreo: TMenuItem;			        // SS_660_MDV_20121010
    mnuABMEstadosListaAmarilla: TMenuItem;              // SS_660_CQU_20121010
    actPersonasVIPNoInhabilitadas: TAction;               //SS_1006_1015_NDR_20121015
    mnu_PersonasVIPNoInhabilitadas: TMenuItem;          //SS_1006_1015_NDR_20121015
    SaltosdeNumeracion: TMenuItem;
    mnu_ABMMotivosNoFacturar: TMenuItem;                                        //TASK_086_MGO_20161121
    mnu_ConsultarPreFacturaciones: TMenuItem;
    mnu_ConsultarFacturaciones: TMenuItem;
    N14: TMenuItem;
    mnu_ABMReglasDescuento: TMenuItem;
    N3: TMenuItem;                                       // PAN_CU.COBO.ADM.CNT.002_20170411
    NavisionABMConceptosProyecto1: TMenuItem;            // PAN_CU.COBO.ADM.CNT.002_20170411
    mniN12: TMenuItem;                                   // PAN_CU.COBO.ADM.CNT.002_20170411
    procedure actCuentasContablesExecute(Sender: TObject);
    procedure actTalonariosExecute(Sender: TObject);
    procedure actLocalesExecute(Sender: TObject);
    procedure actABMTiposComprobantesExecute(Sender: TObject);
    procedure actReaplicarConvenioExecute(Sender: TObject);
    procedure actCarpetasMorososExecute(Sender: TObject);
    procedure act_IngresodeMovimientosCuentaExecute(Sender: TObject);
    procedure actConceptosMovimientosExecute(Sender: TObject);
    procedure AnulacindeGastosporGestindeCobranzas1Click(Sender: TObject);
    procedure GenerarGastosporGestindeCombranzas1Click(Sender: TObject);
    procedure mnu_ImprimirNotaCobroDirectaClick(Sender: TObject);
    procedure mnu_GenerarNotadeCobroDirectaClick(Sender: TObject);
    procedure GenerarEmailsClick(Sender: TObject);
    procedure EliminarHotLinks1Click(Sender: TObject);
    procedure actAjusteFacturacionExecute(Sender: TObject);
    procedure actAsistenteFacturacionExecute(Sender: TObject);
    procedure actSalirExecute(Sender: TObject);
    procedure actUltimosConsumosExecute(Sender: TObject);
    procedure actComprobantesEmitidosExecute(Sender: TObject);
    procedure actCambiarPasswordExecute(Sender: TObject);
    procedure actGruposDeFacturacionExecute(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure actCalendarioExecute(Sender: TObject);
    procedure mnu_SiguienteClick(Sender: TObject);
    procedure mnu_AnteriorClick(Sender: TObject);
    procedure mnu_CascadaClick(Sender: TObject);
    procedure mnu_MosaicoClick(Sender: TObject);
    procedure mnu_ventanaClick(Sender: TObject);
    procedure actIconBarExecute(Sender: TObject);
    procedure actAcercaDeExecute(Sender: TObject);
    procedure actFacturacionUnCOnvenioExecute(Sender: TObject);
    procedure ExploradordeProcesosdeFacturacin1Click(Sender: TObject);
    procedure mnu_EditarReclamosClick(Sender: TObject);
    procedure mnu_EstadisticaReclamosClick(Sender: TObject);
    procedure mnu_OrdenesServicioBloquadasClick(Sender: TObject);
    procedure actIngresoComprobantesExecute(Sender: TObject);
    procedure actConsultaInicialConvenioExecute(Sender: TObject);
    procedure actExploradorProcesosFacturacionExecute(Sender: TObject);
    procedure actAnulacionNotasCobroExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure act_BarraDeNavegacionExecute(Sender: TObject);
    procedure act_BarraDeHotLinksExecute(Sender: TObject);
    procedure actAgregarCuotaComprobanteExecute(Sender: TObject);
    procedure actABMCotizacionesUFExecute(Sender: TObject);
    procedure mnu_ControlIndemnizacionesClick(Sender: TObject);
    procedure mnuMovimientosNoFacturadosClick(Sender: TObject);
    procedure mnu_ABMTiposdeDeudasClick(Sender: TObject);
    procedure mnu_ABMEmpresasRecaudadorasClick(Sender: TObject);
    procedure mnuTipificacionyNumeracionMasivaClick(Sender: TObject);
    procedure frmAnulacionNotaCobroDirectaClick(Sender: TObject);
    procedure mnuImprimirNCaNQClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actTasasInteresExecute(Sender: TObject);
    {INICIO	: CFU 20170224 TASK_114_CFU_20170224-Administrar_Tasas_De_Interes
    procedure actTasasInteresReajustablesExecute(Sender: TObject);
    TERMINO	: CFU 20170224 TASK_114_CFU_20170224-Administrar_Tasas_De_Interes}
    procedure actPregeneracionDevolucionDineroExecute(Sender: TObject); //SS_959_ALA_20111003
    procedure mnuInhabilitacionPorListaAmarillaClick(Sender: TObject);  //SS_660_CQU_20121010
    procedure mnuImprimirNCInfractoresClick(Sender: TObject);           //SS_660_CQU_20121010
    procedure mnuABMEstadosListaAmarillaClick(Sender: TObject);         //SS_660_CQU_20121010
    procedure mnuEmpresasCorreoClick(Sender: TObject);                  //SS_660_CQU_20121010
    procedure actPersonasVIPNoInhabilitadasExecute(Sender: TObject);    //SS_1006_1015_NDR_20121015
    procedure SaltosdeNumeracionClick(Sender: TObject);             	//SS_1233_MCA_20141201
    function CambiarEventoMenu(Menu: TMenu): Boolean;
    procedure mnu_CrearComprobanteSDClick(Sender: TObject);
    procedure mnu_ConceptosMovimientosMaestroClick(Sender: TObject);
    procedure mnu_ABMMotivosNoFacturarClick(Sender: TObject);			// TASK_086_MGO_20161121
    procedure mnu_ConsultarPreFacturacionesClick(Sender: TObject);
    procedure mnu_ConsultarFacturacionesClick(Sender: TObject);
    procedure mnu_ABMReglasDescuentoClick(Sender: TObject);
    procedure NavisionABMConceptosProyecto1Click(Sender: TObject);
    procedure mniN12Click(Sender: TObject);		// PAN_

  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;

    procedure RefrescarEstadosIconBar;
    procedure InicializarIconBar;
    procedure GuardarUserSettings;
  public
	{ Public declarations }
	function Inicializa: Boolean;
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);
  end;

var
  MainForm: TMainForm;

implementation

uses Frm_Startup;

resourcestring
    PAGINA_PROCESOS     = 'pagProcesos';
    PAGINA_CONSULTAS    = 'pagConsultas';
    CAPTION_PROCESS     = 'Procesos';
    CAPTION_QUERIES     = 'Consultas';
    CAPTION_TABLES      = 'Tablas';

    CAPTION_WIZARD					= 'Asistente de Facturaci�n';
    CAPTION_MANUAL_INVOICING		= 'Facturaci�n Manual';
    CAPTION_INVOICE_ADJUSTMENT		= 'Ajustes de Facturaci�n';
    CAPTION_INVOICE_INPUT			= 'Ingreso de Comprobantes';
    CAPTION_PROMOTIONS				= 'Promociones';
    CAPTION_INVOICING_GROUPS		= 'Grupos de Facturaci�n';
    CAPTION_CALENDAR				= 'Calendario de Facturaci�n';
    CAPTION_INITIAL_REPORT			= 'Iniciar Consulta de Convenios';
    CAPTION_EMITTED_INVOICES		= 'Comprobantes Emitidos';
    CAPTION_LAST_TRANSITS			= 'Ultimos Consumos';
    CAPTION_INVOICE_PROCESS_RESULTS	= 'Explorador de Procesos de Facturaci�n';

    HINT_WIZARD						= 'Consolidaci�n e impresi�n de movimientos';
	HINT_MANUAL_INVOICING			= 'Permite realizar una factura en forma manual';
    HINT_INVOICE_ADJUSTMENT			= 'Permite realizar ajustes de facturaci�n';
    HINT_INVOICE_INPUT				= 'Permite ingresar comprobantes';
    HINT_PROMOTIONS					= 'Permite definir las promociones';
    HINT_INVOICING_GROUPS			= 'Permite definir los grupos de facturaci�n';
    HINT_CALENDAR					= 'Permite definir el calendario de facturaci�n';
    HINT_EMITTED_INVOICES			= 'Permite ver e imprimir comprobantes emitidos';
    HINT_LAST_TRANSITS				= 'Permite consultar los �ltimos consumos pendientes de facturaci�n';
    HINT_MANUAL_INVOICING_CHECKING 	= 'Permite la verificaci�n manual de una factura';
	HINT_INITIAL_REPORT				= 'Permite consultar el estado de un Convenio';
    HINT_INVOICE_PROCESS_RESULTS	= 'Permite consultar los resultados de los Procesos de Facturaci�n';

    MSG_PERMISOS_FACTURACION        			= 'Permisos de facturaci�n';
    MSG_GENERAR_INTERESES           			= 'Permite facturar con / sin intereses generados de per�odos anteriores';
    MSG_PERMISOS_CAMBIAR_PARAMETROS_FACTURACION = 'Permite cambiar los par�metros de Facturaci�n';
    MSG_PERMISOS_ANULAR_RECIBO_DESDE_CARTOLA    = 'Permite anular recibos desde la cartola';
    MSG_PERMISOS_VER_TRANSITOS_FORMATO_CSV      = 'Permite ver los tr�nsitos en Formato CSV';
    MSG_PERMISOS_GENERAR_GASTO_COBRANZA_UNA_NK  = 'Permite seleccionar una Nota de Cobro cualquiera para la Generaci�n de Gastos de Cobranza';
    MSG_PERMISOS_CONCEPTOS_MOV_AGREGAR          = 'Permite agregar un concepto de movimientos';
    MSG_PERMISOS_CONCEPTOS_MOV_MODIFICACION     = 'Permite modificar un concepto de movimientos';
    MSG_PERMISOS_CONCEPTOS_MOV_BAJA             = 'Permite eliminar un concepto de movimientos';
    CAPTION_USER = 'Usuario: %s';
    MSG_PERMISOS_DATOS_CLIENTE          = 'Datos Cliente';
    MSG_EDICION_ACTIONLIST_TAG_PROPIOS  = 'Permite editar los ActionList de Telev�as Propios';
    MSG_EDICION_ACTIONLIST_TAG_TERCEROS = 'Permite editar los ActionList de Telev�as de Terceros';
    MSG_REALIZAR_RECLAMO_TRANSITOS      = 'Habilita a Realizar Reclamos de Transitos';
    MSG_REALIZAR_RECLAMO_ESTACIONAMIENTOS= 'Habilita a Realizar Reclamos de Estacionamientos';
    MSG_VER_PAT_PAC = 'Habilita a ver n�mero completo de Tarjeta de Cr�dito.';

    MSG_PERMISOS_IMPRIMIR_CARPETAS_LEGALES = 'Habilita a imprimir convenios en Carpeta Legal.';
    MSG_PERMISOS_ENVIAR_CARPETAS_LEGALES = 'Habilita a marcar como enviado un convenio a Legales.';
    MSG_PERMISOS_SALIDA_CARPETAS_LEGALES = 'Habilita a dar salida a un convenio en Carpetas Legales.';
    MSG_PERMISOS_ELIMINAR_CARPETAS_LEGALES = 'Habilita Eliminar un convenio en Carpetas Legales.';
    MSG_PERMISOS_PASAR_A_CARPETAS_LEGALES = 'Habilita Pasar un convenio a Carpetas Legales.';         //SS_1100_NDR_20130417

    MSG_ENVIAR_MAIL_NK = 'Habilita a enviar Comprobantes al cliente v�a mail.';

    //Revision 4
    MSG_PERMISOS_FECHA_EMISION_FACTURACION_MANUAL   = 'Permite editar la Fecha de Emisi�n en Facturaci�n Manual.';
    MSG_FECHA_RESERVA_DINERO                        = 'Permite editar la fecha de Emisi�n del comprobante Devoluci�n de Dinero';     //SS_959_MBE_20120528
    MSG_PUEDE_ELIMINAR_RESERVA_DEV_DINERO           = 'Permite activar el bot�n Eliminar en la ventana de reserva';                  //SS_959_MBE_20130226
    MSG_IMPRIMIR_NC_INFRACTORES                     = 'Permite imprimir las notas de cobro or morosidad que fueron generadas masivamente';  // SS_660_CQU_20121010
    MSG_ELIMINAR_CONVENIO_LISTA_AMARILLA            = 'Permite eliminar un convenio de lista amarilla';                                     // SS_660_CQU_20121010
    MSG_AGREGAR_CONVENIO_LISTA_AMARILLA             = 'Permite ingresar un convenio a seguimiento por lista amarilla';                                     // SS_660_CQU_20121010
    MSG_CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA      = 'Permite cambiar el estado de un convenio en lista amarilla';                                        // SS_660_CQU_20121010
    MSG_REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA      = 'Permite re-enviar carta de notificaci�n a convenios en lista amarilla';                             // SS_660_CQU_20121010
    MSG_AUTORIZA_FISCALIA_CONVENIO_LISTA_AMARILLA   = 'Permite que fiscal�a de la autorizaci�n para que un convenio sea dado de alta en lista amarilla';   // SS_660_CQU_20121010
    MSG_AUTORIZA_GERENCIA_CONVENIO_LISTA_AMARILLA   = 'Permite que gerencia de la autorizaci�n para que un convenio sea dado de alta en lista amarilla';   // SS_660_CQU_20121010
    MSG_RECHAZAR_CARTA_CONVENIO_LISTA_AMARILLA      = 'Permite rechazar carta de notificaci�n a convenios en lista amarilla';                              // SS_660_CQU_20130604

    //INICIO: TASK_018_ECA_20160530
    MSG_CONSULTA_CONVENIO_SEGURIDAD            = 'Seguridad - Consulta Convenio';
    MSG_TIPO_CONVENIO_TODOS                    = 'Permite Consultar todos los Convenios';
    MSG_TIPO_CONVENIO_RNUT                     = 'Permite Consultar todos los Convenios tipo RNUT';
    MSG_TIPO_CONVENIO_CTA_COMERCIAL            = 'Permite Consultar todos los Convenios tipo CUENTA COMERCIAL';
    MSG_TIPO_CONVENIO_INFRACTOR                = 'Permite Consultar todos los Convenios tipo INFRACTOR';
    //FIN: TASK_018_ECA_20160530
    //INICIO: TASK_117_JMA_20170320
    MSG_SEGURIDAD_ADMINISTRACION_ABM           = 'Seguridad - Administraci�n de ABMs';
    MSG_TIPOS_COMPROBANTES_ADMINISTRAR         = 'Tipos de Comprobantes - Administrar';
    //TERMINO: TASK_117_JMA_20170320

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}



function TMainForm.Inicializa: Boolean;
const
    FuncionesAdicionales: Array[1..39] of TPermisoAdicional = (                                                                // SS_660_CQU_20130604 //SS_1100_NDR_20130417 // SS_1047_CQU_20120612 // SS_660_CQU_20121010 //TASK_018_ECA_20160530 //TASK_117_JMA_20170320
        (Funcion: 'PERMISOS_FACTURACION'; Descripcion: MSG_PERMISOS_FACTURACION; Nivel: 1),
        (Funcion: 'GENERAR_INTERESES'; Descripcion: MSG_GENERAR_INTERESES; Nivel: 2),
        (Funcion: 'CAMBIAR_PARAMETROS_FACTURACION'; Descripcion: MSG_PERMISOS_CAMBIAR_PARAMETROS_FACTURACION; Nivel: 2),
        (Funcion: 'ANULAR_RECIBO_DESDE_CARTOLA'; Descripcion: MSG_PERMISOS_ANULAR_RECIBO_DESDE_CARTOLA; Nivel: 2),
        (Funcion: 'VER_TRANSITOS_FORMATO_CSV'; Descripcion: MSG_PERMISOS_VER_TRANSITOS_FORMATO_CSV; Nivel: 2),
        (Funcion: 'GENERAR_GASTO_COBRANZA_UNA_NK'; Descripcion: MSG_PERMISOS_GENERAR_GASTO_COBRANZA_UNA_NK; Nivel: 2),
        (Funcion: 'AGREGAR_CONCEPTOS_MOVIMIENTOS'; Descripcion: MSG_PERMISOS_CONCEPTOS_MOV_AGREGAR; Nivel: 2),
        (Funcion: 'MODIFICACION_CONCEPTOS_MOVIMIENTOS'; Descripcion: MSG_PERMISOS_CONCEPTOS_MOV_MODIFICACION; Nivel: 2),
        (Funcion: 'ELIMINACION_CONCEPTOS_MOVIMIENTOS'; Descripcion: MSG_PERMISOS_CONCEPTOS_MOV_BAJA; Nivel: 2),
        (Funcion: 'BTN_IMPRIMIR_CARPETAS_LEGALES'; Descripcion: MSG_PERMISOS_IMPRIMIR_CARPETAS_LEGALES; Nivel: 2),
        (Funcion: 'BTN_ENVIO_CARPETA_LEGALES'; Descripcion: MSG_PERMISOS_ENVIAR_CARPETAS_LEGALES; Nivel: 2),
        (Funcion: 'BTN_SALIDA_CARPETA_LEGALES'; Descripcion: MSG_PERMISOS_SALIDA_CARPETAS_LEGALES; Nivel: 2),
        (Funcion: 'BTN_ELIMINAR_CARPETA_LEGALES'; Descripcion: MSG_PERMISOS_ELIMINAR_CARPETAS_LEGALES; Nivel: 2),
        (Funcion: 'IMPRIMIR_NC_INFRACTORES'; Descripcion: MSG_IMPRIMIR_NC_INFRACTORES; Nivel: 2),                                       // SS_660_CQU_20121010
        (Funcion: 'ELIMINAR_CONVENIO_LISTA_AMARILLA'; Descripcion: MSG_ELIMINAR_CONVENIO_LISTA_AMARILLA; Nivel: 2),                     // SS_660_CQU_20121010
        (Funcion: 'AGREGAR_CONVENIO_LISTA_AMARILLA'; Descripcion: MSG_AGREGAR_CONVENIO_LISTA_AMARILLA; Nivel: 2),                       // SS_660_CQU_20121010
        (Funcion: 'CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA'; Descripcion: MSG_CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA; Nivel: 2),         // SS_660_CQU_20121010
        (Funcion: 'REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA'; Descripcion: MSG_REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA; Nivel: 2),         // SS_660_CQU_20121010
        (Funcion: 'AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA'; Descripcion:  MSG_AUTORIZA_FISCALIA_CONVENIO_LISTA_AMARILLA; Nivel: 2),      // SS_660_CQU_20121010
        (Funcion: 'AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA'; Descripcion:  MSG_AUTORIZA_GERENCIA_CONVENIO_LISTA_AMARILLA; Nivel: 2),      // SS_660_CQU_20121010
        (Funcion: 'RECHAZAR_CARTA_CONVENIO_LISTA_AMARILLA'; Descripcion: MSG_RECHAZAR_CARTA_CONVENIO_LISTA_AMARILLA; Nivel: 2),         // SS_660_CQU_20130604
        (Funcion: 'FECHA_EMISION_FACTURACION_MANUAL'; Descripcion: MSG_PERMISOS_FECHA_EMISION_FACTURACION_MANUAL; Nivel: 2), // Revision 4
        (Funcion: 'FECHA_EMISION_DEVOLUCION_DINERO'; Descripcion: MSG_FECHA_RESERVA_DINERO; Nivel: 2),                       // SS_959_MBE_20120528
        (Funcion: 'PUEDE_ELIMINAR_RESERVA_DEV_DINERO'; Descripcion: MSG_PUEDE_ELIMINAR_RESERVA_DEV_DINERO; Nivel: 2),        // SS_959_MBE_20130226
        (Funcion: 'PERMISOS_DATOS_CLIENTE'; Descripcion: MSG_PERMISOS_DATOS_CLIENTE; Nivel: 1),
        (Funcion: 'EDICION_ACTIONLIST_TAG_PROPIOS'; Descripcion: MSG_EDICION_ACTIONLIST_TAG_PROPIOS; Nivel: 2),
        (Funcion: 'EDICION_ACTIONLIST_TAG_TERCEROS'; Descripcion: MSG_EDICION_ACTIONLIST_TAG_TERCEROS; Nivel: 2),
        (Funcion: 'REALIZAR_RECLAMO_TRANSITOS'; Descripcion: MSG_REALIZAR_RECLAMO_TRANSITOS; Nivel: 2),
        (Funcion: 'Enviar_NK_Via_Mail'; Descripcion: MSG_ENVIAR_MAIL_NK; Nivel: 3),
        (Funcion: 'Ver_Numero_PAT_PAC'; Descripcion: MSG_VER_PAT_PAC; Nivel: 2) ,
        (Funcion: 'REALIZAR_RECLAMO_ESTACIONAMIENTOS'; Descripcion: MSG_REALIZAR_RECLAMO_ESTACIONAMIENTOS; Nivel: 2),         //SS_1100_NDR_20130417
        (Funcion: 'BTN_PASAR_A_CARPETA_LEGALES'; Descripcion: MSG_PERMISOS_PASAR_A_CARPETAS_LEGALES; Nivel: 2)                //SS_1100_NDR_20130417
        //INNICIO: TASK_018_20160530
        //Seguridad consulta tipo de convenio
        ,(Funcion: 'seguridad_consulta_convenio'; Descripcion: MSG_CONSULTA_CONVENIO_SEGURIDAD;      	Nivel: 1),
        (Funcion: 'TIPO_CONVENIO_TODOS'; Descripcion: MSG_TIPO_CONVENIO_TODOS; Nivel: 2),
        (Funcion: 'TIPO_CONVENIO_RNUT'; Descripcion: MSG_TIPO_CONVENIO_RNUT; Nivel: 2),
        (Funcion: 'TIPO_CONVENIO_CTA_COMERCIAL'; Descripcion: MSG_TIPO_CONVENIO_CTA_COMERCIAL; Nivel: 2),
        (Funcion: 'TIPO_CONVENIO_INFRACTOR'; Descripcion: MSG_TIPO_CONVENIO_INFRACTOR; Nivel: 2)
        //FIN:TASK_018_20160530
        //INICIO: TASK_117_JMA_20170320
        ,(Funcion: 'seguridad_administracion_abms'; Descripcion: MSG_SEGURIDAD_ADMINISTRACION_ABM;      	Nivel: 1)
        ,(Funcion: 'tipos_comprobantes_administrar'; Descripcion: MSG_TIPOS_COMPROBANTES_ADMINISTRAR;      	Nivel: 2)
        //TERMINO: TASK_117_JMA_20170320
        );                                                                                                                    //SS_1100_NDR_20130417
var
	f: TLoginForm;
    ok: boolean;
begin
	Result 	:= False;
    Application.Title   := SYS_FACTURACION_TITLE;
    SistemaActual       := SYS_FACTURACION;
    Width   := Screen.WorkAreaWidth;
    Height  := Screen.WorkAreaHeight;
    Left    := Screen.WorkAreaLeft;
    Top     := Screen.WorkAreaTop;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    CambiarEventoMenu(Menu);                                    //SS_1147_NDR_20141216
    try
        Application.CreateForm(TLoginForm, f);
        DMConnections.BaseCAC.Connected := False;
        if f.Inicializar(DMConnections.BaseCAC, SYS_FACTURACION) and (f.ShowModal = mrOk) then begin
            UsuarioSistema := f.CodigoUsuario;

            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
            //    Exit;
            //end;
            //
            //// Generamos el men� en la base de datos?
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
            //    Ok := GenerateMenuFile(Menu, SYS_FACTURACION, DMConnections.BaseCAC, FuncionesAdicionales );
            //end;

            //CargarInformes(DMConnections.BaseCAC, SYS_FACTURACION, menu);     // TASK_150_MGO_20170313      //SS_1437_NDR_20151230

            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, Menu, FuncionesAdicionales) then begin
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            ok := True;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------

            if ok then begin
                Caption := Caption +  ' >> ' + ObtenerNombreConcesionariaNativa();                                             //SS_1147AAA_NDR_20150414
                statusBar.Panels[0].text := Format(CAPTION_USER, [UsuarioSistema]);
                StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, [InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')])) + ' >> ' + ObtenerNombreConcesionariaNativa(); //SS_1147AAA_NDR_20150414
                StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));
                CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_FACTURACION);
                HabilitarPermisosMenu(Menu);
                CargarInformes(DMConnections.BaseCAC, SYS_FACTURACION, menu);   // TASK_150_MGO_20170313
                actSalir.Enabled := menuSalir.enabled;
                //Genero el Menu de Reclamos Generales
                GenerarMenuReclamosGenerales(mnu_ReclamosGenerales);
                CambiarEventoMenu(Menu);                                    //SS_1147_NDR_20141216
                Result := True;

                //INICIO: TASK_024_ECA_20160606
                mnu_talonarios.Visible:=False;
                //FIN: TASK_024_ECA_20160606
                mnu_ExploradorProcesosdeFacturacion.Visible := False;           // TASK_154_MGO_20170317
            end else begin
                Result := False;
            end;
        end
        else                                                                    //SS_1436_NDR_20151230
        begin                                                                   //SS_1436_NDR_20151230
            UsuarioSistema := f.CodigoUsuario;                                  //SS_1436_NDR_20151230
        end;                                                                    //SS_1436_NDR_20151230
        if Result then InicializarIconBar;
        if Result then VerificarExistenCotizacionesDelDia;
    finally
        f.Release;
        if Assigned(FormStartup) then FreeAndNil(FormStartup);
    end;
end;


{-----------------------------------------------------------------
                   mnuInhabilitacionPorListaAmarillaClick

Author      : mbecerra
Date        : 02-Octubre-2012
Description :       (Ref SS 660)
                Se invoca a la ventana de convenios inhabilitados
                por lista amarilla

----------------------------------------------------------------------}
procedure TMainForm.mnuInhabilitacionPorListaAmarillaClick(Sender: TObject);
begin
    Application.CreateForm(TListaDeConveniosEnListaAmarillaForm, ListaDeConveniosEnListaAmarillaForm);              //SS_660_MBE_20121002
    if ListaDeConveniosEnListaAmarillaForm.Inicializar() then ListaDeConveniosEnListaAmarillaForm.ShowModal;        //SS_660_MBE_20121002
    ListaDeConveniosEnListaAmarillaForm.Release;                                                                    //SS_660_MBE_20121002
end;

procedure TMainForm.EliminarHotLinks1Click(Sender: TObject);
begin
	GNavigator.ClearHotLinks;
end;

procedure TMainForm.actAjusteFacturacionExecute(Sender: TObject);
Var f: TfrmAjusteFacturacion;
begin
	if FindFormOrCreate(TfrmAjusteFacturacion, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

procedure TMainForm.actAsistenteFacturacionExecute(Sender: TObject);
Var
	f: TFormFacturacionMasiva;
begin
	if FindFormOrCreate(TFormFacturacionMasiva, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

procedure TMainForm.actSalirExecute(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.actUltimosConsumosExecute(Sender: TObject);
var
	f: TFormUltimosConsumos;
begin
	if FindFormOrCreate(TFormUltimosConsumos, f) then
		f.Show
	else begin
		if not f.Inicializar(0, 0) then f.Release;
	end;
end;

procedure TMainForm.actComprobantesEmitidosExecute(Sender: TObject);
var
	f: TFormComprobantesEmitidos;
begin
	if FindFormOrCreate(TFormComprobantesEmitidos, f) then
		f.Show
	else begin
		if not f.Inicializar(0) then f.Release;
	end;
end;

procedure TMainForm.actCambiarPasswordExecute(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnection.DMConnections.BaseCAC,UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.actGruposDeFacturacionExecute(Sender: TObject);
var
	f: TFormGruposFacturacion;
begin
	if FindFormOrCreate(TFormGruposFacturacion, f) then
		f.Show
	else begin
		if not f.Inicializa then f.Release;
	end;
end;

procedure TMainForm.actCalendarioExecute(Sender: TObject);
var
	f: TFrmAgendaFacturacion;
begin
	if FindFormOrCreate(TFrmAgendaFacturacion, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
    DrawMDIBackground(imagenFondo.Picture.Graphic);
end;

procedure TMainForm.frmAnulacionNotaCobroDirectaClick(Sender: TObject);
var
	f : TAnulacionNotaCobroDirectaForm;
begin
	if FindFormOrCreate(TAnulacionNotaCobroDirectaForm, f) then f.Show
    else if not f.Inicializar() then f.Release;
end;

procedure TMainForm.mnu_SiguienteClick(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

procedure TMainForm.mnuMovimientosNoFacturadosClick(Sender: TObject);
resourcestring
    MSG_TITULO = 'Movimientos No Facturados';

var
	f : TMovimientosNoFacturadosForm;
begin
    if FindFormOrCreate(TMovimientosNoFacturadosForm, f) then f.Show
    else begin
    	if not f.Inicializar(MSG_TITULO) then f.Release
        else f.Show;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: mnu_ABMEmpresasRecaudadorasClick
  Author: mpiazza
  Date Created: 03/02/2009
  Description: Llamo al formulario de empresas recaudadoras
  Return N/A
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_ABMEmpresasRecaudadorasClick(Sender: TObject);
var
    f: TTABMEmpresasRecaudadorasForm;
begin
    if FindFormOrCreate(TTABMEmpresasRecaudadorasForm, f) then
        f.Show
    else begin
  	    if not f.Inicializar then
            f.Release
        else
            f.Show;
    end;
end;

// INICIO : TASK_086_MGO_20161121
procedure TMainForm.mnu_ABMMotivosNoFacturarClick(Sender: TObject);
var
  f : TfrmABMMotivosNoFacturar;
begin

  if FindFormOrCreate(TfrmABMMotivosNoFacturar, f) then
        f.Show
   else if not f.Inicializar(True) then
       frmABMMotivosNoFacturar.Release;
end;
// FIN : TASK_086_MGO_20161121

// INICIO : TASK_146_MGO_20170303
procedure TMainForm.mnu_ABMReglasDescuentoClick(Sender: TObject);
var
    f: TfrmABMReglasDescuentos;
begin
    if FindFormOrCreate(TfrmABMReglasDescuentos, f) then
        f.Show
    else begin
        if f.Inicializar then
            f.Show
        else
            f.Release;
    end;
end;
// FIN : TASK_146_MGO_20170303

procedure TMainForm.mnu_ABMTiposdeDeudasClick(Sender: TObject);
var
    f: TABMTiposDeudasForm;
begin
    if FindFormOrCreate(TABMTiposDeudasForm, f) then
        f.Show
    else begin
  	    if not f.Inicializar then
            f.Release
        else
            f.Show;
    end;
end;

procedure TMainForm.mnu_AnteriorClick(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

procedure TMainForm.mnu_CascadaClick(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;



procedure TMainForm.mnu_ControlIndemnizacionesClick(Sender: TObject);
var
	f: TFormControlIndemnizaciones;
begin
    if FindFormOrCreate(TFormControlIndemnizaciones, f) then
        f.Show
    else begin
  	    if not f.Inicializar(StringReplace(mnu_ControlIndemnizaciones.Caption, '&', EmptyStr, [rfReplaceAll])) then f.release
        else f.show; // modal;
    end;
end;

{----------------------------------------------------------------------------------
                                 mnu_CrearComprobanteSDClick

Author		:	mbecerra
Date		:	11-Mayo-2015
Description	:	SS_1268_MBE_20150511
            	Despliega la ventana de Ingreso de comprobante SD
                
-----------------------------------------------------------------------------------}
procedure TMainForm.mnu_CrearComprobanteSDClick(Sender: TObject);
begin
	// INICIO		:20160613 CFU
    //Descripci�n	 :Se elimina esta opci�n
    {
    if FindFormOrCreate(TIngresoEspecialSDForm, IngresoEspecialSDForm) then begin
        IngresoEspecialSDForm.Show;
    end
    else if not IngresoEspecialSDForm.Inicializar() then IngresoEspecialSDForm.Release;
    }
    //TERMINO      2016/06/13 cfu
end;

procedure TMainForm.mnu_MosaicoClick(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

procedure TMainForm.mnu_ventanaClick(Sender: TObject);
begin
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

procedure TMainForm.NavisionABMConceptosProyecto1Click(Sender: TObject);      // PAN_CU.COBO.ADM.CNT.002_20170411
var                                                                           // PAN_CU.COBO.ADM.CNT.002_20170411
	f:TFormConceptosProyeecto;                                                // PAN_CU.COBO.ADM.CNT.002_20170411
begin                                                                         // PAN_CU.COBO.ADM.CNT.002_20170411
	if FindFormOrCreate(TFormConceptosProyeecto, f) then                      // PAN_CU.COBO.ADM.CNT.002_20170411
		f.Show                                                                // PAN_CU.COBO.ADM.CNT.002_20170411
	else begin                                                                // PAN_CU.COBO.ADM.CNT.002_20170411
		if not f.Inicializa then f.Release;                                   // PAN_CU.COBO.ADM.CNT.002_20170411
	end;                                                                      // PAN_CU.COBO.ADM.CNT.002_20170411
end;                                                                          // PAN_CU.COBO.ADM.CNT.002_20170411

procedure TMainForm.actIconBarExecute(Sender: TObject);
begin
    GNavigator.IconBarVisible   := not GNavigator.IconBarVisible;
 	actIconBar.Checked          := GNavigator.IconBarVisible;
end;

{******************************** Function Header ******************************
Function Name: actAcercaDeExecute
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : ggomez
    Date : 29/01/2007
    Description : En la llamada f.Inicializa, agregu� el segundo par�metro ''
        debido a que cambi� el perfil de la funci�n en las Utiles.
*******************************************************************************}
procedure TMainForm.actAcercaDeExecute(Sender: TObject);
{                                                         SS_925_MBE_20131223
resourcestring
    CAPTION_ACERCADE = 'Facturaci�n';
Var
	f: TAboutFrm;
    a: TfrmLocalAssistance;
}
begin
{                                                        SS_925_MBE_20131223
    Application.CreateForm(TfrmLocalAssistance, a);
    if a.Inicializar then begin
	    Application.CreateForm(TAboutFrm, f);
	    if f.Inicializa(CAPTION_ACERCADE, '', '', a) then f.ShowModal;
	    f.Release;
    end;
    a.Release;
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

procedure TMainForm.actFacturacionUnCOnvenioExecute(Sender: TObject);
var
	f: TFacturacionManualForm;
begin
	if FindFormOrCreate(TFacturacionManualForm, f) then
		f.Show
	else begin
		if not f.Inicializar(TC_NOTA_COBRO) then f.Release;
	end;
end;

procedure TMainForm.ExploradordeProcesosdeFacturacin1Click(
 Sender: TObject);
begin
end;


{-----------------------------------------------------------------------------
  Function Name: mnu_EditarReclamosClick
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: Listado de los Reclamos de los Usuarios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_EditarReclamosClick(Sender: TObject);
Var
	f: TFReclamos;
begin
	if FindFormOrCreate(TFReclamos, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_EstadisticaReclamosClick
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: Reporte de Estadistica de Reclamos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_EstadisticaReclamosClick(Sender: TObject);
var
    f:TFRptEstadisticaReclamos;
begin
    //muestro el reporte
    Application.createForm(TFRptEstadisticaReclamos, f);
    if not f.Inicializar('Estad�stica de Reclamos') then f.Release;
end;

{-----------------------------------------------------------------------------
  Function Name: Mnu_OrdenesServicioBloquadasClick
  Author:    lgisuk
  Date Created: 06/04/2005
  Description: Ordenes de Servicio Bloqueadas
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_OrdenesServicioBloquadasClick(Sender: TObject);
Var
	f:  TFOrdenesServicioBloquedas;
begin
	if FindFormOrCreate( TFOrdenesServicioBloquedas, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: actIngresoComprobantesExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actIngresoComprobantesExecute(Sender: TObject);
var
	f: TfrmIngresarComprobantes;
begin
	if FindFormOrCreate(TfrmIngresarComprobantes, f) then
		f.Show
	else begin
		if not f.Inicializar(False) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: actConsultaInicialConvenioExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actConsultaInicialConvenioExecute(Sender: TObject);
var
	f: TFormInicioConsultaConvenio;
begin
	if FindFormOrCreate(TFormInicioConsultaConvenio, f) then
		f.Show
	else begin
		if f.Inicializar(0, -1, -1) then begin
            f.Show;
        end
        else f.Release;
	end;
end;

procedure TMainForm.actPersonasVIPNoInhabilitadasExecute(Sender: TObject);      //SS_1006_1015_NDR_20121015
var                                                                             //SS_1006_1015_NDR_20121015
    f: TformPersonasVIPNoInhabilitadas;                                         //SS_1006_1015_NDR_20121015
begin                                                                           //SS_1006_1015_NDR_20121015
    if FindFormOrCreate(TformPersonasVIPNoInhabilitadas, f) then begin          //SS_1006_1015_NDR_20121015
        f.Show;                                                                 //SS_1006_1015_NDR_20121015
    end else begin                                                              //SS_1006_1015_NDR_20121015
       if f.Inicializar then f.Show else f.Release;                             //SS_1006_1015_NDR_20121015
    end;                                                                        //SS_1006_1015_NDR_20121015
end;                                                                            //SS_1006_1015_NDR_20121015


procedure TMainForm.actExploradorProcesosFacturacionExecute(Sender: TObject);
var
    f: TfrmInvoicing;
begin
    if FindFormOrCreate(TfrmInvoicing, f) then begin
        f.Show;
    end else begin
        if f.Inicializar then f.Show else f.Release;
    end;
end;

procedure TMainForm.actAnulacionNotasCobroExecute(Sender: TObject);
var
	f: Tfrm_AnulacionNotasCobro;
begin
	if FindFormOrCreate(Tfrm_AnulacionNotasCobro, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
resourcestring                                                                                    //SS_1436_NDR_20151230
	MSG_REGISTRO_CAPTION	= 'Error al Grabar Registro de Operaciones';                          //SS_1436_NDR_20151230
var                                                                                               //SS_1436_NDR_20151230
  DescriError: array[0..255] of char;                                                             //SS_1436_NDR_20151230
begin
    (* Guardar las opciones de configuraci�n del usuario. *)
    GuardarUserSettings;


    if CrearRegistroOperaciones(                                                                  //SS_1436_NDR_20151230
            DMCOnnections.BaseCAC,                                                                //SS_1436_NDR_20151230
            SYS_FACTURACION,                                                                      //SS_1436_NDR_20151230
            RO_MOD_LOGOUT,                                                                        //SS_1436_NDR_20151230
            RO_AC_LOGOUT,                                                                         //SS_1436_NDR_20151230
            0,                                                                                    //SS_1436_NDR_20151230
            GetMachineName,                                                                       //SS_1436_NDR_20151230
            UsuarioSistema,                                                                       //SS_1436_NDR_20151230
            'LOGOUT DEL SISTEMA',                                                                 //SS_1436_NDR_20151230
            0,0,0,0,                                                                              //SS_1436_NDR_20151230
            DescriError) <> -1 then ModalResult := mrOk                                           //SS_1436_NDR_20151230
    else                                                                                          //SS_1436_NDR_20151230
    begin                                                                                         //SS_1436_NDR_20151230
      MsgBoxErr(MSG_ERROR_ACCESO, StrPas(DescriError), MSG_REGISTRO_CAPTION, MB_ICONSTOP);        //SS_1436_NDR_20151230
    end;                                                                                          //SS_1436_NDR_20151230
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
    {$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF};
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.InicializarIconBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: None
  Result:    None
  Description: Inicializa la barra de �conos.
-----------------------------------------------------------------------------}
procedure TMainForm.InicializarIconBar;
var
    Bmp: TBitmap;
    NavigatorHasPages : Boolean;
begin
    //------------------------------------------------------------------------
	// Navegador
    //------------------------------------------------------------------------
	Bmp                         := TBitmap.Create;
	GNavigator                  := TNavigator.Create(Self);
    GNavigator.ShowBrowserBar   := GetUserSettingShowBrowserBar;
    act_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;

    GNavigator.ShowHotLinkBar   := GetUserSettingShowHotLinksBar;
    act_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;

    GNavigator.IconBarVisible   := False;
    NavigatorHasPages           := False;

	//----------------------------------------------------------
	// Agregamos las p�ginas e �conos HABILITADAS al toolbar
	//----------------------------------------------------------
	if 	ExisteAcceso('mnu_Facturacion') or  ExisteAcceso('mnu_AjusteFacturacion') or
            ExisteAcceso('mnu_FacturacioManual') or ExisteAcceso('mnu_IngresoComprobantes') then begin

		GNavigator.AddPage(PAGINA_PROCESOS, CAPTION_PROCESS, EmptyStr);
    	NavigatorHasPages := True;
    end;

	if 	ExisteAcceso('mnu_ConsultainicialConvenios') or ExisteAcceso('mnu_ComprobantesEmitidos') or
        	ExisteAcceso('mnu_UltimosConsumos') or ExisteAcceso('mnu_ExploradorProcesosdeFacturacion') then begin

  		GNavigator.AddPage(PAGINA_CONSULTAS, CAPTION_QUERIES, EmptyStr);
    	NavigatorHasPages := True;
    end;

	//----------------------------------------------------------
	// Agregamos items en la p�gina de Procesos
	if ExisteAcceso('mnu_Facturacion') then begin
        Imagenes.GetBitmap(3, Bmp);
        GNavigator.AddIcon(PAGINA_PROCESOS, CAPTION_WIZARD, HINT_WIZARD, Bmp, actAsistenteFacturacionExecute);
        Bmp.Assign(nil);
    end;

    actCarpetasMorosos.Enabled := ExisteAcceso('mnu_CarpetaDeudores');

	//----------------------------------------------------------
    // Carga los Iconos del ToolBar
	if	ExisteAcceso('mnu_AjusteFacturacion') then begin
		Imagenes.GetBitmap(1, Bmp);
        GNavigator.AddIcon(PAGINA_PROCESOS, CAPTION_INVOICE_ADJUSTMENT, HINT_INVOICE_ADJUSTMENT, Bmp, actAjusteFacturacionExecute);
        Bmp.Assign(nil);
    end;

	if	ExisteAcceso('mnu_FacturacioManual') then begin
        Imagenes.GetBitmap(6, Bmp);
        GNavigator.AddIcon(PAGINA_PROCESOS, CAPTION_MANUAL_INVOICING, HINT_MANUAL_INVOICING, Bmp, actFacturacionUnCOnvenioExecute);
        Bmp.Assign(nil);
	end;

	if	ExisteAcceso('mnu_IngresoComprobantes') then begin
        Imagenes.GetBitmap(7, Bmp);
        GNavigator.AddIcon(PAGINA_PROCESOS, CAPTION_INVOICE_INPUT, HINT_INVOICE_INPUT, Bmp, actIngresoComprobantesExecute);
        Bmp.Assign(nil);
	end;

    // --------------------------------------------------
    // Agregamos items en la p�gina de Consultas
	if 	ExisteAcceso('mnu_ConsultainicialConvenios') then begin
        Imagenes.GetBitmap(8, Bmp);
        GNavigator.AddIcon(PAGINA_CONSULTAS, CAPTION_INITIAL_REPORT, HINT_INITIAL_REPORT, Bmp, actConsultaInicialConvenioExecute);
        Bmp.Assign(nil);
	end;

	if 	ExisteAcceso('mnu_ComprobantesEmitidos') then begin
        Imagenes.GetBitmap(4, Bmp);
        GNavigator.AddIcon(PAGINA_CONSULTAS, CAPTION_EMITTED_INVOICES, HINT_EMITTED_INVOICES, Bmp, actComprobantesEmitidosExecute);
        Bmp.Assign(nil);
    end;

	if 	ExisteAcceso('mnu_UltimosConsumos') then begin
        Imagenes.GetBitmap(0, Bmp);
        GNavigator.AddIcon(PAGINA_CONSULTAS, CAPTION_LAST_TRANSITS, HINT_LAST_TRANSITS, Bmp, actUltimosConsumosExecute);
        Bmp.Assign(nil);
    end;

	if 	ExisteAcceso('mnu_ExploradorProcesosdeFacturacion') then begin
        Imagenes.GetBitmap(5, Bmp);
        GNavigator.AddIcon(PAGINA_CONSULTAS, CAPTION_INVOICE_PROCESS_RESULTS, HINT_INVOICE_PROCESS_RESULTS, Bmp, actExploradorProcesosFacturacionExecute);
        Bmp.Assign(nil);
    end;

    actReaplicarConvenio.Enabled := ExisteAcceso('mnu_ReaplicarConvenio');

    actABMTiposComprobantes.Enabled := ExisteAcceso('mnu_TiposComprobantes');

    actLocales.Enabled := ExisteAcceso('mnu_Locales');

    actTalonarios.Enabled := ExisteAcceso('mnu_Talonarios');

    actCuentasContables.Enabled := ExisteAcceso('mnu_CuentasContables');


    //Rev.10 / 22-Julio-2010 / Nelson Droguett Sierra --------------------------------------
    actTasasInteres.Enabled := ExisteAcceso('mnuTasasInteres');

	if NavigatorHasPages then GNavigator.IconBarActivePage := 0;

	Bmp.Free;
	Update;

    RefrescarEstadosIconBar;

    GNavigator.IconBarVisible   := GetUserSettingShowIconBar;
    actIconBar.Checked		    := GNavigator.IconBarVisible;

    GNavigator.ConnectionCOP    := Nil;
    GNavigator.ConnectionCAC    := DMConnections.BaseCAC;

end;



procedure TMainForm.mnuTipificacionyNumeracionMasivaClick(Sender: TObject);
var
    Tm : TTipificacionMasivaForm;
begin
    if FindFormOrCreate(TTipificacionMasivaForm, Tm) then Tm.Show
    else  if not tm.Inicializar then tm.Release;
end;



procedure TMainForm.RefrescarEstadosIconBar;
begin
    if (GNavigator = Nil) then Exit;

    GNavigator.SetIconEnabled(PAGINA_PROCESOS, CAPTION_WIZARD, actAsistenteFacturacion.Enabled);
    GNavigator.SetIconEnabled(PAGINA_PROCESOS, CAPTION_INVOICE_ADJUSTMENT, actAjusteFacturacion.Enabled);
    GNavigator.SetIconEnabled(PAGINA_PROCESOS, CAPTION_MANUAL_INVOICING, actFacturacionUnCOnvenio.Enabled);
    GNavigator.SetIconEnabled(PAGINA_PROCESOS, CAPTION_INVOICE_INPUT, actIngresoComprobantes.Enabled);

    GNavigator.SetIconEnabled(PAGINA_CONSULTAS, CAPTION_INITIAL_REPORT, actConsultaInicialConvenio.Enabled);
    GNavigator.SetIconEnabled(PAGINA_CONSULTAS, CAPTION_EMITTED_INVOICES, actComprobantesEmitidos.Enabled);
    GNavigator.SetIconEnabled(PAGINA_CONSULTAS, CAPTION_LAST_TRANSITS, actUltimosConsumos.Enabled);
    GNavigator.SetIconEnabled(PAGINA_CONSULTAS, CAPTION_INVOICE_PROCESS_RESULTS, actExploradorProcesosFacturacion.Enabled);
end;

procedure TMainForm.SaltosdeNumeracionClick(Sender: TObject);                   //SS_1233_MCA_20141201
var                                                                             //SS_1233_MCA_20141201
    f: TFormSaltosNumeracion;                                                   //SS_1233_MCA_20141201
begin                                                                           //SS_1233_MCA_20141201
    if FindFormOrCreate(TFormSaltosNumeracion, f) then begin                    //SS_1233_MCA_20141201
        f.Show;                                                                 //SS_1233_MCA_20141201
    end else begin                                                              //SS_1233_MCA_20141201
        if not f.Inicializa() then                                              //SS_1233_MCA_20141201
            f.Release                                                           //SS_1233_MCA_20141201
        else                                                                    //SS_1233_MCA_20141201
            f.Show;                                                             //SS_1233_MCA_20141201
    end;                                                                        //SS_1233_MCA_20141201
end;                                                                            //SS_1233_MCA_20141201

procedure TMainForm.act_BarraDeNavegacionExecute(Sender: TObject);
begin
    GNavigator.ShowBrowserBar       := not GNavigator.ShowBrowserBar;
    act_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;
end;

procedure TMainForm.act_BarraDeHotLinksExecute(Sender: TObject);
begin
    GNavigator.ShowHotLinkBar   := not GNavigator.ShowHotLinkBar;
    act_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.GuardarUserSettings
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: None
  Result:    None
  Description: Guarda las opciones de configuraci�n para el usuario logueado
    en el Sistema Operativo.
-----------------------------------------------------------------------------}
procedure TMainForm.GuardarUserSettings;
begin
    SetUserSettingShowIconBar(actIconBar.Checked);
    SetUserSettingShowBrowserBar(act_BarraDeNavegacion.Checked);
    SetUserSettingShowHotLinksBar(act_BarraDeHotLinks.Checked);
end;

procedure TMainForm.GenerarEmailsClick(Sender: TObject);
var
    f: TfrmMailsFacturacion;
begin
    if FindFormOrCreate(TfrmMailsFacturacion, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar then f.Release;
    end;
end;

procedure TMainForm.mnu_GenerarNotadeCobroDirectaClick(Sender: TObject);
var
    f: Tform_NotaDeCobroDirecta;
begin
    if FindFormOrCreate(Tform_NotaDeCobroDirecta, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar then f.Release;
    end;
end;

procedure TMainForm.mnu_ImprimirNotaCobroDirectaClick(Sender: TObject);
var
    f: Tform_ImprimirNotaCobroDirecta;
begin
    if FindFormOrCreate(Tform_ImprimirNotaCobroDirecta, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar then f.Release;
    end;
end;

procedure TMainForm.GenerarGastosporGestindeCombranzas1Click(Sender: TObject);
var
    f: TfrmGeneracionGastosGestionCobranza;
begin
    if FindFormOrCreate(TfrmGeneracionGastosGestionCobranza, f) then f.Show
    else if not f.Inicializar(GenerarGastosporGestindeCombranzas1.Caption) then f.Release;
end;

procedure TMainForm.AnulacindeGastosporGestindeCobranzas1Click(Sender: TObject);
var
    f: TfrmAnulacionGastosGestionCobranza;
begin
    if FindFormOrCreate(TfrmAnulacionGastosGestionCobranza, f) then f.Show
	else if not f.Inicializar(AnulacindeGastosporGestindeCobranzas1.Caption) then f.Release;
end;



procedure TMainForm.mnu_ConceptosMovimientosMaestroClick(Sender: TObject);
var
	f:TFormConceptosMovimientos;
begin
    Application.CreateForm(TFormConceptosMovimientos, f);
    if f.Inicializa('', False) then f.ShowModal;
    f.Release;
end;

// INICIO : TASK_100_MGO_20161223
procedure TMainForm.mnu_ConsultarFacturacionesClick(Sender: TObject);
var
    frmFacturaciones : TfrmFacturaciones;
begin
    if FindFormOrCreate(TfrmFacturaciones, frmFacturaciones) then
        frmFacturaciones.Show
    else if frmFacturaciones.Inicializar then
        frmFacturaciones.Show
    else
        frmFacturaciones.Release;
end;
// FIN : TASK_100_MGO_20161223

// INICIO : TASK_086_MGO_20161121
procedure TMainForm.mnu_ConsultarPreFacturacionesClick(Sender: TObject);
var
    frmPreFacturaciones : TfrmPreFacturaciones;
begin
    if FindFormOrCreate(TfrmPreFacturaciones, frmPreFacturaciones) then
        frmPreFacturaciones.Show
    else if frmPreFacturaciones.Inicializar then
        frmPreFacturaciones.Show
    else
        frmPreFacturaciones.Release;
end;
// FIN : TASK_086_MGO_20161121

procedure TMainForm.actConceptosMovimientosExecute(Sender: TObject);
var
	f:TFormConceptosMovimientos;
begin
    Application.CreateForm(TFormConceptosMovimientos, f);
    if f.Inicializa then f.ShowModal;
    f.Release;
end;

procedure TMainForm.act_IngresodeMovimientosCuentaExecute(Sender: TObject);
var
	f:TfrmIngresarMovimientos;
begin
	// Abre el form
	if FindFormOrCreate(TfrmIngresarMovimientos, f) then f.Show
	else if not f.Inicializar then f.Release;

end;

procedure TMainForm.actCarpetasMorososExecute(Sender: TObject);
var
    f: TFormCarpetaLegales;
begin
    if FindFormOrCreate(TFormCarpetaLegales, f) then
        f.Show
    else begin
  	    if not f.Inicializar(mnu_CarpetaDeudores.Caption) then
            f.Release
        else
            f.Show; // modal;
    end;

end;

procedure TMainForm.actReaplicarConvenioExecute(Sender: TObject);
var
	f: TformReaplicarConvenio;
begin
    if FindFormOrCreate(TformReaplicarConvenio, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;

procedure TMainForm.actABMCotizacionesUFExecute(Sender: TObject);
var
	f: TFormABMCotizacionDiariaUF;
begin
    if FindFormOrCreate(TFormABMCotizacionDiariaUF, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;

procedure TMainForm.actABMTiposComprobantesExecute(Sender: TObject);
var
	f: TFormTiposComprobante;
begin
    if FindFormOrCreate(TFormTiposComprobante, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;

procedure TMainForm.actLocalesExecute(Sender: TObject);
var
	f: TFormLocales;
begin
    if FindFormOrCreate(TFormLocales, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;
//INICIO BLOQUE SS_959_ALA_20111003
procedure TMainForm.actPregeneracionDevolucionDineroExecute(Sender: TObject);
begin
    if FindFormOrCreate(TReservaComprobanteDevDineroForm, ReservaComprobanteDevDineroForm) then
        ReservaComprobanteDevDineroForm.Show
    else begin
  	    if not ReservaComprobanteDevDineroForm.Inicializar then ReservaComprobanteDevDineroForm.release
        else ReservaComprobanteDevDineroForm.show; // modal;
    end;
end;
//TERMINO BLOQUE SS_959_ALA_20111003

procedure TMainForm.actTalonariosExecute(Sender: TObject);
var
	f: TFormABMTalonariosComprobantesLocales;
begin
    if FindFormOrCreate(TFormABMTalonariosComprobantesLocales, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;

//Rev.10 / 22-Julio-2010 / Nelson Droguett Sierra--------------------------------------------
procedure TMainForm.actTasasInteresExecute(Sender: TObject);
var
    f: TFAbmTasasInteres;
begin
    if FindFormOrCreate(TFAbmTasasInteres, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;
end;

{INICIO	: CFU 20170224 TASK_114_CFU_20170224-Administrar_Tasas_De_Interes
procedure TMainForm.actTasasInteresReajustablesExecute(Sender: TObject);
var
    f: TFAbmTasasInteresReajustables;
begin
    if FindFormOrCreate(TFAbmTasasInteresReajustables, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;
end;
TERMINO	: CFU 20170224 TASK_114_CFU_20170224-Administrar_Tasas_De_Interes}

//FinRev.10-----------------------------------------------------------------------------------

procedure TMainForm.actCuentasContablesExecute(Sender: TObject);
var
	f: TFormCuentasContables;
begin
    if FindFormOrCreate(TFormCuentasContables, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;

procedure TMainForm.actAgregarCuotaComprobanteExecute(Sender: TObject);
var
	f: TIncrementoCuotaArriendoForm;
begin
    if FindFormOrCreate(TIncrementoCuotaArriendoForm, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;

procedure TMainForm.mnuImprimirNCaNQClick(Sender: TObject);
var
    f: Tform_ImprimirNotaCreditoNQ;
begin
    if FindFormOrCreate(Tform_ImprimirNotaCreditoNQ, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar then f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure     : TMainForm.mnuImprimirNCInfractoresClick
  Author        : CQuezadaI
  Date          : 10-Diciembre-2012
  Firma         : SS_660_CQU_20121010
  Description   : Inicializa el formulario de Impresi�n de Infractores Masivos
  Parameters    : Sender: TObject
  Result        : None
-----------------------------------------------------------------------------}
procedure TMainForm.mnuImprimirNCInfractoresClick(Sender: TObject);
var
    f: Tform_SeleccionarProcesosMorosos;
begin
    if FindFormOrCreate(Tform_SeleccionarProcesosMorosos, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar then f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure     : TMainForm.mnuABMEstadosListaAmarillaClick
  Author        : CQuezadaI
  Date          : 18-Febrero-2013
  Firma         : SS_660_CQU_20121010
  Description   : Inicializa el formulario ABM de Estado de Convenios en Lista Amarilla
  Parameters    : Sender: TObject
  Result        : None
-----------------------------------------------------------------------------}
procedure TMainForm.mniN12Click(Sender: TObject);                     // PAN_CU.COBO.ADM.CNT.001_20170411
var                                                                   // PAN_CU.COBO.ADM.CNT.001_20170411
	f:TFormDimensiones;                                               // PAN_CU.COBO.ADM.CNT.001_20170411
begin                                                                 // PAN_CU.COBO.ADM.CNT.001_20170411
	if FindFormOrCreate(TFormDimensiones, f) then                     // PAN_CU.COBO.ADM.CNT.001_20170411
		f.Show                                                        // PAN_CU.COBO.ADM.CNT.001_20170411
	else begin                                                        // PAN_CU.COBO.ADM.CNT.001_20170411
		if not f.Inicializa then f.Release;                           // PAN_CU.COBO.ADM.CNT.001_20170411
	end;                                                              // PAN_CU.COBO.ADM.CNT.001_20170411
end;                                                                  // PAN_CU.COBO.ADM.CNT.001_20170411

procedure TMainForm.mnuABMEstadosListaAmarillaClick(Sender: TObject);
var
    f : TFormEstadosListaAmarilla;
begin
    if FindFormOrCreate(TFormEstadosListaAmarilla, f) then
        f.Show
    else if not f.Inicializa then f.Release;
end;

{-----------------------------------------------------------------------------
  Procedure     : TMainForm.mnuABMEstadosCartasListaAmarillaClick
  Author        : MDiaz
  Date          : 19-Febrero-2013
  Firma         : SS_660_MDV_20121010
  Description   : Inicializa el formulario ABM de Estado de Cartas Lista Amarilla
  Parameters    : Sender: TObject
  Result        : None
-----------------------------------------------------------------------------}

procedure TMainForm.mnuEmpresasCorreoClick(Sender: TObject);
var
    f : TFormEmpresasCorreo;
begin
    if FindFormOrCreate(TFormEmpresasCorreo, f) then
        f.Show
    else if not f.Inicializa then f.Release;
end;


//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TMainForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
  with ACanvas do
  begin
    S := TMenuItem(Sender).Caption;
    if Selected then
    begin
        Brush.Color := FColorMenuSel;
        Font.Color := FColorFontSel;
    end
    else
    begin
        if TMenuItem(Sender).Enabled then
        begin
            Brush.Color := FColorMenu;
            Font.Color := FColorFont;
            Font.Style:=[fsBold];
        end
        else
        begin
            Brush.Color := FColorMenu;
            Font.Color := clGrayText;
            Font.Style:=[];
        end;
    end;

    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 10 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

    if S='-' then
    begin
      with ACanvas do
      begin
        ACanvas.MoveTo(ARect.Left,ARect.top + 4);
        ACanvas.Pen.Color := FColorFont;
        ACanvas.LineTo(ARect.Right, ARect.top + 4 );
      end;
    end;

  end;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TMainForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

end.

