{********************************** Unit Header ********************************
Revision 1
Author: nefernandez
Date: 20/03/2007
Description : Se ampli� el espacio a los t�tulos de las columnas del reporte y
se agreg� la fecha al extremo superior derecho

Revision 2
Author: nefernandez
Date: 21/03/2007
Description : El reporte se imprime solo si tiene datos. Se agregaron campos
para mostrar los datos del cliente


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

*******************************************************************************}

unit rptTransitosNoFacturadosfrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppCtrls, ppPrnabl, ppClass, ppBands, ppCache, UtilRB, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppDBPipe, DB, ADODB, ppParameter, ppVar,
  DBClient, UtilProc, ConstParametrosGenerales;                                            //SS_1147_NDR_20140710

type
  TRptTransitosNoFacturadosForm = class(TForm)
    spObtenerTransitosNoFacturadosDeConvenio: TADOStoredProc;
    dsspObtenerTransitosNoFacturadosDeConvenio: TDataSource;
    plTransitosNoFacturados: TppDBPipeline;
    plTransitosNoFacturadosppField1: TppField;
    plTransitosNoFacturadosppField2: TppField;
    plTransitosNoFacturadosppField3: TppField;
    plTransitosNoFacturadosppField4: TppField;
    plTransitosNoFacturadosppField5: TppField;
    plTransitosNoFacturadosppField6: TppField;
    plTransitosNoFacturadosppField7: TppField;
    plTransitosNoFacturadosppField8: TppField;
    plTransitosNoFacturadosppField9: TppField;
    rptTransitosNoFacturados: TppReport;
    rbiTransitosNoFacturados: TRBInterface;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppLabel61: TppLabel;
    ppLabel70: TppLabel;
    ppLine24: TppLine;
    ppLabel71: TppLabel;
    ppImage3: TppImage;
    ppLine25: TppLine;
    ppLabel72: TppLabel;
    ppLabel73: TppLabel;
    ppLabel74: TppLabel;
    ppLabel75: TppLabel;
    ppLabel76: TppLabel;
    ppLine26: TppLine;
    ppDBText52: TppDBText;
    ppDBText53: TppDBText;
    ppDBText54: TppDBText;
    ppDBText55: TppDBText;
    ppDBText56: TppDBText;
    ppShape41: TppShape;
    ppLabel80: TppLabel;
    ppShape40: TppShape;
    ppDBCalc8: TppDBCalc;
    ppShape9: TppShape;
    ppShape8: TppShape;
    lblTransitosNoFacturadosTotal: TppLabel;
    dbcTransitosnoFacturadosTotal: TppDBCalc;
    ppParameterList1: TppParameterList;
    ppSystemVariable1: TppSystemVariable;
    ppApellido: TppLabel;
    ppNombre: TppLabel;
    ppConvenio: TppLabel;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    procedure rptTransitosNoFacturadosBeforePrint(Sender: TObject);
  private
    { Private declarations }
    FCodigoConvenio: integer;
    FApellido: string;
    FNombre: string;
    FConvenio: string;
  public
    { Public declarations }
    function Inicializar(Recorset: TClientDataSet):boolean;
  end;

var
  RptTransitosNoFacturadosForm: TRptTransitosNoFacturadosForm;

implementation

uses DMConnection;

{$R *.dfm}

function TRptTransitosNoFacturadosForm.Inicializar(Recorset: TClientDataSet): boolean;
var                                                                                                     //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                                                 //SS_1147_NDR_20140710
begin

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage3.Picture.LoadFromFile(Trim(RutaLogo));                                                 //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    Result := False;
    // Revision 2
    FCodigoConvenio := Recorset.FieldByName('cdsConveniosEnCarpetaCodigoConvenio').AsInteger;
    FApellido  := Recorset.FieldByName('cdsConveniosEnCarpetaApellido').AsString;
    FNombre    := Recorset.FieldByName('cdsConveniosEnCarpetaNombre').AsString;
    FConvenio  := Recorset.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString;

    spObtenerTransitosNoFacturadosDeConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
    spObtenerTransitosNoFacturadosDeConvenio.Open;

    if spObtenerTransitosNoFacturadosDeConvenio.IsEmpty then begin
        Result := False;
        Exit;
    end;

    if not rbiTransitosNoFacturados.Execute(True) then Exit;

end;

procedure TRptTransitosNoFacturadosForm.rptTransitosNoFacturadosBeforePrint(
  Sender: TObject);
begin
    // Revision 2
    ppApellido.Text     := FApellido;
    ppNombre.Text       := FNombre;
    ppConvenio.Text     := FConvenio;
end;

end.
