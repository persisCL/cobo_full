unit Facturaciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util, Validate, DateEdit, ListBoxEx, DBListEx,
  ResultadoFacturacion, DmiCtrls, VariantComboBox, DMConnection;

type
  TfrmFacturaciones = class(TForm)
    lstProcesos: TDBListEx;
    lbl1: TLabel;
    lbl2: TLabel;
    btnFiltrar: TButton;
    btnSalir: TButton;
    btnConsultar: TButton;
    spProcesosFacturacion_SELECT: TADOStoredProc;
    dsProcesos: TDataSource;
    dateDesde: TDateEdit;
    dateHasta: TDateEdit;
    vcbGrupoFacturacion: TVariantComboBox;
    lbl3: TLabel;
    lbl4: TLabel;
    txtAno: TNumericEdit;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFiltrarClick(Sender: TObject);
    procedure CargarProcesosFacturacion;
    procedure btnConsultarClick(Sender: TObject);
    function Inicializar: Boolean;
    procedure lstProcesosDblClick(Sender: TObject);
    procedure ConsultarFacturacion;
    procedure vcbGrupoFacturacionChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFacturaciones: TfrmFacturaciones;

implementation

{$R *.dfm}

function TfrmFacturaciones.Inicializar: Boolean;
var
    spObtenerGruposFacturacion: TADOStoredProc;
begin
    Result := True;

    txtAno.Value := Year(Now);

    vcbGrupoFacturacion.Clear;
    spObtenerGruposFacturacion := TADOStoredProc.Create(Self);
    try
        spObtenerGruposFacturacion.Connection := DMConnections.BaseCAC;
        spObtenerGruposFacturacion.ProcedureName := 'ObtenerGruposFacturacion';
        spObtenerGruposFacturacion.CommandTimeout := 30;
        spObtenerGruposFacturacion.Open;

        while not spObtenerGruposFacturacion.Eof do begin
            vcbGrupoFacturacion.Items.Add(spObtenerGruposFacturacion.FieldByName('Descripcion').AsString,
                                            spObtenerGruposFacturacion.FieldByName('CodigoGrupoFacturacion').AsInteger);

            spObtenerGruposFacturacion.Next;
        end;
    finally
        spObtenerGruposFacturacion.Free;
    end;
    vcbGrupoFacturacion.Items.Add('INFRACTORES', 0);                            // TASK_142_MGO_20170220
    vcbGrupoFacturacion.ItemIndex := 0;

    try
        CargarProcesosFacturacion;
    except
        Result := False;
    end;
end;

procedure TfrmFacturaciones.lstProcesosDblClick(Sender: TObject);
begin
    ConsultarFacturacion;
end;

procedure TfrmFacturaciones.vcbGrupoFacturacionChange(Sender: TObject);
begin
    CargarProcesosFacturacion;
end;

procedure TfrmFacturaciones.ConsultarFacturacion;
var
    FormResultadoFacturacion : TFormResultadoFacturacion;
begin
    if not spProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsInteger > 0 then
        Exit;

    Application.CreateForm(TFormResultadoFacturacion, FormResultadoFacturacion);
    if FormResultadoFacturacion.Inicializar(
            spProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsInteger) then begin
        FormResultadoFacturacion.Show;
    end else
        FormResultadoFacturacion.Release;
end;

procedure TfrmFacturaciones.btnConsultarClick(Sender: TObject);
begin
    ConsultarFacturacion;
end;

procedure TfrmFacturaciones.btnFiltrarClick(Sender: TObject);
begin
    CargarProcesosFacturacion;
end;

procedure TfrmFacturaciones.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmFacturaciones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmFacturaciones.CargarProcesosFacturacion;
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_SELECT = 'Error al obtener procesos de Facturación';
begin
    if (dateDesde.Date > dateHasta.Date) and (not dateDesde.IsEmpty) and (not dateHasta.IsEmpty) then begin
        MsgBox('La fecha desde no puede ser mayor a la fecha hasta','Error',MB_ICONSTOP);
        Exit;
    end;

    try
        spProcesosFacturacion_SELECT.Close;
        spProcesosFacturacion_SELECT.Parameters.Refresh;
        if not dateDesde.IsEmpty then
            spProcesosFacturacion_SELECT.Parameters.ParamByName('@FechaDesde').Value := dateDesde.Date;
        if not dateHasta.IsEmpty then
            spProcesosFacturacion_SELECT.Parameters.ParamByName('@FechaHasta').Value := dateHasta.Date;
        if vcbGrupoFacturacion.Value > -1 then
            spProcesosFacturacion_SELECT.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := vcbGrupoFacturacion.Value;
        if txtAno.ValueInt > 0 then
            spProcesosFacturacion_SELECT.Parameters.ParamByName('@Ano').Value := txtAno.ValueInt;

        spProcesosFacturacion_SELECT.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_SELECT, e.Message, MSG_ERROR, MB_ICONSTOP);
        end;
    end;
end;

end.
