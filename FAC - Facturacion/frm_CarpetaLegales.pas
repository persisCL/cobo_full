{********************************** Unit Header ********************************
File Name : frm_CarpetaLegales.pas
Author : jconcheyro
Date Created: 18/10/2006
Language : ES-AR
Description :

Revision 1
Author: nefernandez
Date: 02/03/2007
Language: ES-AR
Description : No mostrar decimales en la columna Deuda.

Revision 2
Author: nefernandez
Date: 07/03/2007
Language: ES-AR
Description : Reemplazar el procedimiento de Imprimir Comprobantes en Carpeta
Legal, para que en vez de llamar a un reporte "global", llame a reportes
individuales (Caratula, Factura, FacturaDetallada, TransitosNoFacturados) por
cada Convenio a imprimir. Tambi�n se creo un form para presentar la opci�n de
incluir o no el detalle de tr�nsitos (FacturaDetallada) en este proceso.

Revision 3
Author: nefernandez
Date: 21/03/2007
Description : Se realizaron los siguientes cambios, de acuerdo a SS-146
- Mostrar el mensaje "Proceso Finalizado" solo si se enviaron "Convenios a Ingresar"
  a "Convenios en Carpeta"
- Mostrar mensajes para indicar si se est� queriendo reenviar convenios que ya est�n
  en carpeta legal y si se est� queriendo dar salida a convenios que ya fueron dados
  de baja
- Eliminar todos los convenios seleccionados. Anteriormente solo se eliminaba el
  convenio en el cual se estaba posicionado.
- Mostrar los datos del cliente en el reporte de Transitos no Facturados

Revision 4
Author: nefernandez
Date: 22/03/2007
Description: Al ejecutar el Reporte de Factura se puede indicar, que por defecto las opciones
de impresi�n, aparezcan checkeadas.

Revision 5
Author: mpiazza
Date: 03/02/2009
Description: SS-146 en envio legales se agrega el llamado al formulario para
seleccionar tipo de deuda y empresa recaudadora, actualizacion de los datos en
la base de datos y actualizacion de los datos seleccionados en la grilla

Revision 6
Author: mpiazza
Date: 03/03/2009
Description: SS-146 en envio legales se cambia el tipo de dato de dos campos y
              se modifica la funcion de ordenamiento


Revision 7
Author: Nelson Droguett Sierra
Date: 27/04/2009
Description: Se incluye la reimpresion de documentos electronicos

Firma       : SS_1047_CQU_20120612
Description : Se quita el filtro que se hace al ingresar al formulario

Firma       : SS_1100_NDR_20130417
Description : Permiso al boton "Pasar a Carpetas Legales"

Firma       : SS_660_CQU_20130822
Descripcion : Se pinta la celda que contiene el NumeroConvenio cuando est� en lista amarilla
              Se muestra un balloon cuando se quiera seleccionar un convenio el lista amarilla
              Se agerga uso de SysUtilsCN y se llama a su funci�n DBListExMostrarMensajeBalloon
              Se agrega en los ClientDataSet los campos para saber si est� en ListaAmarilla

Firma       : SS_660_CQU_20131112
Descripcion : Se modifica la correcci�n anterior para que la validaci�n s�lo la haga al momento
              de presionar bot�n y no al momento de seleccionar el convenio.
			  Se modifican los mensajes que se despliegan cuando un convenio est� en lista amarilla.


Firma       : SS_1399_NDR_20151005
Descripcion : Cuando Imprime los comprobantes no debe imprimir los 'SD' ni 'SA'

Firma       : SS_1399_NDR_20151026
Descripcion : Imprimir un reporte con los compropbantes SD impagos
*******************************************************************************}
unit frm_CarpetaLegales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, DmiCtrls, VariantComboBox, ListBoxEx,
  DBListEx, DbList, DBClient, DB, ADODB, Menus, DMConnection, UtilProc, Util,
  UtilDb, PeaProcs, CollPnl, ImgList, Validate, DateEdit, BuscaClientes,
  ReporteFactura, frmReporteFacturacionDetallada, frmReporteBoletaFacturaElectronica,
  frmRptCarpetasMorososCaratula, rptTransitosNoFacturadosfrm,
  frmRptCarpetasMorososcomprobantesSD,                                                                          //SS_1399_NDR_20151026
  SysUtilsCN,   // SS_660_CQU_20130822
  ConstParametrosGenerales, ImprimirCarpetaLegalForm, frmSeleccionTipoDeudaEmpresa, PeaTypes,ppPrintr, ppTypes, //SS_1399_NDR_20151026
  ppCtrls, ppDB, ppDBPipe, UtilRB, ppParameter, ppBands, ppClass, ppVar,                                        //SS_1399_NDR_20151026
  ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport;                                                        //SS_1399_NDR_20151026

type
  TFormCarpetaLegales = class(TForm)
    pnlPie: TPanel;
    pnlCentral: TPanel;
    pgConvenios: TPageControl;
    tsConveniosEnCarpeta: TTabSheet;
    tsConveniosAIngresar: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    dblConveniosAIngresar: TDBListEx;
    spConveniosEnCarpetaLegal: TADOStoredProc;
    spConveniosAIngresar: TADOStoredProc;
    dsConveniosEnCarpeta: TDataSource;
    cdsConveniosEnCarpeta: TClientDataSet;
    dsConveniosAIngresar: TDataSource;
    cdsConveniosAIngresar: TClientDataSet;
    nbBotones: TNotebook;
    btnImprimir: TButton;
    btnEnvioLegales: TButton;
    btnSalidaLegales: TButton;
    btnPasarACarpetasLegales: TButton;
    dblConveniosEnCarpeta: TDBListEx;
    mnuSeleccion: TPopupMenu;
    mnuSeleccionar: TMenuItem;
    spActualizarConvenio: TADOStoredProc;
    CollapsablePanel1: TCollapsablePanel;
    Label1: TLabel;
    peRUTCliente: TPickEdit;
    Label3: TLabel;
    neDiasDeuda: TNumericEdit;
    Label5: TLabel;
    neDeudaMinima: TNumericEdit;
    Label6: TLabel;
    neDeudaMaxima: TNumericEdit;
    Label4: TLabel;
    neLineas: TNumericEdit;
    btnLimpiarFiltro: TButton;
    btnFiltrar: TButton;
    spConvenioExisteEnCarpeta: TADOStoredProc;
    btnSalir: TButton;
    Img_Tilde: TImageList;
    mnuSeleccionarTodos: TMenuItem;
    mnuDeseleccionarTodos: TMenuItem;
    btnEliminarLegales: TButton;
    spEliminarConvenio: TADOStoredProc;
    lblEstadoProceso: TLabel;
    spObtenerComprobantes: TADOStoredProc;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaIdConvenioCarpetaLegal: TIntegerField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaCodigoConvenio: TIntegerField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaApellido: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaApellidoMaterno: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaNombre: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraImpresion: TDateTimeField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioImpresion: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraEnvioALegales: TDateTimeField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioEnvioLegales: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraSalidaLegales: TDateTimeField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraCreacion: TDateTimeField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioCreacion: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraActualizacion: TDateTimeField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioActualizacion: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaNumeroDocumento: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaNumeroConvenio: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaSeleccionado: TBooleanField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaApellidoMostrar: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaDias: TIntegerField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaDeudaStr: TCurrencyField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioSalidaLegales: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaDiasDeudaTotal: TIntegerField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaImporteDeudaActualizada: TCurrencyField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaAliasEmpresaRecaudadora: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaDescripcionTipoDeuda: TStringField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaCodigoEmpresasRecaudadoras: TIntegerField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaCodigoTipoDeuda: TIntegerField;
    cdsConveniosEnCarpetacdsConveniosEnCarpetaDescripcionConcesionaria: TStringField;	// SS_1036_PDO_20120418
    cdsConveniosEnCarpetacdsConveniosEnCarpetaEnListaAmarilla: TBooleanField;	        // SS_660_CQU_20130822
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnLimpiarFiltroClick(Sender: TObject);
    procedure mnuSeleccionarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblConveniosEnCarpetaDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblConveniosEnCarpetaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dblConveniosEnCarpetaDblClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnEnvioLegalesClick(Sender: TObject);
    procedure pgConveniosChange(Sender: TObject);
    procedure btnSalidaLegalesClick(Sender: TObject);
    procedure ActualizarConvenios( Operacion: string);
    procedure btnPasarACarpetasLegalesClick(Sender: TObject);
    procedure dblConveniosAIngresarDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblConveniosAIngresarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dblConveniosAIngresarDblClick(Sender: TObject);
    procedure mnuSeleccionarTodosClick(Sender: TObject);
    procedure mnuDeseleccionarClick(Sender: TObject);
    procedure mnuDeseleccionarTodosClick(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure btnEliminarLegalesClick(Sender: TObject);
    procedure OrdenarColumna(Sender: TObject);
  private
    { Private declarations }
  	FUltimaBusqueda : TBusquedaCliente;
    FImprimirDetalleTransito: Integer;
    fEmpresasRecaudadoras       : integer;
    fTipoDeuda                  : integer;
    fAliasEmpresaRecaudadora    : string;
    fDescripcionTipoDeuda       : string;
    procedure OrdenarGridPorColumnaCustom(Grilla: TCustomDBListEx; Columna: TDBListExColumn; NombreCampo:string; NoCambiarOrden: boolean=False);
    procedure FiltrarConveniosAIngresar;
    procedure FiltrarConveniosEnCarpeta;
    procedure LiberarMemoria;       // SS_1017_PDO_20120514
  public
    { Public declarations }
    function Inicializar(const Titulo: string): boolean;
  end;

resourcestring																											// SS_660_CQU_20130822
    //MSG_CONVENIO_EN_LA = 'No se puede seleccionar el convenio %s '#13#10'debido a que se encuentra en Lista Amarilla';                // SS_660_CQU_20131112	// SS_660_CQU_20130822
    MSG_CONVENIO_EN_LA = 'El convenio %s no se puede enviar a legales '#13#10'debido a que se encuentra en Lista Amarilla';             // SS_660_CQU_20131112
    MSG_CONVENIOS_EN_LA= 'Los siguientes convenios no se pueden enviar a legales '#13#10'debido a que se encuentran en Lista Amarilla'; // SS_660_CQU_20131112

var
  FormCarpetaLegales: TFormCarpetaLegales;

implementation

{$R *.dfm}

function TFormCarpetaLegales.Inicializar(const Titulo: string): boolean;
resourcestring
    MSG_ERROR_SPID = 'Se ha producido un error determinando el Id de sesi�n de base de datos';
Var
	S: TSize;
begin
    CenterForm(self);

	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);


    btnEnvioLegales.Enabled := ExisteAcceso('BTN_ENVIO_CARPETA_LEGALES');
    btnImprimir.Enabled := ExisteAcceso('BTN_IMPRIMIR_CARPETAS_LEGALES');
    btnSalidaLegales.Enabled := ExisteAcceso('BTN_SALIDA_CARPETA_LEGALES');
    btnEliminarLegales.Enabled := ExisteAcceso('BTN_ELIMINAR_CARPETA_LEGALES');
    btnPasarACarpetasLegales.Enabled := ExisteAcceso('BTN_PASAR_A_CARPETA_LEGALES');    //SS_1100_NDR_20130417

    pgConvenios.ActivePage := tsConveniosEnCarpeta;
    nbBotones.ActivePage := 'pgEnviosSalidas';
    Result := True ;

    //spConveniosAIngresar.Execproc ;
    //btnFiltrarClick(self);    // SS_1047_CQU_20120612
    btnLimpiarFiltroClick(self);
    peRutCliente.SetFocus;
end;




{-----------------------------------------------------------------------------
  Function Name: FiltrarConveniosEnCarpeta
  Author:
  Date Created:  /  /
  Description:
  Parameters: N/A
  Return Value: N/A

  Revision : 1
    Date: 02/03/2009
    Author: mpiazza
    Description:  Ref (ss-146) Se cambiaron tipo de datos de dos campos
                  para el ordenamiento
-----------------------------------------------------------------------------}
procedure TFormCarpetaLegales.FiltrarConveniosEnCarpeta;
resourcestring
    ERROR_OBT_CONVENIOS         = 'Error obteniendo convenios';
var
    NombreAMostrar : string;
begin
    Screen.Cursor := crHourGlass;
    try
        try
            cdsConveniosEnCarpeta.EmptydataSet;
            cdsConveniosEnCarpeta.DisableControls;
            spConveniosEnCarpetaLegal.Close;
            spConveniosEnCarpetaLegal.CommandTimeout := 1000;
            spConveniosEnCarpetaLegal.Parameters.ParamByName('@RUT').Value               := iif(peRUTCLiente.Text <> EmptyStr,PadL(Trim(peRUTCLiente.Text), 9, '0'), EmptyStr);
            spConveniosEnCarpetaLegal.Parameters.ParamByName('@DiasDeuda').Value         := neDiasDeuda.ValueInt;
            spConveniosEnCarpetaLegal.Parameters.ParamByName('@DeudaEnPesosDesde').Value := neDeudaMinima.ValueInt * 100;
            spConveniosEnCarpetaLegal.Parameters.ParamByName('@DeudaEnPesosHasta').Value := neDeudaMaxima.ValueInt * 100;
            spConveniosEnCarpetaLegal.Parameters.ParamByName('@CantidadLineas').Value    := neLineas.ValueInt;
            spConveniosEnCarpetaLegal.Open;
            while not spConveniosEnCarpetaLegal.eof do begin
                cdsConveniosEnCarpeta.Append;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaIdConvenioCarpetaLegal').AsInteger  := spConveniosEnCarpetaLegal.FieldByName('IdConvenioCarpetaLegal').AsInteger ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoConvenio'        ).AsInteger  := spConveniosEnCarpetaLegal.FieldByName('CodigoConvenio'        ).AsInteger ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaApellido').AsString         := spConveniosEnCarpetaLegal.FieldByName('Apellido').AsString;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaApellidoMaterno').AsString  := spConveniosEnCarpetaLegal.FieldByName('ApellidoMaterno').AsString;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNombre').AsString           := spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString;

                NombreAMostrar := '';
                NombreAMostrar :=   trim(spConveniosEnCarpetaLegal.FieldByName('Apellido').AsString) + ' ' + trim(spConveniosEnCarpetaLegal.FieldByName('ApellidoMaterno').AsString);

                if trim(spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString) <> '' then NombreAMostrar := NombreAMostrar + ', ' + trim(spConveniosEnCarpetaLegal.FieldByName('Nombre').AsString);

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaApellidoMostrar').AsString := NombreAMostrar;

                if spConveniosEnCarpetaLegal.FieldByName('FechaHoraImpresion'    ).AsDateTime > 0 then
                    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraImpresion'    ).AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraImpresion'    ).AsDateTime ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioImpresion'      ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioImpresion'      ).AsString ;
                if spConveniosEnCarpetaLegal.FieldByName('FechaHoraEnvioLegales' ).AsDateTime > 0 then
                    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraEnvioALegales').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraEnvioLegales' ).AsDateTime ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioEnvioLegales'   ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioEnvioLegales'   ).AsString ;

                if spConveniosEnCarpetaLegal.FieldByName('FechaHoraSalidaLegales').AsDateTime > 0 then
                    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraSalidaLegales').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraSalidaLegales').AsDateTime ;


                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioSalidaLegales'  ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioSalidaLegales'  ).AsString ;


                if spConveniosEnCarpetaLegal.FieldByName('FechaHoraCreacion'     ).AsDateTime > 0 then
                    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraCreacion'     ).AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraCreacion'     ).AsDateTime ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioCreacion'       ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioCreacion'       ).AsString ;

                if spConveniosEnCarpetaLegal.FieldByName('FechaHoraActualizacion').AsDateTime > 0 then
                    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraActualizacion').AsDateTime := spConveniosEnCarpetaLegal.FieldByName('FechaHoraActualizacion').AsDateTime ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioActualizacion'  ).AsString   := spConveniosEnCarpetaLegal.FieldByName('UsuarioActualizacion'  ).AsString ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroDocumento'       ).AsString   := spConveniosEnCarpetaLegal.FieldByName('NumeroDocumento'       ).AsString ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio'        ).AsString   := spConveniosEnCarpetaLegal.FieldByName('NumeroConvenio'        ).AsString ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado'          ).AsBoolean  := False;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaDias'                  ).AsInteger  := spConveniosEnCarpetaLegal.FieldByName('DiasDeuda'             ).AsInteger ;
                // Revision 1
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaDeudaStr'              ).AsCurrency   := spConveniosEnCarpetaLegal.FieldByName('ImporteDeuda' ).AsFloat;

                // Revision 5
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaDiasDeudaTotal').AsInteger  := spConveniosEnCarpetaLegal.FieldByName('DiasDeudaTotal').AsInteger ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaImporteDeudaActualizada').AsCurrency   := spConveniosEnCarpetaLegal.FieldByName('ImporteDeudaActualizada').AsFloat;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaAliasEmpresaRecaudadora').AsString   := spConveniosEnCarpetaLegal.FieldByName('AliasEmpresaRecaudadora').AsString ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaDescripcionTipoDeuda').AsString   := spConveniosEnCarpetaLegal.FieldByName('DescripcionTipoDeuda').AsString ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoEmpresasRecaudadoras').AsInteger   := spConveniosEnCarpetaLegal.FieldByName('CodigoEmpresasRecaudadoras').AsInteger ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoTipoDeuda').AsInteger   := spConveniosEnCarpetaLegal.FieldByName('CodigoTipoDeuda').AsInteger ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaDescripcionConcesionaria').AsString := spConveniosEnCarpetaLegal.FieldByName('DescripcionConcesionaria').AsString;   // SS_1036_PDO_20120418

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaEnListaAmarilla').AsBoolean  := spConveniosEnCarpetaLegal.FieldByName('EnListaAmarilla').AsBoolean;                  // SS_660_CQU_20130822

                spConveniosEnCarpetaLegal.Next;
            end;
            cdsConveniosEnCarpeta.First;
            cdsConveniosEnCarpeta.EnableControls;
        except
            on E: Exception do begin
                      screen.Cursor := crDefault;
                  MsgBoxErr(ERROR_OBT_CONVENIOS, e.message, caption, MB_ICONSTOP);
            end;
        end;
    finally
        dblConveniosEnCarpeta.Invalidate;
        Screen.Cursor := crDefault;
    end;

end;


procedure TFormCarpetaLegales.FiltrarConveniosAIngresar;
resourcestring
    ERROR_OBT_CONVENIOS         = 'Error obteniendo convenios';
    MSG_CONVENIOS_NO_ENCONTRADOS= 'No se han encontrado convenios con este criterio de b�squeda';
    MSG_TITULO                  = 'Carpeta Legal';
var
    CantidadRegistros: integer;
begin
    Screen.Cursor := crHourGlass;
    try
        try
            cdsConveniosAIngresar.EmptyDataSet;
            cdsConveniosAIngresar.DisableControls;
            spConveniosAIngresar.Close;
            spConveniosAIngresar.Parameters.ParamByName('@RUT').Value               := iif(peRUTCLiente.Text <> EmptyStr,PadL(Trim(peRUTCLiente.Text), 9, '0'), EmptyStr);
            spConveniosAIngresar.Parameters.ParamByName('@DiasDeuda').Value         := neDiasDeuda.ValueInt;
            spConveniosAIngresar.Parameters.ParamByName('@DeudaEnPesosDesde').Value := neDeudaMinima.ValueInt * 100;
            spConveniosAIngresar.Parameters.ParamByName('@DeudaEnPesosHasta').Value := neDeudaMaxima.ValueInt * 100;
            spConveniosAIngresar.Parameters.ParamByName('@CantidadLineas').Value    := neLineas.ValueInt;
            lblEstadoProceso.Caption := 'Abriendo consulta en la base';
            Application.ProcessMessages;
            CantidadRegistros := 0 ;
            spConveniosAIngresar.Open;

            if spConveniosAIngresar.Eof then MsgBox(MSG_CONVENIOS_NO_ENCONTRADOS, MSG_TITULO, MB_ICONINFORMATION);
            while not spConveniosAIngresar.Eof  do begin
                cdsConveniosAIngresar.Append;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado'  ).AsBoolean   := False;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngCodigoConvenio').AsInteger   := spConveniosAIngresar.FieldByName('CodigoConvenio').AsInteger;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroConvenio').AsString    := spConveniosAIngresar.FieldByName('NumeroConvenio').AsString;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngApellido'       ).AsString   := spConveniosAIngresar.FieldByName('Apellido'       ).AsString ;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngApellidoMaterno').AsString   := spConveniosAIngresar.FieldByName('ApellidoMaterno').AsString ;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNombre'         ).AsString   := spConveniosAIngresar.FieldByName('Nombre'         ).AsString ;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroDoc').AsString   := spConveniosAIngresar.FieldByName('NumeroDocumento').AsString ;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngDias'           ).AsInteger  := spConveniosAIngresar.FieldByName('Dias'           ).AsInteger;
                // Revision 1
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngDeudaStr'       ).AsString   := FloatToStrF(spConveniosAIngresar.FieldByName('Deuda').AsFloat/100, ffCurrency,20,0) ;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngDeuda'          ).AsFloat    := spConveniosAIngresar.FieldByName('Deuda'          ).AsFloat ;
                cdsConveniosAIngresar.FieldByName('cdsConveniosAIngEnListaAmarilla').AsBoolean  := spConveniosAIngresar.FieldByName('EnListaAmarilla').AsBoolean;                           // SS_660_CQU_20130822
                spConveniosAIngresar.Next;
                Inc(CantidadRegistros);
                if ((CantidadRegistros mod 100 ) = 0) then begin
                    lblEstadoProceso.Caption := 'Registros Cargados : ' + IntToStr(CantidadRegistros);
                    Application.ProcessMessages;
                end;

            end;
            cdsConveniosAIngresar.First;
            cdsConveniosAIngresar.EnableControls;

        except
            on E: Exception do begin
                      screen.Cursor := crDefault;
                  MsgBoxErr(ERROR_OBT_CONVENIOS, e.message, caption, MB_ICONSTOP);
            end;
        end;
    finally
        lblEstadoProceso.Caption := '';
        dblConveniosAIngresar.Invalidate;
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnEnvioLegalesClick
  Author: ???????
  Date Created: ??/??/????
  Description: cargo una cvariantcombobox con los tipos de deuda
  Parameters: Genera el envio a legales

    Revision 1
    Author: mpiazza
    Date: 03/02/2009
    Description: SS-146 en envio legales se agrega el llamado al formulario para
    seleccionar tipo de deuda y empresa recaudadora.
-----------------------------------------------------------------------------}

procedure TFormCarpetaLegales.btnEnvioLegalesClick(Sender: TObject);
resourcestring
    MSG_ENVIAR_CARPETAS   = 'Se han seleccionado %d convenios para env�o a legales con fecha %s.';
    CAPTION_ENVIAR        = 'Env�o de Carpetas Morosos a Legales.';
    MSG_NO_HAY_CONVENIOS_SELECCIONADOS = 'No hay convenios seleccionados para env�o a legales';
    MSG_FINALIZADO        = 'Proceso finalizado';
    MSG_CONVENIOS_DESMARCADOS        = 'Los siguientes convenios seleccionados ya se encuentran enviados a legales (no pueden ser enviados nuevamente):';
var
    CantidadRegistrosSeleccionados, CantidadRegistrosDesmarcados: integer;
    ConveniosDesmarcados: String;
    fSeleccionTipoDeudaEmpresa: TSeleccionTipoDeudaEmpresaForm;
    ConvenioSeleccionadoEnListaAmarilla : Boolean;          // SS_660_CQU_20131112
    ConveniosDesmarcadosEnListaAmarilla: String;            // SS_660_CQU_20131112
    CantidadRegistrosDesmarcadosEnListaAmarilla: Integer;   // SS_660_CQU_20131112
begin
    cdsConveniosEnCarpeta.First;
    cdsConveniosEnCarpeta.DisableControls;
    CantidadRegistrosSeleccionados := 0;
    CantidadRegistrosDesmarcados := 0;
    ConveniosDesmarcados := '';
    ConvenioSeleccionadoEnListaAmarilla := False;			// SS_660_CQU_20131112
    CantidadRegistrosDesmarcadosEnListaAmarilla := 0;       // SS_660_CQU_20131112
    ConveniosDesmarcadosEnListaAmarilla := '';              // SS_660_CQU_20131112
          //desmarco como seleccionados los que tengan envio o salida previamente seteado
            while not cdsConveniosEnCarpeta.eof do begin
                if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean then
                    if (cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraEnvioALegales').AsDateTime = 0) and
                       (cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraSalidaLegales').AsDateTime = 0) then
                        CantidadRegistrosSeleccionados := CantidadRegistrosSeleccionados + 1
                    else begin
                        cdsConveniosEnCarpeta.Edit;
                        cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean := False;
                        cdsConveniosEnCarpeta.Post;
                        CantidadRegistrosDesmarcados := CantidadRegistrosDesmarcados + 1;
                        //ConveniosDesmarcados := ConveniosDesmarcados + cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString + CHR(13);                                   // SS_660_CQU_20131112
                        ConveniosDesmarcados := ConveniosDesmarcados + FormatearNumeroConvenio(cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString) + CHR(13);            // SS_660_CQU_20131112
                    end;
                if  cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean and                                                                                                // SS_660_CQU_20131112
                    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaEnListaAmarilla').AsBoolean                                                                                                 // SS_660_CQU_20131112
                then begin                                                                                                                                                                              // SS_660_CQU_20131112
                    cdsConveniosEnCarpeta.Edit;                                                                                                                                                         // SS_660_CQU_20131112
                    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean := False;                                                                                          // SS_660_CQU_20131112
                    cdsConveniosEnCarpeta.Post;                                                                                                                                                         // SS_660_CQU_20131112
                    ConvenioSeleccionadoEnListaAmarilla := True;                                                                                                                                        // SS_660_CQU_20131112
                    ConveniosDesmarcadosEnListaAmarilla := ConveniosDesmarcados + FormatearNumeroConvenio(cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString) + CHR(13); // SS_660_CQU_20131112
                    CantidadRegistrosDesmarcadosEnListaAmarilla := CantidadRegistrosDesmarcadosEnListaAmarilla + 1;                                                                                     // SS_660_CQU_20131112
                    CantidadRegistrosSeleccionados := CantidadRegistrosSeleccionados -1;                                                                                                                // SS_660_CQU_20131112
                end;                                                                                                                                                                                    // SS_660_CQU_20131112

                cdsConveniosEnCarpeta.Next;
            end;

            if CantidadRegistrosSeleccionados = 0  then begin
                if CantidadRegistrosDesmarcados = 0 then
                    // Solo se muestra este mensaje si no se seleccion� ning�n registro
                    MsgBox(MSG_NO_HAY_CONVENIOS_SELECCIONADOS, Caption, 0)
                else
                    // Se muestra este mensaje si todos los registros seleccionados ya estan en carpeta legal
                    MsgBox(MSG_CONVENIOS_DESMARCADOS + CHR(13) + ConveniosDesmarcados, Caption, 0);

                cdsConveniosEnCarpeta.First;
                cdsConveniosEnCarpeta.EnableControls;
                Exit;
            end;

            // Se muestra este mensaje si existen algunos registros seleccionados que ya estan en carpeta legal
            if CantidadRegistrosDesmarcados > 0 then
                MsgBox(MSG_CONVENIOS_DESMARCADOS + CHR(13) + ConveniosDesmarcados, Caption, 0);

            if ConvenioSeleccionadoEnListaAmarilla then begin                                                   // SS_660_CQU_20131112
                if CantidadRegistrosDesmarcadosEnListaAmarilla > 1 then begin                                   // SS_660_CQU_20131112
                    MsgBox(MSG_CONVENIOS_EN_LA + CHR(13) + ConveniosDesmarcadosEnListaAmarilla, Caption, 0);    // SS_660_CQU_20131112
                    Exit;                                                                                       // SS_660_CQU_20131112
                end else begin                                                                                  // SS_660_CQU_20131112
                    MsgBox(Format(MSG_CONVENIO_EN_LA, [ConveniosDesmarcadosEnListaAmarilla])                    // SS_660_CQU_20131112
                        , Caption, MB_ICONINFORMATION);                                                         // SS_660_CQU_20131112
                    Exit;                                                                                       // SS_660_CQU_20131112
                end;                                                                                            // SS_660_CQU_20131112
            end;                                                                                                // SS_660_CQU_20131112


            try
                if MsgBox(Format(MSG_ENVIAR_CARPETAS, [CantidadRegistrosSeleccionados, DateTimeToStr(NowBase(spActualizarConvenio.Connection)) ]), CAPTION_ENVIAR, MB_OKCANCEL) = mrOk then begin
                 fSeleccionTipoDeudaEmpresa := TSeleccionTipoDeudaEmpresaForm.Create(Application);
                  if fSeleccionTipoDeudaEmpresa.Inicializar() then begin
                      if fSeleccionTipoDeudaEmpresa.ShowModal = mrOK then begin
                          fEmpresasRecaudadoras := fSeleccionTipoDeudaEmpresa.cbEmpresaRecaudadora.Value;
                          fTipoDeuda := fSeleccionTipoDeudaEmpresa.cbTipoDeuda.Value;
                          fAliasEmpresaRecaudadora    := fSeleccionTipoDeudaEmpresa.cbEmpresaRecaudadora.Text;
                          fDescripcionTipoDeuda       := fSeleccionTipoDeudaEmpresa.cbTipoDeuda.Text;
                          ActualizarConvenios('Envio');
                          MsgBox(MSG_FINALIZADO , Caption, 0);
                      end;
                  end;
                end;
            finally
                cdsConveniosEnCarpeta.First;
                cdsConveniosEnCarpeta.EnableControls;
            end;
end;



procedure TFormCarpetaLegales.btnFiltrarClick(Sender: TObject);
resourcestring                                                                                          // SS_1047_CQU_20120612
    MSG_FILTRO_VACIO = 'No ha indicado filtros.' + CRLF                                                 // SS_1047_CQU_20120612
    + 'Esto puede tardar varios minutos' + CRLF                                                         // SS_1047_CQU_20120612
    + '�Est� seguro de ejecutar esta b�squeda?';                                                        // SS_1047_CQU_20120612
var                                                                                                     // SS_1047_CQU_20120612
    Continuar : Boolean;                                                                                // SS_1047_CQU_20120612
begin
    //if pgConvenios.ActivePage = tsConveniosEnCarpeta then FiltrarConveniosEnCarpeta                   // SS_1047_CQU_20120612
    //else FiltrarConveniosAIngresar;                                                                   // SS_1047_CQU_20120612

    Continuar := True;                                                                                  // SS_1047_CQU_20120612
    // Valido que exista almenos un dato en el filtro si no existe, consulto.                           // SS_1047_CQU_20120612
    if (peRutCliente.Text = '') and (neLineas.ValueInt = 0) and (neDeudaMinima.ValueInt = 0)            // SS_1047_CQU_20120612
        and (neDeudaMaxima.ValueInt = 0) and (neDiasDeuda.ValueInt = 0)                                 // SS_1047_CQU_20120612
    then Continuar := (MsgBox(MSG_FILTRO_VACIO, 'Atenci�n', MB_YESNO + MB_ICONQUESTION) = ID_YES);      // SS_1047_CQU_20120612

    if Continuar then begin                                                                             // SS_1047_CQU_20120612
        if pgConvenios.ActivePage = tsConveniosEnCarpeta then FiltrarConveniosEnCarpeta                 // SS_1047_CQU_20120612
        else FiltrarConveniosAIngresar;                                                                 // SS_1047_CQU_20120612
    end else Exit;                                                                                      // SS_1047_CQU_20120612
end;

procedure TFormCarpetaLegales.btnImprimirClick(Sender: TObject);
resourcestring
    MSG_IMPRIMIR_CARPETAS   = 'Se han seleccionado %d convenios para impresi�n.';
    CAPTION_IMPRIMIR        = 'Impresi�n de Carpetas Morosos.';
    MSG_NO_HAY_CONVENIOS_SELECCIONADOS = 'No hay convenios seleccionados para imprimir';
var
    CantidadRegistrosSeleccionados: integer;
    // Revision 2
    RptCarpetasMorososCaratula: TfrmCarpetasMorososCaratula;
    RptFactura: TFrmReporteFactura;
    // Rev.7 / 27-04-2009 / Nelson Droguett Sierra
    RptBoletaFacturaElectronica : TReporteBoletaFacturaElectronicaForm;
    RptCarpetasMorososComprobantesSD : TfrmRptCarpetasMorososComprobantesSD;                        //SS_1399_NDR_20151026


    sError : string;
    ImFdo  : boolean;
    RptFacturacionDetallada: TFormReporteFacturacionDetallada;
    RptTransitosNoFacturados: TRptTransitosNoFacturadosForm;
    fImprimirCarpetaLegal: TfrmImprimirCarpetaLegal;
    bCrearReporteFE,bCrearReporteF,bCrearReporteD,bDebeSalirDialogo:Boolean;

begin
    cdsConveniosEnCarpeta.First;
    cdsConveniosEnCarpeta.DisableControls;
    CantidadRegistrosSeleccionados := 0;
    bDebeSalirDialogo:=True;
    while not cdsConveniosEnCarpeta.eof do begin
        if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean then
            CantidadRegistrosSeleccionados := CantidadRegistrosSeleccionados + 1 ;
        cdsConveniosEnCarpeta.Next;
    end;

    if CantidadRegistrosSeleccionados = 0  then begin
        MsgBox(MSG_NO_HAY_CONVENIOS_SELECCIONADOS, Caption, 0);
        cdsConveniosEnCarpeta.First;
        cdsConveniosEnCarpeta.EnableControls;
        Exit;
    end;

    try
        // Revision 2
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'IMPRIMIR_DETALLE_TRANSITO', FImprimirDetalleTransito);

        if not Assigned(fImprimirCarpetaLegal) then begin                               // SS_1017_PDO_20120514
            //Application.CreateForm(TfrmImprimirCarpetaLegal,fImprimirCarpetaLegal);   // SS_1017_PDO_20120514
            fImprimirCarpetaLegal :=  TfrmImprimirCarpetaLegal.Create(nil);             // SS_1017_PDO_20120514
        end;                                                                            // SS_1017_PDO_20120514



        if (fImprimirCarpetaLegal.Inicializar(Format(MSG_IMPRIMIR_CARPETAS, [CantidadRegistrosSeleccionados]), FImprimirDetalleTransito)) and
           (fImprimirCarpetaLegal.ShowModal = mrOk) then begin

            FImprimirDetalleTransito := fImprimirCarpetaLegal.FImprimirDetalleTransito;

            // Recorrer los convenios
            cdsConveniosEnCarpeta.First;

            while not cdsConveniosEnCarpeta.Eof do begin
                if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean = True then begin
                    try
                        // Imprimir la caratula
                        if not Assigned(RptCarpetasMorososCaratula) then begin                                  // SS_1017_PDO_20120514
                            //Application.CreateForm(TfrmCarpetasMorososCaratula,RptCarpetasMorososCaratula);   // SS_1017_PDO_20120514
                            RptCarpetasMorososCaratula := TfrmCarpetasMorososCaratula.Create(nil);              // SS_1017_PDO_20120514
                        end;                                                                                    // SS_1017_PDO_20120514




                        if not RptCarpetasMorososCaratula.Inicializar(cdsConveniosEnCarpeta) then Exit;

                        // Obtener las NK impagas del convenio
                        spObtenerComprobantes.Close;
                        spObtenerComprobantes.Parameters.ParamByName('@CodigoConvenio').Value := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoConvenio').Value;
                        spObtenerComprobantes.Open;

                        LiberarMemoria;		// SS_1017_PDO_20120514

                        // Por cada NK imprimir los reportes de Factura y FacturacionDetallada
                        while not spObtenerComprobantes.Eof do begin
                            // Rev.7 / 27-04-2009 / Nelson Droguett Sierra
                            if ((spObtenerComprobantes.FieldByName('TipoComprobante').AsString=TC_NOTA_COBRO) AND
                                ((spObtenerComprobantes.FieldByName('TipoComprobanteFiscal').AsString=TC_FACTURA_EXENTA) OR
                                 (spObtenerComprobantes.FieldByName('TipoComprobanteFiscal').AsString=TC_FACTURA_AFECTA) OR
                                 (spObtenerComprobantes.FieldByName('TipoComprobanteFiscal').AsString=TC_BOLETA_EXENTA) OR
                                 (spObtenerComprobantes.FieldByName('TipoComprobanteFiscal').AsString=TC_BOLETA_AFECTA)
                                )
                               ) then
                            begin
//                             ShowMessage(spObtenerComprobantes.FieldByName('TipoComprobanteFiscal').AsString+' '+spObtenerComprobantes.FieldByName('NumeroComprobanteFiscal').AsString);
                              Try
                                // *********************************************************************************
                                // *********************************************************************************
                                // *********************************************************************************
                                // Reporte de Factura Electronica
                                bCrearReporteFE := not Assigned(RptBoletaFacturaElectronica);
                                if bCrearReporteFE then begin                                                                       // SS_1017_PDO_20120514
                                    //Application.CreateForm(TReporteBoletaFacturaElectronicaForm,RptBoletaFacturaElectronica);     // SS_1017_PDO_20120514
                                    RptBoletaFacturaElectronica := TReporteBoletaFacturaElectronicaForm.Create(nil);                // SS_1017_PDO_20120514
                                end;                                                                                                // SS_1017_PDO_20120514





                                ImFdo := True;
                                if not RptBoletaFacturaElectronica.Inicializar( DMConnections.BaseCAC,
                                                                                spObtenerComprobantes.FieldByName('TipoComprobante').AsString,
                                                                                spObtenerComprobantes.FieldByName('NumeroComprobante').AsInteger,
                                                                                sError, ImFdo) then begin
                                    ShowMessage( sError );
                                    Exit;
                                end;
                                // Revision 4: Activar por defecto las opciones de impresi�n

                                RptBoletaFacturaElectronica.rptReporteFactura.Device:=RptCarpetasMorososCaratula.pprCaratula.Device;
                                RptBoletaFacturaElectronica.rptReporteFactura.DeviceType:=RptCarpetasMorososCaratula.pprCaratula.DeviceType;
                                RptBoletaFacturaElectronica.rptReporteFactura.PrinterSetup:=RptCarpetasMorososCaratula.pprCaratula.PrinterSetup;
                                RptBoletaFacturaElectronica.rbiFactura.Execute(bCrearReporteFE );

                                // Reporte de Facturacion Detallada
                                if FImprimirDetalleTransito = 1 then begin
                                    bCrearReporteD := Not Assigned(RptFacturacionDetallada);
                                    if bCrearReporteD then begin                                                                // SS_1017_PDO_20120514
                                       //Application.CreateForm(TformreportefacturacionDetallada, RptFacturacionDetallada);     // SS_1017_PDO_20120514
                                       RptFacturacionDetallada := TformreportefacturacionDetallada.Create(nil);                 // SS_1017_PDO_20120514
                                    end;                                                                                        // SS_1017_PDO_20120514


                                    if RptFacturacionDetallada.Inicializar(DMConnections.BaseCAC,
                                                                            spObtenerComprobantes.FieldByName('TipoComprobante').AsString,
                                                                            spObtenerComprobantes.FieldByName('NumeroComprobante').AsInteger,
                                                                            sError, True) then begin
                                        RptFacturacionDetallada.ObtenerDetalleTransitos(spObtenerComprobantes.FieldByName('TipoComprobante').AsString,spObtenerComprobantes.FieldByName('NumeroComprobante').AsInteger);
                                        if NOT(RptFacturacionDetallada.ppDetallada.DataSource.DataSet.IsEmpty) then begin
                                          RptFacturacionDetallada.MostrarMensajeImpresionExistosa := False;
                                          RptFacturacionDetallada.rptReporteFacturacionDetallada.Device:=RptCarpetasMorososCaratula.pprCaratula.Device;
                                          RptFacturacionDetallada.rptReporteFacturacionDetallada.DeviceType:=RptCarpetasMorososCaratula.pprCaratula.DeviceType;
                                          RptFacturacionDetallada.rptReporteFacturacionDetallada.PrinterSetup:=RptCarpetasMorososCaratula.pprCaratula.PrinterSetup;
                                          RptFacturacionDetallada.rbiDetallada.Execute(bCrearReporteD OR bDebeSalirDialogo);
                                          bDebeSalirDialogo:=False;
                                        end;


                                    end;
                                end;
                                // *********************************************************************************
                                // *********************************************************************************
                                // *********************************************************************************
                              Finally
                                //if Assigned(RptBoletaFacturaElectronica) then
                                //    RptBoletaFacturaElectronica.Release;
                                //if Assigned(RptFacturacionDetallada) then
                                //    RptFacturacionDetallada.Release;
                              End;
                            end
                            else if ((spObtenerComprobantes.FieldByName('TipoComprobante').AsString<>TC_SALDO_INICIAL_DEUDOR)   //SS_1399_NDR_20151005
                                      AND                                                                                       //SS_1399_NDR_20151005
                                     (spObtenerComprobantes.FieldByName('TipoComprobante').AsString<>TC_SALDO_INICIAL_ACREEDOR) //SS_1399_NDR_20151005
                                    ) then                                                                                      //SS_1399_NDR_20151005
                            begin
//                              ShowMessage(spObtenerComprobantes.FieldByName('TipoComprobanteFiscal').AsString+' '+spObtenerComprobantes.FieldByName('NumeroComprobanteFiscal').AsString);
                              Try
                                // Reporte de Factura
                                bCrearReporteF:=Not Assigned(RptFactura);
                                if  bCrearReporteF then begin                                   // SS_1017_PDO_20120514
                                    //Application.CreateForm(TFrmReporteFactura,RptFactura);    // SS_1017_PDO_20120514
                                    RptFactura := TFrmReporteFactura.Create(nil);               // SS_1017_PDO_20120514
                                end;                                                            // SS_1017_PDO_20120514

                                ImFdo := True;
                                if not RptFactura.Inicializar(DMConnections.BaseCAC,
                                                            spObtenerComprobantes.FieldByName('TipoComprobante').AsString,
                                                            spObtenerComprobantes.FieldByName('NumeroComprobante').AsInteger,
                                                            sError, ImFdo) then begin
                                    ShowMessage( sError );
                                    Exit;
                                end;
                                // Revision 4: Activar por defecto las opciones de impresi�n

                                RptFactura.rptReporteFactura.Device:=RptCarpetasMorososCaratula.pprCaratula.Device;
                                RptFactura.rptReporteFactura.DeviceType:=RptCarpetasMorososCaratula.pprCaratula.DeviceType;
                                RptFactura.rptReporteFactura.PrinterSetup:=RptCarpetasMorososCaratula.pprCaratula.PrinterSetup;
                                RptFactura.rbiFactura.Execute(bCrearReporteF);



                                {if RptCarpetasMorososCaratula.pprCaratula.DeviceType<>'Printer' then
                                begin
                                  RptFactura.rptReporteFactura.Print;
                                end
                                else
                                begin
                                  RptFactura.rptReporteFactura.Print;
                                end;}


                                // Reporte de Facturacion Detallada
                                if FImprimirDetalleTransito = 1 then begin
                                    bCrearReporteD:= Not Assigned(RptFacturacionDetallada);
                                    if bCrearReporteD then begin                                                                // SS_1017_PDO_20120514
                                        //Application.CreateForm(TformreportefacturacionDetallada, RptFacturacionDetallada);    // SS_1017_PDO_20120514
                                        RptFacturacionDetallada := TformreportefacturacionDetallada.Create(nil);                // SS_1017_PDO_20120514
                                    end;                                                                                        // SS_1017_PDO_20120514




                                    if RptFacturacionDetallada.Inicializar(DMConnections.BaseCAC, spObtenerComprobantes.FieldByName('TipoComprobante').AsString,
                                      spObtenerComprobantes.FieldByName('NumeroComprobante').AsInteger, sError, True) then begin
                                      RptFacturacionDetallada.ObtenerDetalleTransitos(spObtenerComprobantes.FieldByName('TipoComprobante').AsString,spObtenerComprobantes.FieldByName('NumeroComprobante').AsInteger);
                                      if NOT(RptFacturacionDetallada.ppDetallada.DataSource.DataSet.IsEmpty) then begin
                                        RptFacturacionDetallada.MostrarMensajeImpresionExistosa := False;
                                        RptFacturacionDetallada.rptReporteFacturacionDetallada.Device:=RptCarpetasMorososCaratula.pprCaratula.Device;
                                        RptFacturacionDetallada.rptReporteFacturacionDetallada.DeviceType:=RptCarpetasMorososCaratula.pprCaratula.DeviceType;
                                        RptFacturacionDetallada.rptReporteFacturacionDetallada.PrinterSetup:=RptCarpetasMorososCaratula.pprCaratula.PrinterSetup;
                                        RptFacturacionDetallada.rbiDetallada.Execute(bCrearReporteD OR bDebeSalirDialogo);
                                        bDebeSalirDialogo:=False;
                                      end;
                                    end;
                                end;
                              Finally
                                //if Assigned(RptFactura) then
                                //    RptFactura.Release;
                                //if Assigned(RptFacturacionDetallada) then
                                //    RptFacturacionDetallada.Release;
                              End;
                            end;
                            spObtenerComprobantes.Next;
                        end; // while de Comprobantes del Convenio

                        // Imprimir el reporte de Transitos No Facturados
                        if Not Assigned(RptTransitosNoFacturados) then begin                                    // SS_1017_PDO_20120514
                            //Application.CreateForm(TRptTransitosNoFacturadosForm,RptTransitosNoFacturados);   // SS_1017_PDO_20120514
                            RptTransitosNoFacturados := TRptTransitosNoFacturadosForm.Create(nil);              // SS_1017_PDO_20120514
                        end;                                                                                    // SS_1017_PDO_20120514


                        RptTransitosNoFacturados.Inicializar(cdsConveniosEnCarpeta);



                        //BEGIN : SS_1399_NDR_20151026 ---------------------------------------------------------------------------------
                        // Imprimir los comprobantes SD
                        if not Assigned(RptCarpetasMorososComprobantesSD) then
                        begin
                            RptCarpetasMorososComprobantesSD := TfrmRptCarpetasMorososComprobantesSD.Create(nil);
                        end;
                        with RptCarpetasMorososComprobantesSD.spObtenerComprobantes do
                        begin
                          Close;
                          Parameters.Refresh;
                          Parameters.ParamByName('@CodigoConvenio').Value := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoConvenio').Value;
                          Parameters.ParamByName('@SoloSD').Value := 1;
                          Open;
                        end;
                        if not RptCarpetasMorososComprobantesSD.Inicializar(cdsConveniosEnCarpeta) then Exit;
                        //END : SS_1399_NDR_20151026 ---------------------------------------------------------------------------------

                    finally
                        //if Assigned(RptCarpetasMorososCaratula) then
                        //    RptCarpetasMorososCaratula.Release;
                        //if Assigned(RptTransitosNoFacturados) then
                        //    RptTransitosNoFacturados.Release;
                    end;
                end;
                cdsConveniosEnCarpeta.Next;
            end; // while de los Convenios
            ActualizarConvenios('Imprime');
        end;
    finally
        cdsConveniosEnCarpeta.First;
        cdsConveniosEnCarpeta.EnableControls;

        if Assigned(RptBoletaFacturaElectronica) then begin     // SS_1017_PDO_20120514
            //RptBoletaFacturaElectronica.Release;              // SS_1017_PDO_20120514
            FreeAndNil(RptBoletaFacturaElectronica);            // SS_1017_PDO_20120514
        end;                                                    // SS_1017_PDO_20120514
        if Assigned(RptFactura) then begin                      // SS_1017_PDO_20120514
            //RptFactura.Release;                               // SS_1017_PDO_20120514
            FreeAndNil(RptFactura);                             // SS_1017_PDO_20120514
        end;                                                    // SS_1017_PDO_20120514
        if Assigned(RptFacturacionDetallada) then begin         // SS_1017_PDO_20120514
            //RptFacturacionDetallada.Release;                  // SS_1017_PDO_20120514
            FreeAndNil(RptFacturacionDetallada);                // SS_1017_PDO_20120514
        end;                                                    // SS_1017_PDO_20120514
        if Assigned(RptCarpetasMorososCaratula) then begin      // SS_1017_PDO_20120514
            //RptCarpetasMorososCaratula.Release;               // SS_1017_PDO_20120514
            FreeAndNil(RptCarpetasMorososCaratula);             // SS_1017_PDO_20120514
        end;                                                    // SS_1017_PDO_20120514
        if Assigned(RptTransitosNoFacturados) then begin        // SS_1017_PDO_20120514
            //RptTransitosNoFacturados.Release;                 // SS_1017_PDO_20120514
            FreeAndNil(RptTransitosNoFacturados)                // SS_1017_PDO_20120514
        end;                                                    // SS_1017_PDO_20120514
        if Assigned(fImprimirCarpetaLegal) then begin           // SS_1017_PDO_20120514
            //fImprimirCarpetaLegal.Release;                    // SS_1017_PDO_20120514
            FreeAndNil(fImprimirCarpetaLegal);                  // SS_1017_PDO_20120514
        end;                                                    // SS_1017_PDO_20120514
        if Assigned(RptCarpetasMorososComprobantesSD) then      //SS_1399_NDR_20151026
        begin                                                   //SS_1399_NDR_20151026
            FreeAndNil(RptCarpetasMorososComprobantesSD);       //SS_1399_NDR_20151026
        end;                                                    //SS_1399_NDR_20151026
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarConvenios
  Author: ???????
  Date Created: ??/??/????
  Description: Actualiza los convenios de acuerdo al tipo de accion y
  actualiza los datos del dataset

    Revision 1
    Author: mpiazza
    Date: 04/02/2009
    Description: SS-146 se agrego dos campos en la actualizacion de la grilla,
    y dos parametros en la actualizacion del sp, empresarecaudadora y tipodeuda
    Revision :2
        Author : vpaszkowicz
        Date : 02/03/2009
        Description : Agrego el codigo de empresa recaudadora y el del tipo
        deuda al client dataset durante el envio, para que este en la tabla
        temporal y cause errores si despu�s se quiere sacar de legales o imprimir
-----------------------------------------------------------------------------}
procedure TFormCarpetaLegales.ActualizarConvenios(Operacion: string);
var
    FechaProceso: TDateTime;
begin
    FechaProceso:= NowBase(spActualizarConvenio.Connection);
    cdsConveniosEnCarpeta.first;
    while not cdsConveniosEnCarpeta.eof do begin
        if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean then begin
            spActualizarConvenio.Parameters.ParamByName('@Accion'                 ).Value  := 1 ; //alta
            spActualizarConvenio.Parameters.ParamByName('@IdConvenioCarpetaLegal' ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaIdConvenioCarpetaLegal').AsInteger;
            spActualizarConvenio.Parameters.ParamByName('@CodigoConvenio'         ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoConvenio'        ).AsInteger;
            spActualizarConvenio.Parameters.ParamByName('@NumeroDocumento'        ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroDocumento'       ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@Apellido'               ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaApellido'              ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@ApellidoMaterno'        ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaApellidoMaterno'       ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@Nombre'                 ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNombre'                ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@UsuarioCreacion'        ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioCreacion'       ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@NumeroConvenio'         ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio'        ).AsString ;
            spActualizarConvenio.Parameters.ParamByName('@DiasDeuda'              ).Value  := 0; //no se actualizan nunca
            spActualizarConvenio.Parameters.ParamByName('@ImporteDeuda'           ).Value  := 0; //no se actualizan nunca

            if Operacion = 'Imprime' then begin
                cdsConveniosEnCarpeta.Edit;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraImpresion'    ).AsDateTime := FechaProceso;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioImpresion'      ).AsString   := UsuarioSistema;
                cdsConveniosEnCarpeta.Post;
            end;

            spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion'     ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraImpresion'    ).AsDateTime;
            if spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion'  ).Value  = 0 then
                spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion' ).Value  := null ;

            spActualizarConvenio.Parameters.ParamByName('@UsuarioImpresion'       ).Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioImpresion'      ).AsString  ;

            if Operacion = 'Envio' then begin
                cdsConveniosEnCarpeta.Edit;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraEnvioALegales').AsDateTime := FechaProceso ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioEnvioLegales'   ).AsString   := UsuarioSistema          ;

                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaAliasEmpresaRecaudadora').AsString := fAliasEmpresaRecaudadora ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoEmpresasRecaudadoras').AsInteger := fEmpresasRecaudadoras;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoTipoDeuda').AsInteger := fTipoDeuda;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaDescripcionTipoDeuda'   ).AsString   := fDescripcionTipoDeuda  ;

                cdsConveniosEnCarpeta.Post;
            end;

            spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales'  ).Value := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraEnvioALegales').AsDateTime ;
            spActualizarConvenio.Parameters.ParamByName('@UsuarioEnvioLegales'    ).Value := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioEnvioLegales'   ).AsString   ;

            if spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales'  ).Value  = 0 then
                spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales' ).Value  := null ;


            if Operacion = 'Salida' then begin
                cdsConveniosEnCarpeta.Edit;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraSalidaLegales').AsDateTime := FechaProceso ;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioSalidaLegales').AsString   := UsuarioSistema ;
                cdsConveniosEnCarpeta.Post;
            end;

            spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales' ).Value   := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraSalidaLegales').AsDateTime ;
            spActualizarConvenio.Parameters.ParamByName('@UsuarioSalidaLegales' ).Value     := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaUsuarioSalidaLegales').AsString ;


            spActualizarConvenio.Parameters.ParamByName('@UsuarioActualizacion'   ).Value := UsuarioSistema;

            if spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales'  ).Value  = 0 then
                spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales' ).Value  := null ;
            //Si esta en la tabla es por Envio o porque ya estaba grabado
            if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoEmpresasRecaudadoras').AsInteger = 0 then
                spActualizarConvenio.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value  := NULL
            else    spActualizarConvenio.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoEmpresasRecaudadoras').AsInteger;
            //Si esta en la tabla es por Envio o porque ya estaba grabado            
            if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoTipoDeuda').AsInteger = 0 then
                spActualizarConvenio.Parameters.ParamByName('@CodigoTipoDeuda').Value := NULL
            else spActualizarConvenio.Parameters.ParamByName('@CodigoTipoDeuda').Value := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaCodigoTipoDeuda').AsInteger;
            spActualizarConvenio.Execproc;
        end;
        cdsConveniosEnCarpeta.Next;
    end;
    fEmpresasRecaudadoras := 0;
    fTipoDeuda := 0;

end;


procedure TFormCarpetaLegales.btnLimpiarFiltroClick(Sender: TObject);
begin
    peRutCliente.Text := '';
    neLineas.ValueInt := 0;
    neDeudaMaxima.ValueInt := 0;
    neDeudaMinima.ValueInt := 0;
    neDiasDeuda.ValueInt := 0;
end;



procedure TFormCarpetaLegales.btnSalidaLegalesClick(Sender: TObject);
resourcestring
    MSG_SALIDA_CARPETAS   = 'Se han seleccionado %d convenios para salida de legales con fecha %s.';
    CAPTION_ENVIAR        = 'Salida de Legales de Carpetas.';
    MSG_NO_HAY_CONVENIOS_SELECCIONADOS = 'No hay convenios seleccionados para salida de legales';
    MSG_FINALIZADO = 'Proceso Finalizado' ;
    //MSG_CONVENIOS_DESMARCADOS = 'Los siguientes convenios seleccionados no tienen ingreso en legales o ya tienen salida (no se los puede dar salida):';   // SS_660_CQU_20131112
    MSG_CONVENIOS_DESMARCADOS = 'A los siguientes convenios seleccionados no se les puede dar salida de legales:';                                          // SS_660_CQU_20131112
var
    CantidadRegistrosSeleccionados, CantidadRegistrosDesmarcados: integer;
    ConveniosDesmarcados: String;
begin
    cdsConveniosEnCarpeta.First;
    cdsConveniosEnCarpeta.DisableControls;
    CantidadRegistrosSeleccionados := 0;
    CantidadRegistrosDesmarcados := 0;
    ConveniosDesmarcados := '';
    //los que puedo procesar tienen que tener fecha de envio asignada y fecha de salida vacia
    while not cdsConveniosEnCarpeta.eof do begin
        if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean then
            if (cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraEnvioALegales').AsDateTime > 0) and
               (cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaFechaHoraSalidaLegales').AsDateTime = 0) then
                CantidadRegistrosSeleccionados := CantidadRegistrosSeleccionados + 1
            else begin
                cdsConveniosEnCarpeta.Edit;
                cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean := False;
                cdsConveniosEnCarpeta.Post;
                CantidadRegistrosDesmarcados := CantidadRegistrosDesmarcados + 1;
                //ConveniosDesmarcados := ConveniosDesmarcados + cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString + CHR(13);                           // SS_660_CQU_20131112
                ConveniosDesmarcados := ConveniosDesmarcados + FormatearNumeroConvenio(cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString) + CHR(13);    // SS_660_CQU_20131112
            end;
        cdsConveniosEnCarpeta.Next;
    end;

    if CantidadRegistrosSeleccionados = 0  then begin
        if CantidadRegistrosDesmarcados = 0 then
            // Solo se muestra este mensaje si no se seleccion� ning�n registro
            MsgBox(MSG_NO_HAY_CONVENIOS_SELECCIONADOS, Caption, 0)
        else
            // Se muestra este mensaje si todos los registros seleccionados no estan en carpeta legal
            MsgBox(MSG_CONVENIOS_DESMARCADOS + CHR(13) + ConveniosDesmarcados, Caption, 0);

        cdsConveniosEnCarpeta.First;
        cdsConveniosEnCarpeta.EnableControls;
        Exit;
    end;

    // Se muestra este mensaje si existen algunos registros seleccionados que no estan en carpeta legal
    if CantidadRegistrosDesmarcados > 0 then
        MsgBox(MSG_CONVENIOS_DESMARCADOS + CHR(13) + ConveniosDesmarcados, Caption, 0);

    try
        if MsgBox(Format(MSG_SALIDA_CARPETAS, [CantidadRegistrosSeleccionados, DateTimeToStr(NowBase(spActualizarConvenio.Connection)) ]), CAPTION_ENVIAR, MB_OKCANCEL) = mrOk then begin
            ActualizarConvenios('Salida');
            MsgBox(MSG_FINALIZADO , Caption, 0);
        end;

    finally
        cdsConveniosEnCarpeta.First;
        cdsConveniosEnCarpeta.EnableControls;
    end;
end;

procedure TFormCarpetaLegales.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormCarpetaLegales.btnEliminarLegalesClick(Sender: TObject);
resourcestring
    MSG_CONFIRMA_ELIMINAR_CONVENIO = 'Confirma la eliminaci�n del convenio %s de carpetas legales?';
    MSG_ERROR_ELIMINNDO_CONVENIO = 'Se ha producido un error eliminando el convenio de carpetas legales?';
    MSG_NO_HAY_CONVENIOS_SELECCIONADOS = 'No hay convenios seleccionados para eliminar de legales';
    MSG_ELIMINAR_CONVENIOS = 'Se han seleccionado %d convenios para eliminar de legales con fecha %s.';
    CAPTION_ELIMINAR = 'Eliminaci�n de Convenios de Carpetas Legales.';
    MSG_FINALIZADO = 'Proceso finalizado';
var
    CantidadRegistrosSeleccionados: integer;
begin
    //Eliminar, en realidad se marca el registro como eliminado
    //ELIMINA SOLAMENTE EL ACTUAL, NO LA LISTA DE SELECCIONADOS

    // Revision 3: Se podran eliminar todos los registros marcados
    cdsConveniosEnCarpeta.First;
    cdsConveniosEnCarpeta.DisableControls;
    CantidadRegistrosSeleccionados := 0;
    while not cdsConveniosEnCarpeta.eof do begin
        if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean then
                CantidadRegistrosSeleccionados := CantidadRegistrosSeleccionados + 1;

        cdsConveniosEnCarpeta.Next;
    end;

    if CantidadRegistrosSeleccionados = 0  then begin
        MsgBox(MSG_NO_HAY_CONVENIOS_SELECCIONADOS, Caption, 0);
        cdsConveniosEnCarpeta.First;
        cdsConveniosEnCarpeta.EnableControls;
        Exit;
    end;

    try
        if MsgBox(Format(MSG_ELIMINAR_CONVENIOS, [CantidadRegistrosSeleccionados, DateTimeToStr(NowBase(spEliminarConvenio.Connection)) ]), CAPTION_ELIMINAR, MB_OKCANCEL) = mrOk then begin
            cdsConveniosEnCarpeta.First;
            while not cdsConveniosEnCarpeta.eof do begin
                if cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean then begin
                    spEliminarConvenio.Parameters.ParamByName('@IdConvenioCarpetaLegal').Value  := cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaIdConvenioCarpetaLegal').AsInteger ;
                    spEliminarConvenio.Parameters.ParamByName('@UsuarioElimina').Value          := UsuarioSistema;
                    spEliminarConvenio.Parameters.ParamByName('@FechaHoraActualizacion').Value  := NowBase(spActualizarConvenio.Connection);
                    spEliminarConvenio.ExecProc;
                    cdsConveniosEnCarpeta.Delete;
                end
                else
                    cdsConveniosEnCarpeta.Next;
            end;
            MsgBox(MSG_FINALIZADO , Caption, 0);
        end;
    finally
        cdsConveniosEnCarpeta.EnableControls;
        cdsConveniosEnCarpeta.First;
    end;
end;

procedure TFormCarpetaLegales.dblConveniosAIngresarDblClick(Sender: TObject);
begin
	if cdsConveniosAIngresar.IsEmpty then Exit;

	dblConveniosAIngresar.Invalidate;
    cdsConveniosAIngresar.Edit;
    cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado').AsBoolean := not cdsConveniosAIngresar.FieldByName( 'cdsConveniosAIngSeleccionado' ).AsBoolean;         // SS_660_CQU_20131112
    ////cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado').AsBoolean := not cdsConveniosAIngresar.FieldByName( 'cdsConveniosAIngSeleccionado' ).AsBoolean;     // SS_660_CQU_20131112    // SS_660_CQU_20130822
    //if not cdsConveniosAIngresar.FieldByName('cdsConveniosAIngEnListaAmarilla').AsBoolean then                                                                              // SS_660_CQU_20131112    // SS_660_CQU_20130822
    //    cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado').AsBoolean := not cdsConveniosAIngresar.FieldByName( 'cdsConveniosAIngSeleccionado' ).AsBoolean    // SS_660_CQU_20131112    // SS_660_CQU_20130822
    //else                                                                                                                                                                    // SS_660_CQU_20131112    // SS_660_CQU_20130822
    //    DBListExMostrarMensajeBalloon(  (Sender as TDBListEx),                                                                                                              // SS_660_CQU_20131112    // SS_660_CQU_20130822
    //                                    'cdsConveniosAIngSeleccionado',                                                                                                     // SS_660_CQU_20131112    // SS_660_CQU_20130822
    //                                    Caption,                                                                                                                            // SS_660_CQU_20131112    // SS_660_CQU_20130822
    //                                    Format(MSG_CONVENIO_EN_LA, [cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroConvenio').AsString]));                        // SS_660_CQU_20131112    // SS_660_CQU_20130822
    cdsConveniosAIngresar.Post;
	dblConveniosAIngresar.Invalidate;

end;

procedure TFormCarpetaLegales.dblConveniosAIngresarDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    if (Column.FieldName = 'cdsConveniosAIngSeleccionado') then begin
        Text := '';
        Sender.Canvas.FillRect(Rect);
        bmp := TBitMap.Create;
        try
            DefaultDraw := False;
            if (cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado').AsBoolean ) then begin
                Img_Tilde.GetBitmap(0, Bmp);
            end else begin
                Img_Tilde.GetBitmap(1, Bmp);
            end;
            Sender.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end;
    end;
    // Pinto el n�mero de convenio con el color amarillo si est� en Lsita Amarilla          // SS_660_CQU_20130822
    if  (Column.FieldName = 'cdsConveniosAIngNumeroConvenio') and                           // SS_660_CQU_20130822
        (cdsConveniosAIngresar.FieldByName('cdsConveniosAIngEnListaAmarilla').AsBoolean)    // SS_660_CQU_20130822
    then begin                                                                              // SS_660_CQU_20130822
        if not (State = [odSelected]) or not (State = [odSelected, odFocused]) then begin   // SS_660_CQU_20130822
            Sender.Canvas.Brush.Color   := clYellow;                                        // SS_660_CQU_20130822
            Sender.Canvas.Font.Color    := clBlack;                                         // SS_660_CQU_20130822
        end else                                                                            // SS_660_CQU_20130822
            Sender.Canvas.Font.Color    := clYellow;                                        // SS_660_CQU_20130822
    end;                                                                                    // SS_660_CQU_20130822
end;

procedure TFormCarpetaLegales.dblConveniosAIngresarKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if Key = VK_SPACE then dblConveniosAIngresarDblClick(Sender);
end;

procedure TFormCarpetaLegales.OrdenarColumna(
  Sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumnaCustom(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;
{-----------------------------------------------------------------------------
  Function Name: OrdenarGridPorColumnaCustom
  Author: mpiazza
  Date Created: 05/02/2009
  Description: ss-146 Recreo la funcion localmente de
  ordenargridporcolumnacustom por que no utiliza el TADODATASET sno el
  TCLIENTDATASET como fuente de datos, al no tener los mismos metodos
  implemento este, para pasar el prodedimiento se necesita ver mediante RTTI
  que tipo de componente es ya que los dos heredan de TCOMPONENT

-----------------------------------------------------------------------------}
procedure TFormCarpetaLegales.OrdenarGridPorColumnaCustom(
  Grilla: TCustomDBListEx; Columna: TDBListExColumn; NombreCampo: string;
  NoCambiarOrden: boolean);
var
  iContador     : integer;
  iIndex        : TIndexDef;
  CampoaIndexar : string;
begin
    if not NoCambiarOrden then begin
        if Columna.Sorting = csAscending then
            Columna.Sorting := csDescending
        else
            Columna.Sorting := csAscending;
        //TClientDataSet(Grilla.DataSource.DataSet).Sort := NombreCampo + iif(Columna.Sorting = csAscending, ' ASC' ,' DESC'); end

        if TClientDataSet(Grilla.DataSource.DataSet).IndexDefs.Count = 0 then begin
          for iContador := 0 to TClientDataSet(Grilla.DataSource.DataSet).FieldDefs.Count - 1 do begin
              CampoaIndexar := TClientDataSet(Grilla.DataSource.DataSet).FieldDefs[iContador].Name;
              TClientDataSet(Grilla.DataSource.DataSet).AddIndex('ASC' + copy(CampoaIndexar, 18,  Length(CampoaIndexar)), CampoaIndexar, []) ;
              TClientDataSet(Grilla.DataSource.DataSet).AddIndex( 'DES' + copy(CampoaIndexar, 18,  Length(CampoaIndexar)), CampoaIndexar, [ixDescending]);
          end;
        end;
         TClientDataSet(Grilla.DataSource.DataSet).IndexName := iif(Columna.Sorting = csAscending, 'ASC' ,'DES') + copy(NombreCampo, 18,  Length(NombreCampo)) ;
        //        :=  NombreCampo+ iif(Columna.Sorting = csAscending, ' ASC' ,' DESC'); end
    end;

end;

procedure TFormCarpetaLegales.dblConveniosEnCarpetaDblClick(Sender: TObject);
begin
	if cdsConveniosEnCarpeta.IsEmpty then Exit;

	dblConveniosEnCarpeta.Invalidate;

    cdsConveniosEnCarpeta.Edit;
    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean := not cdsConveniosEnCarpeta.FieldByName( 'cdsConveniosEnCarpetaSeleccionado' ).AsBoolean;         // SS_660_CQU_20131112
    ////cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean := not cdsConveniosEnCarpeta.FieldByName( 'cdsConveniosEnCarpetaSeleccionado' ).AsBoolean;     // SS_660_CQU_20131112  // SS_660_CQU_20130822
    //if not cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaEnListaAmarilla').AsBoolean then                                                                                   // SS_660_CQU_20131112  // SS_660_CQU_20130822
    //    cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean := not cdsConveniosEnCarpeta.FieldByName( 'cdsConveniosEnCarpetaSeleccionado' ).AsBoolean    // SS_660_CQU_20131112  // SS_660_CQU_20130822
    //else																																												// SS_660_CQU_20131112  // SS_660_CQU_20130822
    //    DBListExMostrarMensajeBalloon(  (Sender as TDBListEx),                                                                                                                        // SS_660_CQU_20131112  // SS_660_CQU_20130822
    //                                    'cdsConveniosEnCarpetaSeleccionado',                                                                                                          // SS_660_CQU_20131112  // SS_660_CQU_20130822
    //                                    Caption,                                                                                                                                      // SS_660_CQU_20131112  // SS_660_CQU_20130822
    //                                    Format(MSG_CONVENIO_EN_LA, [cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString]));                             // SS_660_CQU_20131112  // SS_660_CQU_20130822
    cdsConveniosEnCarpeta.Post;

	dblConveniosEnCarpeta.Invalidate;

end;

procedure TFormCarpetaLegales.dblConveniosEnCarpetaDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    if (Column.FieldName = 'cdsConveniosEnCarpetaSeleccionado') then begin
        Text := '';
        Sender.Canvas.FillRect(Rect);
        bmp := TBitMap.Create;
        try
            DefaultDraw := False;
            if (cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean ) then begin
                Img_Tilde.GetBitmap(0, Bmp);
            end else begin
                Img_Tilde.GetBitmap(1, Bmp);
            end;
            Sender.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end;
    end;
    if (Column.FieldName = 'cdsConveniosEnCarpetaImporteDeudaActualizada') then begin
          Text := FloatToStrF(cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaImporteDeudaActualizada').AsCurrency / 100, ffCurrency,20,0) ;


    end;
    if (Column.FieldName = 'cdsConveniosEnCarpetaDeudaStr') then begin
          Text := FloatToStrF(cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaDeudaStr').AsCurrency / 100, ffCurrency,20,0) ;


    end;
    // Pinto el n�mero de convenio con el color amarillo si est� en Lsita Amarilla              // SS_660_CQU_20130822
    if  (Column.FieldName = 'cdsConveniosEnCarpetaNumeroConvenio') and                          // SS_660_CQU_20130822
        (cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaEnListaAmarilla').AsBoolean)   // SS_660_CQU_20130822
    then begin                                                                                  // SS_660_CQU_20130822
        if not (State = [odSelected]) or not (State = [odSelected, odFocused]) then begin       // SS_660_CQU_20130822
            Sender.Canvas.Brush.Color   := clYellow;                                            // SS_660_CQU_20130822
            Sender.Canvas.Font.Color    := clBlack;                                             // SS_660_CQU_20130822
        end else                                                                                // SS_660_CQU_20130822
            Sender.Canvas.Font.Color    := clYellow;                                            // SS_660_CQU_20130822
    end;                                                                                        // SS_660_CQU_20130822
end;

procedure TFormCarpetaLegales.dblConveniosEnCarpetaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if Key = VK_SPACE then dblConveniosEnCarpetaDblClick(Sender);
end;

procedure TFormCarpetaLegales.mnuDeseleccionarClick(Sender: TObject);
begin
    if pgConvenios.ActivePage = tsConveniosEnCarpeta then dblConveniosEnCarpetaDblClick(Sender)
    else dblConveniosAIngresarDblClick(Sender);
end;

procedure TFormCarpetaLegales.mnuDeseleccionarTodosClick(Sender: TObject);
begin
    if pgConvenios.ActivePage = tsConveniosAIngresar then begin
        cdsConveniosAIngresar.DisableControls;
        cdsConveniosAIngresar.First;
        while not cdsConveniosAIngresar.eof do begin
            cdsConveniosAIngresar.Edit;
            cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado'  ).AsBoolean := False;
            cdsConveniosAIngresar.Post;
            cdsConveniosAIngresar.Next;
        end;
        cdsConveniosAIngresar.EnableControls;
        cdsConveniosAIngresar.First;
    end else begin
        cdsConveniosEnCarpeta.DisableControls;
        cdsConveniosEnCarpeta.First;
        while not cdsConveniosEnCarpeta.eof do begin
            cdsConveniosEnCarpeta.Edit;
            cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado'  ).AsBoolean := False;
            cdsConveniosEnCarpeta.Post;
            cdsConveniosEnCarpeta.Next;
        end;
        cdsConveniosEnCarpeta.EnableControls;
        cdsConveniosEnCarpeta.First;
    end;
end;

procedure TFormCarpetaLegales.mnuSeleccionarClick(Sender: TObject);
begin
    if pgConvenios.ActivePage = tsConveniosEnCarpeta then dblConveniosEnCarpetaDblClick(Sender)
    else dblConveniosAIngresarDblClick(Sender);
end;


procedure TFormCarpetaLegales.mnuSeleccionarTodosClick(Sender: TObject);
begin
    if pgConvenios.ActivePage = tsConveniosAIngresar then begin
        cdsConveniosAIngresar.DisableControls;
        cdsConveniosAIngresar.First;
        while not cdsConveniosAIngresar.eof do begin
            cdsConveniosAIngresar.Edit;
            cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado'  ).AsBoolean := True;
            cdsConveniosAIngresar.Post;
            cdsConveniosAIngresar.Next;
        end;
        cdsConveniosAIngresar.EnableControls;
        cdsConveniosAIngresar.First;
    end else begin
        cdsConveniosEnCarpeta.DisableControls;
        cdsConveniosEnCarpeta.First;
        while not cdsConveniosEnCarpeta.eof do begin
            cdsConveniosEnCarpeta.Edit;
            cdsConveniosEnCarpeta.FieldByName('cdsConveniosEnCarpetaSeleccionado'  ).AsBoolean := True;
            cdsConveniosEnCarpeta.Post;
            cdsConveniosEnCarpeta.Next;
        end;
        cdsConveniosEnCarpeta.EnableControls;
        cdsConveniosEnCarpeta.First;
    end;
end;

procedure TFormCarpetaLegales.btnPasarACarpetasLegalesClick(Sender: TObject);
resourcestring
    MSG_EXISTE_CONVENIO_SIN_IMPRIMIR = 'Existe registro ingresado para el convenio %s sin fecha de Salida Legales. No se puede reingresar en Legales';
    MSG_ERROR_INGRESO_CONVENIO = 'Error al intentar ingresar el Convenio.';
    MSG_FINALIZADO = 'Proceso Finalizado' ;
var
  Fecha: TDateTime;
  ConvenioCargado: boolean;
  ConveniosListaAmarilla : string;                                                                          // SS_660_CQU_20131112
  ConvenioSeleccionadoEnListaAmarilla : Integer;                                                            // SS_660_CQU_20131112
begin
    cdsConveniosAIngresar.DisableControls;
    cdsConveniosAIngresar.First;
    ConveniosListaAmarilla := '';                                                                           // SS_660_CQU_20131112
    ConvenioSeleccionadoEnListaAmarilla := 0;                                                               // SS_660_CQU_20131112
    while not cdsConveniosAIngresar.Eof do begin
        //if cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado').AsBoolean = True then begin  // SS_660_CQU_20131112
        if cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado').AsBoolean and                  // SS_660_CQU_20131112
           not (cdsConveniosAIngresar.FieldByName('cdsConveniosAIngEnListaAmarilla').AsBoolean)             // SS_660_CQU_20131112
        then begin                                                                                          // SS_660_CQU_20131112
            //controlo si el convenio seleccionado se puede cargar en carpetas legales o no
            //se puede si no se carg� nunca, o si est� cargado pero con fecha de salida legales
            spConvenioExisteEnCarpeta.Parameters.ParamByName('@CodigoConvenio').Value  := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngCodigoConvenio').AsInteger ;
            spConvenioExisteEnCarpeta.Parameters.ParamByName('@ConvenioCargado').Value := null;
            spConvenioExisteEnCarpeta.Parameters.ParamByName('@FechaHoraSalidaLegales').Value := null;
            spConvenioExisteEnCarpeta.ExecProc;

            ConvenioCargado := ( spConvenioExisteEnCarpeta.Parameters.ParamByName('@ConvenioCargado').Value = 'true' ) ;
            if spConvenioExisteEnCarpeta.Parameters.ParamByName('@FechaHoraSalidaLegales').Value = null  then Fecha := 0
            else  Fecha := spConvenioExisteEnCarpeta.Parameters.ParamByName('@FechaHoraSalidaLegales').Value ;

            if ConvenioCargado and (Fecha = 0) then begin
                //MsgBox(  format(MSG_EXISTE_CONVENIO_SIN_IMPRIMIR,[cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroConvenio').AsString]), Caption , MB_OK)                        // SS_660_CQU_20131112
                MsgBox(  format(MSG_EXISTE_CONVENIO_SIN_IMPRIMIR,[FormatearNumeroConvenio(cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroConvenio').AsString)]), Caption , MB_OK) // SS_660_CQU_20131112
            end else begin
                try
                    spActualizarConvenio.Parameters.ParamByName('@Accion'         ).Value         := 2 ; //alta
                    spActualizarConvenio.Parameters.ParamByName('@CodigoConvenio' ).Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngCodigoConvenio').AsInteger;
                    spActualizarConvenio.Parameters.ParamByName('@NumeroDocumento').Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroDoc').AsString;
                    spActualizarConvenio.Parameters.ParamByName('@NumeroConvenio' ).Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroConvenio').AsString;
                    spActualizarConvenio.Parameters.ParamByName('@Apellido'       ).Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngApellido').AsString;
                    spActualizarConvenio.Parameters.ParamByName('@ApellidoMaterno').Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngApellidoMaterno').AsString;
                    spActualizarConvenio.Parameters.ParamByName('@Nombre'         ).Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNombre').AsString;
                    spActualizarConvenio.Parameters.ParamByName('@UsuarioCreacion').Value         := UsuarioSistema;
                    spActualizarConvenio.Parameters.ParamByName('@DiasDeuda'      ).Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngDias').AsInteger;
                    spActualizarConvenio.Parameters.ParamByName('@ImporteDeuda'   ).Value         := cdsConveniosAIngresar.FieldByName('cdsConveniosAIngDeuda').AsFloat;
                    spActualizarConvenio.Parameters.ParamByName('@FechaHoraImpresion'    ).Value  := null;
                    spActualizarConvenio.Parameters.ParamByName('@UsuarioImpresion'      ).Value  := null;
                    spActualizarConvenio.Parameters.ParamByName('@FechaHoraEnvioLegales' ).Value  := null;
                    spActualizarConvenio.Parameters.ParamByName('@UsuarioEnvioLegales'   ).Value  := null;
                    spActualizarConvenio.Parameters.ParamByName('@FechaHoraSalidaLegales').Value  := null;
                    spActualizarConvenio.Parameters.ParamByName('@UsuarioSalidaLegales'  ).Value  := null;
                    spActualizarConvenio.Execproc;
                    // Revision 3
                    MsgBox(MSG_FINALIZADO , Caption, 0);

                except
                    on E: Exception do begin
                        MsgBoxErr(Format(MSG_ERROR_INGRESO_CONVENIO, [cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroConvenio').AsString]), e.Message, Self.Caption, MB_OK);
                    end;
                end;
            end;
        //end;
        end else if cdsConveniosAIngresar.FieldByName('cdsConveniosAIngSeleccionado').AsBoolean and                         // SS_660_CQU_20131112
                    cdsConveniosAIngresar.FieldByName('cdsConveniosAIngEnListaAmarilla').AsBoolean                          // SS_660_CQU_20131112
        then begin                                                                                                          // SS_660_CQU_20131112
            ConvenioSeleccionadoEnListaAmarilla := ConvenioSeleccionadoEnListaAmarilla + 1;                                 // SS_660_CQU_20131112
            ConveniosListaAmarilla := ConveniosListaAmarilla + FormatearNumeroConvenio(                                     // SS_660_CQU_20131112
                                            cdsConveniosAIngresar.FieldByName('cdsConveniosAIngNumeroConvenio').AsString)   // SS_660_CQU_20131112
                                            + CHR(13);                                                                      // SS_660_CQU_20131112
        end;                                                                                                                // SS_660_CQU_20131112
        cdsConveniosAIngresar.Next;
    end;

    if ConveniosListaAmarilla <> '' then begin                                              // SS_660_CQU_20131112
        if ConvenioSeleccionadoEnListaAmarilla > 1 then begin                               // SS_660_CQU_20131112
                MsgBox(MSG_CONVENIOS_EN_LA + CHR(13) + ConveniosListaAmarilla, Caption, 0); // SS_660_CQU_20131112
                Exit;                                                                       // SS_660_CQU_20131112
        end else begin                                                                      // SS_660_CQU_20131112
            MsgBox(Format(MSG_CONVENIO_EN_LA, [ConveniosListaAmarilla])                     // SS_660_CQU_20131112
                , Caption, MB_ICONINFORMATION);                                             // SS_660_CQU_20131112
            Exit;                                                                           // SS_660_CQU_20131112
        end;                                                                                // SS_660_CQU_20131112
    end;                                                                                    // SS_660_CQU_20131112

    cdsConveniosAIngresar.EnableControls;
end;



procedure TFormCarpetaLegales.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormCarpetaLegales.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);
    Application.createForm(TFormBuscaClientes,F);
    try
    	if F.Inicializa(FUltimaBusqueda) then begin
            F.ShowModal;
            if F.ModalResult = idOk then begin
                FUltimaBusqueda := F.UltimaBusqueda;
                peRUTCliente.Text :=  F.Persona.NumeroDocumento;
                peRUTCliente.SetFocus;
            end;
        end;
    finally
        if Assigned(F) then
            F.Release;
    end;
end;

procedure TFormCarpetaLegales.pgConveniosChange(Sender: TObject);
resourcestring
    MSG_AVISO_SIGUE_USANDO_MISMOS_FILTROS = 'Aplica los mismos filtros de selecci�n a la pr�xima ventana?' ;
begin
    if pgConvenios.ActivePage = tsConveniosEnCarpeta then
        nbBotones.ActivePage := 'pgEnviosSalidas'
    else
        nbBotones.ActivePage := 'pgAceptarCancelar';

//    if MsgBox(MSG_AVISO_SIGUE_USANDO_MISMOS_FILTROS ,Caption, MB_OKCANCEL) = mrOK then
//        btnFiltrarClick(self)
//    else
//        peRUTCliente.SetFocus;

end;

// SS_1017_PDO_20120514 Inicio Bloque
 procedure TFormCarpetaLegales.LiberarMemoria;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
end;
// SS_1017_PDO_20120514 Fin bloque

end.
