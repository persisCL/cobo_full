{********************************** Unit Header ********************************
File Name : frm_FacturacionUnConvenio.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
	Author : ggomez
	Date : 22/03/2006
	Description : Coloqu� los CommandTimeout de los SP y TQuery a 300 segundos,
		ya que par aun convenio que tiene muchas cuentas daba timeout al
		facturarlo manualmente.

Revision 2:
	Author : jconcheyro
	Date : 21/04/2006
	Description : Se eliminaron las llamadas a EliminarLotesSinComprobantes
		ya que si estaba corriendo un proceso masivo y se lanzaba uno manual, arruinaba
		los comprobantes del masivo

Revision 3:
    Author : ggomez
    Date : 26/04/2006
    Description : Quit� el if que se hac�a en la impresi�n del comporobante. Lo
        hice ya que si el operador respond�a que NO quer�a imprimir, mostraba un
        mensaje de error que no deb�a mostrar. Esto lo hac�a ya que el m�todo
        ImprimirComprobante, retorna False si el operador responde que no quiere
        imprimir.

Revision 3:
	Author : jconcheyro
	Date : 30/11/2006
	Description : Se valida que la fecha de corte m�nima posible sea la de inicio de cobranza.

Revision 4:
    Author : FSandi
    Date : 12/03/2007
    Description : Se modific� el formulario para que los datos de la grilla se obtengan de un
                ClientDataSet. Ahora la pantalla permite marcar los conceptos que se desean
                Facturar.

Revision 5:
    Author : nefernandez
    Date : 16/03/2007
    Description : Se pasa el parametro Usuario al SP "GenerarMovimientosTransitos" -
                para auditoria. Tambien se reemplazo el uso de un TADOQuery por la
                llamada a un SP, para actualizar la FechaHora de impresion del
                Comprobante



    Revision : 7
        Author : vpaszkowicz
        Date : 08/07/2008
        Description : Quito la posibilidad de desmarcar conceptos relacionados
        a peajes.

    Revision : 8
        Author : rharris
        Date : 21/01/2009
        Description : (Ref.:SS 627)
                Se modifica el proceso de facturacion para que:
                    1.- utilice tablas de trabajo para el calculo de intereses
                    2.- calculo los consumos de los movimientos prefacturados (masivamente)


*******************************************************************************}
unit frm_FacturacionUnConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Dateedit, ExtCtrls, utilProc, DmiCtrls, util, DBCtrls,
  ImgList, ADODB, DMConnection, ComCtrls, BuscaClientes, Utildb, DB,
  DPSControls, VariantComboBox, constParametrosGenerales, Validate,
  peaTypes, PeaProcs, dateUtils, ListBoxEx, DBListEx, ReporteFactura,
  frmReporteFacturacionDetallada, DBClient, Menus;


type
  TfrmFacturacionUnConvenio = class(TForm)
    ObtenerCLiente: TADOStoredProc;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    peRUTCliente: TPickEdit;
    GroupBox5: TGroupBox;
    lNombreCompleto: TLabel;
    lDomicilio: TLabel;
    lComuna: TLabel;
    lRegion: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label2: TLabel;
    deFechaCorte: TDateEdit;
    deFechaEmision: TDateEdit;
    Label4: TLabel;
    Label5: TLabel;
    deFechaVencimiento: TDateEdit;
    cbConveniosCliente: TVariantComboBox;
    lMedioPago: TLabel;
    ObtenerDatosMedioPagoAutomaticoConvenio: TADOStoredProc;
    Label6: TLabel;
    GroupBox1: TGroupBox;
    lDomicilioFacturacion: TLabel;
    lComunaFacturacion: TLabel;
    lRegionFacturacion: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    GenerarMovimientosTransitos: TADOStoredProc;
    QryObtenerDomicilioFacturacion: TADOQuery;
    Bevel1: TBevel;
    Label7: TLabel;
    DBListEx1: TDBListEx;
    ObtenerUltimosConsumos: TADOStoredProc;
    dsInformeFactura: TDataSource;
    spObtenerComprobanteFacturado: TADOStoredProc;
    Label8: TLabel;
    NumeroConvenio: TEdit;
    spObtenerConveniosCliente: TADOStoredProc;
    tmrConsulta: TTimer;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    spObtenerDatosFacturacionDetallada: TADOStoredProc;
    spCrearProcesoFacturacion: TADOStoredProc;
    chkFacturarCompleto: TCheckBox;
    chkFacturarSinIntereses: TCheckBox;
    btnSalir: TButton;
    btnFacturar: TButton;
    cdConceptos: TClientDataSet;
    ilImages: TImageList;
    PopFacturacionManual: TPopupMenu;
    MnuSeleccionarTodo: TMenuItem;
    MnuDeSeleccionarTodo: TMenuItem;
    SpCargarConceptosAFacturarFacturadorManual: TADOStoredProc;
    SPActualizarFechaHoraFinProcesoFacturacion: TADOStoredProc;
    spActualizarFechaImpresionComprobantes: TADOStoredProc;
    spGenerarTablaTrabajoInteresesPorFacturar: TADOStoredProc;
    spLlenarDetalleConsumoComprobantes: TADOStoredProc;
    procedure cbConveniosClienteDrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure btnSalirClick(Sender: TObject);
    procedure btnFacturarClick(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure deFechaCorteExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure deFechaVencimientoExit(Sender: TObject);
    procedure deFechaEmisionChange(Sender: TObject);
    procedure deFechaEmisionExit(Sender: TObject);
    procedure deFechaCorteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NumeroConvenioChange(Sender: TObject);
    procedure tmrConsultaTimer(Sender: TObject);
    procedure DBListEx1DblClick(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure PopFacturacionManualPopup(Sender: TObject);
    procedure MnuSeleccionarTodoClick(Sender: TObject);
    procedure MnuDeSeleccionarTodoClick(Sender: TObject);
    procedure DBListEx1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
	FUltimaBusqueda : TBusquedaCliente;
    FDiasVencimiento: integer;
	FFechaCorteMaximaPosible: TDateTime;
	FCancelar: Boolean;
    FImprimirFondoNK: Integer;
    // Para mantener los convenios dados de baja, para luego saber cuales hay
    // que colorear en el combo de Convenios.
    FListaConveniosBaja: TStringList;
    FFechaCorteMininaPosible: TDateTime;
    function ValidarConsumos: Boolean;
    function EsFechaHabil(aFecha: TDateTime): Boolean;
    procedure ObtenerResumenAFacturar(Fecha: TDateTime);
    procedure ClearCaptions;
    procedure CargarDataset;
	procedure MostrarDomicilioFacturacion;
    Function ImprimirComprobante(TipoComprobante:string; NroComprobante: int64): Boolean;
    Function ImprimirFacturacionDetallada(TipoComprobante:string; NroComprobante: int64) : boolean;
    procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor);
    function CargarDatosIntereses(CodigoConvenio: Integer; FechaEmision: TDatetime; NumeroProcesoFacturacion: Integer): Integer;
    procedure LlenarDetalleConsumoComprobantes (NumeroProcesoFacturacion: Integer);
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;


implementation

{$R *.dfm}

const
    CONCEPTO_PEAJES_PERIODO = 1;
    CONCEPTO_PEAJES_OTROS_PERIODOS = 2;

resourcestring
    MSG_CLIENTE                         = 'Debe especificarse un cliente y el convenio a facturar.';
    MSG_COMVENIO                        = 'Debe especificarse un convenio a facturar.';
    MSG_FECHA_CORTE_NO_EXISTE           = 'Debe especificarse una fecha de corte.';
    MSG_FECHA_EMISION_NO_EXISTE         = 'Debe especificarse una fecha de emisi�n.';
    MSG_FECHA_VENCIMIENTO_NO_EXISTE     = 'Debe especificarse una fecha de vencimiento.';
    MSG_FECHA_CORTE_INCORRECTA          = 'La fecha de corte no puede ser mayor a la fecha actual.';
    MSG_FECHA_VENCIMIENTO_INCORRECTA    = 'La fecha de vencimiento no puede ser menor o igual a la fecha de emisi�n.';
    MSG_FECHA_EMISION_INCORRECTA        = 'La fecha de emisi�n no puede ser menor a la fecha de corte.';
    MSG_FECHA_EMISION_ANTERIOR_A_ULTIMA	= 'La fecha de emisi�n no puede ser anterior a la fecha de emisi�n '#10#13 +
    										'de la �ltima Nota de Cobro Emitida ';
    MSG_FECHA_VENCIMIENTO_NO_HABIL      = 'La fecha de vencimiento debe corresponder a un d�a laborable.';
    MSG_FECHA_EMISION_NO_HABIL          = 'La fecha de emisi�n debe corresponder a un d�a laborable.';

//Revision 4
var
    CodigoPeajes, CodigoPeajesAnteriores : Integer;
//Fin Revision 4


{******************************** Function Header ******************************
Function Name: EsFechaHabil
Author       : fmalisia
Date Created : 22-11-2004
Description  : Verifica si una fecha corresponde a un dia laborable
               (Distinto de Sabado y Domingo)
Parameters   : aFecha: TDateTime
Return Value : Boolean
*******************************************************************************}
function TfrmFacturacionUnConvenio.EsFechaHabil(aFecha: TDateTime): Boolean;
begin
    result := not ((DayOfTheWeek(aFecha) = daySaturday) or (DayOfTheWeek(aFecha) = daySunday));
end;


{******************************** Function Header ******************************
Function Name: inicializar
Author       : fmalisia
Date Created : 24-11-2004
Description  : Iniciliza el form.
Parameters   : None
Return Value : Boolean

Revision 1:
    Author : nefernandez
    Date : 29/01/2009
    Description : SS 782: Se agrega el permiso para habilitar o no la edici�n de la "Fecha de Emisi�n".
*******************************************************************************}
function TfrmFacturacionUnConvenio.inicializar: Boolean;
Resourcestring
    ERROR_CANNOT_INITIALIZE = 'No se puede inicializar';
    ERROR_CANT_LOAD_INITIAL_DATE = 'No puedo obtener el Parametro General: FECHA_INICIO_COBRANZA';
    ERROR_OBTENIENDO_CONSTANTES_PEAJE = 'Ocurri� un error al obtener las constantes de peajes de periodos anteriores y peajes de periodo actual';
begin
    try
        CenterForm(Self);
        chkFacturarSinIntereses.Visible := ExisteAcceso('GENERAR_INTERESES');
        deFechaEmision.Enabled := ExisteAcceso('FECHA_EMISION_FACTURACION_MANUAL'); //Revision 1
        ClearCaptions;
		FFechaCorteMaximaPosible := NowBase(DMConnections.BaseCAC);
        Result := ObtenerParametroGeneral(DMCOnnections.BaseCAC, DIAS_VENCIMIENTO_COMPROBANTE, FDiasVencimiento);
        if result then begin
            deFechaEmision.Date := NowBase(DMConnections.BaseCAC);
            deFechaCorte.Date := FFechaCorteMaximaPosible;
            deFechaVencimiento.Date := deFechaEmision.Date + FDiasVencimiento;
        end;
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'FECHA_INICIO_COBRANZA', FFechaCorteMininaPosible) then begin
            MsgBoxErr(ERROR_CANNOT_INITIALIZE, ERROR_CANT_LOAD_INITIAL_DATE, Caption, MB_ICONSTOP);
            Exit;
        end;
    except
        on e : Exception do begin
            MsgBoxErr('No se pudo cargar la Facturaci�n Manual', e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;
//Revision 4
    try
      CodigoPeajes := QueryGetValueInt(DMConnections.BaseCAC, 'Select Dbo.CONST_CONCEPTO_PEAJE_PERIODO ()');
      CodigoPeajesAnteriores := QueryGetValueInt(DMConnections.BaseCAC, 'Select Dbo.CONST_CONCEPTO_PEAJE_PERIODOS_ANTERIORES ()');
    except
        on e: Exception do begin
            msgBoxErr(ERROR_OBTENIENDO_CONSTANTES_PEAJE, e.Message, caption, MB_ICONSTOP);
        end;
    end;
//Fin Revision 4
end;


procedure TfrmFacturacionUnConvenio.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: btnFacturarClick
Author       : fmalisia
Date Created : 24-11-2004
Description  : Ejecuta el proceso de facturacion para un convenio.
Parameters   : Sender: TObject
Return Value : None

Revision 1:
    Author : ggomez
    Date : 23/03/2006
    Description :
        - Quit� la transacci�n que se abr�a aqu�, ya que el SP
        GenerarMovimientosTransitos.
        - Modifiqu� el c�digo para que tenga m�s try except para poder
        dar un mensaje en cada sentencia que puede fallar.

Revision 2:
    Author : ggomez
    Date : 23/03/2006
    Description : Quit� el if que se hac�a en la impresi�n del comporobante. Lo
        hice ya que si el operador respond�a que NO quer�a imprimir, mostraba un
        mensaje de error que no deb�a mostrar. Esto lo hac�a ya que el m�todo
        ImprimirComprobante, retorna False si el operador responde que no quiere
        imprimir.

*******************************************************************************}
procedure TfrmFacturacionUnConvenio.btnFacturarClick(Sender: TObject);
resourcestring
    ERROR_INVOICING     		= 'Error ejecutando el proceso de facturaci�n.';
    ERROR_INVOICE_NOT_GENERATED = 'El comprobante no ha sido generado';
    ERROR_NOITEM_BILLING        = 'Proceso de Facturaci�n no realizado.';
    ERROR_NOITEM_BILLING_MSG    = 'No existen items ni ajuste para facturar.';
    ERROR_PRINTING      		= 'Error al intentar imprimir el comprobante';
	  MSG_NO_INVOICE      		= 'El comprobante no ha sido emitido.';
  	MSG_INVOICING_OK    		= 'Proceso de facturaci�n finalizado con �xito.';
  	MSG_INVOICE_AGAIN   		= 'Desea realizar la facturaci�n para otro convenio? ';
  	MSG_ASK_GENERATE    		= 'Desea generar el comprobante?' + CRLF + CRLF +
    								'N�mero de Convenio: %s' + CRLF +
                 		'Fecha de Corte: %s' + CRLF +
                 		'Fecha de Vencimiento: %s';
  	MSG_ASK_PRINT       		= 'Desea imprimir el comprobante?';
    MSG_ASK_CREATE_INVOICE		= 'Si emite una Nota de Cobro con fecha futura, luego no '#10#13 +
    								'podr� emitir Notas de Cobro con fechas anteriores a �sta.'#10#13 +
                    'Desea emitirla de todos modos ?';
	  MSG_CANCEL_INVOICING		= 'La emisi�n del comprobante ha sido cancelada por el usuario.';
    MSG_ASK_USE_BACKGROUND_IMAGE= 'Incluir Imagen de Fondo?';
    MSG_ERROR_GET_LAST_BILL_DATE = 'Ha ocurrido un error al obtener la �ltima Fecha de Emisi�n de Nota de Cobro.';
    MSG_ERROR_CREATE_BILLING_PROCESS = 'Ha ocurrido un error al crear el Proceso de Facturaci�n.';
    MSG_ERROR_CREATE_INVOICE        = 'Ha ocurrido un error al crear el Comprobante.';
    MSG_ERROR_UPDATE_BILLING_PROCESS = 'Ha ocurrido un error al actualizar la Fecha de Finalizaci�n del Proceso de Facturaci�n.';
    MSG_ERROR_GET_INVOICE_NUMBER = 'Ha ocurrido un error al obtener el N� del Comprobante generado. No se puede imprimir el Comprobante.';
    MSG_ERROR_FECHA_CORTE = 'La fecha de corte m�nima debe ser %s%';
    MSG_ERROR_PARCIAL_PROCESS = 'Ha ocurrido un error al realizar el proceso de Facturaci�n Parcial';
var
    NumeroProcesoFacturacion : integer;
    DescripcionError : string;
    dUltimaFechaEmision : TDateTime;
    i, CantidadMovimientos, CodigoConcepto : Integer; //Revision 3
    CargaInteresesOK    : Integer;
begin
	// Obtiene la �ltima Fecha de Emisi�n
    try
        dUltimaFechaEmision := QueryGetValueDateTime(DMConnections.BaseCAC,
                                    'SELECT dbo.ObtenerUltimaFechaEmisionNotasCobro()');
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_GET_LAST_BILL_DATE, E.Message, Caption, MB_ICONSTOP);
            Exit;
        end;
    end; // except

    // Validamos que esten los datos completos y sean v�lidos.
    if not ValidateCOntrols(
            [peRUTCliente,
            cbConveniosCliente,
            deFechaCorte,
            deFechaEmision,
            deFechaVencimiento,
            deFechaCorte,
            deFechaEmision,
            deFechaEmision,
            deFechaVencimiento,
            deFechaVencimiento,
            deFechaCorte],
            [(trim(peRUTCliente.text) <> '') and (ObtenerCliente.recordCount > 0),
            (cbConveniosCliente.Items.count > 0),
            deFechaCorte.Date <> nulldate,
            deFechaEmision.date <> nulldate,
            deFechaVencimiento.date <> nulldate,
            deFechaCorte.Date <= FFechaCorteMaximaPosible,
            deFechaEmision.date >= deFechaCorte.Date,
            deFechaEmision.date >= dUltimaFechaEmision,
            deFechaVencimiento.Date > deFechaEmision.Date,
            EsFechaHabil(deFechaVencimiento.date),
            deFechaCorte.Date >= FFechaCorteMininaPosible],
            caption,
            [MSG_CLIENTE,
            MSG_COMVENIO,
            MSG_FECHA_CORTE_NO_EXISTE,
            MSG_FECHA_EMISION_NO_EXISTE,
            MSG_FECHA_VENCIMIENTO_NO_EXISTE,
            MSG_FECHA_CORTE_INCORRECTA,
            MSG_FECHA_EMISION_INCORRECTA,
            MSG_FECHA_EMISION_ANTERIOR_A_ULTIMA + FormatDateTime( '(dd/mm/yyyy)', dUltimaFechaEmision),
            MSG_FECHA_VENCIMIENTO_INCORRECTA,
            MSG_FECHA_VENCIMIENTO_NO_HABIL,
            format(MSG_ERROR_FECHA_CORTE , [DateTimeToStr(FFechaCorteMininaPosible)])
            ]) then begin

        Exit;
	end;

	if (deFechaEmision.Date > Date) then begin
    	if (MsgBox(MSG_ASK_CREATE_INVOICE, caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES) then begin
        	deFechaEmision.SetFocus;
			Exit;
        end;
    end;

    // Generamos el comprobante
    screen.Cursor := crHourGlass;
	try
    	FCancelar := MsgBox(Format(MSG_ASK_GENERATE, [cbConveniosCliente.Items[cbConveniosCliente.ItemIndex].Caption,
        				DateToStr(deFechaCorte.date), DateToStr(deFechaVencimiento.date)]),
                        Caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES;

        if not FCancelar then begin
        	try

//				DMConnections.BaseCAC.BeginTrans;

                try
                    spCrearProcesoFacturacion.Parameters.ParamByName('@Operador').Value := UsuarioSistema;
                    spCrearProcesoFacturacion.Parameters.ParamByName('@FacturacionManual').Value := True;
                    spCrearProcesoFacturacion.ExecProc;
                    NumeroProcesoFacturacion := spCrearProcesoFacturacion.Parameters.paramByName('@NumeroProcesoFacturacion').Value;
                except
                    on E: Exception do begin
    					          MsgBoxErr(MSG_ERROR_CREATE_BILLING_PROCESS, E.Message, Caption, MB_ICONSTOP);
                        Exit;
                    end;
                end; // except

                //Agregar los conceptos que se escogieron para facturar dentro de la tabla ConceptosAFacturarFacturadorManual
                //Revision 4
                try
                    CdConceptos.Edit;
                    CantidadMovimientos := CdConceptos.RecordCount;
                    CdConceptos.First;
                    For i:=0 to CantidadMovimientos -1 do begin
                        if (not (cdConceptos.FieldByName('Seleccionado').AsBoolean)) then begin
                            CodigoConcepto := cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                            SPCargarConceptosAFacturarFacturadorManual.Parameters.ParamByName('@NumeroProcesoFacturacion').Value:=NumeroProcesoFacturacion;
                            SPCargarConceptosAFacturarFacturadorManual.Parameters.ParamByName('@CodigoConcepto').Value:=CodigoConcepto;
                            SPCargarConceptosAFacturarFacturadorManual.ExecProc;
                        end;
                        CdConceptos.Next;
                    end;
                except
                    on E: Exception do begin
                        MsgBoxErr(MSG_ERROR_PARCIAL_PROCESS, E.Message, Caption, MB_ICONSTOP);
                        Exit;
                    end;
                end; // except
               //FIN Revision 4

               //Cargo tabla de trabajo para calcular intereses
                CargaInteresesOK := CargarDatosIntereses(strToInt(cbConveniosCliente.Value),deFechaEmision.date, NumeroProcesoFacturacion);
                if CargaInteresesOK < 0 then begin
                    Exit;
                end;

                DescripcionError := EmptyStr;
                try
                    GenerarMovimientosTransitos.Parameters.ParamByname('@CodigoConvenio').Value     := strToInt(cbConveniosCliente.Value);
                    GenerarMovimientosTransitos.Parameters.ParamByname('@ConvenioAnterior').Value   := 0;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@GrupoFacturacion').Value   := 0;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@FechaCorte').Value         := deFechaCorte.date;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@FechaEmision').Value       := deFechaEmision.date;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@FechaVencimiento').Value   := deFechaVencimiento.Date;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@NoGenerarAjusteSencillo').Value := chkFacturarCompleto.Checked;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@NoGenerarIntereses').Value  := chkFacturarSinIntereses.Checked;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@ConveniosProcesados').Value := 0;
                    GenerarMovimientosTransitos.Parameters.ParamByname('@DescripcionError').Value    := '';
                    // Revision 5
                    GenerarMovimientosTransitos.Parameters.ParamByname('@CodigoUsuario').Value := UsuarioSistema;
                    GenerarMovimientosTransitos.ExecProc;
                    DescripcionError := GenerarMovimientosTransitos.Parameters.ParamByname('@DescripcionError').Value;
                except
                    on E: Exception do begin
                        DescripcionError := GenerarMovimientosTransitos.Parameters.ParamByname('@DescripcionError').Value;
    					MsgBoxErr(MSG_ERROR_CREATE_INVOICE, DescripcionError + ' ' + E.Message, Caption, MB_ICONSTOP);
                        Exit;
                    end;
                end; // except

				if (DescripcionError = '') then begin

                    // una vez creado el comprobante sin error
                    //se genera el detalle de consumos de este
                    //desde la tabla DetalleConsumoMovimientos
                    LlenarDetalleConsumoComprobantes(NumeroProcesoFacturacion);

                    try
                        SPActualizarFechaHoraFinProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
                        SPActualizarFechaHoraFinProcesoFacturacion.ExecProc;
                    except
                        on E: Exception do begin
                            MsgBoxErr(MSG_ERROR_UPDATE_BILLING_PROCESS, E.Message, Caption, MB_ICONSTOP);
                            Exit;
                        end;
                    end; // except
//		   		  	DMConnections.BaseCAC.CommitTrans;
                end else begin
//                	DMConnections.BaseCAC.RollbackTrans;
                end;

                try
                    spObtenerComprobanteFacturado.Close;
                    spObtenerComprobanteFacturado.Parameters.ParamByName('@CodigoConvenio').Value 			:= strToInt(cbConveniosCliente.Value);
                    spObtenerComprobanteFacturado.Parameters.ParamByName('@NumeroProcesoFacturacion').Value 	:= NumeroProcesoFacturacion;
                    spObtenerComprobanteFacturado.Open;
                except
                    on E: Exception do begin
    					MsgBoxErr(MSG_ERROR_GET_INVOICE_NUMBER, E.Message, Caption, MB_ICONSTOP);
                        spObtenerComprobanteFacturado.Close;
                        Exit;
                    end;
                end; // except

				if (DescripcionError <> '') then
                	MsgBox(DescripcionError, caption, MB_ICONINFORMATION)
                else if (spObtenerComprobanteFacturado.IsEmpty) then
                	MsgBoxErr(ERROR_NOITEM_BILLING, ERROR_NOITEM_BILLING_MSG, caption, MB_ICONERROR)
                else begin
                   // FCancelar := MsgBox(MSG_ASK_PRINT, caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES;
                   // if not FCancelar then begin
                        // Para imprimir la factura necesito recuperar el Nro de Factura
                        try
                            ImprimirComprobante( spObtenerComprobanteFacturado.FieldByName('TipoComprobante').AsString ,
                                                spObtenerComprobanteFacturado.FieldByName('NumeroComprobante').AsInteger );
                        except
                            on E: Exception do begin
                                MsgBoxErr(ERROR_PRINTING, E.Message, Caption, MB_ICONERROR);
                            end;
                        end; // except
                    //end;

                    MsgBox(MSG_INVOICING_OK, caption, MB_OK + MB_ICONINFORMATION)
                end;
			except
				on e: exception do begin
					screen.Cursor := crDefault;
//                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
					MsgBoxErr(ERROR_INVOICING, e.message, caption, MB_ICONSTOP);
            	end;
            end; // except
        end; // if not FCancelar

        if MsgBox(MSG_INVOICE_AGAIN, caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES then begin
        	self.close;
        end else begin
        	peRutCliente.setFocus;
			deFechaEmision.Date := NowBase(DMConnections.BaseCAC);
			deFechaCorte.Date := FFechaCorteMaximaPosible;
			deFechaVencimiento.Date := deFechaEmision.Date + FDiasVencimiento;
			peRutCliente.Text := '';
      btnFacturar.Enabled := False;
      //Revision 4
      cdConceptos.Edit;
      cdConceptos.EmptyDataSet;
      //FIN Revision 4
        end;
    finally
        screen.Cursor := crDefault;
    end;
end;



{******************************** Function Header ******************************
Function Name: cbConveniosClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la lista de convenios se refrescan
               la lista de items de la factura a realizar.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.cbConveniosClienteChange(Sender: TObject);
resourcestring
    ERROR_DATOS_MEDIO_PAGO = 'Error calculando el tipo de medio de pago';
begin
  //Revision 4
    cdConceptos.Edit;
    cdConceptos.EmptyDataSet;
  //FIN Revision 4
    // Obtenemos la descripci�n del medio de pago
    lMedioPago.Caption := '';
    try
        ObtenerDatosMedioPagoAutomaticoConvenio.close;
        ObtenerDatosMedioPagoAutomaticoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := cbConveniosCliente.value;
        ObtenerDatosMedioPagoAutomaticoConvenio.open;

        if (ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger = TPA_PAT) then
            lMedioPago.Caption := TPA_PAT_DESC
        else
            if (ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger = TPA_PAC) then
                lMedioPago.Caption := TPA_PAC_DESC
            else
                if (ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger = TPA_NINGUNO) then
                    lMedioPago.Caption := TPA_NINGUNO_DESC;
    except
        on e: exception do begin
            msgBoxErr(ERROR_DATOS_MEDIO_PAGO, e.Message, caption, MB_ICONSTOP);
        end;
    end;

 	// Muestra el Domicilio de Facturacion
    MostrarDomicilioFacturacion;
    // Obtenemos el resumen a Facturar para la fecha de corte actual
    ObtenerResumenAFacturar(deFechaCorte.date);

end;

{******************************** Function Header ******************************
Function Name: peRUTClienteButtonClick
Author       : fmalisia
Date Created : 24-11-2004
Description  : Permite buscar un cliente por medio del form FormBuscaClientes.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    application.createForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peRUTCliente.Text :=  F.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
            tmrConsultaTimer(nil);
        end;
    end;
    F.free;
end;


{******************************** Function Header ******************************
Function Name: peRUTClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia el RUT del cliente se cargan todos sus datos asociados
               (Lista de convenios, nombre, domicilio y detalle de la factura a realizar).
Parameters   : Sender: TObject
Return Value : None

Revision 1:
Author: Fsandi
Date: 31-05-2007
Description: Se limpian los check de ajuste e intereses cuando se cambia el rut
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.peRUTClienteChange(Sender: TObject);
begin
  //Revision 4
  cdConceptos.Edit;
  cdConceptos.EmptyDataSet;
  chkFacturarCompleto.Checked := False;     //revision 1
  chkFacturarSinIntereses.Checked := False;  //revision 1

  //FIN Revision 4
	if (activeControl <> sender) then Exit;

	tmrConsulta.Enabled := False;
	tmrConsulta.Enabled := True;
end;

//Reviison 4
procedure TfrmFacturacionUnConvenio.PopFacturacionManualPopup(Sender: TObject);
begin
    CdConceptos.Edit;
    if CdConceptos.RecordCount > 0 then begin
        MnuSeleccionarTodo.Enabled := True;
        MnuDeSeleccionarTodo.Enabled := True;
    end else begin
        MnuSeleccionarTodo.Enabled := False;
        MnuDeSeleccionarTodo.Enabled := False;
    end;
end;
//Fin Revision 4

{******************************** Function Header ******************************
Function Name: deFechaCorteExit
Author       : fmalisia
Date Created : 24-11-2004
Description  : Al dejar el control de fecha de corte se valida si es correcta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.deFechaCorteExit(Sender: TObject);
begin
	if deFechaCorte.Date = nulldate then begin
		MsgBoxBalloon(MSG_FECHA_CORTE_NO_EXISTE, caption, MB_ICONSTOP, deFechaCorte);
        exit;
	end;
	if deFechaCorte.Date > FFechaCorteMaximaPosible then begin
		MsgBoxBalloon(MSG_FECHA_CORTE_INCORRECTA, caption, MB_ICONSTOP, deFechaCorte);
	end;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author :
Date Created :
Description :
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None

Revision : 1
    Author : ggomez
    Date : 30/11/2006
    Description : Agregu� la liberaci�n de la lista de convenios dados de baja.
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if FListaConveniosBaja <> Nil then begin
        FreeAndNil(FListaConveniosBaja);
    end;

	Action := caFree;
  CdConceptos.Close;
end;

{******************************** Function Header ******************************
Function Name: deFechaVencimientoExit
Author       : fmalisia
Date Created : 24-11-2004
Description  : Al dejar el control de fecha de vencimiento se valida que sea correcta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.deFechaVencimientoExit(
  Sender: TObject);
begin
	if deFechaVencimiento.Date = nulldate then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_NO_EXISTE, caption, MB_ICONSTOP, deFechaVencimiento);
        exit;
	end;
	if deFechaCorte.Date > deFechaVencimiento.Date then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_INCORRECTA, caption, MB_ICONSTOP, deFechaVencimiento);
        exit;
	end;
	if not EsFechaHabil(deFechaVencimiento.date) then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_NO_HABIL, caption, MB_ICONSTOP, deFechaVencimiento);
	end;
end;

{******************************** Function Header ******************************
Function Name: deFechaEmisionChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la fecha de emisi�n se actualiza la de vencimiento en funci�n de esta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.deFechaEmisionChange(Sender: TObject);
begin
    deFechaVencimiento.Date := deFechaEmision.Date + FDiasVencimiento;
end;

{******************************** Function Header ******************************
Function Name: deFechaEmisionExit
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando se dejka el control de fecha de emisi�n se valida si es correcta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.deFechaEmisionExit(Sender: TObject);
begin
	if deFechaEmision.Date = nulldate then begin
		MsgBoxBalloon(MSG_FECHA_EMISION_NO_EXISTE, caption, MB_ICONSTOP, deFechaEmision);
        exit;
	end;
	if deFechaEmision.Date < deFechaCorte.Date then begin
		MsgBoxBalloon(MSG_FECHA_EMISION_INCORRECTA, caption, MB_ICONSTOP, deFechaEmision);
        exit;
	end;
    {if not EsFechaHabil(deFechaEmision.date) then begin
		MsgBoxBalloon(MSG_FECHA_EMISION_NO_HABIL, caption, MB_ICONSTOP, deFechaEmision);
	end;}
end;

{******************************** Function Header ******************************
Function Name: deFechaCorteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la fecha de corte se actualizan los items a facturar.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.deFechaCorteChange(Sender: TObject);
var
    fecha: TDateTime;
begin
    Fecha := deFechaCorte.date;
    ObtenerResumenAFacturar(Fecha);
end;

procedure TfrmFacturacionUnConvenio.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;

{******************************** Function Header ******************************
Function Name: ObtenerResumenAFacturar
Author       : fmalisia
Date Created : 24-11-2004
Description  : Obtiene el detalle de la factura resultado.
Parameters   : Fecha: TDateTime
Return Value : None
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.ObtenerResumenAFacturar(Fecha: TDateTime);
resourcestring
    ERROR_INFORME_FACTURA = 'Error obteniendo el resumen a facturar.';
begin
    try
        ObtenerUltimosConsumos.Close;
        ObtenerUltimosConsumos.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConveniosCliente.Value <> null, cbConveniosCliente.Value, 0);
        ObtenerUltimosConsumos.Parameters.ParamByName('@IndiceVehiculo').Value := null;
        ObtenerUltimosConsumos.Parameters.ParamByName('@FechaCorte').Value := iif((Fecha = nulldate) or (year(fecha) < 1970) or (year(fecha) > 2079), null, fecha);
        ObtenerUltimosConsumos.Open;
  //Revision 4
        CargarDataSet;
  //Fin de Revision 4
        btnFacturar.Enabled := ValidarConsumos;//not ObtenerUltimosConsumos.IsEmpty;
    except
        on e: Exception do begin
            msgBoxErr(ERROR_INFORME_FACTURA, e.Message, caption, MB_ICONSTOP);
        end;
    end;
end;


procedure TfrmFacturacionUnConvenio.ClearCaptions;
begin
    lNombreCompleto.Caption         := '';
    lMedioPago.Caption              := '';
    lDomicilio.Caption              := '';
    lComuna.Caption                 := '';
    lRegion.Caption                 := '';
    lDomicilioFacturacion.Caption   := '';
    lComunaFacturacion.Caption      := '';
    lRegionFacturacion.Caption      := '';
end;

//Revision 4
procedure TfrmFacturacionUnConvenio.CargarDataset;
begin
    if not cdconceptos.Active then cdConceptos.CreateDataSet;
    cdConceptos.Open;
    cdConceptos.Edit;
    cdConceptos.EmptyDataSet;
    while not ObtenerUltimosConsumos.Eof do begin
        // Insertamos nuestro registro
        cdConceptos.AppendRecord([True, ObtenerUltimosConsumos.FieldByName('CONCEPTO').AsString,
          ObtenerUltimosConsumos.FieldByName('DescImporte').AsString, ObtenerUltimosConsumos.FieldByName('CODIGOCONCEPTO').AsInteger]);
        ObtenerUltimosConsumos.Next;
    end;
end;

{******************************** Function Header ******************************
Function Name: DBListEx1DblClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None
Revision :1
    Author : vpaszkowicz
    Date : 08/07/2008
    Description : Le quito la posiblitdad de desfiltrar cualquier concepto que
    sea de peajes.
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.DBListEx1DblClick(Sender: TObject);
var
    CodigoConcepto : Integer;

    //Dado que los peajes se manejan por duo (actuales y anteriores),
    //Este procedimiento marca el otro concepto de peaje cuando hay uno marcado
    procedure MarcarDuoPeajes(Marcado : Integer);
    var
        i, CantidadRegistros, RegAMarcar : integer;
    begin
        RegAMarcar := 0;
        if Marcado = CodigoPeajes then RegAMarcar := CodigoPeajesAnteriores;
        if Marcado = CodigoPeajesAnteriores then RegAMarcar := CodigoPeajes;
        CantidadRegistros := CdConceptos.RecordCount;
        CdConceptos.First;
        For i := 0 to CantidadRegistros do begin
            if (cdConceptos.FieldByName('CodigoConcepto').AsInteger = RegAMarcar) then begin
                CdConceptos.Edit;
                CdConceptos.FieldByName('Seleccionado').AsBoolean := not (cdConceptos.FieldByName('Seleccionado').AsBoolean);
                Break;
            end;
            CdConceptos.Next;
        end;
    end;

begin
    //cdConceptos.Edit;
    CodigoConcepto := cdConceptos.FieldByName('CodigoConcepto').AsInteger;
    if ((cdConceptos.RecordCount > 0) and not (CodigoConcepto in [CONCEPTO_PEAJES_PERIODO, CONCEPTO_PEAJES_OTROS_PERIODOS])) then begin
        cdConceptos.Edit;
        cdConceptos.FieldByName('Seleccionado').AsBoolean := not (cdConceptos.FieldByName('Seleccionado').AsBoolean);
        //Verificamos, si el item que se esta marcando es un peaje anterior o un peaje del periodo
        //entonces tambien se marca el otro, usando el procedimiento MarcarDuoPeajes

        //Comentado en Rev.1
        {if ((CodigoConcepto = CodigoPeajes) or
         (CodigoConcepto = CodigoPeajesAnteriores)) then begin
            MarcarDuoPeajes (CodigoConcepto);
        end;}
        //cdConceptos.Edit;
        cdConceptos.Post;
    end;
end;


procedure TfrmFacturacionUnConvenio.DBListEx1DrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    cdConceptos.Edit;
    if cdConceptos.RecordCount>0 then begin
        if Column.FieldName = 'Seleccionado' then begin
              if cdConceptos.FieldByName('Seleccionado').AsBoolean = True then begin
                  ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
              end else if cdConceptos.FieldByName('Seleccionado').AsBoolean = False then begin
                  ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
              end;
              DefaultDraw := False;
        end;
    end else begin
        ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
    end;
end;
procedure TfrmFacturacionUnConvenio.DBListEx1KeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key = #32  then  DBListEx1DblClick(Self);
end;

//FIN Revision 4

{-----------------------------------------------------------------------------
  Function Name: ImprimirComprobante
  Author:    ndonadio
  Date Created: 21/12/2004
  Description: Llama al report para imprimir una factura.
  Parameters: TipoComprobante:string; NroComprobante: int64;
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TfrmFacturacionUnConvenio.ImprimirComprobante(TipoComprobante:string;NroComprobante: int64): Boolean;
var
    //Qry:    TADOQuery;
    f:      TFrmReporteFactura;
    sError: string;
begin
    Result := False;
    Application.CreateForm(TFrmReporteFactura,f);
    try
        if not f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, sError, (FImprimirFondoNK = 1)) then begin
            ShowMessage( sError );
        	Exit;
        end;
        Result := f.Ejecutar;
    	if Result then begin
            // Hago que se marque como Impreso el Comprobante...
            // Se reemplazo el uso de un TADOQuery por la llamada a un SP
            spActualizarFechaImpresionComprobantes.Parameters.ParamByname('@TipoComprobante').Value := TipoComprobante;
            spActualizarFechaImpresionComprobantes.Parameters.ParamByname('@NumeroComprobante').Value := NroComprobante;
            spActualizarFechaImpresionComprobantes.Parameters.ParamByname('@CodigoUsuario').Value := UsuarioSistema;
            spActualizarFechaImpresionComprobantes.ExecProc;

            ImprimirFacturacionDetallada(TipoComprobante, NroComprobante);
        end;
	finally
        f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ImprimirFacturacionDetallada
  Author:    flamas
  Date Created: 29/03/2005
  Description: Imprime la facturaci�n Detallada si corresponde
  Parameters: TipoComprobante:string; NroComprobante: int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TfrmFacturacionUnConvenio.ImprimirFacturacionDetallada(TipoComprobante:string; NroComprobante: int64): Boolean;
resourcestring
	MSG_ERROR_GETTING_DETAIL 	= 'Error obteniendo datos de Facturaci�n Detallada';
var
    f: TformReporteFacturacionDetallada;
    Error:String;
begin
    result := True;
	with spObtenerDatosFacturacionDetallada, Parameters do begin
        Close;
    	ParamByName('@TipoComprobante').Value := TipoComprobante;
        ParamByName('@NumeroComprobante').Value := NroComprobante;
        try
        	Open;
            if (FieldByName('ImpresionDetallada').AsBoolean) then begin
                // Lanza el reporte de Facturacion detallada...
                Screen.Cursor := crHourGlass;
                Application.CreateForm(TformreportefacturacionDetallada,f);
                try
                    if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, Error, True) then
                        result := f.Ejecutar;
                finally
                    f.Release;
                    Screen.Cursor := crDefault;
                end;
            end;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_DETAIL, e.Message, Caption, MB_ICONSTOP);
            	result := False;
            end;
        end;
	end;
end;

procedure TfrmFacturacionUnConvenio.NumeroConvenioChange(Sender: TObject);
var
	Documento : String;
begin
	if (activeControl = sender) then begin
		// voy buscando el cliente que tenga este convenio, si existe pongo su RUT en el buscador
		Documento := Trim( QueryGetValue (DMConnections.BaseCAC, 'SELECT ISNULL (NumeroDocumento, '''') FROM Personas WHERE CodigoPersona = (SELECT CodigoCliente FROM Convenio WHERE NumeroConvenio = ' + QuotedStr(NumeroConvenio.Text) + ')'));

		if Documento <> '' then peRUTCliente.setFocus;
		peRUTCliente.Text := Documento
	end
end;

{-----------------------------------------------------------------------------
  Function Name: tmrConsultaTimer
  Author:    flamas
  Date Created: 08/03/2005
  Description: Timer que ejecuta la consulta
  Parameters: Sender: TObject
  Return Value: None

  Revision : 1
      Author : ggomez
      Date : 30/11/2006
      Description : Modifiqu� para que en la lista de convenios dados de baja
        se almacene el C�digo de Convenio en lugar de la posici�n del convenio
        dentro del combo.
-----------------------------------------------------------------------------}
procedure TfrmFacturacionUnConvenio.tmrConsultaTimer(Sender: TObject);
var
    DescripcionCalle: AnsiString;
    sNumeroConvenio: string;
    nIndex : integer;
begin
    //lista para guardar los valores (�ndices) de los convenios dados de baja
    if FListaConveniosBaja <> Nil then begin
        FreeAndNil(FListaConveniosBaja);
    end;
    FListaConveniosBaja := TStringList.Create;

    try
		Screen.Cursor := crHourglass;
		tmrConsulta.Enabled := False;
    	sNumeroConvenio := trim(NumeroConvenio.Text);
		ClearCaptions;
		cbConveniosCliente.Items.clear;

		NumeroConvenio.Text := '';

		ObtenerUltimosConsumos.close;
        if (length(peRUTCliente.Text) < 7) then begin
            Exit;
        end;

        // Obtenemos el cliente seleccionado
        with obtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value := NULL;
            Parameters.ParamByName('@CodigoDocumento').value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').value := trim(PadL(peRUTCLiente.Text, 9, '0'));
            open;
        end;

        // Obtenemos la lista de convenios para el cliente seleccionado
        nIndex := 0;
        With spObtenerConveniosCliente do begin
            Parameters.ParamByName('@CodigoCliente').Value := obtenerCliente.fieldbyname('CodigoCliente').AsInteger;
            open;
            First;
            while not spObtenerConveniosCliente.Eof do begin
                cbConveniosCliente.Items.Add(spObtenerConveniosCliente.fieldbyname('NumeroConvenioFormateado').AsString,
                  trim(spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString));

                // Si el convenio esta dado de baja, almacenarlo en la lista de
                // convenios dados de baja.
                if spObtenerConveniosCliente.FieldByName('CodigoEstadoConvenio').Value = ESTADO_CONVENIO_BAJA then begin
                    FListaConveniosBaja.Add(Trim(spObtenerConveniosCliente.FieldByName('CodigoConvenio').AsString));
                end;

                if (trim(spObtenerConveniosCliente.fieldbyname('NumeroConvenio').AsString) = sNumeroConvenio) then nIndex := cbConveniosCliente.Items.Count - 1;

                spObtenerConveniosCliente.Next;
            end;
            close
        end;

        cbConveniosCliente.ItemIndex := nIndex;
        cbConveniosClienteChange(cbConveniosCliente);

        with ObtenerCliente do begin
            // Mostramos el nombre completo del cliente
            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString), Trim(FieldByName('Nombre').asString))
            else
                lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString));

            // Mostramos el domicilio Personal
            DescripcionCalle := ObtenerDescripCalle(DMConnections.BaseCAC,
              PAIS_CHILE,
                  trim(ObtenerCliente.FieldByName('CodigoRegion').asString),
                  trim(ObtenerCliente.FieldByName('CodigoComuna').asString),
                  ObtenerCliente.FieldByName('CodigoCalle').asInteger,
                  trim(ObtenerCliente.FieldByName('CalleDesnormalizada').asString));
            lDomicilio.Caption := ArmarDomicilioSimple(DMConnections.BaseCAC, DescripcionCalle,
              Trim(ObtenerCliente.FieldByName('Numero').asString), 0, '',
              Trim(ObtenerCliente.FieldByName('detalle').asString));
            lComuna.Caption := BuscarDescripcionComuna(DMConnections.BaseCAC,
              PAIS_CHILE,
              trim(ObtenerCliente.FieldByName('Codigoregion').asString),
              trim(ObtenerCliente.FieldByName('CodigoComuna').asString));
            lRegion.Caption := BuscarDescripcionRegion(DMConnections.BaseCAC,
              PAIS_CHILE,
              trim(ObtenerCliente.FieldByName('CodigoRegion').asString));
        end;
    finally
		Screen.Cursor := crDefault;
    end;
end;

//Revision 4
procedure TfrmFacturacionUnConvenio.MnuDeSeleccionarTodoClick(Sender: TObject);
var
    i, CantidadMovimientos : Integer;
begin
    CantidadMovimientos := CdConceptos.RecordCount;
    CdConceptos.First;
    For i:=0 to CantidadMovimientos -1 do begin
        if not cdConceptos.FieldByName('CodigoConcepto').asInteger in [CONCEPTO_PEAJES_PERIODO, CONCEPTO_PEAJES_OTROS_PERIODOS] then begin
            CdConceptos.Edit;
            cdConceptos.FieldByName('Seleccionado').AsBoolean := False;
            CdConceptos.Next;
        end;
    end;
end;

procedure TfrmFacturacionUnConvenio.MnuSeleccionarTodoClick(Sender: TObject);
var
i, CantidadMovimientos : Integer;

begin

    CantidadMovimientos := CdConceptos.RecordCount;
    CdConceptos.First;
    For i:=0 to CantidadMovimientos -1 do begin
        CdConceptos.Edit;
        cdConceptos.FieldByName('Seleccionado').AsBoolean := True;
        CdConceptos.Next;
    end;
end;

//FIN Revision 4
procedure TfrmFacturacionUnConvenio.MostrarDomicilioFacturacion;
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := cbConveniosCliente.value;
        try
        	ExecProc;
    		lDomicilioFacturacion.Caption	:= ParamByName('@Domicilio').Value;
 			lComunaFacturacion.Caption		:= ParamByName('@Comuna').Value;
            lRegionFacturacion.Caption 		:= ParamByName('@Region').Value;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

{*******************************************************************************
Revision : 1
    Author : nefernandez
    Date : 23/08/2007
    Description : SS 548: Cambio del AsInteger a AsVariant para el campo IMPORTE
*******************************************************************************}
function TfrmFacturacionUnConvenio.ValidarConsumos: Boolean;
    function IncluirEnNotaCobro(CodigoConcepto: integer):Boolean;
    var
        Q: AnsiString;
    begin
        Q := QueryGetValue(DMCOnnections.BaseCAC,
                   Format( ' SELECT IncluirNotaCobro FROM ConceptosMovimiento WITH(NOLOCK) '+
                           ' WHERE CodigoConcepto = %d ',[CodigoConcepto]));
        Result := (TRIM(Q) = 'True')
    end;
var
    Suma: int64;
begin
    Result := False;
//    if ObtenerUltimosConsumos.IsEmpty then Exit;
    if cbConveniosCliente.Text = EmptyStr then Exit; 
    with ObtenerUltimosConsumos do begin
        DisableControls;
        First;
        Suma := 0;
        while NOT Eof do begin
            if IncluirEnNotaCobro(FieldByName('CodigoConcepto').AsInteger) then
                        Suma := Suma + FieldByName('IMPORTE').AsVariant;
            Next;
        end;
        EnableControls;
    end;
    Result := (Suma >= 0);   
end;

{******************************** Function Header ******************************
Function Name: cbConveniosClienteDrawItem
Author :
Date Created :
Description :
Parameters : Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState
Return Value : None

Revision : 1
    Author : ggomez
    Date : 30/11/2006
    Description :
        - Modifiqu� para que use la propiedad privada FListaConveniosBaja.
        - Modifiqu� para que no use la posici�n del item para saber si un convenio
        est� dado de baja, en su lugar ahora se usa el Codigo de Convenio para
        saber si est� entre los convenios dados de baja.
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.cbConveniosClienteDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
    CodigoConvenio: AnsiString;
begin
    CodigoConvenio := cbConveniosCliente.Items[Index].Value;
    // Si el Convenio est� en la lista de los convenios dados de baja, entonces
    // colorearlo de manera diferente a los que NO est�n de baja.
    if FListaConveniosBaja.IndexOf(CodigoConvenio) <> -1 then begin
        ItemCBColor(Control, Index, Rect, cl3DLight, clRed)
    end else begin
        ItemCBColor(Control, Index, Rect, $00FAEBDE, clBlack);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure Name: ItemCBColor
  Author:    ddiaz
  Date Created: 18/09/2006
  Description: Pinta en el combobox los convenios con distinto solor seg�n el estado (Vigrnte o Baja)
                SS 0341
-----------------------------------------------------------------------------}
procedure TfrmFacturacionUnConvenio.ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor);
begin
    with (cmbBox as TVariantComboBox) do begin
        Canvas.Brush.color := ColorFondo;
        Canvas.FillRect(R);
        Canvas.Font.Color := ColorTexto;
        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption);
   end;
end;

{*******************************************************************************
    Procedure Name: CargarDatosIntereses
    Author : rharris
    Date : 24/12/2008
    Description : (SS 627) se calculan los intereses desde tablas de trabajo
*******************************************************************************}
function TfrmFacturacionUnConvenio.CargarDatosIntereses;
begin
	try
		spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := NULL;
		spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@FechaEmision').Value := FechaEmision;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
		spGenerarTablaTrabajoInteresesPorFacturar.ExecProc;
		Result := 1;
	except
		on e: Exception do begin
			MsgBox(e.Message, 'Error', MB_ICONSTOP);
			Result := -1;
		end;
	end;
end;

{*******************************************************************************
    Procedure Name: LlenarDetalleConsumoComprobantes
    Author : rharris
    Date : 24/12/2008
    Description : (SS 627) se calculan los detalle de consumo del comprobante
    desde los movimientos prefacturados
*******************************************************************************}
procedure TfrmFacturacionUnConvenio.LlenarDetalleConsumoComprobantes;
begin
	try
		spLlenarDetalleConsumoComprobantes.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
		spLlenarDetalleConsumoComprobantes.ExecProc;
	except
		on e: Exception do begin
			MsgBox(e.Message, 'Error', MB_ICONSTOP);
		end;
	end;
end;

end.


