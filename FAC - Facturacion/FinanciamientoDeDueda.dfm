object FrmFinanciamientoDeuda: TFrmFinanciamientoDeuda
  Left = 144
  Top = 148
  Anchors = []
  BorderStyle = bsDialog
  Caption = 'Financiamiento de Deuda'
  ClientHeight = 512
  ClientWidth = 858
  Color = clBtnFace
  Constraints.MinHeight = 505
  Constraints.MinWidth = 860
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    858
    512)
  PixelsPerInch = 96
  TextHeight = 13
  object btnCancelar: TDPSButton
    Left = 754
    Top = 483
    Width = 92
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
  end
  object btnAceptar: TDPSButton
    Left = 656
    Top = 483
    Width = 92
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object PageControl2: TPageControl
    Left = 4
    Top = 201
    Width = 851
    Height = 277
    ActivePage = TabSheet6
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabIndex = 1
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Financiamiento'
      ImageIndex = 2
      DesignSize = (
        843
        249)
      object Label2: TLabel
        Left = 8
        Top = 120
        Width = 120
        Height = 13
        Caption = 'Financiamientos Actuales'
      end
      object dbgFinanciamiento: TDBListEx
        Left = 7
        Top = 1
        Width = 829
        Height = 113
        Anchors = [akLeft, akTop, akRight]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 85
            Header.Caption = 'C'#243'digo Cuenta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CodigoCuenta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 253
            Header.Caption = 'Cuenta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriCuenta'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 113
            Header.Caption = 'Monto Adeudado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriImporte'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Entrega Inicial'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'EntregaInicial'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Deuda Convenida'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DeudaConvenida'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 71
            Header.Caption = 'Inter'#233's'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Interes'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 95
            Header.Caption = 'Importe Cuota'
            Header.Font.Charset = ANSI_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'ImporteCuota'
          end>
        DataSource = dsDeudaPorCuenta
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
      object dbgFinanciamientosActuales: TDBListEx
        Left = 7
        Top = 135
        Width = 829
        Height = 111
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaAlta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 90
            Header.Caption = 'C'#243'digo Cuenta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CodigoCuenta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 173
            Header.Caption = 'Cuenta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriCuentas'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 105
            Header.Caption = 'Importe Financiado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriImporteAFinanciar'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'Pie Entregado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriPieEntregado'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 70
            Header.Caption = 'Inter'#233's'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Interes'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 74
            Header.Caption = 'Vigencia'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaVigencia'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 52
            Header.Caption = 'Cuotas'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CantidadCuotas'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 90
            Header.Caption = 'Importe Cuota'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriImporteCuota'
          end>
        DataSource = dsFinanciamientosVigentes
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Comprobantes Vencidos'
      DesignSize = (
        843
        249)
      object dbgComprobantes: TDBListEx
        Left = 4
        Top = 2
        Width = 835
        Height = 244
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 112
            Header.Caption = 'Comprobante'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriComprobanteNumero'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 63
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaHora'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 78
            Header.Caption = 'Fecha Venc.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Vencimiento'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 90
            Header.Caption = 'Importe'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriImporte'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 128
            Header.Caption = 'Comprobante Ajustado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriComprobanteAjustado'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 255
            Header.Caption = 'D'#233'bito'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescripcionDebito'
          end>
        DataSource = dsFacturas
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
    end
  end
  object gbFinanciamiento: TGroupBox
    Left = 4
    Top = 78
    Width = 851
    Height = 122
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Configuraci'#243'n del Financiamiento '
    TabOrder = 0
    DesignSize = (
      851
      122)
    object Bevel1: TBevel
      Left = 8
      Top = 90
      Width = 835
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      Shape = bsTopLine
    end
    object Label19: TLabel
      Left = 8
      Top = 25
      Width = 115
      Height = 13
      Caption = 'Tasa de Interes (%):'
      FocusControl = nePlazoPrimerCuota
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 432
      Top = 25
      Width = 116
      Height = 13
      Caption = 'Cantidad de Cuotas:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label22: TLabel
      Left = 640
      Top = 25
      Width = 112
      Height = 13
      Caption = 'Plazo Primer Cuota:'
      FocusControl = neEntregaInicial
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel
      Left = 232
      Top = 25
      Width = 87
      Height = 13
      Caption = 'Entrega Inicial:'
      FocusControl = neInteres
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 9
      Top = 99
      Width = 88
      Height = 13
      Caption = 'Total de la Deuda:'
    end
    object lDeudaTotal: TLabel
      Left = 99
      Top = 99
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = '                     0'
    end
    object Bevel2: TBevel
      Left = 8
      Top = 50
      Width = 835
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      Shape = bsTopLine
    end
    object Label4: TLabel
      Left = 188
      Top = 99
      Width = 97
      Height = 13
      Caption = 'Recargo por Inter'#233's:'
    end
    object lImporteInteres: TLabel
      Left = 287
      Top = 99
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = '                        0'
    end
    object Label3: TLabel
      Left = 385
      Top = 99
      Width = 114
      Height = 13
      Caption = 'Importe Total por Cuota:'
    end
    object lImporteCuota: TLabel
      Left = 500
      Top = 99
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = '                       0'
    end
    object neInteres: TNumericEdit
      Left = 128
      Top = 21
      Width = 89
      Height = 21
      Color = 16444382
      MaxLength = 6
      TabOrder = 0
      OnChange = neInteresChange
      Decimals = 2
    end
    object neEntregaInicial: TNumericEdit
      Left = 323
      Top = 21
      Width = 97
      Height = 21
      Color = 16444382
      MaxLength = 12
      TabOrder = 1
      Decimals = 2
    end
    object nePlazoPrimerCuota: TNumericEdit
      Left = 758
      Top = 21
      Width = 65
      Height = 21
      Color = 16444382
      MaxLength = 6
      TabOrder = 3
      Decimals = 0
    end
    object neCantidadCuotas: TNumericEdit
      Left = 552
      Top = 21
      Width = 73
      Height = 21
      Color = 16444382
      MaxLength = 6
      TabOrder = 2
      Decimals = 0
    end
    object cbReemplazarFinanciamientos: TCheckBox
      Left = 16
      Top = 61
      Width = 225
      Height = 17
      Caption = 'Reemplazar los Financiamientos Vigentes'
      TabOrder = 4
      OnClick = cbReemplazarFinanciamientosClick
    end
    object btnCalcular: TDPSButton
      Left = 667
      Top = 59
      Width = 168
      Anchors = [akRight, akBottom]
      Caption = '&Calcular Financiamiento'
      TabOrder = 5
      OnClick = btnCalcularClick
    end
  end
  object gbPlanFinanciamiento: TGroupBox
    Left = 5
    Top = 0
    Width = 850
    Height = 73
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Plan de Financiamiento'
    TabOrder = 1
    object Label18: TLabel
      Left = 8
      Top = 26
      Width = 113
      Height = 13
      Caption = 'Plan de Financiamiento:'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label29: TLabel
      Left = 10
      Top = 52
      Width = 94
      Height = 13
      Caption = 'Tasa de Interes (%):'
      FocusControl = nePlazoPrimerCuota
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 107
      Top = 52
      Width = 65
      Height = 13
      DataField = 'DescriInteres'
      DataSource = saPlanesFinanciamiento
    end
    object Label25: TLabel
      Left = 178
      Top = 52
      Width = 56
      Height = 13
      Caption = 'Pie M'#237'nimo:'
    end
    object DBText1: TDBText
      Left = 241
      Top = 52
      Width = 65
      Height = 13
      DataField = 'DescriPieMinimo'
      DataSource = saPlanesFinanciamiento
    end
    object Label26: TLabel
      Left = 314
      Top = 52
      Width = 96
      Height = 13
      Caption = 'Cantidad de Cuotas:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 416
      Top = 52
      Width = 73
      Height = 13
      DataField = 'CantidadCuotas'
      DataSource = saPlanesFinanciamiento
    end
    object Label27: TLabel
      Left = 506
      Top = 52
      Width = 127
      Height = 13
      Caption = 'Plazo para la Primer Cuota:'
      FocusControl = neEntregaInicial
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText4: TDBText
      Left = 636
      Top = 52
      Width = 65
      Height = 13
      DataField = 'PlazoPrimerCuota'
      DataSource = saPlanesFinanciamiento
    end
    object Label28: TLabel
      Left = 714
      Top = 52
      Width = 56
      Height = 13
      Caption = 'Frecuencia:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText5: TDBText
      Left = 775
      Top = 52
      Width = 58
      Height = 13
      DataField = 'DescriFactorFrecuencia'
      DataSource = saPlanesFinanciamiento
    end
    object cbPlanesFinanciamiento: TComboBox
      Left = 152
      Top = 22
      Width = 345
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbPlanesFinanciamientoChange
    end
  end
  object dsFacturas: TDataSource
    DataSet = ObtenerListaComprobantes
    Left = 232
    Top = 200
  end
  object dsDeudaPorCuenta: TDataSource
    DataSet = ClientDataSet
    Left = 112
    Top = 288
  end
  object ObtenerDeudaCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDeudaCuenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConsiderarFinanciamientos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 176
    Top = 288
  end
  object ObtenerListaComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListaComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Vencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 264
    Top = 200
  end
  object ObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 240
    Top = 16
  end
  object PlanesFinanciamiento: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoFinanciamiento'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  CantidadCuotas,'
      
        '  DescriPieMinimo = dbo.FloatToStr(CAST(PieMinimo AS DECIMAL) / ' +
        '100),'
      '  PieMinimo = CAST(PieMinimo AS DECIMAL(12,2)) / 100,'
      '  PlazoPrimerCuota,'
      '  Interes,'
      '  DescriInteres = CAST(Interes AS DECIMAL(12,2)),'
      '  FactorFrecuencia,'
      
        '  DescriFactorFrecuencia = RTRIM(Dbo.ObtenerDescripcionFactorFre' +
        'cuencia(FactorFrecuencia))'
      'FROM'
      '  PlanesFinanciamiento'
      'WHERE'
      '  CodigoFinanciamiento = :CodigoFinanciamiento')
    Left = 744
    Top = 56
  end
  object saPlanesFinanciamiento: TDataSource
    DataSet = PlanesFinanciamiento
    Left = 712
    Top = 56
  end
  object ClientDataSet: TClientDataSet
    Aggregates = <>
    DisableStringTrim = True
    FieldDefs = <
      item
        Name = 'CodigoCuenta'
        DataType = ftInteger
      end
      item
        Name = 'DescriCuenta'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescriImporte'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Importe'
        DataType = ftFloat
      end
      item
        Name = 'EntregaInicial'
        DataType = ftFloat
      end
      item
        Name = 'DeudaConvenida'
        DataType = ftFloat
      end
      item
        Name = 'Interes'
        DataType = ftFloat
      end
      item
        Name = 'ImporteCuota'
        DataType = ftFloat
      end>
    IndexDefs = <>
    IndexFieldNames = 'CodigoCuenta'
    Params = <>
    StoreDefs = True
    Left = 144
    Top = 288
  end
  object GenerarFinanciamiento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarFinanciamiento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@EntregaInicial'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@CantidadCuotas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Interes'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@FactorFrecuencia'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@PlazoPrimerCuota'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ReemplazarFinanciamientos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 288
  end
  object ObtenerFinanciamientos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFinanciamientos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstadoFinanciamiento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 144
    Top = 400
  end
  object dsFinanciamientosVigentes: TDataSource
    DataSet = ObtenerFinanciamientos
    Left = 112
    Top = 400
  end
  object PagarComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PagarComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 440
    Top = 288
  end
  object CalcularCuotaFinanciamiento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CalcularCuotaFinanciamiento'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadCuotas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FrecuenciaCuotas'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@ImporteAFinanciar'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@InteresAnual'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@ImportePorInteres'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@ImporteCuota'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 368
    Top = 288
  end
  object AgregarPagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'AgregarPagoComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@FormaPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoEntidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroCuenta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroCheque'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Vencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Monto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@Cuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Cupon'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Autorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 471
    Top = 288
  end
end
