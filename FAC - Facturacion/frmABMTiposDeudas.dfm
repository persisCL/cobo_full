object ABMTiposDeudasForm: TABMTiposDeudasForm
  Left = 0
  Top = 0
  Caption = 'Mantenimiento de Tipos de Deudas'
  ClientHeight = 339
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object tbTiposDeudas: TAbmToolbar
    Left = 0
    Top = 0
    Width = 560
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar, btImprimir]
    OnClose = tbTiposDeudasClose
    ExplicitTop = -6
  end
  object lbTiposDeudas: TAbmList
    Left = 0
    Top = 35
    Width = 560
    Height = 133
    TabStop = True
    TabOrder = 1
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'45'#0'C'#243'digo'
      #0'90'#0'Tipo de Deuda'
      #0'62'#0'Descripci'#243'n')
    Table = spObtenerTiposDeudas
    Style = lbOwnerDrawFixed
    OnClick = lbTiposDeudasClick
    OnDrawItem = lbTiposDeudasDrawItem
    OnRefresh = lbTiposDeudasRefresh
    OnInsert = lbTiposDeudasInsert
    OnDelete = lbTiposDeudasDelete
    OnEdit = lbTiposDeudasEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = tbTiposDeudas
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 298
    Width = 560
    Height = 41
    Align = alBottom
    TabOrder = 2
    object Notebook: TNotebook
      Left = 362
      Top = 1
      Width = 197
      Height = 39
      Align = alRight
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btnSalir: TButton
          Left = 109
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = 'Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btnAceptar: TButton
          Left = 26
          Top = 6
          Width = 79
          Height = 26
          Caption = 'Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
  end
  object pnlTiposDeudas: TPanel
    Left = 0
    Top = 168
    Width = 560
    Height = 130
    Align = alBottom
    Enabled = False
    TabOrder = 3
    object lblCodigo: TLabel
      Left = 46
      Top = 19
      Width = 41
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTipoDeuda: TLabel
      Left = 46
      Top = 59
      Width = 83
      Height = 13
      Caption = 'Tipo de Deuda:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDescripcion: TLabel
      Left = 46
      Top = 99
      Width = 68
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtTipoDeuda: TEdit
      Left = 143
      Top = 56
      Width = 121
      Height = 21
      MaxLength = 10
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 143
      Top = 96
      Width = 369
      Height = 21
      MaxLength = 60
      TabOrder = 1
    end
    object neCodigoTipoDeuda: TNumericEdit
      Left = 143
      Top = 16
      Width = 66
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 2
    end
  end
  object spObtenerTiposDeudas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTiposDeudas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ObtenerTodos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = False
      end>
    Left = 160
    Top = 96
  end
  object spActualizarTiposDeudas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTiposDeudas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoOperacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoTipoDeuda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoDeuda'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 200
    Top = 96
  end
  object spEliminarTiposDeudas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTiposDeudas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDeuda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 240
    Top = 96
  end
end
