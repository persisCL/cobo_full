object frmIngresarMovimientos: TfrmIngresarMovimientos
  Left = 205
  Top = 125
  Anchors = []
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Ingresar Movimientos'
  ClientHeight = 561
  ClientWidth = 664
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    664
    561)
  PixelsPerInch = 96
  TextHeight = 13
  object gbDatosConvenio: TGroupBox
    Left = 6
    Top = 7
    Width = 652
    Height = 138
    Caption = ' Datos del Convenio '
    TabOrder = 0
    object Label3: TLabel
      Left = 14
      Top = 111
      Width = 105
      Height = 13
      Caption = 'Convenios del Cliente:'
    end
    object Label7: TLabel
      Left = 9
      Top = 305
      Width = 122
      Height = 13
      Caption = 'Detalle del Comprobante: '
    end
    object cbConveniosCliente: TVariantComboBox
      Left = 126
      Top = 107
      Width = 259
      Height = 19
      Style = vcsOwnerDrawFixed
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbConveniosClienteChange
      OnDrawItem = cbConveniosClienteDrawItem
      Items = <>
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 19
      Width = 637
      Height = 80
      Caption = ' Buscar ...'
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 23
        Width = 78
        Height = 13
        Caption = 'RUT del Cliente:'
      end
      object Label8: TLabel
        Left = 265
        Top = 23
        Width = 103
        Height = 13
        Caption = 'N'#250'mero de Convenio:'
      end
      object peRUTCliente: TPickEdit
        Left = 107
        Top = 19
        Width = 139
        Height = 21
        CharCase = ecUpperCase
        Color = 16444382
        Enabled = True
        TabOrder = 0
        EditorStyle = bteTextEdit
        OnButtonClick = peRUTClienteButtonClick
      end
      object BtnBuscar: TButton
        Left = 553
        Top = 17
        Width = 75
        Height = 25
        Caption = '&Buscar'
        TabOrder = 1
        OnClick = BtnBuscarClick
      end
      object txtNumeroConvenio: TEdit
        Left = 377
        Top = 19
        Width = 160
        Height = 21
        Color = 16444382
        TabOrder = 2
        OnChange = txtNumeroConvenioChange
      end
      object BtnLimpiar: TButton
        Left = 553
        Top = 48
        Width = 75
        Height = 25
        Caption = 'Limpiar'
        TabOrder = 3
        OnClick = BtnLimpiarClick
      end
    end
  end
  object gbDatosCliente: TGroupBox
    Left = 6
    Top = 149
    Width = 652
    Height = 100
    Caption = ' Datos del Cliente '
    TabOrder = 1
    object lNombreCompleto: TLabel
      Left = 81
      Top = 19
      Width = 83
      Height = 13
      Caption = 'lNombreCompleto'
    end
    object lDomicilio: TLabel
      Left = 81
      Top = 38
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComuna: TLabel
      Left = 81
      Top = 57
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegion: TLabel
      Left = 81
      Top = 76
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label31: TLabel
      Left = 14
      Top = 19
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Apellido:'
    end
    object Label33: TLabel
      Left = 14
      Top = 38
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label34: TLabel
      Left = 14
      Top = 57
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label35: TLabel
      Left = 14
      Top = 76
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
  end
  object gbItemsPendientes: TGroupBox
    Left = 5
    Top = 252
    Width = 651
    Height = 275
    Caption = ' Items Pendientes '
    TabOrder = 2
    DesignSize = (
      651
      275)
    object Label2: TLabel
      Left = 296
      Top = 128
      Width = 32
      Height = 13
      Caption = 'Label2'
    end
    object Label4: TLabel
      Left = 407
      Top = 251
      Width = 27
      Height = 13
      Caption = 'Total:'
    end
    object dbMovimientosAgregados: TDBListEx
      Left = 8
      Top = 17
      Width = 545
      Height = 224
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 25
          Header.Caption = ' '
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Marcado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 110
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHora'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 140
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Concepto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 160
          Header.Caption = 'Detalle'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Observaciones'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescImporte'
        end>
      DataSource = dsMovimientosAgregados
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
    object btnAgregarItem: TButton
      Tag = 10
      Left = 558
      Top = 110
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Agregar'
      Enabled = False
      TabOrder = 1
      OnClick = btnAgregarItemClick
    end
    object btnModificarItem: TButton
      Tag = 20
      Left = 558
      Top = 135
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Modificar'
      Enabled = False
      TabOrder = 2
      OnClick = btnAgregarItemClick
    end
    object btnEliminarItem: TButton
      Left = 558
      Top = 160
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Eliminar'
      Enabled = False
      TabOrder = 3
      OnClick = btnEliminarItemClick
    end
    object txtTotal: TEdit
      Left = 440
      Top = 247
      Width = 113
      Height = 21
      ReadOnly = True
      TabOrder = 4
    end
  end
  object btnSalir: TButton
    Left = 569
    Top = 532
    Width = 88
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object btn_Continuar: TButton
    Left = 477
    Top = 533
    Width = 88
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Confirmar'
    TabOrder = 4
    OnClick = btn_ContinuarClick
  end
  object dsMovimientosAgregados: TDataSource
    DataSet = cdsMovimientosAgregados
    Left = 46
    Top = 328
  end
  object cdsMovimientosAgregados: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Marcado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'NumeroMovimiento'
        DataType = ftInteger
      end
      item
        Name = 'FechaHora'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftWord
      end
      item
        Name = 'Concepto'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescImporte'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ImporteCtvs'
        DataType = ftLargeint
      end
      item
        Name = 'Nuevo'
        DataType = ftBoolean
      end
      item
        Name = 'Cantidad'
        DataType = ftFloat
      end
      item
        Name = 'PorcentajeIVA'
        DataType = ftFloat
      end
      item
        Name = 'TipoComprobante'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'IncluyeIVA'
        DataType = ftBoolean
      end
      item
        Name = 'ValorNeto'
        DataType = ftFloat
      end
      item
        Name = 'ImporteIVA'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterScroll = cdsMovimientosAgregadosAfterScroll
    OnCalcFields = cdsMovimientosAgregadosCalcFields
    Left = 85
    Top = 330
  end
  object Imagenes: TImageList
    Left = 80
    Top = 372
    Bitmap = {
      494C0101020004001C0010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 262
    Top = 387
  end
  object spAgregarMovimientoCuentaIVA: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarMovimientoCuentaIVA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteIVA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcentajeIVA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 146
    Top = 385
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 382
    Top = 379
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 274
    Top = 329
  end
end
