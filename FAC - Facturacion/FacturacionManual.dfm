object frmEmisionFactura: TfrmEmisionFactura
  Left = 210
  Top = 129
  Width = 734
  Height = 572
  Anchors = []
  Caption = 'Facturaci'#243'n Manual'
  Color = clBtnFace
  Constraints.MinHeight = 465
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    726
    538)
  PixelsPerInch = 96
  TextHeight = 13
  object btnSalir: TDPSButton
    Left = 646
    Top = 509
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object btnFacturar: TDPSButton
    Left = 536
    Top = 509
    Width = 102
    Anchors = [akRight, akBottom]
    Caption = '&Facturar'
    TabOrder = 2
    OnClick = btnFacturarClick
  end
  object btnImprimir: TDPSButton
    Left = 375
    Top = 509
    Width = 154
    Anchors = [akRight, akBottom]
    Caption = '&Imprimir Resumen Factura'
    TabOrder = 1
    Visible = False
    OnClick = btnImprimirClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 726
    Height = 502
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    Caption = 'Panel1'
    TabOrder = 0
    DesignSize = (
      726
      502)
    object gbMovSeleccionados: TGroupBox
      Left = 6
      Top = 209
      Width = 715
      Height = 287
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Factura resultado'
      TabOrder = 2
      DesignSize = (
        715
        287)
      object Bevel1: TBevel
        Left = 8
        Top = 13
        Width = 700
        Height = 44
        Anchors = [akLeft, akTop, akRight]
        Shape = bsBottomLine
      end
      object lApellidoNombre: TLabel
        Left = 72
        Top = 19
        Width = 156
        Height = 13
        AutoSize = False
        Caption = 'Nombre y del cliente '
      end
      object Bevel2: TBevel
        Left = 8
        Top = 252
        Width = 700
        Height = 10
        Anchors = [akLeft, akRight, akBottom]
        Shape = bsBottomLine
      end
      object LImporteTotal: TLabel
        Left = 475
        Top = 265
        Width = 57
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'importe total'
      end
      object lDomicilio: TLabel
        Left = 72
        Top = 39
        Width = 625
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'Domicilio'
      end
      object lVencimiento: TLabel
        Left = 128
        Top = 265
        Width = 60
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'lVencimiento'
      end
      object txtMedioPago: TLabel
        Left = 512
        Top = 19
        Width = 197
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'txtMedioPago'
      end
      object Label4: TLabel
        Left = 14
        Top = 19
        Width = 52
        Height = 13
        Caption = 'Nombre: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 402
        Top = 19
        Width = 90
        Height = 13
        Caption = 'Medio de Pago:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 16
        Top = 265
        Width = 109
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Fecha de Vencimiento:'
      end
      object Label9: TLabel
        Left = 407
        Top = 265
        Width = 65
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Importe Total:'
      end
      object Label10: TLabel
        Left = 14
        Top = 39
        Width = 56
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ListBox: TListBox
        Left = 8
        Top = 60
        Width = 620
        Height = 197
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ParentFont = False
        TabOrder = 0
        OnClick = ListBoxClick
      end
      object btnAgregarLinea: TDPSButton
        Left = 643
        Top = 61
        Width = 62
        Anchors = [akTop, akRight]
        Caption = 'Agregar'
        TabOrder = 1
        OnClick = btnAgregarLineaClick
      end
      object btnEliminarLinea: TDPSButton
        Left = 643
        Top = 93
        Width = 62
        Anchors = [akTop, akRight]
        Caption = 'Eliminar'
        TabOrder = 2
        OnClick = btnEliminarLineaClick
      end
    end
    object gbMovDisponibles: TGroupBox
      Left = 6
      Top = 62
      Width = 714
      Height = 146
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Lista de movimientos'
      TabOrder = 1
      DesignSize = (
        714
        146)
      object CheckListBox: TCheckListBox
        Left = 8
        Top = 16
        Width = 619
        Height = 121
        OnClickCheck = CheckListBoxClickCheck
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ItemHeight = 14
        ParentFont = False
        TabOrder = 0
      end
      object btnSeleccionarTodo: TDPSButton
        Left = 642
        Top = 16
        Width = 62
        Anchors = [akTop, akRight]
        Caption = 'Todo'
        TabOrder = 1
        OnClick = btnSeleccionarTodoClick
      end
      object btnInvertirSeleccion: TDPSButton
        Left = 642
        Top = 48
        Width = 62
        Anchors = [akTop, akRight]
        Caption = 'Invertir '
        TabOrder = 2
        OnClick = btnInvertirSeleccionClick
      end
    end
    object GroupBox4: TGroupBox
      Left = 6
      Top = 1
      Width = 715
      Height = 61
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Par'#225'metros de b'#250'squeda'
      TabOrder = 0
      object Label1: TLabel
        Left = 6
        Top = 17
        Width = 78
        Height = 13
        Caption = 'RUT del Cliente:'
      end
      object Label2: TLabel
        Left = 329
        Top = 17
        Width = 65
        Height = 13
        Caption = 'Fecha L'#237'mite:'
      end
      object Label3: TLabel
        Left = 138
        Top = 16
        Width = 113
        Height = 13
        Caption = 'N'#250'meros de Convenios:'
      end
      object deFechaHora: TDateEdit
        Left = 329
        Top = 31
        Width = 92
        Height = 21
        AutoSelect = False
        TabOrder = 2
        OnChange = deFechaHoraChange
        OnEnter = deFechaHoraEnter
        OnExit = deFechaHoraExit
        Date = -693594.000000000000000000
      end
      object cbConveniosCliente: TComboBox
        Left = 138
        Top = 31
        Width = 182
        Height = 21
        Style = csDropDownList
        Enabled = False
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbConveniosClienteChange
      end
      object peCodigoCliente: TPickEdit
        Left = 6
        Top = 31
        Width = 123
        Height = 21
        CharCase = ecUpperCase
        Enabled = True
        TabOrder = 0
        OnChange = peCodigoClienteChange
        Decimals = 0
        EditorStyle = bteTextEdit
        OnButtonClick = peCodigoClienteButtonClick
      end
    end
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 48
    Top = 152
  end
  object CrearLoteFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearLoteFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Total'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 152
  end
  object CrearProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearProcesoFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 152
  end
  object UpdateNumeroLote: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'LoteFacturacion'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoConvenio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NumeroMovimiento'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'UPDATE MovimientosCuentas'
      'SET LoteFacturacion = :LoteFacturacion'
      'WHERE CodigoConvenio = :CodigoConvenio '
      '      AND NumeroMovimiento = :NumeroMovimiento')
    Left = 288
    Top = 136
  end
  object CrearComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
      end
      item
        Name = '@PeriodoInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@PeriodoFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
      end
      item
        Name = '@TipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
      end
      item
        Name = '@Calle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@DetalleDomicilio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@TotalComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@SaldoAnterior'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@TotalAPagar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@AjusteSencilloAnterior'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@AjusteSencilloActual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoTipoCuenta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@NumeroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@CodigoTipoTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
      end
      item
        Name = '@FechaVencimientoTarjeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@CodigoEmisorTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@Inserto1'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@Inserto2'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@Inserto3'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@Inserto4'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@ImprDetallada'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@EnvioElectronico'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
      end
      item
        Name = '@CodigoImpresoraFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Prepared = True
    Left = 224
    Top = 152
  end
  object AgregarMovimientoCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarMovimientoCuenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 168
  end
  object ActualizarProcesoFacturacion: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CantidadCuentas'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'NumeroProcesoFacturacion'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE  ProcesosFacturacion'
      'SET CantCuentasFacturadas = :CantidadCuentas,'
      '        CantFacturas = 1,'
      '         TotalMenteImpreso = 1'
      'WHERE '
      '         NumeroProcesoFacturacion = :NumeroProcesoFacturacion')
    Left = 384
    Top = 152
  end
  object ActualizarNumeroMovimiento: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoConvenio'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'IndiceVehiculo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'FechaHora'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'NumeroMovimiento'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select CodigoConcesionaria, NumeroViaje'
      '  into #Viajes'
      '  from Viajes'
      ' where Viajes.CodigoConvenio = :CodigoConvenio '
      '   and  Viajes.IndiceVehiculo = :IndiceVehiculo '
      '   and Viajes.CodigoConcesionaria = :CodigoConcesionaria'
      '   and Viajes.fechaHoraInicio < :FechaHora'
      '   and VIajes.NumeroMovimiento IS NULL'
      ''
      ''
      'UPDATE Viajes'
      '   SET NumeroMovimiento = :NumeroMovimiento'
      '  FROM #Viajes'
      ' WHERE Viajes.CodigoConcesionaria = #Viajes.CodigoConcesionaria'
      '   AND Viajes.NumeroViaje = #Viajes.NumeroViaje'
      ''
      'DROP TABLE #Viajes')
    Left = 320
    Top = 152
  end
  object Imagenes: TImageList
    Left = 416
    Top = 152
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000313139003131390031313900313139003131
      3900313139003131390031313900313139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A5A5A0042424200313139000000000000000000000000000000
      0000000000000000000000000000313139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210018212100182121001821
      2100182121001821210018212100182121001821210018212100182121001821
      210018212100182121001821210018212100FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF005A63630039423900000000000000
      000000000000635A42007B6B4200313139000000000031313900313139003131
      3900313139003131390000000000313139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000182121002929310029293100292931002929
      3100292931002929310029293100292931002929310029293100292931002929
      310029293100292931002929310029293100000000003939310073734A008484
      7B00000000006B634A00947B4A00313139000000000000000000000000000000
      0000000000000000000000000000313139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210018212100182121001821
      2100182121001821210018212100182121001821210018212100182121001821
      21001821210018212100182121001821210029293100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00292931000000000000000000424239006363
      4A00000000006B634A009C845200313139000000000031313900313139003131
      3900313139003131390000000000313139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210018212100182121001821
      2100182121001821210018212100182121001821210018212100182121001821
      21001821210018212100182121001821210029293100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002929310029293100FFFF
      FF002121290031313900FFFFFF00292931000000000000000000000000006363
      42004A4A42006B634A00947B4A00313139000000000000000000000000000000
      0000000000000000000000000000313139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000001821210029293100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF005252
      5A0029293100FFFFFF00FFFFFF00292931000000000000000000000000000000
      0000525239005A5239008C7B4A00313139003131390031313900313139003131
      3900313139003131390031313900313139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210000000000000000000000
      0000000000000000000018212100000000000000000018212100182121000000
      00000000000000000000000000001821210029293100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00292931000000000000000000000000000000
      000000000000847B5A00AD9C5A00B5A56300B5A56300A59C5A006B6352000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210000000000000000000000
      0000000000000000000018212100000000000000000018212100000000000000
      00000000000000000000000000001821210029293100FFFFFF00292931002929
      3100292931002929310029293100292931002929310029293100292931002929
      31002929310029293100FFFFFF00292931000000000000000000000000000000
      000000000000000000004A4A420052524A004A4A42004A4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210000000000000000000000
      0000000000001821210018212100182121000000000018212100182121000000
      00000000000000000000000000001821210029293100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00292931000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000001821210029293100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002929
      31002929310029293100FFFFFF00292931000000000000000000000000000000
      00000000000000000000847B5A00A58C52009C8C5200424A4200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001821210018212100182121001821
      2100182121001821210018212100182121001821210018212100182121001821
      210018212100182121001821210018212100292931007B7B84007B7B84007B7B
      84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B
      84007B7B84007B7B84007B7B8400292931000000000000000000000000000000
      0000000000004A4A4200CEAD6300F7C67300CEAD6300636339008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002929310029293100292931002929
      3100292931002929310029293100292931002929310029293100292931002929
      3100292931002929310029293100292931000000000000000000000000000000
      0000000000004A524200CEAD6300CEAD6300CEAD630073734200636B6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000737373006B6B4A007B7B42007373420031393100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000525A5200636B630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFE000000FFFFFFFFF8FE0000
      00007FFE388200007FFE000088FE000000000000C882000000000000E0FE0000
      7FFE0000F00000007D9E0000F81F00007DBE0000FC3F0000789E0000FFFF0000
      7FFE0000FC3F000000000000F81F000000000000F81F0000FFFFFFFFF83F0000
      FFFFFFFFFE7F0000FFFFFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object ObtenerInformeFactura: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInformeFactura'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigosCuentas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CantidadLineas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 152
  end
  object qryConvenios: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoCliente'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT CodigoTipoMedioPago, Descripcion, dbo.FormatearNumeroConv' +
        'enio(NumeroConvenio) as NumeroConvenio, CodigoConvenio FROM Conv' +
        'enio WITH (NOLOCK), TipoMedioPago WITH (NOLOCK)'
      
        'WHERE Convenio.TipoMedioPagoAutomatico = TipoMedioPago.CodigoTip' +
        'oMedioPago'
      '      and MedioPagoAutomatico = 1'
      '      and Convenio.CodigoCliente = :CodigoCliente')
    Left = 112
    Top = 152
  end
  object ObtenerDebitoAutomatico: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoCLiente'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoDebitoAutomatico'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TipoDebito, CodigoEntidad, CuentaDebito'
      'FROM DebitoAutomatico'
      'WHERE DebitoAutomatico.CodigoPersona = :CodigoCLiente'
      
        'AND DebitoAutomatico.CodigoDebitoAutomatico = :CodigoDebitoAutom' +
        'atico')
    Left = 160
    Top = 152
  end
  object ActualizarTotalLoteFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTotalLoteFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 352
    Top = 152
  end
  object CrearProcesoFacturacionGrupoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearProcesoFacturacionGrupoFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 184
  end
  object ObtenerDomicilioConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 456
    Top = 152
  end
  object ObtenerDatosMedioPagoAutomaticoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosMedioPagoAutomaticoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 488
    Top = 152
  end
end
