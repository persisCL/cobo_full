{********************************** File Header ********************************
File Name   : FrmConsultaTransitosComprobanteAnular.pas
Author      : jconcheyro
Date Created: 07/03/2006
Language    : ES-AR
Description : Seleccion los tr�nsitos de un Comprobante para pasarlos a la nota de credito


Revision 1
Author      : jconcheyro
Date Created: 27/07/2006
Description : Se le agregan los decimales para evitar diferencias en la cuenta corriente

Revision 2
Author      : jconcheyro
Date Created: 21/12/2006
Description : Se le sacan los decimales porque estan prohibidos en chile

Revision 3
Author      : jconcheyro
Date Created: 04/04/2007
Description : Despues de muchas definiciones, se soluciona el tema del redondeo.
			  Se hace desde la base un Ceiling de la suma de los importes de los transitos
			  entonces llegamos de 99.01 a 100.00 por ejemplo.

Revision 4
Author      : FSandi
Date Created: 17/08/2007
Description : Se cambian las asignaciones .asinteger para importes, a .asvariant para evitar valores
                incorrectos al superar el limite del integer.

*******************************************************************************}
unit FrmConsultaTransitosComprobanteAnular;

interface

uses
  //consultas transito no cliente
  DmConnection,
  UtilProc,
  peaprocs,
  util,
  UtilDB,
  Peatypes,
  MenuesContextuales,
  ConstParametrosGenerales,
  // General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB,
  DmiCtrls, VariantComboBox, Validate, DateEdit,Abm_obj,RStrings,StrUtils,
  ImgList;

type
  TFormConsultaTransitosComprobanteAnular = class(TForm)
	Panel2: TPanel;
	dbl_Transitos: TDBListEx;
	Panel3: TPanel;
	Panel4: TPanel;
	dsTransitos: TDataSource;
	lblTotales: TLabel;
	lnCheck: TImageList;
	ilCamara: TImageList;
	BtnSalir: TButton;
	spObtenerTransitosPorMovimiento: TADOStoredProc;
	lblTotSeleccionados: TLabel;
	btnSelectAll: TButton;
	btnDeselectAll: TButton;
	btnInvertirSeleccion: TButton;
	procedure dbl_TransitosKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure btnInvertirSeleccionClick(Sender: TObject);
	procedure btnDeselectAllClick(Sender: TObject);
	procedure btnSelectAllClick(Sender: TObject);
	procedure dbl_TransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
	procedure FormShow(Sender: TObject);
	procedure btn_FiltrarClick(Sender: TObject);
	procedure btn_LimpiarClick(Sender: TObject);
	procedure dbl_TransitosColumns0HeaderClick(Sender: TObject);
	procedure dbl_TransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
	procedure dbl_TransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
	procedure dbl_TransitosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure ActualizarImporteTotalSeleccionados;
  private
	FListaTransitos: TStringList;
	FImporteDeTransitosSeleccionados: Int64;
	FNumeroMovimiento : int64;
	FCantidadTotalDeTransitos: integer;
	Function  ListaTransitosInclude(valor:string):boolean;
	Function  ListaTransitosExclude(valor:string):boolean;
	Function  ListaTransitosIn(valor:string):boolean;
	{ Private declarations }
  public
	{ Public declarations }
	function Inicializar(aNumeroMovimiento: int64; MDIChild: Boolean; var FListaDeTransitos:TStringList): Boolean;
	function ImporteTotal: Int64;
	function SelectedAll: boolean;
  end;

var
  FormConsultaTransitosComprobanteAnular: TFormConsultaTransitosComprobanteAnular;


implementation

{$R *.dfm}


const
	CONST_COLUMNA_IMAGEN = 9;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.Inicializar
  Author:    rcastro
  Date:      05-May-2005
  Arguments: txtCaption: String; MDIChild: Boolean
  Result:    Boolean
-----------------------------------------------------------------------------}
function TFormConsultaTransitosComprobanteAnular.Inicializar( aNumeroMovimiento: int64; MDIChild: Boolean; var FListaDeTransitos:TStringList) : Boolean;
Var S: TSize;
begin
	Result := True;
	try
		if MDIChild then begin
			S := GetFormClientSize(Application.MainForm);
			SetBounds(0, 0, S.cx, S.cy);
		end else begin
			FormStyle := fsNormal;
			Visible := False;
		end;
		CenterForm(Self);
	except
		result := False;
	end;
	FListaTransitos := FListaDeTransitos;
	FImporteDeTransitosSeleccionados := 0;
	FCantidadTotalDeTransitos := 0;
	FNumeroMovimiento := aNumeroMovimiento;
	btn_FiltrarClick(Self);
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.FormShow
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.FormShow(Sender: TObject);
begin

end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.btn_FiltrarClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.btn_FiltrarClick(Sender: TObject);
begin
	try
		spObtenerTransitosPorMovimiento.DisableControls;
		with spObtenerTransitosPorMovimiento do begin
			close;
			commandtimeout:=500;
			Parameters.Refresh;
			Parameters.ParamByName('@NumeroMovimiento').Value:= FNumeroMovimiento;
		end;
		spObtenerTransitosPorMovimiento.Open;
		FCantidadTotalDeTransitos := spObtenerTransitosPorMovimiento.RecordCount ;
	finally
		spObtenerTransitosPorMovimiento.EnableControls;
	end;
	ActualizarImporteTotalSeleccionados;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.btn_LimpiarClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.btn_LimpiarClick(Sender: TObject);
begin
	spObtenerTransitosPorMovimiento.close;

end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.dbl_TransitosColumns0HeaderClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.dbl_TransitosColumns0HeaderClick(Sender: TObject);
begin
	if spObtenerTransitosPorMovimiento.IsEmpty then Exit;

	if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
	else TDBListExColumn(sender).Sorting := csAscending;

	spObtenerTransitosPorMovimiento.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosInclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: agrego un valor a la lista de transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormConsultaTransitosComprobanteAnular.ListaTransitosInclude(valor:string):boolean;
begin
	result:=false;
	try
		//Se hace necesario cargar el NumCorrCA y el NumeroMovimiento porque  es una lista que va a contener los
		//NumCorrCA de todos los movimientos del comprobante, y como es necesario saber si se seleccionaron TODOS los de
		//un movimiento, necesitamos tener la informacion del numerode movimiento.
		//Cargamos el string de forma NumCorrCA=NumeroMovimiento. Ver Metodo SelectedAll
		if not ListaTransitosIn(valor) then flistatransitos.Add(valor+'='+IntToStr(FNumeroMovimiento));
		result:=true;
	except
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosExclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: quito un valor a la lista de Transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormConsultaTransitosComprobanteAnular.ListaTransitosExclude(valor:string):boolean;
var index:integer;
begin
	result:=false;
	try
		index:=flistatransitos.IndexOf(valor+'='+IntToStr(FNumeroMovimiento));
		if index <> -1 then flistatransitos.Delete(index);
		result:=true;
	except
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosIn
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: Devuelve si un valor esta en la lista transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormConsultaTransitosComprobanteAnular.ListaTransitosIn(valor:string):boolean;
begin
	result := not (flistatransitos.IndexOf(valor+'='+IntToStr(FNumeroMovimiento)) = -1);
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.dbl_TransitosDrawText
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.dbl_TransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	i: Integer;
begin

	//muestra si esta seleccionado para realizar un reclamo o no
	if Column = dbl_transitos.Columns[0] then begin
		i:=iif(not ListaTransitosIn(spObtenerTransitosPorMovimiento.FieldByName('numcorrca').Asstring),0,1);
		//verifica si esta seleccionado o no
		lnCheck.Draw(dbl_transitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;




end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.dbl_TransitosLinkClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.dbl_TransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
	Numcorrca: string;
begin
	Screen.Cursor := crHourGlass ;
	//selecciono/desselecciono transito
	if Column = dbl_transitos.Columns[0] then begin
		Numcorrca := spObtenerTransitosPorMovimiento.FieldByName('numcorrca').Asstring;
		if ListaTransitosin(Numcorrca) then begin
			ListaTransitosExclude(Numcorrca);
			//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
			FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant; //Revision 4
		end else begin
			ListaTransitosInclude(Numcorrca);
			//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
			FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant; //Revision 4
		end;
		//dibuja el cambio
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC, RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeTransitosSeleccionados) div 100);
		Sender.Invalidate;
		Sender.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.dbl_TransitosCheckLink
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.dbl_TransitosCheckLink(Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
	if column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN] then begin
		IsLink := (Column = dbl_Transitos.Columns[CONST_COLUMNA_IMAGEN]) and
			(spObtenerTransitosPorMovimiento['RegistrationAccessibility'] > 0);
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: dblTransitosContextPopup
  Author: lgisuk
  Date Created: 14/04/2005
  Description: asigna el menu contextual a la grilla
  Parameters: Sender: TObject; MousePos: TPoint; var Handled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.dbl_TransitosContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
	if not spObtenerTransitosPorMovimiento.IsEmpty then Handled :=
	MostrarMenuContextualMultiplesTransitosNoCliente(dbl_Transitos.ClientToScreen(MousePos),Flistatransitos);
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.BtnSalirClick
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.BtnSalirClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerTransitosPorMovimiento.Active then begin
		FImporteDeTransitosSeleccionados := 0;
		spObtenerTransitosPorMovimiento.DisableControls ;
		spObtenerTransitosPorMovimiento.First ;
		while not spObtenerTransitosPorMovimiento.eof do begin
			if ListaTransitosIn(spObtenerTransitosPorMovimiento.FieldByName('numcorrca').Asstring) then
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant; //Revision 4
			spObtenerTransitosPorMovimiento.Next ;
		end;
		spObtenerTransitosPorMovimiento.EnableControls ;
		spObtenerTransitosPorMovimiento.Close;
	end;
	Close;
	Screen.Cursor := crDefault ;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormConsultaTransitosComprobanteAnular.FormClose
  Author:    rcastro
  Date:      05-May-2005
  Arguments: Sender: TObject; var Action: TCloseAction
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormConsultaTransitosComprobanteAnular.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	 action := caFree;
end;

function TFormConsultaTransitosComprobanteAnular.ImporteTotal: Int64;
begin
	Result := RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeTransitosSeleccionados) div 100 ;
end;



procedure TFormConsultaTransitosComprobanteAnular.btnSelectAllClick(
  Sender: TObject);
Var
	Numcorrca: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerTransitosPorMovimiento.Active then begin
		spObtenerTransitosPorMovimiento.DisableControls;
		spObtenerTransitosPorMovimiento.First;
		while not spObtenerTransitosPorMovimiento.eof do begin
			Numcorrca := spObtenerTransitosPorMovimiento.FieldByName('numcorrca').Asstring;
			if not ListaTransitosin(Numcorrca) then begin
				ListaTransitosInclude(Numcorrca);
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant; //Revision 4
			end;
			spObtenerTransitosPorMovimiento.Next;
		end;
		spObtenerTransitosPorMovimiento.EnableControls;
		spObtenerTransitosPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeTransitosSeleccionados) div 100);
		dbl_Transitos.Invalidate;
		dbl_Transitos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;


procedure TFormConsultaTransitosComprobanteAnular.btnDeselectAllClick(
  Sender: TObject);
Var
	Numcorrca: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerTransitosPorMovimiento.Active then begin
		spObtenerTransitosPorMovimiento.DisableControls;
		spObtenerTransitosPorMovimiento.First;
		while not spObtenerTransitosPorMovimiento.eof do begin
			Numcorrca := spObtenerTransitosPorMovimiento.FieldByName('numcorrca').Asstring;
			if ListaTransitosin(Numcorrca) then begin
				ListaTransitosExclude(Numcorrca);
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant;   //Revision 4
			end;
			spObtenerTransitosPorMovimiento.Next;
		end;
		spObtenerTransitosPorMovimiento.EnableControls;
		spObtenerTransitosPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeTransitosSeleccionados) div 100 );
		dbl_Transitos.Invalidate;
		dbl_Transitos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

procedure TFormConsultaTransitosComprobanteAnular.btnInvertirSeleccionClick(Sender: TObject);
Var
	Numcorrca: string;
begin
	Screen.Cursor := crHourGlass ;
	if spObtenerTransitosPorMovimiento.Active then begin
		spObtenerTransitosPorMovimiento.DisableControls;
		spObtenerTransitosPorMovimiento.First;
		while not spObtenerTransitosPorMovimiento.eof do begin
			Numcorrca := spObtenerTransitosPorMovimiento.FieldByName('numcorrca').Asstring;
			if ListaTransitosin(Numcorrca) then begin
				ListaTransitosExclude(Numcorrca);
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados - spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant;  //Revision 4
			end else begin
				ListaTransitosInclude(Numcorrca);
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant;   //Revision 4
			end;
			spObtenerTransitosPorMovimiento.Next;
		end;
		spObtenerTransitosPorMovimiento.EnableControls;
		spObtenerTransitosPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeTransitosSeleccionados) div 100);
		dbl_Transitos.Invalidate;
		dbl_Transitos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

procedure TFormConsultaTransitosComprobanteAnular.dbl_TransitosKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  //si presiona spaceBar, genera lo mismo que el click de seleccion / deselecci�n
  if Key = VK_SPACE then
	dbl_TransitosLinkClick(dbl_Transitos , dbl_transitos.Columns[0]);

end;

function TFormConsultaTransitosComprobanteAnular.SelectedAll: boolean;
var
  xIndex : integer;
  xCantidad : integer;
begin
	xCantidad := 0;
	//Como en la lista se carg� un string del tipo 'NumCorrCA=NumeroMovimiento', el ValueFromIndex nos devuelve el NumeroMovimiento
	for xIndex := 0 to FListaTransitos.Count -1 do
		if FlistaTransitos.ValueFromIndex[xIndex]  = IntToStr(FNumeroMovimiento) then Inc(xCantidad);

	Result := xCantidad = FCantidadTotalDeTransitos ;
end;

procedure TFormConsultaTransitosComprobanteAnular.ActualizarImporteTotalSeleccionados;
Var
	Numcorrca: string;
begin
	Screen.Cursor := crHourGlass ;
	if (spObtenerTransitosPorMovimiento.Active) and  (FListaTransitos.Count > 0) then begin
		spObtenerTransitosPorMovimiento.DisableControls;
		spObtenerTransitosPorMovimiento.First;
		FImporteDeTransitosSeleccionados := 0;
		while not spObtenerTransitosPorMovimiento.eof do begin
			Numcorrca := spObtenerTransitosPorMovimiento.FieldByName('numcorrca').Asstring;
			if ListaTransitosin(Numcorrca) then
				//FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + RedondearINTConCentavosHaciaAbajoBase(DMConnections.BaseCAC, spObtenerTransitosPorMovimiento.FieldByName('Importe').AsInteger);
				FImporteDeTransitosSeleccionados := FImporteDeTransitosSeleccionados + spObtenerTransitosPorMovimiento.FieldByName('Importe').AsVariant;  //Revision 4
			spObtenerTransitosPorMovimiento.Next;
		end;
		spObtenerTransitosPorMovimiento.EnableControls;
		spObtenerTransitosPorMovimiento.First;
		lblTotSeleccionados.Caption := FormatearImporte(DMConnections.BaseCAC,RedondearINTConCentavosHaciaArribaBase(DMConnections.BaseCAC, FImporteDeTransitosSeleccionados) div 100 );
		dbl_Transitos.Invalidate;
		dbl_Transitos.Repaint;
	end;
	Screen.Cursor := crDefault ;
end;

end.
