object FormControlIndemnizaciones: TFormControlIndemnizaciones
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Control de Indemnizaciones'
  ClientHeight = 516
  ClientWidth = 732
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Pnl1: TPanel
    Left = 0
    Top = 296
    Width = 732
    Height = 174
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      732
      174)
    object LblConcepto: TLabel
      Left = 9
      Top = 108
      Width = 55
      Height = 13
      Caption = 'Concepto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblAlmacen: TLabel
      Left = 9
      Top = 75
      Width = 49
      Height = 13
      Caption = 'Almac'#233'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblDescripcion: TLabel
      Left = 9
      Top = 43
      Width = 68
      Height = 13
      Caption = 'Descripci'#243'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblOperacion: TLabel
      Left = 9
      Top = 11
      Width = 59
      Height = 13
      Caption = 'Operaci'#243'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 9
      Top = 140
      Width = 110
      Height = 13
      Caption = 'Tipo de Asignaci'#243'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtDescripcion: TEdit
      Left = 83
      Top = 41
      Width = 320
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = 16444382
      MaxLength = 50
      TabOrder = 1
    end
    object CbOperacion: TVariantComboBox
      Left = 83
      Top = 9
      Width = 144
      Height = 21
      Style = vcsDropDownList
      Anchors = [akLeft, akTop, akRight]
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
    object CbAlmacen: TVariantComboBox
      Left = 83
      Top = 73
      Width = 320
      Height = 21
      Style = vcsDropDownList
      Anchors = [akLeft, akTop, akRight]
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object CbConcepto: TVariantComboBox
      Left = 83
      Top = 106
      Width = 320
      Height = 21
      Style = vcsDropDownList
      Anchors = [akLeft, akTop, akRight]
      Color = 16444382
      ItemHeight = 13
      TabOrder = 3
      Items = <>
    end
    object cbTipoAsignacionTag: TComboBox
      Left = 136
      Top = 137
      Width = 145
      Height = 21
      Color = 16444382
      ItemHeight = 0
      TabOrder = 4
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 33
    Width = 732
    Height = 263
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 0
    object Lista: TAbmList
      Left = 1
      Top = 1
      Width = 730
      Height = 261
      TabStop = True
      TabOrder = 0
      Align = alClient
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      SubTitulos.Sections = (
        #0'72'#0'Operaci'#243'n     '
        
          #0'243'#0'Descripci'#243'n Motivo                                         ' +
          '       '
        #0'106'#0'Almac'#233'n                   '
        #0'126'#0'Concepto                        ')
      Table = tblMotivosMovimientos
      Style = lbOwnerDrawFixed
      OnClick = ListaClick
      OnProcess = ListaProcess
      OnDrawItem = ListaDrawItem
      OnRefresh = ListaRefresh
      OnInsert = ListaInsert
      OnDelete = ListaDelete
      OnEdit = ListaEdit
      Access = [accAlta, accBaja, accModi]
      Estado = Normal
      ToolBar = AbmToolbar1
    end
  end
  object Pnl3: TPanel
    Left = 0
    Top = 470
    Width = 732
    Height = 46
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      732
      46)
    object Notebook: TNotebook
      Left = 527
      Top = 6
      Width = 197
      Height = 37
      Anchors = [akTop, akRight]
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 111
          Top = 0
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 26
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 732
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
  end
  object tblMotivosMovimientos: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'dbo.MotivosMovimientosCuentasTelevias'
    Left = 32
    Top = 68
  end
  object ObtenerConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConceptosComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 568
    Top = 344
  end
  object spObtenerDatosAlmacen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosAlmacen;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 616
    Top = 344
  end
end
