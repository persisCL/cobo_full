unit frmObservacionesConveniosListaAmarilla;

{
  Firma       : SS_660F_MCA_20140805
  Descripcion : Nuevo formulario que permite ingresar observaciones a los convenios inhabilitados
                por lista amarilla.
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, ExtCtrls,
  DMConnection, PeaProcs, DB, ADODB, UtilProc, Util, Abm_obj, ImgList;
type
  TObservacionesConvenioListaAmarillaForm = class(TForm)
    pnl1: TPanel;
    DBListaObservaciones: TDBListEx;
    pnl3: TPanel;
    lblObservaciones: TLabel;
    mmoObservaciones: TMemo;
    grp1: TGroupBox;
    lblConcesionaria: TLabel;
    lblEstado: TLabel;
    lblEstadoConvenio: TLabel;
    lblCodigoConcesionaria: TLabel;
    lblConvenio: TLabel;
    lblNumeroConvenio: TLabel;
    lblFechaObservacion: TLabel;
    lblFechaObs: TLabel;
    spObtenerObservacionesConvenioListaAmarilla: TADOStoredProc;
    dsObtenerObservacionesConvenioListaAmarilla: TDataSource;
    spAgregarObservacionConvenioListaAmarilla: TADOStoredProc;
    pnl2: TPanel;
    ilCheck: TImageList;
    spEliminarObservacionConvenioListaAmarilla: TADOStoredProc;
    chkTraerBajas: TCheckBox;
    btnSalir: TButton;
    pnl4: TPanel;
    btnEliminar: TButton;
    btnGrabar: TButton;
    procedure btnSalirClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure DBListaObservacionesClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure chkTraerBajasClick(Sender: TObject);
    procedure DBListaObservacionesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure mmoObservacionesChange(Sender: TObject);
  private
    { Private declarations }
    FIdConvenioInhabilitado: Integer;
    FEstadoConvenio: Integer;
    FIdObservacion: Integer;
    procedure Limpiar;
    function ObtenerObservaciones(IdConvenioInhabilitado: Integer; TraerBajas: Boolean) : Boolean;
  public
    { Public declarations }
    procedure Inicializar(NumeroConvenio: string; CodigoConvenio, CodigoConcesionaria: Integer; Concesionaria: String; EstadoConvenio: Integer; DescripcionEstado: string; IdConvenioInhabilitado: Integer);
    function GuardarObservacion(Observacion: string; IdConvenioInhabilitado, EstadoConvenio: Integer; Usuario :string; out MsgError: string): Boolean;
    function EliminarObservacion(IdObservacion: Integer; Usuario: string; out MsgError: String): Boolean;
  end;

var
  ObservacionesConvenioListaAmarillaForm: TObservacionesConvenioListaAmarillaForm;

implementation

const
  MSG_CAPTION = 'Observaciones de Convenios en Lista Amarilla';
  MSG_ERROR_CARGA_OBSERVACIONES = 'Ocurrió el siguiente error al intentar obtener las observaciones de Convenios en lista amarilla';
  CAPTION_VALIDAR_OBSERVACIONES = 'Validar Observaciones';
  MSG_OBSERVACION_VACIA = 'Debe ingresar alguna observación';
  MSG_ERROR_GUARDAR_OBSERVACION = 'Se ha producido un error al guardar la observación';
  MSG_ELIMINAR_OBSERVACIONES = 'Se ha eliminado la observación correctamente.';
  MSG_SIN_SELECCION = 'No ha seleccionado Observación a eliminar';
  MSG_ERROR_ELIMINAR_OBSERVACION = 'Se ha producido un error al eliminar la observación';
  MSG_GRABADO = 'La observación se ha grabado correctamente.';
  MSG_ELIMINADO = 'Se ha eliminado la observación correctamente.';
{$R *.dfm}

procedure TObservacionesConvenioListaAmarillaForm.btnGrabarClick(
  Sender: TObject);
var
  Error: string;
begin
     if not ValidateControls([mmoObservaciones],
      [mmoObservaciones.Text <> ''],
      CAPTION_VALIDAR_OBSERVACIONES, [MSG_OBSERVACION_VACIA]) then exit;


     if not GuardarObservacion(mmoObservaciones.Text, FIdConvenioInhabilitado, FEstadoConvenio, UsuarioSistema, Error) then begin
        MsgBoxErr(MSG_ERROR_GUARDAR_OBSERVACION, Error, Caption, MB_ICONERROR);
        exit;
     end;

     if not ObtenerObservaciones(FIdConvenioInhabilitado, chkTraerBajas.Checked) then
        MsgBoxErr(MSG_ERROR_CARGA_OBSERVACIONES, MSG_ERROR_CARGA_OBSERVACIONES, Caption, MB_ICONERROR);

end;

procedure TObservacionesConvenioListaAmarillaForm.btnEliminarClick(
  Sender: TObject);
var
  Error: string;
begin
      if MsgBox('¿Está seguro que desea eliminar la observación?', 'Eliminar Observación', MB_ICONQUESTION + MB_YESNO) = mrYes  then
      begin
          if FIdObservacion>0 then begin

              if not EliminarObservacion(FIdObservacion, UsuarioSistema, Error) then begin
                  MsgBoxErr(MSG_ERROR_ELIMINAR_OBSERVACION, Error, Caption, MB_ICONERROR);
                  exit;
              end;

              if not ObtenerObservaciones(FIdConvenioInhabilitado, chkTraerBajas.Checked) then
                MsgBoxErr(MSG_ERROR_CARGA_OBSERVACIONES, MSG_ERROR_CARGA_OBSERVACIONES, Caption, MB_ICONERROR);

          end
          else begin
              MsgBox(MSG_SIN_SELECCION, Caption, MB_OK);
          end;
      end;

end;

procedure TObservacionesConvenioListaAmarillaForm.btnSalirClick(
  Sender: TObject);
begin
  Close;
end;

procedure TObservacionesConvenioListaAmarillaForm.chkTraerBajasClick(
  Sender: TObject);
begin
     if not ObtenerObservaciones(FIdConvenioInhabilitado, chkTraerBajas.Checked) then
        MsgBoxErr(MSG_ERROR_CARGA_OBSERVACIONES, MSG_ERROR_CARGA_OBSERVACIONES, Caption, MB_ICONERROR);

end;

procedure TObservacionesConvenioListaAmarillaForm.DBListaObservacionesClick(
  Sender: TObject);
begin
  if not spObtenerObservacionesConvenioListaAmarilla.FieldByName('Eliminado').AsBoolean then begin

      FIdObservacion := spObtenerObservacionesConvenioListaAmarilla.FieldByName('IdObservacion').AsInteger;
      btnEliminar.Enabled := True;
  end else
      btnEliminar.Enabled := False;
end;

procedure TObservacionesConvenioListaAmarillaForm.DBListaObservacionesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
  if (not spObtenerObservacionesConvenioListaAmarilla.FieldByName('Eliminado').AsBoolean = True) then
      Sender.Canvas.Font.Color := clWindowText
  else
      Sender.Canvas.Font.Color := clBlue;
end;

procedure TObservacionesConvenioListaAmarillaForm.Inicializar(NumeroConvenio: string; CodigoConvenio, CodigoConcesionaria: Integer; Concesionaria: String; EstadoConvenio: Integer; DescripcionEstado: string; IdConvenioInhabilitado: Integer);
begin

    Self.Caption := MSG_CAPTION;
    CenterForm(Self);
    Limpiar;

    lblFechaObs.Caption := DateToStr(NowBase(DMConnections.BaseCAC));
    lblConvenio.Caption := NumeroConvenio;
    lblConcesionaria.Caption := Concesionaria;
    lblEstado.Caption := DescripcionEstado;

    FIdConvenioInhabilitado := IdConvenioInhabilitado;
    FEstadoConvenio := EstadoConvenio;

    if not ObtenerObservaciones(FIdConvenioInhabilitado, chkTraerBajas.Checked) then
        MsgBoxErr(MSG_ERROR_CARGA_OBSERVACIONES, MSG_ERROR_CARGA_OBSERVACIONES, Caption, MB_ICONERROR);

//     mmoObservaciones.SetFocus;
end;


procedure TObservacionesConvenioListaAmarillaForm.Limpiar;
begin
    mmoObservaciones.Clear;
    Screen.Cursor := crDefault;
    btnGrabar.Enabled := False;
    btnEliminar.Enabled := False;
end;

procedure TObservacionesConvenioListaAmarillaForm.mmoObservacionesChange(
  Sender: TObject);
begin
    if mmoObservaciones.Text <> '' then btnGrabar.Enabled := True
    else  btnGrabar.Enabled := False;
end;

function TObservacionesConvenioListaAmarillaForm.ObtenerObservaciones(IdConvenioInhabilitado: Integer; TraerBajas: Boolean): boolean;
begin
  Screen.Cursor := crHourGlass;
  with spObtenerObservacionesConvenioListaAmarilla do begin
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@IdConvenioInhabilitado').Value := IdConvenioInhabilitado;
        Parameters.ParamByName('@TraerBajas').Value := TraerBajas;
        try
            Open;
            Result:= True;
        except on e:exception do begin
                Screen.Cursor := crDefault;
                Result:=False;
            end;
        end;

    end;
  Screen.Cursor := crDefault;
end;

function TObservacionesConvenioListaAmarillaForm.GuardarObservacion(Observacion: string; IdConvenioInhabilitado, EstadoConvenio: Integer; Usuario:string; out MsgError: string): Boolean;
var
  Retorno : integer;
begin
  Screen.Cursor := crHourGlass;
  Result:= False;
  with spAgregarObservacionConvenioListaAmarilla do begin
      Parameters.Refresh;
      parameters.ParamByName('@IdConvenioInhabilitado').Value := IdConvenioInhabilitado ;
      parameters.ParamByName('@EstadoConvenio').Value := EstadoConvenio;
      parameters.ParamByName('@Observacion').Value := Observacion;
      parameters.ParamByName('@UsuarioCreacion').Value := Usuario;
      Parameters.ParamByName('@MensajeError').Value := EmptyStr;
      try
        ExecProc;
        Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno < 0 then begin
            MsgError := iif(Parameters.ParamByName('@MensajeError').Value = null, '', Parameters.ParamByName('@MensajeError').Value);
        end
        else begin
            MsgBox(MSG_GRABADO, Caption, MB_ICONINFORMATION);
            Limpiar;
            Result := True;
            exit;
        end;
        except on e:exception do begin
                MsgError := e.Message;
                Screen.Cursor := crDefault;
                exit;
            end;
        end;
  end;
  Screen.Cursor := crDefault;
end;

function TObservacionesConvenioListaAmarillaForm.EliminarObservacion(IdObservacion: Integer; Usuario: string; out MsgError: String): Boolean;
var
  Retorno : integer;
begin
  Screen.Cursor := crHourGlass;
  Result:= False;
  with spEliminarObservacionConvenioListaAmarilla do begin
      Parameters.Refresh;
      parameters.ParamByName('@IdObservacion').Value := IdObservacion;
      parameters.ParamByName('@Usuario').Value := Usuario;
      Parameters.ParamByName('@MensajeError').Value := EmptyStr;
      try
        ExecProc;
        Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno < 0 then begin
            MsgError := iif(Parameters.ParamByName('@MensajeError').Value = null, '', Parameters.ParamByName('@MensajeError').Value);
        end
        else begin
            MsgBox(MSG_ELIMINADO, Caption, MB_ICONINFORMATION);
            Limpiar;
            Result := True;
            exit;
        end;
        except on e:exception do begin
                MsgError := e.Message;
                Screen.Cursor := crDefault;
                exit;
            end;
        end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TObservacionesConvenioListaAmarillaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  action := caFree;
end;

end.
