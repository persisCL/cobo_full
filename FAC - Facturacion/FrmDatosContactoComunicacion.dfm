object FormDatosContactoComunicacion: TFormDatosContactoComunicacion
  Left = 215
  Top = 214
  BorderStyle = bsDialog
  Caption = 'Datos del Contacto'
  ClientHeight = 245
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 10
    Top = 29
    Width = 69
    Height = 13
    Caption = 'Documento:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblApellido: TLabel
    Left = 10
    Top = 53
    Width = 50
    Height = 13
    Caption = 'Apellido:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblNombre: TLabel
    Left = 10
    Top = 79
    Width = 62
    Height = 13
    Caption = 'Nombre(s):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 11
    Top = 104
    Width = 45
    Height = 13
    Caption = 'Domicilio:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblApellidoMaterno: TLabel
    Left = 369
    Top = 52
    Width = 81
    Height = 13
    Caption = 'Apellido materno:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 440
    Top = 104
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
  end
  object Label12: TLabel
    Left = 11
    Top = 133
    Width = 91
    Height = 13
    Caption = 'Tel'#233'fono particular:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label21: TLabel
    Left = 14
    Top = 156
    Width = 28
    Height = 13
    Caption = 'Email:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object txt_nombre: TEdit
    Left = 129
    Top = 75
    Width = 229
    Height = 21
    Color = 16444382
    TabOrder = 3
  end
  object txtDomicilio: TEdit
    Left = 129
    Top = 101
    Width = 296
    Height = 21
    MaxLength = 255
    TabOrder = 4
  end
  object txt_apellido: TEdit
    Left = 129
    Top = 50
    Width = 229
    Height = 21
    Color = 16444382
    TabOrder = 1
  end
  object mcTipoNumeroDocumento: TMaskCombo
    Left = 129
    Top = 25
    Width = 228
    Height = 21
    Items = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Color = 16444382
    ComboWidth = 107
    ItemIndex = -1
    OmitCharacterRequired = False
    TabOrder = 0
  end
  object txt_apellidoMaterno: TEdit
    Left = 459
    Top = 48
    Width = 215
    Height = 21
    TabOrder = 2
  end
  object txtNumeroCalle: TEdit
    Left = 486
    Top = 100
    Width = 116
    Height = 21
    MaxLength = 20
    TabOrder = 5
  end
  object Panel10: TPanel
    Left = 0
    Top = 204
    Width = 688
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 8
    object btnAceptar: TDPSButton
      Left = 486
      Top = 8
      Width = 97
      Caption = '&Asociar Datos'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnSalir: TDPSButton
      Left = 587
      Top = 8
      Width = 92
      Cancel = True
      Caption = '&Salir'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object txtTelefonoParticular: TEdit
    Left = 129
    Top = 126
    Width = 121
    Height = 21
    Hint = 'N'#250'mero de tel'#233'fono'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
  end
  object txtEmailParticular: TEdit
    Left = 129
    Top = 152
    Width = 228
    Height = 21
    Hint = 'direcci'#243'n de e-mail'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
  end
end
