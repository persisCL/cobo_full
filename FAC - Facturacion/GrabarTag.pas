unit GrabarTag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DmiCtrls, ExtCtrls, DbList, ComCtrls, Db, DBTables, UtilDB,
  PeaTypes, GrabacionMedioPago, UtilProc, Util, Grids, DBGrids, DPSGrid,
  ADODB, DMConnection,DateUtils,PeaProcs,Variants, DPSControls;

type
  TfrmGrabarTags = class(TForm)
    Panel2: TPanel;
    Panel1: TPanel;
	Qry_SinGrabar: TADOQuery;
    Grilla: TDPSGrid;
    DataSource: TDataSource;
    Panel3: TPanel;
    cb_antena: TCheckBox;
    cb_modo: TComboBox;
	cb_ubicacion: TComboBox;
    Label1: TLabel;
    Label3: TLabel;
    UpdateMaestro: TADOQuery;
    InsertAsignado: TADOQuery;
    EliminarPendiente: TADOQuery;
    RegistrarCambioUbicacion: TADOStoredProc;
    bt_grabar: TDPSButton;
    Button1: TDPSButton;
    ActualizarActionListEnPorceso: TADOStoredProc;
	procedure bt_grabarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cb_modoChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoOrdenServicio : integer;
    EsParaUnCliente: boolean;
	procedure RefrescarListaGrabacion;
    function  CodificarTag(ConAntena: Boolean; var DescriError: AnsiString):Boolean;
    function  EliminarTagCodificado(Cuenta: Integer; var DescriError: AnsiString):Boolean;
  public
    { Public declarations }
	function Inicializa(MDIChild: Boolean; OrdenServicio: integer): Boolean;
  end;


implementation

{$R *.DFM}

resourcestring
    CAPTION_CODIFICACION    = 'Codificaci�n de TAGs';


function TfrmGrabarTags.Inicializa(MDIChild: Boolean; OrdenServicio: integer): Boolean;
resourcestring
    MSG_NO_EXISTEN_TAGS		= 'No hay ning�n TAG para codificar.';
Var
	S: TSize;
    FCodigoCuenta: integer;
begin
	Result := false;
    EsParaUnCliente := false;
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
       	CenterForm(Self);
	end;

	Grilla.Columns.Items[0].Title.Caption := MSG_COL_TITLE_CUENTA;
	Grilla.Columns.Items[1].Title.Caption := MSG_COL_TITLE_CLIENTE;
	Grilla.Columns.Items[2].Title.Caption := MSG_COL_TITLE_CATEGORIA;
	Grilla.Columns.Items[3].Title.Caption := MSG_COL_TITLE_PATENTE;

	if not OpenTables([qry_SinGrabar]) then Exit;
	FCodigoOrdenServicio := OrdenServicio;
    if (OrdenServicio > 0) then begin
    	FCodigoCuenta := QueryGetValueInt(DMConnections.BaseCAC,
        	'SELECT CodigoCuenta FROM OrdenesServicioAdhesion ' +
            ' WHERE CodigoOrdenServicio = ' + IntToStr(OrdenServicio));
		qry_SinGrabar.Filtered	:= True;
        qry_SinGrabar.Filter	:= 'CodigoCuenta = ' + IntToStr(FCodigoCuenta);
        EsParaUnCliente         := true;
    end;

	if not (qry_SinGrabar.RecordCount > 0) then begin
		MsgBox(MSG_NO_EXISTEN_TAGS, CAPTION_CODIFICACION, MB_ICONINFORMATION);
	end else begin
//Pablo		CargarUbicacionesTags(DMCOnnections.BaseCAC,cb_ubicacion, 1);
       	cb_modo.ItemIndex			:= 1;
        cb_ubicacion.ItemIndex 		:= 2;
        if (OrdenServicio > 0) then begin
            cb_Modo.Enabled			:= False;
			cb_ubicacion.Enabled	:= False;
            ActiveControl			:= bt_grabar;
        end else begin
			cb_Modo.Enabled			:= True;
			cb_ubicacion.Enabled	:= True;
			ActiveControl 			:= cb_modo;
        end;
		Qry_SinGrabar.Last;
		Result := qry_SinGrabar.recordcount > 0;
	end;

end;

procedure TfrmGrabarTags.bt_grabarClick(Sender: TObject);
resourcestring
    MSG_ERROR_CODIFICACION = 'No se ha podido Codificar el TAG Indicado.';
    MSG_CONTINUACION       = 'Por favor, retire el TAG de la antena.';
var descriError: AnsiString;
begin
	// Determinamos que tipo de Codificacion vamos a usar, Automatica o Individual
	case cb_modo.ItemIndex of
		0:  begin
				(* Modo Automatico *)
				Grilla.SetFocus;
				with qry_SinGrabar do begin
					First;
					while not eof do begin
						(* Codificamos cada tag de la lista segun se use antena o no *)
						if CodificarTag(cb_antena.Checked, descriError) then begin
							MsgBox(MSG_CONTINUACION, CAPTION_CODIFICACION, MB_ICONINFORMATION);
						end else begin
							MsgBoxErr(MSG_ERROR_CODIFICACION, DescriError, CAPTION_CODIFICACION, MB_ICONSTOP);
							Exit;
						end;
						Next;
					end;
				end;
				RefrescarListaGrabacion;
			end;
		1:  begin
				(* Modo Manual *)
				if CodificarTag(cb_antena.Checked, DescriError) then begin
                    if not EsParaUnCliente then
    					RefrescarListaGrabacion;
				end else begin
//    				MsgBoxErr(MSG_ERROR_CODIFICACION, DescriError, CAPTION_CODIFICACION, MB_ICONSTOP);
                    Exit;
				end;
			end;
	end;
	if (FCodigoOrdenServicio > 0) then ModalResult := mrOk;
end;

procedure TfrmGrabarTags.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TfrmGrabarTags.Button1Click(Sender: TObject);
begin
	Close;
end;

procedure TfrmGrabarTags.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;

procedure TfrmGrabarTags.cb_modoChange(Sender: TObject);
begin
	if cb_modo.ItemIndex = 0 then begin
		cb_ubicacion.Enabled	:= false;
		cb_ubicacion.ItemIndex	:= 0;
	end else begin
		cb_ubicacion.Enabled	:= true;
	end;
end;

function TfrmGrabarTags.CodificarTag(ConAntena: Boolean; var DescriError: AnsiString): Boolean;

	function ObtenerUbicacionTag(ContextMark: Integer; ContractSerialNumber: AnsiString): Integer;
	begin
		Result := QueryGetValueInt(DMConnections.BaseCAC, Format(
		  'SELECT CodigoUbicacion FROM MaestroTags WHERE ' +
		  'ContextMark = %d AND ContractSerialNumber = %s',
		  [ContextMark, ContractSerialNumber]));
	end;
resourcestring
    MSG_CUENTAS = 'Cuentas: %s ';
Const
	MAXTAGYEARSLIFE : Integer = 7;
var
	MPData: TMPData;
	NumeroTAG: AnsiString;
	Context_Mark : Integer;
	f: TGrabarMedioPagoForm;
begin
    DescriError := '';
	Result := false;
	// Primero vaciamos el MPData
	FillChar(MPData, SizeOf(MPData), 0);
	// Static
	MPData.Static.Categoria := qry_SinGrabar.FieldByName('Categoria').AsInteger;
	// Grabar
	Application.CreateForm(TGrabarMedioPagoForm, f);
	f.Show;
	f.Update;
	if f.Inicializa(MPData, ConAntena,
        Format(MSG_CUENTAS, [qry_Singrabar.fieldbyname('CodigoCuenta').AsString]),
        CODIFICACION_ASIGNACION) then begin
		f.Hide;
		if (f.ShowModal = mrOk) then begin
			Update;
			NumeroTAG := f.NumeroTAG;
			Context_Mark := f.ContextMark;
			Screen.cursor := crHourGlass;

			try
            	DMConnections.BaseCAC.BeginTrans;
				try
					(* Guardamos la nueva ubicaci�n del Tag *)
					with RegistrarCambioUbicacion do begin
						  Parameters.ParamByName('@ContextMark').value                := Context_Mark;
						  Parameters.ParamByName('@ContractSerialNumber').value       := StrToFloat(NumeroTag);
						  Parameters.ParamByName('@CodigoUbicacionInicial').value     := ObtenerUbicacionTag(Context_Mark, NumeroTag);
						  Parameters.ParamByName('@CodigoUbicacionFinal').value       := IVal(StrRight(cb_ubicacion.text, 10));
						  Parameters.ParamByName('@FechaHoraPactada').value           := Now;
						  Parameters.ParamByName('@FechaHoraLlegada').Value           := Now;
						  Parameters.ParamByName('@CodigoCuentaAsignada').value       := qry_SinGrabar.fieldbyname('CodigoCuenta').AsInteger;
						  Parameters.ParamByName('@Estado').value                     := 'N';
						  ExecProc;
					end;
					(* Actualizamos los asignados *)
					with InsertAsignado do begin
						Parameters.ParamByName('Tag').Value                     := NumeroTAG;
						Parameters.ParamByName('Contextmark').Value             := Context_Mark;
						Parameters.ParamByname('Vencimiento').Value             := IncYear(Now,MAXTAGYEARSLIFE);
						Parameters.ParamByName('FechaAlta').Value               := Now;
						Parameters.ParamByName('FechaHoraActualizacion').Value  := Now;
						Parameters.ParamByName('Cuenta').Value                  := qry_SinGrabar.fieldbyname('CodigoCuenta').AsInteger;
						ExecSql;
					end;
					(* Actualizamos el Maestro *)
					with UpdateMaestro do begin
						Parameters.ParamByName('Tag').Value                     := NumeroTAG;
						Parameters.ParamByName('Contextmark').Value             := Context_Mark;
						Parameters.ParamByName('Cuenta').Value                  := qry_SinGrabar.fieldbyname('CodigoCuenta').AsInteger;
						Parameters.ParamByName('Ubicacion').Value               := Ival(StrRight(cb_ubicacion.text, 10));
						ExecSql;
					end;
                    (* Agregamos el TAG a la action List *)
                    with ActualizarActionListEnPorceso do begin
                        Parameters.ParamByName('@ContextMark').value := Context_Mark;
                        Parameters.ParamByName('@ContractSerialNumber').value := NumeroTag;
                        Parameters.ParamByName('@ClearBlackList').Value := not f.CBNegra.Checked;
                        Parameters.ParamByName('@SetBlackList').Value := f.CBNegra.Checked;
                        Parameters.ParamByName('@ClearGreenList').Value := not f.CBVerde.Checked;
                        Parameters.ParamByName('@SetGreenList').Value := f.CBVerde.Checked;
                        Parameters.ParamByName('@ClearGrayList').Value := not f.CBGris.Checked;
                        Parameters.ParamByName('@SetGrayList').Value := f.cbGris.Checked;
                        Parameters.ParamByName('@ClearYellowList').Value := not f.CBAmarilla.Checked;
                        Parameters.ParamByName('@SetYellowList').Value := f.CBAmarilla.Checked;
                        Parameters.ParamByName('@ExceptionHandling').Value := False;
                        Parameters.ParamByName('@MMINotOk').Value := False;
                        Parameters.ParamByName('@MMIContactOperator').Value := False;
                        ExecProc;
                    end;
					// Eliminamos el Tag que se codifico de la lista de pendientes
					if not EliminarTagCodificado(qry_SinGrabar.FieldByname('CodigoCuenta').AsInteger, DescriError) then begin
                        exit;
                    end;
					DMConnections.BaseCAC.CommitTrans;
					// Listo.
					Result := true;
				except
					on E: Exception do begin
						DescriError := e.Message;
					end;
				end;
			finally
				if not Result then DMConnections.BaseCAC.RollbackTrans;
				Screen.Cursor := crDefault;
			end;
		end;
	end;
	f.Release;
end;

procedure TfrmGrabarTags.RefrescarListaGrabacion;
resourcestring
    MSG_FINALIZACION = 'No hay m�s TAGs para codificar.';
begin
	qry_SinGrabar.Active := False;
	qry_SinGrabar.Active := True;
	if (qry_SinGrabar.IsEmpty) then begin
		MsgBox(MSG_FINALIZACION, CAPTION_CODIFICACION, MB_ICONINFORMATION);
		Close;
	end;
end;

function TfrmGrabarTags.EliminarTagCodificado(Cuenta: Integer; var DescriError: AnsiString): Boolean;
begin
    DescriError := '';
	Screen.Cursor := crHourGlass;
	Result := false;
	// Eliminamos de la tabla temporaria el Tag que se codifico.
	try
		try
			EliminarPendiente.Parameters.ParamByName('Cuenta').Value := Cuenta;
			EliminarPendiente.ExecSQL;
			Result := (EliminarPendiente.RowsAffected = 1);
		except
			On E: Exception do
                DescriError := e.Message;
		end;
	finally
		Screen.Cursor := crDefault;
	end;
end;


end.
