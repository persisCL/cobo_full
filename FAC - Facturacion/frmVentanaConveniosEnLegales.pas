{------------------------------------------------------------------------
        frmVentanaConveniosEnLegales

    Author      : MDI
    Date        : 14-03-2013
    Description :   (Ref SS 660)
                    Muestra el resumen de convenios sacados de carpetas legales
                    al ser pasados de carta enviada a inhabilitados lista amarilla

  Autor         : CQuezada
  Fecha         : 02-07-2013
  Firma         : SS_660_CQU_20130628
  Descripci�n   : Se quita un Label, se agrega el texto de lbl2 al lbl1
                  y se modifican las dimensiones del formulario.

  Autor        : CQuezadaI
  Fecha        : 20-07-2013
  Firma        : SS_660_CQU_20130711
  Descripci�n  : Se modifica el mensaje y se formatea el n�mero de convenio.

  Firma         : SS_660_NDR_20131112
  Descripcion   : En la ventana de aviso se duplican los convenios
                  (porque tienen diferente concesionaria). Se debe mostrar
                  solo 1 vez
-------------------------------------------------------------------------}

unit frmVentanaConveniosEnLegales;

interface

uses
    constParametrosGenerales,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, ListBoxEx, DBListEx, ExtCtrls, DmConnection,
  DB, ADODB, VariantComboBox, UtilProc, Util, PeaProcs,
  DmiCtrls, SysUtilsCN, PeaTypes, DBClient, MidasLib, Grids, DBGrids;

type
  TVentanaConveniosEnLegales = class(TForm)
    lbl1: TLabel;
    //lbl2: TLabel; // SS_660_CQU_20130628
    lstConvenios: TListBox;
    btnCerrar: TButton;
    procedure btnCerrarClick(Sender: TObject);
  private
    FListadoConveniosLegales : TClientDataSet;
  public
    function Inicializar(ListadoConveniosLegales : TClientDataSet):Boolean;
  end;

var
  VentanaConveniosEnLegales: TVentanaConveniosEnLegales;

implementation

{$R *.dfm}

procedure TVentanaConveniosEnLegales.btnCerrarClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    MDI
  Date Created: 14/03/2013
  Description: Inicializa el formulario y carga la info de los convenios sacados de legales
  Parameters: ListadoConveniosLegales: TClientDataSet
  Return Value: boolean
-----------------------------------------------------------------------------}
function TVentanaConveniosEnLegales.Inicializar(ListadoConveniosLegales: TClientDataSet) : Boolean;
begin

    FListadoConveniosLegales := TClientDataSet.Create(Self);
    FListadoConveniosLegales := ListadoConveniosLegales;

    FListadoConveniosLegales.First;

    while not FListadoConveniosLegales.Eof do begin
         if (lstConvenios.Items.IndexOf(FListadoConveniosLegales.FieldByName('NumeroConvenio').AsString)=-1) then  //SS_660_NDR_20131112
             lstConvenios.Items.Add(FListadoConveniosLegales.FieldByName('NumeroConvenio').AsString);              //SS_660_NDR_20131112
         FListadoConveniosLegales.Next;
    end;

    Result := True;
end;

end.
