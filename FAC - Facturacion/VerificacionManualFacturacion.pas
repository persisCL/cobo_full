unit VerificacionManualFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, validate,
  Dateedit, Buttons, ExtCtrls, utilProc, DmiCtrls, util, utilDB, ADODB, DMConnection, ComCtrls, BuscaClientes,
  DPSGrid, peaTypes, peaProcs, DB, Grids, DBGrids, DPSControls, ListBoxEx,DBListEx, Navigator;

type
  TfrmVerificacionManualFacturacion = class(TForm)
    dsFacturas: TDataSource;
    dsDatosComprobante: TDataSource;
    ObtenerDatosComprobante: TADOStoredProc;
    dsFacturacionDetallada: TDataSource;
    ObtenerFacturacionDetallada: TADOStoredProc;
    pcDetalleComprobante: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    gbMovDisponibles: TGroupBox;
    dbgListaFacturas: TDPSGrid;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    cbProcesosFacturacion: TComboBox;
    ObtenerMuestreoProcesosFacturacion: TADOStoredProc;
    nePorcentaje: TNumericEdit;
	Label1: TLabel;
	Label2: TLabel;
	lCantidadComprobantes: TLabel;
	CantidadDeComprobantes: TADOQuery;
	ObtenerProcesosFacturacionSinImprimir: TADOStoredProc;
    dbgDetalles: TDBListEx;
    dbgDetalleViajes: TDBListEx;
    btnSalir: TButton;
    btnActualizar: TButton;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnSalirClick(Sender: TObject);
	procedure dsFacturasDataChange(Sender: TObject; Field: TField);
	procedure cbProcesosFacturacionChange(Sender: TObject);
	procedure dsDatosComprobanteDataChange(Sender: TObject; Field: TField);
	procedure nePorcentajeChange(Sender: TObject);
	procedure btnActualizarClick(Sender: TObject);
	procedure dbgListaFacturasOrderChange(Sender: TDPSGrid;
	  var ACol: Integer; var OrderIndicator: TOrderIndicator);
    procedure dbgListaFacturasTitleClick(Column: TColumn);
    procedure dbgListaFacturasWillClickTitle(Sender: TDPSGrid;
      Column: TColumn; var ExecuteAction: Boolean);
    procedure dbgDetalleViajesLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
  private
	{ Private declarations }
    FNavigator : TNavigator;
	FColumnOrderly: Integer;
	FOrderIndicator: TOrderIndicator;

	procedure CargarComprobantes(CamposOrden: AnsiString);
  public
	{ Public declarations }
	function inicializa(Navigator : TNavigator = nil): Boolean;
  end;


implementation

{$R *.dfm}

{ TfrmAnularReimprimirFactura }



function TfrmVerificacionManualFacturacion.inicializa(Navigator : TNavigator = nil): Boolean;
resourcestring
    MSG_FACTURAS_SIN_IMPRIMIR = '%s - %s - Facturas Sin Imprimir: %d';
    MSG_PROCESOS_FACTURACION = 'Todos los Procesos de Facturaci�n - Facturas Sin Imprimir: %d';
var
    CantidadComprobantesSinImprimir: Integer;
begin

  FNavigator := Navigator;

  with dbgListaFacturas do begin
        Columns[0].Title.Caption := MSG_COL_TITLE_COMPROBANTE;
        Columns[1].Title.Caption := MSG_COL_TITLE_FECHA;
        Columns[2].Title.Caption := MSG_COL_TITLE_FECHA_VENC;
        Columns[3].Title.Caption := MSG_COL_TITLE_IMPORTE;
        Columns[4].Title.Caption := MSG_COL_TITLE_DEBITO;
    end;

	with dbgDetalles do begin
		Columns[0].Header.Caption := MSG_COL_TITLE_CONCESIONARIA;
		Columns[1].Header.Caption := MSG_COL_TITLE_FECHA;
		Columns[2].Header.Caption := MSG_COL_TITLE_CUENTA;
		Columns[3].Header.Caption := MSG_COL_TITLE_DESCRIPCION;
		Columns[4].Header.Caption := MSG_COL_TITLE_IMPORTE;
	end;

	with dbgDetalleViajes do begin
		Columns[0].Header.Caption := MSG_COL_TITLE_CONCESIONARIA;
		Columns[1].Header.Caption := MSG_COL_TITLE_NUMERO_VIAJE;
		Columns[2].Header.Caption := MSG_COL_TITLE_PATENTE;
		Columns[3].Header.Caption := MSG_COL_TITLE_CATEGORIA;
		Columns[4].Header.Caption := MSG_COL_TITLE_FH_INICIO;
		Columns[5].Header.Caption := MSG_COL_TITLE_PUNTOS_COBRO;
		Columns[6].Header.Caption := MSG_COL_TITLE_IMPORTE;
	end;

	result := true;
	Width  := GetFormClientSize(Application.MainForm).cx;
	Height := GetFormClientSize(Application.MainForm).cy;
	CenterForm(Self);

	screen.Cursor := crHourGlass;
	try
		try
			cbProcesosFacturacion.Items.Clear;
            CantidadComprobantesSinImprimir := 0;
			with ObtenerProcesosFacturacionSinImprimir do begin
				open;
				while not eof do begin
					cbProcesosFacturacion.items.Add(format(MSG_FACTURAS_SIN_IMPRIMIR,
                      [FormatDateTime('dd/mm/yyyy',FieldByName('FechaHora').asDateTime),
                      trim(FieldByName('Operador').asString),
					  FieldByName('CantFacturasSinImprimir').asInteger])  + space(200) +
					  FieldByName('NumeroProcesoFacturacion').asString);
                      CantidadComprobantesSinImprimir := CantidadComprobantesSinImprimir + FieldByName('CantFacturasSinImprimir').AsInteger;
					next;
				end;
				close;
			end;

//			CantidadComprobantesSinImprimir := QueryGetValueInt(DMConnections.BaseCAC,
//			  'Select Count(*) from Comprobantes ' +
//			  'where Comprobantes.FechaHoraImpreso IS NULL and Comprobantes.Estado = ''I''');

			cbProcesosFacturacion.items.Insert(0,
              Format(MSG_PROCESOS_FACTURACION, [CantidadComprobantesSinImprimir]) + space(200) + '0');
			cbProcesosFacturacion.ItemIndex := 0;
		except
			result := False;
		end;
	finally
		screen.Cursor := crDefault;
	end;
	update;
	nePorcentaje.value := 10.00;

	FColumnOrderly := 0;
	FOrderIndicator := oiUp;
	CargarComprobantes('TipoComprobante, NumeroComprobante');
end;


procedure TfrmVerificacionManualFacturacion.CargarComprobantes(CamposOrden: AnsiString);
resourcestring
    MSG_MUESTRA_FACTURACION = 'La muestra a generar puede contener m�s de 500 comprobantes. ' +
      '�Desea generar igualmente la muestra?';
    CAPTION_MUESTRA_FACTURACION = 'Verificaci�n Manual de Facturaci�n';
var CantidadComprobantesSinImprimir, grupo: Integer;
begin
// Cargamos la informacion del comprobante
	screen.Cursor := crHourGlass;
	try
		grupo := IVal(strRight(cbProcesosFacturacion.Items[cbProcesosFacturacion.ItemIndex],20));
		if grupo = 0 then begin
			CantidadComprobantesSinImprimir := QueryGetValueInt(DMConnections.BaseCAC,
			  'Select Count(*) from Comprobantes ' +
			  'where Comprobantes.FechaHoraImpreso IS NULL and Comprobantes.Estado = ''I''');
		end else begin
			CantidadDeComprobantes.parameters.ParamByname('NumeroProcesoFacturacion').value := grupo;
			CantidadDeComprobantes.open;
			CantidadComprobantesSinImprimir := CantidadDeComprobantes.FieldByName('CantidadComprobantes').asInteger;
		end;
		if (CantidadComprobantesSinImprimir * (nePorcentaje.value / 100)  > 500) then
			if MsgBox(MSG_MUESTRA_FACTURACION, CAPTION_MUESTRA_FACTURACION, MB_YESNO + MB_ICONQUESTION) <> IDYES then
				Exit;
		With ObtenerMuestreoProcesosFacturacion do begin
			close;
			Parameters.ParamByName('@NumeroProcesoFacturacion').Value :=
			  IVal(strRight(cbProcesosFacturacion.Items[cbProcesosFacturacion.ItemIndex],20));
			Parameters.ParamByName('@Porcentaje').Value := nePorcentaje.value;
			open;
			sort := CamposOrden;
			lCantidadComprobantes.Caption := intToStr(recordCount);
		end;
	finally
		btnActualizar.enabled := false;
		CantidadDeComprobantes.Close;
		screen.Cursor := crDefault;
	end;
end;

procedure TfrmVerificacionManualFacturacion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmVerificacionManualFacturacion.btnSalirClick(Sender: TObject);
begin
    Close;
end;


procedure TfrmVerificacionManualFacturacion.dsFacturasDataChange(Sender: TObject;
  Field: TField);
begin
    if field = nil then begin
        screen.Cursor := crHourGlass;
        try
            ObtenerDatosComprobante.close;
            ObtenerDatosComprobante.Parameters.ParamByName('@TipoComprobante').Value :=
                (TDataSource(Sender).dataset as TADOStoredProc).fieldByName('TipoComprobante').asString;
            ObtenerDatosComprobante.Parameters.ParamByName('@NumeroComprobante').Value :=
                (TDataSource(Sender).dataset as TADOStoredProc).fieldByName('NumeroComprobante').asinteger;
            ObtenerDatosComprobante.open;
		finally
			screen.Cursor := crDefault;
		end;
	end;
end;

procedure TfrmVerificacionManualFacturacion.cbProcesosFacturacionChange(
  Sender: TObject);
begin
	btnActualizar.enabled := true;
end;

procedure TfrmVerificacionManualFacturacion.dsDatosComprobanteDataChange(
  Sender: TObject; Field: TField);
begin
	if Field = nil then begin
		ObtenerFacturacionDetallada.Close;
		if not ObtenerDatosComprobante.Eof then Begin
			 with ObtenerFacturacionDetallada do begin
				 Parameters.ParamByName('@CodigoConvenio').Value := ObtenerDatosComprobante.FieldByName('CodigoConvenio').asInteger;
				 Parameters.ParamByName('@TipoComprobante').Value := ObtenerDatosComprobante.FieldByName('TipoComprobante').asString[1];
				 Parameters.ParamByName('@NumeroComprobante').Value := ObtenerDatosComprobante.FieldByName('NumeroComprobante').asFloat;
				 Open;
			end;
		end;
	end;
end;

procedure TfrmVerificacionManualFacturacion.nePorcentajeChange(
  Sender: TObject);
begin
	btnActualizar.enabled := true;
end;

procedure TfrmVerificacionManualFacturacion.btnActualizarClick(
  Sender: TObject);
begin
	FColumnOrderly := 0;
	FOrderIndicator := oiUp;
	CargarComprobantes('TipoComprobante, NumeroComprobante');
end;

procedure TfrmVerificacionManualFacturacion.dbgListaFacturasOrderChange(
  Sender: TDPSGrid; var ACol: Integer;
  var OrderIndicator: TOrderIndicator);
begin
	ACol := FColumnOrderly;
	OrderIndicator := FOrderIndicator;
end;

procedure TfrmVerificacionManualFacturacion.dbgListaFacturasTitleClick(
  Column: TColumn);
begin
	FColumnOrderly := Column.Index;
	with dbgListaFacturas do begin
		// Esta ordenado por un campo y ese campo es el presionado ahora cambiamos el orden
		// Si esta para ariba asignamos orden descendiente
		// Caso contrario asignamos el orden del campo presionado y por default ascendente
		if (ColumnOrderly > -1) and
		  (Column.FieldName = Columns.Items[ColumnOrderly].FieldName) and
		  (OrderIndicator = oiUp) then begin
			FOrderIndicator := oiDown;
			if Column.FieldName = 'DescriComprobanteNumero' then
				ObtenerMuestreoProcesosFacturacion.Sort := 'TipoComprobante, NumeroComprobante DESC'
			else
				ObtenerMuestreoProcesosFacturacion.Sort := Column.FieldName + ' DESC';
		end else begin
			FOrderIndicator := oiUp;
			if Column.FieldName = 'DescriComprobanteNumero' then
				ObtenerMuestreoProcesosFacturacion.Sort := 'TipoComprobante, NumeroComprobante DESC'
			else
				ObtenerMuestreoProcesosFacturacion.Sort := Column.FieldName + ' DESC';
		end;
	end;
end;

procedure TfrmVerificacionManualFacturacion.dbgListaFacturasWillClickTitle(
  Sender: TDPSGrid; Column: TColumn; var ExecuteAction: Boolean);
begin
	ExecuteAction := true;
end;

procedure TfrmVerificacionManualFacturacion.dbgDetalleViajesLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
{	if (Column.FieldName = 'NumeroViaje') and (not ObtenerFacturacionDetallada.IsEmpty) then begin

    	 FNavigator.JumpTo(TNavWindowDetalleViaje,
	      TNavWindowDetalleViaje.CreateBookmark(
		  ObtenerFacturacionDetallada.FieldByName('InfraccionSaldoInsuficiente').asBoolean,
		  ObtenerFacturacionDetallada.FieldByName('InfraccionTAGInhabilitado').asBoolean,
		  ObtenerMuestreoProcesosFacturacion.FieldByName('CodigoPersona').AsInteger,
		  ObtenerFacturacionDetallada.FieldByName('CodigoConcesionaria').asInteger,
		  ObtenerFacturacionDetallada.FieldByName('NumeroViaje').asInteger,
		  ObtenerFacturacionDetallada.FieldByName('Patente').asString,
		  ObtenerFacturacionDetallada.FieldByName('Concesionaria').asString,
		  ObtenerFacturacionDetallada.FieldByName('ContractSerialNumber').asFloat,
		  ObtenerFacturacionDetallada.FieldByName('FechaHoraInicio').asDateTime, 0,
		  ObtenerFacturacionDetallada.FieldByName('PuntosCobro').asString,
		  ObtenerFacturacionDetallada.FieldByName('Importe').asFloat));

	end;}

end;

end.
