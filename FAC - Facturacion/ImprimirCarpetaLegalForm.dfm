object frmImprimirCarpetaLegal: TfrmImprimirCarpetaLegal
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Impresi'#243'n en Carpeta Legal'
  ClientHeight = 144
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblMensaje: TLabel
    Left = 40
    Top = 24
    Width = 249
    Height = 17
    Alignment = taCenter
    AutoSize = False
    Caption = 'MENSAJE'
  end
  object chkImprimirDetalle: TCheckBox
    Left = 88
    Top = 61
    Width = 147
    Height = 17
    Caption = 'Imprimir Detalle Tr'#225'nsitos '
    TabOrder = 0
    OnClick = chkImprimirDetalleClick
  end
  object btnAceptar: TButton
    Left = 64
    Top = 96
    Width = 75
    Height = 25
    Caption = '&Imprimir'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 200
    Top = 96
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
end
