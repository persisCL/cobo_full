object AnulacionNotaCobroDirectaForm: TAnulacionNotaCobroDirectaForm
  Left = 0
  Top = 0
  Caption = 'AnulacionNotaCobroDirectaForm'
  ClientHeight = 478
  ClientWidth = 671
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object gbDocumento: TGroupBox
    Left = 0
    Top = 0
    Width = 671
    Height = 105
    Align = alTop
    Caption = 'Documento a Anular'
    TabOrder = 0
    DesignSize = (
      671
      105)
    object Label1: TLabel
      Left = 48
      Top = 32
      Width = 137
      Height = 13
      Caption = 'Numero Documento Interno:'
    end
    object rbtnPDU: TRadioButton
      Left = 200
      Top = 41
      Width = 113
      Height = 17
      Caption = 'PDU'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbtnPDUClick
    end
    object rbtnBHTU: TRadioButton
      Left = 200
      Top = 64
      Width = 113
      Height = 17
      Caption = 'BHTU'
      TabOrder = 1
      OnClick = rbtnBHTUClick
    end
    object btnAgregar: TButton
      Left = 296
      Top = 46
      Width = 75
      Height = 25
      Caption = 'Agregar'
      TabOrder = 2
      OnClick = btnAgregarClick
    end
    object btnSalir: TButton
      Left = 568
      Top = 16
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 3
      OnClick = btnSalirClick
    end
    object neDocumento: TNumericEdit
      Left = 48
      Top = 51
      Width = 137
      Height = 21
      TabOrder = 4
    end
  end
  object dbDetalles: TGroupBox
    Left = 0
    Top = 105
    Width = 671
    Height = 303
    Align = alClient
    Caption = 'Detalle del Comprobante a Generar'
    TabOrder = 1
    object lstvDetalle: TListView
      Left = 2
      Top = 15
      Width = 667
      Height = 233
      Align = alClient
      Columns = <
        item
          Caption = 'Tipo Documento'
          Width = 100
        end
        item
          Caption = 'N'#250'mero Documento'
          Width = 150
        end
        item
          Alignment = taRightJustify
          Caption = 'Monto'
          Width = 120
        end>
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
    end
    object GroupBox2: TGroupBox
      Left = 2
      Top = 248
      Width = 667
      Height = 53
      Align = alBottom
      TabOrder = 1
      object Label2: TLabel
        Left = 328
        Top = 24
        Width = 128
        Height = 16
        Caption = 'Total Comprobante:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTotal: TLabel
        Left = 470
        Top = 24
        Width = 46
        Height = 16
        Caption = 'lblTotal'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 408
    Width = 671
    Height = 70
    Align = alBottom
    TabOrder = 2
    object btnGenerar: TButton
      Left = 200
      Top = 24
      Width = 225
      Height = 25
      Caption = 'Generar Nota de Cr'#233'dito Electr'#243'nica'
      TabOrder = 0
      OnClick = btnGenerarClick
    end
  end
  object spAnularNotaCobroNQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AnularNotaCobroNQ'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 512
    Top = 16
  end
end
