object FormUltimosConsumos: TFormUltimosConsumos
  Left = 130
  Top = 190
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Anchors = []
  Caption = #218'ltimos Consumos'
  ClientHeight = 514
  ClientWidth = 884
  Color = clBtnFace
  Constraints.MinHeight = 504
  Constraints.MinWidth = 720
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    884
    514)
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Parametros: TGroupBox
    Left = 4
    Top = 0
    Width = 876
    Height = 60
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Par'#225'metros de b'#250'squeda'
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object Label2: TLabel
      Left = 145
      Top = 18
      Width = 53
      Height = 13
      Caption = 'Convenios:'
    end
    object lb_CodigoCliente: TLabel
      Left = 9
      Top = 18
      Width = 78
      Height = 13
      Caption = 'N'#250'mero de RUT'
    end
    object Label3: TLabel
      Left = 367
      Top = 18
      Width = 42
      Height = 13
      Caption = 'Cuentas:'
    end
    object peNumeroDocumento: TPickEdit
      Left = 9
      Top = 33
      Width = 133
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnChange = peNumeroDocumentoChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenio: TVariantComboBox
      Left = 144
      Top = 33
      Width = 217
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
      OnChange = ActualizarCombos
      OnDrawItem = cbConvenioDrawItem
      Items = <>
    end
    object cbCuenta: TVariantComboBox
      Left = 367
      Top = 33
      Width = 193
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = ActualizarCombos
      Items = <>
    end
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 62
    Width = 873
    Height = 63
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos del Cliente'
    Color = clBtnFace
    ParentColor = False
    TabOrder = 1
    DesignSize = (
      873
      63)
    object lApellidoNombre: TLabel
      Left = 72
      Top = 19
      Width = 444
      Height = 16
      Anchors = [akLeft, akTop, akBottom]
      AutoSize = False
      Caption = 'Apellido Cliente'
    end
    object Label4: TLabel
      Left = 12
      Top = 19
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LDomicilio: TLabel
      Left = 72
      Top = 39
      Width = 620
      Height = 16
      Anchors = [akLeft, akTop, akBottom]
      AutoSize = False
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 12
      Top = 39
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 128
    Width = 869
    Height = 381
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 0
      Top = 153
      Width = 869
      Height = 7
      Cursor = crVSplit
      Align = alTop
      Color = clBtnFace
      ParentColor = False
    end
    object gb_MovSeleccionados: TGroupBox
      Left = 0
      Top = 0
      Width = 869
      Height = 153
      Align = alTop
      Caption = 'Ultimos Consumos'
      Color = clBtnFace
      ParentColor = False
      TabOrder = 0
      DesignSize = (
        869
        153)
      object label9: TLabel
        Left = 705
        Top = 132
        Width = 56
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = 'Monto total:'
      end
      object lTotalProximosMovimientos: TLabel
        Left = 769
        Top = 132
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = 'Monto Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbgFacturas: TDBListEx
        Left = 2
        Top = 16
        Width = 858
        Height = 112
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 300
            Header.Caption = 'Concepto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CONCEPTO'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DESCIMPORTE'
          end>
        DataSource = dsUltimosConsumos
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
    end
    object gb_MovDisponibles: TGroupBox
      Left = 0
      Top = 160
      Width = 869
      Height = 221
      Align = alClient
      Caption = 'Detalle de Tr'#225'nsitos '
      Color = clBtnFace
      ParentColor = False
      TabOrder = 1
      DesignSize = (
        869
        221)
      object dbgDetalles: TDBListEx
        Left = 6
        Top = 16
        Width = 857
        Height = 199
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Fecha/Hora'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbgDetallesColumns0HeaderClick
            FieldName = 'FECHAHORA'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dbgDetallesColumns0HeaderClick
            FieldName = 'PATENTE'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 160
            Header.Caption = 'Punto de Cobro'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dbgDetallesColumns0HeaderClick
            FieldName = 'PUNTOCOBRO'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Categor'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dbgDetallesColumns0HeaderClick
            FieldName = 'CATEGORIA'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Tipo Horario'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dbgDetallesColumns0HeaderClick
            FieldName = 'TIPOHORARIO'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dbgDetallesColumns0HeaderClick
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Concesionaria'
          end>
        DataSource = dsUltimosConsumosDetalle
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnContextPopup = dbgDetallesContextPopup
        OnDrawText = dbgDetallesDrawText
      end
    end
  end
  object ObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 360
    Top = 71
  end
  object dsUltimosConsumos: TDataSource
    DataSet = spObtenerUltimosConsumos
    Left = 48
    Top = 175
  end
  object spObtenerUltimosConsumos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = spObtenerUltimosConsumosAfterOpen
    AfterClose = spObtenerUltimosConsumosAfterClose
    ProcedureName = 'ObtenerUltimosConsumos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 80
    Top = 175
  end
  object dsUltimosConsumosDetalle: TDataSource
    DataSet = spObtenerUltimosConsumosDetalle
    Left = 99
    Top = 406
  end
  object spObtenerUltimosConsumosDetalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerUltimosConsumosDetalle;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 264
    Top = 360
  end
  object tmrConsulta: TTimer
    Enabled = False
    OnTimer = tmrConsultaTimer
    Left = 342
    Top = 414
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 98
    Top = 361
  end
end
