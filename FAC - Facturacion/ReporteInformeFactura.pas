unit ReporteInformeFactura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppStrtch, ppRegion, ppClass, ppVar,
  ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppDBBDE, ppComm, ppRelatv,
  ppDBPipe, DB, ADODB, DBClient, DMConnection, peaProcs, RBSetup,
  ppParameter;

type
  TformReporteInformeFactura = class(TForm)
    dsDatosCliente: TDataSource;
    rp_Factura: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel12: TppLabel;
    ppLine1: TppLine;
    ppLabel16: TppLabel;
    ppLabel1: TppLabel;
    ppDBText10: TppDBText;
    ppLabel6: TppLabel;
    ppDBText2: TppDBText;
    lApellidoNombre: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText3: TppDBText;
    NroItem: TppVariable;
    ppFooterBand1: TppFooterBand;
    ppLine3: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppRegion1: TppRegion;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppRegion2: TppRegion;
    ppLabel18: TppLabel;
    lCuentaDebito: TppLabel;
    Fact_Vencimiento: TppLabel;
    rbi_Factura: TRBInterface;
    ItemsComprobante: TClientDataSet;
    ppSystemVariable1: TppSystemVariable;
    lFechaVencimiento: TppLabel;
    dsDetalle: TDataSource;
    ppDetalle: TppDBPipeline;
    ppDatosCliente: TppDBPipeline;
    ppImage3: TppImage;
    ppLabel24: TppLabel;
    ppRegion3: TppRegion;
    ppLabel13: TppLabel;
    ppDBCalc2: TppDBCalc;
    ppLabel15: TppLabel;
    ppDBCalc3: TppDBCalc;
    lDomicilio: TppLabel;
    spObtenerDatosCliente: TADOStoredProc;
    lNumeroConvenio: TppLabel;
    ppParameterList1: TppParameterList;
    procedure ppDetailBand1BeforePrint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute(CodigoCliente: LongInt; NumeroConvenio, MedioPago: string; showInterface: Boolean);
  end;

implementation

{$R *.dfm}


procedure TformReporteInformeFactura.Execute(CodigoCliente: LongInt; NumeroConvenio, MedioPago: string; showInterface: Boolean);
var
    Cpostal: AnsiString;
    PRNConfig : TRBConfig;
begin
    // Obtener datos del cliente
(*Pablo
    QryObtenerDatosCliente.close;
    QryObtenerDatosCliente.Parameters.ParamByName('CodigoCliente').value := CodigoCliente;
    QryObtenerDatosCliente.open;

    lApellidoNombre.Caption	:= ArmaNombreCompleto(trim(QryObtenerDatosCliente.fieldByName('Apellido').asString),
                                        trim(QryObtenerDatosCliente.fieldByName('ApellidoMaterno').asString),
                                        trim(QryObtenerDatosCliente.fieldByName('Nombre').asString));

    if trim(QryObtenerDatosCliente.fieldByName('CodigoPostal').asString) <> '' then
        Cpostal := '  (' + trim(QryObtenerDatosCliente.fieldByName('CodigoPostal').asString) + ') ';
    lDomicilio.Caption := ArmarDomicilioSimple(QryObtenerDatosCliente.Connection,
                                trim(QryObtenerDatosCliente.fieldByName('Calle').asString),
                                QryObtenerDatosCliente.fieldByName('Numero').AsInteger,
                                QryObtenerDatosCliente.fieldByName('Piso').AsInteger,
                                trim(QryObtenerDatosCliente.fieldByName('Dpto').asString),
                                trim(QryObtenerDatosCliente.fieldByName('Detalle').asString) + Cpostal);
*)
    spObtenerDatosCliente.Parameters.ParamByName('@CodigoCliente').Value := CodigoCliente;
    spObtenerDatosCliente.Parameters.ParamByName('@CodigoDocumento').Value := null;
    spObtenerDatosCliente.Parameters.ParamByName('@NumeroDocumento').Value := null;
    spObtenerDatosCliente.Open;

    lApellidoNombre.Caption	:= trim(spObtenerDatosCliente.fieldByName('Nombre').asString);

    if trim(spObtenerDatosCliente.fieldByName('CodigoPostal').asString) <> '' then
        Cpostal := '  (' + trim(spObtenerDatosCliente.fieldByName('CodigoPostal').asString) + ') ';
    lDomicilio.Caption := trim(spObtenerDatosCliente.fieldByName('DescripcionDomicilio').asString)
                            + Cpostal;
    lNumeroConvenio.Caption := NumeroConvenio;
    lCuentaDebito.Caption := MedioPago;

    PRNConfig := rbi_Factura.GetConfig;
    PRNConfig.DeviceType := 'Printer';
    rbi_Factura.SetConfig(PRNConfig);

	rbi_Factura.Execute(ShowInterface);
end;


procedure TformReporteInformeFactura.ppDetailBand1BeforePrint(Sender: TObject);
begin
    NroItem.Value := NroItem.Value + 1;
end;

end.
