unit AbmFinanciamientos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons, DateEdit,
  DB, DBTables, ExtCtrls, DbList, Util, UtilProc, UtilDb, peatypes, Abm_obj, OleCtrls,  DmiCtrls, Mask, ComCtrls,
  PeaProcs, CheckLst, dmConnection, ADODB, validate, DBCtrls,
  DPSControls;

type
  TFrmPlanesFinanciamientos = class(TForm)
	Panel2: TPanel;
	DBList1: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    tblPlanesFinanciamiento: TADOTable;
    QryMaxCodigo: TADOQuery;
    pDatos: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txt_descripcion: TEdit;
    txt_Codigo: TNumericEdit;
    neInteres: TNumericEdit;
    neCantidadCuotas: TNumericEdit;
    cbFactorFrecuencia: TComboBox;
    Label2: TLabel;
    nePieMinimo: TNumericEdit;
	Label6: TLabel;
    nePlazoPrimerCuota: TNumericEdit;
    cbTiposPieMinimo: TComboBox;
    Label7: TLabel;

	function  DBList1Process(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure DBList1Insert(Sender: TObject);
	procedure DBList1Edit(Sender: TObject);
	procedure DBList1Delete(Sender: TObject);
	procedure DBList1Click(Sender: TObject);
	procedure DBList1Refresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cbTiposPieMinimoChange(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
    procedure Volver_Campos;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FrmPlanesFinanciamientos  : TFrmPlanesFinanciamientos;

implementation

{$R *.DFM}

function TFrmPlanesFinanciamientos.Inicializa: boolean;
resourcestring
    MSG_PORCENTAJE = 'Porcentaje';
    MSG_IMPORTE    = 'Importe';
var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Result := False;

	if not OpenTables([TblPlanesFinanciamiento]) then Exit;
	Notebook.PageIndex := 0;

	cbTiposPieMinimo.Items.Clear;
	cbTiposPieMinimo.Items.Add(PadR(MSG_PORCENTAJE, 200, ' ') + TD_PORCENTUAL);
	cbTiposPieMinimo.Items.Add(PadR(MSG_IMPORTE, 200, ' ') + TD_IMPORTE);
	cbTiposPieMinimo.ItemIndex := 0;

	DbList1.Reload;
    Volver_Campos();
	Result := True;
end;


procedure TFrmPlanesFinanciamientos.Limpiar_Campos();
begin
	txt_Codigo.Clear;
	txt_Descripcion.Clear;
    cbFactorFrecuencia.ItemIndex := 0;
    nePieMinimo.value := 0;
    neInteres.value := 0;
    neCantidadCuotas.value := 0;
    nePlazoPrimerCuota.value := 0;
end;

function TFrmPlanesFinanciamientos.DBList1Process(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	 Result := True;
end;

procedure TFrmPlanesFinanciamientos.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFrmPlanesFinanciamientos.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    pDatos.Enabled     := True;
	DbList1.Enabled    := False;
    txt_Codigo.Enabled := False;
  	Notebook.PageIndex := 1;
    txt_Descripcion.SetFocus;
end;

procedure TFrmPlanesFinanciamientos.DBList1Edit(Sender: TObject);
begin
    DbList1.Enabled    	    := False;
    pDatos.Enabled          := True;
    txt_Codigo.enabled      := False;
  	Notebook.PageIndex      := 1;
 	txt_Descripcion.SetFocus;
end;

procedure TFrmPlanesFinanciamientos.DBList1Delete(Sender: TObject);
resourcestring
    MSG_ELIMIMAR_FINANCIAMIENTO = '�Est� seguro de querer eliminar el Plan de Financiamiento seleccionado?';
    CAPTION_ELIMINAR_FINANCIAMIENTO = 'Eliminar Plan Financiamiento';
    MSG_ERROR_ELIMINAR_FINANCIAMIENTO = 'No se pudo eliminar el Plan de Fianciamiento.';
begin
    Screen.Cursor := crHourGlass;
    If MsgBox(MSG_ELIMIMAR_FINANCIAMIENTO, CAPTION_ELIMINAR_FINANCIAMIENTO, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        DMConnections.BaseCAC.BeginTrans;
        try
            (Sender as TDbList).Table.Delete;
            DMConnections.BaseCAC.CommitTrans;
        Except
            On E: Exception do begin
                (Sender as TDbList).Table.Cancel;
                MsgBoxErr(MSG_ERROR_ELIMINAR_FINANCIAMIENTO, e.message, CAPTION_ELIMINAR_FINANCIAMIENTO, MB_ICONSTOP);
                DMConnections.BaseCAC.RollbackTrans;
            end;
        end;
        DbList1.Reload;
    end;
	DbList1.Estado     		:= Normal;
	DbList1.Enabled    		:= True;
	Screen.Cursor      		:= crDefault;
end;

procedure TFrmPlanesFinanciamientos.DBList1Click(Sender: TObject);
begin
	with (Sender as TDbList).Table do begin
		txt_Codigo.Value	        := FieldByName('CodigoFinanciamiento').AsInteger;
		txt_Descripcion.text 		:= Trim(FieldByName('Descripcion').AsString);

        nePieMinimo.value := (FieldByName('PieMinimo').asInteger / 100);
        neInteres.value := FieldByName('Interes').asFloat;
        neCantidadCuotas.value := FieldByName('CantidadCuotas').asInteger;
        nePlazoPrimerCuota.value := FieldByName('PlazoPrimerCuota').asInteger;

        if FieldByName('FactorFrecuencia').asFloat = 0.5 then cbFactorFrecuencia.ItemIndex := 0
        else if FieldByName('FactorFrecuencia').asFloat = 1.0 then cbFactorFrecuencia.ItemIndex := 1
        else if FieldByName('FactorFrecuencia').asFloat = 2.0 then cbFactorFrecuencia.ItemIndex := 2;
  	end;
end;

procedure TFrmPlanesFinanciamientos.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFrmPlanesFinanciamientos.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFrmPlanesFinanciamientos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, FieldByName('CodigoFinanciamiento').AsString);
        TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFrmPlanesFinanciamientos.FormShow(Sender: TObject);
begin
	DBList1.Reload;
end;

procedure TFrmPlanesFinanciamientos.BtnAceptarClick(Sender: TObject);
resourcestring
    // Mensajes de validaci�n
    CAPTION_VALIDACION_FINANCIAMIENTO = 'Validar datos del Plan de Financiamiento';
    MSG_CUOTAS         = 'Es necesario indicar la cantidad de cuotas.';
    MSG_DESCRIPCION    = 'El Plan de Financiamiento debe tener una descripci�n.';
    // Mensajes de informaci�n
    CAPTION_ACTUALIZAR_FINANCIAMIENTO = 'Actualizar Plan de Financiamiento';
    MSG_ACTUALIZAR_FINANCIAMIENTO     = 'No se pudo actualizar el Plan de Financiamiento.';
begin
	if not ValidateControls([txt_Descripcion, neCantidadCuotas],
      [Trim(txt_Descripcion.text) <> '', neCantidadCuotas.value > 0],
      CAPTION_VALIDACION_FINANCIAMIENTO,
      [MSG_DESCRIPCION, MSG_CUOTAS]) then exit;
	Screen.Cursor := crHourGlass;
	DMConnections.BaseCAC.BeginTrans;
  	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                QryMaxCodigo.Open;
                txt_Codigo.value := QryMaxCodigo.FieldByNAme('Codigo').AsInteger + 1;
				QryMaxCodigo.Close;
			end else begin
            	Edit;
            end;
			FieldByName('CodigoFinanciamiento').value := strToInt(trim(txt_Codigo.text));
            FieldByName('Descripcion').asString := trim(txt_Descripcion.text);
            FieldByName('Interes').asFloat := neInteres.Value;
            FieldByName('CantidadCuotas').asInteger := trunc(neCantidadCuotas.value);
			FieldByName('PlazoPrimerCuota').asInteger :=  trunc(nePlazoPrimerCuota.value);

			FieldByName('TipoPieMinimo').AsString := trim(strRight(cbTiposPieMInimo.text,20));
			FieldByName('PieMinimo').asInteger := trunc(nePieMinimo.value * 100);
			if cbFactorFrecuencia.ItemIndex = 0 then FieldByName('FactorFrecuencia').value := 0.5
            else if cbFactorFrecuencia.ItemIndex = 1 then FieldByName('FactorFrecuencia').value := 1.0
            else if cbFactorFrecuencia.ItemIndex = 2 then FieldByName('FactorFrecuencia').value := 2.0;
            Post;
            DMConnections.BaseCAC.CommitTrans;
            Volver_Campos;
		except On E: Exception do begin
    		Cancel;
            DMConnections.BaseCAC.RollbackTrans;
	    	MsgBoxErr(MSG_ACTUALIZAR_FINANCIAMIENTO, E.message, CAPTION_ACTUALIZAR_FINANCIAMIENTO, MB_ICONSTOP);
            end;
		end;
    end;
	Screen.Cursor 	   := crDefault;
end;

procedure TFrmPlanesFinanciamientos.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFrmPlanesFinanciamientos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFrmPlanesFinanciamientos.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFrmPlanesFinanciamientos.Volver_Campos;
begin
	pDatos.enabled      := false;
	DbList1.Estado     	:= Normal;
	DbList1.Enabled     := True;
	DBList1.Reload;
	DbList1.SetFocus;
	txt_Codigo.Enabled  := True;
	Notebook.PageIndex  := 0;
end;

procedure TFrmPlanesFinanciamientos.cbTiposPieMinimoChange(Sender: TObject);
begin
	if (sender as TComboBox).ItemIndex = 0 then begin
        nePieMinimo.Decimals := 2;
		nePieMinimo.MaxLength := 6;
	end else begin
		nePieMinimo.MaxLength := 14;
        nePieMinimo.Decimals := 0;
    end;
    if length(nePieMinimo.Text) > nePieMinimo.MaxLength then
        nePieMinimo.value := 0
    else
        nePieMinimo.value := nePieMinimo.value * 1;
end;

end.
