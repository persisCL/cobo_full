unit ThreadTipificacion;

interface

uses
	DM_ThreadFact,
  Classes,Windows, SysUtils, UtilProc, Variants, Util;

type
  TTYNMasivaThread = class(TThread)
  private
    { Private declarations }
    FNumeroProcesoFacturacion : integer;
    FLogOperaciones : ^integer;
    FUsuario : string;
    DM : TDMThreadFacturacion;

  protected
    procedure Execute; override;
    procedure TipificarYNumerar;
  public
    constructor Create(NumeroProcesoFacturacion : integer; Usuario : string; var LogOperaciones : integer);
	destructor Destroy; override;
  end;

implementation

{ TTipificacionYNumeracion }

constructor TTYNMasivaThread.Create(NumeroProcesoFacturacion : integer; Usuario : string; var LogOperaciones : integer);
begin
    inherited Create(True);
    FreeOnTerminate := True;
    {inicializar el stored}
    DM := TDMThreadFacturacion.Create(nil);
    DM.GenerarMovimientosTransitos.Close;
    DM.GenerarMovimientosTransitos.ProcedureName := 'TipificacionYNumeracionMasiva';
    FNumeroProcesoFacturacion := NumeroProcesoFacturacion;
    FUsuario := Usuario;
    FLogOperaciones := @LogOperaciones;
    Resume;
end;

procedure TTYNMasivaThread.Execute;
begin
	{ Place thread code here }
	TipificarYNumerar();

end;

destructor TTYNMasivaThread.Destroy;
begin
    DM.Free;
    inherited;
end;

procedure TTYNMasivaThread.TipificarYNumerar;
begin
    with DM.GenerarMovimientosTransitos do begin
        Parameters.Refresh;
        Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
        Parameters.ParamByName('@Usuario').Value := FUsuario;
        Parameters.ParamByName('@LogOperaciones').Value := 0;
        ExecProc;
        FLogOperaciones^ := Parameters.ParamByName('@LogOperaciones').Value;
    end;
end;

end.
