object frmAccionesEmail: TfrmAccionesEmail
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Confirmar Acciones Adicionales'
  ClientHeight = 242
  ClientWidth = 618
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 64
    Height = 14
    Caption = 'Atenci'#243'n !'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblAtencion: TLabel
    Left = 16
    Top = 38
    Width = 570
    Height = 26
    Caption = 
      'Existen e-mails actualmente cargados para este proceso de Factur' +
      'aci'#243'n Masivo (%d). Desea realizar algunas de estas acciones adic' +
      'ionales antes de continuar?'
    WordWrap = True
  end
  object btnAceptar: TButton
    Left = 448
    Top = 208
    Width = 75
    Height = 25
    Caption = '&Continuar'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 528
    Top = 208
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object rgOpciones: TRadioGroup
    Left = 16
    Top = 80
    Width = 585
    Height = 105
    Caption = 'Opciones Posibles'
    ItemIndex = 0
    Items.Strings = (
      
        'S'#237', deseo eliminar todos los e-mails que existan del proceso sin' +
        ' importarme que se hayan enviado '#243' no'
      
        'S'#237', pero s'#243'lo deseo eliminar aquellos e-mails que no se hayan en' +
        'viado todav'#237'a de este Proceso'
      
        'S'#237', deseo crear nuevos e-mails para este proceso sin afectar los' +
        ' que puedan existir. (Duplicar los Mismos)')
    TabOrder = 2
  end
end
