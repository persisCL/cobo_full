object FormConceptosMovimientos: TFormConceptosMovimientos
  Left = 273
  Top = 177
  BorderStyle = bsDialog
  Caption = 'Mantenimiento de Conceptos de Movimientos'
  ClientHeight = 586
  ClientWidth = 774
  Color = clBtnFace
  Constraints.MinHeight = 440
  Constraints.MinWidth = 480
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dblConceptos: TAbmList
    Left = 0
    Top = 41
    Width = 774
    Height = 205
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'111'#0'C'#243'digo de Concepto  '
      #0'64'#0'Descripci'#243'n')
    HScrollBar = True
    RefreshTime = 100
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblConceptosClick
    OnDblClick = dblConceptosDblClick
    OnProcess = dblConceptosProcess
    OnDrawItem = dblConceptosDrawItem
    OnRefresh = dblConceptosRefresh
    OnInsert = dblConceptosInsert
    OnDelete = dblConceptosDelete
    OnEdit = dblConceptosEdit
    OnSectionClick = dblConceptosSectionClick
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
    ExplicitHeight = 182
  end
  object pnlCampos: TPanel
    Left = 0
    Top = 287
    Width = 774
    Height = 260
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object lblDescripcion: TLabel
      Left = 13
      Top = 44
      Width = 72
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoConcepto: TLabel
      Left = 13
      Top = 19
      Width = 100
      Height = 13
      Caption = 'C'#243'digo de Concepto:'
    end
    object lblGrupoImprenta: TLabel
      Left = 13
      Top = 122
      Width = 142
      Height = 13
      Caption = 'C'#243'digo de Grupo de Imprenta:'
    end
    object lblPrecio: TLabel
      Left = 13
      Top = 70
      Width = 41
      Height = 13
      Caption = 'Precio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblConceptoAnula: TLabel
      Left = 13
      Top = 148
      Width = 110
      Height = 13
      Caption = 'Concepto Opuesto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMoneda: TLabel
      Left = 13
      Top = 96
      Width = 54
      Height = 13
      Caption = 'Moneda :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblLibroContable: TLabel
      Left = 13
      Top = 200
      Width = 87
      Height = 13
      Caption = 'Libro Contable:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblColumnaContable: TLabel
      Left = 13
      Top = 227
      Width = 107
      Height = 13
      Caption = 'Columna Contable:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDescuentoRecargo: TLabel
      Left = 13
      Top = 174
      Width = 147
      Height = 13
      Alignment = taRightJustify
      Caption = 'Es Descuento o Recargo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 178
      Top = 38
      Width = 278
      Height = 21
      Color = clBtnFace
      MaxLength = 50
      TabOrder = 1
    end
    object txt_CodigoConcepto: TNumericEdit
      Left = 178
      Top = 11
      Width = 105
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 0
      TabOrder = 0
    end
    object chkIncFactura: TCheckBox
      Left = 533
      Top = 115
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Para Incluir en Factura:'
      TabOrder = 11
      OnClick = chkClicked
    end
    object chkIncNotaCredito: TCheckBox
      Left = 533
      Top = 138
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Para Incluir en Nota de Cr'#233'dito:'
      TabOrder = 12
      OnClick = chkClicked
    end
    object chkIncDebito: TCheckBox
      Left = 533
      Top = 161
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Para Incluir en Nota de D'#233'bito:'
      TabOrder = 13
      OnClick = chkClicked
    end
    object chkIncBoleta: TCheckBox
      Left = 533
      Top = 92
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Para Incluir en Boleta:'
      TabOrder = 10
      OnClick = chkClicked
    end
    object chkAfectoIVA: TCheckBox
      Left = 533
      Top = 69
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Afecto IVA:'
      TabOrder = 9
    end
    object neGrupoImprenta: TNumericEdit
      Left = 178
      Top = 116
      Width = 105
      Height = 21
      Color = clBtnFace
      TabOrder = 4
    end
    object nePrecio: TNumericEdit
      Left = 178
      Top = 64
      Width = 105
      Height = 21
      Color = clBtnFace
      MaxLength = 11
      TabOrder = 2
      Decimals = 2
    end
    object cbMoneda: TComboBox
      Left = 178
      Top = 91
      Width = 65
      Height = 21
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 3
    end
    object cbColumna: TVariantComboBox
      Left = 178
      Top = 222
      Width = 205
      Height = 21
      Style = vcsDropDownList
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 8
      Items = <>
    end
    object cbLibro: TVariantComboBox
      Left = 178
      Top = 195
      Width = 205
      Height = 21
      Style = vcsDropDownList
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 7
      OnChange = cbLibroChange
      Items = <>
    end
    object vcbEsDesctoRecargo: TVariantComboBox
      Left = 178
      Top = 170
      Width = 145
      Height = 21
      Style = vcsDropDownList
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 6
      Items = <
        item
          Caption = 'Nada'
          Value = 0
        end
        item
          Caption = 'Descuento'
          Value = 1
        end
        item
          Caption = 'Recargo'
          Value = 2
        end>
    end
    object chkAfectaFechaCorte: TCheckBox
      Left = 533
      Top = 230
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Afecta la Fecha de Corte:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 16
    end
    object chkFacturacionInmediata: TCheckBox
      Left = 533
      Top = 207
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Concepto Fact. Inmediata:'
      TabOrder = 15
    end
    object chkIncNotaCobroDirecta: TCheckBox
      Left = 533
      Top = 184
      Width = 222
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Para Incluir en Nota Cobro Directa :'
      TabOrder = 14
      OnClick = chkClicked
    end
    object cbbConceptoAnula: TVariantComboBox
      Left = 178
      Top = 143
      Width = 278
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 5
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 547
    Width = 774
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Notebook: TNotebook
      Left = 577
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 26
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 774
    Height = 41
    Align = alTop
    TabOrder = 0
    object lblConcesionaria: TLabel
      Left = 198
      Top = 14
      Width = 85
      Height = 13
      Margins.Top = 20
      Caption = 'Concesionaria:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 193
      Height = 39
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Panel1'
      TabOrder = 0
      object AbmToolbar1: TAbmToolbar
        Left = 0
        Top = 0
        Width = 193
        Height = 33
        Habilitados = [btAlta, btModi, btSalir, btBuscar]
        OnClose = AbmToolbar1Close
      end
    end
    object cbConcesionaria: TVariantComboBox
      Left = 289
      Top = 11
      Width = 245
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Visible = False
      OnChange = cbConcesionariaChange
      Items = <>
    end
  end
  object pnlMaestro: TPanel
    Left = 0
    Top = 246
    Width = 774
    Height = 41
    Align = alBottom
    Enabled = False
    TabOrder = 2
    Visible = False
    ExplicitTop = 223
    object lblCodigoConceptoMaestro: TLabel
      Left = 13
      Top = 14
      Width = 90
      Height = 13
      Caption = 'Concepto Maestro:'
    end
    object cbbConceptoMaestro: TVariantComboBox
      Left = 178
      Top = 11
      Width = 278
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbbConceptoMaestroChange
      Items = <>
    end
  end
  object tblConceptosMovimiento: TADOTable
    Tag = 1
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'codigoConcepto'
    TableName = 'dbo.ConceptosMovimiento'
    Left = 252
    Top = 72
  end
  object tblConceptosMovimientoMaestro: TADOTable
    Tag = 1
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'codigoConcepto'
    TableName = 'dbo.ConceptosMovimientoMaestro'
    Left = 100
    Top = 72
  end
end
