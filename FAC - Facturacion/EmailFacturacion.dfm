object frmMailsFacturacion: TfrmMailsFacturacion
  Left = 0
  Top = 0
  Width = 817
  Height = 481
  Caption = 'Generar e-mails de Facturaci'#243'n Masiva'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    809
    447)
  PixelsPerInch = 96
  TextHeight = 13
  object nbWizard: TNotebook
    Left = 0
    Top = 0
    Width = 809
    Height = 444
    Anchors = [akLeft, akTop, akRight, akBottom]
    PageIndex = 1
    TabOrder = 0
    object TPage
      Left = 0
      Top = 0
      Caption = 'Primera'
      object Label1: TLabel
        Left = 32
        Top = 48
        Width = 433
        Height = 89
        AutoSize = False
        Caption = 
          'Este proceso generar'#225' los e-mails que correspondan enviar despu'#233 +
          's de haberse generado un proceso de Facturaci'#243'n Masivo en el Sis' +
          'tema Central de Peaje.  Antes de ejecutar este proceso es posibl' +
          'e que deba verificar que la configuraci'#243'n relativa al env'#237'o de e' +
          '-mails este correctamente establecida. Como resultado de este pr' +
          'oceso se crear'#225'n e-mails que ser'#225'n enviados autom'#225'ticamente a to' +
          'dos aquellos convenios  que est'#233'n adheridos a ser notificados cu' +
          'ando se les ha creado una nueva Nota Cobro. '
        WordWrap = True
      end
      object Label2: TLabel
        Left = 32
        Top = 22
        Width = 64
        Height = 14
        Caption = 'Atenci'#243'n !'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 32
        Top = 136
        Width = 433
        Height = 17
        AutoSize = False
        Caption = 
          'Ante Cualquier Duda Consulte al Administrador del Sistema Centra' +
          'l de Peaje'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Panel3: TPanel
        Left = 0
        Top = 403
        Width = 809
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          809
          41)
        object btnCerrar: TButton
          Left = 644
          Top = 10
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = '&Cerrar'
          Default = True
          TabOrder = 0
          OnClick = btnCerrarClick
        end
        object btnSiguiente: TButton
          Left = 724
          Top = 10
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Siguiente'
          TabOrder = 1
          OnClick = btnSiguienteClick
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Segunda'
      object Panel2: TPanel
        Left = 0
        Top = 403
        Width = 809
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          809
          41)
        object lblDetalle: TLabel
          Left = 8
          Top = 16
          Width = 492
          Height = 13
          Caption = 
            'Se visualizan s'#243'lo las 100 primeras notificaciones  de un total ' +
            'de %d Convenios facturados pendientes '
        end
        object btnAnterior: TButton
          Left = 647
          Top = 9
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Anterior'
          TabOrder = 0
          OnClick = btnAnteriorClick
        end
        object btnFinalizar: TButton
          Left = 727
          Top = 9
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = '&Finalizar'
          Enabled = False
          TabOrder = 1
          OnClick = btnFinalizarClick
        end
      end
      object dblComprobantesPendientes: TDBListEx
        Left = 0
        Top = 113
        Width = 809
        Height = 290
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Corresponde Notificaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Notificacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'e-Mail'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'email'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Convenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Apellido'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Apellido'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Apellido Materno'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'ApellidoMaterno'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Nombre'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Calle'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Calle'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'N'#250'mero'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Numero'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Detalle Domicilio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DetalleDomicilio'
          end>
        DataSource = DataSource1
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
      object cpBusqueda: TCollapsablePanel
        Left = 0
        Top = 0
        Width = 809
        Height = 113
        Align = alTop
        Animated = False
        Caption = 'B'#250'squeda de Notificaciones'
        BarColorStart = 14071199
        BarColorEnd = 10646097
        RightMargin = 0
        LeftMargin = 0
        ArrowLocation = cpaRightTop
        Style = cpsWinXP
        InternalSize = 90
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        DesignSize = (
          809
          113)
        object Label4: TLabel
          Left = 19
          Top = 36
          Width = 219
          Height = 13
          Caption = 'Seleccione un  Proceso de Facturaci'#243'n Masiva'
        end
        object Label5: TLabel
          Left = 504
          Top = 80
          Width = 252
          Height = 13
          Anchors = [akRight, akBottom]
          Caption = 'Si desea explorar  los e-mails registrados haga "click"'
        end
        object lblAca: TLabel
          Left = 760
          Top = 80
          Width = 20
          Height = 13
          Cursor = crHandPoint
          Anchors = [akRight, akBottom]
          Caption = 'ac'#225
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          OnClick = lblAcaClick
        end
        object bteProcesos: TBuscaTabEdit
          Left = 19
          Top = 52
          Width = 249
          Height = 21
          Enabled = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Decimals = 0
          EditorStyle = bteTextEdit
          BuscaTabla = btProcesos
        end
        object cbSoloPendientes: TCheckBox
          Left = 21
          Top = 81
          Width = 436
          Height = 17
          Caption = 
            'Seleccionar s'#243'lo los convenios facturados que tienen notificacio' +
            'n indicada y pendiente'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object btnObtenerPreview: TButton
          Left = 627
          Top = 48
          Width = 153
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Previsualizar Notificaciones'
          TabOrder = 3
          OnClick = btnObtenerPreviewClick
        end
      end
    end
  end
  object spObtenerProcesos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerProcesosNotificacionPendientes'
    Parameters = <>
    Left = 312
    Top = 48
  end
  object btProcesos: TBuscaTabla
    Caption = 'Procesos Masivos de Facturaci'#243'n con e-mails Pendientes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    HelpContext = 0
    Dataset = spObtenerProcesos
    OnProcess = btProcesosProcess
    OnSelect = btProcesosSelect
    Left = 280
    Top = 48
  end
  object spPreview: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPreviewNotificacionesPendientes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@EnvioElectronico'
        Attributes = [paNullable]
        DataType = ftBoolean
      end>
    Left = 712
    Top = 192
  end
  object DataSource1: TDataSource
    DataSet = spPreview
    Left = 672
    Top = 192
  end
  object spContarNotificaciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ContarNotificacionesPendientes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@EnvioElectronico'
        Attributes = [paNullable]
        DataType = ftBoolean
      end>
    Left = 744
    Top = 192
  end
  object spVerificar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarEmailsExistentes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 616
    Top = 272
  end
  object spCrearMails: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearEMailsFacturacionMasiva'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 744
    Top = 224
  end
end
