object FormPromociones: TFormPromociones
  Left = 247
  Top = 153
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Promociones'
  ClientHeight = 422
  ClientWidth = 562
  Color = clBtnFace
  Constraints.MinHeight = 440
  Constraints.MinWidth = 570
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 383
    Width = 562
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 365
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 6
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 562
    Height = 172
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'65'#0'C'#243'digo        '
      
        #0'211'#0'Descripci'#243'n                                                ' +
        ' '
      #0'94'#0'Fecha/Hora Inicio'
      #0'90'#0'Fecha/Hora Baja'
      #0'41'#0'Estado')
    HScrollBar = True
    RefreshTime = 100
    Table = Promociones
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnProcess = DBList1Process
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 562
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object PageControl: TPageControl
    Left = 0
    Top = 205
    Width = 562
    Height = 178
    ActivePage = Tab_General
    Align = alBottom
    TabOrder = 3
    object Tab_General: TTabSheet
      Caption = 'General'
      Enabled = False
      object Label1: TLabel
        Left = 10
        Top = 39
        Width = 72
        Height = 13
        Caption = 'Descripci'#243'n:'
        FocusControl = txt_descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 10
        Top = 11
        Width = 39
        Height = 13
        Caption = 'C'#243'digo: '
        FocusControl = txt_CodigoPromocion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 10
        Top = 68
        Width = 91
        Height = 13
        Caption = 'Tipo Par'#225'metro:'
        FocusControl = cb_TiposParametro
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 252
        Top = 96
        Width = 65
        Height = 13
        Caption = 'Valor Final:'
        FocusControl = neValorFinal
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 12
        Top = 96
        Width = 72
        Height = 13
        Caption = 'Valor Inicial:'
        FocusControl = neValorInicial
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 10
        Top = 125
        Width = 95
        Height = 13
        Caption = 'Tipo Descuento:'
        FocusControl = cb_TiposDescuento
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 252
        Top = 125
        Width = 66
        Height = 13
        Caption = 'Descuento:'
        FocusControl = txt_Descuento
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_descripcion: TEdit
        Left = 114
        Top = 35
        Width = 338
        Height = 21
        Color = 16444382
        MaxLength = 60
        TabOrder = 0
      end
      object txt_CodigoPromocion: TNumericEdit
        Left = 114
        Top = 7
        Width = 121
        Height = 21
        TabStop = False
        Color = clBtnFace
        MaxLength = 4
        TabOrder = 1
      end
      object cb_TiposParametro: TComboBox
        Left = 114
        Top = 64
        Width = 234
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 2
        OnChange = cb_TiposParametroChange
      end
      object neValorInicial: TNumericEdit
        Left = 114
        Top = 92
        Width = 121
        Height = 21
        Color = 16444382
        MaxLength = 14
        TabOrder = 3
      end
      object neValorFinal: TNumericEdit
        Left = 332
        Top = 92
        Width = 121
        Height = 21
        Color = 16444382
        MaxLength = 14
        TabOrder = 4
      end
      object cb_TiposDescuento: TComboBox
        Left = 114
        Top = 121
        Width = 123
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 5
        OnChange = cb_TiposDescuentoChange
      end
      object txt_Descuento: TNumericEdit
        Left = 332
        Top = 121
        Width = 121
        Height = 21
        Color = 16444382
        MaxLength = 12
        TabOrder = 6
        Decimals = 2
      end
    end
    object tab_Vigencia: TTabSheet
      Caption = 'Vigencia'
      ImageIndex = 2
      object Label8: TLabel
        Left = 10
        Top = 14
        Width = 122
        Height = 13
        Caption = 'Fecha de Activaci'#243'n:'
        FocusControl = deFechaActivacion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 10
        Top = 47
        Width = 129
        Height = 13
        Caption = 'Fecha de Finalizaci'#243'n:'
        FocusControl = deFechaBaja
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object deFechaActivacion: TDateEdit
        Left = 168
        Top = 10
        Width = 103
        Height = 21
        AutoSelect = False
        Color = 16444382
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object deFechaBaja: TDateEdit
        Left = 168
        Top = 43
        Width = 103
        Height = 21
        AutoSelect = False
        Color = 16444382
        TabOrder = 1
        Date = -693594.000000000000000000
      end
    end
    object Tab_Clientes: TTabSheet
      Caption = 'Planes Comerciales'
      ImageIndex = 3
      object chk_PlanesComerciales: TCheckListBox
        Left = 0
        Top = 0
        Width = 554
        Height = 150
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object Tab_observaciones: TTabSheet
      Caption = 'Observaciones'
      ImageIndex = 3
      object REditObservaciones: TRichEdit
        Left = 0
        Top = 0
        Width = 554
        Height = 150
        Align = alClient
        Enabled = False
        Lines.Strings = (
          '')
        TabOrder = 0
      end
    end
  end
  object spObtenerPromocionesPorPlanComercial: TADOStoredProc
    Connection = DMConnections.BaseCAC
    MarshalOptions = moMarshalModifiedOnly
    ProcedureName = 'ObtenerPromocionesPorPlanComercial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 277
    Top = 133
  end
  object tblPromocionPlanComercial: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'PromocionPlanComercial'
    Left = 208
    Top = 133
  end
  object qryEliminarPromocionPlanComercial: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'NUMEROPROMOCION'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM PROMOCIONPLANCOMERCIAL'
      'WHERE NumeroPromocion = :NUMEROPROMOCION')
    Left = 242
    Top = 133
  end
  object ActualizarPromocion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    MarshalOptions = moMarshalModifiedOnly
    ProcedureName = 'ActualizarPromocion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraActivacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraBaja'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@TipoParametro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ValorInicial'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@ValorFinal'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@TipoDescuento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@ValorDescuento'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 1073741823
        Value = Null
      end>
    Left = 140
    Top = 133
  end
  object InsertarPromocion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarPromocion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraActivacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraBaja'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@TipoParametro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ValorInicial'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@ValorFinal'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@TipoDescuento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@ValorDescuento'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 174
    Top = 133
  end
  object EliminarPromocion: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'NumeroPromocion'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM Promociones WHERE NumeroPromocion = :NumeroPromocion')
    Left = 106
    Top = 133
  end
  object Promociones: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'Promociones'
    Left = 72
    Top = 133
  end
end
