object MovimientosNoFacturadosForm: TMovimientosNoFacturadosForm
  Left = 0
  Top = 0
  Caption = 'MovimientosNoFacturadosForm'
  ClientHeight = 436
  ClientWidth = 741
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 741
    Height = 129
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 31
      Top = 20
      Width = 21
      Height = 13
      Caption = 'Rut:'
    end
    object Label2: TLabel
      Left = 31
      Top = 74
      Width = 49
      Height = 13
      Caption = 'Convenio:'
    end
    object btnBuscar: TButton
      Left = 305
      Top = 90
      Width = 75
      Height = 25
      Caption = '&Buscar'
      TabOrder = 4
      OnClick = btnBuscarClick
    end
    object btnEliminar: TButton
      Left = 641
      Top = 90
      Width = 75
      Height = 25
      Caption = '&Eliminar'
      TabOrder = 5
      OnClick = btnEliminarClick
    end
    object chkConveniosBaja: TCheckBox
      Left = 58
      Top = 44
      Width = 201
      Height = 17
      Caption = 'Incluir convenios dados de baja'
      TabOrder = 1
    end
    object btnConvenios: TButton
      Left = 305
      Top = 15
      Width = 75
      Height = 25
      Hint = 'Obtener los convenios asociados a este Rut'
      Caption = 'Convenios'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnConveniosClick
    end
    object edtRut: TEdit
      Left = 58
      Top = 17
      Width = 201
      Height = 21
      MaxLength = 15
      TabOrder = 0
      Text = 'edtRut'
      OnKeyPress = edtRutKeyPress
    end
    object cbConvenios: TVariantComboBox
      Left = 58
      Top = 93
      Width = 201
      Height = 19
	  Hint = 'Ingres el N'#250'mero de Convenio'
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
	  ParentShowHint = False
	  ShowHint = True
      TabOrder = 3
      OnDrawItem = cbConveniosDrawItem
      OnKeyPress = cbConveniosKeyPress
      Items = <>
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 129
    Width = 741
    Height = 266
    Align = alClient
    Caption = 'Movimientos No facturados '
    TabOrder = 1
    object lvMovimientos: TListView
      Left = 2
      Top = 15
      Width = 737
      Height = 249
      Align = alClient
      Checkboxes = True
      Columns = <
        item
          Caption = 'Concepto'
          Width = 250
        end
        item
          Alignment = taRightJustify
          Caption = 'Importe'
          Width = 150
        end
        item
          Alignment = taRightJustify
          Caption = 'Fecha Hora'
          Width = 150
        end
        item
          Caption = 'N'#176' Movimiento'
          Width = 150
        end>
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 395
    Width = 741
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      741
      41)
    object btnSalir: TButton
      Left = 641
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object spObtenerConveniosClientes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 512
    Top = 16
  end
  object spObtenerMovimientosNoFacturados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMovimientosNoFacturados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 544
    Top = 16
  end
  object spEliminarMovimientosNoFacturados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarMovimientosNoFacturados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioModificacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 576
    Top = 16
  end
end
