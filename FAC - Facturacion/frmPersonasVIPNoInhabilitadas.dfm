object formPersonasVIPNoInhabilitadas: TformPersonasVIPNoInhabilitadas
  Left = 0
  Top = 165
  Caption = 'Personas VIP NO Inhabilitadas'
  ClientHeight = 490
  ClientWidth = 934
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 449
    Width = 934
    Height = 41
    Align = alBottom
    TabOrder = 0
    object pnlButtons: TPanel
      Left = 745
      Top = 1
      Width = 188
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnGrabar: TButton
        Left = 14
        Top = 5
        Width = 75
        Height = 25
        Caption = '&Grabar'
        TabOrder = 0
        OnClick = btnGrabarClick
      end
      object btnSalir: TButton
        Left = 102
        Top = 5
        Width = 75
        Height = 25
        Caption = '&Salir'
        TabOrder = 1
        OnClick = btnSalirClick
      end
    end
  end
  object pnlCentral: TPanel
    Left = 0
    Top = 41
    Width = 934
    Height = 408
    Align = alClient
    TabOrder = 1
    object DBLEPersonasVIP: TDBListEx
      Left = 1
      Top = 1
      Width = 932
      Height = 406
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Rut'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Rut'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 300
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = ordenarColumna
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Desde'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = ordenarColumna
          FieldName = 'FechaInicio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Hasta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = ordenarColumna
          FieldName = 'FechaFinalizacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Usuario Inicio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = ordenarColumna
          FieldName = 'CodigoUsuarioInicio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Usuario Fin'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = ordenarColumna
          FieldName = 'CodigoUsuarioFinalizacion'
        end>
      DataSource = dsObtenerPersonaVIPNoInhabilitada
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 934
    Height = 41
    Align = alTop
    TabOrder = 2
    object Label4: TLabel
      Left = 6
      Top = 17
      Width = 26
      Height = 13
      Caption = 'RUT:'
      Color = 16776699
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 231
      Top = 19
      Width = 402
      Height = 16
      AutoSize = False
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
    end
    object peNumeroDocumento: TPickEdit
      Left = 38
      Top = 15
      Width = 134
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnEnter = peNumeroDocumentoEnter
      OnKeyPress = peNumeroDocumentoKeyPress
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
  end
  object spObtenerDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = 'NULL'
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = 'NULL'
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 219
    Top = 8
  end
  object spObtenerPersonaVIPNoInhabilitada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerPersonaVIPNoInhabilitada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 248
    Top = 8
  end
  object dsObtenerPersonaVIPNoInhabilitada: TDataSource
    DataSet = spObtenerPersonaVIPNoInhabilitada
    Left = 280
    Top = 8
  end
  object spConveniosEnListaAmarillaPorRut: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ConveniosEnListaAmarillaPorRut'
    Parameters = <>
    Left = 328
    Top = 8
  end
end
