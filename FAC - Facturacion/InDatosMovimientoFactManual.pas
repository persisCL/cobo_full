unit InDatosMovimientoFactManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, DmiCtrls, DB, ADODB, StdCtrls, ExtCtrls, Util, utilProc,
  validate, Dateedit, PeaProcs, peatypes, DPSControls, UtilFacturacion;

type
  TFrmAgregarMovFactManual = class(TForm)
    Panel2: TPanel;
    eObservaciones: TEdit;
    Label1: TLabel;
    cbConceptos: TComboBox;
    Label2: TLabel;
    neImporte: TNumericEdit;
    Label3: TLabel;
    Label4: TLabel;
    cbCuentasCliente: TComboBox;
    lConceptoImporte: TLabel;
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
    procedure cbConceptosChange(Sender: TObject);
    procedure cbConceptosExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializa(CodigoConvenio: LongInt): Boolean;
  end;


function InDatosMovimiento(CodigoConvenio: longint;
                           var IndiceVehiculo, CodigoConcepto:LongInt;
                           var Observaciones: AnsiString;
                           var Importe: Double;
                           var Patente : String): Boolean;


implementation

{$R *.dfm}

function InDatosMovimiento(CodigoConvenio: longint;
                           var IndiceVehiculo, CodigoConcepto:LongInt;
                           var Observaciones: AnsiString;
                           var Importe: Double;
                           var Patente : String): Boolean;
var
  F: TFrmAgregarMovFactManual;

begin
    result := false;
    application.CreateForm(TFrmAgregarMovFactManual,F);
    with F do begin
        if Inicializa(CodigoConvenio) then begin
            cbCuentasCliente.ItemIndex := 0;
            cbConceptos.ItemIndex := 0;
            btnAceptar.enabled := false;
            ShowModal;
            IndiceVehiculo := IVal(StrRight(cbCuentasCliente.Items[cbCuentasCliente.ItemIndex], 20));
            CodigoConcepto := IVal(StrRight(cbConceptos.Items[cbConceptos.ItemIndex], 20));
            Observaciones := iif(trim(eObservaciones.text) <> '', eObservaciones.Text, Trim(copy(cbConceptos.text, 0, 65)));
            Importe := neImporte.Value;
            Patente := Trim(ParseParamByNumber(cbCuentasCliente.Text, 1, '-'));
            result := ModalResult = idOk;
        end;
    end;
end;


function TFrmAgregarMovFactManual.Inicializa(CodigoConvenio: LongInt): Boolean;
resourcestring
    MSG_CONCEPTOS       = 'No existen conceptos para agregar el movimiento.';
    MSG_CUENTAS         = 'No existen cuentas para agregar el movimiento';
    CAPTION_VALIDAR_MOVIMIENTO = 'Validar Datos del Movimiento';
begin
    result := true;
    lConceptoImporte.caption := '';
    try
        CargarConceptosExternos(DMConnections.BaseCAC, cbConceptos, 0);
        if cbConceptos.Items.Count = 0 then begin
            msgBox(MSG_CONCEPTOS, CAPTION_VALIDAR_MOVIMIENTO, MB_OK);
            result := false;
            exit;
        end;

        CargarCuentasConvenio(DMConnections.BaseCAC, cbCuentasCliente, CodigoConvenio, False )
    except
        result := false;
    end;
end;

procedure TFrmAgregarMovFactManual.cbConceptosChange(Sender: TObject);
resourcestring
    CAPTION_CONCESIONARIA   = 'A favor de la Concesionaria';
    CAPTION_CLIENTE         = 'A favor del Cliente';
begin
    btnAceptar.Enabled := neImporte.value <> 0;
    if neImporte.value = 0 then
        lConceptoImporte.caption := ''
    else if neImporte.value > 0 then
        lConceptoImporte.Caption := CAPTION_CONCESIONARIA
    else
        lConceptoImporte.Caption := CAPTION_CLIENTE
end;

procedure TFrmAgregarMovFactManual.cbConceptosExit(Sender: TObject);
begin
    if Trim(eObservaciones.Text) = '' then
        eObservaciones.Text := trim(strLeft(Trim(cbConceptos.text),length(cbConceptos.text)-20));
end;

procedure TFrmAgregarMovFactManual.FormShow(Sender: TObject);
begin
    cbCuentasCliente.setFocus;
end;

end.
