object FormEstadosListaAmarilla: TFormEstadosListaAmarilla
  Left = 134
  Top = 155
  Caption = 'Mantenimiento de Estados de Lista Amarilla'
  ClientHeight = 398
  ClientWidth = 584
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 584
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object dblEstados: TAbmList
    Left = 0
    Top = 33
    Width = 584
    Height = 247
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'97'#0'C'#243'digo Estado'
      #0'100'#0'Descripci'#243'n Estado')
    HScrollBar = True
    RefreshTime = 100
    Table = tblEstados
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblEstadosClick
    OnDrawItem = dblEstadosDrawItem
    OnRefresh = dblEstadosRefresh
    OnInsert = dblEstadosInsert
    OnDelete = dblEstadosDelete
    OnEdit = dblEstadosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 280
    Width = 584
    Height = 79
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 18
      Top = 54
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txtDescripcionEstados
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoEstado: TLabel
      Left = 18
      Top = 22
      Width = 41
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtDescripcionEstados: TEdit
      Left = 124
      Top = 50
      Width = 297
      Height = 21
      Color = 16444382
      MaxLength = 30
      TabOrder = 1
    end
    object txtCodigoEstado: TNumericEdit
      Left = 124
      Top = 18
      Width = 57
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 359
    Width = 584
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 387
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btnSalir: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
  end
  object tblEstados: TADOTable
    Connection = DMConnections.BaseCAC
    Filtered = True
    AfterOpen = tblEstadosAfterOpen
    TableName = 'EstadosConveniosListaAmarilla'
    Left = 388
    Top = 292
  end
end
