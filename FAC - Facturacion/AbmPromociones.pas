unit AbmPromociones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, PeaTypes ,Abm_obj, OleCtrls,  DmiCtrls, Mask, ComCtrls,
  PeaProcs, CheckLst, dmConnection, ADODB, validate, DBCtrls, Rstrings,
  Math, DPSControls;

type
  TFormPromociones = class(TForm)
	Panel2: TPanel;
	DBList1: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Notebook: TNotebook;
    PageControl: TPageControl;
    Tab_General: TTabSheet;
    Label1: TLabel;
    Label15: TLabel;
	txt_descripcion: TEdit;
	txt_CodigoPromocion: TNumericEdit;
    Tab_Clientes: TTabSheet;
    chk_PlanesComerciales: TCheckListBox;
    cb_TiposParametro: TComboBox;
    Label2: TLabel;
    neValorInicial: TNumericEdit;
    neValorFinal: TNumericEdit;
    Label3: TLabel;
	Label4: TLabel;
    Label5: TLabel;
    cb_TiposDescuento: TComboBox;
    txt_Descuento: TNumericEdit;
    Label6: TLabel;
    spObtenerPromocionesPorPlanComercial: TADOStoredProc;
    tblPromocionPlanComercial: TADOTable;
    qryEliminarPromocionPlanComercial: TADOQuery;
    tab_Vigencia: TTabSheet;
    deFechaActivacion: TDateEdit;
    Label8: TLabel;
    Label9: TLabel;
    deFechaBaja: TDateEdit;
    ActualizarPromocion: TADOStoredProc;
    InsertarPromocion: TADOStoredProc;
	EliminarPromocion: TADOQuery;
	Promociones: TADOTable;
    Tab_observaciones: TTabSheet;
    REditObservaciones: TRichEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;

	function  DBList1Process(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure DBList1Insert(Sender: TObject);
	procedure DBList1Edit(Sender: TObject);
	procedure DBList1Delete(Sender: TObject);
	procedure DBList1Click(Sender: TObject);
	procedure DBList1Refresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	procedure cb_TiposDescuentoChange(Sender: TObject);
    procedure cb_TiposParametroChange(Sender: TObject);

  private
	{ Private declarations }
	procedure Limpiar_Campos;
	procedure Volver_Campos;
	Procedure CargarPlanesComerciales;
	function ObtenerEstado: AnsiString;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormPromociones  : TFormPromociones;

implementation

{$R *.DFM}

const
    TP_CANTIDAD_TRANSITO = 'CT';
    TP_IMPORTE_TRANSITO = 'IT';
    TP_CANTIDAD_VIAJE = 'CV';
    TP_IMPORTE_VIAJE = 'IV';

resourcestring
	EP_PROGRAMADA   = 'Programada';
    EP_VIGENTE      = 'Vigente';
    EP_FINALIZADA   = 'Finalizada';

function TFormPromociones.Inicializa: boolean;
resourcestring
    MSG_CANTIDAD_TRANSITOS  = 'Cantidad de Tr�nsitos';
    MSG_IMPORTE_TRANSITOS   = 'Monto de Tr�nsitos';
    MSG_CANTIDAD_VIAJES     = 'Cantidad de Viajes';
    MSG_IMPORTE_VIAJES      = 'Monto de Viajes';
    MSG_PORCENTAJE          = 'Porcentaje';
    MSG_IMPORTE             = 'Monto';

var
	i: integer;
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Result := False;
	Tab_General.Enabled 	:= False;
	Tab_Clientes.Enabled   	:= False;
	Tab_Vigencia.Enabled 	:= False;

	if not OpenTables([Promociones]) then Exit;

	PageControl.ActivePage := Tab_General;
	notebook.PageIndex := 0;
	DbList1.Reload;
	For i:= 0 to chk_PlanesComerciales.Items.Count - 1 do begin
		chk_PlanesComerciales.ItemEnabled[i] := False;
	end;

	cb_TiposParametro.Items.Clear;
	cb_TiposParametro.Items.Add(PadR(MSG_CANTIDAD_TRANSITOS, 200, ' ') + TP_CANTIDAD_TRANSITO);
	cb_TiposParametro.Items.Add(PadR(MSG_IMPORTE_TRANSITOS, 200, ' ') + TP_IMPORTE_TRANSITO);
	cb_TiposParametro.Items.Add(PadR(MSG_CANTIDAD_VIAJES, 200, ' ') + TP_CANTIDAD_VIAJE);
    cb_TiposParametro.Items.Add(PadR(MSG_IMPORTE_VIAJES, 200, ' ') + TP_IMPORTE_VIAJE);
    cb_TiposParametro.ItemIndex := 0;

    cb_TiposDescuento.Items.Clear;
    cb_TiposDescuento.Items.Add(PadR(MSG_PORCENTAJE, 200, ' ') + TD_PORCENTUAL);
    cb_TiposDescuento.Items.Add(PadR(MSG_IMPORTE, 200, ' ') + TD_IMPORTE);
    cb_TiposDescuento.ItemIndex := 0;

	Result := True;
end;


procedure TFormPromociones.Limpiar_Campos();
begin
	txt_CodigoPromocion.Clear;
	txt_Descripcion.Clear;
    cb_TiposParametro.ItemIndex := 0;
	neValorInicial.Value := 0;
	neValorFinal.Value := 0;
	cb_TiposDescuento.ItemIndex := 0;
	txt_Descuento.Value := 0;
    deFechaActivacion.date := NowBase(DMConnections.BaseCAC);
    deFechaBaja.Date := NowBase(DMConnections.BaseCAC);
end;

function TFormPromociones.DBList1Process(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	 Result := True;
end;

procedure TFormPromociones.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFormPromociones.DBList1Insert(Sender: TObject);
var
	i: integer;
begin
	Tab_General.Enabled 	:= True;
	Tab_Clientes.Enabled   	:= True;
	Tab_Vigencia.Enabled 	:= True;
    Tab_observaciones.Enabled  := True;
    REditObservaciones.Enabled := True;
	Limpiar_Campos;
	DbList1.Enabled    		:= False;
	Notebook.PageIndex 		:= 1;
	PageControl.ActivePage	:= Tab_General;
    txt_CodigoPromocion.Enabled := False;
	txt_Descripcion.SetFocus;
    For i:= 0 to chk_PlanesComerciales.Items.Count - 1 do begin
		chk_PlanesComerciales.ItemEnabled[i] := True;
        chk_PlanesComerciales.Checked[i] := False;
    end;
end;

procedure TFormPromociones.DBList1Edit(Sender: TObject);
var
	i: Integer;
begin
	// Editamos por completo solo las promociones que aun no empezaron
	if ObtenerEstado = EP_PROGRAMADA then begin
		DbList1.Enabled    			:= False;
        Notebook.PageIndex 			:= 1;
    	Tab_General.Enabled 		:= True;
    	Tab_Clientes.Enabled 		:= True;
	    Tab_Vigencia.Enabled 		:= True;
        Tab_observaciones.Enabled  := True;
        REditObservaciones.Enabled := True;
        txt_CodigoPromocion.enabled := False;
        PageControl.ActivePage 	    := Tab_General;
    	txt_Descripcion.SetFocus;
        for i:= 0 to chk_PlanesComerciales.Items.Count - 1 do
		    chk_PlanesComerciales.ItemEnabled[i] := True;
    end else begin
		// Si ya empezo pero no termino edito solo la fecha de baja
        if ObtenerEstado = EP_VIGENTE then begin
            DbList1.Enabled            := False;
            Notebook.PageIndex 		   := 1;
            Tab_Vigencia.Enabled 	   := True;
            deFechaActivacion.Enabled  := false;
            Tab_observaciones.Enabled  := True;
            REditObservaciones.Enabled := True;
            PageControl.ActivePage 	   := Tab_Vigencia;
            deFechaBaja.SetFocus;
        end else
            MsgBox(MSG_CAPTION_EDIT_PROMOCION,caption,MB_ICONSTOP);
		// Si ya termino no se puede hacer nada, volvemos a estado normal
		DbList1.Estado := Normal;

    end;
end;

procedure TFormPromociones.DBList1Delete(Sender: TObject);
resourcestring
    MSG_ELIMINAR_PROMOCION       = '�Est� seguro de querer eliminar esta Promoci�n?';
    MSG_ERROR_ELIMINAR_PROMOCION = 'No se pudo eliminar la Promoci�n seleccionada.';
    MSG_ESTADO_PROMOCION         = 'No es posible eliminar una promoci�n que ya esta vigente o a finalizado.';
    CAPTION_ELIMINAR_PROMOCION   = 'Eliminar Promoci�n';
begin
	if ObtenerEstado = EP_PROGRAMADA then begin
		Screen.Cursor := crHourGlass;
		If MsgBox(MSG_ELIMINAR_PROMOCION, CAPTION_ELIMINAR_PROMOCION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
			DMConnections.BaseCAC.BeginTrans;
			try
				qryEliminarPromocionPlanComercial.parameters.ParamByName('NumeroPromocion').value := Trunc(txt_CodigoPromocion.Value);
				qryEliminarPromocionPlanCOmercial.ExecSql;
				(Sender as TDbList).Table.Delete;
				DMConnections.BaseCAC.CommitTrans;
			Except
				On E: Exception do begin
					(Sender as TDbList).Table.Cancel;
					MsgBoxErr(MSG_ERROR_ELIMINAR_PROMOCION, e.message, CAPTION_ELIMINAR_PROMOCION, MB_ICONSTOP);
					DMConnections.BaseCAC.RollbackTrans;
				end;
			end;
			DbList1.Reload;
		end;
	end else
		MsgBox(MSG_ESTADO_PROMOCION, CAPTION_ELIMINAR_PROMOCION, MB_OK);
	DbList1.Estado     		:= Normal;
	DbList1.Enabled    		:= True;
	Tab_General.Enabled 	:= False;
	Tab_Clientes.Enabled 	:= False;
	Tab_Vigencia.Enabled 	:= False;
    Tab_observaciones.Enabled 	:= False;
	Notebook.PageIndex 		:= 0;
	Screen.Cursor      		:= crDefault;
end;

procedure TFormPromociones.DBList1Click(Sender: TObject);
var
	i: integer;
begin
	with (Sender as TDbList).Table do begin
		txt_CodigoPromocion.Value	:= FieldByName('NumeroPromocion').AsInteger;
		txt_Descripcion.text 		:= Trim(FieldByName('Descripcion').AsString);

        if FieldByName('TipoParametro').asString = TP_CANTIDAD_TRANSITO then begin
            cb_TiposParametro.ItemIndex := 0;
            neValorInicial.Value        := FieldByName('ValorInicial').AsVariant;
       		neValorFinal.Value      	:= FieldByName('ValorFinal').AsVariant;
        end else if FieldByName('TipoParametro').asString = TP_IMPORTE_TRANSITO then begin
            cb_TiposParametro.ItemIndex := 1;
            neValorInicial.Value        := FieldByName('ValorInicial').AsVariant / 100;
       		neValorFinal.Value      	:= FieldByName('ValorFinal').AsVariant / 100;
        end else if FieldByName('TipoParametro').asString = TP_CANTIDAD_VIAJE then begin
            cb_TiposParametro.ItemIndex := 2;
            neValorInicial.Value        := FieldByName('ValorInicial').AsVariant;
       		neValorFinal.Value      	:= FieldByName('ValorFinal').AsVariant;
		end else if FieldByName('TipoParametro').asString = TP_IMPORTE_VIAJE then begin
			cb_TiposParametro.ItemIndex := 3;
			neValorInicial.Value        := FieldByName('ValorInicial').AsVariant / 100;
			neValorFinal.Value      	:= FieldByName('ValorFinal').AsVariant / 100;
		end;

		if FieldByName('TipoDescuento').asString = TD_PORCENTUAL then cb_TiposDescuento.ItemIndex := 0
		else if FieldByName('TipoDescuento').asString = TD_IMPORTE then cb_TiposDescuento.ItemIndex := 1;

		txt_Descuento.Value      	:= FieldByName('ValorDescuento').AsVariant / 100;
        deFechaActivacion.date      := FieldByName('FechaHoraActivacion').asDateTime;
        deFechaBaja.date            := FieldByName('FechaHoraBaja').asDateTime;
        for i:= 0 to cb_TiposParametro.Items.Count -1 do begin
        	if Trim(Copy(cb_TiposParametro.Items[i], 201, 10)) = FieldByName('TipoParametro').AsString  then begin
                cb_TiposParametro.ItemIndex := i;
            	break;
            end;
        end;

        for i:= 0 to cb_TiposDescuento.Items.Count -1 do begin
        	if Trim(Copy(cb_TiposDescuento.Items[i], 201, 10)) = FieldByName('TipoDescuento').AsString  then begin
                cb_TiposDescuento.ItemIndex := i;
            	break;
            end;
        end;

        REditObservaciones.Text := FieldByName('Observaciones').AsString;
        
        CargarPlanesComerciales;
	end;
end;

procedure TFormPromociones.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormPromociones.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormPromociones.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, FieldByName('NumeroPromocion').AsString);
        TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
        TextOut(Cols[2], Rect.Top, FormatDateTime(ShortDateFormat,FieldByName('FechaHoraActivacion').asDateTime));
        TextOut(Cols[3], Rect.Top, FormatDateTime(ShortDateFormat,FieldByName('FechaHoraBaja').asDateTime));
        TextOut(Cols[4], Rect.Top, ObtenerEstado);
	end;
end;

procedure TFormPromociones.FormShow(Sender: TObject);
begin
	DBList1.Reload;
end;

procedure TFormPromociones.BtnAceptarClick(Sender: TObject);
resourcestring
    CAPTION_ACTUALIZAR_PROMOCION = 'Actualizar Promoci�n';

	function execInsertarPromocion:LongInt;
    resourcestring
        MSG_INSERTAR_PROMOCION = 'No se pudo insetar la nueva Promoci�n.';
        CAPTION_INSERTAR_PROMOCION = 'Insertar Promoci�n';
	begin
		result := 0;
		try
			with InsertarPromocion do begin
				Parameters.ParamByName('@FechaHoraActivacion').value := deFechaActivacion.date;
				Parameters.ParamByName('@FechaHoraBaja').value :=  deFechaBaja.date;
				Parameters.ParamByName('@Descripcion').Value := txt_Descripcion.Text;
				Parameters.ParamByName('@TipoParametro').Value := trim(strRight(cb_TiposParametro.text,20));
				Parameters.ParamByName('@ValorInicial'). value := neValorInicial.Value;
				Parameters.ParamByName('@ValorFinal').value := neValorFinal.Value;
				Parameters.ParamByName('@TipoDescuento').value := trim(strRight(cb_TiposDescuento.text,20));
				Parameters.ParamByName('@ValorDescuento').value := txt_descuento.value;
                Parameters.ParamByName('@Observaciones').value := REditObservaciones.Text;
				execProc;
				result := Parameters.ParamByName('@NumeroPromocion').value;
			end;
		except On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_INSERTAR_PROMOCION, E.message, CAPTION_INSERTAR_PROMOCION, MB_ICONSTOP);
			end;
		end;
	end;

	function execActualizarPromocion:Boolean;
    resourcestring
        MSG_ACTUALIZAR_PROMOCION = 'No se pudieron actualizar los datos de la Promoci�n.';
	begin
		result := true;
		try
			with ActualizarPromocion do begin
				Close;
				Parameters.ParamByName('@NumeroPromocion').value := txt_CodigoPromocion.value;
				Parameters.ParamByName('@FechaHoraActivacion').value := deFechaActivacion.date;
				Parameters.ParamByName('@FechaHoraBaja').value :=  deFechaBaja.date;
				Parameters.ParamByName('@Descripcion').Value := txt_Descripcion.Text;
				Parameters.ParamByName('@TipoParametro').Value := trim(strRight(cb_TiposParametro.text,20));
				Parameters.ParamByName('@ValorInicial'). value := neValorInicial.ValueInt;
				Parameters.ParamByName('@ValorFinal').value := neValorFinal.valueInt;
				Parameters.ParamByName('@TipoDescuento').value := trim(strRight(cb_TiposDescuento.text,20));
				Parameters.ParamByName('@ValorDescuento').value := txt_descuento.value;
                Parameters.ParamByName('@Observaciones').value := REditObservaciones.Text;
				execProc;
			end;
        except On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ACTUALIZAR_PROMOCION, E.message, CAPTION_ACTUALIZAR_PROMOCION, MB_ICONSTOP);
                result := false;
			end;
        end;
    end;


resourcestring
    CAPTION_VALIDAR_PROMOCION  = 'Validar datos de la Promoci�n';
    MSG_PLAN_COMERCIAL_SELECCIONADO = 'Debe existir al menos un plan comercial seleccionado.';
    MSG_DESCRIPCION             = 'La promoci�n debe tener una descripci�n.';
    MSG_VALOR_FINAL             = 'El valor final debe ser mayor al inicial.';
    MSG_DISTINTO_CERO           = 'El valor final debe ser distinto de cero.';
    MSG_VALOR_DESCUENTO         = 'Debe existir un valor de descuento.';
    MSG_RANGO_VIGENCIA          = 'Rango de vigencia es inv�lido.';
    MSG_FECHA_ACTIVACION_VACIA  = 'La fecha de activaci�n debe contener un valor.';
    MSG_FECHA_ACTIVACION        = 'La fecha de activaci�n debe ser mayor o igual a la fecha actual.';
    MSG_FECHA_FINALIZACION_VACIA = 'La fecha de finalizaci�n de debe contener un valor.';
    MSG_PLAN_COMERCIAL          = 'No se pudieron actualizar los Planes Comerciales para La Promoci�n';

var i, cantMarcados: Integer;
    DBList1Alta: Boolean;

begin
	cantMarcados := 0;
	for i := 0 to chk_PlanesComerciales.items.count - 1 do begin
        if chk_PlanesComerciales.Checked[i] then
            inc(cantMarcados);
    end;
    if CantMarcados = 0 then begin
		MsgBox(MSG_PLAN_COMERCIAL_SELECCIONADO, CAPTION_VALIDAR_PROMOCION, MB_ICONSTOP);
		exit;
	end;
	if ((DbList1.Estado = Alta) or (ObtenerEstado = EP_PROGRAMADA)) and
	  not ValidateControls([txt_Descripcion, neValorInicial, neValorFinal, txt_descuento, deFechaActivacion,
      deFechaActivacion, deFechaBaja, deFechaBaja],
      [Trim(txt_Descripcion.text) <> '', neValorInicial.value <= neValorFinal.value, neValorFinal.value > 0,
      txt_Descuento.value <> 0, deFechaActivacion.date <= deFechaBaja.date, deFechaActivacion.date <> nullDate,
      deFechaActivacion.date >= date, deFechaBaja.date <> nullDate],
	  CAPTION_VALIDAR_PROMOCION,
      [MSG_DESCRIPCION, MSG_VALOR_FINAL, MSG_DISTINTO_CERO, MSG_VALOR_DESCUENTO, MSG_RANGO_VIGENCIA,
      MSG_FECHA_ACTIVACION_VACIA, MSG_FECHA_ACTIVACION, MSG_FECHA_FINALIZACION_VACIA ]) then exit
	else if not ValidateControls([deFechaBaja,deFechaBaja,deFechaBaja],
      [deFechaActivacion.date <= deFechaBaja.date, deFechaBaja.date >= date, deFechaBaja.date <> nullDate],
      CAPTION_VALIDAR_PROMOCION,
      [MSG_RANGO_VIGENCIA, MSG_FECHA_ACTIVACION, MSG_FECHA_FINALIZACION_VACIA]) then exit;
	Screen.Cursor := crHourGlass;
	DMConnections.BaseCAC.BeginTrans;

	if (DbList1.Estado = Alta) then begin
		txt_CodigoPromocion.value := ExecInsertarPromocion;
		if (txt_CodigoPromocion.value = 0) then exit;
	end else
		if not execActualizarPromocion then exit;

	//Ahora agrego o elimino los Planes Comerciales por Promocion.
	if not OpenTables([tblPromocionPlanComercial]) then begin
		DMConnections.BaseCAC.RollbackTrans;
		Screen.Cursor := crDefault;
		exit;
	end;
	try
		qryEliminarPromocionPlanComercial.parameters.ParamByName('NumeroPromocion').value := Trunc(txt_CodigoPromocion.Value);
		qryEliminarPromocionPlanCOmercial.ExecSql;
	except
		On E: Exception do begin
			DMConnections.BaseCAC.RollbackTrans;
			Screen.Cursor := crDefault;
			MsgBoxErr(MSG_PLAN_COMERCIAL, E.message, CAPTION_ACTUALIZAR_PROMOCION, MB_ICONSTOP);
			exit;
		end;
	end;
	for i:= 0 to chk_PlanesComerciales.Items.Count - 1 do begin
		if chk_PlanesComerciales.Checked[i] then begin
			try
				 tblPromocionPlanComercial.Append;
				 tblPromocionPlanComercial.FieldByName('NumeroPromocion').AsInteger	:= Trunc(txt_CodigoPromocion.Value);
				 tblPromocionPlanComercial.FieldByName('PlanComercial').AsInteger :=
				   IVal(StrRight(chk_PlanesComerciales.Items[i], 20));
				 tblPromocionPlanComercial.Post;
			except
				On E: Exception do begin
					tblPromocionPlanComercial.Cancel;
					DMConnections.BaseCAC.RollbackTrans;
					Screen.Cursor := crDefault;
					MsgBoxErr(MSG_PLAN_COMERCIAL, E.message, CAPTION_ACTUALIZAR_PROMOCION, MB_ICONSTOP);
					exit;
				end;
			end;
		end;
	end;
	tblPromocionPlanComercial.Close;
	DMConnections.BaseCAC.CommitTrans;
	DBList1Alta := DbList1.Estado = Alta;
	Volver_Campos;
	if DBList1Alta then begin
		Promociones.Last;
		DBList1.Reload;
	end;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormPromociones.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFormPromociones.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormPromociones.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormPromociones.Volver_Campos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DBList1.Reload;
	DbList1.SetFocus;
	Tab_General.Enabled 		:= False;
	Tab_Clientes.Enabled 		:= False;
	Tab_Vigencia.Enabled 		:= False;
	deFechaActivacion.Enabled   := True;
	Notebook.PageIndex 			:= 0;
    txt_CodigoPromocion.Enabled := True;
    REditObservaciones.Enabled  := False;
end;


procedure TFormPromociones.CargarPlanesComerciales;
Var
	i: Integer;
begin
    with spObtenerPromocionesPorPlanComercial do begin
		parameters.ParamByName('@NumeroPromocion').value := Promociones.fieldByName('NumeroPromocion').asINteger;
		if not OpenTables([spObtenerPromocionesPorPlanComercial]) then Exit;
		chk_PlanesComerciales.Items.Clear;
		i := 0;
		While not Eof do begin
			chk_PlanesComerciales.Items.Add(PadR(FieldByName('Descripcion').AsString, 500, ' ') +
				  IStr(FieldByName('PlanComercial').AsInteger, 10));
			chk_PlanesComerciales.Checked[i] := FieldByName('Pertenece').AsInteger > 0;
			Inc(i);
			Next;
		end;
		for i := 0 to chk_PlanesComerciales.Items.Count - 1 do
			chk_PlanesComerciales.ItemEnabled[i] := DbList1.Estado in [Alta, Modi];
		Close;
	end;
end;

procedure TFormPromociones.cb_TiposDescuentoChange(Sender: TObject);
begin
	if (sender as TComboBox).ItemIndex = 0 then begin
		txt_Descuento.MaxLength := 6;
		txt_Descuento.decimals := 2;
		txt_Descuento.Value := 0.00;
	end else begin
		txt_Descuento.MaxLength := 14;
		txt_Descuento.decimals := 0;
		txt_Descuento.Value := 0;
	end;
end;

function TFormPromociones.ObtenerEstado: AnsiString;
Var
	Activacion, Baja: TDateTime;
begin
	Activacion := Promociones.FieldByName('FechaHoraActivacion').AsDateTime;
	Baja 	   := Promociones.FieldByName('FechaHoraBaja').AsDateTime;
	if Ceil(Activacion) > date then
		Result := EP_PROGRAMADA
	else begin
		if Ceil(Baja) >= date then
			Result := EP_VIGENTE
		else begin
			Result := EP_FINALIZADA;
		end;
	end;
end;

procedure TFormPromociones.cb_TiposParametroChange(Sender: TObject);
begin
	neValorInicial.Value := 0;
	neValorFinal.Value := 0;
end;

end.
