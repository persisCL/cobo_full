{********************************** Unit Header ********************************
File Name : frmRptCarpetasMorosos.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
    Author : ggomez
    Date : 01/09/2006
    Description : Agregado de reportes que ya exist�an, por ejemplo, ReporteFatura.pas.
        Se copi� el c�digo aqu� pues no pudimos reutilizar lo existente, debido
        a esta copia hay c�digo que permace muy similar a su original.

Revision 2:
    Author: FSandi
    Date: 11-09-2007
    Description : SS 544 - El detalle de consumos ahora se obtiene de una tabla, no se utilizan
                los peajes para calcularlo.


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

*******************************************************************************}
unit frmRptCarpetasMorosos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, UtilRB, ppPrnabl, ppCtrls, daDataView, daQueryDataView, daADO, ppModule,
  daDataModule, ppParameter, ppBands, ppCache, ppStrtch, ppSubRpt, raCodMod, DBClient, DMConnection, ppVar, uTILpROC,
  ppRegion, ppPageBreak, ppFormWrapper, ppRptExp, RBSetup, ppTypes,
  ppBarCod, ppMemo, TeEngine, Series, ExtCtrls, TeeProcs, Chart, DbChart, Util, UtilDB, ConstParametrosGenerales;

type
  TTipoPago = (tpNinguno, tpPAT, tpPAC);
  TfrmCarpetasMorosos = class(TForm)
    subRptNK: TppSubReport;
    plConvenios: TppDBPipeline;
    dsComprobantesImpagos: TDataSource;
    dsConvenios: TDataSource;
    plComprobantesImpagos: TppDBPipeline;
    spCargarComprobantes: TADOStoredProc;
    spObtenerComprobantes: TADOStoredProc;
    plComprobantesImpagosppField58: TppField;
    plComprobantesImpagosppField59: TppField;
    pprCaratula: TppReport;
    ppParameterList2: TppParameterList;
    rbiCaratula: TRBInterface;
    spBorrarTmps: TADOStoredProc;
    spComprobantes: TADOStoredProc;
    spComprobantesNumeroConvenioFormateado: TStringField;
    spComprobantesNumeroConvenio: TStringField;
    spComprobantesCodigoConvenio: TIntegerField;
    spComprobantesTipoComprobante: TStringField;
    spComprobantesNumeroComprobante: TLargeintField;
    spComprobantesPeriodoInicial: TDateTimeField;
    spComprobantesPeriodoFinal: TDateTimeField;
    spComprobantesFechaEmision: TDateTimeField;
    spComprobantesFechaVencimiento: TDateTimeField;
    spComprobantesNombreCliente: TStringField;
    spComprobantesDomicilio: TStringField;
    spComprobantesComuna: TStringField;
    spComprobantesComentarios: TStringField;
    spComprobantesCodigoBarras: TStringField;
    spComprobantesCodigoPostal: TStringField;
    spComprobantesTotalAPagar: TStringField;
    spComprobantesCodigoTipoMedioPago: TWordField;
    spComprobantesEstadoPago: TStringField;
    spComprobantesSaldoPendiente: TStringField;
    spComprobantesAjusteSencilloAnterior: TStringField;
    spComprobantesAjusteSencilloActual: TStringField;
    spComprobantesFechaHoy: TDateTimeField;
    spObtenerCargos: TADOStoredProc;
    spUltimosDoce: TADOStoredProc;
    spUltimosDoceIDh: TDateTimeField;
    spUltimosDoceMes: TStringField;
    spUltimosDoceTotalAPagar: TBCDField;
    spObtenerConsumos: TADOStoredProc;
    spObtenerConsumosNroMatricula: TStringField;
    spObtenerConsumosKilometros: TBCDField;
    spObtenerConsumosDImporteCongestion: TStringField;
    spObtenerConsumosDImportePunta: TStringField;
    spObtenerConsumosDImporteNormal: TStringField;
    spObtenerConsumosDImporte: TStringField;
    spMensaje: TADOStoredProc;
    spMensajeTexto: TStringField;
    plComprobantes: TppDBPipeline;
    dsComprobantes: TDataSource;
    dbcUltimosDoce: TDBChart;
    Series1: TBarSeries;
    ppTitleBand2: TppTitleBand;
    ppLabel7: TppLabel;
    ppImgLogoNK: TppImage;
    ppLabel1: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppLine6: TppLine;
    ppNombre: TppLabel;
    ppApellido: TppLabel;
    ppConvenio: TppLabel;
    ppLine7: TppLine;
    ppLabel8: TppLabel;
    ppLabel32: TppLabel;
    ppLabel33: TppLabel;
    crComprobantesImpagos: TppChildReport;
    ppDetailNK: TppDetailBand;
    ppLblCopiaFiel: TppLabel;
    ppShape18: TppShape;
    ppdbtNombre: TppDBText;
    ppdbtDir1: TppDBText;
    ppdbtDir2: TppDBText;
    ppdbtTotalAPagar: TppDBText;
    ppLabel20: TppLabel;
    ppdbtNumConvenioFormat: TppDBText;
    ppLine3: TppLine;
    ppLabel26: TppLabel;
    pplblTotal: TppLabel;
    pplblCongestion: TppLabel;
    pplblPunta: TppLabel;
    pplblNormal: TppLabel;
    pplblKm: TppLabel;
    ppLine10: TppLine;
    lblTestLegth: TppLabel;
    ppdbbcConvenio: TppDBBarCode;
    ppShape17: TppShape;
    ppLabel25: TppLabel;
    ppSubReport2: TppSubReport;
    rptReporteConsumo: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand3: TppDetailBand;
    dbtDetalleConsumoNroMatricula: TppDBText;
    dbtDetalleConsumoKilometros: TppDBText;
    dbtDetalleConsumoImporteNormal: TppDBText;
    dbtDetalleConsumoImportePunta: TppDBText;
    dbtDetalleConsumoImporteCongestion: TppDBText;
    dbtDetalleConsumoImporte: TppDBText;
    ppLblAnexo: TppLabel;
    raCodeModule1: TraCodeModule;
    ppDBText24: TppDBText;
    ppSubReport4: TppSubReport;
    ppChildReport4: TppChildReport;
    ppTitleBand4: TppTitleBand;
    ppDetailBand5: TppDetailBand;
    ppDBMemo1: TppDBMemo;
    ppSummaryBand2: TppSummaryBand;
    raCodeModule2: TraCodeModule;
    imgGraficoUltimosDoce: TppImage;
    ppLabel13: TppLabel;
    ppDBText23: TppDBText;
    ppdbbcConvenio2: TppDBBarCode;
    imgPublicidad: TppImage;
    imgPACPAT: TppImage;
    imgPagada: TppImage;
    ppLabel29: TppLabel;
    ppLabel28: TppLabel;
    ppdbtNroNK2: TppDBText;
    ppdbtConvenio2: TppDBText;
    ppdbtVencimiento2: TppDBText;
    ppdbtTotalAPagar2: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    gfConvenio: TppGroupFooterBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppRegion1: TppRegion;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppShape3: TppShape;
    ppLabel4: TppLabel;
    ppShape4: TppShape;
    ppLabel15: TppLabel;
    ppShape5: TppShape;
    ppShape6: TppShape;
    ppShape7: TppShape;
    ppLine4: TppLine;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppDBText22: TppDBText;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppLine9: TppLine;
    plObtenerCargos: TppDBPipeline;
    plUltimosDoce: TppDBPipeline;
    plMensaje: TppDBPipeline;
    dsObtenerCargos: TDataSource;
    dsUltimosDoce: TDataSource;
    dsObtenerConsumos: TDataSource;
    dsMensaje: TDataSource;
    ppSubReport3: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    raCodeModule4: TraCodeModule;
    spObtenerCargosDescripcion: TStringField;
    spObtenerCargosImporte: TLargeintField;
    spObtenerCargosDescImporte: TStringField;
    plComprobantesImpagosppField60: TppField;
    ppSubRptDetConsumos: TppSubReport;
    ppRegion2: TppRegion;
    ppShape10: TppShape;
    ppShape11: TppShape;
    ppShape12: TppShape;
    ppLabel14: TppLabel;
    ppShape13: TppShape;
    ppLabel18: TppLabel;
    ppShape14: TppShape;
    ppShape15: TppShape;
    ppShape16: TppShape;
    ppLine5: TppLine;
    ppLabel19: TppLabel;
    ppLabel21: TppLabel;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppShape19: TppShape;
    ppLabel22: TppLabel;
    ppLine11: TppLine;
    ppColumnHeaderBand2: TppColumnHeaderBand;
    ppLine8: TppLine;
    ppLabel27: TppLabel;
    ppLabel24: TppLabel;
    ppLabel23: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLabel34: TppLabel;
    ppLine12: TppLine;
    ppDetailBand1: TppDetailBand;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppColumnFooterBand2: TppColumnFooterBand;
    ppGroup4: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    ppLine14: TppLine;
    ppLabel35: TppLabel;
    ppLblCongestion17: TppLabel;
    ppLblPunta17: TppLabel;
    pplblNormal17: TppLabel;
    pplblKm17: TppLabel;
    ppLbltotal17: TppLabel;
    ppLine15: TppLine;
    pbSaltoDetalleConsumos: TppPageBreak;
    ppChildReport2: TppChildReport;
    plConsumos: TppDBPipeline;
    pbSaltoComprobante: TppPageBreak;
    ppTitleBand1: TppTitleBand;
    ppFooterBand1: TppFooterBand;
    spObtenerTransitosNoFacturadosDeConvenio: TADOStoredProc;
    dsspObtenerTransitosNoFacturadosDeConvenio: TDataSource;
    plTransitosNoFacturados: TppDBPipeline;
    pbSaltoTransitosNoFacturados: TppPageBreak;
    srTransitosNoFacturados: TppSubReport;
    ppChildReport6: TppChildReport;
    ppTitleBand7: TppTitleBand;
    ppSystemVariable4: TppSystemVariable;
    ppLabel61: TppLabel;
    ppLabel70: TppLabel;
    ppLine24: TppLine;
    ppLabel71: TppLabel;
    ppDBText45: TppDBText;
    ppColumnHeaderBand5: TppColumnHeaderBand;
    ppLine25: TppLine;
    ppLabel72: TppLabel;
    ppLabel73: TppLabel;
    ppLabel74: TppLabel;
    ppLabel75: TppLabel;
    ppLabel76: TppLabel;
    ppLine26: TppLine;
    ppDetailBand7: TppDetailBand;
    ppDBText52: TppDBText;
    ppDBText53: TppDBText;
    ppDBText54: TppDBText;
    ppDBText55: TppDBText;
    ppDBText56: TppDBText;
    ppColumnFooterBand5: TppColumnFooterBand;
    ppFooterBand6: TppFooterBand;
    ppGroup8: TppGroup;
    ppGroupHeaderBand8: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppShape40: TppShape;
    ppShape41: TppShape;
    ppLabel80: TppLabel;
    ppDBCalc8: TppDBCalc;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    lblTransitosNoFacturadosTotal: TppLabel;
    ppShape8: TppShape;
    dbcTransitosnoFacturadosTotal: TppDBCalc;
    ppShape9: TppShape;
    srTransitosNoFacturadosNoHay: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand5: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppLine13: TppLine;
    lbllblTransitosNoFacturadosNoHayTit: TppLabel;
    lblTransitosNoFacturadosNoHayFecha: TppLabel;
    svTransitosNoFacturadosNoHayFecha: TppSystemVariable;
    lblTransitosNoFacturadosNoHayMensaje: TppLabel;
    ppImage2: TppImage;
    srComprobantesImpagosNoHay: TppSubReport;
    ppChildReport5: TppChildReport;
    ppTitleBand8: TppTitleBand;
    ppDetailBand8: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    lblComprobantesImpagosNoHayFecha: TppLabel;
    svComprobantesImpagosNoHayFecha: TppSystemVariable;
    lblComprobantesImpagosNoHayTit: TppLabel;
    lineComprobantesImpagosNoHay: TppLine;
    lblComprobantesImpagosNoHayMensaje: TppLabel;
    ppHeaderBand2: TppHeaderBand;
    ppLabel38: TppLabel;
    cdsConvenios: TClientDataSet;
    ppImage1: TppImage;
    ppImage4: TppImage;
    ppImage3: TppImage;
    lblUsuario: TppLabel;
    procedure ppDetailNKBeforePrint(Sender: TObject);
    procedure pprCaratulaBeforePrint(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ppLblCopiaFielPrint(Sender: TObject);
    procedure plComprobantesBeforeClose(Sender: TObject);
  private
    FSesion: Smallint;
    FTipoComprobante: String;
    FNumeroComprobante: Int64;
    FDirImagenPublicidad, FDirImagenPagada, FDirImagenPAT, FDirImagenPAC, FDirImagenLogoNK,
    FNombreImagenPagada, FNombreImagenPublicidad,FNombreImagenFondo, FNombreImagenLogoNK,
    FDirImagenFondo, FNombreImagenPAC,
    FNombreImagenPAT, FNombreImagenFondoAnterior: AnsiString;
    FConexion: TADOConnection;
    FTipoPago : TTipoPago;
    FMostrarConsumoAparte: Boolean;
    FIncluirFondo: boolean;
    FIsPDF: Boolean;
    FPreparado: boolean;
    FFechaDeCorteNuevoRutConcesionaria: TDateTime;
    procedure CerrarDatosNotaDeCobro;

    procedure PrepararNotaDeCobro;
    procedure ObtenerConsumosComprobante(TipoCompro: AnsiString; NumeroCompro: Int64);

    procedure ObtenerTransitosNoFacturados(CodigoConvenio: Integer);

    procedure ObtenerValoresGenerales;

    function EjecutarReporte: Boolean;
  public
    FImprimioComprobante: Boolean;
    function Inicializar(Recorset: TClientDataSet; IdSesion: Smallint): Boolean;
    property MostrarConsumoAparte: boolean read FMostrarConsumoAparte;
  end;

implementation

uses ImprimirWO;
{$R *.dfm}

{ TrptCarpeatasMorosos }
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_EJECUTAR_REPORTE  = 'Error al mostrar el reporte.';
    MSG_ERROR_SP_COMPROBANTES   = 'Error al intentar procesar Comprobante %s %s';
    MSG_ERROR_RPT_NK_DET_INI    = 'Error al intentar inicializar el Reporte de N�ta de Cobro %s';
    MSG_ERROR_REPORTES_TRAN     = 'Error al intenar ejecutar Reporte de Detalle de Tr�nsitos.';
    MSG_ERROR_REPORTES_NK       = 'Error al intenar ejecutar Reporte de NK.';
    MSG_ERROR_REPORTE_TRNF      = 'Error al intentar ejecutar la impresi�n de los tr�nsitos no facturados.';
    MSG_ERROR_SP_DETALLLE_TRANSITOS = 'Error al intentar procesar Tr�nsitos No Facturados. Comprobante %s %s';
    MSG_NO_EXISTEN_COMP = 'No existen comprobantes para los siguientes Convenios: ';
    MSG_ERROR_OBTENIENDO_DATOS  = 'Error al obtener los datos para el reporte del convenio c�digo: %s';
    MSG_MISSED_IMAGES_FILES = 'No existen todas las im�genes requeridas para '
        + CRLF + 'poder imprimir una Nota de Cobro en el sistema';
    MSG_ERROR_TABLAS_TMP = 'Error al intentar borrar las tablas temporales on ID de Sesi�n %s.';
    MSG_ERROR_SP = 'Error al intentar ejecutar procedimiento almacenado %s';

var
  DescriErr: String;

function TfrmCarpetasMorosos.EjecutarReporte: Boolean;
begin
    Result := False;
    cdsConvenios.First;
    // Por cada convenio seleccionado para imprimir, ejecutar el reporte.
    while not cdsConvenios.Eof do begin
        if cdsConvenios.FieldByName('cdsConveniosEnCarpetaSeleccionado').AsBoolean = True then begin
            try
                //ejecuto la carga de comprobantes para el convenio actual de los seleccionados.
                spCargarComprobantes.Close;
                spCargarComprobantes.Parameters.Refresh;
                spCargarComprobantes.Parameters.ParamByName('@CodigoConvenio').Value := cdsConvenios.FieldByName('cdsConveniosEnCarpetaCodigoConvenio').Value;
                spCargarComprobantes.ExecProc;

                //obtengo los comprobantes del convenio
                spObtenerComprobantes.Close;
                spObtenerComprobantes.Parameters.ParamByName('@IdSesion').Value := FSesion;
                spObtenerComprobantes.Parameters.ParamByName('@CodigoConvenio').Value := cdsConvenios.FieldByName('cdsConveniosEnCarpetaCodigoConvenio').Value;
                spObtenerComprobantes.Open;
    
                // Si no hay tr�nsitos Comprobantes Impagos, entonces no mostrar el subreporte
                subRptNK.Visible                    := not spObtenerComprobantes.IsEmpty;
                srComprobantesImpagosNoHay.Visible  := not subRptNK.Visible;
    
                // Obtener los tr�nsitos no facturados.
                ObtenerTransitosNoFacturados(cdsConvenios.FieldByName('cdsConveniosEnCarpetaCodigoConvenio').Value);
    
                // Si no hay tr�nsitos No Facturados, entonces no mostrar el subreporte
                srTransitosNoFacturados.Visible         := not spObtenerTransitosNoFacturadosDeConvenio.IsEmpty;
                srTransitosNoFacturadosNoHay.Visible    := not srTransitosNoFacturados.Visible;
    
                // Mostrar el reporte para el convenio actual.
                if not rbiCaratula.Execute(True) then begin
                    Exit;
                end;
            except
                on E: Exception do begin
                    MsgBoxErr(Format(MSG_ERROR_OBTENIENDO_DATOS,
                        [cdsConvenios.FieldByName('cdsConveniosEnCarpetaCodigoConvenio').AsString]),
                        E.Message, Self.Caption, MB_OK);
                    Exit;
                end;
            end;
    
            try
                //borro los datos de las tbls temporales de la sesi�n y paso al siguiente convenio
                spBorrarTmps.Parameters.ParamByName('@IdSesion').Value := FSesion;
                spBorrarTmps.ExecProc;
            except
                on E: Exception do begin
                    MsgBoxErr(Format(MSG_ERROR_TABLAS_TMP, [VarToStr(spBorrarTmps.Parameters.ParamByName('@IdSesion').Value)]),
                      E.Message, Self.Caption, MB_OK);
                end;
            end;
        end;
        cdsConvenios.Next;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created :
Description :
Parameters : Recorset: TClientDataSet; IdSesion: Smallint
Return Value : Boolean

Revision 1:
    Author : ggomez
    Date : 01/09/2006
    Description : Agregu� la carga de valores generales al reporte.
*******************************************************************************}
function TfrmCarpetasMorosos.Inicializar(Recorset: TClientDataSet; IdSesion: Smallint): Boolean;
resourcestring
    MSG_ERROR_CARGAR_VALORES_GENERALES = 'Error al obtener los valores iniciales para el reporte.';
var                                                                                                     //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                                                 //SS_1147_NDR_20140710

begin

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImgLogoNK.Picture.LoadFromFile(Trim(RutaLogo));                                                 //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710


    Result := False;
    DescriErr := '';
    cdsConvenios.Data := Recorset.Data;
    FSesion := IdSesion;

    FConexion := DMConnections.BaseCAC;
    if not FConexion.Connected then FConexion.Open;

    //Asignamos la conexion que nos indicaron usar
    spComprobantes.Connection       := FConexion;
    spObtenerCargos.Connection      := FConexion;
    spUltimosDoce.Connection        := FConexion;
    spObtenerConsumos.Connection    := FConexion;
    spMensaje.Connection            := FConexion;

    // Inicializar las variables con las imagenes de la Nota de Cobro
    try
        ObtenerValoresGenerales;
    except
        on E: Exception do begin
            Result := False;
    	    MsgBoxErr(MSG_ERROR_CARGAR_VALORES_GENERALES, E.Message, MSG_ERROR, MB_ICONSTOP);
            Exit;
        end;
    end;

    if EjecutarReporte then begin
        Result := True;
    end;
end;



procedure TfrmCarpetasMorosos.pprCaratulaBeforePrint(Sender: TObject);
begin
    ppApellido.Text := cdsConvenios.FieldByName('cdsConveniosEnCarpetaApellido').AsString;
    ppNombre.Text := cdsConvenios.FieldByName('cdsConveniosEnCarpetaNombre').AsString;
    ppConvenio.Text := cdsConvenios.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString;
end;

procedure TfrmCarpetasMorosos.plComprobantesBeforeClose(Sender: TObject);
begin
//
end;

procedure TfrmCarpetasMorosos.ppDetailNKBeforePrint(Sender: TObject);
begin
    PrepararNotaDeCobro;

end;

procedure TfrmCarpetasMorosos.PrepararNotaDeCobro;
var
    TmpFile: String;
    CodigoTipoMedioPago: Byte;
    Pagada : Boolean;
	OldDecimalSeparator : char;
    OldThousandSeparator : char;
    Desplazamiento: single;
    oldHeight: single;
    oldWidth: single;
    CantVehiculos: integer;
    FechaCreacion: TDateTime;
    NombreSP_Error: string;
begin
    FPreparado := true;
    Pagada := False;

    // Setear las variables con el comprobante a imprimir.
    FTipoComprobante    := spObtenerComprobantes.FieldByName('TipoComprobante').AsString;
    FNumeroComprobante  := spObtenerComprobantes.FieldByName('NumeroComprobante').AsInteger;

    // Obtener la Fecha de creaci�n de la Nota de Cobro a Imprimir
//    FechaCreacion := QueryGetValueDateTime(DMConnections.BaseCAC, format('SELECT dbo.ObtenerFechaCreacionComprobante(''%s'',%d)',
//            [FTipoComprobante,FNumeroComprobante]));
    FechaCreacion := spObtenerComprobantes.FieldByName('FechaCreacion').AsDateTime;

    // Si la Fecha de Creaci�n del Comprobante es anterior a la Fecha desde cuando la
    // concesionaria tiene nuevo RUT, entonces imprimir el RUT Anterior, sino
    // imprimir el RUT Nuevo.
    if ( FechaCreacion < FFechaDeCorteNuevoRutConcesionaria ) then begin
        ppImage1.Picture.LoadFromFile(FNombreImagenFondoAnterior);
    end else begin
        ppImage1.Picture.LoadFromFile(FNombreImagenFondo);
    end; // else if

     CerrarDatosNotaDeCobro;

    // Guarda los separadores decimales y los cambia
    // por "," para decimales y "." para miles
    OldDecimalSeparator	:= DecimalSeparator;
    OldThousandSeparator:= ThousandSeparator;
    DecimalSeparator	:= ',';
    ThousandSeparator	:= '.';
    try
        NombreSP_Error := spComprobantes.ProcedureName;
        spComprobantes.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        spComprobantes.Open;

        //Determinamos el tipo de medio de pago del comprobante y si est� pagado
        CodigoTipoMedioPago := spComprobantes.FieldByName('CodigoTipoMedioPago').AsInteger;
        Pagada := (spComprobantes.FieldByName('EstadoPago').AsString = 'P');
        FTipoPago := TTipoPago(CodigoTipoMedioPago);

        NombreSP_Error := spObtenerCargos.ProcedureName;
        spObtenerCargos.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        spObtenerCargos.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        spObtenerCargos.Open;

        ObtenerConsumosComprobante(FTipoComprobante, FNumeroComprobante);

        // Si no hay detalle de consumos, entonces no mostrar el subreporte de consumos
        // que sale en hoja aparte
        pbSaltoDetalleConsumos.Visible  := not spObtenerConsumos.IsEmpty;
        ppSubRptDetConsumos.Visible     := not spObtenerConsumos.IsEmpty;

        NombreSP_Error := spMensaje.ProcedureName;
        spMensaje.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        spMensaje.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        spMensaje.Open;
        NombreSP_Error := spUltimosDoce.ProcedureName;
        spUltimosDoce.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        spUltimosDoce.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        spUltimosDoce.Open;
    except
        on E: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_SP, [NombreSP_Error]), E.Message, Self.Caption, MB_OK);
        end;
    end;
    // Creamos el gr�fico y lo guardamos como imagen a disco
    dbcUltimosDoce.RefreshData ;
    TmpFile := GoodDir(GetTempDir) + TempFile + '.bmp';
    dbcUltimosDoce.SaveToBitmapFile(TmpFile);

    // Asigno la imagen del grafico al img del Report
    imgGraficoUltimosDoce.Picture.LoadFromFile(TmpFile);
    DeleteFile(TmpFile);

    //Determinamos cuales de todas las imagenes de pago tenemos que mostrar
    imgPACPAT.Visible := False;
    imgPagada.Visible := False;
    if (Pagada) and (FTipoPago = tpNinguno) and FileExists(FNombreImagenPagada) then begin
        imgPagada.Picture.LoadFromFile(FNombreImagenPagada);
        imgPagada.Visible := True;
    end else begin
        case FTipoPago of
            tpPAC:
                begin
                    imgPACPAT.Picture.LoadFromFile(FNombreImagenPAC);
                    imgPACPAT.Visible := True;
                end;
            tpPAT:
                begin
                    imgPACPAT.Picture.LoadFromFile(FNombreImagenPAT);
                    imgPACPAT.Visible := True;
                end;
        end;
    end;

    if FileExists(FNombreImagenPublicidad) then
      imgPublicidad.Picture.LoadFromFile(FNombreImagenPublicidad);

    // Vuelve a dejar los separadores decimales como estaban
    DecimalSeparator  := OldDecimalSeparator;
    ThousandSeparator := OldThousandSeparator;

    if not FIsPDF then begin
        // Chequea que no haya lineas sin completar en la direccion
        Desplazamiento := 0;
        if    (  spComprobantes.FieldByName('Domicilio').IsNull)
           or (  TRIM( spComprobantes.FieldByName('Domicilio').AsString) = '')
        then begin
                With  ppdbtDir1 do begin
                    Desplazamiento := Desplazamiento +  Height;
                    Visible := False;
                end;
        end
        else begin
               if Length(ppdbtDir1.Text) < 40 then begin
                    oldHeight   := ppdbtDir1.Height;
                    oldWidth    := ppdbtDir1.Width ;
                    ppdbtDir1.Autosize := True;
                    Desplazamiento := Desplazamiento + (oldHeight - ppdbtDir1.Height);
                    ppdbtDir1.Autosize := False;
                    ppdbtDir1.Width := oldWidth;
               end;
               ppdbtDir1.WordWrap := True;
        end;
        lblTestLegth.Visible := FALSE;

        if spComprobantes.FieldByName('Comuna').IsNull
           or (  TRIM( spComprobantes.FieldByName('Comuna').AsString) = '')
        then begin
                    Desplazamiento := Desplazamiento +  ppdbtDir2.Height;
                    ppdbtDir2.Visible := False;
        end
        else begin
            ppdbtDir2.Top := ppdbtDir2.Top - Desplazamiento;
        end;
        if spComprobantes.FieldByName('CodigoPostal').IsNull
           or (  TRIM( spComprobantes.FieldByName('CodigoPostal').AsString) = '')
        then begin
                    ppdbbcConvenio.Visible := False;
        end
        else begin
            ppdbbcConvenio.Top := ppdbbcConvenio.Top - Desplazamiento;
        end;

        ppdbtNumConvenioFormat.Top := ppdbtNumConvenioFormat.Top - Desplazamiento - iif( ppdbbcConvenio.Visible, 0, ppdbbcConvenio.Height);
        pplabel20.Top := pplabel20.Top - Desplazamiento - iif( ppdbbcConvenio.Visible, 0, ppdbbcConvenio.Height);
    end;
    lblTestLegth.Visible := FALSE;

    pplblKm.Caption         := iif( spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
                            Trim(Format('%.2n', [real(spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value)] )));
    pplblNormal.Caption     := iif( spObtenerConsumos.Parameters.ParamByName('@TotalN').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalN').Value );
    pplblCongestion.Caption := iif( spObtenerConsumos.Parameters.ParamByName('@TotalC').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalC').Value );
    pplblPunta.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalP').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalP').Value );
    pplblTotal.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value );

    CantVehiculos :=  spObtenerconsumos.RecordCount ;
    FMostrarConsumoAparte := False;
    if CantVehiculos > 17 then begin
        pbSaltoDetalleConsumos.Visible  := True;
        ppSubRptDetConsumos.Visible     := True;

        dbtDetalleConsumoNroMatricula.Visible       := False;
        dbtDetalleConsumoKilometros.Visible         := False;
        dbtDetalleConsumoImporteNormal.Visible      := False;
        dbtDetalleConsumoImportePunta.Visible       := False;
        dbtDetalleConsumoImporteCongestion.Visible  := False;
        dbtDetalleConsumoImporte.Visible            := False;

        ppLblAnexo.Visible := True;
        crComprobantesImpagos.Bands[1].Height := 2.2080;

        pplblKm17.Caption := iif(spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
                                   Trim(Format('%.2n', [real(spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value)] )));
        pplblNormal17.Caption := iif(spObtenerConsumos.Parameters.ParamByName('@TotalN').Value = null,'',
                                       spObtenerConsumos.Parameters.ParamByName('@TotalN').Value );
        pplblCongestion17.Caption := iif(spObtenerConsumos.Parameters.ParamByName('@TotalC').Value = null,'',
                                           spObtenerConsumos.Parameters.ParamByName('@TotalC').Value );
        pplblPunta17.Caption := iif(spObtenerConsumos.Parameters.ParamByName('@TotalP').Value = null,'',
                                      spObtenerConsumos.Parameters.ParamByName('@TotalP').Value );
        pplblTotal17.Caption := iif(spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value = null,'',
                                      spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value );
        FMostrarConsumoAparte := True;
    end else begin
        pbSaltoDetalleConsumos.Visible := False;
        ppSubRptDetConsumos.Visible := False;

        dbtDetalleConsumoNroMatricula.Visible       := True;
        dbtDetalleConsumoKilometros.Visible         := True;
        dbtDetalleConsumoImporteNormal.Visible      := True;
        dbtDetalleConsumoImportePunta.Visible       := True;
        dbtDetalleConsumoImporteCongestion.Visible  := True;
        dbtDetalleConsumoImporte.Visible            := True;

        ppLblAnexo.Visible := False;
        crComprobantesImpagos.Bands[1].Height := 0.1775;
        FMostrarConsumoAparte := False;
    end;

end;


procedure TfrmCarpetasMorosos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    spComprobantes.Close;
    spUltimosDoce.Close;
    spMensaje.Close;
    spObtenerConsumos.Close;
    spObtenerTransitosNoFacturadosDeConvenio.Close;
end;



procedure TfrmCarpetasMorosos.ppLblCopiaFielPrint(Sender: TObject);
begin
    if FIsPDF then ppLblCopiaFiel.Top := ppdbtDir1.Top + ppLblCopiaFiel.Height;
end;

procedure TfrmCarpetasMorosos.ObtenerConsumosComprobante(TipoCompro: AnsiString;
  NumeroCompro: Int64);
{var
    sp: TADOStoredProc;
  	NumCorrCADesde: Int64;
    NumCorrCAHasta: Int64;}//revision 2
begin
    spObtenerConsumos.Close;

  {  sp := TADOStoredProc.Create(nil);
    try
        try
            sp.Connection       := spObtenerConsumos.Connection;
            // Colocar 500 segundos, pues hay convenios para los que puede demorar algo m�s que 30 seg.
            sp.CommandTimeout   := 500;

            // Cargar la Tabla con Los NumCorrCA de los tr�nsitos de la NK.
            sp.ProcedureName := 'ImprimirNK_CargarNumCorrCADeNotaCobro';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
            sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
            sp.ExecProc;
            sp.Close;

            // Cargar la Tabla con el resto de los datos de tr�nsitos de la NK.
            sp.ProcedureName := 'ImprimirNK_CargarTransitosDeNotaCobro';
            sp.Parameters.Refresh;

            // Inicializar variables
            NumCorrCADesde  := -1;
            NumCorrCAHasta  := 0;
            // Mientras haya tr�nsitos para cargar.
            while (NumCorrCADesde <> NumCorrCAHasta) do begin
                NumCorrCADesde := NumCorrCaHasta + 1;

                sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
                sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
                sp.Parameters.ParamByName('@NumCorrCADesde').Value  := NumCorrCADesde;
                sp.Parameters.ParamByName('@NumCorrCAHasta').Value  := 0;
                sp.ExecProc;
                // Obtener el �ltimo n�mero de tr�nsito procesado
                NumCorrCAHasta := sp.Parameters.ParamByName('@NumCorrCAHasta').Value;

                sp.Close;
            end; // while
        except
            on E: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_SP, [sp.ProcedureName]), E.Message, Self.Caption, MB_OK);
            end;
        end;
    finally
        sp.Close;
        sp.Free;
    end; // finally   } //revision 2

    // Obtener los datos de los consumos.
    try
        spObtenerConsumos.Parameters.Refresh;                                                      //revision 2
        spObtenerConsumos.Parameters.ParamByName('@TipoComprobante').Value      := TipoCompro;     //revision 2
        spObtenerConsumos.Parameters.ParamByName('@NumeroComprobante').Value    := NumeroCompro;   //revision 2
        spObtenerConsumos.Open;
    except
        on E: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_SP, [spObtenerConsumos.ProcedureName]), E.Message, Self.Caption, MB_OK);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CerrarDatosNotaDeCobro
Author : ggomez
Date Created : 31/08/2006
Description : Cierra los componentes de datos asociados a la Nota de Cobro.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmCarpetasMorosos.CerrarDatosNotaDeCobro;
begin
    if spComprobantes.Active    then spComprobantes.Close;
    if spObtenerCargos.Active   then spObtenerCargos.Close;
    if spObtenerConsumos.Active then spObtenerConsumos.Close;
    if spMensaje.Active         then spMensaje.Close;
    if spUltimosDoce.Active     then spUltimosDoce.Close;

end;



{******************************** Function Header ******************************
Function Name: ObtenerTransitosNoFacturados
Author : ggomez
Date Created : 01/09/2006
Description : Obtiene los tr�nsitos no facturados de un convenio.
Parameters : CodigoConvenio: Integer
Return Value : None
*******************************************************************************}
procedure TfrmCarpetasMorosos.ObtenerTransitosNoFacturados(
  CodigoConvenio: Integer);
begin
    spObtenerTransitosNoFacturadosDeConvenio.Close;
    try
        spObtenerTransitosNoFacturadosDeConvenio.Parameters.ParamByName('@codigoConvenio').Value := CodigoConvenio;
        spObtenerTransitosNoFacturadosDeConvenio.Open;
    except
        on E: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_SP, [spObtenerTransitosNoFacturadosDeConvenio.ProcedureName]),
              E.Message, Self.Caption, MB_OK);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerValoresGenerales
Author : ggomez
Date Created : 01/09/2006
Description : Carga los valores que se utilizan en el reporte como por ejemplo
    las imagenes de las Nota de Cobro.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmCarpetasMorosos.ObtenerValoresGenerales;
var
    // Declarada por si alguna vez se requiere imprimir este reporte desde la web.
    // Esto lo hice para mantener el c�digo lo m�s similar posible al ReporteFactura.pas.
    EsWeb: Boolean;
    // Declarada por si alguna vez se requiere imprimir este reporte con fondo o sin fondo.
    // Esto lo hice para mantener el c�digo lo m�s similar posible al ReporteFactura.pas.
    IncluirFondo: Boolean;
begin
    EsWeb := False;
    IncluirFondo := True;

    // Obtener la fecha desde cuando la concesionaria tiene un nuevo RUT.
    FFechaDeCorteNuevoRutConcesionaria := NullDate;
    ObtenerParametroGeneral(FConexion, 'FECHA_DE_CORTE_RUT', FFechaDeCorteNuevoRutConcesionaria);

    if EsWeb then begin
        ObtenerParametroGeneral(FConexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPublicidad);
        ObtenerParametroGeneral(FConexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPagada);
        ObtenerParametroGeneral(FConexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPAT);
        ObtenerParametroGeneral(FConexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPAC);
        ObtenerParametroGeneral(FConexion, 'DIR_WEB_IMAGENES_NC', FDirImagenFondo);
    end else begin
        ObtenerParametroGeneral(FConexion, 'DIR_LOGO_NOTA_COBRO', FDirImagenLogoNK);
        ObtenerParametroGeneral(FConexion, 'DIR_IMAGEN_PUBLICIDAD', FDirImagenPublicidad);
        ObtenerParametroGeneral(FConexion, 'DIR_IMAGEN_PAGADA', FDirImagenPagada);
        ObtenerParametroGeneral(FConexion, 'DIR_IMAGEN_SELLO_PAT', FDirImagenPAT);
        ObtenerParametroGeneral(FConexion, 'DIR_IMAGEN_SELLO_PAC', FDirImagenPAC);
        ObtenerParametroGeneral(FConexion, 'DIR_IMAGEN_FONDO_NOTA_COBRO', FDirImagenFondo);
    end;

    ObtenerParametroGeneral(FConexion, 'NOMBRE_LOGO_NOTA_COBRO', FNombreImagenLogoNK);
    ObtenerParametroGeneral(FConexion, 'NOMBRE_IMAGEN_PUBLICIDAD', FNombreImagenPublicidad);
    ObtenerParametroGeneral(FConexion, 'NOMBRE_IMAGEN_PAGADA', FNombreImagenPagada);
    ObtenerParametroGeneral(FConexion, 'NOMBRE_IMAGEN_SELLO_PAT', FNombreImagenPAT);
    ObtenerParametroGeneral(FConexion, 'NOMBRE_IMAGEN_SELLO_PAC', FNombreImagenPAC);

    ObtenerParametroGeneral(FConexion, 'IMAGEN_FONDO_NOTA_COBRO', FNombreImagenFondo);
    ObtenerParametroGeneral(FConexion, 'IMAGEN_FONDO_NOTA_COBRO_ANTERIOR', FNombreImagenFondoAnterior);

    FDirImagenLogoNK := GoodDir(FDirImagenLogoNK);
    FDirImagenPublicidad := GoodDir(FDirImagenPublicidad);
    FDirImagenPAT := GoodDir(FDirImagenPAT);
    FDirImagenPAC := GoodDir(FDirImagenPAC);
    FNombreImagenLogoNK := FDirImagenLogoNK + FNombreImagenLogoNK;
    FNombreImagenPublicidad := FDirImagenPublicidad + FNombreImagenPublicidad;
    FNombreImagenPAT := FDirImagenPAT + FNombreImagenPAT;
    FNombreImagenPAC := FDirImagenPAC + FNombreImagenPAC;
    FDirImagenPagada := GoodDir(FDirImagenPagada);
    FNombreImagenPagada := FDirImagenPagada + FNombreImagenPagada;
    FDirImagenFondo := GoodDir(FDirImagenFondo);
    FNombreImagenFondo := FDirImagenFondo + FNombreImagenFondo;

    FNombreImagenFondoAnterior := FDirImagenFondo + FNombreImagenFondoAnterior;

    // Determina si usa el fondo o no
    FIncluirfondo := IncluirFondo;

    // Verificamos que existan las im�genes que se necesitan obligatoriamente para poder
    // imprimir una nota de cobro
    //PAT y PAC
    if (not FileExists(FNombreImagenLogoNK)) or (not FileExists(FNombreImagenLogoNK)) then begin
        raise Exception.Create(MSG_MISSED_IMAGES_FILES)
    end else begin
        ppImgLogoNK.Picture.LoadFromFile(FNombreImagenLogoNK);
    end;

    if (not FileExists(FNombreImagenPAT))
      or (not FileExists(FNombreImagenPAC)) then begin
        raise Exception.Create(MSG_MISSED_IMAGES_FILES);
    end;

    // Fondo
    if (not FileExists(FNombreImagenFondo)) then begin
        raise Exception.Create(MSG_MISSED_IMAGES_FILES);
    end;
    if (not FileExists(FNombreImagenFondoAnterior)) then begin
        raise Exception.Create(MSG_MISSED_IMAGES_FILES);
    end;

    //Pagada
    if (not FileExists(FNombreImagenPagada)) then begin
        raise Exception.Create(MSG_MISSED_IMAGES_FILES);
    end;
end;

end.
