unit ParametrosProcesoFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ComCtrls, UtilProc, Util, Validate, DmiCtrls, DMConnection;

type
  TfrmParametrosProcesoFacturacion = class(TForm)
    lbl10: TLabel;
    lbl13: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl14: TLabel;
    lbl17: TLabel;
    txtValMinNK: TNumericEdit;
    txtValMinNKPagoAuto: TNumericEdit;
    txtValMinDeuda: TNumericEdit;
    txtValMinNKBaja: TNumericEdit;
    txtMesesForzarNK: TNumericEdit;
    txtDiasMinNK: TNumericEdit;
    btnGuardar: TButton;
    btnCancelar: TButton;
    lbl1: TLabel;
    lbl2: TLabel;
    txtValMinImpAfe: TNumericEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  frmParametrosProcesoFacturacion: TfrmParametrosProcesoFacturacion;

implementation

{$R *.dfm}

function TfrmParametrosProcesoFacturacion.Inicializar: Boolean;
resourcestring
    MSG_ERROR_OBTENER = 'Ha ocurrido un error al obtener los parámetros';
    MSG_CAPTION = 'Parámetros de Facturación';
var
  spObtenerParametrosFacturacion: TADOStoredProc;
begin
    Result := True;
    spObtenerParametrosFacturacion := TADOStoredProc.Create(Self);
    try
        try
            spObtenerParametrosFacturacion.Connection := DMConnections.BaseCAC;
            spObtenerParametrosFacturacion.CommandTimeout := 30;
            spObtenerParametrosFacturacion.ProcedureName := 'ObtenerParametrosFacturacion';
            spObtenerParametrosFacturacion.Parameters.Refresh;

            spObtenerParametrosFacturacion.Open;

            if spObtenerParametrosFacturacion.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerParametrosFacturacion.Parameters.ParamByName('@ErrorDescription').Value);

            if spObtenerParametrosFacturacion.RecordCount > 0 then begin
                txtValMinNK.Value := spObtenerParametrosFacturacion.FieldByName('VALOR_MINIMO_NOTA_COBRO').AsInteger;
                txtValMinNKPagoAuto.Value := spObtenerParametrosFacturacion.FieldByName('VALOR_MINIMO_NK_PAGO_AUTOMATICO').AsInteger;
                txtValMinDeuda.Value := spObtenerParametrosFacturacion.FieldByName('VALOR_MINIMO_DEUDA_CONVENIO').AsInteger;
                txtValMinNKBaja.Value := spObtenerParametrosFacturacion.FieldByName('VALOR_MINIMO_NK_CONVENIO_BAJA').AsInteger;
                txtMesesForzarNK.Value := spObtenerParametrosFacturacion.FieldByName('VALOR_MINIMO_NK_FORZAR_MESES').AsInteger;
                txtDiasMinNK.Value := spObtenerParametrosFacturacion.FieldByName('DIAS_MIN_ENTRE_EMISIONES_FACTURAS').AsInteger;
                txtValMinImpAfe.Value := spObtenerParametrosFacturacion.FieldByName('VALOR_MINIMO_IMPORTE_AFECTO').AsInteger;   // TASK_115_MGO_20170119
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_OBTENER, e.Message, MSG_CAPTION, MB_ICONSTOP);
                Result := False;
            end;
        end;
    finally
        spObtenerParametrosFacturacion.Free;
    end;
end;

procedure TfrmParametrosProcesoFacturacion.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
    Close;
end;

procedure TfrmParametrosProcesoFacturacion.btnGuardarClick(Sender: TObject);
resourcestring
    MSG_ERROR_GUARDAR = 'Ha ocurrido un error al guardar los parámetros';
    MSG_CAPTION = 'Parámetros de Facturación';
    MSG_EXITO = 'Los Parámetros de Facturación se han actualizado exitosamente';
var
  spActualizarParametrosFacturacion: TADOStoredProc;
begin
    spActualizarParametrosFacturacion := TADOStoredProc.Create(Self);
    try
        try
            spActualizarParametrosFacturacion.Connection := DMConnections.BaseCAC;
            spActualizarParametrosFacturacion.CommandTimeout := 30;
            spActualizarParametrosFacturacion.ProcedureName := 'ActualizarParametrosFacturacion';

            spActualizarParametrosFacturacion.Parameters.Refresh;
            spActualizarParametrosFacturacion.Parameters.ParamByName('@DIAS_MIN_ENTRE_EMISIONES_FACTURAS').Value := txtDiasMinNK.ValueInt;
            spActualizarParametrosFacturacion.Parameters.ParamByName('@VALOR_MINIMO_DEUDA_CONVENIO').Value := txtValMinDeuda.ValueInt;
            spActualizarParametrosFacturacion.Parameters.ParamByName('@VALOR_MINIMO_NK_CONVENIO_BAJA').Value := txtValMinNKBaja.ValueInt;
            spActualizarParametrosFacturacion.Parameters.ParamByName('@VALOR_MINIMO_NK_FORZAR_MESES').Value := txtMesesForzarNK.ValueInt;
            spActualizarParametrosFacturacion.Parameters.ParamByName('@VALOR_MINIMO_NK_PAGO_AUTOMATICO').Value := txtValMinNKPagoAuto.ValueInt;
            spActualizarParametrosFacturacion.Parameters.ParamByName('@VALOR_MINIMO_NOTA_COBRO').Value := txtValMinNK.ValueInt;         
            spActualizarParametrosFacturacion.Parameters.ParamByName('@VALOR_MINIMO_IMPORTE_AFECTO').Value := txtValMinImpAfe.ValueInt;     // TASK_115_MGO_20170119

            spActualizarParametrosFacturacion.ExecProc;

            if spActualizarParametrosFacturacion.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spActualizarParametrosFacturacion.Parameters.ParamByName('@ErrorDescription').Value);
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_GUARDAR, e.Message, MSG_CAPTION, MB_ICONSTOP);
                Exit;
            end;
        end;
    finally
        spActualizarParametrosFacturacion.Free;
    end;

    MsgBox(MSG_EXITO, MSG_CAPTION, MB_OK);
    
    ModalResult := mrOk;
end;

procedure TfrmParametrosProcesoFacturacion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
