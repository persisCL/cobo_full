object form_ImprimirNotaCobroMorosos: Tform_ImprimirNotaCobroMorosos
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Imprimir Nota de Cobro Morosos'
  ClientHeight = 433
  ClientWidth = 468
  Color = clBtnFace
  Constraints.MaxHeight = 471
  Constraints.MaxWidth = 484
  Constraints.MinHeight = 461
  Constraints.MinWidth = 474
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlSuperior: TPanel
    Left = 0
    Top = 0
    Width = 468
    Height = 33
    Align = alTop
    TabOrder = 0
    object lblMensaje: TLabel
      Left = 8
      Top = 9
      Width = 297
      Height = 13
      Caption = 'Seleccione la Nota de Cobro por Morosidad que desea imprimir'
    end
  end
  object pnlCentral: TPanel
    Left = 0
    Top = 33
    Width = 468
    Height = 359
    Align = alClient
    Caption = 'pnlCentral'
    TabOrder = 1
    object dblNotasCobroMorosos: TDBListEx
      Left = 1
      Top = 1
      Width = 466
      Height = 357
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          MinWidth = 100
          Header.Caption = 'Tipo Compr.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'TipoComprobante'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          MinWidth = 100
          Header.Caption = 'N'#186' Comprobante'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumeroComprobante'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          MinWidth = 90
          Header.Caption = 'Fecha Emisi'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaEmision'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          MinWidth = 120
          Header.Caption = 'Estado Comprobante'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriEstado'
        end>
      DataSource = dsObtenerNotasCobroPorProcesoFacturacion
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dblNotasCobroMorososDrawText
    end
  end
  object pnlInferior: TPanel
    Left = 0
    Top = 392
    Width = 468
    Height = 41
    Align = alBottom
    Caption = 'pnlInferior'
    TabOrder = 2
    DesignSize = (
      468
      41)
    object btnImprimirComprobante: TButton
      Left = 54
      Top = 8
      Width = 131
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Imprimir Comprobante...'
      Default = True
      TabOrder = 0
      OnClick = btnImprimirComprobanteClick
    end
    object btnImprimirDetalle: TButton
      Left = 190
      Top = 8
      Width = 131
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Imprimir &Detalle...'
      TabOrder = 1
      OnClick = btnImprimirDetalleClick
    end
    object btnCerrar: TButton
      Left = 326
      Top = 8
      Width = 131
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cerrar'
      TabOrder = 2
      OnClick = btnCerrarClick
    end
  end
  object spObtenerNotasCobroPorProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterScroll = spObtenerNotasCobroPorProcesoFacturacionAfterScroll
    ProcedureName = 'ObtenerNotasCobroPorProcesoFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroProceso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 316
    Top = 4
  end
  object dsObtenerNotasCobroPorProcesoFacturacion: TDataSource
    DataSet = spObtenerNotasCobroPorProcesoFacturacion
    Left = 347
    Top = 4
  end
end
