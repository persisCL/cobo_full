object frmGrabarTags: TfrmGrabarTags
  Left = 177
  Top = 153
  Width = 714
  Height = 404
  Caption = 
    'Codificaci'#243'n de Tags [ Lista de Tags Pendientes de Codificaci'#243'n ' +
    ']'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefaultPosOnly
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 329
    Width = 706
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 463
      Top = 0
      Width = 243
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object bt_grabar: TDPSButton
        Left = 82
        Top = 10
        Caption = '&Codificar'
        Default = True
        TabOrder = 0
        OnClick = bt_grabarClick
      end
      object Button1: TDPSButton
        Left = 162
        Top = 10
        Cancel = True
        Caption = '&Salir'
        TabOrder = 1
        OnClick = Button1Click
      end
    end
    object cb_antena: TCheckBox
      Left = 8
      Top = 16
      Width = 163
      Height = 17
      Caption = 'Codificar con antena'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object Grilla: TDPSGrid
    Left = 0
    Top = 0
    Width = 706
    Height = 271
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoCuenta'
        Title.Caption = 'Cuenta'
        Title.Color = 16444382
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NombreCliente'
        Title.Caption = 'Cliente'
        Title.Color = 16444382
        Width = 193
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Patente'
        Title.Color = 16444382
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DescriCategoria'
        Title.Caption = 'Categoria'
        Title.Color = 16444382
        Width = 200
        Visible = True
      end>
  end
  object Panel3: TPanel
    Left = 0
    Top = 271
    Width = 706
    Height = 58
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 18
      Width = 75
      Height = 28
      AutoSize = False
      Caption = 'Modo de codificaci'#243'n:'
      WordWrap = True
    end
    object Label3: TLabel
      Left = 248
      Top = 20
      Width = 63
      Height = 30
      AutoSize = False
      Caption = 'Ubicaci'#243'n del tag:'
      WordWrap = True
    end
    object cb_modo: TComboBox
      Left = 88
      Top = 24
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cb_modoChange
      Items.Strings = (
        'Masiva'
        'Individual')
    end
    object cb_ubicacion: TComboBox
      Left = 320
      Top = 24
      Width = 238
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'Autom'#225'tica'
        'Individual')
    end
  end
  object Qry_SinGrabar: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      '  Cuentas.CodigoCuenta,'
      '  Cuentas.CodigoCliente,'
      '  Cuentas.Patente,'
      '  Vehiculostipos.Categoria,'
      '  RTRIM(Clientes.Apellido) + '#39' '#39' + '
      '      IsNull(RTRIM(Clientes.ApellidoMaterno) + '#39', '#39', '#39#39') + '
      '      RTRIM(Clientes.Nombre) AS NombreCliente,'
      '  Categorias.Descripcion AS DescriCategoria'
      'FROM'
      '  Cuentas,'
      '  Clientes,'
      '  VehiculosModelos,'
      '  VehiculosTipos,'
      '  Categorias,'
      '  TempTagPendientes'#9
      'WHERE'
      '  TempTagPendientes.CodigoCuenta = Cuentas.CodigoCuenta'
      '  AND Cuentas.TipoAdhesion = 1'
      '  AND Clientes.CodigoCliente = Cuentas.CodigoCliente'
      '  AND Cuentas.CodigoMarca = VehiculosModelos.CodigoMarca'
      '  AND Cuentas.CodigoModelo = VehiculosModelos.CodigoModelo'
      
        '  AND VehiculosModelos.TipoVehiculo = VehiculosTipos.CodigoTipoV' +
        'ehiculo'
      '  AND VehiculosTipos.Categoria = Categorias.Categoria')
    Left = 528
    Top = 72
  end
  object DataSource: TDataSource
    DataSet = Qry_SinGrabar
    Left = 528
    Top = 40
  end
  object UpdateMaestro: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'UBICACION'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CUENTA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'TAG'
        Attributes = [paSigned]
        DataType = ftBCD
        Precision = 11
        Size = 19
        Value = Null
      end
      item
        Name = 'CONTEXTMARK'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE'
      '    MAESTROTAGS'
      'SET'
      '    CODIFICADO = 1,'
      '    CODIGOUBICACION  = :UBICACION,'
      '    ULTIMACUENTAASIGNADA =:CUENTA'
      'WHERE'
      '    CONTRACTSERIALNUMBER= :TAG AND CONTEXTMARK = :CONTEXTMARK')
    Left = 528
    Top = 104
  end
  object InsertAsignado: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'ContextMark'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'TAG'
        Attributes = [paSigned]
        DataType = ftBCD
        Precision = 11
        Size = 19
        Value = Null
      end
      item
        Name = 'Vencimiento'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'FechaAlta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'FechaHoraActualizacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'Cuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO'
      '    TAGSASIGNADOS'
      
        '    (Contextmark,ContractserialNumber,Vencimiento,FechaHoraAlta,' +
        'FechaHoraBaja,MotivoBaja,FechaHoraActualizacion,CodigoCuenta)'
      ''
      '    VALUES'
      
        '    (:ContextMark, :TAG, :Vencimiento, :FechaAlta, Null, Null, :' +
        'FechaHoraActualizacion, :Cuenta)')
    Left = 528
    Top = 136
  end
  object EliminarPendiente: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CUENTA'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM TEMPTAGPENDIENTES'
      'WHERE CODIGOCUENTA = :CUENTA')
    Left = 528
    Top = 168
  end
  object RegistrarCambioUbicacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarCambioUbicacionTag;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 11
        Value = Null
      end
      item
        Name = '@CodigoUbicacionInicial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUbicacionFinal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraPactada'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraLlegada'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoCuentaAsignada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 528
    Top = 200
  end
  object ActualizarActionListEnPorceso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarActionListEnPorceso'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@ClearBlackList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetBlackList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearGreenList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetGreenList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearGrayList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetGrayList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearYellowList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetYellowList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ExceptionHandling'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@MMINotOk'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@MMIContactOperator'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 528
    Top = 232
  end
end
