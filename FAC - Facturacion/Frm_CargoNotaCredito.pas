{-----------------------------------------------------------------------------
 File Name: Frm_CargoNotaCredito.pas
 Author:    Flamas
 Date Created: 24/01/2005
 Language: ES-AR
 Description: Agrega un item a ser facturado

 Revision: 1
 Author:    jconcheyro
 Date Created: 22/11/2006
 Language: ES-AR
 Description: en el editor de Fecha siempre se carga el parametro recibido, se
 muestre o no

 Revision: 2
 Author:    jconcheyro
 Date Created: 12/12/2006
 Language: ES-AR
 Description: Tuve que forzar el change del edImporte en el change del concepto
 porque si el precio no cambia, no salta el evento.
 

 Revision 3
 Author: mbecerra
 Date: 21-Abril-2009
 Description:		(Ref. Facturaci�n Electr�nica)
 
                Se realizan los siguientes cambios:

    	1.-		Se agregan los comprobantes Nota de Cr�dito y Nota de D�bito
                a la combobox.

        2.-     Se impide ingresar un monto negativo. Si el Concepto asociado
            	al movimiento es "Descuento" se agrega el signo menos
                De lo contrario el signo positivo.

        3.-		Los Conceptos se filtran por el tipo de documento, de acuerdo al
                tipo coincidente en la tabla ConceptosMovimientos


        08-Mayo-2009
        4.-		Los conceptos operan ahora de acuerdo al signo del comprobante.
            	Por ejemplo: Un descuento es negativo en una NK, pero positivo en una NC

 Revision 4
 Author: Nelson Droguett Sierra
 Date: 21-Julio-2009
 Description:		(Ref. Facturaci�n Electr�nica)
                Al elegir del combo de conceptos, el calculo del IVA se hace desde
                una variable, cuando se hacia desde el TextBox, como el textBox no
                tenia decimales, hacia el calculo solo con numeros redondeados, lo
                que daba diferencias de 1 peso. Al hacerlo en variable, hace bien el
                redondeo y en los controles de texto se muestra sin decimales. En la
                tabla movimientoscuenta, el valor del importe y el valor del IVA se
                revisaron y estan grabados correctamente.

Firma       : SS_1012_ALA_20111123
Description : Se arregla validaci�n cuando el concepto contiene IVA, para que valide
              el total del concepto (incluido el IVA).


Firma		: SS_1144_MCA_20131118
Description : Se eliminan los conceptos de anulacion asociados a Peaje, Estacionamiento e Infractores.

Etiqueta    :   TASK_050_MGO_20160720
Descripci�n :   Se agrega c�digo a la descripci�n del concepto en el combo de conceptos
-----------------------------------------------------------------------------}
unit Frm_CargoNotaCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, VariantComboBox, DmiCtrls, DB, ADODB,
  PeaProcs, Util, ExtCtrls, UtilDB,PeaTypes, TimeEdit, ABMConceptosMovimiento,
  DMConnection;

type
  TfrmCargoNotaCredito = class(TForm)
    lblConcepto: TLabel;
    cbConcepto: TVariantComboBox;
    btnCancelar: TButton;
    btnAceptar: TButton;
    lblObservaciones: TLabel;
    spObtenerConceptosComprobante: TADOStoredProc;
    lblImporte: TLabel;
    edImporte: TNumericEdit;
    edDetalle: TEdit;
    Bevel1: TBevel;
    lblImporteIVA: TLabel;
    edImporteIVA: TNumericEdit;
    edPorcentajeIVA: TNumericEdit;
    lblPorcentajeIVA: TLabel;
    cbTipoComprobante: TVariantComboBox;
    lblTipoComprobante: TLabel;
    lblFecha: TLabel;
    edFecha: TDateEdit;
    btnABMConceptos: TButton;
    edTotal: TNumericEdit;
    lblTotal: TLabel;
    lblSignoNeto: TLabel;
    lblSignoIVA: TLabel;
    lblSignoTotal: TLabel;
    procedure cbTipoComprobanteChange(Sender: TObject);
    procedure btnABMConceptosClick(Sender: TObject);
    procedure edImporteChange(Sender: TObject);
    procedure cbConceptChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnAceptarClick(Sender: TObject);
    procedure edImporteKeyPress(Sender: TObject; var Key: Char);
  private
	{ Private declarations }
	FImporteOriginal: double;
    FTipoComprobante: string;
    FListaEsDescuentoRecargo,
    FListaCodigosConceptosAfectosIVA: TStringList;
    FPorcentajeIVA: double;
    FNoSePuedeAcreditarIVA :boolean;
    FConceptosDeAnulacion: TStringList;	// SS_1144_MCA_20131118
	procedure CargarComboConceptos(TipoComprobante:string);
    procedure HabilitarControlesIVA;
    procedure CargarComboTiposComprobantes(TipoComprobante: string);
  public
	{ Public declarations }
    FSignoMovimiento : integer;			{+1 (positivo) � -1 (negativo)}
    
	function Inicializar (  aFechaEnabled: boolean;
                            aComboTipoComprobanteEnabled:boolean;
                            aABMConceptosVisible:boolean;
                            TipoComprobante:string ;
                            aPorcentajeIVA:double;
                            aFecha: TDateTime;
                            aNoSePuedeAcreditarIVA:boolean): Boolean; overload;
    //se usa para el alta de un concepto nuevo

	function Inicializar( aFechaEnabled: boolean;
                          aComboTipoComprobanteEnabled:boolean;
                          aABMConceptosVisible:boolean;
                          aComboConceptosEnabled:boolean;
                          aCodigoConcepto:integer;
                          aImporteCtvs,
                          aImporteOriginal:double;
                          aObservaciones,
                          aDescripcion,
                          TipoComprobante:string;
                          aPorcentajeIVA:double;
                          aFecha: TDateTime;
                          aNoSePuedeAcreditarIVA:boolean ): boolean ;overload;

  	function Inicializar(aFechaEnabled: boolean;												//SS_1144_MCA_20131118
                            aComboTipoComprobanteEnabled:boolean;								//SS_1144_MCA_20131118
                            aABMConceptosVisible:boolean;										//SS_1144_MCA_20131118
                            TipoComprobante:string ;											//SS_1144_MCA_20131118
                            aPorcentajeIVA:double; aFecha: TDateTime;							//SS_1144_MCA_20131118
                            aNoSePuedeAcreditarIVA:boolean;										//SS_1144_MCA_20131118
                            FListaCodigosConceptosAnulacion: TStringList ): Boolean;overload;	//SS_1144_MCA_20131118

    //se usa para modificar un concepto existente

    procedure ColocarSigno;
  end;

var
  frmCargoNotaCredito: TfrmCargoNotaCredito;

implementation

{$R *.dfm}

//REV.3  Descripcion de Descuento o Recargo
const
    EsDescuento	= '1';
    EsRecargo	= '2';
    EsNormal	= '0';

{-----------------------------------------------------------------------------
            	ColocarSigno
  Author: mbecerra
  Date: 08-Mayo-2009
  Description:	(Ref. Facturaci�n Electr�nica)
                Despliega el signo del concepto, de acuerdo a la combinaci�n
                del signo del comprobante y si es descuento o no
-----------------------------------------------------------------------------}
procedure TfrmCargoNotaCredito.ColocarSigno;
begin
	lblSignoNeto.Caption := '';
    lblSignoIVA.Caption := '';
    lblSignoTotal.Caption := '';
    if FSignoMovimiento < 0 then begin           {Colocar un valor negativo}
    	lblSignoNeto.Caption := '-';
        if edImporteIVA.Visible then lblSignoIVA.Caption := '-';
    	if edTotal.Visible then lblSignoTotal.Caption := '-';
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 24/01/2005
  Description: Inicializa el Form - Carga los Conceptos de Facturaci�n
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmCargoNotaCredito.Inicializar(aFechaEnabled: boolean;
                            aComboTipoComprobanteEnabled:boolean;
                            aABMConceptosVisible:boolean;
                            TipoComprobante:string ;
                            aPorcentajeIVA:double; aFecha: TDateTime;
                            aNoSePuedeAcreditarIVA:boolean ): Boolean;
begin
    //se podria haber inferido que son cargos con IVA si el porcentaje es mayor a cero, pero uno
    //nunca sabe lo que puede pasar.

    edFecha.Date := aFecha;
    edFecha.Visible  := aFechaEnabled ;
    lblFecha.Visible := aFechaEnabled ;

    FSignoMovimiento := 1;
    ColocarSigno();

    cbTipoComprobante.Visible := aComboTipoComprobanteEnabled;
    lblTipoComprobante.Visible := aComboTipoComprobanteEnabled;
    if cbTipoComprobante.Visible then CargarComboTiposComprobantes(TipoComprobante);
    if edfecha.Visible and (not lblTipoComprobante.Visible) then begin //si se ve la fecha pero no el combo de tipo Comprobante corro la fecha
        lblFecha.Left := 33;
        edFecha.Left := 114;
    end;
    btnABMConceptos.Visible := aABMConceptosVisible;

	result := True;
	FImporteOriginal:= 0 ; //el importe original del cargo que se pasa al credito.
    if TipoComprobante = '' then FTipoComprobante := cbTipoComprobante.Value
    else FTipoComprobante := TipoComprobante;
    FListaCodigosConceptosAfectosIVA	:= TStringList.Create;
    FListaEsDescuentoRecargo			:= TStringList.Create;
    FListaCodigosConceptosAfectosIVA.Sorted := True;
    FPorcentajeIVA := aPorcentajeIVA; //19
    FNoSePuedeAcreditarIVA := aNoSePuedeAcreditarIVA;
    //si FNoSePuedeAcreditarIVA es true entonces entendemos que estamos haciendo un credito en
    //el cual ya no se puede incluir IVA, por ejemplo en el caso de boletas acreditadas mas alla de X dias
    // X dias viene dado por un parametro general.
	CargarComboConceptos(FTipoComprobante);
    HabilitarControlesIVA;
end;

function TfrmCargoNotaCredito.Inicializar(aFechaEnabled: boolean;
                            aComboTipoComprobanteEnabled:boolean;
                            aABMConceptosVisible:boolean;
                            TipoComprobante:string ;
                            aPorcentajeIVA:double; aFecha: TDateTime;
                            aNoSePuedeAcreditarIVA:boolean;
                            FListaCodigosConceptosAnulacion: TStringList ): Boolean;		//SS_1144_MCA_20131118
begin
    //se podria haber inferido que son cargos con IVA si el porcentaje es mayor a cero, pero uno
    //nunca sabe lo que puede pasar.

    FConceptosDeAnulacion := TStringList.Create();                              // SS_1144_MCA_20131118
    FConceptosDeAnulacion.Assign(FListaCodigosConceptosAnulacion);                   // SS_1144_MCA_20131118
    edFecha.Date := aFecha;
    edFecha.Visible  := aFechaEnabled ;
    lblFecha.Visible := aFechaEnabled ;

    FSignoMovimiento := 1;
    ColocarSigno();

    cbTipoComprobante.Visible := aComboTipoComprobanteEnabled;
    lblTipoComprobante.Visible := aComboTipoComprobanteEnabled;
    if cbTipoComprobante.Visible then CargarComboTiposComprobantes(TipoComprobante);
    if edfecha.Visible and (not lblTipoComprobante.Visible) then begin //si se ve la fecha pero no el combo de tipo Comprobante corro la fecha
        lblFecha.Left := 33;
        edFecha.Left := 114;
    end;
    btnABMConceptos.Visible := aABMConceptosVisible;

	result := True;
	FImporteOriginal:= 0 ; //el importe original del cargo que se pasa al credito.
    if TipoComprobante = '' then FTipoComprobante := cbTipoComprobante.Value
    else FTipoComprobante := TipoComprobante;
    FListaCodigosConceptosAfectosIVA	:= TStringList.Create;
    FListaEsDescuentoRecargo			:= TStringList.Create;
    FListaCodigosConceptosAfectosIVA.Sorted := True;
    FPorcentajeIVA := aPorcentajeIVA; //19
    FNoSePuedeAcreditarIVA := aNoSePuedeAcreditarIVA;
    //si FNoSePuedeAcreditarIVA es true entonces entendemos que estamos haciendo un credito en
    //el cual ya no se puede incluir IVA, por ejemplo en el caso de boletas acreditadas mas alla de X dias
    // X dias viene dado por un parametro general.
	CargarComboConceptos(FTipoComprobante);
    HabilitarControlesIVA;
end;

function TfrmCargoNotaCredito.Inicializar(aFechaEnabled: boolean;
                          aComboTipoComprobanteEnabled:boolean;
                          aABMConceptosVisible:boolean;
                          aComboConceptosEnabled:boolean;
                          aCodigoConcepto:integer;
                          aImporteCtvs,
                          aImporteOriginal:double;
                          aObservaciones,
                          aDescripcion,
                          TipoComprobante:string;
                          aPorcentajeIVA:double;
                          aFecha: TDateTime;
                          aNoSePuedeAcreditarIVA: boolean ): boolean;
var
  i:integer;
begin
	//este form se usa para modificar lo que se va a incluir en una nota de credito.
	//Puede ser para dar el alta de un nuevo concepto, en ese caso el combo de conceptos est� habilitado
	//Modificar un concepto que se agreg� a la nota de cr�dito, ac� tambi�n el combo est� habilitado
	//pero si el concepto a editar surji� como resultado de revertir uno desde la NK original, el c�digo de concepto no es modificable

	result := True;

    edFecha.Date := aFecha;
    edFecha.Visible  := aFechaEnabled ;
    lblFecha.Visible := aFechaEnabled ;

    lblSignoNeto.Caption := '';
    lblSignoIVA.Caption := '';
    lblSignoTotal.Caption := '';


    cbTipoComprobante.Visible := aComboTipoComprobanteEnabled;
    lblTipoComprobante.Visible := aComboTipoComprobanteEnabled;
    if cbTipoComprobante.Visible then CargarComboTiposComprobantes(TipoComprobante);
    if edfecha.Visible and (not lblTipoComprobante.Visible) then begin //si se ve la fecha pero no el combo de tipo Comprobante corro la fecha
        lblFecha.Left := 33;
        edFecha.Left := 114;
    end;

    btnABMConceptos.Visible := aABMConceptosVisible;
    FTipoComprobante := TipoComprobante;
    FListaCodigosConceptosAfectosIVA	:= TStringList.Create;
    FListaEsDescuentoRecargo			:= TStringList.Create;
    FListaCodigosConceptosAfectosIVA.Sorted := True;
    FPorcentajeIVA := aPorcentajeIVA;
    FNoSePuedeAcreditarIVA := aNoSePuedeAcreditarIVA;
    //si FNoSePuedeAcreditarIVA es true entonces entendemos que estamos haciendo un credito en
    //el cual ya no se puede incluir IVA, por ejemplo en el caso de boletas acreditadas mas alla de X dias
    // X dias viene dado por un parametro general.
    if TipoComprobante = '' then FTipoComprobante := cbTipoComprobante.Value
    else FTipoComprobante := TipoComprobante;

	cbConcepto.Enabled := aComboConceptosEnabled;
    CargarComboConceptos(FTipoComprobante);
    for i:= 0 to cbConcepto.Items.Count -1  do
        if cbConcepto.Items[i].Value = aCodigoConcepto then cbConcepto.ItemIndex := i;

	FImporteOriginal:= aImporteOriginal  ; //es el importe original del cargo que se pasa al credito.
	//el cargo que lo anule no puede ser de un importe mayor al original.
    HabilitarControlesIVA;
	edDetalle.Text 					:= aObservaciones;
    if (edPorcentajeIVA.visible) {and (not FNoSePuedeAcreditarIVA)} then begin // se puede acreditar el IVA
    	edImporte.Value := aImporteCtvs / ( 1 + FPorcentajeIVA / 100 ) ;  //necesito el neto grabado
    end
    else edImporte.Value := aImporteCtvs ; //si no se puede acreditar iva, ahora ac� est� llegando directamente el neto gravado.

    //REV.3
    if edImporte.Value < 0 then FSignoMovimiento := -1
    else FSignoMovimiento := 1;

    ColocarSigno();
    edImporte.Value := FSignoMovimiento * edImporte.Value;
    edImporteChange(Self);
end;


{-----------------------------------------------------------------------------
  Function Name: CargarComboConceptos
  Author:    flamas
  Date Created: 24/01/2005
  Description: Carga los conceptos de facturaci�n
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TfrmCargoNotaCredito.CargarComboConceptos(TipoComprobante:string);
begin
    FListaCodigosConceptosAfectosIVA.Clear;
    FListaEsDescuentoRecargo.Clear;
    cbConcepto.Clear;			//REV.3
	with spObtenerConceptosComprobante do begin
        Parameters.Refresh;			// TASK_050_MGO_20160720
		Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
		Open;
		while not EOF do begin

        	if FConceptosDeAnulacion <> nil then begin                                                                  
                if FConceptosDeAnulacion.IndexOf(FieldByName('CodigoConcepto').AsString) < 0 then begin     			
                    { INICIO : TASK_050_MGO_20160720
                    cbConcepto.Items.Add( Trim( FieldByName( 'Descripcion' ).AsString ),
                                        FieldByName( 'CodigoConcepto' ).AsString );
                    }
                    cbConcepto.Items.Add(Trim(FieldByName('DescripcionCombo').AsString),
                                        FieldByName('CodigoConcepto').AsString);
                    // FIN : TASK_050_MGO_20160720

                    FListaEsDescuentoRecargo.Add(FieldByName('EsDescuentoRecargo').AsString);

                    //como el IVA depende exclusivamente del concepto, en la lista llevo los codigos de conceptos
                    //afectos al IVA
                    if (FieldByName( 'AfectoIVA' ).AsBoolean ) //and (FNoSePuedeAcreditarIVA = False)                   
                    then
                        //si FNoSePuedeAcreditarIVA es true entonces no podemos calcular iva en los conceptos que vayamos a agregar.
                        //en ese caso no llenamos la lista de cargos con iva
                        FListaCodigosConceptosAfectosIVA.Add(FieldByName( 'CodigoConcepto' ).AsString);                 

                end;
            end                                                                                                         
            else begin
                 	{ INICIO : TASK_050_MGO_20160720
                    cbConcepto.Items.Add( Trim( FieldByName( 'Descripcion' ).AsString ),
                                        FieldByName( 'CodigoConcepto' ).AsString );
                    }
                    cbConcepto.Items.Add(Trim(FieldByName('DescripcionCombo').AsString),
                                        FieldByName('CodigoConcepto').AsString);
                    // FIN : TASK_050_MGO_20160720
                    
                    FListaEsDescuentoRecargo.Add(FieldByName('EsDescuentoRecargo').AsString);

                    //como el IVA depende exclusivamente del concepto, en la lista llevo los codigos de conceptos
                //afectos al IVA
                    if (FieldByName( 'AfectoIVA' ).AsBoolean ) //and (FNoSePuedeAcreditarIVA = False)
                    then
                        //si FNoSePuedeAcreditarIVA es true entonces no podemos calcular iva en los conceptos que vayamos a agregar.
                        //en ese caso no llenamos la lista de cargos con iva
                    FListaCodigosConceptosAfectosIVA.Add(FieldByName( 'CodigoConcepto' ).AsString);

            end;
            Next;
		end;
		Close;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    flamas
  Date Created: 24/01/2005
  Description: Valida los valores de los campos para poder aceptar
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TfrmCargoNotaCredito.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_CONCEPT_MUST_HAVE_A_VALUE 	= 'Debe especificar un concepto';
	MSG_AMMOUNT_MUST_HAVE_A_VALUE 	= 'Debe especificar un monto';
	ERROR_IMPORTE_MENOROIGUAL_ORIGINAL = 'el importe del cargo en el cr�dito no puede superar al del cargo original %0f';
var
  index : integer;
begin
	if (FImporteOriginal > 0) then begin
        if(edTotal.Visible) then begin                                                                                         //SS_1012_ALA_20111123
            if ValidateControls(                                                                                               //SS_1012_ALA_20111123
			    [edTotal],                                                                                                     //SS_1012_ALA_20111123
    			[edTotal.Value <= FImporteOriginal ],                                                                          //SS_1012_ALA_20111123
    			caption,                                                                                                       //SS_1012_ALA_20111123
	    		[Format(ERROR_IMPORTE_MENOROIGUAL_ORIGINAL,[FImporteOriginal])]) then                                          //SS_1012_ALA_20111123
	       	ModalResult := mrOK;                                                                                               //SS_1012_ALA_20111123
        end                                                                                                                    //SS_1012_ALA_20111123
        else begin                                                                                                             //SS_1012_ALA_20111123
            if ValidateControls(
			    [edImporte],
    			[edImporte.Value <= FImporteOriginal ],
    			caption,
	    		[Format(ERROR_IMPORTE_MENOROIGUAL_ORIGINAL,[FImporteOriginal])]) then
	       	ModalResult := mrOK;
        end;                                                                                                                   //SS_1012_ALA_20111123

	end else if ValidateControls([cbConcepto, edImporte],
								[cbConcepto.ItemIndex >= 0, edImporte.ValueInt > 0  ],
								caption,
								[MSG_CONCEPT_MUST_HAVE_A_VALUE, MSG_AMMOUNT_MUST_HAVE_A_VALUE]) then
		ModalResult := mrOK;

    if not FListaCodigosConceptosAfectosIVA.Find(cbConcepto.Value, index) then
        edImporteIVA.Value := 0; //lo limpio ac� porque es mejor que limpiarlo en el onchange del importe

end;


procedure TfrmCargoNotaCredito.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    FreeAndNil(FListaCodigosConceptosAfectosIVA);
    FreeAndNil(FConceptosDeAnulacion);                                           //SS_1144_MCA_20131118
	Action := caFree;
end;

procedure TfrmCargoNotaCredito.btnCancelarClick(Sender: TObject);
begin
	Close;
end;


procedure TfrmCargoNotaCredito.HabilitarControlesIVA;
var
  index : integer;
begin
    if FListaCodigosConceptosAfectosIVA.Find(cbConcepto.Value, index) then begin
        lblImporteIVA.Visible     := True;
        lblPorcentajeIVA.Visible  := True;
        lblTotal.Visible          := True;
        edTotal.Visible           := True;
        edImporteIVA.Visible      := True;
        edPorcentajeIVA.Visible   := True;
        edImporteIVA.Value        := 0;
        edPorcentajeIVA.Value     := FPorcentajeIVA  ;
    end else begin
        lblImporteIVA.Visible     := False;
        lblPorcentajeIVA.Visible  := False;
        lblTotal.Visible          := False;
        edTotal.Visible           := False;
        edImporteIVA.Visible      := False;
        edPorcentajeIVA.Visible   := False;
        edImporteIVA.Value        := 0;
        edPorcentajeIVA.Value     := 0;
    end;
end;

procedure TfrmCargoNotaCredito.cbConceptChange(Sender: TObject);
//REV.3
resourcestring
	MSG_SQL = 'SELECT dbo.ObtenerPrecioConceptoMovimiento(''%s'')';

var
	EsDescuentoRecargo : string;
    SignoConcepto, SignoComprobante : integer;
    // Rev.4 / 21-07-2009 / Nelson Droguett Sierra
    dImporte,dIva:Double;

begin
	edDetalle.Text := '';
    HabilitarControlesIVA;
    // Rev.4 / 21-07-2009 / Nelson Droguett Sierra -----------------------------
    dImporte := QueryGetValueInt(DMConnections.BaseCAC,
                format(MSG_SQL,[cbConcepto.Value])) / 100;
    edImporte.Value :=dImporte;
    dIva := dImporte * FPorcentajeIVA / 100 ;
    edImporteIva.Value:=dIva;
    edTotal.Value := dImporte+dIva;
    //--------------------------------------------------------------------------


    //REV.3  Determinar el signo del movimiento
    EsDescuentoRecargo := FListaEsDescuentoRecargo[cbConcepto.ItemIndex];
    if Trim(EsDescuentoRecargo) = '' then EsDescuentoRecargo := '0';

    if EsDescuentoRecargo = EsDescuento then SignoConcepto := -1
    else SignoConcepto := 1;

    //Determinar el signo del comprobante
    if cbConcepto.Value = TC_NOTA_CREDITO then SignoComprobante := -1
    else SignoComprobante := 1;

    FSignoMovimiento := SignoConcepto * SignoComprobante;
    ColocarSigno();

    //FIN REV.3
    //edImporteChange(Self);
end;

procedure TfrmCargoNotaCredito.edImporteChange(Sender: TObject);
begin
    edImporteIVA.Value := edImporte.Value * FPorcentajeIVA / 100 ;
    edTotal.Value := edImporte.Value + edImporteIVA.Value ;
end;

procedure TfrmCargoNotaCredito.edImporteKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key = '-' then Key := #0;		{no puede ingresar el signo}

end;

procedure TfrmCargoNotaCredito.CargarComboTiposComprobantes(TipoComprobante: string);
var
    index: integer;
begin
	cbTipoComprobante.Clear;		// REV.3

    //por ahora solo se usa en la pantalla de ingreso de Movimientos cuenta, y el �nico
    //tipo de comprobante solicitado hasta ahora es la NK
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_COBRO),TC_NOTA_COBRO);

    //REV.3
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_CREDITO), TC_NOTA_CREDITO);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_DEBITO), TC_NOTA_DEBITO);
    
    //en el futuro alguien no necesitar� volver a escribir esto, que ahora de mucho no sirve
    for index := 0 to cbTipoComprobante.Items.Count -1 do
        if (cbTipoComprobante.Items[index].Value = TipoComprobante) then begin
            cbTipoComprobante.ItemIndex := index;
            Exit;
        end;

    if cbTipoComprobante.ItemIndex = -1 then cbTipoComprobante.ItemIndex := 0 ;
end;

procedure TfrmCargoNotaCredito.btnABMConceptosClick(Sender: TObject);
var
	f : TFormConceptosMovimientos;
begin
    Application.CreateForm(TFormConceptosMovimientos, f);
    if f.Inicializa(cbTipoComprobante.Value) then begin
        f.Visible := False;
        f.ShowModal ;
    end;
    f.Release;
    CargarComboConceptos(cbTipoComprobante.Value);
end;

procedure TfrmCargoNotaCredito.cbTipoComprobanteChange(Sender: TObject);
begin
  CargarComboConceptos(cbTipoComprobante.Value);
end;

end.
