object FormResultadoFacturacion: TFormResultadoFacturacion
  Left = 62
  Top = 76
  Anchors = []
  Caption = 'Resultado de Facturaci'#243'n'
  ClientHeight = 730
  ClientWidth = 1154
  Color = clBtnFace
  Constraints.MinHeight = 540
  Constraints.MinWidth = 1125
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlListado: TPanel
    Left = 0
    Top = 241
    Width = 1154
    Height = 489
    Align = alClient
    TabOrder = 0
    ExplicitTop = 230
    ExplicitHeight = 500
    object spl1: TSplitter
      Left = 1
      Top = 337
      Width = 1152
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      Beveled = True
      Color = clBtnFace
      ParentColor = False
      ExplicitTop = 307
      ExplicitWidth = 1215
    end
    object lstComprobantes: TDBListEx
      Left = 1
      Top = 17
      Width = 1152
      Height = 320
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'DescriEstado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Comprobante'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'DescriComprobante'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 130
          Header.Caption = 'Comprobante Fiscal'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'ComprobanteFiscal'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'RUT'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'NumeroConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Emisi'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'FechaEmision'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Vencimiento'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'FechaVencimiento'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Width = 120
          Header.Caption = 'Total a Pagar'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsImporteHeaderClick
          FieldName = 'DescriAPagar'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Total Comprobante'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsImporteMesHeaderClick
          FieldName = 'DescriTotal'
        end>
      DataSource = dsFacturas
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      ExplicitHeight = 331
    end
    object pnlAcciones: TPanel
      Left = 1
      Top = 1
      Width = 1152
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object lbl4: TLabel
        Left = 12
        Top = 1
        Width = 130
        Height = 13
        Caption = 'Lista de Comprobantes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblResultado: TLabel
        Left = 148
        Top = 1
        Width = 48
        Height = 13
        Caption = 'Resultado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
    end
    object pnlDetalle: TPanel
      Left = 1
      Top = 342
      Width = 1152
      Height = 146
      Align = alBottom
      TabOrder = 2
      ExplicitTop = 353
      DesignSize = (
        1152
        146)
      object pgcDetalles: TPageControl
        Left = 0
        Top = -1
        Width = 1150
        Height = 146
        ActivePage = tsDetalleComprobante
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object tsDetalleComprobante: TTabSheet
          Caption = '&Detalle Comprobante'
          object lstDetalle: TDBListEx
            Left = 0
            Top = 0
            Width = 1142
            Height = 118
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 250
                Header.Caption = 'Concepto del Cargo'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Descripcion'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 90
                Header.Caption = 'Monto'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'Importe'
              end>
            DataSource = dsDetalle
            DragReorder = True
            ParentColor = False
            TabOrder = 0
            TabStop = True
            OnDrawText = lstDetalleDrawText
          end
        end
        object tsTransitos: TTabSheet
          Caption = '&Tr'#225'nsitos'
          ImageIndex = 1
          object lstTransitos: TDBListEx
            Left = 0
            Top = 27
            Width = 1142
            Height = 91
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 120
                Header.Caption = 'Fecha / Hora'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'FechaHora'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 350
                Header.Caption = 'Punto de Cobro'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DescriPtoCobro'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Patente'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Patente'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Categor'#237'a'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DescriCategoria'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 100
                Header.Caption = 'TAG'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Etiqueta'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 100
                Header.Caption = 'Tarifa'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'TipoHorarioDesc'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Width = 100
                Header.Caption = 'Monto'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DescriImporte'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = [fsUnderline]
                Width = 60
                Header.Caption = 'Imagen'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = True
                FieldName = 'Imagen'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Anulado'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'TransitoAnulado'
              end>
            DataSource = dsTransitosPorComprobante
            DragReorder = True
            ParentColor = False
            TabOrder = 0
            TabStop = True
            OnCheckLink = lstTransitosCheckLink
            OnDrawText = lstTransitosDrawText
            OnLinkClick = lstTransitosLinkClick
          end
          object pnl1: TPanel
            Left = 0
            Top = 0
            Width = 1142
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            DesignSize = (
              1142
              27)
            object lbl_TransitosDesde: TLabel
              Left = 9
              Top = 6
              Width = 34
              Height = 13
              Caption = 'Desde:'
            end
            object lbl_TransitosHasta: TLabel
              Left = 145
              Top = 6
              Width = 31
              Height = 13
              Caption = 'Hasta:'
            end
            object lbl_Patente: TLabel
              Left = 377
              Top = 6
              Width = 40
              Height = 13
              Caption = 'Patente:'
            end
            object lblTransitosFechasRestablecer: TLabel
              Left = 278
              Top = 6
              Width = 93
              Height = 13
              Cursor = crHandPoint
              Hint = 'Haga Click ac'#225' para reestablecer las fechas por defecto'
              Caption = 'Fechas por defecto'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsUnderline]
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              OnClick = lblTransitosFechasRestablecerClick
            end
            object lblTransitosCSV: TLabel
              Left = 983
              Top = 6
              Width = 63
              Height = 13
              Cursor = crHandPoint
              Hint = 'Exportar esta consulta a formato CSV'
              Anchors = [akTop, akRight]
              Caption = 'Exportar CSV'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHotLight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              OnClick = lblTransitosCSVClick
              ExplicitLeft = 1046
            end
            object btnTransitosFiltrar: TButton
              Left = 1063
              Top = 1
              Width = 75
              Height = 24
              Anchors = [akTop, akRight]
              Caption = 'Filtrar'
              Default = True
              TabOrder = 3
              OnClick = btnTransitosFiltrarClick
            end
            object date_TransitosDesde: TDateEdit
              Left = 49
              Top = 3
              Width = 90
              Height = 21
              AutoSelect = False
              TabOrder = 0
              Date = -693594.000000000000000000
            end
            object date_TransitosHasta: TDateEdit
              Left = 182
              Top = 3
              Width = 90
              Height = 21
              AutoSelect = False
              TabOrder = 1
              Date = -693594.000000000000000000
            end
            object edtTransitosPatente: TEdit
              Left = 423
              Top = 3
              Width = 81
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 10
              TabOrder = 2
            end
          end
        end
      end
      object chkMostrarDetalle: TCheckBox
        Left = 183
        Top = 1
        Width = 308
        Height = 17
        Caption = 'Mostrar detalle de comprobantes (relentiza la navegaci'#243'n)'
        TabOrder = 1
        OnClick = chkMostrarDetalleClick
      end
    end
  end
  object clpnlProceso: TCollapsablePanel
    Left = 0
    Top = 0
    Width = 1154
    Height = 241
    Align = alTop
    Animated = False
    Direction = cpdDown
    Caption = 'Proceso de Facturaci'#243'n N'#176' %d'
    BarColorStart = 14071199
    BarColorEnd = 10646097
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    InternalSize = 218
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    DesignSize = (
      1154
      241)
    object gbCliente: TGroupBox
      Left = 2
      Top = 114
      Width = 551
      Height = 101
      Caption = ' Por Cliente / Cuenta '
      TabOrder = 1
      DesignSize = (
        551
        101)
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 78
        Height = 13
        Caption = 'N'#250'mero de RUT'
      end
      object Label3: TLabel
        Left = 209
        Top = 17
        Width = 45
        Height = 13
        Caption = 'Convenio'
      end
      object lblverConvenio: TLabel
        Left = 427
        Top = 35
        Width = 73
        Height = 13
        Cursor = crHandPoint
        Caption = 'Ver Convenio...'
        Color = clBtnFace
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentColor = False
        ParentFont = False
        Transparent = True
        OnClick = lblverConvenioClick
      end
      object lblDomicilio: TLabel
        Left = 71
        Top = 78
        Width = 74
        Height = 13
        Caption = 'DomicilioCliente'
      end
      object lbl3: TLabel
        Left = 9
        Top = 78
        Width = 56
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblConcesionaria: TLabel
        Left = 474
        Top = 10
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Concesionaria'
        ExplicitLeft = 522
      end
      object lblConvenio: TLabel
        Left = 72
        Top = 59
        Width = 100
        Height = 13
        Caption = 'N'#250'mero de Convenio'
      end
      object lbl5: TLabel
        Left = 8
        Top = 59
        Width = 58
        Height = 13
        Caption = 'Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl6: TLabel
        Left = 208
        Top = 59
        Width = 48
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNombre: TLabel
        Left = 273
        Top = 59
        Width = 72
        Height = 13
        Caption = 'Apellido Cliente'
      end
      object peNumeroDocumento: TPickEdit
        Left = 8
        Top = 32
        Width = 192
        Height = 21
        CharCase = ecUpperCase
        Enabled = True
        TabOrder = 0
        OnChange = peNumeroDocumentoChange
        EditorStyle = bteTextEdit
        OnButtonClick = peNumeroDocumentoButtonClick
      end
      object cbConvenios: TVariantComboBox
        Left = 208
        Top = 32
        Width = 203
        Height = 19
        Style = vcsOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbConveniosChange
        OnDrawItem = cbConveniosDrawItem
        Items = <>
      end
    end
    object grpProceso: TGroupBox
      Left = 1
      Top = 3
      Width = 552
      Height = 111
      Caption = 'Proceso de Facturaci'#243'n'
      TabOrder = 2
      object lbl7: TLabel
        Left = 8
        Top = 17
        Width = 116
        Height = 13
        Caption = 'N'#250'mero de Proceso:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNumeroProcesoFacturacion: TLabel
        Left = 130
        Top = 17
        Width = 142
        Height = 13
        Caption = 'lblNumeroProcesoFacturacion'
      end
      object lbl8: TLabel
        Left = 8
        Top = 36
        Width = 36
        Height = 13
        Caption = 'Inicio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl9: TLabel
        Left = 167
        Top = 36
        Width = 22
        Height = 13
        Caption = 'Fin:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblFechaHoraInicio: TLabel
        Left = 50
        Top = 36
        Width = 88
        Height = 13
        Caption = 'lblFechaHoraInicio'
      end
      object lblFechaHoraFin: TLabel
        Left = 195
        Top = 36
        Width = 77
        Height = 13
        Caption = 'lblFechaHoraFin'
      end
      object lbl18: TLabel
        Left = 326
        Top = 55
        Width = 57
        Height = 13
        Caption = 'Operador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblOperador: TLabel
        Left = 430
        Top = 55
        Width = 54
        Height = 13
        Caption = 'lblOperador'
      end
      object lbl27: TLabel
        Left = 326
        Top = 17
        Width = 85
        Height = 13
        Caption = 'Comprobantes:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComprobantes: TLabel
        Left = 430
        Top = 17
        Width = 78
        Height = 13
        Caption = 'lblComprobantes'
      end
      object lbl28: TLabel
        Left = 8
        Top = 55
        Width = 39
        Height = 13
        Caption = 'Grupo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl29: TLabel
        Left = 8
        Top = 74
        Width = 33
        Height = 13
        Caption = 'Ciclo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblGrupoFacturacionDescri: TLabel
        Left = 50
        Top = 55
        Width = 125
        Height = 13
        Caption = 'lblGrupoFacturacionDescri'
      end
      object lblCicloAno: TLabel
        Left = 50
        Top = 74
        Width = 52
        Height = 13
        Caption = 'lblCicloAno'
      end
      object lbl2: TLabel
        Left = 326
        Top = 36
        Width = 73
        Height = 13
        Caption = 'Monto Total:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblMontoTotal: TLabel
        Left = 430
        Top = 36
        Width = 64
        Height = 13
        Caption = 'lblMontoTotal'
      end
    end
    object grpParams: TGroupBox
      Left = 559
      Top = 0
      Width = 594
      Height = 114
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Par'#225'metros Utilizados'
      TabOrder = 3
      object lbl14: TLabel
        Left = 326
        Top = 17
        Width = 101
        Height = 13
        Caption = 'Meses Forzar NK:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl17: TLabel
        Left = 326
        Top = 36
        Width = 116
        Height = 13
        Caption = 'D'#237'as M'#237'n. entre NK:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl15: TLabel
        Left = 326
        Top = 74
        Width = 54
        Height = 13
        Caption = 'Valor UF:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl16: TLabel
        Left = 326
        Top = 55
        Width = 58
        Height = 13
        Caption = 'Valor IVA:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValIVA: TLabel
        Left = 448
        Top = 55
        Width = 42
        Height = 13
        Caption = 'lblValIVA'
      end
      object lblValUF: TLabel
        Left = 448
        Top = 74
        Width = 39
        Height = 13
        Caption = 'lblValUF'
      end
      object lblDiasMinEmiFac: TLabel
        Left = 448
        Top = 36
        Width = 83
        Height = 13
        Caption = 'lblDiasMinEmiFac'
      end
      object lblMesForGenNK: TLabel
        Left = 448
        Top = 17
        Width = 80
        Height = 13
        Caption = 'lblMesForGenNK'
      end
      object lbl10: TLabel
        Left = 11
        Top = 17
        Width = 85
        Height = 13
        Caption = 'Valor M'#237'n. NK:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinNK: TLabel
        Left = 202
        Top = 17
        Width = 57
        Height = 13
        Caption = 'lblValMinNK'
      end
      object lbl13: TLabel
        Left = 11
        Top = 36
        Width = 185
        Height = 13
        Caption = 'Valor M'#237'n. NK Pago Autom'#225'tico:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinNKPagAut: TLabel
        Left = 202
        Top = 36
        Width = 92
        Height = 13
        Caption = 'lblValMinNKPagAut'
      end
      object lblValMinDeuConv: TLabel
        Left = 202
        Top = 55
        Width = 87
        Height = 13
        Caption = 'lblValMinDeuConv'
      end
      object lbl11: TLabel
        Left = 11
        Top = 55
        Width = 162
        Height = 13
        Caption = 'Valor M'#237'n. Deuda Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinNKBaja: TLabel
        Left = 202
        Top = 74
        Width = 78
        Height = 13
        Caption = 'lblValMinNKBaja'
      end
      object lbl12: TLabel
        Left = 11
        Top = 74
        Width = 171
        Height = 13
        Caption = 'Valor M'#237'n. NK Convenio Baja:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl19: TLabel
        Left = 11
        Top = 93
        Width = 151
        Height = 13
        Caption = 'Valor M'#237'n. Importe Afecto:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinImpAfe: TLabel
        Left = 202
        Top = 93
        Width = 75
        Height = 13
        Caption = 'lblValMinImpAfe'
      end
    end
    object grpFiltros: TGroupBox
      Left = 559
      Top = 115
      Width = 594
      Height = 100
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Filtros'
      TabOrder = 4
      DesignSize = (
        594
        100)
      object lblImporteDesde: TLabel
        Left = 129
        Top = 16
        Width = 64
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'Monto Desde'
        ParentShowHint = False
        ShowHint = True
      end
      object lblImporteHasta: TLabel
        Left = 244
        Top = 16
        Width = 61
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'Monto Hasta'
        ParentShowHint = False
        ShowHint = True
      end
      object lbl1: TLabel
        Left = 9
        Top = 16
        Width = 81
        Height = 13
        Caption = 'Muestra aleatoria'
      end
      object lblExportarCSV: TLabel
        Left = 516
        Top = 10
        Width = 63
        Height = 13
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = 'Exportar CSV'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblExportarCSVClick
      end
      object BTNBuscarComprobante: TButton
        Left = 513
        Top = 65
        Width = 71
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Buscar'
        Default = True
        TabOrder = 3
        OnClick = BTNBuscarComprobanteClick
      end
      object txtImporteDesde: TNumericEdit
        Left = 129
        Top = 32
        Width = 96
        Height = 21
        TabOrder = 1
      end
      object txtImporteHasta: TNumericEdit
        Left = 243
        Top = 32
        Width = 100
        Height = 21
        TabOrder = 2
      end
      object txtMuestraAleatoria: TNumericEdit
        Left = 9
        Top = 32
        Width = 88
        Height = 21
        TabOrder = 0
      end
    end
  end
  object dsFacturas: TDataSource
    DataSet = spObtenerComprobantesResultado
    OnDataChange = dsFacturasDataChange
    Left = 456
    Top = 384
  end
  object spObtenerComprobantesResultado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerComprobantesResultado'
    Parameters = <>
    Left = 456
    Top = 328
  end
  object Detalle: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Importe'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 232
    Top = 642
  end
  object dsDetalle: TDataSource
    DataSet = Detalle
    Left = 272
    Top = 650
  end
  object spObtenerDetalleConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleConceptosComprobante'
    Parameters = <>
    Left = 104
    Top = 634
  end
  object tmrConsultaRUT: TTimer
    Enabled = False
    OnTimer = tmrConsultaRUTTimer
    Left = 550
    Top = 302
  end
  object spProcesosFacturacion_SELECT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ProcesosFacturacion_SELECT'
    Parameters = <>
    Left = 154
    Top = 329
  end
  object dlgSaveCSV: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 'Archivos Comma Separated Value|*.csv|Todos los Archivos|*.*'
    InitialDir = '.'
    Title = 'Guardar Como...'
    Left = 864
    Top = 328
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 602
    Top = 369
  end
  object dsTransitosPorComprobante: TDataSource
    DataSet = spObtenerTransitosPorComprobante
    Left = 624
    Top = 634
  end
  object spObtenerTransitosPorComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTransitosPorComprobante'
    Parameters = <>
    Left = 720
    Top = 650
  end
end
