unit ABMMotivosNoFacturar;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
    ToolWin, ComCtrls, UtilProc, Util;

resourcestring
    MSG_ERROR_UPDATE = 'Error Actualizando Registro';
    MSG_ERROR_INSERT = 'Error Agregando Registro';
    MSG_ERROR_DELETE = 'Error Eliminando Registro';
    MSG_ERROR_SELECT = 'Error Consultando Registros';

type
  TfrmABMMotivosNoFacturar = class(TForm)
    dbgrdGrid: TDBGrid;
    pnlControles: TPanel;
    lbl01: TLabel;
    lbl1: TLabel;
    edtDescripcion: TEdit;
    edtCodigo: TEdit;
    pnlInferior: TPanel;
    btnGuardar: TButton;
    btnCancelar: TButton;
    spUpdate: TADOStoredProc;
    spDelete: TADOStoredProc;
    spInsert: TADOStoredProc;
    dsGrid: TDataSource;
    pnlSuperior: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btn3: TToolButton;
    btnBuscar: TToolButton;
    ilImagenes: TImageList;
    il1: TImageList;
    spSelect: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spSelectAfterScroll(DataSet: TDataSet);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);         
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure dbgrdGridDblClick(Sender: TObject);
  private
    { Private declarations }
    CantidadRegistros : integer;
    Posicion          : TBookmark;
    Accion            : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno           : Integer;
    MensajeError      : string;
  public
    { Public declarations }
  end;

var
  frmABMMotivosNoFacturar: TfrmABMMotivosNoFacturar;

implementation

{$R *.dfm}

function TfrmABMMotivosNoFacturar.Inicializar(MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
    if MDIChild then begin
        FormStyle := fsMDIChild;
        S := GetFormClientSize(Self);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;
    Position := poMainFormCenter;
    Result := True;
end;

procedure TfrmABMMotivosNoFacturar.btnAgregarClick(Sender: TObject);
begin
    HabilitaBotones('000000110');
    edtCodigo.Text    := '';
    edtDescripcion.Enabled := True;
    edtDescripcion.Text    := '';
    edtDescripcion.SetFocus;
    Accion := 1;
end;

procedure TfrmABMMotivosNoFacturar.btnCancelarClick(Sender: TObject);
begin
    if CantidadRegistros > 0 then
        HabilitaBotones('111100001')
    else
        HabilitaBotones('110000001');

    try
        spSelect.GotoBookmark(Posicion);
    except
    end;

    with edtDescripcion do begin
        Enabled := False;
        Text    := spSelect.FieldByName('Descripcion').AsString;
    end;

    with edtCodigo do begin
        Enabled := False;
        Text    := spSelect.FieldByName('CodigoMotivo').AsString;
    end;
end;

procedure TfrmABMMotivosNoFacturar.btnEditarClick(Sender: TObject);
begin
    HabilitaBotones('000000110');

    with edtDescripcion do begin
        Enabled := True;   
        SetFocus;
    end;
    
    Accion := 3;
end;

procedure TfrmABMMotivosNoFacturar.btnEliminarClick(Sender: TObject);
begin
    HabilitaBotones('000000000');
    Accion := 2;
    
    try
        Posicion := spSelect.GetBookmark;
    except
    end;

    if Application.MessageBox(PChar('¿Desea eliminar este Motivo de No Facturación? - ' + spSelect.FieldByName('Descripcion').AsString),'Confirmación',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
        with spDelete do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoMotivo').Value  := spSelect.FieldByName('CodigoMotivo').AsInteger;
            try
                ExecProc;
                Retorno := spDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
                if Retorno <> 0 then begin
                    MensajeError := spDelete.Parameters.ParamByName('@ErrorDescription').Value;
                    MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
                end;
            except
                on E : Exception do begin
                    MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
                end;
            end;
        end;

    TraeRegistros;
    if CantidadRegistros > 0 then
        HabilitaBotones('111100001')
    else
        HabilitaBotones('110000001');
end;

procedure TfrmABMMotivosNoFacturar.btnGuardarClick(Sender: TObject);
begin
    if Length(edtDescripcion.Text) = 0 then begin
        MsgBoxBalloon('Ingrese la descripción del Motivo','Validación',MB_ICONSTOP,edtDescripcion);
        Exit;
    end;

    try
        Posicion := spSelect.GetBookmark;
    except
    end;

    if Accion = 1 then begin
        with spInsert do begin
            Parameters.Refresh;
            Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
            Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            try
                ExecProc;
                Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                if Retorno <> 0 then begin
                    MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
                    MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
                end;
            except
                on E : Exception do begin
                    MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
                end;
            end;
        end;
    end;

    if Accion = 3 then begin
        with spUpdate do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoMotivo').Value := spSelect.FieldByName('CodigoMotivo').Value;
            Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
            Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;

            try
                ExecProc;
                Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                if Retorno <> 0 then begin
                    MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
                    MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
                end;
            except
                on E : Exception do begin
                    MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
                end;
            end;
        end;
    end;

    edtDescripcion.Enabled := False;
    edtCodigo.Enabled := False;

    TraeRegistros;
    if CantidadRegistros > 0 then
        HabilitaBotones('111100001')
    else
        HabilitaBotones('110000001');
end;

procedure TfrmABMMotivosNoFacturar.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMMotivosNoFacturar.dbgrdGridDblClick(Sender: TObject);
begin
    btnEditar.Click;
end;

procedure TfrmABMMotivosNoFacturar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmABMMotivosNoFacturar.FormCreate(Sender: TObject);
begin
    TraeRegistros;
    if CantidadRegistros > 0 then
        HabilitaBotones('111100001')
    else
        HabilitaBotones('110000001');
end;

procedure TfrmABMMotivosNoFacturar.HabilitaBotones(Botones : string);
begin
    btnSalir.Enabled      := Botones[1] = '1';
    btnAgregar.Enabled    := Botones[2] = '1';
    btnEliminar.Enabled   := Botones[3] = '1';
    btnEditar.Enabled     := Botones[4] = '1';
    btnImprimir.Enabled   := Botones[5] = '1';
    btnBuscar.Enabled     := Botones[6] = '1';
    btnGuardar.Enabled    := Botones[7] = '1';
    btnCancelar.Enabled   := Botones[8] = '1';
    dbgrdGrid.Enabled     := Botones[9] = '1';
end;

procedure TfrmABMMotivosNoFacturar.spSelectAfterScroll(DataSet: TDataSet);
begin
    edtDescripcion.Text := DataSet.FieldByName('Descripcion').AsString;
    edtCodigo.Text := DataSet.FieldByName('CodigoMotivo').AsString;
end;

function TfrmABMMotivosNoFacturar.TraeRegistros;
begin
    try
        spSelect.Close;
        spSelect.Open;

        CantidadRegistros := spSelect.RecordCount;
        try
            spSelect.GotoBookmark(Posicion);
        except
        end;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
        end;
    end;
end;

end.
