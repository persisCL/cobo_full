{ *******************************************************
Revision 1
Author: nefernandez
Date: 16/03/2007
Description : Se agrego el parametro Usuario al SP "GenerarMovimientosTransitos" -
                para auditoria
******************************************************* }
unit DM_ThreadFact;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Util, UtilDB, UtilProc, ADODB, DMConnection;

type
  TDMThreadFacturacion = class(TDataModule)
	GenerarMovimientosTransitos: TADOStoredProc;
    spPREFAC_GenerarMovimientosTransitos: TADOStoredProc;	//TASK_086_MGO_20161121
	procedure DataModuleCreate(Sender: TObject);
  private
	{ Private declarations }
	Conn: TDMConnections;
  public
    { Public declarations }
  end;

var
  DMThreadFacturacion: TDMThreadFacturacion;

implementation

{$R *.DFM}

procedure TDMThreadFacturacion.DataModuleCreate(Sender: TObject);
begin
    //Creamos el DMConnection
	Conn := TDMConnections.Create(Self);
    // Lo Configuramos para que sepa a d�nde conectarse
    Conn.SetUp;
	GenerarMovimientosTransitos.Connection := Conn.BaseCAC;
end;

end.
