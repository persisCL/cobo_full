object DMThreadFacturacion: TDMThreadFacturacion
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 146
  Width = 242
  object GenerarMovimientosTransitos: TADOStoredProc
    ExecuteOptions = [eoExecuteNoRecords]
    CommandTimeout = 0
    ProcedureName = 'GenerarMovimientosTransitos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConvenioAnterior'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@GrupoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NoGenerarAjusteSencillo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NoGenerarIntereses'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ConveniosProcesados'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 97
    Top = 7
  end
  object spPREFAC_GenerarMovimientosTransitos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    CommandTimeout = 0
    ProcedureName = 'PREFAC_GenerarMovimientosTransitos;1'
    Parameters = <>
    Left = 105
    Top = 71
  end
end
