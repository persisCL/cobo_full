{-------------------------------------------------------------------------------
 File Name: frm_ImprimirNotaCobroDirecta.pas
 Author:  mlopez
 Date Created: 01/06/2005
 Language: ES-AR
 Description: Permite imprimir la nota de cobro generada

 
  Revision 1
  Author: mbecerra
  Date: 04-Septiembre-2008
  Description: Se agrega el filtro por proveedor DayPass

  Revision 2:
  Author: mbecerra
  Date: 03-Octubre-2008
  Description: Se agrega que el resultado pueda ordenarse por columna

  Revision 3
  Author: mbecerra
  Date: 14-Junio-2009
  Description:		(Ref Facturacion Electronica)
            	Se agrega la impresi�n de la factura exenta electronica

    	31-Agosto-2009
               Se modifica para que puedan imprimir Boletas exentas tambi�n.

  Firma       : SS-985-NDR-20111114
  Description : Se fija, en caso que la NQ este anulada, si tiene historicoTransitosDaypass
                Si es asi, puede imprimir el detalle igual (antes salia en blanco)
-------------------------------------------------------------------------------}
unit frm_ImprimirNotaCobroDirecta;

interface

uses
  //Imprimir nota de cobro directa
  DMConnection,
  PeaTypes,
  PeaProcs,
  Util,
  UtilProc,
  frm_ReporteNotaCobroDirecta,
  frmReporteBoletaFacturaElectronica,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, DB, ADODB, VariantComboBox,
  UtilDB;                                                                       //SS-985-NDR-20111114

type
  Tform_ImprimirNotaCobroDirecta = class(TForm)
    dbl_NotasCobro: TDBListEx;
    Label1: TLabel;
    btn_ImprimirComprobante: TButton;
    btn_Cerrar: TButton;
    sp_ObtenerNotasCobroNQ: TADOStoredProc;
    ds_ObtenerNotasCobroNQ: TDataSource;
    btn_ImprimirDetalle: TButton;
    btnBuscar: TButton;
    rbNotasCobroPDU: TRadioButton;
    rbNotasCobroBHTU: TRadioButton;
    Label2: TLabel;
    cbProveedorDayPass: TVariantComboBox;
    procedure btnBuscarClick(Sender: TObject);
    procedure btn_ImprimirDetalleClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_CerrarClick(Sender: TObject);
    procedure btn_ImprimirComprobanteClick(Sender: TObject);
    procedure OrdenarPorColumna(Sender: TObject);
    procedure sp_ObtenerNotasCobroNQAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar : Boolean;
    procedure CargarProveedoresDayPass;
  end;

var
  form_ImprimirNotaCobroDirecta: Tform_ImprimirNotaCobroDirecta;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author: mlopez
  Date Created: 01/06/2005
  Description: Inicializo este formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tform_ImprimirNotaCobroDirecta.Inicializar: Boolean;
begin
    btn_ImprimirComprobante.Enabled := False;
    btn_ImprimirDetalle.Enabled     := False;
    Result := True;
    {Revision 1:  cargar los proveedores DayPass}
    CargarProveedoresDayPass();
end;

{-----------------------------------------------------------------------------
  Function Name: OrdenarPorColumna
  Author: mbecerra
  Date Created: 03-Octubre-2008
  Description: Ordena los datos de la TDBListEx de acuerdo a la columna que seleccione el usuario
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.OrdenarPorColumna(Sender: TObject);
begin
	if not sp_ObtenerNotasCobroNQ.Active then Exit
    else
        OrdenarGridPorColumna(	TDBListExColumn(Sender).Columns.List,
                                TDBListExColumn(Sender),
                                TDBListExColumn(Sender).FieldName);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarProveedoresDayPass
  Author: mbecerra
  Date Created: 04-Sept-2008
  Description: carga los proveedores DayPass
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.CargarProveedoresDayPass;
var
  	I : Integer;
  	Qry : TADOQuery;
    S : AnsiString;
begin
    Qry := TAdoQuery.Create(nil);
    try
        Qry.Connection := DMConnections.BaseCAC;
        S := 'SELECT * FROM ProveedoresDayPass WITH (nolock) ORDER BY Descripcion';
        Qry.SQL.Text := S;
        Qry.Open;
        cbProveedorDayPass.Clear;
        I := -1;
        while not Qry.Eof do begin
          cbProveedorDayPass.Items.Add(	Qry.FieldByName('Descripcion').AsString + Space(200) +  {descripci�n}
          								Qry.FieldByName('NumeroDocumento').AsString,            {Rut}
                            			Qry.FieldByName('CodigoProveedorDayPass').AsInteger);   {codigoProveedor}

          Qry.Next;
        end;
    finally
        Qry.Close;
        Qry.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarClick
  Author: mlopez
  Date Created: 01/06/2005
  Description: Permito obtener las Notas de Cobro directas a imprimir
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.btnBuscarClick(Sender: TObject);       
resourcestring
    MSG_ERROR_EMPTY_DATA 	= 'No hay datos de acuerdo a las opciones seleccionadas';
	MSG_SIN_PROV 			= 'Falta seleccionar el proveedor DayPass';

begin
	if cbProveedorDayPass.Text = '' then begin
    	MsgBox(MSG_SIN_PROV, Caption, MB_ICONEXCLAMATION);
        Exit;
    end;

    sp_ObtenerNotasCobroNQ.Close;
    sp_ObtenerNotasCobroNQ.Parameters.Refresh;
    if rbNotasCobroPDU.Checked then begin	// Buscar las Notas de Cobro Directas por PDU
        sp_ObtenerNotasCobroNQ.Parameters.ParamByName('@TipoComprobante').Value := TC_NOTA_COBRO_DIRECTA;
    end else begin							// Buscar las Notas de Cobro Directas por BHTU
        sp_ObtenerNotasCobroNQ.Parameters.ParamByName('@TipoComprobante').Value := TC_NOTA_COBRO_DIRECTA_BHTU;
    end;

    sp_ObtenerNotasCobroNQ.Parameters.ParamByName('@RutProveedorDayPass').Value := Trim(StrRight(cbProveedorDayPass.Text,200));
    sp_ObtenerNotasCobroNQ.CommandTimeout := 500;
    sp_ObtenerNotasCobroNQ.Open;

    btn_ImprimirComprobante.Enabled := not sp_ObtenerNotasCobroNQ.IsEmpty;
    btn_ImprimirDetalle.Enabled := (not sp_ObtenerNotasCobroNQ.IsEmpty) and (
                                                                              (sp_ObtenerNotasCobroNQ.FieldByName('EstadoPago').AsString <> 'A') or                                                       //SS-985-NDR-20111114
                                                                              (StrToBool(QueryGetValue( DMConnections.BaseCAC,                                                                            //SS-985-NDR-20111114
                                                                                                        Format('SELECT dbo.NotaCobroNQTieneHistorico('+''''+'%s'+''''+',%d)',                             //SS-985-NDR-20111114
                                                                                                                [ sp_ObtenerNotasCobroNQ.FieldByName('TipoComprobante').AsString,                         //SS-985-NDR-20111114
                                                                                                                  sp_ObtenerNotasCobroNQ.FieldByName('NumeroComprobante').AsInteger                       //SS-985-NDR-20111114
                                                                                                                ]                                                                                         //SS-985-NDR-20111114
                                                                                                              )                                                                                           //SS-985-NDR-20111114
                                                                                                      )                                                                                                   //SS-985-NDR-20111114
                                                                                          )                                                                                                               //SS-985-NDR-20111114
                                                                              )                                                                                                                           //SS-985-NDR-20111114
                                                                            );

    if sp_ObtenerNotasCobroNQ.IsEmpty then begin
        MsgBox(MSG_ERROR_EMPTY_DATA, Caption, MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: sp_ObtenerNotasCobroNQAfterOpen
  Author: mlopez
  Date Created: 01/06/2005
  Description: Habilito / Deshabilito botones para imprimir
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.sp_ObtenerNotasCobroNQAfterScroll(
  DataSet: TDataSet);
begin
    if sp_ObtenerNotasCobroNQ.Active then begin
        btn_ImprimirDetalle.Enabled :=  (sp_ObtenerNotasCobroNQ.FieldByName('EstadoPago').AsString <> 'A') or                                                       //SS-985-NDR-20111114
                                        (StrToBool(QueryGetValue( DMConnections.BaseCAC,                                                                            //SS-985-NDR-20111114
                                                                  Format('SELECT dbo.NotaCobroNQTieneHistorico('+''''+'%s'+''''+',%d)',                             //SS-985-NDR-20111114
                                                                          [ sp_ObtenerNotasCobroNQ.FieldByName('TipoComprobante').AsString,                         //SS-985-NDR-20111114
                                                                            sp_ObtenerNotasCobroNQ.FieldByName('NumeroComprobante').AsInteger                       //SS-985-NDR-20111114
                                                                          ]                                                                                         //SS-985-NDR-20111114
                                                                        )                                                                                           //SS-985-NDR-20111114
                                                                )                                                                                                   //SS-985-NDR-20111114
                                                    )                                                                                                               //SS-985-NDR-20111114
                                        );                                                                                                                          //SS-985-NDR-20111114
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_ImprimirComprobanteClick
  Author: mlopez
  Date Created: 01/06/2005
  Description: Permito imprimir la nota de cobro
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
Revision 1:
    Author : ggomez
    Date : 16/03/2006
    Description : Agregu� el par�metro TipoComprobante en la llamada al m�todo
          ImprimirComprobante.
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.btn_ImprimirComprobanteClick(Sender: TObject);
var
    f : Tform_ReporteNotaCobroDirecta;
    e : TReporteBoletaFacturaElectronicaForm;
    TipoComprobante, sError : string;
begin
    Screen.Cursor := crHourGlass;
    //REV.3
    with sp_ObtenerNotasCobroNQ do begin
    	if	(FieldByName('TipoComprobanteFiscal').AsString <> TC_NOTA_COBRO_DIRECTA) and
        	(FieldByName('TipoComprobanteFiscal').AsString <> TC_NOTA_COBRO_DIRECTA_BHTU) then begin
        	Application.CreateForm(TReporteBoletaFacturaElectronicaForm, e);
        	try
            	if e.Inicializar(	DMConnections.BaseCAC, FieldByName('TipoComprobante').AsString,
            						FieldByName('NumeroComprobante').AsInteger, sError) then begin
                	e.Ejecutar();
            	end
            	else ShowMessage(sError);
            finally
                e.Release;
            end;
        end
        else begin
        	Application.CreateForm(Tform_ReporteNotaCobroDirecta, f);
            try
                f.ImprimirComprobante(	FieldByName('TipoComprobante').AsString,
                    					FieldByName('NumeroComprobante').AsInteger);
            finally
                f.Release;
            end;

        end;  {else}
    end;    {with}


    Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_ImprimirDetalleClick
  Author: mlopez
  Date Created: 01/06/2005
  Description: Permito imprimir el detalle de la nota de cobro
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
Revision 1:
    Author : ggomez
    Date : 16/03/2006
    Description : Agregu� el par�metro TipoComprobante en la llamada al m�todo
          ImprimirDetalle.
----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.btn_ImprimirDetalleClick(Sender: TObject);
var
    f: Tform_ReporteNotaCobroDirecta;
begin
    Screen.Cursor := crHourGlass;
    Application.CreateForm(Tform_ReporteNotaCobroDirecta, f);
    try
        f.ImprimirDetalle(sp_ObtenerNotasCobroNQ.FieldByName('TipoComprobante').AsString,
            sp_ObtenerNotasCobroNQ.FieldByName('NumeroComprobante').AsInteger);
    finally
        f.Release;
        Screen.Cursor := crDefault;
    end;
    Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_CerrarClick
  Author: lgisuk
  Date Created:  20/03/06
  Description:  Permito cerrar el formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.btn_CerrarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author: lgisuk
  Date Created: 20/03/06
  Description: Libero el formulario de memoria
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroDirecta.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    sp_ObtenerNotasCobroNQ.Close;
    Action := caFree;
end;

end.
