unit DMProcInterfaces;

interface

uses
  SysUtils, Classes, DMConnection, DB, ADODB, comCtrls, variants, PeaProcs, peaTypes, util,
  utilProc, dialogs, Windows, utilDB;


type
  TDMInterfaces = class(TDataModule)
	InterfazEntradaDebitoAutomatico: TADOStoredProc;
	tblEntrada: TADOTable;
	InterfazEntradaJuzgado: TADOStoredProc;
	tblSalida: TADOTable;
	InterfazSalidaDebitoAutomatico: TADOStoredProc;
	InterfazEntradaRegistroPatentes: TADOStoredProc;
	InterfazSalidaAgenciaCobranza: TADOStoredProc;
	InterfazSalidaJuzgado: TADOStoredProc;
  private
	{ Private declarations }
	FIDRegistroOperacion: Integer;
	FDirInterfacesSalida: AnsiString;

	Function EjecutarEntradaDebitosAutomaticos(FileSource: AnsiString; ModuloInterfaz: Integer;
	  TipoDebito: AnsiString; CodeEntity: Variant; ProgressBar: TProgressBar;
	  var MensajeResultado: AnsiString): Integer;
	Function EjecutarEntradaJuzgado(FileSource: AnsiString; ModuloInterfaz: Integer;
	  ProgressBar: TProgressBar; var MensajeResultado: AnsiString): Integer;
	Function EjecutarSalidaDebitosAutomaticos(ModuloInterfaz: Integer; TipoDebito: AnsiString;
	  CodeEntity, IDRegistroOriginal, NameEntity: Variant; ProgressBar: TProgressBAR;
	  var MensajeResultado: AnsiString): Integer;
	Function EjecutarSalidaJuzgado(ModuloInterfaz: Integer; IDRegistroOriginal: Variant;
	  ProgressBar: TProgressBAR; var MensajeResultado: AnsiString): Integer;
	Function EjecutarEntradaRegistroPatente(FileSource: AnsiString; ModuloInterfaz: Integer;
	  ProgressBar: TProgressBAR; var MensajeResultado: AnsiString): Integer;
	Function EjecutarSalidaAgenciaCobranza(ModuloInterfaz: Integer; IDRegistroOriginal:Variant;
	  ProgressBar: TProgressBar; var MensajeResultado: AnsiString): Integer;

  public
	{ Public declarations }
	Procedure EjecutarInterfaz(FileSource: AnsiString; ModuloInterfaz: Integer; Reservado1, Reservado2, Reservado3: Variant;
	  aProgressBar: TProgressBAR);
  end;

var
  DMInterfaces: TDMInterfaces;

implementation

{$R *.dfm}

// Todas las funciones de interfaz retornan los siguientes valores:
//      0: Todo Ok.
//      1: Existio un error sobre algun comprobante procesado.
//      2: Existio un error grave por lo cual el proceso de la interfaz se interrumpio.


resourcestring
    MSG_GENERAR_INTERFAZ        = 'Error generando la Interfaz.';
    CAPTION_GENERAR_INTERFAZ    = 'Proceso de Interfaz';


function formatearComprobante(Conn: TADOConnection; TipoComprobante: Char;  NumeroComprobante: Double):AnsiString;
var Query: TADOQuery;
begin
	Query := TADOQuery.Create(nil);
	with Query do begin
		Connection := Conn;
		SQL.Add(format('select dbo.ObtenerDescripcionTipoComprobante(''%s'') + '':'' + ' +
		  'CONVERT(VARCHAR(10), %.0f) As DescriComprobanteNumero', [TipoComprobante, NumeroComprobante]));
		Open;
		result := fieldByName('DescriComprobanteNumero').asString;
		Free;
	end;
end;

function CrearInterface(Conn: TADOConnection; ModuloInterfaz: Integer; Reservado1, Reservado2, Reservado3: Variant): Integer;
var DescriError: array[0..255] of char;
begin
	result := CrearRegistroOperaciones(Conn, SYS_FACTURACION, ModuloInterfaz, RO_AC_EJECUCION_INTERFAZ,
      0, getMachineName, UsuarioSistema, 'Interfaz', 0, Reservado1, Reservado2, Reservado3, DescriError);
    if Result = -1 then begin
    	MsgBoxErr(MSG_GENERAR_INTERFAZ, DescriError, CAPTION_GENERAR_INTERFAZ, MB_OK);
		result := -1;
	end;
end;

Function TDMInterfaces.EjecutarEntradaDebitosAutomaticos(FileSource: AnsiString;
  ModuloInterfaz: Integer; TipoDebito: AnsiString; CodeEntity: Variant; ProgressBar: TProgressBar;
  var MensajeResultado: AnsiString): Integer;

var DescriError: array[0..255] of char;
    MensajeError: AnsiString;
	CantidadComprobantes, CantidadErrores: Integer;

begin
    Result := 0;
	CantidadErrores := 0;
	with tblEntrada do begin
		try
			connectionString := 'Provider=MSDASQL.1;Persist Security Info=False;Extended Properties="DSN=Textos;' +
			  format('DBQ=%s;DefaultDir=;DriverId=27;FIL=text;MaxBufferSize=2048;PageTimeout=5;"',[ExtractFileDir(FileSource)]);
			TableName := StringReplace(ExtractFileName(FileSource),' ','_',[rfReplaceAll]);
			Open;
		except
			on E: Exception do begin
				MensajeResultado := format('Error intentado abrir archivo de entrada. Mensaje de error: %s', [E.message]);
				result := 2;
				exit;
			end;
		end;

		if assigned(ProgressBar) then begin
			ProgressBar.max := recordCount + 1;
			ProgressBar.position := 1;
		end;
		CantidadComprobantes := recordCount;
		first;
		while not eof do begin
			if assigned(ProgressBar) then
				ProgressBar.position := ProgressBar.position + 1;
			try
				InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@TipoComprobante').Value :=
				  FieldByName('TipoComprobante').asString[1];
				InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@NumeroComprobante').value :=
				  FieldByName('NumeroComprobante').asFloat;
				InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@Importe').value :=
				  FieldByName('ImporteTotal').asFloat;
				InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@TipoDebito').Value := TipoDebito;
				InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@CodigoEntidad').Value := CodeEntity;
				InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@CuentaDebito').value :=
				  FieldByName('CuentaDebito').asString;
				InterfazEntradaDebitoAutomatico.execProc;
				if ((InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@RETURN_VALUE').value > 0) and
				  (InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@RETURN_VALUE').value < 6)) then begin
					case InterfazEntradaDebitoAutomatico.Parameters.ParamByName('@RETURN_VALUE').value of
						1: MensajeError := 'El Comprobante no existe.';
						2: MensajeError := 'El Comprobante a�n no esta debitado.';
						3: MensajeError := 'El Comprobante ya esta pago.';
						4: MensajeError := 'El comprobante forma parte de un tr�mite judicial.';
						5: MensajeError := 'El medio de pago es incorrecto.';
					end;
					// Creamos el registro de operaci�n
   					inc(CantidadErrores);
					if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION, ModuloInterfaz,
					  RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
					  format('Comprobante: %s',[formatearComprobante(DMConnections.BaseCAC,
						fieldByName('TipoComprobante').asString[1], FieldByName('NumeroComprobante').asFloat)]),
					  FIDRegistroOperacion, null, null, MensajeError, DescriError) = -1 then begin
                        MensajeResultado := DescriError;
                        result := 2;
                        break;
                    end;
				end;
			except
				On E: Exception do begin
					// Creamos el registro de operaci�n
   					inc(CantidadErrores);
					if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION, ModuloInterfaz,
					  RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
					  'Error procesando el archivo de entrada.', FIDRegistroOperacion, null, null, E.Message,
                      DescriError) = -1 then begin
                        result := 2;
                        MensajeResultado := DescriError;
                        break;
                     end;
				end;
			end;
			next;
		end;
		close;
	end;
    if (result = 0) then
        if (CantidadErrores = 0) then
    		MensajeResultado := format('Proceso finalizado con �xito. Total de Comprobantes: %d',
	    	  [CantidadComprobantes]);
    	begin
            result := 1;
       		MensajeResultado := format('Proceso finalizado con error. Total de Comprobantes: %d - ' +
    		  'Comprobantes que fallaron: %d.',[CantidadComprobantes, CantidadErrores])
        end;
	if assigned(ProgressBar) then
		ProgressBar.position := 0;
end;


Function TDMInterfaces.EjecutarEntradaJuzgado(FileSource: AnsiString; ModuloInterfaz: Integer;
  ProgressBar: TProgressBar; var mensajeResultado: AnsiString): Integer;

  function GrabarError(Patente, ResolucionEnviada, ResolucionActual,
	MensajeError: AnsiString; ExpedienteEnviado, IDRegistroOperacion: Integer;
	FechaTramiteJudicial, ExpedienteActual: Variant): Boolean;
  var DescriError: array[0..255] of char;
      Fecha, Evento: AnsiString;
	  Expediente: Integer;
  begin
      Result := True;

	  if ExpedienteActual = null then
		  Expediente := 0
	  else
		  Expediente := Integer(ExpedienteActual);

	  try
		  fecha := trim(formatDateTime('dd/mm/yyyy', varToDateTime(FechaTramiteJudicial)));
	  except
		  fecha := 'No Asignada';
	  end;

	  // Formamos el mensaje del registro de operaci�n
	  evento := format('Patente: %10.10s - Fecha Inicio Tr�mite Judicial: %s - N�mero Expediente Enviado: %d - N�mero Expediente Actual: %d - ' +
		'Resoluci�n Enviada: %2.2s - Resoluci�n Actual: %2.2s',
		[Patente, Fecha, ExpedienteEnviado, Expediente, ResolucionEnviada, ResolucionActual]);

	  // Creamos el registro de operaci�n
	  if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION, ModuloInterfaz,
		RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
		evento, IDRegistroOperacion, null, null, MensajeError, DescriError) = -1 then begin
          result := False;
      end;
  end;

var DescriError: array[0..255] of char;
    Patente, MensajeError: AnsiString;
	CantidadInfracciones, CantidadPatentes, CantidadErrores: Integer;
begin
	CantidadErrores := 0;
	CantidadPatentes := 0;
	Patente := '';
    result := 0;
	with tblEntrada do begin
		try
			connectionString := 'Provider=MSDASQL.1;Persist Security Info=False;Extended Properties="DSN=Textos;' +
			  format('DBQ=%s;DefaultDir=;DriverId=27;FIL=text;MaxBufferSize=2048;PageTimeout=5;"',[ExtractFileDir(FileSource)]);
			TableName := StringReplace(ExtractFileName(FileSource),' ','_',[rfReplaceAll]);
			Open;
		except
			on E: Exception do begin
				MensajeResultado := format('Error intentado abrir archivo de entrada. Mensaje de error: %s', [E.message]);
				result := 2;
				exit;
			end;
		end;
		if assigned(ProgressBar) then begin
			ProgressBar.max := recordCount;
			ProgressBar.position := 0;
		end;
		CantidadInfracciones := recordCount;
		first;
		while not eof do begin
			if assigned(ProgressBar) then
				ProgressBar.position := ProgressBar.position + 1;
			// Ejecutamos el proceso
			try
				if Patente <> FieldByName('Patente').asString then begin
					inc(CantidadPatentes);
					Patente := FieldByName('Patente').asString;
				end;
				InterfazEntradaJuzgado.Parameters.ParamByName('@Patente').value :=
				  trim(FieldByName('Patente').asString);
				InterfazEntradaJuzgado.Parameters.ParamByname('@NumeroHistorialInfraccion').value :=
				  FieldByName('NumeroHistorialInfraccion').asInteger;
				InterfazEntradaJuzgado.Parameters.ParamByName('@NumeroExpediente').value :=
				  FieldByName('NumeroExpediente').asInteger;
				InterfazEntradaJuzgado.Parameters.ParamByName('@Resolucion').value :=
				  trim(FieldByName('Resolucion').asString);
				InterfazEntradaJuzgado.execProc;
				if InterfazEntradaJuzgado.Parameters.ParamByName('@RETURN_VALUE').value <> 0 then begin
					case InterfazEntradaJuzgado.Parameters.ParamByName('@RETURN_VALUE').value of
						1:  MensajeError := 'La infracci�n no existe.';
						2:  MensajeError := 'La infracci�n no se ha enviado al juzgado.';
						3:  MensajeError := 'La infracci�n ya ha sido resuelta.';
						4:  MensajeError := 'El expediente indicado no se corresponde con el asignado por el Juzgado de Policia Local.';
					end;
        			inc(CantidadErrores);
					if GrabarError(FieldByName('Patente').asString, FieldByName('Resolucion').asString,
					  VarToSTr(InterfazEntradaJuzgado.Parameters.paramByName('@ResolucionActual').value),
					  MensajeError, FieldByName('NumeroExpediente').asInteger,
					  FIDRegistroOperacion, InterfazEntradaJuzgado.Parameters.paramByName('@FechaTramiteJudicial').value,
					  InterfazEntradaJuzgado.Parameters.ParamByName('@ExpedienteActual').value) then begin
                          MensajeResultado := DescriError;
            			  result := 2;
                          Break;
                      end;
				end;
			except
				On E: Exception do begin
       				inc(CantidadErrores);
					if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION, ModuloInterfaz,
					  RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
					  'Error procesando el archivo de entrada.', FIDRegistroOperacion, null, null, E.Message, DescriError) = -1 then begin
        				result := 2;
                        MensajeResultado := DescriError;
                        Break;
                    end;
				end;
			end;
			next;
		end;
		close;
	end;
	if result = 0 then
        if (CantidadErrores = 0) then
        	MensajeResultado := format('Proceso finalizado con �xito. Total de Infracciones: %d - ' +
    		  'Total de Patentes: %d', [CantidadInfracciones, CantidadPatentes])
        else begin
            result := 1;
	    	MensajeResultado := format('Proceso finalizado con error. Total de Infracciones: %d - ' +
		      'Total de Patentes: %d - Infracciones que fallaron: %d.', [CantidadInfracciones, CantidadPatentes,
    		  CantidadErrores])
    	end;
	if assigned(ProgressBar) then
		ProgressBar.position := 0;
end;


Function TDMInterfaces.EjecutarSalidaDebitosAutomaticos(ModuloInterfaz: Integer; TipoDebito: AnsiString;
  CodeEntity, IDRegistroOriginal, NameEntity: Variant; ProgressBar: TProgressBAR; var MEnsajeResultado: AnsiString): Integer;
var DescriError: array[0..255] of char;
    FileName: AnsiString;
	F: TextFile;
	CantidadComprobantes, CantidadErrores: Integer;
    Consulta: AnsiString;
begin
	CantidadComprobantes := 0;
	CantidadErrores := 0;
    result := 0;
	try
		FileName := format('sal_%s_%s.txt',[TipoDebito, stringReplace(trim(NameEntity),' ','_',[rfReplaceAll])]);
		AssignFile(F, format('%s\%s',[FDirInterfacesSalida, FileName]));
   		ReWrite(F);
		CloseFile(F);
		tblSalida.ConnectionString := 'Provider=MSDASQL.1;Persist Security Info=False;Extended Properties="DSN=Textos;' +
		  format('DBQ=%s;DefaultDir=;DriverId=27;FIL=text;MaxBufferSize=2048;PageTimeout=5;"',[FDirInterfacesSalida]);
		tblSalida.TableName := FileName;
		tblSalida.open;
	except
		on E: Exception do begin
			MensajeResultado := format('Error intentado abrir archivo de salida. Mensaje de error: %s', [E.message]);
			result := 2;
			exit;
		end;
	end;
	try
		// Obtenemos los comprobantes a debitar
		with InterfazSalidaDebitoAutomatico do begin
			Parameters.ParamByName('@TipoDebito').value := TipoDebito;
			Parameters.ParamByName('@CodigoEntidad').value := CodeEntity;
			Parameters.ParamByName('@IDRegistroOperacion').value := IDRegistroOriginal;
			Open;
			if Assigned(ProgressBar) then begin
				ProgressBar.Max := RecordCount + 1;
				ProgressBar.Position := 1;
			end;
			CantidadComprobantes := RecordCount;

			// Procesamos cada uno de los comprobantes a enviar
			while not eof do begin
				try
					TblSalida.append;
					TblSalida.fieldByName('TipoComprobante').asString := FieldByName('TipoComprobante').asString;
					TblSalida.FieldByName('NumeroComprobante').asFloat := FieldByName('NumeroComprobante').asFloat;
					TblSalida.fieldByName('ImporteTotal').asFloat := FieldByName('ImporteTotal').asFloat;
					TblSalida.FieldByName('CuentaDebito').asString := FieldByName('CuentaDebito').asString;
					TblSalida.Post;
					if IDRegistroOriginal = null then begin
                        Consulta := format('UPDATE Comprobantes SET IDRegistroOperacion = %d ' +
						  'WHERE TipoComprobante = ''%s'' AND NumeroComprobante = %.0f',
						  [FIDRegistroOperacion, FieldByName('TipoComprobante').asString[1], FieldByName('NumeroComprobante').asFloat]);
						QueryExecute(DMConnections.BaseCAC, Consulta);
					end;
				except
					on E: Exception do begin
					    TblSalida.cancel;
						// Generamos el nuevo registro de operaciones
                        inc(CantidadErrores);
					    if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION,
						  ModuloInterfaz, RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
						  'Error procesando el archivo resultado.', FIDRegistroOperacion, null, null, E.message, DescriError) = -1 then begin
                            MensajeResultado := DescriError;
                            result := 2;
                            break;
                        end;
					end;
				end;
				if Assigned(ProgressBar) then
					ProgressBar.Position := ProgressBar.Position + 1;
				next;
			end;
			close;
		end;
	finally
		tblSalida.close;
		if result = 0 then
            if (CantidadErrores = 0) then
      			MensajeResultado := format('Proceso finalizado con �xito. Total de Comprobantes: %d',
	    		  [CantidadComprobantes])
            else begin
                result := 1;
		    	MensajeResultado := format('Proceso finalizado con error. Total de Comprobantes: %d - ' +
			      'Comprobantes que fallaron: %d.',[CantidadComprobantes, CantidadErrores])
    		end;
		if assigned(ProgressBar) then
			ProgressBar.position := 0;
	end;
end;


Function TDMInterfaces.EjecutarSalidaJuzgado(ModuloInterfaz: Integer;
  IDRegistroOriginal: Variant; ProgressBar: TProgressBAR; var MensajeResultado: AnsiString): Integer;
var F: TextFile;
	DescriError: array[0..255] of char;
    Patente, FileName: AnsiString;
	CantidadInfracciones, CantidadPatentes, CantidadErrores: Integer;
begin
	CantidadInfracciones := 0;
	CantidadPatentes := 0;
	CantidadErrores := 0;
    result := 0;
	// Obtenemos y vaciamos el archivo
	try
		FileName := format('Sal_%s.txt', [StringReplace(NE_JUZGADO_LOCAL,' ','_',[rfReplaceAll])]);
		AssignFile(F, format('%s\%s',[FDirInterfacesSalida, FileName]));
		ReWrite(F);
		CloseFile(F);
		tblSalida.ConnectionString := 'Provider=MSDASQL.1;Persist Security Info=False;Extended Properties="DSN=Textos;' +
		  format('DBQ=%s;DefaultDir=;DriverId=27;FIL=text;MaxBufferSize=2048;PageTimeout=5;"',[FDirInterfacesSalida]);
		tblSalida.TableName := FileName;
		tblSalida.open;
	except
		on E: Exception do begin
			MensajeResultado := format('Error intentado abrir archivo de salida. Mensaje de error: %s', [E.message]);
			result := 2;
			exit;
		end;
	end;

	try
		// Obtenemos las infracciones a enviar
		with InterfazSalidaJuzgado do begin
			Parameters.ParamByName('@IDRegistroOperacion').value := IDRegistroOriginal;
			Open;
			if Assigned(ProgressBar) then begin
				ProgressBar.Max := RecordCount + 1;
				ProgressBar.Position := 1;
			end;
			CantidadInfracciones := RecordCount;

			// Agregamos al archivo cada item obtenido
			while not eof do begin
				try
					if (Patente <> FieldByName('Patente').asString) then begin
						Patente := FieldByName('Patente').asString;
						inc(CantidadPatentes);
					end;
					TblSalida.append;
					TblSalida.fieldByName('Patente').asString := FieldByName('Patente').asString;
					TblSalida.FieldByName('NumeroHistorialInfraccion').asInteger := FieldByName('NumeroHistorialInfraccion').asInteger;
					TblSalida.fieldByName('ImporteInfraccion').asFloat := FieldByName('ImporteInfraccion').asFloat;
					TblSalida.FieldByName('MotivoInfraccion').asString := FieldByName('MotivoInfraccion').asString;
					TblSalida.fieldByName('CodigoCuenta').asInteger := FieldByName('CodigoCuenta').asInteger;
					TblSalida.FieldByName('Marca').asString := FieldByName('Marca').asString;
					TblSalida.fieldByName('Modelo').asString := FieldByName('Modelo').asString;
					TblSalida.FieldByName('Color').asString := FieldByName('Color').asString;
					TblSalida.fieldByName('Apellido').asString := FieldByName('Apellido').asString;
					TblSalida.FieldByName('Nombre').asString := FieldByName('Nombre').asString;
					TblSalida.fieldByName('CodigoDocumento').asString := FieldByName('CodigoDocumento').asString;
					TblSalida.FieldByName('NumeroDocumento').asString := FieldByName('NumeroDocumento').asString;
					TblSalida.fieldByName('Domicilio').asString := FieldByName('Domicilio').asString;
					TblSalida.FieldByName('CodigoPostal').asString := FieldByName('CodigoPostal').asString;
					TblSalida.fieldByName('Comuna').asString := FieldByName('Comuna').asString;
					TblSalida.FieldByName('Region').asString := FieldByName('Region').asString;
					TblSalida.fieldByName('Pais').asString := FieldByName('Pais').asString;
					TblSalida.FieldByName('PuntoCobro').asString := FieldByName('PuntoCobro').asString;
					TblSalida.fieldByName('Concesionaria').asString := FieldByName('Concesionaria').asString;
					TblSalida.FieldByName('FechaHora').asDateTime := FieldByName('FechaHora').asDateTime;
					TblSalida.fieldByName('TipoComprobante').asString := FieldByName('TipoComprobante').asString;
					TblSalida.FieldByName('NumeroComprobante').asFloat := FieldByName('NumeroComprobante').asFloat;
					TblSalida.fieldByName('FechaHoraComprobante').asDateTime := FieldByName('Fecha').asDateTime;
					TblSalida.FieldByName('VencimientoComprobante').asDateTime := FieldByName('Vencimiento').asDateTime;
					TblSalida.Post;
					if IDRegistroOriginal = null then begin
						QueryExecute(DMConnections.BaseCAC, format('UPDATE HistorialInfractores SET FechaTramiteJudicial = GETDATE(), ' +
						  ' IDOperacionTramiteJudicial = %d WHERE Patente = ''%s'' AND NumeroHistorialInfraccion = %d',
						  [FIDRegistroOperacion, trim(FieldByName('Patente').asString), FieldByName('NumeroHistorialInfraccion').asInteger]));
					end;
				except
					on E: Exception do begin
						TblSalida.cancel;
						// Generamos el nuevo registro de operaciones
                        inc(CantidadErrores);
						if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION,
						  ModuloInterfaz, RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
						  'Error procesando el archivo resultado.', FIDRegistroOperacion, null, null, E.message, DescriError) = -1 then begin;
                            MensajeResultado := DescriError;
                            result := 2;
                            break;
                       end;
					end;
				end;
				if Assigned(ProgressBar) then
					ProgressBar.Position := ProgressBar.Position + 1;
				next;
			end;
			close;
		end;
	finally
		tblSalida.close;
        if result = 0 then
            if (CantidadErrores = 0) then
       			MensajeResultado := format('Proceso finalizado con �xito. Total de Infracciones: %d - ' +
    			  'Total de Patentes: %d', [CantidadInfracciones, CantidadPatentes])
            else begin
                result := 1;
    			MensajeResultado := format('Proceso finalizado con error. Total de Infracciones: %d - ' +
	    		  'Total de Patentes: %d - Infracciones que fallaron: %d.', [CantidadInfracciones, CantidadPatentes,
		    	  CantidadErrores])
            end;
		if assigned(ProgressBar) then
			ProgressBar.position := 0;
	end;
end;

function TDMInterfaces.EjecutarEntradaRegistroPatente(FileSource: AnsiString; ModuloInterfaz: Integer;
  ProgressBar: TProgressBAR; var MensajeResultado: AnsiString): Integer;
var	CantidadPatentes, CantidadErrores: Integer;
   	DescriError: array[0..255] of char;
begin
	CantidadErrores := 0;
    result := 0;
	with tblEntrada do begin
		try
			connectionString := 'Provider=MSDASQL.1;Persist Security Info=False;Extended Properties="DSN=Textos;' +
			  format('DBQ=%s;DefaultDir=;DriverId=27;FIL=text;MaxBufferSize=2048;PageTimeout=5;"',[ExtractFileDir(FileSource)]);
			TableName := StringReplace(ExtractFileName(FileSource),' ','_',[rfReplaceAll]);
			Open;
		except
			on E: Exception do begin
				MensajeResultado := format('Error intentado abrir archivo de entrada. Mensaje de error: %s', [E.message]);
				result := 2;
				exit;
			end;
		end;
		if assigned(ProgressBar) then begin
			ProgressBar.max := recordCount + 1;
			ProgressBar.position := 1;
		end;
		CantidadPatentes := recordCount;
		first;
		while not eof do begin
			if assigned(ProgressBar) then
				ProgressBar.position := ProgressBar.position + 1;
			try
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@Patente').Value :=
				  trim(FieldByName('Patente').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@Apellido').value :=
				  trim(FieldByName('Apellido').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@Nombre').value :=
				  trim(FieldByName('Nombre').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@CodigoDocumento').Value :=
				  trim(FieldByName('CodigoDocumento').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@NumeroDocumento').Value :=
				  trim(FieldByName('NumeroDocumento').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@Domicilio').value :=
				  trim(FieldByName('Domicilio').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@CodigoPostal').value :=
				  trim(FieldByName('CodigoPostal').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@Comuna').Value :=
				  trim(FieldByName('Comuna').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@Region').Value :=
				  trim(FieldByName('Region').asString);
				InterfazEntradaRegistroPatentes.Parameters.ParamByName('@Pais').Value :=
				  trim(FieldByName('Pais').asString);
				InterfazEntradaRegistroPatentes.execProc;
			except
				on E:Exception do begin
					// Creamos el registro de operaci�n
                    inc(CantidadErrores);
					if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION, ModuloInterfaz,
					  RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
					  'Error procesando el archivo de entrada.', FIDRegistroOperacion, null, null, E.Message, DescriError) = -1 then begin;
                        MensajeResultado := DescriError;
        				result := 2;
                        break;
                    end;
				end;
			end;
			next;
		end;
		close;
	end;
    if result = 0 then
        if (CantidadErrores = 0) then
            MensajeResultado := format('Proceso finalizado con �xito. Total de Patentes: %d',
	    	  [CantidadPatentes])
        else begin
            Result := 1;
    		MensajeResultado := format('Proceso finalizado con error. Total de Patentes: %d - ' +
	    	  'Patentes que fallaron: %d.',[CantidadPatentes, CantidadErrores])
	    end;
	if assigned(ProgressBar) then
		ProgressBar.position := 0;
end;


Function TDMInterfaces.EjecutarSalidaAgenciaCobranza(ModuloInterfaz: Integer; IDRegistroOriginal:Variant;
  ProgressBar: TProgressBar; var MensajeResultado: AnsiString): Integer;
var	DescriError: array[0..255] of char;
    FileName, DescRegion, DescComuna, Domicilio: AnsiString;
	F: TextFile;
	CodigoCuenta, CantidadCuentas, CantidadComprobantes, CantidadErrores: Integer;
begin
	CantidadComprobantes := 0;
	CantidadErrores := 0;
	CantidadCuentas := 0;
	CodigoCuenta := 0;
    result := 0;
	try
		FileName := format('\sal_%s.txt',[stringReplace(NE_AGENCIA_COBRANZA,' ','_',[rfReplaceAll])]);
		AssignFile(F, format('%s\%s',[FDirInterfacesSalida, FileName]));
		ReWrite(F);
		CloseFile(F);
		tblSalida.ConnectionString := 'Provider=MSDASQL.1;Persist Security Info=False;Extended Properties="DSN=Textos;' +
		  format('DBQ=%s;DefaultDir=;DriverId=27;FIL=text;MaxBufferSize=2048;PageTimeout=5;"',[FDirInterfacesSalida]);
		tblSalida.TableName := FileName;
		tblSalida.open;
	except
		on E: Exception do begin
			MensajeResultado := format('Error intentado abrir archivo de salida. Mensaje de error: %s', [E.message]);
			result := 2;
			exit;
		end;
	end;

	try
		// Obtenemos los comprobantes a debitar
		with InterfazSalidaAgenciaCobranza do begin
			Parameters.ParamByName('@IDRegistroOperacion').value := IDRegistroOriginal;
			Open;
			if Assigned(ProgressBar) then begin
				ProgressBar.Max := RecordCount + 1;
				ProgressBar.Position := 1;
			end;
			CantidadComprobantes := RecordCount;
			// Procesamos cada uno de los comprobantes a enviar
			while not eof do begin
				if CodigoCuenta <> FieldByName('CodigoCuenta').asInteger then begin
					inc(CantidadCuentas);
					CodigoCuenta := FieldByName('CodigoCuenta').asInteger;
				end;
                DescRegion := ''; DescComuna := ''; Domicilio := '';
                DescRegion := BuscarDescripcionRegion(DMConnections.BaseCAC,
                                        FieldByName('CodigoPais').Value,
                                        FieldByName('region').Value);
                DescComuna := BuscarDescripcionComuna(DMConnections.BaseCAC,
                                        FieldByName('CodigoPais').Value,
                                        FieldByName('region').Value,
                                        FieldByName('comuna').Value);
				try
					TblSalida.append;
					TblSalida.fieldByName('CodigoCuenta').asInteger := FieldByName('CodigoCuenta').asInteger;
					TblSalida.FieldByName('NumeroHistorialMora').asInteger := FieldByName('NumeroHistorialMora').asInteger;
					TblSalida.fieldByName('Importe').asFloat := FieldByName('Importe').asFloat;
					TblSalida.FieldByName('Apellido').asString := FieldByName('Apellido').asString;
                    TblSalida.FieldByName('ApellidoMaterno').asString := FieldByName('Apellido').asString;
					TblSalida.FieldByName('Nombre').asString := FieldByName('Nombre').asString;
					TblSalida.FieldByName('CodigoDocumento').asString := FieldByName('CodigoDocumento').asString;
					TblSalida.FieldByName('NumeroDocumento').asString := FieldByName('NumeroDocumento').asString;
                    Domicilio := ArmarDomicilioSimple(DMConnections.BaseCAC,FieldByName('Calle').AsString,
                            FieldByName('numero').Asinteger, FieldByName('piso').AsInteger, FieldByName('dpto').AsString,
                            FieldByName('detalle').AsString);
					TblSalida.FieldByName('Domicilio').asString := Domicilio;
					TblSalida.FieldByName('CodigoPostal').asString := FieldByName('CodigoPostal').asString;
					TblSalida.FieldByName('Comuna').asString := DescComuna;
					TblSalida.FieldByName('Region').asString := DescRegion;
					TblSalida.FieldByName('Pais').asString := FieldByName('Pais').asString;
					TblSalida.FieldByName('TipoComprobante').asString := FieldByName('TipoComprobante').asString;
					TblSalida.FieldByName('NumeroComprobante').asFloat := FieldByName('NumeroComprobante').asFloat;
					TblSalida.FieldByName('Fecha').asDateTime := FieldByName('Fecha').asDateTime;
					TblSalida.Post;
					if IDRegistroOriginal = null then begin
						QueryExecute(DMConnections.BaseCAC, format('UPDATE HistorialMorosos SET FechaEnvioAgencia = GETDATE(), ' +
						  ' EnviarAgencia = 0, IDOperacionEnvioAgencia = %d ' +
						  ' WHERE CodigoCuenta = %d AND TipoComprobante = ''%s'' AND NumeroComprobante = %.0f AND NumeroHistorialMora = %d',
						  [FIDRegistroOperacion, FieldByName('CodigoCuenta').asInteger, FieldByName('TipoCOmprobante').asString,
						   FieldByName('NumeroComprobante').asFloat, FieldByName('NumeroHistorialMora').asInteger]));
					end;
				except
					on E: Exception do begin
						TblSalida.cancel;
						// Generamos el nuevo registro de operaciones
                        inc(CantidadErrores);
						if CrearRegistroOperaciones(DMConnections.BaseCAC, SYS_FACTURACION, RO_MOD_SALIDA_AGENCIA_COBRANZA,
						  RO_AC_ERROR_EJECUTANDO_INTERFAZ, CS_ALTA, getMachineName, UsuarioSistema,
						  'Error procesando el archivo resultado.', FIDRegistroOperacion, null, null, E.message, descriError) = -1 then begin
                            result := 2;
                            MensajeResultado := DescriError;
                            break;
                        end;
					end;
				end;
				if Assigned(ProgressBar) then
					ProgressBar.Position := ProgressBar.Position + 1;
				next;
			end;
			close;
		end;
	finally
		tblSalida.close;
		if result = 0 then
            if (CantidadErrores = 0) then
   			    MensajeResultado := format('Proceso finalizado con �xito. Total de Comprobantes: %d - Total de Cuentas: %d',
    			  [CantidadCuentas, CantidadComprobantes])
            else begin
                result := 1;
    			MensajeResultado := format('Proceso finalizado con error. Total de Comprobantes: %d - Total de Cuentas: %d - Comprobantes que fallaron: %d.',
	    		  [CantidadCuentas, CantidadComprobantes, CantidadErrores])
		    end;
		if assigned(ProgressBar) then
			ProgressBar.position := 0;
	end;
end;



procedure TDMInterfaces.EjecutarInterfaz(FileSource: AnsiString; ModuloInterfaz: Integer; Reservado1, Reservado2, Reservado3: Variant;
  aProgressBar: TProgressBAR);
resourcestring
    MSG_FIN_INTERFAZ            = 'El proceso de la interfaz se completo con �xito.';
    MSG_ABORTAR_INTERFAZ        = 'El proceso de la interfaz fue abortado.';
    MSG_FIN_INTERFAZ_CON_ERROR  = 'El proceso de la interfaz finalizo con errores.';

var DescriError: ansiString;
    mensajeResultado: AnsiString;
    resultado: integer;

begin
    resultado := 0;
	FDirInterfacesSalida := goodDir(trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT DirArchivosInterfaces FROM Parametros')));

	// Generamos el registro de log
	FIDRegistroOperacion := CrearInterface(DMConnections.BaseCAC, ModuloInterfaz, Reservado1, reservado2, Reservado3);
	if FIDRegistroOperacion = -1 then exit;

	// Ejecutamos la interfaz
	case ModuloInterfaz of
		RO_MOD_ENTRADA_TARJETA_CREDITO:
			resultado := EjecutarEntradaDebitosAutomaticos(FileSource, RO_MOD_ENTRADA_TARJETA_CREDITO,
			  DA_TARJETA_CREDITO, Reservado1, aProgressBar, mensajeResultado);
		RO_MOD_SALIDA_TARJETA_CREDITO:
			resultado := EjecutarSalidaDebitosAutomaticos(RO_MOD_SALIDA_TARJETA_CREDITO, DA_TARJETA_CREDITO,
			  Reservado1, Reservado2, reservado3, aProgressBar, mensajeResultado);
		RO_MOD_ENTRADA_BANCO:
			resultado := EjecutarEntradaDebitosAutomaticos(FileSource, RO_MOD_ENTRADA_BANCO, DA_CUENTA_BANCARIA,
			  Reservado1, aProgressBar, mensajeResultado);
		RO_MOD_SALIDA_BANCO:
			resultado := EjecutarSalidaDebitosAutomaticos(RO_MOD_SALIDA_TARJETA_CREDITO, DA_CUENTA_BANCARIA,
			  Reservado1, Reservado2, reservado3, aProgressBar, mensajeResultado);
		RO_MOD_ENTRADA_JUZGADO_LOCAL:
			resultado := EjecutarEntradaJuzgado(FileSource, RO_MOD_ENTRADA_JUZGADO_LOCAL, aProgressBar,
			  mensajeResultado);
		RO_MOD_SALIDA_JUZGADO_LOCAL:
			resultado := EjecutarSalidaJuzgado(RO_MOD_SALIDA_JUZGADO_LOCAL, Reservado2, aProgressBar, mensajeResultado);
		RO_MOD_ENTRADA_REGISTRO_PATENTES:
			resultado := EjecutarEntradaRegistroPatente(FileSource, RO_MOD_ENTRADA_REGISTRO_PATENTES, aProgressBar,
			  MensajeResultado);
		RO_MOD_SALIDA_AGENCIA_COBRANZA:
			resultado := EjecutarSalidaAgenciaCobranza(RO_MOD_SALIDA_AGENCIA_COBRANZA, Reservado2, aProgressBar,
			  MensajeResultado);
	end;

    // Si la interfaz finalizo con errores mostramos el mensaje
    case resultado of
        0 : MsgBox(MSG_FIN_INTERFAZ, CAPTION_GENERAR_INTERFAZ, MB_ICONINFORMATION or MB_OK);
        1 : MsgBoxErr(MSG_FIN_INTERFAZ_CON_ERROR, MensajeResultado, CAPTION_GENERAR_INTERFAZ, MB_OK);
        2 : MsgBoxErr(MSG_ABORTAR_INTERFAZ, MensajeResultado, CAPTION_GENERAR_INTERFAZ, MB_OK);
    end;

	// Actualizamos el registro log
	if not ActualizarRegistroOperacion(DMConnections.BaseCAC, SYS_FACTURACION, FIDRegistroOperacion, CS_ALTA, (Resultado = 0),
	  mensajeResultado, Reservado1, Reservado2, Reservado3, DescriError) then
        MsgBoxErr(MSG_GENERAR_INTERFAZ, DescriError, CAPTION_GENERAR_INTERFAZ, MB_OK);
end;

end.
