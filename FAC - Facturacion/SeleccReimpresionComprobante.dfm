object frmSeleccReimpresion: TfrmSeleccReimpresion
  Left = 286
  Top = 281
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Reimpresi'#243'n de Comprobante'
  ClientHeight = 124
  ClientWidth = 353
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblTipoImpresion: TLabel
    Left = 15
    Top = 13
    Width = 321
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Reimpresi'#243'n de Comprobantes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblDescontarGratis: TLabel
    Left = 15
    Top = 37
    Width = 321
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Ser'#225' descontada de sus impresiones gratis'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblDetalle: TLabel
    Left = 15
    Top = 61
    Width = 321
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Cantidad de impresiones gratis :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btnImprimir: TButton
    Left = 3
    Top = 96
    Width = 113
    Height = 25
    Caption = '&Imprimir con Cobro'
    ModalResult = 1
    TabOrder = 0
  end
  object btnImprimirGratis: TButton
    Left = 118
    Top = 96
    Width = 113
    Height = 25
    Caption = 'Imprimir &Gratis'
    ModalResult = 4
    TabOrder = 1
    OnClick = btnImprimirGratisClick
  end
  object btnCancelar: TButton
    Left = 234
    Top = 96
    Width = 113
    Height = 25
    Caption = '&Cancelar'
    Default = True
    ModalResult = 2
    TabOrder = 2
  end
end
