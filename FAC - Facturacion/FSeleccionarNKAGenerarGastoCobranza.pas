unit FSeleccionarNKAGenerarGastoCobranza;

interface

uses
    Util, UtilProc, PeaProcs,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, ExtCtrls;

type
  TfrmSeleccionarNKAGenerarGastoCobranza = class(TForm)
    Label1: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    neNK: TNumericEdit;
    procedure FormActivate(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

var
  frmSeleccionarNKAGenerarGastoCobranza: TfrmSeleccionarNKAGenerarGastoCobranza;

implementation

{$R *.dfm}

{ TForm1 }

function TfrmSeleccionarNKAGenerarGastoCobranza.Inicializar(Titulo: AnsiString): boolean;
begin
    Caption := Titulo;
    Result := True;
end;

procedure TfrmSeleccionarNKAGenerarGastoCobranza.btnAceptarClick(Sender: TObject);
resourcestring
    ERROR_INVALID_NUMBER        = 'El valor ingresado no es v�lido para una Nota de Cobro';
begin
    if not ValidateControls( [neNK], [neNK.ValueInt > 0], Caption, [ERROR_INVALID_NUMBER]) then begin
        ModalResult := mrNOne;
        Exit;
    end;
end;


procedure TfrmSeleccionarNKAGenerarGastoCobranza.FormActivate(Sender: TObject);
begin
    neNK.SetFocus;
end;

end.
