object form_NotaDeCobroDirecta: Tform_NotaDeCobroDirecta
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Proceso de Facturaci'#243'n de Notas de Cobro Directas'
  ClientHeight = 429
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    628
    429)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 88
    Top = 137
    Width = 258
    Height = 13
    Caption = 'Fecha hasta la cual desea incluir pases diarios'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 90
    Top = 243
    Width = 214
    Height = 13
    Caption = 'Tipos de DayPass que  Desea Facturar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 88
    Top = 188
    Width = 274
    Height = 13
    Caption = 'Fecha de emisi'#243'n de la Nota de Cobro Electr'#243'nica'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 88
    Top = 20
    Width = 265
    Height = 13
    Caption = 'Cliente para emitir la Nota de Cobro Electr'#243'nica'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 90
    Top = 76
    Width = 79
    Height = 13
    Caption = 'Concesionaria'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 90
    Top = 325
    Width = 185
    Height = 13
    Caption = 'Concesionaria que recibe factura'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
    Visible = False
  end
  object deFechaHasta: TDateEdit
    Left = 88
    Top = 158
    Width = 121
    Height = 21
    AutoSelect = False
    TabOrder = 2
    Date = -693594.000000000000000000
  end
  object btnFacturar: TButton
    Left = 444
    Top = 396
    Width = 97
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Facturar'
    TabOrder = 6
    OnClick = btnFacturarClick
  end
  object btnCancelar: TButton
    Left = 545
    Top = 396
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    Default = True
    TabOrder = 7
    OnClick = btnCancelarClick
  end
  object deFechaEmision: TDateEdit
    Left = 88
    Top = 212
    Width = 121
    Height = 21
    AutoSelect = False
    TabOrder = 3
    Date = -693594.000000000000000000
  end
  object cbNombreClienteNQ: TVariantComboBox
    Left = 88
    Top = 40
    Width = 409
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  object rbtnPDU: TRadioButton
    Left = 96
    Top = 272
    Width = 333
    Height = 17
    Caption = 'PDU - Pases Diario Unificado'
    Checked = True
    TabOrder = 4
    TabStop = True
  end
  object rbtnBHTU: TRadioButton
    Left = 96
    Top = 295
    Width = 321
    Height = 17
    Caption = 'BHTU - Pases Diario Boleto Habilitaci'#243'n Tard'#237'a Unificado'
    TabOrder = 5
  end
  object cbConcesionaria: TVariantComboBox
    Left = 88
    Top = 101
    Width = 409
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items = <>
  end
  object cbConcesionariaFac: TVariantComboBox
    Left = 88
    Top = 350
    Width = 409
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 8
    Visible = False
    Items = <>
  end
  object spCrearNotaCobroNQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearNotaCobroNQ'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoClienteNQ'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoDayPass'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 591
    Top = 17
  end
  object ExisteComprobanteMasNuevo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ExisteComprobanteMasNuevo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdReturnValue
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@RutProveedorDayPass'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 591
    Top = 49
  end
end
