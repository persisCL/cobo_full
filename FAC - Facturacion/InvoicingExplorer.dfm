object frmInvoicing: TfrmInvoicing
  Left = 138
  Top = 131
  Caption = 'Explorador de Procesos de Facturaci'#243'n'
  ClientHeight = 476
  ClientWidth = 852
  Color = clBtnFace
  Constraints.MinHeight = 510
  Constraints.MinWidth = 860
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 852
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 8
      Width = 180
      Height = 13
      Caption = 'Seleccione un proceso de facturaci'#243'n'
    end
    object Label2: TLabel
      Left = 296
      Top = 28
      Width = 292
      Height = 13
      Caption = 'Si desea emitir el reporte resumen de este proceso haga click '
    end
    object Label3: TLabel
      Left = 592
      Top = 28
      Width = 18
      Height = 13
      Caption = 'ac'#225
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = Label3Click
      OnMouseEnter = Label3MouseEnter
      OnMouseLeave = Label3MouseLeave
    end
    object BuscaTabEdit1: TBuscaTabEdit
      Left = 24
      Top = 24
      Width = 265
      Height = 21
      Enabled = True
      MaxLength = 100
      ReadOnly = True
      TabOrder = 0
      EditorStyle = bteTextEdit
      BuscaTabla = btProcesosFacturacion
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 57
    Width = 852
    Height = 419
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Comprobantes'
      object Panel2: TPanel
        Left = 0
        Top = 326
        Width = 844
        Height = 65
        Align = alBottom
        ParentColor = True
        TabOrder = 0
        DesignSize = (
          844
          65)
        object Panel3: TPanel
          Left = 2
          Top = 2
          Width = 256
          Height = 59
          Anchors = [akTop]
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object Bevel1: TBevel
            Left = 8
            Top = 16
            Width = 241
            Height = 41
          end
          object Label4: TLabel
            Left = 23
            Top = 24
            Width = 35
            Height = 13
            Caption = 'Primera'
          end
          object Label5: TLabel
            Left = 116
            Top = 24
            Width = 29
            Height = 13
            Caption = #218'ltima'
          end
          object Label6: TLabel
            Left = 194
            Top = 24
            Width = 42
            Height = 13
            Caption = 'Cantidad'
          end
          object lblPrimeraNC: TLabel
            Left = 16
            Top = 40
            Width = 49
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'lblPrimeraNC'
          end
          object lblUltimaNC: TLabel
            Left = 104
            Top = 40
            Width = 49
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'Label14'
          end
          object lblCantidadNC: TLabel
            Left = 192
            Top = 40
            Width = 49
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'Label14'
          end
          object Label7: TLabel
            Left = 59
            Top = 2
            Width = 122
            Height = 13
            Alignment = taCenter
            Caption = ' Notas de Cobro Emitidas '
          end
        end
        object Panel4: TPanel
          Left = 254
          Top = 2
          Width = 585
          Height = 59
          Anchors = [akTop, akRight]
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object Bevel2: TBevel
            Left = 8
            Top = 16
            Width = 569
            Height = 42
          end
          object Label8: TLabel
            Left = 487
            Top = 24
            Width = 79
            Height = 13
            Alignment = taRightJustify
            Caption = 'Total A Pagar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label9: TLabel
            Left = 16
            Top = 24
            Width = 88
            Height = 13
            Alignment = taRightJustify
            Caption = 'Peajes del Periodo'
          end
          object Label10: TLabel
            Left = 414
            Top = 24
            Width = 43
            Height = 13
            Alignment = taRightJustify
            Caption = 'Intereses'
          end
          object Label11: TLabel
            Left = 141
            Top = 24
            Width = 88
            Height = 13
            Alignment = taRightJustify
            Caption = 'Peajes de Per.Ant.'
          end
          object Label12: TLabel
            Left = 260
            Top = 24
            Width = 98
            Height = 13
            Alignment = taRightJustify
            Caption = 'Facturacion Vencida'
          end
          object lblPeajesPer: TLabel
            Left = 15
            Top = 40
            Width = 89
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Label14'
          end
          object lblPeajesAnt: TLabel
            Left = 141
            Top = 40
            Width = 89
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Label14'
          end
          object lblFactVencida: TLabel
            Left = 267
            Top = 40
            Width = 89
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Label14'
          end
          object lblIntereses: TLabel
            Left = 373
            Top = 40
            Width = 85
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Label14'
          end
          object lblTotalAPagar: TLabel
            Left = 482
            Top = 40
            Width = 84
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Label14'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label13: TLabel
            Left = 280
            Top = 2
            Width = 41
            Height = 13
            Alignment = taCenter
            Caption = ' Totales '
          end
        end
      end
      object DBListEx1: TDBListEx
        Left = 0
        Top = 33
        Width = 844
        Height = 293
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 75
            Header.Caption = 'NC Nro.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx1Columns5HeaderClick
            FieldName = 'NumeroComprobante'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx1Columns5HeaderClick
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 75
            Header.Caption = 'RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx1Columns5HeaderClick
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 350
            Header.Caption = 'Nombre del Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx1Columns5HeaderClick
            FieldName = 'Nombre'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 110
            Header.Caption = 'Total Nota de Cobro'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx1Columns5HeaderClick
            FieldName = 'desc_TotalComprobante'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 110
            Header.Caption = 'Total A Pagar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx1Columns5HeaderClick
            FieldName = 'desc_TotalAPagar'
          end>
        DataSource = dsComprobantesFacturados
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 844
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label14: TLabel
          Left = 526
          Top = 8
          Width = 184
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cantidad de Comprobantes a Mostrar   '
        end
        object neCantidadComprobantes: TNumericEdit
          Left = 707
          Top = 4
          Width = 57
          Height = 21
          TabOrder = 0
          Value = 100.000000000000000000
        end
        object btnActualizarCO: TButton
          Left = 765
          Top = 4
          Width = 68
          Height = 21
          Caption = 'Actualizar'
          TabOrder = 1
          OnClick = btnActualizarCOClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Convenios Facturados'
      ImageIndex = 1
      object Label18: TLabel
        Left = 176
        Top = 104
        Width = 69
        Height = 13
        Caption = 'NombreCliente'
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 844
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label15: TLabel
          Left = 544
          Top = 8
          Width = 166
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cantidad de Convenios a Mostrar   '
        end
        object Label16: TLabel
          Left = 8
          Top = 8
          Width = 154
          Height = 13
          Caption = 'Total de Convenios Facturados: '
        end
        object Label17: TLabel
          Left = 288
          Top = 8
          Width = 146
          Height = 13
          Caption = 'Total de Convenios a Facturar:'
        end
        object lblConveniosFacturados: TLabel
          Left = 168
          Top = 8
          Width = 113
          Height = 13
          Caption = 'lblConveniosFacturados'
        end
        object lblConveniosAFacturar: TLabel
          Left = 440
          Top = 8
          Width = 106
          Height = 13
          Caption = 'lblConveniosAFacturar'
        end
        object neCantidadConveniosFacturados: TNumericEdit
          Left = 707
          Top = 4
          Width = 57
          Height = 21
          TabOrder = 0
          Value = 100.000000000000000000
        end
        object btnActualizarCF: TButton
          Left = 765
          Top = 4
          Width = 68
          Height = 21
          Caption = 'Actualizar'
          TabOrder = 1
          OnClick = btnActualizarCOClick
        end
      end
      object DBListEx2: TDBListEx
        Left = 0
        Top = 33
        Width = 844
        Height = 358
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'N'#250'mero de Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx2Columns0HeaderClick
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx2Columns0HeaderClick
            FieldName = 'NombreCliente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 120
            Header.Caption = 'Fecha de Alta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx2Columns0HeaderClick
            FieldName = 'FechaAlta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx2Columns0HeaderClick
            FieldName = 'desc_EstadoConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx2Columns0HeaderClick
            FieldName = 'desc_Concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Pago Autom'#225'tico'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx2Columns0HeaderClick
            FieldName = 'desc_MedioPago'
          end>
        DataSource = dsConvenios
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Convenios No Facturados'
      ImageIndex = 2
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 844
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label19: TLabel
          Left = 544
          Top = 8
          Width = 166
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cantidad de Convenios a Mostrar   '
        end
        object Label20: TLabel
          Left = 8
          Top = 8
          Width = 173
          Height = 13
          Caption = 'Total de Convenios NO Facturados: '
        end
        object Label21: TLabel
          Left = 288
          Top = 8
          Width = 146
          Height = 13
          Caption = 'Total de Convenios a Facturar:'
        end
        object lblConveniosNOFacturados: TLabel
          Left = 182
          Top = 8
          Width = 100
          Height = 13
          AutoSize = False
          Caption = 'lblConveniosFacturados'
        end
        object lblConveniosAFacturar2: TLabel
          Left = 440
          Top = 8
          Width = 90
          Height = 13
          AutoSize = False
          Caption = 'lblConveniosAFacturar'
        end
        object neCantidadConveniosNoFacturados: TNumericEdit
          Left = 707
          Top = 4
          Width = 57
          Height = 21
          TabOrder = 0
          Value = 100.000000000000000000
        end
        object btnActualizarNF: TButton
          Left = 765
          Top = 4
          Width = 68
          Height = 21
          Caption = 'Actualizar'
          TabOrder = 1
          OnClick = btnActualizarCOClick
        end
      end
      object DBListEx3: TDBListEx
        Left = 0
        Top = 33
        Width = 844
        Height = 358
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'N'#250'mero de Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx3Columns0HeaderClick
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx3Columns0HeaderClick
            FieldName = 'NombreCliente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 120
            Header.Caption = 'Fecha de Alta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx3Columns0HeaderClick
            FieldName = 'FechaAlta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx3Columns0HeaderClick
            FieldName = 'desc_EstadoConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx3Columns0HeaderClick
            FieldName = 'desc_Concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Pago Autom'#225'tico'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx3Columns0HeaderClick
            FieldName = 'desc_MedioPago'
          end>
        DataSource = dsConveniosNOFacturados
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Movimientos de Cuenta'
      ImageIndex = 3
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 844
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label22: TLabel
          Left = 535
          Top = 8
          Width = 175
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cantidad de Movimientos a Mostrar   '
        end
        object Label27: TLabel
          Left = 8
          Top = 8
          Width = 183
          Height = 13
          Caption = 'Cantidad de Movimientos Generados:  '
        end
        object lblCantmov: TLabel
          Left = 200
          Top = 8
          Width = 52
          Height = 13
          Caption = 'lblCantmov'
        end
        object neMovimientos: TNumericEdit
          Left = 707
          Top = 4
          Width = 57
          Height = 21
          TabOrder = 0
          Value = 100.000000000000000000
        end
        object btnActualizarMO: TButton
          Left = 765
          Top = 4
          Width = 68
          Height = 21
          Caption = 'Actualizar'
          TabOrder = 1
          OnClick = btnActualizarCOClick
        end
      end
      object DBListEx4: TDBListEx
        Left = 0
        Top = 33
        Width = 844
        Height = 293
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 75
            Header.Caption = 'Nro. Mov.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            OnHeaderClick = DBListEx4Columns0HeaderClick
            FieldName = 'NumeroMovimiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 120
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx4Columns0HeaderClick
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 120
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx4Columns0HeaderClick
            FieldName = 'FechaHora'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Concepto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx4Columns0HeaderClick
            FieldName = 'ConceptoMovimiento'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Importe'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = DBListEx4Columns0HeaderClick
            FieldName = 'Importe'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 200
            Header.Caption = 'Observaciones'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx4Columns0HeaderClick
            FieldName = 'Observaciones'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 45
            Header.Caption = 'Fact.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = DBListEx4Columns0HeaderClick
            FieldName = 'Facturado'
          end>
        DataSource = dsMovimientos
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
      object Panel9: TPanel
        Left = 0
        Top = 326
        Width = 844
        Height = 65
        Align = alBottom
        TabOrder = 2
        DesignSize = (
          844
          65)
        object TPanel
          Left = 152
          Top = 3
          Width = 689
          Height = 60
          Anchors = [akRight, akBottom]
          BevelOuter = bvNone
          TabOrder = 0
          object Bevel3: TBevel
            Left = 8
            Top = 16
            Width = 673
            Height = 41
          end
          object Label23: TLabel
            Left = 240
            Top = 2
            Width = 177
            Height = 13
            Caption = ' Importes Totales de los Movimientos '
          end
          object Label24: TLabel
            Left = 16
            Top = 24
            Width = 139
            Height = 13
            Caption = 'Importe Total de Movimientos'
          end
          object Label25: TLabel
            Left = 232
            Top = 24
            Width = 195
            Height = 13
            Caption = 'Importe Total de Movimientos Facturados'
          end
          object Label26: TLabel
            Left = 464
            Top = 24
            Width = 212
            Height = 13
            Caption = 'Importe Total de Movimientos No Facturados'
          end
          object lblTotalMov: TLabel
            Left = 16
            Top = 40
            Width = 137
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'lblTotalMov'
          end
          object lblMovFact: TLabel
            Left = 232
            Top = 39
            Width = 201
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'Label27'
          end
          object lblMovNOFact: TLabel
            Left = 464
            Top = 39
            Width = 209
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = 'Label27'
          end
        end
      end
    end
  end
  object btProcesosFacturacion: TBuscaTabla
    Caption = 'Procesos de Facturaci'#243'n'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = spProcesosFacturacion
    OnSelect = btProcesosFacturacionSelect
    Left = 296
  end
  object dsProcesosFacturacion: TDataSource
    DataSet = spProcesosFacturacion
    Left = 368
  end
  object spProcesosFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 120
    ProcedureName = 'ListarProcesosFacturacion'
    Parameters = <>
    Left = 336
    object spProcesosFacturacionNumeroProcesoFacturacion: TAutoIncField
      FieldName = 'NumeroProcesoFacturacion'
      ReadOnly = True
    end
    object spProcesosFacturacionFechaHoraInicio: TDateTimeField
      FieldName = 'FechaHoraInicio'
    end
    object spProcesosFacturacionOperador: TStringField
      FieldName = 'Operador'
      FixedChar = True
    end
    object spProcesosFacturacionFechaHoraFin: TDateTimeField
      FieldName = 'FechaHoraFin'
    end
  end
  object spReporte: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerReporteFacturacionMasiva'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NUMPROCFACT'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 408
    object spReportePrimeraNotaCobro: TLargeintField
      FieldName = 'PrimeraNotaCobro'
      ReadOnly = True
    end
    object spReporteUltimaNotaCobro: TLargeintField
      FieldName = 'UltimaNotaCobro'
      ReadOnly = True
    end
    object spReporteCantidadNotasCobro: TLargeintField
      FieldName = 'CantidadNotasCobro'
      ReadOnly = True
    end
    object spReporteTotalAPagar: TBCDField
      FieldName = 'TotalAPagar'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object spReportedesc_TotalAPagar: TStringField
      FieldName = 'desc_TotalAPagar'
      ReadOnly = True
    end
    object spReporteTotalPeajesPeriodo: TBCDField
      FieldName = 'TotalPeajesPeriodo'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object spReportedesc_TotalPeajesPeriodo: TStringField
      FieldName = 'desc_TotalPeajesPeriodo'
      ReadOnly = True
    end
    object spReporteTotalPeajesPerAnterior: TBCDField
      FieldName = 'TotalPeajesPerAnterior'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object spReportedesc_TotalPeajesPerAnterior: TStringField
      FieldName = 'desc_TotalPeajesPerAnterior'
      ReadOnly = True
    end
    object spReporteTotalIntereses: TBCDField
      FieldName = 'TotalIntereses'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object spReportedesc_TotalIntereses: TStringField
      FieldName = 'desc_TotalIntereses'
      ReadOnly = True
    end
    object spReporteTotalFacturacionVencida: TBCDField
      FieldName = 'TotalFacturacionVencida'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object spReportedesc_TotalFacturacionVencida: TStringField
      FieldName = 'desc_TotalFacturacionVencida'
      ReadOnly = True
    end
    object spReporteCodigoUsuario: TStringField
      FieldName = 'CodigoUsuario'
      ReadOnly = True
    end
    object spReporteNombreUsuario: TStringField
      FieldName = 'NombreUsuario'
      ReadOnly = True
      Size = 100
    end
    object spReporteNumeroProcesoFacturacion: TIntegerField
      FieldName = 'NumeroProcesoFacturacion'
      ReadOnly = True
    end
  end
  object dsReporte: TDataSource
    DataSet = spReporte
    Left = 440
  end
  object spObtenerComprobantesFacturados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 180
    ProcedureName = 'ObtenerComprobantesFacturados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumProcFact'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 24
    Top = 152
    object spObtenerComprobantesFacturadosTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object spObtenerComprobantesFacturadosNumeroComprobante: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object spObtenerComprobantesFacturadosCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object spObtenerComprobantesFacturadosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object spObtenerComprobantesFacturadosNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object spObtenerComprobantesFacturadosNombre: TStringField
      FieldName = 'Nombre'
      ReadOnly = True
      Size = 153
    end
    object spObtenerComprobantesFacturadosTotalAPagar: TLargeintField
      FieldName = 'TotalAPagar'
      ReadOnly = True
    end
    object spObtenerComprobantesFacturadosdesc_TotalComprobante: TStringField
      FieldName = 'desc_TotalComprobante'
      ReadOnly = True
    end
    object spObtenerComprobantesFacturadosdesc_TotalAPagar: TStringField
      FieldName = 'desc_TotalAPagar'
      ReadOnly = True
    end
    object spObtenerComprobantesFacturadosTotalComprobante: TLargeintField
      FieldName = 'TotalComprobante'
      ReadOnly = True
    end
  end
  object dsComprobantesFacturados: TDataSource
    DataSet = spObtenerComprobantesFacturados
    Left = 56
    Top = 152
  end
  object spObtenerConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 180
    ProcedureName = 'ObtenerConveniosFacturadosPorProceso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 24
    Top = 192
    object spObtenerConvenioscodigoconvenio: TIntegerField
      FieldName = 'codigoconvenio'
    end
    object spObtenerConveniosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 19
    end
    object spObtenerConveniosNombreCliente: TStringField
      FieldName = 'NombreCliente'
      Size = 100
    end
    object spObtenerConveniosCodigoCliente: TIntegerField
      FieldName = 'CodigoCliente'
    end
    object spObtenerConveniosCodigoEstadoConvenio: TWordField
      FieldName = 'CodigoEstadoConvenio'
    end
    object spObtenerConveniosCodigoConcesionaria: TWordField
      FieldName = 'CodigoConcesionaria'
    end
    object spObtenerConveniosFechaAlta: TDateTimeField
      FieldName = 'FechaAlta'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object spObtenerConveniosTipoMedioPagoAutomatico: TWordField
      FieldName = 'TipoMedioPagoAutomatico'
    end
    object spObtenerConveniosdesc_Concesionaria: TStringField
      FieldName = 'desc_Concesionaria'
      FixedChar = True
      Size = 50
    end
    object spObtenerConveniosdesc_EstadoConvenio: TStringField
      FieldName = 'desc_EstadoConvenio'
      Size = 50
    end
    object spObtenerConveniosdesc_MedioPago: TStringField
      FieldName = 'desc_MedioPago'
      Size = 30
    end
  end
  object dsConvenios: TDataSource
    DataSet = spObtenerConvenios
    Left = 56
    Top = 192
  end
  object spConveniosNOFacturados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 180
    ProcedureName = 'ObtenerConveniosNOFacturadosPorProceso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 24
    Top = 232
    object IntegerField1: TIntegerField
      FieldName = 'codigoconvenio'
    end
    object StringField1: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 19
    end
    object StringField2: TStringField
      FieldName = 'NombreCliente'
      Size = 100
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodigoCliente'
    end
    object WordField1: TWordField
      FieldName = 'CodigoEstadoConvenio'
    end
    object WordField2: TWordField
      FieldName = 'CodigoConcesionaria'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'FechaAlta'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object WordField3: TWordField
      FieldName = 'TipoMedioPagoAutomatico'
    end
    object StringField3: TStringField
      FieldName = 'desc_Concesionaria'
      FixedChar = True
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'desc_EstadoConvenio'
      Size = 50
    end
    object StringField5: TStringField
      FieldName = 'desc_MedioPago'
      Size = 30
    end
  end
  object dsConveniosNOFacturados: TDataSource
    DataSet = spConveniosNOFacturados
    Left = 56
    Top = 232
  end
  object spMovimientos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 180
    ProcedureName = 'ObtenerMovimientosPorProceso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@TotalMovimientos'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 30
        Value = Null
      end
      item
        Name = '@TotalFacturados'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 30
        Value = Null
      end
      item
        Name = '@TotalNoFacturados'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 30
        Value = Null
      end>
    Left = 24
    Top = 272
    object spMovimientoscodigoconvenio: TIntegerField
      FieldName = 'codigoconvenio'
    end
    object spMovimientosNumeroMovimiento: TAutoIncField
      FieldName = 'NumeroMovimiento'
      ReadOnly = True
    end
    object spMovimientosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object spMovimientosIndiceVehiculo: TIntegerField
      FieldName = 'IndiceVehiculo'
    end
    object spMovimientosCodigoCliente: TIntegerField
      FieldName = 'CodigoCliente'
    end
    object spMovimientosNombreCliente: TStringField
      FieldName = 'NombreCliente'
      ReadOnly = True
      Size = 100
    end
    object spMovimientosFechaHora: TDateTimeField
      FieldName = 'FechaHora'
    end
    object spMovimientosConceptoMovimiento: TStringField
      FieldName = 'ConceptoMovimiento'
      FixedChar = True
      Size = 50
    end
    object spMovimientosObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 60
    end
    object spMovimientosImporte: TStringField
      FieldName = 'Importe'
      ReadOnly = True
    end
    object spMovimientosFacturado: TStringField
      FieldName = 'Facturado'
      ReadOnly = True
      Size = 2
    end
  end
  object dsMovimientos: TDataSource
    DataSet = spMovimientos
    Left = 56
    Top = 272
  end
end
