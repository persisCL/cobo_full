object RechazarCartaListaAmarillaForm: TRechazarCartaListaAmarillaForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Rechazar Cartas'
  ClientHeight = 194
  ClientWidth = 411
  Color = clBtnFace
  Constraints.MaxHeight = 222
  Constraints.MaxWidth = 417
  Constraints.MinHeight = 222
  Constraints.MinWidth = 417
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object bvlFondo: TBevel
    Left = 8
    Top = 6
    Width = 393
    Height = 158
  end
  object lblMotivoDevolucion: TLabel
    Left = 27
    Top = 49
    Width = 93
    Height = 13
    Caption = 'Motivo del Rechazo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblObservacion: TLabel
    Left = 27
    Top = 73
    Width = 60
    Height = 13
    Caption = 'Observaci'#243'n'
  end
  object lblDatoCarta: TLabel
    Left = 12
    Top = 10
    Width = 385
    Height = 32
    Align = alCustom
    Alignment = taCenter
    Caption = 'lblDatoCarta'
    Constraints.MaxHeight = 32
    Constraints.MaxWidth = 385
    Constraints.MinHeight = 32
    Constraints.MinWidth = 385
    WordWrap = True
  end
  object btnGrabar: TButton
    Left = 211
    Top = 166
    Width = 75
    Height = 25
    Caption = 'Grabar'
    TabOrder = 2
    OnClick = btnGrabarClick
  end
  object btnSalir: TButton
    Left = 313
    Top = 166
    Width = 75
    Height = 25
    Caption = 'Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object cboMotivoRechazo: TVariantComboBox
    Left = 162
    Top = 46
    Width = 231
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 0
    OnChange = cboMotivoRechazoChange
    Items = <>
  end
  object txtObservacion: TMemo
    Left = 162
    Top = 73
    Width = 231
    Height = 85
    Color = 16444382
    MaxLength = 500
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object spRechazarCartaListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RechazarCartaListaAmarilla'
    Parameters = <>
    Left = 16
    Top = 164
  end
  object spObtenerMotivosDevolucionComprobantesCorreo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMotivosDevolucionComprobantesCorreo'
    Parameters = <>
    Left = 48
    Top = 165
  end
  object qryObtenerIDCarta: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'IDConvenioInhabilitado'
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoConvenio'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT TOP 1 IDHistoricoCartasListaAmarilla, CodigoMotivoDevoluc' +
        'ion '
      
        'FROM HistoricoCartasListaAmarilla WITH(NOLOCK, INDEX = IX_Histor' +
        'icoCartasListaAmarilla_IDCodConvMotivo)'
      'WHERE'
      #9'IDConvenioInhabilitado = :IDConvenioInhabilitado AND'
      #9'CodigoConvenio = :CodigoConvenio'
      'ORDER BY FechaCreacion DESC')
    Left = 80
    Top = 165
  end
end
