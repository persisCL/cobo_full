{-----------------------------------------------------------------------------
 File Name      : frmCambiarEstadoListaAmarilla
 Author         : Claudio Quezada Ib��ez
 Date Created   : 20-Febrero-2013
 Language       : ES-CL
 Firma          : SS_660_CQU_20121010
 Description    : Graba el hist�rico de env�o de cartas por Lista Amarilla

 Author         : Claudio Quezada Ib��ez
 Date Created   : 20-Julio-2013
 Language       : ES-CL
 Firma          : SS_660_CQU_20130711
 Description    : Se modifica el t�tulo de la ventana y algunos mensajes
                  de "Hist�rico de Cartas" por "Rechazo de Cartas"
                  Se ajusta el tipo de letra de un Label
-----------------------------------------------------------------------------}
unit frmHistoricoCartasListaAmarilla;

interface

uses
    constParametrosGenerales, Windows, Messages, SysUtils, Variants, Classes,
    Graphics, Controls, Forms, Dialogs, StdCtrls, Validate, DateEdit, ListBoxEx,
    DBListEx, ExtCtrls, DmConnection, DB, ADODB, VariantComboBox, UtilProc,
    Util, PeaProcs, DmiCtrls, SysUtilsCN, PeaTypes;

type
  THistoricoCartasListaAmarillaForm = class(TForm)
    btnGrabar: TButton;
    btnSalir: TButton;
    bvlFondo: TBevel;
    spGuardarHistoricoCartasListaAmarilla: TADOStoredProc;
    cboEmpresaCorreo: TVariantComboBox;
    lblEmpresaCorreo: TLabel;
    //lblMotivoDevolucion : TLabel;             // SS_660_CQU_20130604
    //cboMotivoDevolucion: TVariantComboBox;    // SS_660_CQU_20130604
    lblFechaEnvio: TLabel;
    deFechaEnvio: TDateEdit;
    //lblObservacion: TLabel;                   // SS_660_CQU_20130604
    //txtObservacion: TMemo;                    // SS_660_CQU_20130604
    procedure btnSalirClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
  private
    { Private declarations }
    FIDConvenioInhabilitado,
    FCodigoConvenio         :   Integer;
    FFechaEnvioAnterior     :   TDateTime;

  public
    { Public declarations }
    function Inicializar(IDConvenioInhabilitado, CodigoConvenio : integer; FechaAnterior : TDateTime) : boolean;
    function Validaciones : boolean;
    procedure LlenaCombos;
  end;

var
  HistoricoCartasListaAmarillaForm: THistoricoCartasListaAmarillaForm;

implementation

{$R *.dfm}

function THistoricoCartasListaAmarillaForm.Inicializar;
resourcestring
    //MSG_ERROR_INICIALIZA    =   'No se puede cargar la ventana de Hist�rico de Cartas';     // SS_660_CQU_20130711
    MSG_ERROR_INICIALIZA    =   'No se puede cargar la ventana para el Reenv�o de Cartas';    // SS_660_CQU_20130711
begin
    try
        CenterForm(Self);
        FIDConvenioInhabilitado :=  IDConvenioInhabilitado;
        FFechaEnvioAnterior     :=  FechaAnterior;
        FCodigoConvenio         :=  CodigoConvenio;
        LlenaCombos();
        Result := True;
    except
        on e:exception do begin
            MsgBoxErr(MSG_ERROR_INICIALIZA, e.Message, Caption, MB_ICONERROR);
            Result := False;
        end;
    end;
end;

function THistoricoCartasListaAmarillaForm.Validaciones;
resourcestring
    //MSG_GRABAR              =   'Problema al grabar el hist�rico';   // SS_660_CQU_20130711
    MSG_GRABAR              =   'Problema al Reenviar Carta';          // SS_660_CQU_20130711
    MSG_FALTA_FECHA         =   'Debe ingresar una fecha';
    MSG_FECHA_MALA          =   'La fecha de env�o elegida debe ser superior a la fecha de env�o anterior (%s)';
    //MSG_FALTA_OBSERVACION   =   'Debe ingresar una observaci�n';                                                                      // SS_660_CQU_20130604
begin
    Result := False;

    if not ValidateControls(
            //[deFechaEnvio, deFechaEnvio, txtObservacion],                                                                             // SS_660_CQU_20130604
            [deFechaEnvio, deFechaEnvio],                                                                                               // SS_660_CQU_20130604
            [deFechaEnvio.Date <> NullDate,
            deFechaEnvio.Date >= FFechaEnvioAnterior],                                                                                  // SS_660_CQU_20130604
            //Trim(txtObservacion.Text) <> ''],                                                                                         // SS_660_CQU_20130604
            MSG_GRABAR,
            //[MSG_FALTA_FECHA, Format(MSG_FECHA_MALA, [FormatDateTime('dd/mm/yyyy', FFechaEnvioAnterior)]), MSG_FALTA_OBSERVACION])    // SS_660_CQU_20130604
            [MSG_FALTA_FECHA, Format(MSG_FECHA_MALA, [FormatDateTime('dd/mm/yyyy', FFechaEnvioAnterior)])])                             // SS_660_CQU_20130604
    then Exit
    else Result := True;
end;

procedure THistoricoCartasListaAmarillaForm.LlenaCombos;
resourcestring
    MSG_CARGAR_EMPRESAS =   'Error al cargar las empresas de correo, mensaje del sistema: ';
    //MSG_CARGAR_MOTIVOS  =   'Error al cargar los motivos de devoluci�n de correspondencia, mensaje del sistema: ';   // SS_660_CQU_20130604
var
    SP: TAdoStoredProc;
begin
    try
        // Creo el SP
        SP := TADOStoredProc.Create(nil);
        SP.Connection := DMConnections.BaseCAC;
        try
            // Obtener Empresas de Correo
            SP.ProcedureName := 'ObtenerEmpresaCorreo';
            SP.Open;
            while not SP.Eof do begin
                cboEmpresaCorreo.Items.Add( SP.FieldByName('Nombre').AsString,
                                            SP.FieldByName('CodigoEmpresaCorreo').AsInteger);
                SP.Next;
            end;
            SP.Close;
            cboEmpresaCorreo.ItemIndex := 0;
        except
            on e:exception do begin
                raise Exception.Create(MSG_CARGAR_EMPRESAS + e.Message);
            end;
        end;

        {try                                                                                            // SS_660_CQU_20130604
            // Obtener Motivos de Devolucion                                                            // SS_660_CQU_20130604
            SP.ProcedureName := 'ObtenerMotivosDevolucionComprobantesCorreo';                           // SS_660_CQU_20130604
            SP.Open;                                                                                    // SS_660_CQU_20130604
            while not SP.Eof do begin                                                                   // SS_660_CQU_20130604
                cboMotivoDevolucion .Items.Add( SP.FieldByName('Detalle').AsString,                     // SS_660_CQU_20130604
                                                SP.FieldByName('CodigoMotivoDevolucion').AsInteger);    // SS_660_CQU_20130604
                SP.Next;                                                                                // SS_660_CQU_20130604
            end;                                                                                        // SS_660_CQU_20130604
            SP.Close;                                                                                   // SS_660_CQU_20130604
            cboMotivoDevolucion.ItemIndex := 0;                                                         // SS_660_CQU_20130604
        except                                                                                          // SS_660_CQU_20130604
            on e:exception do begin                                                                     // SS_660_CQU_20130604
                raise Exception.Create(MSG_CARGAR_MOTIVOS + e.Message);                                 // SS_660_CQU_20130604
            end;                                                                                        // SS_660_CQU_20130604
        end;}                                                                                           // SS_660_CQU_20130604
    finally
        FreeAndNil(SP);
    end;

end;

procedure THistoricoCartasListaAmarillaForm.btnGrabarClick(Sender: TObject);
resourcestring
    //MSG_EXITO           = 'El Hist�rico ha sido grabado exitosamente';                  // SS_660_CQU_20130711
    MSG_EXITO           = 'La carta ha sido reenviada correctamente';                     // SS_660_CQU_20130711
    //MSG_ERROR_SQL       = 'Ocurri� un error al intentar grabar el Hist�rico de Cartas'; // SS_660_CQU_20130711
    MSG_ERROR_SQL       = 'Ocurri� un error al intentar reenviar la Carta';               // SS_660_CQU_20130711
begin
    if not Validaciones() then Exit;

    with spGuardarHistoricoCartasListaAmarilla do begin
        Parameters.Refresh;
        Parameters.ParamByName('@IDConvenioInhabilitado').Value :=  FIDConvenioInhabilitado;
        Parameters.ParamByName('@CodigoConvenio').Value         :=  FCodigoConvenio;
        //Parameters.ParamByName('@CodigoMotivoDevolucion').Value :=  cboMotivoDevolucion.Value;    // SS_660_CQU_20130604
        Parameters.ParamByName('@CodigoMotivoDevolucion').Value :=  null;                           // SS_660_CQU_20130604
        Parameters.ParamByName('@CodigoEmpresaCorreo').Value    :=  cboEmpresaCorreo.Value;
        Parameters.ParamByName('@FechaEnvio').Value             :=  deFechaEnvio.Date;
        //Parameters.ParamByName('@Observacion').Value            :=  Trim(txtObservacion.Text);    // SS_660_CQU_20130604
        Parameters.ParamByName('@Observacion').Value            :=  'Re-Env�o de Carta';            // SS_660_CQU_20130604        
		Parameters.ParamByName('@UsuarioCreacion').Value        :=  UsuarioSistema;

        try
            DMConnections.BaseCAC.BeginTrans;
            ExecProc;

            DMConnections.BaseCAC.CommitTrans;
            MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
            ModalResult := mrOk;
        except on e:exception do begin
                if DMConnections.BaseCAC.InTransaction then begin
                    DMConnections.BaseCAC.RollbackTrans;
                end;

                MsgBoxErr(MSG_ERROR_SQL, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    end;
end;

procedure THistoricoCartasListaAmarillaForm.btnSalirClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

end.
