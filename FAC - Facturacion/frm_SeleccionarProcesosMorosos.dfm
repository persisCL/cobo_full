object form_SeleccionarProcesosMorosos: Tform_SeleccionarProcesosMorosos
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Selecci'#243'n de Procesos de Facturaci'#243'n por Morosidad'
  ClientHeight = 433
  ClientWidth = 394
  Color = clBtnFace
  Constraints.MaxHeight = 471
  Constraints.MaxWidth = 410
  Constraints.MinHeight = 471
  Constraints.MinWidth = 410
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    394
    433)
  PixelsPerInch = 96
  TextHeight = 13
  object lblSeleccione: TLabel
    Left = 12
    Top = 77
    Width = 329
    Height = 13
    Caption = 
      'Seleccione el N'#250'mero de Proceso de Facturaci'#243'n que desea procesa' +
      'r'
  end
  object dblNotasCobroInfraccion: TDBListEx
    Left = 8
    Top = 96
    Width = 378
    Height = 299
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 134
        MinWidth = 134
        MaxWidth = 200
        Header.Caption = 'Nro. Proceso Facturaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'NumeroProcesoFacturacion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'Fecha Inicio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'FechaHoraInicio'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'Fecha T'#233'rmino'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'FechaHoraFin'
      end>
    DataSource = dsObtenerProcesosFacturacionMorosos
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDblClick = dblNotasCobroInfraccionDblClick
    OnDrawText = dblNotasCobroInfraccionDrawText
  end
  object btnCerrar: TButton
    Left = 253
    Top = 401
    Width = 131
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cerrar'
    TabOrder = 1
    OnClick = btnCerrarClick
  end
  object grpFiltro: TGroupBox
    Left = 8
    Top = 5
    Width = 377
    Height = 65
    Caption = 'Filtrar Resultados'
    TabOrder = 2
    DesignSize = (
      377
      65)
    object lbl_TransitosDesde: TLabel
      Left = 7
      Top = 18
      Width = 34
      Height = 13
      Caption = 'Desde:'
    end
    object lbl_TransitosHasta: TLabel
      Left = 104
      Top = 18
      Width = 32
      Height = 13
      Caption = 'Hasta:'
    end
    object lblProcesoFacturacion: TLabel
      Left = 200
      Top = 18
      Width = 71
      Height = 13
      Caption = 'N'#176' Facturaci'#243'n'
    end
    object deFechaDesde: TDateEdit
      Left = 7
      Top = 36
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object deFechaHasta: TDateEdit
      Left = 102
      Top = 36
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object btnBuscar: TButton
      Left = 311
      Top = 32
      Width = 60
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Buscar'
      TabOrder = 2
      OnClick = btnBuscarClick
    end
    object txtNumeroProceso: TNumericEdit
      Left = 197
      Top = 36
      Width = 108
      Height = 21
      Color = clWhite
      TabOrder = 3
    end
  end
  object spObtenerProcesosFacturacionMorosos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'ObtenerProcesosFacturacionMorosos;1'
    Parameters = <>
    Left = 20
    Top = 132
  end
  object dsObtenerProcesosFacturacionMorosos: TDataSource
    DataSet = spObtenerProcesosFacturacionMorosos
    Left = 56
    Top = 131
  end
end
