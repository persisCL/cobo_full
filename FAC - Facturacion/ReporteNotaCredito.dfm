object formReporteNotaCredito: TformReporteNotaCredito
  Left = 346
  Top = 250
  AutoScroll = False
  Caption = 'ReporteNotaCredito'
  ClientHeight = 222
  ClientWidth = 202
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object qryObtenerTarjetaCredito: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoConvenio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoTipoTarjetaCredito'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct EmisoresTarjetasCredito.Descripcion, ConvenioMed' +
        'ioPagoAutomaticoPAT.NumeroTarjetaCredito'
      '  from ConvenioMedioPagoAutomaticoPAT, EmisoresTarjetasCredito'
      
        ' where ConvenioMedioPagoAutomaticoPAT.CodigoConvenio = :CodigoCo' +
        'nvenio'
      
        '   and ConvenioMedioPagoAutomaticoPAT.CodigoTipoTarjetaCredito =' +
        ' :CodigoTipoTarjetaCredito'
      
        '   and ConvenioMedioPagoAutomaticoPAT.CodigoEmisorTarjetaCredito' +
        ' = EmisoresTarjetasCredito.CodigoEmisorTarjetaCredito')
    Left = 168
    Top = 72
  end
  object qryObtenerCuentaBancaria: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoConvenio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoBanco'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct Bancos.Descripcion, ConvenioMedioPagoAutomaticoP' +
        'AC.NroCuentaBancaria'
      '  from ConvenioMedioPagoAutomaticoPAC, Bancos'
      
        ' where ConvenioMedioPagoAutomaticoPAC.CodigoConvenio = :CodigoCo' +
        'nvenio'
      '   and ConvenioMedioPagoAutomaticoPAC.CodigoBanco = :CodigoBanco'
      
        '   and ConvenioMedioPagoAutomaticoPAC.CodigoBanco = Bancos.Codig' +
        'oBanco'
      '   '
      '')
    Left = 136
    Top = 72
  end
  object ObtenerDatosNotaCredito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosNotaCredito'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 40
    object ObtenerDatosNotaCreditoCodigoTipoMedioPago: TWordField
      FieldName = 'CodigoTipoMedioPago'
    end
    object ObtenerDatosNotaCreditoNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 30
    end
    object ObtenerDatosNotaCreditoNumeroConvenioFormateado: TStringField
      FieldName = 'NumeroConvenioFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object ObtenerDatosNotaCreditoPAC_CodigoBanco: TIntegerField
      FieldName = 'PAC_CodigoBanco'
    end
    object ObtenerDatosNotaCreditoPAC_CodigoTipoCuentaBancaria: TWordField
      FieldName = 'PAC_CodigoTipoCuentaBancaria'
    end
    object ObtenerDatosNotaCreditoPAC_NroCuentaBancaria: TStringField
      FieldName = 'PAC_NroCuentaBancaria'
      Size = 50
    end
    object ObtenerDatosNotaCreditoPAC_Sucursal: TStringField
      FieldName = 'PAC_Sucursal'
      Size = 50
    end
    object ObtenerDatosNotaCreditoPAT_CodigoTipoTarjetaCredito: TWordField
      FieldName = 'PAT_CodigoTipoTarjetaCredito'
    end
    object ObtenerDatosNotaCreditoPAT_NumeroTarjetaCredito: TStringField
      FieldName = 'PAT_NumeroTarjetaCredito'
    end
    object ObtenerDatosNotaCreditoPAT_FechaVencimiento: TStringField
      FieldName = 'PAT_FechaVencimiento'
      Size = 5
    end
    object ObtenerDatosNotaCreditoPAT_CodigoEmisorTarjetaCredito: TWordField
      FieldName = 'PAT_CodigoEmisorTarjetaCredito'
    end
    object ObtenerDatosNotaCreditoTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 1
    end
    object ObtenerDatosNotaCreditoNumeroComprobante: TBCDField
      FieldName = 'NumeroComprobante'
      Precision = 18
      Size = 0
    end
    object ObtenerDatosNotaCreditoFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object ObtenerDatosNotaCreditoVencimiento: TDateTimeField
      FieldName = 'Vencimiento'
    end
    object ObtenerDatosNotaCreditoConcesionaria: TStringField
      FieldName = 'Concesionaria'
      ReadOnly = True
      FixedChar = True
      Size = 50
    end
    object ObtenerDatosNotaCreditoCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object ObtenerDatosNotaCreditoNumeroMovimiento: TAutoIncField
      FieldName = 'NumeroMovimiento'
      ReadOnly = True
    end
    object ObtenerDatosNotaCreditoIndiceVehiculo: TIntegerField
      FieldName = 'IndiceVehiculo'
    end
    object ObtenerDatosNotaCreditoCodigoConcepto: TWordField
      FieldName = 'CodigoConcepto'
    end
    object ObtenerDatosNotaCreditoDescripcion: TStringField
      FieldName = 'Descripcion'
      ReadOnly = True
      FixedChar = True
      Size = 60
    end
    object ObtenerDatosNotaCreditoImporte: TBCDField
      FieldName = 'Importe'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
    object ObtenerDatosNotaCreditoImporteTotal: TBCDField
      FieldName = 'ImporteTotal'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
    object ObtenerDatosNotaCreditoDescriImporte: TStringField
      FieldName = 'DescriImporte'
      ReadOnly = True
    end
    object ObtenerDatosNotaCreditoPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
  end
  object dsDatosCliente: TDataSource
    DataSet = ObtenerDatosCliente
    Left = 104
    Top = 8
  end
  object dsDatosComprobantes: TDataSource
    DataSet = ObtenerDatosNotaCredito
    Left = 104
    Top = 40
  end
  object dsCuentaDebito: TDataSource
    Left = 104
    Top = 72
  end
  object ppCuentaDebito: TppDBPipeline
    DataSource = dsCuentaDebito
    OpenDataSource = False
    UserName = 'CuentaDebito'
    Left = 72
    Top = 72
  end
  object ActualizarComprobanteImpreso: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = 0d
      end
      item
        Name = 'TipoComprobante'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = ''
      end
      item
        Name = 'NumeroComprobante'
        Attributes = [paSigned]
        DataType = ftBCD
        Precision = 18
        Size = 19
        Value = 0c
      end>
    SQL.Strings = (
      'UPDATE '
      #9'Comprobantes'
      'SET '
      #9'Imprimir = 0, '
      #9'FechaHoraImpreso = :FechaHora'
      'WHERE '
      #9'TipoComprobante = :TipoComprobante'
      #9'AND NumeroComprobante = :NumeroComprobante'
      '')
    Left = 168
    Top = 40
  end
  object rbi_NotaCredito: TRBInterface
    Report = rp_NotaCredito
    Caption = 'Impresi'#243'n de Nota de Cr'#233'dito'
    OrderIndex = 0
    Devices = [rbidPrinter, rbidPreview]
    Left = 8
    Top = 40
  end
  object ppDatosComprobantes: TppDBPipeline
    DataSource = dsDatosComprobantes
    OpenDataSource = False
    UserName = 'DatosComprobantes'
    Left = 72
    Top = 40
    object ppDatosComprobantesppField1: TppField
      FieldAlias = 'CodigoTipoMedioPago'
      FieldName = 'CodigoTipoMedioPago'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField2: TppField
      FieldAlias = 'NumeroConvenio'
      FieldName = 'NumeroConvenio'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField3: TppField
      FieldAlias = 'NumeroConvenioFormateado'
      FieldName = 'NumeroConvenioFormateado'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField4: TppField
      FieldAlias = 'PAC_CodigoBanco'
      FieldName = 'PAC_CodigoBanco'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField5: TppField
      FieldAlias = 'PAC_CodigoTipoCuentaBancaria'
      FieldName = 'PAC_CodigoTipoCuentaBancaria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField6: TppField
      FieldAlias = 'PAC_NroCuentaBancaria'
      FieldName = 'PAC_NroCuentaBancaria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField7: TppField
      FieldAlias = 'PAC_Sucursal'
      FieldName = 'PAC_Sucursal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField8: TppField
      FieldAlias = 'PAT_CodigoTipoTarjetaCredito'
      FieldName = 'PAT_CodigoTipoTarjetaCredito'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField9: TppField
      FieldAlias = 'PAT_NumeroTarjetaCredito'
      FieldName = 'PAT_NumeroTarjetaCredito'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField10: TppField
      FieldAlias = 'PAT_FechaVencimiento'
      FieldName = 'PAT_FechaVencimiento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField11: TppField
      FieldAlias = 'PAT_CodigoEmisorTarjetaCredito'
      FieldName = 'PAT_CodigoEmisorTarjetaCredito'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField12: TppField
      FieldAlias = 'TipoComprobante'
      FieldName = 'TipoComprobante'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 11
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField13: TppField
      FieldAlias = 'NumeroComprobante'
      FieldName = 'NumeroComprobante'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 12
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField14: TppField
      FieldAlias = 'Fecha'
      FieldName = 'Fecha'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 13
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField15: TppField
      FieldAlias = 'Vencimiento'
      FieldName = 'Vencimiento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 14
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField16: TppField
      FieldAlias = 'Concesionaria'
      FieldName = 'Concesionaria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 15
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField17: TppField
      FieldAlias = 'CodigoConvenio'
      FieldName = 'CodigoConvenio'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 16
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField18: TppField
      FieldAlias = 'NumeroMovimiento'
      FieldName = 'NumeroMovimiento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 17
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField19: TppField
      FieldAlias = 'IndiceVehiculo'
      FieldName = 'IndiceVehiculo'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 18
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField20: TppField
      FieldAlias = 'CodigoConcepto'
      FieldName = 'CodigoConcepto'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 19
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField21: TppField
      FieldAlias = 'Descripcion'
      FieldName = 'Descripcion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 20
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField22: TppField
      FieldAlias = 'Importe'
      FieldName = 'Importe'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 21
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField23: TppField
      FieldAlias = 'ImporteTotal'
      FieldName = 'ImporteTotal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 22
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField24: TppField
      FieldAlias = 'DescriImporte'
      FieldName = 'DescriImporte'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 23
      Searchable = False
      Sortable = False
    end
    object ppDatosComprobantesppField25: TppField
      FieldAlias = 'Patente'
      FieldName = 'Patente'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 24
      Searchable = False
      Sortable = False
    end
  end
  object ppDatosCliente: TppDBPipeline
    DataSource = dsDatosCliente
    OpenDataSource = False
    UserName = 'DatosCliente'
    Left = 72
    Top = 8
    object ppDatosClienteppField1: TppField
      FieldAlias = 'CodigoPersona'
      FieldName = 'CodigoPersona'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField2: TppField
      FieldAlias = 'CodigoDocumento'
      FieldName = 'CodigoDocumento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField3: TppField
      FieldAlias = 'NumeroDocumento'
      FieldName = 'NumeroDocumento'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField4: TppField
      FieldAlias = 'Personeria'
      FieldName = 'Personeria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField5: TppField
      FieldAlias = 'PersoneriaDescripcion'
      FieldName = 'PersoneriaDescripcion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField6: TppField
      FieldAlias = 'ApellidoNombre'
      FieldName = 'ApellidoNombre'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField7: TppField
      FieldAlias = 'Nombre'
      FieldName = 'Nombre'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField8: TppField
      FieldAlias = 'DescripcionDomicilio'
      FieldName = 'DescripcionDomicilio'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 7
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField9: TppField
      FieldAlias = 'DescripcionComuna'
      FieldName = 'DescripcionComuna'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 8
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField10: TppField
      FieldAlias = 'DescripcionRegion'
      FieldName = 'DescripcionRegion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 9
      Searchable = False
      Sortable = False
    end
    object ppDatosClienteppField11: TppField
      FieldAlias = 'CodigoPostal'
      FieldName = 'CodigoPostal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 10
      Searchable = False
      Sortable = False
    end
  end
  object ObtenerDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 8
  end
  object rp_NotaCredito: TppReport
    AutoStop = False
    DataPipeline = ppDatosComprobantes
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297127
    PrinterSetup.mmPaperWidth = 210079
    PrinterSetup.PaperSize = 9
    Template.FileName = 'c:\temp\aaa.rtm'
    DeviceType = 'Screen'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    Left = 40
    Top = 41
    Version = '9.02'
    mmColumnWidth = 0
    DataPipelineName = 'ppDatosComprobantes'
    object ppHeaderBand2: TppHeaderBand
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 97367
      mmPrintPosition = 0
      object ppShape23: TppShape
        UserName = 'Shape23'
        Brush.Color = 4365353
        Shape = stRoundRect
        mmHeight = 6350
        mmLeft = 2117
        mmTop = 91017
        mmWidth = 131234
        BandType = 0
      end
      object ppShape17: TppShape
        UserName = 'Shape17'
        Brush.Color = 4365353
        Shape = stRoundRect
        mmHeight = 6350
        mmLeft = 2116
        mmTop = 62971
        mmWidth = 131234
        BandType = 0
      end
      object ppShape18: TppShape
        UserName = 'Shape18'
        Brush.Color = 16441741
        Shape = stRoundRect
        mmHeight = 6350
        mmLeft = 2117
        mmTop = 79111
        mmWidth = 131234
        BandType = 0
      end
      object ppsbCargos: TppSubReport
        UserName = 'sbCargos'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ParentWidth = False
        TraverseAllData = False
        DataPipelineName = 'ppCargosComprobante'
        mmHeight = 6615
        mmLeft = 0
        mmTop = 70644
        mmWidth = 132821
        BandType = 0
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        object ppChildReport1: TppChildReport
          AutoStop = False
          DataPipeline = ppCargosComprobante
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.mmMarginBottom = 0
          PrinterSetup.mmMarginLeft = 0
          PrinterSetup.mmMarginRight = 0
          PrinterSetup.mmMarginTop = 0
          PrinterSetup.mmPaperHeight = 297127
          PrinterSetup.mmPaperWidth = 210079
          PrinterSetup.PaperSize = 9
          Version = '9.02'
          mmColumnWidth = 0
          DataPipelineName = 'ppCargosComprobante'
          object ppTitleBand1: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppDetailBand3: TppDetailBand
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 3969
            mmPrintPosition = 0
            object ppDBText30: TppDBText
              UserName = 'DBText30'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'Descripcion'
              DataPipeline = ppCargosComprobante
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppCargosComprobante'
              mmHeight = 3969
              mmLeft = 6085
              mmTop = 0
              mmWidth = 100277
              BandType = 4
            end
            object ppDBText32: TppDBText
              UserName = 'DBText32'
              OnGetText = ppDBText28GetText
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'Importe'
              DataPipeline = ppCargosComprobante
              DisplayFormat = '#######0,;(#######0,)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'ppCargosComprobante'
              mmHeight = 3969
              mmLeft = 106363
              mmTop = 0
              mmWidth = 20902
              BandType = 4
            end
          end
          object ppSummaryBand3: TppSummaryBand
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object raCodeModule1: TraCodeModule
            ProgramStream = {00}
          end
        end
      end
      object ppShape33: TppShape
        UserName = 'Shape33'
        Brush.Color = 16441741
        Shape = stRoundRect
        mmHeight = 6615
        mmLeft = 61648
        mmTop = 16140
        mmWidth = 75936
        BandType = 0
      end
      object ppLabel34: TppLabel
        UserName = 'Label101'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Importe Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 62971
        mmTop = 17463
        mmWidth = 28840
        BandType = 0
      end
      object ppShape21: TppShape
        UserName = 'Shape2'
        Brush.Color = 16441741
        Shape = stRoundRect
        mmHeight = 6615
        mmLeft = 61648
        mmTop = 8996
        mmWidth = 75936
        BandType = 0
      end
      object ppDBText18: TppDBText
        UserName = 'DBText14'
        OnGetText = ppDBText18GetText
        AutoSize = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'DescripcionCalle'
        DataPipeline = ppDomicilioFacturacion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppDomicilioFacturacion'
        mmHeight = 4022
        mmLeft = 5821
        mmTop = 45773
        mmWidth = 17949
        BandType = 0
      end
      object ppLabel25: TppLabel
        UserName = 'Label11'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'CONCESIONARIA COSTANERA NORTE S.A.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3260
        mmLeft = 3704
        mmTop = 2117
        mmWidth = 55563
        BandType = 0
      end
      object ppLabel26: TppLabel
        UserName = 'Label13'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'RUT: 96.920.010 - 4'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3260
        mmLeft = 3704
        mmTop = 5821
        mmWidth = 55563
        BandType = 0
      end
      object ppLabel27: TppLabel
        UserName = 'Label14'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'General Prieto 1430'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3260
        mmLeft = 3704
        mmTop = 9525
        mmWidth = 55563
        BandType = 0
      end
      object ppLabel29: TppLabel
        UserName = 'Label10'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'CONCEPTOS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4191
        mmLeft = 3969
        mmTop = 64029
        mmWidth = 24342
        BandType = 0
      end
      object ppDBText20: TppDBText
        UserName = 'DBText5'
        AutoSize = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Nombre'
        DataPipeline = ppDatosCliente
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppDatosCliente'
        mmHeight = 4022
        mmLeft = 5821
        mmTop = 41010
        mmWidth = 12573
        BandType = 0
      end
      object ppDBText21: TppDBText
        UserName = 'DBText7'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'NumeroConvenioFormateado'
        DataPipeline = ppDatosComprobantes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDatosComprobantes'
        mmHeight = 4498
        mmLeft = 99219
        mmTop = 10054
        mmWidth = 37306
        BandType = 0
      end
      object ppLabel31: TppLabel
        UserName = 'Label19'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'N'#250'mero de Convenio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 62971
        mmTop = 10319
        mmWidth = 35983
        BandType = 0
      end
      object ppDBText25: TppDBText
        UserName = 'DBText4'
        AutoSize = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'DescriComuna'
        DataPipeline = ppDomicilioFacturacion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppDomicilioFacturacion'
        mmHeight = 4022
        mmLeft = 5821
        mmTop = 51329
        mmWidth = 23326
        BandType = 0
      end
      object ppDBText26: TppDBText
        UserName = 'DBText8'
        AutoSize = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'DescriRegion'
        DataPipeline = ppDomicilioFacturacion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppDomicilioFacturacion'
        mmHeight = 4022
        mmLeft = 5821
        mmTop = 56621
        mmWidth = 21082
        BandType = 0
      end
      object ppShape34: TppShape
        UserName = 'Shape34'
        Brush.Color = 16441741
        Shape = stRoundRect
        mmHeight = 6615
        mmLeft = 61648
        mmTop = 1852
        mmWidth = 75936
        BandType = 0
      end
      object ppLabel47: TppLabel
        UserName = 'Label103'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'DETALLE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4191
        mmLeft = 3440
        mmTop = 92075
        mmWidth = 47096
        BandType = 0
      end
      object ppLabel39: TppLabel
        UserName = 'Label7'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Importe Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 5027
        mmTop = 80169
        mmWidth = 29104
        BandType = 0
      end
      object ppLabel42: TppLabel
        UserName = 'Label42'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Nota de Cr'#233'dito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 62971
        mmTop = 3440
        mmWidth = 28840
        BandType = 0
      end
      object ppDBText29: TppDBText
        UserName = 'DBText29'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'NumeroComprobante'
        DataPipeline = ppDatosComprobantes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDatosComprobantes'
        mmHeight = 4233
        mmLeft = 99749
        mmTop = 3440
        mmWidth = 36777
        BandType = 0
      end
      object ppImage3: TppImage
        UserName = 'Image3'
        Center = False
        MaintainAspectRatio = False
        Stretch = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Picture.Data = {
          07544269746D617076D10000424D76D100000000000036000000280000004A01
          000036000000010018000000000040D10000120B0000120B0000000000000000
          0000FFFFFFFFFFFFFFFFFFFFFFFFE5F3E887C79551AF66299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C4251AF6687C795E5F3E8FFFFFFFFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFF87C795299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C4287C795FFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFF6CBB7D299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C426CBB7DFFFFFFFFFFFF
          0000FFFFFF87C795299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C4287C795FFFFFF
          0000E5F3E8299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42E5F3E8
          000087C795299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4287C795
          000051AF66299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4251AF66
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C4250AE65E4F3E7FFFFFFE4F3E75EB47136A2
          4DAEDAB8FFFFFFF2F9F3AEDAB836A24D5EB471D7ECDBFFFFFFF2F9F3AEDAB836
          A24D299C4251AF66FFFFFF94CDA044A85AFFFFFFC9E6D0299C425EB471FFFFFF
          94CDA0A1D4ACFFFFFF36A24D94CDA0FFFFFF86C79436A24EFFFFFFFFFFFFFFFF
          FFF2F9F3299C42FFFFFFC9E6D0299C42E4F3E7FFFFFF81C590FFFFFF86C79429
          9C4294CDA0FFFFFF5EB471299C42299C42299C42C9E6D0E4F3E7299C42BCE0C4
          FFFFFF5EB471299C4294CDA0F2F9F3FFFFFFBCE0C450AE65299C42AEDAB8FFFF
          FF36A24DA1D4ACFFFFFF79C188299C4294CDA0FFFFFF50AE65299C425EB471FF
          FFFFFFFFFFFFFFFFD7ECDB299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4243A859
          D7ECDBFFFFFFE4F3E76BBB7D299C4250AE65BCE0C4FFFFFFFFFFFFD7ECDBAEDA
          B843A859299C42BCE0C4C9E6D050AE65299C4236A24DAEDAB8F2F9F3FFFFFFE4
          F3E7BCE0C45EB471299C42299C42299C42BCE0C4C9E6D094CDA0299C42299C42
          299C425EB471C9E6D0A1D4AC299C42299C42AEDAB8FFFFFFD7ECDB5EB47179C1
          88C9E6D079C188299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42E4F3E7FFFFFFA2D4ADAFDAB96BBB7D86C7
          94FFFFFFBDE0C4A2D4ADFFFFFFC9E6D036A24EF2F9F3AEDAB8A2D4ADFFFFFFD7
          ECDB299C42299C42FFFFFFC9E6D0299C42AEDAB8FFFFFFA1D4ACAEDAB8FFFFFF
          94CDA079C188FFFFFF5EB471D7ECDBFFFFFFBCE0C4299C42E4F3E7FFFFFF94CE
          A194CEA1299C42C9E6D0F2F9F336A24DFFFFFFD7ECDB299C42E4F3E7E4F3E794
          CDA0C9E6D0FFFFFF5EB471299C42299C42299C4294CDA0FFFFFF43A859F2F9F3
          FFFFFF94CDA050AE65FFFFFFE5F3E894CEA1F2F9F4F2F9F343A85986C794FFFF
          FF5EB471C9E6D0FFFFFF44A85A299C426BBB7DFFFFFF79C188299C42299C42FF
          FFFFE4F3E794CEA194CEA1299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4286C794
          FFFFFFFFFFFFCAE7D0A1D4AC43A859D7ECDBFFFFFFE4F3E7CAE7D0CAE7D0F2F9
          F35EB471299C42C9E6D0FFFFFF86C794299C4286C794FFFFFFF2F9F3CAE7D0CA
          E7D0E4F3E794CDA0299C42299C4236A24DFFFFFFFFFFFFFFFFFF50AE65299C42
          299C4251AF66FFFFFFF2F9F3299C425EB471FFFFFFFFFFFFD8EDDCD8EDDC85C7
          94FFFFFFC9E6D0299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42FFFFFFFFFFFF299C42299C42299C4294CD
          A0FFFFFF5FB571299C42CAE7D0FFFFFF5EB471299C42299C426BBB7DF2F9F3FF
          FFFF50AE65299C42C9E6D0F2F9F3299C4244A85AFFFFFFF2F9F4E5F3E8FFFFFF
          79C18851AF66FFFFFFAED9B7FFFFFFFFFFFFE4F3E7299C42BCE0C4FFFFFF299C
          42299C42299C42A1D4ACFFFFFFA1D4ACFFFFFF94CDA0299C4279C188FFFFFFE5
          F3E8F2F9F4FFFFFF44A85A299C42299C42299C426BBB7DFFFFFF93CD9FFFFFFF
          FFFFFFBCE0C45EB471FFFFFFBDE0C4299C427AC189FFFFFFAEDAB85EB471FFFF
          FFAEDAB8F2F9F3E4F3E7299C42299C4244A85AFFFFFFAEDAB8299C42299C42C9
          E6D0F2F9F3299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4279C188
          FFFFFFC9E6D0299C42299C42299C42FFFFFFFFFFFF5EB471299C42299C42299C
          4236A24E299C4294CDA0FFFFFFAEDAB8299C42C9E6D0FFFFFF94CDA0299C4229
          9C42299C4244A85A44A85A299C425EB471FFFFFFFEFFFEFFFFFFC9E6D0299C42
          299C42299C42FFFFFFFFFFFF43A85986C794FFFFFFD8EDDC299C427AC189E4F3
          E7FFFFFFE4F3E7299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42E4F3E7FFFFFF36A24D299C42299C4294CD
          A0FFFFFF87C795299C4287C795FFFFFF94CDA043A859E4F3E7FFFFFFFFFFFFF2
          F9F344A85A299C42A1D4ACFFFFFF50AE65299C42AEDAB8FFFFFF87C795E5F3E8
          5EB471299C42F2F9F3D7ECDBFFFFFFF1F8F2FFFFFF36A24D94CDA0FFFFFFFFFF
          FFFFFFFF299C426BBB7DFFFFFFFFFFFFFFFFFFBCE0C450AE65299C42E4F3E7D7
          ECDBCAE7D0FFFFFF299C42299C42299C42299C4244A85AFFFFFFFFFFFFFFFFFF
          FEFFFEE4F3E751AF66FFFFFFCAE7D0299C4236A24EFFFFFFF2F9F3299C42FFFF
          FFFFFFFFFFFFFFE4F3E76BBB7D299C42299C42F2F9F3D7ECDB299C42299C42A1
          D4ACFFFFFFFFFFFFD7ECDB299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4251AF66
          FFFFFFF2F9F3299C42299C42299C42FFFFFFFFFFFFAEDAB886C7945EB471299C
          42299C42299C426BBB7DFFFFFFC9E6D0299C42BCE0C4FFFFFFC9E6D094CDA05E
          B47136A24D299C42299C42299C4286C794FFFFFFC9E6D0E4F3E7FFFFFF6BBB7D
          299C42299C42C9E6D0FFFFFF6BBB7D5EB471FFFFFFD7ECDB299C42299C42BDE0
          C4FFFFFFFFFFFF36A24D299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42A1D4ACFFFFFF79C188299C42299C4251AF
          66FFFFFFC9E6D0299C425FB571FFFFFFC9E6D086C794FFFFFFF2F9F387C79544
          A85A299C42299C4279C188FFFFFF79C188299C4244A85AFFFFFF87C79587C795
          5EB471299C42C9E6D0FFFFFFFFFFFFA1D4ACFFFFFF5EB4715EB471FFFFFFC9E6
          D094CEA1299C4244A85AFFFFFFBDE0C487C795FFFFFFBCE0C4299C4279C188FF
          FFFF7AC189BDE0C4299C42299C42299C42299C42299C42E4F3E7FFFFFFFFFFFF
          94CDA0FFFFFF36A24DE4F3E7FFFFFF36A24E299C42FFFFFFFFFFFF299C42D7EC
          DBFFFFFF5FB571E5F3E8FFFFFF43A859299C42C9E6D0FFFFFF299C42299C4279
          C188FFFFFFBCE0C494CEA1299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          FFFFFFFFFFFF43A859299C42299C42C9E6D0FFFFFFFFFFFFFFFFFFFFFFFFF2F9
          F379C188299C4244A85AFFFFFFFFFFFF299C4294CDA0FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFA1D4AC36A24D299C42A1D4ACFFFFFF94CDA079C188FFFFFFE4F3E7
          299C42299C42A1D4ACFFFFFF94CDA044A85AFFFFFFFFFFFF36A24E299C4244A8
          5AFFFFFFFFFFFF5EB471299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C4244A85AF2F9F3F2F9F36BBB7D79C188299C
          42BCE0C4FFFFFF86C79494CDA0FFFFFF94CDA044A85AFFFFFFE4F3E779C188AE
          DAB850AE6579C188AEDAB8FFFFFFC9E6D094CDA043A859AEDAB8FFFFFFFFFFFF
          36A24E299C4294CDA0FFFFFFE4F3E751AF66FFFFFF94CDA0299C42FFFFFFD7EC
          DB94CDA079C188299C42F2F9F3E4F3E794CDA0FFFFFFF2F9F3299C42299C42D7
          ECDBFFFFFFD7ECDB299C42299C42299C42299C42299C42BCE0C4FFFFFFBCE0C4
          6BBB7DFFFFFF5EB4716CBB7DFFFFFFBCE0C46BBB7DFFFFFFF2F9F3299C42AEDA
          B8FFFFFF94CDA0E4F3E7FFFFFF5EB47194CDA0C9E6D0FFFFFFAEDAB886C79451
          AF66FFFFFFC9E6D094CDA06BBB7D299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          C9E6D0FFFFFF6BBB7D299C42299C4286C794FFFFFFBCE0C4299C425FB571E5F3
          E8FFFFFF5EB471299C42F2F9F3FFFFFF50AE6544A85AFFFFFFF2F9F336A24E44
          A85ABDE0C4FFFFFFA1D4AC299C42C9E6D0FFFFFF86C794299C42E4F3E7FFFFFF
          86C794299C4279C188FFFFFFC9E6D0299C42C9E6D0FFFFFF94CEA1299C42299C
          42BDE0C4FFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C4251AF66E4F3E7FFFFFFFFFFFF86C7
          9436A24EAFDAB9FFFFFFFFFFFFE4F3E744A85A299C426CBB7DE4F3E7FFFFFFFF
          FFFFAEDAB894CDA0FFFFFFFFFFFFFFFFFFFFFFFF86C79436A24EF2F9F3FFFFFF
          299C42299C426BBB7DFFFFFFA1D4AC299C42F2F9F3BCE0C4299C42D7ECDBFFFF
          FFFFFFFFFFFFFF299C42C9E6D0FFFFFFFFFFFFF2F9F394CEA1299C42299C425F
          B571FFFFFFC9E6D0299C42299C42299C42299C42299C4294CDA0FFFFFF86C794
          44A85AFFFFFF94CDA0299C427AC189E4F3E7FFFFFFFFFFFF7AC189299C4286C7
          94FFFFFFFFFFFFFFFFFFC9E6D036A24EE4F3E7FFFFFFFFFFFFFFFFFFFFFFFF43
          A859FFFFFFFFFFFFFFFFFFE4F3E7299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C425EB471
          BCE0C4FFFFFFAEDAB85EB47143A85936A24ED7ECDBFFFFFF86C79436A24EA2D4
          ADFFFFFF94CDA0299C42C9E6D0FFFFFF79C188299C42A1D4ACFFFFFFBCE0C444
          A85A94CDA0FFFFFFC9E6D0299C42FFFFFFFFFFFF5EB471299C425FB571FFFFFF
          F2F9F336A24D51AF66FFFFFFF2F9F3299C4251AF66F2F9F3FFFFFF94CDA05EB4
          71AEDAB8FFFFFFBCE0C4299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42D7ECDB
          FFFFFFFFFFFFFFFFFFFFFFFFC9E6D0299C4236A24ED7ECDBFFFFFFFFFFFFFFFF
          FFF2F9F351AF66299C4294CDA0FFFFFFA1D4AC299C42299C42A2D4ADFFFFFFFF
          FFFFFFFFFFFFFFFF94CDA043A859FFFFFFFFFFFF44A85A299C42299C42C9E6D0
          FFFFFFA1D4AC299C42FFFFFFFFFFFF43A859299C4251AF66E4F3E7FFFFFFFFFF
          FFFFFFFFFFFFFFE4F3E7299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4251AF66
          7AC189FFFFFFFFFFFF5FB57151AF66299C42299C42299C425FB57194CEA187C7
          9544A85A299C42299C4279C188FFFFFFC9E6D0299C42299C42299C4244A85A87
          C79594CEA151AF66299C4236A24E5FB5715FB571299C42299C42299C4244A85A
          5FB5715FB571299C4251AF665FB57136A24E299C42299C42299C425FB57187C7
          9594CEA187C7955FB571299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42FFFFFFFFFFFF43A859299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C4251AF66FFFFFFFFFFFF299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C4250AE6543A859299C425EB47136A24D299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42C9E6D0FFFFFF6BBB7D299C42299C4236A24D299C4250AE65299C4250AE
          6536A24D299C4279C18836A24DFFFFFFFFFFFF50AE655EB471A1D4AC299C4294
          CDA094CDA0299C42D7ECDBBCE0C443A859F2F9F3A1D4AC94CDA0FFFFFF79C188
          A1D4ACFFFFFF86C79494CDA0FFFFFFC9E6D0299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C425FB5716CBB7D44A85A299C4236A24E299C4251AF66299C4251AF
          6636A24E299C427AC18936A24EA2D4ADCAE7D05FB5716CBB7DA2D4AD299C42AF
          DAB994CEA1299C42D7ECDBBCE0C444A85AF2F9F3A1D4AC94CDA0FFFFFF86C794
          AEDAB8FFFFFF94CDA094CDA0FFFFFFC9E6D0299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F9F36CBB7D299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C425E
          B4716BBB7D94CDA0A1D4ACC9E6D0C9E6D0E4F3E7FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C4250AE6550AE6536A24D50AE65299C4243A859
          50AE6551AF6643A85986C7947CC28B36A24E299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F9F351AF66299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C425EB47194CDA0BCE0C4E4F3E7FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C4236A24D299C42299C425EB471299C4279C188299C
          4236A24DBCE0C4299C426BBB7DAEDAB8299C42AEDAB894CDA036A24DF2F9F386
          C79450AE65FFFFFF94CDA086C794FFFFFF79C188C9E6D0FFFFFF50AE65F2F9F3
          FFFFFF5EB471E4F3E7FFFFFFAEDAB8299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF51AF66299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C4250AE6586C794C9E6D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C4236A24E299C4251AF66299C
          42299C427AC189299C4251AF666CBB7D299C427AC1896CBB7D299C42BDE0C45F
          B57144A85ACAE7D06CBB7D6CBB7DCAE7D051AF66A1D4ACE4F3E744A85AD7ECDB
          F2F9F344A85ABCE0C4FFFFFF6CBB7D299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF86C794299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C4243A85994CD
          A0E4F3E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C4250AE65299C4243A85950AE65299C4250AE6543A859299C4279C1885EB471
          299C4279C18879C188299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFE4F3E7299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C4279C188C9E6D0FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C425EB471299C425EB47143A859299C42A1D4AC299C426BBB
          7D79C188299C42AEDAB879C18843A859F2F9F350AE6586C794FFFFFF50AE65AE
          DAB8FFFFFF50AE65E4F3E7FFFFFF65B877FFFFFFF2F9F35EB471FFFFFFF2F9F3
          50AE65FFFFFFFFFFFF5EB471299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF79C188299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C4279C188E4F3E7FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C4251AF66299C4236A2
          4E44A85A299C425FB57144A85A299C4287C7955FB57151AF6694CEA136A24E5F
          B571BDE0C436A24E94CEA1AFDAB936A24EBDE0C4A2D4AD44A85AE4F3E7A2D4AD
          36A24EE4F3E7E4F3E744A85A299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF36A24E299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C425EB471D7ECDBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFD7ECDB299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C4294CDA0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFC9E6D0299C42299C42299C42299C42299C42299C42299C42299C42
          299C42AEDAB8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFC9E6D0299C42299C42299C42299C42299C42299C42299C42299C42
          A1D4ACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF299C42299C42299C42299C42299C42299C42299C425EB471
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF6BBB7D299C42299C42299C42299C42299C42299C42BCE0C4
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFC9E6D0299C42299C42299C42299C42299C42299C42D7ECDB
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF86C794299C42299C42299C42299C42299C42C9E6D0
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5EB471299C42299C42299C42299C42A1D4AC
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F9F379C188299C42299C42299C4244A85A
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1D4AC36A24D299C42299C42
          86C794FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4F3E75EB471299C42
          299C42A2D4ADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9E6D0
          5EB471299C426CBB7DE4F3E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFE4F3E786C79443A8597AC189BDE0C4FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF94CDA0299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          0000299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          000051AF66299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4251AF66
          000087C795299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C4287C795
          0000E4F3E7299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42E4F3E7
          0000FFFFFF87C795299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C4287C795FFFFFF
          0000FFFFFFFFFFFF6CBB7D299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C426CBB7DFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFF86C794299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C4286C794FFFFFFFFFFFFFFFFFF
          0000FFFFFFFFFFFFFFFFFFFFFFFFE4F3E786C79451AF66299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42
          299C42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C
          42299C42299C42299C42299C42299C42299C42299C42299C42299C42299C4229
          9C42299C42299C42299C4251AF6686C794E4F3E7FFFFFFFFFFFFFFFFFFFFFFFF
          0000}
        mmHeight = 14023
        mmLeft = 1852
        mmTop = 25135
        mmWidth = 130175
        BandType = 0
      end
      object ppDBText19: TppDBText
        UserName = 'DBText19'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'ImporteTotal'
        DataPipeline = ppDatosComprobantes
        DisplayFormat = '#######0,;(#######0,)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDatosComprobantes'
        mmHeight = 4233
        mmLeft = 99749
        mmTop = 17463
        mmWidth = 36777
        BandType = 0
      end
      object ppDBText33: TppDBText
        UserName = 'DBText33'
        OnGetText = ppDBText28GetText
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'ImporteTotal'
        DataPipeline = ppDatosComprobantes
        DisplayFormat = '#######0,;(#######0,)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDatosComprobantes'
        mmHeight = 4233
        mmLeft = 102129
        mmTop = 80169
        mmWidth = 25928
        BandType = 0
      end
      object ppLabel1: TppLabel
        UserName = 'Label1'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Independencia - Santiago - Chile'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 8
        Font.Style = []
        Transparent = True
        mmHeight = 3175
        mmLeft = 3704
        mmTop = 12700
        mmWidth = 41275
        BandType = 0
      end
    end
    object ppDetailBand2: TppDetailBand
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 3969
      mmPrintPosition = 0
      object ppDBText27: TppDBText
        UserName = 'DBText1'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Descripcion'
        DataPipeline = ppDatosComprobantes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppDatosComprobantes'
        mmHeight = 3969
        mmLeft = 3969
        mmTop = 0
        mmWidth = 96309
        BandType = 4
      end
      object ppDBText28: TppDBText
        UserName = 'DBText3'
        OnGetText = ppDBText28GetText
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Importe'
        DataPipeline = ppDatosComprobantes
        DisplayFormat = '#######0,;(#######0,)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppDatosComprobantes'
        mmHeight = 3969
        mmLeft = 102129
        mmTop = 0
        mmWidth = 25928
        BandType = 4
      end
    end
    object ppFooterBand2: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 0
      mmPrintPosition = 0
    end
    object ppSummaryBand2: TppSummaryBand
      mmBottomOffset = 0
      mmHeight = 0
      mmPrintPosition = 0
    end
    object raCodeModule2: TraCodeModule
      ProgramStream = {00}
    end
    object ppParameterList2: TppParameterList
    end
  end
  object ObtenerDomicilioConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 104
  end
  object ppDomicilioFacturacion: TppDBPipeline
    DataSource = dsDomicilioFacturacion
    UserName = 'DomicilioFacturacion'
    Left = 72
    Top = 104
  end
  object dsDomicilioFacturacion: TDataSource
    DataSet = ObtenerDomicilioConvenio
    Left = 104
    Top = 104
  end
  object ObtenerConceptosNotaCredito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConceptosNotaCredito'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 136
    object ObtenerConceptosNotaCreditoCodigoConcepto: TWordField
      FieldName = 'CodigoConcepto'
    end
    object ObtenerConceptosNotaCreditoDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 50
    end
    object ObtenerConceptosNotaCreditoImporte: TBCDField
      FieldName = 'Importe'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
    object ObtenerConceptosNotaCreditoDescriImporte: TStringField
      FieldName = 'DescriImporte'
      ReadOnly = True
    end
  end
  object dsObtenerCargosComprobante: TDataSource
    DataSet = ObtenerConceptosNotaCredito
    Left = 104
    Top = 136
  end
  object ppCargosComprobante: TppDBPipeline
    DataSource = dsObtenerCargosComprobante
    OpenDataSource = False
    UserName = 'DatosComprobantes1'
    Left = 72
    Top = 136
    object ppCargosComprobanteppField1: TppField
      FieldAlias = 'CodigoConcepto'
      FieldName = 'CodigoConcepto'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object ppCargosComprobanteppField2: TppField
      FieldAlias = 'Descripcion'
      FieldName = 'Descripcion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object ppCargosComprobanteppField3: TppField
      FieldAlias = 'Importe'
      FieldName = 'Importe'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object ppCargosComprobanteppField4: TppField
      FieldAlias = 'DescriImporte'
      FieldName = 'DescriImporte'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
  end
end
