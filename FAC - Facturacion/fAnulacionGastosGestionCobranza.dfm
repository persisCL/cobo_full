object frmAnulacionGastosGestionCobranza: TfrmAnulacionGastosGestionCobranza
  Left = 307
  Top = 348
  BorderStyle = bsDialog
  Caption = 'frmAnulacionGastosGestionCobranza'
  ClientHeight = 126
  ClientWidth = 412
  Color = clBtnFace
  Constraints.MinHeight = 160
  Constraints.MinWidth = 420
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    412
    126)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 5
    Width = 393
    Height = 84
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object Label1: TLabel
    Left = 74
    Top = 42
    Width = 136
    Height = 13
    Caption = 'N'#250'mero de la Nota de Cobro'
  end
  object neNumeroComprobante: TNumericEdit
    Left = 226
    Top = 39
    Width = 105
    Height = 21
    TabOrder = 0
    Decimals = 0
  end
  object btnSalir: TButton
    Left = 328
    Top = 97
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Salir'
    TabOrder = 1
    OnClick = btnSalirClick
  end
  object btnAnular: TButton
    Left = 248
    Top = 97
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Anular'
    TabOrder = 2
    OnClick = btnAnularClick
  end
  object spAnularGastoGestionCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AnularGastoGestionCobranza;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 12
    Top = 93
  end
end
