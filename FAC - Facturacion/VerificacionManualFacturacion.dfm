object frmVerificacionManualFacturacion: TfrmVerificacionManualFacturacion
  Left = 226
  Top = 188
  Width = 745
  Height = 465
  Anchors = []
  Caption = 'Verificaci'#243'n Manual de Facturaci'#243'n'
  Color = clBtnFace
  Constraints.MinHeight = 465
  Constraints.MinWidth = 745
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    737
    431)
  PixelsPerInch = 96
  TextHeight = 13
  object pcDetalleComprobante: TPageControl
    Left = 2
    Top = 216
    Width = 733
    Height = 188
    ActivePage = TabSheet4
    Anchors = [akLeft, akRight, akBottom]
    MultiLine = True
    TabIndex = 1
    TabOrder = 2
    object TabSheet3: TTabSheet
      Caption = 'Detalle de Comprobante'
      DesignSize = (
        725
        160)
      object dbgDetalles: TDBListEx
        Left = 2
        Top = 2
        Width = 720
        Height = 155
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Fecha'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Nro Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NumeroConvenioFormateado'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Patente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Importe'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriImporte'
          end>
        DataSource = dsDatosComprobante
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Detalle de Viajes'
      ImageIndex = 1
      DesignSize = (
        725
        160)
      object dbgDetalleViajes: TDBListEx
        Left = 3
        Top = 2
        Width = 719
        Height = 155
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 92
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Concesionaria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 63
            Header.Caption = 'Viaje'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = True
            FieldName = 'NumeroViaje'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 52
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Patente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Categor'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Categoria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 140
            Header.Caption = 'Fecha/Hora de Inicio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaHoraInicio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 125
            Header.Caption = 'Puntos de Cobro'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'PuntosCobro'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 72
            Header.Caption = 'Importe'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriImporte'
          end>
        DataSource = dsFacturacionDetallada
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnLinkClick = dbgDetalleViajesLinkClick
      end
    end
  end
  object gbMovDisponibles: TGroupBox
    Left = 4
    Top = 44
    Width = 730
    Height = 165
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Lista de comprobantes'
    TabOrder = 1
    DesignSize = (
      730
      165)
    object Label2: TLabel
      Left = 9
      Top = 146
      Width = 142
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Comprobantes seleccionados:'
    end
    object lCantidadComprobantes: TLabel
      Left = 156
      Top = 146
      Width = 112
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'lCantidadComprobantes'
    end
    object dbgListaFacturas: TDPSGrid
      Left = 8
      Top = 14
      Width = 714
      Height = 127
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = dsFacturas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnTitleClick = dbgListaFacturasTitleClick
      OnWillClickTitle = dbgListaFacturasWillClickTitle
      OnOrderChange = dbgListaFacturasOrderChange
      Columns = <
        item
          Expanded = False
          FieldName = 'DescriComprobanteNumero'
          Title.Caption = 'Comprobante'
          Width = 112
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fecha'
          Width = 63
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimiento'
          Title.Caption = 'Fecha Venc.'
          Width = 73
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'DescriImporte'
          Title.Caption = 'Importe'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DescripcionDebito'
          Title.Caption = 'D'#233'bito'
          Width = 350
          Visible = True
        end>
    end
  end
  object GroupBox4: TGroupBox
    Left = 4
    Top = -1
    Width = 730
    Height = 44
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Par'#225'metros de b'#250'squeda'
    TabOrder = 0
    object Label5: TLabel
      Left = 10
      Top = 18
      Width = 116
      Height = 13
      Caption = 'Proceso de Facturaci'#243'n:'
    end
    object Label1: TLabel
      Left = 488
      Top = 18
      Width = 57
      Height = 13
      Caption = 'Porcentaje: '
    end
    object cbProcesosFacturacion: TComboBox
      Left = 132
      Top = 14
      Width = 341
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbProcesosFacturacionChange
      Items.Strings = (
        'Factura'
        'Nota de Cr'#233'dito'
        'Nota de D'#233'bito')
    end
    object nePorcentaje: TNumericEdit
      Left = 551
      Top = 14
      Width = 73
      Height = 21
      MaxLength = 6
      TabOrder = 1
      OnChange = nePorcentajeChange
      Decimals = 2
    end
    object btnActualizar: TButton
      Left = 640
      Top = 12
      Width = 75
      Height = 25
      Caption = '&Actualizar'
      TabOrder = 2
      OnClick = btnActualizarClick
    end
  end
  object btnSalir: TButton
    Left = 674
    Top = 427
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object dsFacturas: TDataSource
    DataSet = ObtenerMuestreoProcesosFacturacion
    OnDataChange = dsFacturasDataChange
    Left = 224
    Top = 128
  end
  object dsDatosComprobante: TDataSource
    DataSet = ObtenerDatosComprobante
    OnDataChange = dsDatosComprobanteDataChange
    Left = 104
    Top = 304
  end
  object ObtenerDatosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosComprobante'
    Parameters = <
      item
        Name = '@TipoComprobante'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        DataType = ftFloat
        Value = Null
      end>
    Left = 136
    Top = 304
  end
  object dsFacturacionDetallada: TDataSource
    DataSet = ObtenerFacturacionDetallada
    Left = 360
    Top = 311
  end
  object ObtenerFacturacionDetallada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerFacturacionDetallada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 392
    Top = 311
  end
  object ObtenerMuestreoProcesosFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMuestreoProcesosFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Porcentaje'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end>
    Left = 256
    Top = 128
  end
  object CantidadDeComprobantes: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'NumeroProcesoFacturacion'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT COUNT(*) AS CantidadComprobantes '
      '  FROM Comprobantes, LotesFacturacion'
      
        ' WHERE LotesFacturacion.NumeroProcesoFacturacion = :NumeroProces' +
        'oFacturacion'
      
        '   AND LotesFacturacion.TipoComprobante = Comprobantes.TipoCompr' +
        'obante'
      
        '   AND LotesFacturacion.NumeroComprobante = Comprobantes.NumeroC' +
        'omprobante'
      '   AND Comprobantes.FechaHoraImpreso IS NULL'
      '   AND Comprobantes.Estado = '#39'I'#39)
    Left = 216
    Top = 24
  end
  object ObtenerProcesosFacturacionSinImprimir: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerProcesosFacturacionSinImprimir'
    Parameters = <>
    Left = 184
    Top = 24
  end
end
