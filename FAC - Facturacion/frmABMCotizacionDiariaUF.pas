{********************************** Unit Header ********************************
File Name : frmABMCotizacionDiariaUF.pas
Author : jconcheyro
Date Created: 1/12/2006
Language : ES-AR
Description :  ABM de cotizaciones de Monedas en relacion al peso

Revision: 1
Author : jconcheyro
Date Created: 18/12/2006
Language : ES-AR
Description : como se agrega la cotizacion de UTM, ahora se agrega la columna Moneda

Firma       : SS_1309_NDR_20150615
Descripcion : Se elimina el uso del TRUNC porque modificaba el valor ingresado

*******************************************************************************}
unit frmABMCotizacionDiariaUF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB, DMConnection,
  DPSControls, Menus, Validate;

type
  TFormABMCotizacionDiariaUF = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    CotizacionesDiariasUF: TADOTable;
    GroupB: TPanel;
    Label15: TLabel;
    Panel1: TPanel;
    Notebook: TNotebook;
    Label1: TLabel;
    neCotizacion: TNumericEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    CotizacionesDiariasUFFechaVigencia: TDateTimeField;
    CotizacionesDiariasUFCotizacion: TLargeintField;
    deFechaVigencia: TDateEdit;
    MainMenu1: TMainMenu;
    CotizacionesDiariasUFFechaHoraCreacion: TDateTimeField;
    CotizacionesDiariasUFUsuarioCreacion: TStringField;
    CotizacionesDiariasUFFechaHoraModificacion: TDateTimeField;
    CotizacionesDiariasUFUsuarioModificacion: TStringField;
    CotizacionesDiariasUFMoneda: TStringField;
    cbMoneda: TComboBox;
    lblMoneda: TLabel;
    spObtenerListaMonedas: TADOStoredProc;
    spObtenerCotizacionFechaMoneda: TADOStoredProc;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cbMonedaKeyPress(Sender: TObject; var Key: Char);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializar : boolean;
  end;

var
  FormABMCotizacionDiariaUF  : TFormABMCotizacionDiariaUF;

implementation

resourcestring
	MSG_DELETE_QUESTION		= 'Est seguro de querer eliminar la Cotizaci�n?';
    MSG_DELETE_ERROR		= 'No se puede eliminar la Cotizaci�n porque hay datos que dependendientes.';
    MSG_DELETE_CAPTION 		= 'Eliminar la Cotizaci�n';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar la Cotizaci�n.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar la Cotizaci�n';
    MSG_ACTUALIZAR_CAPTION_MONEDA = 'Actualizar la Moneda'; //TASK_026_ECA_20160608
    MSG_ERROR_CARGANDO_MONEDAS = 'Se ha producido un error cargando la lista de monedas';

{$R *.DFM}

function TFormABMCotizacionDiariaUF.Inicializar: boolean;
//INCIO: TASK_026_ECA_20160608
var
    moneda: string;
begin
    Result := False;
    CenterForm(Self);
    try
        spObtenerListaMonedas.Open;
        cbMoneda.Items.Clear;
        while not spObtenerListaMonedas.eof do begin
            moneda:= Trim(spObtenerListaMonedas.FieldByName('Moneda').AsString);
            if moneda<>'' then
                cbMoneda.Items.Add(moneda);

            spObtenerListaMonedas.Next;
        end;
//FIN: TASK_026_ECA_20160608
        spObtenerListaMonedas.Close;
    except on E: Exception do begin
            MsgBoxErr(MSG_ERROR_CARGANDO_MONEDAS, Caption, Caption, MB_ICONSTOP );
            Exit;
        end;
    end;

	if not OpenTables([CotizacionesDiariasUF]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;

end;

procedure TFormABMCotizacionDiariaUF.Limpiar_Campos();
begin
    deFechaVigencia.Date := NowBase(DMConnections.BaseCAC);
    neCotizacion.Value := 0;
end;

function TFormABMCotizacionDiariaUF.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
	Texto :=  Tabla.FieldByName('FechaVigencia').AsString ;
end;

procedure TFormABMCotizacionDiariaUF.BtSalirClick(Sender: TObject);
begin
	Close;
end;

//INICIO: TASK_026_ECA_20160608
procedure TFormABMCotizacionDiariaUF.cbMonedaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if ( StrScan('ABCDEFGHIJKLMN�OPQRSTUVWXYZ'+chr(7)+chr(8), UpCase(Key)) = nil ) then  Key := #0;
end;
//FIN: TASK_026_ECA_20160608

procedure TFormABMCotizacionDiariaUF.ListaInsert(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Alta;
	Limpiar_Campos;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
    deFechaVigencia.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormABMCotizacionDiariaUF.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
    deFechaVigencia.SetFocus;
	Screen.Cursor    := crDefault;

end;

procedure TFormABMCotizacionDiariaUF.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			CotizacionesDiariasUF.Delete;
		Except
			On E: EDataBaseError do begin
				CotizacionesDiariasUF.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMCotizacionDiariaUF.ListaClick(Sender: TObject);
var
  index: integer;
//INICIO: TASK_026_ECA_20160608
  moneda: string;
begin
     deFechaVigencia.Date   := CotizacionesDiariasUF.FieldByName('FechaVigencia').AsDateTime;
     neCotizacion.Value     := CotizacionesDiariasUF.FieldByName('Cotizacion').AsInteger / 100;
    for index := 0 to cbMoneda.Items.Count - 1 do begin
        moneda:=  Trim(cbMoneda.Items[index]);
        if moneda = Trim(CotizacionesDiariasUF.FieldByName('Moneda').AsString) then begin
            cbMoneda.ItemIndex := index;
            Break;
        end;
    end;
//FIN: TASK_026_ECA_20160608
end;

procedure TFormABMCotizacionDiariaUF.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormABMCotizacionDiariaUF.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormABMCotizacionDiariaUF.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, DateTimeToStr(CotizacionesDiariasUF.FieldByName('FechaVigencia').AsDateTime));
		TextOut(Cols[1], Rect.Top, FloatToStr(CotizacionesDiariasUF.FieldByName('Cotizacion').AsFloat / 100));
		TextOut(Cols[2], Rect.Top, CotizacionesDiariasUF.FieldByName('Moneda').AsString);
	end;
end;

procedure TFormABMCotizacionDiariaUF.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormABMCotizacionDiariaUF.BtnAceptarClick(Sender: TObject);
resourcestring
    //INICIO	: TASK_112_CFU_20170223
	MSG_YA_EXISTE_COTIZACION	= 'Ya existe cotizaci�n (%s) para fecha y moneda ingresadas';
    //TERMINO	: TASK_112_CFU_20170223
    MSG_COTIZACION_VACIA 		= 'Cotizaci�n vac�a';
    MSG_MONEDA_INVALIDA  		= 'Moneda no v�lida';
begin
    if not ValidateControls([neCotizacion],
                [neCotizacion.Value >0  ],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_COTIZACION_VACIA]) then exit;

    //INICIO: TASK_026_ECA_20160608
     if  not ValidateControls([cbMoneda],
                [(Length(Trim(cbMoneda.Text))>0) and (Length(Trim(cbMoneda.Text))<4)],
                MSG_ACTUALIZAR_CAPTION_MONEDA,
                [MSG_MONEDA_INVALIDA]) then
                Exit;
    //FIN: TASK_026_ECA_20160608

    //INICIO	: TASK_112_CFU_20170223
    //Valido que no exista cotizaci�n para fecha/moneda ingresadas en caso que sea un ingreso de valor
    if Lista.Estado = Alta then begin
        with spObtenerCotizacionFechaMoneda, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@Moneda').Value		:= UpperCase(cbMoneda.Text);
            ParamByName('@FechaVigencia').Value	:= deFechaVigencia.Date;
            Open;
            if not FieldByName('Cotizacion').IsNull then begin
                MsgBox(Format(MSG_YA_EXISTE_COTIZACION, [FieldByName('Cotizacion').Value/100]), MSG_ACTUALIZAR_CAPTION, MB_ICONINFORMATION);
                Exit;
            end;

            Close;
        end;
    end;
    //TERMINO	: TASK_112_CFU_20170223

	Screen.Cursor := crHourGlass;
	With CotizacionesDiariasUF do begin
		Try
			if Lista.Estado = Alta then Append else Edit;

			FieldByName('FechaVigencia').AsDateTime  := deFechaVigencia.Date;
			//FieldByName('Cotizacion').AsInteger       := trunc(neCotizacion.Value * 100 );       //SS_1309_NDR_20150615
			FieldByName('Cotizacion').AsInteger         := StrToInt(FloatToStr(neCotizacion.Value * 100)) ;                //SS_1309_NDR_20150615
            FieldByName('Moneda').AsString              := UpperCase(cbMoneda.Text); //cbMoneda.Text;  TASK_026_ECA_20160608    

            if Lista.Estado = Alta then begin
              FieldByName('UsuarioCreacion').AsString 	  := UsuarioSistema;
              FieldByName('FechaHoraCreacion').AsDateTime := NowBase(CotizacionesDiariasUF.Connection);
            end;

			FieldByName('UsuarioModificacion').AsString 	  := UsuarioSistema;
			FieldByName('FechaHoraModificacion').AsDateTime := NowBase(CotizacionesDiariasUF.Connection);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado     	:= Normal;
	Lista.Enabled    	:= True;
	GroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
	Lista.Reload;
	Lista.SetFocus;
    //INICIO: TASK_026_ECA_20160608
    spObtenerListaMonedas.Open;
    cbMoneda.Items.Clear;
    while not spObtenerListaMonedas.eof do begin
        if Trim(spObtenerListaMonedas.FieldByName('Moneda').AsString)<>'' then
            cbMoneda.Items.Add(Trim(spObtenerListaMonedas.FieldByName('Moneda').AsString));
        spObtenerListaMonedas.Next;
    end;
    spObtenerListaMonedas.Close;
    //FIN: TASK_026_ECA_20160608
	Screen.Cursor 	   := crDefault;
end;

procedure TFormABMCotizacionDiariaUF.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormABMCotizacionDiariaUF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormABMCotizacionDiariaUF.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
