object frmSeleccionarNKAGenerarGastoCobranza: TfrmSeleccionarNKAGenerarGastoCobranza
  Left = 313
  Top = 228
  BorderStyle = bsDialog
  Caption = 'frmSeleccionarNKAGenerarGastoCobranza'
  ClientHeight = 118
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 386
    Height = 73
  end
  object Label1: TLabel
    Left = 24
    Top = 19
    Width = 209
    Height = 39
    Caption = 
      'Ingrese el N'#250'mero de la Nota de Cobro para la cual desea generar' +
      ' los Gastos de Gesti'#243'n de Cobranza'
    WordWrap = True
  end
  object btnAceptar: TButton
    Left = 240
    Top = 88
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 320
    Top = 88
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object neNK: TNumericEdit
    Left = 264
    Top = 32
    Width = 97
    Height = 21
    AutoSize = False
    TabOrder = 0
    Decimals = 0
  end
end
