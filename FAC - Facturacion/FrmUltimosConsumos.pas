{-----------------------------------------------------------------------------
 Unit Name: FrmUltimosConsumos
 Author:
 Purpose:
 History:

 Revision 1:
 Author:    ggomez
 Date:      27/06/2005
 Description:   Modifiqu� para que herede de TForm y se comporte como MDI.

 Revision 2:
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

 Revision 3:
 Author: mbecerra
 Date: 13-Mayo-2010
 Descripcion:		(Ref: Fase II)
            	Se agrega la columna concesionaria a la grilla de �ltimos consumos
                y a la grilla de Detalle Ultimos Consumos.

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se crea llamado a DrawItem para verificar si el combo del convenio
                posee alg�n convenio con el texto "(Baja)" y en caso de encontrarlo
                llamar a la funci�n que lo colorear� en rojo.
-----------------------------------------------------------------------------}
unit FrmUltimosConsumos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Controls, Forms, Dialogs,
  Navigator, StdCtrls, ExtCtrls, Buttons, DmiCtrls, ADODB, UtilDB, Grids,
  DBGrids, Util, UtilProc, DMConnection, peaprocs, ComCtrls, BuscaClientes,
  peatypes, DPSControls, ListBoxEx, DBListEx, VariantComboBox, utilFacturacion,
  DB, MenuesContextuales, Graphics;

type
  TFormUltimosConsumos = class(TForm)
    ObtenerCliente: TADOStoredProc;
    dsUltimosConsumos: TDataSource;
    dsUltimosConsumosDetalle: TDataSource;
    gb_Parametros: TGroupBox;
    Label2: TLabel;
    lb_CodigoCliente: TLabel;
    peNumeroDocumento: TPickEdit;
    GroupBox1: TGroupBox;
	lApellidoNombre: TLabel;
    Label4: TLabel;
    LDomicilio: TLabel;
    Label10: TLabel;
    spObtenerUltimosConsumosDetalle: TADOStoredProc;
    spObtenerUltimosConsumos: TADOStoredProc;
    Label3: TLabel;
    Panel1: TPanel;
    gb_MovSeleccionados: TGroupBox;
    label9: TLabel;
    lTotalProximosMovimientos: TLabel;
    dbgFacturas: TDBListEx;
    gb_MovDisponibles: TGroupBox;
    dbgDetalles: TDBListEx;
    Splitter1: TSplitter;
    cbConvenio: TVariantComboBox;
    cbCuenta: TVariantComboBox;
    tmrConsulta: TTimer;
    spObtenerDomicilioFacturacion: TADOStoredProc;
	procedure ActualizarCombos(Sender: TObject);
	procedure ActualizarDatos;
    procedure btnSalirClick(Sender: TObject);
	procedure peNumeroDocumentoChange(Sender: TObject);
	procedure peNumeroDocumentoButtonClick(Sender: TObject);
	procedure spObtenerUltimosConsumosAfterOpen(DataSet: TDataSet);
	procedure spObtenerUltimosConsumosAfterClose(DataSet: TDataSet);
    procedure dbgDetallesColumns0HeaderClick(Sender: TObject);
    procedure dbgDetallesContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure tmrConsultaTimer(Sender: TObject);
    procedure dbgDetallesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbConvenioDrawItem(Control: TWinControl; Index: Integer;                    //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                               //SS_1120_MVI_20130820

  private
	FUltimaBusqueda : TBusquedaCliente;
	FApellido, FApellidoMaterno, FNombre: AnsiString;
    FCodigoCliente : integer;
	Function CargarCliente: Boolean;
  public
	{ Public declarations }
    procedure MostrarDomicilioFacturacion(CodigoConvenio : integer);

	function Inicializar(CodigoConvenio: Integer = 0; IndiceVehiculo: Integer = 0): Boolean;
  end;

var
  FormUltimosConsumos: TFormUltimosConsumos;



implementation

{$R *.dfm}

resourcestring
    MSG_TODAS_FECHAS        = 'Todas las fechas';
    MSG_TODAS_FORMAS_PAGO   = 'Todas las formas de pago';
    MSG_TODAS_CUENTAS       = 'Todos los convenios';
    MSG_TODOS_VEHICULOS     = 'Todos los veh�culos';


{------------------------------------------------------------------------------}
{ Inicializa la ventana
{------------------------------------------------------------------------------}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:  /  /
  Description:
  Parameters: CodigoConvenio: Integer = 0; IndiceVehiculo: Integer = 0
  Return Value: Boolean

  Revision : 1
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormUltimosConsumos.Inicializar(CodigoConvenio: Integer = 0; IndiceVehiculo: Integer = 0): Boolean;
resourcestring
    MSG_ERROR_INICIALIZAR = 'Error al inicializar el formulario.';
var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

    Color                   := clbtnFace;
    lApellidoNombre.Caption := EmptyStr;
    lDomicilio.Caption      := EmptyStr;

 {REV.3
    with dbgFacturas.Columns do begin
		Items[1].Header.Caption := MSG_COL_TITLE_DESCRIPCION;
		Items[2].Header.Caption := MSG_COL_TITLE_IMPORTE;
	end;

    with dbgDetalles.Columns do begin
        Items[0].Header.Caption := MSG_COL_TITLE_FECHA_HORA;
        Items[1].Header.Caption := MSG_COL_TITLE_PATENTE;
		Items[2].Header.Caption := MSG_COL_TITLE_PUNTO_COBRO;
		Items[3].Header.Caption := MSG_COL_TITLE_CATEGORIA;
		Items[4].Header.Caption := MSG_COL_TITLE_TIPO_DE_HORARIO;
		Items[5].Header.Caption := MSG_COL_TITLE_IMPORTE;
	end;
    }

	lTotalProximosMovimientos.Caption := FormatFloat(FORMATO_IMPORTE, 0.00);

	Update;

    FCodigoCliente := -1;

    Result := True;
    if (CodigoConvenio > 0) and (IndiceVehiculo > 0) then begin
        try
            peNumeroDocumento.Text := QueryGetValue(DMConnections.BaseCAC, Format(
                'SELECT Personas.NumeroDocumento FROM Personas WITH (NOLOCK) , Convenio  WITH (NOLOCK) ' +
                'WHERE Personas.CodigoPersona = Convenio.CodigoCliente ' +
                'AND Convenio.CodigoConvenio = %d', [CodigoConvenio]));

            cbConvenio.Value    := CodigoConvenio;
            cbCuenta.Value      := IndiceVehiculo;

        except
            on E: Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, Self.Caption, MB_ICONERROR);
            end;

        end; // except
    end;

	if not Result then Exit;
end;

{------------------------------------------------------------------------------}
{ Carga los Datos del cliente
{------------------------------------------------------------------------------}
function TFormUltimosConsumos.CargarCliente: Boolean;
resourcestring
    MSG_FECHAS_FACTURACION = 'No se pudieron obtener las pr�ximas Fechas de Facturaci�n.';
    CAPTION_OBTENER_FECHAS = 'Obtener Fechas de Facturaci�n';
begin
	Result := true;
    with ObtenerCliente do begin
	    close;
	    Parameters.ParamByName('@NumeroDocumento').value := PadL(peNumeroDocumento.Text, 9, '0');
        Parameters.ParamByName('@CodigoDocumento').value := TIPO_DOCUMENTO_RUT;
	    Open;
        if not eof then begin
            FCodigoCliente := FieldByName('CodigoCliente').AsInteger;
            FApellido := Trim(FieldByName('Apellido').asString);
            FApellidoMaterno := Trim(FieldByName('ApellidoMaterno').asString);
            FNombre := Trim(FieldByName('Nombre').asString);
            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lApellidoNombre.Caption :=  ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre)
            else
                lApellidoNombre.Caption :=  ArmaRazonSocial(FApellido, FNombre);

        end else begin
            FCodigoCliente := -1;
            lApellidoNombre.Caption := '';
            lDomicilio.Caption := '';
        end;
    end;

	cursor := crDefault;
end;

{------------------------------------------------------------------------------}
{ Actualiza los Combos Fechas, Contratos, etc.
{------------------------------------------------------------------------------}
procedure TFormUltimosConsumos.ActualizarCombos(Sender: TObject);
var
	ActualizarCuentas, ActualizarVehiculos : Boolean;
begin
	// Verifico que combo Actualizo.
	ActualizarCuentas	:= False;
	ActualizarVehiculos	:= False;
	if Sender = cbConvenio then begin
        ActualizarVehiculos := True;
	end else if Sender = nil then begin
		ActualizarCuentas	:= True;
        ActualizarVehiculos := True;
	end;

    // Verificamos si debemos actualizar el Combo de Cuentas
    if ActualizarCuentas then CargarConveniosRUT(DMCOnnections.BaseCAC,
      cbConvenio, 'RUT', PadL(peNumeroDocumento.Text, 9, '0' ));

    // Verificamos si debemos actualizar el Combo de Vehiculos
    if ActualizarVehiculos then begin
    	CargarCuentasConvenio(DMCOnnections.BaseCAC, cbCuenta, iif(cbConvenio.ItemIndex >= 0, cbConvenio.Value, -1), True);
		if (cbConvenio.ItemIndex >= 0) then
			MostrarDomicilioFacturacion(cbConvenio.Value);
    end;

    // Finalmente Actualizamos las grillas
    if ( Sender <> nil ) then ActualizarDatos;
end;

procedure TFormUltimosConsumos.MostrarDomicilioFacturacion(CodigoConvenio : integer);
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        try
        	ExecProc;
    		lDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

{------------------------------------------------------------------------------}
{ Actualiza los datos de los Grids
{------------------------------------------------------------------------------}
procedure TFormUltimosConsumos.ActualizarDatos;
resourcestring
    MSG_OBTENER_ULTIMOS_CONSUMOS = 'No se pudieron obtener los �ltimos consumos.';
    CAPTION_OBTENER_ULTIMOS_CONSUMOS = 'Obtener Ultimos Consumos';
    MSG_OBTENER_TRANSITOS = 'No se pudieron obtener los tr�nsitos.';
    CAPTION_OBTENER_TRANSITOS = 'Obtener Tr�nsitos';
begin
    Screen.Cursor := crHourGlass;
    try
        try
            spObtenerUltimosConsumos.Close;
            spObtenerUltimosConsumosDetalle.Close;

            spObtenerUltimosConsumos.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
            spObtenerUltimosConsumosDetalle.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
            if cbCuenta.ItemIndex = 0 then begin
                spObtenerUltimosConsumos.Parameters.ParamByName('@IndiceVehiculo').Value := null;
                spObtenerUltimosConsumosDetalle.Parameters.ParamByName('@IndiceVehiculo').Value := null;
            end else begin
                spObtenerUltimosConsumos.Parameters.ParamByName('@IndiceVehiculo').Value := cbCuenta.Value;
                spObtenerUltimosConsumosDetalle.Parameters.ParamByName('@IndiceVehiculo').Value := cbCuenta.Value;
            end;

            spObtenerUltimosConsumos.Parameters.ParamByName('@FechaCorte').Value  := null;
            spObtenerUltimosConsumos.Open;
        except
            On E: Exception do begin
                MsgBoxErr(MSG_OBTENER_ULTIMOS_CONSUMOS, e.message, CAPTION_OBTENER_ULTIMOS_CONSUMOS, MB_ICONSTOP);
            end;
        end;

        try
            spObtenerUltimosConsumosDetalle.Open;
        Except
            On E: Exception do begin
                MsgBoxErr(MSG_OBTENER_TRANSITOS, e.message, CAPTION_OBTENER_TRANSITOS, MB_ICONSTOP);
                spObtenerUltimosConsumosDetalle.Close;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end; // finally

end;

{------------------------------------------------------------------------------}
procedure TFormUltimosConsumos.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{------------------------------------------------------------------------------}
{  Busca un persona y trae los datos
{------------------------------------------------------------------------------}
procedure TFormUltimosConsumos.peNumeroDocumentoButtonClick( Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes , f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Cursor := crHourGlass;
			peNumeroDocumento.Text := Trim(f.Persona.NumeroDocumento);
			peNumeroDocumento.Cursor := crDefault;
            tmrConsultaTimer(nil);
	   	end;
	end;
    f.Release;
end;

{------------------------------------------------------------------------------}
{  Actualiza los datos de un Cliente y ejecuta la consulta
{------------------------------------------------------------------------------}
procedure TFormUltimosConsumos.peNumeroDocumentoChange(Sender: TObject);
begin

    if (ActiveControl <> Sender) then Exit;

    tmrConsulta.Enabled := False;
    tmrConsulta.Enabled := True;
end;

{------------------------------------------------------------------------------}
{  Suma el total de la Factura
{------------------------------------------------------------------------------}
procedure TFormUltimosConsumos.spObtenerUltimosConsumosAfterOpen( DataSet: TDataSet);
var
    total: Double;
begin
	total := 0;
	with DataSet do begin
			disableControls;
			while not eof do begin
				total := total + FieldByName('Importe').asFloat;
				Next;
			end;
			first;
			enableControls;
	end;
    lTotalProximosMovimientos.Caption := FormatFloat(FORMATO_IMPORTE, Total);
end;

procedure TFormUltimosConsumos.spObtenerUltimosConsumosAfterClose( DataSet: TDataSet);
begin
	lTotalProximosMovimientos.Caption := FormatFloat(FORMATO_IMPORTE, 0.00);
end;

{-----------------------------------------------------------------------------
  Function Name: dbgDetallesColumns0HeaderClick
  Author:    flamas
  Date Created: 29/12/2004
  Description: Ordena por Columna 
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormUltimosConsumos.dbgDetallesColumns0HeaderClick(Sender: TObject);
begin
    if NOT  spObtenerUltimosConsumosDetalle.Active then Exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;

    spObtenerUltimosConsumosDetalle.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

procedure TFormUltimosConsumos.dbgDetallesContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  if not spObtenerUltimosConsumosDetalle.IsEmpty then
  	Handled := MostrarMenuContextualTransito(dbgDetalles.ClientToScreen(MousePos),
          		spObtenerUltimosConsumosDetalle.FieldByName('NumCorrCA').asInteger);
end;

procedure TFormUltimosConsumos.tmrConsultaTimer(Sender: TObject);
begin
    try
		Screen.Cursor := crHourglass;
		tmrConsulta.Enabled := False;
    	lApellidoNombre.Caption := '';
	    lDomicilio.Caption := '';
	    CargarCliente;
		ActualizarCombos(nil);
	    ActualizarDatos;
    finally
		Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormUltimosConsumos.dbgDetallesDrawText
  Author:    lgisuk
  Date:      02-May-2005
  description: asigno el formato a los campos de la grilla de detalle
  		       de ultimos consumos
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormUltimosConsumos.dbgDetallesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column = dbgDetalles.Columns[0] then
       Text := FormatDateTime('dd-mm-yyyy hh:nn', spObtenerUltimosConsumosDetalle.FieldByName('FechaHora').AsDateTime)
    else if Column = dbgDetalles.Columns[5] then begin
      {muestro los montos de cada transaccion en dos decimales}
      Text := formatfloat('$ #,##0.00; $ -#,##0.00', spObtenerUltimosConsumosDetalle.FieldByName('Importe').Asfloat);
    end;
end;


procedure TFormUltimosConsumos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TFormUltimosConsumos.cbConvenioDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TFormUltimosConsumos.cbConvenioDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin

  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenio.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
// END: SS_1120_MVI_20130820 ----------------------------------------------------------------------------------
end.


