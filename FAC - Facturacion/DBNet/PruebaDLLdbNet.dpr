program PruebaDLLdbNet;





uses
  Forms,
  frmMain in 'frmMain.pas' {MainForm},
  DTEControlDLL in '..\DTEControlDLL.pas',
  ConstParametrosGenerales in '..\..\Comunes\ConstParametrosGenerales.pas',
  DMConnection in '..\..\Comunes\DMConnection.pas' {DMConnections: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDMConnections, DMConnections);
  Application.Run;
end.
