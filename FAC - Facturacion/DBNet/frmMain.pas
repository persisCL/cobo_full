unit frmMain;

interface

uses
    DTEControlDLL,
    DMConnection,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ShellApi, ExtCtrls, Jpeg;

type
  TMainForm = class(TForm)
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edtRutEmi: TEdit;
    edtDVEmi: TEdit;
    edtFolio: TEdit;
    edtMonto: TEdit;
    edtRutRecep: TEdit;
    edtDVRecep: TEdit;
    edtRazonSocial: TEdit;
    edtDescrip: TEdit;
    Label2: TLabel;
    cbxTipoDoc: TComboBox;
    Label9: TLabel;
    Button1: TButton;
    mmoLog: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Label11: TLabel;
    edtFolioNuevo: TEdit;
    dtpFechaEmision: TDateTimePicker;
    dtpFechaTimbre: TDateTimePicker;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    imgTimbre: TImage;
    lblArchivo: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Limpiar;
    function ConectarBase : boolean;
    function ObtieneTipoDocumento : integer;
    procedure CopiarArreglo(Destino : array of Char; Origen : string);
    procedure CopiarArregloChar(Destino : string; Origen : array of Char; LargoMax : integer);
  end;


  		RptaSolicitaTedDll = record
			strTed : array[1..1001] of Char;
			strTms : array[1..31] of Char;
			Codigo : array[1..11] of Char;
        	Mensaje: array[1..101] of Char;
        end;

  TSolicitaTed = function  (	RutEmisor : integer; DigitoVerEmisor : PChar; TipoDocumento, Folio : integer;
   							FechaHoraEmision : PChar; Monto : double; GlosaItem1 : PChar;
                            RutReceptor : integer; DigitoVerReceptor,
                            RazonSocialReceptor, FechaHoraTimbre : PChar): RptaSolicitaTedDll; cdecl;
var
  MainForm: TMainForm;

implementation

{$R *.dfm}

function TMainForm.ConectarBase;
resourcestring
    MSG_CONNECT_ERROR = 'Ha ocurrido un error conectando a la base de datos. Detalle: %s';
begin
    Result := False;
    try
        if not DMConnections.BaseCAC.Connected then begin
            DMConnections.SetUp;
            DMConnections.BaseCAC.Connected := True;
        end;
        Result := True;
    except
        on e: Exception do begin
            mmolog.Lines.add(Format(MSG_CONNECT_ERROR, [e.Message]));
        end;
    end;
end;

function TMainForm.ObtieneTipoDocumento;
begin
    case cbxTipoDoc.ItemIndex of
        0 : Result := 33;
        1 : Result := 34;
        2 : Result := 39;
        3 : Result := 41;
        4 : Result := 52;
        5 : Result := 56;
        6 : Result := 61;
        else Result := -1;
    end;
end;

procedure TMainForm.Limpiar;
begin
    edtRutEmi.Text := '';
    edtDVEmi.Text := '';
    cbxTipoDoc.ItemIndex := -1;
    edtFolio.Text := '';
    dtpFechaEmision.DateTime := 0;
    edtMonto.Text := '';
    edtDescrip.Text := '';
    edtRutRecep.Text := '';
    edtDVRecep.Text := '';
    edtRazonSocial.Text := '';
    dtpFechaTimbre.DateTime := 0;

end;

procedure TMainForm.CopiarArreglo;
var
	i, largo : integer;
begin
    largo := Length(Origen);
    for i := 1 to Largo do begin
        Destino[i] := Origen[i];
    end;

end;

procedure TMainForm.CopiarArregloChar;
var
	i : integer;
begin

    i := 1;
    Destino := '';
    while (i <= LargoMax) and (origen[i]<> Chr(0)) do
        Destino := Destino + Origen[i];

end;

procedure TMainForm.Button10Click(Sender: TObject);
var
    Mensaje : string;
begin
	if not DMConnections.BaseCAC.Connected then ConectarBase();
    
    if ConfigurarVariable_EGATE_HOME(Mensaje) then mmoLog.Lines.Add('EGATE_HOME Exitosa: ' + PARAM_DIR_EGATE_HOME)
    else mmoLog.Lines.Add('EGATE_HOME Error: ' + Mensaje);

end;

procedure TMainForm.Button11Click(Sender: TObject);
const
	ValorPrueba = 'D:\SUITE\';
    //ValorPrueba = '\\pino\op_test\Aplicaciones\DBNET\suite\';
var
    Valor : string;
begin
    Valor := GetEnvironmentVariable('EGATE_HOME');
    mmoLog.Lines.Add('EGATE_HOME  antes: ' + Valor);
    SetEnvironmentVariable(PChar('EGATE_HOME'), PChar(ValorPrueba));
    Valor := GetEnvironmentVariable('EGATE_HOME');
    mmoLog.Lines.Add('EGATE_HOME  Despu�s: ' + Valor);
    if ValorPrueba = Valor then PARAM_DIR_EGATE_HOME := Valor;

end;

procedure TMainForm.Button1Click(Sender: TObject);
{var
	TipoDocumento : integer;
    Timbre : string;}
begin
   {	mmoLog.Clear;
    TipoDocumento := ObtieneTipoDocumento();
    if ObtenerTimbreDBNet(	StrToInt(edtRutEmi.Text), edtDVEmi.Text, TipoDocumento,
                            StrToInt(edtFolio.Text), dtpFechaEmision.Date,
                            StrToInt(edtMonto.Text), edtDescrip.Text,
                            StrToInt(edtRutRecep.Text), edtDVRecep.Text, edtRazonSocial.Text,
                            dtpFechaTimbre.DateTime, Timbre) then
    	mmoLog.Lines.Add(Timbre)
    else
    	mmoLog.Lines.Add('Error');

    }

end;

procedure TMainForm.Button2Click(Sender: TObject);
{var
    Folio : RptaSolicitaFolioDll;}
begin
{    Folio := SolicitaFolio(StrToInt(edtFolioNuevo.Text));
    mmoLog.Clear;
    mmoLog.Lines.Add('Folio : ' + IntToStr(Folio.Folio));
    mmoLog.Lines.Add('Codigo: ' + folio.Codigo);
    mmoLog.Lines.Add('Mensaje: ' + folio.Mensaje);}
end;

procedure TMainForm.Button3Click(Sender: TObject);
begin
	Limpiar();
end;

procedure TMainForm.Button4Click(Sender: TObject);
{var
	Folio : RptaAccionFolioDll;}
begin
 {	Folio := ConfirmaFolio(StrToInt(edtFolioNuevo.Text));
    mmoLog.Clear;
    mmoLog.Lines.Add('Codigo: ' + folio.Codigo);
    mmoLog.Lines.Add('Mensaje: ' + folio.Mensaje);}
end;

procedure TMainForm.Button5Click(Sender: TObject);
{var
	Folio : RptaAccionFolioDll;}
begin
	{Folio := ReversaFolio(StrToInt(edtFolioNuevo.Text));
    mmoLog.Clear;
    mmoLog.Lines.Add('Codigo: ' + folio.Codigo);
    mmoLog.Lines.Add('Mensaje: ' + folio.Mensaje);}

end;

procedure TMainForm.Button6Click(Sender: TObject);
var
	resp : TTimbreArchivo;
    ArchivoImg : string;
    TipoDoc : integer;
    GenerarImagen : boolean;
begin
	mmoLog.Clear;
    imgTimbre.Picture := nil;
	TipoDoc := ObtieneTipoDocumento();
    GenerarImagen := (TButton(sender).Tag = 10);
	if ObtenerTimbreDBNet(	StrToInt(edtRutEmi.Text), edtDVEmi.Text, TipoDoc,
                            StrToInt(edtfolio.text), dtpFechaEmision.Date,
                            StrToInt(edtMonto.Text), edtDescrip.Text,
                            StrToInt(edtRutRecep.Text), edtDVRecep.Text,
                            edtRazonSocial.Text, dtpFechaTimbre.DateTime,
                            GenerarImagen, resp ) then begin
    	mmoLog.Lines.Add(resp.Timbre);
        lblArchivo.Caption := resp.Archivo;
        if GenerarImagen then begin
        	ArchivoImg := PARAM_DIR_EGATE_HOME + 'out\html\' + resp.Archivo + '.jpg';
            if FileExists(ArchivoImg) then  imgTimbre.Picture.LoadFromFile(ArchivoImg)
            else mmoLog.Lines.Add('No se encontr� archivo: ' + ArchivoImg);
        end;

    end
    else
    	mmoLog.Lines.Add('Error');

end;

procedure TMainForm.Button7Click(Sender: TObject);
var
	Cod : integer;
begin
	Cod := CargarDLL();
    if Cod = 0 then mmoLog.Lines.Add('DLL OK')
    else mmoLog.Lines.Add('Error al cargar DLL: ' + ObtieneErrorAlCargarDLL(Cod));
end;

procedure TMainForm.Button8Click(Sender: TObject);
begin
    if LiberarDLL() then mmoLog.Lines.Add('DLL Liberada OK')
    else mmoLog.Lines.Add('Error al Liberar DLL');
    
end;

procedure TMainForm.Button9Click(Sender: TObject);
var
	ArchivoIn, ArchivoOut, Ejecutable, Parametros : string;
begin
	{generar imagen usando el ejecutable}
    ArchivoIn	:= PARAM_DIR_EGATE_HOME + 'in\ted\' + lblArchivo.Caption + '.ted';
    ArchivoOut	:= PARAM_DIR_EGATE_HOME + 'out\html\' + lblArchivo.Caption + '.jpg';
    Ejecutable	:= PARAM_DIR_EGATE_HOME + 'bin\egate_pdf417.exe';
    Parametros	:= ArchivoIn + ' ' + ArchivoOut + ' jpg';
	ShellExecute(Handle, 'open', PChar(Ejecutable), PChar(Parametros), PChar(PARAM_DIR_EGATE_HOME), SW_SHOWMINIMIZED);
    Sleep(200);  //esperar que el shell ejecute el programa
    if FileExists(ArchivoOut) then  imgTimbre.Picture.LoadFromFile(ArchivoOut)
    else mmoLog.Lines.Add('No se encontr� archivo: ' + ArchivoOut);
end;

end.
