{-----------------------------------------------------------------------------
 File Name: FrmControlIndemnizaciones.pas
 Author:    SGuastoni
 Date Created: 18/12/2006
 Language: ES-AR
 Description: ABM para guardar los detalles de control de idemnizaciones.


 Revision   : 1
 Fecha      : 02-07-2009
 Autor      : Nelson Droguett Sierra
Descripcion : Se agrega la operacion de reactivacion
              y el almacen TELEV�AS EN ARRIENDO EN CUOTAS
-----------------------------------------------------------------------------}
unit FrmControlIndemnizaciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,PeaProcs,DMConnection,PeaTypes,Util, DB, ADODB,
  DbList, Abm_obj,UtilDB, VariantComboBox, UtilProc;

type
  TFormControlIndemnizaciones = class(TForm)
    Pnl1: TPanel;
    LblConcepto: TLabel;
    LblAlmacen: TLabel;
    txtDescripcion: TEdit;
    LblDescripcion: TLabel;
    LblOperacion: TLabel;
    Panel1: TPanel;
    tblMotivosMovimientos: TADOTable;
    CbOperacion: TVariantComboBox;
    CbAlmacen: TVariantComboBox;
    CbConcepto: TVariantComboBox;
    Pnl3: TPanel;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    AbmToolbar1: TAbmToolbar;
    ObtenerConceptosComprobante: TADOStoredProc;
    Lista: TAbmList;
    Label1: TLabel;
    cbTipoAsignacionTag: TComboBox;
    spObtenerDatosAlmacen: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelarClick(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure BtnSalirClick(Sender: TObject);
    function ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
    procedure ListaRefresh(Sender: TObject);
    procedure ListaEdit(Sender: TObject);
    procedure ListaClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure ListaInsert(Sender: TObject);
    procedure ListaDelete(Sender: TObject);
  private
    { Private declarations }
    procedure CargarConceptoMovimiento(var Combo: TVariantComboBox);
    procedure limpiarCampos;
    procedure HabilitarControles;
    function  ValidarDatos : Boolean;
    function  CargarAlmacenes( var DescError:string): boolean;
  public
    Function  Inicializar(Titulo:string) : boolean;
    { Public declarations }
  end;

var
  FormControlIndemnizaciones: TFormControlIndemnizaciones;

implementation

uses Main;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    sguastoni
  Date Created: 18/12/2006
  Description: Guarda los cambios en la base de datos
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.BtnAceptarClick(Sender: TObject);
begin
    //Se realizan los cambios.
    if Lista.Estado = Modi then begin
        if ValidarDatos then begin
            tblMotivosMovimientos.Edit;
            tblMotivosMovimientos.FieldByName('OperacionTagCAC').Value           := CbOperacion.Value;
            tblMotivosMovimientos.FieldByName('DescripcionMotivo').Value      := txtDescripcion.Text;
            tblMotivosMovimientos.FieldByName('CodigoAlmacen').Value          := CbAlmacen.Value;
            if (CbConcepto.ItemIndex = 0) then tblMotivosMovimientos.FieldByName('CodigoConcepto').Value := Null
            else tblMotivosMovimientos.FieldByName('CodigoConcepto').Value    := CbConcepto.Value;
            tblMotivosMovimientos.FieldByName('FechaHoraModificacion').Value  := NowBase(DMConnections.BaseCAC);
            tblMotivosMovimientos.FieldByName('UsuarioModificacion').Value    := UsuarioSistema;
            tblMotivosMovimientos.FieldByName('TipoAsignacionTag').Value      := cbTipoAsignacionTag.Text;
            tblMotivosMovimientos.Post;
        end else exit;
    end else if lista.Estado = Alta then begin
        if ValidarDatos then begin
            tblMotivosMovimientos.Append;
            tblMotivosMovimientos.FieldByName('OperacionTagCAC').Value           := CbOperacion.Value;
            tblMotivosMovimientos.FieldByName('DescripcionMotivo').Value      := txtDescripcion.Text;
            tblMotivosMovimientos.FieldByName('CodigoAlmacen').Value          := CbAlmacen.Value;
            if (CbConcepto.ItemIndex = 0) then tblMotivosMovimientos.FieldByName('CodigoConcepto').Value := Null
            else tblMotivosMovimientos.FieldByName('CodigoConcepto').Value    := CbConcepto.Value;
            tblMotivosMovimientos.FieldByName('FechaHoraCreacion').Value      := NowBase(DMConnections.BaseCAC);
            tblMotivosMovimientos.FieldByName('UsuarioCreacion').Value        := UsuarioSistema;
            tblMotivosMovimientos.FieldByName('FechaHoraModificacion').Value  := NowBase(DMConnections.BaseCAC);
            tblMotivosMovimientos.FieldByName('UsuarioModificacion').Value    := UsuarioSistema;
            tblMotivosMovimientos.FieldByName('TipoAsignacionTag').Value      := cbTipoAsignacionTag.Text;
            tblMotivosMovimientos.Post;
        end else exit;
    end;
    Lista.Estado := Normal;
    Notebook.PageIndex := 0;
    HabilitarControles;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:    sguastoni
  Date Created: 18/12/2006
  Description:  Cancela la operaci�n que alta o modificaci�n
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.BtnCancelarClick(Sender: TObject);
begin
    if (Lista.Estado in [Modi,Alta,Baja]) then Lista.Estado := Normal;
    Notebook.PageIndex := 0;
    HabilitarControles;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Sale del formulario
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.BtnSalirClick(Sender: TObject);
begin
   Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Cuando se cierra elimina el formulario
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name: HabilitarControles
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Habilita o deshabilita controles seg�n corresponda
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.HabilitarControles;
begin
    if lista.Estado in [Modi,Alta] then begin
        Lista.Enabled := False;
        CbOperacion.Enabled := True;
        txtDescripcion.Enabled := True;
        CbAlmacen.Enabled := True;
        CbConcepto.Enabled := True;
        cbTipoAsignacionTag.Enabled := True;
    end else begin
        Lista.Enabled := True;
        CbOperacion.Enabled := False;
        txtDescripcion.Enabled := False;
        CbAlmacen.Enabled := False;
        CbConcepto.Enabled := False;
        cbTipoAsignacionTag.Enabled := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarConceptoMovimiento
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Carga el combo con los conceptos de movimientos.
  Parameters:   Combo: TVariantComboBox
  Return Value:
-----------------------------------------------------------------------------}
function TFormControlIndemnizaciones.CargarAlmacenes( var DescError:string): boolean;
Resourcestring
    MSG_ERROR_ALMACEN = 'Error obteniendo datos de Almac�n';
begin
    Result := True;
    try
//        spObtenerDatosAlmacen.Parameters.ParamByName('@CodigoAlmacen').Value := CONST_ALMACEN_EN_COMODATO;
//        spObtenerDatosAlmacen.Open;
//        if not spObtenerDatosAlmacen.eof then CbAlmacen.Items.Add(spObtenerDatosAlmacen.FieldByName('Descripcion').AsString, CONST_ALMACEN_EN_COMODATO )
//        else begin
//            Result := False ;
//            DescError := MSG_ERROR_ALMACEN + ' ' + CONST_STR_ALMACEN_COMODATO ;
//        end;
//        spObtenerDatosAlmacen.Close;
//
//        spObtenerDatosAlmacen.Parameters.ParamByName('@CodigoAlmacen').Value := CONST_ALMACEN_ARRIENDO;
//        spObtenerDatosAlmacen.Open;
//        if not spObtenerDatosAlmacen.eof then CbAlmacen.Items.Add(spObtenerDatosAlmacen.FieldByName('Descripcion').AsString, CONST_ALMACEN_ARRIENDO )
//        else begin
//            if Result = False  then //ambos almacenes fallaron
//                DescError := MSG_ERROR_ALMACEN + ' ' + CONST_STR_ALMACEN_ARRIENDO + ' y ' + CONST_STR_ALMACEN_COMODATO
//            else begin
//                Result := False ;
//                DescError := MSG_ERROR_ALMACEN + ' ' + CONST_STR_ALMACEN_ARRIENDO ;
//            end;
//        end;
//        spObtenerDatosAlmacen.Close;

        // Rev.1 / 02-07-2009 / Nelson Droguett Sierra -----------------------------------------------------
        spObtenerDatosAlmacen.Parameters.ParamByName('@CodigoAlmacen').Value := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
        spObtenerDatosAlmacen.Open;
        if not spObtenerDatosAlmacen.eof then CbAlmacen.Items.Add(spObtenerDatosAlmacen.FieldByName('Descripcion').AsString, CONST_ALMACEN_ENTREGADO_EN_CUOTAS )
        else begin
            if Result = False  then // ya habia fallado al menos uno anterior, agrega este almacen al mensaje de error
                DescError := DescError + ' y ' + CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS
            else begin
                Result := False ;
                DescError := MSG_ERROR_ALMACEN + ' ' + CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS ;
            end;
        end;
        spObtenerDatosAlmacen.Close;
        //---------------------------------------------------------------------------------------------------
    except
        on E: Exception do begin
            DescError := E.Message ;
            Result := False;
        end;
    end;

end;

procedure TFormControlIndemnizaciones.CargarConceptoMovimiento(var Combo: TVariantComboBox);
ResourceString
    MSG_SELECCION = '(Ninguno)';
var
    FCodigoConceptoArriendo : Integer;
begin
    Combo.Clear;
    Combo.Items.Add(MSG_SELECCION,'0');
{INICIO: TASK_005_JMA_20160418		
            FCodigoConceptoArriendo := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA()');
}
			FCodigoConceptoArriendo := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}

    ObtenerConceptosComprobante.Parameters.ParamByName('@TipoComprobante').Value := TC_NOTA_COBRO;
    ObtenerConceptosComprobante.Open;
    while not ObtenerConceptosComprobante.EOF do begin
        if (FCodigoConceptoArriendo <> ObtenerConceptosComprobante.FieldByName('CodigoConcepto').AsInteger) then begin
            Combo.Items.Add( padl(ObtenerConceptosComprobante.FieldByName( 'CodigoConcepto' ).AsString ,4) + ' - ' + ObtenerConceptosComprobante.FieldByName( 'Descripcion' ).AsString ,
                                    ObtenerConceptosComprobante.FieldByName( 'CodigoConcepto' ).AsString );
        end;
        ObtenerConceptosComprobante.Next;
    end;
    ObtenerConceptosComprobante.Close;
    ObtenerConceptosComprobante.Parameters.ParamByName('@TipoComprobante').Value := TC_BOLETA;
    ObtenerConceptosComprobante.Open;
    while not ObtenerConceptosComprobante.EOF do begin
        if (FCodigoConceptoArriendo <> ObtenerConceptosComprobante.FieldByName('CodigoConcepto').AsInteger) then begin
            Combo.Items.Add( padl(ObtenerConceptosComprobante.FieldByName( 'CodigoConcepto' ).AsString ,4) + ' - ' + ObtenerConceptosComprobante.FieldByName( 'Descripcion' ).AsString ,
                                    ObtenerConceptosComprobante.FieldByName( 'CodigoConcepto' ).AsString );
            end;
        ObtenerConceptosComprobante.Next;
		end;
    ObtenerConceptosComprobante.Close;

    Combo.ItemIndex := 0;
end;


{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Inicializa el formulario
  Parameters:   Titulo:string
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormControlIndemnizaciones.Inicializar(Titulo:string) : boolean;
resourcestring
    MSG_ERROR = 'Error buscando datos de los conceptos de movimientos';
    TXT_ERROR = 'Error';
Var
	S: TSize;
    DescError: string;
begin
    Result := False;
	S := GetFormClientSize(Application.MainForm);
    CenterForm(self);
	//SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblMotivosMovimientos]) then exit;
    // filtro por el tipo de concepto
    CbOperacion.Items.Clear;
    CbOperacion.Items.Add(CONST_OPERACION_TAG_ELIMINAR_STR,CONST_OPERACION_TAG_ELIMINAR);
    CbOperacion.Items.Add(CONST_OPERACION_TAG_CAMBIAR_STR,CONST_OPERACION_TAG_CAMBIAR);
    CbOperacion.Items.Add(CONST_OPERACION_TAG_PERDIDO_STR,CONST_OPERACION_TAG_PERDIDO);
    CbOperacion.Items.Add(CONST_OPERACION_TAG_AGREGAR_STR,CONST_OPERACION_TAG_AGREGAR);
    CbOperacion.Items.Add(CONST_OPERACION_TAG_CBIOVEH_STR,CONST_OPERACION_TAG_CBIOVEH);
    CbOperacion.Items.Add(CONST_OPERACION_TAG_REACTIVAR_STR,CONST_OPERACION_TAG_REACTIVAR);

    CbOperacion.ItemIndex := 0;
    // se cargan los maestros de alamacenes.
    if not CargarAlmacenes(DescError) then begin
        MsgBoxErr(TXT_ERROR, DescError, Caption , MB_ICONSTOP);
        Result := False;
        Exit;
    end;

    // se cargan los conceptos movimientos
    CargarConceptoMovimiento(CbConcepto);
    //if CbAlmacen.Items.Count > 0 then CbAlmacen.ItemIndex := 0;

    cbTipoAsignacionTag.Items.Add( CONST_OP_TRANSFERENCIA_TAG_NUEVO );
    cbTipoAsignacionTag.Items.Add( CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA );


    Caption := Titulo;
    Lista.ReLoad;
    Notebook.PageIndex := 0;
    HabilitarControles;
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: limpiarCampos
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Limpia los controles.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.limpiarCampos;
begin
    // Aqui se limpian los campos.
    txtDescripcion.Clear;
    CbOperacion.ItemIndex := 0;
    CbAlmacen.ItemIndex := 0;
    CbConcepto.ItemIndex := 0;
    cbTipoAsignacionTag.ItemIndex := 0;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaClick
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Llena los controles con los valores en la grilla.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.ListaClick(Sender: TObject);
begin
    CbAlmacen.Value := tblMotivosMovimientos.FieldByName('CodigoAlmacen').Value;
    if tblMotivosMovimientos.FieldByName('CodigoConcepto').Value = Null then CbConcepto.Value := '0'
    else CbConcepto.Value := tblMotivosMovimientos.FieldByName('CodigoConcepto').Value;
    CbOperacion.Value := tblMotivosMovimientos.FieldByName('OperacionTagCAC').Value;
    txtDescripcion.Text := tblMotivosMovimientos.FieldByName('DescripcionMotivo').AsString;
    cbTipoAsignacionTag.ItemIndex := cbTipoAsignacionTag.Items.IndexOf(tblMotivosMovimientos.FieldByName('TipoAsignacionTag').AsString);

end;

{-----------------------------------------------------------------------------
  Function Name: ListaDelete
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Elimina un registro.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.ListaDelete(Sender: TObject);
resourcestring
    MSG_QUESTION_ELIMINAR = '�Esta seguro de eliminar el registro?';
begin
    if (MsgBox( MSG_QUESTION_ELIMINAR, Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin
        tblMotivosMovimientos.Delete;
    end;
    BtnCancelarClick(Self);
    HabilitarControles;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaDrawItem
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Dibuja la grilla.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.ListaDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);

        if (FieldByName('OperacionTagCAC').AsInteger = CONST_OPERACION_TAG_ELIMINAR) then TextOut(Cols[0], Rect.Top, CONST_OPERACION_TAG_ELIMINAR_STR)
        else if (FieldByName('OperacionTagCAC').AsInteger = CONST_OPERACION_TAG_CAMBIAR) then TextOut(Cols[0], Rect.Top, CONST_OPERACION_TAG_CAMBIAR_STR)
        else if (FieldByName('OperacionTagCAC').AsInteger = CONST_OPERACION_TAG_PERDIDO) then TextOut(Cols[0], Rect.Top, CONST_OPERACION_TAG_PERDIDO_STR)
        else if (FieldByName('OperacionTagCAC').AsInteger = CONST_OPERACION_TAG_AGREGAR) then TextOut(Cols[0], Rect.Top, CONST_OPERACION_TAG_AGREGAR_STR)
        else if (FieldByName('OperacionTagCAC').AsInteger = CONST_OPERACION_TAG_CBIOVEH) then TextOut(Cols[0], Rect.Top, CONST_OPERACION_TAG_CBIOVEH_STR)
        // Rev.1 / 02-0-20009 / Nelson Droguett Sierra ---------------------------------------------------------------------------------------------------
        else if (FieldByName('OperacionTagCAC').AsInteger = CONST_OPERACION_TAG_REACTIVAR) then TextOut(Cols[0], Rect.Top, CONST_OPERACION_TAG_REACTIVAR_STR);
        // -----------------------------------------------------------------------------------------------------------------------------------------------
        TextOut(Cols[1], Rect.Top, FieldByName('DescripcionMotivo').AsString);
        TextOut(Cols[2], Rect.Top, FieldByName('CodigoAlmacen').AsString);
        TextOut(Cols[3], Rect.Top, FieldByName('CodigoConcepto').AsString);
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEdit
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Edita los datos de la grilla.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.ListaEdit(Sender: TObject);
begin
    HabilitarControles;
    Notebook.PageIndex := 1;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaInsert
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Inserta datos en la base.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.ListaInsert(Sender: TObject);
Resourcestring
    MSG_ERROR_ALTA_REGISTRO = 'Error al hacer el alta en un registro';
    MSG_ERROR_DETALLE       = 'No se puede agregar un registro en la base de datos. Consulte con el administrador del sistema.';
begin
    HabilitarControles;
    limpiarCampos;
    Notebook.PageIndex := 1;
    if CbOperacion.CanFocus then CbOperacion.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaProcess
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Procesa los mensajes en la grilla.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormControlIndemnizaciones.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaRefresh
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Refresca la grilla.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormControlIndemnizaciones.ListaRefresh(Sender: TObject);
begin
    if Lista.Empty then limpiarCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarDatos
  Author:    sguastoni
  Date Created: 18/06/2006
  Description:  Valida los datos ingresado por el usuario.
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormControlIndemnizaciones.ValidarDatos: Boolean;
resourcestring
    MSG_ERROR_OPERACION = 'Debe elegir una Operaci�n v�lida';
    MSG_ERROR_ALMACEN   = 'Debe elegir un Almacen v�lido';
    MSG_ERROR_CONCEPTO  = 'Debe elegir un Concepto v�lido';
    MSG_ERROR_CONCEPTO_PRECIO_CERO  = 'El Concepto seleccionado no puede ser de precio cero';
    MSG_DESCRIPCION_NULA = 'Debe ingresar una descripci�n';
begin
    // Se validan datos.
    Result := True;
    if (CbOperacion.ItemIndex < 0) then  begin
        Result := False;
        MsgBoxBalloon(MSG_ERROR_OPERACION, caption, MB_ICONSTOP, CbOperacion);
        CbOperacion.SetFocus;
        exit;
    end;
    if (CbAlmacen.ItemIndex < 0) then  begin
        Result := False;
        MsgBoxBalloon(MSG_ERROR_ALMACEN, caption, MB_ICONSTOP, CbAlmacen);
        CbAlmacen.SetFocus;
        exit;
    end;
    if (CbConcepto.ItemIndex < 0) then  begin
        Result := False;
        MsgBoxBalloon(MSG_ERROR_CONCEPTO, caption, MB_ICONSTOP, CbConcepto);
        CbConcepto.SetFocus;
        exit;
    end;
    if (CbConcepto.ItemIndex > 0) and (QueryGetValueInt(DMConnections.BaseCAC,
                format('select dbo.ObtenerPrecioConceptoMovimiento(''%s'')',[cbConcepto.Value])) = 0) then  begin
        Result := False;
        MsgBoxBalloon(MSG_ERROR_CONCEPTO_PRECIO_CERO, caption, MB_ICONSTOP, CbConcepto);
        CbConcepto.SetFocus;
        exit;
    end;
    if (Trim(txtDescripcion.Text)='') then  begin
        Result := False;
        MsgBoxBalloon(MSG_DESCRIPCION_NULA, caption, MB_ICONSTOP, txtDescripcion);
        txtDescripcion.SetFocus;
        exit;
    end;
end;

end.
