unit for_ImprimirNotaCobroDirecta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, frm_ReporteNotaCobroDirecta,
  DMConnection;

type
  Tform_ImprimirNotaCobroDirecta = class(TForm)
    DBListEx1: TDBListEx;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
  private
    { Private declarations }
    function ImprimirNQ(NumeroComprobante: LongInt): Boolean;
  public
    { Public declarations }
  end;

var
  form_ImprimirNotaCobroDirecta: Tform_ImprimirNotaCobroDirecta;

implementation

{$R *.dfm}

function Tform_ImprimirNotaCobroDirecta.ImprimirNQ(NumeroComprobante: LongInt): Boolean;
resourcestring
	MSG_ERROR_GETTING_DETAIL 	= 'Error obteniendo datos de Facturación Detallada';
var
    f: Tform_ReporteNotaCobroDirecta;
    Error:String;
begin
    Screen.Cursor := crHourGlass;
    Result := False;
    Application.CreateForm(Tform_ReporteNotaCobroDirecta,f);
    try
        if f.Inicializar(NumeroComprobante) then
            Result := f.EjecutarReporte;
    finally
        f.Release;
        Screen.Cursor := crDefault;
    end;
    Screen.Cursor := crDefault;
end;

end.
