{-----------------------------------------------------------------------------
 File Name: frmReporteFacturacion
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 26/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura dentro del DFM

    Author      :   CquezadaI
    Date        :   05-Julio-2013
    Firma       :   SS_660_CQU_20130628
    Descripction:   Se agrega el numero de proceso de facturacion de morosos y normales
-----------------------------------------------------------------------------}
unit frmReporteFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd, ppClass,
  ppReport, UtilRB, DMConnection, DBClient, ppStrtch, ppCTMain, ppPrnabl,
  ppCtrls, ppBands, ppCache, ppDBBDE, Provider, UtilProc, PeaProcs,
  ppSubRpt, ppModule, raCodMod, ppParameter, ConstParametrosGenerales, Util;

type
  TformReporteFacturacion = class(TForm)
	rbi1: TRBInterface;
    ppReporteFacturacion: TppReport;
    dsReporte: TDataSource;
    spObtenerReporte: TADOStoredProc;
    qryAgenda: TADOQuery;
    dsAgenda: TDataSource;
    qryAgendaAo: TIntegerField;
    qryAgendaCodigoGrupoFacturacion: TWordField;
    qryAgendaCiclo: TWordField;
    qryAgendaFechaProgramadaEjecucion: TDateTimeField;
    qryAgendaFechaCorte: TDateTimeField;
    qryAgendaFechaEmision: TDateTimeField;
    qryAgendaFechaVencimiento: TDateTimeField;
    qryAgendaFechaImpresion: TDateTimeField;
    qryAgendaFechaRealFacturacion: TDateTimeField;
    qryAgendaFactorFrecuencia: TBCDField;
    ppdbAgenda: TppDBPipeline;
    ppdbReporte: TppDBPipeline;
    ppParameterList1: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppDBText17: TppDBText;
    ppLabel19: TppLabel;
    ppDBText16: TppDBText;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppLabel13: TppLabel;
    ppDBText11: TppDBText;
    ppLabel14: TppLabel;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppDBText14: TppDBText;
    ppLabel12: TppLabel;
    ppDBText15: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel20: TppLabel;
    ppLabel26: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    raCodeModule1: TraCodeModule;
    raCodeModule2: TraCodeModule;
    pplblTiempoInsumido: TppLabel;
    ppShape2: TppShape;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppLabel2: TppLabel;
    ppShape1: TppShape;
    ppTitleBand2: TppTitleBand;
    ppImgLogoCN: TppImage;
    pplblfecha: TppLabel;
    ppLabel4: TppLabel;
    pplblUserName: TppLabel;
    spObtenerReportePrimeraNotaCobro: TLargeintField;
    spObtenerReporteUltimaNotaCobro: TLargeintField;
    spObtenerReporteCantidadNotasCobro: TLargeintField;
    spObtenerReporteTotalAPagar: TBCDField;
    spObtenerReportedesc_TotalAPagar: TStringField;
    spObtenerReporteTotalPeajesPeriodo: TBCDField;
    spObtenerReportedesc_TotalPeajesPeriodo: TStringField;
    spObtenerReporteTotalPeajesPerAnterior: TBCDField;
    spObtenerReportedesc_TotalPeajesPerAnterior: TStringField;
    spObtenerReporteTotalIntereses: TBCDField;
    spObtenerReportedesc_TotalIntereses: TStringField;
    spObtenerReporteTotalFacturacionVencida: TBCDField;
    spObtenerReportedesc_TotalFacturacionVencida: TStringField;
    spObtenerReporteCodigoUsuario: TStringField;
    spObtenerReporteNombreUsuario: TStringField;
    spObtenerReporteNumeroProcesoFacturacion: TIntegerField;
    ppdbAgendappField11: TppField;                // SS_660_CQU_20130628
    spObtenerReporteAmbosProcesos: TStringField;  // SS_660_CQU_20130628
    ppdbReporteppField17: TppField;               // SS_660_CQU_20130628
    procedure cdsAgendaAfterClose(DataSet: TDataSet);
    procedure rbi1Execute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FNombreImagenLogo: string;
    FdirLogo: String;
    FTiempoInsumido: TDateTime;
    FNumeroProcesofacturacion: integer;
  public
    { Public declarations }
    function Inicializa( NumeroProcesoFacturacion: Integer; Tiempo: TDateTime): boolean;
    function Ejecutar: Boolean;
end;

var
  formReporteFacturacion: TformReporteFacturacion;

implementation

{$R *.dfm}
function TformReporteFacturacion.Inicializa( NumeroProcesoFacturacion: Integer; Tiempo: TDateTime): boolean;
resourcestring
    MSG_MISSED_IMAGES_FILES = 'Falta el archivo de imagen del Logo de Costanera Norte'  ;
begin
    Result := True;
    screen.Cursor := crHourglass;
    try
        FTiempoInsumido := Tiempo;
        FNumeroProcesofacturacion := NumeroProcesoFacturacion;
        ObtenerParametroGeneral(DMConnections.BaseCAC , 'DIR_LOGO_NOTA_COBRO', FDirLogo);
        ObtenerParametroGeneral(DMConnections.BaseCAC , 'NOMBRE_LOGO_NOTA_COBRO', FNombreImagenLogo);
        FNombreImagenLogo := GoodDir(FDirLogo) + FNombreImagenLogo;
        if (not FileExists(FNombreImagenLogo)) then begin
           MsgBox(MSG_MISSED_IMAGES_FILES,'Advertencia',MB_ICONWARNING);
        end;
    finally
        screen.Cursor := crDefault;
    end;
end;

function TformReporteFacturacion.Ejecutar:Boolean;
begin
    Result := True;
    try
        if NOT rbi1.Execute(True) then exit;
    except
        Result := False;
    end;
end;

procedure TformReporteFacturacion.cdsAgendaAfterClose(DataSet: TDataSet);
begin
    Free;
end;

procedure TformReporteFacturacion.rbi1Execute(Sender: TObject;
  var Cancelled: Boolean);
resourcestring
    MSG_ERROR = 'No se puede imprimir el reporte.';
begin
    try
        pplblTiempoInsumido.Caption := 'Tiempo insumido en la Facturación: ' +FormatDateTime('hh:nn:ss', FTiempoInsumido);
        pplblUserName.caption := 'Usuario: '+UsuarioSistema;
        pplblFecha.Caption := 'Fecha de Impresión: '+DateTimeToStr(NowBase(DMConnections.BaseCAC));
        spObtenerReporte.Parameters.ParamByName('@NUMPROCFACT').Value := FNumeroProcesoFacturacion;
        spObtenerReporte.Open ;

        qryAgenda.Parameters.ParamByName('NUMPROCFACT').Value := FNumeroProcesoFacturacion;
        qryAgenda.Open;
        qryAgenda.Refresh;               
        qryAgenda.Active := True;
        if FileExists(FNombreImagenLogo) then ppImgLogoCN.Picture.LoadFromFile(FNombreImagenLogo);
    except
        on e:exception do begin
            MsgBoxErr(MSG_ERROR,e.Message,'ERROR',MB_ICONSTOP);
            Cancelled := True;
        end;
    end;
end;

end.
