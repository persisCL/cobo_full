{-------------------------------------------------------------------
        	frmTipificacionMasiva

 Author: mbecerra
 Date: 12-Febrero-2009
 Description:	Realiza las siguientes tareas de la facturaci�n electr�nica:
                	* Tipificaci�n de Comprobantes
                    * Numeraci�n de Comprobantes
                    * Aplicar (actualizar saldo del Convenio)
                    * Generar el registro en las base de DBNet

 Revision 1:
 Author: mbecerra
 Date: 30-Mayo-2009
 Description:	Se desea que el sistema despliegue todos los procesos
            	de facturaci�n que tengan comprobantes sin Tipificar.

                Por lo tanto se cambia la componente
                		neProcesoFacturacion : TNumericEdit
                por una
                    	cbxProcesoFacturacion: TComboBox

                y se agrega un nuevo stored procedure:
                		ObtenerProcesosFacturacionSinTerminarTipificacion

 Revision 2
 Author: mbecerra
 Date: 29-Enero-2010
 Description:			(Ref Fase II)
            	Se agrega una nueva funci�n: InicializarDesatendido, la cual
                realiza el proceso completo de tipificaci�n para un
                numero de proceso dado.

 Firma          :   SS_933_MBE_20111102_2130
 Description    :   Se modifican y agregan algunos mensajes, para
                    mejor compresi�n del operador
                    
 Revision 3
 Author         :   cquezada
 Date           :   22-Marzo-2012
 Description    :   Se agrega el reenv�o a la cola de DBNet.
 Frima          :   SS_330_CQU_20120322

 Firma          :   SS_330_CQU_20120419
 Description    :   Se agerga un registro al log de interfase,
                    la idea es que si existe error en el proceso
                    de reenv�o a la cola, se registre dicho error.

 Firma          :   SS_330_CQU_20120420
 Description    :   Se modifica el desarrollo eliminado la obtenci�n de un dato
                    con el que se registraban los errores, la obtenci�n de
                    dicho dato no es necesaria hacerla porque ya existe.

 Firma          :   SS_330_CQU_20120426
 Description    :   Modifica el mensaje de error al reintentar el env�o a DBNet.
 --------------------------------------------------------------------}
 
unit frmTipificacionMasiva;

interface

uses
	DMConnection,
    UtilProc,
    PeaProcs,
    UtilDB,
    Util,
    frmReporteTipificacionYNumeracionInterfaces,
    ThreadTipificacion,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, ExtCtrls, DB, ADODB, ComCtrls, Buttons;

type
  TTipificacionMasivaForm = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    btnComenzar: TButton;
    btnSalir: TButton;
    mmoLog: TMemo;
    Label2: TLabel;
    pgbNumerados: TProgressBar;
    tmerProgreso: TTimer;
    lblTotal: TLabel;
    spCrearLeerBorrarLogTYNM: TADOStoredProc;
    btnDetener: TButton;
    cbxNumeroProcesoFacturacion: TComboBox;
    spObtenerProcesosFacturacionSinTerminarTipificacion: TADOStoredProc;
    btnBuscar: TSpeedButton;
    tmerForm: TTimer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnComenzarClick(Sender: TObject);
    procedure tmerProgresoTimer(Sender: TObject);
    procedure btnDetenerClick(Sender: TObject);
    procedure cbxNumeroProcesoFacturacionKeyPress(Sender: TObject;
      var Key: Char);
    procedure btnBuscarClick(Sender: TObject);
    procedure tmerFormTimer(Sender: TObject);

  private
    { Private declarations }
    FEsDesatendida : boolean;		//REV.2
  public
    { Public declarations }
    function Inicializar : boolean;
    procedure CargarProcesosFacturacion;
    procedure MensajeLog(Msg : string; EsLineaNueva : boolean);
    function CrearLeerBorrarTablaTemporal(Accion : byte): boolean;
    function CrearLeerTablaTemporalDetencion(Accion: byte; ValorAInsertar : integer = -1) : boolean;
    procedure VerReporte( CodigoOperacionInterface : integer);

    //REV.2
    function InicializarDesatendido (NumeroProcesoFacturacion : integer) : boolean;
  end;

var
  TipificacionMasivaForm: TTipificacionMasivaForm;

implementation

{$R *.dfm}

const
	// Constantes relacionadas a la tabla temporal #LogTYNM
    cCrearTabla		= 1;
    cLeerTabla		= 2;
    cBorrarTabla	= 3;
    cGrabarTabla	= 4;

var
	//cantidad de registros procesados y totales
    ValorDetencion : integer;
    TotalRegistros,
    RegistrosProcesados,
    RegistrosExitosos : int64;
    CargandoTabla,                  // cuando lee la primera vez que el total de registros es cero
    EsTablaTemporal,                //indica si debe o no mostrar el mensaje de finalizaci�n
    StoredTrabajando : boolean;		// indica que el stored est� en proceso


{-------------------------------------------------------
        Inicializar

Author: mbecerra
Date: 12-Febrero-2009
Description:	Inicializa el Formulario
--------------------------------------------------------}
function TTipificacionMasivaForm.Inicializar;
resourcestring
	MSG_CAPTION	= 'Facturaci�n Electr�nica: Tipificaci�n y Numeraci�n Masiva';
begin
	Caption := MSG_CAPTION;
    CenterForm(Self);
    mmoLog.Clear;
    pgbNumerados.Visible := False;
    lblTotal.Caption := '';
    btnDetener.Visible := False;
    cbxNumeroProcesoFacturacion.Clear;
    FEsDesatendida := False;			//REV.2
    Result := True;
end;

{-------------------------------------------------------
        InicializarDesatendido

Author: mbecerra
Date: 29-Enero-2010
Description:	Inicializa el proceso completo de tipificaci�n de manera
            	desatendida
--------------------------------------------------------}
function TTipificacionMasivaForm.InicializarDesatendido;
begin
	Result := Inicializar();
    if not Result then Exit;

    FEsDesatendida := True;
    cbxNumeroProcesoFacturacion.Text := IntToStr(NumeroProcesoFacturacion);
    tmerForm.Enabled := True;
end;

{-------------------------------------------------------
        CargarProcesosFacturacion

Author: mbecerra
Date: 30-Mayo-2009
Description:	Recupera los procesos de facturaci�n con
            	Comprobantes sin Tipificar
--------------------------------------------------------}
procedure TTipificacionMasivaForm.CargarProcesosFacturacion;
resourcestring
	MSG_ERROR = 'Ha ocurrido un error al intentar obtener los Procesos de Facturaci�n';
    
begin
    Screen.Cursor := crHourGlass;
    try
    	cbxNumeroProcesoFacturacion.Clear;
    	with spObtenerProcesosFacturacionSinTerminarTipificacion do begin
    		Close;
            CommandTimeout := 600;
        	Open;
        	while not Eof do begin
            	cbxNumeroProcesoFacturacion.Items.Add(Fields[0].AsString);
        		Next;
        	end;
        end;
    except on e:exception do begin
         MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

    Screen.Cursor := crDefault;
end;


{-------------------------------------------------------
        cbxNumeroProcesoFacturacionKeyPress

Author: mbecerra
Date: 30-Mayo-2009
Description:	Fiuerza a escritura de s�lo n�meros
--------------------------------------------------------}
procedure TTipificacionMasivaForm.cbxNumeroProcesoFacturacionKeyPress(
  Sender: TObject; var Key: Char);
begin
	if not (Key in [#8, '0'..'9']) then Key := #0;

end;

{-------------------------------------------------------
        VerReporte

Author: mbecerra
Date: 24-Marzo-2009
Description:	Muestra el Reporte de la ejecuci�n de la operaci�n de Tipificaci�n
--------------------------------------------------------}
procedure TTipificacionMasivaForm.VerReporte;
resourcestring
    MSG_CONTINUAR = 'Desea visualizar el reporte?';

var
	Reporte : TReporteTipificacionYNumeracionInterfacesForm;
begin
	if (not FEsDesatendida) and (CodigoOperacionInterface > 0) then begin    //REV.2
    	if MsgBox(MSG_CONTINUAR, Caption, MB_YESNO) = IDYES then begin
        	Application.CreateForm(TReporteTipificacionYNumeracionInterfacesForm,Reporte );
            if not Reporte.Inicializar(CodigoOperacionInterface) then Reporte.Release;
        end;
    end;
end;

{-------------------------------------------------------
        CrearLeerTablaTemporalDetencion

Author: mbecerra
Date: 07-Abril-2009
Description:	Crea, lee y borra la tabla Temporal que posibilita la detenci�n del
            	stored de Tipificacion masiva

                Valores posibles son:
                	NULL	= stored ejecutandose sin peticion de detencion
                    1       = stored termin� de manera normal
                    2		= petici�n de detenci�n enviada, sin confirmar
                    3		= petici�n de detenci�n confirmada
--------------------------------------------------------}
function TTipificacionMasivaForm.CrearLeerTablaTemporalDetencion;
resourcestring
	SQL_CREAR	= 'CREATE TABLE ##Finalizar_Ejecucion (Finalizar TINYINT)';
    SQL_BORRAR	= 'DROP TABLE ##Finalizar_Ejecucion';
    SQL_LEER	= 'SELECT Finalizar FROM ##Finalizar_Ejecucion (NOLOCK)';
    SQL_INSERT	= 'INSERT INTO ##Finalizar_Ejecucion VALUES (%d)';
    MSG_FALLA	= 'ERROR: %s Tabla temporal Detenci�n: %s';
    MSG_CREAR	= 'CREAR';
    MSG_LEER	= 'LEER';
    MSG_BORRAR	= 'ELIMINAR';
begin
	Result := False;
	try
    	if Accion = cCrearTabla then begin
        	QueryExecute(DMConnections.BaseCAC, SQL_CREAR);  //se usa la conexi�n del programa para que no se borre la tabla si se cierra la otra conexi�n
    	end
        else if Accion = cLeerTabla then begin
            ValorDetencion := QueryGetValueInt(DMConnections.BaseCAC, SQL_LEER);
        end
        else if Accion = cBorrarTabla then begin
            QueryExecute(DMConnections.BaseCAC, SQL_BORRAR);
        end
        else if Accion = cGrabarTabla then begin
             QueryExecute(DMConnections.BaseCAC, Format(SQL_INSERT, [ValorAInsertar]));
        end;

        Result := True;

    except on e:exception do begin
            if Accion = cCrearTabla then MensajeLog(Format(MSG_FALLA, [MSG_CREAR, e.Message]), True)
            else if Accion = cLeerTabla then MensajeLog(Format(MSG_FALLA, [MSG_LEER, e.Message]), True)
            else if Accion = cBorrarTabla then MensajeLog(Format(MSG_FALLA, [MSG_BORRAR, e.Message]), True)
            else MensajeLog(e.Message, True);
    	end;
    end;
end;

{-------------------------------------------------------
        CrearLeerBorrarTablaTemporal

Author: mbecerra
Date: 17-Febrero-2009
Description:	Crea, lee y borra la tabla Temporal que muestra el progreso
            	del proceso de Tipificaci�n
--------------------------------------------------------}
function TTipificacionMasivaForm.CrearLeerBorrarTablaTemporal;
resourcestring
	MSG_FALLA	= 'ERROR: %s Tabla temporal : %s';
    MSG_CREAR	= 'CREAR';
    MSG_LEER	= 'LEER';
    MSG_BORRAR	= 'ELIMINAR';
begin
	Result := False;
    EsTablaTemporal := True;
	try
    	spCrearLeerBorrarLogTYNM.Close;
    	spCrearLeerBorrarLogTYNM.Parameters.ParamByName('@Accion').Value	:= Accion;
    	spCrearLeerBorrarLogTYNM.Parameters.ParamByName('@Total').Value 	:= -1;
    	spCrearLeerBorrarLogTYNM.Parameters.ParamByName('@Progreso').Value	:= -1;
        spCrearLeerBorrarLogTYNM.Parameters.ParamByName('@Exitosos').Value	:= -1;
    	spCrearLeerBorrarLogTYNM.ExecProc;
    	if Accion = cLeerTabla then begin
    		TotalRegistros		:= spCrearLeerBorrarLogTYNM.Parameters.ParamByName('@Total').Value;
        	RegistrosProcesados := spCrearLeerBorrarLogTYNM.Parameters.ParamByName('@Progreso').Value;
            RegistrosExitosos	:= spCrearLeerBorrarLogTYNM.Parameters.ParamByName('@Exitosos').Value;
    	end;

        Result := True;
    except on e:exception do begin
            if Accion = cCrearTabla then MensajeLog(Format(MSG_FALLA, [MSG_CREAR, e.Message]), True)
            else if Accion = cLeerTabla then MensajeLog(Format(MSG_FALLA, [MSG_LEER, e.Message]), True)
            else if Accion = cBorrarTabla then MensajeLog(Format(MSG_FALLA, [MSG_BORRAR, e.Message]), True)
            else MensajeLog(e.Message, True);
    	end;
    end;

    EsTablaTemporal := False;
end;

{-------------------------------------------------------
        MensajeLog

Author: mbecerra
Date: 12-Febrero-2009
Description:	Despliega un mensaje en el Log
--------------------------------------------------------}
procedure TTipificacionMasivaForm.MensajeLog;
begin
    if not EsLineaNueva then mmoLog.Lines[0] := mmoLog.Lines[0] + Msg
    //else mmoLog.Lines.Insert(0, FormatDateTime('yyyy-mm-dd hh:nn:ss', Now) + ' --> ' + Msg);      // TASK_127_MGO_20170127
    else mmoLog.Lines.Add(FormatDateTime('yyyy-mm-dd hh:nn:ss', Now) + ' --> ' + Msg);              // TASK_127_MGO_20170127

    mmoLog.Refresh;
end;

{-------------------------------------------------------
        tmerFormTimer

Author: mbecerra
Date: 29-Enero-2010
Description:	Inicia el proceso de Tipificaci�n desatendida, invocando
            	al bot�n btnComenzar.

                Se hace as� para que:
                1.-		La funci�n InicializarDesatendido devuelva el control
                    	al formulario anterior
                2.-		Al demorarse el Timer en arrancar, permite la visualizaci�n
                    	del formulario en pantalla
--------------------------------------------------------}
procedure TTipificacionMasivaForm.tmerFormTimer(Sender: TObject);
begin
	tmerForm.Enabled := False;
    btnComenzarClick(btnComenzar);
end;

{-------------------------------------------------------
        tmerProgresoTimer

Author: mbecerra
Date: 17-Febrero-2009
Description:	Lee desde la tabla temporal el avance en el proceso de numeraci�n
            	y refresca en pantalla ese avance
--------------------------------------------------------}
procedure TTipificacionMasivaForm.tmerProgresoTimer(Sender: TObject);
resourcestring
	MSG_TOTAL	= '%.0n  de  %.0n';
    MSG_DATOS   = 'Cargando comprobantes a procesar...';
    MSG_DETENCION_NORMAL	= 'Proceso finalizado completamente';               //SS_933_MBE_20111102_2130
    MSG_DETENCION_USUARIO	= 'Proceso ha sido detenido por el usuario';
begin
    CrearLeerBorrarTablaTemporal(cLeerTabla);

    //mostrar el avance en el proceso de tipificaci�n
    if TotalRegistros > 0 then pgbNumerados.Max := TotalRegistros;
	if RegistrosProcesados > 0 then pgbNumerados.Position := RegistrosProcesados;
    if (TotalRegistros > 0) and (RegistrosProcesados >= 0) then begin
    	lblTotal.Caption := Format(MSG_TOTAL, [1.0 * RegistrosProcesados, 1.0 * TotalRegistros]);
    end;

    // ver si debe desplegar el mensaje que se est� cargando la tabla temporal
    if CargandoTabla and (TotalRegistros = 0) then begin
    	CargandoTabla := False;
        MensajeLog(MSG_DATOS, True);
    end;

    //ver el estado de detenci�n del stored
    if CrearLeerTablaTemporalDetencion(cLeerTabla) then begin
    	if ValorDetencion = 1 then MensajeLog(MSG_DETENCION_NORMAL, True);
        if ValorDetencion = 3 then MensajeLog(MSG_DETENCION_USUARIO, True);
        StoredTrabajando := not (ValorDetencion in [1,3]);
    end;

end;

{-------------------------------------------------------
        btnBuscarClick

Author: mbecerra
Date: 30-Mayo-2009
Description:	Llama a la funci�n de b�squeda de Procesos de Facturaci�n
--------------------------------------------------------}
procedure TTipificacionMasivaForm.btnBuscarClick(Sender: TObject);
resourcestring
	MSG_TARDAR	= 'Esto puede tardar algunos minutos �Est� seguro de continuar?';
    MSG_BUSCAR	= 'Buscando Procesos de facturaci�n...';

begin
    if MsgBox(MSG_TARDAR, Caption, MB_ICONQUESTION + MB_YESNO) = IDYES then begin
    	mmoLog.Clear;
        MensajeLog(MSG_BUSCAR, True);
    	CargarProcesosFacturacion();
        mmoLog.Clear;
        cbxNumeroProcesoFacturacion.SetFocus;
        cbxNumeroProcesoFacturacion.Perform(CB_SHOWDROPDOWN,1,0);
    end;
end;

{-------------------------------------------------------
        btnComenzarClick

Author: mbecerra
Date: 12-Febrero-2009
Description:	Ejecuta el ciclo de Numeraci�n Fiscal, para la Facturaci�n Electr�nica.
--------------------------------------------------------}
procedure TTipificacionMasivaForm.btnComenzarClick(Sender: TObject);
resourcestring
    MSG_TOTAL		= 'Total Registros : %.0n,  Total Procesados: %.0n,  Total exitosos: %.0n';
    MSG_FALTA_NPF	= 'No ha indicado el N�mero de Proceso de Facturaci�n';
    MSG_ERROR		= 'Ocurri� un error al ejecutar el proceso: ';
    MSG_PROCESANDO	= 'Procesando...';
    MSG_NUM_LOG     = 'N�mero Proceso facturaci�n: %.0n';                //SS_933_MBE_20111102_2130
    //MSG_DBNET_ERROR = 'Error al insertar el proceso de facturaci�n en la cola de env�o a DBNet.'; // SS_330_CQU_20120322
    MSG_DBNET_ERROR = 'Error al reinsertar el proceso de facturaci�n en la cola de env�o a DBNet.'; // SS_330_CQU_20120426
var
    LogOperaciones : integer;
    ProcesoEnColaDBNet : integer; // SS_330_CQU_20120322
    ConsultaSQL : string; // SS_330_CQU_20120322
begin
    if not ValidateControls(	[cbxNumeroProcesoFacturacion], [cbxNumeroProcesoFacturacion.Text <> ''],
    							Caption, [MSG_FALTA_NPF]) then Exit;

    try

        //Crear la tabla temporal para dar la posibilidad al usuario de
        // detener el proceso
        CrearLeerTablaTemporalDetencion(cCrearTabla);

        //comenzar el proceso
    	btnComenzar.Enabled := False;
        lblTotal.Caption := '';
    	mmoLog.Clear;
        pgbNumerados.Position := 0;
        pgbNumerados.Visible := True;
        CargandoTabla		:= True;
    	TotalRegistros		:= -1;
    	RegistrosProcesados	:= -1;
        RegistrosExitosos	:= -1;


       //Crear la tabla Temporal
        if CrearLeerBorrarTablaTemporal(cCrearTabla) then begin
        	//ejecutar el stored de tipificacion en un Thread
            TTYNMasivaThread.Create(StrToInt(cbxNumeroProcesoFacturacion.Text), UsuarioSistema, LogOperaciones);
            btnDetener.Visible := True;
			tmerProgreso.Enabled := True;
        	StoredTrabajando	:= True;
            MensajeLog(MSG_PROCESANDO, True);
        	//esperar que finalice el stored
        	while StoredTrabajando do Application.ProcessMessages;

            // SS_330_CQU_20120322
            // Genero Consulta ya que el procedimiento no devuelve nada si no es de esta manera
            ConsultaSQL := 'DECLARE @Retorno INT'
            + ' EXEC @Retorno = InsertarNumeroProcesoFacturacionEnColaEnvioDBNet '
            + cbxNumeroProcesoFacturacion.Text
            + #10 + #13
            + ' SELECT @retorno AS Retorno';
            // Ejecuto y Parseo el resultado
            ProcesoEnColaDBNet := QueryGetValueInt(DMConnections.BaseCAC, ConsultaSQL);
            // Eval�o el resultado y despliego mensaje si es necesario
            if (ProcesoEnColaDBNet = -1) then
            begin
                    // SS_330_CQU_20120419
                    ConsultaSQL := 'DECLARE @CodigoOperacionInterfase INT' + CRLF
                    + ' DECLARE @TipoErrorSQL INT' + CRLF
                    + ' SET @TipoErrorSQL = dbo.CONST_IH_INTERFACES_ERROR_SQL()' + CRLF

                    // [Eliminaci�n de C�digo Inservible] // SS_330_CQU_20120420

                    // Grabo el error en el log de operaciones
                    + ' EXEC AgregarErrorInterfase ' + IntToStr(LogOperaciones)
                    + ', ''' + MSG_DBNET_ERROR + ''''
                    + ', @TipoErrorSQL ';
                    // Ejecuto la consulta completa
                    QueryExecute(DMConnections.BaseCAC, ConsultaSQL);
                    // SS_330_CQU_20120419
                    MensajeLog(MSG_DBNET_ERROR, True);
            end;
            // SS_330_CQU_20120322

            tmerProgreso.Enabled := False;

            //Borrar tabla temporal
            CrearLeerBorrarTablaTemporal(cBorrarTabla);
            CrearLeerTablaTemporalDetencion(cBorrarTabla);

            //mostrar el proceso de facturaci�n
            MensajeLog(Format(MSG_NUM_LOG, [1.0 * LogOperaciones]), True);       //SS_933_MBE_20111102_2130
            
            //indicar estad�sticas
            MensajeLog(Format(MSG_TOTAL, [	1.0 * TotalRegistros,
            								1.0 * RegistrosProcesados,
                                            1.0 * RegistrosExitosos]), True);
            VerReporte(LogOperaciones);

        end;

    except on e:exception do begin
            MensajeLog(MSG_ERROR + e.Message, True);
    	end;
    end;

 //   adcBASE_CAC.Connected	:= False;
    btnDetener.Visible		:= False;
    btnComenzar.Enabled		:= True;
    pgbNumerados.Visible	:= False;
    tmerProgreso.Enabled	:= False;
    lblTotal.Caption		:= '';
end;

{-------------------------------------------------------
        btnSalirClick

Author: mbecerra
Date: 12-Febrero-2009
Description:	Cierra el formulario principal.
--------------------------------------------------------}
procedure TTipificacionMasivaForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

{-------------------------------------------------------
        FormClose

Author: mbecerra
Date: 12-Febrero-2009
Description:	Libera el formulario desde memoria.
--------------------------------------------------------}
procedure TTipificacionMasivaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

{-------------------------------------------------------
        btnDetenerClick

Author: mbecerra
Date: 07-Abril-2009
Description:	Intenta detener el proceso masivo.
--------------------------------------------------------}
procedure TTipificacionMasivaForm.btnDetenerClick(Sender: TObject);
resourcestring
	MSG_SEGURO	= 'Est� seguro de querer detener el proceso?';
    MSG_DETENER	= 'Intentando detener el proceso masivo...';
begin
    if MsgBox(MSG_SEGURO, Caption, MB_ICONQUESTION + MB_YESNO) = IDYES then begin
    	if CrearLeerTablaTemporalDetencion(cGrabarTabla, 2) then MensajeLog(MSG_DETENER, True);
    end;

end;

end.
