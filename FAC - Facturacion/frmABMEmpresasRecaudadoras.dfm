object TABMEmpresasRecaudadorasForm: TTABMEmpresasRecaudadorasForm
  Left = 0
  Top = 0
  Caption = 'Empresas Recaudadoras'
  ClientHeight = 708
  ClientWidth = 1199
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object tbEmpresasRecaudadoras: TAbmToolbar
    Left = 0
    Top = 0
    Width = 1199
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar, btImprimir]
    OnClose = tbEmpresasRecaudadorasClose
  end
  object pnlEmpresasRecaudadoras: TPanel
    Left = 0
    Top = 248
    Width = 1199
    Height = 419
    Align = alBottom
    Enabled = False
    TabOrder = 1
    Visible = False
    object lblCodigo: TLabel
      Left = 52
      Top = 24
      Width = 41
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombreEmpresa: TLabel
      Left = 46
      Top = 46
      Width = 47
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDescripcion: TLabel
      Left = 25
      Top = 156
      Width = 68
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblAliasEmpresa: TLabel
      Left = 63
      Top = 102
      Width = 30
      Height = 13
      Caption = 'Alias:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 67
      Top = 129
      Width = 26
      Height = 13
      Caption = 'Giro:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNumeroDocumento: TLabel
      Left = 67
      Top = 75
      Width = 26
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbleMail: TLabel
      Left = 60
      Top = 183
      Width = 33
      Height = 13
      Caption = 'eMail:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtNombreEmpresa: TEdit
      Left = 99
      Top = 43
      Width = 433
      Height = 21
      MaxLength = 60
      TabOrder = 1
    end
    object neCodigoEmpresasRecaudadoras: TNumericEdit
      Left = 99
      Top = 16
      Width = 66
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
    end
    inline freDomicilio: TFrameDomicilio
      Left = 32
      Top = 208
      Width = 953
      Height = 125
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      TabOrder = 8
      TabStop = True
      ExplicitLeft = 32
      ExplicitTop = 208
      ExplicitWidth = 953
      ExplicitHeight = 125
      inherited Pnl_Arriba: TPanel
        Width = 953
        ExplicitWidth = 953
        inherited Label14: TLabel
          Left = 16
          ExplicitLeft = 16
        end
        inherited Label6: TLabel
          Left = 28
          ExplicitLeft = 28
        end
        inherited cb_Regiones: TComboBox
          Left = 67
          Top = -1
          ExplicitLeft = 67
          ExplicitTop = -1
        end
        inherited txt_numeroCalle: TEdit
          TabOrder = 4
        end
        inherited cb_BuscarCalle: TComboBuscarCalle
          Left = 67
          TabOrder = 3
          ExplicitLeft = 67
        end
        inherited txt_CodigoPostal: TEdit
          TabOrder = 2
        end
      end
      inherited Pnl_Abajo: TPanel
        Width = 953
        ExplicitWidth = 953
        inherited Label25: TLabel
          Left = 25
          ExplicitLeft = 25
        end
        inherited txt_Detalle: TEdit
          Left = 67
          Top = 1
          ExplicitLeft = 67
          ExplicitTop = 1
        end
      end
      inherited Pnl_Medio: TPanel
        Left = -6
        Top = 76
        Height = 39
        ExplicitLeft = -6
        ExplicitTop = 76
        ExplicitHeight = 39
      end
    end
    object txtDescripcion: TEdit
      Left = 99
      Top = 153
      Width = 433
      Height = 21
      MaxLength = 255
      TabOrder = 6
    end
    object txtAliasEmpresa: TEdit
      Left = 99
      Top = 99
      Width = 433
      Height = 21
      MaxLength = 60
      TabOrder = 4
    end
    object txtGiro: TEdit
      Left = 99
      Top = 126
      Width = 433
      Height = 21
      MaxLength = 40
      TabOrder = 5
    end
    object txtNumeroDocumento: TEdit
      Left = 99
      Top = 72
      Width = 78
      Height = 21
      MaxLength = 8
      TabOrder = 2
    end
    object txtCodigoDocumento: TEdit
      Left = 183
      Top = 72
      Width = 30
      Height = 21
      MaxLength = 1
      TabOrder = 3
    end
    object txteMail: TEdit
      Left = 99
      Top = 180
      Width = 433
      Height = 21
      MaxLength = 255
      TabOrder = 7
    end
    inline freTelefono: TFrameTelefono
      Left = 99
      Top = 329
      Width = 697
      Height = 49
      TabOrder = 9
      TabStop = True
      ExplicitLeft = 99
      ExplicitTop = 329
      ExplicitWidth = 697
      ExplicitHeight = 49
    end
    object chkJudicial: TCheckBox
      Left = 347
      Top = 70
      Width = 97
      Height = 17
      Caption = 'Judicial'
      TabOrder = 10
    end
    object chkpreJudicial: TCheckBox
      Left = 459
      Top = 70
      Width = 73
      Height = 17
      Caption = 'Pre Judicial'
      TabOrder = 11
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 667
    Width = 1199
    Height = 41
    Align = alBottom
    TabOrder = 2
    object Notebook: TNotebook
      Left = 1001
      Top = 1
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btnSalir: TButton
          Left = 109
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = 'Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btnAceptar: TButton
          Left = 26
          Top = 6
          Width = 79
          Height = 26
          Caption = 'Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
  end
  object lbEmpresasRecaudadoras: TAbmList
    Left = 0
    Top = 35
    Width = 1199
    Height = 213
    TabStop = True
    TabOrder = 3
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'41'#0'C'#243'digo'
      #0'273'#0'Nombre'
      #0'88'#0'Documento'
      #0'172'#0'Alias'
      #0'154'#0'Giro'
      #0'140'#0'Descripci'#243'n'
      #0'222'#0'eMail'
      #0'125'#0'Tel'#233'fono'
      #0'121'#0'Regi'#243'n'
      #0'131'#0'Comuna')
    HScrollBar = True
    Table = spObtenerEmpresasRecaudadoras
    Style = lbOwnerDrawVariable
    OnDblClick = lbEmpresasRecaudadorasEdit
    OnDrawItem = lbEmpresasRecaudadorasDrawItem
    OnRefresh = lbEmpresasRecaudadorasRefresh
    OnInsert = lbEmpresasRecaudadorasInsert
    OnDelete = lbEmpresasRecaudadorasDelete
    OnEdit = lbEmpresasRecaudadorasEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = tbEmpresasRecaudadoras
  end
  object spEliminarEmpresasRecaudadoras: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarEmpresasRecaudadoras;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 240
    Top = 96
  end
  object spActualizarEmpresasRecaudadoras: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEmpresasRecaudadoras;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NombreEmpresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@AliasEmpresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Giro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@eMail'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 80
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Dpto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 80
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDireccionExterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Anexo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
      end
      item
        Name = '@Judicial'
        Attributes = [paNullable]
        DataType = ftBoolean
      end
      item
        Name = '@PreJudicial'
        Attributes = [paNullable]
        DataType = ftBoolean
      end>
    Left = 200
    Top = 96
  end
  object spObtenerEmpresasRecaudadoras: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEmpresasRecaudadoras;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 96
    object spObtenerEmpresasRecaudadorasCodigoEmpresasRecaudadoras: TAutoIncField
      FieldName = 'CodigoEmpresasRecaudadoras'
      ReadOnly = True
    end
    object spObtenerEmpresasRecaudadorasNombreEmpresa: TStringField
      FieldName = 'NombreEmpresa'
      FixedChar = True
      Size = 60
    end
    object spObtenerEmpresasRecaudadorasCodigoDocumento: TStringField
      FieldName = 'CodigoDocumento'
      FixedChar = True
      Size = 4
    end
    object spObtenerEmpresasRecaudadorasNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object spObtenerEmpresasRecaudadorasAliasEmpresa: TStringField
      FieldName = 'AliasEmpresa'
      FixedChar = True
      Size = 60
    end
    object spObtenerEmpresasRecaudadorasGiro: TStringField
      FieldName = 'Giro'
      FixedChar = True
      Size = 40
    end
    object spObtenerEmpresasRecaudadorasDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 255
    end
    object spObtenerEmpresasRecaudadoraseMail: TStringField
      FieldName = 'eMail'
      Size = 255
    end
    object spObtenerEmpresasRecaudadorasCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object spObtenerEmpresasRecaudadorasCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object spObtenerEmpresasRecaudadorasCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
    object spObtenerEmpresasRecaudadorasCodigoCalle: TIntegerField
      FieldName = 'CodigoCalle'
    end
    object spObtenerEmpresasRecaudadorasComuna: TStringField
      FieldName = 'Comuna'
      ReadOnly = True
      FixedChar = True
      Size = 100
    end
    object spObtenerEmpresasRecaudadorasRegion: TStringField
      FieldName = 'Region'
      ReadOnly = True
      FixedChar = True
      Size = 100
    end
    object spObtenerEmpresasRecaudadorasDescripcionCiudad: TStringField
      FieldName = 'DescripcionCiudad'
      Size = 80
    end
    object spObtenerEmpresasRecaudadorasNumero: TStringField
      FieldName = 'Numero'
      FixedChar = True
    end
    object spObtenerEmpresasRecaudadorasPiso: TWordField
      FieldName = 'Piso'
    end
    object spObtenerEmpresasRecaudadorasDpto: TStringField
      FieldName = 'Dpto'
      Size = 40
    end
    object spObtenerEmpresasRecaudadorasCalleDesnormalizada: TStringField
      FieldName = 'CalleDesnormalizada'
      Size = 80
    end
    object spObtenerEmpresasRecaudadorasDetalle: TStringField
      FieldName = 'Detalle'
      Size = 100
    end
    object spObtenerEmpresasRecaudadorasCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      Size = 10
    end
    object spObtenerEmpresasRecaudadorasCodigoSegmento: TIntegerField
      FieldName = 'CodigoSegmento'
    end
    object spObtenerEmpresasRecaudadorasCodigoDireccionExterno: TStringField
      FieldName = 'CodigoDireccionExterno'
    end
    object spObtenerEmpresasRecaudadorasCodigoArea: TSmallintField
      FieldName = 'CodigoArea'
    end
    object spObtenerEmpresasRecaudadorasValor: TStringField
      FieldName = 'Valor'
      Size = 255
    end
    object spObtenerEmpresasRecaudadorasAnexo: TIntegerField
      FieldName = 'Anexo'
    end
    object spObtenerEmpresasRecaudadorasCodigoTipoMedioContacto: TWordField
      FieldName = 'CodigoTipoMedioContacto'
    end
    object spObtenerEmpresasRecaudadorasHorarioDesde: TDateTimeField
      FieldName = 'HorarioDesde'
    end
    object spObtenerEmpresasRecaudadorasHorarioHasta: TDateTimeField
      FieldName = 'HorarioHasta'
    end
    object spObtenerEmpresasRecaudadorasFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object spObtenerEmpresasRecaudadorasUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object spObtenerEmpresasRecaudadorasFechaHoraModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
    object spObtenerEmpresasRecaudadorasUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
      FixedChar = True
    end
    object spObtenerEmpresasRecaudadorasJudicial: TBooleanField
      FieldName = 'Judicial'
    end
    object spObtenerEmpresasRecaudadorasPreJudicial: TBooleanField
      FieldName = 'PreJudicial'
    end
  end
end
