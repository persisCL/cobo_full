object frmAgendaFacturacion: TfrmAgendaFacturacion
  Left = 220
  Top = 273
  Anchors = []
  Caption = 'Calendario de Facturaci'#243'n'
  ClientHeight = 470
  ClientWidth = 804
  Color = clBtnFace
  Constraints.MinHeight = 465
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    804
    470)
  PixelsPerInch = 96
  TextHeight = 13
  object pnl_Datos: TPanel
    Left = 0
    Top = 0
    Width = 804
    Height = 428
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      804
      428)
    object gbMovSeleccionados: TGroupBox
      Left = 6
      Top = 2
      Width = 275
      Height = 420
      Anchors = [akLeft, akTop, akBottom]
      Caption = ' Grupos Programados '
      TabOrder = 0
      DesignSize = (
        275
        420)
      object lbl_Anio: TLabel
        Left = 8
        Top = 22
        Width = 22
        Height = 13
        Caption = 'A'#241'o:'
      end
      object cbAnios: TComboBox
        Left = 34
        Top = 18
        Width = 178
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
        OnChange = cbAniosChange
        OnEnter = cbAniosEnter
      end
      object tvGrupos: TTreeView
        Left = 8
        Top = 44
        Width = 257
        Height = 365
        Anchors = [akLeft, akTop, akRight, akBottom]
        HideSelection = False
        Images = ImageList
        Indent = 19
        ReadOnly = True
        RowSelect = True
        TabOrder = 1
        OnChange = tvGruposChange
        OnChanging = tvGruposChanging
      end
      object btn_NuevoCalendario: TButton
        Left = 219
        Top = 18
        Width = 22
        Height = 21
        Hint = 'Agregar un nuevo calendario'
        Caption = '+'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = btn_NuevoCalendarioClick
      end
      object btn_EliminarCalendario: TButton
        Left = 243
        Top = 18
        Width = 22
        Height = 21
        Hint = 'Eliminar un calendario'
        Caption = '-'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = btn_EliminarCalendarioClick
      end
    end
    object gbFechas: TGroupBox
      Left = 288
      Top = 2
      Width = 509
      Height = 359
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = ' Informaci'#243'n de Ciclos de Facturaci'#243'n '
      TabOrder = 1
      object Notebook: TNotebook
        Left = 2
        Top = 15
        Width = 505
        Height = 342
        Align = alClient
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          object lbl_FechaProgramada: TLabel
            Left = 14
            Top = 27
            Width = 158
            Height = 13
            Caption = 'Fecha Programada de Ejecuci'#243'n:'
          end
          object lbl_FechaDeCorte: TLabel
            Left = 14
            Top = 59
            Width = 76
            Height = 13
            Caption = 'Fecha de Corte:'
          end
          object lbl_FechaDeEmision: TLabel
            Left = 14
            Top = 91
            Width = 87
            Height = 13
            Caption = 'Fecha de Emisi'#243'n:'
          end
          object lbl_FechaDeVencimiento: TLabel
            Left = 14
            Top = 123
            Width = 109
            Height = 13
            Caption = 'Fecha de Vencimiento:'
          end
          object lbl_Estado: TLabel
            Left = 16
            Top = 152
            Width = 313
            Height = 13
            AutoSize = False
            Caption = 'Estado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbl_DebeSerDiaHabil: TLabel
            Left = 288
            Top = 124
            Width = 102
            Height = 13
            Caption = 'Debe ser un d'#237'a h'#225'bil'
          end
          object lbl_FechaCorteHint: TLabel
            Left = 288
            Top = 60
            Width = 197
            Height = 13
            Caption = 'Incluye los tr'#225'nsitos de la fecha ingresada'
          end
          object deFechaVencimiento: TDateEdit
            Left = 192
            Top = 120
            Width = 90
            Height = 21
            AutoSelect = False
            Color = 16444382
            TabOrder = 3
            OnChange = deFechaProgramadaChange
            AllowEmpty = False
            Date = -693594.000000000000000000
          end
          object deFechaEmision: TDateEdit
            Left = 192
            Top = 88
            Width = 90
            Height = 21
            AutoSelect = False
            Color = 16444382
            TabOrder = 2
            OnChange = deFechaProgramadaChange
            AllowEmpty = False
            Date = -693594.000000000000000000
          end
          object deFechaCorte: TDateEdit
            Left = 192
            Top = 56
            Width = 90
            Height = 21
            AutoSelect = False
            Color = 16444382
            TabOrder = 1
            OnChange = deFechaProgramadaChange
            AllowEmpty = False
            Date = -693594.000000000000000000
          end
          object deFechaProgramada: TDateEdit
            Left = 192
            Top = 24
            Width = 90
            Height = 21
            AutoSelect = False
            Color = 16444382
            TabOrder = 0
            OnChange = deFechaProgramadaChange
            AllowEmpty = False
            Date = -693594.000000000000000000
          end
        end
        object TPage
          Left = 0
          Top = 0
          DesignSize = (
            505
            342)
          object dbgCiclos: TDBListEx
            Left = 3
            Top = 4
            Width = 493
            Height = 299
            Anchors = [akLeft, akTop, akRight, akBottom]
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 150
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Ciclo'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 150
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'FechaProgramadaEjecucion'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 150
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'FechaCorte'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 150
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'FechaEmision'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 150
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'FechaVencimiento'
              end>
            DataSource = dsClientDataSet
            DragReorder = True
            ParentColor = False
            TabOrder = 5
            TabStop = True
          end
          object btnQuincenal: TButton
            Left = 171
            Top = 311
            Width = 77
            Height = 25
            Hint = 'Generar ciclos quincenales'
            Anchors = [akRight, akBottom]
            Caption = 'Quincenal'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = pmiQuincenalClick
          end
          object btnMensual: TButton
            Left = 254
            Top = 311
            Width = 77
            Height = 25
            Hint = 'Generar ciclos mensuales'
            Anchors = [akRight, akBottom]
            Caption = 'Mensual'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = pmiMensualClick
          end
          object btnBimestral: TButton
            Left = 337
            Top = 311
            Width = 77
            Height = 25
            Hint = 'Generar ciclos bimestrales'
            Anchors = [akRight, akBottom]
            Caption = 'Bimestral'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = pmiBimestralClick
          end
          object btn_EliminarCiclosGrupo: TButton
            Left = 420
            Top = 311
            Width = 77
            Height = 25
            Hint = 'Eliminar los ciclos del grupo seleccionado'
            Anchors = [akRight, akBottom]
            Caption = 'Eliminar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = pmiEliminarCiclosGrupoClick
          end
          object btnSemanal: TButton
            Left = 90
            Top = 311
            Width = 75
            Height = 25
            Hint = 'Generar ciclos semanales'
            Anchors = [akRight, akBottom]
            Caption = 'Semanal'
            TabOrder = 0
            OnClick = btnSemanalClick
          end
        end
      end
    end
    object gb_InfoGrupo: TGroupBox
      Left = 288
      Top = 363
      Width = 510
      Height = 59
      Anchors = [akLeft, akRight, akBottom]
      Caption = ' Informaci'#243'n del Grupo de Facturaci'#243'n '
      TabOrder = 2
      object lbl_Grupos: TLabel
        Left = 8
        Top = 25
        Width = 108
        Height = 13
        Caption = 'Frecuencia del Grupo: '
      end
      object lbl_Frecuencia: TLabel
        Left = 118
        Top = 25
        Width = 69
        Height = 13
        Caption = 'lbl_Frecuencia'
      end
    end
  end
  object btnSalir: TButton
    Left = 723
    Top = 434
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object btnAceptar: TButton
    Left = 562
    Top = 434
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 642
    Top = 434
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object QryAnios: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'select distinct a'#241'o from agendaFacturacion  WITH (NOLOCK) ')
    Left = 24
    Top = 64
  end
  object qryFechasFacturacion: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'A'#241'o'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Ciclo'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'CodigoGrupoFacturacion'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from agendaFacturacion  WITH (NOLOCK) '
      'where a'#241'o = :A'#241'o and '
      'ciclo = :Ciclo and '
      'CodigoGrupoFacturacion = :CodigoGrupoFacturacion')
    Left = 56
    Top = 64
  end
  object ClientDataSet: TClientDataSet
    Aggregates = <>
    Filtered = True
    FieldDefs = <
      item
        Name = 'CodigoGrupoFacturacion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoCiclo'
        DataType = ftInteger
      end
      item
        Name = 'Ciclo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaProgramadaEjecucion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaCorte'
        DataType = ftDateTime
      end
      item
        Name = 'FechaEmision'
        DataType = ftDateTime
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDateTime
      end
      item
        Name = 'FechaRealFacturacion'
        DataType = ftDateTime
      end
      item
        Name = 'FactorFrecuencia'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'Index'
        Fields = 'CodigoGrupoFacturacion; CodigoCiclo'
      end>
    IndexFieldNames = 'CodigoGrupoFacturacion; CodigoCiclo'
    Params = <>
    StoreDefs = True
    AfterPost = ClientDataSetAfterPost
    Left = 24
    Top = 112
  end
  object ImageList: TImageList
    Left = 110
    Top = 289
    Bitmap = {
      494C010104000900080010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDEDE00B5B5BD004A52
      4A0052524A004A4A4A00424242007B848400B5BDBD003139420039424A00424A
      520042525A009C9CA500FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004A52
      4A0052524A004A4A4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000052525200DEDED600ADADAD006B63
      4200C6AD6300CEAD630094845200424A4A00B5B5B50029312900293939008CCE
      D6005A9CAD00848C9400E7EFEF004A5A63000000000000000000000000000000
      0000C6C6BD00C6BDAD00E7DEDE00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDED60094949400736B
      4200C6AD6300CEAD630000000000EFEFEF004A4A520039424200EFEFEF00FFFF
      FF00FFFFFF00FFFFFF00EFEFEF00000000000000000000000000000000000000
      0000C6C6BD00000000008C8C8C0063636300525A5A008C8C8C008C8C8C008C8C
      8C008C8C8C008C8C8C000000000000000000424A42004A4A390084848C00736B
      4200DEBD6B00F7C67300AD945A0031424A003139310031393100BDEFEF00B5F7
      FF0063ADBD00848C9400315A6B00425A6300000000000000000000000000EFEF
      EF00AD9C8C009C9C940094948C00A5A59C00CEC6BD00E7E7E700FFFFFF000000
      00000000000000000000000000000000000000000000424A39007B7B7B007B73
      4A00DEBD6B00F7C6730000000000FFFFFF003131390018182100D6D6D6000000
      00000000000000000000FFFFFF0000000000000000000000000000000000EFEF
      EF00AD9C8C0000000000F7F7FF005A63630021292900DEDEE700000000000000
      000000000000000000000000000000000000C6C6C6005A63520042423900736B
      4200CEB56B00DEBD6B00A5945A0029292900525A42004A636300B5F7FF00A5F7
      FF0063ADBD005A848C00294A5200F7F7FF00000000000000000000000000CECE
      C600A59C8C009C9C9C008C8484009C949400A59C9C00A5A5A500ADADA500D6CE
      CE00EFEFE7000000000000000000000000000000000063635200393939007B73
      4A00CEB56B00DEBD6B000000000073737B00292931004A4A5200313939000000
      00000000000000000000FFFFFF0000000000000000000000000000000000CECE
      C600A59C8C0000000000EFEFEF0029293100292931004A4A5200000000000000
      000000000000000000000000000000000000FFFFFF00EFEFEF00212121006363
      3900B5A56300B5A563008C84520039423100213131007BD6EF0084E7FF0084E7
      FF0063D6F70021394200ADB5B500FFFFFF00000000000000000000000000ADA5
      94009C949400847B7B00BDAD9C00A59C8C00A59C9400948C8C00A5A59C00B5B5
      B500C6C6BD00BDBDBD00000000000000000000000000DEDEDE00212121006B6B
      4200B5A56300B5A5630000000000BDBDC6006B6B7300DEDEDE00394242005A63
      63000000000000000000FFFFFF0000000000000000000000000000000000ADA5
      94009C949400000000004A4A520042424A00BDBDBD0029293100C6C6C6000000
      000000000000000000000000000000000000FFFFFF00BDBDBD00394242002931
      2900525239005A63390031393100395A4A006BAD8C0029424A00316373004284
      9C002139420031393100B5BDBD00FFFFFF000000000000000000EFEFEF009C94
      84008C8C8C00948C7B0073736B0084847B008C8C840084847B00BDB5AD00ADAD
      A5009C9C9C00B5B5AD00E7E7E7000000000000000000BDBDBD00313939002931
      290063634200525A390000000000FFFFFF000000000000000000CECECE002121
      2900C6C6C600F7F7F700FFFFFF00000000000000000000000000EFEFEF009C94
      84008C8C8C0000000000F7F7FF00DEDEDE0000000000949C9C00212929000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00EFEFEF003942
      390021292900293131005A8C7B006BB59C006BAD94006B9C7B00212931001829
      31005A735A009CA5A500FFFFFF00FFFFFF000000000000000000CECEC6008C8C
      8400847B73007B736B007B7B73008C84840073736B007B736B00ADADA5009494
      8C0073736B00847B7300A5A5A500FFFFFF000000000000000000D6DED600394A
      4200212929002929290000000000FFFFFF00000000000000000000000000CECE
      CE0031393900DEDEDE00E7E7E700000000000000000000000000CECEC6008C8C
      8400847B730000000000F7F7FF000000000000000000F7F7F7008C8C8C003939
      4200EFEFEF00000000000000000000000000FFFFFF00FFFFFF00F7F7F7003939
      31008C7B4A008C7B4A00394A3900639C84006BA58C001839420052BDD60052BD
      CE004A636B00EFEFEF00FFFFFF00FFFFFF000000000000000000B5ADA5008C8C
      8400948C7B007B736B00DED6D6009C948C007B736B00ADA5A5009C948C00736B
      63007B736B00ADA5A500FFFFFF00000000000000000000000000E7E7EF004239
      31008C7B4A006B63420000000000FFFFFF000000000000000000000000000000
      0000C6C6C60029313100CECECE00000000000000000000000000B5ADA5008C8C
      8400948C7B0000000000F7F7FF00000000000000000000000000000000006363
      6B0031393900BDBDC6000000000000000000FFFFFF00FFFFFF00BDBDBD006B63
      4A00F7C67300AD9C5A00424A310052846B0052846B00106B7B0063E7FF00CEFF
      FF0039636B00CECED600FFFFFF00FFFFFF000000000000000000A59C8C007B73
      73007B7B73007B736B008C8C8400736B6B007B7B7300A59C9C0084847B007373
      6B00ADADA5000000000000000000000000000000000000000000A5A5AD00736B
      4A00F7C67300A594520000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A59C8C007B73
      73007B7B730000000000BDBDC600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D6004A4A5200636B6B000000000000000000FFFFFF00FFFFFF00E7E7E7005A5A
      5200948C4A008C7B4A00292929004A6352004A6B52001839420052BDCE004294
      A50052636B00FFFFFF00FFFFFF00FFFFFF0000000000000000008C847B00847B
      730073736B00BDBDB500BDBDBD0084847B00DED6D600B5ADAD00736B6300ADA5
      A500F7F7F7000000000000000000000000000000000000000000CECED6006363
      5200948C4A006B634200212929004A6B5200394A4200396B7B004AADBD003163
      6B0084949C0000000000000000000000000000000000000000008C847B00847B
      730073736B000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00636B6B006B6B6B00FFFFFF006B6B6B0063636300FFFFFF006B737B00525A
      5A00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000847B73007B73
      6B00736B630073736B00736B63007B736B008C847B007B736B007B736B00BDBD
      BD00000000000000000000000000000000000000000000000000000000000000
      0000636363008C8C8C00FFFFFF005A5A5A0084848400FFFFFF006B737B009C9C
      A500000000000000000000000000000000000000000000000000847B73007B73
      6B00736B630073736B00736B63007B736B008C847B007B736B007B736B00BDBD
      BD0000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7DEDE004A524A00527342005273420039393100FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B7373009C9C
      8C0052ADB500639CA5006B848400738C8C007B847B0073736B008C847B00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000BDB5B5005A6B4A00526B31004A5A3100E7EFDE00000000000000
      00000000000000000000000000000000000000000000000000007B7373009C9C
      8C0052ADB500639CA5006B848400738C8C007B847B0073736B008C847B00FFFF
      FF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00737B73005A846B006BA584006BA58400526B42009C9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000073736B009CB5
      AD0029BDE70029ADCE0021BDDE0021B5D60021B5D6004ABDD600ADADA5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A524A00639463006BA56B006BA56B00424A3900000000000000
      000000000000000000000000000000000000000000000000000073736B009CB5
      AD0029BDE70029ADCE0021BDDE0021B5D60021B5D6004ABDD600ADADA5000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0073736B0052735A0063A594006BA5840029312900CECECE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000008C8C8400C6B5
      9C003931310039313100948C7B0063635A00394242006B737300CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A635A00639463006BB59C006BA56B0031393100FFFFFF000000
      00000000000000000000000000000000000000000000000000008C8C8400C6B5
      9C003931310039313100948C7B0063635A00394242006B737300CECECE000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00848C840042524A00424A4200DEE7DE00E7E7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000F7F7F7004242
      4200181818002929210018181800212118002929210018181800EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0063636300424A42003939310000000000000000000000
      0000000000000000000000000000000000000000000000000000F7F7F7004242
      4200181818002929210018181800212118002929210018181800EFEFEF000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00E7E7E700E7E7E700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A
      4A0073737300393939004A4A42006B6B6B00636363004A4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004A4A
      4A000000000039393900000000006B6B6B00000000004A4A4A00000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000008000FFFFE000F8010000F0FF8000F001
      0000E01F801CE03D0000E007801CE03D0000E003800CE01D0000C00180C0C09D
      0000C000C0E0C1850000C001C0F0C1E10000C007C000C0010000C007C007C001
      0000C00FF00FC00F0000C00FF83FC00F0000C01FF83FC01F0000C01FF81FC01F
      0000C01FF87FC01F0000E03FFFFFEABF00000000000000000000000000000000
      000000000000}
  end
  object dsClientDataSet: TDataSource
    DataSet = ClientDataSet
    OnDataChange = dsClientDataSetDataChange
    Left = 56
    Top = 112
  end
  object ObtenerGruposPorAnioAgenda: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerGruposPorA'#241'oAgenda;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@A'#241'o'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 104
    Top = 112
  end
  object EliminarCiclos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'A'#241'o'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoGrupoFacturacion'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM AgendaFacturacion'
      'WHERE A'#241'o = :A'#241'o AND '
      'CodigoGrupoFacturacion = :CodigoGrupoFacturacion ')
    Left = 88
    Top = 64
  end
  object spObtenerAgendafacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerAgendaFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Anio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 112
  end
  object InsertarFechas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarFechasAgendaFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Anio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Ciclo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaProgramadaEjecucion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FactorFrecuencia'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 2
        Value = Null
      end>
    Left = 168
    Top = 112
  end
  object UpdateFechas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFechasAgendaFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Anio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Ciclo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaProgramadaEjecucion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FactorFrecuencia'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 2
        Value = Null
      end>
    Left = 200
    Top = 112
  end
  object sp_EliminarCalendario: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarAgendaFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Anio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 104
    Top = 144
  end
end
