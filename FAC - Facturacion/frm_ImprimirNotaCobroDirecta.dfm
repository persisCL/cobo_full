object form_ImprimirNotaCobroDirecta: Tform_ImprimirNotaCobroDirecta
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Imprimir Nota de Cobro Directa'
  ClientHeight = 433
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    468
    433)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 101
    Width = 263
    Height = 13
    Caption = 'Seleccione la Nota de Cobro Directa que desea imprimir'
  end
  object Label2: TLabel
    Left = 22
    Top = 67
    Width = 98
    Height = 13
    Caption = 'Proveedor DayPass:'
  end
  object dbl_NotasCobro: TDBListEx
    Left = 8
    Top = 120
    Width = 452
    Height = 275
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 95
        Header.Caption = 'Tipo Compr.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'TipoComprobante'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'N'#186' Comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'NumeroComprobante'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'Fecha Emisi'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'FechaEmision'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Estado Comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescriEstado'
      end>
    DataSource = ds_ObtenerNotasCobroNQ
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
  end
  object btn_ImprimirComprobante: TButton
    Left = 55
    Top = 401
    Width = 131
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Imprimir Comprobante...'
    Default = True
    TabOrder = 2
    OnClick = btn_ImprimirComprobanteClick
  end
  object btn_Cerrar: TButton
    Left = 327
    Top = 401
    Width = 131
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cerrar'
    TabOrder = 4
    OnClick = btn_CerrarClick
  end
  object btn_ImprimirDetalle: TButton
    Left = 191
    Top = 401
    Width = 131
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Imprimir &Detalle...'
    TabOrder = 3
    OnClick = btn_ImprimirDetalleClick
  end
  object btnBuscar: TButton
    Left = 382
    Top = 11
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '&Buscar'
    TabOrder = 0
    OnClick = btnBuscarClick
  end
  object rbNotasCobroPDU: TRadioButton
    Left = 8
    Top = 11
    Width = 188
    Height = 17
    Hint = 'Seleccione para buscar las Notas de Cobro Directas por PDU'
    Caption = 'Notas de Cobro Directas para PDU'
    Checked = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    TabStop = True
  end
  object rbNotasCobroBHTU: TRadioButton
    Left = 8
    Top = 31
    Width = 194
    Height = 17
    Hint = 'Seleccione para buscar las Notas de Cobro Directas por BHTU'
    Caption = 'Notas de Cobro Directas para BHTU'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
  end
  object cbProveedorDayPass: TVariantComboBox
    Left = 126
    Top = 64
    Width = 291
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 7
    Items = <>
  end
  object sp_ObtenerNotasCobroNQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterScroll = sp_ObtenerNotasCobroNQAfterScroll
    ProcedureName = 'ObtenerNotasCobroNQ;1'
    Parameters = <>
    Left = 292
    Top = 188
  end
  object ds_ObtenerNotasCobroNQ: TDataSource
    DataSet = sp_ObtenerNotasCobroNQ
    Left = 304
    Top = 240
  end
end
