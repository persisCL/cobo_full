object FormSaltosNumeracion: TFormSaltosNumeracion
  Left = 0
  Top = 0
  ActiveControl = chkOcultarEliminados
  Caption = 'Mantenimiento de Saltos en Correlativo Comprobantes'
  ClientHeight = 494
  ClientWidth = 678
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 451
    Width = 678
    Height = 43
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Notebook: TNotebook
      Left = 481
      Top = 0
      Width = 197
      Height = 43
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btnBtnSalir: TButton
          Left = 76
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btnBtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btnBtnAceptar: TButton
          Left = 29
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnBtnAceptarClick
        end
        object btnBtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btnBtnCancelarClick
        end
      end
    end
  end
  object dblSaltosNumeracion: TAbmList
    Left = 0
    Top = 33
    Width = 678
    Height = 290
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'105'#0'Tipo Comprobante'
      #0'111'#0'N'#250'mero Folio Desde'
      #0'114'#0'N'#250'mero Folio Hasta'
      #0'115'#0'Usuario Creaci'#243'n'
      #0'108'#0'Fecha Creaci'#243'n'
      #0'113'#0'Usuario Modificaci'#243'n'
      #0'101'#0'Fecha Modificaci'#243'n'
      #0'100'#0'Usuario Eliminaci'#243'n'
      #0'94'#0'Fecha Eliminaci'#243'n')
    HScrollBar = True
    RefreshTime = 10
    Table = tblSaltosComprobantes
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblSaltosNumeracionClick
    OnDblClick = dblSaltosNumeracionDblClick
    OnProcess = dblSaltosNumeracionProcess
    OnDrawItem = dblSaltosNumeracionDrawItem
    OnRefresh = dblSaltosNumeracionRefresh
    OnInsert = dblSaltosNumeracionInsert
    OnDelete = dblSaltosNumeracionDelete
    OnEdit = dblSaltosNumeracionEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = abmtlbr1
  end
  object abmtlbr1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 678
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = abmtlbr1Close
    ExplicitLeft = 24
    ExplicitTop = -6
  end
  object pnlGroupB: TPanel
    Left = 0
    Top = 323
    Width = 678
    Height = 128
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object lblNombre: TLabel
      Left = 9
      Top = 81
      Width = 112
      Height = 13
      Caption = 'N'#250'mero Folio Hasta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl1: TLabel
      Left = 9
      Top = 22
      Width = 104
      Height = 13
      Caption = '&Tipo Comprobante'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl2: TLabel
      Left = 8
      Top = 51
      Width = 115
      Height = 13
      Caption = 'N'#250'mero Folio Desde'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_folioDesde: TNumericEdit
      Left = 130
      Top = 43
      Width = 130
      Height = 21
      MaxLength = 0
      TabOrder = 0
    end
    object txt_foliohasta: TNumericEdit
      Left = 130
      Top = 72
      Width = 130
      Height = 21
      MaxLength = 0
      TabOrder = 1
    end
    object cboTipoComprobante: TVariantComboBox
      Left = 131
      Top = 14
      Width = 205
      Height = 21
      Style = vcsDropDownList
      Color = clSkyBlue
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
  end
  object chkOcultarEliminados: TCheckBox
    Left = 510
    Top = 10
    Width = 147
    Height = 17
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = 'Ocultar/ Mostrar Eliminados'
    TabOrder = 4
    OnClick = chkOcultarEliminadosClick
  end
  object tblSaltosComprobantes: TADOTable
    Tag = 1
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'TipoComprobante'#13#10
    TableName = 'SaltosNumerosComprobantes'
    Left = 316
    Top = 75
    object strngfldSaltosComprobantesTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object lrgntfldSaltosComprobantesNumeroFolioDesde: TLargeintField
      FieldName = 'NumeroFolioDesde'
    end
    object lrgntfldSaltosComprobantesNumeroFolioHasta: TLargeintField
      FieldName = 'NumeroFolioHasta'
    end
    object strngfldSaltosComprobantesUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object dtmfldSaltosComprobantesFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object strngfldSaltosComprobantesUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
      FixedChar = True
    end
    object dtmfldSaltosComprobantesFechaHoraModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
    object blnfldSaltosComprobantesEliminado: TBooleanField
      FieldName = 'Eliminado'
    end
    object strngfldSaltosComprobantesUsuarioEliminacion: TStringField
      FieldName = 'UsuarioEliminacion'
      FixedChar = True
    end
    object dtmfldSaltosComprobantesFechaHoraEliminacion: TDateTimeField
      FieldName = 'FechaHoraEliminacion'
    end
  end
  object spObtenerTiposComprobantesSaltosFolios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposComprobantesSaltosFolios;1'
    Parameters = <>
    Left = 496
    Top = 112
  end
end
