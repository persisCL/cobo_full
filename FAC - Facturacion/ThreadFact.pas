{ *******************************************************
Revision 1
Author: nefernandez
Date: 16/03/2007
Description : Se pasa el parametro Usuario al SP "GenerarMovimientosTransitos" -
                para auditoria
                
                
Revision 2
Author: jconcheyro
Date: 27/03/2007
Description : Se modifica la lectura de parametros del store para poder obtener los 
mensajes de error
                
Etiqueta	:	TASK_086_MGO_20161121
Descripción	:	Se agrega Pre Facturación

Etiqueta    :   TASK_097_MGO_20161221
Descripción :   Se mejoran mensajes de error

******************************************************* }
unit ThreadFact;

interface

uses
  Classes, Windows, SysUtils, DM_ThreadFact, UtilProc, Variants, Util, ADODB, DMConnection;

type
  TThreadFacturacionGrupo = class(TThread)
  private
	{ Private declarations }
	FGrupo: Integer;
	FAnio: Integer;
	FCiclo: Integer;
	FTerminar: ^Boolean;
    FPreFacturacion: Boolean;
	DM: TDMThreadFacturacion;
	FThreadsCorriendo: ^Integer;
	FConveniosProcesados: ^Integer;
    FErrorFacturacion: ^Boolean;
	FNumeroProcesoFacturacion: Integer;
	FFechaCorte, FFechaEmision, FFechaVencimiento: TDateTime;
  protected
	procedure Execute; override;
	function FacturarConvenios(var ConvenioAnterior: Integer): Integer;
  public
	constructor Create(Grupo, Anio, Ciclo: Integer; FechaCorte, FechaEmision,
	  FechaVencimiento: TDateTime; NumeroProcesoFacturacion: Integer;
	  var ConveniosProcesados, ThreadsCorriendo: Integer; var ErrorFacturacion, 
      Terminar; PreFacturacion: Boolean);
	destructor Destroy; override;
  end;

implementation

{ TThreadFacturacion }

constructor TThreadFacturacionGrupo.Create(Grupo, Anio, Ciclo: Integer; FechaCorte, FechaEmision,
	  FechaVencimiento: TDateTime; NumeroProcesoFacturacion: Integer;
	  var ConveniosProcesados, ThreadsCorriendo: Integer; var ErrorFacturacion, 
      Terminar; PreFacturacion: Boolean);                                       
begin
	Inherited Create(True);
	FreeOnTerminate := True;
	FGrupo := Grupo;
	FAnio  := Anio;
    FCiclo := Ciclo;
	FFechaCorte := FechaCorte;
	FFechaEmision := FechaEmision;
    FFechaVencimiento := FechaVencimiento;
	FNumeroProcesoFacturacion := NumeroProcesoFacturacion;
	FTerminar := @Terminar;
    FPreFacturacion := PreFacturacion;

	FErrorFacturacion := @ErrorFacturacion;
	FConveniosProcesados := @ConveniosProcesados;
	FThreadsCorriendo := @ThreadsCorriendo;
	Inc(FThreadsCorriendo^);
	DM := TDMThreadFacturacion.Create(nil);
	Resume;
end;

destructor TThreadFacturacionGrupo.Destroy;
begin
	DM.Free;
	Dec(FThreadsCorriendo^);
	inherited;
end;

procedure TThreadFacturacionGrupo.Execute;
Var
	CodigoConvenio, Procesados: Integer;
begin
	//try                                                                       // TASK_097_MGO_20161221
		CodigoConvenio := 0;
		While not Terminated and (not FTerminar^) do begin
			Procesados := FacturarConvenios(CodigoConvenio);
			if Procesados = 0 then Break;
			if Procesados < 0 then begin
            	FErrorFacturacion^ := True;
                Break;                                                          // TASK_097_MGO_20161221
			  	//raise Exception.Create('Error al generar Facturación');       // TASK_097_MGO_20161221
            end;
			Inc(FConveniosProcesados^, Procesados);
        end;
	//except                                                                    // TASK_097_MGO_20161221
	//	on e: exception do MsgBox(e.message);                                   // TASK_097_MGO_20161221
	//end;                                                                      // TASK_097_MGO_20161221
end;

function TThreadFacturacionGrupo.FacturarConvenios(var ConvenioAnterior: Integer): Integer;
resourcestring                                                                  // TASK_097_MGO_20161221
    MSG_ERROR = 'Error en el proceso entre los convenios %d y %d.';             // TASK_097_MGO_20161221
var
    Reintentos: Integer;
    mensajeDeadlock: AnsiString;
    DescripcionError: string;
    ConvenioHasta: integer;
begin
    // Intentamos facturar un grupo de convenios.
    Reintentos  := 0;
    Result      := -1;
    while Reintentos < 5 do begin
        try
            if not FPreFacturacion then
                with DM.GenerarMovimientosTransitos do begin
                    Parameters.ParamByname('@CodigoConvenio').Value := 0;
                    Parameters.ParamByname('@ConvenioAnterior').Value := ConvenioAnterior;
                    Parameters.ParamByname('@GrupoFacturacion').Value := FGrupo;
                    Parameters.ParamByname('@FechaCorte').Value := FFechaCorte;
                    Parameters.ParamByname('@FechaEmision').Value := FFechaEmision;
                    Parameters.ParamByname('@FechaVencimiento').Value := FFechaVencimiento;
                    Parameters.ParamByname('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
                    Parameters.ParamByname('@NoGenerarAjusteSencillo').Value := True;
                    Parameters.ParamByname('@ConveniosProcesados').Value := 0;
                    Parameters.ParamByname('@DescripcionError').Value    := '' ;
                    Parameters.ParamByname('@CodigoUsuario').Value := UsuarioSistema;
                    ExecProc;
                    DescripcionError := Parameters.ParamByname('@DescripcionError').Value;
                    ConvenioHasta    := Parameters.ParamByname('@ConvenioAnterior').Value;
                    //if DescripcionError <> ''  then MsgBox('Error en la facturación ' + DescripcionError);       // TASK_097_MGO_20161221
                    if DescripcionError <> EmptyStr then                                                           // TASK_097_MGO_20161221
                        raise Exception.Create(DescripcionError);                                                  // TASK_097_MGO_20161221

                    ConvenioAnterior := Parameters.ParamByname('@ConvenioAnterior').Value;
                    Result           := Parameters.ParamByname('@ConveniosProcesados').Value;
                    Break;
                end
            else
                with DM.spPREFAC_GenerarMovimientosTransitos do begin
                    Parameters.Refresh;
                    Parameters.ParamByname('@ResetearProceso').Value := IIf(ConvenioAnterior = 0, 1, 0);
                    Parameters.ParamByname('@CodigoConvenio').Value := 0;
                    Parameters.ParamByname('@ConvenioAnterior').Value := ConvenioAnterior;
                    Parameters.ParamByname('@GrupoFacturacion').Value := FGrupo;
                    Parameters.ParamByname('@FechaCorte').Value := FFechaCorte;
                    Parameters.ParamByname('@FechaEmision').Value := FFechaEmision;
                    Parameters.ParamByname('@FechaVencimiento').Value := FFechaVencimiento;
                    Parameters.ParamByname('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
                    Parameters.ParamByname('@NoGenerarAjusteSencillo').Value := False;  
                    Parameters.ParamByname('@ConveniosProcesados').Value := 0;
                    Parameters.ParamByname('@DescripcionError').Value    := '' ;
                    Parameters.ParamByname('@CodigoUsuario').Value := UsuarioSistema;
                    ExecProc;
                    DescripcionError := Parameters.ParamByname('@DescripcionError').Value;
                    ConvenioHasta    := Parameters.ParamByname('@ConvenioAnterior').Value;
                    //if DescripcionError <> ''  then MsgBox('Error en la facturación ' + DescripcionError);           // TASK_097_MGO_20161221
                    if DescripcionError <> EmptyStr then                                                               // TASK_097_MGO_20161221
                        raise Exception.Create(DescripcionError);                                                      // TASK_097_MGO_20161221
                    ConvenioAnterior := Parameters.ParamByname('@ConvenioAnterior').Value;
                    Result           := Parameters.ParamByname('@ConveniosProcesados').Value;
                    Break;
                end;
        except
            on e: exception do begin
                //ConvenioHasta := ConvenioAnterior;                            // TASK_097_MGO_20161221
                if (pos('deadlock', e.Message) > 0) then begin
                    ConvenioHasta := ConvenioAnterior;                          // TASK_097_MGO_20161221
                    mensajeDeadlock := e.message;
                    inc(Reintentos);
                    sleep(1000);
                end else begin
                    //MsgBox( format('Error en la facturación rango convenios %d  %d  ',[ConvenioAnterior ,ConvenioHasta ])    // TASK_097_MGO_20161221
                    //+  ' ' + DescripcionError + ' ' + e.message);                                                            // TASK_097_MGO_20161221
                    MsgBoxErr(Format(MSG_ERROR, [ConvenioAnterior, ConvenioHasta]), e.Message, 'Error', MB_ICONERROR);         // TASK_097_MGO_20161221
                    ConvenioHasta := ConvenioAnterior;                                                                         // TASK_097_MGO_20161221
                    break;
                end;
            end;
        end;
    end;
    // Si se ejecuto 3 veces, es por que las tres veces dio deadlock, entonces
    // mostramos el mensaje y retonramos -1 por que hubo error
    if Reintentos = 3 then begin
        MsgBox('Error en la facturación ' + mensajeDeadLock);
    end;
end;

end.
