unit AbmPlanesComerciales;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask,
  ComCtrls, PeaProcs, CheckLst, dmConnection, ADODB, validate,
  DBCtrls, DPSControls;

type
  TFrmPlanesComerciales = class(TForm)
	Panel2: TPanel;
	DBList1: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Notebook: TNotebook;
    PageControl: TPageControl;
    Tab_General: TTabSheet;
    Label1: TLabel;
    Label15: TLabel;
    txt_descripcion: TEdit;
    txt_CodigoPlanComercial: TNumericEdit;
    Tab_Categorias: TTabSheet;
    clbCategorias: TCheckListBox;
    neImporteDetalle: TNumericEdit;
    neImporteEnvioDomicilio: TNumericEdit;
    Label3: TLabel;
    Label4: TLabel;
    ObtenerPlanComercialCategoria: TADOStoredProc;
    tblPlanesComerciales: TADOTable;
    tblPlanesComercialesCategorias: TADOTable;
    EliminarPlanComercialTipoPago: TADOQuery;
    tab_TipoPago: TTabSheet;
    clbTipoPago: TCheckListBox;
    tab_AdhesionTipoCliente: TTabSheet;
    clbTipoAdhesionTipoCliente: TCheckListBox;
    tblPlanesComercialesTipoPago: TADOTable;
    tblPlanesComercialesPosibles: TADOTable;
    ObtenerPlanesComercialesPosibles: TADOStoredProc;
    ObtenerPlanComercialTiposPago: TADOStoredProc;
    EliminarPlanComercialPosible: TADOQuery;
    ELiminarPlanComercialCategoria: TADOQuery;
    InsertarPlanComercial: TADOStoredProc;
    ActualizarPlanComercial: TADOQuery;
    TabObservaciones: TTabSheet;
    MemoObservaciones: TMemo;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;

	function  DBList1Process(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure DBList1Insert(Sender: TObject);
	procedure DBList1Edit(Sender: TObject);
	procedure DBList1Delete(Sender: TObject);
	procedure DBList1Click(Sender: TObject);
	procedure DBList1Refresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
    procedure Volver_Campos;
    Procedure CargarCategorias;
    procedure CargarPlanesComercialesPosibles;
    procedure CargarPlanComercialTiposPago;
    function SeleccionoAlgunItem(clb: TCheckListBox): boolean; 
    Function ActualizarCategorias: Boolean;
    Function ActualizarTipoPago: Boolean;
    Function ActualizarPlanesComercialesPosibles: Boolean;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FrmPlanesComerciales  : TFrmPlanesComerciales;

implementation

{$R *.DFM}

type TPlanComercialPosible = record
        TipoCliente: Integer;
        TipoAdhesion: Integer;
    end;
    PTPlanComercialPosible = ^TPlanComercialPosible;

function TFrmPlanesComerciales.Inicializa: boolean;
var
	i: integer;
  	S: TSize;
begin
    Result := False;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

    Tab_General.Enabled 	    := False;
    Tab_Categorias.Enabled 	    := False;
    Tab_TipoPago.Enabled        := False;
    Tab_AdhesionTipoCliente.Enabled 	:= False;

	if not OpenTables([tblPlanesComerciales]) then Exit;

    PageControl.ActivePage := Tab_General;
    notebook.PageIndex := 0;
	DbList1.Reload;

    for i:= 0 to clbTipoPago.Items.Count - 1 do
        clbTipoPago.ItemEnabled[i] := False;
    for i:= 0 to clbCategorias.Items.Count - 1 do
        clbCategorias.ItemEnabled[i] := False;
    for i:= 0 to clbTipoAdhesionTipoCliente.Items.Count - 1 do
        clbTipoAdhesionTipoCliente.ItemEnabled[i] := False;
    MemoObservaciones.Enabled := false;

	Result := True;
end;


procedure TFrmPlanesComerciales.Limpiar_Campos();
begin
	txt_CodigoPlanComercial.Clear;
	txt_Descripcion.Clear;
    neImporteDetalle.Value := 0;
    neImporteEnvioDomicilio.value := 0;
    MemoObservaciones.Clear;
end;

function TFrmPlanesComerciales.DBList1Process(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	 Result := True;
end;

procedure TFrmPlanesComerciales.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFrmPlanesComerciales.DBList1Insert(Sender: TObject);
var
	i: integer;
begin
    Tab_General.Enabled 	    := True;
    Tab_Categorias.Enabled 	    := True;
    Tab_TipoPago.Enabled        := True;
    Tab_AdhesionTipoCliente.Enabled 	:= True;
	Limpiar_Campos;
	DbList1.Enabled    		    := False;
	Notebook.PageIndex 		    := 1;
	PageControl.ActivePage	    := Tab_General;
    txt_CodigoPlanComercial.Enabled := False;
	txt_Descripcion.SetFocus;

    for i:= 0 to clbTipoPago.Items.Count - 1 do begin
        clbTipoPago.ItemEnabled[i] := True;
        clbTipoPago.Checked[i] := False;
    end;
    for i:= 0 to clbCategorias.Items.Count - 1 do begin
        clbCategorias.ItemEnabled[i] := True;
        clbCategorias.checked[i] := False;
    end;
    for i:= 0 to clbTipoAdhesionTipoCliente.Items.Count - 1 do begin
        clbTipoAdhesionTipoCliente.ItemEnabled[i] := True;
        clbTipoAdhesionTipoCliente.Checked[i] := False;
    end;
    MemoObservaciones.Enabled := True;
end;

procedure TFrmPlanesComerciales.DBList1Edit(Sender: TObject);
var i: Integer;
begin
    DbList1.Enabled    			    := False;
    Notebook.PageIndex 			    := 1;
    Tab_General.Enabled 	        := True;
    Tab_Categorias.Enabled 	        := True;
    Tab_TipoPago.Enabled            := True;
    Tab_AdhesionTipoCliente.Enabled := True;
    txt_CodigoPlanComercial.enabled := True;
    PageControl.ActivePage 	        := Tab_General;
    txt_Descripcion.SetFocus;
    for i:= 0 to clbTipoPago.Items.Count - 1 do
        clbTipoPago.ItemEnabled[i] := True;
    for i:= 0 to clbCategorias.Items.Count - 1 do
        clbCategorias.ItemEnabled[i] := True;
    for i:= 0 to clbTipoAdhesionTipoCliente.Items.Count - 1 do
        clbTipoAdhesionTipoCliente.ItemEnabled[i] := True;
    MemoObservaciones.Enabled := true;
end;

procedure TFrmPlanesComerciales.DBList1Delete(Sender: TObject);

resourcestring
    CAPTION_ELIMINARPLANCOMERCIAL = 'Eliminar Plan Comercial';

    procedure InicializarControles;
    begin
        DbList1.Estado     		:= Normal;
        DbList1.Enabled    		:= True;
        Tab_General.Enabled 	:= False;
        Tab_Categorias.Enabled 	:= False;
        Tab_TipoPago.Enabled    := False;
        Tab_AdhesionTipoCliente.Enabled 	:= False;
        Notebook.PageIndex 		:= 0;
        Screen.Cursor      		:= crDefault;
    end;

    Function execEliminarPlanComercialPosible: Boolean;
    resourcestring
        MSG_ADHESIONTIPOCLIENTE = 'No se pudieron eliminar los Tipos de Adhesi�n y los Tipos de Cliente para el Plan Comercial.';
    begin
        result := True;
        try
            EliminarPlanComercialPosible.parameters.ParamByName('PlanComercial').value := Trunc(txt_CodigoPlanComercial.Value);
            EliminarPlanCOmercialPosible.ExecSql;
        except
            On E: Exception do begin
                result := False;
                MsgBoxErr(MSG_ADHESIONTIPOCLIENTE, E.message, CAPTION_ELIMINARPLANCOMERCIAL, MB_ICONSTOP);
                exit;
            end;
        end;
    end;

    function execEliminarTiposPago:Boolean;
    resourcestring
        MSG_TIPOPAGO = 'No se pudieron eliminar los Tipos de Pago para el Plan Comercial.';
    begin
        Result := True;
        try
            EliminarPlanComercialTipoPago.parameters.ParamByName('PlanComercial').value := Trunc(txt_CodigoPlanComercial.Value);
            EliminarPlanCOmercialTipoPago.ExecSql;
        except
            On E: Exception do begin
                Result := False;
                MsgBoxErr(MSG_TIPOPAGO, E.message, CAPTION_ELIMINARPLANCOMERCIAL, MB_ICONSTOP);
                exit;
            end;
        end;
    end;

    Function execEliminarCategorias: Boolean;
    resourcestring
        MSG_CATEGORIA = 'No se pudieron eliminar las Categorias para el Plan Comercial.';
    begin
        Result := True;
        try
            EliminarPlanComercialCategoria.parameters.ParamByName('PlanComercial').value := Trunc(txt_CodigoPlanComercial.Value);
            EliminarPlanComercialCategoria.ExecSql;
        except
            On E: Exception do begin
                Result := False;
                MsgBoxErr(MSG_CATEGORIA, E.message, CAPTION_ELIMINARPLANCOMERCIAL, MB_ICONSTOP);
                exit;
            end;
        end;
    end;
resourcestring
    MSG_ELIMINARPLANCOMERCIAL = '�Est� seguro de querer eliminar este Plan Comercial?';
    MSG_ERRORELIMINARPLANCOMERCIAL = 'No se pudo eliminar el Plan Comercial seleccionado.';

begin
    Screen.Cursor := crHourGlass;
    If MsgBox(MSG_ELIMINARPLANCOMERCIAL, CAPTION_ELIMINARPLANCOMERCIAL, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        DMConnections.BaseCAC.BeginTrans;
        // Eliminamos Planes comerciales posibles
        if not execEliminarPlanComercialPosible then begin
            DMConnections.BaseCAC.RollbackTrans;
            InicializarControles;
            exit;
        end;
        // Eliminamos Categorias
        if not execEliminarCategorias then begin
            DMConnections.BaseCAC.RollbackTrans;
            InicializarControles;
            exit;
        end;
        // Eliminamos Tipos de Pago
        if not execEliminarTiposPago then begin
            DMConnections.BaseCAC.RollbackTrans;
            InicializarControles;
            exit;
        end;
        try
            (Sender as TDbList).Table.Delete;
            DMConnections.BaseCAC.CommitTrans;
        Except
            On E: Exception do begin
                (Sender as TDbList).Table.Cancel;
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ERRORELIMINARPLANCOMERCIAL, e.message, CAPTION_ELIMINARPLANCOMERCIAL, MB_ICONSTOP);
            end;
        end;
        DbList1.Reload;
    end;
    InicializarControles;
end;

procedure TFrmPlanesComerciales.DBList1Click(Sender: TObject);
begin
	with (Sender as TDbList).Table do begin
		txt_CodigoPlanComercial.Value	:= FieldByName('PlanComercial').AsInteger;
		txt_Descripcion.text 		    := Trim(FieldByName('Descripcion').AsString);

   		neImporteDetalle.Value      	:= FieldByName('ImporteHojaDetalle').AsFloat;
        neImporteEnvioDomicilio.Value   := FieldByName('ImporteEnvioDomicilio').asFloat;
        MemoObservaciones.Text          := FieldByName('Observaciones').AsString;

        CargarCategorias;
        CargarPlanComercialTiposPago;
        CargarPlanesComercialesPosibles;
	end;
end;

procedure TFrmPlanesComerciales.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFrmPlanesComerciales.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFrmPlanesComerciales.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, FieldByName('PlanComercial').AsString);
        TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFrmPlanesComerciales.FormShow(Sender: TObject);
begin
	DBList1.Reload;
end;

procedure TFrmPlanesComerciales.BtnAceptarClick(Sender: TObject);

    function execInsertarPlanComercial:Integer;
    resourcestring
        MSG_INSERTAR_PLAN_COMERCIAL     = 'No se pudo insertar el nuevo Plan Comercial.';
        CAPTION_INSERTAR_PLAN_COMERCIAL = 'Insertar Plan Comercial';
    begin
        result := 0;
        try
            with InsertarPlanComercial do begin
                Parameters.ParamByName('@ImporteHojaDetalle').value := neImporteDetalle.Value;
                Parameters.ParamByName('@ImporteEnvioDomicilio').value :=  neImporteEnvioDomicilio.Value;
                Parameters.ParamByName('@Descripcion').Value := txt_Descripcion.Text;
                Parameters.ParamByName('@Observaciones').Value := MemoObservaciones.Text;
                execProc;
                result := Parameters.ParamByName('@PlanComercial').value;
            end;
        except On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_INSERTAR_PLAN_COMERCIAL, E.message, CAPTION_INSERTAR_PLAN_COMERCIAL, MB_ICONSTOP);
            end;
        end;
    end;

    function execActualizarPlanComercial:Boolean;
    resourcestring
        MSG_ACTUALIZAR_PLAN_COMERCIAL = 'No se pudieron actualizar los datos del Plan Comercial.';
        CAPTION_ACTUALIZAR_PLAN_COMERCIAL = 'Actualizar Plan Comercial';

    begin
        result := true;
        try
            with ActualizarPlanComercial do begin
                Parameters.ParamByName('PlanComercial').value := txt_CodigoPlanComercial.value;
                Parameters.ParamByName('ImporteHojaDetalle').value := neImporteDetalle.Value;
                Parameters.ParamByName('ImporteEnvioDomicilio').value :=  neImporteEnvioDomicilio.Value;
                Parameters.ParamByName('Descripcion').Value := txt_Descripcion.Text;
                Parameters.ParamByName('Observaciones').Value := MemoObservaciones.Text;
                execSQL;
            end;
        except On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ACTUALIZAR_PLAN_COMERCIAL, E.message, CAPTION_ACTUALIZAR_PLAN_COMERCIAL, MB_ICONSTOP);
                result := false;
            end;
        end;
    end;

resourcestring
    CAPTION_VALIDAR_PLAN_COMERCIAL = 'Validar Plan Comercial';
    MSG_DESCRIPCION = 'El Plan Comercial debe tener una descripci�n.';
    MSG_TIPOS_PAGO  = 'Debe seleccionar al menos un tipo de pago';
    MSG_CATEGORIAS  = 'Debe seleccionar al menos una categor�a';
    MSG_TIPO_CLIENTE_ADHESION  = 'Debe seleccionar al menos una posibilidad de tipo de cliente - adhesi�n';

begin
    if not ValidateControls([txt_Descripcion],
      [Trim(txt_Descripcion.text) <> ''],
      CAPTION_VALIDAR_PLAN_COMERCIAL,
      [MSG_DESCRIPCION]) then exit;

    if not SeleccionoAlgunItem(clbTipoPago) then begin
        MsgBox(MSG_TIPOS_PAGO, CAPTION_VALIDAR_PLAN_COMERCIAL, MB_ICONSTOP);
        PageControl.ActivePage := tab_TipoPago;
        exit;
    end;
    if not SeleccionoAlgunItem(clbCategorias) then begin
        MsgBox(MSG_CATEGORIAS, CAPTION_VALIDAR_PLAN_COMERCIAL, MB_ICONSTOP);
        PageControl.ActivePage := Tab_Categorias;
        exit;
    end;
    if not SeleccionoAlgunItem(clbTipoAdhesionTipoCliente) then begin
        MsgBox(MSG_TIPO_CLIENTE_ADHESION, CAPTION_VALIDAR_PLAN_COMERCIAL, MB_ICONSTOP);
        PageControl.ActivePage := tab_AdhesionTipoCliente;        
        exit;
    end;

	Screen.Cursor := crHourGlass;
    DMConnections.BaseCAC.BeginTrans;
    if (DbList1.Estado = Alta) then begin
        txt_CodigoPlanComercial.value := ExecInsertarPlanComercial;
        if (txt_CodigoPlanComercial.value = 0) then exit;
    end else
        if not execActualizarPlanComercial then exit;

    //Ahora agrego o elimino las categorias por Promocion.
    if not ActualizarCategorias then begin
        DMConnections.BaseCAC.RollbackTrans;
        Screen.Cursor := crDefault;
        exit;
    end;

    if not ActualizarTipoPago then begin
        DMConnections.BaseCAC.RollbackTrans;
        Screen.Cursor := crDefault;
        exit;
    end;
    if not ActualizarPlanesComercialesPosibles then begin
        DMConnections.BaseCAC.RollbackTrans;
        Screen.Cursor := crDefault;
        exit;
    end;

    DMConnections.BaseCAC.CommitTrans;
    Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFrmPlanesComerciales.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFrmPlanesComerciales.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFrmPlanesComerciales.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFrmPlanesComerciales.Volver_Campos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DBList1.Reload;
	DbList1.SetFocus;
    Tab_General.Enabled 	    := False;
    Tab_Categorias.Enabled 	    := False;
    Tab_TipoPago.Enabled        := False;
    Tab_AdhesionTipoCliente.Enabled 	:= False;
	Notebook.PageIndex 			:= 0;
    PageControl.ActivePage := Tab_General;
    txt_CodigoPlanComercial.Enabled := True;
end;


procedure TFrmPlanesComerciales.CargarCategorias;
Var
	i: Integer;
begin
    with ObtenerPlanComercialCategoria do begin
        parameters.ParamByName('@PlanComercial').value := tblPlanesComerciales.fieldByName('PlanComercial').asInteger;
        if not OpenTables([ObtenerPlanComercialCategoria]) then Exit;
        clbCategorias.Items.Clear;
        i := 0;
        While not Eof do begin
            clbCategorias.Items.Add(PadR(FieldByName('Descripcion').AsString, 500, ' ') +
                  IStr(FieldByName('Categoria').AsInteger, 10));
            clbCategorias.Checked[i] := FieldByName('Pertenece').AsInteger > 0;
            Inc(i);
            Next;
        end;
        for i := 0 to clbCategorias.Items.Count - 1 do
            clbCategorias.ItemEnabled[i] := DbList1.Estado in [Alta, Modi];
	    Close;
    end;
end;

procedure TFrmPlanesComerciales.CargarPlanComercialTiposPago;
Var
	i: Integer;
begin
    with ObtenerPlanComercialTiposPago do begin
        parameters.ParamByName('@PlanComercial').value := tblPlanesComerciales.fieldByName('PlanComercial').asINteger;
        if not OpenTables([ObtenerPlanComercialTiposPago]) then Exit;
        clbTipoPago.Items.Clear;
        i := 0;
        While not Eof do begin
            clbTipoPago.Items.Add(PadR(FieldByName('Descripcion').AsString, 500, ' ') +
                  Trim(FieldByName('CodigoTipoMedioPago').AsString));
            clbTipoPago.Checked[i] := FieldByName('Pertenece').AsInteger > 0;
            Inc(i);
            Next;
        end;
        for i := 0 to clbTipoPago.Items.Count - 1 do
            clbTipoPago.ItemEnabled[i] := DbList1.Estado in [Alta, Modi];
	    Close;
    end;
end;

procedure TFrmPlanesComerciales.CargarPlanesComercialesPosibles;
Var i: Integer;
    PlanComercialPosible: PTPlanComercialPosible;
    Descripcion: AnsiString;
begin
    with ObtenerPlanesComercialesPosibles do begin
        parameters.ParamByName('@PlanComercial').value := tblPlanesComerciales.fieldByName('PlanComercial').asINteger;
        if not OpenTables([ObtenerPlanesComercialesPosibles]) then Exit;
        while clbTipoAdhesionTipoCliente.Items.Count > 0 do begin
            dispose(PTPlanComercialPosible(clbTipoAdhesionTipoCliente.Items.Objects[0]));
            clbTipoAdhesionTipoCliente.Items.Delete(0);
        end;
        i := 0;
        While not Eof do begin
            new(PlanComercialPosible);
            PlanComercialPosible^.TipoCliente := FieldByName('TipoCliente').AsInteger;
            PlanComercialPosible^.TipoAdhesion := FieldByName('TipoAdhesion').AsInteger;
            Descripcion := Format('%s - %s',[trim(FieldByName('DescripcionTipoCliente').AsString), Trim(FieldByName('DescripcionTipoAdhesion').AsString)]);
            clbTipoAdhesionTipoCliente.Items.AddObject(Descripcion, TObject(PlanComercialPosible));
            clbTipoAdhesionTipoCliente.Checked[i] := FieldByName('Pertenece').AsInteger > 0;
            Inc(i);
            Next;
        end;
        for i := 0 to clbTipoAdhesionTipoCliente.Items.Count - 1 do
            clbTipoAdhesionTipoCliente.ItemEnabled[i] := DbList1.Estado in [Alta, Modi];
	    Close;
    end;
end;

Function  TFrmPlanesComerciales.ActualizarCategorias;
resourcestring
    MSG_CATEGORIAS = 'No se pudieron actualizar las Categor�as para el Plan Comercial.';
    CAPTION_ACTUALIZAR_CATEGORIAS = 'Actualizar Categor�as del Plan Comercial';

var i:Integer;
begin
    Result := true;
    if not OpenTables([tblPlanesComercialesCategorias]) then begin
        result := false;
        exit;
    end;
    try
        EliminarPlanComercialCategoria.parameters.ParamByName('PlanComercial').value := Trunc(txt_CodigoPlanComercial.Value);
        EliminarPlanComercialCategoria.ExecSql;
    except
        On E: Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
            MsgBoxErr(MSG_CATEGORIAS, E.message, CAPTION_ACTUALIZAR_CATEGORIAS, MB_ICONSTOP);
            exit;
        end;
    end;
    for i:= 0 to clbCategorias.Items.Count - 1 do begin
        if clbCategorias.Checked[i] then begin
            try
                 tblPlanesComercialesCategorias.Append;
                 tblPlanesComercialesCategorias.FieldByName('PlanComercial').AsInteger :=
                        Trunc(txt_CodigoPlanComercial.Value);
                 tblPlanesComercialesCategorias.FieldByName('Categoria').AsInteger :=
                        IVal(StrRight(clbCategorias.Items[i], 20));
                 tblPlanesComercialesCategorias.Post;
            except
                On E: Exception do begin
                    tblPlanesComercialesCategorias.Cancel;
                    MsgBoxErr(MSG_CATEGORIAS, E.message, CAPTION_ACTUALIZAR_CATEGORIAS, MB_ICONSTOP);
                    result := false;
                    exit;
                end;
            end;
        end;
    end;
    tblPlanesComercialesCategorias.Close;
end;



Function TFrmPlanesComerciales.ActualizarTipoPago;
resourcestring
    MSG_TIPO_PAGO = 'No se pudieron actualizar los Tipos de Pago para el Plan Comercial.';
    CAPTION_TIPO_PAGO = 'Actualizar Tipos de Pago del Plan Comercial';
var i:Integer;
begin
    result := True;
    if not OpenTables([tblPlanesComercialesTipoPago]) then begin
        result := false;
        exit;
    end;
    try
        EliminarPlanComercialTipoPago.parameters.ParamByName('PlanComercial').value := Trunc(txt_CodigoPlanComercial.Value);
        EliminarPlanCOmercialTipoPago.ExecSql;
    except
        On E: Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
            MsgBoxErr(MSG_TIPO_PAGO, E.message, CAPTION_TIPO_PAGO, MB_ICONSTOP);
            exit;
        end;
    end;
    for i:= 0 to clbTipoPago.Items.Count - 1 do begin
        if clbTipoPago.Checked[i] then begin
            try
                 tblPlanesComercialesTipoPago.Append;
                 tblPlanesComercialesTipoPago.FieldByName('PlanComercial').AsInteger :=
                        Trunc(txt_CodigoPlanComercial.Value);
                 tblPlanesComercialesTipoPago.FieldByName('CodigoTipoPago').AsString :=
                        Trim(StrRight(clbTipoPago.Items[i], 20));
                 tblPlanesComercialesTipoPago.Post;
            except
                On E: Exception do begin
                    tblPlanesComercialesTipoPago.Cancel;
                    MsgBoxErr(MSG_TIPO_PAGO, E.message, CAPTION_TIPO_PAGO, MB_ICONSTOP);
                    result := false;
                    exit;
                end;
            end;
        end;
    end;
    tblPlanesComercialesTipoPago.Close;
end;


Function TFrmPlanesComerciales.ActualizarPlanesComercialesPosibles;
resourcestring
    MSG_TIPO_ADHESION_TIPO_CLIENTE = 'No se pudieron actualizar los Tipos de Adhesi�n y Tipos de Cliente para el Plan Comercial.';
    CAPTION_TIPO_ADHESION_TIPO_CLIENTE = 'Actualizar Tipos de Adhesi�n y Tipos de Cliente del Plan Comercial';
var i:Integer;
begin
    result := true;
    if not OpenTables([tblPlanesComercialesPosibles]) then begin
        result := false;
        exit;
    end;
    try
        EliminarPlanComercialPosible.parameters.ParamByName('PlanComercial').value := Trunc(txt_CodigoPlanComercial.Value);
        EliminarPlanCOmercialPosible.ExecSql;
    except
        On E: Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
            MsgBoxErr(MSG_TIPO_ADHESION_TIPO_CLIENTE, E.message, CAPTION_TIPO_ADHESION_TIPO_CLIENTE, MB_ICONSTOP);
            exit;
        end;
    end;
    for i:= 0 to clbTipoAdhesionTipoCLiente.Items.Count - 1 do begin
        if clbTipoAdhesionTipoCLiente.Checked[i] then begin
            try
                 tblPlanesComercialesPosibles.Append;
                 tblPlanesComercialesPosibles.FieldByName('PlanComercial').AsInteger :=
                        Trunc(txt_CodigoPlanComercial.Value);
                 tblPlanesComercialesPosibles.FieldByName('TipoCliente').AsInteger :=
                        PTPlanComercialPosible(clbTipoAdhesionTipoCliente.Items.Objects[i])^.TipoCliente;
                 tblPlanesComercialesPosibles.FieldByName('TipoAdhesion').AsInteger :=
                        PTPlanComercialPosible(clbTipoAdhesionTipoCliente.Items.Objects[i])^.TipoAdhesion;
                 tblPlanesComercialesPosibles.Post;
            except
                On E: Exception do begin
                    tblPlanesComercialesPosibles.Cancel;
                    MsgBoxErr(MSG_TIPO_ADHESION_TIPO_CLIENTE, E.message, CAPTION_TIPO_ADHESION_TIPO_CLIENTE, MB_ICONSTOP);
                    result := false;
                    exit;
                end;
            end;
        end;
    end;
    tblPlanesComercialesPosibles.Close;
end;

function TFrmPlanesComerciales.SeleccionoAlgunItem(clb: TCheckListBox): boolean;
var
    i: integer;
    hayAlguno: boolean;
begin
    hayAlguno := false;
    for i:= 0 to clb.Items.Count - 1 do begin
        if clb.Checked[i] then
            hayAlguno := true;
    end;
    result := hayAlguno;
end;

end.
