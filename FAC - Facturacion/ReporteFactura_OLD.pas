unit ReporteFactura_OLD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppStrtch, ppRegion, ppClass, ppVar,
  ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppDBBDE, ppComm, ppRelatv,
  ppDBPipe, DB, ADODB, peaTypes, DMConnection, RBSetup,
  pptypes, peaprocs, ExtCtrls, jpeg, ppBarCod, ppParameter, ppSubRpt,
  ppModule, raCodMod, UtilProc, RStrings, ppMemo;

type
  TformReporteFactura = class(TForm)
    qryObtenerTarjetaCredito: TADOQuery;
    qryObtenerCuentaBancaria: TADOQuery;
    ObtenerDatosComprobante: TADOStoredProc;
    dsDatosCliente: TDataSource;
    dsDatosComprobantes: TDataSource;
    dsCuentaDebito: TDataSource;
    ppCuentaDebito: TppDBPipeline;
    rp_FacturaOrg: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDBText14: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText3: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppLine3: TppLine;
    ppDBCalc1: TppDBCalc;
    ppDBText13: TppDBText;
    ActualizarComprobanteImpreso: TADOQuery;
    rbi_Factura: TRBInterface;
    ppDatosComprobantes: TppDBPipeline;
    ppDatosCliente: TppDBPipeline;
    ppImage1: TppImage;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppLabel15: TppLabel;
    ppLabel2: TppLabel;
    ppImage2: TppImage;
    ppShape3: TppShape;
    ppShape4: TppShape;
    ppShape5: TppShape;
    ppShape6: TppShape;
    ppLabel3: TppLabel;
    ppLabel5: TppLabel;
    ppShape7: TppShape;
    ppLabel6: TppLabel;
    ppShape8: TppShape;
    ppShape9: TppShape;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel1: TppLabel;
    ppLabel12: TppLabel;
    ppLabel16: TppLabel;
    ppDBCalc2: TppDBCalc;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppDBText2: TppDBText;
    ppDBText4: TppDBText;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppLabel4: TppLabel;
    ppDBCalc3: TppDBCalc;
    ppLabel10: TppLabel;
    ppLabel20: TppLabel;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppLabel19: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppLine7: TppLine;
    ppSummaryBand1: TppSummaryBand;
    ppBarCode1: TppBarCode;
    ppBarCode2: TppBarCode;
    ppShape10: TppShape;
    ppShape11: TppShape;
    ppShape12: TppShape;
    ppShape13: TppShape;
    ppShape14: TppShape;
    ppShape15: TppShape;
    ppShape16: TppShape;
    ppParameterList1: TppParameterList;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ObtenerDatosCliente: TADOStoredProc;
    rp_Factura: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppShape21: TppShape;
    ppDBText18: TppDBText;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel29: TppLabel;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppLabel31: TppLabel;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppDetailBand2: TppDetailBand;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppImage4: TppImage;
    ppLabel39: TppLabel;
    ppLabel40: TppLabel;
    ppLabel41: TppLabel;
    ppBarCode3: TppBarCode;
    ppBarCode4: TppBarCode;
    ppSummaryBand2: TppSummaryBand;
    ppParameterList2: TppParameterList;
    ppShape33: TppShape;
    ppShape34: TppShape;
    ppShape35: TppShape;
    ppLabel32: TppLabel;
    ppLabel34: TppLabel;
    ppShape17: TppShape;
    ppShape19: TppShape;
    ppLabel47: TppLabel;
    ppShape22: TppShape;
    ppDBText22: TppDBText;
    ppLabel35: TppLabel;
    ppLabel37: TppLabel;
    ppLabel43: TppLabel;
    ObtenerDomicilioConvenio: TADOStoredProc;
    ppDomicilioFacturacion: TppDBPipeline;
    dsDomicilioFacturacion: TDataSource;
    ppLabel42: TppLabel;
    ppDBText29: TppDBText;
    ppShape18: TppShape;
    ppShape23: TppShape;
    ppLabel30: TppLabel;
    ppDBText24: TppDBText;
    ppLine8: TppLine;
    ppLine9: TppLine;
    ObtenerCargosComprobante: TADOStoredProc;
    dsObtenerCargosComprobante: TDataSource;
    ppCargosComprobante: TppDBPipeline;
    ppsbCargos: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppDBText30: TppDBText;
    ObtenerCargosComprobanteCodigoConcepto: TWordField;
    ObtenerCargosComprobanteDescripcion: TStringField;
    ObtenerCargosComprobanteImporte: TBCDField;
    ObtenerCargosComprobanteDescriImporte: TStringField;
    ppDBText32: TppDBText;
    raCodeModule1: TraCodeModule;
    raCodeModule2: TraCodeModule;
    ppImage3: TppImage;
    ppDBText19: TppDBText;
    ppDBText33: TppDBText;
    ObtenerDatosComprobanteCodigoTipoMedioPago: TWordField;
    ObtenerDatosComprobanteNumeroConvenio: TStringField;
    ObtenerDatosComprobanteNumeroConvenioFormateado: TStringField;
    ObtenerDatosComprobantePAC_CodigoBanco: TIntegerField;
    ObtenerDatosComprobantePAC_CodigoTipoCuentaBancaria: TWordField;
    ObtenerDatosComprobantePAC_NroCuentaBancaria: TStringField;
    ObtenerDatosComprobantePAC_Sucursal: TStringField;
    ObtenerDatosComprobantePAT_CodigoTipoTarjetaCredito: TWordField;
    ObtenerDatosComprobantePAT_NumeroTarjetaCredito: TStringField;
    ObtenerDatosComprobantePAT_FechaVencimiento: TStringField;
    ObtenerDatosComprobantePAT_CodigoEmisorTarjetaCredito: TWordField;
    ObtenerDatosComprobanteTipoComprobante: TStringField;
    ObtenerDatosComprobanteNumeroComprobante: TBCDField;
    ObtenerDatosComprobanteFecha: TDateTimeField;
    ObtenerDatosComprobanteVencimiento: TDateTimeField;
    ObtenerDatosComprobanteConcesionaria: TStringField;
    ObtenerDatosComprobanteCodigoConvenio: TIntegerField;
    ObtenerDatosComprobanteNumeroMovimiento: TAutoIncField;
    ObtenerDatosComprobanteIndiceVehiculo: TIntegerField;
    ObtenerDatosComprobanteCodigoConcepto: TWordField;
    ObtenerDatosComprobanteDescripcion: TStringField;
    ObtenerDatosComprobanteImporte: TBCDField;
    ObtenerDatosComprobanteImporteTotal: TBCDField;
    ObtenerDatosComprobanteDescriImporte: TStringField;
    ObtenerDatosComprobantePatente: TStringField;
    ppShape20: TppShape;
    ppLabel24: TppLabel;
    ObtenerDatosComprobanteFechaHoraImpreso: TDateTimeField;
    ppLabelFechaEmision: TppLabel;
    ppLabel28: TppLabel;
    ObtenerSaldoAnterior: TADOStoredProc;
    ppLabel44: TppLabel;
    ppDBCalc4: TppDBCalc;
    dsObtenerSaldoAnterior: TDataSource;
    ppObtenerSaldoAnterior: TppDBPipeline;
    ppDBText34: TppDBText;
    ppDBText35: TppDBText;
    LabelFechaVencimiento: TppLabel;
    ppLabelVencimiento: TppLabel;
    ppLabel33: TppLabel;
    txtMensajes: TppMemo;
    ObtenerTagsSinBateria: TADOStoredProc;
    procedure ppGroup1GetBreakValue(Sender: TObject;
      var aBreakValue: String);
    procedure ppLTituloCargosGetText(Sender: TObject; var Text: String);
    procedure pplSubtotalGetText(Sender: TObject; var Text: String);
    procedure ppBarCode1Print(Sender: TObject);
    procedure ppDBText18GetText(Sender: TObject; var Text: String);
    procedure ppDBText28GetText(Sender: TObject; var Text: String);
    procedure ppLabelFechaEmisionGetText(Sender: TObject; var Text: String);
    procedure LabelFechaVencimientoGetText(Sender: TObject;
      var Text: String);
    procedure ppLabelVencimientoGetText(Sender: TObject; var Text: String);
    procedure txtMensajesPrint(Sender: TObject);
  private
    FDiasVencimiento: integer;
  public
    { Public declarations }
     function ConfigurarImpresora: AnsiString;
     function getImpresora:AnsiString;
     Function Execute(TipoComprobante: Char; CodigoCliente: Integer; NumeroComprobante: Double;
       ShowInterface: Boolean; titulo: AnsiString; DiasVencimiento: integer): Boolean;
  end;

implementation

{$R *.dfm}


function TformReporteFactura.ConfigurarImpresora: AnsiString;
begin
    rbi_Factura.Configure;
    result := getImpresora;
end;

Function TFormReporteFactura.Execute(TipoComprobante: Char; CodigoCliente: Integer; NumeroComprobante: Double; showInterface: Boolean; titulo: AnsiString; DiasVencimiento: integer):Boolean;
resourcestring
    MSG_FACTURA_TARJETA_CREDITO = 'Factura debitada en la Tarjeta de Credito %s n�mero %s';
    MSG_FACTURA_CUENTA_BANCARIA = 'Factura debitada en la Cuenta Bancaria %s n�mero %s';

var
	Config, ConfigOriginal: TRBConfig;
    Cpostal, FileTemp: AnsiString;
begin
    Result := False;
    // Control que exista el directorio donde se guardaran los rpt
    FileTemp := ObtenerNombreArchivoFactura(DMConnections.BaseCAC,'F', NumeroComprobante);

    if (not DirectoryExists(ExtractFilePath(FileTemp))) then begin
            MsgBoxErr(MSG_ERROR_DIRECTORIO_ACCESO, 'El proceso de impresi�n concluir�.',Caption, MB_ICONSTOP);
            Exit;
    end;

    FDiasVencimiento := DiasVencimiento;

// Obtener datos del cliente
    ObtenerDatosCliente.close;
    ObtenerDatosCliente.Parameters.ParamByName('@CodigoCliente').value := CodigoCliente;
    ObtenerDatosCliente.open;

// Obtener datos a imprimir
    ObtenerDatosComprobante.Close;
    ObtenerDatosComprobante.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
    ObtenerDatosComprobante.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    ObtenerDatosComprobante.open;
// Obtener informacion de cuenta de debito
    if assigned(dsCuentaDebito.DataSet) then
        dsCuentaDebito.DataSet.close;
    if ObtenerDatosComprobante.FieldByName('CodigoTipoMedioPago').value = TPA_NINGUNO then begin
//        lCuentaDebito.Caption := MSG_FACTURA_PAGO_MANUAL;
    end else begin
        if ObtenerDatosComprobante.FieldByName('CodigoTipoMedioPago').value = TPA_PAT then begin
            dsCuentaDebito.DataSet := qryObtenerTarjetaCredito;
            qryObtenerTarjetaCredito.parameters.paramByName('CodigoTipoTarjetaCredito').value :=
                ObtenerDatosComprobante.FieldByName('PAT_CodigoTipoTarjetaCredito').value;
            qryObtenerTarjetaCredito.parameters.paramByName('CodigoConvenio').value := ObtenerDatosComprobante.FieldByName('CodigoConvenio').AsInteger;
            qryObtenerTarjetaCredito.open;
        end else begin
            dsCuentaDebito.DataSet := qryObtenerCuentaBancaria;
            qryObtenerCuentaBancaria.parameters.paramByName('CodigoBanco').value :=
                ObtenerDatosComprobante.FieldByName('PAC_CodigoBanco').value;
            qryObtenerTarjetaCredito.parameters.paramByName('CodigoConvenio').value := ObtenerDatosComprobante.FieldByName('CodigoConvenio').AsInteger;
            qryObtenerCuentaBancaria.open;
        end;
    end;

    if trim(ObtenerDatosCliente.fieldByName('CodigoPostal').asString) <> '' then
        Cpostal := '  (' + trim(ObtenerDatosCliente.fieldByName('CodigoPostal').asString) + ') ';

    ObtenerDomicilioConvenio.Close;
    ObtenerDomicilioConvenio.Parameters.ParamByName('@CodigoConvenio').Value := ObtenerDatosComprobante.FieldByName('CodigoConvenio').AsInteger;
    ObtenerDomicilioConvenio.Open;

    ObtenerCargosComprobante.Close;
    ObtenerCargosComprobante.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
    ObtenerCargosComprobante.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    ObtenerCargosComprobante.open;

    ObtenerSaldoAnterior.Close;
    ObtenerSaldoAnterior.Parameters.ParamByName('@CodigoConvenio').Value := ObtenerDatosComprobante.FieldByName('CodigoConvenio').AsInteger;
    ObtenerSaldoAnterior.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
    ObtenerSaldoAnterior.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    ObtenerSaldoAnterior.open;

    ObtenerTagsSinBateria.Close;
    ObtenerTagsSinBateria.Parameters.ParamByName('@CodigoConvenio').Value := ObtenerDatosComprobante.FieldByName('CodigoConvenio').AsInteger;
    ObtenerTagsSinBateria.Open;


 	//Grabar Factura a Disco
	//FileTemp := ObtenerNombreArchivoFactura(DMConnections.BaseCAC,'F', NumeroComprobante);
    ConfigOriginal		:= rbi_Factura.GetConfig;
    Config 				:= ConfigOriginal;
    Config.FileName		:= FileTemp;
    Config.DeviceType	:= dtArchive;
    rbi_Factura.SetConfig(Config);
    rbi_Factura.Execute(False);
//    ConfigOriginal.DeviceType := dtScreen + dtPrinter; // Para que la segunda vez imprima
    rbi_Factura.SetConfig(ConfigOriginal);

    //Ahora me fijo que habia configurado el Usuario
    if ConfigOriginal.DeviceType <> dtArchive then
        rbi_Factura.Execute(ShowInterface);

    //	Marcar el comprobante como impreso
    ActualizarComprobanteImpreso.Close;
    ActualizarComprobanteImpreso.Parameters.ParamByName('FechaHora').value := NowBase(DMConnections.BaseCAC);
    ActualizarComprobanteImpreso.Parameters.ParamByName('TipoComprobante').value := TipoComprobante;
    ActualizarComprobanteImpreso.Parameters.ParamByName('NumeroComprobante').value := NumeroComprobante;
    ActualizarComprobanteImpreso.Parameters.ParamByName('DiasVencimiento').value := DiasVencimiento;
    ActualizarComprobanteImpreso.ExecSQL;
    Result := True;
end;


function TformReporteFactura.getImpresora: AnsiString;
begin
    result := rbi_Factura.GetConfig.PrinterName;
end;

procedure TformReporteFactura.ppGroup1GetBreakValue(Sender: TObject;
  var aBreakValue: String);
begin
    aBreakValue := ObtenerDatosComprobante.FieldByName('Concesionaria').asString;
end;

procedure TformReporteFactura.ppLTituloCargosGetText(Sender: TObject;
  var Text: String);
resourcestring
    MSG_CONCEPTOS_COMUNES = 'Conceptos comunes';
    MSG_CONCESIONARIA = 'Concesionaria %s';
begin
    if ObtenerDatosComprobante.FieldByName('Concesionaria').asString = '' then
        text := MSG_CONCEPTOS_COMUNES
    else
        text := Format(MSG_CONCESIONARIA, [Trim(ObtenerDatosComprobante.FieldByName('Concesionaria').asString)]);
end;

procedure TformReporteFactura.pplSubtotalGetText(Sender: TObject;
  var Text: String);
resourcestring
    MSG_SUBTOTAL_CONCEPTOS_COMUNES = 'Subtotal Conceptos comunes';
    MSG_SUBTOTAL_CONCESIONARIA = 'Subtotal Concesionaria %s';
begin
    if ObtenerDatosComprobante.FieldByName('Concesionaria').asString = '' then
        text := MSG_SUBTOTAL_CONCEPTOS_COMUNES
    else
        text := Format(MSG_SUBTOTAL_CONCESIONARIA, [Trim(ObtenerDatosComprobante.FieldByName('Concesionaria').asString)]);
end;

procedure TformReporteFactura.ppBarCode1Print(Sender: TObject);
begin
    ppBarCode1.Data := ObtenerDatosComprobante.fieldbyname('NumeroConvenioFormateado').AsString;
    ppBarCode2.Data := ObtenerDatosComprobante.fieldbyname('NumeroConvenioFormateado').AsString;
end;

procedure TformReporteFactura.ppDBText18GetText(Sender: TObject;
  var Text: String);
begin
    Text := Trim(Text) + ' ' + Trim(ObtenerDomicilioConvenio.fieldbyname('Numero').AsString);
end;

procedure TformReporteFactura.ppDBText28GetText(Sender: TObject;
  var Text: String);
begin
    Text := '$ ' + Trim(Text);
end;

procedure TformReporteFactura.ppLabelFechaEmisionGetText(Sender: TObject;
  var Text: String);
begin
    Text := FormatDateTime('dd-mm-yyyy', NowBase(DMConnections.BaseCAC));
end;

procedure TformReporteFactura.LabelFechaVencimientoGetText(Sender: TObject;
  var Text: String);
begin
    Text := FormatDateTime('dd-mm-yyyy', NowBase(DMConnections.BaseCAC) + FDiasVencimiento);
end;

procedure TformReporteFactura.ppLabelVencimientoGetText(Sender: TObject;
  var Text: String);
var
    Fecha: TDateTime;
    NombreMes: String;
begin
    Fecha := NowBase(DMConnections.BaseCAC);
    NombreMes := MesNombre(Fecha + FDiasVencimiento);
    Text := FormatDateTime('dd-"' + Trim(NombreMes) + '"-yyyy', Fecha + FDiasVencimiento);
end;

procedure TformReporteFactura.txtMensajesPrint(Sender: TObject);
begin
    if (ObtenerTagsSinBateria.Active) and (ObtenerTagsSinBateria.RecordCount > 0) then begin
        txtMensajes.Lines.Add(MSG_CAPTION_BATERIA_BAJA);
        ObtenerTagsSinBateria.First;
        while not ObtenerTagsSinBateria.Eof do begin
            txtMensajes.Lines.Add('- ' + Trim(ObtenerTagsSinBateria.fieldbyName('Patente').AsString));
            ObtenerTagsSinBateria.Next;
        end;
        txtMensajes.Lines.Add(' ');
        txtMensajes.Lines.Add(MSG_CAPTION_PEDIDO_PRESENTARCION);
    end;

end;

end.
