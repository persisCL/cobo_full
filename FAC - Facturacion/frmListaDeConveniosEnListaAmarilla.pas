{------------------------------------------------------------------------
        frmListaDeConveniosEnListaAmarilla

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description :       (Ref SS 660)
                    Permite listar todos los convenio que est�n en
                    seguimiento de lista amarilla

    Author      : CQuezadaI
    Date        : 10-Octubre-2012                    
    Firma       : SS_660_CQU_20121010
    Description : Agrega orden a las columnas del dblListaAmarilla.
                  Agrega mensaje cuando se filtra y no encuetra registros.
                  Agrega validaci�n a las fechas en el filtro.
                  Modifica el Layout ordenando los objetos del formulario.
                  Modifica el Combo de estado para que parta siempre con la opci�n "Todos".
                  Agrega el re-env�o de cartas.

    Author      : MDiazV
    Date        : 07-Marzo-2013
    Firma       : SS_660_MDI_20130307
    Descripci�n : Agrega cambio masivo de estados.
                  Agrega exclusi�n de convenios de carpetas legales cuando son pasados a inhabilitados.
                  Agrega opci�n de seleccionar todos y opciones afines en el listado de convenios.
                  Agrega cambio de nombre de columna Fecha Ingreso por �Fecha Pre-Evaluaci�n�
                  Se corrige funcionamiento de botones, de acuerdo a registros seleccionados en el listado convenios
                  Se suprime el cambio de estado de convenio por selecci�n simple en el listado. S�lo se cambian los que sean chequeados
                  Se deja de usar procedure dsObtenerConveniosEnListaAmarillaDataChange.

    Author      : MDiazV
    Date        : 09-Abril-2013
    Firma       : SS_660_MDI_20130409
    Descripci�n : Se suprime cambia caption de bot�n Filtrar por Buscar.
                  Se oculta antiguo boton Buscar.
                  Se a�ade la opcion de filtrar datos por rut cliente.
                  Se carga convenios del rut, por medio de 'enter' o por medio de bot�n Buscar.
                  Se mejora comportamiento de botones de selecci�n y cambio estado.
                  Se a�ade par�metro NumeroDocumento al sp ObtenerConveniosEnListaAmarilla.
                  Se corrige mensaje en formulario cambio de estado al realizar cambio masivo.

    Author      : MDiazV
    Date        : 10-Abril-2013
    Firma       : SS_660_MDI_20130409
    Descripci�n : Se mejora comportamiento de opci�n cargar convenios del rut por medio de enter o boton buscar.

    Author      : MDiazV
    Date        : 11-04-2013
    Firma       : SS_660_MDI_20130409
    Descripci�n : Se asegura que bot�n "Cambiar Estado" no se habilite en estado pagado o eliminado.

    Author      : MDiazV
    Date        : 12-04-2013
    Firma       : SS_660_MDI_20130409
    Descripci�n : Se validan permisos de usuario para habilitaci�n de botones "Nuevo Convenio", "Cambio Estado" y "Re-Enviar Carta"

    Author      : MDiazv
    Date        : 24-04-2013
    Firma       : SS_660_MDI_20130409
    Descripci�n : Se depura c�digo y se modifica el cambio de estado de convenio masivo y unitario, haciendo llamaba a la misma funcion para ambos casos

    Author      : Cquezada
    Date        : 15-05-2013
    Firma       : SS_660_CQU_20130515
    Descripci�n : Corrige las observaciones indicadas de TestCN, no habilitaba el bot�n "CambiarEstado" cuando se seleccionaba todo y hab�a un solo registro.

    Author      : Cquezada
    Date        : 12-06-2013
    Firma       : SS_660_CQU_20130604
    Descripcion : Agrega bot�n para rechazar una carta enviada por Lista Amarilla.

    Author      : Cquezada
    Date        : 28-06-2013
    Firma       : SS_660_CQU_20130628
    Descripcion : Corrige validaci�n del bot�n AutGerencia

    Author      : Mvillarroel
    Date        : 11-07-2013
    Firma       : SS_660_MVI_20130711
    Descripcion : Se agrega campo concesionaria y se valida si al modificar masivamente convenios en estado "Pre-Evaluaci�n",
                  existen convenios con distintas concesionarias.

    Author      : Cquezada
    Date        : 19-07-2013
    Firma       : SS_660_CQU_20130711
    Descripcion : Se agrega la concesionaria como par�metro y resultado de un Stored
                  Se agrega m�todo que obtiene el ID de una columna por su nombre
                  Se agerga Combo con la concesionaria para ser elegido en el filtro
                  Se agerga Orden a las nuevas columnas de Concesionaria y Total de NK Impagas
                  Se cambian Caption de varias columnas y se centra sus t�tulos

    Author      : Cquezada
    Date        : 01-08-2013
    Firma       : SS_660_CQU_20130711
    Descripcion : Se comenta un l�nea que est� de m�s (colocada por error) en el m�todo btnFiltrarClick

    Author      : Cquezada
    Date        : 08-08-2013
    Firma       : SS_660_CQU_20130711
    Descripcion : Se modifica <Seleccione Concesionaria> por <Todas> al cargar el combo de concesionarias

    Firma       :   SS_1120_MVI_20130820
    Descripcion :   Se cambia el llamado a llenar el combo de convenios para que utilice el procedimiento
                    CargarConveniosRUT.
                    Se modifica el procedimiento OnDrawItem para que llame al procedimeinto ItemComboBoxColor.

    Autor       : CQuezadaI
    Firma       : SS_660_CQU_20131104
    Descripcion : Se agrega una funcion (RevisaFiltro) que valida que se haya ingresado algun dato en el filtro y consulta si contin�a.

  Firma         : SS_660_NDR_20131112
  Descripcion   : Se pone el cursor de HourGlass para las consultas que podrian demorar.

    Firma       :   SS_660_CQU_20140307
    Descripcion :   Se agrega un par de l�neas para deshabilitar el bot�n btnAnularCartas ya que quedaba habilitado cuando no deb�a.

    Firma       : SS_660F_MCA_20140805
    Descripcion : se agrega nueva columna Observaci�n con imagen que al pinchar sobre ella, abre una nueva ventana que permite ingresar observaciones a los convenios
                  inhabilitados por lista amarilla.

    Firma       :   SS_1231_MBE_20141114
    Description :   Se agrega la combobox de Autorizaciones que permita el filtro por dichos bits

    Firma       :   SS_1232_CQU_20141124
    Description :   Se agregan las columnas Deuda CN, Deuda AMB, Deuda Otras

    Firma       :   SS_1249_MCA_20150406
    Descripcion :   se agrega constante columna observacion para identificar dicha columna
        

    Firma       : SS_1408_MCA_20151027
    Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

-------------------------------------------------------------------------}
unit frmListaDeConveniosEnListaAmarilla;

interface

uses
    DMConnection,
    BuscaClientes,
    frmCambiarEstadoListaAmarilla,
    frmNuevoConvenioListaAmarilla,
    frmHistoricoCartasListaAmarilla,                                                // SS_660_CQU_20121010
    frmListaDeHistoricoCartasListaAmarilla,                                         // SS_660_CQU_20121010
    ConstParametrosGenerales,                                                       // SS_660_CQU_20121010
    frmRechazarCartaListaAmarilla,                                                  // SS_660_CQU_20130604
    frmObservacionesConveniosListaAmarilla,   //SS_660F_MCA_20140805
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, ListBoxEx, DBListEx, ExtCtrls,
  DB, ADODB, VariantComboBox, UtilProc, Util, PeaProcs,  PeaTypes,
  DmiCtrls, RStrings, SysUtilsCN, ImgList, DBClient, Provider, SimpleDS;            // SS_660_CQU_20121010

type
    TListaDeConveniosEnListaAmarillaForm = class(TForm)
    dblListaAmarilla: TDBListEx;
    spObtenerConveniosEnListaAmarilla: TADOStoredProc;
    dsObtenerConveniosEnListaAmarilla: TDataSource;
    spObtenerCliente: TADOStoredProc;
    spObtenerConveniosCliente: TADOStoredProc;                         // SS_660_MDI_20130409
    ilCheck: TImageList;                        // SS_660_MDI_20130409
    cdsConvenios: TClientDataSet;                         // SS_660_MDI_20130409
    cdsConveniosIDConvenioInhabilitado: TIntegerField;    // SS_660_MDI_20130409
    cdsConveniosCodigoConvenio: TIntegerField;            // SS_660_MDI_20130409
    cdsConveniosEstado: TIntegerField;                    // SS_660_MDI_20130409
    cdsConveniosNumeroConvenio: TStringField;             // SS_660_MDI_20130409
    cdsConveniosDescripcion: TStringField;                // SS_660_MDI_20130409
    cdsConveniosFechaEnvioCarta: TDateTimeField;          // SS_660_MDI_20130409
    cdsConveniosEstaEnLegales: TBooleanField;             // SS_660_MDI_20130409
    cdsConveniosNumeroDocumento: TStringField;                                // SS_660_MDI_20130409
    //cdsConveniosCantReEnvio: TIntegerField;             // SS_660_CQU_20130604  // SS_660_MDI_20130409
    cdsConveniosAutorizaFiscalia: TBooleanField;          // SS_660_MDI_20130409
    cdsConveniosAutorizaGerencia: TBooleanField;                              // SS_660_MDI_20130409
    cdsConveniosFechaAutFiscalia: TDateField;             // SS_660_MDI_20130409
    cdsConveniosUsuarioAutFiscalia: TStringField;         // SS_660_MDI_20130409
    cdsConveniosFechaAutGerencia: TDateField;             // SS_660_MDI_20130409
    cdsConveniosUsuarioAutGerencia: TStringField;         // SS_660_MDI_20130409
    spAutorizarConvenioPorFiscalia: TADOStoredProc;       // SS_660_MDI_20130409
    spAutorizarConvenioPorGerencia: TADOStoredProc;                            // SS_660_CQU_20130604
    cdsConveniosPuedeAutorizarFiscalia: TBooleanField;  // SS_660_CQU_20130604
    cdsConveniosPuedeAutorizarGerencia: TBooleanField;  // SS_660_CQU_20130604
    cdsConveniosPuedeReEnviarCarta: TBooleanField;      // SS_660_CQU_20130604
    cdsConveniosPuedeAnularCarta: TBooleanField;
    cdsConveniosConcesionaria: TStringField;
    cdsConveniosTotalApagar: TStringField;
    cdsConveniosCantidadNKImpagas: TStringField;                // SS_660_CQU_20130711
    spObtenerConcesionarias: TADOStoredProc;            // SS_660_CQU_20130711
    cdsConveniosCodigoConcesionaria: TSmallintField;
    grpControl: TGroupBox;
    btnAutFiscalia: TButton;
    btnAutGerencia: TButton;
    btnNuevoConvenio: TButton;
    btnCambiarEstado: TButton;
    btnReEnviarCarta: TButton;
    btnSeleccionarTodos: TButton;
    btnDeselectAll: TButton;
    btnInvertirSeleccion: TButton;
    btnSalir: TButton;
    btnAnularCarta: TButton;
    grpCliente: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    peRUTCliente: TPickEdit;
    vcbConveniosCliente: TVariantComboBox;
    btnBuscar: TButton;
    grpFiltro: TGroupBox;
    lblEstados: TLabel;
    lblFechaCreacion: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    lblFechaAlta: TLabel;
    lblFechaBaja: TLabel;
    lblEstacConceaionaria: TLabel;
    vcbEstados: TVariantComboBox;
    deFechaIngresoDesde: TDateEdit;
    deFechaIngresoHasta: TDateEdit;
    deFechaAltaHasta: TDateEdit;
    deFechaAltaDesde: TDateEdit;
    deFechaBajaDesde: TDateEdit;
    deFechaBajaHasta: TDateEdit;
    btnFiltrar: TButton;
    btnLimpiar: TButton;
    vcbConcesionarias: TVariantComboBox;
    Label5: TLabel;
    cbbAutorizaciones: TComboBox;    // SS_660_CQU_20130711


    procedure btnFiltrarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCambiarEstadoClick(Sender: TObject);
    procedure btnNuevoConvenioClick(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure vcbConveniosClienteDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure peRUTClienteKeyPress(Sender: TObject; var Key: Char);
    procedure btnLimpiarClick(Sender: TObject);
    procedure dsObtenerConveniosEnListaAmarillaDataChange(Sender: TObject;
      Field: TField);
    procedure dblListaAmarillaColumnsHeaderClick(Sender: TObject);  // SS_660_CQU_20121010
    procedure btnReEnviarCartaClick(Sender: TObject);
    procedure dblListaAmarillaLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dblListaAmarillaDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnSeleccionarTodosClick(Sender: TObject);  // SS_660_MDI_20130307
    procedure btnDeselectAllClick(Sender: TObject);       // SS_660_MDI_20130307
    procedure btnInvertirSeleccionClick(Sender: TObject); // SS_660_MDI_20130307
    procedure btnAutFiscaliaClick(Sender: TObject);       // SS_660_MDI_20130409
    procedure btnAutGerenciaClick(Sender: TObject);       // SS_660_MDI_20130409
    procedure btnAnularCartaClick(Sender: TObject);       // SS_660_CQU_20130604
    private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;                    // SS_660_MDI_20130307
    FListaConveniosBaja: TStringList;                     // SS_660_MDI_20130307
    //FCantReEnvios   :   integer;                        // SS_660_CQU_20130604  // SS_660_CQU_20121010
    FGlosaActual : string;                                // SS_660_MDI_20130307
    FEstadoActual : Integer;                              // SS_660_MDI_20130307
    FPrimerEstado : Integer;                              // SS_660_MDI_20130307
    procedure CargarConcesionarias;                       // SS_660_CQU_20130711
  public
    { Public declarations }
     function Inicializar() : Boolean;
     procedure Limpiar;
     procedure LimpiaConvenio;                              //SS_660_MDI_20130409
     procedure CargarEstadosConveniosListaAmarilla;
     //procedure CargarCantidadReEnvios;                      // SS_660_CQU_20130604

  	Function  ListaConveniosIn(valor:string):boolean;         // SS_660_MDI_20130307
  	Function  ListaConveniosInclude(valor:string):boolean;    // SS_660_MDI_20130307
	Function  ListaConveniosExclude(valor:string):boolean;    // SS_660_MDI_20130307
  	Function  ValidaEstados():Boolean;                        // SS_660_MDI_20130307
    Function  RevisaFiltro():Boolean;                         // SS_660_CQU_20131104
  end;

var
  ListaDeConveniosEnListaAmarillaForm: TListaDeConveniosEnListaAmarillaForm;

implementation
{$R *.dfm}


{------------------------------------------------------------------------
        ItemCBColor

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : procedimiento que permite cambiar el color de un elemento
                    de una combobox.

-------------------------------------------------------------------------}
procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor);
begin
    with (cmbBox as TVariantComboBox) do begin
        Canvas.Brush.color := ColorFondo;
        Canvas.FillRect(R);
        Canvas.Font.Color := ColorTexto;
        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption);
   end;
end;

{------------------------------------------------------------------------
        Inicializar

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Inicializa el formulario: variables, objetos, etc.

-------------------------------------------------------------------------}

function TListaDeConveniosEnListaAmarillaForm.Inicializar() : boolean;
resourcestring
    MSG_CAPTION = 'Listado de convenios en Lista Amarilla';
begin
    FListaConveniosBaja := TStringList.Create;
    Self.Caption := MSG_CAPTION;
    CenterForm(Self);
    Limpiar();
    CargarEstadosConveniosListaAmarilla();
    CargarConcesionarias();													// SS_660_CQU_20130711
    //CargarCantidadReEnvios();                                            // SS_660_CQU_20130604 // SS_660_CQU_20121010
    Result:= True;
    LimpiaConvenio();                                                      // SS_660_MDI_20130409
    if not ExisteAcceso('AGREGAR_CONVENIO_LISTA_AMARILLA') then begin      // SS_660_MDI_20130409
        btnNuevoConvenio.Enabled := False;                                 // SS_660_MDI_20130409
    end;                                                                   // SS_660_MDI_20130409
end;

{------------------------------------------------------------------------
        Limpiar

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Limpia los controles para el ingreso de nuevos datos

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.Limpiar;
begin
    peRUTCliente.Text := '';
    // vcbConveniosCliente.Clear;               // SS_660_MDI_20130409
    LimpiaConvenio();                           // SS_660_MDI_20130409
    deFechaIngresoDesde.Date := NullDate;
    deFechaIngresoHasta.Date := NullDate;
    deFechaAltaDesde.Date := NullDate;
    deFechaAltaHasta.Date := NullDate;
    deFechaBajaDesde.Date := NullDate;
    deFechaBajaHasta.Date := NullDate;
    vcbEstados.ItemIndex := 0;                  // SS_660_CQU_20121010
    spObtenerConveniosEnListaAmarilla.Close;    // SS_660_CQU_20121010
    btnCambiarEstado.Enabled := False;          // SS_660_MDI_20130307
    btnReEnviarCarta.Enabled := False;          // SS_660_MDI_20130307
    btnSeleccionarTodos.Enabled := False;       // SS_660_MDI_20130307
    btnDeselectAll.Enabled := False;            // SS_660_MDI_20130307
    btnInvertirSeleccion.Enabled := False;      // SS_660_MDI_20130307
    btnAutFiscalia.Enabled := False;            // SS_660_MDI_20130409
    btnAutGerencia.Enabled := False;            // SS_660_MDI_20130409
    btnAnularCarta.Enabled := False;            // SS_660_CQU_20130604
    vcbConcesionarias.ItemIndex := 0;           // SS_660_CQU_20130711
    cbbAutorizaciones.ItemIndex := 0;           // SS_1231_MBE_20141114
end;

{------------------------------------------------------------------------
        peRUTClienteButtonClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Permite invocar la ventana de b�squeda por Rut.

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.peRUTClienteButtonClick(
  Sender: TObject);
var
    fbc : TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, fbc);
        if fbc.Inicializa(FUltimaBusqueda)then begin
          if (fbc.ShowModal = mrok) then begin
              FUltimaBusqueda := fbc.UltimaBusqueda;
              peRUTCliente.Text := Trim(fbc.Persona.NumeroDocumento);
          end;

    end;

    fbc.Release;

end;

{------------------------------------------------------------------------
        peRUTClienteKeyPress

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Simula un ENTER o Limpia la combo de convenios.

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.peRUTClienteKeyPress(
  Sender: TObject; var Key: Char);
begin
    if Key = #13 then begin
        Key := #0;
        btnBuscarClick(nil);
    end
    else
        LimpiaConvenio();                           // SS_660_MDI_20130409
        deFechaIngresoDesde.Date := NullDate;       // SS_660_MDI_20130409
        deFechaIngresoHasta.Date := NullDate;       // SS_660_MDI_20130409
        deFechaAltaDesde.Date := NullDate;          // SS_660_MDI_20130409
        deFechaAltaHasta.Date := NullDate;          // SS_660_MDI_20130409
        deFechaBajaDesde.Date := NullDate;          // SS_660_MDI_20130409
        deFechaBajaHasta.Date := NullDate;          // SS_660_MDI_20130409
        vcbEstados.ItemIndex := 0;                  // SS_660_CQU_20121010
        spObtenerConveniosEnListaAmarilla.Close;    // SS_660_CQU_20121010
        btnCambiarEstado.Enabled := False;          // SS_660_MDI_20130307
        btnReEnviarCarta.Enabled := False;          // SS_660_MDI_20130307
        btnSeleccionarTodos.Enabled := False;       // SS_660_MDI_20130307
        btnDeselectAll.Enabled := False;            // SS_660_MDI_20130307
        btnInvertirSeleccion.Enabled := False;      // SS_660_MDI_20130307
        vcbConcesionarias.ItemIndex := 0;           // SS_660_CQU_20130711
end;


{------------------------------------------------------------------------
        vcbConveniosClienteDrawItem

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Invoca al procedimiento de cambio de color dependiendo
                    del estado del convenio.

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.vcbConveniosClienteDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
//var                                                                                                   //SS_1120_MVI_20130820
//    CodigoConvenio: AnsiString;                                                                       //SS_1120_MVI_20130820
begin
//    CodigoConvenio := vcbConveniosCliente.Items[Index].Value;                                         //SS_1120_MVI_20130820
    // Si el Convenio est� en la lista de los convenios dados de baja, entonces                         //SS_1120_MVI_20130820
    // colorearlo de manera diferente a los que NO est�n de baja.                                       //SS_1120_MVI_20130820
//    if FListaConveniosBaja.IndexOf(CodigoConvenio) <> -1 then begin                                   //SS_1120_MVI_20130820
//        ItemCBColor(Control, Index, Rect, cl3DLight, clRed)                                           //SS_1120_MVI_20130820
//    end else begin                                                                                    //SS_1120_MVI_20130820
//        ItemCBColor(Control, Index, Rect, $00FAEBDE, clBlack);                                        //SS_1120_MVI_20130820
//    end;                                                                                              //SS_1120_MVI_20130820
                                                                                                        //SS_1120_MVI_20130820
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConveniosCliente.Items[Index].Caption ) > 0 then  begin             //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
     ItemComboBoxColor(Control, Index, Rect, State, clred, true);                    //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
  end                                                                                         //SS_1120_MVI_20130820
  else begin                                                                                  //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
     ItemComboBoxColor(Control, Index, Rect, State);                                          //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
  end;                                                                                        //SS_1120_MVI_20130820


end;

{------------------------------------------------------------------------
        CargarEstadosConveniosListaAmarilla

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Llena la combobox de Estados de Inhabilitaci�n

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.CargarEstadosConveniosListaAmarilla;
resourcestring
    MSG_ERROR_CARGA_ESTADOS = 'Ocurri� el siguiente error al intentar obtener los Estados para lista amarilla';
var
    SP: TAdoStoredProc;
begin
    try
        vcbEstados.Clear;
        vcbEstados.Items.Add('<Todos>', -1);
        try
            SP := TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'ObtenerEstadosConveniosListaAmarilla';
            SP.Open;
            while not sp.Eof do begin
                vcbEstados.Items.Add(   SP.FieldByName('descripcion').AsString,
                                        SP.FieldByName('Estado').AsInteger);
                SP.Next;
            end;
            SP.Close;
            
            //vcbEstados.ItemIndex := 0;                                // SS_660_CQU_20121010
            vcbEstados.ItemIndex := vcbEstados.Items.IndexOfValue(-1);  // SS_660_CQU_20121010
        except on e:exception do begin
                MsgBoxErr(MSG_ERROR_CARGA_ESTADOS, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        FreeAndNil(SP);
    end;
end;

{------------------------------------------------------------------------
        btnFiltrarClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Obtiene el listado de convenios en lista amarilla,
                    acorde a los filtros indicados

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnFiltrarClick(Sender: TObject);
resourcestring
    MSG_ERROR_CARGA_CONVENIOS = 'Ocurri� el siguiente error al intentar obtener los Convenios en lista amarilla';
    MSG_BUSQUEDA_VACIA        = 'No se encontr� ning�n registro con esos par�metros de b�squeda';       // SS_660_CQU_20121010

begin
    Screen.Cursor := crHourGlass;                   // SS_660_CQU_20130711

    if vcbConveniosCliente.ItemIndex < 1 then begin  // SS_660_MDI_20130409
        btnBuscarClick(nil);                         // SS_660_MDI_20130409
    end;                                             // SS_660_MDI_20130409

    if Trim(peRUTCliente.Text) = '' then begin       // SS_660_MDI_20130409
        LimpiaConvenio();                            // SS_660_MDI_20130409
    end;                                             // SS_660_MDI_20130409

    cdsConvenios.EmptyDataSet;

    // Inicio Bloque SS_660_CQU_20121010
    if not ValidateControls([deFechaIngresoDesde, deFechaAltaDesde, deFechaBajaDesde],
        [not ((deFechaIngresoDesde.Date <> Util.NullDate) and (deFechaIngresoHasta.Date <> Util.NullDate) and (deFechaIngresoDesde.Date > deFechaIngresoHasta.Date)),
         not ((deFechaAltaDesde.Date <> Util.NullDate) and (deFechaAltaHasta.Date <> Util.NullDate) and (deFechaAltaDesde.Date > deFechaAltaHasta.Date)),
         not ((deFechaBajaDesde.Date <> Util.NullDate) and (deFechaBajaHasta.Date <> Util.NullDate) and (deFechaBajaDesde.Date > deFechaBajaHasta.Date))
        ]
        , Caption
        , [MSG_VALIDAR_ORDEN_FECHA, MSG_VALIDAR_ORDEN_FECHA, MSG_VALIDAR_ORDEN_FECHA]) then Exit;
    // Fin Bloque SS_660_CQU_20121010
    if not RevisaFiltro then Exit;  // SS_660_CQU_20131104

    //obtener datos
    Screen.Cursor := crHourGlass;                   // SS_660_NDR_20131112
    spObtenerConveniosEnListaAmarilla.Close;
    with spObtenerConveniosEnListaAmarilla do begin
        Parameters.Refresh;
        Parameters.ParamByName('@NumeroDocumento').Value    := IIf(Trim(peRUTCliente.Text) <> '', peRUTCliente.Text, null);              // SS_660_MDI_20130409
        Parameters.ParamByName('@CodigoConvenio').Value     := iif(vcbConveniosCliente.ItemIndex > 0, vcbConveniosCliente.Value, null);
        Parameters.ParamByName('@Estado').Value             := iif(vcbEstados.ItemIndex > 0, vcbEstados.Value, null);
        Parameters.ParamByName('@FechaIngresoDesde').Value  := iif(deFechaIngresoDesde.Date > 0, deFechaIngresoDesde.Date, null);
        Parameters.ParamByName('@FechaIngresoHasta').Value  := iif(deFechaIngresoHasta.Date > 0, deFechaIngresoHasta.Date, null);
        Parameters.ParamByName('@FechaAltaDesde').Value     := iif(deFechaAltaDesde.Date > 0, deFechaAltaDesde.Date, null);
        Parameters.ParamByName('@FechaAltaHasta').Value     := iif(deFechaAltaHasta.Date > 0, deFechaAltaHasta.Date, null);
        Parameters.ParamByName('@FechaBajaDesde').Value     := iif(deFechaBajaDesde.Date > 0, deFechaBajaDesde.Date, null);
        Parameters.ParamByName('@FechaBajaHasta').Value     := iif(deFechaBajaHasta.Date > 0, deFechaBajaHasta.Date, null);
		//Parameters.ParamByName('@CodigoConvenio').Value     := iif(vcbConveniosCliente.ItemIndex > 0, vcbConveniosCliente.Value, null);	// SS_660_CQU_20130711
        Parameters.ParamByName('@CodigoConcesionaria').Value:= iif(vcbConcesionarias.ItemIndex > 0, vcbConcesionarias.Value, null); // SS_660_CQU_20130711
        Parameters.ParamByName('@AutorizaFiscalia').Value   := null;                        // SS_1231_MBE_20141114
        Parameters.ParamByName('@AutorizaGerencia').Value   := null;                        // SS_1231_MBE_20141114
        if cbbAutorizaciones.ItemIndex = 1 then begin                                       // SS_1231_MBE_20141114
            Parameters.ParamByName('@AutorizaFiscalia').Value := 1;                         // SS_1231_MBE_20141114
        end;                                                                                // SS_1231_MBE_20141114
        if cbbAutorizaciones.ItemIndex = 2 then begin                                       // SS_1231_MBE_20141114
            Parameters.ParamByName('@AutorizaGerencia').Value := 1;                         // SS_1231_MBE_20141114
        end;                                                                                // SS_1231_MBE_20141114
        if cbbAutorizaciones.ItemIndex = 3 then begin                                       // SS_1231_MBE_20141114
            Parameters.ParamByName('@AutorizaFiscalia').Value := 1;                         // SS_1231_MBE_20141114
            Parameters.ParamByName('@AutorizaGerencia').Value := 1;                         // SS_1231_MBE_20141114
        end;                                                                                // SS_1231_MBE_20141114
        if cbbAutorizaciones.ItemIndex = 4 then begin                                       // SS_1231_MBE_20141114
            Parameters.ParamByName('@AutorizaFiscalia').Value := 0;                         // SS_1231_MBE_20141114
            Parameters.ParamByName('@AutorizaGerencia').Value := 0;                         // SS_1231_MBE_20141114
        end;                                                                                // SS_1231_MBE_20141114

        try
            Open;
            if IsEmpty then MsgBox(MSG_BUSQUEDA_VACIA, Caption, MB_ICONINFORMATION + MB_OK);    //  SS_660_CQU_20121010
        except on e:exception do begin
                MsgBoxErr(MSG_ERROR_CARGA_CONVENIOS, e.Message, Caption, MB_ICONERROR);
                Screen.Cursor := crDefault;                                                     // SS_660_CQU_20140307
                Exit;                                                                           // SS_660_CQU_20130711
            end;
        end;
    end;

    btnSeleccionarTodos.Enabled := False;       // SS_660_MDI_20130307
    btnDeselectAll.Enabled :=  False;           // SS_660_MDI_20130307
    btnInvertirSeleccion.Enabled :=  False;     // SS_660_MDI_20130307
    btnCambiarEstado.Enabled := False;          // SS_660_MDI_20130409
    btnReEnviarCarta.Enabled := False;          // SS_660_MDI_20130409
    btnAutFiscalia.Enabled := False;            // SS_660_CQU_20130711
    btnAutGerencia.Enabled := False;            // SS_660_CQU_20130711
    btnAnularCarta.Enabled := False;            // SS_660_CQU_20140307

    if spObtenerConveniosEnListaAmarilla.RecordCount > 0 then begin

        btnSeleccionarTodos.Enabled := True;    // SS_660_MDI_20130307
        btnDeselectAll.Enabled :=  True;        // SS_660_MDI_20130307
        btnInvertirSeleccion.Enabled :=  True;  // SS_660_MDI_20130307
        
    end;

    Screen.Cursor := crDefault;                 // SS_660_CQU20130711
end;

{------------------------------------------------------------------------
        btnAutFiscaliaClick

    Author      : MDiazV
    Date        : 19-04-2013
    Description : Autoriza por fiscal�a el o los convenios seleccionados.

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnAutFiscaliaClick(
  Sender: TObject);
resourcestring
   //MSG_ERROR = 'No puede realizar la autorizaci�n. Uno o m�s convenios ya se encuentran autorizados';   // SS_660_CQU_20130604
   MSG_ERROR = 'Los Convenios seleccionados ya fueron autorizados por fiscal�a';                          // SS_660_CQU_20130604
   //MSG_AVISO = 'Est� por Autorizar por Fiscal�a los registros seleccionados. Desea continuar?';         // SS_660_CQU_20130604
   MSG_AVISO = 'Ser�n Autorizados por Fiscal�a %d Convenios.' + CRLF + '�Desea continuar?';               // SS_660_CQU_20130604
   MSG_ERROR_SQL = 'Ocurri� un error al intentar Autorizar por Fiscal�a los convenios seleccionados';
   MSG_UPDATE_OK = 'La Autorizaci�n se realiz� correctamente';
var
    control, error : Boolean;
    Retorno : integer;
    SinAutorizar : Boolean;                                                                 // SS_660_CQU_20130604
begin
    //control := False;                                                                     // SS_660_CQU_20130604
    error   := False;

    control := cdsConvenios.Locate('AutorizaFiscalia', 1, []);                              // SS_660_CQU_20130604
    SinAutorizar := cdsConvenios.Locate('AutorizaFiscalia', 0, []);                         // SS_660_CQU_20130604
    //with cdsConvenios do begin                                                            // SS_660_CQU_20130604
    //    First;                                                                            // SS_660_CQU_20130604
    //    while not Eof do begin                                                            // SS_660_CQU_20130604
    //                                                                                      // SS_660_CQU_20130604
    //        if FieldByName('AutorizaFiscalia').AsBoolean then begin                       // SS_660_CQU_20130604
    //            control := True;                                                          // SS_660_CQU_20130604
    //        end;                                                                          // SS_660_CQU_20130604
    //        Next;                                                                         // SS_660_CQU_20130604
    //    end;                                                                              // SS_660_CQU_20130604

    //end;                                                                                  // SS_660_CQU_20130604

    //if control then begin                                                                 // SS_660_CQU_20130604
    if control and not SinAutorizar then begin                                              // SS_660_CQU_20130604

        MsgBox(MSG_ERROR, Caption, MB_ICONERROR);
        //cdsConvenios.EmptyDataSet;                                                        // SS_660_CQU_20130604
        //spObtenerConveniosEnListaAmarilla.Close;                                          // SS_660_CQU_20130604
        //spObtenerConveniosEnListaAmarilla.Open;                                           // SS_660_CQU_20130604
        //btnAutFiscalia.Enabled := False;                                                  // SS_660_CQU_20130604
        //btnAutGerencia.Enabled := False;                                                  // SS_660_CQU_20130604
    end
    else begin

        if SinAutorizar and control then begin                                              // SS_660_CQU_20130604
            cdsConvenios.Filter     := 'AutorizaFiscalia = 0';                              // SS_660_CQU_20130604
            cdsConvenios.Filtered   := True;                                                // SS_660_CQU_20130604
        end;                                                                                // SS_660_CQU_20130604

        //if (MsgBox(MSG_AVISO, 'Atenci�n', MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin// SS_660_CQU_20130604
        if (MsgBox(Format(MSG_AVISO, [cdsConvenios.RecordCount]),                           // SS_660_CQU_20130604
                            'Atenci�n', MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin    // SS_660_CQU_20130604

            //DMConnections.BaseCAC.Execute('BEGIN TRAN spAutorizarConvenioPorFiscalia');   // SS_660_CQU_20130604
            DMConnections.BaseCAC.BeginTrans;                                               // SS_660_CQU_20130604
            cdsConvenios.First;
            while not cdsConvenios.Eof do begin

                spAutorizarConvenioPorFiscalia.Close;

                spAutorizarConvenioPorFiscalia.Parameters.Refresh;
                spAutorizarConvenioPorFiscalia.Parameters.ParamByName('@IDConvenioInhabilitado').Value :=  cdsConvenios.FieldByName('IDConvenioInhabilitado').AsInteger;
                spAutorizarConvenioPorFiscalia.Parameters.ParamByName('@CodigoUsuario').Value          :=  UsuarioSistema;
                spAutorizarConvenioPorFiscalia.Parameters.ParamByName('@MensajeError').Value           :=  null;

                try
                    spAutorizarConvenioPorFiscalia.ExecProc;
                    Retorno := spAutorizarConvenioPorFiscalia.Parameters.ParamByName('@RETURN_VALUE').Value;
                    if Retorno < 0 then begin
                        error := True;
                        end;
                except on e:exception do begin
                    error := True;
                    end;
                end;

                cdsConvenios.Next;

            end;
            if error then begin
                //DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAutorizarConvenioPorFiscalia');    // SS_660_CQU_20130604
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;    // SS_660_CQU_20130604
                MsgBox(MSG_ERROR_SQL, Caption, MB_ICONERROR);
            end
            else begin
                //DMConnections.BaseCAC.Execute('COMMIT TRAN spAutorizarConvenioPorFiscalia');      // SS_660_CQU_20130604
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;      // SS_660_CQU_20130604
                MsgBox(MSG_UPDATE_OK, Caption, MB_ICONINFORMATION);
            end;
        //end;
        end else error := True;                                                                     // SS_660_CQU_20130711
        
        if cdsConvenios.Filtered then begin                                                         // SS_660_CQU_20130604
            cdsConvenios.Filter     := EmptyStr;                                                    // SS_660_CQU_20130604
            cdsConvenios.Filtered   := False;                                                       // SS_660_CQU_20130604
        end;
        //if spObtenerConveniosEnListaAmarilla.Active then begin                                    // SS_660_CQU_20130604
        if not error and spObtenerConveniosEnListaAmarilla.Active then begin                        // SS_660_CQU_20130604
            Screen.Cursor := crHourGlass;                                                           //SS_660_NDR_20131112
            cdsConvenios.EmptyDataSet;
            btnAutFiscalia.Enabled := False;
            btnAutGerencia.Enabled := False;
            spObtenerConveniosEnListaAmarilla.Close;
            spObtenerConveniosEnListaAmarilla.Open;
            btnCambiarEstado.Enabled := False;                                                      // SS_660_CQU_20130711
            Screen.Cursor := crDefault;                                                             //SS_660_NDR_20131112
        end;

    end;
end;

procedure TListaDeConveniosEnListaAmarillaForm.btnBuscarClick(Sender: TObject);
resourcestring                                                                          // SS_660_CQU_20130711
    MSG_ERROR_RUT   =   'El Rut ingresado no es v�lido';                                // SS_660_CQU_20130711
    MSG_SIN_CONVENIO=   'No se encontraron Convenios para el Rut ingresado';            // SS_660_CQU_20130711
var                                                                                     // SS_660_CQU_20130711
    Rut : string;                                                                       // SS_660_CQU_20130711
begin
    FListaConveniosBaja.Clear;
    vcbConveniosCliente.Clear;
    LimpiaConvenio();                                                                   // SS_660_CQU_20130711

    try
		Screen.Cursor := crHourglass;

        Rut := Trim(PadL(peRUTCLiente.Text, 9, '0'));                                   // SS_660_CQU_20130711
        if not ValidarRUT(DMConnections.BaseCAC, Rut) then begin                        // SS_660_CQU_20130711
            MsgBoxBalloon(MSG_ERROR_RUT, Caption, MB_ICONINFORMATION, peRUTCliente);    // SS_660_CQU_20130711
            Exit;                                                                       // SS_660_CQU_20130711
        end;                                                                            // SS_660_CQU_20130711

        // Obtenemos el cliente seleccionado
        with spObtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value      := NULL;
            Parameters.ParamByName('@CodigoDocumento').Value    := TIPO_DOCUMENTO_RUT;
            //Parameters.ParamByName('@NumeroDocumento').Value    := Trim(PadL(peRUTCLiente.Text, 9, '0')); // SS_660_CQU_20130711
            Parameters.ParamByName('@NumeroDocumento').Value    := Rut;                                     // SS_660_CQU_20130711
            Open;
            if IsEmpty then begin
                Close;
                if not (peRUTCLiente.Text = EmptyStr) then                                                  // SS_660_CQU_20130711
                    MsgBox(MSG_SIN_CONVENIO, Caption, MB_ICONINFORMATION + MB_OK);                          // SS_660_CQU_20130711
                Exit;
            end;
        end;

        //obtenemos los convenios del cliente
//        spObtenerConveniosCliente.Parameters.ParamByName('@CodigoCliente').Value := spObtenerCliente.FieldByName('CodigoCliente').AsInteger;      //SS_1120_MVI_20130820
//        spObtenerConveniosCliente.Open;                                                                                                           //SS_1120_MVI_20130820
//        //LimpiaConvenio();        // SS_660_CQU_20130711 // SS_660_MDI_20130409                                                                  //SS_1120_MVI_20130820
//        while not spObtenerConveniosCliente.Eof do begin                                                                                          //SS_1120_MVI_20130820
//            vcbConveniosCliente.Items.Add(  spObtenerConveniosCliente.FieldByName('NumeroConvenioFormateado').AsString,                           //SS_1120_MVI_20130820
//                                            spObtenerConveniosCliente.FieldByName('CodigoConvenio').AsInteger);                                   //SS_1120_MVI_20130820
//                                                                                                                                                  //SS_1120_MVI_20130820
//            // Si el convenio esta dado de baja, almacenarlo en la lista de                                                                       //SS_1120_MVI_20130820
//            // convenios dados de baja.                                                                                                           //SS_1120_MVI_20130820
//            if spObtenerConveniosCliente.FieldByName('CodigoEstadoConvenio').Value = ESTADO_CONVENIO_BAJA then begin                              //SS_1120_MVI_20130820
//                    FListaConveniosBaja.Add(Trim(spObtenerConveniosCliente.FieldByName('CodigoConvenio').AsString));                              //SS_1120_MVI_20130820
//            end;                                                                                                                                  //SS_1120_MVI_20130820
//                                                                                                                                                  //SS_1120_MVI_20130820
//            spObtenerConveniosCliente.Next;                                                                                                       //SS_1120_MVI_20130820
//        end;                                                                                                                                      //SS_1120_MVI_20130820
//                                                                                                                                                  //SS_1120_MVI_20130820
//        spObtenerConveniosCliente.Close;                                                                                                          //SS_1120_MVI_20130820
            EstaClienteConMensajeEspecial(DMCOnnections.BaseCAC, Rut);                                                                              //SS_1408_MCA_20151027
            CargarConveniosRUT(DMCOnnections.BaseCAC,vcbConveniosCliente, 'RUT', Rut, False, -1, False, True, True);                                //SS_1120_MVI_20130820                                 //SS_1120_MVI_20130820
                                                                                                                                                    //SS_1120_MVI_20130820
        if vcbConveniosCliente.Items.Count > 0 then vcbConveniosCliente.ItemIndex := 0;                                                             //SS_1120_MVI_20130820


    finally
		Screen.Cursor := crDefault;
    end;
end;

{------------------------------------------------------------------------
        btnLimpiarClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : invoca a la funci�n de limpieza de loc campos

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnLimpiarClick(Sender: TObject);
begin
    Limpiar();
end;

{------------------------------------------------------------------------
        btnNuevoConvenioClick

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Invoca al nuevo formulario que ingresa un convenio en
                    lista amarilla

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnNuevoConvenioClick(
  Sender: TObject);
begin
    Application.CreateForm(TNuevoConvenioListaAmarillaForm, NuevoConvenioListaAmarillaForm);
    if NuevoConvenioListaAmarillaForm.Inicializar() then begin
        if NuevoConvenioListaAmarillaForm.ShowModal = mrOk then begin
            if spObtenerConveniosEnListaAmarilla.Active then begin
                Screen.Cursor := crHourGlass;                                           //SS_660_NDR_20131112
                spObtenerConveniosEnListaAmarilla.Close;
                spObtenerConveniosEnListaAmarilla.Open;
                Screen.Cursor := crDefault;                                             //SS_660_NDR_20131112
            //end;                                                                      // SS_660_CQU_20130711
            end else if peRUTCliente.Text = EmptyStr then begin                         // SS_660_CQU_20130711
                peRUTCliente.Text := NuevoConvenioListaAmarillaForm.peRUTCliente.Text;  // SS_660_CQU_20130711
                btnFiltrar.Click;                                                       // SS_660_CQU_20130711
            end;                                                                        // SS_660_CQU_20130711
        end;
    end;
    NuevoConvenioListaAmarillaForm.Release;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaConveniosIn
  Author:    MDI
  Date Created: 04/03/2013
  Description: Devuelve si un valor esta en la lista Convenios
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TListaDeConveniosEnListaAmarillaForm.ListaConveniosIn(valor:string):boolean;
var estado : Boolean;
begin
    //cdsConvenios.Filter := 'IDConvenioInhabilitado = ' + valor;       // SS_660_CQU_20130628
    //cdsConvenios.Filtered := True;                                    // SS_660_CQU_20130628
    //cdsConvenios.First;                                               // SS_660_CQU_20130628
    //estado := cdsConvenios.Eof;                                       // SS_660_CQU_20130628
    estado := cdsConvenios.Locate('IDConvenioInhabilitado', valor, []); // SS_660_CQU_20130628
    //cdsConvenios.Filtered := False;                                   // SS_660_CQU_20130628
    //Result := not (estado);                                           // SS_660_CQU_20130628
    Result := estado;                                                   // SS_660_CQU_20130628
end;


{-----------------------------------------------------------------------------
  Function Name: ListaConveniosInclude
  Author:    MDI
  Date Created: 04/03/2013
  Description: agrego un valor a la lista de convenios
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TListaDeConveniosEnListaAmarillaForm.ListaConveniosInclude(valor:string):boolean;
begin
	result:=false;
	try
      cdsConvenios.Append;
      cdsConvenios.FieldByName('IDConvenioInhabilitado').Value := spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsInteger;
      cdsConvenios.FieldByName('CodigoConvenio').Value         := spObtenerConveniosEnListaAmarilla.FieldByName('CodigoConvenio').AsInteger;
      cdsConvenios.FieldByName('Estado').Value                 := spObtenerConveniosEnListaAmarilla.FieldByName('Estado').AsInteger;
      cdsConvenios.FieldByName('NumeroConvenio').Value         := spObtenerConveniosEnListaAmarilla.FieldByName('NumeroConvenio').AsString;
      cdsConvenios.FieldByName('Descripcion').Value            := spObtenerConveniosEnListaAmarilla.FieldByName('Descripcion').AsString;
      cdsConvenios.FieldByName('FechaEnvioCarta').Value        := spObtenerConveniosEnListaAmarilla.FieldByName('FechaEnvioCarta').AsDateTime;
      cdsConvenios.FieldByName('EstaEnLegales').Value          := spObtenerConveniosEnListaAmarilla.FieldByName('EstaEnLegales').AsBoolean;
      cdsConvenios.FieldByName('NumeroDocumento').Value        := spObtenerConveniosEnListaAmarilla.FieldByName('NumeroDocumento').AsString;
      //cdsConvenios.FieldByName('CantReEnvio').Value            := spObtenerConveniosEnListaAmarilla.FieldByName('CantReEnvio').AsInteger;           // SS_660_CQU_20130604
      cdsConvenios.FieldByName('AutorizaFiscalia').Value       := spObtenerConveniosEnListaAmarilla.FieldByName('AutorizaFiscalia').AsString;         // SS_660_MDI_20130409
      cdsConvenios.FieldByName('FechaAutorizaFiscalia').Value  := spObtenerConveniosEnListaAmarilla.FieldByName('FechaAutorizaFiscalia').AsDateTime;  // SS_660_MDI_20130409
      cdsConvenios.FieldByName('UsuarioAutorizaFiscalia').Value := spObtenerConveniosEnListaAmarilla.FieldByName('UsuarioAutorizaFiscalia').AsString; // SS_660_MDI_20130409
      cdsConvenios.FieldByName('AutorizaGerencia').Value       := spObtenerConveniosEnListaAmarilla.FieldByName('AutorizaGerencia').AsString;         // SS_660_MDI_20130409
      cdsConvenios.FieldByName('FechaAutorizaGerencia').Value  := spObtenerConveniosEnListaAmarilla.FieldByName('FechaAutorizaGerencia').AsDateTime;  // SS_660_MDI_20130409
      cdsConvenios.FieldByName('UsuarioAutorizaGerencia').Value := spObtenerConveniosEnListaAmarilla.FieldByName('UsuarioAutorizaGerencia').AsString; // SS_660_MDI_20130409
      cdsConvenios.FieldByName('PuedeAutorizarFiscalia').Value  := spObtenerConveniosEnListaAmarilla.FieldByName('PuedeAutorizarFiscalia').AsBoolean;   // SS_660_CQU_20130604
      cdsConvenios.FieldByName('PuedeAutorizarGerencia').Value  := spObtenerConveniosEnListaAmarilla.FieldByName('PuedeAutorizarGerencia').AsBoolean;   // SS_660_CQU_20130604
      cdsConvenios.FieldByName('PuedeReEnviarCarta').Value      := spObtenerConveniosEnListaAmarilla.FieldByName('PuedeReEnviarCarta').AsBoolean;       // SS_660_CQU_20130604
      cdsConvenios.FieldByName('PuedeAnularCarta').Value        := spObtenerConveniosEnListaAmarilla.FieldByName('PuedeAnularCarta').AsBoolean;         // SS_660_CQU_20130604
      cdsConvenios.FieldByName('Concesionaria').Value        := spObtenerConveniosEnListaAmarilla.FieldByName('Concesionaria').AsString;     /////////////////********/////////////
      cdsConvenios.FieldByName('CodigoConcesionaria').Value     := spObtenerConveniosEnListaAmarilla.FieldByName('CodigoConcesionaria').AsInteger;      // SS_660_CQU_20130711
      cdsConvenios.Post;
      //if cdsConvenios.RecordCount = 1 then  begin
      FPrimerEstado := -1;
      cdsConvenios.First;
      while (not cdsConvenios.Eof) and (FPrimerEstado = -1) do begin
          FPrimerEstado := cdsConvenios.FieldByName('Estado').AsInteger;
          cdsConvenios.Next;
      end;
      ValidaEstados();
      result:=true;
	except
  end;
end;


{-----------------------------------------------------------------------------
  Function Name: ListaConveniosExclude
  Author:    MDI
  Date Created: 04/03/2013
  Description: quito un valor a la lista de convenios
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TListaDeConveniosEnListaAmarillaForm.ListaConveniosExclude(valor:string):boolean;
begin
	result:=false;
    try
        with cdsConvenios do begin
            Open;
            Filter := 'IDConvenioInhabilitado = ' + valor;
            Filtered := True;
            while not Eof do
              Delete;
            Filtered := False;
            cdsConvenios.First;
            //FPrimerEstado := cdsConvenios.FieldByName('Estado').AsInteger;
            FPrimerEstado := -1;
            cdsConvenios.First;
            while (not cdsConvenios.Eof) and (FPrimerEstado = -1) do begin
                FPrimerEstado := cdsConvenios.FieldByName('Estado').AsInteger;
                cdsConvenios.Next;
            end;
        end;
        ValidaEstados();
    except
    end;
end;

{------------------------------------------------------------------------
        FormClose

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Limpia la lista al momento de salir

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.FormClose(Sender: TObject;
    var Action: TCloseAction);
begin
    FListaConveniosBaja.Free;
end;

{------------------------------------------------------------------------
        dsObtenerConveniosEnListaAmarillaDataChange

    Author      : mbecerra
    Date        : 01-Octubre-2012
    Description : Determina si habilitar o no el bot�n de Cambio de estado

-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.dsObtenerConveniosEnListaAmarillaDataChange(
  Sender: TObject; Field: TField);

begin
    if (not spObtenerConveniosEnListaAmarilla.Eof) and (Field = nil) then begin
        // INICIO SS_660_MDI_20130409
        if cdsConvenios.RecordCount = 0 then begin
            btnCambiarEstado.Enabled := False;
            btnReEnviarCarta.Enabled := False;
        end

        else if cdsConvenios.RecordCount = 1 then begin

           btnCambiarEstado.Enabled := ((FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) and
                                        (FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) and
                                       (cdsConvenios.RecordCount > 0) and
                                        ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA'));
           btnReEnviarCarta.Enabled := ((cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA) and   // SS_660_CQU_20121010
                                        //(FCantReEnvios > cdsConvenios.FieldByName('CantReEnvio').AsInteger) and                                 // SS_660_CQU_20130604  // SS_660_CQU_20121010
                                          ExisteAcceso('REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA'));
        end
        else begin
            btnCambiarEstado.Enabled := ((FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) and
                                          (FPrimerEstado <>  CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) and
                                          ValidaEstados() and   // SS_660_MDI_20130412
                                          ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA'));
            btnReEnviarCarta.Enabled := False;
        end;
        // FIN SS_660_MDI_20130409
        btnSeleccionarTodos.Enabled := True;    // SS_660_MDI_20130307
        btnDeselectAll.Enabled :=  True;        // SS_660_MDI_20130307
        btnInvertirSeleccion.Enabled :=  True;  // SS_660_MDI_20130307
    end;

end;


procedure TListaDeConveniosEnListaAmarillaForm.btnCambiarEstadoClick(
  Sender: TObject);
resourcestring
   MSG_ERROR = 'No puede realizar cambio a convenios con distinto estado';
   MSG_CAMBIAR_VARIAS_CONCESIONARIAS = '�Est� seguro de cambiar estado a convenios de distintas concesionarias?';       //SS_660_MVI_20130711
   CAPTION_GENERAR_INFRACCIONES = 'Cambio de estado varias concesionarias';                                             //SS_660_MVI_20130711

var concecionarias: string;                                                                                             //SS_660_MVI_20130711
    flagConcesionaria: integer;                                                                                         //SS_660_MVI_20130711

begin

    FGlosaActual := cdsConvenios.FieldByName('Descripcion').AsString;                         // SS_660_MDI_20130409
    FEstadoActual := cdsConvenios.FieldByName('Estado').AsInteger;                            // SS_660_MDI_20130409

    if cdsConvenios.RecordCount = 1 then  begin

        Application.CreateForm(TCambiarEstadoListaAmarillaForm, CambiarEstadoListaAmarillaForm);
        if CambiarEstadoListaAmarillaForm.Inicializar(cdsConvenios, FEstadoActual, FGlosaActual) then   // SS_660_MDI_20130409
        begin
            if CambiarEstadoListaAmarillaForm.ShowModal = mrOk then begin                               // SS_660_MDI_20130409
                Screen.Cursor := crHourGlass;                                                           //SS_660_NDR_20131112
                cdsConvenios.EmptyDataSet;                                                              // SS_660_MDI_20130409
                btnCambiarEstado.Enabled := False;                                                      // SS_660_MDI_20130409
                btnReEnviarCarta.Enabled := False;                                                      // SS_660_MDI_20130409
                btnAutFiscalia.Enabled   := False;                                                      // SS_660_MDI_20130409
                btnAutGerencia.Enabled   := False;                                                      // SS_660_MDI_20130409
                btnAnularCarta.Enabled   := False;                                                      // SS_660_CQU_20130604
                spObtenerConveniosEnListaAmarilla.Close;                                                // SS_660_MDI_20130409
                spObtenerConveniosEnListaAmarilla.Open;                                                 // SS_660_MDI_20130409
                Screen.Cursor:=crDefault;                                                               //SS_660_NDR_20131112 
            end;
        end;

        CambiarEstadoListaAmarillaForm.Release;
    end
    // SS_660_MDI_20130307
    else begin
                                                                                                           //SS_660_MVI_20130711
        if FPrimerEstado = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION then begin                  //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
             cdsConvenios.First;                                                                           //SS_660_MVI_20130711
             concecionarias := '';                                                                         //SS_660_MVI_20130711
             flagConcesionaria := 0;                                                                       //SS_660_MVI_20130711
            while not cdsConvenios.Eof do begin                                                            //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
                if concecionarias = '' then begin                                                          //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
                   concecionarias := cdsConvenios.FieldByName('Concesionaria').AsString;                   //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
                end                                                                                        //SS_660_MVI_20130711
                else begin                                                                                 //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
                   if concecionarias <> cdsConvenios.FieldByName('Concesionaria').AsString then begin      //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
                           flagConcesionaria := 1;                                                         //SS_660_MVI_20130711
                           // buscar forma para pasar al ultimo cdsConvenios y salir del while             //SS_660_MVI_20130711
                   end                                                                                     //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
                end;                                                                                       //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
                cdsConvenios.Next;                                                                         //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
            end;                                                                                           //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
          if flagConcesionaria = 1 then begin                                                              //SS_660_MVI_20130711
                                                                                                           //SS_660_MVI_20130711
              if MsgBox(MSG_CAMBIAR_VARIAS_CONCESIONARIAS, CAPTION_GENERAR_INFRACCIONES, MB_YESNO + MB_ICONQUESTION) = IDNO then begin   //SS_660_MVI_20130711
                                                                                                                                         //SS_660_MVI_20130711
                 Exit;                                                                                                                   //SS_660_MVI_20130711
                                                                                                                                         //SS_660_MVI_20130711
              end                                                                                                                        //SS_660_MVI_20130711
                                                                                                                                         //SS_660_MVI_20130711
          end                                                                                                                            //SS_660_MVI_20130711
                                                                                                                                         //SS_660_MVI_20130711
        end;                                                                                                                             //SS_660_MVI_20130711
                                                                                                                                         //SS_660_MVI_20130711



        if not ValidateControls([btnCambiarEstado],     // SS_660_CQU_20130604
                                [ValidaEstados()],      // SS_660_CQU_20130604
                                Caption,                // SS_660_CQU_20130604
                                [MSG_ERROR]) then Exit; // SS_660_CQU_20130604
        //if ValidaEstados() then  begin                // SS_660_CQU_20130604

            Application.CreateForm(TCambiarEstadoListaAmarillaForm, CambiarEstadoListaAmarillaForm);
            if CambiarEstadoListaAmarillaForm.Inicializar(cdsConvenios, FEstadoActual, FGlosaActual) then
            begin
                if CambiarEstadoListaAmarillaForm.ShowModal = mrOk then begin
                    Screen.Cursor := crHourGlass;                               //SS_660_NDR_20131112
                    cdsConvenios.EmptyDataSet;
                    btnCambiarEstado.Enabled := False;  // SS_660_MDI_20130307
                    btnReEnviarCarta.Enabled := False;  // SS_660_MDI_20130307
                    btnAutFiscalia.Enabled   := False;  // SS_660_MDI_20130409
                    btnAutGerencia.Enabled   := False;  // SS_660_MDI_20130409
                    btnAnularCarta.Enabled   := False;  // SS_660_CQU_20130604
                    spObtenerConveniosEnListaAmarilla.Close;
                    spObtenerConveniosEnListaAmarilla.Open;
                    Screen.Cursor := crDefault;                                 //SS_660_NDR_20131112
                end;
            end;
        //end                                           // SS_660_CQU_20130604
        //else begin                                    // SS_660_CQU_20130604
        //    MsgBox(MSG_ERROR, Caption, MB_ICONERROR); // SS_660_CQU_20130604
        //end;                                          // SS_660_CQU_20130604
    end;
    // FIN SS_660_MDI_20130307
end;

{---------------------------------------------------------------------------------
Function Name   : btnDeselectAllClick
Author          : MDI
Date Created    : 04/03/2013
Description     : Cancela la selecci�n hecha  en el listado de convenios
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_MDI_20130304
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnDeselectAllClick(
  Sender: TObject);
var  FIDConvenioInhabilitado : string;
begin
    Screen.Cursor := crHourGlass ;
    if spObtenerConveniosEnListaAmarilla.Active then begin
        spObtenerConveniosEnListaAmarilla.DisableControls;
        spObtenerConveniosEnListaAmarilla.First;
        while not spObtenerConveniosEnListaAmarilla.eof do begin
            FIDConvenioInhabilitado := spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsString;
            if ListaConveniosIn(FIDConvenioInhabilitado) then begin
                ListaConveniosExclude(FIDConvenioInhabilitado);
            end;
            spObtenerConveniosEnListaAmarilla.Next;
        end;
        spObtenerConveniosEnListaAmarilla.EnableControls;
        spObtenerConveniosEnListaAmarilla.First;
        dblListaAmarilla.Invalidate;
        dblListaAmarilla.Repaint;
    end;
    btnCambiarEstado.Enabled := False;  // SS_660_MDI_20130307
    btnReEnviarCarta.Enabled := False;  // SS_660_MDI_20130307
    btnAutFiscalia.Enabled   := False;  // SS_660_MDI_20130409
    btnAutGerencia.Enabled   := False;  // SS_660_MDI_20130409
    btnAnularCarta.Enabled   := False;  // SS_660_CQU_20130604
    Screen.Cursor := crDefault ;

end;

{---------------------------------------------------------------------------------
Function Name   : dblListaAmarillaColumnsHeaderClick
Author          : CQuezadaI
Date Created    : 10/10/2012
Description     : Ordena las columnas del dblListEx
Parameters      : Sender: TObject;
Return Value    : None
Firma           : SS_660_CQU_20121010
----------------------------------------------------------------------------------}




procedure TListaDeConveniosEnListaAmarillaForm.dblListaAmarillaColumnsHeaderClick(Sender: TObject);
begin
	  if spObtenerConveniosEnListaAmarilla.IsEmpty then Exit;

	  if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   	  else TDBListExColumn(sender).Sorting := csAscending;

	  spObtenerConveniosEnListaAmarilla.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{---------------------------------------------------------------------------------
Function Name   : dblListaAmarillaDrawText
Author          : CQuezadaI
Date Created    : 25/02/2012
Description     : Pinta un campo en la Grilla
Parameters      : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
                    State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
                    var ItemWidth: Integer; var DefaultDraw: Boolean
Return Value    : None
Firma           : SS_660_CQU_20121010
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.dblListaAmarillaDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
  var i : Integer;
  var bmp : TBitmap;                                // SS_660_MDI_20130409
begin
    { // SS_660_CQU_20130604 -- INICIA ELIMINA BLOQUE
    // INICIO  SS_660_MDI_20130409
    if cdsConvenios.RecordCount = 0 then begin
        btnCambiarEstado.Enabled := False;
        btnReEnviarCarta.Enabled := False;
    end

    else if cdsConvenios.RecordCount = 1 then begin

       btnCambiarEstado.Enabled := ((FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) and
                                    (FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) and
                                   (cdsConvenios.RecordCount > 0) and
                                    ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA'));                  // SS_660_MDI_20130409
       btnReEnviarCarta.Enabled := ((cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA) and                              // SS_660_CQU_20121010
                                    (FCantReEnvios > cdsConvenios.FieldByName('CantReEnvio').AsInteger) and   // SS_660_MDI_20130409
                                      ExisteAcceso('REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA'));                // SS_660_MDI_20130409
    end
    else begin
        btnCambiarEstado.Enabled := ((FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) and
                                      (FPrimerEstado <>  CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) and
                                      ValidaEstados() and   // SS_660_MDI_20130412
                                      ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA'));                               // SS_660_MDI_20130409
        btnReEnviarCarta.Enabled := False;
    end;
    // FIN  SS_660_MDI_20130409
    } // SS_660_CQU_20130604 -- FIN ELIMINA BLOQUE
    if Column = dblListaAmarilla.Columns[0] then begin
        i:=iif(ListaConveniosIn(spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').Asstring),1,0);
  //      //verifica si esta seleccionado o no
        ilCheck.Draw(dblListaAmarilla.Canvas, Rect.Left + 3, Rect.Top + 1, i);
        DefaultDraw := False;
        ItemWidth   := ilCheck.Width + 2;
    end;

    // INICIO SS_660_MDI_20130409
    with Sender do  begin
        if (Column.FieldName = 'AutorizaFiscalia') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
		      	DefaultDraw := False;
            try
                if (spObtenerConveniosEnListaAmarilla.FieldByName('AutorizaFiscalia').AsBoolean) then begin
                    ilCheck.GetBitmap(1, Bmp);
                end else begin
                    ilCheck.GetBitmap(0, Bmp);
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;
    end;

    with Sender do  begin
        if (Column.FieldName = 'AutorizaGerencia') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
		      	DefaultDraw := False;
            try
                if (spObtenerConveniosEnListaAmarilla.FieldByName('AutorizaGerencia').AsBoolean) then begin
                    ilCheck.GetBitmap(1, Bmp);
                end else begin
                    ilCheck.GetBitmap(0, Bmp);
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;
    end;
    // FIN SS_660_MDI_20130409
    // Pinto la columna
    if Column.FieldName = 'HistoricoCarta' then begin
      Column.Font.Color := clBlue;
      Column.Font.Style := [fsUnderline];
    end;
    //end;

    with Sender do begin                                                        //SS_660F_MCA_20140805
        if Column.Header.Caption='Observaci�n' then begin                       //SS_660F_MCA_20140805
            DefaultDraw := False;                                               //SS_660F_MCA_20140805
            Text := '';                                                         //SS_660F_MCA_20140805
                                                                                //SS_660F_MCA_20140805
            Canvas.FillRect(Rect);                                              //SS_660F_MCA_20140805
            try                                                                 //SS_660F_MCA_20140805
	              bmp := TBitMap.Create;                                          //SS_660F_MCA_20140805
                ilCheck.GetBitmap(2, Bmp);                                      //SS_660F_MCA_20140805
               	Canvas.Draw(((Rect.Right - Rect.Left) div 2) - bmp.Width + Rect.Left, Rect.Top + 1, bmp); //SS_660F_MCA_20140805
            finally                                                             //SS_660F_MCA_20140805
                bmp.Free;                                                       //SS_660F_MCA_20140805
            end;                                                                //SS_660F_MCA_20140805
  	    end;                                                                    //SS_660F_MCA_20140805
    end;                                                                        //SS_660F_MCA_20140805




end;

{---------------------------------------------------------------------------------
Function Name   : dblListaAmarillaLinkClick
Author          : CQuezadaI
Date Created    : 25/02/2013
Description     : Muestra el Hist�rico de Cartas
Parameters      : Sender: TCustomDBListEx; Column: TDBListExColumn
Return Value    : None
Firma           : SS_660_CQU_20121010
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.dblListaAmarillaLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
var
    ConsultarHistoricoForm  :   TListaDeHistoricoCartasListaAmarillaForm;
    FIDConvenioInhabilitado         :   string;
    ObservacionesConveniosListaAmarilla : TObservacionesConvenioListaAmarillaForm;		//SS_660F_MCA_20140805
begin

    Screen.Cursor := crHourGlass ;
    // SS_660_MDI_20130307
	  //selecciono/desselecciono transito
    if Column = dblListaAmarilla.Columns[0] then begin
        FIDConvenioInhabilitado := spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsString;
        if ListaConveniosIn(FIDConvenioInhabilitado) then begin
            ListaConveniosExclude(FIDConvenioInhabilitado);
        end else begin
            ListaConveniosInclude(FIDConvenioInhabilitado);
        end;
        //dibuja el cambio
        // FIN SS_660_MDI_20130307
        Sender.Invalidate;
        Sender.Repaint;
        cdsConvenios.Filtered := False;
        // SS_660_MDI_20130307
        if cdsConvenios.RecordCount = 0 then begin
            btnCambiarEstado.Enabled := False;  // SS_660_MDI_20130409
            btnReEnviarCarta.Enabled := False;  // SS_660_MDI_20130409
            btnAutFiscalia.Enabled   := False;  // SS_660_MDI_20130409
            btnAutGerencia.Enabled   := False;  // SS_660_MDI_20130409
            btnAnularCarta.Enabled   := False;  // SS_660_CQU_20130604
        end

        else begin
            if cdsConvenios.RecordCount = 1 then begin
               //btnCambiarEstado.Enabled := (spObtenerConveniosEnListaAmarilla.FieldByName('Estado').AsInteger < CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO);
                btnCambiarEstado.Enabled := ((FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) and
                                            (FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) and
                                           (cdsConvenios.RecordCount > 0) and
                                            ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA'));                  // SS_660_MDI_20130409
                //btnReEnviarCarta.Enabled := ((cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA) and  // SS_660_CQU_20130604 // SS_660_CQU_20121010
                //                            (FCantReEnvios > cdsConvenios.FieldByName('CantReEnvio').AsInteger) and                                   // SS_660_CQU_20130604 // SS_660_CQU_20121010
                //                              ExisteAcceso('REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA'));                                                // SS_660_CQU_20130604 // SS_660_MDI_20130409
                btnReEnviarCarta.Enabled := (ExisteAcceso('REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA') and                                                 // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeReEnviarCarta').AsBoolean);                                                  // SS_660_CQU_20130604

                //btnAutFiscalia.Enabled := (cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION) and    // SS_660_CQU_20130604
                //                          ExisteAcceso('AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA');                                                      // SS_660_CQU_20130604 // SS_660_MDI_20130409
                btnAutFiscalia.Enabled := (ExisteAcceso('AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA') and                                                    // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeAutorizarFiscalia').AsBoolean);                                              // SS_660_CQU_20130604

                //btnAutGerencia.Enabled := (cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION) and    // SS_660_CQU_20130604
                //                          ExisteAcceso('AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA');                                                      // SS_660_CQU_20130604 // SS_660_MDI_20130409
                btnAutGerencia.Enabled := (ExisteAcceso('AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA') and                                                    // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeAutorizarGerencia').AsBoolean);                                              // SS_660_CQU_20130604

                btnAnularCarta.Enabled := (ExisteAcceso('RECHAZAR_CARTA_CONVENIO_LISTA_AMARILLA') and                                                   // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeAnularCarta').AsBoolean);                                                    // SS_660_CQU_20130604

            end
            else begin
                btnCambiarEstado.Enabled := ((FPrimerEstado <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) and
                                              (FPrimerEstado <>  CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) and
                                              ValidaEstados() and                                                     // SS_660_MDI_20130409
                                              ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA'));                // SS_660_MDI_20130409
                btnReEnviarCarta.Enabled := False;                                                                    // SS_660_MDI_20130409
                btnAutFiscalia.Enabled := (FPrimerEstado = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION) and   // SS_660_MDI_20130409
                                          cdsConvenios.Locate('AutorizaFiscalia', 0, []) and                          // SS_660_CQU_20130604
                                          ValidaEstados() and ExisteAcceso('AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA');  // SS_660_MDI_20130409
                btnAutGerencia.Enabled := (FPrimerEstado = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION) and   // SS_660_MDI_20130409
                                          //cdsConvenios.Locate('AutorizaGerencia', 0, []) and                                    // SS_660_CQU_20130628  // SS_660_CQU_20130604
                                          cdsConvenios.Locate('AutorizaFiscalia;AutorizaGerencia', VarArrayOf(['1','0']), []) and // SS_660_CQU_20130628
                                          ValidaEstados() and ExisteAcceso('AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA');  // SS_660_MDI_20130409
                btnAnularCarta.Enabled := False;                                                                      // SS_660_CQU_20130604
            end;
            
        end;
    end;

    //if (Column = dblListaAmarilla.Columns[8]) and (spObtenerConveniosEnListaAmarilla.FieldByName('HistoricoCarta').AsString <> '') then begin	// SS_660_CQU_20130711 // SS_660_MDI_20130307       //SS_660_MVI_20130711
    if (Column = dblListaAmarilla.Columns[DBListExObtenerIDColumna(dblListaAmarilla, 'HistoricoCarta', 0)])                                     // SS_660_CQU_20130711
        and (spObtenerConveniosEnListaAmarilla.FieldByName('HistoricoCarta').AsString <> '')                                                    // SS_660_CQU_20130711
    then begin																																	// SS_660_CQU_20130711
        try
            ConsultarHistoricoForm := TListaDeHistoricoCartasListaAmarillaForm.Create(nil);
            if ConsultarHistoricoForm.Inicializar(spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsInteger,
                                                spObtenerConveniosEnListaAmarilla.FieldByName('NumeroConvenio').AsString,
                                                spObtenerConveniosEnListaAmarilla.FieldByName('Descripcion').AsString) then
            begin
                if ConsultarHistoricoForm.ShowModal = mrOk then begin
                    Screen.Cursor := crHourGlass;                               //SS_660_NDR_20131112
                    spObtenerConveniosEnListaAmarilla.Close;
                    spObtenerConveniosEnListaAmarilla.Open;
                    Screen.Cursor := crDefault;                                 //SS_660_NDR_20131112
                end;
            end;
        finally
            if Assigned(ConsultarHistoricoForm) then FreeAndNil(ConsultarHistoricoForm);
        end;
    end;

    //if Column = dblListaAmarilla.Columns[18] then begin                       //SS_1249_MCA_20150406 //SS_660F_MCA_20140805
    if Column.Header.Caption = 'Observaci�n' then begin                         //SS_1249_MCA_20150406 //SS_660F_MCA_20140805
        Application.CreateForm(TObservacionesConvenioListaAmarillaForm, ObservacionesConveniosListaAmarilla);                     //SS_660F_MCA_20140805
        ObservacionesConveniosListaAmarilla.Inicializar(spObtenerConveniosEnListaAmarilla.FieldByName('NumeroConvenio').AsString, //SS_660F_MCA_20140805
          spObtenerConveniosEnListaAmarilla.FieldByName('CodigoConvenio').AsInteger,        //SS_660F_MCA_20140805
          spObtenerConveniosEnListaAmarilla.FieldByName('CodigoConcesionaria').AsInteger,   //SS_660F_MCA_20140805
          spObtenerConveniosEnListaAmarilla.FieldByName('Concesionaria').AsString,          //SS_660F_MCA_20140805
          spObtenerConveniosEnListaAmarilla.FieldByName('Estado').AsInteger,                //SS_660F_MCA_20140805
          spObtenerConveniosEnListaAmarilla.FieldByName('Descripcion').AsString,            //SS_660F_MCA_20140805
          spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsInteger);//SS_660F_MCA_20140805
          ObservacionesConveniosListaAmarilla.ShowModal;        //SS_660F_MCA_20140805
        ObservacionesConveniosListaAmarilla.Release;                          //SS_660F_MCA_20140805

    end;


    Screen.Cursor := crDefault ;  // SS_660_MDI_20130307
end;

{---------------------------------------------------------------------------------
Function Name   : btnReEnviarCartaClick
Author          : CQuezadaI
Date Created    : 20/02/2013
Description     : Guarda el nuevo estado de las cartas enviadas
Parameters      : Sender: TObject;
Return Value    : None
Firma           : SS_660_CQU_20121010
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnReEnviarCartaClick(Sender: TObject);
resourcestring                                                          // SS_660_CQU_20130604
    MSG_REENVIO_MASIVO  =   'No se puede reenviar cartas masivamente';  // SS_660_CQU_20130604
var
    GenerarHistoricoForm : THistoricoCartasListaAmarillaForm;
begin
    if not ValidateControls([btnReEnviarCarta],                         // SS_660_CQU_20130604
                            [cdsConvenios.RecordCount = 1],             // SS_660_CQU_20130604
                            Caption,                                    // SS_660_CQU_20130604
                            [MSG_REENVIO_MASIVO]) then Exit;            // SS_660_CQU_20130604

    try
        GenerarHistoricoForm := THistoricoCartasListaAmarillaForm.Create(nil);
        if GenerarHistoricoForm.Inicializar(cdsConvenios.FieldByName('IDConvenioInhabilitado').AsInteger,
                                            cdsConvenios.FieldByName('CodigoConvenio').AsInteger,
                                            cdsConvenios.FieldByName('FechaEnvioCarta').AsDateTime) then
        // SS_660_MDI_20130307
        {if GenerarHistoricoForm.Inicializar(spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsInteger,
                                            spObtenerConveniosEnListaAmarilla.FieldByName('CodigoConvenio').AsInteger,
                                            spObtenerConveniosEnListaAmarilla.FieldByName('FechaEnvioCarta').AsDateTime) then}
        // FIN SS_660_MDI_20130307
        begin
            if GenerarHistoricoForm.ShowModal = mrOk then begin
                Screen.Cursor := crHourGlass;                               //SS_660_NDR_20131112
                spObtenerConveniosEnListaAmarilla.Close;
                spObtenerConveniosEnListaAmarilla.Open;
                cdsConvenios.EmptyDataSet;  // SS_660_CQU_20130604
                Screen.Cursor := crDefault;                                //SS_660_NDR_20131112
            end;
        end;
    finally
        if Assigned(GenerarHistoricoForm) then FreeAndNil(GenerarHistoricoForm);
    end;
    //cdsConvenios.EmptyDataSet;       // SS_660_CQU_20130604  // SS_660_MDI_20130307
    btnCambiarEstado.Enabled := False; // SS_660_MDI_20130307
    btnReEnviarCarta.Enabled := False; // SS_660_MDI_20130307
end;

{---------------------------------------------------------------------------------
Function Name   : btnSeleccionarTodosClick
Author          : MDI
Date Created    : 04/03/2013
Description     : Selecciona todos los registros de listado de convenios
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_MDI_20130307
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnSeleccionarTodosClick(
  Sender: TObject);
var
  FIDConvenioInhabilitado : string;
begin
    Screen.Cursor := crHourGlass ;
    if spObtenerConveniosEnListaAmarilla.Active then begin
        spObtenerConveniosEnListaAmarilla.DisableControls;
        spObtenerConveniosEnListaAmarilla.First;
        while not spObtenerConveniosEnListaAmarilla.eof do begin
            FIDConvenioInhabilitado := spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsString;
            if not ListaConveniosIn(FIDConvenioInhabilitado) then begin
                ListaConveniosInclude(FIDConvenioInhabilitado);
            end;
            spObtenerConveniosEnListaAmarilla.Next;
        end;
        spObtenerConveniosEnListaAmarilla.EnableControls;
        spObtenerConveniosEnListaAmarilla.First;
        dblListaAmarilla.Invalidate;
        dblListaAmarilla.Repaint;
    end;
    Screen.Cursor := crDefault ;

    btnCambiarEstado.Enabled := ValidaEstados() and ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA');                         // SS_660_MDI_20130409
    //btnAutFiscalia.Enabled := ValidaEstados() and ExisteAcceso('AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA') and                       // SS_660_CQU_20130604 // SS_660_MDI_20130409
    //                        (cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION); // SS_660_CQU_20130604 // SS_660_MDI_20130409
    btnAutFiscalia.Enabled := (ExisteAcceso('AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA') and                                            // SS_660_CQU_20130604
                              cdsConvenios.Locate('AutorizaFiscalia', 0, []));                                                      // SS_660_CQU_20130604
    //btnAutGerencia.Enabled := ValidaEstados() and ExisteAcceso('AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA') and                       // SS_660_CQU_20130604 // SS_660_MDI_20130409
    //                         (cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION);// SS_660_CQU_20130604 // SS_660_MDI_20130409
    btnAutGerencia.Enabled := (ExisteAcceso('AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA') and                                            // SS_660_CQU_20130604
                                           //cdsConvenios.Locate('AutorizaGerencia', 0, []));                                       // SS_660_CQU_20130628 // SS_660_CQU_20130604
                                           cdsConvenios.Locate('AutorizaFiscalia;AutorizaGerencia', VarArrayOf(['1','0']), []));    // SS_660_CQU_20130628

    // SS_660_MDI_20130307
    if cdsConvenios.RecordCount > 1 then begin
        btnReEnviarCarta.Enabled := False;
        btnAnularCarta.Enabled := False;                                                                                // SS_660_CQU_20130604
        if (cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) or              // SS_660_MDI_20130409
            (cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) then begin  // SS_660_MDI_20130409
            btnCambiarEstado.Enabled := False;                                                                          // SS_660_MDI_20130409
        end;                                                                                                            // SS_660_MDI_20130409

    end;
    // FIN SS_660_MDI_20130307
    cdsConvenios.Filtered := False;
end;

{---------------------------------------------------------------------------------
Function Name   : CargarCantidadReEnvios
Author          : CQuezadaI
Date Created    : 25/02/2013
Description     : Obtiene la cantidad de reenv�os posibles para una carta de Lista amarilla
Parameters      : None
Return Value    : None
Firma           : SS_660_CQU_20121010

Firma           : SS_660_CQU_20130604
Descripcion     : Se elimina la funci�n, se usa en SQL ahora
----------------------------------------------------------------------------------}
{procedure TListaDeConveniosEnListaAmarillaForm.CargarCantidadReEnvios;                                                     // SS_660_CQU_20130604
resourcestring                                                                                                              // SS_660_CQU_20130604
    CANT_REENVIO_CARTA_LISTA_AMARILLA   =   'CANT_REENVIO_CARTA_LISTA_AMARILLA';                                            // SS_660_CQU_20130604
    MSG_ERROR_PARAMETRO			        =   'No se pudo leer el par�metro general %s';                                      // SS_660_CQU_20130604
    MSG_ERROR                           =   'Ocurri� un error';                                                             // SS_660_CQU_20130604
begin                                                                                                                       // SS_660_CQU_20130604
    try                                                                                                                     // SS_660_CQU_20130604
        if not ObtenerParametroGeneral(	DMConnections.BaseCAC, CANT_REENVIO_CARTA_LISTA_AMARILLA, FCantReEnvios) then begin // SS_660_CQU_20130604
            MsgBoxErr(MSG_ERROR, Format(MSG_ERROR_PARAMETRO, [CANT_REENVIO_CARTA_LISTA_AMARILLA]), Caption, MB_ICONERROR);  // SS_660_CQU_20130604
            FCantReEnvios := -1; //si no se encuentra, se asume un valor                                                    // SS_660_CQU_20130604
        end                                                                                                                 // SS_660_CQU_20130604
    except                                                                                                                  // SS_660_CQU_20130604
        on e:exception do begin                                                                                             // SS_660_CQU_20130604
            MsgBoxErr(Format(MSG_ERROR_PARAMETRO, [CANT_REENVIO_CARTA_LISTA_AMARILLA]), e.Message, Caption, MB_ICONERROR);  // SS_660_CQU_20130604
        end;                                                                                                                // SS_660_CQU_20130604
    end;                                                                                                                    // SS_660_CQU_20130604
end;}                                                                                                                       // SS_660_CQU_20130604

procedure TListaDeConveniosEnListaAmarillaForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;


{---------------------------------------------------------------------------------
Function Name   : btnInvertirSeleccionClick
Author          : MDI
Date Created    : 04/03/2013
Description     : invierte la selecci�n hecha en el listado de convenios
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_MDI_20130307
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnInvertirSeleccionClick(
  Sender: TObject);
var FIDConvenioInhabilitado : string;
begin
    Screen.Cursor := crHourGlass ;
    if spObtenerConveniosEnListaAmarilla.Active then begin
        spObtenerConveniosEnListaAmarilla.DisableControls;
        spObtenerConveniosEnListaAmarilla.First;
        while not spObtenerConveniosEnListaAmarilla.eof do begin
            FIDConvenioInhabilitado := spObtenerConveniosEnListaAmarilla.FieldByName('IDConvenioInhabilitado').AsString;
            if ListaConveniosIn(FIDConvenioInhabilitado) then begin
                ListaConveniosExclude(FIDConvenioInhabilitado);
            end else begin
                ListaConveniosInclude(FIDConvenioInhabilitado);
            end;
            spObtenerConveniosEnListaAmarilla.Next;
        end;
        spObtenerConveniosEnListaAmarilla.EnableControls;
        spObtenerConveniosEnListaAmarilla.First;
        dblListaAmarilla.Invalidate;
        dblListaAmarilla.Repaint;
    end;
    // SS_660_MDI_20130307
    btnReEnviarCarta.Enabled := False;
    btnAnularCarta.Enabled := False;                                                                                  // SS_660_CQU_20130604
    //btnCambiarEstado.Enabled := (ValidaEstados() and (cdsConvenios.RecordCount > 0));                               // SS_660_MDI_20130409
    if ((cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) or               // SS_660_MDI_20130409
        (cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO))then begin    // SS_660_MDI_20130409
        btnCambiarEstado.Enabled := False;                                                                            // SS_660_MDI_20130409
    end                                                                                                               // SS_660_MDI_20130409
    else begin                                                                                                        // SS_660_MDI_20130409
        //btnCambiarEstado.Enabled := True

        if cdsConvenios.RecordCount > 0 then begin                                                                              // SS_660_MDI_20130409

            if cdsConvenios.FieldByName('Estado').AsInteger = CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION then begin    // SS_660_MDI_20130409
                                                                                                                                // SS_660_MDI_20130409
                //btnAutFiscalia.Enabled := True;                                                                               // SS_660_CQU_20130604  // SS_660_MDI_20130409
                //btnAutGerencia.Enabled := True;                                                                               // SS_660_CQU_20130604  // SS_660_MDI_20130409
                btnAutFiscalia.Enabled := (ExisteAcceso('AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA') and                            // SS_660_CQU_20130604
                                           cdsConvenios.Locate('AutorizaFiscalia', 0, []));                                     // SS_660_CQU_20130604
                btnAutGerencia.Enabled := (ExisteAcceso('AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA') and                            // SS_660_CQU_20130604
                                           //cdsConvenios.Locate('AutorizaGerencia', 0, []));                                   // SS_660_CQU_20130628 // SS_660_CQU_20130604
                                           cdsConvenios.Locate('AutorizaFiscalia;AutorizaGerencia', VarArrayOf(['1','0']), []));// SS_660_CQU_20130628

            end;                                                                                                                // SS_660_MDI_20130409
            //if ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA') then begin                                              // SS_660_CQU_20130604  // SS_660_MDI_20130409
            //    btnCambiarEstado.Enabled := True;                                                                             // SS_660_CQU_20130604  // SS_660_MDI_20130409
            //end;                                                                                                              // SS_660_CQU_20130604  // SS_660_MDI_20130409
            btnCambiarEstado.Enabled := (ExisteAcceso('CAMBIAR_ESTADO_CONVENIO_LISTA_AMARILLA') and                             // SS_660_CQU_20130604
                                        ValidaEstados());                                                                       // SS_660_CQU_20130604
            if cdsConvenios.RecordCount = 1 then begin                                                                          // SS_660_MDI_20130409
                                                                                                                                // SS_660_MDI_20130409
                //if ExisteAcceso('REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA') then begin                                          // SS_660_CQU_20130515  // SS_660_MDI_20130409
                //if ExisteAcceso('REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA') and                                                 // SS_660_CQU_20130604  // SS_660_CQU_20130515
                //    (cdsConvenios.FieldByName('CantReEnvio').AsInteger > 0)                                                   // SS_660_CQU_20130604  // SS_660_CQU_20130515
                //then begin                                                                                                    // SS_660_CQU_20130604  // SS_660_CQU_20130515
                //    btnReEnviarCarta.Enabled := True;                                                                         // SS_660_CQU_20130604  // SS_660_CQU_20130515
                //end;                                                                                                          // SS_660_CQU_20130604  // SS_660_MDI_20130409

                btnAutFiscalia.Enabled := (ExisteAcceso('AUTORIZA_FISCALIA_CONV_LISTA_AMARILLA') and                            // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeAutorizarFiscalia').AsBoolean);                      // SS_660_CQU_20130604

                btnAutGerencia.Enabled := (ExisteAcceso('AUTORIZA_GERENCIA_CONV_LISTA_AMARILLA') and                            // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeAutorizarGerencia').AsBoolean);                      // SS_660_CQU_20130604

                btnReEnviarCarta.Enabled := (ExisteAcceso('REENVIAR_CARTA_CONVENIO_LISTA_AMARILLA') and                         // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeReEnviarCarta').AsBoolean);                          // SS_660_CQU_20130604

                btnAnularCarta.Enabled := (ExisteAcceso('RECHAZAR_CARTA_CONVENIO_LISTA_AMARILLA') and                           // SS_660_CQU_20130604
                                            cdsConvenios.FieldByName('PuedeAnularCarta').AsBoolean);                            // SS_660_CQU_20130604
            end;                                                                                                          // SS_660_MDI_20130409
        end
        else begin
            btnCambiarEstado.Enabled := False;                                                                            // SS_660_MDI_20130409
            btnReEnviarCarta.Enabled := False;                                                                            // SS_660_MDI_20130409
            btnAutFiscalia.Enabled   := False;                                                                            // SS_660_MDI_20130409
            btnAutGerencia.Enabled   := False;                                                                            // SS_660_MDI_20130409
            btnAnularCarta.Enabled   := False;                                                                            // SS_660_CQU_20130604
        end;
    end;                                                                                                                  // SS_660_MDI_20130409

    // FIN SS_660_MDI_20130307
    Screen.Cursor := crDefault ;

end;

{---------------------------------------------------------------------------------
Function Name   : ValidaEstados
Author          : MDI
Date Created    : 14/03/2013
Description     : valida que todos los convenios seleccionados para enviar a cambio de estado masivo sean del mismo estado
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_MDI_20130307
----------------------------------------------------------------------------------}
function TListaDeConveniosEnListaAmarillaForm.ValidaEstados():Boolean;
var
    CantidadTotal : Integer;
    CantidadFiltrada : Integer;
begin
    Result := True;
    //if cdsConvenios.RecordCount > 1 then begin        // SS_660_CQU_20130515
    if cdsConvenios.RecordCount > 0 then begin          // SS_660_CQU_20130515
        CantidadTotal := cdsConvenios.RecordCount;
        cdsConvenios.Filter := 'Estado = ' + IntToStr(FPrimerEstado);
        cdsConvenios.Filtered := True;
        CantidadFiltrada := cdsConvenios.RecordCount;
        cdsConvenios.Filter := EmptyStr;
        cdsConvenios.Filtered := False;
        if (CantidadTotal > CantidadFiltrada) then begin
            Result := False;
            Exit;   // SS_660_MDI_20130307
        end;
    end
    else begin
        if (spObtenerConveniosEnListaAmarilla.FieldByName('Estado').AsInteger <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO) and             // SS_660_MDI_20130409
            (spObtenerConveniosEnListaAmarilla.FieldByName('Estado').AsInteger <> CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO) then begin  // SS_660_MDI_20130409
            Result := False;
            Exit;   // SS_660_MDI_20130307
        end;
    end;

end;

{---------------------------------------------------------------------------------
Function Name   : LimpiaConvenio
Author          : MDI
Date Created    : 09/04/2013
Description     : limpia combo convenio
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_MDI_20130409
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.LimpiaConvenio;
begin
    vcbConveniosCliente.Clear;
    vcbConveniosCliente.Items.Add('<Todos>', -1);
    vcbConveniosCliente.ItemIndex := vcbConveniosCliente.Items.IndexOfValue(-1);
end;


{------------------------------------------------------------------------
        btnAutGerenciaClick

    Author      : MDiazV
    Date        : 19-04-2013
    Description : Autoriza por gerencia el o los convenios seleccionados.

    Autor       : CQuezadaI
    Fecha       : 13-06-2013
    Firma       : SS_660_CQU_20130604
    Descripci�n : Modifica la transacci�n, no se hace por SQL sino por Objeto
-------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnAutGerenciaClick(
  Sender: TObject);
resourcestring
   //MSG_ERROR = 'No puede realizar la autorizaci�n. Uno o m�s convenios ya se encuentran autorizados';   // SS_660_CQU_20130604
   MSG_ERROR = 'Los Convenios seleccionados ya fueron autorizados por Gerencia';                          // SS_660_CQU_20130604
   //MSG_AVISO = 'Est� por Autorizar por Gerencia los registros seleccionados. Desea continuar?';         // SS_660_CQU_20130604
   MSG_AVISO = 'Ser�n Autorizados por Gerencia %d Convenios.' + CRLF + '�Desea continuar?';               // SS_660_CQU_20130604
   MSG_ERROR_SQL = 'Ocurri� un error al intentar Autorizar por Gerencia los convenios seleccionados';
   MSG_UPDATE_OK = 'La Autorizaci�n se realiz� correctamente';
var
    control, error : Boolean;
    Retorno : integer;
    SinAutorizar : Boolean;                                                                               // SS_660_CQU_20130604
begin
    //control := False;                                                                                   // SS_660_CQU_20130604
    error   := False;
    //control := cdsConvenios.Locate('AutorizaGerencia', 1, []);                                          // SS_660_CQU_20130628  // SS_660_CQU_20130604
    //SinAutorizar := cdsConvenios.Locate('AutorizaGerencia', 0, []);                                     // SS_660_CQU_20130628  // SS_660_CQU_20130604
    control := cdsConvenios.Locate('AutorizaFiscalia;AutorizaGerencia', VarArrayOf(['1','1']), []);       // SS_660_CQU_20130628
    SinAutorizar := cdsConvenios.Locate('AutorizaFiscalia;AutorizaGerencia', VarArrayOf(['1','0']), []);  // SS_660_CQU_20130628
    //with cdsConvenios do begin                                                                          // SS_660_CQU_20130604

    //    while not Eof do begin                                                                          // SS_660_CQU_20130604

    //        if FieldByName('AutorizaGerencia').AsBoolean then begin                                     // SS_660_CQU_20130604
    //            control := True;                                                                        // SS_660_CQU_20130604
    //        end;                                                                                        // SS_660_CQU_20130604
    //        Next;                                                                                       // SS_660_CQU_20130604
    //    end;                                                                                            // SS_660_CQU_20130604

    //end;                                                                                                // SS_660_CQU_20130604

    //if control then begin                                                                               // SS_660_CQU_20130604
    if control and not SinAutorizar then begin                                                            // SS_660_CQU_20130604

        MsgBox(MSG_ERROR, Caption, MB_ICONERROR);
        //cdsConvenios.EmptyDataSet;                                                                      // SS_660_CQU_20130604
        //spObtenerConveniosEnListaAmarilla.Close;                                                        // SS_660_CQU_20130604
        //spObtenerConveniosEnListaAmarilla.Open;                                                         // SS_660_CQU_20130604
        //btnAutFiscalia.Enabled := False;                                                                // SS_660_CQU_20130604
        //btnAutGerencia.Enabled := False;                                                                // SS_660_CQU_20130604

    end
    else begin

        //if SinAutorizar and control then begin                                                          // SS_660_CQU_20130628 // SS_660_CQU_20130604
        //    cdsConvenios.Filter     := 'AutorizaGerencia = 0';                                          // SS_660_CQU_20130628 // SS_660_CQU_20130604
            cdsConvenios.Filter     := 'AutorizaFiscalia = 1 AND AutorizaGerencia = 0';                   // SS_660_CQU_20130628
            cdsConvenios.Filtered   := True;                                                              // SS_660_CQU_20130604
        //end;                                                                                            // SS_660_CQU_20130628 // SS_660_CQU_20130604

        //if (MsgBox(MSG_AVISO, 'Atenci�n', MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin              // SS_660_CQU_20130604
        if (MsgBox(Format(MSG_AVISO, [cdsConvenios.RecordCount]),                                         // SS_660_CQU_20130604
                    'Atenci�n', MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin                          // SS_660_CQU_20130604

            //DMConnections.BaseCAC.Execute('BEGIN TRAN spAutorizarConvenioPorGerencia');                 // SS_660_CQU_20130604
            DMConnections.BaseCAC.BeginTrans;                                                             // SS_660_CQU_20130604
            cdsConvenios.First;
            while not cdsConvenios.Eof do begin

                spAutorizarConvenioPorGerencia.Close;

                spAutorizarConvenioPorGerencia.Parameters.Refresh;
                spAutorizarConvenioPorGerencia.Parameters.ParamByName('@IDConvenioInhabilitado').Value :=  cdsConvenios.FieldByName('IDConvenioInhabilitado').AsInteger;
                spAutorizarConvenioPorGerencia.Parameters.ParamByName('@CodigoUsuario').Value          :=  UsuarioSistema;
                spAutorizarConvenioPorGerencia.Parameters.ParamByName('@MensajeError').Value           :=  null;

                try
                    spAutorizarConvenioPorGerencia.ExecProc;
                    Retorno := spAutorizarConvenioPorGerencia.Parameters.ParamByName('@RETURN_VALUE').Value;
                    if Retorno < 0 then begin
                        error := True;
                        end;
                except on e:exception do begin
                    error := True;
                    end;
                end;

                cdsConvenios.Next;

            end;
            if error then begin
                //DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAutorizarConvenioPorGerencia');          // SS_660_CQU_20130604
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;          // SS_660_CQU_20130604
                MsgBox(MSG_ERROR_SQL, Caption, MB_ICONERROR);
            end
            else begin
                //DMConnections.BaseCAC.Execute('COMMIT TRAN spAutorizarConvenioPorGerencia');            // SS_660_CQU_20130604
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;            // SS_660_CQU_20130604
                MsgBox(MSG_UPDATE_OK, Caption, MB_ICONINFORMATION);
            end;
        //end;                                                                                            // SS_660_CQU_20130711
        end else error := True;                                                                           // SS_660_CQU_20130711
        if cdsConvenios.Filtered then begin                                                               // SS_660_CQU_20130604
            cdsConvenios.Filter     := EmptyStr;                                                          // SS_660_CQU_20130604
            cdsConvenios.Filtered   := False;                                                             // SS_660_CQU_20130604
        end;                                                                                              // SS_660_CQU_20130604
        //if spObtenerConveniosEnListaAmarilla.Active then begin                                          // SS_660_CQU_20130604
        if not error and spObtenerConveniosEnListaAmarilla.Active then begin                              // SS_660_CQU_20130604
            Screen.Cursor := crHourGlass;                               //SS_660_NDR_20131112
            cdsConvenios.EmptyDataSet;
            btnAutFiscalia.Enabled := False;
            btnAutGerencia.Enabled := False;
            spObtenerConveniosEnListaAmarilla.Close;
            spObtenerConveniosEnListaAmarilla.Open;
            btnCambiarEstado.Enabled := False;                                                              // SS_660_CQU_20130711
            Screen.Cursor := crDefault;                                 //SS_660_NDR_20131112
        end;

    end;
end;

{---------------------------------------------------------------------------------
Procedure Name  : btnAnularCartaClick
Author          : CQuezadaI
Date Created    : 12/06/2013
Description     : Rechaza una Carta enviada por Lista Amarilla
Parameters      : Sender: TObject
Return Value    : None
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.btnAnularCartaClick(Sender: TObject);
resourcestring
    MSG_PROBLEMA_RECHAZO    =   'Ocurri� un problema al rechazar la carta';
    MSG_NO_RECHAZAR_MASIVO  =   'No se puede rechazar cartas masivamente';
var
    RechazarCartaForm : TRechazarCartaListaAmarillaForm;
begin
    if not ValidateControls([btnAnularCarta],
                            [cdsConvenios.Recordcount = 1],
                            Caption,
                            [MSG_NO_RECHAZAR_MASIVO]) then Exit;

    try
        RechazarCartaForm := TRechazarCartaListaAmarillaForm.Create(nil);
        if RechazarCartaForm.Inicializar(
                            cdsConvenios.FieldByName('IDConvenioInhabilitado').AsInteger,
                            cdsConvenios.FieldByName('CodigoConvenio').AsInteger,
                            cdsConvenios.FieldByName('FechaEnvioCarta').AsDateTime,
                            cdsConvenios.FieldByName('NumeroConvenio').AsString,
                            cdsConvenios.FieldByName('Concesionaria').AsString                //SS_660_MVI_20130711
        ) then begin
            if RechazarCartaForm.ShowModal = mrOk then begin
                Screen.Cursor := crHourGlass;                               //SS_660_NDR_20131112
                spObtenerConveniosEnListaAmarilla.Close;
                spObtenerConveniosEnListaAmarilla.Open;
                cdsConvenios.EmptyDataSet;
                btnAnularCarta.Enabled := False;  // SS_660_CQU_20130628
                Screen.Cursor := crDefault;                                 //SS_660_NDR_20131112
            end else begin
                MsgBox('Se cancel� el rechazo de carta', Caption, MB_ICONASTERISK);
            end;
        end;
    finally
        if Assigned(RechazarCartaForm) then FreeAndNil(RechazarCartaForm);
        //btnAnularCarta.Enabled := False;        // SS_660_CQU_20130628
    end;
end;

{---------------------------------------------------------------------------------
Procedure Name  : CargarConcesionarias
Author          : CQuezadaI
Date Created    : 18/07/2013
Description     : Carga las concesionarias
Parameters      : None
Return Value    : None
Firma           : SS_660_CQU_20130711
----------------------------------------------------------------------------------}
procedure TListaDeConveniosEnListaAmarillaForm.CargarConcesionarias;
resourcestring
    ERROR_CONCESIONES   = 'No hay concesionarias definidas.';
var
    CodigoConcesionaria : Integer;
    Concesionaria       : String;
begin
    try
        try
            // Cargo las Concesionarias
            spObtenerConcesionarias.Parameters.Refresh;
            spObtenerConcesionarias.Parameters.ParamByName('@CodigoTipoConcesionaria').Value    := Null;
            spObtenerConcesionarias.Parameters.ParamByName('@FiltraPorConcepto').Value          := 0;
            spObtenerConcesionarias.Open;
            if spObtenerConcesionarias.IsEmpty then raise exception.Create(ERROR_CONCESIONES);

            spObtenerConcesionarias.Filter := 'PermiteInhabilitarListaAmarilla = 1';
            spObtenerConcesionarias.Filtered := True;

            if not spObtenerConcesionarias.Eof then begin
                vcbConcesionarias.Items.Add('<Todas>', 0);
                while not spObtenerConcesionarias.Eof do begin
                    CodigoConcesionaria := spObtenerConcesionarias.FieldByName('CodigoConcesionaria').AsInteger;
                    Concesionaria       := spObtenerConcesionarias.FieldByName('Descripcion').AsString;
                    // Agrego la Concesionaria al combo
                    vcbConcesionarias.Items.Add(Concesionaria, CodigoConcesionaria);
                    spObtenerConcesionarias.Next;
                end;
            end;
        except
            on e:exception do begin
                MsgBoxErr('Problema al cargar concesionarias', e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        if vcbConcesionarias.Items.Count <= 1 then begin
            vcbConcesionarias.Clear;
            vcbConcesionarias.Items.Add('<Sin Concesionaria Habilitada>', -1);
            vcbConcesionarias.ItemIndex := vcbConcesionarias.Items.IndexOfValue(-1);
        end else
            vcbConcesionarias.ItemIndex := vcbConcesionarias.Items.IndexOfValue(0);
    end;
end;

{---------------------------------------------------------------------------------
Procedure Name  : RevisaFiltro
Author          : CQuezadaI
Date Created    : 18/07/2013
Description     : Revisa si el filtro tiene datos, si no tiene, avisa que se demorar�
Parameters      : None
Return Value    : None
Firma           : SS_660_CQU_20131104
----------------------------------------------------------------------------------}
function TListaDeConveniosEnListaAmarillaForm.RevisaFiltro(): Boolean;
resourcestring
    MSG_AVISO   =   'No ha ingresado datos en el filtro, se traer�n todos los registros encontrados y esto puede tardar un tiempo considerable. �desea continuar?';
begin
    Result := True;
    if  (peRUTCliente.Text = '') and
        (vcbConveniosCliente.ItemIndex <= 0) and
        (vcbEstados.ItemIndex <= 0) and
        (deFechaIngresoDesde.Date <= 0) and
        (deFechaIngresoHasta.Date <= 0) and
        (deFechaAltaDesde.Date <= 0) and
        (deFechaAltaHasta.Date <= 0) and
        (deFechaBajaDesde.Date <= 0) and
        (deFechaBajaHasta.Date <= 0) and
        (vcbConcesionarias.ItemIndex <= 0)   and                // SS_1231_MBE_20141114
        (cbbAutorizaciones.ItemIndex <= 0)                      // SS_1231_MBE_20141114
    then begin
        if (MsgBox(Format(MSG_AVISO, [cdsConvenios.RecordCount]), 'Atenci�n', MB_YESNO + MB_ICONQUESTION) <> ID_YES) then
            Result := False;
    end;
end;

end.
