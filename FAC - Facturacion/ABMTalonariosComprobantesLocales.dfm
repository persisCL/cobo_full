object FormABMTalonariosComprobantesLocales: TFormABMTalonariosComprobantesLocales
  Left = 241
  Top = 174
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Talonarios de Comprobantes en Locales'
  ClientHeight = 423
  ClientWidth = 627
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 384
    Width = 627
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 305
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          ExplicitWidth = 0
          ExplicitHeight = 0
          object BtnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object Lista: TAbmList
    Left = 0
    Top = 33
    Width = 627
    Height = 188
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'45'#0'Local.   '
      #0'107'#0'Tipo Comprobante.   '
      #0'154'#0'Desde N'#250'mero Comprobante.  '
      #0'148'#0'Hasta N'#250'mero Comprobante. ')
    HScrollBar = True
    RefreshTime = 10
    Table = TalonariosComprobantesLocales
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaClick
    OnProcess = ListaProcess
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
    OnInsert = ListaInsert
    OnDelete = ListaDelete
    OnEdit = ListaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 627
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar, btImprimir]
    OnClose = AbmToolbar1Close
  end
  object GroupB: TPanel
    Left = 0
    Top = 221
    Width = 627
    Height = 163
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object lblTipoComprobante: TLabel
      Left = 9
      Top = 40
      Width = 108
      Height = 13
      Caption = '&Tipo Comprobante:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblLocal: TLabel
      Left = 9
      Top = 13
      Width = 40
      Height = 13
      Caption = '&Local: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDesdeNumero: TLabel
      Left = 9
      Top = 64
      Width = 88
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = '&Desde N'#250'mero:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object lblHastaNumero: TLabel
      Left = 9
      Top = 90
      Width = 85
      Height = 13
      Caption = '&Hasta N'#250'mero:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblEmisionTalonario: TLabel
      Left = 9
      Top = 113
      Width = 105
      Height = 13
      Caption = '&Emisi'#243'n Talonario:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 9
      Top = 136
      Width = 131
      Height = 13
      Caption = '&Vencimiento Talonario:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbTipoComprobante: TVariantComboBox
      Left = 122
      Top = 35
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object neDesdeNumero: TNumericEdit
      Left = 144
      Top = 61
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object neHastaNumero: TNumericEdit
      Left = 144
      Top = 87
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object cbLocales: TVariantComboBox
      Left = 56
      Top = 9
      Width = 417
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbLocalesChange
      Items = <>
    end
    object GroupBox1: TGroupBox
      Left = 280
      Top = 40
      Width = 441
      Height = 113
      Caption = 'Datos del Local '
      TabOrder = 4
      object lblRegionTxt: TLabel
        Left = 11
        Top = 17
        Width = 34
        Height = 13
        Caption = 'Region'
      end
      object lblRegion: TLabel
        Left = 60
        Top = 17
        Width = 44
        Height = 13
        Caption = 'lblRegion'
      end
      object lblCalletxt: TLabel
        Left = 11
        Top = 40
        Width = 23
        Height = 13
        Caption = 'Calle'
      end
      object lblCalle: TLabel
        Left = 60
        Top = 40
        Width = 33
        Height = 13
        Caption = 'lblCalle'
      end
      object lblNumeroTxt: TLabel
        Left = 11
        Top = 62
        Width = 37
        Height = 13
        Caption = 'N'#250'mero'
      end
      object lblNumero: TLabel
        Left = 60
        Top = 62
        Width = 47
        Height = 13
        Caption = 'lblNumero'
      end
      object lblComunaTxt: TLabel
        Left = 11
        Top = 83
        Width = 39
        Height = 13
        Caption = 'Comuna'
      end
      object lblComuna: TLabel
        Left = 62
        Top = 83
        Width = 49
        Height = 13
        Caption = 'lblComuna'
      end
    end
    object deFechaEmisionTalonario: TDateEdit
      Left = 144
      Top = 112
      Width = 121
      Height = 21
      AutoSelect = False
      TabOrder = 5
      Date = -693594.000000000000000000
    end
    object deFechaVencimientoTalonario: TDateEdit
      Left = 144
      Top = 136
      Width = 121
      Height = 21
      AutoSelect = False
      TabOrder = 6
      Date = -693594.000000000000000000
    end
  end
  object TalonariosComprobantesLocales: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'TalonariosComprobantesLocales'
    Left = 316
    Top = 75
    object TalonariosComprobantesLocalesTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object TalonariosComprobantesLocalesNumeroLocal: TIntegerField
      FieldName = 'NumeroLocal'
    end
    object TalonariosComprobantesLocalesNumeroComprobanteDesde: TIntegerField
      FieldName = 'NumeroComprobanteDesde'
    end
    object TalonariosComprobantesLocalesNumeroComprobanteHasta: TIntegerField
      FieldName = 'NumeroComprobanteHasta'
    end
    object TalonariosComprobantesLocalesFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object TalonariosComprobantesLocalesUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object TalonariosComprobantesLocalesFechaHoraActualizacion: TDateTimeField
      FieldName = 'FechaHoraActualizacion'
    end
    object TalonariosComprobantesLocalesUsuarioActualizacion: TStringField
      FieldName = 'UsuarioActualizacion'
      FixedChar = True
    end
    object TalonariosComprobantesLocalesIDTalonariosComprobantesLocales: TAutoIncField
      FieldName = 'IDTalonariosComprobantesLocales'
      ReadOnly = True
    end
    object TalonariosComprobantesLocalesFechaEmision: TDateTimeField
      FieldName = 'FechaEmision'
    end
    object TalonariosComprobantesLocalesFechaVencimiento: TDateTimeField
      FieldName = 'FechaVencimiento'
    end
  end
  object spObtenerTiposComprobantesPrenumerados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposComprobantesPrenumerados;1'
    Parameters = <>
    Left = 440
    Top = 144
  end
  object spObtenerLocalesComprobantesPrenumerados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerLocalesComprobantesPrenumerados;1'
    Parameters = <>
    Left = 336
    Top = 144
  end
  object spObtenerDomicilioLocal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioLocal;1'
    Parameters = <>
    Left = 512
    Top = 144
  end
end
