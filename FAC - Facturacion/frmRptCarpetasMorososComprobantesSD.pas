{********************************** Unit Header ********************************
File Name     : frmRptCarpetasMorososComprobantesSD.pas
Author        : Nelson Droguett S
Date Created  : 2015-10-26
Language      : ES-AR
Description   : Reporte de Comprobantes SD impagos para imprimirlo en carpetas legales
*******************************************************************************}
unit frmRptCarpetasMorososComprobantesSD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, UtilRB, ppPrnabl, ppCtrls, daDataView, daQueryDataView, daADO, ppModule,
  daDataModule, ppParameter, ppBands, ppCache, ppStrtch, ppSubRpt, raCodMod, DBClient, DMConnection, ppVar, uTILpROC,
  ppRegion, ppPageBreak, ppFormWrapper, ppRptExp, RBSetup, ppTypes,
  ppBarCod, ppMemo, TeEngine, Series, ExtCtrls, TeeProcs, Chart, DbChart, Util, UtilDB, PeaTypes,
  ConstParametrosGenerales;

type
  TfrmRptCarpetasMorososComprobantesSD = class(TForm)
    pprComprobantesSD: TppReport;
    ppParameterList2: TppParameterList;
    rbiComprobantesSD: TRBInterface;
    ppTitleBand2: TppTitleBand;
    ppLabel7: TppLabel;
    ppImgLogoNK: TppImage;
    pplblFecha: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppLine6: TppLine;
    ppNombre: TppLabel;
    ppApellido: TppLabel;
    ppConvenio: TppLabel;
    ppLine7: TppLine;
    pplblConvenio: TppLabel;
    pplblNombre: TppLabel;
    pplblApellido: TppLabel;
    ppFooterBand2: TppFooterBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    gfConvenio: TppGroupFooterBand;
    lblUsuario: TppLabel;
    spObtenerComprobantes: TADOStoredProc;
    dsObtenerComprobantes: TDataSource;
    plObtenerComprobantes: TppDBPipeline;
    ppdbtTipoComp: TppDBText;
    ppdbtNumeroComp: TppDBText;
    ppdbtTipoDoc: TppDBText;
    ppdbtNumDoc: TppDBText;
    ppdbtSaldo: TppDBText;
    ppdbtFechaEmision: TppDBText;
    ppdbtFechaVenc: TppDBText;
    ppdbtTotalComp: TppDBText;
    ppLine1: TppLine;
    pplblTipoComp: TppLabel;
    pplblNumComp: TppLabel;
    pplblTipoDoc: TppLabel;
    pplblNumDoc: TppLabel;
    pplblFechaEmision: TppLabel;
    pplblFechaVenc: TppLabel;
    pplblTotalComp: TppLabel;
    pplblSaldoComp: TppLabel;
    pplblTotalComprobantes: TppLabel;
    pplblTotalSaldoComprobantes: TppLabel;
    ppLine2: TppLine;
    procedure pprComprobantesSDBeforePrint(Sender: TObject);
  private
    FApellido: string;
    FNombre: string;
    FConvenio: string;
    FTotalComprobantes: string;
    FSaldoComprobantes: string;
  public
    function Inicializar(Recorset: TClientDataSet): Boolean;
  end;

implementation

uses ImprimirWO;
{$R *.dfm}

{ TrptCarpetasMorososComprobantesSD }

{******************************** Function Header ******************************
Function Name: Inicializar
Author        : Nelson Droguett
Date Created  : 26-10-2015
Description   : Inicializa Reporte de Comprobantes SD impagos
Parameters    : Recorset: TClientDataSet;
Return Value  : Boolean
*******************************************************************************}
function TfrmRptCarpetasMorososComprobantesSD.Inicializar(Recorset: TClientDataSet): Boolean;
resourcestring
        cConvenio = '%s - %s';
var
  RutaLogo: AnsiString;
begin
    Result := False;

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);
    try
      ppImgLogoNK.Picture.LoadFromFile(Trim(RutaLogo));
    except
      On E: Exception do begin
        Screen.Cursor := crDefault;
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);
        Screen.Cursor := crHourGlass;
      end;
    end;

    FApellido  := Recorset.FieldByName('cdsConveniosEnCarpetaApellido').AsString;
    FNombre    := Recorset.FieldByName('cdsConveniosEnCarpetaNombre').AsString;
    FConvenio  := Format(cConvenio,[Recorset.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString,
                                    Recorset.FieldByName('cdsConveniosEnCarpetaDescripcionConcesionaria').AsString]
                        );
    FTotalComprobantes:=spObtenerComprobantes.Parameters.ParamByName('@TotalComprobantes').Value;
    FSaldoComprobantes:=spObtenerComprobantes.Parameters.ParamByName('@SaldoComprobantes').Value;
    if not rbiComprobantesSD.Execute(True) then
      Exit
    else
      Result := True;
end;


{******************************** Function Header ******************************
Function Name: Inicializar
Author        : Nelson Droguett
Date Created  : 26-10-2015
Description   : Asigna valores a etiquetas del reporte antes de imprimir
Parameters    : Recorset: TClientDataSet;
Return Value  : Boolean
*******************************************************************************}
procedure TfrmRptCarpetasMorososComprobantesSD.pprComprobantesSDBeforePrint(Sender: TObject);
begin
    ppApellido.Text     := FApellido;
    ppNombre.Text       := FNombre;
    ppConvenio.Text     := FConvenio;
    pplblTotalComprobantes.Text:=FTotalComprobantes;
    pplblTotalSaldoComprobantes.Text:=FSaldoComprobantes;
end;

end.
