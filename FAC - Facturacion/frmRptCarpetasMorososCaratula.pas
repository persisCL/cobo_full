{********************************** Unit Header ********************************
File Name : frmRptCarpetasMorososCaratula.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
    Author : ggomez
    Date : 01/09/2006
    Description : Agregado de reportes que ya exist�an, por ejemplo, ReporteFatura.pas.
        Se copi� el c�digo aqu� pues no pudimos reutilizar lo existente, debido
        a esta copia hay c�digo que permace muy similar a su original.

Revision 2:
    Author : nefernandez
    Date : 07/03/2007
    Description : Quit� el par�metro IdSesion de la funci�n 'Inicializar' porque
    ya no se usa el procedimiento de cargar/recuperar los comprobantes desde una
    tabla temporal. Tambi�n le agregu� un t�tulo al formulario del reporte



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

*******************************************************************************}
unit frmRptCarpetasMorososCaratula;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ppProd, ppClass, ppReport, ppComm, ppRelatv, ppDB, ppDBPipe, UtilRB, ppPrnabl, ppCtrls, daDataView, daQueryDataView, daADO, ppModule,
  daDataModule, ppParameter, ppBands, ppCache, ppStrtch, ppSubRpt, raCodMod, DBClient, DMConnection, ppVar, uTILpROC,
  ppRegion, ppPageBreak, ppFormWrapper, ppRptExp, RBSetup, ppTypes,
  ppBarCod, ppMemo, TeEngine, Series, ExtCtrls, TeeProcs, Chart, DbChart, Util, UtilDB, PeaTypes,   // SS_1036_PDO_20120418
  ConstParametrosGenerales;                                                                         //SS_1147_NDR_20140710

type
  TfrmCarpetasMorososCaratula = class(TForm)
    pprCaratula: TppReport;
    ppParameterList2: TppParameterList;
    rbiCaratula: TRBInterface;
    ppTitleBand2: TppTitleBand;
    ppLabel7: TppLabel;
    ppImgLogoNK: TppImage;
    ppLabel1: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppLine6: TppLine;
    ppNombre: TppLabel;
    ppApellido: TppLabel;
    ppConvenio: TppLabel;
    ppLine7: TppLine;
    ppLabel8: TppLabel;
    ppLabel32: TppLabel;
    ppLabel33: TppLabel;
    ppFooterBand2: TppFooterBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    gfConvenio: TppGroupFooterBand;
    lblUsuario: TppLabel;
    procedure pprCaratulaBeforePrint(Sender: TObject);
  private
    FApellido: string;
    FNombre: string;
    FConvenio: string;
  public
    // Revision 2
    function Inicializar(Recorset: TClientDataSet): Boolean;
  end;

implementation

uses ImprimirWO;
{$R *.dfm}

{ TrptCarpeatasMorosos }

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created :
Description :
Parameters : Recorset: TClientDataSet; IdSesion: Smallint
Return Value : Boolean

Revision 1:
    Author : ggomez
    Date : 01/09/2006
    Description : Agregu� la carga de valores generales al reporte.
*******************************************************************************}
// Revision 2
function TfrmCarpetasMorososCaratula.Inicializar(Recorset: TClientDataSet): Boolean;
    resourcestring                      // SS_1036_PDO_20120418
        cConvenio = '%s - %s - %s';     // SS_1036_PDO_20120418
var                                                                                                     //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                                                 //SS_1147_NDR_20140710
begin
    Result := False;

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImgLogoNK.Picture.LoadFromFile(Trim(RutaLogo));                                                 //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FApellido  := Recorset.FieldByName('cdsConveniosEnCarpetaApellido').AsString;
    FNombre    := Recorset.FieldByName('cdsConveniosEnCarpetaNombre').AsString;
    FConvenio  :=                                                                                                                           // SS_1036_PDO_20120418
        Format(cConvenio,                                                                                                                   // SS_1036_PDO_20120418
                [Recorset.FieldByName('cdsConveniosEnCarpetaNumeroConvenio').AsString,                                                      // SS_1036_PDO_20120418
                 Recorset.FieldByName('cdsConveniosEnCarpetaDescripcionConcesionaria').AsString,                                            // SS_1036_PDO_20120418
                 FloatToStrF(Recorset.FieldByName('cdsConveniosEnCarpetaImporteDeudaActualizada').AsCurrency / 100, ffCurrency,20,0)]);     // SS_1036_PDO_20120418

    if not rbiCaratula.Execute(True) then Exit
    else Result := True;
end;

procedure TfrmCarpetasMorososCaratula.pprCaratulaBeforePrint(Sender: TObject);
begin
    ppApellido.Text     := FApellido;
    ppNombre.Text       := FNombre;
    ppConvenio.Text     := FConvenio;
end;


end.
