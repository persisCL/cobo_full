{-----------------------------------------------------------------------------
 File Name: Frm_AgregarItemFacturacion.pas
 Author:    Flamas
 Date Created: 24/01/2005
 Language: ES-AR
 Description: Agrega un item a ser facturado

-----------------------------------------------------------------------------}
unit Frm_AgregarConceptoMovimiento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, VariantComboBox, DmiCtrls, DB, ADODB,
  PeaProcs, Util, ExtCtrls, UtilDB,PeaTypes, TimeEdit, ABMConceptosMovimiento, UtilProc;

CONST
    MAXIMO_INT = 2147483647;
    MINIMO_INT = -2147483648;

type
  TfrmAgregarConceptoMovimiento = class(TForm)
    Label1: TLabel;
    edFecha: TDateEdit;
    Label2: TLabel;
    cbConcepto: TVariantComboBox;
    btnCancelar: TButton;
    btnAceptar: TButton;
    Label3: TLabel;
    spObtenerConceptosComprobante: TADOStoredProc;
    Label4: TLabel;
    edImporte: TNumericEdit;
    edDetalle: TEdit;
    Bevel1: TBevel;
    chk_ImporteIncluyeIVA: TCheckBox;
    btnConceptos: TButton;
    Label5: TLabel;
    VcTipoComprobante: TVariantComboBox;
    procedure cbConceptoChange(Sender: TObject);
    procedure VcTipoComprobanteChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnConceptosClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    function CargarCombo: boolean;
  public
    { Public declarations }
    function Inicializar : Boolean; overload;
    function Inicializar(aDate: TDateTime; aCodigoConcepto: integer;
      aImporteCtvs: double; aObservaciones, TipoComprobante: string; IncluyeIva: boolean = False): Boolean; overload;

  end;

var
  frmAgregarConceptoMovimiento: TfrmAgregarConceptoMovimiento;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 24/01/2005
  Description: Inicializa el Form - Carga los Conceptos de Facturaci�n
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
{******************************** Function Header ******************************
Function Name: Inicializar
Author : llazarte
Date Created : 21/02/2006
Description :  Se abre el stored de acuerdo al tipo de comprobante deseado
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmAgregarConceptoMovimiento.Inicializar : Boolean;
begin
    // siempre levanto con NK - por default
    VcTipoComprobante.ItemIndex := 0;
    Result := CargarCombo;
    chk_ImporteIncluyeIVA.Visible := True;
    chk_ImporteIncluyeIVA.Checked := False;
end;


function TfrmAgregarConceptoMovimiento.Inicializar(aDate:TDateTime ; aCodigoConcepto:integer;
  aImporteCtvs:double; aObservaciones, TipoComprobante: string; IncluyeIva: boolean = False): Boolean;
var
  i:integer;
begin
	result := True;
    VcTipoComprobante.Value := TipoComprobante;
	CargarCombo;
    edFecha.Date := aDate;
    for i:= 0 to cbConcepto.Items.Count -1  do
      if cbConcepto.Items[i].Value = aCodigoConcepto then cbConcepto.ItemIndex := i;
    edDetalle.Text 					:= aObservaciones;
    edImporte.Value 				:= aImporteCtvs ;
    // me fijo en el chk
    if spObtenerConceptosComprobante.Lookup('CodigoConcepto', cbConcepto.Value, 'AfectoIVA') then begin
        chk_ImporteIncluyeIVA.Visible := True;
    end else chk_ImporteIncluyeIVA.Visible := False;

    chk_ImporteIncluyeIVA.Checked := IncluyeIva;
end;

{******************************** Function Header ******************************
Function Name: CargarCombo
Author : llazarte
Date Created : 21/02/2006
Description : Cargar el combo de conceptos de acuerdo al tipo de comprobante seleccionado
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmAgregarConceptoMovimiento.CargarCombo: boolean;
begin
    Result := False;
    Cursor := crHourGlass;
    spObtenerConceptosComprobante.Close;
    spObtenerConceptosComprobante.Parameters.ParamByName('@TipoComprobante').Value := VcTipoComprobante.Value;
    cbConcepto.Clear;
    if not OpenTables([spObtenerConceptosComprobante]) then begin
        Cursor := crDefault;
        Exit;
    end;
    while not spObtenerConceptosComprobante.Eof do begin
        if ((spObtenerConceptosComprobante.FieldByName('CodigoConcepto').AsInteger <> 1) and (spObtenerConceptosComprobante.FieldByName('CodigoConcepto').AsInteger <> 2)) then begin
            cbConcepto.Items.Add(Trim(spObtenerConceptosComprobante.FieldByName('Descripcion').AsString),
              spObtenerConceptosComprobante.FieldByName('CodigoConcepto').AsString);
        end;
        spObtenerConceptosComprobante.Next;
    end;
    Cursor := crDefault;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    flamas
  Date Created: 24/01/2005
  Description: Valida los valores de los campos para poder aceptar
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TfrmAgregarConceptoMovimiento.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_DATE_MUST_HAVE_A_VALUE 		= 'Debe especificar una fecha.';
    MSG_CONCEPT_MUST_HAVE_A_VALUE 	= 'Debe especificar un concepto';
    MSG_AMMOUNT_MUST_HAVE_A_VALUE 	= 'Debe especificar un monto';
    MSG_FECHA_MINIMA                = '01/01/2000';
    MSG_FECHA_ERRONEA               = 'La fecha ingresada no puede ser menor al 01/01/1950';
    TXT_ATENCION                    = 'Atenci�n';
    MSG_ERROR_RANGO                 = 'El n�mero ingresado supera el rango de n�meros permitidos';
var
    ValidarImporte: int64;
begin
    if ValidateControls(
    	[edFecha, cbConcepto, edImporte],
    	[edFecha.Date <> nulldate, cbConcepto.ItemIndex >= 0, edImporte.ValueInt <> 0],
    	caption,
        [MSG_DATE_MUST_HAVE_A_VALUE, MSG_CONCEPT_MUST_HAVE_A_VALUE, MSG_AMMOUNT_MUST_HAVE_A_VALUE]) then begin
         // me fijo si es superior, por ahora puesto aqui, no se si queda esta control
        if (edFecha.Date < EncodeDate(1950, 1, 1)) then begin
            MsgBoxBalloon(MSG_FECHA_ERRONEA, TXT_ATENCION, MB_ICONINFORMATION, edFecha);
            Exit;
        end;
        // me fijo por el rango del tipo de datos entero
        ValidarImporte := edImporte.ValueInt * 100;
        if (ValidarImporte > MAXIMO_INT) OR (ValidarImporte < MINIMO_INT) then begin
            MsgBoxBalloon(MSG_ERROR_RANGO, TXT_ATENCION, MB_ICONINFORMATION, edImporte);
            Exit;
        end;
    	ModalResult := mrOK;
    end;
end;

procedure TfrmAgregarConceptoMovimiento.btnConceptosClick(Sender: TObject);
var
	f : TFormConceptosMovimientos;
begin
    Application.CreateForm(TFormConceptosMovimientos, f);
    if f.Inicializa(VcTipoComprobante.Value) then begin
        f.Visible := False;
        f.ShowModal ;
    end;
    f.Release;
    CargarCombo;
end;

procedure TfrmAgregarConceptoMovimiento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TfrmAgregarConceptoMovimiento.btnCancelarClick(Sender: TObject);
begin
	Close;
end;

{******************************** Function Header ******************************
Function Name: VcTipoComprobanteChange
Author : llazarte
Date Created : 21/02/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAgregarConceptoMovimiento.VcTipoComprobanteChange(
  Sender: TObject);
begin
    // se aplican filtros en el combo de Conceptos de acuerdo al valor seleccionado.
    CargarCombo;
end;


{******************************** Function Header ******************************
Function Name: cbConceptoChange
Author : llazarte
Date Created : 22/02/2006
Description : Se muestra el check de IVA de acuerdo al campo AfectoIVA cargado en el ABM.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TfrmAgregarConceptoMovimiento.cbConceptoChange(Sender: TObject);
begin
    // habilito o no el check del IVA - si el Campo AfectoIVA = 1
    if spObtenerConceptosComprobante.Lookup('CodigoConcepto', cbConcepto.Value, 'AfectoIVA') then begin
        chk_ImporteIncluyeIVA.Visible := True;
    end else chk_ImporteIncluyeIVA.Visible := False;
    chk_ImporteIncluyeIVA.Checked := False;
end;

end.
