object FrmPlanesFinanciamientos: TFrmPlanesFinanciamientos
  Left = 258
  Top = 222
  Width = 760
  Height = 488
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Planes de Financiamientos'
  Color = clBtnFace
  Constraints.MinHeight = 440
  Constraints.MinWidth = 570
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 415
    Width = 752
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 555
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 6
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 752
    Height = 200
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'65'#0'C'#243'digo        '
      
        #0'211'#0'Descripci'#243'n                                                ' +
        ' ')
    HScrollBar = True
    RefreshTime = 100
    Table = tblPlanesFinanciamiento
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnProcess = DBList1Process
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 752
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object pDatos: TPanel
    Left = 0
    Top = 233
    Width = 752
    Height = 182
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 10
      Top = 40
      Width = 72
      Height = 13
      Caption = 'Descripci'#243'n:'
      FocusControl = txt_descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 10
      Top = 12
      Width = 39
      Height = 13
      Caption = 'C'#243'digo: '
      FocusControl = txt_Codigo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 285
      Top = 124
      Width = 116
      Height = 13
      Caption = 'Cantidad de Cuotas:'
      FocusControl = neCantidadCuotas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 10
      Top = 124
      Width = 115
      Height = 13
      Caption = 'Tasa de Interes (%):'
      FocusControl = neInteres
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 10
      Top = 153
      Width = 68
      Height = 13
      Caption = 'Frecuencia:'
      FocusControl = cbFactorFrecuencia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 96
      Width = 68
      Height = 13
      Caption = 'Pie M'#237'nimo:'
      FocusControl = nePieMinimo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 285
      Top = 96
      Width = 179
      Height = 13
      Caption = 'Plazo para la 1ra. Cuota (Dias):'
      FocusControl = nePlazoPrimerCuota
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 10
      Top = 68
      Width = 95
      Height = 13
      Caption = 'Tipo pie m'#237'nimo:'
      FocusControl = txt_descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_descripcion: TEdit
      Left = 128
      Top = 36
      Width = 412
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 1
    end
    object txt_Codigo: TNumericEdit
      Left = 128
      Top = 8
      Width = 121
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 4
      TabOrder = 0
      Decimals = 0
    end
    object neInteres: TNumericEdit
      Left = 128
      Top = 120
      Width = 121
      Height = 21
      Color = 16444382
      MaxLength = 5
      TabOrder = 5
      Decimals = 2
    end
    object neCantidadCuotas: TNumericEdit
      Left = 466
      Top = 120
      Width = 73
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 6
      Decimals = 0
    end
    object cbFactorFrecuencia: TComboBox
      Left = 128
      Top = 149
      Width = 121
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 7
      Items.Strings = (
        'Quincenal'
        'Mensual'
        'Bimestral')
    end
    object nePieMinimo: TNumericEdit
      Left = 128
      Top = 92
      Width = 121
      Height = 21
      Color = 16444382
      TabOrder = 3
      Decimals = 2
    end
    object nePlazoPrimerCuota: TNumericEdit
      Left = 466
      Top = 92
      Width = 73
      Height = 21
      Color = 16444382
      MaxLength = 12
      TabOrder = 4
      Decimals = 0
    end
    object cbTiposPieMinimo: TComboBox
      Left = 128
      Top = 64
      Width = 121
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbTiposPieMinimoChange
      Items.Strings = (
        'Porcentual'
        'Absoluto')
    end
  end
  object tblPlanesFinanciamiento: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'PlanesFinanciamiento'
    Left = 107
    Top = 133
  end
  object QryMaxCodigo: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT MAX(CodigoFinanciamiento) AS Codigo'
      'FROM PLanesFinanciamiento')
    Left = 138
    Top = 133
  end
end
