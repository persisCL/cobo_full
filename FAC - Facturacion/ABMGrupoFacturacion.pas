{-----------------------------------------------------------------------------
 Unit Name: ABMGrupoFacturacion
 Author:
 Purpose:
 Description:
 History:

 Revision 1:
  Author:    ggomez
  Date:      14-Feb-2005
  Description: Al aceptar la eliminaci�n de un grupo, si ocurre un error,
    muestro un mensaje de error, si es un EDatabaseError. Este puede ocurrir si
    el grupo est� asociado a alg�n registro de la tabla AgendaFacturacion, en
    este caso se viola el Foreign Key: FK_AgendaFacturacion_GruposFacturacion.


  Revision 2:
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura dentro del DFM

    Etiqueta    :   TASK_051_MGO_20160720
    Descripci�n :   Se elimina transacci�n Delphi
-----------------------------------------------------------------------------}
unit ABMGrupoFacturacion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask, ComCtrls, PeaProcs, validate,
  Dateedit, Util, ADODB, DMConnection, DPSControls;

type
  TFormGruposFacturacion = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    GruposFacturacion: TADOTable;
    Label15: TLabel;
    txt_CodigoGrupo: TNumericEdit;
    qry_MaxGrupo: TADOQuery;
    //Label2: TLabel;						// TASK_132_MGO_20170207
    //cbFrecuenciaFacturacion: TComboBox;	// TASK_132_MGO_20170207
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function DBList1Process(Tabla: TDataSet; var Texto: String): Boolean;
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormGruposFacturacion: TFormGruposFacturacion;

implementation

{$R *.DFM}

function TFormGruposFacturacion.Inicializa: boolean;
Var
	S: TSize;
begin
    Result := False;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([GruposFacturacion]) then exit;
    Notebook.PageIndex := 0;
    DBList1.Reload;
   	Result := True;
end;

procedure TFormGruposFacturacion.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormGruposFacturacion.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	    txt_CodigoGrupo.Value	:= FieldByName('CodigoGrupoFacturacion').AsInteger;
		txt_Descripcion.text	:= Trim(FieldByName('Descripcion').AsString);
        //if FieldByName('FactorFrecuencia').asFloat = 0.5 then cbFrecuenciaFacturacion.ItemIndex := 0          // TASK_132_MGO_20170207
        //else if FieldByName('FactorFrecuencia').asFloat = 1.0 then cbFrecuenciaFacturacion.ItemIndex := 1     // TASK_132_MGO_20170207
        //else if FieldByName('FactorFrecuencia').asFloat = 2.0 then cbFrecuenciaFacturacion.ItemIndex := 2;    // TASK_132_MGO_20170207
	end;
end;

procedure TFormGruposFacturacion.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoGrupoFacturacion').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
        //if tabla.FieldByName('FactorFrecuencia').asFloat = 0.5 then cbFrecuenciaFacturacion.ItemIndex := 0        // TASK_132_MGO_20170207
        //else if tabla.FieldByName('FactorFrecuencia').asFloat = 1.0 then cbFrecuenciaFacturacion.ItemIndex := 1   // TASK_132_MGO_20170207
        //else if tabla.FieldByName('FactorFrecuencia').asFloat = 2.0 then cbFrecuenciaFacturacion.ItemIndex := 2;  // TASK_132_MGO_20170207
	end;
end;

procedure TFormGruposFacturacion.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    txt_CodigoGrupo.Enabled	:= False;
   	txt_CodigoGrupo.color := clBtnFace;
    txt_Descripcion.setFocus;
end;

procedure TFormGruposFacturacion.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormGruposFacturacion.Limpiar_Campos();
begin
	txt_CodigoGrupo.Clear;
	txt_Descripcion.Clear;
end;

procedure TFormGruposFacturacion.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormGruposFacturacion.DBList1Delete(Sender: TObject);
resourcestring
    CAPTION_ELIMINAR_GRUPO_FACTURACION = 'Eliminar Grupo de Facturaci�n';
    MSG_ELIMINAR_GRUPO_FACTURACION = '�Est� seguro de querer eliminar el Grupo de Facturaci�n seleccionado?';
    MSG_ERROR_ELIMINAR_GRUPO_FACTURACION = 'No se pudo eliminar el Grupo de Facturaci�n seleccionado.';
    MSG_ERROR_GRUPO_ASOCIADO_A_CICLOS_FACTURACION = 'Puede ser que el grupo est� asociado a Ciclos de Facturaci�n.';
    MSG_ERROR_O = ' O ';
    MSG_ERROR_GRUPO_ASOCIADO_A_CONVENIO = 'Puede ser que el grupo est� asociado a Convenios.';
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_ELIMINAR_GRUPO_FACTURACION, CAPTION_ELIMINAR_GRUPO_FACTURACION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		except
            on E: EDatabaseError do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_ERROR_ELIMINAR_GRUPO_FACTURACION + CRLF
                    + MSG_ERROR_GRUPO_ASOCIADO_A_CONVENIO + MSG_ERROR_O + CRLF
                    + MSG_ERROR_GRUPO_ASOCIADO_A_CICLOS_FACTURACION, E.Message,
                    CAPTION_ELIMINAR_GRUPO_FACTURACION, MB_ICONSTOP);
            end;
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_ERROR_ELIMINAR_GRUPO_FACTURACION, e.message, CAPTION_ELIMINAR_GRUPO_FACTURACION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormGruposFacturacion.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoGrupo.Enabled := False;
	txt_CodigoGrupo.color := clBtnFace;
	txt_Descripcion.SetFocus;
end;

procedure TFormGruposFacturacion.BtnAceptarClick(Sender: TObject);
resourcestring
    CAPTION_VALIDAR_GRUPO_FACTURACION = 'Validar datos del Grupos de Facturaci�n';
    CAPTION_ACTUALIZAR_GRUPO_FACTURACION = 'Actualizar Grupo de Facturaci�n';
    MSG_DESCRIPCION = 'El grupo de facturaci�n debe contener una descripci�n.';
    MSG_ACTUALIZAR_GRUPO_FACTURACION  = 'No se pudieron actualizar los datos del Grupo de Facturaci�n';
begin
    if not validateControls([txt_Descripcion],
      [trim(txt_Descripcion.Text) <> ''],
      CAPTION_VALIDAR_GRUPO_FACTURACION,
      [MSG_DESCRIPCION]) then exit;
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
        //DMConnections.BaseCAC.BeginTrans;                                     // TASK_051_MGO_20160720
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                qry_MaxGrupo.Open;
                txt_CodigoGrupo.Value := qry_MaxGrupo.FieldByNAme('CodigoGrupoFacturacion').AsInteger + 1;
				qry_MaxGrupo.Close;
			end else begin
            	Edit;
            end;
			FieldByName('CodigoGrupoFacturacion').AsInteger 	:= Trunc(txt_CodigoGrupo.Value);
    		FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
            //if cbFrecuenciaFacturacion.ItemIndex = 0 then FieldByName('FactorFrecuencia').value := 0.5        // TASK_132_MGO_20170207
            //else if cbFrecuenciaFacturacion.ItemIndex = 1 then FieldByName('FactorFrecuencia').value := 1.0   // TASK_132_MGO_20170207
            //else if cbFrecuenciaFacturacion.ItemIndex = 2 then FieldByName('FactorFrecuencia').value := 2.0;  // TASK_132_MGO_20170207
			Post;
            //DMConnections.BaseCAC.CommitTrans;                                // TASK_051_MGO_20160720
		except
			On E: Exception do begin
				Cancel;
                //DMConnections.BaseCAC.RollbackTrans;                          // TASK_051_MGO_20160720
				MsgBoxErr(MSG_ACTUALIZAR_GRUPO_FACTURACION, E.message, CAPTION_ACTUALIZAR_GRUPO_FACTURACION, MB_ICONSTOP);
            end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormGruposFacturacion.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormGruposFacturacion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormGruposFacturacion.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormGruposFacturacion.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
    txt_CodigoGrupo.Enabled := True;
   	txt_CodigoGrupo.color := clWindow;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

function TFormGruposFacturacion.DBList1Process(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Result := True;
end;

end.
