{-----------------------------------------------------------------------------
 File Name: frm_IngresarMovimientos.pas
 Author:    jconcheyro
 Date Created: 20/12/2005
 Language: ES-AR
 Description:   Permite ingresar Movimientos basados en cualquier concepto a las cuentas

 Revision: 1
 Author:    jconcheyro
 Date Created: 31/08/2006
 Language: ES-AR
 Description: Incorpora la carga de Movimientos con discriminacion de IVA

  Revision: 2
 Author:    FSandi
 Date Created: 02/08/2007
 Language: ES-AR
 Description: Corrige la utilizacion de importes para utilizar tipo float en lugar de integer

 Revision 3:
    Author: dAllegretti
    Date: 22/05/2008
    Description:    Se le agreg� el parametro @UsuarioCreacion del Stored spAgregarMovimientoCuentaIVA
                    y la correspondiente asignaci�n en el llamado al stored.


 Revision 4:
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

 Revision 5:
 Author: mbecerra
 Date: 08-Mayo-2009
 Description:	(Ref. Facturaci�n Electr�nica)
            	Se juntan los eventos OnClick de los botones btnAgregarItem y btnModificarItem
                Adem�s, se auto-impone el signo del Item ingresado de acuerdo
                al campo EsDescuentoRecargo de la tabla ConceptosMovimiento

  Firma       :   SS_1120_MVI_20130820
  Descripcion :   Se cambia el llamado a llenar el combo de convenios para que utilice el procedimiento  CargarConveniosRUT.
                  Se cambia estilo del comboBox a: vcsOwnerDrawFixed, se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.
                  
----------------------------------------------------------------------------}
unit frm_IngresarMovimientos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Dateedit, ExtCtrls, utilProc, DmiCtrls, util, DBCtrls,
  ImgList, ADODB, DMConnection, ComCtrls, BuscaClientes, Utildb, DB,
  DPSControls, VariantComboBox, constParametrosGenerales, Validate,
  peaTypes, PeaProcs, dateUtils, ListBoxEx, DBListEx, DBClient,
  frm_CargoNotaCredito, TimeEdit;

type

  TfrmIngresarMovimientos = class(TForm)
    gbDatosConvenio: TGroupBox;
    Label3: TLabel;
    cbConveniosCliente: TVariantComboBox;
    Label7: TLabel;
    dsMovimientosAgregados: TDataSource;
    gbDatosCliente: TGroupBox;
    lNombreCompleto: TLabel;
    lDomicilio: TLabel;
    lComuna: TLabel;
    lRegion: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    gbItemsPendientes: TGroupBox;
    dbMovimientosAgregados: TDBListEx;
    cdsMovimientosAgregados: TClientDataSet;
    Imagenes: TImageList;
    ObtenerCLiente: TADOStoredProc;
    spAgregarMovimientoCuentaIVA: TADOStoredProc;
    spObtenerConveniosCliente: TADOStoredProc;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    btnSalir: TButton;
    btnAgregarItem: TButton;
    btnModificarItem: TButton;
    btnEliminarItem: TButton;
    btn_Continuar: TButton;
    Label2: TLabel;
    Label4: TLabel;
    txtTotal: TEdit;
    GroupBox1: TGroupBox;
    peRUTCliente: TPickEdit;
    Label1: TLabel;
    BtnBuscar: TButton;
    Label8: TLabel;
    txtNumeroConvenio: TEdit;
    BtnLimpiar: TButton;
    procedure BtnLimpiarClick(Sender: TObject);
    procedure BtnBuscarClick(Sender: TObject);
    procedure btn_ContinuarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure txtNumeroConvenioChange(Sender: TObject);
    procedure cdsMovimientosAgregadosAfterScroll(DataSet: TDataSet);
    procedure btnAgregarItemClick(Sender: TObject);
    procedure btnModificarItemClick(Sender: TObject);
    procedure btnEliminarItemClick(Sender: TObject);
    procedure cdsMovimientosAgregadosCalcFields(DataSet: TDataSet);
    procedure Limpiar;
    procedure cbConveniosClienteDrawItem(Control: TWinControl; Index: Integer;                        //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                                           //SS_1120_MVI_20130820
//    procedure tmrConsultaTimer(Sender: TObject);
  private
    { Private declarations }
	FUltimaBusqueda : TBusquedaCliente;
	FCodigoCliente : Integer;
    // Almacena el C�digo de Convenio a quien se le emite el Comprobante.
    FCodigoConvenio: Integer;
    procedure ClearCaptions;
    procedure MostrarDomicilioFacturacion;
	procedure AgregarNuevosMovimientosCuenta;
    procedure TotalizarMovimientos;
    function ObtenerIVA: Double;
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;


implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: inicializar
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Iniciliza el form.
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TfrmIngresarMovimientos.Inicializar: Boolean;
begin
	FCodigoCliente              := -1;
	FCodigoConvenio             := -1;
    cdsMovimientosAgregados.CreateDataSet;
	ClearCaptions;
    Result := True;
    CenterForm(self);
end;


procedure TfrmIngresarMovimientos.btnSalirClick(Sender: TObject);
begin
    Close;
end;


{-----------------------------------------------------------------------------
  Function Name: MostrarDomicilioFacturacion
  Author:    jconcheyro
  Date Created: 20/12/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmIngresarMovimientos.MostrarDomicilioFacturacion;
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        try
        	ExecProc;
    		lDomicilio.Caption	:= ParamByName('@Domicilio').Value;
 			lComuna.Caption		:= ParamByName('@Comuna').Value;
            lRegion.Caption 	:= ParamByName('@Region').Value;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: cbConveniosClienteChange
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Cuando cambia la lista de convenios se refrescan
               la lista de items de la factura a realizar.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarMovimientos.cbConveniosClienteChange(Sender: TObject);
begin
    FCodigoConvenio := cbConveniosCliente.Value;
	MostrarDomicilioFacturacion;
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarMovimientos.cbConveniosClienteDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TfrmIngresarMovimientos.cbConveniosClienteDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConveniosCliente.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                      
end;
//END:SS_1120_MVI_20130820 -----------------------------------------------------

{******************************** Function Header ******************************
Function Name: peRUTClienteButtonClick
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Permite buscar un cliente por medio del form FormBuscaClientes.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarMovimientos.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    Application.createForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) and (F.ShowModal = mrOk) then begin
        FUltimaBusqueda   := F.UltimaBusqueda;
        peRUTCliente.Text :=  F.Persona.NumeroDocumento;
        peRUTCliente.setFocus;
        ClearCaptions;
        cbConveniosCliente.Clear;
    end;
    F.free;
end;


{******************************** Function Header ******************************
Function Name: peRUTClienteChange
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Cuando cambia el RUT del cliente se cargan todos sus datos asociados
               (Lista de convenios, nombre, domicilio y detalle de la factura a realizar).
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarMovimientos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    // Desactivar la Impresora Fiscal
	Action := caFree;
end;

procedure TfrmIngresarMovimientos.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;

procedure TfrmIngresarMovimientos.ClearCaptions;
begin
    lNombreCompleto.Caption	:= '';
    lDomicilio.Caption      := '';
    lComuna.Caption         := '';
    lRegion.Caption 		:= '';
    cdsMovimientosAgregados.EmptyDataSet;
    txtTotal.Clear;
end;

procedure TfrmIngresarMovimientos.txtNumeroConvenioChange(Sender: TObject);
//var
//	Documento : String;
begin
//	if (activeControl = sender) then begin
//		// voy buscando el cliente que tenga este convenio, si existe pongo su RUT en el buscador
//    	Documento := Trim( QueryGetValue (DMConnections.BaseCAC,
//        'SELECT ISNULL (NumeroDocumento, '''') FROM Personas (NOLOCK) WHERE CodigoPersona = (SELECT CodigoCliente FROM Convenio (NOLOCK) WHERE NumeroConvenio = ' +
//        QuotedStr(NumeroConvenio.Text) + ')') );
//		if Documento <> '' then peRUTCliente.setFocus;
//		peRUTCliente.Text := Documento
//	end
end;


{******************************** Function Header ******************************
Function Name: cdsObtenerPendienteFacturacionAfterScroll
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Habilita y deshabilita los botones de modificar y eliminar
Parameters   : DataSet: TDataSet
Return Value : None
*******************************************************************************}
procedure TfrmIngresarMovimientos.cdsMovimientosAgregadosAfterScroll(DataSet: TDataSet);
begin
    // Habilito el bot�n de Modificar si es un item Nuevo y no estoy
    // registrando un comprobante.
	btnModificarItem.Enabled := True;
    btnEliminarItem.Enabled	:= btnModificarItem.Enabled;
end;

{******************************** Function Header ******************************
Function Name: btnAgregarItemClick
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Agrega un item a facturar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarMovimientos.btnAgregarItemClick(Sender: TObject);
const
    Boton_Agregar = 10;
    Boton_Modific = 20;
var
	f : TfrmCargoNotaCredito;
    Exito : boolean;
begin
	if (TButton(Sender).Tag = Boton_Modific) and (cdsMovimientosAgregados.IsEmpty) then Exit;

    Application.CreateForm(TfrmCargoNotaCredito, f);
    f.Visible := False;
    with cdsMovimientosAgregados do begin
    	//REV.5
        if TButton(Sender).Tag = Boton_Agregar then begin
        	Exito := f.Inicializar(True, True, True, '', ObtenerIVA, NowBase(DMConnections.BaseCAC), False);
        end
        else begin
            Exito := f.Inicializar( True, True, True, True, cdsMovimientosAgregados.FieldByName('CodigoConcepto').AsInteger,
                                    cdsMovimientosAgregados.FieldByName('ImporteCtvs').AsFloat,
                                    99999999,
                                    cdsMovimientosAgregados.FieldByName('Observaciones' ).AsString,
                                    ' ',
                                    cdsMovimientosAgregados.FieldByName('TipoComprobante').AsString,
                                    ObtenerIVA,
                                    cdsMovimientosAgregados.FieldByName('FechaHora' ).AsDateTime, False)
        end;

    	if Exito then begin
    		if f.ShowModal = mrOK then begin
            	if TButton(Sender).Tag = 10 then Append
                else Edit;
                
                FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
                FieldByName('FechaHora').AsDateTime     := f.edFecha.Date;
                FieldByName('CodigoConcepto').AsInteger := f.cbConcepto.Value;
                FieldByName('Concepto').AsString        := f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
                FieldByName('Observaciones').AsString   := f.edDetalle.Text;
                //REV.5 FieldByName('DescImporte').AsString     := FormatearImporteConDecimales(DMConnections.BaseCAC,f.edImporte.Value + f.edImporteIVA.Value, True, True, True);
                FieldByName('DescImporte').AsString     := FormatearImporteConDecimales(DMConnections.BaseCAC, f.FSignoMovimiento * ( f.edImporte.Value + f.edImporteIVA.Value), True, True, True);
                //REV.5 FieldByName('ImporteCtvs').AsFloat      := f.edImporte.Value + f.edImporteIVA.Value;
                FieldByName('ImporteCtvs').AsFloat      := f.FSignoMovimiento * (f.edImporte.Value + f.edImporteIVA.Value);
                FieldByName('Nuevo').AsBoolean          := True;
                FieldByName('Cantidad').AsFloat         := 1;
                //REV.5 FieldByName('PorcentajeIVA').AsFloat    := f.edPorcentajeIVA.Value;
                FieldByName('PorcentajeIVA').AsFloat    := f.FSignoMovimiento * f.edPorcentajeIVA.Value;
                //REV.5 FieldByName('ValorNeto').Value          := f.edImporte.Value ;
                FieldByName('ValorNeto').Value          := f.FSignoMovimiento * f.edImporte.Value ;
                //REV.5 FieldByName('ImporteIVA').Value         := f.edImporteIVA.Value;
                FieldByName('ImporteIVA').Value         := f.FSignoMovimiento * f.edImporteIVA.Value;

                FieldByName('TipoComprobante').Value    := f.cbTipoComprobante.Value;
                Post;
                btnModificarItem.Enabled    := not cdsMovimientosAgregados.IsEmpty;
                btnEliminarItem.Enabled     := not cdsMovimientosAgregados.IsEmpty;
                cdsMovimientosAgregadosAfterScroll( cdsMovimientosAgregados );
            end;
        end;
    end;
    f.Free;
    TotalizarMovimientos;
end;

{******************************** Function Header ******************************
Function Name: btnModificarItemClick
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Modifica un item a facturar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarMovimientos.btnModificarItemClick(Sender: TObject);
var
	f : TfrmCargoNotaCredito;
begin
    // Si no hay items no permitir modificar.

//Comentado en REV.5
{    if cdsMovimientosAgregados.IsEmpty then Exit;

    Application.CreateForm(TfrmCargoNotaCredito, f);
    f.Visible := False;
    with cdsMovimientosAgregados do begin
    	if f.Inicializar(True, True, True, True, cdsMovimientosAgregados.FieldByName('CodigoConcepto').AsInteger,
                                    cdsMovimientosAgregados.FieldByName('ImporteCtvs').AsFloat,
                                    99999999,
                                    cdsMovimientosAgregados.FieldByName('Observaciones' ).AsString,
                                    ' ',
                                    cdsMovimientosAgregados.FieldByName('TipoComprobante').AsString,
                                    ObtenerIVA,
                                    cdsMovimientosAgregados.FieldByName('FechaHora' ).AsDateTime, False) then begin
    		if f.ShowModal = mrOK then begin
            	Edit;
                FieldByName( 'FechaHora' ).AsDateTime       := f.edFecha.Date;
                FieldByName( 'CodigoConcepto' ).AsInteger   := f.cbConcepto.Value;
                FieldByName( 'Concepto' ).AsString          := f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
                FieldByName( 'Observaciones' ).AsString     := f.edDetalle.Text;
                FieldByName('DescImporte').AsString         := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value + f.edImporteIVA.Value, True, True, True);
                FieldByName('ImporteCtvs').AsFloat          := f.edImporte.Value + f.edImporteIVA.Value;
                FieldByName('Cantidad').AsFloat             := 1;
                FieldByName('TipoComprobante').Value        := f.cbTipoComprobante.Value;
                FieldByName('PorcentajeIVA').AsFloat        := f.edPorcentajeIVA.Value;
                FieldByName('ValorNeto').Value              := f.edImporte.Value ;
                FieldByName('ImporteIVA').Value             := f.edImporteIVA.Value;
                Post;
            end;
        end;
    end;
    TotalizarMovimientos;
}
end;

{******************************** Function Header ******************************
Function Name: btnEliminarItemClick
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Elimina un item a facturar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarMovimientos.btnEliminarItemClick(Sender: TObject);
resourcestring
	MSG_ITEM_DELETE_CONFIRMATION = 'Desea eliminar el item ?';
begin
    // Si no hay items no permitir eliminar.
    if cdsMovimientosAgregados.IsEmpty then Exit;

	if MsgBox(	MSG_ITEM_DELETE_CONFIRMATION, Caption,
    			MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES then begin
		cdsMovimientosAgregados.Delete;
        TotalizarMovimientos;
    end;
end;


{******************************** Function Header ******************************
Procedure Name: AgregarNuevosMovimientosCuenta
Author:    jconcheyro
Date Created: 20/12/2005
Description  : Agrega los Movimientos de cuenta correspondientes a los
				nuevos items cargados
Parameters   :
Return Value : boolean
*******************************************************************************}
procedure TfrmIngresarMovimientos.AgregarNuevosMovimientosCuenta;
resourcestring
	MSG_ERROR_UPDATING_ITEM = 'Error actualizando Movimientos de Cuenta.';
begin
	with cdsMovimientosAgregados do begin
    	DisableControls;
        try
            First;
            while not EOF do begin
                if ( FieldByName( 'Nuevo' ).AsBoolean ) then
                    with spAgregarMovimientoCuentaIVA, Parameters do begin
                        try
                            Refresh;                                                // TASK_075_MGO_20160930
                            ParamByName('@CodigoConvenio').Value 		:= cdsMovimientosAgregados.FieldByName( 'CodigoConvenio' ).AsInteger;
                            ParamByName('@CodigoConvenioVehiculo').Value:= NULL;    // TASK_075_MGO_20160930
                            ParamByName('@IndiceVehiculo').Value 		:= NULL;
                            ParamByName('@FechaHora').Value				:= cdsMovimientosAgregados.FieldByName( 'FechaHora' ).AsDateTime;
                            ParamByName('@CodigoConcepto').Value		:= cdsMovimientosAgregados.FieldByName( 'CodigoConcepto' ).AsInteger;
                            ParamByName('@Importe' ).Value				:= cdsMovimientosAgregados.FieldByName( 'ImporteCtvs' ).asFloat * 100; //Revision 2
                            ParamByName('@LoteFacturacion' ).Value		:= NULL;
                            ParamByName('@EsPago' ).Value				:= False;
                            ParamByName('@Observaciones' ).Value		:= cdsMovimientosAgregados.FieldByName( 'Observaciones' ).AsString;
                            ParamByName('@NumeroPromocion' ).Value		:= NULL;
                            ParamByName('@NumeroFinanciamiento' ).Value	:= NULL;
                            ParamByName('@NumeroMovimiento' ).Value     := NULL;
                            ParamByName('@ImporteIVA').Value            := cdsMovimientosAgregados.FieldByName('ImporteIVA').asFloat * 100;  //Revision 2
                            ParamByName('@PorcentajeIVA').Value         := cdsMovimientosAgregados.FieldByName('PorcentajeIVA').AsFloat * 100 ;
                            ParamByName('@UsuarioCreacion').Value       := UsuarioSistema;
                            ExecProc;
                            cdsMovimientosAgregados.Edit;
                            cdsMovimientosAgregados.FieldByName( 'NumeroMovimiento' ).Value := ParamByName( '@NumeroMovimiento' ).Value;
                            cdsMovimientosAgregados.Post;
                        except
                            on E : Exception do begin
                                raise Exception.Create(MSG_ERROR_UPDATING_ITEM + CRLF + E.Message);
                            end;
                        end; // except
                end; // with
                Next;
            end; // while
        finally
            EnableControls;
        end; // finally
    end; // with
end;





procedure TfrmIngresarMovimientos.cdsMovimientosAgregadosCalcFields(DataSet: TDataSet);
begin
    cdsMovimientosAgregados.FieldByName('FechaDescrip').AsString := DateToStr(cdsMovimientosAgregados.FieldByName('FechaHora').AsDateTime)
end;

procedure TfrmIngresarMovimientos.btn_ContinuarClick(Sender: TObject);
resourcestring
	MSG_NOFILAS 	= 'No hay movimientos ingresados para cargar';
	MSG_NORUT   	= 'RUT vac�o';
	MSG_NOCONVENIO 	= 'No hay ning�n Convenio seleccionado';
    CAPTION			= 'Ingreso de Movimientos Cuentas';
    MSG_CONFIRMAR	= 'Confirma el ingreso de estos Movimientos?' ;
begin
    if not ValidateControls([peRUTCliente , cbConveniosCliente, dbMovimientosAgregados],
      [ peRUTCliente.Text <> '' , FCodigoConvenio <> 0 ,  cdsMovimientosAgregados.IsEmpty = False],
      CAPTION,
      [MSG_NORUT, MSG_NOCONVENIO, MSG_NOFILAS]) then exit;

    if MsgBox(MSG_CONFIRMAR, CAPTION, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
		AgregarNuevosMovimientosCuenta;
        ClearCaptions;
        peRUTCliente.Clear;
        cbConveniosCliente.Clear;
        txtNumeroConvenio.Clear;
    end;

end;

{******************************** Function Header ******************************
Function Name: TotalizarMovimientos
Author : llazarte
Date Created : 21/02/2006
Description : Totaliza el monto de los movimientos ingresados en la lista.
Se refresca en el alta, modificacion y eliminacion de movimientos.
Parameters : None
Return Value : None
*******************************************************************************}

procedure TfrmIngresarMovimientos.TotalizarMovimientos;
var
    Total: double;
begin
    // totalizo los movimientos ingresados
    cdsMovimientosAgregados.First;
    Total := 0;
    while not cdsMovimientosAgregados.Eof do begin
        Total := Total + cdsMovimientosAgregados.FieldByName('ImporteCtvs').AsFloat;
        cdsMovimientosAgregados.Next;
    end;
    txtTotal.Text := FormatearImporteConDecimales(DMConnections.BaseCAC, Total, True, True, True);
end;

{******************************** Function Header ******************************
Function Name: BtnBuscarClick
Author : llazarte
Date Created : 27/02/2006
Description : se cambi� la b�squeda a un bot�n, porque en el change no quedaba claro, cuando no encontraba
ningun resultado para la busqueda, no se daba ningun mensaje al usuario.
Parameters : Sender: TObject
Return Value : None

Revision 1:
    Date: 26/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

*******************************************************************************}

procedure TfrmIngresarMovimientos.BtnBuscarClick(Sender: TObject);
resourcestring
    MSG_RESULTADO_BUSQUEDA = 'No se han encontrado datos para el convenio solicitado';
    TXT_ATENCION           = 'Atenci�n';
var
	Documento: String;
    CodigoConvenio: Variant;

begin
    // Me fijo si tengo que buscar por convenio: Hace este filtro primero - Para enviar al procedure el RUT
    if (trim(txtNumeroConvenio.Text) <> '') and (trim(peRUTCliente.Text) = '') then begin
		// voy buscando el cliente que tenga este convenio, si existe pongo su RUT en el buscador
    	Documento := Trim( QueryGetValue (DMConnections.BaseCAC,
        'SELECT ISNULL (NumeroDocumento, '''') FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = (SELECT CodigoCliente FROM Convenio  WITH (NOLOCK) WHERE NumeroConvenio = ' +
        QuotedStr(txtNumeroConvenio.Text) + ')') );
		if Documento <> '' then begin
            peRUTCliente.setFocus;
    		peRUTCliente.Text := Documento;
        end else begin
            MsgBox(MSG_RESULTADO_BUSQUEDA, TXT_ATENCION, MB_ICONINFORMATION);
            peRUTCliente.Clear;
            cbConveniosCliente.Clear;
            ClearCaptions;
            btnAgregarItem.Enabled   := False;
            btnModificarItem.Enabled := False;
            btnEliminarItem.Enabled  := False;
            Exit;
        end;
	end;
    // Ahora busco los datos del cliente - Se ejecuta si no se ingreso un Convenio, o se ingres� un Convenio correcto
	Screen.Cursor := crHourglass;
    try
        ClearCaptions;
        cbConveniosCliente.Items.Clear;
        // Obtenemos el cliente seleccionado
        with ObtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value   := NULL;
            Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').Value := iif(peRUTCLiente.Text <> EmptyStr,PadL(Trim(peRUTCLiente.Text), 9, '0'), EmptyStr);
            Open;
        end;

        // Obtenemos la lista de convenios para el cliente seleccionado
    //    CodigoConvenio := Null;                                                                                                                  	//SS_1120_MVI_20130820
    //    With spObtenerConveniosCliente do begin                                                                                                  	//SS_1120_MVI_20130820
    //        Parameters.ParamByName('@CodigoCliente').Value          := ObtenerCliente.fieldbyname('CodigoCliente').AsInteger;                    	//SS_1120_MVI_20130820
		//	Parameters.ParamByName('@IncluirConveniosDeBaja').Value := True;                                                                       	//SS_1120_MVI_20130820
    //        Open;                                                                                                                                	//SS_1120_MVI_20130820
    //        First;                                                                                                                               	//SS_1120_MVI_20130820
    //        while not spObtenerConveniosCliente.Eof do begin                                                                                     	//SS_1120_MVI_20130820
    //            cbConveniosCliente.Items.Add(spObtenerConveniosCliente.fieldbyname('NumeroConvenioFormateado').AsString,                         	//SS_1120_MVI_20130820
    //              trim(spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString));                                                       	//SS_1120_MVI_20130820
    //            if Trim(spObtenerConveniosCliente.fieldbyname('NumeroConvenio').AsString) = Trim(txtNumeroConvenio.Text) then                    	//SS_1120_MVI_20130820
    //                CodigoConvenio := spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString;                                          	//SS_1120_MVI_20130820
    //            spObtenerConveniosCliente.Next;                                                                                                  	//SS_1120_MVI_20130820
    //        end;                                                                                                                                 	//SS_1120_MVI_20130820
    //        Close;                                                                                                                               	//SS_1120_MVI_20130820
    //    end;                                                                                                                                     	//SS_1120_MVI_20130820
                                                                                                                                                   	//SS_1120_MVI_20130820
            CargarConveniosRUT(DMCOnnections.BaseCAC,cbConveniosCliente, 'RUT',                                         							//SS_1120_MVI_20130820
            PadL(peRUTCLiente.Text, 9, '0' ));                                                                          							//SS_1120_MVI_20130820
                                                                                                                                                   	//SS_1120_MVI_20130820
        // Si no se recuperan convenios es porque ocurri� un inconveniente al
        // retornarlos, ya que un cliente siempre tiene convenios.
    //    if (CodigoConvenio <> null ) then cbConveniosCliente.Value := CodigoConvenio                                    							//SS_1120_MVI_20130820
    //    if  cbConveniosCliente.Items.Count > 0  then cbConveniosCliente.Value := CodigoConvenio                        							//SS_1120_MVI_20130820
    //    else begin                                                                                                     							//SS_1120_MVI_20130820
       if  cbConveniosCliente.Items.Count = 0  then begin                                                                							//SS_1120_MVI_20130820
                                                                                                                         							//SS_1120_MVI_20130820
            if trim(txtNumeroConvenio.Text) <> '' then begin
                MsgBox(MSG_RESULTADO_BUSQUEDA, TXT_ATENCION, MB_ICONINFORMATION);
                cbConveniosCliente.Clear;
                ClearCaptions;
                btnAgregarItem.Enabled := False;
                btnModificarItem.Enabled := False;
                btnEliminarItem.Enabled  := False;
                Exit;
            end else cbConveniosCliente.ItemIndex := 0;
        end;

        cbConveniosClienteChange(cbConveniosCliente);
        with ObtenerCliente do begin
            FCodigoCliente := FieldByName('CodigoCliente').AsInteger;
            // Mostramos el nombre completo del cliente
            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString), Trim(FieldByName('Nombre').asString))
            else
                lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString));

            btnAgregarItem.Enabled := ( cbConveniosCliente.Items.Count > 0 ) ;
        end;
        if (cbConveniosCliente.Items.Count = 0) then begin
            MsgBox(MSG_RESULTADO_BUSQUEDA, TXT_ATENCION, MB_ICONINFORMATION);
            btnAgregarItem.Enabled := False;
            btnModificarItem.Enabled := False;
            btnEliminarItem.Enabled  := False;
        end;

    finally
		Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnLimpiarClick
Author : llazarte
Date Created : 01/03/2006
Description :  Limpia los campos para una nueva busqueda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TfrmIngresarMovimientos.BtnLimpiarClick(Sender: TObject);
begin
    Limpiar;
end;

procedure TfrmIngresarMovimientos.Limpiar;
begin
    peRUTCliente.Clear;
    txtNumeroConvenio.Clear;
    cbConveniosCliente.Clear;
    ClearCaptions;
    btnAgregarItem.Enabled := False;
    btnModificarItem.Enabled := False;
    btnEliminarItem.Enabled  := False;
end;

function TfrmIngresarMovimientos.ObtenerIVA: Double;
begin
    //se usa now porque la emision de los creditos en la base es getdate
    Result := QueryGetValueInt(DMConnections.BaseCAC,
                                'SELECT dbo.ObtenerIVA(''' + FormatDateTime('yyyymmdd',
                                NowBase(spAgregarMovimientoCuentaIVA.Connection)) + ''')');
    Result := Result / 100;       // esto devuelve un 0.19 porque en la base dice 1900

end;



end.



