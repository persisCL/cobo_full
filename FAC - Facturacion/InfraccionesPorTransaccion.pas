unit InfraccionesPorTransaccion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, Grids,
  validate, Buttons, ExtCtrls, utilProc, util, ADODB, DBGrids, DB, ImgList, ComCtrls, DmiCtrls, DMConnection,
  BuscaClientes, navigator, DPSGrid, peaProcs, DBClient, UtilDB, ImgProcs, ImgTypes, peaTypes, DPSControls, ListBoxEx,
  DBListEx, UtilFacturacion, DateEdit, JPEGPlus, Filtros, ImagePlus, Menus, frmReporteInfracciones,
  constParametrosGenerales;

type
  TFrmTransitosInfractores = class(TForm)
    Panel1: TPanel;
    gbViajes: TGroupBox;
    gpBusqueda: TGroupBox;
    Imagenes: TImageList;
    ObtenerTransitosAnomalosInfraccionJuzgado: TADOStoredProc;
	dsTransitos: TDataSource;
    ProgressBar: TProgressBar;
    txtFechaInicio: TDateEdit;
    txtFechaFin: TDateEdit;
    Label1: TLabel;
    Label2: TLabel;
    dbgTransitos: TDPSGrid;
    ActualizarInfraccionesTransitos: TADOStoredProc;
    StatusBar: TStatusBar;
    Label3: TLabel;
    Label4: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    imgPatente: TImagePlus;
    dblDetalle: TDBListEx;
    dsDetalle: TDataSource;
    ObtenerTransitosAnomalosInfraccion: TADOStoredProc;
    Label5: TLabel;
    ImagenVehiculo: TImagePlus;
    PopupMenu: TPopupMenu;
    mnu_Aceptar: TMenuItem;
    mnu_Rechazar: TMenuItem;
    mnu_SinEstado: TMenuItem;
    cdsObtenerTransitosAnomalosInfraccionJuzgado: TClientDataSet;
    ObtenerTransitosAnomalosInfraccionJuzgadoCodigoJuzgado: TIntegerField;
    ObtenerTransitosAnomalosInfraccionJuzgadoFecha: TDateTimeField;
    ObtenerTransitosAnomalosInfraccionJuzgadoJuzgado: TStringField;
    ObtenerTransitosAnomalosInfraccionCodigoConcesionaria: TWordField;
    ObtenerTransitosAnomalosInfraccionNumeroViaje: TIntegerField;
    ObtenerTransitosAnomalosInfraccionFechaHora: TDateTimeField;
    ObtenerTransitosAnomalosInfraccionHora: TStringField;
    ObtenerTransitosAnomalosInfraccionNumeroPuntoCobro: TWordField;
    ObtenerTransitosAnomalosInfraccionDescriPuntosCobro: TStringField;
    ObtenerTransitosAnomalosInfraccionCategoria: TStringField;
    ObtenerTransitosAnomalosInfraccionJuzgadoPatenteValidada: TStringField;
    ObtenerTransitosAnomalosInfraccionPatenteValidada: TStringField;
    btnSalir: TButton;
    btnMarcarTodo: TButton;
    btnInvertirSeleccion: TButton;
    btnFiltrar: TButton;
    btnGenerarInfracciones: TButton;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnSalirClick(Sender: TObject);
	procedure btnGenerarInfraccionesClick(Sender: TObject);
	procedure dsTransitosDataChange(Sender: TObject; Field: TField);
    procedure StatusBarDrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnMarcarTodoClick(Sender: TObject);
    procedure btnInvertirSeleccionClick(Sender: TObject);
    procedure CargarImagen;
    procedure GetImagePlate(aGraphic: TGraphic; TipoImagen: TTipoImagen);
    procedure dbgTransitosKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dblDetalleClick(Sender: TObject);
    procedure dblDetalleKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SeleccionarItem;
    procedure dbgTransitosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure mnu_AceptarClick(Sender: TObject);
    procedure mnu_RechazarClick(Sender: TObject);
    procedure mnu_SinEstadoClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
  private
    FCantidadInfraccionesSeleccionadas: integer;
    FPositionPlate: Array[tiFrontal..tiPosterior2] of TRect;
  public
	{ Public declarations }
	function inicializa(): Boolean;
  end;

implementation

{$R *.dfm}

{ TfrmAnularReimprimirFactura }

function TFrmTransitosInfractores.inicializa(): Boolean;
resourcestring
    MSG_TODOS_USUARIOS       = 'Todos los usuarios NO Registrados';
    MSG_TODAS_PREPAGAS       = 'Todas las infracciones de cuentas prepagas';
    MSG_TODAS_INFRACCIONES   = 'Todas las infracciones';
    MSG_COL_TITLE_ITE_INFRACCION = 'Monto Infracci�n';
    MSG_COL_TITLE_MOTIVO_INFRACCION = 'Motivo de Infracci�n';
begin
    progressBar.parent := statusBar;

    FCantidadInfraccionesSeleccionadas := 0;

    btnGenerarInfracciones.Enabled := FCantidadInfraccionesSeleccionadas > 0;

    Width  := GetFormClientSize(Application.MainForm).cx;
	Height := GetFormClientSize(Application.MainForm).cy;
	CenterForm(Self);
	update;

    txtFechaFin.Date :=  NowBase(DMConnections.BaseCAC);

	Result := True;
end;

procedure TFrmTransitosInfractores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFrmTransitosInfractores.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFrmTransitosInfractores.btnGenerarInfraccionesClick(
  Sender: TObject);
resourcestring
    CAPTION_GENERAR_INFRACCIONES = 'Generar Infracciones';
    MSG_ERROR_GENERAR_INFRACCIONES = 'No se pudieron generar las infracciones.';
    MSG_GENERAR_INFRACCIONES = '�Est� seguro de generar infracciones de los transitos seleccionados?';
var
    Primero: boolean;
    PrimerCodigoInfraccion, UltimoCodigoInfraccion: longint;
    f: TFormReporteInfracciones;
begin
	if MsgBox(MSG_GENERAR_INFRACCIONES, CAPTION_GENERAR_INFRACCIONES, MB_YESNO + MB_ICONQUESTION) = IDYES then begin

        Primero := True;
        PrimerCodigoInfraccion := -1;
        UltimoCodigoInfraccion := -1;
        try
            if cdsObtenerTransitosAnomalosInfraccionJuzgado.RecordCount >0 then begin
                screen.cursor := crHourGlass;
                progressBar.max := cdsObtenerTransitosAnomalosInfraccionJuzgado.RecordCount - 1;
                cdsObtenerTransitosAnomalosInfraccionJuzgado.DisableControls;
                with ActualizarInfraccionesTransitos.Parameters do begin
                    cdsObtenerTransitosAnomalosInfraccionJuzgado.First;
                    while not cdsObtenerTransitosAnomalosInfraccionJuzgado.Eof do begin
                        try
                            if Trim(cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString) <> '' then begin
                                ActualizarInfraccionesTransitos.Close;
                                ParamByName('@Patente').Value := iif(trim(cdsObtenerTransitosAnomalosInfraccionJuzgado.fieldbyname('Patente').AsString) = '', null, trim(cdsObtenerTransitosAnomalosInfraccionJuzgado.fieldbyname('Patente').AsString));
                                ParamByName('@Fecha').Value := cdsObtenerTransitosAnomalosInfraccionJuzgado.fieldbyname('Fecha').Value;
                                ParamByName('@Juzgado').Value := cdsObtenerTransitosAnomalosInfraccionJuzgado.fieldbyname('CodigoJuzgado').Value;
                                ParamByName('@Estado').Value := cdsObtenerTransitosAnomalosInfraccionJuzgado.fieldbyname('Estado').Value;
                                ParamByName('@CodigoUsuario').Value := UsuarioSistema;

                                ActualizarInfraccionesTransitos.ExecProc;
                                if Primero then begin
                                    PrimerCodigoInfraccion := ParamByName('@CodigoInfraccion').Value;
                                    Primero := False;
                                end;
                                UltimoCodigoInfraccion := ParamByName('@CodigoInfraccion').Value;
                            end;
                        except
                            On E: Exception do begin
                                MsgBoxErr(MSG_ERROR_GENERAR_INFRACCIONES, E.message, CAPTION_GENERAR_INFRACCIONES, MB_ICONSTOP);
                                Exit;
                            end;
                        end;
                        cdsObtenerTransitosAnomalosInfraccionJuzgado.Next;
                        progressBar.StepIt;
                    end;
                end;
            end;
        finally
            screen.cursor := crDefault;
            cdsObtenerTransitosAnomalosInfraccionJuzgado.EnableControls;
            progressBar.Position := 0;

            if not Primero then begin
                Application.CreateForm(TFormReporteInfracciones, f);
                f.Execute(PrimerCodigoInfraccion, UltimoCodigoInfraccion, 'A', True, 'Informe de Infracciones');
                f.Release;

                Application.CreateForm(TFormReporteInfracciones, f);
                f.Execute(PrimerCodigoInfraccion, UltimoCodigoInfraccion, 'R', True, 'Informe de Infracciones Rechazadas');
                f.Release;
            end;

            btnFiltrarClick(Sender);
            SeleccionarItem;
        end;
    end;
end;

procedure TFrmTransitosInfractores.dsTransitosDataChange(Sender: TObject;
  Field: TField);
begin
(*
	image.LoadImages((Sender as TDataSource).DataSet.FieldByName('CodigoConcesionaria').AsInteger,
	  (Sender as TDataSource).DataSet.FieldByName('NumeroPuntoCobro').AsInteger,
	  (Sender as TDataSource).DataSet.FieldByName('NumeroViaje').AsInteger,
	  etValidado, (Sender as TDataSource).DataSet.FieldByName('FechaTransito').AsDateTime);
*)
end;

procedure TFrmTransitosInfractores.StatusBarDrawPanel(
  StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
begin
	Panel.Width := statusBar.width - 422;
	progressBar.BoundsRect := Classes.Rect(Rect.left - 1, Rect.top - 1, Rect.Left + Panel.Width - 1, Rect.bottom + 1);
end;

procedure TFrmTransitosInfractores.btnFiltrarClick(Sender: TObject);
resourcestring
    MSG_VALIDAR_FECHA_INICIO = 'Debe ingresar la fecha de inicio.';
    MSG_VALIDAR_FECHA_FIN = 'Debe ingresar la fecha de fin.';
begin
    if txtFechaInicio.Date = nulldate then begin
        MsgBoxBalloon(MSG_VALIDAR_FECHA_INICIO, caption, MB_ICONSTOP, txtFechaInicio);
        txtFechaInicio.SetFocus;
        Exit;
    end;
    if txtFechaFin.Date = nulldate then begin
        MsgBoxBalloon(MSG_VALIDAR_FECHA_FIN, caption, MB_ICONSTOP, txtFechaFin);
        txtFechaFin.SetFocus;
        Exit;
    end;

    ObtenerTransitosAnomalosInfraccionJuzgado.Close;
    ObtenerTransitosAnomalosInfraccionJuzgado.Parameters.ParamByName('@FechaInicio').Value :=  iif(txtFechaInicio.Date = nulldate, null, txtFechaInicio.Date);
    ObtenerTransitosAnomalosInfraccionJuzgado.Parameters.ParamByName('@FechaFin').Value :=  iif(txtFechaFin.Date = nulldate, null, txtFechaFin.Date);
    ObtenerTransitosAnomalosInfraccionJuzgado.Open;

    cdsObtenerTransitosAnomalosInfraccionJuzgado.Close;
    cdsObtenerTransitosAnomalosInfraccionJuzgado.CreateDataSet;
    cdsObtenerTransitosAnomalosInfraccionJuzgado.EmptyDataSet;

    ObtenerTransitosAnomalosInfraccion.Close;

    if ObtenerTransitosAnomalosInfraccionJuzgado.RecordCount > 0 then begin
        ObtenerTransitosAnomalosInfraccionJuzgado.First;
        while not ObtenerTransitosAnomalosInfraccionJuzgado.Eof do begin
            cdsObtenerTransitosAnomalosInfraccionJuzgado.AppendRecord([ObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('PatenteValidada').AsString,
                                ObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Fecha').AsDateTime,
                                ObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Juzgado').AsString,
                                '',
                                ObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('CodigoJuzgado').AsString]);

            ObtenerTransitosAnomalosInfraccionJuzgado.Next;
        end;
    end;

    cdsObtenerTransitosAnomalosInfraccionJuzgado.First;


    FCantidadInfraccionesSeleccionadas := 0;
    btnGenerarInfracciones.Enabled := FCantidadInfraccionesSeleccionadas > 0;
  	StatusBar.Panels[0].text := format('Infracciones seleccionadas: %d', [FCantidadInfraccionesSeleccionadas]);

    imgPatente.Picture.Assign(nil);
    ImagenVehiculo.Picture.Assign(nil);
end;

procedure TFrmTransitosInfractores.btnMarcarTodoClick(Sender: TObject);
begin
    dbgTransitos.MarcarTodo;
end;

procedure TFrmTransitosInfractores.btnInvertirSeleccionClick(
  Sender: TObject);
begin
	dbgTransitos.InvertirMarcas;
end;

procedure TFrmTransitosInfractores.CargarImagen;
{var
    imagen: TJPEGPlusImage;}
begin
    imgPatente.Picture.Assign(nil);
    ImagenVehiculo.Picture.Assign(nil);

//  imagen := TJPEGPlusImage.Create;
{   try
   if ObtenerImagenTransito(DMConnections.BaseCAC,
    							ObtenerTransitosAnomalosInfraccion.FieldbyName('CodigoConcesionaria').AsInteger,
                                ObtenerTransitosAnomalosInfraccion.FieldbyName('NumeroPuntoCobro').AsInteger,
                                ObtenerTransitosAnomalosInfraccion.FieldbyName('NumeroViaje').AsInteger,
                                ObtenerTransitosAnomalosInfraccion.FieldbyName('FechaHora').AsDateTime,
                                tiFrontal, imagen, DataImage, DescriError, Error) then begin

            FPositionPlate[tifrontal] := Rect(DataImage.DataImageVR.UpperLeftLPN.X,
              DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.LowerRigthLPN.X,
              DataImage.DataImageVR.LowerRigthLPN.Y);

            GetImagePlate(imagen, tiFrontal);
        end
        else begin
            if ObtenerImagenTransito(DMConnections.BaseCAC,
                                    ObtenerTransitosAnomalosInfraccion.FieldbyName('CodigoConcesionaria').AsInteger,
                                    ObtenerTransitosAnomalosInfraccion.FieldbyName('NumeroPuntoCobro').AsInteger,
                                    ObtenerTransitosAnomalosInfraccion.FieldbyName('NumeroViaje').AsInteger,
                                    ObtenerTransitosAnomalosInfraccion.FieldbyName('FechaHora').AsDateTime,
                                    tiFrontal2, imagen, DataImage, DescriError, Error) then begin

                FPositionPlate[tifrontal] := Rect(DataImage.DataImageVR.UpperLeftLPN.X,
                  DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.LowerRigthLPN.X,
                  DataImage.DataImageVR.LowerRigthLPN.Y);

                GetImagePlate(imagen, tiFrontal2);
            end
            else begin
                imgPatente.Picture.Assign(nil);
                ImagenVehiculo.Picture.Assign(nil);
            end;
        end;
    finally
        imagen.free;
    end;}
end;

procedure TFrmTransitosInfractores.GetImagePlate(aGraphic: TGraphic; TipoImagen: TTipoImagen);
resourcestring
    CAPTION_POS_PATENTE = 'Posici�n de Patente Desconocida';
var
    bmpSource, bmpDest: TBitMap;

begin
    bmpSource := TBitMap.create;
    bmpDest := TBitMap.create;

    Screen.Cursor := crHourGlass;

    try
        if ((FPositionPlate[TipoImagen].Left = 0) and (FPositionPlate[TipoImagen].Right = 0)) then begin
            imgPatente.Picture.Bitmap.transparent := true;
        end else begin
            bmpSource.Assign(aGraphic);
            DrawMask(bmpSource, FPositionPlate[TipoImagen].top - 6);
            ImagenVehiculo.Picture.Assign(bmpSource);

            bmpDest.Width := imgPatente.Width;
            bmpDest.Height := imgPatente.Height;
            bmpDest.Canvas.CopyRect(Rect(0, 0, imgPatente.Width, imgPatente.Height), bmpSource.Canvas,
                Rect(FPositionPlate[TipoImagen].Left - 5, FPositionPlate[TipoImagen].top - 5,
                FPositionPlate[TipoImagen].right + 5, FPositionPlate[TipoImagen].Bottom + 5));
            imgPatente.Picture.Bitmap.transparent := false;
            imgPatente.Picture.Assign(bmpDest);
        end;
    finally
        Screen.Cursor := crDefault;
        bmpSource.free;
        bmpDest.free;
    end;
end;


procedure TFrmTransitosInfractores.dbgTransitosKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    SeleccionarItem;
end;

procedure TFrmTransitosInfractores.dblDetalleClick(Sender: TObject);
begin
    if ((ObtenerTransitosAnomalosInfraccion.Active) and (ObtenerTransitosAnomalosInfraccion.RecordCount > 0) and (dbgTransitos.SelectedRows.Count > 0)) then begin
        CargarImagen;
    end
    else begin
        imgPatente.Picture.Assign(nil);
        ImagenVehiculo.Picture.Assign(nil);
    end;

end;

procedure TFrmTransitosInfractores.dblDetalleKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    dblDetalleClick(nil);
end;

procedure TFrmTransitosInfractores.SeleccionarItem;
begin
    ObtenerTransitosAnomalosInfraccion.Close;
    if ((cdsObtenerTransitosAnomalosInfraccionJuzgado.RecordCount > 0) and (dbgTransitos.SelectedRows.Count > 0)) then begin
        ObtenerTransitosAnomalosInfraccion.Parameters.ParamByName('@Fecha').Value := cdsObtenerTransitosAnomalosInfraccionJuzgado.fieldbyname('Fecha').AsDateTime;
        ObtenerTransitosAnomalosInfraccion.Parameters.ParamByName('@Patente').Value := Trim(cdsObtenerTransitosAnomalosInfraccionJuzgado.fieldbyname('Patente').AsString);
        ObtenerTransitosAnomalosInfraccion.Open;
        dblDetalleClick(nil);
    end;
end;

procedure TFrmTransitosInfractores.dbgTransitosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if ((cdsObtenerTransitosAnomalosInfraccionJuzgado.Active) and (cdsObtenerTransitosAnomalosInfraccionJuzgado.RecordCount > 0)) then begin
        if Button = mbRight then begin
            PopupMenu.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
        end
        else SeleccionarItem;
    end;
end;

procedure TFrmTransitosInfractores.mnu_AceptarClick(Sender: TObject);
var
    i: integer;
begin
    if dbgTransitos.SelectedRows.Count>0 then begin
        screen.cursor := crHourGlass;
        dbgTransitos.DataSource.DataSet.DisableControls;

        with dbgTransitos.DataSource.DataSet do begin
            for i:=0 to dbgTransitos.SelectedRows.Count-1 do begin
                GotoBookmark(pointer(dbgTransitos.SelectedRows.Items[i]));
                if trim(dbgTransitos.DataSource.DataSet.FieldByName('Estado').AsString) = '' then
                    FCantidadInfraccionesSeleccionadas := FCantidadInfraccionesSeleccionadas + 1;
                cdsObtenerTransitosAnomalosInfraccionJuzgado.Edit;
                cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString := 'A';
                cdsObtenerTransitosAnomalosInfraccionJuzgado.UpdateRecord;
                dbgTransitos.Refresh;
            end;
        end;
        screen.cursor := crDefault;
        dbgTransitos.DataSource.DataSet.EnableControls;
        btnGenerarInfracciones.Enabled := FCantidadInfraccionesSeleccionadas > 0;
    	StatusBar.Panels[0].text := format('Infracciones seleccionadas: %d', [FCantidadInfraccionesSeleccionadas]);
    end;
end;

procedure TFrmTransitosInfractores.mnu_RechazarClick(Sender: TObject);
var
    i: integer;
begin
    if dbgTransitos.SelectedRows.Count>0 then begin
        screen.cursor := crHourGlass;

        dbgTransitos.DataSource.DataSet.DisableControls;
        with dbgTransitos.DataSource.DataSet do begin
            for i:=0 to dbgTransitos.SelectedRows.Count-1 do begin
                GotoBookmark(pointer(dbgTransitos.SelectedRows.Items[i]));

                if trim(dbgTransitos.DataSource.DataSet.FieldByName('Estado').AsString) = '' then
                    FCantidadInfraccionesSeleccionadas := FCantidadInfraccionesSeleccionadas + 1;

                cdsObtenerTransitosAnomalosInfraccionJuzgado.Edit;
                cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString := 'R';
                cdsObtenerTransitosAnomalosInfraccionJuzgado.UpdateRecord;
                dbgTransitos.Refresh;
            end;
        end;
        screen.cursor := crDefault;
        dbgTransitos.DataSource.DataSet.EnableControls;
        btnGenerarInfracciones.Enabled := FCantidadInfraccionesSeleccionadas > 0;
    	StatusBar.Panels[0].text := format('Infracciones seleccionadas: %d', [FCantidadInfraccionesSeleccionadas]);
    end;
end;


procedure TFrmTransitosInfractores.mnu_SinEstadoClick(Sender: TObject);
var
    i: integer;
begin
    if dbgTransitos.SelectedRows.Count>0 then begin
        screen.cursor := crHourGlass;
        dbgTransitos.DataSource.DataSet.DisableControls;
        with dbgTransitos.DataSource.DataSet do begin
            for i:=0 to dbgTransitos.SelectedRows.Count-1 do begin
                GotoBookmark(pointer(dbgTransitos.SelectedRows.Items[i]));
                if (trim(cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString) <> '') then
                    FCantidadInfraccionesSeleccionadas := FCantidadInfraccionesSeleccionadas - 1;
                cdsObtenerTransitosAnomalosInfraccionJuzgado.Edit;
                cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString := '';
                cdsObtenerTransitosAnomalosInfraccionJuzgado.UpdateRecord;
            end;
        end;
        screen.cursor := crDefault;
        dbgTransitos.DataSource.DataSet.EnableControls;
        btnGenerarInfracciones.Enabled := FCantidadInfraccionesSeleccionadas > 0;
    	StatusBar.Panels[0].text := format('Infracciones seleccionadas: %d', [FCantidadInfraccionesSeleccionadas]);
    end;
end;

procedure TFrmTransitosInfractores.PopupMenuPopup(Sender: TObject);
begin
    if dbgTransitos.SelectedRows.Count = 1 then begin
        mnu_Aceptar.Enabled := not (cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString = 'A');
        mnu_Rechazar.Enabled := not (cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString = 'R');
        mnu_SinEstado.Enabled := not (cdsObtenerTransitosAnomalosInfraccionJuzgado.FieldByName('Estado').AsString = '');
    end
    else begin
        mnu_Aceptar.Enabled := True;
        mnu_Rechazar.Enabled := True;
        mnu_SinEstado.Enabled := True;
    end;
end;

end.
