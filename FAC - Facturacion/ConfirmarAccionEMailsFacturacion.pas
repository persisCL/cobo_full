unit ConfirmarAccionEMailsFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DMConnection, ExtCtrls, UtilDB;

type
  TfrmAccionesEmail = class(TForm)
    Label1: TLabel;
    lblAtencion: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    rgOpciones: TRadioGroup;
    procedure btnAceptarClick(Sender: TObject);
  private
    FNumeroProceso: Integer;
    procedure EliminarTodos;
    procedure EliminarNoEnviados;
  public
    function Inicializar(NumeroProcesoFacturacion: Integer): Boolean;
  end;

var
  frmAccionesEmail: TfrmAccionesEmail;

implementation

{$R *.dfm}

{ TfrmAccionesEmail }

function TfrmAccionesEmail.Inicializar(NumeroProcesoFacturacion: Integer): Boolean;
begin
    FNumeroProceso := NumeroProcesoFacturacion;
    lblAtencion.Caption := Format(lblAtencion.Caption, [FNumeroProceso]);
    Result := True;
end;

procedure TfrmAccionesEmail.btnAceptarClick(Sender: TObject);
begin
    case rgOpciones.ItemIndex of
        0: EliminarTodos;
        1: EliminarNoEnviados;
    end;

end;

procedure TfrmAccionesEmail.EliminarNoEnviados;
begin
   QueryExecute(DMConnections.BaseCAC,
     'DELETE FROM EMAIL_MENSAJES WHERE ENVIADO = 0 AND ' +
     'NUMEROPROCESOFACTURACION = ' + IntToStr(FNumeroProceso));
end;

procedure TfrmAccionesEmail.EliminarTodos;
begin
   QueryExecute(DMConnections.BaseCAC, 'DELETE FROM EMAIL_MENSAJES ' +
     'WHERE NUMEROPROCESOFACTURACION = ' + IntToStr(FNumeroProceso));
end;

end.

