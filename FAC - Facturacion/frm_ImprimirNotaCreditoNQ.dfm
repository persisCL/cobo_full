object form_ImprimirNotaCreditoNQ: Tform_ImprimirNotaCreditoNQ
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Imprimir Nota de Cr'#233'dito a Nota de Cobro Directa'
  ClientHeight = 433
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    502
    433)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 112
    Width = 351
    Height = 13
    Caption = 
      'Seleccione la Nota de Cr'#233'dito a Nota de Cobro Directa que desea ' +
      'imprimir'
  end
  object Label2: TLabel
    Left = 8
    Top = 87
    Width = 98
    Height = 13
    Caption = 'Proveedor DayPass:'
  end
  object Label3: TLabel
    Left = 272
    Top = 11
    Width = 34
    Height = 13
    Caption = 'Desde:'
  end
  object Label4: TLabel
    Left = 275
    Top = 56
    Width = 32
    Height = 13
    Caption = 'Hasta:'
  end
  object dbl_NotasCobro: TDBListEx
    Left = 8
    Top = 128
    Width = 486
    Height = 266
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 95
        Header.Caption = 'Tipo Compr.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'TipoComprobante'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 185
        Header.Caption = 'N'#186' Comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'NumeroComprobante'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Emisi'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = OrdenarPorColumna
        FieldName = 'FechaEmision'
      end>
    DataSource = ds_ObtenerNotasCreditoNQ
    DragReorder = True
    ParentColor = False
    TabOrder = 6
    TabStop = True
  end
  object btn_ImprimirNotaCredito: TButton
    Left = 225
    Top = 401
    Width = 131
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Imprimir Nota de Cr'#233'dito'
    Default = True
    Enabled = False
    TabOrder = 7
    OnClick = btn_ImprimirNotaCreditoClick
  end
  object btn_Cerrar: TButton
    Left = 361
    Top = 401
    Width = 131
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cerrar'
    TabOrder = 8
    OnClick = btn_CerrarClick
  end
  object btnBuscar: TButton
    Left = 419
    Top = 7
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '&Buscar'
    TabOrder = 5
    OnClick = btnBuscarClick
  end
  object cbProveedorDayPass: TVariantComboBox
    Left = 112
    Top = 84
    Width = 291
    Height = 21
    Style = vcsDropDownList
    DroppedWidth = 0
    ItemHeight = 13
    TabOrder = 4
    Items = <>
  end
  object deFDesde: TDateEdit
    Left = 312
    Top = 8
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 2
    Date = -693594.000000000000000000
  end
  object deFHasta: TDateEdit
    Left = 313
    Top = 52
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 3
    Date = -693594.000000000000000000
  end
  object rbNotasCobroPDU: TRadioButton
    Left = 8
    Top = 11
    Width = 188
    Height = 17
    Hint = 'Seleccione para buscar las Notas de Cobro Directas por PDU'
    Caption = 'Notas de Cobro Directas para PDU'
    Checked = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    TabStop = True
  end
  object rbNotasCobroBHTU: TRadioButton
    Left = 8
    Top = 55
    Width = 194
    Height = 17
    Hint = 'Seleccione para buscar las Notas de Cobro Directas por BHTU'
    Caption = 'Notas de Cobro Directas para BHTU'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TabStop = True
  end
  object sp_ObtenerNotasCreditoNQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterOpen = sp_ObtenerNotasCreditoNQAfterOpen
    ProcedureName = 'ObtenerNotasCreditoNQ;1'
    Parameters = <>
    Left = 328
    Top = 120
  end
  object ds_ObtenerNotasCreditoNQ: TDataSource
    DataSet = sp_ObtenerNotasCreditoNQ
    Left = 360
    Top = 120
  end
end
