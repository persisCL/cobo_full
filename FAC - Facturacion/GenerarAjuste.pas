unit GenerarAjuste;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, DB, ADODB, StdCtrls, ExtCtrls, Util, utilProc,
  PeaProcs, Mask, DBCtrls, Grids, DBGrids, DPSGrid, DBClient, ImgList, Utildb,
  peaTypes, AgregarMovimiento, jpeg, DateUtils,
  ComCtrls, DPSControls, DBListEx, ListBoxEx, UtilFacturacion,
  ReporteNotaCredito, ReporteNotaDebito;

type
  TFrmGenerarAjuste = class(TForm)
    Notebook: TNotebook;
    rgTipoAjuste: TRadioGroup;
    cdsMovimientos: TClientDataSet;
    Label5: TLabel;
    dsMovimientos: TDataSource;
    Label2: TLabel;
    Bevel4: TBevel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    AgregarMovimientoCuenta: TADOStoredProc;
    AgregarMovimientoTransito: TADOStoredProc;
    CrearLoteFacturacion: TADOStoredProc;
	ObtenerCliente: TADOStoredProc;
	CrearComprobante: TADOStoredProc;
	CrearProcesoFacturacion: TADOStoredProc;
    cdsTransitosAjustados: TClientDataSet;
	Label1: TLabel;
	dsTransitosAjustados: TDataSource;
    dbgMovimientos: TDPSGrid;
	CrearProcesoFacturacionGrupoFacturacion: TADOStoredProc;
	lImporteTotal: TLabel;
	Bevel2: TBevel;
	Image3: TImage;
	Bevel1: TBevel;
	Image1: TImage;
	cbSobreComprobante: TCheckBox;
	Label4: TLabel;
	Label6: TLabel;
    cbCuentasDebito: TComboBox;
    Label11: TLabel;
    btnCancelar: TDPSButton;
    btnAgregar: TDPSButton;
    btnEliminar: TDPSButton;
    btnModificar: TDPSButton;
    btnSiguiente: TDPSButton;
    btnAnterior: TDPSButton;
    dbgTransitos: TDBListEx;
    ObtenerDomicilioConvenio: TADOStoredProc;
    ObtenerDatosMedioPagoAutomaticoConvenio: TADOStoredProc;
    AgregarMovimientoCuentaAjuste: TADOStoredProc;
	procedure btnSiguienteClick(Sender: TObject);
	procedure btnAnteriorClick(Sender: TObject);
	procedure NotebookPageChanged(Sender: TObject);
	procedure btnCancelarClick(Sender: TObject);
	procedure btnAgregarClick(Sender: TObject);
	procedure dsMovimientosDataChange(Sender: TObject; Field: TField);
	procedure btnEliminarClick(Sender: TObject);
	procedure rgTipoAjusteClick(Sender: TObject);
	procedure btnModificarClick(Sender: TObject);
	procedure dbgMovimientosDrawColumnCell(Sender: TObject;
	  const Rect: TRect; DataCol: Integer; Column: TColumn;
	  State: TGridDrawState);
	procedure cbSobreComprobanteClick(Sender: TObject);
	procedure cbCuentasDebitoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dbgMovimientosDblClick(Sender: TObject);
  private
	{ Private declarations }
	ImporteComprobanteAAjustar, FImporteTotal, FImporteComprobante: Double;
    NumeroComprobanteAAjustar, FNumeroComprobante : Int64;
	FCodigoCliente: Integer;
	FTipoComprobante, TipoComprobanteAAjustar: Char;
	FModoPago: ANsiString;
    FCodigoConvenio: Integer;
    FMedioPago: integer;
  public
	{ Public declarations }
	Function Inicializa(aTipoComprobante: Char; aNumeroComprobante: Int64;
	  aCodigoCliente: Integer; aImporteComprobante: Double; CodigoCovenio: Integer): Boolean;
  end;

	Function RealizarAjuste(aTipoComprobante: Char; aNumeroComprobante: Int64;
	  aCodigoCliente: Integer; aImporteComprobante: Double): Boolean;


implementation

{$R *.dfm}

resourcestring
    CAPTION_VALIDAR_IMPORTE = 'Validar Importe del Ajuste';
    CAPTION_MOVIMIENTOS = 'Generar Ajuste de Facturaci�n (Movimientos a Facturar)';
    MSG_IMPORTE_COMPROBANTE = 'El importe de ajuste debe ser menor o igual al saldo del comprobante.';

function RealizarAjuste(aTipoComprobante: Char; aNumeroComprobante: Int64;
  aCodigoCliente: Integer; aImporteComprobante: Double): Boolean;
var
    F: TFrmGenerarAjuste;
begin
//	application.CreateForm(TFrmGenerarAjuste,F);
    FindFormOrCreate(TFrmGenerarAjuste,F);
	F.Inicializa(aTipoComprobante, aNumeroComprobante, aCodigoCliente, aImporteComprobante,
        QueryGetValueInt(DMConnections.BaseCAC,format('SELECT CodigoConvenio FROM LotesFacturacion WHERE NumeroComprobante = ' + FloatToStr(aNumeroComprobante) + ' and TipoComprobante = ''%s''' ,[aTipoComprobante])));
	F.ShowModal;
	result := F.ModalResult = mrOk;
end;

function TFrmGenerarAjuste.Inicializa(aTipoComprobante: Char; aNumeroComprobante: Int64;
  aCodigoCliente: Integer; aImporteComprobante: Double; CodigoCovenio: Integer): Boolean;
begin
	result := true;
	Caption := CAPTION_MOVIMIENTOS;
    FCodigoConvenio := CodigoCovenio;

    with dbgMovimientos do begin
        Columns[0].Title.Caption := MSG_COL_TITLE_PATENTE;
        Columns[1].Title.Caption := MSG_COL_TITLE_FECHA;
        Columns[2].Title.Caption := MSG_COL_TITLE_CONCEPTO;
        Columns[3].Title.Caption := MSG_COL_TITLE_IMPORTE;
    end;

    with dbgTransitos do begin
		Columns[0].Header.Caption := MSG_COL_TITLE_HORA;
//		Columns[1].Header.Caption := MSG_COL_TITLE_CONCESIONARIA;
		Columns[1].Header.Caption := MSG_COL_TITLE_PUNTO_COBRO;
		Columns[2].Header.Caption := MSG_COL_TITLE_CATEGORIA;
		Columns[3].Header.Caption := MSG_COL_TITLE_IMPORTE;
		Columns[4].Header.Caption := MSG_COL_TITLE_ITE_AJUSTADO;
    end;



	btnEliminar.enabled := False;
	btnModificar.Enabled := false;
	NoteBook.PageIndex := 0;
	btnAnterior.Enabled := false;

	dbgMovimientos.Columns[2].Width := 300;
	FImporteTotal := 0;
	lImporteTotal.Caption := FormatFloat(FORMATO_IMPORTE,FImporteTotal);

//	cargarCuentasDebito(DMCOnnections.BaseCAC, cbCuentasDebito, aCodigoCliente);
//	cbCuentasDebito.ItemIndex := 0;
//	cbCuentasDebitoChange(cbCuentasDebito);
//	cbCuentasDebito.Enabled := false;

	FTipoComprobante := aTipoComprobante;
	FNumeroComprobante := aNumeroComprobante;
	FCodigoCliente := aCodigoCliente;
	FImporteComprobante := aImporteComprobante;

	if (FTipoComprobante = ' ') or (FTipoComprobante = TC_NOTA_CREDITO) then begin
		cbSobreComprobante.Checked := False;
		cbSobreComprobante.Enabled := False;
	end else begin
		cbSobreComprobante.Checked := True;
		cbSobreComprobante.Enabled := True;
	end;
	cbSobreComprobanteClick(cbSobreComprobante);
    rgTipoAjusteClick(rgTipoAjuste);
end;

procedure TFrmGenerarAjuste.btnSiguienteClick(Sender: TObject);
var
    ProcesoFacturacion, LoteFacturacion, NumeroNotaCredito, NumeroMovimiento: Integer;
    F: TFormReporteNotaCredito;
    G: TFormReporteNotaDebito;

    procedure InsertarMovimientoCuentaAjuste;
	begin
		with AgregarMovimientoCuentaAjuste do begin
			close;
			Parameters.ParamByName('@CodigoConvenio').value := FCodigoConvenio;
			Parameters.ParamByName('@IndiceVehiculo').value := cdsMovimientos.FieldByName('IndiceVehiculo').asInteger;
  		    Parameters.ParamByName('@NumeroMovimiento').value := NumeroMovimiento;
  		    Parameters.ParamByName('@Usuario').value := UsuarioSistema;
            ExecProc;
        end;
    end;

	procedure InsertarMovimientoCuenta;
	begin
		with AgregarMovimientoCuenta do begin
			close;
			Parameters.ParamByName('@CodigoConvenio').value := FCodigoConvenio;
			Parameters.ParamByName('@IndiceVehiculo').value := iif(cdsMovimientos.FieldByName('IndiceVehiculo').asInteger = -1, NULL, cdsMovimientos.FieldByName('IndiceVehiculo').asInteger);
			Parameters.ParamByName('@FechaHora').Value := iif(TimeOf(cdsMovimientos.FieldByName('Fecha').asDateTime) = 0,
			  cdsMovimientos.FieldByName('Fecha').asDateTime + TimeOf(NowBase(DMConnections.BaseCAC)), cdsMovimientos.FieldByName('Fecha').asDateTime);
			Parameters.ParamByName('@Importe').value := cdsMovimientos.FieldByName('Importe').asFloat;
			Parameters.ParamByName('@CodigoConcepto').value := cdsMovimientos.FieldByName('CodigoConcepto').asInteger;
			Parameters.ParamByName('@FechaCorte').value := iif(TimeOf(cdsMovimientos.FieldByName('Fecha').asDateTime) = 0,
			  cdsMovimientos.FieldByName('Fecha').asDateTime + TimeOf(NowBase(DMConnections.BaseCAC)), cdsMovimientos.FieldByName('Fecha').asDateTime);
			Parameters.ParamByName('@LoteFacturacion').value := iif(LoteFacturacion <> 0, LoteFacturacion, null);
			Parameters.ParamByName('@EsPago').value := 0;
			Parameters.ParamByName('@Observaciones').value := cdsMovimientos.FieldByName('Observaciones').asString;
			Parameters.ParamByName('@NumeroPromocion').value := null;
			Parameters.ParamByName('@NumeroFinanciamiento').value := null;
			ExecProc;
			NumeroMovimiento := Parameters.ParamByName('@NumeroMovimiento').value;
		end;
	end;

	procedure InsertarMovimientoTransito;
	begin
		with AgregarMovimientoTransito do begin
			close;
			Parameters.ParamByName('@CodigoConvenio').value := FCodigoConvenio;
			Parameters.ParamByName('@IndiceVehiculo').value := cdsMovimientos.FieldByName('IndiceVehiculo').asInteger;
			Parameters.ParamByName('@NumeroMovimiento').Value := NumeroMovimiento;
			Parameters.ParamByName('@CodigoConcesionaria').value := cdsTransitosAjustados.FieldByName('CodigoConcesionaria').asInteger;
			Parameters.ParamByName('@NumeroViaje').value := cdsTransitosAjustados.FieldByName('NumeroViaje').asString;
			Parameters.ParamByName('@NumeroPuntoCobro').value := cdsTransitosAjustados.FieldByName('NumeroPuntoCobro').AsInteger;
			Parameters.ParamByName('@Importe').value := cdsTransitosAjustados.FieldByName('ImporteAjustado').asFloat;
			ExecProc;
		end;
	end;

	procedure InsertarComprobante(TipoComprobante: Char);
	begin
		ObtenerCliente.Parameters.ParamByName('@CodigoCliente').Value := FCodigoCliente;
		ObtenerCliente.open;
		// Obtenemos el tipo de debito, la entidad y la cuenta debito
		with CrearComprobante do begin
			// Parametros del comprobante
			Parameters.ParamByName('@TipoComprobante').value := TipoComprobante;
			Parameters.ParamByName('@SubTipoComprobante').value := '';
			Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
			Parameters.ParamByName('@ImporteTotal').Value := FImporteTotal;
			Parameters.ParamByName('@FechaVencimiento').Value := NowBase(DMConnections.BaseCAC) +
			                    QueryGetValueInt(DMConnections.BaseCAC,
                                'SELECT PlazoVencimientoFacturas FROM Parametros');
			if TipoComprobanteAAjustar = ' ' then
				Parameters.ParamByName('@TipoComprobanteAjustado').Value := null
			else
				Parameters.ParamByName('@TipoComprobanteAjustado').Value := TipoComprobanteAAjustar;
			if NumeroComprobanteAAjustar = 0 then
				Parameters.ParamByName('@NumeroComprobanteAjustado').Value := null
			else
				Parameters.ParamByName('@NumeroComprobanteAjustado').Value := NumeroComprobanteAAjustar;

			// Parametros del cliente
			Parameters.ParamByName('@CodigoPersona').value := FCodigoCliente;
			Parameters.ParamByName('@Apellido').value := ObtenerCliente.FieldByName('Apellido').Value;
            Parameters.ParamByName('@ApellidoMaterno').value := ObtenerCliente.FieldByName('ApellidoMaterno').Value;
			Parameters.ParamByName('@Nombre').value := ObtenerCliente.FieldByName('Nombre').Value;
			Parameters.ParamByName('@TipoDocumento').Value := ObtenerCliente.FieldByName('CodigoDocumento').Value;
			Parameters.ParamByName('@NumeroDocumento').value := ObtenerCliente.FieldByName('NumeroDocumento').Value;
			Parameters.ParamByName('@Personeria').value := ObtenerCliente.FieldByName('Personeria').Value;
			Parameters.ParamByName('@Sexo').value := ObtenerCliente.FieldByName('Sexo').Value;

            //Domicilio Facturacion
            Parameters.ParamByName('@Calle').value := ObtenerDomicilioConvenio.FieldByName('DescriCalle').Value;
            Parameters.ParamByName('@numero').value := ObtenerDomicilioConvenio.FieldByName('Numero').Value;
            Parameters.ParamByName('@detalleDomicilio').value := ObtenerDomicilioConvenio.FieldByName('Detalle').Value;
            Parameters.ParamByName('@CodigoPostal').value := ObtenerDomicilioConvenio.FieldByName('CodigoPostal').Value;
            Parameters.ParamByName('@CodigoComuna').Value := ObtenerDomicilioConvenio.FieldByName('CodigoComuna').Value;
            Parameters.ParamByName('@CodigoRegion').Value := ObtenerDomicilioConvenio.FieldByName('CodigoRegion').Value;

            Parameters.ParamByName('@CodigoTipoMedioPago').Value := FMedioPago;

			// Parametros de la forma de pago
            if (FMedioPago = TPA_PAT) then begin
                Parameters.ParamByName('@CodigoTipoTarjeta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoTipoTarjetaCredito').AsInteger;
                Parameters.ParamByName('@NumeroTarjetaCredito').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('NumeroTarjetaCredito').AsString;
                Parameters.ParamByName('@FechaVencimientoTarjeta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('FechaVencimiento').AsString;
                Parameters.ParamByName('@CodigoEmisorTarjeta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoEmisorTarjetaCredito').AsInteger;
            end
            else begin
                if (FMedioPago = TPA_PAC) then begin
                    Parameters.ParamByName('@CodigoBanco').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoBanco').AsInteger;
                    Parameters.ParamByName('@CodigoTipoCuenta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoTipoCuentaBancaria').AsInteger;
                    Parameters.ParamByName('@NumeroCuentaBancaria').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('NroCuentaBancaria').AsString;
                    Parameters.ParamByName('@Sucursal').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('Sucursal').AsString;
                end;
            end;
			ExecProc;
			NumeroNotaCredito := Parameters.ParamByName('@NumeroComprobante').Value;
		end;
		ObtenerCliente.close;
	end;

	procedure InsertarLoteFacturacion(TipoComprobante: Char);
	begin
		with CrearLoteFacturacion do begin
			Parameters.ParamByName('@CodigoConvenio').value := FCodigoConvenio;
//			Parameters.ParamByName('@IndiceVehiculo').value := cdsMovimientos.FieldByName('IndiceVehiculo').asInteger;
//			Parameters.ParamByName('@Total').value := -1 * cdsMovimientos.FieldByName('Importe').asFloat;
			Parameters.ParamByName('@Total').value := cdsMovimientos.FieldByName('Importe').asFloat * 100;
			Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
			Parameters.ParamByName('@NumeroProcesoFacturacion').value := ProcesoFacturacion;
			Parameters.ParamByName('@TipoComprobante').value := TipoComprobante;
			Parameters.ParamByName('@NumeroComprobante').value := NumeroNotaCredito;
			ExecProc;
			LoteFacturacion := Parameters.ParamByName('@LoteFacturacion').value;
		end;
	end;

resourcestring
    MSG_IMPORTE_NC = 'El importe de ajuste por una nota de cr�dito debe ser menor a cero.';
    MSG_IMPORTE_ND = 'El importe de ajuste por una nota de d�bito debe ser mayor a cero.';
begin
	if (sender as TDPSButton).Caption = '&Finalizar' then begin
		if (rgTipoAjuste.ItemIndex = 1) or (rgTipoAjuste.ItemIndex = 2) then begin
			// Si es una nota de credito, insertamos el comprobante (Nota de credito) luego
			// cada lote de facturacion y sus correspondientes movimientos
			If (rgTipoAjuste.ItemIndex = 1) and (FImporteTotal > 0) then begin
				MsgBox(MSG_IMPORTE_NC, CAPTION_VALIDAR_IMPORTE, MB_OK);
				exit;
			end;

    		if ((rgTipoAjuste.ItemIndex = 1) and (cbSobreComprobante.Checked) and (Abs(FImporteTotal) > ImporteComprobanteAAjustar)) then Begin
	    		MsgBox(MSG_IMPORTE_COMPROBANTE, CAPTION_VALIDAR_IMPORTE, MB_OK);
		    	exit;
    		end;

			If (rgTipoAjuste.ItemIndex = 2) and (FImporteTotal < 0) then begin
				MsgBox(MSG_IMPORTE_ND, CAPTION_VALIDAR_IMPORTE, MB_OK);
				exit;
			end;


//            ObtenerDatosMedioPagoAutomaticoConvenio.close;
//            ObtenerDatosMedioPagoAutomaticoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
//            ObtenerDatosMedioPagoAutomaticoConvenio.Open;

//            FMedioPago := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger;
            FMedioPago := TPA_NINGUNO;

            ObtenerDomicilioConvenio.Close;
            ObtenerDomicilioConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ObtenerDomicilioConvenio.Open;

			// Generamos el proceso de facturacion
			CrearProcesoFacturacion.Parameters.ParamByName('@Operador').value := usuarioSistema;
			CrearProcesoFacturacion.ExecProc;
			ProcesoFacturacion := CrearProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value;

			CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@NumeroProcesoFacturacion').value :=
			  ProcesoFacturacion;
			CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@CodigoGrupoFacturacion').value :=
			  QueryGetValueInt(DMConnections.BaseCAC,
			  'select CodigoGrupoFacturacion from Convenio where Convenio.codigocliente = ' +
			  intToStr(FCodigoCliente));
			CrearProcesoFacturacionGrupoFacturacion.execPROC;

			// Generamos el comprobante
			InsertarComprobante(iif(rgTipoAjuste.ItemIndex = 1, TC_NOTA_CREDITO, TC_NOTA_DEBITO)[1]);
		end;
		// Agregamos en la base todos los movimientos generados y los ajustes para cada
		// transacci�n de ese ajuste. Si fue creda una nota de credito se agregan los
		// correspondientes lotes de facturacion.
		cdsMovimientos.DisableControls;
		cdsTransitosAjustados.DisableControls;
		cdsMovimientos.First;
		cdsTransitosAjustados.Filter := 'IndiceVehiculo = ' +
		  cdsMovimientos.FieldByName('IndiceVehiculo').asString +
		  ' and NumeroMovimiento = ' +
		  cdsMovimientos.FieldByName('NumeroMovimiento').asString;
		while not cdsMovimientos.eof do begin
			if (rgTipoAjuste.ItemIndex > 0) then
				InsertarLoteFacturacion(iif(rgTipoAjuste.ItemIndex = 1, TC_NOTA_CREDITO, TC_NOTA_DEBITO)[1])
			else
				LoteFacturacion := 0;
			InsertarMovimientoCuenta;
            if (rgTipoAjuste.ItemIndex = 0) then
                InsertarMovimientoCuentaAjuste;
			cdsTransitosAjustados.first;
			while not cdsTransitosAjustados.Eof do begin
				InsertarMovimientoTransito;
				cdsTransitosAjustados.Next;
			end;
			cdsMovimientos.Next;
			cdsTransitosAjustados.Filter := 'IndiceVehiculo = ' +
			  cdsMovimientos.FieldByName('IndiceVehiculo').asString +
			  ' and NumeroMovimiento = ' +
			  cdsMovimientos.FieldByName('NumeroMovimiento').asString;
		end;
		cdsMovimientos.EnableControls;
		cdsTransitosAjustados.EnableControls;

        if (rgTipoAjuste.ItemIndex = 1) then begin
            // Imprimimos la Nota de Credito
            Application.CreateForm(TFormReporteNotaCredito, F);
            F.Execute(TC_NOTA_CREDITO, trunc(FCodigoCliente), NumeroNotaCredito, True, '');
            F.free;
        end
        else if (rgTipoAjuste.ItemIndex = 2) then begin
                // Imprimimos la Nota de Debito
                Application.CreateForm(TFormReporteNotaDebito, G);
                G.Execute(TC_NOTA_DEBITO, trunc(FCodigoCliente), NumeroNotaCredito, True, '');
                G.free;
            end;

		modalResult := mrOk;
	end else begin
		NoteBook.PageIndex := NoteBook.PageIndex + 1;
		dsMovimientosDataChange(dsMovimientos, nil);
	end;
end;

procedure TFrmGenerarAjuste.btnAnteriorClick(Sender: TObject);
begin
	NoteBook.PageIndex := NoteBook.PageIndex - 1;
end;

procedure TFrmGenerarAjuste.NotebookPageChanged(Sender: TObject);
resourcestring
    CAPTION_FINALIZAR = '&Finalizar';
    CAPTION_SIGUIENTE = '&Siguiente';
begin
	if (sender as TNoteBook).PageIndex = 1 then begin
		btnAnterior.Enabled := true;
		btnSiguiente.Caption := CAPTION_FINALIZAR;
		btnSiguiente.enabled := not dsMovimientos.DataSet.IsEmpty;
	end;
	if (sender as TNoteBook).PageIndex = 0 then begin
		btnAnterior.Enabled := False;
		btnSiguiente.enabled := true;
		btnSiguiente.Caption := CAPTION_SIGUIENTE;
    end;
end;

procedure TFrmGenerarAjuste.btnCancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFrmGenerarAjuste.btnAgregarClick(Sender: TObject);
var
    NumeroMovimiento, CodigoConcepto, IndiceVehiculo: Integer;
	Fecha: TDateTime;
	Patente, Concepto, Observaciones: AnsiString;
	Importe: Double;
begin
    // Agregamos un movimiento para el ajuste
	NumeroMovimiento := cdsMovimientos.RecordCount + 1;
	IndiceVehiculo := 0;
	Fecha := NowBase(DMConnections.BaseCAC);
	Observaciones := '';
	Importe := 0;
	codigoConcepto := 0;
	cdsTransitosAjustados.DisableControls;
	if AgregarMovimientoAjuste(FCodigoCLiente, FCodigoConvenio,
	  FModoPago, rgTipoAjuste.ItemIndex,
	  cdsTransitosAjustados, NumeroMovimiento, NumeroComprobanteAAjustar, IndiceVehiculo, Fecha,  Observaciones,
	  Importe, CodigoConcepto, Patente) then begin
		if trim(Observaciones) = '' then
			Concepto := QueryGetValue(DMConnections.BaseCAC,
			  'Select Descripcion from ConceptosMovimiento where CodigoConcepto = ' + intToStr(CodigoConcepto))
		else
			Concepto := Observaciones;
		cdsMovimientos.AppendRecord([IndiceVehiculo, NumeroMovimiento, Fecha, Concepto, Observaciones,
		  Importe, CodigoConcepto, Patente]);
		FImporteTotal := FImporteTotal + Importe;
		lImporteTotal.Caption := FormatFloat(FORMATO_IMPORTE,FImporteTotal);
		dsMovimientosDataChange(dsMovimientos,nil);
	end;
	cdsTransitosAjustados.EnableControls;
end;

procedure TFrmGenerarAjuste.btnEliminarClick(Sender: TObject);
begin
    // Eliminamos un movimeiento del ajuste, si el ajuste involucra transacciones eliminamos las
    // transacciones de la tabla temporal
	with cdsTransitosAjustados do begin
        DisableControls;
        first;
        while not eof do begin
            delete;
        end;
        enableControls;
    end;
    FImporteTotal := FImporteTotal - cdsMovimientos.FieldByName('Importe').asFloat;
	lImporteTotal.Caption := FormatFloat(FORMATO_IMPORTE,FImporteTotal);
	cdsMovimientos.delete;
end;

procedure TFrmGenerarAjuste.btnModificarClick(Sender: TObject);
var NumeroMovimiento, CodigoConcepto, IndiceVehiculo: Integer;
	Fecha: TDateTime;
	Concepto, Observaciones: AnsiString;
    Patente : String;
	ImporteOld, Importe: Double;
begin
	// Asigno los datos del movimiento seleccionado
	NumeroMovimiento := cdsMovimientos.FieldByName('NumeroMovimiento').asInteger;
	IndiceVehiculo := cdsMovimientos.FieldByName('IndiceVehiculo').asInteger;
	Fecha := cdsMovimientos.FieldByName('Fecha').asDateTime;
	Observaciones := cdsMovimientos.FieldByName('Observaciones').asString;
	Importe := cdsMovimientos.FieldByName('Importe').asFloat;
	codigoConcepto := cdsMovimientos.FieldByName('CodigoConcepto').asInteger;
	cdsTransitosAjustados.DisableControls;
	if AgregarMovimientoAjuste(FCodigoCliente, FCodigoConvenio,FModoPago, rgTipoAjuste.ItemIndex,
	  cdsTransitosAjustados, NumeroMovimiento, NumeroComprobanteAAjustar, IndiceVehiculo, Fecha,Observaciones,
	  Importe, CodigoConcepto, Patente) then begin
(*
		if ((Trim(TipoComprobanteAAjustar) <> '') and ((FImporteTotal - cdsMovimientos.FieldByName('Importe').asFloat + Importe) > ImporteComprobanteAAjustar)) then Begin
			MsgBox(MSG_IMPORTE_COMPROBANTE, CAPTION_VALIDAR_IMPORTE, MB_OK);
			exit;
		end;
*)
		if trim(Observaciones) = '' then
			Concepto := QueryGetValue(DMConnections.BaseCAC,
			  'Select Descripcion from ConceptosMovimiento where CodigoConcepto = ' + intToStr(CodigoConcepto))
		else
			Concepto := Observaciones;
		with cdsMovimientos do begin
			importeOld := FieldByName('Importe').asFloat;
			edit;
			FieldByName('IndiceVehiculo').asInteger := IndiceVehiculo;
			FieldByName('NumeroMovimiento').asInteger := NumeroMovimiento;
			FieldByName('Fecha').asDateTime := Fecha;
			FieldByName('Concepto').asString := Concepto;
			FieldByName('Observaciones').AsString := Observaciones;
			FieldByName('Importe').asFloat := Importe;
			FieldByName('CodigoConcepto').asInteger := CodigoConcepto;
            FieldByName('Patente').AsString := Patente;
			post;
			FImporteTotal := FImporteTotal + (Importe - ImporteOld);
			LImporteTotal.Caption := formatFloat(FORMATO_IMPORTE,FImporteTotal);
		end;
    end;
    cdsTransitosAjustados.EnableControls
end;

procedure TFrmGenerarAjuste.dsMovimientosDataChange(Sender: TObject;
  Field: TField);
begin
	if (Field = nil) then begin
		if (not cdsMovimientos.IsEmpty) then begin
			cdsTransitosAjustados.Filter := 'IndiceVehiculo = ' +
			  cdsMovimientos.FieldByName('IndiceVehiculo').asString;
(*
			cdsTransitosAjustados.Filter := 'IndiceVehiculo = ' +
			  cdsMovimientos.FieldByName('IndiceVehiculo').asString +
			  ' and NumeroMovimiento = ' +
			 cdsMovimientos.FieldByName('NumeroMovimiento').asString;
*)
			btnEliminar.enabled := True;
			btnModificar.Enabled := True;
			btnSiguiente.Enabled := True;
		end else begin
			btnEliminar.enabled := False;
			btnModificar.Enabled := False;
			btnSiguiente.Enabled := NoteBook.ActivePage = 'Inicio';
		end;
	end;
end;


procedure TFrmGenerarAjuste.rgTipoAjusteClick(Sender: TObject);
resourcestring
    CAPTION_NOTA_CREDITO = 'Generar Ajuste de Facturaci�n (Nota de Cr�dito)';
    CAPTION_NOTA_DEBITO  = 'Generar Ajuste de Facturaci�n (Nota de D�bito)';
var bm: TBookMark;
begin
    // Seleccionamos el tipo de ajuste
	cursor := crHourGlass;
	case (sender as TRadioGroup).ItemIndex of
		0 : begin
                // Seleccionamos Agregar Movimientos a Facturar en el pr�ximo ciclo
				Caption := CAPTION_MOVIMIENTOS;
				cbSobreComprobante.Enabled := (FTipoComprobante <> ' ') and (FTipoComprobante <> TC_NOTA_CREDITO);
				cbSobreComprobanteClick(cbSobreComprobante);
				cbCuentasDebito.Enabled := False;
			end;
		1 : begin
                // Seleccionamos Realizar un Nota de Credito
				Caption := CAPTION_NOTA_CREDITO;
				cbSobreComprobante.Enabled := (FTipoComprobante <> ' ') and (FTipoComprobante <> TC_NOTA_CREDITO);
				cbSobreComprobanteClick(cbSobreComprobante);
				cbCuentasDebito.Enabled := False;
                // Asignamos la fecha actual en los movimientos ya asignados
				with cdsMovimientos do begin
					bm := getBookMark;
					disableControls;
					first;
					while not eof do begin
						edit;
						fieldByName('Fecha').AsDateTime := Date();
						next;
					end;
					enableControls;
					gotoBookMark(bm);
					freeBookMark(bm);
				end;
			end;
		2 : begin
                // Seleccionamos generar una nota de d�bito
				Caption := CAPTION_NOTA_DEBITO;
				cbSobreComprobante.Enabled := False;
				cbCuentasDebito.enabled := True;
				TipoComprobanteAAjustar := ' ';
				NumeroComprobanteAAjustar := 0;
				ImporteComprobanteAAjustar := 0;
			end;
	end;
	cdsMovimientos.EmptyDataSet;
	cdsTransitosAjustados.emptyDataSet;
	FImporteTotal := 0;
	lImporteTotal.Caption := FormatFloat(FORMATO_IMPORTE, FImporteTotal);
	cursor := crDefault;
end;

procedure TFrmGenerarAjuste.dbgMovimientosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
	if (Column.FieldName = 'Importe') then begin
		(Sender as TDBGrid).canvas.fillRect(Rect);
		if Column.Field.asFloat <> 0 then
			(Sender as TDBGrid).canvas.TextOut(rect.Left + 1, rect.Top + 1, FormatFloat(FORMATO_IMPORTE,Column.Field.asFloat));
	end;
end;

procedure TFrmGenerarAjuste.cbSobreComprobanteClick(Sender: TObject);
begin
	if (Sender as TCheckBox).Checked then begin
		TipoComprobanteAAjustar := FTipoComprobante;
		NumeroComprobanteAAjustar := FNumeroComprobante;
		ImporteComprobanteAAjustar := FImporteComprobante;
	end else begin
		TipoComprobanteAAjustar := ' ';
		NumeroComprobanteAAjustar := 0;
		ImporteComprobanteAAjustar := 0;
	end;
	cdsMovimientos.EmptyDataSet;
	cdsTransitosAjustados.emptyDataSet;
	FImporteTotal := 0;
	lImporteTotal.Caption := FormatFloat(FORMATO_IMPORTE,FImporteTotal);
end;

procedure TFrmGenerarAjuste.cbCuentasDebitoChange(Sender: TObject);
begin
	if StrRight(TComboBoxEx(Sender).Items[TComboBoxEx(Sender).ItemIndex], 2) = TP_POSPAGO_MANUAL then
		FModoPago := TP_POSPAGO_MANUAL
	else
		FModoPago := TP_DEBITO_AUTOMATICO;
	cdsMovimientos.EmptyDataSet;
	cdsTransitosAjustados.emptyDataSet;
	FImporteTotal := 0;
	lImporteTotal.Caption := FormatFloat(FORMATO_IMPORTE,FImporteTotal);
end;

procedure TFrmGenerarAjuste.FormCreate(Sender: TObject);
begin
	cdsTransitosAjustados.CreateDataSet;
	cdsMovimientos.CreateDataSet;
end;

procedure TFrmGenerarAjuste.dbgMovimientosDblClick(Sender: TObject);
begin
    if btnModificar.Enabled then btnModificar.Click;
end;

end.



