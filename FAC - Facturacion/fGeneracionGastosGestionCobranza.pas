
{********************************** Unit Header ********************************
File Name : fGeneracionGastosGestionCobranza.pas
Author : ndonadio
Date Created: 20/10/2005
Language : ES-AR
Description :  Permite listar un conjunto de Comprobante candidatos
  a generar gastos por gesti�n de cobranza y seleccionar de ellos
  para cuales se generar�n estos gastos.

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia el llamado a llenar el combo de convenios para que utilice el procedimiento
                CargarConveniosRUT.
                Se crea llamado a DrawItem para verificar si el combo del convenio
                posee alg�n convenio con el texto "(Baja)" y en caso de encontrarlo
                llamar a la funci�n que lo colorear� en rojo.
*******************************************************************************}
unit fGeneracionGastosGestionCobranza;

interface

uses
    // Utiles varios
    Util, UtilProc, PeaTypes, PeaProcs, BuscaClientes,
    // Parametros generales y base de datos
    ConstParametrosGenerales, UtilDB, DMConnection,
    // seleccion de UNA NK
    FSeleccionarNKAGenerarGastoCobranza,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, ExtCtrls, DB, ADODB, Validate, DateEdit, VariantComboBox,
  DmiCtrls, DBClient, ImgList, ComCtrls;

type
  TfrmGeneracionGastosGestionCobranza = class(TForm)
    pnlFiltros: TPanel;
    pnlBotones: TPanel;
    dblComprobantesVEncidos: TDBListEx;
    btnGenerar: TButton;
    btnSalir: TButton;
    btnFiltrar: TButton;
    spObtenerListaComprobantesVencidos: TADOStoredProc;
    chkSoloEnviadosSerbanc: TCheckBox;
    gbComprobantes: TGroupBox;
    gbConvenio: TGroupBox;
    peRUTCliente: TPickEdit;
    cbConvenio: TVariantComboBox;
    deFechaFiltro: TDateEdit;
    neImporteHasta: TNumericEdit;
    lblNKEspecifica: TLabel;
    lblAqui: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    tmrConsulta: TTimer;
    spObtenerConveniosCliente: TADOStoredProc;
    ObtenerCLiente: TADOStoredProc;
    neImporteDesde: TNumericEdit;
    btnLimpiarFiltros: TButton;
    lblFechaVencimiento: TLabel;
    neCantidad: TNumericEdit;
    Label8: TLabel;
    pnlSeleccion: TPanel;
    btnInvertirSeleccion: TButton;
    btnDeselectAll: TButton;
    btnSelectAll: TButton;
    btnMarcar: TButton;
    Label9: TLabel;
    dsComprobantesVencidos: TDataSource;
    cdsComprobantesVencidos: TClientDataSet;
    cdsComprobantesVencidosTipoComprobante: TStringField;
    cdsComprobantesVencidosNumeroComprobante: TLargeintField;
    cdsComprobantesVencidosFechaEmision: TDateTimeField;
    cdsComprobantesVencidosFechaVencimiento: TDateTimeField;
    cdsComprobantesVencidosTotalComprobante: TIntegerField;
    cdsComprobantesVencidosTotalAPagar: TIntegerField;
    cdsComprobantesVencidosEnviadoSerbanc: TBooleanField;
    cdsComprobantesVencidosFechaActualizadooSerbanc: TDateTimeField;
    cdsComprobantesVencidosNumeroConvenio: TStringField;
    Imagenes: TImageList;
    cdsComprobantesVencidosMarcado: TBooleanField;
    lblCantidadMostrada: TLabel;
    spAgregarGastoGestionCobranza: TADOStoredProc;
    cdsComprobantesVencidosCodigoConvenio: TIntegerField;
    pbProgreso: TProgressBar;
    procedure deFechaFiltroExit(Sender: TObject);
    procedure dblComprobantesVEncidosColumns0HeaderClick(Sender: TObject);
    procedure dblComprobantesVEncidosDblClick(Sender: TObject);
    procedure dblComprobantesVEncidosKeyPress(Sender: TObject; var Key: Char);
    procedure lblAquiClick(Sender: TObject);
    procedure btnInvertirSeleccionClick(Sender: TObject);
    procedure btnMarcarClick(Sender: TObject);
    procedure dblComprobantesVEncidosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn;
      Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
      var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnLimpiarFiltrosClick(Sender: TObject);
    procedure tmrConsultaTimer(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure btnDeselectAllClick(Sender: TObject);
    procedure btnSelectAllClick(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure cbConvenioDrawItem(Control: TWinControl; Index: Integer;                //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                           //SS_1120_MVI_20130820
  private
    { Private declarations }
    FProcesando,
    FCargandoDatos,
    FAllSelected,
    FNoneSelected   : boolean;
    FCantidadDias   : Integer;
 	FUltimaBusqueda : TBusquedaCliente;
    FFechaLimite,
    FMaxDate,
    FMinDate        : TDateTime;
    FTipoCalculo    : AnsiString;
    FCantidadMarcados: Integer;
    procedure HabilitarBotones;
    procedure CargarCDS;
    function ObtenerNK: Integer;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

var
  frmGeneracionGastosGestionCobranza: TfrmGeneracionGastosGestionCobranza;

implementation

{$R *.dfm}

procedure TfrmGeneracionGastosGestionCobranza.btnSalirClick(Sender: TObject);
begin
    if not FProcesando then Close;
end;

procedure TfrmGeneracionGastosGestionCobranza.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmGeneracionGastosGestionCobranza.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := (not FProcesando) and (not FCargandoDatos);
end;

{******************************** Function Header ******************************
Function Name: inicializar
Author : ndonadio
Date Created : 17/10/2005
Description :   Inicializa el formulario...
Parameters : Titulo: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmGeneracionGastosGestionCobranza.Inicializar(Titulo: AnsiString): boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE = 'No se puede inicializar el formulario';
    ERROR_CANT_GET_PAR_GRAL = 'Error al intentar obtener el Par�metro General "%s".';
const
    CANT_DIAS_GENERACION_GASTOS_GESTION = 'CANT_DIAS_GENERACION_GASTOS_GESTION';
var
    S: TSize;
begin
    Result := False;
    Caption := Titulo;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    //lblNKEspecifica.Visible := ExisteAcceso('GENERAR_GASTO_COBRANZA_UNA_NK');
    lblAqui.Enabled := ExisteAcceso('GENERAR_GASTO_COBRANZA_UNA_NK');
    if not ObtenerParametroGeneral(DMConnections.BaseCAC,CANT_DIAS_GENERACION_GASTOS_GESTION, FCantidadDias) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANT_GET_PAR_GRAL, [CANT_DIAS_GENERACION_GASTOS_GESTION]), Caption, MB_ICONERROR);
        Exit;
    end;
    try
        FMaxDate := NowBase(DMConnections.BaseCAC);
        FMinDate := EncodeDate(1900,01,01);
    except
        on e:exception do begin
            MsgBoxErr(ERROR_CANNOT_INITIALIZE, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
    FCantidadMarcados       := 0;
    neImporteHasta.ValueInt := 0;
    FProcesando             := False;
    FCargandoDatos          := False;
    FAllSelected            := False;
    FNoneSelected           := True;
    btnLimpiarFiltrosClick(Self);
    lblCantidadMostrada.Caption := '';
    HabilitarBotones;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 17/10/2005
Description : Habilita o deshabilita los botones de acuerdo a
                diversos eventos...
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.HabilitarBotones;
var
    TengoDatos,
    HabilitarFiltrar,
    HabilitarSeleccionar,
    HabilitarProcesar,
    HabilitarSalir      : Boolean;
begin
    with cdsComprobantesVencidos do begin
        TengoDatos := Active AND (not IsEmpty);
    end;
    if TengoDatos then begin
        FAllSelected    := (FCantidadMarcados   = cdsComprobantesVencidos.RecordCount);
        FNoneSelected   := (FCantidadMarcados   = 0);
    end
    else begin
        FAllSelected    := False;;
        FNoneSelected   := True;;
    end;

    HabilitarFiltrar    := (not FCargandoDatos) AND (not FProcesando);
    HabilitarSeleccionar:= TengoDatos AND (not FCargandoDatos) AND (not FProcesando);
    HabilitarProcesar   := TengoDatos AND (not FCargandoDatos) AND (not FProcesando);
    HabilitarSalir      := (not FProcesando) AND (not FCargandoDatos);

    pnlFiltros.Enabled      := HabilitarFiltrar;

    pnlSeleccion.Enabled    := HabilitarSeleccionar;
    btnMarcar.Enabled       := HabilitarSeleccionar;
    btnSelectAll.Enabled    := HabilitarSeleccionar AND (not FAllSelected);
    btnDeselectAll.Enabled  := HabilitarSeleccionar AND (not FNoneSelected);
    btnInvertirSeleccion.Enabled := HabilitarSeleccionar ; // AND (not FNoneSelected) AND (not FAllSelected);

    btnSalir.Enabled    :=  HabilitarSalir;
    btnGenerar.Enabled  :=  HabilitarProcesar AND (FCantidadMarcados > 0);
end;


{******************************** Function Header ******************************
Function Name: btnFiltrarClick
Author : ndonadio
Date Created : 17/10/2005
Description : Aplicar los filtros seleccionados. Reemplaza lo actual y
             se pierde la seleccion realizada....
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.btnFiltrarClick(Sender: TObject);
resourcestring
    ERROR_RETRIEVING_DATA       = 'No se pueden obtener los comprobantes.';
    ERROR_QTY_INVALID           = 'La cantidad ingresada no es correcta. (Cantidad a Mostrar entre 1 y 50.000)';
    ERROR_START_AMOUNT_INVALID  = 'La cantidad ingresada no es correcta. (Importe Desde entre 0 e Importe Hasta)';
    ERROR_END_AMOUNT_INVALID    = 'La cantidad ingresada no es correcta. (Importe Hasta entre Importe Desde y 9.999.999.999)';
    ERROR_DATE_INVALID          = 'La fecha ingresada no es correcta. (Fecha entre %s y %s';
    CAP_SHOWED_QTY              = '%d Comprobantes recuperados.';
const
    FORMATO_FECHA   = 'dd/mm/yyyy';
var
    FileName: AnsiString;
begin

    if not ValidateControls( [neCantidad, neImporteDesde, neImporteHasta, deFechaFiltro],
      [((neCantidad.ValueInt > 0) AND (neCantidad.ValueInt <= 50000)),
       ((neImporteDesde.ValueInt >= 0) AND (neImporteDesde.ValueInt <= neImporteHasta.ValueInt)),
       ((neImporteHasta.ValueInt >= neImporteDesde.ValueInt) AND (neImporteHasta.ValueInt <= 9999999999)),
       ((deFechaFiltro.IsEmpty) OR ((deFechaFiltro.Date >= FMinDate) AND (deFechaFiltro.Date <= FMaxDate)))]
       , Caption,
       [ERROR_QTY_INVALID, ERROR_START_AMOUNT_INVALID, ERROR_END_AMOUNT_INVALID,
        Format(ERROR_DATE_INVALID,[FormatDateTime(FORMATO_FECHA, FMinDate), FormatDateTime(FORMATO_FECHA, FMaxDate)])]) then exit;

    FCargandoDatos := True;
    Screen.Cursor := crHourGlass;
    FileName := TempFile;
    cdsComprobantesVencidos.DisableControls;
    HabilitarBotones;
    Application.ProcessMessages;
    try
        try
            with spObtenerListaComprobantesVencidos, Parameters do begin
                Close;
                // Limpiar parametros...
                Parameters.Refresh;
                // Cargar Parametros de Filtrado...
                ParamByName('@Cantidad').Value := neCantidad.ValueInt;
                if (cbConvenio.Items.Count > 0) AND (cbConvenio.ItemIndex >= 0) then
                  ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
                if not deFechaFiltro.IsEmpty then
                  ParamByName('@FechaVencimiento').Value := FFechaLimite;
                if (neImporteDesde.ValueInt > 0) then
                  ParamByName('@ImporteDesde').Value := neImporteDesde.ValueInt*100;
                if (neImporteHasta.ValueInt > 0) then
                  ParamByName('@ImporteHasta').Value := neImporteHasta.ValueInt*100;
                ParamByName('@SoloEnviadosSerbanc').Value := chkSoloEnviadosSerbanc.Checked;
                Open;
                CargarCDS;
                lblCantidadMostrada.Caption :=  Format(CAP_SHOWED_QTY,[Integer(ParamByName('@Cantidad').Value)]);
            end;
            FTipoCalculo := 'M';
            //dblComprobantesVencidos.Refresh;
        except
            on e:exception do begin
                MsgBoxErr( ERROR_RETRIEVING_DATA, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        cdsComprobantesVencidos.EnableControls;
        FCargandoDatos := False;
        HabilitarBotones;
        Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnGenerarClick
Author : ndonadio
Date Created : 17/10/2005
Description :  Genera los movimientos segun lo definido. Primero actualiza la
               lista de seleccionados en una tabla temporal...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.btnGenerarClick(Sender: TObject);
resourcestring
    ERROR_GENERATING_CHARGES    =   'Error al Generar los Gastos para los Comprobantes Seleccionados.';
    ERROR_PROCESS_FAILED        =   'El Proceso de Generaci�n de Cargos ha fallado.';
    CAP_PROGRESO                =   'Procesados: %d Comprobantes.';
    PROCESS_OK                  =   'El proceso termin� con �xito. Se generaron %d gastos';
var
    CantProcesados: Integer;
    CantGenerados:  Integer;
begin
    FProcesando := True;
    pbProgreso.Visible := True;
    pbProgreso.Position := 0;
    pbProgreso.Min      := 0;
    pbProgreso.Step     := 1;
    pbProgreso.Max      := cdsComprobantesVencidos.RecordCount;
    CantProcesados      := 0;
    CantGenerados       := 0;
    HabilitarBotones;
    Screen.Cursor := crHourglass;
    cdsComprobantesVencidos.DisableControls;
    try
        cdsComprobantesVencidos.First;
        While not cdsComprobantesVencidos.EOF do begin
            try
                if cdsComprobantesVencidos.FieldByName('Marcado').AsBoolean then begin
                    with spAgregarGastoGestionCobranza, Parameters do begin
                        // Inicio una transaccion...
                        DMConnections.BaseCAC.BeginTrans;
                        Parameters.Refresh;
                        ParamByName('@TipoComprobante').Value := cdsComprobantesVencidos.FieldByName('TipoComprobante').asString;
                        ParamByName('@NumeroComprobante').Value := cdsComprobantesVencidos.FieldByName('NumeroComprobante').asInteger;
                        ParamByName('@TotalEnviado').Value := cdsComprobantesVencidos.FieldByName('TotalComprobante').asInteger*100;
                        ParamByName('@usuario').Value := UsuarioSistema;
                        ParamByName('@FechaGasto').Value := cdsComprobantesVencidos.FieldByName('FechaVencimiento').AsDateTime + FCantidadDias;
                        ParamByName('@CodigoConvenio').Value := cdsComprobantesVencidos.FieldByName('CodigoConvenio').asInteger;
                        ParamByName('@TipoCalculo').Value := FTipoCalculo;
                        ParamByName('@ImporteGasto').Value := 0;
                        ExecProc;
                        if ParamByName('@RETURN_VALUE').Value < 0 then begin
                            DMConnections.BaseCAC.RollbackTrans;
                            MsgBoxErr(ERROR_GENERATING_CHARGES, ERROR_PROCESS_FAILED, Caption, MB_ICONSTOP);
                            Exit;
                        end;   // if
                        inc(CantGenerados);
                        // Cierro la transaccion
                        DMConnections.BaseCAC.CommitTrans;
                    end;  // with
                end; // if
            except
                on e:exception do begin
                    DMConnections.BaseCAC.RollbackTrans;
                    MsgBoxErr(ERROR_GENERATING_CHARGES, e.Message, Caption, MB_ICONSTOP);
                    Exit;
                end;
            end;
            inc(CantProcesados);
            lblCantidadMostrada.Caption := Format(CAP_PROGRESO, [CantProcesados]);
            cdsComprobantesVencidos.Next;
            pbProgreso.StepIt;
            Application.ProcessMessages;
        end;     // while
        pbProgreso.Position := pbProgreso.Max;
        MsgBox(Format(PROCESS_OK,[CantGenerados]), Caption, MB_ICONINFORMATION);
    finally
        pbProgreso.Visible := False;
        lblCantidadMostrada.Caption := '';
        FProcesando := False;
        btnLimpiarFiltrosClick(Self);
        Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnSelectAllClick
Author : ndonadio
Date Created : 17/10/2005
Description :   Click en el boton e marcar Todos
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.btnSelectAllClick(Sender: TObject);
var
    bm: TBookmark;
begin
    cdsComprobantesVencidos.DisableControls;
    Screen.Cursor := crHourGlass;
    try
        with cdsComprobantesVencidos do begin
            bm := GetBookmark;
            First;
            while not eof do begin
                if not FieldByName('Marcado').AsBoolean then begin
                    cdsComprobantesVencidos.Edit;
                    FieldByName('Marcado').AsBoolean := True;
                    cdsComprobantesVencidos.Post;
                end;
                next;
            end;
            GotoBookmark(bm);
       end;
    finally
        cdsComprobantesVencidos.EnableControls;
        dblComprobantesVencidos.Refresh;
        Screen.Cursor := crDefault;
        FCantidadMarcados := cdsComprobantesVencidos.RecordCount;
        HabilitarBotones;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnDeselectAllClick
Author : ndonadio
Date Created : 17/10/2005
Description :  Click en el boton de desmarcar TODOS
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.btnDeselectAllClick(Sender: TObject);
var
    bm: TBookmark;
begin
    cdsComprobantesVencidos.DisableControls;
    Screen.Cursor := crHourGlass;
    try
        with cdsComprobantesVencidos do begin
            bm := GetBookmark;
            First;
            while not eof do begin
                if FieldByName('Marcado').AsBoolean then begin
                    cdsComprobantesVencidos.Edit;
                    FieldByName('Marcado').AsBoolean := False;
                    cdsComprobantesVencidos.Post;
                end;
                next;
            end;
            GotoBookmark(bm);
        end;
    finally
        cdsComprobantesVencidos.EnableControls;
        dblComprobantesVencidos.Refresh;
        Screen.Cursor := crDefault;
        FCantidadMarcados := 0;
        HabilitarBotones;
    end;
end;

{******************************** Function Header ******************************
Function Name: peRUTClienteButtonClick
Author : ndonadio
Date Created : 20/10/2005
Description : Abre la ventana de b�squeda de Clientes
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    application.createForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peRUTCliente.Text :=  F.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
			tmrConsultaTimer(nil);
        end;
    end;
    F.free;
end;

{******************************** Function Header ******************************
Function Name: peRUTClienteChange
Author : ndonadio
Date Created : 20/10/2005
Description : Reinicia el timer cada vez que se modifica el peRUTCliente
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.peRUTClienteChange(Sender: TObject);
begin
	if (activeControl <> sender) then Exit;

	tmrConsulta.Enabled := False;
	tmrConsulta.Enabled := True;
end;

{******************************** Function Header ******************************
Function Name: tmrConsultaTimer
Author : ndonadio
Date Created : 20/10/2005
Description : Si cay� el timer, trata de obtener la persona para el RUT
                escrito en el peRUTCliente
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.tmrConsultaTimer(Sender: TObject);
//var                                                                                         //SS_1120_MVI_20130820
//    CodigoConvenio : Variant;                                                               //SS_1120_MVI_20130820
begin
	Screen.Cursor := crHourglass;
    try
        tmrConsulta.Enabled := False;

        cbConvenio.Items.Clear;

        // Obtenemos el cliente seleccionado
      //  with obtenerCliente do begin                                                                                         //SS_1120_MVI_20130820
      //      Close;                                                                                                           //SS_1120_MVI_20130820
      //      Parameters.ParamByName('@CodigoCliente').Value := NULL;                                                          //SS_1120_MVI_20130820
      //      Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;                                          //SS_1120_MVI_20130820
      //      Parameters.ParamByName('@NumeroDocumento').Value := iif(peRUTCLiente.Text <> EmptyStr,                           //SS_1120_MVI_20130820
      //                                                          PadL(Trim(peRUTCLiente.Text), 9, '0'), EmptyStr);            //SS_1120_MVI_20130820
      //      Open;                                                                                                            //SS_1120_MVI_20130820
      //  end;                                                                                                                 //SS_1120_MVI_20130820

        // Obtenemos la lista de convenios para el cliente seleccionado
      //  CodigoConvenio := Null;                                                                                              	//SS_1120_MVI_20130820
      //  With spObtenerConveniosCliente do begin                                                                              	//SS_1120_MVI_20130820
      //      Parameters.ParamByName('@CodigoCliente').Value := obtenerCliente.fieldbyname('CodigoCliente').AsInteger;         	//SS_1120_MVI_20130820
			//Parameters.ParamByName('@IncluirConveniosDeBaja').Value := True;                                                 	//SS_1120_MVI_20130820
      //      Open;                                                                                                            	//SS_1120_MVI_20130820
      //      First;                                                                                                           	//SS_1120_MVI_20130820
      //      while not spObtenerConveniosCliente.Eof do begin                                                                 	//SS_1120_MVI_20130820
      //          cbConvenio.Items.Add(spObtenerConveniosCliente.fieldbyname('NumeroConvenioFormateado').AsString,             	//SS_1120_MVI_20130820
      //            trim(spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString));                                   	//SS_1120_MVI_20130820
      //          spObtenerConveniosCliente.Next;                                                                              	//SS_1120_MVI_20130820
      //      end;                                                                                                             	//SS_1120_MVI_20130820
      //      Close;                                                                                                           	//SS_1120_MVI_20130820
      //      if  cbConvenio.Items.Count > 0 then cbConvenio.ItemIndex := 0;                                                   	//SS_1120_MVI_20130820
      //  end;                                                                                                                 	//SS_1120_MVI_20130820
                                                                                                                               	//SS_1120_MVI_20130820
      CargarConveniosRUT(DMCOnnections.BaseCAC,cbConvenio, 'RUT',                                         						//SS_1120_MVI_20130820
            PadL(peRUTCLiente.Text, 9, '0' ));                                                            						//SS_1120_MVI_20130820
                                                                                                          						//SS_1120_MVI_20130820
      if  cbConvenio.Items.Count > 0 then cbConvenio.ItemIndex := 0;                                      						//SS_1120_MVI_20130820

    finally
        Screen.Cursor := crDefault
    end;
end;

{******************************** Function Header ******************************
Function Name: btnLimpiarFiltrosClick
Author : ndonadio
Date Created : 19/10/2005
Description : Limpia los filtros...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.btnLimpiarFiltrosClick(Sender: TObject);
var
    i: integer;
begin
    // limpia los ordenes
    for i:= 0 to (dblComprobantesVencidos.Columns.Count - 1) do
        dblComprobantesVencidos.Columns[i].Sorting := csNone;
    cbConvenio.Clear;
    cbConvenio.Items.Clear;
    peRUTCliente.Clear;
    neImporteDesde.Clear;
    neImporteHasta.Clear;
    neCantidad.Clear;
    deFechaFiltro.Clear;
    chkSoloEnviadosSerbanc.Checked := False;
    spObtenerListaComprobantesVencidos.Close;
    if cdsComprobantesVencidos.Active then cdsComprobantesVencidos.EmptyDataSet;
    cdsComprobantesVencidos.EnableControls;
    cdsComprobantesVencidos.Close;
    FProcesando := False;
    FCargandoDatos := False;
    lblFechaVencimiento.Caption := '';
    neCantidad.ValueInt := 1000;
    neImporteDesde.ValueInt := 0;
    neImporteHasta.ValueInt := 9999999999;
    lblCantidadMostrada.Caption := '';
    HabilitarBotones;
end;


{******************************** Function Header ******************************
Function Name: CargarCDS
Author : ndonadio
Date Created : 19/10/2005
Description : Carga el CDS
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.CargarCDS;
begin
    if cdsComprobantesVencidos.Active then
        cdsComprobantesVencidos.EmptyDataSet
    else begin
        cdsComprobantesVencidos.CreateDataSet;
        cdsComprobantesVencidos.Open;
    end;
    spObtenerListaComprobantesVencidos.First;

    with spObtenerListaComprobantesVencidos do begin
        FCantidadMarcados   := 0;
        while not EOF do begin
            cdsComprobantesVencidos.Append;
            cdsComprobantesVencidos.FieldByName('TipoComprobante').Value :=  FieldByName('TipoComprobante').Value;
            cdsComprobantesVencidos.FieldByName('NumeroComprobante').Value :=  FieldByName('NumeroComprobante').Value;
            cdsComprobantesVencidos.FieldByName('NumeroConvenio').Value :=  FieldByName('NumeroConvenio').Value;
            cdsComprobantesVencidos.FieldByName('CodigoConvenio').Value :=  FieldByName('CodigoConvenio').Value;
            cdsComprobantesVencidos.FieldByName('FechaEmision').Value :=  FieldByName('FechaEmision').Value;
            cdsComprobantesVencidos.FieldByName('FechaVencimiento').Value :=  FieldByName('FechaVencimiento').Value;
            cdsComprobantesVencidos.FieldByName('TotalComprobante').Value :=  FieldByName('TotalComprobante').Value;
            cdsComprobantesVencidos.FieldByName('TotalAPagar').Value :=  FieldByName('TotalAPagar').Value;
            cdsComprobantesVencidos.FieldByName('EnviadoSerbanc').Value :=  FieldByName('EnviadoSerbanc').Value;
            cdsComprobantesVencidos.FieldByName('FechaActualizadoSerbanc').Value :=  FieldByName('FechaActualizadoSerbanc').Value;
            cdsComprobantesVencidos.FieldByName('Marcado').Value :=  FieldByName('EnviadoSerbanc').Value;
            cdsComprobantesVencidos.Post;
            if FieldByName('EnviadoSerbanc').Value then begin
                FNoneSelected := False;
                inc(FCantidadMarcados);
            end;
            Next;
        end;
        cdsComprobantesVencidos.First;
        Close;
    end;
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TfrmGeneracionGastosGestionCobranza.cbConvenioDrawItem
  Date:      20-August-2013
  Firm:
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
  Firma     :   SS_1120_MVI_20130820
-----------------------------------------------------------------------------}
procedure TfrmGeneracionGastosGestionCobranza.cbConvenioDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin

  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenio.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                               
end;
// END:SS_1120_MVI_20130820 -------------------------------------------------

{******************************** Function Header ******************************
Function Name: dblComprobantesVEncidosDrawText
Author : ndonadio
Date Created : 20/10/2005
Description : Dibuja los checkbox en las columnas que corresponden
                y formatea los importes
Parameters : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean
Return Value : None

Revision 1:
    Author : ggomez
    Date : 05/06/2006
    Description : Agregu� la sentencia DefaultDraw := False, para que se pinten
        los tildes de las columnas en las que se muestra la imagen del tilde.
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.dblComprobantesVEncidosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp, Blanco: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcado') then begin
            DefaultDraw := False;
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (cdsComprobantesVencidos.FieldByName('Marcado').AsBoolean) then
                Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo balnco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;

        if (Column.FieldName = 'EnviadoSerbanc') then begin
            DefaultDraw := False;
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (cdsComprobantesVencidos.FieldByName('EnviadoSerbanc').AsBoolean) then
            Imagenes.GetBitmap(0, Bmp)
            else
            Imagenes.GetBitmap(1, Bmp);

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo balnco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;
        if (Column.FieldName = 'TotalAPagar') then Text := FormatFloat(FORMATO_IMPORTE, StrToInt(Text));
        if (Column.FieldName = 'TotalComprobante') then Text := FormatFloat(FORMATO_IMPORTE, StrToInt(Text));
    end;
end;


{******************************** Function Header ******************************
Function Name: btnMarcarClick
Author : ndonadio
Date Created : 20/10/2005
Description : Marca o Desmarca un Item
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.btnMarcarClick(Sender: TObject);
begin
    cdsComprobantesVencidos.Edit;
    cdsComprobantesVencidos.FieldByName('Marcado').AsBoolean := not cdsComprobantesVencidos.FieldByName('Marcado').AsBoolean;
    cdsComprobantesVencidos.Post;
    dblComprobantesVencidos.Refresh;
    if cdsComprobantesVencidos.FieldByName('Marcado').AsBoolean then inc(FCantidadMarcados)
    else dec(FCantidadMarcados);
    HabilitarBotones;
end;

{******************************** Function Header ******************************
Function Name: btnInvertirSeleccionClick
Author : ndonadio
Date Created : 20/10/2005
Description : Invierte la seleccion
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.btnInvertirSeleccionClick(Sender: TObject);
var
    bm: TBookmark;
begin
    cdsComprobantesVencidos.DisableControls;
    Screen.Cursor := crHourGlass;
    try
        with cdsComprobantesVencidos do begin
            bm := GetBookmark;
            First;
            while not eof do begin
                cdsComprobantesVencidos.Edit;
                FieldByName('Marcado').AsBoolean := not FieldByName('Marcado').AsBoolean;
                cdsComprobantesVencidos.Post;
                next;
            end;
            GotoBookmark(bm);
       end;
    finally
        cdsComprobantesVencidos.EnableControls;
        dblComprobantesVencidos.Refresh;
        Screen.Cursor := crDefault;
        FCantidadMarcados :=  cdsComprobantesVencidos.RecordCount - FCantidadMarcados;
        HabilitarBotones;
    end;
end;

{******************************** Function Header ******************************
Function Name: lblAquiClick
Author : ndonadio
Date Created : 20/10/2005
Description : Obtiene el Comprobante seleccionado.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.lblAquiClick(Sender: TObject);
resourcestring
    ERROR_RETRIEVING_DATA   = 'No se pueden obtener los comprobantes.';
    THERE_IS_NOT_SUCH_NK    = 'La Nota de Cobro N� %d no existe o no est� en condiciones ' + CRLF +
      'de que se le generen Gastos por Gesti�n de Cobranza';

    CAP_SHOWED_QTY  = '%d Comprobantes recuperados.';
const
    FORMATO_FECHA   = 'dd/mm/yyyy';
var
    NumComp: Integer;
begin
    if not lblAqui.Enabled then Exit;
    NumComp := ObtenerNK;
    if NumComp <= 0 then Exit;
    FCargandoDatos := True;
    btnLimpiarFiltrosClick(Self);
    Screen.Cursor := crHourGlass;
    cdsComprobantesVencidos.DisableControls;
    HabilitarBotones;
    Application.ProcessMessages;
    try
        try
            with spObtenerListaComprobantesVencidos, Parameters do begin
                Close;
                // Limpiar parametros...
                Parameters.Refresh;
                // Cargar Parametros de Filtrado...
                ParamByName('@Cantidad').Value := 1;
                ParamByName('@TipoComprobante').Value := 'NK';
                ParamByName('@NumeroComprobante').Value := NumComp;
                Open;
                CargarCDS;
                lblCantidadMostrada.Caption :=  Format(CAP_SHOWED_QTY,[Integer(ParamByName('@Cantidad').Value)]);
            end;
            FTipoCalculo := 'I';
            dblComprobantesVencidos.Refresh;
        except
            on e:exception do begin
                MsgBoxErr( ERROR_RETRIEVING_DATA, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
        if cdsComprobantesVencidos.IsEmpty then MsgBox(Format(THERE_IS_NOT_SUCH_NK, [NumComp]), Caption, MB_ICONINFORMATION);
    finally
        cdsComprobantesVencidos.EnableControls;
        FCargandoDatos := False;
        HabilitarBotones;
        Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: dblComprobantesVEncidosKeyPress
Author : ndonadio
Date Created : 20/10/2005
Description : Acciona el button de marcar al presionar la barra espaciadora sobre
                la grilla
Parameters : Sender: TObject; var Key: Char
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.dblComprobantesVEncidosKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = ' ' ) and (btnMarcar.Enabled) then
      btnMarcar.Click;
end;

{******************************** Function Header ******************************
Function Name: dblComprobantesVEncidosDblClick
Author : ndonadio
Date Created : 20/10/2005
Description : Acciona el button de marcar al hacer doble-click sobre
                la grilla
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.dblComprobantesVEncidosDblClick(Sender: TObject);
begin
    if btnMarcar.Enabled then btnMarcar.Click;
end;

procedure TfrmGeneracionGastosGestionCobranza.dblComprobantesVEncidosColumns0HeaderClick(
  Sender: TObject);
begin
    if (cdsComprobantesVencidos.Active) then
        if  (TDBListExColumn(Sender).Sorting = csNone) then begin
            cdsComprobantesVencidos.IndexFieldNames := TDBListExColumn(Sender).FieldName;
            dblComprobantesVencidos.Refresh;
            TDBListExColumn(Sender).Sorting := csDescending
        end
        else begin
            cdsComprobantesVencidos.IndexFieldNames := '';
            dblComprobantesVencidos.Refresh;
            TDBListExColumn(Sender).Sorting := csNone;
        end;
end;

function TfrmGeneracionGastosGestionCobranza.ObtenerNK: Integer;
var
    f: TfrmSeleccionarNKAGenerarGastoCobranza;
begin
    Result := -1;
    Application.CreateForm(TfrmSeleccionarNKAGenerarGastoCobranza,f);
    if f.Inicializar(Caption) then
        if f.ShowModal = mrOK then Result := f.neNK.ValueInt;

    f.Release;
end;

{******************************** Function Header ******************************
Function Name: deFechaFiltroExit
Author : ndonadio
Date Created : 26/10/2005
Description : Valido la fecha al salir del DateEdit
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmGeneracionGastosGestionCobranza.deFechaFiltroExit(Sender: TObject);
resourcestring
    ERROR_DATE_INVALID          = 'La fecha ingresada no es correcta. (Fecha entre %s y %s';
    CAP_LIMIT_DATE  = 'Fecha M�x. de Vencimiento: %s ( %d d�as de plazo)';
const
    FORMATO_FECHA   = 'dd/mm/yyyy';
begin

    if not ValidateControls( [deFechaFiltro],
      [(deFechaFiltro.IsEmpty) OR ((deFechaFiltro.Date >= FMinDate) AND (deFechaFiltro.Date <= FMaxDate))]
       , Caption,
       [Format(ERROR_DATE_INVALID,[FormatDateTime(FORMATO_FECHA, FMinDate), FormatDateTime(FORMATO_FECHA, FMaxDate)])]) then exit;

    if not deFechaFiltro.IsEmpty then begin
        FFechaLimite := deFechaFiltro.Date - FCantidadDias;
        lblFechaVencimiento.Caption := Format( CAP_LIMIT_DATE, [FormatDateTime('dd/mm/yyyy', FFechaLimite), FCantidadDias]);
    end
    else begin
        FFechaLimite := FMaxDate;
        lblFechaVencimiento.Caption := '';
    end;

end;

end.
