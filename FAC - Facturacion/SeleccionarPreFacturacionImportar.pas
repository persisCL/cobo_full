unit SeleccionarPreFacturacionImportar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, ListBoxEx, DBListEx, UtilProc;

type
  TfrmSeleccionarPreFacturacionImportar = class(TForm)
    lstProcesos: TDBListEx;
    spPREFAC_ProcesosFacturacion_SELECT: TADOStoredProc;
    dsProcesos: TDataSource;
    pnl1: TPanel;
    pnl2: TPanel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    lbl1: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FNumeroProcesoFacturacionSel: Integer;
    function Inicializar(CodigoGrupoFacturacion, NumeroProcesoFacturacionIgnorar: Integer): Boolean;
  end;

var
  frmSeleccionarPreFacturacionImportar: TfrmSeleccionarPreFacturacionImportar;

implementation

{$R *.dfm}

function TfrmSeleccionarPreFacturacionImportar.Inicializar(CodigoGrupoFacturacion: Integer; NumeroProcesoFacturacionIgnorar: Integer): Boolean;
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_SELECT = 'Error al obtener procesos de Pre Facturación';
begin
    Result := True;

     try
        spPREFAC_ProcesosFacturacion_SELECT.Close;
        spPREFAC_ProcesosFacturacion_SELECT.Parameters.Refresh;
        spPREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := CodigoGrupoFacturacion;
        spPREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@NumeroProcesoFacturacionIgnorar').Value := NumeroProcesoFacturacionIgnorar;
        spPREFAC_ProcesosFacturacion_SELECT.Open;
    except
        on e: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR_SELECT, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

procedure TfrmSeleccionarPreFacturacionImportar.btnAceptarClick(
  Sender: TObject);
resourcestring
    MSG_SIN_CONVENIOS = 'El proceso de Pre-Facturación seleccionado no posee selección de Convenios para importar.';
begin
    if not spPREFAC_ProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsInteger > 0 then
        Exit;

    if (spPREFAC_ProcesosFacturacion_SELECT.FieldByName('IncluidosManuales').AsInteger = 0)
      and (spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ExcluidosManuales').AsInteger = 0) then begin
        MsgBox(MSG_SIN_CONVENIOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;

    FNumeroProcesoFacturacionSel := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsInteger;

    ModalResult := mrOk;
end;

procedure TfrmSeleccionarPreFacturacionImportar.btnCancelarClick(
  Sender: TObject);
begin
    ModalResult := mrCancel;
    Close;
end;

procedure TfrmSeleccionarPreFacturacionImportar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
