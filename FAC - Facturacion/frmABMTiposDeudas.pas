{********************************** File Header *********************************
File Name   : frmABMTiposDeudas
Author      : nefernandez
Date Created: 26/01/2009
Language    : ES-AR
Description : Realiza la administraci�n de los tipos de deudas.
********************************************************************************}
unit frmABMTiposDeudas;

interface

uses
  UtilProc,
  UtilDB,
  Util,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DbList, Abm_obj, DB, ADODB, StdCtrls, DmiCtrls;

type
  TABMTiposDeudasForm = class(TForm)
    tbTiposDeudas: TAbmToolbar;
    lbTiposDeudas: TAbmList;
    pnlBotones: TPanel;
    pnlTiposDeudas: TPanel;
    spObtenerTiposDeudas: TADOStoredProc;
    Notebook: TNotebook;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    lblCodigo: TLabel;
    lblTipoDeuda: TLabel;
    lblDescripcion: TLabel;
    txtTipoDeuda: TEdit;
    txtDescripcion: TEdit;
    neCodigoTipoDeuda: TNumericEdit;
    spActualizarTiposDeudas: TADOStoredProc;
    spEliminarTiposDeudas: TADOStoredProc;
    procedure lbTiposDeudasDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure lbTiposDeudasClick(Sender: TObject);
    procedure tbTiposDeudasClose(Sender: TObject);
    procedure lbTiposDeudasInsert(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure lbTiposDeudasEdit(Sender: TObject);
    procedure lbTiposDeudasRefresh(Sender: TObject);
    procedure lbTiposDeudasDelete(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  ABMTiposDeudasForm: TABMTiposDeudasForm;

implementation

{$R *.dfm}


{******************************** Function Header ******************************
Function Name: Limpiar_Campos
Author: nefernandez
Date Created: 26/01/2009
Description: Vac�a todos los campos del panel de detalle
Parameters: None
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.Limpiar_Campos;
begin
    neCodigoTipoDeuda.Clear;
    txtTipoDeuda.Clear;
	txtDescripcion.clear;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author: nefernandez
Date Created: 26/01/2009
Description: Proceso inicial que se ejecuta la crear el formulario. Presenta los
registros de TiposDeudas en la grilla
Parameters: None
Return Value: Boolean
*******************************************************************************}
procedure TABMTiposDeudasForm.btnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION		    = 'Actualizar tipo de deuda';
    MSG_ACTUALIZAR_ERROR		    = 'No se pudieron actualizar los datos del tipo de deuda';
    MSG_VALIDAR_CAPTION 		    = 'Validar datos del tipo de deuda';
    MSG_VALIDAR_TIPODEUDA           = 'Debe ingresar el tipo de deuda';
	MSG_VALIDAR_DESCRIPCION 	    = 'Debe ingresar una descripci�n';
var
    CodigoTipoDeuda: Integer;
    CodigoOperacion: Char;
begin
   	if (Trim(txtTipoDeuda.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_TIPODEUDA, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtTipoDeuda);
		Exit;
    end;

   	if (Trim(txtDescripcion.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_DESCRIPCION, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtDescripcion);
		Exit;
    end;

	Screen.Cursor := crHourGlass;
    try
        if lbTiposDeudas.Estado = Alta then begin
            CodigoOperacion := 'I';
            CodigoTipoDeuda := 0;
        end else begin
            CodigoOperacion := 'U';
            CodigoTipoDeuda := spObtenerTiposDeudas.FieldByName('CodigoTipoDeuda').AsInteger;
        end;

        spActualizarTiposDeudas.Parameters.ParamByName('@TipoOperacion').Value := CodigoOperacion;
        spActualizarTiposDeudas.Parameters.ParamByName('@CodigoTipoDeuda').Value := CodigoTipoDeuda;;
        spActualizarTiposDeudas.Parameters.ParamByName('@TipoDeuda').Value := txtTipoDeuda.Text;
        spActualizarTiposDeudas.Parameters.ParamByName('@Descripcion').Value := txtDescripcion.Text;
        spActualizarTiposDeudas.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;

        spActualizarTiposDeudas.ExecProc;
    except
        on E: Exception do begin
            MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
        end;
    end;

	pnlTiposDeudas.Enabled := False;
	lbTiposDeudas.Estado := Normal;
	lbTiposDeudas.Enabled := True;
	lbTiposDeudas.Reload;
	lbTiposDeudas.SetFocus;
	Notebook.PageIndex := 0;
	Screen.Cursor := crDefault;

end;

{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author: nefernandez
Date Created: 27/01/2009
Description: Cancela la edici�n del tipo de deuda
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.btnCancelarClick(Sender: TObject);
begin
	pnlBotones.Enabled := False;
	lbTiposDeudas.Estado := Normal;
	lbTiposDeudas.Enabled := True;
	lbTiposDeudas.SetFocus;
	Notebook.PageIndex := 0;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author: nefernandez
Date Created: 26/01/2009
Description: Llama al metodo para cerrar el formulario
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author: nefernandez
Date Created: 26/01/2009
Description: Cierra el formulario
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author: nefernandez
Date Created: 27/01/2009
Description: Proceso inicial que se ejecuta la crear el formulario. Presenta los
registros de TiposDeudas activos en la grilla
Parameters: None
Return Value: Boolean
*******************************************************************************}
function TABMTiposDeudasForm.Inicializar: Boolean;
resourcestring
    ERROR_MSG = 'Error de Inicializacion';
begin
    Result := False;
    FormStyle := fsMDIChild;
    CenterForm(Self);
    try

    	if not OpenTables([spObtenerTiposDeudas]) then exit;
        lbTiposDeudas.Reload;
   	    Notebook.PageIndex := 0;

        Result := True;
    except
        On E: exception do begin
            exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: lbTiposDeudasClick
Author: nefernandez
Date Created: 26/01/2009
Description: Proceso que se ejecuta al seleccionar un registro de la grilla.
Recupera todos los campos del registro seleccionado en el panel de detalle
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.lbTiposDeudasClick(Sender: TObject);
begin
    neCodigoTipoDeuda.Value := spObtenerTiposDeudas.FieldByName('CodigoTipoDeuda').AsInteger;
    txtTipoDeuda.Text := spObtenerTiposDeudas.FieldByName('TipoDeuda').AsString;
    txtDescripcion.Text := spObtenerTiposDeudas.FieldByName('Descripcion').AsString;
end;

{******************************** Function Header ******************************
Function Name: lbTiposDeudasDelete
Author: nefernandez
Date Created: 26/01/2009
Description: Proceso para eliminar l�gicamente el registro seleccionado en la grilla
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.lbTiposDeudasDelete(Sender: TObject);
resourcestring
	MSG_ELIMINAR_TIPODEUDA_CAPTION            = 'Eliminar Tipo de Deuda';
	MSG_ELIMINAR_TIPODEUDA_ERROR              = 'No se puede eliminar este Tipo de Deuda';
	MSG_ELIMINAR_TIPODEUDA_QUESTION           = '�Est� seguro de querer eliminar este Tipo de Deuda?';
begin
	Screen.Cursor := crHourGlass;
	if MsgBox( MSG_ELIMINAR_TIPODEUDA_QUESTION, MSG_ELIMINAR_TIPODEUDA_CAPTION, MB_YESNO) = IDYES then begin
        try
            spEliminarTiposDeudas.Parameters.ParamByName('@CodigoTipoDeuda').Value := spObtenerTiposDeudas.FieldByName('CodigoTipoDeuda').AsInteger;
            spEliminarTiposDeudas.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;

            spEliminarTiposDeudas.ExecProc;
        except
            On E: exception do begin
                MsgBoxErr(MSG_ELIMINAR_TIPODEUDA_ERROR, E.message, MSG_ELIMINAR_TIPODEUDA_CAPTION, MB_ICONSTOP);
            end;
        end;
		lbTiposDeudas.Reload;
	end;
	lbTiposDeudas.Estado       := Normal;
	lbTiposDeudas.Enabled      := True;
	pnlTiposDeudas.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
    spObtenerTiposDeudas.Close;
    spObtenerTiposDeudas.Open;
end;

{******************************** Function Header ******************************
Function Name: lbTiposDeudasDrawItem
Author: nefernandez
Date Created: 26/01/2009
Description: Coloca el valor de las columnas en la grilla
Parameters: Sender: TDBList, Tabla: TDataSet, Rect: TRect, State: TOwnerDrawState, Cols: TColPositions
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.lbTiposDeudasDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	with Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(FieldByName('CodigoTipoDeuda').AsString));
		TextOut(Cols[1], Rect.Top, Trim(FieldByName('TipoDeuda').AsString));
		TextOut(Cols[2], Rect.Top, Trim(FieldByName('Descripcion').AsString));
	end;
end;

{******************************** Function Header ******************************
Function Name: lbTiposDeudasEdit
Author: nefernandez
Date Created: 27/01/2009
Description: Pone la pantalla en modo de edici�n (del registro seleccionado)
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.lbTiposDeudasEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	lbTiposDeudas.Estado     := Modi;
	lbTiposDeudas.Enabled    := False;
	pnlTiposDeudas.Enabled   := True;
	Notebook.PageIndex := 1;
    txtTipoDeuda.SetFocus;
	Screen.Cursor    := crDefault;
end;

{******************************** Function Header ******************************
Function Name: lbTiposDeudasInsert
Author: nefernandez
Date Created: 27/01/2009
Description: Pone la pantalla en modo de inserci�n de un nuevo registro
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.lbTiposDeudasInsert(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
    Limpiar_Campos;
    lbTiposDeudas.Estado := Alta;
    lbTiposDeudas.Enabled := False;
    pnlTiposDeudas.Enabled := True;
    Notebook.PageIndex := 1;
	txtTipoDeuda.SetFocus;
	Screen.Cursor    := crDefault;
end;

{******************************** Function Header ******************************
Function Name: lbTiposDeudasRefresh
Author: nefernandez
Date Created: 27/01/2009
Description: Proceso de refresh de la grilla. Si no hay registros en la grilla,
se vac�a los campos del panel de detalle
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.lbTiposDeudasRefresh(Sender: TObject);
begin
	if lbTiposDeudas.Empty then Limpiar_Campos;
end;

{******************************** Function Header ******************************
Function Name: tbTiposDeudasClose
Author: nefernandez
Date Created: 27/01/2009
Description: Llama al m�todo de cierre del formulario
Parameters: Sender: TObject
Return Value: None
*******************************************************************************}
procedure TABMTiposDeudasForm.tbTiposDeudasClose(Sender: TObject);
begin
	 Close;
end;

end.
