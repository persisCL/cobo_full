unit frmABMEmpresasRecaudadoras;

{********************************** File Header ********************************
File Name : frmABMEmpresasRecaudadoras.pas
Author : mpiazza
Date Created: 02/02/2009
Language : ES-AR
Description : SS-146 Este formulario es un ABM de empresa recaudadora,
contiene dos frames, uno de telefono y otro de domicilio

Revision 1:
    Author : mpiazza
    Date : 04/06/2009
    Description : SS 803: Se agrega el campo Judicial
*******************************************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbList, Abm_obj, DB, ADODB, StdCtrls, ExtCtrls, DmiCtrls,
  PeaTypes,
  PeaProcs,
  UtilProc,
  UtilDB,
  Util,
  DMConnection,
  FreDomicilio, FreTelefono  ;

type
  TTABMEmpresasRecaudadorasForm = class(TForm)
    tbEmpresasRecaudadoras: TAbmToolbar;
    pnlEmpresasRecaudadoras: TPanel;
    lblCodigo: TLabel;
    lblNombreEmpresa: TLabel;
    txtNombreEmpresa: TEdit;
    neCodigoEmpresasRecaudadoras: TNumericEdit;
    pnlBotones: TPanel;
    Notebook: TNotebook;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    spEliminarEmpresasRecaudadoras: TADOStoredProc;
    spActualizarEmpresasRecaudadoras: TADOStoredProc;
    spObtenerEmpresasRecaudadoras: TADOStoredProc;
    lbEmpresasRecaudadoras: TAbmList;
    freDomicilio: TFrameDomicilio;
    txtDescripcion: TEdit;
    lblDescripcion: TLabel;
    txtAliasEmpresa: TEdit;
    lblAliasEmpresa: TLabel;
    txtGiro: TEdit;
    Label1: TLabel;
    txtNumeroDocumento: TEdit;
    lblNumeroDocumento: TLabel;
    txtCodigoDocumento: TEdit;
    txteMail: TEdit;
    lbleMail: TLabel;
    freTelefono: TFrameTelefono;
    chkJudicial: TCheckBox;
    chkpreJudicial: TCheckBox;
    spObtenerEmpresasRecaudadorasCodigoEmpresasRecaudadoras: TAutoIncField;
    spObtenerEmpresasRecaudadorasNombreEmpresa: TStringField;
    spObtenerEmpresasRecaudadorasCodigoDocumento: TStringField;
    spObtenerEmpresasRecaudadorasNumeroDocumento: TStringField;
    spObtenerEmpresasRecaudadorasAliasEmpresa: TStringField;
    spObtenerEmpresasRecaudadorasGiro: TStringField;
    spObtenerEmpresasRecaudadorasDescripcion: TStringField;
    spObtenerEmpresasRecaudadoraseMail: TStringField;
    spObtenerEmpresasRecaudadorasCodigoPais: TStringField;
    spObtenerEmpresasRecaudadorasCodigoRegion: TStringField;
    spObtenerEmpresasRecaudadorasCodigoComuna: TStringField;
    spObtenerEmpresasRecaudadorasCodigoCalle: TIntegerField;
    spObtenerEmpresasRecaudadorasComuna: TStringField;
    spObtenerEmpresasRecaudadorasRegion: TStringField;
    spObtenerEmpresasRecaudadorasDescripcionCiudad: TStringField;
    spObtenerEmpresasRecaudadorasNumero: TStringField;
    spObtenerEmpresasRecaudadorasPiso: TWordField;
    spObtenerEmpresasRecaudadorasDpto: TStringField;
    spObtenerEmpresasRecaudadorasCalleDesnormalizada: TStringField;
    spObtenerEmpresasRecaudadorasDetalle: TStringField;
    spObtenerEmpresasRecaudadorasCodigoPostal: TStringField;
    spObtenerEmpresasRecaudadorasCodigoSegmento: TIntegerField;
    spObtenerEmpresasRecaudadorasCodigoDireccionExterno: TStringField;
    spObtenerEmpresasRecaudadorasCodigoArea: TSmallintField;
    spObtenerEmpresasRecaudadorasValor: TStringField;
    spObtenerEmpresasRecaudadorasAnexo: TIntegerField;
    spObtenerEmpresasRecaudadorasCodigoTipoMedioContacto: TWordField;
    spObtenerEmpresasRecaudadorasHorarioDesde: TDateTimeField;
    spObtenerEmpresasRecaudadorasHorarioHasta: TDateTimeField;
    spObtenerEmpresasRecaudadorasFechaHoraCreacion: TDateTimeField;
    spObtenerEmpresasRecaudadorasUsuarioCreacion: TStringField;
    spObtenerEmpresasRecaudadorasFechaHoraModificacion: TDateTimeField;
    spObtenerEmpresasRecaudadorasUsuarioModificacion: TStringField;
    spObtenerEmpresasRecaudadorasJudicial: TBooleanField;
    spObtenerEmpresasRecaudadorasPreJudicial: TBooleanField;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbEmpresasRecaudadorasClick(Sender: TObject);
    procedure lbEmpresasRecaudadorasDelete(Sender: TObject);
    procedure lbEmpresasRecaudadorasRefresh(Sender: TObject);
    procedure lbEmpresasRecaudadorasInsert(Sender: TObject);
    procedure lbEmpresasRecaudadorasEdit(Sender: TObject);
    procedure lbEmpresasRecaudadorasDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure tbEmpresasRecaudadorasClose(Sender: TObject);
  private
    Domicilio: TDatosDomicilio;
    procedure Limpiar_Campos;
  public
    function Inicializar: Boolean;
    { Public declarations }
  end;

var
  TABMEmpresasRecaudadorasForm: TTABMEmpresasRecaudadorasForm;

implementation

{$R *.dfm}

{ TTABMEmpesasRecaudadorasForm }

{-----------------------------------------------------------------------------
  Procedure:btnAceptarClick
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: realiza las validaciones de los datos y guarda en la base de datos

    Revision 1:
    Author : mpiazza
    Date : 04/06/2009
    Description : SS 803: Se agrega el campo Judicial
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.btnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION		    = 'Actualizar Empresa Recaudadora';
  MSG_ACTUALIZAR_ERROR		      = 'No se pudieron actualizar los datos de Empresa Recaudadora';
  MSG_VALIDAR_CAPTION 		      = 'Validar datos de Empresa Recaudadora';
  MSG_VALIDAR_NOMBREEMPRESA     = 'Debe ingresar un nombre';
 	MSG_VALIDAR_DESCRIPCION 	    = 'Debe ingresar una descripci�n';
 	MSG_VALIDAR_ALIAS 	          = 'Debe ingresar un Alias';

 	MSG_VALIDAR_TELEFONO 	          = 'Debe ingresar un Tel�fono';

  MSG_VALIDAR_NUMEROCALLE 	    = 'Debe ingresar un n�mero de calle';
  MSG_VALIDAR_NUMERODOCUMENTO   = 'Debe ingresar un n�mero de Documento correcto';
var
    CodigoTipoDeuda: Integer;
    CodigoOperacion: Char;
begin
   	if (Trim(txtNombreEmpresa.Text) = '') then begin
      MsgBoxBalloon( MSG_VALIDAR_NOMBREEMPRESA, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtNombreEmpresa);
      Exit;
    end;

   	if (Trim(txtAliasEmpresa.Text) = '') then begin
      MsgBoxBalloon( MSG_VALIDAR_ALIAS, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtAliasEmpresa);
      Exit;
    end;

   	if (Trim(txtDescripcion.Text) = '') then begin
      MsgBoxBalloon( MSG_VALIDAR_DESCRIPCION, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtDescripcion);
      Exit;
    end;

    if not freTelefono.ValidarTelefono then begin
      Exit;
    end;


   	if not (freDomicilio.ValidarDomicilio ) then begin
      Exit;
    end;

   	if not ValidarRUT(DMConnections.BaseCAC , Trim(txtNumeroDocumento.text) + trim(txtCodigoDocumento.text)) then begin
      MsgBoxBalloon( MSG_VALIDAR_NUMERODOCUMENTO, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtNumeroDocumento);
      Exit;
    end;

  	Screen.Cursor := crHourGlass;
    try
      with spActualizarEmpresasRecaudadoras.Parameters do begin
        ParamByName('@CodigoEmpresasRecaudadoras').Value := neCodigoEmpresasRecaudadoras.ValueInt;
        ParamByName('@NombreEmpresa').Value := txtNombreEmpresa.Text;
        ParamByName('@Descripcion').Value := txtDescripcion.Text;
        ParamByName('@AliasEmpresa').Value := txtAliasEmpresa.Text;
        ParamByName('@Giro').Value := txtGiro.text;

        ParamByName('@NumeroDocumento').Value := txtNumeroDocumento.text;
        ParamByName('@CodigoDocumento').Value := txtCodigoDocumento.text;

        //frame de domicilio
        ParamByName('@Numero').Value := freDomicilio.Numero;
        ParamByName('@CodigoPais').Value := freDomicilio.Pais ;
        ParamByName('@CodigoRegion').Value := freDomicilio.Region;
        ParamByName('@CodigoComuna').Value := freDomicilio.Comuna;
        ParamByName('@CodigoCalle').Value := iif(freDomicilio.EsCalleNormalizada, freDomicilio.CodigoCalle, null);
        ParamByName('@DescripcionCiudad').Value := freDomicilio.DescriComuna;

        ParamByName('@Piso').Value := freDomicilio.Piso;
        ParamByName('@Dpto').Value := freDomicilio.Depto;
        ParamByName('@Detalle').Value := freDomicilio.Detalle;
        ParamByName('@CodigoPostal').Value := freDomicilio.CodigoPostal;
        ParamByName('@CodigoSegmento').Value := freDomicilio.CodigoSegmento;
        ParamByName('@CalleDesnormalizada').Value := iif(not (freDomicilio.EsCalleNormalizada),
                                                                    freDomicilio.DescripcionCalle, null);
        ParamByName('@eMail').Value := txteMail.Text;
        //datos del telefono
        ParamByName('@CodigoArea').Value            := FreTelefono.CodigoArea;
        ParamByName('@Valor').Value                 := FreTelefono.NumeroTelefono;
        ParamByName('@CodigoTipoMedioContacto').Value := FreTelefono.CodigoTipoTelefono;
        ParamByName('@HorarioDesde').Value          := FreTelefono.HorarioDesde;
        ParamByName('@HorarioHasta').Value          := FreTelefono.HorarioHasta;
        ParamByName('@Anexo').Value                 := null;

        //datos de auditoria
        ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
        ParamByName('@Usuario').Value :=   UsuarioSistema;

        ParamByName('@Judicial').Value :=  chkJudicial.Checked ;
        ParamByName('@preJudicial').Value :=  chkpreJudicial.Checked;

      end;


        spActualizarEmpresasRecaudadoras.ExecProc;
    except
        on E: Exception do begin
            MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
        end;
    end;

	pnlEmpresasRecaudadoras.Enabled := False;
	lbEmpresasRecaudadoras.Estado := Normal;
	lbEmpresasRecaudadoras.Enabled := True;
	lbEmpresasRecaudadoras.Reload;
	lbEmpresasRecaudadoras.SetFocus;
	Notebook.PageIndex := 0;
  pnlEmpresasRecaudadoras.Visible := false;
  Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Procedure: btnCancelarClick
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Cancela la operacion de edicion, dejando el panel de datos oculto
  y devolviendo el control de la grilla
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.btnCancelarClick(Sender: TObject);
begin
	pnlEmpresasRecaudadoras.Enabled := False;
	lbEmpresasRecaudadoras .Estado := Normal;
	lbEmpresasRecaudadoras.Enabled := True;
	lbEmpresasRecaudadoras.SetFocus;
	Notebook.PageIndex := 0;
  pnlEmpresasRecaudadoras.Visible := false;
	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Procedure: btnSalirClick
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: permite el cierre del formulario MDI
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Procedure: FormClose
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: Permite y acepta el cierre del formulario
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

{-----------------------------------------------------------------------------
  Procedure: Inicializar
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: N/A
  Result: Boolean
  Purpose: inicializa todos los controles y grilla del formulario
-----------------------------------------------------------------------------}
function TTABMEmpresasRecaudadorasForm.Inicializar: Boolean;
resourcestring
    ERROR_MSG = 'Error de Inicializacion';
    STR_PRINC_TEL = 'Tel�fono';
begin

    Result := False;
    FormStyle := fsMDIChild;
    CenterForm(Self);
    try
      freDomicilio.Inicializa();
      FreTelefono.Inicializa(STR_PRINC_TEL, true);
    	if not OpenTables([spObtenerEmpresasRecaudadoras]) then exit;
        lbEmpresasRecaudadoras.Reload;
   	    Notebook.PageIndex := 0;

        Result := True;
    except
        On E: exception do begin
            exit;
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: Limpiar_Campos
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: N/A
  Result: N/A
  Purpose: limpa de datos todos los controles

  Revision 1:
    Author : mpiazza
    Date : 04/06/2009
    Description : SS 803: Se agrega el campo Judicial
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.Limpiar_Campos;
begin
    neCodigoEmpresasRecaudadoras.Clear;
    chkJudicial.Checked := false;
    chkpreJudicial.checked := false;
    txtNombreEmpresa.Text := '';
    txtDescripcion.text := '';
    txtAliasEmpresa.text := '';
    txtGiro.text := '';
    txtNumeroDocumento.Text := '';
    txtCodigoDocumento.text := '';
    txteMail.Text := '';
    freDomicilio.Limpiar;
    FreTelefono.CargarTelefono(CODIGO_AREA_SANTIAGO,'',-1);
end;
{-----------------------------------------------------------------------------
  Procedure: tbEmpresasRecaudadorasClose
  Author:    mpiazza
  Date:      04/02/2009
  Arguments: N/A
  Result: N/A
  Purpose: Sale del formulario
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.tbEmpresasRecaudadorasClose(
  Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Procedure: lbEmpresasRecaudadorasClick
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: cuando se ejecuta el click de edicion, carga todos los datos
  y muestra el panel de datos

  Revision 1:
    Author : mpiazza
    Date : 04/06/2009
    Description : SS 803: Se agrega el campo Judicial
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.lbEmpresasRecaudadorasClick(
  Sender: TObject);
begin
    with spObtenerEmpresasRecaudadoras do begin
      neCodigoEmpresasRecaudadoras.Value := spObtenerEmpresasRecaudadoras.FieldByName('CodigoEmpresasRecaudadoras').AsInteger;
      txtNombreEmpresa.Text := trim(FieldByName('NombreEmpresa').AsString);
      txtDescripcion.Text := trim(FieldByName('Descripcion').AsString);
      txtAliasEmpresa.text := trim(FieldByName('AliasEmpresa').AsString);
      txtGiro.text := trim(FieldByName('Giro').AsString);
      freDomicilio.txt_numeroCalle.Text := trim(FieldByName('Numero').AsString);
      txtNumeroDocumento.text := trim(FieldByName('NumeroDocumento').AsString);
      txtCodigoDocumento.Text := trim(FieldByName('CodigoDocumento').Asstring);
      txteMail.text := trim(FieldByName('eMail').AsString);
      freDomicilio.txt_numeroCalle.Text := trim( FieldByName('Numero').AsString);
      chkJudicial.Checked := FieldByName('@Judicial').AsBoolean  ;
      chkpreJudicial.checked  := FieldByName('@preJudicial').AsBoolean  ;
      with Domicilio do begin
          CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
          CodigoSegmento := iif(FieldByName('CodigoSegmento').IsNull, -1, FieldByName('CodigoSegmento').AsInteger);
          NumeroCalleSTR := trim(FieldByName('Numero').AsString);
          NumeroCalle := FormatearNumeroCalle(FieldByName('Numero').AsString);
          Dpto := FieldByName('Dpto').asstring;
          Detalle := FieldByName('Detalle').AsString;
          CodigoPais := FieldByName('CodigoPais').AsString;
          CodigoRegion := FieldByName('CodigoRegion').AsString;
          CodigoComuna := FieldByName('CodigoComuna').AsString;
          CalleDesnormalizada := iif(FieldByName('CodigoCalle').IsNull, FieldByName('CalleDesnormalizada').AsString,'');
          CodigoPostal := FieldByName('CodigoPostal').AsString;
      end;
      FreDomicilio.CargarDatosDomicilio(Domicilio);
      FreTelefono.CargarTelefono(FieldByName('CodigoArea').Asinteger,FieldByName('Valor').AsString, FieldByName('CodigoTipoMedioContacto').Asinteger, FieldByName('HorarioDesde').Asdatetime, FieldByName('HorarioHasta').Asdatetime);
    end;
    pnlEmpresasRecaudadoras.Visible := true;
end;

{-----------------------------------------------------------------------------
  Procedure: lbEmpresasRecaudadorasDelete
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: dado un registro seleccionado, se marca un registro como borrado
  El sp internamente deja guardado en una tabla de auditoria_ los datos
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.lbEmpresasRecaudadorasDelete(
  Sender: TObject);
resourcestring
	MSG_EMPRESA_RECAUDADORA_CAPTION            = 'Eliminar Empresa Recaudadora';
	MSG_EMPRESA_RECAUDADORA_ERROR              = 'No se puede eliminar esta Empresa Recaudadora';
	MSG_EMPRESA_RECAUDADORA_QUESTION           = '�Est� seguro de querer eliminar esta Empresa Recaudadora?';
begin
	Screen.Cursor := crHourGlass;
	if MsgBox( MSG_EMPRESA_RECAUDADORA_QUESTION, MSG_EMPRESA_RECAUDADORA_CAPTION, MB_YESNO) = IDYES then begin
        try
            spEliminarEmpresasRecaudadoras.Parameters.ParamByName('@CodigoEmpresasRecaudadoras').Value := spObtenerEmpresasRecaudadoras.FieldByName('CodigoEmpresasRecaudadoras').AsInteger;
            spEliminarEmpresasRecaudadoras.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;

            spEliminarEmpresasRecaudadoras.ExecProc;
        except
            On E: exception do begin
                MsgBoxErr(MSG_EMPRESA_RECAUDADORA_ERROR, E.message, MSG_EMPRESA_RECAUDADORA_CAPTION, MB_ICONSTOP);
            end;
        end;
		lbEmpresasRecaudadoras.Reload;
	end;
	lbEmpresasRecaudadoras.Estado       := Normal;
	lbEmpresasRecaudadoras.Enabled      := True;
	pnlEmpresasRecaudadoras.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
    spObtenerEmpresasRecaudadoras.Close;
    spObtenerEmpresasRecaudadoras.Open;
end;

{-----------------------------------------------------------------------------
  Procedure: lbEmpresasRecaudadorasRefresh
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: realiza el refresco de la grilla y de todos los controles
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.lbEmpresasRecaudadorasRefresh(
  Sender: TObject);
begin
	if lbEmpresasRecaudadoras.Empty then Limpiar_Campos;
end;

{-----------------------------------------------------------------------------
  Procedure: lbEmpresasRecaudadorasInsert
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: prepara el formulario para dar de alta un nuevo registro
  en modo de edicion.
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.lbEmpresasRecaudadorasInsert(
  Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
    Limpiar_Campos;
    lbEmpresasRecaudadoras.Estado := Alta;
    lbEmpresasRecaudadoras.Enabled := False;
    pnlEmpresasRecaudadoras.Enabled := True;
    Notebook.PageIndex := 1;
    pnlEmpresasRecaudadoras.Visible := true;
    txtNombreEmpresa.SetFocus;
	  Screen.Cursor    := crDefault;
end;

{-----------------------------------------------------------------------------
  Procedure: lbEmpresasRecaudadorasEdit
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: Sender: TObject
  Result: N/A
  Purpose: inicia todos los valores del formulario para hacer
  una edicion de un registro

  Revision 1:
    Author : mpiazza
    Date : 04/06/2009
    Description : SS 803: Se agrega el campo Judicial
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.lbEmpresasRecaudadorasEdit(
  Sender: TObject);
begin
    Screen.Cursor    := crHourGlass;
    with spObtenerEmpresasRecaudadoras do begin
      neCodigoEmpresasRecaudadoras.Value := spObtenerEmpresasRecaudadoras.FieldByName('CodigoEmpresasRecaudadoras').AsInteger;
      txtNombreEmpresa.Text := trim(FieldByName('NombreEmpresa').AsString);
      txtDescripcion.Text := trim(FieldByName('Descripcion').AsString);
      txtAliasEmpresa.text := trim(FieldByName('AliasEmpresa').AsString);
      txtGiro.text := trim(FieldByName('Giro').AsString);
      freDomicilio.txt_numeroCalle.Text := trim(FieldByName('Numero').AsString);
      txtNumeroDocumento.text := trim(FieldByName('NumeroDocumento').AsString);
      txtCodigoDocumento.Text := trim(FieldByName('CodigoDocumento').Asstring);
      txteMail.text := trim(FieldByName('eMail').AsString);
      freDomicilio.txt_numeroCalle.Text := trim( FieldByName('Numero').AsString);
      chkJudicial.Checked := FieldByName('Judicial').AsBoolean;
      chkPreJudicial.Checked := FieldByName('PreJudicial').AsBoolean;
      with Domicilio do begin
          CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
          CodigoSegmento := iif(FieldByName('CodigoSegmento').IsNull, -1, FieldByName('CodigoSegmento').AsInteger);
          NumeroCalleSTR := trim(FieldByName('Numero').AsString);
          NumeroCalle := FormatearNumeroCalle(FieldByName('Numero').AsString);
          Dpto := FieldByName('Dpto').asstring;
          Detalle := FieldByName('Detalle').AsString;
          CodigoPais := FieldByName('CodigoPais').AsString;
          CodigoRegion := FieldByName('CodigoRegion').AsString;
          CodigoComuna := FieldByName('CodigoComuna').AsString;
          CalleDesnormalizada := iif(FieldByName('CodigoCalle').IsNull, FieldByName('CalleDesnormalizada').AsString,'');
          CodigoPostal := FieldByName('CodigoPostal').AsString;
      end;
      FreDomicilio.CargarDatosDomicilio(Domicilio);
      FreTelefono.CargarTelefono(FieldByName('CodigoArea').Asinteger,FieldByName('Valor').AsString, FieldByName('CodigoTipoMedioContacto').Asinteger, FieldByName('HorarioDesde').Asdatetime, FieldByName('HorarioHasta').Asdatetime);
    end;
    pnlEmpresasRecaudadoras.Visible := true;
    lbEmpresasRecaudadoras.Estado     := Modi;
    lbEmpresasRecaudadoras.Enabled    := False;
    pnlEmpresasRecaudadoras.Enabled   := True;
    Notebook.PageIndex := 1;
    pnlEmpresasRecaudadoras.Visible := true;
    txtNombreEmpresa.SetFocus;
    Screen.Cursor    := crDefault;
end;

{-----------------------------------------------------------------------------
  Procedure: lbEmpresasRecaudadorasDrawItem
  Author:    mpiazza
  Date:      02/02/2009
  Arguments: 
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Result: N/A
  Purpose: Dibuja la grilla con datos
-----------------------------------------------------------------------------}
procedure TTABMEmpresasRecaudadorasForm.lbEmpresasRecaudadorasDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	with Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(FieldByName('CodigoEmpresasRecaudadoras').AsString));
		TextOut(Cols[1], Rect.Top, Trim(FieldByName('NombreEmpresa').AsString));
		TextOut(Cols[2], Rect.Top, Trim(FieldByName('NumeroDocumento').AsString));
    TextOut(Cols[3], Rect.Top, Trim(FieldByName('AliasEmpresa').AsString));
    TextOut(Cols[4], Rect.Top, Trim(FieldByName('Giro').AsString));
    TextOut(Cols[5], Rect.Top, Trim(FieldByName('Descripcion').AsString));
    TextOut(Cols[6], Rect.Top, Trim(FieldByName('eMail').AsString));
    TextOut(Cols[7], Rect.Top, Trim(FieldByName('Valor').AsString));
    TextOut(Cols[8], Rect.Top, Trim(FieldByName('Region').AsString));
    TextOut(Cols[9], Rect.Top, Trim(FieldByName('Comuna').AsString));
	end;
end;

end.
