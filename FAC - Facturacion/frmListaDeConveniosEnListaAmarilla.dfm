object ListaDeConveniosEnListaAmarillaForm: TListaDeConveniosEnListaAmarillaForm
  Left = 0
  Top = 0
  Anchors = []
  Caption = 'Inhabilitaci'#243'n por Lista Amarilla'
  ClientHeight = 508
  ClientWidth = 1040
  Color = clBtnFace
  Constraints.MinHeight = 386
  Constraints.MinWidth = 736
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dblListaAmarilla: TDBListEx
    Left = 0
    Top = 161
    Width = 1040
    Height = 251
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 20
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = True
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'N'#250'mero de Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'NumeroConvenio'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Concesionaria Inhabilitada'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'Concesionaria'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Comprobantes Impagos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'CantidadNKImpagas'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Deuda del Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'TotalApagar'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Deuda CN'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'DeudaCN'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Deuda AMB'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'DeudaAMB'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Deuda Otras'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'DeudaOtros'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Pre-Evaluaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'FechaIngreso'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha Alta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'FechaAlta'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha Baja'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'FechaBaja'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'Descripcion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Motivo Inhabilitaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'MotivoInhabilitacion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Env'#237'o Carta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'FechaEnvioCarta'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Hist'#243'rico Carta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = True
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'HistoricoCarta'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'Autorizado Fiscal'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'AutorizaFiscalia'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Fecha Aut. Fiscal'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'FechaAutorizaFiscalia'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 130
        Header.Caption = 'Usuario Aut. Fiscal'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'UsuarioAutorizaFiscalia'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 130
        Header.Caption = 'Autorizado Gerencia'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'AutorizaGerencia'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 130
        Header.Caption = 'Fecha Aut. Gerencia'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'FechaAutorizaGerencia'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 130
        Header.Caption = 'Usuario Aut. Gerencia'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblListaAmarillaColumnsHeaderClick
        FieldName = 'UsuarioAutorizaGerencia'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'Observaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = True
      end>
    DataSource = dsObtenerConveniosEnListaAmarilla
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDrawText = dblListaAmarillaDrawText
    OnLinkClick = dblListaAmarillaLinkClick
  end
  object grpControl: TGroupBox
    Left = 0
    Top = 412
    Width = 1040
    Height = 96
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      1040
      96)
    object btnAutFiscalia: TButton
      Left = 450
      Top = 60
      Width = 90
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Aut. Fiscal'#237'a'
      Enabled = False
      TabOrder = 0
      OnClick = btnAutFiscaliaClick
    end
    object btnAutGerencia: TButton
      Left = 550
      Top = 60
      Width = 90
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Aut. Gerencia'
      Enabled = False
      TabOrder = 1
      OnClick = btnAutGerenciaClick
    end
    object btnNuevoConvenio: TButton
      Left = 53
      Top = 60
      Width = 90
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Nuevo Convenio'
      TabOrder = 2
      OnClick = btnNuevoConvenioClick
    end
    object btnCambiarEstado: TButton
      Left = 149
      Top = 60
      Width = 90
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Cambiar Estado'
      Enabled = False
      TabOrder = 3
      OnClick = btnCambiarEstadoClick
    end
    object btnReEnviarCarta: TButton
      Left = 351
      Top = 60
      Width = 90
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Re-Enviar Carta'
      Enabled = False
      TabOrder = 4
      OnClick = btnReEnviarCartaClick
    end
    object btnSeleccionarTodos: TButton
      Left = 114
      Top = 20
      Width = 126
      Height = 25
      Align = alCustom
      Caption = 'Seleccionar Todos'
      TabOrder = 5
      OnClick = btnSeleccionarTodosClick
    end
    object btnDeselectAll: TButton
      Left = 258
      Top = 19
      Width = 126
      Height = 25
      Align = alCustom
      Caption = 'Deseleccionar Todos'
      TabOrder = 6
      OnClick = btnDeselectAllClick
    end
    object btnInvertirSeleccion: TButton
      Left = 390
      Top = 19
      Width = 126
      Height = 25
      Align = alCustom
      Caption = 'Invertir Selecci'#243'n'
      TabOrder = 7
      OnClick = btnInvertirSeleccionClick
    end
    object btnSalir: TButton
      Left = 922
      Top = 22
      Width = 89
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 8
      OnClick = btnSalirClick
    end
    object btnAnularCarta: TButton
      Left = 251
      Top = 60
      Width = 90
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Rechazar Carta'
      Enabled = False
      TabOrder = 9
      OnClick = btnAnularCartaClick
    end
  end
  object grpCliente: TGroupBox
    Left = 0
    Top = 0
    Width = 1040
    Height = 49
    Align = alTop
    Caption = 'Buscar Convenio'
    TabOrder = 2
    object Label1: TLabel
      Left = 17
      Top = 21
      Width = 24
      Height = 13
      Caption = 'RUT:'
    end
    object Label3: TLabel
      Left = 282
      Top = 21
      Width = 49
      Height = 13
      Caption = 'Convenio:'
    end
    object peRUTCliente: TPickEdit
      Left = 96
      Top = 17
      Width = 161
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnKeyPress = peRUTClienteKeyPress
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object vcbConveniosCliente: TVariantComboBox
      Left = 344
      Top = 18
      Width = 216
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
      OnDrawItem = vcbConveniosClienteDrawItem
      Items = <>
    end
    object btnBuscar: TButton
      Left = 566
      Top = 15
      Width = 89
      Height = 25
      Caption = 'Buscar'
      Default = True
      TabOrder = 2
      Visible = False
      OnClick = btnBuscarClick
    end
  end
  object grpFiltro: TGroupBox
    Left = 0
    Top = 49
    Width = 1040
    Height = 112
    Align = alTop
    Caption = 'Datos del Filtro'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object lblEstados: TLabel
      Left = 334
      Top = 58
      Width = 37
      Height = 13
      Caption = 'Estado:'
    end
    object lblFechaCreacion: TLabel
      Left = 16
      Top = 33
      Width = 73
      Height = 13
      Caption = 'Fecha Ingreso:'
    end
    object Label2: TLabel
      Left = 121
      Top = 12
      Width = 30
      Height = 13
      Caption = 'Desde'
    end
    object Label4: TLabel
      Left = 223
      Top = 12
      Width = 28
      Height = 13
      Caption = 'Hasta'
    end
    object lblFechaAlta: TLabel
      Left = 31
      Top = 59
      Width = 58
      Height = 13
      Caption = 'Fecha Alta: '
    end
    object lblFechaBaja: TLabel
      Left = 29
      Top = 83
      Width = 60
      Height = 13
      Caption = 'Fecha Baja: '
    end
    object lblEstacConceaionaria: TLabel
      Left = 300
      Top = 33
      Width = 71
      Height = 13
      Caption = 'Concesionaria:'
    end
    object Label5: TLabel
      Left = 297
      Top = 83
      Width = 74
      Height = 13
      Caption = 'Autorizaciones:'
    end
    object vcbEstados: TVariantComboBox
      Left = 377
      Top = 55
      Width = 195
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 5
      Items = <>
    end
    object deFechaIngresoDesde: TDateEdit
      Left = 95
      Top = 31
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object deFechaIngresoHasta: TDateEdit
      Left = 192
      Top = 31
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object deFechaAltaHasta: TDateEdit
      Left = 192
      Top = 56
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 4
      Date = -693594.000000000000000000
    end
    object deFechaAltaDesde: TDateEdit
      Left = 95
      Top = 56
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 3
      Date = -693594.000000000000000000
    end
    object deFechaBajaDesde: TDateEdit
      Left = 95
      Top = 80
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 6
      Date = -693594.000000000000000000
    end
    object deFechaBajaHasta: TDateEdit
      Left = 192
      Top = 80
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 7
      Date = -693594.000000000000000000
    end
    object btnFiltrar: TButton
      Left = 598
      Top = 29
      Width = 89
      Height = 25
      Caption = 'Buscar'
      TabOrder = 9
      OnClick = btnFiltrarClick
    end
    object btnLimpiar: TButton
      Left = 598
      Top = 71
      Width = 89
      Height = 25
      Caption = 'Limpiar'
      TabOrder = 10
      OnClick = btnLimpiarClick
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 377
      Top = 30
      Width = 195
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object cbbAutorizaciones: TComboBox
      Left = 377
      Top = 80
      Width = 195
      Height = 21
      Style = csDropDownList
      Color = clWhite
      ItemHeight = 13
      TabOrder = 8
      Items.Strings = (
        '<Todas>'
        'S'#243'lo Fiscal'#237'a'
        'S'#243'lo Gerencia'
        'Ambas'
        'Ninguna')
    end
  end
  object spObtenerConveniosEnListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConveniosEnListaAmarilla;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = 1
      end
      item
        Name = '@FechaIngresoDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaIngresoHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaAltaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaAltaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBajaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBajaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 384
    Top = 280
  end
  object dsObtenerConveniosEnListaAmarilla: TDataSource
    DataSet = spObtenerConveniosEnListaAmarilla
    Left = 416
    Top = 280
  end
  object spObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 320
    Top = 280
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 352
    Top = 280
  end
  object ilCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 480
    Top = 281
    Bitmap = {
      494C01010300040094010F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848284008482
      8400848284008482840084828400848284008482840084828400E7E7E700FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084828400CECFCE00CECFCE00EFE7EF00EFEBEF00EFEFEF00FFF3
      FF00FFF7FF00FFFBFF0084828400E7E7E700FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084828400CECFCE00CECF
      CE00EFE7EF00EFEBEF00EFEFEF00FFF3FF00FFF7FF00FFFBFF0084828400E7E7
      E700FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084828400CECFCE00CECFCE00EFEBEF00EFEBEF00EFEBEF00FFF3
      FF00FFFBFF00FFFBFF0084828400E7E7E700FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084828400CECFCE00CECF
      CE0084828400848284008482840084828400FFF7FF00FFFBFF0084828400E7E7
      E700FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084828400CECFCE00CECFCE00EFE7EF00EFEBEF00EFEFEF00FFF3
      FF00FFF7FF00FFFBFF0084828400E7E7E700FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084828400CECFCE00CECF
      CE0084828400848284009C9A9C009C9A9C00FFF7FF00FFFBFF0084828400E7E7
      E700FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084828400CECFCE00CECFCE00EFE7EF00EFEBEF00EFEFEF00FFF3
      FF00FFF7FF00FFFBFF0084828400E7E7E700FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084828400CECFCE00CECF
      CE0084828400848284009C9A9C009C9A9C00FFF7FF00FFFBFF0084828400E7E7
      E700FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084828400CECFCE00CECFCE00EFE7EF00EFEBEF00EFEFEF00FFF3
      FF00FFF7FF00FFFBFF0084828400E7E7E700FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084828400CECFCE00CECF
      CE0084828400848284009C9A9C009C9A9C00FFFBFF00FFFBFF0084828400E7E7
      E700FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084828400CECFCE00CECFCE00EFE7EF00EFEBEF00EFEFEF00FFF3
      FF00FFF7FF00FFFBFF0084828400E7E7E700FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084828400CECFCE00CECF
      CE00EFE7EF00EFEBEF00EFEFEF00FFF3FF00FFF7FF00FFFBFF0084828400E7E7
      E700FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084828400848284008482840084828400848284008482
      84008482840084828400E7E7E700FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFFC01800008003000780080000BFFB7FF780080000BFFB77F780080000
      BFFB63F780080000BFFB41F780080000BFFB48F780080000BFFB5C7780080000
      BFFB7E3780080000BFFB7F1780080000BFFB7F9780080000BFFB7FD780080000
      BFFB7FF78008000080030007C0180000FFFFFFFFFFF800000000000000000000
      0000000000000000000000000000}
  end
  object cdsConvenios: TClientDataSet
    Active = True
    Aggregates = <>
    AutoCalcFields = False
    FieldDefs = <
      item
        Name = 'IDConvenioInhabilitado'
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'Concesionaria'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Estado'
        DataType = ftInteger
      end
      item
        Name = 'NumeroConvenio'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FechaEnvioCarta'
        DataType = ftDateTime
      end
      item
        Name = 'EstaEnLegales'
        DataType = ftBoolean
      end
      item
        Name = 'NumeroDocumento'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'AutorizaFiscalia'
        DataType = ftBoolean
      end
      item
        Name = 'AutorizaGerencia'
        DataType = ftBoolean
      end
      item
        Name = 'FechaAutorizaFiscalia'
        DataType = ftDate
      end
      item
        Name = 'UsuarioAutorizaFiscalia'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaAutorizaGerencia'
        DataType = ftDate
      end
      item
        Name = 'UsuarioAutorizaGerencia'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PuedeAutorizarFiscalia'
        DataType = ftBoolean
      end
      item
        Name = 'PuedeAutorizarGerencia'
        DataType = ftBoolean
      end
      item
        Name = 'PuedeReEnviarCarta'
        DataType = ftBoolean
      end
      item
        Name = 'PuedeAnularCarta'
        DataType = ftBoolean
      end
      item
        Name = 'TotalApagar'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CantidadNKImpagas'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoConcesionaria'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    FetchOnDemand = False
    Params = <>
    StoreDefs = True
    Left = 448
    Top = 280
    Data = {
      B00200009619E0BD010000001800000016000000000003000000B00216494443
      6F6E76656E696F496E686162696C697461646F04000100000000000E436F6469
      676F436F6E76656E696F04000100000000000D436F6E636573696F6E61726961
      01004900000001000557494454480200020014000645737461646F0400010000
      0000000E4E756D65726F436F6E76656E696F0100490000000100055749445448
      0200020014000B4465736372697063696F6E0100490000000100055749445448
      0200020032000F4665636861456E76696F436172746108000800000000000D45
      737461456E4C6567616C657302000300000000000F4E756D65726F446F63756D
      656E746F0100490000000100055749445448020002001400104175746F72697A
      6146697363616C69610200030000000000104175746F72697A61476572656E63
      696102000300000000001546656368614175746F72697A6146697363616C6961
      0400060000000000175573756172696F4175746F72697A6146697363616C6961
      01004900000001000557494454480200020014001546656368614175746F7269
      7A61476572656E6369610400060000000000175573756172696F4175746F7269
      7A61476572656E63696101004900000001000557494454480200020014001650
      756564654175746F72697A617246697363616C69610200030000000000165075
      6564654175746F72697A6172476572656E636961020003000000000012507565
      64655265456E7669617243617274610200030000000000105075656465416E75
      6C6172436172746102000300000000000B546F74616C41706167617201004900
      000001000557494454480200020014001143616E74696461644E4B496D706167
      6173010049000000010005574944544802000200140013436F6469676F436F6E
      636573696F6E6172696102000100000000000000}
    object cdsConveniosIDConvenioInhabilitado: TIntegerField
      FieldName = 'IDConvenioInhabilitado'
    end
    object cdsConveniosCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsConveniosEstado: TIntegerField
      FieldName = 'Estado'
    end
    object cdsConveniosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
    end
    object cdsConveniosDescripcion: TStringField
      DisplayWidth = 50
      FieldName = 'Descripcion'
      Size = 50
    end
    object cdsConveniosFechaEnvioCarta: TDateTimeField
      FieldName = 'FechaEnvioCarta'
    end
    object cdsConveniosEstaEnLegales: TBooleanField
      FieldName = 'EstaEnLegales'
    end
    object cdsConveniosNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
    end
    object cdsConveniosAutorizaFiscalia: TBooleanField
      FieldName = 'AutorizaFiscalia'
    end
    object cdsConveniosAutorizaGerencia: TBooleanField
      FieldName = 'AutorizaGerencia'
    end
    object cdsConveniosFechaAutFiscalia: TDateField
      FieldName = 'FechaAutorizaFiscalia'
    end
    object cdsConveniosUsuarioAutFiscalia: TStringField
      FieldName = 'UsuarioAutorizaFiscalia'
    end
    object cdsConveniosFechaAutGerencia: TDateField
      FieldName = 'FechaAutorizaGerencia'
    end
    object cdsConveniosUsuarioAutGerencia: TStringField
      FieldName = 'UsuarioAutorizaGerencia'
    end
    object cdsConveniosPuedeAutorizarFiscalia: TBooleanField
      FieldName = 'PuedeAutorizarFiscalia'
    end
    object cdsConveniosPuedeAutorizarGerencia: TBooleanField
      FieldName = 'PuedeAutorizarGerencia'
    end
    object cdsConveniosPuedeReEnviarCarta: TBooleanField
      FieldName = 'PuedeReEnviarCarta'
    end
    object cdsConveniosPuedeAnularCarta: TBooleanField
      FieldName = 'PuedeAnularCarta'
    end
    object cdsConveniosConcesionaria: TStringField
      FieldName = 'Concesionaria'
    end
    object cdsConveniosTotalApagar: TStringField
      FieldName = 'TotalApagar'
    end
    object cdsConveniosCantidadNKImpagas: TStringField
      FieldName = 'CantidadNKImpagas'
    end
    object cdsConveniosCodigoConcesionaria: TSmallintField
      FieldName = 'CodigoConcesionaria'
    end
  end
  object spAutorizarConvenioPorFiscalia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AutorizarConvenioPorFiscalia'
    Parameters = <>
    Left = 512
    Top = 280
  end
  object spAutorizarConvenioPorGerencia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AutorizarConvenioPorGerencia'
    Parameters = <>
    Left = 544
    Top = 280
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 576
    Top = 280
  end
end
