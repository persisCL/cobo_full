#ifdef __cplusplus


extern "C" {
#endif
#define dllexport __declspec(dllexport)
struct RptaSolicitaFolioDll{
			int		folio;
			char	codigo[11]; //[OK, ERR]
			char    mensaje[101];      
};

struct RptaAccionFolioDll{
		char	codigo[11]; 
        char    mensaje[101];      
};


struct RptaSolicitaTedDll{
		char	strTed[1001];
		char	strTms[31];
		char	codigo[11]; 
        char    mensaje[101];
};

dllexport RptaSolicitaFolioDll SolicitaFolio (int); 
dllexport RptaAccionFolioDll ConfirmaFolio (int); 
dllexport RptaAccionFolioDll ReversaFolio (int); 
dllexport RptaSolicitaTedDll SolicitaTed (int,char *,int,int, char*, double,char*,int,char *,char*, char*);
dllexport RptaSolicitaTedDll GeneraImg (int,char *,int,int, char*, double,char*,int,char *,char*, char*);

#ifdef __cplusplus

}
#endif
//extern "C" {
//#define dllexport __declspec(dllexport)
/*struct RptaSolicitaFolioDll{
			int		folio;
			char	codigo[10]; //[OK, ERR]
			char    mensaje[100];      
};

struct RptaAccionFolioDll{
		char	codigo[10]; 
        char    mensaje[100];      
};

RptaSolicitaFolioDll SolicitaFolio (int); 
RptaAccionFolioDll ConfirmaFolio (int); 
RptaAccionFolioDll ReversaFolio (int); 
*/
