unit DetallesTransitosFacturados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DPSPageControl, ListBoxEx, DBListEx,
  DPSControls, ExtCtrls, DBCtrls, Buttons, DB, ADODB, DMConnection;

type
  TfrmDetalleTransitosFacturados = class(TForm)
    btnSalir: TDPSButton;
    GroupBox1: TGroupBox;
    LNumeroDocumento: TLabel;
    Label6: TLabel;
    lApellidoNombre: TLabel;
    LDomicilio: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    GroupBox2: TGroupBox;
    DBListEx1: TDBListEx;
    DPSPageControl1: TDPSPageControl;
    tsTRX_Detalle: TTabSheet;
    tsTRX_Anomalias: TTabSheet;
    tsTRX_Imagenes: TTabSheet;
    CheckBox1: TCheckBox;
    Panel1: TPanel;
    DPSPageControl2: TDPSPageControl;
    tsDatosClientes_TAG: TTabSheet;
    tsDatosClientes_Cuenta: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    dbtNroComprobante: TDBText;
    dbtFechaEmision: TDBText;
    dbtVencimiento: TDBText;
    dbtImporte: TDBText;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    GroupBox3: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    dbtFechaTrxFacturados: TDBText;
    dbtImporteTRXFacturados: TDBText;
    dbtCantidadTRXFacturados: TDBText;
    dsTransitosPorItem: TDataSource;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dbtFechaHoraTRX: TDBText;
    dbtPuntoCobroNumero: TDBText;
    dbtPuntoCobroDescripcion: TDBText;
    dbtPuntoCobroSentido: TDBText;
    dbtImporteTRX: TDBText;
    dbtConvenio: TDBText;
    dbtPatente: TDBText;
    dbtCategoria: TDBText;
    lbAnomalias: TLabel;
    lbImagenes: TLabel;
    dbchkDiscCategoria: TDBCheckBox;
    dbchkDiscPatente: TDBCheckBox;
    dbchkTAGInhabilitadoRobo: TDBCheckBox;
    dbchkTAGInhabilitadoMora: TDBCheckBox;
    dbchkInfraccion: TDBCheckBox;
    dbchkTrxInfractor: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    dbchkTAGilegal: TDBCheckBox;
    dbchkTAGPocoProbable: TDBCheckBox;
    dbchkTAGNoDetectado: TDBCheckBox;
    Bevel1: TBevel;
    GroupBox4: TGroupBox;
    dbtPatenteDetectada: TDBText;
    dbtPatenteValidada: TDBText;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    dbtConfiabilidadPatenteDetectada: TDBText;
    GroupBox5: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    dbtCategoriaDetectada: TDBText;
    dbtCategoriaValidada: TDBText;
    dbtDescriCategoriaDetectada: TDBText;
    dbtDescriCategoriaValidada: TDBText;
    Label30: TLabel;
    Label31: TLabel;
    dbtDescripcionCategoria: TDBText;
    Image1: TImage;
    dsAnomalias: TDataSource;
    tblAnomalias: TADOTable;
    Label23: TLabel;
    spTransitosPorItem: TADOStoredProc;
    spTransitosPorItemNumCorrCA: TLargeintField;
    spTransitosPorItemNumeroPuntoCobro: TWordField;
    spTransitosPorItemFechaHora: TDateTimeField;
    spTransitosPorItemIDItemPeajeCuenta: TIntegerField;
    spTransitosPorItemIDDetalleTransito: TIntegerField;
    spTransitosPorItemImporte: TBCDField;
    spTransitosPorItemDescriImporte: TStringField;
    spTransitosPorItemDescPtoCobro: TStringField;
    spTransitosPorItemSentido: TStringField;
    spTransitosPorItemCodigoConvenio: TIntegerField;
    spTransitosPorItemNumeroConvenioFormateado: TStringField;
    spTransitosPorItemIndiceVehiculo: TIntegerField;
    spTransitosPorItemPatente: TStringField;
    spTransitosPorItemCategoria: TWordField;
    spTransitosPorItemDescCategoria: TStringField;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Label17: TLabel;
    Label19: TLabel;
    Label24: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    dsCuentas: TDataSource;
    Label37: TLabel;
    spCuentas: TADOStoredProc;
    spCuentasCodigoConvenio: TIntegerField;
    spCuentasIndiceVehiculo: TIntegerField;
    spCuentasFechaAltaCuenta: TDateTimeField;
    spCuentasFechaBajaCuenta: TDateTimeField;
    spCuentasCodigoEstadoCuenta: TWordField;
    spCuentasFechaAltaTAG: TDateTimeField;
    spCuentasFechaBajaTAG: TDateTimeField;
    spCuentasPatente: TStringField;
    spCuentasDescriMarca: TStringField;
    spCuentasModelo: TStringField;
    spCuentasAnioVehiculo: TSmallintField;
    spCuentasDescriColor: TStringField;
    spCuentasDescriTipoVehiculo: TStringField;
    spCuentasTieneAcoplado: TStringField;
    spCuentasDescriCategoriaVehiculo: TStringField;
    spCuentasDescriCategoriaAcoplado: TStringField;
    spCuentasFechaFinGarantiaMOPTT: TDateTimeField;
    spCuentasModeloTAG: TStringField;
    spCuentasEdoSituacion: TStringField;
    spCuentasEdoConservacion: TStringField;
    spCuentasEdoFuncionamiento: TStringField;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBText8: TDBText;
    DBText9: TDBText;
    DBText10: TDBText;
    DBText11: TDBText;
    DBText12: TDBText;
    DBText13: TDBText;
    Label43: TLabel;
    Label44: TLabel;
    Label46: TLabel;
    DBText14: TDBText;
    DBText15: TDBText;
    procedure btnSalirClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure dsTransitosPorItemDataChange(Sender: TObject; Field: TField);
    procedure dsCuentasDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Inicializa(NroDocu: string; ApeNom: string; Domic: String; NroItem: Integer);
  end;

  function TieneImagenes:boolean;
var
  frmDetalleTransitosFacturados: TfrmDetalleTransitosFacturados;

implementation

{$R *.dfm}

procedure TfrmDetalleTransitosFacturados.Inicializa(NroDocu: string; ApeNom: string; Domic: String; NroItem: Integer);
begin
    LNumeroDocumento.Caption := NroDocu;
    LApellidoNombre.Caption := ApeNom;
    LDomicilio.Caption := Domic;
    spTransitosPorItem.Parameters.ParamByName('@IDItemPeajeCuenta').Value := NroItem;
end;

procedure TfrmDetalleTransitosFacturados.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmDetalleTransitosFacturados.CheckBox1Click(Sender: TObject);
begin
    tsTRX_Imagenes.TabVisible := TieneImagenes AND CheckBox1.Checked;
    tsTRX_Anomalias.TabVisible := CheckBox1.Checked;
end;

{*****************************************************
* Funci�n: TieneImagenes()                           *
* Parametros: NINGUNO                                *
* Autor: Nicol�s Donadio Manolidis                   *
* Fecha: 29.11.2004                                  *
* Descripcion: Devuelve VERDADERO o FALSO segun      *
*              encuentre imagenes para el TRX Actual *
******************************************************}
Function TieneImagenes: boolean;
begin
{Por ahora siempre devuelve verdadero!}
   Result:= True;
end;

procedure TfrmDetalleTransitosFacturados.dsTransitosPorItemDataChange(
  Sender: TObject; Field: TField);
begin
    CheckBox1.Checked := False;
    tblAnomalias.Requery();
    tsTRX_Anomalias.TabVisible := NOT tblAnomalias.IsEmpty;
    tsTRX_Imagenes.TabVisible := False;
    if TieneImagenes then    lbImagenes.Caption := 'SI'
    else lbImagenes.Caption := 'NO';
    if tblAnomalias.IsEmpty then lbAnomalias.caption := 'NO'
    else    lbAnomalias.caption := 'SI';
    if Field = nil then begin
        screen.cursor := crHourGlass;
        try
            spCuentas.Close;
            if not spTransitosPorItem.Eof  then Begin
                 with spCuentas do begin
                     Parameters.ParamByName('@CConvenio').Value := spTransitosPorItem.FieldByName('CodigoConvenio').asInteger;
                     Parameters.ParamByName('@IVehiculo').Value := spTransitosPorItem.FieldByName('IndiceVehiculo').asInteger;
                     Open;
                end;
            end;

        finally
            screen.cursor := crDefault;
        end;
    end;
end;

procedure TfrmDetalleTransitosFacturados.dsCuentasDataChange(
  Sender: TObject; Field: TField);
begin
    label35.Visible := spCuentas.FieldByName('TieneAcoplado').Value = 'SI';
    label34.Visible := Label35.Visible;

end;

end.
