object FormCarpetaLegales: TFormCarpetaLegales
  Left = 0
  Top = 0
  Caption = 'Carpeta Legales'
  ClientHeight = 462
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlPie: TPanel
    Left = 0
    Top = 421
    Width = 1016
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      1016
      41)
    object lblEstadoProceso: TLabel
      Left = 32
      Top = 14
      Width = 3
      Height = 13
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object nbBotones: TNotebook
      Left = 250
      Top = 3
      Width = 550
      Height = 35
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'pgEnviosSalidas'
        object btnImprimir: TButton
          Left = 34
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Imprimir'
          TabOrder = 0
          OnClick = btnImprimirClick
        end
        object btnEnvioLegales: TButton
          Left = 160
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Env'#237'o Legales'
          TabOrder = 1
          OnClick = btnEnvioLegalesClick
        end
        object btnSalidaLegales: TButton
          Left = 286
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Salida Legales'
          TabOrder = 2
          OnClick = btnSalidaLegalesClick
        end
        object btnEliminarLegales: TButton
          Left = 412
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Eliminar'
          TabOrder = 3
          OnClick = btnEliminarLegalesClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'pgAceptarCancelar'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object btnPasarACarpetasLegales: TButton
          Left = 153
          Top = 6
          Width = 189
          Height = 25
          Caption = '&Pasar a Carpetas Legales'
          TabOrder = 0
          OnClick = btnPasarACarpetasLegalesClick
        end
      end
    end
    object btnSalir: TButton
      Left = 928
      Top = 9
      Width = 81
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Salir'
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object pnlCentral: TPanel
    Left = 0
    Top = 57
    Width = 1016
    Height = 364
    Align = alClient
    TabOrder = 0
    object pgConvenios: TPageControl
      Left = 1
      Top = 1
      Width = 1014
      Height = 362
      ActivePage = tsConveniosAIngresar
      Align = alClient
      TabOrder = 0
      OnChange = pgConveniosChange
      object tsConveniosEnCarpeta: TTabSheet
        Caption = 'Convenios En Carpeta'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 1006
          Height = 334
          Align = alClient
          Caption = 'Panel1'
          TabOrder = 0
          object dblConveniosEnCarpeta: TDBListEx
            Left = 1
            Top = 1
            Width = 1004
            Height = 332
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 75
                Header.Caption = 'Seleccionado'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosEnCarpetaSeleccionado'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 120
                Header.Caption = 'Convenio'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaNumeroConvenio'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 140
                Header.Caption = 'Fecha Ingreso'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaFechaHoraCreacion'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 100
                Header.Caption = 'Documento'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaNumeroDocumento'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 250
                Header.Caption = 'Cliente'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaApellidoMostrar'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 140
                Header.Caption = 'Fecha Impresi'#243'n'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaFechaHoraImpresion'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Usuario'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaUsuarioCreacion'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 90
                Header.Caption = 'Usuario Legales'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaUsuarioEnvioLegales'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 140
                Header.Caption = 'Fecha Env'#237'o Legales'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaFechaHoraEnvioALegales'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 90
                Header.Caption = 'Usuario Salida'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaUsuarioSalidaLegales'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 140
                Header.Caption = 'Fecha Salida Legales'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaFechaHoraSalidaLegales'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'D'#237'as'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaDias'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 120
                Header.Caption = 'Deuda'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaDeudaStr'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 120
                Header.Caption = 'Deuda Actualizada'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaImporteDeudaActualizada'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Total D'#237'as'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaDiasDeudaTotal'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Empresa Recaudadora'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaAliasEmpresaRecaudadora'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Tipo de Deuda'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                OnHeaderClick = OrdenarColumna
                FieldName = 'cdsConveniosEnCarpetaDescripcionTipoDeuda'
              end>
            DataSource = dsConveniosEnCarpeta
            DragReorder = True
            ParentColor = False
            PopupMenu = mnuSeleccion
            TabOrder = 0
            TabStop = True
            OnDblClick = dblConveniosEnCarpetaDblClick
            OnDrawText = dblConveniosEnCarpetaDrawText
            OnKeyDown = dblConveniosEnCarpetaKeyDown
          end
        end
      end
      object tsConveniosAIngresar: TTabSheet
        Caption = 'Convenios a Ingresar'
        ImageIndex = 1
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 1006
          Height = 334
          Align = alClient
          Caption = 'Panel2'
          TabOrder = 0
          object dblConveniosAIngresar: TDBListEx
            Left = 1
            Top = 1
            Width = 1004
            Height = 332
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 90
                Header.Caption = 'Seleccionado'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngSeleccionado'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Numero de Convenio'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngNumeroConvenio'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Apellido'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngApellido'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Apellido Materno'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngApellidoMaterno'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Nombre'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngNombre'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Nro Doc'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngNumeroDoc'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 100
                Header.Caption = 'Deuda'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngDeudaStr'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 60
                Header.Caption = 'D'#237'as'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'cdsConveniosAIngDias'
              end>
            DataSource = dsConveniosAIngresar
            DragReorder = True
            ParentColor = False
            PopupMenu = mnuSeleccion
            TabOrder = 0
            TabStop = True
            OnDblClick = dblConveniosAIngresarDblClick
            OnDrawText = dblConveniosAIngresarDrawText
            OnKeyDown = dblConveniosAIngresarKeyDown
          end
        end
      end
    end
  end
  object CollapsablePanel1: TCollapsablePanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 57
    Align = alTop
    Animated = False
    Direction = cpdDown
    Caption = 'Filtros de Selecci'#243'n'
    BarColorStart = 14071199
    BarColorEnd = 10646097
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    InternalSize = 34
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    DesignSize = (
      1016
      57)
    object Label1: TLabel
      Left = 6
      Top = 8
      Width = 77
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object Label3: TLabel
      Left = 234
      Top = 8
      Width = 145
      Height = 13
      Caption = 'Antig'#252'edad m'#237'nima de deuda :'
    end
    object Label5: TLabel
      Left = 431
      Top = 8
      Width = 137
      Height = 13
      Caption = 'Deuda del Convenio entre $ '
    end
    object Label6: TLabel
      Left = 650
      Top = 8
      Width = 22
      Height = 13
      Caption = 'y $ :'
    end
    object Label4: TLabel
      Left = 750
      Top = 8
      Width = 47
      Height = 13
      Caption = 'Limitar a :'
    end
    object peRUTCliente: TPickEdit
      Left = 89
      Top = 5
      Width = 139
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = True
      TabOrder = 1
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object neDiasDeuda: TNumericEdit
      Left = 386
      Top = 5
      Width = 37
      Height = 21
      MaxLength = 4
      TabOrder = 2
    end
    object neDeudaMinima: TNumericEdit
      Left = 576
      Top = 5
      Width = 64
      Height = 21
      TabOrder = 3
    end
    object neDeudaMaxima: TNumericEdit
      Left = 678
      Top = 5
      Width = 66
      Height = 21
      TabOrder = 4
    end
    object neLineas: TNumericEdit
      Left = 803
      Top = 5
      Width = 66
      Height = 21
      TabOrder = 5
      Value = 100.000000000000000000
    end
    object btnLimpiarFiltro: TButton
      Left = 877
      Top = 3
      Width = 76
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Limpiar filtros'
      TabOrder = 6
      OnClick = btnLimpiarFiltroClick
    end
    object btnFiltrar: TButton
      Left = 957
      Top = 3
      Width = 58
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Filtrar'
      Default = True
      TabOrder = 7
      OnClick = btnFiltrarClick
    end
  end
  object spConveniosEnCarpetaLegal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CarpetasMorosos_ObtenerConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@DiasDeuda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DeudaEnPesosDesde'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@DeudaEnPesosHasta'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadLineas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 328
  end
  object spConveniosAIngresar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 180
    ProcedureName = 'CarpetasMorosos_BuscarConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@DiasDeuda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DeudaEnPesosDesde'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@DeudaEnPesosHasta'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadLineas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 368
    Top = 336
  end
  object dsConveniosEnCarpeta: TDataSource
    DataSet = cdsConveniosEnCarpeta
    Left = 16
    Top = 328
  end
  object cdsConveniosEnCarpeta: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'cdsConveniosEnCarpetaIdConvenioCarpetaLegal'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaCodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaApellido'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosEnCarpetaApellidoMaterno'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosEnCarpetaNombre'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraImpresion'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioImpresion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraEnvioALegales'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioEnvioLegales'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraSalidaLegales'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaFechaHoraActualizacion'
        DataType = ftDateTime
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioActualizacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaNumeroDocumento'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'cdsConveniosEnCarpetaNumeroConvenio'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaSeleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'cdsConveniosEnCarpetaApellidoMostrar'
        DataType = ftString
        Size = 155
      end
      item
        Name = 'cdsConveniosEnCarpetaDias'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaDeudaStr'
        DataType = ftCurrency
      end
      item
        Name = 'cdsConveniosEnCarpetaUsuarioSalidaLegales'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosEnCarpetaDiasDeudaTotal'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaImporteDeudaActualizada'
        DataType = ftCurrency
      end
      item
        Name = 'cdsConveniosEnCarpetaAliasEmpresaRecaudadora'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosEnCarpetaDescripcionTipoDeuda'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosEnCarpetaCodigoEmpresasRecaudadoras'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaCodigoTipoDeuda'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosEnCarpetaDescripcionConcesionaria'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'cdsConveniosEnCarpetaEnListaAmarilla'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 48
    Top = 328
    Data = {
      B20A00009619E0BD01000000180000001D000000000003000000B20A1F636473
      436F6E76656E696F73456E436172706574614964436F6E76656E696F04000100
      00000100094649454C444E414D452C000980636473436F6E76656E696F73456E
      436172706574614964436F6E76656E696F436172706574614C6567616C001F63
      6473436F6E76656E696F73456E43617270657461436F6469676F436F6E760400
      010000000100094649454C444E414D4524000980636473436F6E76656E696F73
      456E43617270657461436F6469676F436F6E76656E696F001D636473436F6E76
      656E696F73456E436172706574614170656C6C69646F01004900000001000557
      49445448020002003C001F636473436F6E76656E696F73456E43617270657461
      4170656C6C69646F4D610100490000000200055749445448020002003C000946
      49454C444E414D4525000980636473436F6E76656E696F73456E436172706574
      614170656C6C69646F4D617465726E6F001B636473436F6E76656E696F73456E
      436172706574614E6F6D6272650100490000000100055749445448020002001E
      001F636473436F6E76656E696F73456E436172706574614665636861486F7261
      490800080000000100094649454C444E414D4528000980636473436F6E76656E
      696F73456E436172706574614665636861486F7261496D70726573696F6E001F
      636473436F6E76656E696F73456E436172706574615573756172696F496D7001
      00490000000200055749445448020002001400094649454C444E414D45260009
      80636473436F6E76656E696F73456E436172706574615573756172696F496D70
      726573696F6E001F636473436F6E76656E696F73456E43617270657461466563
      6861486F7261450800080000000100094649454C444E414D452C000980636473
      436F6E76656E696F73456E436172706574614665636861486F7261456E76696F
      414C6567616C6573001F636473436F6E76656E696F73456E4361727065746155
      73756172696F456E760100490000000200055749445448020002001400094649
      454C444E414D4529000980636473436F6E76656E696F73456E43617270657461
      5573756172696F456E76696F4C6567616C6573001F636473436F6E76656E696F
      73456E436172706574614665636861486F726153080008000000010009464945
      4C444E414D452C000980636473436F6E76656E696F73456E4361727065746146
      65636861486F726153616C6964614C6567616C6573001F636473436F6E76656E
      696F73456E436172706574614665636861486F72614308000800000001000946
      49454C444E414D4527000980636473436F6E76656E696F73456E436172706574
      614665636861486F72614372656163696F6E001F636473436F6E76656E696F73
      456E436172706574615573756172696F43726501004900000002000557494454
      48020002001400094649454C444E414D4525000980636473436F6E76656E696F
      73456E436172706574615573756172696F4372656163696F6E001F636473436F
      6E76656E696F73456E436172706574614665636861486F726141080008000000
      0100094649454C444E414D452C000980636473436F6E76656E696F73456E4361
      72706574614665636861486F726141637475616C697A6163696F6E001F636473
      436F6E76656E696F73456E436172706574615573756172696F41637401004900
      00000200055749445448020002001400094649454C444E414D452A0009806364
      73436F6E76656E696F73456E436172706574615573756172696F41637475616C
      697A6163696F6E001F636473436F6E76656E696F73456E436172706574614E75
      6D65726F446F63750100490000000200055749445448020002000B0009464945
      4C444E414D4525000980636473436F6E76656E696F73456E436172706574614E
      756D65726F446F63756D656E746F001F636473436F6E76656E696F73456E4361
      72706574614E756D65726F436F6E760100490000000200055749445448020002
      001400094649454C444E414D4524000980636473436F6E76656E696F73456E43
      6172706574614E756D65726F436F6E76656E696F001F636473436F6E76656E69
      6F73456E4361727065746153656C656363696F6E610200030000000100094649
      454C444E414D4522000980636473436F6E76656E696F73456E43617270657461
      53656C656363696F6E61646F001F636473436F6E76656E696F73456E43617270
      6574614170656C6C69646F4D6F0100490000000200055749445448020002009B
      00094649454C444E414D4525000980636473436F6E76656E696F73456E436172
      706574614170656C6C69646F4D6F73747261720019636473436F6E76656E696F
      73456E436172706574614469617304000100000000001D636473436F6E76656E
      696F73456E436172706574614465756461537472080004000000010007535542
      545950450200490006004D6F6E6579001F636473436F6E76656E696F73456E43
      6172706574615573756172696F53616C01004900000002000557494454480200
      02001400094649454C444E414D452A000980636473436F6E76656E696F73456E
      436172706574615573756172696F53616C6964614C6567616C6573001F636473
      436F6E76656E696F73456E436172706574614469617344657564615404000100
      00000100094649454C444E414D4524000980636473436F6E76656E696F73456E
      43617270657461446961734465756461546F74616C001F636473436F6E76656E
      696F73456E43617270657461496D706F72746544657508000400000002000753
      5542545950450200490006004D6F6E657900094649454C444E414D452D000980
      636473436F6E76656E696F73456E43617270657461496D706F72746544657564
      6141637475616C697A616461001F636473436F6E76656E696F73456E43617270
      657461416C696173456D7072650100490000000200055749445448020002003C
      00094649454C444E414D452D000980636473436F6E76656E696F73456E436172
      70657461416C696173456D707265736152656361756461646F7261001F636473
      436F6E76656E696F73456E436172706574614465736372697063696F01004900
      00000200055749445448020002003C00094649454C444E414D452A0009806364
      73436F6E76656E696F73456E436172706574614465736372697063696F6E5469
      706F4465756461001F636473436F6E76656E696F73456E43617270657461436F
      6469676F456D70720400010000000100094649454C444E414D45300009806364
      73436F6E76656E696F73456E43617270657461436F6469676F456D7072657361
      7352656361756461646F726173001F636473436F6E76656E696F73456E436172
      70657461436F6469676F5469706F0400010000000100094649454C444E414D45
      25000980636473436F6E76656E696F73456E43617270657461436F6469676F54
      69706F4465756461001F636473436F6E76656E696F73456E4361727065746144
      65736372697063696F0100490000000200055749445448020002003200094649
      454C444E414D452E000980636473436F6E76656E696F73456E43617270657461
      4465736372697063696F6E436F6E636573696F6E61726961001F636473436F6E
      76656E696F73456E43617270657461456E4C69737461416D6102000300000001
      00094649454C444E414D4525000980636473436F6E76656E696F73456E436172
      70657461456E4C69737461416D6172696C6C61000000}
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaIdConvenioCarpetaLegal: TIntegerField
      FieldName = 'cdsConveniosEnCarpetaIdConvenioCarpetaLegal'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaCodigoConvenio: TIntegerField
      FieldName = 'cdsConveniosEnCarpetaCodigoConvenio'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaApellido: TStringField
      FieldName = 'cdsConveniosEnCarpetaApellido'
      Size = 60
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaApellidoMaterno: TStringField
      FieldName = 'cdsConveniosEnCarpetaApellidoMaterno'
      Size = 60
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaNombre: TStringField
      FieldName = 'cdsConveniosEnCarpetaNombre'
      Size = 30
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraImpresion: TDateTimeField
      FieldName = 'cdsConveniosEnCarpetaFechaHoraImpresion'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioImpresion: TStringField
      FieldName = 'cdsConveniosEnCarpetaUsuarioImpresion'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraEnvioALegales: TDateTimeField
      FieldName = 'cdsConveniosEnCarpetaFechaHoraEnvioALegales'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioEnvioLegales: TStringField
      FieldName = 'cdsConveniosEnCarpetaUsuarioEnvioLegales'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraSalidaLegales: TDateTimeField
      FieldName = 'cdsConveniosEnCarpetaFechaHoraSalidaLegales'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraCreacion: TDateTimeField
      FieldName = 'cdsConveniosEnCarpetaFechaHoraCreacion'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioCreacion: TStringField
      FieldName = 'cdsConveniosEnCarpetaUsuarioCreacion'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaFechaHoraActualizacion: TDateTimeField
      FieldName = 'cdsConveniosEnCarpetaFechaHoraActualizacion'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioActualizacion: TStringField
      FieldName = 'cdsConveniosEnCarpetaUsuarioActualizacion'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaNumeroDocumento: TStringField
      FieldName = 'cdsConveniosEnCarpetaNumeroDocumento'
      Size = 11
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaNumeroConvenio: TStringField
      FieldName = 'cdsConveniosEnCarpetaNumeroConvenio'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaSeleccionado: TBooleanField
      FieldName = 'cdsConveniosEnCarpetaSeleccionado'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaApellidoMostrar: TStringField
      FieldName = 'cdsConveniosEnCarpetaApellidoMostrar'
      Size = 155
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaDias: TIntegerField
      FieldName = 'cdsConveniosEnCarpetaDias'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaDeudaStr: TCurrencyField
      FieldName = 'cdsConveniosEnCarpetaDeudaStr'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaUsuarioSalidaLegales: TStringField
      FieldName = 'cdsConveniosEnCarpetaUsuarioSalidaLegales'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaDiasDeudaTotal: TIntegerField
      FieldName = 'cdsConveniosEnCarpetaDiasDeudaTotal'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaImporteDeudaActualizada: TCurrencyField
      FieldName = 'cdsConveniosEnCarpetaImporteDeudaActualizada'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaAliasEmpresaRecaudadora: TStringField
      FieldName = 'cdsConveniosEnCarpetaAliasEmpresaRecaudadora'
      Size = 60
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaDescripcionTipoDeuda: TStringField
      FieldName = 'cdsConveniosEnCarpetaDescripcionTipoDeuda'
      Size = 60
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaCodigoEmpresasRecaudadoras: TIntegerField
      FieldName = 'cdsConveniosEnCarpetaCodigoEmpresasRecaudadoras'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaCodigoTipoDeuda: TIntegerField
      FieldName = 'cdsConveniosEnCarpetaCodigoTipoDeuda'
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaDescripcionConcesionaria: TStringField
      FieldName = 'cdsConveniosEnCarpetaDescripcionConcesionaria'
      Size = 50
    end
    object cdsConveniosEnCarpetacdsConveniosEnCarpetaEnListaAmarilla: TBooleanField
      FieldName = 'cdsConveniosEnCarpetaEnListaAmarilla'
    end
  end
  object dsConveniosAIngresar: TDataSource
    DataSet = cdsConveniosAIngresar
    Left = 304
    Top = 336
  end
  object cdsConveniosAIngresar: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'cdsConveniosAIngSeleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'cdsConveniosAIngCodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosAIngNumeroConvenio'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosAIngApellido'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosAIngApellidoMaterno'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosAIngNombre'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cdsConveniosAIngNumeroDoc'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosAIngDias'
        DataType = ftInteger
      end
      item
        Name = 'cdsConveniosAIngDeuda'
        DataType = ftFloat
      end
      item
        Name = 'cdsConveniosAIngDeudaStr'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cdsConveniosAIngEnListaAmarilla'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 336
    Top = 336
    Data = {
      E30100009619E0BD01000000180000000B000000000003000000E3011C636473
      436F6E76656E696F7341496E6753656C656363696F6E61646F02000300000000
      001E636473436F6E76656E696F7341496E67436F6469676F436F6E76656E696F
      04000100000000001E636473436F6E76656E696F7341496E674E756D65726F43
      6F6E76656E696F01004900000001000557494454480200020014001863647343
      6F6E76656E696F7341496E674170656C6C69646F010049000000010005574944
      5448020002003C001F636473436F6E76656E696F7341496E674170656C6C6964
      6F4D617465726E6F0100490000000100055749445448020002003C0016636473
      436F6E76656E696F7341496E674E6F6D62726501004900000001000557494454
      48020002003C0019636473436F6E76656E696F7341496E674E756D65726F446F
      63010049000000010005574944544802000200140014636473436F6E76656E69
      6F7341496E6744696173040001000000000015636473436F6E76656E696F7341
      496E674465756461080004000000000018636473436F6E76656E696F7341496E
      67446575646153747201004900000001000557494454480200020014001F6364
      73436F6E76656E696F7341496E67456E4C69737461416D6172696C6C61020003
      00000000000000}
  end
  object mnuSeleccion: TPopupMenu
    Left = 240
    Top = 272
    object mnuSeleccionar: TMenuItem
      Caption = 'Seleccionar / Deseleccionar (espacio)'
      OnClick = mnuSeleccionarClick
    end
    object mnuSeleccionarTodos: TMenuItem
      Caption = 'Seleccionar todos'
      OnClick = mnuSeleccionarTodosClick
    end
    object mnuDeseleccionarTodos: TMenuItem
      Caption = 'Deseleccionar todos'
      OnClick = mnuDeseleccionarTodosClick
    end
  end
  object spActualizarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CarpetasMorosos_ActualizarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IdConvenioCarpetaLegal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@FechaHoraImpresion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioImpresion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraEnvioLegales'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioEnvioLegales'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraSalidaLegales'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioSalidaLegales'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@UsuarioActualizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@DiasDeuda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteDeuda'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDeuda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 312
    Top = 378
  end
  object spConvenioExisteEnCarpeta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CarpetasMorosos_ConvenioExisteEnCarpeta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConvenioCargado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@FechaHoraSalidaLegales'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 352
    Top = 378
  end
  object Img_Tilde: TImageList
    Left = 434
    Top = 318
    Bitmap = {
      494C010102000400440010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object spEliminarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CarpetasMorosos_EliminarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdConvenioCarpetaLegal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@UsuarioElimina'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraActualizacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 408
    Top = 160
  end
  object spObtenerComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctDynamic
    ProcedureName = 'CarpetasMorosos_ObtenerComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 376
    Top = 224
  end
end
