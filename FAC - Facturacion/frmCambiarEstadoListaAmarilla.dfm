object CambiarEstadoListaAmarillaForm: TCambiarEstadoListaAmarillaForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Cambiar Estado'
  ClientHeight = 247
  ClientWidth = 320
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 300
    Height = 177
  end
  object lblFechaCarta: TLabel
    Left = 24
    Top = 107
    Width = 89
    Height = 26
    AutoSize = False
    Caption = 'Fecha Env'#237'o Carta de notificaci'#243'n:'
    WordWrap = True
  end
  object Label2: TLabel
    Left = 42
    Top = 84
    Width = 71
    Height = 13
    Caption = 'Nuevo estado:'
  end
  object Label3: TLabel
    Left = 64
    Top = 14
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Convenio:'
  end
  object Label4: TLabel
    Left = 43
    Top = 52
    Width = 70
    Height = 13
    Caption = 'Estado Actual:'
  end
  object lblEstadoActual: TLabel
    Left = 131
    Top = 52
    Width = 66
    Height = 13
    Caption = 'Estado Actual'
  end
  object lblConvenio: TLabel
    Left = 131
    Top = 14
    Width = 98
    Height = 13
    Caption = 'XXX-XXXXXXXXX-XXX'
  end
  object lblEmpresaCorreo: TLabel
    Left = 42
    Top = 145
    Width = 71
    Height = 29
    AutoSize = False
    Caption = 'Empresa de Mensajer'#237'a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object lblConcesionaria: TLabel
    Left = 42
    Top = 32
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Concesionaria:'
  end
  object lblConcesionariaDato: TLabel
    Left = 131
    Top = 32
    Width = 67
    Height = 13
    Caption = 'Concesionaria'
  end
  object btnCambiar: TButton
    Left = 122
    Top = 209
    Width = 75
    Height = 25
    Caption = 'Cambiar'
    TabOrder = 0
    OnClick = btnCambiarClick
  end
  object btnCancelar: TButton
    Left = 225
    Top = 209
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 1
    OnClick = btnCancelarClick
  end
  object deFechaCarta: TDateEdit
    Left = 131
    Top = 110
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 2
    Date = -693594.000000000000000000
  end
  object vcbNuevoEstado: TVariantComboBox
    Left = 131
    Top = 81
    Width = 166
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 3
    OnChange = vcbNuevoEstadoChange
    Items = <>
  end
  object cboEmpresaCorreo: TVariantComboBox
    Left = 131
    Top = 145
    Width = 166
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 4
    Items = <>
  end
  object spCambiarEstadoConvenioListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CambiarEstadoConvenioListaAmarilla'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEnvioCarta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NuevoEstado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 8
    Top = 184
  end
  object dsConveniosEnCarpeta: TDataSource
    DataSet = cdsConveniosEnCarpeta
    Left = 40
    Top = 216
  end
  object cdsConveniosEnCarpeta: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'IdConvenioCarpetaLegal'
        DataType = ftInteger
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'Apellido'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'ApellidoMaterno'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Nombre'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FechaHoraImpresion'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioImpresion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraEnvioALegales'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioEnvioLegales'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraSalidaLegales'
        DataType = ftDateTime
      end
      item
        Name = 'FechaHoraCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraActualizacion'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioActualizacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroDocumento'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'NumeroConvenio'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'ApellidoMostrar'
        DataType = ftString
        Size = 155
      end
      item
        Name = 'Dias'
        DataType = ftInteger
      end
      item
        Name = 'DeudaStr'
        DataType = ftCurrency
      end
      item
        Name = 'UsuarioSalidaLegales'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DiasDeudaTotal'
        DataType = ftInteger
      end
      item
        Name = 'ImporteDeudaActualizada'
        DataType = ftCurrency
      end
      item
        Name = 'AliasEmpresaRecaudadora'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescripcionTipoDeuda'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CodigoEmpresasRecaudadoras'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoDeuda'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionConcesionaria'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    FetchOnDemand = False
    Params = <>
    StoreDefs = True
    Left = 72
    Top = 184
    Data = {
      BB0300009619E0BD01000000180000001C000000000003000000BB0316496443
      6F6E76656E696F436172706574614C6567616C04000100000000000E436F6469
      676F436F6E76656E696F0400010000000000084170656C6C69646F0100490000
      000100055749445448020002003C000F4170656C6C69646F4D617465726E6F01
      00490000000100055749445448020002003C00064E6F6D627265010049000000
      0100055749445448020002001E00124665636861486F7261496D70726573696F
      6E0800080000000000105573756172696F496D70726573696F6E010049000000
      0100055749445448020002001400164665636861486F7261456E76696F414C65
      67616C65730800080000000000135573756172696F456E76696F4C6567616C65
      730100490000000100055749445448020002001400164665636861486F726153
      616C6964614C6567616C65730800080000000000114665636861486F72614372
      656163696F6E08000800000000000F5573756172696F4372656163696F6E0100
      490000000100055749445448020002001400164665636861486F726141637475
      616C697A6163696F6E0800080000000000145573756172696F41637475616C69
      7A6163696F6E01004900000001000557494454480200020014000F4E756D6572
      6F446F63756D656E746F0100490000000100055749445448020002000B000E4E
      756D65726F436F6E76656E696F01004900000001000557494454480200020014
      000C53656C656363696F6E61646F02000300000000000F4170656C6C69646F4D
      6F73747261720100490000000100055749445448020002009B00044469617304
      0001000000000008446575646153747208000400000001000753554254595045
      0200490006004D6F6E657900145573756172696F53616C6964614C6567616C65
      7301004900000001000557494454480200020014000E44696173446575646154
      6F74616C040001000000000017496D706F727465446575646141637475616C69
      7A616461080004000000010007535542545950450200490006004D6F6E657900
      17416C696173456D707265736152656361756461646F72610100490000000100
      055749445448020002003C00144465736372697063696F6E5469706F44657564
      610100490000000100055749445448020002003C001A436F6469676F456D7072
      6573617352656361756461646F72617304000100000000000F436F6469676F54
      69706F44657564610400010000000000184465736372697063696F6E436F6E63
      6573696F6E6172696101004900000001000557494454480200020032000000}
    object cdsIdConvenioCarpetaLegal: TIntegerField
      FieldName = 'IdConvenioCarpetaLegal'
    end
    object cdsCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsApellido: TStringField
      FieldName = 'Apellido'
      Size = 60
    end
    object cdsApellidoMaterno: TStringField
      FieldName = 'ApellidoMaterno'
      Size = 60
    end
    object cdsNombre: TStringField
      FieldName = 'Nombre'
      Size = 30
    end
    object cdsFechaHoraImpresion: TDateTimeField
      FieldName = 'FechaHoraImpresion'
    end
    object cdsUsuarioImpresion: TStringField
      FieldName = 'UsuarioImpresion'
    end
    object cdsFechaHoraEnvioALegales: TDateTimeField
      FieldName = 'FechaHoraEnvioALegales'
    end
    object cdsUsuarioEnvioLegales: TStringField
      FieldName = 'UsuarioEnvioLegales'
    end
    object cdsFechaHoraSalidaLegales: TDateTimeField
      FieldName = 'FechaHoraSalidaLegales'
    end
    object cdsFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object cdsUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
    end
    object cdsFechaHoraActualizacion: TDateTimeField
      FieldName = 'FechaHoraActualizacion'
    end
    object cdsUsuarioActualizacion: TStringField
      FieldName = 'UsuarioActualizacion'
    end
    object cdsNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      Size = 11
    end
    object cdsNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
    end
    object cdsSeleccionado: TBooleanField
      FieldName = 'Seleccionado'
    end
    object cdsApellidoMostrar: TStringField
      FieldName = 'ApellidoMostrar'
      Size = 155
    end
    object cdsDias: TIntegerField
      FieldName = 'Dias'
    end
    object cdsDeudaStr: TCurrencyField
      FieldName = 'DeudaStr'
    end
    object cdsUsuarioSalidaLegales: TStringField
      FieldName = 'UsuarioSalidaLegales'
    end
    object cdsDiasDeudaTotal: TIntegerField
      FieldName = 'DiasDeudaTotal'
    end
    object cdsImporteDeudaActualizada: TCurrencyField
      FieldName = 'ImporteDeudaActualizada'
    end
    object cdsAliasEmpresaRecaudadora: TStringField
      FieldName = 'AliasEmpresaRecaudadora'
      Size = 60
    end
    object cdsDescripcionTipoDeuda: TStringField
      FieldName = 'DescripcionTipoDeuda'
      Size = 60
    end
    object cdsCodigoEmpresasRecaudadoras: TIntegerField
      FieldName = 'CodigoEmpresasRecaudadoras'
    end
    object cdsCodigoTipoDeuda: TIntegerField
      FieldName = 'CodigoTipoDeuda'
    end
    object cdsDescripcionConcesionaria: TStringField
      FieldName = 'DescripcionConcesionaria'
      Size = 50
    end
  end
  object spConveniosEnCarpetaLegal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CarpetasMorosos_ObtenerConvenio;1'
    Parameters = <
      item
        Name = '@CodigoConvenio'
        DataType = ftInteger
        Value = Null
      end>
    Left = 40
    Top = 184
  end
  object spActualizarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CarpetasMorosos_ActualizarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IdConvenioCarpetaLegal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@FechaHoraImpresion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioImpresion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraEnvioLegales'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioEnvioLegales'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraSalidaLegales'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioSalidaLegales'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@UsuarioActualizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@DiasDeuda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteDeuda'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDeuda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 72
    Top = 216
  end
  object cdsExcluidos: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroConvenio'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 8
    Top = 216
    Data = {
      3D0000009619E0BD0100000018000000010000000000030000003D000E4E756D
      65726F436F6E76656E696F010049000000010005574944544802000200140000
      00}
    object cdsExcluidosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
    end
  end
  object cdsSacadosLegales: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 104
    Top = 184
    Data = {
      3D0000009619E0BD0100000018000000010000000000030000003D000E4E756D
      65726F436F6E76656E696F010049000000010005574944544802000200140000
      00}
    object cdsSacadosLegalesNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
    end
  end
  object spReglaValidacionListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ReglaValidacionListaAmarilla'
    Parameters = <>
    Left = 273
    Top = 16
  end
end
