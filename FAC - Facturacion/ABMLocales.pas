unit ABMLocales;


{

Firma       : SS_1147_MCA_20150422
Descripcion : se corrige error de focus
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB,DMConnection,
  DPSControls, FreTelefono, FreDomicilio, PeaTypes, Variants;

type
  TFormLocales = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Locales: TADOTable;
    GroupB: TPanel;
    lblEmpresa: TLabel;
    lblCodigo: TLabel;
    Panel1: TPanel;
    Notebook: TNotebook;
    txtEmpresa: TEdit;
    txtNumeroLocal: TEdit;
    FrameDomicilio: TFrameDomicilio;
    FrameTelefono: TFrameTelefono;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    LocalesNumeroLocal: TAutoIncField;
    LocalesEmpresa: TStringField;
    LocalesCodigoMedioComunicacion: TIntegerField;
    LocalesCodigoDomicilio: TIntegerField;
    LocalesFechaHoraCreacion: TDateTimeField;
    LocalesUsuarioCreacion: TStringField;
    LocalesFechaHoraActualizacion: TDateTimeField;
    LocalesUsuarioActualizacion: TStringField;
    spObtenerDomicilioLocal: TADOStoredProc;
    spObtenerMedioComunicacionLocal: TADOStoredProc;
    spActualizarDomicilio: TADOStoredProc;
    spActualizarMedioComunicacionLocal: TADOStoredProc;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function GuardarDomicilio: integer;
    function GuardarMedioComunicacion:integer;

  private
	{ Private declarations }
    procedure Limpiar_Campos;
    procedure TraerDomicilio;
    procedure TraerTelefono;    
  public
	{ Public declarations }
	Function Inicializar : boolean;
  end;

var
  FormLocales  : TFormLocales;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Local?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Local porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Local';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Local.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Local';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';

{$R *.DFM}

function TFormLocales.Inicializar: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    FrameDomicilio.Inicializa();
    FrameTelefono.Inicializa('Tel�fomo Principal', true, False, False);
	if not OpenTables([Locales]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;
    Notebook.PageIndex := 0;                        //SS_1147_MCA_20150422
end;

procedure TFormLocales.Limpiar_Campos();
begin
    txtEmpresa.Clear;
    txtNumeroLocal.Clear;
    FrameTelefono.Limpiar;
    FrameDomicilio.Limpiar;
end;

function TFormLocales.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
//	Texto :=  PadR(Trim(Tabla.FieldByName('TipoComprobante').AsString), 4, ' ' ) + ' '+
//	  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormLocales.BtSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormLocales.ListaInsert(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Alta;
	Limpiar_Campos;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txtEmpresa.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormLocales.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txtEmpresa.SetFocus;
	Screen.Cursor    := crDefault;

end;

procedure TFormLocales.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			Locales.Delete;
		Except
			On E: EDataBaseError do begin
				Locales.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormLocales.ListaClick(Sender: TObject);
begin
    txtEmpresa.Text	    := Trim(Locales.FieldByName('Empresa').AsString);
    txtNumeroLocal.Text	:= Trim(Locales.FieldByName('NumeroLocal').AsString);
    TraerDomicilio;
end;

procedure TFormLocales.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormLocales.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormLocales.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('NumeroLocal').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Empresa').AsString);
	end;
end;

procedure TFormLocales.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormLocales.BtnAceptarClick(Sender: TObject);
begin
    if not FrameDomicilio.ValidarDomicilio then Exit;
    if not ValidateControls([txtEmpresa],
                [trim(txtEmpresa.text) <> ''],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION]) then exit;
	Screen.Cursor := crHourGlass;
	With Locales do begin
		Try
			if Lista.Estado = Alta then Append else Edit;

            FieldByName('Empresa').AsString     := txtEmpresa.Text;

            if Lista.Estado = Alta then begin
              FieldByName('UsuarioCreacion').AsString 	  := UsuarioSistema;
              FieldByName('FechaHoraCreacion').AsDateTime := NowBase(Locales.Connection);
            end;

			FieldByName('UsuarioActualizacion').AsString 	  := UsuarioSistema;
			FieldByName('FechaHoraActualizacion').AsDateTime := NowBase(Locales.Connection);
            FieldByName('CodigoDomicilio').AsInteger := GuardarDomicilio;
            FieldByName('CodigoMedioComunicacion').AsInteger := GuardarMedioComunicacion;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado     	:= Normal;
	Lista.Enabled    	:= True;
	GroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
    TraerDomicilio;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormLocales.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormLocales.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormLocales.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormLocales.TraerDomicilio;
var
    Domicilio: TDatosDomicilio;
begin
    spObtenerDomicilioLocal.Close;
    spObtenerDomicilioLocal.Parameters.Refresh;
    spObtenerDomicilioLocal.Parameters.ParamByName('@NumeroLocal').Value   := StrToInt(txtNumeroLocal.Text);
    spObtenerDomicilioLocal.Open;
    Domicilio.CodigoCalle           := iif( spObtenerDomicilioLocal.FieldByName('CodigoCalle').IsNull, -1,spObtenerDomicilioLocal.FieldByName('CodigoCalle').AsInteger);
    Domicilio.CodigoSegmento        := iif(spObtenerDomicilioLocal.FieldByName('CodigoSegmento').IsNull, -1, spObtenerDomicilioLocal.FieldByName('CodigoSegmento').AsInteger);
    Domicilio.NumeroCalleSTR        := spObtenerDomicilioLocal.FieldByName('Numero').AsString;
    Domicilio.NumeroCalle           := FormatearNumeroCalle(spObtenerDomicilioLocal.FieldByName('Numero').AsString);
    Domicilio.Detalle               := spObtenerDomicilioLocal.FieldByName('Detalle').AsString;
    Domicilio.CodigoPais            := spObtenerDomicilioLocal.FieldByName('CodigoPais').AsString;
    Domicilio.CodigoRegion          := spObtenerDomicilioLocal.FieldByName('CodigoRegion').AsString;
    Domicilio.CodigoComuna          := spObtenerDomicilioLocal.FieldByName('CodigoComuna').AsString;
    Domicilio.CalleDesnormalizada   := iif(spObtenerDomicilioLocal.FieldByName('CodigoCalle').IsNull, spObtenerDomicilioLocal.FieldByName('CalleDesnormalizada').AsString,'');
    Domicilio.DescripcionCiudad     := spObtenerDomicilioLocal.FieldByName('DescriComuna').AsString;;
    Domicilio.CodigoPostal          := spObtenerDomicilioLocal.FieldByName('CodigoPostal').AsString;
    Domicilio.CodigoDomicilio       := spObtenerDomicilioLocal.FieldByName('CodigoDomicilio').AsInteger;
    FrameDomicilio.CargarDatosDomicilio(Domicilio);
    TraerTelefono;
end;





procedure TFormLocales.TraerTelefono;
begin
    spObtenerMedioComunicacionLocal.Close;
    spObtenerMedioComunicacionLocal.Parameters.Refresh;
    spObtenerMedioComunicacionLocal.Parameters.ParamByName('@CodigoMedioComunicacion').Value := Locales.FieldByName('CodigoMedioComunicacion').asInteger;
    spObtenerMedioComunicacionLocal.Open;
    FrameTelefono.CargarTelefono(spObtenerMedioComunicacionLocal.FieldByName('CodigoArea').Value,
		spObtenerMedioComunicacionLocal.FieldByName('Valor').Value,
		1,
		spObtenerMedioComunicacionLocal.FieldByName('HorarioDesde').Value,
		spObtenerMedioComunicacionLocal.FieldByName('HorarioHasta').Value,
        Locales.FieldByName('CodigoMedioComunicacion').asInteger);

end;

function TFormLocales.GuardarDomicilio: integer;
begin
    spActualizarDomicilio.Parameters.ParamByName('@CodigoTipoDomicilio').Value := 1;

    spActualizarDomicilio.Parameters.ParamByName('@CodigoPais').Value           := FrameDomicilio.Pais;
    spActualizarDomicilio.Parameters.ParamByName('@CodigoRegion').Value         := FrameDomicilio.Region;
    spActualizarDomicilio.Parameters.ParamByName('@CodigoComuna').Value         := FrameDomicilio.Comuna;
    spActualizarDomicilio.Parameters.ParamByName('@DescripcionCiudad').Value    := FrameDomicilio.DescriComuna;
    spActualizarDomicilio.Parameters.ParamByName('@CodigoCalle').Value          := iif( FrameDomicilio.CodigoCalle <1, null, FrameDomicilio.CodigoCalle);
    spActualizarDomicilio.Parameters.ParamByName('@CodigoSegmento').Value       := FrameDomicilio.CodigoSegmento;
    spActualizarDomicilio.Parameters.ParamByName('@CalleDesnormalizada').Value  := iif( FrameDomicilio.CodigoCalle >=1 , null, Trim(FrameDomicilio.DescripcionCalle)) ;
    spActualizarDomicilio.Parameters.ParamByName('@Numero').Value               := FrameDomicilio.Numero;
    spActualizarDomicilio.Parameters.ParamByName('@Piso').Value                 := iif( FrameDomicilio.Piso < 1 , null, FrameDomicilio.Piso);
    spActualizarDomicilio.Parameters.ParamByName('@Depto').Value                := iif( FrameDomicilio.Depto = '', null, FrameDomicilio.Depto);
    spActualizarDomicilio.Parameters.ParamByName('@CodigoPostal').Value         := iif( FrameDomicilio.CodigoPostal = '', null, FrameDomicilio.CodigoPostal);
    spActualizarDomicilio.Parameters.ParamByName('@Detalle').Value              := iif( FrameDomicilio.Detalle = '', null, FrameDomicilio.Detalle);
    spActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value      := FrameDomicilio.CodigoDomicilio;
    spActualizarDomicilio.ExecProc ;
    Result := spActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value ;

end;





function TFormLocales.GuardarMedioComunicacion: integer;
begin
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@CodigoArea'             ).Value := FrameTelefono.CodigoArea;
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@Valor'                  ).Value := FrameTelefono.NumeroTelefono;
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@Anexo'                  ).Value := '';
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@HorarioDesde'           ).Value := FrameTelefono.HorarioDesde;
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@HorarioHasta'           ).Value := FrameTelefono.HorarioHasta;
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@CodigoDomicilio'        ).Value := spActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@Observaciones'          ).Value := '';
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@EstadoVerificacion'     ).Value := 'N';
    spActualizarMedioComunicacionLocal.Parameters.ParamByName('@CodigoMedioComunicacion').Value := Locales.FieldByName('CodigoMedioComunicacion').asInteger;
    spActualizarMedioComunicacionLocal.ExecProc;
    Result := spActualizarMedioComunicacionLocal.Parameters.ParamByName('@CodigoMedioComunicacion').Value;

end;

end.
