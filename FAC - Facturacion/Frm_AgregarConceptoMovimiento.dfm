object frmAgregarConceptoMovimiento: TfrmAgregarConceptoMovimiento
  Left = 346
  Top = 189
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Agregar Concepto de Movimiento'
  ClientHeight = 198
  ClientWidth = 467
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    467
    198)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 452
    Height = 153
  end
  object Label1: TLabel
    Left = 20
    Top = 24
    Width = 30
    Height = 13
    Caption = 'Fecha'
  end
  object Label2: TLabel
    Left = 20
    Top = 79
    Width = 46
    Height = 13
    Caption = 'Concepto'
  end
  object Label3: TLabel
    Left = 20
    Top = 107
    Width = 33
    Height = 13
    Caption = 'Detalle'
  end
  object Label4: TLabel
    Left = 20
    Top = 135
    Width = 35
    Height = 13
    Caption = 'Importe'
  end
  object Label5: TLabel
    Left = 20
    Top = 51
    Width = 105
    Height = 13
    Caption = 'Tipo de Comprobante:'
  end
  object edFecha: TDateEdit
    Left = 137
    Top = 20
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    Date = -693594.000000000000000000
  end
  object cbConcepto: TVariantComboBox
    Left = 137
    Top = 75
    Width = 243
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = cbConceptoChange
    Items = <>
  end
  object btnCancelar: TButton
    Left = 386
    Top = 168
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 6
    OnClick = btnCancelarClick
  end
  object btnAceptar: TButton
    Left = 307
    Top = 168
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 5
    OnClick = btnAceptarClick
  end
  object edImporte: TNumericEdit
    Left = 137
    Top = 131
    Width = 94
    Height = 21
    MaxLength = 11
    TabOrder = 3
    Decimals = 0
  end
  object edDetalle: TEdit
    Left = 137
    Top = 103
    Width = 243
    Height = 21
    MaxLength = 59
    TabOrder = 2
  end
  object chk_ImporteIncluyeIVA: TCheckBox
    Left = 249
    Top = 133
    Width = 129
    Height = 17
    Hint = 'Indica si el importe ingresado ya incluye el I.V.A.'
    Caption = 'Importe incluye I.V.A:'
    TabOrder = 4
    Visible = False
  end
  object btnConceptos: TButton
    Left = 389
    Top = 75
    Width = 65
    Height = 22
    Caption = '&Consultar'
    TabOrder = 7
    OnClick = btnConceptosClick
  end
  object VcTipoComprobante: TVariantComboBox
    Left = 136
    Top = 47
    Width = 243
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 8
    OnChange = VcTipoComprobanteChange
    Items = <
      item
        Caption = 'Nota de Cobro'
        Value = 'NK'
      end
      item
        Caption = 'Todos'
        Value = 'Null'
      end>
  end
  object spObtenerConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConceptosComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 9
    Top = 161
  end
end
