{********************************** File Header ********************************
File Name : ABMTalonariosComprobantesLocales.pas
Author : jconcheyro
Date Created: 06/09/2006
Language : ES-AR
Description :
*******************************************************************************}
unit ABMTalonariosComprobantesLocales;

{
Firma       : SS_1147_MCA_20150422
Descripcion : se corrige error de focus
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB,DMConnection,
  DPSControls, VariantComboBox, Validate;

type
  TFormABMTalonariosComprobantesLocales = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    TalonariosComprobantesLocales: TADOTable;
    GroupB: TPanel;
    lblTipoComprobante: TLabel;
    lblLocal: TLabel;
    Panel1: TPanel;
    Notebook: TNotebook;
    TalonariosComprobantesLocalesTipoComprobante: TStringField;
    TalonariosComprobantesLocalesNumeroLocal: TIntegerField;
    TalonariosComprobantesLocalesNumeroComprobanteDesde: TIntegerField;
    TalonariosComprobantesLocalesNumeroComprobanteHasta: TIntegerField;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    cbTipoComprobante: TVariantComboBox;
    neDesdeNumero: TNumericEdit;
    neHastaNumero: TNumericEdit;
    lblDesdeNumero: TLabel;
    lblHastaNumero: TLabel;
    cbLocales: TVariantComboBox;
    spObtenerTiposComprobantesPrenumerados: TADOStoredProc;
    spObtenerLocalesComprobantesPrenumerados: TADOStoredProc;
    spObtenerDomicilioLocal: TADOStoredProc;
    TalonariosComprobantesLocalesFechaHoraCreacion: TDateTimeField;
    TalonariosComprobantesLocalesUsuarioCreacion: TStringField;
    TalonariosComprobantesLocalesFechaHoraActualizacion: TDateTimeField;
    TalonariosComprobantesLocalesUsuarioActualizacion: TStringField;
    GroupBox1: TGroupBox;
    lblRegionTxt: TLabel;
    lblRegion: TLabel;
    lblCalletxt: TLabel;
    lblCalle: TLabel;
    lblNumeroTxt: TLabel;
    lblNumero: TLabel;
    lblComunaTxt: TLabel;
    lblComuna: TLabel;
    TalonariosComprobantesLocalesIDTalonariosComprobantesLocales: TAutoIncField;
    deFechaEmisionTalonario: TDateEdit;
    deFechaVencimientoTalonario: TDateEdit;
    TalonariosComprobantesLocalesFechaEmision: TDateTimeField;
    TalonariosComprobantesLocalesFechaVencimiento: TDateTimeField;
    lblEmisionTalonario: TLabel;
    Label2: TLabel;
    procedure cbLocalesChange(Sender: TObject);

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure CargarLocales;
    procedure CargarTiposComprobantes;


  private
	{ Private declarations }
    procedure Limpiar_Campos;
    function SolapaConRango(TipoComprobante: string;  DesdeNumero, HastaNumero,
      RegistroActual: integer): boolean;
  public
	{ Public declarations }
	Function Inicializar : boolean;
  end;

var
  FormABMTalonariosComprobantesLocales  : TFormABMTalonariosComprobantesLocales;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Talonario del Local?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Talonario del Local porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Talonario del Local';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Talonario del Local.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Talonario del Local';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';

{$R *.DFM}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : None
Return Value : boolean
*******************************************************************************}
function TFormABMTalonariosComprobantesLocales.Inicializar: boolean;
Var
	S: TSize;
begin
//	S := GetFormClientSize(Application.MainForm);
//	SetBounds(0, 0, S.cx, S.cy);
    CenterForm(Self);
	if not OpenTables([TalonariosComprobantesLocales]) then
    	Result := False
    else begin
        CargarLocales;
        CargarTiposComprobantes;
    	Result := True;
       	Lista.Reload;
	end;

    Notebook.PageIndex := 0;                            //SS_1147_MCA_20150422

end;

{******************************** Function Header ******************************
Function Name: Limpiar_Campos
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters :
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.Limpiar_Campos();
begin
	neDesdeNumero.Clear;
	neHastaNumero.Clear;
    deFechaEmisionTalonario.Date := now;
    deFechaVencimientoTalonario.Date := now;
    lblRegion.Caption := '';
    lblCalle.Caption := '';
    lblNumero.Caption := '';
    lblComuna.Caption := '';
end;

{******************************** Function Header ******************************
Function Name: ListaProcess
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Tabla: TDataSet; var Texto: string
Return Value : Boolean
*******************************************************************************}
function TFormABMTalonariosComprobantesLocales.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
	Texto :=  PadR(Trim(Tabla.FieldByName('TipoComprobante').AsString), 4, ' ' ) + ' '+
	  Tabla.FieldByName('NumeroLocal').AsString;
end;

{******************************** Function Header ******************************
Function Name: BtSalirClick
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.BtSalirClick(Sender: TObject);
begin
	Close;
end;

{******************************** Function Header ******************************
Function Name: ListaInsert
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.ListaInsert(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Alta;
	Limpiar_Campos;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	cbLocales.SetFocus;
    cbLocalesChange(self);
	Screen.Cursor    := crDefault;
end;

{******************************** Function Header ******************************
Function Name: ListaEdit
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	Screen.Cursor    := crDefault;
    cbLocalesChange(self);
	cbLocales.SetFocus;

end;

{******************************** Function Header ******************************
Function Name: ListaDelete
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			TalonariosComprobantesLocales.Delete;
		Except
			On E: EDataBaseError do begin
				TalonariosComprobantesLocales.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

{******************************** Function Header ******************************
Function Name: ListaClick
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.ListaClick(Sender: TObject);
var
  index: integer;
begin
    for index := 0 to cbLocales.Items.count - 1 do
        if cbLocales.Items[index].Value = TalonariosComprobantesLocales.FieldByName('NumeroLocal').AsInteger then begin
            cbLocales.ItemIndex := index;
            Break;
        end;

    for index := 0 to cbTipoComprobante.Items.count - 1 do
        if cbTipoComprobante.Items[index].Value = trim(TalonariosComprobantesLocales.FieldByName('TipoComprobante').AsString) then begin
            cbTipoComprobante.ItemIndex := index;
            Break;
        end;

    neDesdeNumero.Value := TalonariosComprobantesLocales.FieldByName('NumeroComprobanteDesde').AsInteger;
    neHastaNumero.Value := TalonariosComprobantesLocales.FieldByName('NumeroComprobanteHasta').AsInteger;
    deFechaEmisionTalonario.Date := TalonariosComprobantesLocales.FieldByName('FechaEmision').AsDateTime;
    deFechaVencimientoTalonario.Date := TalonariosComprobantesLocales.FieldByName('FechaVencimiento').AsDateTime;
    cbLocalesChange(self);
end;

{******************************** Function Header ******************************
Function Name: ListaRefresh
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

{******************************** Function Header ******************************
Function Name: AbmToolbar1Close
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

{******************************** Function Header ******************************
Function Name: ListaDrawItem
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Tabla.FieldByName('NumeroLocal').AsString);
		TextOut(Cols[1], Rect.Top, Tabla.FieldByName('TipoComprobante').AsString);
		TextOut(Cols[2], Rect.Top, Tabla.FieldByName('NumeroComprobanteDesde').AsString);
		TextOut(Cols[3], Rect.Top, Tabla.FieldByName('NumeroComprobanteHasta').AsString);
	end;
end;

{******************************** Function Header ******************************
Function Name: FormShow
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_RANGO_SUPERPONE = 'El rango de comprobantes indicado se solapa con uno existente';
    MSG_ERROR_RANGO_ERRONEO = 'El rango de comprobantes indicado es err�neo';
    MSG_ERROR_RANGO_ERRONEO_FECHAS = 'El rango de fechas indicado es err�neo';

begin
    if not ValidateControls([neDesdeNumero,neHastaNumero],
                [neDesdeNumero.Value <> 0, neHastaNumero.Value <> 0 ],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION, MSG_DESCRIPCION]) then exit;

    if (neDesdeNumero.ValueInt > neHastaNumero.ValueInt) then begin
        MsgBoxErr(MSG_ERROR_RANGO_ERRONEO, MSG_ERROR_RANGO_ERRONEO, Caption, MB_ICONSTOP);
        Exit;
    end;

    if (deFechaVencimientoTalonario.Date < deFechaEmisionTalonario.Date ) then begin
        MsgBoxErr(MSG_ERROR_RANGO_ERRONEO, MSG_ERROR_RANGO_ERRONEO, Caption, MB_ICONSTOP);
        Exit;
    end;


    if (Lista.Estado = Alta) and (SolapaConRango(cbTipoComprobante.Value,neDesdeNumero.ValueInt,neHastaNumero.ValueInt, 0)) then begin
        MsgBoxErr(MSG_ERROR_RANGO_SUPERPONE, MSG_ERROR_RANGO_SUPERPONE, Caption, MB_ICONSTOP);
        Exit;
    end;

    if (Lista.Estado = Modi) and (SolapaConRango(cbTipoComprobante.Value,neDesdeNumero.ValueInt,neHastaNumero.ValueInt, TalonariosComprobantesLocales.FieldByName('IDTalonariosComprobantesLocales').AsInteger)) then begin
        MsgBoxErr(MSG_ERROR_RANGO_SUPERPONE, MSG_ERROR_RANGO_SUPERPONE, Caption, MB_ICONSTOP);
        Exit;
    end;



	Screen.Cursor := crHourGlass;
	With TalonariosComprobantesLocales do begin
		Try
			if Lista.Estado = Alta then Append else Edit;

			FieldByName('TipoComprobante').AsString   := cbTipoComprobante.Value;
			FieldByName('NumeroLocal').AsInteger 	   := cbLocales.Value;
            FieldByName('NumeroComprobanteDesde').AsInteger := neDesdeNumero.ValueInt;
            FieldByName('NumeroComprobanteHasta').AsInteger := neHastaNumero.ValueInt;
            FieldByName('FechaEmision').AsDateTime := deFechaEmisionTalonario.Date;
            FieldByName('FechaVencimiento').AsDateTime := deFechaVencimientoTalonario.Date;
            if Lista.Estado = Alta then begin
              FieldByName('UsuarioCreacion').AsString 	  := UsuarioSistema;
              FieldByName('FechaHoraCreacion').AsDateTime := NowBase(TalonariosComprobantesLocales.Connection);
            end;

			FieldByName('UsuarioActualizacion').AsString 	  := UsuarioSistema;
			FieldByName('FechaHoraActualizacion').AsDateTime := NowBase(TalonariosComprobantesLocales.Connection);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado     	:= Normal;
	Lista.Enabled    	:= True;
	GroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

{******************************** Function Header ******************************
Function Name: BtnCancelarClick
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: BtnSalirClick
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

{******************************** Function Header ******************************
Function Name: CargarTiposComprobantes
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : None
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.CargarTiposComprobantes;
begin
    spObtenerTiposComprobantesPrenumerados.Open;
    while not spObtenerTiposComprobantesPrenumerados.eof do begin
        cbTipoComprobante.Items.Add(spObtenerTiposComprobantesPrenumerados.FieldByName('Descripcion').Value, trim(spObtenerTiposComprobantesPrenumerados.FieldByName('TipoComprobante').Value));
        spObtenerTiposComprobantesPrenumerados.Next;
    end;
    spObtenerTiposComprobantesPrenumerados.Close;
    cbTipoComprobante.ItemIndex := 0;

end;

{******************************** Function Header ******************************
Function Name: CargarLocales
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : None
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.CargarLocales;
begin
    spObtenerLocalesComprobantesPrenumerados.Open;
    while not spObtenerLocalesComprobantesPrenumerados.eof do begin
        cbLocales.Items.Add(spObtenerLocalesComprobantesPrenumerados.FieldByName('Empresa').Value, spObtenerLocalesComprobantesPrenumerados.FieldByName('NumeroLocal').Value);
        spObtenerLocalesComprobantesPrenumerados.Next;
    end;
    spObtenerLocalesComprobantesPrenumerados.Close;
    cbLocales.ItemIndex := 0;
end;

{******************************** Function Header ******************************
Function Name: cbLocalesChange
Author : jconcheyro
Date Created : 06/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormABMTalonariosComprobantesLocales.cbLocalesChange(
  Sender: TObject);
begin
    spObtenerDomicilioLocal.Parameters.Refresh;
    spObtenerDomicilioLocal.Parameters.ParamByName('@NumeroLocal').Value := cbLocales.Value;
    spObtenerDomicilioLocal.Open;
    lblRegion.Caption := spObtenerDomicilioLocal.FieldByName('DescriRegion').AsString;
    lblCalle.Caption  := trim(spObtenerDomicilioLocal.FieldByName('DescriCalle').AsString);
    if lblCalle.Caption = '' then
        lblCalle.Caption := trim(spObtenerDomicilioLocal.FieldByName('CalleDesnormalizada').AsString);
    lblNumero.Caption := spObtenerDomicilioLocal.FieldByName('Numero').AsString;
    lblComuna.Caption := spObtenerDomicilioLocal.FieldByName('DescriComuna').AsString;
    spObtenerDomicilioLocal.Close;
end;

{******************************** Function Header ******************************
Function Name: SolapaConRango
Author : jconcheyro
Date Created : 06/09/2006
Description : Al momento de dar el alta o modificar un rango de comprobantes, debemos saber si no se va
superponer con uno existente. Hay que tener en cuenta que si est� modificando un registro, se puede dar esa
superposicion siempre y cuando el registro sea el que estamos editando. por eso el ultimo parametro
es el numero de registro, que en el caso de un alta se le pasa como cero

Parameters : TipoComprobante, DesdeNumero, HastaNumero, RegistroActual
Return Value : boolean
*******************************************************************************}
function TFormABMTalonariosComprobantesLocales.SolapaConRango( TipoComprobante: string;  DesdeNumero, HastaNumero, RegistroActual: integer ) : boolean;
begin
    Result := QueryGetValue(DMConnections.BaseCAC,
                Format('select dbo.TalonariosComprobantesLocalesSolapaConRango(''%s'',%d,%d,%d)',
                [TipoComprobante, DesdeNumero, HastaNumero, RegistroActual])) = 'True';

end;


end.
