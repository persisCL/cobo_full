{-----------------------------------------------------------------------------
 Unit Name: agendaFacturacion
 Author:
 Purpose:
 Description:
    - ClientDataSet se utiliza para almacenar los ciclos de un a�o. Se realizan
    filtros sobre �l para obtener los ciclos de un grupo del a�o.
 History:

 Revision 1:
  Author:    ggomez
  Date:      10-Feb-2005
  Description:
    - Agregu� que al hacer un Nuevo Calendario el operador debe
    ingresar el a�o del nuevo calendario.
    - En el combo cbAnios cambi� la propiedad Sorted a True. Porque ahora se
    permite ingresar un a�o anterior a los exitentes en el combo.
    - Al hacer nuevo calendario, no se generan ciclos para los grupos.

 Revision 2:
  Author:    ggomez
  Date:      11-Feb-2005
  Description:
    - Modifiqu� para que la imagen de los grupos sea la de facturado s�lo si
    todos los ciclos del grupo est�n facturados.

 Revision 3:
  Author:    ggomez
  Date:      14-Feb-2005
  Description:
    - Agregue la funci�n ExisteGrupoEnBD.
    - btn_AceptarClick: Agregu� c�digo para verificar que exista un grupo en la
        base de datos antes de hacer un Insert o un Update.

 Revision 4:
  Author:    ggomez
  Date:      16-Feb-2005
  Description:
    - Agregu� la funcionalidad Eliminar un calendario
    (btn_EliminarCalendarioClick).

 Revision 5:
  Author:    ggomez
  Date:      21-Feb-2005
  Description:
    - Cambi� que al seleccionar un mes para comenzar a generar ciclos de
    facturaci�n permita seleccionar cualquier mes y no s�lo el mes actual y los
    posteriores al actual.
    - Modifiqu� la funci�n ExisteGrupoEnBD para que use la funci�n de la BD
    ExisteGrupoFacturacion.

 Revision 6:
  Author:    ggomez
  Date:      22-Feb-2005
  Description:
    - Agregu� un label con la explicaci�n de que Fecha Corte Incluye los
    tr�nsitos producidos en dicha fecha de corte.
    - Agregu� el control de que la Fecha de Emisi�n debe ser posterior a la
    Fecha de Corte.
    - Elimin� la restriccion de que la Fecha de Emisi�n sea un d�a h�bil.

 Revision 7:
  Author:    ggomez
  Date:      23-Feb-2005
  Description:
    - Agregu� el control de que la Fecha Programada de Ejecuci�n debe ser
    posterior a la Fecha de Corte.
    - Cambi� la forma de generar las fechas al generar los ciclos de
    facturaci�n. Ahora las fechas se basan en la fecha de Corte y no en la de
    Ejecuci�n.
    - Agregu� el control de que las fechas de Corte de ciclos consecutivos,
    respeten una cantidad de d�as de diferencia. Esta cantidad de d�as se
    obtiene desde la tabla ParametrosGenerales.
    Para hacer esta funcionalidad agregu� el campo FactorFrecuencia a la tabla
    AgendaFacturacion, modifiqu� el sp ObtenerAgendaFacturacion para que retorne
    el campo FactorFrecuencia, modifiqu� los sp AgregarFechasAgendaFacturacion y
    ActualizarFechasAgendaFacturacion, y agregu� el campo FactoFrecuencia a
    ClientDataSet.

 Revision 8:
  Author:    ggomez
  Date:      24-Feb-2005
  Description:
    - Agregu� un campo m�s a la estructura TGrupoCiclo: AlMenosUnCicloFacturado.
    Este campo indica si al menos un ciclo del grupo est� facturado, es
    necesario porque, por un requisito (S�lo colocar la imagen del tilde sobre
    los grupos que tengan todos sus ciclos facturados), la utilizaci�n del campo
    YaFacturado para saber si un grupo ten�a al menos un grupo facturado, ya no
    era correcta.
    - Agregu� la funcionalidad de eliminar los ciclos de un grupo.

 Revision 9:
  Author:    jconcheyro
  Date:      28/12/2006
  Description: Creo la funcion RespetaAnioCalendarioMenos1 para la fecha de corte
  que puede ser del a�o anterior a la fecha de proceso.


Revision 10:
  Author : pdominguez
  Date   : 09/11/2009
  Description : SS 843
    - Se modificaron/a�adieron los siguientes procedimientos/funciones:
      tvGruposChange

 Revision 11:
  Author:       plaza
  Date:         15/02/2010
  Description:  SS 419
    - Se modifico el mensaje de error cuando se ingresa una fecha de corte
      invalida.

-----------------------------------------------------------------------------}
unit agendaFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, validate, Dateedit, Buttons, ExtCtrls, DmiCtrls, ComCtrls,
  Grids, DBGrids, CheckLst, UtilProc, DateUtils, DMConnection,
  DB, ADODB, math, util, DBClient, DBCtrls, ImgList, Menus,
  DPSControls, ListBoxEx, DBListEx, UtilDB, ConstParametrosGenerales,
  Peaprocs, FrmSeleccionarMes, FrmIngresarAnio;

type
  TfrmAgendaFacturacion = class(TForm)
    pnl_Datos: TPanel;
    gbMovSeleccionados: TGroupBox;
    gbFechas: TGroupBox;
    QryAnios: TADOQuery;
	qryFechasFacturacion: TADOQuery;
    ClientDataSet: TClientDataSet;
	cbAnios: TComboBox;
    lbl_Anio: TLabel;
    ImageList: TImageList;
    dsClientDataSet: TDataSource;
    Notebook: TNotebook;
    tvGrupos: TTreeView;
    deFechaVencimiento: TDateEdit;
    deFechaEmision: TDateEdit;
    deFechaCorte: TDateEdit;
	deFechaProgramada: TDateEdit;
    lbl_FechaProgramada: TLabel;
    lbl_FechaDeCorte: TLabel;
    lbl_FechaDeEmision: TLabel;
    lbl_FechaDeVencimiento: TLabel;
    lbl_Estado: TLabel;
    gb_InfoGrupo: TGroupBox;
    lbl_Grupos: TLabel;
    lbl_Frecuencia: TLabel;
    ObtenerGruposPorAnioAgenda: TADOStoredProc;
    EliminarCiclos: TADOQuery;
	dbgCiclos: TDBListEx;
    spObtenerAgendafacturacion: TADOStoredProc;
    InsertarFechas: TADOStoredProc;
    UpdateFechas: TADOStoredProc;
    lbl_DebeSerDiaHabil: TLabel;
    sp_EliminarCalendario: TADOStoredProc;
    lbl_FechaCorteHint: TLabel;
    btnSalir: TButton;
    btn_NuevoCalendario: TButton;
    btn_EliminarCalendario: TButton;
    btnQuincenal: TButton;
    btnMensual: TButton;
    btnBimestral: TButton;
    btn_EliminarCiclosGrupo: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnSemanal: TButton;                                                        // TASK_133_MGO_20170207
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure cbAniosChange(Sender: TObject);
    procedure tvGruposChange(Sender: TObject; Node: TTreeNode);
    procedure dsClientDataSetDataChange(Sender: TObject; Field: TField);
	procedure pmiQuincenalClick(Sender: TObject);
    procedure pmiDefaultClick(Sender: TObject);
    procedure pmiMensualClick(Sender: TObject);
	procedure pmiBimestralClick(Sender: TObject);
	//procedure PopupMenuPopup(Sender: TObject);                                // TASK_133_MGO_20170207
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure btnCancelarClick(Sender: TObject);
	procedure ClientDataSetAfterPost(DataSet: TDataSet);
	procedure deFechaProgramadaChange(Sender: TObject);
	procedure cbAniosEnter(Sender: TObject);
    procedure tvGruposChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure btn_NuevoCalendarioClick(Sender: TObject);
    procedure btn_EliminarCalendarioClick(Sender: TObject);
    procedure pmiEliminarCiclosGrupoClick(Sender: TObject);
    procedure btnSemanalClick(Sender: TObject);
  private
	{ Private declarations }
    (* Para mantiener el ItemIndex, del combo de a�os, seleccionado previamente
    a la ejecuci�n del evento Change. *)
    FItemIndexAnioAnterior: Integer;
	FCantidadGrupos, FAnio, FItemIndexAnioOld: Integer;
    // Indica si se est� insertando un nuevo caledario.
	FInsertando: Boolean;
	FCargandoDataSet: Boolean;              
    FFechasSoloDiaHabil: Boolean;   // TASK_133_MGO_20170207

	function FechaValida (Fecha: TDateTime) : Boolean;
	procedure LimpiarTree;
	procedure InicializarCiclos(Node: TTreeNode; FactorFrecuencia: Double; MesDesdeDondeGenerar: Word);
	procedure CargarCiclos;
	procedure EliminarCiclosGrupo;
    function IngresarAnio: AnsiString;
    function SeleccionarMes: Integer;

    function ObtenerProximoDiaHabil(Fecha: TDateTime): TDateTime;
    function IncrementarFecha(Fecha: TDateTime; IncrementoMes: Double): TDateTime;

    function ObtenerIndiceNodo(Grupo, Ciclo: Integer): Integer;
    function VerificarDiasHabilesCiclos: Boolean;
    procedure CargarGruposEnArbol(Anio: Integer);
    function ConfirmarCambios(Sender: TObject): Integer;
    procedure RealizarGeneracionCiclos(Frecuencia: Double);

    function RespetaAnioCalendario(Fecha: TDateTime; Anio: Word): Boolean;

    function ExisteGrupoEnBD(Conn: TADOConnection; CodigoGrupo: Integer): Boolean;

    function CiclosCalendarioTodosNoFacturados(CodigoGrupo: Integer): Boolean;

    function VerificarFechasCorteCiclos: Boolean;
    function RespetaAnioCalendarioMenos1(Fecha: TDateTime; Anio: Word): Boolean;
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;


implementation

{$R *.dfm}
resourcestring
	MSG_PRIMERO     = 'Primero';
    MSG_SEGUNDO     = 'Segundo';
    MSG_TERCERO     = 'Tercero';
    MSG_CUARTO      = 'Cuarto';
    MSG_QUINTO      = 'Quinto';
    MSG_SEXTO       = 'Sexto';
	MSG_SEPTIMO     = 'S�ptimo';
	MSG_OCTAVO      = 'Octavo';
    MSG_NOVENO      = 'Noveno';
	MSG_DECIMO      = 'D�cimo';
    MSG_DECIMOPRIMERO = 'D�cimo Primero';
    MSG_DECIMOSEGUNDO = 'D�cimo Segundo';
    MSG_DECIMOTERCERO = 'D�cimo Tercero';
    MSG_DECIMOCUARTO  = 'D�cimo Cuarto';
    MSG_DECIMOQUINTO  = 'D�cimo Quinto';
    MSG_DECIMOSEXTO   = 'D�cimo Sexto';
	MSG_DECIMOSEPTIMO = 'D�cimo S�ptimo';
    MSG_DECIMOOCTAVO  = 'D�cimo Octavo';
	MSG_DECIMONOVENO  = 'D�cimo Noveno';
    MSG_VIGESIMO      = 'Vig�simo';
	MSG_VIGESIMOPRIMERO = 'Vig�simo Primero';
	MSG_VIGESIMOSEGUNDO = 'Vig�simo Segundo';
	MSG_VIGESIMOTERCERO = 'Vig�simo Tercero';
	MSG_VIGESIMOCUARTO = 'Vig�simo Cuarto';

    MSG_NUEVO_CALENDARIO = 'Nuevo Calendario';

const
	NOMBRE_CICLOS: array[1..24] of string = (MSG_PRIMERO, MSG_SEGUNDO, MSG_TERCERO, MSG_CUARTO, MSG_QUINTO,
      MSG_SEXTO, MSG_SEPTIMO, MSG_OCTAVO, MSG_NOVENO, MSG_DECIMO, MSG_DECIMOPRIMERO, MSG_DECIMOSEGUNDO,
      MSG_DECIMOTERCERO, MSG_DECIMOCUARTO, MSG_DECIMOQUINTO, MSG_DECIMOSEXTO, MSG_DECIMOSEPTIMO, MSG_DECIMOOCTAVO,
      MSG_DECIMONOVENO, MSG_VIGESIMO, MSG_VIGESIMOPRIMERO, MSG_VIGESIMOSEGUNDO, MSG_VIGESIMOTERCERO, MSG_VIGESIMOCUARTO);
const
    FACTOR_FRECUENCIA: array[0..2] of Double = (0.5, 1.0, 2.0);


type TGrupoCiclo = record
        // C�digo del grupo de facturaci�n.
        Grupo: Integer;
        // Frecuencia de ciclos del grupo de facturaci�n.
        FrecuenciaGrupo: Double;
        // C�digo del ciclo de facturaci�n.
		Ciclo: Integer;
        // Indica si el grupo/ciclo est� facturado.
        YaFacturado: Boolean;
        // Indica si al menos un ciclo del grupo est� facturado.
        AlMenosUnCicloFacturado: Boolean;
    end;
    PTGrupoCiclo = ^TGrupoCiclo;


{-----------------------------------------------------------------------------
  Function Name: inicializar
  Author:    gcasais
  Date Created: 16/11/2004
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.Inicializar: Boolean;
resourcestring
    MSG_COL_TITLE_CICLO                 = 'Ciclo';
    MSG_COL_TITLE_FECHA_PROGRAMADA      = 'Fecha Programada';
    MSG_COL_TITLE_FECHA_CORTE           = 'Fecha Corte';
    MSG_COL_TITLE_FECHA_EMISION         = 'Fecha Emisi�n';
	MSG_COL_TITLE_FECHA_VENCIMIENTO     = 'Fecha Vencimiento';
var
	S: TSize;
    vFechasSoloDiaHabil: Integer;   // TASK_133_MGO_20170207
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

    // Ponemos los t�tulos que corresponden
    with dbgCiclos do begin
        Columns[0].Header.Caption := MSG_COL_TITLE_CICLO;
		Columns[1].Header.Caption := MSG_COL_TITLE_FECHA_PROGRAMADA;
        Columns[2].Header.Caption := MSG_COL_TITLE_FECHA_CORTE;
        Columns[3].Header.Caption := MSG_COL_TITLE_FECHA_EMISION;
        Columns[4].Header.Caption := MSG_COL_TITLE_FECHA_VENCIMIENTO;
    end;

	Update;
    FItemIndexAnioAnterior := -1;
	FItemIndexAnioOld := -1;

	FCargandoDataSet := False;
	btnAceptar.Enabled := False;
	btnCancelar.Enabled := False;

	ClientDataSet.CreateDataSet;
	ClientDataSet.EmptyDataSet;
	ClientDataSet.Open;

	// Cargamos el combo de a�os.
	FInsertando := False;
	cbAnios.Clear;
	QryAnios.Open;
	while not QryAnios.Eof do begin
		cbAnios.Items.Add(QryAnios.FieldByName('A�o').Value);
		QryAnios.Next;
	end;
	QryAnios.Close;

    (* Si hay a�os en el combo, cargamos el calendario m�s reciente.
    Si no hay a�os en el combo, deshabilitar los controles del NoteBook. *)
	if cbAnios.Items.Count > 0 then begin
		cbAnios.ItemIndex := cbAnios.Items.Count - 1;
        cbAniosEnter(cbAnios);
    	cbAniosChange(cbAnios);
    end else begin
        lbl_Frecuencia.Caption := EmptyStr;
        Notebook.Enabled := False;
        btnQuincenal.Enabled := Notebook.Enabled;
        btnMensual.Enabled := Notebook.Enabled;
        btnBimestral.Enabled := Notebook.Enabled;
        btn_EliminarCalendario.Enabled := Notebook.Enabled;
    end;

    ObtenerParametroGeneral(DMConnections.BaseCAC, FAC_FECHAS_SOLO_DIA_HABIL, vFechasSoloDiaHabil); // TASK_133_MGO_20170207
    FFechasSoloDiaHabil := vFechasSoloDiaHabil = 1;                                                 // TASK_133_MGO_20170207
    lbl_DebeSerDiaHabil.Visible := FFechasSoloDiaHabil;                                             // TASK_133_MGO_20170207

    Notebook.PageIndex := 1;
    
	Result := True;
end;


procedure TfrmAgendaFacturacion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	LimpiarTree;
	Action := caFree;
end;

procedure TfrmAgendaFacturacion.btnSalirClick(Sender: TObject);
begin
	Close;
end;

function TfrmAgendaFacturacion.FechaValida (Fecha: TDateTime) : Boolean;
var
	a, m, d : Word;
begin
	DecodeDate (Fecha, a, m, d);
	Result := IsValidDate(a, m , d)
end;

procedure TfrmAgendaFacturacion.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_ACTUALIZAR_CALENDARIO = 'No se pudo actualizar el Calendario.';
	CAPTION_ACTUALIZAR_CALENDARIO = 'Actualizar Calendario';
	MSG_ERROR_FECHA_INVALIDA = 'La fecha es inv�lida';
	MSG_ERROR_FECHA_EMISION_NO_HABIL = 'La fecha de Emisi�n debe ser un d�a h�bil';
	MSG_ERROR_FECHA_VENCIMIENTO_NO_HABIL = 'La fecha de Vencimiento debe ser un d�a h�bil';
    MSG_ERROR_FECHAS = 'Error de fechas';
    MSG_ERROR_FECHA_EMISION_FERIADO = 'La fecha de Emisi�n est� cargada como el d�a feriado: ';
    MSG_ERROR_FECHA_VENCIMIENTO_FERIADO = 'La fecha de Vencimiento est� cargada como el d�a feriado: ';
    MSG_ERROR_ACTUALIZAR_CLIENTDATASET = 'Error al actualizar las fechas en el ClientDataSet.';
    CAPTION_ERROR = 'Error';
    MSG_CALENDARIO_SIN_CICLOS = 'El calendario debe contener ciclos de facturaci�n.';
    CAPTION_AGENDA_FACTURACION = 'Agenda de Facturaci�n';
    MSG_ERROR_FECHA_ANIO = 'La fecha debe respetar el a�o del calendario.';
    MSG_ERROR_GRUPO_NO_EXISTE = 'No se pudo actualizar el Calendario.'
        + CRLF + 'El Grupo, C�digo: %d, ya no existe en la base de datos.';
    MSG_FECHA_EMISION_POSTERIOR_A_FECHA_CORTE = 'La fecha de Emisi�n debe ser posterior a la fecha de Corte.';
    MSG_FECHA_PROGRAMADA_POSTERIOR_A_FECHA_CORTE = 'La fecha Programada de Ejecuci�n debe ser posterior a la fecha de Corte.';
var
    g, i, Grupo: Integer;
    DescripFechaVencimiento: AnsiString;
    FechaVencimientoEsFeriado: Boolean;
    AnioCalendario: Word;
    FiltroOriginal: AnsiString;
    Registro: TBookmarkStr;
    FechaProgramada,
    FechaCorte,
    FechaEmision,
    FechaVencimiento: TDateTime;
begin
    (* Almaceno los fechas de los controles porque al sacar el filtro del
    ClientDataSet, se pierden porque se ejecuta el evento ClientDataSetChange. *)
    FechaProgramada := deFechaProgramada.Date;
    FechaCorte := deFechaCorte.Date;
    FechaEmision := deFechaEmision.Date;
    FechaVencimiento := deFechaVencimiento.Date;

    FiltroOriginal := ClientDataSet.Filter;
    Registro := ClientDataSet.Bookmark;
    ClientDataSet.Filter := EmptyStr;
    try
        (* Si el calendario NO tiene ciclos, NO permitir aceptar. *)
        if ClientDataSet.IsEmpty then begin
            MsgBox(MSG_CALENDARIO_SIN_CICLOS, CAPTION_AGENDA_FACTURACION, MB_ICONINFORMATION);
            Exit;
        end;
    finally
        ClientDataSet.Filter := FiltroOriginal;
        ClientDataSet.Bookmark := Registro;

        (* Restauro los fechas de los controles. *)
        deFechaProgramada.Date := FechaProgramada;
        deFechaCorte.Date := FechaCorte;
        deFechaEmision.Date := FechaEmision;
        deFechaVencimiento.Date := FechaVencimiento;
    end;

    (* Si el ClientDataSet est� vac�o, NO controlar las fechas ya que no hay
    ciclos en el grupo seleccionado. *)
    if not ClientDataSet.IsEmpty then begin
        AnioCalendario := FAnio;
        // Validaci�n de las fechas del nodo seleccionado.
        if not (FechaValida(deFechaProgramada.Date) and
                FechaValida(deFechaCorte.Date) and
                FechaValida(deFechaEmision.Date) and
                FechaValida(deFechaVencimiento.Date) and
                RespetaAnioCalendario(deFechaProgramada.Date, AnioCalendario) and
                RespetaAnioCalendarioMenos1(deFechaCorte.Date, AnioCalendario) and
                RespetaAnioCalendario(deFechaEmision.Date, AnioCalendario) and
                RespetaAnioCalendario(deFechaVencimiento.Date, AnioCalendario) and
                EsDiaHabil(DMConnections.BaseCAC, deFechaVencimiento.Date) and
                (deFechaProgramada.Date > deFechaCorte.Date) and
                (deFechaEmision.Date > deFechaCorte.Date))
        then begin
            Notebook.PageIndex := 0;

            (* Realizo este control para no ejecutar EsDiaFeriado si la fecha no es
            v�lida, ya que ese m�todo tirar� error. *)
            FechaVencimientoEsFeriado := False;
            if not FechaValida(deFechaVencimiento.Date) then begin
                FechaVencimientoEsFeriado := EsDiaFeriado(DMConnections.BaseCAC, deFechaVencimiento.Date, DescripFechaVencimiento);
            end;

            // Ya s� que tengo errores, ahora establezco d�nde
            if not ValidateControls([deFechaProgramada, deFechaCorte,
                deFechaEmision, deFechaVencimiento,
                deFechaProgramada,
                deFechaProgramada,
                deFechaCorte,
                deFechaEmision,
                deFechaVencimiento,
                deFechaVencimiento,
                deFechaVencimiento,
                deFechaEmision],
                [FechaValida(deFechaProgramada.Date), FechaValida(deFechaCorte.Date),
                FechaValida(deFechaEmision.Date), FechaValida(deFechaVencimiento.Date),
                RespetaAnioCalendario(deFechaProgramada.Date, AnioCalendario),
                (deFechaProgramada.Date > deFechaCorte.Date),
                RespetaAnioCalendarioMenos1(deFechaCorte.Date, AnioCalendario),
                RespetaAnioCalendario(deFechaEmision.Date, AnioCalendario),
                RespetaAnioCalendario(deFechaVencimiento.Date, AnioCalendario),
                (not EsDomingoOSabado(deFechaVencimiento.Date) or not FFechasSoloDiaHabil),   // TASK_133_MGO_20170207
                (not FechaVencimientoEsFeriado or not FFechasSoloDiaHabil),                   // TASK_133_MGO_20170207
                (deFechaEmision.Date >= deFechaCorte.Date)],
                MSG_ERROR_FECHAS,
                [MSG_ERROR_FECHA_INVALIDA, MSG_ERROR_FECHA_INVALIDA,
                MSG_ERROR_FECHA_INVALIDA, MSG_ERROR_FECHA_INVALIDA,
                MSG_ERROR_FECHA_ANIO,
                MSG_FECHA_PROGRAMADA_POSTERIOR_A_FECHA_CORTE,
                MSG_ERROR_FECHA_ANIO,
                MSG_ERROR_FECHA_ANIO,
                MSG_ERROR_FECHA_ANIO,
                MSG_ERROR_FECHA_VENCIMIENTO_NO_HABIL,
                MSG_ERROR_FECHA_VENCIMIENTO_FERIADO + DescripFechaVencimiento,
                MSG_FECHA_EMISION_POSTERIOR_A_FECHA_CORTE]) then Exit;
        end;
    end; // if not ClientDataSet.IsEmpty

    try
        if NoteBook.PageIndex = 0 then begin
            // Actualizo en el ClientDataSet las fechas con las de los controles.
            with ClientDataSet do begin
                Edit;
                FieldByName('FechaProgramadaEjecucion').AsDateTime := deFechaProgramada.Date;
                FieldByName('FechaCorte').AsDateTime := deFechaCorte.Date;
                FieldByName('FechaEmision').AsDateTime := deFechaEmision.Date;
                FieldByName('FechaVencimiento').AsDateTime := deFechaVencimiento.Date;
                Post;
            end;
        end;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_ACTUALIZAR_CLIENTDATASET, E.Message,
                CAPTION_ERROR, MB_ICONSTOP);
            Exit;
        end;
    end;

    // Validar las fechas de los dem�s ciclos.
    if FFechasSoloDiaHabil then                                                 // TASK_133_MGO_20170207
        if not VerificarDiasHabilesCiclos then Exit;

    (* Validar la diferencia entre las fechas de corte de los ciclos
    consecutivos de caga grupo. *)
    if not VerificarFechasCorteCiclos then Exit;

	Screen.Cursor := crHourGlass;
    try
        DMConnections.BaseCAC.BeginTrans;
        ClientDataSet.DisableControls;

        // Guardamos el Grupo del nodo seleccionado el �rbol.
        Grupo := TGrupoCiclo(tvGrupos.Selected.Data^).Grupo;

        // Si es un calendario nuevo insertamos todo el calendario
        try
            if FInsertando then begin
                ClientDataSet.Filtered := False;
                try
                    ClientDataSet.First;
                    while not ClientDataSet.Eof do begin
                        (* Verificar que el grupo asociado al ciclo exista. Si no existe
                        se viola un Foreign Key. *)
                        if not ExisteGrupoEnBD(DMConnections.BaseCAC, ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger) then
                            DatabaseError(Format(MSG_ERROR_GRUPO_NO_EXISTE, [ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger]))
                        else begin
                            with InsertarFechas do begin
                                Parameters.ParamByName('@Anio').Value := FAnio;
                                Parameters.ParamByName('@CodigoGrupoFacturacion').Value :=
                                  ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger;
                                Parameters.ParamByName('@Ciclo').Value:=
                                  ClientDataSet.FieldByName('CodigoCiclo').asInteger;
                                Parameters.ParamByName('@FechaProgramadaEjecucion').Value :=
                                  ClientDataSet.FieldByName('FechaProgramadaEjecucion').AsDateTime;
                                Parameters.ParamByName('@FechaCorte').Value :=
                                  ClientDataSet.FieldByName('FechaCorte').AsDateTime;
                                Parameters.ParamByName('@FechaEmision').Value :=
                                  ClientDataSet.FieldByName('FechaEmision').AsDateTime;
                                Parameters.ParamByName('@FechaVencimiento').Value :=
                                  ClientDataSet.FieldByName('FechaVencimiento').AsDateTime;
                                Parameters.ParamByName('@Operador').Value := UsuarioSistema;
                                Parameters.ParamByName('@FactorFrecuencia').Value :=
                                    ClientDataSet.FieldByName('FactorFrecuencia').AsFloat;
                                ExecProc;
                            end;
                            ClientDataSet.Next;
                        end;
                    end;
                finally
                    ClientDataSet.Filtered := True;
                end;

                FInsertando := False;
            end else begin
                // Si es un calendario modificado recorremos grupo por grupo
                g := 0;
                i := 0;
                while g < FCantidadGrupos do begin
                    if tvGrupos.Items[i].Level = 0 then begin
                        // Preparamos el clientdataset
                        ClientDataSet.Filter := Format('CodigoGrupoFacturacion = %d', [TGrupoCiclo(tvGrupos.Items[i].Data^).Grupo]);
                        ClientDataSet.First;
                        (* Si el grupo tiene al menos un ciclo facturado,
                            actualizar ciclo por ciclo.
                        Si el grupo NO tiene ning�n ciclo facturado,
                            eliminamos todos sus ciclos y cargamos los nuevos. *)
                        if TGrupoCiclo(tvGrupos.Items[i].Data^).AlMenosUnCicloFacturado then begin
                            while not ClientDataSet.Eof do begin
                                (* Verificar que el grupo asociado al ciclo exista. Si no existe
                                se viola un Foreign Key. *)
                                if not ExisteGrupoEnBD(DMConnections.BaseCAC, ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger) then
                                    DatabaseError(Format(MSG_ERROR_GRUPO_NO_EXISTE, [ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger]))
                                else begin
                                    with UpdateFechas do begin
                                        Parameters.ParamByName('@Anio').Value := FAnio;
                                        Parameters.ParamByName('@CodigoGrupoFacturacion').Value :=
                                          ClientDataSet.FieldByName('CodigoGrupoFacturacion').asInteger;
                                        Parameters.ParamByName('@Ciclo').Value:=
                                          ClientDataSet.FieldByName('CodigoCiclo').asInteger;
                                        Parameters.ParamByName('@FechaProgramadaEjecucion').Value :=
                                          ClientDataSet.FieldByName('FechaProgramadaEjecucion').AsDateTime;
                                        Parameters.ParamByName('@FechaCorte').Value :=
                                          ClientDataSet.FieldByName('FechaCorte').AsDateTime;
                                        Parameters.ParamByName('@FechaEmision').Value :=
                                          ClientDataSet.FieldByName('FechaEmision').AsDateTime;
                                        Parameters.ParamByName('@FechaVencimiento').Value :=
                                          ClientDataSet.FieldByName('FechaVencimiento').AsDateTime;
                                        Parameters.ParamByName('@Operador').Value := UsuarioSistema;
                                        Parameters.ParamByName('@FactorFrecuencia').Value :=
                                            ClientDataSet.FieldByName('FactorFrecuencia').AsFloat;
                                        ExecProc;
                                    end;
                                    ClientDataSet.Next;
                                end;
                            end;
                        end else begin
                            (* Si el grupo NO tiene al menos un ciclo facturado
                            eliminamos todos sus ciclos y cargamos los nuevos. *)
                            EliminarCiclos.Parameters.ParamByName('A�o').Value := FAnio;
                            EliminarCiclos.Parameters.ParamByName('CodigoGrupoFacturacion').Value := TGrupoCiclo(tvGrupos.Items[i].Data^).Grupo;
                            EliminarCiclos.ExecSQL;
                            while not ClientDataSet.Eof do begin
                                (* Verificar que el grupo asociado al ciclo exista. Si no existe
                                se viola un Foreign Key. *)
                                if not ExisteGrupoEnBD(DMConnections.BaseCAC, ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger) then
                                    DatabaseError(Format(MSG_ERROR_GRUPO_NO_EXISTE, [ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger]))
                                else begin
                                    with InsertarFechas do begin
                                        Parameters.ParamByName('@Anio').Value := FAnio;
                                        Parameters.ParamByName('@CodigoGrupoFacturacion').Value :=
                                          ClientDataSet.FieldByName('CodigoGrupoFacturacion').AsInteger;
                                        Parameters.ParamByName('@Ciclo').Value:=
                                          ClientDataSet.FieldByName('CodigoCiclo').AsInteger;
                                        Parameters.ParamByName('@FechaProgramadaEjecucion').Value :=
                                          ClientDataSet.FieldByName('FechaProgramadaEjecucion').AsDateTime;
                                        Parameters.ParamByName('@FechaCorte').Value :=
                                          ClientDataSet.FieldByName('FechaCorte').AsDateTime;
                                        Parameters.ParamByName('@FechaEmision').Value :=
                                          ClientDataSet.FieldByName('FechaEmision').AsDateTime;
                                        Parameters.ParamByName('@FechaVencimiento').Value :=
                                          ClientDataSet.FieldByName('FechaVencimiento').AsDateTime;
                                        Parameters.ParamByName('@Operador').Value := UsuarioSistema;
                                        Parameters.ParamByName('@FactorFrecuencia').Value :=
                                            ClientDataSet.FieldByName('FactorFrecuencia').AsFloat;
                                        ExecProc;
                                    end;
                                    ClientDataSet.Next;
                                end;
                            end;
                        end;
                        Inc(g);
                    end;
                    Inc(i);
                end;
            end;
            DMConnections.BaseCAC.CommitTrans;

            (* Deshabilitar los botones Aceptar y Cancelar. *)
            btnAceptar.Enabled := False;
            btnCancelar.Enabled := False;
            (* Habilito el combo de a�os para que pueda cambiar de a�o. *)
            cbAnios.Enabled := True;
            (* Habilito el bot�n de agregar un nuevo calendario. *)
            btn_NuevoCalendario.Enabled := True;
            (* Habilito el bot�n de eliminar un calendario. *)
            btn_EliminarCalendario.Enabled := True;

        except
            on E: EDatabaseError do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBox(E.Message, CAPTION_ACTUALIZAR_CALENDARIO, MB_ICONSTOP);
            end;
            on E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ACTUALIZAR_CALENDARIO, E.message, CAPTION_ACTUALIZAR_CALENDARIO, MB_ICONSTOP);
            end;
        end;
    finally
    	Screen.Cursor := crDefault;
    end;

    (* Mostrar los datos del grupo que estaba seleccionado al comenzar este
    proceso. *)
    ClientDataSet.Filter := Format('CodigoGrupoFacturacion = %d', [Grupo]);
	if tvGrupos.Selected.Level = 1 then begin
        with ClientDataSet do begin
            if Locate('CodigoGrupoFacturacion; CodigoCiclo', VarArrayOf([TGrupoCiclo(tvGrupos.Selected.Data^).Grupo, TGrupoCiclo(tvGrupos.Selected.Data^).Ciclo]), []) then begin
				deFechaProgramada.Enabled   := FieldByName('FechaRealFacturacion').IsNull;
                deFechaCorte.Enabled        := FieldByName('FechaRealFacturacion').IsNull;
                lbl_FechaCorteHint.Enabled  := deFechaCorte.Enabled;
                deFechaEmision.Enabled      := FieldByName('FechaRealFacturacion').IsNull;
                deFechaVencimiento.Enabled  := FieldByName('FechaRealFacturacion').IsNull;
                lbl_DebeSerDiaHabil.Enabled := deFechaVencimiento.Enabled;
                deFechaProgramada.Date      := FieldByName('FechaProgramadaEjecucion').AsDateTime;
				deFechaCorte.Date           := FieldByName('FechaCorte').AsDateTime;
				deFechaEmision.Date         := FieldByName('FechaEmision').AsDateTime;
                deFechaVencimiento.Date     := FieldByName('FechaVencimiento').AsDateTime;
            end else begin
                ClientDataSet.First;
            end;
        end;
    end;
    ClientDataSet.EnableControls;
end;

procedure TfrmAgendaFacturacion.cbAniosChange(Sender: TObject);
begin
    (* Si el bot�n Aceptar est� habilitado (es porque se han hecho cambios que
    no han sido confirmados) y el a�o seleccionado es distinto al a�o que
    estaba seleccionado, entonces preguntar al operador que acci�n desea
    realizar. *)
    if (btnAceptar.Enabled) and (cbAnios.ItemIndex <> FItemIndexAnioanterior) then begin
        if ConfirmarCambios(cbAnios) = IDCANCEL then Exit;
    end;
    (* Si el bot�n Aceptar sigue habilitado es porque el m�todo
    btnAceptarClick no se ejecut� con exito (puede haber alguna fecha de
    ciclo que no es h�bil). Luego, no se debe permitir continuar con las
    sentencias posteriores. *)
    if btnAceptar.Enabled then begin
        cbAnios.ItemIndex := FItemIndexAnioanterior;
        Exit;
    end;

	(* Si hay a�os en el combo, cargo el arbol con los grupos y los ciclos del
    a�o seleccionado en el combo.
    Si no hay a�os en el combo,
        Eliminar los registros de ClientDataSet
        Limpiar el �rbol
        Deshabilitar el NoteBook. *)
    if cbAnios.Items.Count > 0 then begin
        FAnio := StrToInt((Sender as TComboBox).Items[(Sender as TComboBox).ItemIndex]);
        CargarGruposEnArbol(FAnio);

        CargarCiclos;

        // Seleccionar el primer nodo del �rbol.
    	tvGrupos.Selected := tvGrupos.Items[0];
	    tvGruposChange(tvGrupos, tvGrupos.Selected);
    end else begin
        EliminarCiclosGrupo;
        LimpiarTree;
        cbAnios.Clear;
        lbl_Frecuencia.Caption := EmptyStr;
        Notebook.Enabled := False;
        btnQuincenal.Enabled := Notebook.Enabled;
        btnMensual.Enabled := Notebook.Enabled;
        btnBimestral.Enabled := Notebook.Enabled;
        btn_EliminarCiclosGrupo.Enabled := Notebook.Enabled;
    end;

    (* Actualizar el ItemIndex para que en la pr�xima ejecuci�n del evento
    Change se sepa cual era el ItemIndex anterior. *)
    FItemIndexAnioAnterior := cbAnios.ItemIndex;
end;

{******************************* Procedure Header ******************************
Procedure Name: tvGruposChange
Author :
Date Created :
Parameters : Sender: TObject; Node: TTreeNode
Description :

Revision : 1
    Author : pdominguez
    Date   : 09/11/2009
    Description : SS 843
        - Se deshabilit� el trozo de c�digo que estaba provocando el error de
        grabaci�n reportado en la SS. La grabaci�n de los datos de la rama del
        �rbol en la cual se est� posicionado antes de cambiar, ya se efect�a en
        el evento OnChanging. El problema surg�a cuando se ven�a de una rama del
        �rbol sin elementos a otra con elementos y cambios pendientes de grabar.
*******************************************************************************}
procedure TfrmAgendaFacturacion.tvGruposChange(Sender: TObject;
  Node: TTreeNode);
resourcestring
	MSG_QUINCENAL   = 'Quincenal';
	MSG_MENSUAL     = 'Mensual';
	MSG_BIMESTRAL   = 'Bimestral';
	MSG_FRECUENCIA_NO_ASIGNADA = 'Frecuencia no asignada';
	MSG_CICLO_FACTURADO     = 'Este ciclo ya ha sido facturado.';
	MSG_CICLO_NO_FACTURADO  = 'Este ciclo a�n NO ha sido facturado.';
    MSG_ERROR_ACTUALIZAR_DATOS = 'Error al actualizar los datos';
    MSG_CAPTION_ERROR = 'Error';
begin
    if Node = nil then Exit;

    // Mostrar los datos del grupo (Frecuencia y estado)
    if TGrupoCiclo(Node.Data^).FrecuenciaGrupo = 0.5 then
        lbl_Frecuencia.Caption := MSG_QUINCENAL
    else if TGrupoCiclo(Node.Data^).FrecuenciaGrupo = 1 then
        lbl_Frecuencia.Caption := MSG_MENSUAL
    else if TGrupoCiclo(Node.Data^).FrecuenciaGrupo = 2 then
        lbl_Frecuencia.Caption := MSG_BIMESTRAL
    else lbl_Frecuencia.Caption := MSG_FRECUENCIA_NO_ASIGNADA;

    try
        try
            (* Mostrar los ciclos del grupo seleccionado en el �rbol en una
            grilla. *)
            ClientDataSet.Filter := Format('CodigoGrupoFacturacion = %d', [TGrupoCiclo(Node.data^).Grupo]);
            (* Si el Nodo seleccionado es un Grupo, mostrar la grilla.
            Si es un Ciclo, mostrar las fechas. *)
            if Node.Level = 0 then begin
                NoteBook.PageIndex := 1;
            end else begin
                with ClientDataSet do begin
                    if Locate('CodigoGrupoFacturacion; CodigoCiclo', VarArrayOf([TGrupoCiclo(Node.Data^).Grupo, TGrupoCiclo(Node.Data^).Ciclo]), []) then begin
                        deFechaProgramada.Enabled   := FieldByName('FechaRealFacturacion').IsNull;
                        deFechaCorte.Enabled        := FieldByName('FechaRealFacturacion').IsNull;
                        lbl_FechaCorteHint.Enabled  := deFechaCorte.Enabled;
                        deFechaEmision.Enabled      := FieldByName('FechaRealFacturacion').IsNull;
                        deFechaVencimiento.Enabled  := FieldByName('FechaRealFacturacion').IsNull;
                        lbl_DebeSerDiaHabil.Enabled := deFechaVencimiento.Enabled;
                        deFechaProgramada.Date      := FieldByName('FechaProgramadaEjecucion').AsDateTime;
                        deFechaCorte.Date           := FieldByName('FechaCorte').AsDateTime;
                        deFechaEmision.Date         := FieldByName('FechaEmision').AsDateTime;
                        deFechaVencimiento.Date     := FieldByName('FechaVencimiento').AsDateTime;
                        if FieldByname('FechaRealFacturacion').IsNull then
                            lbl_Estado.Caption := MSG_CICLO_NO_FACTURADO
                        else
                            lbl_Estado.Caption := MSG_CICLO_FACTURADO;

                    end;
                end;
                NoteBook.PageIndex := 0;
            end;
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_ACTUALIZAR_DATOS, E.Message, MSG_CAPTION_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        (* Si al menos un ciclo del grupo est� facturado deshabilitar los
        botones para generar ciclos. *)
        btnQuincenal.Enabled := not TGrupoCiclo(Node.Data^).AlMenosUnCicloFacturado;
        btnMensual.Enabled := not TGrupoCiclo(Node.Data^).AlMenosUnCicloFacturado;
        btnBimestral.Enabled := not TGrupoCiclo(Node.Data^).AlMenosUnCicloFacturado;
        btn_EliminarCiclosGrupo.Enabled := not TGrupoCiclo(Node.Data^).AlMenosUnCicloFacturado;
    end
end;


procedure TfrmAgendaFacturacion.LimpiarTree;
begin
	tvGrupos.Selected := nil;
	while tvGrupos.Items.Count > 0 do begin
		Dispose(tvGrupos.Items[0].Data);
		tvGrupos.Items.Delete(tvGrupos.Items[0]);
	end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.InicializarCiclos
  Author:
  Date:
  Arguments:
    - Node: TTreeNode. Nodo en el �rbol del grupo al que se le generan los
    ciclos.
    - FactorFrecuencia: Double. Frecuencia para la generaci�n de ciclos.
    - MesDesdeDondeGenerar: Word. Mes desde el cual generar ciclos.
  Result:    None
  Description: Genera los ciclos para un grupo.
-----------------------------------------------------------------------------}
procedure TfrmAgendaFacturacion.InicializarCiclos(Node: TTreeNode; FactorFrecuencia: Double; MesDesdeDondeGenerar: Word);
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_OBTENER_PARAMETRO = 'Error al obtener el par�metro ';
    MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS = 'Las fechas de Corte de los Ciclos'
        + ' no respetan la cantidad de d�as de diferencia.';
    MSG_DEBE_CAMBIAR_VALOR_PARAMETRO = 'Debe cambiar el valor del par�metro';
    DOS_PUNTOS = ': ';
var
	i, DiasDiferenciaFechasCorte, DiasVencimiento, DiasEmision: Integer;
	FechaEjecucion, FechaCorte, FechaEmision, FechaVencimiento,
    FechaCorteProxima: TDateTime;
	GrupoCiclo: PTGrupoCiclo;
	NodeHijo: TTreeNode;
    CantidadCiclosGenerar, Ciclo: ShortInt;
    Parametro: AnsiString;
begin
	FCargandoDataSet := True;
    Screen.Cursor := crHourGlass;                                               // TASK_133_MGO_20170207
    try
        CantidadCiclosGenerar := Trunc(((12 - MesDesdeDondeGenerar) + 1) / FactorFrecuencia);

        (* Incializo la Fechade Corte con el primer d�a del mes desde el cual se
        van a generar los ciclos. *)
        FechaCorte := EncodeDate(FAnio, MesDesdeDondeGenerar, 1);

        // Una pasada por el for por cada ciclo a generar.
        for i := 1 to CantidadCiclosGenerar do begin
            (* Si adem�s del ciclo que se est� por generar a�n quedan ciclos por
            generar, obtener la pr�xima fecha de Corte para controlar que las fechas
            de corte de ciclos consecutivos respeten la diferencia especificada en
            la tabla ParametrosGenerales. *)
            FechaCorteProxima := FechaCorte;
            if (i < CantidadCiclosGenerar) then begin
                // Incrementar la Fecha de Corte para obtener la del pr�ximo ciclo.
                FechaCorteProxima := IncrementarFecha(FechaCorte, FactorFrecuencia);
                (* Si la diferencia entre la Fecha de Corte Pr�xima y la anterior
                es inferior a la cantidad de d�as que indica el par�metro general:
                DIAS_DIFERENCIA_FECHAS_CORTE_X (donde X puede ser QUINCENAL, MENSUAL O
                BIMESTRAL) seg�n lo indique FactorFrecuencia, mostrar un mensaje que
                informe de la situaci�n y abortar el proceso de generaci�n de ciclos. *)
                Parametro := EmptyStr;
                if FactorFrecuencia = 0.5 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_QUINCENAL
                //else begin                                                                         // TASK_133_MGO_20170207
                else if FactorFrecuencia = 0.25 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_SEMANAL  // TASK_133_MGO_20170207
                else if FactorFrecuencia = 1.0 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_MENSUAL   // TASK_133_MGO_20170207
                else if FactorFrecuencia = 2.0 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_BIMESTRAL;
                //end;                                                                               // TASK_133_MGO_20170207

                if not ObtenerParametroGeneral(DMConnections.BaseCAC,
                        Parametro, DiasDiferenciaFechasCorte) then begin
                    MsgBoxErr(MSG_ERROR, MSG_ERROR_OBTENER_PARAMETRO +
                        Parametro, MSG_ERROR, MB_ICONERROR);
                    Break;
                end;

                if ((FechaCorteProxima - FechaCorte) < DiasDiferenciaFechasCorte) then begin
                    MsgBox(MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS + CRLF +
                        MSG_DEBE_CAMBIAR_VALOR_PARAMETRO + DOS_PUNTOS +
                        Parametro, Caption, MB_ICONERROR);
                    Break;
                end;

            end;

            (* Incremento la FechaCorte para que FechaEjecucion sea posterior a
            FechaCorte. *)
            FechaEjecucion := IncDay(FechaCorte, 1);

            if not ObtenerParametroGeneral(DMConnections.BaseCAC,
                    'DIAS_EMISION_COMPROBANTE', DiasEmision) then begin
                MsgBoxErr(MSG_ERROR, MSG_ERROR_OBTENER_PARAMETRO +
                    DIAS_EMISION_COMPROBANTE, MSG_ERROR, MB_ICONERROR);
                Break;
            end;

            (* Incremento FechaCorte en 1 (uno) para que FechaEmisi�n sea mayor a
            FechaCorte. *)
            FechaEmision := IncDay(IncDay(FechaCorte, 1), DiasEmision);

            if not ObtenerParametroGeneral(DMConnections.BaseCAC,
                    'DIAS_VENCIMIENTO_COMPROBANTE', DiasVencimiento) then begin
                MsgBoxErr(MSG_ERROR, MSG_ERROR_OBTENER_PARAMETRO +
                    DIAS_VENCIMIENTO_COMPROBANTE, MSG_ERROR, MB_ICONERROR);
                Break;
            end;
            FechaVencimiento := ObtenerProximoDiaHabil(IncDay(FechaEjecucion, DiasVencimiento));

            (* Obtener el n�mero de ciclo de acuerdo al mes desde donde se deben
            generar los ciclos y a la frecuencia de generaci�n. *)
            Ciclo := i + Trunc((MesDesdeDondeGenerar - 1) / FactorFrecuencia);

            with ClientDataSet do begin
                Append;
                FieldByName('CodigoGrupoFacturacion').Value := TGrupoCiclo(Node.Data^).Grupo;
                FieldByName('CodigoCiclo').Value := Ciclo;
                if CantidadCiclosGenerar > 24 then                                  // TASK_133_MGO_20170207
                    FieldByName('Ciclo').Value := '#' + IntToStr(Ciclo)             // TASK_133_MGO_20170207
                else                                                                // TASK_133_MGO_20170207
                    FieldByName('Ciclo').Value := NOMBRE_CICLOS[Ciclo];             // TASK_133_MGO_20170207
                FieldByName('FechaProgramadaEjecucion').Value := FechaEjecucion;
                FieldByName('FechaCorte').Value := FechaCorte;
                FieldByName('FechaEmision').Value := FechaEmision;
                FieldByName('FechaVencimiento').Value := FechaVencimiento;
                FieldByName('FactorFrecuencia').Value := FactorFrecuencia;
                Post;
            end;

            // Agregar en el �rbol el nodo para el ciclo.
            New(GrupoCiclo);
            GrupoCiclo^.Grupo := TGrupoCiclo(Node.Data^).Grupo;
            GrupoCiclo^.Ciclo := Ciclo;
            GrupoCiclo^.FrecuenciaGrupo := TGrupoCiclo(Node.Data^).FrecuenciaGrupo;
            GrupoCiclo^.YaFacturado := False;
            GrupoCiclo^.AlMenosUnCicloFacturado := False;
            if CantidadCiclosGenerar > 24 then                                                      // TASK_133_MGO_20170207
                NodeHijo := tvGrupos.Items.AddChildObject(Node, '#' + IntToStr(Ciclo), GrupoCiclo)  // TASK_133_MGO_20170207
            else                                                                                    // TASK_133_MGO_20170207
                NodeHijo := tvGrupos.Items.AddChildObject(Node, NOMBRE_CICLOS[Ciclo], GrupoCiclo);  // TASK_133_MGO_20170207
            NodeHijo.ImageIndex := iif(GrupoCiclo^.YaFacturado, 3, 1);
            NodeHijo.SelectedIndex := NodeHijo.ImageIndex;

            FechaCorte := FechaCorteProxima;
        end; // for
    finally                                                                     // TASK_133_MGO_20170207
	    FCargandoDataSet := False;
        Screen.Cursor := crDefault;                                             // TASK_133_MGO_20170207
    end;                                                                        // TASK_133_MGO_20170207
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.CargarCiclos
  Author:
  Date:
  Arguments: None
  Result:    None
  Description: Carga en el ClientDataSet los ciclos de un a�o.
-----------------------------------------------------------------------------}
procedure TfrmAgendaFacturacion.CargarCiclos;
resourcestring
    MSG_ERROR_BASE_DE_DATOS = 'Error de Base de Datos';
    MSG_ERROR = 'Error';
begin
	ClientDataSet.EmptyDataSet;
	ClientDataSet.Open;
	FCargandoDataSet := True;
    with spObtenerAgendaFacturacion do begin
        Parameters.ParamByName('@Anio').Value := FAnio;
        try
            OpenTables([spObtenerAgendaFacturacion]);
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_BASE_DE_DATOS, E.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
        while not Eof do begin
            ClientDataSet.Append;
            ClientDataSet.FieldByName('CodigoGrupoFacturacion').Value := FieldByName('CodigoGrupoFacturacion').Value;
            ClientDataSet.FieldByName('CodigoCiclo').Value := FieldByName('Ciclo').Value;
            if FieldByName('Ciclo').AsInteger > 24 then                                                     // TASK_133_MGO_20170207
                ClientDataSet.FieldByName('Ciclo').Value := '#' + FieldByName('Ciclo').AsString             // TASK_133_MGO_20170207
            else                                                                                            // TASK_133_MGO_20170207
                ClientDataSet.FieldByName('Ciclo').Value := NOMBRE_CICLOS[FieldByName('Ciclo').AsInteger];  // TASK_133_MGO_20170207
            ClientDataSet.FieldByName('FechaProgramadaEjecucion').Value := FieldByName('FechaProgramadaEjecucion').Value;
            ClientDataSet.FieldByName('FechaCorte').Value := FieldByName('FechaCorte').Value;
            ClientDataSet.FieldByName('FechaEmision').Value := FieldByName('FechaEmision').Value;
            ClientDataSet.FieldByName('FechaVencimiento').Value := FieldByName('FechaVencimiento').Value;
            ClientDataSet.FieldByName('FechaRealFacturacion').Value := FieldByName('FechaRealFacturacion').Value;
            ClientDataSet.FieldByName('FactorFrecuencia').Value := FieldByName('FactorFrecuencia').Value;
            ClientDataSet.Post;

            Next;
        end;
        Close;
    end;
    ClientDataSet.First;
	FCargandoDataSet := False;
end;

procedure TfrmAgendaFacturacion.dsClientDataSetDataChange(Sender: TObject;
  Field: TField);
begin
    with (Sender as TDataSource).DataSet do begin
        if (Field = nil) and (State = dsBrowse) then begin
            deFechaProgramada.Enabled   := FieldByName('FechaRealFacturacion').IsNull;
            deFechaCorte.Enabled        := FieldByName('FechaRealFacturacion').IsNull;
            lbl_FechaCorteHint.Enabled  := deFechaCorte.Enabled;
            deFechaEmision.Enabled      := FieldByName('FechaRealFacturacion').IsNull;
            deFechaVencimiento.Enabled  := FieldByName('FechaRealFacturacion').IsNull;
            lbl_DebeSerDiaHabil.Enabled := deFechaVencimiento.Enabled;

            if not IsEmpty then begin
                deFechaProgramada.Date  := FieldByName('FechaProgramadaEjecucion').AsDateTime;
                deFechaCorte.Date       := FieldByName('FechaCorte').AsDateTime;
                deFechaEmision.Date     := FieldByName('FechaEmision').AsDateTime;
                deFechaVencimiento.Date := FieldByName('FechaVencimiento').AsDateTime;
            end;
        end;
    end;
end;        

// INICIO : TASK_133_MGO_20170207
procedure TfrmAgendaFacturacion.btnSemanalClick(Sender: TObject);
begin
    RealizarGeneracionCiclos(0.25);
end;
// FIN : TASK_133_MGO_20170207

procedure TfrmAgendaFacturacion.pmiQuincenalClick(Sender: TObject);
begin
	RealizarGeneracionCiclos(0.5);
end;

procedure TfrmAgendaFacturacion.pmiDefaultClick(Sender: TObject);
begin
	RealizarGeneracionCiclos(TGrupoCiclo(tvGrupos.Selected.Data^).FrecuenciaGrupo);
end;

procedure TfrmAgendaFacturacion.pmiMensualClick(Sender: TObject);
begin
	RealizarGeneracionCiclos(1);
end;

procedure TfrmAgendaFacturacion.pmiBimestralClick(Sender: TObject);
begin
	RealizarGeneracionCiclos(2);
end;

{ INICIO : TASK_133_MGO_20170207
procedure TfrmAgendaFacturacion.PopupMenuPopup(Sender: TObject);
begin
    (* Si hay un nodo seleccionado en el �rbol y ese nodo es de Grupo y ese
    nodo NO tiene ning�n ciclo facturado, habilitar la opci�n. *)
    pmiQuincenal.Enabled := (tvGrupos.Selected <> nil)
                            and (not TGrupoCiclo(tvGrupos.Selected.data^).AlMenosUnCicloFacturado)
                            and (tvGrupos.Selected.Parent = nil);
    pmiMensual.Enabled := pmiQuincenal.Enabled;
    pmiBimestral.Enabled := pmiQuincenal.Enabled;
    pmiDefault.Enabled := pmiQuincenal.Enabled;
    pmiEliminarCiclosGrupo.Enabled := pmiQuincenal.Enabled;
end;
} // FIN : TASK_133_MGO_20170207

procedure TfrmAgendaFacturacion.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    (* Si el bot�n Aceptar est� habilitado (es porque se han hecho cambios que
    no han sido confirmados), entonces preguntar al operador que acci�n desea
    realizar. *)
	if (btnAceptar.Enabled) then begin
        ConfirmarCambios(Sender);
	end;

	CanClose := not btnAceptar.Enabled;
end;

procedure TfrmAgendaFacturacion.btnCancelarClick(Sender: TObject);
begin
    (* Si estoy insertando un nuevo calendario, eliminar del combo de a�os el
    a�o que se intentaba agregar. *)
	if FInsertando then begin
		cbAnios.Items.Delete(cbAnios.ItemIndex);
		cbAnios.ItemIndex := cbAnios.Items.Count - 1;
		FInsertando := False;
	end;
	btnAceptar.Enabled := False;
	btnCancelar.Enabled := False;

    (* Habilito el combo de a�os para que puede cambiar de a�o. *)
    cbAnios.Enabled := True;

    // Actualizar la informaci�n que se muestra en el form.
	cbAniosChange(cbAnios);

    (* Habilito el bot�n de agregar un nuevo calendario. *)
    btn_NuevoCalendario.Enabled := True;
    (* Habilito el bot�n de eliminar un calendario. *)
    btn_EliminarCalendario.Enabled := True;

    (* Si no hay a�os en el combo, deshabilitar el NoteBook. *)
    if cbAnios.Items.Count = 0 then begin
        lbl_Frecuencia.Caption := EmptyStr;
        NoteBook.Enabled := False;
        btnQuincenal.Enabled := Notebook.Enabled;
        btnMensual.Enabled := Notebook.Enabled;
        btnBimestral.Enabled := Notebook.Enabled;
        btn_EliminarCiclosGrupo.Enabled := Notebook.Enabled;
    end;
end;

procedure TfrmAgendaFacturacion.EliminarCiclosGrupo;
var i: Integer;
begin
    ClientDataSet.First;
    while not ClientDataSet.Eof do
        ClientDataSet.Delete;
    for i := 0 to tvGrupos.Selected.Count - 1 do
        Dispose(tvGrupos.Selected.Item[i].Data);
    tvGrupos.Selected.DeleteChildren;
end;

procedure TfrmAgendaFacturacion.ClientDataSetAfterPost(DataSet: TDataSet);
begin
    if not FCargandoDataSet then begin
        btnAceptar.Enabled := True;
        btnCancelar.Enabled := True;
        (* Deshabilitar el bot�n de Eliminar Calendario. *)
        btn_EliminarCalendario.Enabled := False;
    end;
end;

procedure TfrmAgendaFacturacion.deFechaProgramadaChange(Sender: TObject);
begin
    if ActiveControl = Sender then begin
        btnAceptar.Enabled := True;
        btnCancelar.Enabled := True;
        (* Deshabilitar el bot�n de Eliminar Calendario. *)
        btn_EliminarCalendario.Enabled := False;
    end;
end;

procedure TfrmAgendaFacturacion.cbAniosEnter(Sender: TObject);
begin
	FItemIndexAnioOld := cbAnios.ItemIndex;
end;

{-----------------------------------------------------------------------------
  Procedure: ObtenerProximoDiaHabil
  Author:    ggomez
  Date:      26-Ene-2005
  Arguments: Fecha: TDateTime
  Result:    TDateTime
  Description: Dada una fecha, si �sta es un d�a habil la retorna
    sino retorna el pr�ximo d�a habil siguiente a dicha fecha.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.ObtenerProximoDiaHabil(Fecha: TDateTime): TDateTime;
begin
    Result := Fecha;
    while not EsDiaHabil(DMConnections.BaseCAC, Result) do begin
        Result := IncDay(Result, 1);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.IncrementarFecha
  Author:    ggomez
  Date:      27-Ene-2005
  Arguments: Fecha: TDateTime; IncrementoMes: Double: Cantidad de meses a
    incrementar.
  Result:    TDateTime
  Description: Dada una Fecha y un Incremento de Meses, retorna la fecha
    resultante de sumar tantos meses como Incremento indica a Fecha.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.IncrementarFecha(Fecha: TDateTime;
  IncrementoMes: Double): TDateTime;
begin
    Result := IncMonth(Fecha, Trunc(Int(IncrementoMes)));
    Result := IncDay(Result, Trunc(DaysInMonth(Result) * Frac(IncrementoMes)));
end;

procedure TfrmAgendaFacturacion.tvGruposChanging(Sender: TObject;
  Node: TTreeNode; var AllowChange: Boolean);
resourcestring
    MSG_ERROR_FECHAS = 'Error de fechas';
	MSG_ERROR_FECHA_INVALIDA = 'La fecha es inv�lida';
	MSG_ERROR_FECHA_EMISION_NO_HABIL = 'La fecha de Emisi�n debe ser un d�a h�bil';
	MSG_ERROR_FECHA_VENCIMIENTO_NO_HABIL = 'La fecha de Vencimiento debe ser un d�a h�bil';
    MSG_ERROR_FECHA_EMISION_FERIADO = 'La fecha de Emisi�n est� cargada como el d�a feriado: ';
    MSG_ERROR_FECHA_VENCIMIENTO_FERIADO = 'La fecha de Vencimiento est� cargada como el d�a feriado: ';
    MSG_ERROR_FECHA_ANIO = 'La fecha debe respetar el a�o del calendario.';
    MSG_FECHA_EMISION_POSTERIOR_A_FECHA_CORTE = 'La fecha de Emisi�n debe ser posterior a la fecha de Corte.';
    MSG_FECHA_PROGRAMADA_POSTERIOR_A_FECHA_CORTE = 'La fecha Programada de Ejecuci�n debe ser posterior a la fecha de Corte.';
var
    DescripFechaVencimiento: AnsiString;
    FechaVencimientoEsFeriado: Boolean;
    AnioCalendario: Word;
begin
    if Node = nil then Exit;

    // tvGrupos.Selected es nil cuando se abre el form por primera vez.
    if tvGrupos.Selected = nil then Exit;

    AnioCalendario := StrToInt(cbAnios.Text);
    (* Si est� seleccionado el nodo de un Ciclo y los componentes para las
    fechas est�n habilitados y los controles de las fechas NO tienen fechas
    correctas, NO permitir cambiar de nodo en el �rbol. *)
    if (tvGrupos.Selected.Level = 1) and
        deFechaVencimiento.Enabled and
        not (FechaValida(deFechaProgramada.Date) and
            FechaValida(deFechaCorte.Date) and
            FechaValida(deFechaEmision.Date) and
            FechaValida(deFechaVencimiento.Date) and
            RespetaAnioCalendario(deFechaProgramada.Date, AnioCalendario) and
            RespetaAnioCalendarioMenos1(deFechaCorte.Date, AnioCalendario) and
            RespetaAnioCalendario(deFechaEmision.Date, AnioCalendario) and
            RespetaAnioCalendario(deFechaVencimiento.Date, AnioCalendario) and
            EsDiaHabil(DMConnections.BaseCAC, deFechaVencimiento.Date) and
            (deFechaProgramada.Date > deFechaCorte.Date) and
            (deFechaEmision.Date >= deFechaCorte.Date))
    then begin
        Notebook.PageIndex := 0;

        (* Realizo este control para no ejecutar EsDiaFeriado si la fecha no es
        v�lida, ya que ese m�todo tirar� error. *)
        if not FechaValida(deFechaEmision.Date) then begin
            AllowChange := False;
            Exit;
        end;
        (* Realizo este control para no ejecutar EsDiaFeriado si la fecha no es
        v�lida, ya que ese m�todo tirar� error. *)
        if not FechaValida(deFechaVencimiento.Date) then begin
            AllowChange := False;
            Exit;
        end;

        // Ya s� que tengo errores, ahora establezco d�nde
        FechaVencimientoEsFeriado := EsDiaFeriado(DMConnections.BaseCAC,
                                        deFechaVencimiento.Date, DescripFechaVencimiento);

        if not ValidateControls([deFechaProgramada, deFechaCorte,
            deFechaEmision, deFechaVencimiento,
            deFechaProgramada,
            deFechaProgramada,
            deFechaCorte,
            deFechaEmision,
            deFechaVencimiento,
            deFechaVencimiento,
            deFechaVencimiento,
            deFechaEmision],
            [FechaValida(deFechaProgramada.Date), FechaValida(deFechaCorte.Date),
            FechaValida(deFechaEmision.Date), FechaValida(deFechaVencimiento.Date),
            RespetaAnioCalendario(deFechaProgramada.Date, AnioCalendario),
            (deFechaProgramada.Date > deFechaCorte.Date),
            RespetaAnioCalendarioMenos1(deFechaCorte.Date, AnioCalendario),
            RespetaAnioCalendario(deFechaEmision.Date, AnioCalendario),
            RespetaAnioCalendario(deFechaVencimiento.Date, AnioCalendario),
            (not EsDomingoOSabado(deFechaVencimiento.Date) or not FFechasSoloDiaHabil), // TASK_133_MGO_20170207
            (not FechaVencimientoEsFeriado or not FFechasSoloDiaHabil),                 // TASK_133_MGO_20170207    
            (deFechaEmision.Date > deFechaCorte.Date)],
            MSG_ERROR_FECHAS,
            [MSG_ERROR_FECHA_INVALIDA, MSG_ERROR_FECHA_INVALIDA,
            MSG_ERROR_FECHA_INVALIDA, MSG_ERROR_FECHA_INVALIDA,
            MSG_ERROR_FECHA_ANIO,
            MSG_FECHA_PROGRAMADA_POSTERIOR_A_FECHA_CORTE,
            MSG_ERROR_FECHA_ANIO,
            MSG_ERROR_FECHA_ANIO,
            MSG_ERROR_FECHA_ANIO,
            MSG_ERROR_FECHA_VENCIMIENTO_NO_HABIL,
            MSG_ERROR_FECHA_VENCIMIENTO_FERIADO + DescripFechaVencimiento,
            MSG_FECHA_EMISION_POSTERIOR_A_FECHA_CORTE]) then begin

            (* Setear AlllowChange en False para evitar que cambie el nodo
            seleccionado. *)
            AllowChange := False;
        end;
    end;

    (* Si el nodo seleccionado es un ciclo y hay cambios por confirmar,
    actualizar el cambio en el ClientDataSet. *)
    if (tvGrupos.Selected.Level = 1) and (btnAceptar.Enabled) then begin
        with ClientDataSet do begin
            (* Este edit se hace sobre el registro correcto ya que antes
            se hizo un locate sobre �ste. *)
            Edit;
            FieldByName('FechaProgramadaEjecucion').AsDateTime := deFechaProgramada.Date;
            FieldByName('FechaCorte').AsDateTime := deFechaCorte.Date;
            FieldByName('FechaEmision').AsDateTime := deFechaEmision.Date;
            FieldByName('FechaVencimiento').AsDateTime := deFechaVencimiento.Date;
            Post;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.SeleccionarMes
  Author:    ggomez
  Date:      27-Ene-2005
  Arguments: None
  Result:    Integer
  Description: Muestra un form para que el operador seleccione un mes, este mes
    es se utiliza para comenzar a generar ciclos de facturaci�n a partir desde
    �l.
    Retorna el mes seleccionado por el operador. Si no se seleccion� ninguno
    retorna -1.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.SeleccionarMes: Integer;
var
    FSeleccionarMes: TFormSeleccionarMes;
begin
    (* Solicitar al operador que ingrese el mes a partir del cual se deben
    generar los ciclos. *)
    Result := -1;
    FSeleccionarMes := TFormSeleccionarMes.Create(Self);
    try
        if FSeleccionarMes.Inicializar(1) then
            begin
                FSeleccionarMes.ShowModal;
                Result := FSeleccionarMes.ObtenerMesSeleccionado;
            end;
    finally
        FSeleccionarMes.Release;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: ObtenerIndiceNodo
  Author:    ggomez
  Date:      27-Ene-2005
  Arguments: Grupo, Ciclo: Integer
  Result:    Integer
  Description: Dados un C�digo de Grupo y uno de Ciclo, retorna el �ndice
    del nodo del �rbol tvGrupos, correspondiente a ellos.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.ObtenerIndiceNodo(Grupo, Ciclo: Integer): Integer;
var
    i, j: Integer;
    EncontreNodo: Boolean;
begin
    Result := -1;
    // Buscar el nodo de grupo que tiene como Codigo de Grupo a Grupo.
    i := 0;
    EncontreNodo := False;
    while (not EncontreNodo) and (i < tvGrupos.Items.Count) do begin
        (* Si el nodo es de nivel 0 (cero) verificar si es el del grupo
        buscado. *)
        if tvGrupos.Items[i].Level = 0 then begin
            if TGrupoCiclo(tvGrupos.Items[i].Data^).Grupo = Grupo then begin
                EncontreNodo := True;
                Break;
            end;
        end;
        Inc(i);
    end;

    (* Dentro de los nodos del Grupo buscar el nodo que tiene como C�digo
    de ciclo a Ciclo. *)
    if EncontreNodo then begin
        j := 0;
        EncontreNodo := False;
        while (not EncontreNodo) and (j < tvGrupos.Items[i].Count) do begin
            (* Si el nodo es de nivel 1 (uno) verificar si es el del ciclo
            buscado. *)
            if tvGrupos.Items[i].Item[j].Level = 1 then begin
                if TGrupoCiclo(tvGrupos.Items[i].Item[j].Data^).Ciclo = Ciclo then begin
                    EncontreNodo := True;
                    Break;
                end;
            end;
            Inc(j);
        end;
        if EncontreNodo then Result := tvGrupos.Items[i].Item[j].AbsoluteIndex;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.VerificarDiasHabilesCiclos
  Author:    ggomez
  Date:      27-Ene-2005
  Arguments: None
  Result:    Boolean
  Description: Recorre los ciclos de cada grupo de un calendario verificando
    que las fechas sean d�as h�biles y que el a�o de las fechas respete el a�o
    del calendario al que pertenecen. S�lo verifica las fechas de los ciclos NO
    facturados.
    Si alg�n d�a NO es h�bil, la verificaci�n se detiene retornando False.
    Si todas las fechas son d�as h�biles, retorna True.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.VerificarDiasHabilesCiclos: Boolean;
resourcestring
	MSG_ERROR_FECHA_EMISION_NO_HABIL = 'La fecha de Emisi�n debe ser un d�a h�bil';
	MSG_ERROR_FECHA_VENCIMIENTO_NO_HABIL = 'La fecha de Vencimiento debe ser un d�a h�bil';
    MSG_ERROR_FECHAS = 'Error de Fechas';
    MSG_ERROR_FECHA_EMISION_FERIADO = 'La fecha de Emisi�n est� cargada como el d�a feriado: ';
    MSG_ERROR_FECHA_VENCIMIENTO_FERIADO = 'La fecha de Vencimiento est� cargada como el d�a feriado: ';
var
    // Filtro que tiene el ClientDataSet al comenzar la verificaci�n.
    FiltroOriginal: AnsiString;
    CodigoGrupo,
    CodigoCiclo: Integer;
    IndiceNodo: Integer;
    // Fechas para saber que fecha es la NO h�bil.
    FechaVencimientoEsHabil,
    FechaVencimientoEsFeriado: Boolean;
    DescripFechaVencimiento: AnsiString;
begin
    CodigoGrupo := -1;
    CodigoCiclo := -1;
    Result := True;
    with ClientDataSet do begin
        (* Guardo el filtro que tiene el ClientDataSet. *)
        FiltroOriginal := Filter;
        Filter := EmptyStr;
        try
            FechaVencimientoEsHabil := True;
            FechaVencimientoEsFeriado := True;

            First;
            (* Mientras haya ciclos por testear y las fechas sean h�biles,
            seguir testeando. *)
            while (Result) and (not Eof) do begin
                (* Si la fecha de facturaci�n es null, es porque el ciclo NO
                est� facturado. *)
                if FieldByName('FechaRealFacturacion').IsNull then begin
                    FechaVencimientoEsHabil   := not EsDomingoOSabado(FieldByName('FechaVencimiento').AsDateTime);
                    FechaVencimientoEsFeriado := EsDiaFeriado(DMConnections.BaseCAC, FieldByName('FechaVencimiento').AsDateTime, DescripFechaVencimiento);

                    Result := FechaVencimientoEsHabil
                                and (not FechaVencimientoEsFeriado);
                end;

                if Result then Next;
            end;

            (* Si Result tiene False es porque una fecha es NO h�bil. *)
            if not Result then begin
                (* Seleccionar en el �rbol el nodo del ciclo que tiene el
                error. *)
                CodigoGrupo := FieldByName('CodigoGrupoFacturacion').AsInteger;
                CodigoCiclo := FieldByName('CodigoCiclo').AsInteger;
                IndiceNodo := ObtenerIndiceNodo(CodigoGrupo, CodigoCiclo);
                tvGrupos.Selected := nil;
                tvGrupos.Select(tvGrupos.Items[IndiceNodo]);
                (* Llamar a ValidateControls para mostrar el mensaje con el
                error. *)
                ValidateControls([deFechaEmision, deFechaVencimiento,
                    deFechaEmision, deFechaVencimiento],
                    [FechaVencimientoEsHabil or not FFechasSoloDiaHabil,            // TASK_133_MGO_20170207
                    (not FechaVencimientoEsFeriado) or not FFechasSoloDiaHabil],    // TASK_133_MGO_20170207
                    MSG_ERROR_FECHAS,
                    [MSG_ERROR_FECHA_VENCIMIENTO_NO_HABIL,
                    MSG_ERROR_FECHA_VENCIMIENTO_FERIADO + DescripFechaVencimiento]);

            end;
        finally
            (* Si NO encontr� una fecha no h�bil, restaurar el filtro que ten�a
            el ClientDataSet al iniciar el proceso.
            Si encontr� una fecha no h�bil, filtrar por el grupo del ciclo que
            tiene el error y seleccionar el registro que tiene el error. *)
            if Result then Filter := FiltroOriginal
            else begin
                Filter := 'CodigoGrupoFacturacion = ' + FieldByName('CodigoGrupoFacturacion').AsString;
                Locate('CodigoGrupoFacturacion; CodigoCiclo',
                        VarArrayOf([CodigoGrupo, CodigoCiclo]), []);
            end;
        end;
    end;
end;

procedure TfrmAgendaFacturacion.btn_NuevoCalendarioClick(Sender: TObject);
resourcestring
    MSG_ANIO_EXISTENTE = 'Ya existe un calendario de facturaci�n con el a�o ingresado.';
    CAPTION_TITULO = 'Nuevo Calendario';
var
	IndiceNuevoAnio: Integer;
    AnioIngresado: AnsiString;
begin
    (* Si el bot�n Aceptar est� habilitado (es porque se han hecho cambios que
    no han sido confirmados), entonces preguntar al operador que acci�n desea
    realizar. *)
	if (btnAceptar.Enabled) then begin
        if ConfirmarCambios(btn_NuevoCalendario) = IDCANCEL then Exit;
	end;

    (* Si el bot�n Aceptar sigue habilitado es porque el m�todo
    ConfirmarCambios no se ejecut� totalmente (puede haber alguna fecha de
    ciclo que no es h�bil). Luego, no se debe permitir continuar con las
    sentencias posteriores. *)
    if btnAceptar.Enabled then Exit;

    (* Solicitar al operador que ingrese el a�o para el nuevo calendario.
    Si no ingres� un a�o, NO continuar. *)
    AnioIngresado := IngresarAnio;
    if AnioIngresado = EmptyStr then Exit;
    (* Si el a�o ingresado ya existe entre los calendarios, NO continuar. *)
    if cbAnios.Items.IndexOf(AnioIngresado) <> -1 then begin
        MsgBox(MSG_ANIO_EXISTENTE, CAPTION_TITULO, MB_ICONINFORMATION);
        Exit;
    end;

    FAnio := StrToInt(AnioIngresado);

    (* Agregar el a�o ingresado por el operador al combo de manera ordenada. *)
    IndiceNuevoAnio := cbAnios.Items.Add(IntToStr(FAnio));
    cbAnios.ItemIndex := IndiceNuevoAnio;

	// Cargo el �rbol con los grupos.
    CargarGruposEnArbol(FAnio);

    (* Elimino los registros del ClientDataSet, porque un nuevo a�o no tiene
    ciclos. *)
    if not ClientDataSet.Active then ClientDataSet.Active := True;
    ClientDataSet.EmptyDataSet;

	tvGrupos.Selected := tvGrupos.Items[0];
	tvGruposChange(tvGrupos, tvGrupos.Selected);

    FInsertando := True;

    (* Habilitar el NoteBook. *)
    NoteBook.Enabled := True;
    (* Habilitar los botones de Generaci�n de Ciclos, si hay grupos en el
    �rbol.*)
    btnQuincenal.Enabled := (Notebook.Enabled) and (tvGrupos.Items.Count > 0);
    btnMensual.Enabled := btnQuincenal.Enabled;
    btnBimestral.Enabled := btnQuincenal.Enabled;
    btn_EliminarCiclosGrupo.Enabled := btnQuincenal.Enabled;

    btnAceptar.Enabled := True;
    btnCancelar.Enabled := True;
    (* Deshabilito el combo de A�os para que no pueda cambiar de a�o y de
    esta manera evitar un error que ocurre en al cambiar de a�o cuando se
    est� insertando un nuevo calendario. *)
    cbAnios.Enabled := False;

    (* Deshabilito el bot�n de agregar un nuevo calendario. *)
    btn_NuevoCalendario.Enabled := False;
    (* Deshabilito el bot�n de eliminar un calendario. *)
    btn_EliminarCalendario.Enabled := False;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.CargarGruposEnArbol
  Author:    ggomez
  Date:      01-Feb-2005
  Arguments: Anio: Integer
  Result:    None
  Description: Dado un a�o, carga en el �rbol los grupos correspondientes a �l.
    Adem�s, actualiza la variable global FCantidadGrupos.
-----------------------------------------------------------------------------}
procedure TfrmAgendaFacturacion.CargarGruposEnArbol(Anio: Integer);
var
    GrupoCiclo: PTGrupoCiclo;
	Node, NodeRaiz: TTreeNode;
    Grupo: Integer;
    // Indica si todos los ciclos de un grupo est�n facturados.
    CiclosGrupoEstanFacturados,
    // Indica si al menos hay un ciclo de un grupo que est� facturado.
    AlMenosUnCicloFacturado: Boolean;
begin
    with ObtenerGruposPorAnioAgenda do begin
        Parameters.ParamByName('@A�o').Value := Anio;
        Open;
        // Limpiar el �rbol.
        LimpiarTree;
        FCantidadGrupos := 0;
        while not Eof do begin
            (* Crear un nodo en el �rbol para el grupo. *)
            New(GrupoCiclo);
            GrupoCiclo^.Grupo := FieldByName('CodigoGrupoFacturacion').Value;
            GrupoCiclo^.Ciclo := 0;
            GrupoCiclo^.FrecuenciaGrupo := FieldByName('FactorFrecuencia').Value;
            GrupoCiclo^.YaFacturado := not (FieldByName('FechaRealFacturacion').IsNull);
            GrupoCiclo^.AlMenosUnCicloFacturado := False;
            NodeRaiz := tvGrupos.Items.AddChildObject(nil, Trim(FieldByName('Descripcion').AsString), GrupoCiclo);

            // Inicialiar en False por si no entra al if que est� abajo.
            CiclosGrupoEstanFacturados := False;
            // Inicialiar en False por si no entra al if que est� abajo.
            AlMenosUnCicloFacturado := False;

            (* Si el campo Ciclo NO tiene Null es porque es un Ciclo (esto es
            as� por como est� definido el store procedure
            ObtenerGruposPorAnioAgenda), agregar en el �rbol nodos para los
            ciclos. *)
            if FieldByName('Ciclo').Value <> Null then begin
                (* Inicialiar en True, para indicar que todos los ciclos
                estar�an facturados. *)
                CiclosGrupoEstanFacturados := True;
                Grupo := FieldByName('CodigoGrupoFacturacion').Value;
                (* Crear los nodos en el �rbol para los ciclos del grupo. *)
                while (not Eof) and (Grupo = FieldByName('CodigoGrupoFacturacion').Value) do begin
                    (* Crear un nodo en el �rbol para el ciclo. *)
                    New(GrupoCiclo);
                    GrupoCiclo^.Grupo := FieldByName('CodigoGrupoFacturacion').Value;
                    GrupoCiclo^.Ciclo := FieldByName('Ciclo').Value;
                    GrupoCiclo^.FrecuenciaGrupo := FieldByName('FactorFrecuencia').Value;
                    GrupoCiclo^.YaFacturado := not (FieldByName('FechaRealFacturacion').IsNull);
                    GrupoCiclo^.AlMenosUnCicloFacturado := False;
                    if FieldByName('Ciclo').AsInteger > 24 then                                                                     // TASK_133_MGO_20170207
                        Node := tvGrupos.Items.AddChildObject(NodeRaiz, '#' + FieldByName('Ciclo').AsString, GrupoCiclo)            // TASK_133_MGO_20170207
                    else                                                                                                            // TASK_133_MGO_20170207
                        Node := tvGrupos.Items.AddChildObject(NodeRaiz, NOMBRE_CICLOS[FieldByName('Ciclo').AsInteger], GrupoCiclo); // TASK_133_MGO_20170207

                    (* Si el ciclo est� facturado, indicar que al menos un ciclo
                    est� facturado.
                    Si el ciclo no est� facturado, NO todos los ciclos del
                    grupo est�n facturados. *)
                    if GrupoCiclo^.YaFacturado then
                        AlMenosUnCicloFacturado := True
                    else
                        CiclosGrupoEstanFacturados := False;

                    Node.ImageIndex := iif(GrupoCiclo^.YaFacturado, 3, 1);
                    Node.SelectedIndex := Node.ImageIndex;

                    Next;
                end; // while
            end else Next;

            (* Setear el campo AlMenosUnCicloFacturado para el nodo del grupo. *)
            TGrupoCiclo(NodeRaiz.Data^).AlMenosUnCicloFacturado := AlMenosUnCicloFacturado;
            (* Setear la imagen para el nodo del grupo. *)
            NodeRaiz.ImageIndex := iif(CiclosGrupoEstanFacturados, 2, 0);
            NodeRaiz.SelectedIndex := NodeRaiz.ImageIndex;

            Inc(FCantidadGrupos);
        end;
        Close;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.ConfirmarCambios
  Author:    ggomez
  Date:      01-Feb-2005
  Arguments: Sender: TObject. Objeto que llama al m�todo.
  Result:    Integer
  Description: Muestra un mensaje al operador para que realice una acci�n para
      los cambios que no han sido confirmados.
      Retorna el valor de la respuesta del operador.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.ConfirmarCambios(Sender: TObject): Integer;
resourcestring
    MSG_CONFIRMARCAMBIOS = '�Desea confirmar los cambios realizados?';
    MSG_TITULOCONFIRMARCAMBIOS = 'Agenda Facturaci�n';
begin
    Result := MsgBox(MSG_CONFIRMARCAMBIOS, MSG_TITULOCONFIRMARCAMBIOS, MB_YESNOCANCEL + MB_DEFBUTTON3);

    case Result of
        IDYES:  begin
                    btnAceptarClick(btnAceptar);

                    (* Si el bot�n Aceptar no est� habilitado es porque el
                    m�todo btnAceptarClick se ejecut� con exito. *)
                    if (not btnAceptar.Enabled) then FInsertando := False;
                end;
        IDNO:   begin
                    btnAceptar.Enabled := False;
                    btnCancelar.Enabled := False;

                    FInsertando := False;
                end;
        IDCANCEL:   begin
                        (* Si quien llam� este m�todo es el combo cbAnios,
                        seleccionar en el combo el a�o que estaba previamente. *)
                        if (Sender is TComboBox)  and ((Sender as TComboBox) = cbAnios) then
                            cbAnios.ItemIndex := FItemIndexAnioOld;

                        Exit;
                    end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.RealizarGeneracionCiclos
  Author:    ggomez
  Date:      01-Feb-2005
  Arguments: Frecuencia: Double. Frecuencia con la que se deben generar los
    ciclos.
  Result:    None
  Despcription: Realiza la generaci�n de ciclos.
-----------------------------------------------------------------------------}
procedure TfrmAgendaFacturacion.RealizarGeneracionCiclos(Frecuencia: Double);
var
    MesSeleccionado: Integer;
begin
    // Mostrar el form para seleccionar el mes.
    MesSeleccionado := SeleccionarMes;
    if MesSeleccionado = -1 then Exit;

    // Eliminamos los ciclos actuales de la tabla y del �rbol.
    EliminarCiclosGrupo;

    // Creamos los nuevos ciclos para el nodo seleccionado en el �rbol.
	InicializarCiclos(tvGrupos.Selected, Frecuencia, MesSeleccionado);
	btnAceptar.Enabled := True;
	btnCancelar.Enabled := True;
    (* Deshabilitar el bot�n de Eliminar Calendario. *)
    btn_EliminarCalendario.Enabled := False;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.RespetaAnioCalendario
  Author:    ggomez
  Date:      04-Feb-2005
  Arguments: Fecha: TDateTime; Anio: Word
  Result:    Boolean
  Description: Dada una Fecha y un A�o, retorna True si el a�o de la Fecha es
    igual a Anio o si es igual a Anio + 1.
    Este m�todo se utiliza para controlar que las fechas de los ciclos de un
    calendario respeten el a�o del calendario.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.RespetaAnioCalendario(
  Fecha: TDateTime; Anio: Word): Boolean;
var
    d, m, a: Word;
begin
    DecodeDate(Fecha, a, m, d);
    Result := (a = Anio) or ( a = Anio + 1);
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.RespetaAnioCalendarioMenos1
  Author:    jconcheyro
  Date:      28/12/2006
  Arguments: Fecha: TDateTime; Anio: Word
  Result:    Boolean
  Description: Dada una Fecha y un A�o, retorna True si el a�o de la Fecha es
    igual a Anio o si es igual a Anio - 1.
    Esta version especial es para la fecha de corte, que puede ser de un a�o
    anterior a los que se est�n cargando.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.RespetaAnioCalendarioMenos1(
  Fecha: TDateTime; Anio: Word): Boolean;
var
    d, m, a: Word;
begin
    DecodeDate(Fecha, a, m, d);
    Result := (a = Anio) or ( a = Anio - 1);
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.IngresarAnio
  Author:    ggomez
  Date:      10-Feb-2005
  Arguments: None
  Result:    AnsiString
  Description: Muestra un form para que el operador ingrese un a�o.
    Retorna el a�o ingresado por el operador. Si no se ingres� ninguno
    retorna EmptyStr.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.IngresarAnio: AnsiString;
var
    FIngresarAnio: TFormIngresarAnio;
begin
    (* Solicitar al operador que ingrese un a�o. *)
    Result := EmptyStr;
    FIngresarAnio := TFormIngresarAnio.Create(Self);
    try
        if FIngresarAnio.Inicializar then
            begin
                FIngresarAnio.ShowModal;
                Result := FIngresarAnio.ObtenerAnioIngresado;
            end;
    finally
        FIngresarAnio.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.GrupoExisteEnBD
  Author:    ggomez
  Date:      14-Feb-2005
  Arguments: Conn: TADOConnection; CodigoGrupo: Integer
  Result:    Boolean
  Description: Retorna si un grupo existe o no en la tabla GruposFacturacion de
    la base de datos. Utiliza la funci�n: ExisteGrupoFacturacion.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.ExisteGrupoEnBD(Conn: TADOConnection;
  CodigoGrupo: Integer): Boolean;
const
    CONS_TRUE = 'True';
begin
    Result := (QueryGetValue(Conn,
                'SELECT dbo.ExisteGrupoFacturacion(' + IntToStr(CodigoGrupo) + ')') = CONS_TRUE);
end;

procedure TfrmAgendaFacturacion.btn_EliminarCalendarioClick(
  Sender: TObject);
resourcestring
    MSG_ELIMINAR_CALENDARIO_CICLOS_FACTURADOS = 'No se puede eliminar el calendario'
        + ' porque tiene Ciclos de Facturaci�n facturados.';
    MSG_TITULO = 'Agenda de Facturaci�n';
    MSG_ERROR_ELIMINAR_CALENDARIO = 'Ha ocurrido un error al eliminar el calendario.';
    MSG_SELECCIONAR_CALENDARIO = 'Debe seleccionar un calendario.';
    MSG_DESEA_ELIMINAR = '�Est� seguro que desea eliminar el calendario seleccionado?';
var
    (* Para mantener el �ndice del a�o eliminado. *)
    IndiceEliminado: Integer;
begin
    (* Si no hay calendarios cargados o uno seleccionado en el combo, NO hacer
    nada. *)
    if (cbAnios.Items.Count < 0) or (cbAnios.ItemIndex <= -1) then begin
        MsgBox(MSG_SELECCIONAR_CALENDARIO, MSG_TITULO, MB_ICONINFORMATION);
        Exit;
    end;

    (* Si hay calendarios cargados en el combo y hay un item seleccionado,
    permitir la eliminaci�n. *)
    if (cbAnios.Items.Count > 0) and (cbAnios.ItemIndex > -1) then begin
        (* Preguntar al operador si est� seguro que desea eliminar el calendario
        seleccionado. Si responde que NO, NO continuar. *)
        if MsgBox(MSG_DESEA_ELIMINAR, MSG_TITULO,
                MB_ICONWARNING + MB_YESNO + MB_DEFBUTTON2) <> IDYES then begin
            Exit;
        end;
        (* Verificar que todos los ciclos del calendario est�n NO Facturados. *)
        if CiclosCalendarioTodosNoFacturados(-1) then begin
            IndiceEliminado := cbAnios.ItemIndex;
            try
                (* Intentar la eliminaci�n del calendario de la base de
                datos. *)
                with sp_EliminarCalendario, Parameters do begin
                    ParamByName('@Anio').Value := StrToInt(cbAnios.Text);
                    ExecProc;
                end;

                (* Eliminar el item del combo. *)
                cbAnios.DeleteSelected;
                (* Si queda alg�n item en el combo, seleccionar el m�s cercano al
                eliminado. *)
                if cbAnios.Items.Count > 0 then begin
                    (* Si el �ndice eliminado se puede seleccionar entre los
                    que quedaron, seleccionarlo. *)
                    if IndiceEliminado < cbAnios.Items.Count then cbAnios.ItemIndex := IndiceEliminado
                    else begin
                        (* Si el �ndice eliminado ya no se puede seleccionar,
                        seleccionar el anterior. *)
                        cbAnios.ItemIndex := IndiceEliminado - 1;
                    end;
                end; // if cbAnios.Items.Count > 0

                (* Actualizar el �rbol. *)
                cbAniosChange(cbAnios);
            except
                on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_ELIMINAR_CALENDARIO, E.Message,
                        MSG_TITULO, MB_ICONSTOP);
                end;
            end;

        end else begin
            (* Al menos un ciclo del calendario est� facturado. *)
            MsgBox(MSG_ELIMINAR_CALENDARIO_CICLOS_FACTURADOS, MSG_TITULO,
                MB_ICONINFORMATION);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.CiclosCalendarioTodosNoFacturados
  Author:    ggomez
  Date:      16-Feb-2005
  Arguments: CodigoGrupo: Integer. C�digo de grupo para el cual verificar. Si
    es -1, indica que se deben testear todos los ciclos de todos los grupos.
  Result:    Boolean
  Description: Retorna True si todos los ciclos del �rbol del grupo pasado como
    par�metro, est�n NO Facturados.
    Recorre los nodos del �rbol del grupo, si alguno tiene el campo YaFacturado
    en True, la funci�n retornar� False.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.CiclosCalendarioTodosNoFacturados(CodigoGrupo: Integer): Boolean;
var
    IndiceNodo: Integer;
begin
    Result := True;
    IndiceNodo := 0;
    if CodigoGrupo = -1 then begin
        (* Mientras haya nodos, seguir testeando. *)
        while Result and (IndiceNodo < tvGrupos.Items.Count) do begin
            if TGrupoCiclo(tvGrupos.Items[IndiceNodo].Data^).YaFacturado then
                Result := False;
            Inc(IndiceNodo);
        end;
    end else begin
        (* Testear los ciclos del grupo pasado como par�metro. *)
        while Result and (IndiceNodo < tvGrupos.Items[CodigoGrupo].Count) do begin
            if TGrupoCiclo(tvGrupos.Items[CodigoGrupo].Item[IndiceNodo].Data^).YaFacturado then
                Result := False;
            Inc(IndiceNodo);
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: TfrmAgendaFacturacion.VerificarDiasHabilesCiclos
  Author:    ggomez
  Date:      23-Feb-2005
  Arguments: None
  Result:    Boolean
  Description: Recorre los ciclos de cada grupo de un calendario verificando
    que las fechas de corte de ciclos consecutivos, respeten la cantidad
    m�nima entre ellas.
    S�lo verifica las fechas de los ciclos NO facturados. Es decir, dados los
    ciclos a comparar C1 y C2:
        Si C1.YaFacturado entonces avanzar para testear los pr�ximos dos ciclos.
        Sino, Si C2.YaFacturado entonces avanzar para testear los pr�ximos dos ciclos.

    Si alg�nos ciclos NO respetan la diferencia de d�as, la verificaci�n se
    detiene retornando False.
    Si todos los ciclos respetan la diferencia de d�as, retorna True.

 Revision 1:
  Author:       plaza
  Date:         15/02/2010
  Description:  SS 419
    - Se modifico el mensaje de error cuando se ingresa una fecha de corte
      invalida.
-----------------------------------------------------------------------------}
function TfrmAgendaFacturacion.VerificarFechasCorteCiclos: Boolean;
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_OBTENER_PARAMETRO = 'Error al obtener el par�metro ';
    MSG_ERROR_FECHAS = 'Error de fechas';
{Revision 1
    MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS = 'Las fechas de Corte de los Ciclos'
        + ' no respetan la cantidad de d�as de diferencia.';}
    MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_1 = 'La diferencia de d�as entre las fechas de corte de los ciclos: ';
    MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_2 = ' y ';
    MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_3 = 'no respetan la cantidad de ';
    MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_4 = ' d�a/s ';
{Fin Revision 1}
    MSG_VER_VALOR_PARAMETRO = 'Verifique el el valor del par�metro';
    DOS_PUNTOS = ': ';
var
    // Filtro que tiene el ClientDataSet al comenzar la verificaci�n.
    FiltroOriginal: AnsiString;
    CodigoGrupo, CodigoCiclo, IndiceNodo, DiferenciaDias: Integer;
    FechaCorteAnterior: TDateTime;
    Parametro: AnsiString;
begin
    CodigoGrupo := -1;
    CodigoCiclo := -1;
    Result := True;
    with ClientDataSet do begin
        (* Guardo el filtro que tiene el ClientDataSet. *)
        FiltroOriginal := Filter;
        (* Como el ClientDataSet est� indexado por CodigoGrupoFacturacion; CodigoCiclo,
        al sacar el filtro los registros quedan oredenados por grupo y por ciclo. *)
        Filter := EmptyStr;
        try
            First;
            (* Mientras haya ciclos por testear y NO haya encontrado error,
            seguir testeando. *)
            while (Result) and (not Eof) do begin
                (* Almaceno el Grupo que estoy por tratar. *)
                CodigoGrupo := FieldByName('CodigoGrupoFacturacion').AsInteger;
                (* Recorrer los ciclos de un grupo. *)
                while (Result) and (not Eof) and (CodigoGrupo = FieldByName('CodigoGrupoFacturacion').AsInteger) do begin
                    (* Si la fecha de facturaci�n es null, es porque el ciclo NO
                    est� facturado. *)
                    if FieldByName('FechaRealFacturacion').IsNull then begin
                        FechaCorteAnterior := FieldByName('FechaCorte').AsDateTime;
                        (* Si hay un siguiente ciclo en el grupo y tampoco est�
                        facturado, verificar si respetan la diferencia de d�as. *)
                        if (RecNo < RecordCount) then begin
                            // Obtener el siguiente ciclo.
                            Next;
                            try
                                if (CodigoGrupo = FieldByName('CodigoGrupoFacturacion').AsInteger) and
                                        FieldByName('FechaRealFacturacion').IsNull then begin
                                    Parametro := EmptyStr;
                                    if FieldByName('FactorFrecuencia').AsFloat = 0.5 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_QUINCENAL
                                    //else begin                                                                                                // TASK_133_MGO_20170207
                                    else if FieldByName('FactorFrecuencia').AsFloat = 0.25 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_SEMANAL  // TASK_133_MGO_20170207
                                    else if FieldByName('FactorFrecuencia').AsFloat = 1.0 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_MENSUAL   // TASK_133_MGO_20170207
                                    else if FieldByName('FactorFrecuencia').AsFloat = 2.0 then Parametro := DIAS_DIF_MIN_FECHAS_CORTE_BIMESTRAL;
                                    //end;                                                                                                      // TASK_133_MGO_20170207

                                    (* Obtener la cantidad de d�a de diferencia. *)
                                    if not ObtenerParametroGeneral(DMConnections.BaseCAC,
                                            Parametro, DiferenciaDias) then begin
                                        Result := False;
                                        MsgBoxErr(MSG_ERROR, MSG_ERROR_OBTENER_PARAMETRO +
                                            Parametro, MSG_ERROR, MB_ICONERROR);
                                        Break;
                                    end;

                                    if ((FieldByName('FechaCorte').AsDateTime - FechaCorteAnterior) < DiferenciaDias) then begin
                                        Result := False;
                                    end;
                                end; // if (CodigoGrupo = FieldByName('CodigoGrupoFacturacion').AsInteger)
                            finally
                                // Vuelvo al ciclo anterior.
                                Prior;
                            end;

                        end; // if not Eof
                    end; // if FieldByName('FechaRealFacturacion').IsNull

                    if Result then Next;
                end; // while
            end; // while

            (* Si Result tiene False es porque algunos ciclos NO respetan la
            diferencia de d�as. *)
            if not Result then begin
                (* Seleccionar en el �rbol el nodo del ciclo que tiene el
                error. *)
                CodigoGrupo := FieldByName('CodigoGrupoFacturacion').AsInteger;
                CodigoCiclo := FieldByName('CodigoCiclo').AsInteger;
                IndiceNodo := ObtenerIndiceNodo(CodigoGrupo, CodigoCiclo);
                tvGrupos.Selected := nil;
                tvGrupos.Select(tvGrupos.Items[IndiceNodo]);
                (* Llamar a ValidateControls para mostrar el mensaje con el
                error. *)
{Revision 1
                ValidateControls([deFechaCorte],
                    [False],
                    MSG_ERROR_FECHAS,
                    [MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS + CRLF +
                        MSG_VER_VALOR_PARAMETRO + DOS_PUNTOS + Parametro]);}

                ValidateControls([deFechaCorte],
                    [False],
                    MSG_ERROR_FECHAS,
                    [MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_1 +
                       IntToStr(CodigoCiclo) +
                       MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_2 +
                       IntToStr(CodigoCiclo + 1) + CRLF +
                       MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_3 +
                       IntToStr(DiferenciaDias) +
                       MSG_FECHAS_CORTE_NO_RESPETAN_DIFERENCIA_DIAS_4]);
{Fin Revision 1}
            end;
        finally
            (* Si NO encontr� error, restaurar el filtro que ten�a
            el ClientDataSet al iniciar el proceso.
            Si encontr� error, filtrar por el grupo del ciclo que
            tiene el error y seleccionar el registro que tiene el error. *)
            if Result then Filter := FiltroOriginal
            else begin
                Filter := 'CodigoGrupoFacturacion = ' + FieldByName('CodigoGrupoFacturacion').AsString;
                Locate('CodigoGrupoFacturacion; CodigoCiclo',
                        VarArrayOf([CodigoGrupo, CodigoCiclo]), []);
            end;
        end; // finally
    end; // with
end;

procedure TfrmAgendaFacturacion.pmiEliminarCiclosGrupoClick(Sender: TObject);
resourcestring
    MSG_ELIMINAR_CICLOS_FACTURADOS = 'No se pueden eliminar los ciclos'
        + ' porque al menos uno est� facturado.';
    MSG_TITULO = 'Agenda de Facturaci�n';
begin
    (* Como el ClientDataSet est� filtrado por Grupo, se puede saber si tiene
    ciclos. *)
    if not ClientDataSet.IsEmpty then begin
        (* Verificar que todos los ciclos del grupo est�n NO facturados. *)
        if CiclosCalendarioTodosNoFacturados(TGrupoCiclo(tvGrupos.Selected.Data^).Grupo) then begin
            // Eliminamos los ciclos actuales de la tabla y del �rbol.
            EliminarCiclosGrupo;

            (* Habilitar los botones Aceptar y Cancelar para indicar que hubo una
            modificaci�n del Calendario. *)
            btnAceptar.Enabled := True;
            btnCancelar.Enabled := True;
            (* Deshabilitar el bot�n de Eliminar Calendario. *)
            btn_EliminarCalendario.Enabled := False;
        end else begin
            (* Al menos un ciclo del calendario est� facturado. *)
            MsgBox(MSG_ELIMINAR_CICLOS_FACTURADOS, MSG_TITULO, MB_ICONINFORMATION);
        end;

    end;
end;

end.

