object frmAjusteFacturacion: TfrmAjusteFacturacion
  Left = 25
  Top = 84
  Anchors = []
  Caption = 'Ajuste de Facturaci'#243'n'
  ClientHeight = 549
  ClientWidth = 923
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 647
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    923
    549)
  PixelsPerInch = 96
  TextHeight = 13
  object gbDatosConvenio: TGroupBox
    Left = 152
    Top = 77
    Width = 626
    Height = 77
    Anchors = [akTop]
    Caption = ' Datos del Convenio '
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 22
      Width = 78
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object Label3: TLabel
      Left = 8
      Top = 49
      Width = 105
      Height = 13
      Caption = 'Convenios del Cliente:'
    end
    object Label7: TLabel
      Left = 9
      Top = 305
      Width = 122
      Height = 13
      Caption = 'Detalle del Comprobante: '
    end
    object Label8: TLabel
      Left = 339
      Top = 19
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object peRUTCliente: TPickEdit
      Left = 120
      Top = 18
      Width = 139
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = True
      TabOrder = 0
      OnChange = peRUTClienteChange
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object cbConveniosCliente: TVariantComboBox
      Left = 120
      Top = 45
      Width = 209
      Height = 19
      Style = vcsOwnerDrawFixed
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbConveniosClienteChange
      OnDrawItem = cbConveniosClienteDrawItem
      Items = <>
    end
    object NumeroConvenio: TEdit
      Left = 447
      Top = 15
      Width = 160
      Height = 21
      Color = 16444382
      TabOrder = 1
      OnChange = NumeroConvenioChange
    end
  end
  object gbDatosCliente: TGroupBox
    Left = 152
    Top = 180
    Width = 627
    Height = 100
    Anchors = [akTop]
    Caption = ' Datos del Cliente '
    TabOrder = 1
    object lNombreCompleto: TLabel
      Left = 81
      Top = 19
      Width = 83
      Height = 13
      Caption = 'lNombreCompleto'
    end
    object lDomicilio: TLabel
      Left = 81
      Top = 38
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComuna: TLabel
      Left = 81
      Top = 57
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegion: TLabel
      Left = 81
      Top = 76
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label31: TLabel
      Left = 14
      Top = 19
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Apellido:'
    end
    object Label33: TLabel
      Left = 14
      Top = 38
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label34: TLabel
      Left = 14
      Top = 57
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label35: TLabel
      Left = 14
      Top = 76
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
  end
  object gbAjuste: TGroupBox
    Left = 152
    Top = 306
    Width = 627
    Height = 97
    Anchors = [akTop]
    Caption = ' Datos del Ajuste '
    TabOrder = 2
    object Label10: TLabel
      Left = 14
      Top = 44
      Width = 77
      Height = 13
      Caption = 'Ajuste de Peaje:'
    end
    object Label11: TLabel
      Left = 14
      Top = 69
      Width = 93
      Height = 13
      Caption = 'Ajuste de Intereses:'
    end
    object Label2: TLabel
      Left = 12
      Top = 20
      Width = 82
      Height = 13
      Caption = 'Fecha del Ajuste:'
    end
    object Label4: TLabel
      Left = 248
      Top = 44
      Width = 84
      Height = 13
      Caption = 'Motivo del Ajuste:'
    end
    object Label5: TLabel
      Left = 248
      Top = 69
      Width = 84
      Height = 13
      Caption = 'Motivo del Ajuste:'
    end
    object edFechaAjuste: TDateEdit
      Left = 114
      Top = 17
      Width = 121
      Height = 21
      AutoSelect = False
      TabOrder = 0
      OnChange = edAjustePeajeChange
      Date = -693594.000000000000000000
    end
    object edMotivoAjustePeaje: TEdit
      Left = 342
      Top = 40
      Width = 274
      Height = 21
      Hint = 'Ingrese el motivo del ajuste'
      MaxLength = 60
      TabOrder = 2
    end
    object edMotivoAjusteIntereses: TEdit
      Left = 342
      Top = 65
      Width = 274
      Height = 21
      Hint = 'Ingrese el motivo del ajuste'
      MaxLength = 60
      TabOrder = 4
    end
    object edAjustePeaje: TEdit
      Left = 114
      Top = 39
      Width = 121
      Height = 21
      Hint = 'Ingrese el importe del ajuste con su correspondiente signo'
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = edAjustePeajeChange
      OnKeyPress = edAjustePeajeKeyPress
    end
    object edAjusteIntereses: TEdit
      Left = 114
      Top = 62
      Width = 121
      Height = 21
      Hint = 'Ingrese el importe del ajuste con su correspondiente signo'
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnChange = edAjustePeajeChange
      OnKeyPress = edAjustePeajeKeyPress
    end
  end
  object btnSalir: TButton
    Left = 829
    Top = 522
    Width = 96
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 4
    OnClick = btnSalirClick
  end
  object btnAjustar: TButton
    Left = 732
    Top = 522
    Width = 96
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Ajustar'
    Default = True
    Enabled = False
    TabOrder = 3
    OnClick = btnAjustarClick
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 518
    Top = 102
  end
  object spAgregarMovimientoCuenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarMovimientoCuenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 422
    Top = 102
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 326
    Top = 105
  end
  object tmrConsulta: TTimer
    Enabled = False
    OnTimer = tmrConsultaTimer
    Left = 230
    Top = 102
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 578
    Top = 105
  end
end
