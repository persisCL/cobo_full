object FrmFinanciamientos: TFrmFinanciamientos
  Left = 117
  Top = 163
  Width = 835
  Height = 472
  Anchors = []
  Caption = 'Financiamientos'
  Color = clBtnFace
  Constraints.MinHeight = 465
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    827
    445)
  PixelsPerInch = 96
  TextHeight = 13
  object btnSalir: TDPSButton
    Left = 747
    Top = 391
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 827
    Height = 385
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      827
      385)
    object gbCuentasMorosas: TGroupBox
      Left = 6
      Top = 114
      Width = 815
      Height = 264
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Lista de Financiamientos'
      TabOrder = 1
      DesignSize = (
        815
        264)
      object dbgFinanciamientos: TDPSGrid
        Left = 8
        Top = 16
        Width = 799
        Height = 240
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = dsFinanciamientos
        Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = dbgFinanciamientosDrawColumnCell
        OnTitleClick = dbgFinanciamientosTitleClick
        OnWillClickTitle = dbgFinanciamientosWillClickTitle
        OnOrderChange = dbgFinanciamientosOrderChange
        Columns = <
          item
            Expanded = False
            FieldName = 'Estado'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FechaAlta'
            Title.Caption = 'Fecha'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DescriCuentas'
            Title.Caption = 'Cuenta'
            Width = 173
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'DescriImporteAFinanciar'
            Title.Caption = 'Importe Financiado'
            Width = 99
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'DescriPieEntregado'
            Title.Caption = 'Pie Entregado'
            Width = 74
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Interes'
            Title.Caption = 'Inter'#233's'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FechaVigencia'
            Title.Caption = 'Vigencia'
            Width = 83
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'DescriImporteCuota'
            Title.Caption = 'Importe de Cuota'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CantidadCuotas'
            Title.Caption = 'Cuotas'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CantidadCuotasImpagas'
            Title.Caption = 'Cuotas Impagas'
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CantidadCuotasPagas'
            Title.Caption = 'Cuotas Pagas'
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ProximaFacturacion'
            Title.Caption = 'Pr'#243'xima Facturaci'#243'n'
            Width = 102
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ProximoVencimiento'
            Title.Caption = 'Pr'#243'ximo Vencimiento'
            Width = 118
            Visible = True
          end>
      end
    end
    object GroupBox4: TGroupBox
      Left = 6
      Top = 1
      Width = 815
      Height = 64
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Par'#225'metros de B'#250'squeda'
      TabOrder = 0
      object Label3: TLabel
        Left = 11
        Top = 18
        Width = 88
        Height = 13
        Caption = 'C'#243'digo del Cliente:'
      end
      object Label4: TLabel
        Left = 150
        Top = 18
        Width = 67
        Height = 13
        Caption = 'Desde el d'#237'a: '
      end
      object peCodigoCliente: TPickEdit
        Left = 11
        Top = 33
        Width = 126
        Height = 21
        Enabled = True
        TabOrder = 0
        OnChange = peCodigoClienteChange
        Decimals = 0
        EditorStyle = bteNumericEdit
        OnButtonClick = peCodigoClienteButtonClick
      end
      object deDesdeFecha: TDateEdit
        Left = 150
        Top = 33
        Width = 103
        Height = 21
        AutoSelect = False
        TabOrder = 1
        OnChange = deDesdeFechaChange
        Date = -693594.000000000000000000
      end
    end
    object gbCliente: TGroupBox
      Left = 6
      Top = 68
      Width = 815
      Height = 44
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Datos de Cliente'
      TabOrder = 2
      DesignSize = (
        815
        44)
      object Label6: TLabel
        Left = 8
        Top = 21
        Width = 48
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lApellidoNombre: TLabel
        Left = 58
        Top = 21
        Width = 199
        Height = 13
        AutoSize = False
        Caption = '                                                         '
      end
      object LDomicilio: TLabel
        Left = 328
        Top = 21
        Width = 481
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        Caption = '                    '
      end
      object Label1: TLabel
        Left = 265
        Top = 21
        Width = 56
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object btnCaducar: TDPSButton
    Left = 605
    Top = 391
    Width = 136
    Anchors = [akRight, akBottom]
    Caption = '&Caducar'
    TabOrder = 2
    OnClick = btnCaducarClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 426
    Width = 827
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Style = psOwnerDraw
        Width = 50
      end
      item
        Bevel = pbNone
        Width = 50
      end>
    SimplePanel = False
    OnDrawPanel = StatusBarDrawPanel
  end
  object ProgressBar: TProgressBar
    Left = 6
    Top = 395
    Width = 249
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 5
  end
  object btnGenerar: TDPSButton
    Left = 448
    Top = 391
    Width = 151
    Anchors = [akRight, akBottom]
    Caption = '&Generar Financiamientos'
    TabOrder = 1
    OnClick = btnGenerarClick
  end
  object ObtenerFinanciamientos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerFinanciamientos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@EstadoFinanciamiento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 288
    Top = 152
  end
  object Imagenes: TImageList
    Left = 352
    Top = 152
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object ObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 256
    Top = 152
  end
  object dsFinanciamientos: TDataSource
    DataSet = ObtenerFinanciamientos
    OnDataChange = dsFinanciamientosDataChange
    Left = 224
    Top = 152
  end
  object CaducarFinanciamiento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CaducarFinanciamiento'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 320
    Top = 152
  end
end
