unit FacturacionManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, validate,
  Dateedit, ExtCtrls, utilProc, DmiCtrls, util, DBCtrls, ImgList, ADODB, CheckLst, DMConnection, ComCtrls,
  BuscaClientes, navigator, ReporteFactura, InDatosMovimientoFactManual, ReporteInformeFactura, peaProcs,
  peaTypes, ReporteDetalleViajes, Utildb, DB, DPSControls;

type
  TfrmEmisionFactura = class(TForm)
    ObtenerCLiente: TADOStoredProc;
    CrearLoteFacturacion: TADOStoredProc;
    CrearProcesoFacturacion: TADOStoredProc;
    UpdateNumeroLote: TADOQuery;
    CrearComprobante: TADOStoredProc;
    AgregarMovimientoCuenta: TADOStoredProc;
    ActualizarProcesoFacturacion: TADOQuery;
    ActualizarNumeroMovimiento: TADOQuery;
    Imagenes: TImageList;
    btnSalir: TDPSButton;
    btnFacturar: TDPSButton;
    btnImprimir: TDPSButton;
    Panel1: TPanel;
    gbMovSeleccionados: TGroupBox;
    Bevel1: TBevel;
    lApellidoNombre: TLabel;
    Bevel2: TBevel;
    LImporteTotal: TLabel;
    lDomicilio: TLabel;
    lVencimiento: TLabel;
    txtMedioPago: TLabel;
    ListBox: TListBox;
    btnAgregarLinea: TDPSButton;
    btnEliminarLinea: TDPSButton;
    gbMovDisponibles: TGroupBox;
    CheckListBox: TCheckListBox;
    btnSeleccionarTodo: TDPSButton;
    btnInvertirSeleccion: TDPSButton;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    deFechaHora: TDateEdit;
    cbConveniosCliente: TComboBox;
    peCodigoCliente: TPickEdit;
    ObtenerInformeFactura: TADOStoredProc;
    qryConvenios: TADOQuery;
    ObtenerDebitoAutomatico: TADOQuery;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    ActualizarTotalLoteFacturacion: TADOStoredProc;
    CrearProcesoFacturacionGrupoFacturacion: TADOStoredProc;
    ObtenerDomicilioConvenio: TADOStoredProc;
    ObtenerDatosMedioPagoAutomaticoConvenio: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure CheckListBoxClickCheck(Sender: TObject);
    procedure btnSeleccionarTodoClick(Sender: TObject);
    procedure btnInvertirSeleccionClick(Sender: TObject);
    procedure btnFacturarClick(Sender: TObject);
    procedure btnAgregarLineaClick(Sender: TObject);
    procedure btnEliminarLineaClick(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure peCodigoClienteButtonClick(Sender: TObject);
    procedure cbConveniosMedioPagoChange(Sender: TObject);
    procedure peCodigoClienteChange(Sender: TObject);
    procedure deFechaHoraChange(Sender: TObject);
    procedure deFechaHoraExit(Sender: TObject);
    procedure deFechaHoraEnter(Sender: TObject);
  private
    { Private declarations }
	FUltimaBusqueda : TBusquedaCliente;
    FTotal: Double;
    FNavigator: TNavigator;
    FNumeroComprobante: Double;
    FApellido, FApellidoMaterno, FNombre: AnsiString;
    FIndexCuentaDebito, FIndexCuentaCliente: Integer;
    FCodigoCliente: longint;
    FFechaLimite: TDateTime;
    FMedioPago: integer;
    FDiasVencimiento: integer;
    procedure UpdateListBox(index: Integer);
    procedure LimpiarListas;
    procedure inicializaControles;
  public
    { Public declarations }
    function inicializa(Navigator: TNavigator): Boolean;
//    function findFirst(SearchText: AnsiString): Boolean; override;
  end;


implementation

{$R *.dfm}

{ TfrmAnularReimprimirFactura }

type TItemFactura = record
        IndexLista, CodigoConcepto: Integer;
        Observaciones: AnsiString;
        CodigoConcesionaria, NumeroMovimiento, CodigoConvenio, IndiceVehiculo, VersionPromocion, NumeroPromocion, NumeroFinanciamiento: LongInt;
        Importe: Double;
    end;
	PTItemFactura = ^TItemFactura;

resourcestring
    MSG_FECHA_INCORRECTA    = 'La fecha limite no puede ser mayor a la actual.';
    CAPTION_GENERAR_FACTURA = 'Generar Factura';
    CAPTION_VALIDAR_FECHA   = 'Validar Fecha';

function TfrmEmisionFactura.inicializa(Navigator: TNavigator): Boolean;
begin
    Result := True;

    try
        FNavigator := Navigator;
        deFechaHora.Date := NowBase(DMConnections.BaseCAC);
        FTotal := 0;
        FNumeroComprobante := -1;
        FIndexCuentaDebito := -1;
        FIndexCuentaCliente := -1;
        FCodigoCliente := 0;
        FMedioPago := -1;

        FFechaLimite := NowBase(DMConnections.BaseCAC);

        gbMovDisponibles.Enabled := false;
        gbMovSeleccionados.Enabled := false;
        btnFacturar.Enabled := false;
        btnImprimir.Enabled := false;
        btnSeleccionarTodo.Enabled := false;
        btnInvertirSeleccion.Enabled := false;
        btnAgregarLinea.Enabled := false;
        btnEliminarLinea.Enabled := false;
        cbConveniosCliente.Enabled := false;

        lApellidoNombre.Caption := '';
        lDomicilio.Caption := '';
        txtMedioPago.caption := '';
        lVencimiento.Caption := '';
        lImporteTotal.Caption := '';

        Width  := GetFormClientSize(Application.MainForm).cx;
        Height := GetFormClientSize(Application.MainForm).cy;
        CenterForm(Self);

        FDiasVencimiento := QueryGetValueInt(DMConnections.BaseCAC, 'Select PlazoVencimientoFacturas from Parametros');
    except
        on e : Exception do begin
            MsgBoxErr('No se pudo cargar la Facturación Manual', e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

procedure TfrmEmisionFactura.LimpiarListas;
begin
    While ListBox.Items.Count > 0 do begin
        if TItemFactura(Pointer(ListBox.Items.Objects[0])^).IndexLista = -1 then
            dispose(Pointer(ListBox.Items.Objects[0]));
        ListBox.Items.Delete(0);
    end;
    While CheckListBox.Items.Count > 0 do begin
        dispose(Pointer(CheckListBox.Items.Objects[0]));
        CheckListBox.Items.Delete(0);
    end;
    FNumeroComprobante := 0;
    FTotal := 0;
end;

procedure TfrmEmisionFactura.UpdateListBox(index: Integer);
var i: Integer;
begin
    with CheckListBox do begin
        if Checked[Index] then begin
            ListBox.Items.AddObject(items[Index],items.Objects[Index]);
            FTotal := FTotal + PTItemFactura(items.Objects[Index])^.importe;
        end else begin
            FTotal := FTotal - PTItemFactura(items.Objects[Index])^.importe;
            i := 0;
            while (PTItemFactura(ListBox.Items.Objects[i])^.IndexLista <>
                   PTItemFactura(items.Objects[Index])^.IndexLista) do
                inc(i);
            ListBox.Items.Delete(i);
        end;
        btnFacturar.Enabled := (ListBox.Items.Count > 0) and (FTotal > 0);
        btnImprimir.Enabled := btnFacturar.Enabled;
        ListBoxClick(ListBox);
    end;
end;

procedure TfrmEmisionFactura.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmEmisionFactura.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmEmisionFactura.CheckListBoxClickCheck(Sender: TObject);
begin
    UpdateListBox(TCheckListBox(Sender).ItemIndex);
	lImporteTotal.Caption := format('%12.12s',[formatFloat(FORMATO_IMPORTE, FTotal)]);
end;

procedure TfrmEmisionFactura.btnSeleccionarTodoClick(Sender: TObject);
var i: Integer;
begin
    i := 0;
    While i < CheckListBox.Items.Count do begin
        if not CheckListBox.Checked[i] then begin
			CheckListBox.checked[i] := true;
			UpdateListBox(i);
		end;
    inc(i);
    end;
	lImporteTotal.Caption := format('%12.12s',[formatFloat(FORMATO_IMPORTE,FTotal)]);
end;

procedure TfrmEmisionFactura.btnInvertirSeleccionClick(Sender: TObject);
var i: Integer;
begin
	i := 0;
	While i < CheckListBox.Items.Count do begin
		CheckListBox.Checked[i] := not CheckListBox.Checked[i];
		UpdateListBox(i);
		inc(i);
	end;
	lImporteTotal.Caption := format('%12.12s',[formatFloat(FORMATO_IMPORTE,FTotal)]);
end;

procedure TfrmEmisionFactura.btnFacturarClick(Sender: TObject);
var
	i, j, CantidadCuentas: Integer;
	NumeroMovimiento, ProcesoFacturacion, LoteFacturacion: LongInt;
	F: TFormReporteFactura;
    FDetalle: TFrmDetalleViajes;
	importeTotal: Double;

	procedure InsertarLote;
	begin
		with CrearLoteFacturacion do begin
			Parameters.ParamByName('@CodigoConvenio').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConvenio;
//			Parameters.ParamByName('@IndiceVehiculo').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).IndiceVehiculo;
			Parameters.ParamByName('@Total').value := 0;
			Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
			Parameters.ParamByName('@NumeroProcesoFacturacion').value := ProcesoFacturacion;
			Parameters.ParamByName('@TipoComprobante').value := 'F';
			Parameters.ParamByName('@NumeroComprobante').value := FNumeroComprobante;
			ExecProc;
			LoteFacturacion := Parameters.ParamByName('@LoteFacturacion').value;
		end;
	end;

	procedure InsertarMovimientoCuenta;
	begin
		with AgregarMovimientoCuenta do begin
			Parameters.ParamByName('@CodigoConvenio').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConvenio;
			Parameters.ParamByName('@IndiceVehiculo').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).IndiceVehiculo;
			Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
			Parameters.ParamByName('@Importe').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).Importe;
			Parameters.ParamByName('@CodigoConcepto').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConcepto;
			Parameters.ParamByName('@FechaCorte').value := iif(deFechaHora.date = date, deFechaHora.date + time, deFechaHora.date);
			Parameters.ParamByName('@LoteFacturacion').value := LoteFacturacion;
			Parameters.ParamByName('@EsPago').value := 0;
			Parameters.ParamByName('@Observaciones').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).Observaciones;
			if TItemFactura(Pointer(ListBox.Items.Objects[j])^).NumeroPromocion <> -1 then
				Parameters.ParamByName('@NumeroPromocion').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).NumeroPromocion
			else
				Parameters.ParamByName('@NumeroPromocion').value := null;
			if TItemFactura(Pointer(ListBox.Items.Objects[j])^).NumeroFinanciamiento <> -1 then
				Parameters.ParamByName('@NumeroFinanciamiento').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).NumeroFinanciamiento
			else
				Parameters.ParamByName('@NumeroFinanciamiento').value := null;
            ExecProc;
            NumeroMovimiento := Parameters.ParamByName('@NumeroMovimiento').value;
        end;
		// Si el movimiento es por peaje actualizo los viajes
        if TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConcepto = 1 then begin
            ActualizarNumeroMovimiento.Parameters.ParamByName('NumeroMovimiento').Value := NumeroMovimiento;
            ActualizarNumeroMovimiento.Parameters.ParamByName('CodigoConcesionaria').Value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConcesionaria;
            ActualizarNumeroMovimiento.Parameters.ParamByName('CodigoConvenio').Value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConvenio;
            ActualizarNumeroMovimiento.Parameters.ParamByName('IndiceVehiculo').Value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).IndiceVehiculo;
			ActualizarNumeroMovimiento.Parameters.ParamByName('FechaHora').Value := NowBase(DMConnections.BaseCAC);
            ActualizarNumeroMovimiento.execSQL;
        end;
	end;

    procedure UpdateMovimientoCuenta;
    begin
        with UpdateNumeroLote do begin
			close;
			Parameters.ParamByName('LoteFacturacion').Value := LoteFacturacion;
			Parameters.ParamByName('CodigoConvenio').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConvenio;
//			Parameters.ParamByName('IndiceVehiculo').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).IndiceVehiculo;
			Parameters.ParamByName('NumeroMovimiento').value := TItemFactura(Pointer(ListBox.Items.Objects[j])^).NumeroMovimiento;
			ExecSQL;
		end;
	end;

	procedure InsertarComprobante;
	begin
		// Obtenemos el tipo de debito, la entidad y la cuenta debito
		with CrearComprobante do begin
			// Parametros del comprobante
			Parameters.ParamByName('@TipoComprobante').value := 'F';
			Parameters.ParamByName('@SubTipoComprobante').value := 'A';
			Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
			Parameters.ParamByName('@ImporteTotal').Value := FTotal;
			Parameters.ParamByName('@FechaVencimiento').Value := null;
			Parameters.ParamByName('@TipoComprobanteAjustado').Value := null;
			Parameters.ParamByName('@NumeroComprobanteAjustado').Value := null;

			// Parametros del cliente
            Parameters.ParamByName('@CodigoPersona').value := ObtenerCliente.FieldByName('CodigoCliente').Value;
			Parameters.ParamByName('@Apellido').value := ObtenerCliente.FieldByName('Apellido').Value;
            Parameters.ParamByName('@ApellidoMaterno').value := ObtenerCliente.FieldByName('ApellidoMaterno').Value;
			Parameters.ParamByName('@Nombre').value := ObtenerCliente.FieldByName('Nombre').Value;
			Parameters.ParamByName('@TipoDocumento').Value := ObtenerCliente.FieldByName('CodigoDocumento').Value;
			Parameters.ParamByName('@NumeroDocumento').value := ObtenerCliente.FieldByName('NumeroDocumento').Value;
			Parameters.ParamByName('@Personeria').value := ObtenerCliente.FieldByName('Personeria').Value;
			Parameters.ParamByName('@Sexo').value := ObtenerCliente.FieldByName('Sexo').Value;

            //Domicilio Facturacion
            Parameters.ParamByName('@Calle').value := ObtenerDomicilioConvenio.FieldByName('DescriCalle').Value;
            Parameters.ParamByName('@numero').value := ObtenerDomicilioConvenio.FieldByName('Numero').Value;
            Parameters.ParamByName('@detalleDomicilio').value := ObtenerDomicilioConvenio.FieldByName('Detalle').Value;
            Parameters.ParamByName('@CodigoPostal').value := ObtenerDomicilioConvenio.FieldByName('CodigoPostal').Value;
            Parameters.ParamByName('@CodigoComuna').Value := ObtenerDomicilioConvenio.FieldByName('CodigoComuna').Value;
            Parameters.ParamByName('@CodigoRegion').Value := ObtenerDomicilioConvenio.FieldByName('CodigoRegion').Value;

            Parameters.ParamByName('@CodigoTipoMedioPago').Value := FMedioPago;

			// Parametros de la forma de pago
            if (FMedioPago = TPA_PAT) then begin
                Parameters.ParamByName('@CodigoTipoTarjeta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoTipoTarjetaCredito').AsInteger;
                Parameters.ParamByName('@NumeroTarjetaCredito').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('NumeroTarjetaCredito').AsString;
                Parameters.ParamByName('@FechaVencimientoTarjeta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('FechaVencimiento').AsString;
                Parameters.ParamByName('@CodigoEmisorTarjeta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoEmisorTarjetaCredito').AsInteger;
            end
            else begin
                if (FMedioPago = TPA_PAC) then begin
                    Parameters.ParamByName('@CodigoBanco').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoBanco').AsInteger;
                    Parameters.ParamByName('@CodigoTipoCuenta').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('CodigoTipoCuentaBancaria').AsInteger;
                    Parameters.ParamByName('@NumeroCuentaBancaria').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('NroCuentaBancaria').AsString;
                    Parameters.ParamByName('@Sucursal').Value := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('Sucursal').AsString;
                end
                else begin
                    txtMedioPago.Caption := TPA_NINGUNO_DESC;
                end;
            end;

			ExecProc;
            FNumeroComprobante := Parameters.ParamByName('@NumeroComprobante').Value;
        end;
	end;

	Procedure ExecActualizarTotalLoteFacturacion(CodigoConvenio, IndiceVehiculo: Integer);
	begin
		with ActualizarTotalLoteFacturacion do begin
			parameters.paramByname('@CodigoConvenio').Value := CodigoConvenio;
//			parameters.paramByname('@IndiceVehiculo').Value := IndiceVehiculo;
			parameters.paramByname('@LoteFacturacion').value := LoteFacturacion;
			ExecProc;
		end;
	end;
resourcestring
    MSG_GENERAR_FACTURA  = 'No se pudo generar la Factura.';

Var
	CodigoConvenio: LongInt;
    IndiceVehiculoAnterior: integer;
begin
	// Con los datos del cliente generamos el nuevo comprobante
	// Luego recorremos los lotes generados e insertamos a cada uno
	// de ellos con cada uno de sus respectivos movimientos de cuenta
	// Finalmente generamos los items de comprobantes correspondientes

	DMConnections.BaseCAC.BeginTrans;
	try
		// Generamos el proceso de facturacion y la relacion con el grupo correspondiente
      CrearProcesoFacturacion.Parameters.ParamByName('@Operador').value := usuarioSistema;
		CrearProcesoFacturacion.ExecProc;
		ProcesoFacturacion := CrearProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value;

(*     GrupoFacturacion := QueryGetValueInt(DMConnections.BaseCAC,
          'select codigoGrupoFacturacion from cuentas where cuentas.codigocliente = ' +
          intToStr(FCodigoCliente));
        if GrupoFacturacion > 0 then begin
            CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@NumeroProcesoFacturacion').value :=
              ProcesoFacturacion;
            CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@CodigoGrupoFacturacion').value :=
              GrupoFacturacion;
            CrearProcesoFacturacionGrupoFacturacion.execPROC;
        end;
  *)

        CodigoConvenio := IVal(StrRight(cbConveniosCliente.Items[cbConveniosCliente.ItemIndex], 20));
        IndiceVehiculoAnterior := -1;

        ObtenerDomicilioConvenio.Close;
        ObtenerDomicilioConvenio.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        ObtenerDomicilioConvenio.Open;

		// Generamos el comprobante
		InsertarComprobante;
    	CantidadCuentas := 0;
        j := 0;
//Pablo        PrimerMovimiento := true;
        importeTotal := 0;
        while j < ListBox.Items.Count do begin
            if (TItemFactura(Pointer(ListBox.Items.Objects[j])^).CodigoConvenio = CodigoConvenio) then begin
               inc(CantidadCuentas);
                if IndiceVehiculoAnterior <> TItemFactura(Pointer(ListBox.Items.Objects[j])^).IndiceVehiculo then begin
                    InsertarLote;
                end;
                IndiceVehiculoAnterior := TItemFactura(Pointer(ListBox.Items.Objects[j])^).IndiceVehiculo;
                // Despues de crear el lote si el movimiento no esta creado se crea
                // Si esta creado se asigna el numero de lote al mismo
                if TItemFactura(Pointer(ListBox.Items.Objects[j])^).NumeroMovimiento <> -1 then
                    updateMovimientoCuenta
                else begin
                    insertarMovimientoCuenta;
                end;
                importeTotal := importeTotal + TItemFactura(Pointer(ListBox.Items.Objects[j])^).Importe;
                ExecActualizarTotalLoteFacturacion(CodigoConvenio, TItemFactura(Pointer(ListBox.Items.Objects[j])^).IndiceVehiculo);
            end;
            inc(j);
        end;

		// Actualizamos el proceso de facturacion
		ActualizarProcesoFacturacion.Parameters.ParamByName('CantidadCuentas').value := CantidadCuentas;
		ActualizarProcesoFacturacion.Parameters.ParamByName('NumeroProcesoFacturacion').value := ProcesoFacturacion;
		ActualizarProcesoFacturacion.execSQL;

		DMConnections.BaseCAC.CommitTrans;
	except
		on e: exception do begin
			DMConnections.BaseCAC.RollbackTrans;
			MsgBoxErr(MSG_GENERAR_FACTURA, e.message, CAPTION_GENERAR_FACTURA, MB_ICONSTOP);
            Exit;
		end;
    end;

    // Imprimimos la factura
    Application.CreateForm(TFormReporteFactura, F);
    F.Execute(TC_FACTURA, trunc(FCodigoCliente), FNumeroComprobante, True, MSG_DOCUMENTO_DEMOSTRACION, FDiasVencimiento);
    F.free;
    try
        Application.CreateForm(TfrmDetalleViajes, FDetalle);
        for i := 0 to ListBox.Count - 1 do begin
            if TItemFactura(Pointer(ListBox.Items.Objects[i])^).CodigoConcepto = 2 then begin
                FDetalle.Execute(TC_FACTURA, TItemFactura(Pointer(ListBox.Items.Objects[i])^).CodigoConvenio,
                  FCodigoCliente,FNumeroComprobante, False, MSG_DETALLE_VIAJES);
            end;
        end;
        FNumeroComprobante := 0;

        cbConveniosClienteChange(cbConveniosCliente);
	except
		on e: exception do begin
			MsgBoxErr(MSG_GENERAR_FACTURA, e.message, CAPTION_GENERAR_FACTURA, MB_ICONSTOP);
		end;
    end;
    FDetalle.Free;
end;



{* Problemas:

            Cuando cargo genero los movimientos y los viajes
            (el proceso me los va facturar)

            Cuando selecciono genero los movimientos y los viajes
            (el proceso me los va a facturar)

            Puedo hacer que no se pueda facturar un cliente cuyo grupo
            se este facturando (Que pasa al reves, Tambien se puede no
            permitir comenzar el proceso si se esta facturando un cliente)

*}

procedure TfrmEmisionFactura.btnAgregarLineaClick(Sender: TObject);
var IndiceVehiculo, CodigoConcepto: LongINt;
    Observaciones: AnsiString;
    Patente : AnsiString;
    Importe: Double;
    item: PTItemFactura;
begin
    if InDatosMovimiento(IVal(StrRight(cbConveniosCliente.Items[cbConveniosCliente.ItemIndex], 20)),
                         IndiceVehiculo, CodigoConcepto, Observaciones, Importe, Patente) then begin
        new(item);
        item^.indexLista := -1;
        item^.CodigoConvenio := IVal(StrRight(cbConveniosCliente.Items[cbConveniosCliente.ItemIndex], 20));
        item^.IndiceVehiculo := IndiceVehiculo;
        item^.Importe := Importe;
        item^.NumeroMovimiento := -1;
        item^.CodigoConcepto := CodigoConcepto;
        item^.Observaciones := Observaciones;
        item^.VersionPromocion := -1;
        item^.NumeroPromocion := -1;
        item^.NumeroFinanciamiento := -1;
        item^.CodigoConcesionaria := -1;

//                                       '%-15.15s %-10.10s  %-38.38s %3d %8d $ %12.12s'
//        ListBox.Items.AddObject(format('%-15.15s %-10.10s  %-38.38s %3d %9s   %12.12s',
        ListBox.Items.AddObject(format('%-15.15s %-10.10s  %-38.38s %9s   %12.12s',
                                      ['',
                                       formatDateTime(ShortDateFormat,NowBase(DMConnections.BaseCAC)),
                                       trim(Observaciones),
//                                       0,
                                       Patente, //IVal(StrRight(cbConveniosCliente.Items[cbConveniosCliente.ItemIndex], 20)),
									   FormatFloat(FORMATO_IMPORTE,Importe)]),TObject(item));
        FTotal := FTotal + item^.importe;
		lImporteTotal.Caption := format('%12.12s',[formatFloat(FORMATO_IMPORTE,FTotal)]);
        btnFacturar.Enabled := (ListBox.Items.Count > 0) and (FTotal > 0);
        btnImprimir.Enabled := btnFacturar.Enabled;
        ListBoxClick(ListBox);
    end;
end;

procedure TfrmEmisionFactura.inicializaControles;
begin
    gbMovDisponibles.Enabled := false;
    gbMovSeleccionados.Enabled := false;
    btnFacturar.Enabled := false;
    btnImprimir.Enabled := false;
    btnSeleccionarTodo.Enabled := false;
    btnInvertirSeleccion.Enabled := false;
    btnAgregarLinea.Enabled := false;
    btnEliminarLinea.Enabled := false;
    txtMedioPago.Caption := '';
end;

procedure TfrmEmisionFactura.btnEliminarLineaClick(Sender: TObject);
begin
// IndexLista = -1 significa que se agrego manualmente
    if TItemFactura(Pointer(ListBox.Items.Objects[ListBox.ItemIndex])^).IndexLista = -1 then begin
        FTotal := FTotal - PTItemFactura(ListBox.items.Objects[ListBox.ItemIndex])^.importe;
		lImporteTotal.Caption := format('%12.12s',[formatFloat(FORMATO_IMPORTE,FTotal)]);
        dispose(Pointer(ListBox.Items.Objects[ListBox.ItemIndex]));
        ListBox.DeleteSelected;
    end;
        btnFacturar.Enabled := (ListBox.Items.Count > 0) and (FTotal > 0);
        btnImprimir.Enabled := btnFacturar.Enabled;
    ListBoxClick(ListBox);
end;

procedure TfrmEmisionFactura.ListBoxClick(Sender: TObject);
begin
    btnEliminarLinea.Enabled := false;
    if ListBox.ItemIndex <> -1 then
        btnEliminarLinea.Enabled := TItemFactura(Pointer(ListBox.Items.Objects[ListBox.ItemIndex])^).IndexLista = -1;
end;

procedure TfrmEmisionFactura.btnImprimirClick(Sender: TObject);
resourcestring
    MSG_TARJETA_CREDITO = 'Factura debitada a la Tarjeta de Credito %s';
    MSG_CUENTA_BANCARIA = 'Factura debitada a la Cuenta Bancaria %s';

var F:TFormReporteInformeFactura;
    i: Integer;
begin
    Application.createForm(TFormReporteInformeFactura, F);
// Mostramos cada linea de factura
    F.ItemsComprobante.CreateDataSet;
    for i := 0 to ListBox.Items.Count - 1 do begin
        F.ItemsComprobante.append;
        F.ItemsComprobante.FieldByName('Descripcion').asString := TItemFactura(Pointer(ListBox.Items.Objects[i])^).Observaciones;
        F.ItemsComprobante.FieldByName('Importe').asFloat := TItemFactura(Pointer(ListBox.Items.Objects[i])^).Importe;
        F.ItemsComprobante.post;
    end;
(*Pablo
// Mostramos la cuenta de debito a la que se deberia debitar
    if (StrRight(cbConveniosCliente.Items[cbConveniosCliente.ItemIndex], 2) = TP_POSPAGO_MANUAL) then begin
        F.lCuentaDebito.Caption := MSG_FACTURA_PAGO_MANUAL;
    end else begin
        ObtenerDebitoAutomatico.Parameters.ParamByName('CodigoCliente').Value := FCodigoCliente;
        ObtenerDebitoAutomatico.Parameters.ParamByName('CodigoDebitoAutomatico').Value := IVal(StrRight(cbConveniosCliente.Items[cbConveniosCliente.ItemIndex],20));
        ObtenerDebitoAutomatico.open;
       if ObtenerDebitoAutomatico.FieldByName('TipoDebito').asString = DA_TARJETA_CREDITO then
            F.lCuentaDebito.Caption := format(MSG_TARJETA_CREDITO, [cbConveniosCliente.ItemsEx[cbConveniosMedioPago.ItemIndex].caption])
        else
            F.lCuentaDebito.Caption := format(MSG_CUENTA_BANCARIA, [cbConveniosCliente.ItemsEx[cbConveniosMedioPago.ItemIndex].caption]);

        ObtenerDebitoAutomatico.Close;
    end;
*)
    F.lFechaVencimiento.Caption := formatDateTime(shortDateFormat,NowBase(DMConnections.BaseCAC) + FDiasVencimiento);
    F.Execute(trunc(FCodigoCliente), Trim(StrLeft(cbConveniosCliente.Text, 50)), txtMedioPago.Caption, true);
    F.free;
end;

procedure TfrmEmisionFactura.cbConveniosClienteChange(Sender: TObject);
var i: Integer;
    item : PTItemFactura;
begin
    inicializaControles;
    LimpiarListas;
    lVencimiento.Caption := FormatDateTime(shortDateFormat,NowBase(DMConnections.BaseCAC) + FDiasVencimiento);

    ObtenerDatosMedioPagoAutomaticoConvenio.close;
    ObtenerDatosMedioPagoAutomaticoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := strtoint(trim(StrRight(cbConveniosCliente.Text, 20)));
    ObtenerDatosMedioPagoAutomaticoConvenio.Open;

    FMedioPago := ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger;

    if (FMedioPago = TPA_PAT) then
        txtMedioPago.Caption := TPA_PAT_DESC
    else
        if (FMedioPago = TPA_PAC) then
            txtMedioPago.Caption := TPA_PAC_DESC
        else
            txtMedioPago.Caption := TPA_NINGUNO_DESC;

    with obtenerInformeFactura do begin
        Close;
        Parameters.ParamByName('@CodigoConvenio').value := strtoint(trim(StrRight(cbConveniosCliente.Text, 20)));
        Parameters.ParamByName('@FechaHora').value := deFechaHora.date + 1;
        Parameters.ParamByName('@CantidadLineas').value := 40;
        open;
   // Clear ListBox and CheckListBox
        LimpiarListas;
        lImporteTotal.Caption := '';
        i := 0;
        while not eof do begin
            new(item);
            item^.indexLista := i;
            item^.CodigoConvenio := FieldByName('CodigoConvenio').asInteger;
            item^.IndiceVehiculo := FieldByName('IndiceVehiculo').asInteger;
            item^.Importe := FieldByName('Importe').asFloat;
            item^.NumeroMovimiento := FieldByName('NumeroMovimiento').value;
            item^.CodigoConcesionaria := FieldByName('CodigoConcesionaria').AsInteger;
            item^.CodigoConcepto := FieldByName('CodigoConcepto').asInteger;
            item^.Observaciones := FieldByName('Observaciones').asString;
            if item^.NumeroMovimiento = -1 then begin
                item^.VersionPromocion := -1;
                item^.NumeroPromocion := -1;
                item^.NumeroFinanciamiento := -1;
            end;
//            CheckListBox.Items.AddObject(format('%-15.15s %-10.10s  %-38.38s %3d  ' + Replicate(' ',9) + '  %12.12s',
            CheckListBox.Items.AddObject(format('%-15.15s %-10.10s  %-38.38s  ' + Replicate(' ',9) + '  %12.12s',
                                                [FieldByName('Concesionaria').AsString,
                                                formatDateTime(ShortDateFormat,FieldByName('FechaHora').asDateTime),
                                                trim(FieldByName('Observaciones').asString),
//                                                FieldByName('CantidadViajes').asInteger,
                                                //FieldByName('CodigoConvenio').AsInteger,
												formatFloat(FORMATO_IMPORTE,FieldByName('Importe').asFloat)]),TObject(item));
            next;
            inc(i);
        end;
    end;
    gbMovDisponibles.Enabled := CheckListBox.Items.Count > 0;
    btnSeleccionarTodo.Enabled := gbMovDisponibles.Enabled;
    btnInvertirSeleccion.Enabled := gbMovDisponibles.Enabled;
    gbMovSeleccionados.Enabled := true;
    btnAgregarLinea.Enabled := gbMovSeleccionados.Enabled;
end;

procedure TfrmEmisionFactura.peCodigoClienteButtonClick(Sender: TObject);
var f: TFormBuscaClientes;
begin
    application.createForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peCodigoCliente.Text :=  F.Persona.NumeroDocumento;
            peCodigoCliente.setFocus;
        end;
    end;
    F.free;
end;

procedure TfrmEmisionFactura.cbConveniosMedioPagoChange(Sender: TObject);
resourcestring
    MSG_TODAS_CUENTAS = 'Todas las Cuentas';
begin
    if TComboBoxEx(Sender).ItemIndex <> FIndexCuentaDebito then begin
        LimpiarListas;
        if (StrRight(TComboBoxEx(Sender).Items[TComboBoxEx(Sender).ItemIndex], 2) = TP_POSPAGO_MANUAL) then
            CargarCuentas(DMConnections.BaseCAC, cbConveniosCliente, FCodigoCliente,TP_POSPAGO_MANUAL, 0, 0)
        else
            CargarCuentas(DMConnections.BaseCAC, cbConveniosCliente, FCodigoCliente, TP_DEBITO_AUTOMATICO,
                          IVal(StrRight(TComboBoxEx(Sender).Items[TComboBoxEx(Sender).ItemIndex], 20)), 0);
        cbConveniosCliente.Items.insert(0, MSG_TODAS_CUENTAS);
        cbConveniosCliente.Enabled := cbConveniosCliente.Items.Count > 0;
        if cbConveniosCliente.Enabled then begin
            cbConveniosCliente.ItemIndex := 0;
            cbConveniosClienteChange(cbConveniosCliente);
        end;
    end;
end;



procedure TfrmEmisionFactura.peCodigoClienteChange(Sender: TObject);
resourcestring
    MSG_CUENTAS_PAGO_MANUAL = 'Cuentas de Pago Manual';
begin
    if (activeControl = sender) then begin
        cbConveniosCliente.Items.clear;
        cbConveniosCliente.Enabled := false;
        LimpiarListas;
        InicializaControles;

        if (length((Sender as TPickEdit).Text) < 7) then begin
            FCodigoCliente := -1;
            FMedioPago := -1;
            FApellido := '';
            FApellidoMaterno := '';
            FNombre := '';
            lApellidoNombre.Caption := '';
            txtMedioPago.Caption := '';
            lDomicilio.Caption := '';
            Exit;
        end;

        with obtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value := NULL;
            Parameters.ParamByName('@CodigoDocumento').value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').value := PADL(Trim((Sender as TPickEdit).Text), 9, '0');
            open;
        end;

        if (not obtenerCliente.Eof) and (obtenerCliente.RecordCount = 1) then
            FCodigoCliente := obtenerCliente.fieldbyname('CodigoCliente').AsInteger
        else begin
            FCodigoCliente := -1;
            FMedioPago := -1;
            FApellido := '';
            FApellidoMaterno := '';
            FNombre := '';
            lApellidoNombre.Caption := '';
            txtMedioPago.Caption := '';
        end;

        With qryConvenios do begin
            Parameters.ParamByName('CodigoCliente').Value := FCodigoCliente;
            open;
            First;
            while not qryConvenios.Eof do begin
                cbConveniosCliente.Items.Add(qryConvenios.fieldbyname('NumeroConvenio').AsString  + Space(200) + Trim(qryConvenios.fieldbyname('CodigoConvenio').AsString));
                qryConvenios.Next;
            end;
            close
        end;

        cbConveniosCliente.ItemIndex := 0;
        cbConveniosCliente.Enabled := cbConveniosCliente.Items.Count > 0;

        if cbConveniosCliente.Enabled then begin
            cbConveniosClienteChange(cbConveniosCliente);
        end;

        with ObtenerCliente do
        begin
            FApellido := Trim(FieldByName('Apellido').asString);
            FApellidoMaterno := Trim(FieldByName('ApellidoMaterno').asString);
            FNombre := Trim(FieldByName('Nombre').asString);

            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lApellidoNombre.Caption :=  ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre)
            else
                lApellidoNombre.Caption :=  ArmaRazonSocial(FApellido, FNombre);

            //Muestro el domicilio, si tiene el de documentos, sino el personal y sino ninguno

            if not (FieldByName('CodigoDomicilio').IsNull) then
               lDomicilio.Caption := ArmarDomicilioCompletoPorCodigo(ObtenerCliente.Connection,
                                        FieldByName('CodigoCliente').AsInteger,FieldByName('CodigoDomicilio').AsInteger)
            else lDomicilio.Caption := MSG_NO_ENCONTRO_DOMICILIO;
        end;
    end;
end;

procedure TfrmEmisionFactura.deFechaHoraChange(Sender: TObject);
begin
	if activeControl = sender then begin
		try
			DateToStr((Sender as TDateEdit).Date);
			if (Sender as TDateEdit).Date > NowBase(DMConnections.BaseCAC) then begin
				msgBox(MSG_FECHA_INCORRECTA, CAPTION_VALIDAR_FECHA, MB_OK);
				(Sender as TDateEdit).Date := FFechaLimite;
			end;
			cbConveniosClienteChange(cbConveniosCliente);
		except;
		end;
	end;
end;

procedure TfrmEmisionFactura.deFechaHoraExit(Sender: TObject);
begin
	if (Sender as TDateEdit).Date > NowBase(DMConnections.BaseCAC) then begin
		msgBox(MSG_FECHA_INCORRECTA, CAPTION_GENERAR_FACTURA, MB_OK);
		(Sender as TDateEdit).Date := FFechaLimite;
		(Sender as TDateEdit).setfocus;
	end;
end;

procedure TfrmEmisionFactura.deFechaHoraEnter(Sender: TObject);
begin
	FFechaLimite := (Sender as TDateEdit).date;
end;

end.
