object NuevoConvenioListaAmarillaForm: TNuevoConvenioListaAmarillaForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Nuevo Convenio'
  ClientHeight = 152
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 24
    Height = 13
    Caption = 'RUT:'
  end
  object Label3: TLabel
    Left = 24
    Top = 48
    Width = 107
    Height = 13
    Caption = 'Convenios del Cliente:'
  end
  object lblConcesionaria: TLabel
    Left = 24
    Top = 76
    Width = 71
    Height = 13
    Caption = 'Concesionaria:'
  end
  object peRUTCliente: TPickEdit
    Left = 150
    Top = 21
    Width = 134
    Height = 21
    CharCase = ecUpperCase
    Enabled = True
    TabOrder = 0
    OnChange = peRUTClienteChange
    OnKeyPress = peRUTClienteKeyPress
    EditorStyle = bteTextEdit
    OnButtonClick = peRUTClienteButtonClick
  end
  object vcbConveniosCliente: TVariantComboBox
    Left = 150
    Top = 48
    Width = 195
    Height = 19
    Style = vcsOwnerDrawFixed
    ItemHeight = 13
    TabOrder = 2
    OnChange = vcbConveniosClienteChange
    OnDrawItem = vcbConveniosClienteDrawItem
    Items = <>
  end
  object btnBuscar: TButton
    Left = 290
    Top = 17
    Width = 55
    Height = 25
    Caption = 'Buscar'
    Default = True
    TabOrder = 1
    OnClick = btnBuscarClick
  end
  object btnAceptar: TButton
    Left = 189
    Top = 111
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 4
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 270
    Top = 111
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 5
    OnClick = btnCancelarClick
  end
  object vcbConcesionarias: TVariantComboBox
    Left = 150
    Top = 73
    Width = 195
    Height = 19
    Style = vcsOwnerDrawFixed
    ItemHeight = 13
    TabOrder = 3
    Items = <>
  end
  object spObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 16
    Top = 112
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 112
  end
  object spAgregarConvenioAListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarConvenioAListaAmarilla'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@MensajeError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 80
    Top = 112
  end
  object spReglaValidacionListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ReglaValidacionListaAmarilla'
    Parameters = <>
    Left = 113
    Top = 112
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 144
    Top = 112
  end
end
