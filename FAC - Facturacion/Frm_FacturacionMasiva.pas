{********************************** File Header ********************************
File Name : Frm_FacturacionMasiva.pas
Author : jconcheyro
Date Created: 21/04/2006
Language : ES-AR
Description :

Revision 1: 21/04/2006 se eliminaron las llamadas a EliminarLotesSinComprobantes
ya que si estaba corriendo un proceso masivo y se lanzaba uno manual, arruinaba
los comprobantes del masivo

    Revision : 2
        Author : rharris
        Date : 24/01/2009
        Description : (Ref.:SS 627)
                Se modifica el proceso de facturacion para que:
                    1.- utilice tablas de trabajo para el calculo de intereses
                    2.- calculo los consumos de los movimientos prefacturados (masivamente)

    Revision : 3
        Author : rharris
        Date : 03/03/2009
        Description : (Ref.: SS 784 Facturaci�n Electronica)
                Se modifica el proceso de facturaci�n para que genero los MC por concepto
                de Cuotas Arriendo antes de comenzar con la pre-facturaci�n.


	Revision 4
    Author: mbecerra
    Date: 29-Enero-2010
    Description:		(Ref Fase II)
            	Se desea que el proceso de Tipificaci�n se inicie de manera
                desasistida una vez finalizada la pre-facturaci�n.
                
Firma       : SS_1086_NDR_20130306
Description : Se usa la funcion QueryGetDateTimeValue de SysUtilsCN para llamar a dbo.ObtenerUltimaFechaEmisionNotasCobro()
                
    Author      :   CQuezadaI
    Date        :   09-01-2013
    Firma       :   SS_660_CQU_20121010
    Description :   Se agerga proceso de facturaci�n de infracciones por morosidad,
                    Se agrega llamada a SysUtilsCN
                    Se agrega overload a la funci�n ContarConvenios,
                    ahora se cuenta por convenios en LA o no.
                    Agrega la variable UltimaFechaEmision para obtener la ultima fecha de emision de NK de manera gen�rica
                    Se afinan detalles en la barra de progreso, textos, etc.
                    Se agrega sobre carga del m�todo ContarConvenios para diferenciar
                    si se cuentan los convenios en lista amarilla o los normales.

    Date        :   05-06-2013
    Firma       :   SS_660_CQU_20130604
    Description :   Se agergan correcciones indicadas por QA-CN

    Author      :   CQuezadaI
    Date        :   27-11-2013
    Firma       :   SS_660_CQU_20131127
    Description :   Ajustes para optimizar la Facturaci�n
                    -   Se agrega FCuotasGeneradas para indicar si la generaci�n de Cuotas de Arriendo ya se ejecut�
                    -   Se agrega FInteresesGenerados para indicar si la generaci�n de Intereses ya se ejecut�, en este caso
                        se deber�a actualizar el campo CodigoGrupoFacturacion en la tabla tmp_ConveniosConInteresesPorFacturar
                        para que no se generen nuevamente y los pueda utilizar el proceso normal de facturaci�n. Para ello
                        se cre� una funci�n llamada ActualizarDatosIntereses que realiza dicha acci�n.

Etiqueta    :   TASK_086_MGO_20161121
Descripci�n :   Se quita facturaci�n de morosos (infractores en lista amarilla) y se a�ade Pre-Facturaci�n

Etiqueta    :   TASK_090_MGO_20161213
Descripci�n :   Se agrega ventana espec�fica de par�metros de facturaci�n

Etiqueta    :   TASK_097_MGO_20161221
Descripci�n :   Se mejoran mensajes de error

Etiqueta	:	TASK_102_MGO_20161227
Descripci�n	:	Se agrega validaci�n de par�metros de facturaci�n
*******************************************************************************}
unit Frm_FacturacionMasiva;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, Db, DBTables, Util, UtilDB, PeaProcs, UtilProc,
  Validate, DateEdit, ThreadFact, DmiCtrls, ADODB, DMConnection, Variants,
  PeaTypes, DPSControls, RStrings, jpeg, ListBoxEx, DBListEx, ImgList,
  frmReporteFacturacion, ParamGen, ConstParametrosGenerales, ShellAPI,
  frmTipificacionMasiva, ComprobantesEmitidosPrefac, ParametrosProcesoFacturacion,  // TASK_090_MGO_20161213
  ThreadFactMorosos, SysUtilsCN;

type
  TFormFacturacionMasiva = class(TForm)
    PageControl: TPageControl;
    tab_Inicio: TTabSheet;
	Panel1: TPanel;
	Bevel2: TBevel;
    Bevel3: TBevel;
    Label4: TLabel;
	Label5: TLabel;
    tmrBotones: TTimer;
    tab_FacturarFin: TTabSheet;
    Bevel5: TBevel;
    Label12: TLabel;
    GroupBox2: TGroupBox;
	pb_facturar: TProgressBar;
    Label14: TLabel;
	Label15: TLabel;
    lbl_tiempofacttrans: TLabel;
    lbl_tiempofactrest: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    lbl_conveniosproc: TLabel;
    Label8: TLabel;
    spCrearProcesoFacturacion: TADOStoredProc;
    lFacturando: TLabel;
    Image3: TImage;
	Image4: TImage;
	ListaCiclos: TDBListEx;
	ObtenerCiclosAFacturar: TADOStoredProc;
    dsCiclos: TDataSource;
    lnCheck: TImageList;
    btn_siguiente: TButton;
    btn_cancelar: TButton;
    btn_anterior: TButton;
    btnImprimirReporte: TButton;
    lblParametros: TLabel;
    lblGoToParametros: TLabel;
    spGenerarTablaTrabajoInteresesPorFacturar: TADOStoredProc;
    spLlenarDetalleConsumoComprobantes: TADOStoredProc;
    spGenerarMovimientosCuotasArriendo: TADOStoredProc;
    chkTipificacion: TCheckBox;
    chkPreFacturar: TCheckBox;
    spCrearConveniosInfractoresPendientes: TADOStoredProc;
    spValorizarInfraccionesPendientes: TADOStoredProc;       							// TASK_086_MGO_20161121
	procedure tmrBotonesTimer(Sender: TObject);
    procedure btn_siguienteClick(Sender: TObject);
    procedure btn_anteriorClick(Sender: TObject);
	procedure btn_cancelarClick(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure FormCreate(Sender: TObject);
    procedure ListaCiclosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure ListaCiclosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnImprimirReporteClick(Sender: TObject);
    procedure lblGoToParametrosClick(Sender: TObject);
    procedure chkPreFacturarClick(Sender: TObject);                             // TASK_086_MGO_20161121
    procedure chkPreFacturarKeyPress(Sender: TObject; var Key: Char);           // TASK_086_MGO_20161121
    procedure HabilitarCheckPreFacturar;                                        // TASK_086_MGO_20161121
  private
	{ Private declarations }
	FInicio: Int64;
	FAntHecho: Integer;
	FCancelar: Boolean;
	FErrorFacturacion: Boolean;
	FFacturando: Boolean;
    FPreFacturacion: Boolean;                                                   // TASK_086_MGO_20161121
    //FFacturandoMorosos : Boolean;                                             // TASK_086_MGO_20161121
    //FErrorFacturacionMorosos : Boolean;                                       // TASK_086_MGO_20161121
    FCodigoGrupoFacturacion: Integer;                                           // TASK_102_MGO_20161227
	FCiclosElegidos: set of byte;
    FInsumido: TDateTime;
	FConveniosProcesados, FTotalConvenios, FNumeroProcesoFacturacion, FThreadsCorriendo: Integer;
    FUltimaFechaEmision : TDateTime;                                            
    //FNumeroProcesoFacturacionMorosos, FNumeroProcesoFacturacionNormal : Integer;// TASK_086_MGO_20161121
    //FConveniosProcesadosMorosos : Integer;                                      // TASK_086_MGO_20161121
    FCuotasGeneradas : Boolean;                                                 // TASK_086_MGO_20161121
    //FInteresesGenerados : Boolean;                                            // TASK_086_MGO_20161121
	procedure RevisarEstadoFacturacion;
	procedure Facturar;
	procedure MostrarProgreso;                                    
  	Function ContarConvenios: Integer;   						                         
    //Function ContarConvenios(ListaAmarilla : Boolean) : Integer; overload;    // TASK_086_MGO_20161121
	Function CrearProcesoFacturacion: Integer;                                  // TASK_086_MGO_20161121
    //Function CrearProcesoFacturacion(ProcesoMorosos : Boolean) : Integer;     // TASK_086_MGO_20161121
	Function StringGruposFacturacion: AnsiString;
    procedure MostrarReporte(Tiempo: TDateTime);
    function CargarDatosIntereses: Integer;
    //function ActualizarDatosIntereses : Integer;                              // TASK_086_MGO_20161121
    procedure LlenarDetalleConsumoComprobantes;
    function GenerarMovimientosCuotasArriendo: Integer;
    //procedure RevisarEstadoFacturacionMorosos;                                // TASK_086_MGO_20161121
    //procedure FacturarMorosos;                                                // TASK_086_MGO_20161121
    //procedure LlenarDetalleConsumoComprobantesMorosos;                        // TASK_086_MGO_20161121
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;

var
  FormFacturacionMasiva: TFormFacturacionMasiva;


implementation

resourcestring
	CAPTION_CANCELAR_FACTURACION = 'Cancelar Proceso de Facturaci�n';
	CAPTION_CANCELAR_IMPRESION   = 'Cancelar Proceso de Impresi�n';
    // INICIO : TASK_086_MGO_20161121
    STR_DESCRIPTION_FACTURACION = 'El sistema ya est� listo para congelar los tr�nsitos de los grupos de clientes seleccionados y confeccionar sus facturas. Presione el bot�n de "Comenzar" para comenzar el proceso, o bien el de "Anterior" si desea modificar alg�n par�metro.';
    STR_DESCRIPTION_PREFACTURACION = 'El sistea ya est� listo para generar una simulaci�n de facturaci�n, en la cual podr� excluir o incluir convenios a facturaci�n. Si en cambio desea procesar una facturaci�n real, deseleccione la opci�n "Pre-Facturar".';
    // FIN : TASK_086_MGO_20161121

{$R *.DFM}

function TFormFacturacionMasiva.Inicializar: Boolean;
Var
	i: Integer;
begin
	FCancelar  			:= False;
	FErrorFacturacion	:= False;
    //FErrorFacturacionMorosos := False;                                        // TASK_086_MGO_20161121
	FAntHecho  			:= 0;
	FFacturando		    := False;
    //FFacturandoMorosos  := False;                                             // TASK_086_MGO_20161121
	FCiclosElegidos     := [];
	PageControl.ActivePage := tab_Inicio;

    chkPreFacturar.Checked := True;                                             // TASK_086_MGO_20161121
    FPreFacturacion := True;                                                    // TASK_086_MGO_20161121

    // Si no tiene derechos de acceso para Cambiar los Par�metros,
    // hace invisibles los Labels
	lblParametros.Visible := ExisteAcceso('CAMBIAR_PARAMETROS_FACTURACION');
	lblGoToParametros.Visible := lblParametros.Visible;

	// Cargamos los ciclos y los marcamos a todos para facturar
	Result				:= OpenTables([ObtenerCiclosAFacturar]);
    if Result then begin
        //for i := 1 to ObtenerCiclosAFacturar.RecordCount do                   // TASK_086_MGO_20161121
        //    Include(FCiclosElegidos, i);                                      // TASK_086_MGO_20161121
        tmrBotonesTimer(tmrBotones);
        // Obtiene la �ltima Fecha de Emisi�n                                                                                               // SS_660_CQU_20121010
        FUltimaFechaEmision := SysUtilsCN.QueryGetDateTimeValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerUltimaFechaEmisionNotasCobro()'); // SS_660_CQU_20121010
    end;
end;

procedure TFormFacturacionMasiva.tmrBotonesTimer(Sender: TObject);
resourcestring
    CAPTION_SIGUIENTE = '&Siguiente >';
    CAPTION_COMENZAR = '&Comenzar';
begin
	tmrBotones.Enabled := False;
	if PageControl.ActivePage = tab_Inicio then begin
		btn_siguiente.caption := CAPTION_SIGUIENTE;
		btn_siguiente.enabled := (FCiclosElegidos <> []);
		btn_anterior.enabled := False;
		btn_cancelar.enabled := True;
	end else if PageControl.ActivePage = tab_FacturarFin then begin
		btn_siguiente.caption := CAPTION_COMENZAR;
        { INICIO : TASK_086_MGO_20161121
		if FFacturandoMorosos then begin            
            btn_siguiente.enabled := False;         
            btn_anterior.enabled := False;          
        end;                                        
        if FFacturando then begin                   
            btn_siguiente.enabled := False;         
            btn_anterior.enabled := False;          
        end;                                        
        if not(FFacturando) and not(FFacturandoMorosos) then begin
            btn_siguiente.enabled := True;          
            btn_anterior.enabled := True;           
        end;                                        
        }
        if FFacturando then begin                   
            btn_siguiente.enabled := False;
            btn_anterior.enabled := False;
            chkPreFacturar.Enabled := False;
        end else begin
            btn_siguiente.enabled := True;
            btn_anterior.enabled := True;
            chkPreFacturar.Enabled := True;
        end;
        // FIN : TASK_086_MGO_20161121
		btn_cancelar.enabled := not FCancelar;
	end;
    //if FFacturandoMorosos then RevisarEstadoFacturacionMorosos;               // TASK_086_MGO_20161121
	if FFActurando then RevisarEstadoFacturacion;
    tmrBotones.Enabled := NOT btnImprimirReporte.Enabled;
    pb_facturar.Update;
end;

procedure TFormFacturacionMasiva.btn_siguienteClick(Sender: TObject);
begin
	if PageControl.ActivePage = tab_Inicio then begin
		PageControl.ActivePage := tab_FacturarFin;
	end else if PageControl.ActivePage = tab_FacturarFin then begin
        //FacturarMorosos;    // TASK_086_MGO_20161121
		Facturar;         		// TASK_086_MGO_20161121
	end;
	tmrBotonesTimer(tmrBotones);
end;

procedure TFormFacturacionMasiva.btn_anteriorClick(Sender: TObject);
begin
	if PageControl.ActivePage = tab_FacturarFin then begin
		PageControl.ActivePage := tab_Inicio
	end;
	tmrBotonesTimer(tmrBotones);
end;

procedure TFormFacturacionMasiva.Facturar;
resourcestring
    CAPTION_PREPARANDO 					= 'Preparando la facturaci�n...';
    CAPTION_FACTURANDO 					= 'Facturaci�n en Progreso';
    MSG_NO_HAY_CONVENIOS_PARA_FACTURAR 	= 'No hay convenios para facturar.';
    MSG_FECHA_EMISION_ANTERIOR_A_ULTIMA	= 'La fecha de emisi�n no puede ser anterior a la fecha de emisi�n '#10#13 +
    										'de la �ltima Nota de Cobro Emitida ';
    MSG_FECHA_EMISION_GRUPO_FECHA 		= 'Fecha de Emisi�n del Grupo %d :';
    MSG_CONVENIOS_FACTURADOS            = 'Facturaci�n Morosos: %d convenios procesados'#10#13 +
                                          'Facturaci�n Normal: %d convenios procesados';
    MSG_CONFIRMACION                    = 'Se realizar� un proceso de facturaci�n definitivo. �Desea continuar?';	// TASK_086_MGO_20161121
    MSG_PARAMETROS_FACTURACION          = 'Los par�metros de Facturaci�n han cambiado desde la �ltima Pre-Facturaci�n del Grupo.'#10#13 +      // TASK_102_MGO_20161227
                                            'Realice una nueva Pre-Facturaci�n antes de continuar.';                                           // TASK_102_MGO_20161227
    MSG_INFRACTORES_CONFIRM             = 'Antes de facturar Infractores, el sistema valorizar� las Infracciones pendientes'#10#13 +   // TASK_142_MGO_20170220
                                            'y crear� los Convenios necesarios. �Desea continuar?';                                    // TASK_142_MGO_20170220
    MSG_ERROR_CONVENIOS                 = 'Ha ocurrido un error al crear Convenios Infractores.';                                      // TASK_142_MGO_20170220
    MSG_ERROR_INFRACCIONES              = 'Ha ocurrido un error al valorizar las Infracciones';                                        // TASK_142_MGO_20170220
var
    dUltimaFechaEmision         : TDateTime;
    CargaInteresesOK            : Integer;
    GenerarMovimientosCuotasOK  : Integer;
// INICIO : TASK_102_MGO_20161227
function ValidarParametrosFacturacion(CodigoGrupoFacturacion: Integer): Boolean;
    resourcestring
        QRY_VALIDAR_PARAMETROS_FACTURACION = 'SELECT dbo.ValidarParametrosFacturacion(%d)';
        MSG_ERROR = 'Error validando los par�metros de facturaci�n';
    begin
        try
            Result := QueryGetBooleanValue(DMConnections.BaseCAC,
                Format(QRY_VALIDAR_PARAMETROS_FACTURACION, [CodigoGrupoFacturacion]), 30);
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);
                Result := False;
            end;
        end;
    end;
// FIN : TASK_102_MGO_20161227
begin
    // INICIO : TASK_086_MGO_20161121
    if not FPreFacturacion then begin                                           // TASK_102_MGO_20161227
        if MsgBox(MSG_CONFIRMACION, 'Confirmaci�n', MB_YESNO) = IDNO then
            Exit;

        if not ValidarParametrosFacturacion(FCodigoGrupoFacturacion) then begin // TASK_102_MGO_20161227
            MsgBox(MSG_PARAMETROS_FACTURACION, Self.Caption, MB_ICONSTOP);      // TASK_102_MGO_20161227
            Exit;                                                               // TASK_102_MGO_20161227
        end;                                                                    // TASK_102_MGO_20161227
    end;                                                                        // TASK_102_MGO_20161227
    // FIN : TASK_086_MGO_20161121

    // INICIO : TASK_142_MGO_20170220
    if FCodigoGrupoFacturacion = 0 then begin
        if MsgBox(MSG_INFRACTORES_CONFIRM, 'Confirmaci�n', MB_YESNO) = IDNO then
            Exit;

        try
            Screen.Cursor := crHourGlass;
            try
                spCrearConveniosInfractoresPendientes.Parameters.Refresh;
                spCrearConveniosInfractoresPendientes.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                spCrearConveniosInfractoresPendientes.ExecProc;

                if spCrearConveniosInfractoresPendientes.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spCrearConveniosInfractoresPendientes.Parameters.ParamByName('@ErrorDescription').Value);
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_CONVENIOS, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;

            try
                spValorizarInfraccionesPendientes.Parameters.Refresh;
                spValorizarInfraccionesPendientes.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                spValorizarInfraccionesPendientes.ExecProc;

                if spValorizarInfraccionesPendientes.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spValorizarInfraccionesPendientes.Parameters.ParamByName('@ErrorDescription').Value);
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_INFRACCIONES, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        finally
            Screen.Cursor := crDefault;
        end;
    end;
    // FIN : TASK_142_MGO_20170220

    tmrBotones.Enabled  := false;
	Screen.Cursor       := crHourGlass;
    FFacturando         := False;
    try
        try
            lFacturando.Caption := CAPTION_PREPARANDO;
            lFacturando.Update;
            FCancelar := False;

            //if FNumeroProcesoFacturacionMorosos > 0 then FConveniosProcesadosMorosos := FConveniosProcesados;  // TASK_086_MGO_20161121

            // Inicializaci�n de variables
            FConveniosProcesados := 0;
            FThreadsCorriendo := 0;
            FInicio := GetMSCounter;

            // Obtiene la �ltima Fecha de Emisi�n
            dUltimaFechaEmision := FUltimaFechaEmision;                                                                                                                     // SS_660_CQU_20121010
            // Contamos los convenios (para despu�s poder medir el progreso)
            FTotalConvenios := ContarConvenios;		// TASK_086_MGO_20161121
            pb_facturar.Max := FTotalConvenios;		// TASK_086_MGO_20161121

            // Creamos el proceso de facturacion que vamos a comenzar
            if FTotalConvenios <> 0 then begin

                FNumeroProcesoFacturacion := CrearProcesoFacturacion;         // TASK_086_MGO_20161121
                //FNumeroProcesoFacturacion := CrearProcesoFacturacion(False);    // TASK_086_MGO_20161121
                if FNumeroProcesoFacturacion < 0 then begin
                    FCancelar := True;
                    Exit;
                end;

                //FNumeroProcesoFacturacionNormal := FNumeroProcesoFacturacion;   // TASK_086_MGO_20161121
                // Verifica que las Fechas de Emisi�n sean posteriores
                // a la �ltima Fecha de Emisi�n de las Notas de Cobro
                ObtenerCiclosAFacturar.First;
                while not ObtenerCiclosAFacturar.Eof do begin
                    if ObtenerCiclosAFacturar.RecNo in FCiclosElegidos then begin
                        if (ObtenerCiclosAFacturar.FieldByName('FechaEmision').AsDateTime < dUltimaFechaEmision) then begin
                            MsgBox(MSG_FECHA_EMISION_ANTERIOR_A_ULTIMA + FormatDateTime('(dd/mm/yyyy)', dUltimaFechaEmision) + #10#13#10#13 +
                                   Format(MSG_FECHA_EMISION_GRUPO_FECHA, [ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsInteger]) +
                                            FormatDateTime(' dd/mm/yyyy', ObtenerCiclosAFacturar.FieldByName('FechaEmision').AsDateTime));
                            Exit;
                        end;
                    end;
                    ObtenerCiclosAFacturar.Next;
                end;

                lFacturando.Caption := CAPTION_FACTURANDO;
       			// Creamos los Threads
                // Facturamos los ciclos seleccionados
                ObtenerCiclosAFacturar.First;
                while not ObtenerCiclosAFacturar.Eof do begin
                    if ObtenerCiclosAFacturar.RecNo in FCiclosElegidos then begin
                        //Cargo tabla de trabajo para calcular intereses
                        { INICIO : TASK_086_MGO_20161121
                        CargaInteresesOK := CargarDatosIntereses;
                        if CargaInteresesOK < 0 then begin
                            FCancelar := True;
                            Exit;
                        end;
                        //Cargo tabla de trabajo para calcular intereses
                        if not FCuotasGeneradas then begin
                            GenerarMovimientosCuotasOK := GenerarMovimientosCuotasArriendo;
                            if GenerarMovimientosCuotasOK < 0 then begin
                                FCancelar := True;
                                Exit;
                            end;
                        end;
                        } // FIN : TASK_086_MGO_20161121
                        // Lanzamos un thread para este ciclo
                        // FGL - Modificaci�n por Fecha de Emisi�n
                        TThreadFacturacionGrupo.Create(
                          ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsInteger,
                          ObtenerCiclosAFacturar.FieldByName('A�o').AsInteger,
                          ObtenerCiclosAFacturar.FieldByName('Ciclo').AsInteger,
                          ObtenerCiclosAFacturar.FieldByName('FechaCorte').AsDateTime,
                          ObtenerCiclosAFacturar.FieldByName('FechaEmision').AsDateTime,
                          ObtenerCiclosAFacturar.FieldByName('FechaVencimiento').AsDateTime,
                          FNumeroProcesoFacturacion, FConveniosProcesados, FThreadsCorriendo,
                          FErrorFacturacion, FCancelar, FPreFacturacion);       // TASK_086_MGO_20161121
                    end;
                    ObtenerCiclosAFacturar.Next;
                end;
                FFacturando := True;
            end else begin
                lFacturando.Caption := '';
                // Si no hay convenios para facturar, mostrar un mensaje que lo indique.
                MsgBox(MSG_NO_HAY_CONVENIOS_PARA_FACTURAR, Caption, MB_ICONINFORMATION);	// TASK_086_MGO_20161121
                { INICIO : TASK_086_MGO_20161121
                if FConveniosProcesadosMorosos <= 0 then                                                            
                    MsgBox(MSG_NO_HAY_CONVENIOS_PARA_FACTURAR, Caption, MB_ICONINFORMATION)                         
                else                                                                                                
                    MsgBox(Format(MSG_CONVENIOS_FACTURADOS, [FConveniosProcesadosMorosos, FConveniosProcesados])    
                        , Caption, MB_ICONINFORMATION);                                                             
                } // FIN : TASK_086_MGO_20161121
            end;
        except
            on e : Exception do begin
                FCancelar := True;
                MsgBoxErr(MSG_ERROR_GENERAR_FACTURA, e.Message, Caption, MB_ICONSTOP);
            end;
        end;
    finally
        tmrBotones.Enabled := true;
	    Screen.Cursor := crDefault;
    end;
end;

procedure TFormFacturacionMasiva.btn_cancelarClick(Sender: TObject);
resourcestring
	MSG_CANCELANDO_FACTURACION = 'Cancelando facturaci�n...';
	MSG_CANCELAR_FACTURACION = '�Est� seguro de que desea cancelar la Facturaci�n?';
	MSG_CANCELAR_IMPRESION   = '�Est� seguro de que desea cancelar la Impresi�n?';
begin
    //if FFacturando or FFacturandoMorosos then begin // TASK_086_MGO_20161121
    if FFacturando then begin                     // TASK_086_MGO_20161121
        if MsgBox(MSG_CANCELAR_FACTURACION, CAPTION_CANCELAR_FACTURACION, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
            FCancelar := True;
            lFacturando.Caption := MSG_CANCELANDO_FACTURACION;
            //if FFacturandoMorosos then RevisarEstadoFacturacionMorosos; // TASK_086_MGO_20161121
            if FFacturando then RevisarEstadoFacturacion;
        end;
    end else begin
        Close;
    end;
    tmrBotonesTimer(tmrBotones);
end;

procedure TFormFacturacionMasiva.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := not FFacturando;                              // TASK_086_MGO_20161121
    //CanClose := (not FFacturandoMorosos) and (not FFacturando); // TASK_086_MGO_20161121
end;

procedure TFormFacturacionMasiva.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormFacturacionMasiva.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;

procedure TFormFacturacionMasiva.MostrarProgreso;
resourcestring
	MSG_CALCULANDO  = 'Calculando Tiempos de Procesamiento';
Var
	Hecho: Extended;
	Insumido: TDateTime;
begin
	Insumido := ((GetMSCounter - FInicio) div 1000) / SecsPerDay;
    if FTotalConvenios = 0 then Hecho := 0
      else Hecho := FConveniosProcesados / FTotalConvenios;
	lbl_tiempofacttrans.caption := FormatDateTime('hh:nn:ss', Insumido);
    if (Insumido < 10 / SecsPerDay) or (Hecho = 0) then begin
		lbl_tiempofactrest.caption := MSG_CALCULANDO;
    end else begin
		lbl_tiempofactrest.caption := FormatDateTime('hh:nn:ss', Insumido / Hecho * (1 - Hecho));
    end;
	pb_facturar.position := FConveniosProcesados;                             // TASK_086_MGO_20161121
  	//pb_facturar.position := FConveniosProcesados + FConveniosProcesadosMorosos; // TASK_086_MGO_20161121
    pb_facturar.Update;
    FInsumido := Insumido;
end;

function TFormFacturacionMasiva.ContarConvenios: Integer;
var                                                                             // TASK_142_MGO_20170220
    Grupo: string;                                                              // TASK_142_MGO_20170220
begin
	Screen.Cursor := crHourGlass;
    Grupo := StringGruposFacturacion;                                           // TASK_142_MGO_20170220
    if Grupo = '0' then                                                         // TASK_142_MGO_20170220
        Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadInfractoresFacturar()')    // TASK_142_MGO_20170220
    else                                                                        // TASK_142_MGO_20170220
        Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT Count(*) FROM ' +
	        'Convenio (NOLOCK) WHERE CodigoGrupoFacturacion IN (' + StringGruposFacturacion + ')' );
	Screen.Cursor := crDefault;
end;

function TFormFacturacionMasiva.StringGruposFacturacion: AnsiString;
Var
	R: Integer;
begin
	Result := '';
	R := ObtenerCiclosAFacturar.RecNo;
	ObtenerCiclosAFacturar.First;
    while not ObtenerCiclosAFacturar.Eof do begin
        if ObtenerCiclosAFacturar.RecNo in FCiclosElegidos then begin
            if Result <> '' then Result := Result + ', ';
			Result := Result + ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsString;
        end;
		ObtenerCiclosAFacturar.Next;
    end;
	ObtenerCiclosAFacturar.RecNo := R;
    if Result = '' then Result := 'NULL';
end;

procedure TFormFacturacionMasiva.RevisarEstadoFacturacion;
resourcestring
	// Mensajes de informaci�n en el form FormFacturacion
	CAPTION_FINALIZANDO = 'Finalizando facturaci�n...';
	// Mensajes sobre MsgBox
	CAPTION_FACTURACION_FINALIZADA = 'Facturaci�n finalizada';
	MSG_FACTURACION_FINALIZADA     = 'La Facturaci�n se complet� con �xito.';
	MSG_FACTURACION_CANCELADA      = 'Facturaci�n cancelada. Si lo desea, puede reanudar la facturaci�n m�s tarde.';
	//MSG_FACTURACION_INCOMPLETA     = 'Facturaci�n finaliz� con errores. Si lo desea, puede reanudar la facturaci�n m�s tarde.';   // TASK_097_MGO_20161221
	MSG_FACTURACION_INCOMPLETA     = 'Facturaci�n finaliz� con errores.';                                                           // TASK_097_MGO_20161221
	CAPTION_BTN_CLOSE              = '&Cerrar' ;
    CAPTION_PROCESANDO_CONSUMOS    = 'Generando Detalle de Consumos de los Comprobantes';
    { INICIO : TASK_086_MGO_20161121
    MSG_CONVENIOS_FACTURADOS       =  'Facturaci�n finalizada'#10#13 +                                         
                                    'Facturaci�n Morosos: %d convenios procesados (Nro. Proc: %d)'#10#13 +   
                                    'Facturaci�n Normal: %d convenios procesados (Nro. Proc: %d)';           
    }
    MSG_CONVENIOS_FACTURADOS        = 'Facturaci�n finalizada: %d convenios procesados (Nro. Proc: %d)';
    QRY_UPDATE_PROCESOFAC       = 'UPDATE ProcesosFacturacion SET FechaHoraFin = GETDATE() WHERE NumeroProcesoFacturacion = %d';
    QRY_UPDATE_PROCESOPREFAC    = 'UPDATE PREFAC_ProcesosFacturacion SET FechaHoraFin = GETDATE() WHERE NumeroProcesoFacturacion = %d';
    // FIN : TASK_086_MGO_20161121
var
    TipificacionForm : TTipificacionMasivaForm;
    FormComprobantesEmitidosPrefac : TFormComprobantesEmitidosPrefac;	// TASK_086_MGO_20161121
begin
    if FThreadsCorriendo = 0 then begin

    	lbl_tiempofactrest.caption 	:= CAPTION_FINALIZANDO;
        lFacturando.Caption 		:= CAPTION_FINALIZANDO;
		lFacturando.Update;

        // Generamos el detalle de consumos por comprobante
        LlenarDetalleConsumoComprobantes;

		// Cerramos el proceso de facturaci�n
        { INICIO : TASK_086_MGO_20161121
		QueryExecute(DMConnections.BaseCAC, Format(
		  'UPDATE ProcesosFacturacion SET FechaHoraFin = GETDATE() WHERE NumeroProcesoFacturacion = %d',
		  [FNumeroProcesoFacturacion]));
        }
        QueryExecute(DMConnections.BaseCAC, Format(IIf(not(FPreFacturacion),
                                            QRY_UPDATE_PROCESOFAC, QRY_UPDATE_PROCESOPREFAC),
                                            [FNumeroProcesoFacturacion]));
        // FIN : TASK_086_MGO_20161121

        if FCancelar then begin
			lFacturando.Caption := '';

			MsgBox(MSG_FACTURACION_CANCELADA, CAPTION_CANCELAR_FACTURACION, MB_ICONWARNING);
        end else begin
            if not FErrorFacturacion then begin
				// Si no hubo errores de Facturaci�n
				// Actualizamos los ciclos para indicar que fueron facturados en este proceso
				ObtenerCiclosAFacturar.First;
                if not(FPreFacturacion) then begin                              // TASK_086_MGO_20161121
                    while not ObtenerCiclosAFacturar.Eof do begin
                        if ObtenerCiclosAFacturar.RecNo in FCiclosElegidos then begin
                            QueryExecute(DMConnections.BaseCAC, Format(
                            'UPDATE AgendaFacturacion ' +
                            'SET NumeroProcesoFacturacion = %d, FechaRealFacturacion = GETDATE() ' +
                            'WHERE CodigoGrupoFacturacion = %d AND A�o = %d AND Ciclo = %d',
                            [FNumeroProcesoFacturacion,
                            ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsInteger,
                            ObtenerCiclosAFacturar.FieldByName('A�o').AsInteger,
                            ObtenerCiclosAFacturar.FieldByName('Ciclo').AsInteger]));
                        end;
                        ObtenerCiclosAFacturar.Next;
                    end;
                // INICIO : TASK_086_MGO_20161121
                end else begin
                    with TADOStoredProc.Create(nil) do begin
                        Connection := DMConnections.BaseCAC;
                        ProcedureName := 'PREFAC_InsertarHistoricoComprobantes';
                        CommandTimeout := 60;
                        Parameters.Refresh;
                        Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
                        ExecProc;

                        if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                            MsgBoxErr('Error al guardar hist�rico de pre facturaci�n',
                                    Parameters.ParamByName('@ErrorDescription').Value,
                                    'Error',MB_ICONSTOP);
                    end;
                end;
                // FIN : TASK_086_MGO_20161121

		        lFacturando.Caption := CAPTION_PROCESANDO_CONSUMOS;
		        lFacturando.Update;

		        // Generamos el detalle de consumos por comprobante
            	LlenarDetalleConsumoComprobantes;
            	lbl_tiempofactrest.caption	:= CAPTION_FACTURACION_FINALIZADA;
                lFacturando.Caption 		:= CAPTION_FACTURACION_FINALIZADA;
                if pb_facturar.Position < pb_facturar.Max then begin        
                    while pb_facturar.Position < pb_facturar.Max do begin   
                        pb_facturar.Position := pb_facturar.Position + 1;   
                        pb_facturar.Update;                                 
                    end;                                                    
                end;

                if chkTipificacion.Checked then begin
                    if FindFormOrCreate(TTipificacionMasivaForm, TipificacionForm) then begin
                    	TipificacionForm.Show;
                    end else if not TipificacionForm.InicializarDesatendido(FNumeroProcesoFacturacion) then begin
                        TipificacionForm.Release;
                    end;
                end
                { INICIO : TASK_086_MGO_20161121
                else  MsgBox(Format(MSG_CONVENIOS_FACTURADOS,                                                   
                      [FConveniosProcesadosMorosos, FNumeroProcesoFacturacionMorosos,                           
                      FConveniosProcesados, FNumeroProcesoFacturacionNormal]),                                  
                      CAPTION_FACTURACION_FINALIZADA,                                                           
                      MB_ICONINFORMATION);

                btnImprimirReporte.Enabled := True;
                }
                else MsgBox(Format(MSG_CONVENIOS_FACTURADOS,
                      [FConveniosProcesados, FNumeroProcesoFacturacion]),
                      CAPTION_FACTURACION_FINALIZADA, MB_ICONINFORMATION);

                if FPreFacturacion then begin
                    Application.CreateForm(TFormComprobantesEmitidosPrefac, FormComprobantesEmitidosPrefac);
                    if FormComprobantesEmitidosPrefac.Inicializar(FNumeroProcesoFacturacion) then
                        FormComprobantesEmitidosPrefac.Show
                    else
                        FormComprobantesEmitidosPrefac.Release;

                    btnImprimirReporte.Visible := False;
                end else
                    btnImprimirReporte.Enabled := True;
                // FIN : TASK_086_MGO_20161121
            end else begin
                // Generamos el detalle de consumos por comprobante
                LlenarDetalleConsumoComprobantes;
                lbl_tiempofactrest.caption	:= CAPTION_FACTURACION_FINALIZADA;
                //lFacturando.Caption			:= CAPTION_FACTURACION_FINALIZADA;
                lFacturando.Caption := MSG_FACTURACION_INCOMPLETA;  // TASK_097_MGO_20161221
                //MsgBox(MSG_FACTURACION_INCOMPLETA, CAPTION_FACTURACION_FINALIZADA, MB_ICONINFORMATION);   // TASK_097_MGO_20161221
                btnImprimirReporte.Enabled := False;
            end;
        end;
        
		FFacturando := False;
        tmrBotones.Enabled    := False;
		btn_siguiente.enabled := False;
		btn_anterior.enabled  := False;
        btn_cancelar.Caption  := CAPTION_BTN_CLOSE;
        btn_cancelar.Enabled  := True;
        chkPreFacturar.Enabled := False;                                        // TASK_086_MGO_20161121
    end else begin
        if FAntHecho <> FConveniosProcesados then begin
			MostrarProgreso;
			lbl_conveniosproc.Caption := IntToStr(FConveniosProcesados);
            lbl_conveniosproc.Update;
            if (FAntHecho < FConveniosProcesados) and (FThreadsCorriendo = 0) then
                pb_facturar.position := pb_facturar.Max
            else
    			FAntHecho := FConveniosProcesados;
        end;
        pb_facturar.Update;
    end;
end;

function TFormFacturacionMasiva.CrearProcesoFacturacion: Integer;                                 // TASK_086_MGO_20161121
//function TFormFacturacionMasiva.CrearProcesoFacturacion(ProcesoMorosos : Boolean): Integer;         // TASK_086_MGO_20161121
resourcestring                                                                  // TASK_097_MGO_20161221
    MSG_ERROR = 'Error al crear el proceso de facturaci�n';                     // TASK_097_MGO_20161221
var                                                                                               // TASK_086_MGO_20161121
    CodigoGrupoFacturacion, Ciclo, Ano: Integer;                                                  // TASK_086_MGO_20161121
begin
    try
        // INICIO : // TASK_086_MGO_20161121
        ObtenerCiclosAFacturar.First;
        while not ObtenerCiclosAFacturar.Eof do begin
            if ObtenerCiclosAFacturar.RecNo in FCiclosElegidos then begin
                CodigoGrupoFacturacion := ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsInteger;
                Ciclo := ObtenerCiclosAFacturar.FieldByName('Ciclo').AsInteger;
                Ano := ObtenerCiclosAFacturar.FieldByName('A�o').AsInteger;
            end;
            ObtenerCiclosAFacturar.Next;
        end;

        if not(FPreFacturacion) then
            spCrearProcesoFacturacion.ProcedureName := 'CrearProcesoFacturacion'
        else
            spCrearProcesoFacturacion.ProcedureName := 'PREFAC_CrearProcesoFacturacion';

        spCrearProcesoFacturacion.Parameters.Refresh;
        spCrearProcesoFacturacion.Parameters.paramByName('@NumeroProcesoFacturacion').Value := Null;
        // FIN : // TASK_086_MGO_20161121
		spCrearProcesoFacturacion.Parameters.ParamByName('@Operador').Value := UsuarioSistema;
		spCrearProcesoFacturacion.Parameters.ParamByName('@FacturacionManual').Value := False;
        //spCrearProcesoFacturacion.Parameters.ParamByName('@PorMorosidad').Value := ProcesoMorosos;  // TASK_086_MGO_20161121
        spCrearProcesoFacturacion.Parameters.ParamByName('@PorMorosidad').Value := False;             // TASK_086_MGO_20161121  
        spCrearProcesoFacturacion.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := CodigoGrupoFacturacion;             // TASK_086_MGO_20161121
        spCrearProcesoFacturacion.Parameters.ParamByName('@Ciclo').Value := Ciclo;             // TASK_086_MGO_20161121
        spCrearProcesoFacturacion.Parameters.ParamByName('@Ano').Value := Ano;             // TASK_086_MGO_20161121
        spCrearProcesoFacturacion.ExecProc;
		Result := spCrearProcesoFacturacion.Parameters.paramByName('@NumeroProcesoFacturacion').Value;
    except
        on e: Exception do begin
            //MsgBox(e.Message, 'Error', MB_ICONSTOP);                          // TASK_097_MGO_20161221
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);              // TASK_097_MGO_20161221
            Result := -1;
        end;
    end;
end;

{*******************************************************************************
    Procedure Name: CargarDatosIntereses
    Author : rharris
    Date : 24/12/2008
    Description : (SS 627) se calculan los intereses desde tablas de trabajo
*******************************************************************************}
function TFormFacturacionMasiva.CargarDatosIntereses;
resourcestring                                                                  // TASK_097_MGO_20161221
    MSG_ERROR = 'Error al cargar los datos de intereses';                       // TASK_097_MGO_20161221
begin
    try
        // INICIO : TASK_086_MGO_20161121
        if not(FPreFacturacion) then
            spGenerarTablaTrabajoInteresesPorFacturar.ProcedureName := 'GenerarTablaTrabajoInteresesPorFacturar'
        else
            spGenerarTablaTrabajoInteresesPorFacturar.ProcedureName := 'PREFAC_GenerarTablaTrabajoInteresesPorFacturar';

        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.Refresh;
        // FIN : TASK_086_MGO_20161121
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsInteger;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@FechaEmision').Value := ObtenerCiclosAFacturar.FieldByName('FechaEmision').AsDateTime;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@CodigoConvenio').Value := NULL;
        spGenerarTablaTrabajoInteresesPorFacturar.ExecProc;
        //FInteresesGenerados := True;        // TASK_086_MGO_20161121
        Result := 1;
    except
        on e: Exception do begin
            //MsgBox(e.Message, 'Error', MB_ICONSTOP);
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);             // TASK_097_MGO_20161221
            //FInteresesGenerados := False;   // TASK_086_MGO_20161121
            Result := -1;
        end;
    end;
end;

// INICIO : TASK_086_MGO_20161121
procedure TFormFacturacionMasiva.chkPreFacturarClick(Sender: TObject);
begin
    HabilitarCheckPreFacturar;
end;

procedure TFormFacturacionMasiva.chkPreFacturarKeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key = Char(13) then
        HabilitarCheckPreFacturar;
end;

procedure TFormFacturacionMasiva.HabilitarCheckPreFacturar;
begin
    if chkPreFacturar.Checked then begin
        chkTipificacion.Enabled := False;
        chkTipificacion.Checked := False;
    end else
        chkTipificacion.Enabled := True;

    FPreFacturacion := chkPreFacturar.Checked;
end;
// FIN : TASK_086_MGO_20161121

{*******************************************************************************
    Procedure Name: LlenarDetalleConsumoComprobantes
    Author : rharris
    Date : 24/12/2008
    Description : (SS 627) se calculan los detalle de consumo del comprobante
    desde los movimientos prefacturados
*******************************************************************************}
procedure TFormFacturacionMasiva.LlenarDetalleConsumoComprobantes;
resourcestring                                                                  // TASK_097_MGO_20161221
    MSG_ERROR = 'Error llenando detalle de consumo de Comprobantes.';           // TASK_097_MGO_20161221
begin
    try
        // INICIO : TASK_086_MGO_20161121
        if not(FPreFacturacion) then
            spLlenarDetalleConsumoComprobantes.ProcedureName := 'LlenarDetalleConsumoComprobantes'
        else
            spLlenarDetalleConsumoComprobantes.ProcedureName := 'PREFAC_LlenarDetalleConsumoComprobantes';

        spLlenarDetalleConsumoComprobantes.Parameters.Refresh;
        // FIN : TASK_086_MGO_20161121
        spLlenarDetalleConsumoComprobantes.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
        spLlenarDetalleConsumoComprobantes.ExecProc;
    except
        on e: Exception do begin
            //MsgBox(e.Message, 'Error llenado Detalle Consumo Comprobantes', MB_ICONSTOP);     // TASK_097_MGO_20161221
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);                             // TASK_097_MGO_20161221
            //raise Exception.Create(e.Message);                                                // TASK_097_MGO_20161221
        end;
    end;
end;

procedure TFormFacturacionMasiva.ListaCiclosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
Var
	i: Integer;
begin
    if Column = ListaCiclos.Columns[0] then begin
        i := iif(ObtenerCiclosAFacturar.RecNo in FCiclosElegidos, 0, 1);
        lnCheck.Draw(ListaCiclos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
        DefaultDraw := False;
        ItemWidth   := lnCheck.Width + 2;
    end;
end;

procedure TFormFacturacionMasiva.ListaCiclosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
	i: Integer;
begin
    i := ObtenerCiclosAFacturar.RecNo;
    if i in FCiclosElegidos then Exclude(FCiclosElegidos, i)
    else begin                                                                  // TASK_086_MGO_20161121
        FCiclosElegidos := [];                                                  // TASK_086_MGO_20161121
        Include(FCiclosElegidos, i);                                            // TASK_086_MGO_20161121
        FCodigoGrupoFacturacion := ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsInteger;  // TASK_102_MGO_20161227
    end;                                                                        // TASK_086_MGO_20161121
    Sender.Invalidate;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarReporte
  Author:    ndonadio
  Date Created: 25/02/2005
  Description: Prepara y muestra el reporte de Facturacion
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormFacturacionMasiva.MostrarReporte(Tiempo: TDateTime);
var
    f: TformReporteFacturacion;
begin
    try
		if findFormOrCreate(TFormReporteFacturacion,f) then f.free;
		findFormOrCreate(TFormReporteFacturacion,f)  ;
        if f.Inicializa(FNumeroProcesoFacturacion, Tiempo) then f.Ejecutar
        else f.Free;
    except
        on e:Exception do begin
            MsgBoxErr('Error al intentar crear el Reporte',e.Message,'ERROR',MB_ICONSTOP)
        end;
    end;
end;


procedure TFormFacturacionMasiva.btnImprimirReporteClick(Sender: TObject);
resourcestring
	MSG_NO_EXECUTED     = 'El proceso de facturaci�n no se ha ejecutado';
    TITLE_NO_EXECUTED   = 'Advertencia';
begin
    if FInsumido > 0 then
        MostrarReporte(FInsumido)
    else MsgBox(MSG_NO_EXECUTED,TITLE_NO_EXECUTED,MB_ICONWARNING)
end;

{******************************** Function Header ******************************
Function Name: TFormFacturacionMasiva.lblGoToParametrosClick
Author : gcasais
Date Created : 08/07/2005
Description : Muestra los par�metros relativos a facturaci�n
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TFormFacturacionMasiva.lblGoToParametrosClick(Sender: TObject);
var
{ INICIO : TASK_090_MGO_20161213
    f: TFormParam;
begin
    Application.CreateForm(TFormParam, f);
    if f.Inicializar('F', False) then f.ShowModal;
}
    f: TfrmParametrosProcesoFacturacion;
begin
    Application.CreateForm(TfrmParametrosProcesoFacturacion, f);
    if f.Inicializar then f.ShowModal;
    f.Release;
// FIN : TASK_090_MGO_20161213
end;

{*******************************************************************************
    Procedure Name: GenerarMovimientosCuotasArriendo
    Author : rharris
    Date : 24/12/2008
    Description : (SS 627) se calculan los intereses desde tablas de trabajo
*******************************************************************************}
function TFormFacturacionMasiva.GenerarMovimientosCuotasArriendo;
resourcestring                                                                  // TASK_097_MGO_20161221
    MSG_ERROR = 'Error al generar los movimientos por cuotas de arriendo.';     // TASK_097_MGO_20161221
begin
    try
        // INICIO : TASK_086_MGO_20161121
        if not(FPreFacturacion) then
            spGenerarMovimientosCuotasArriendo.ProcedureName := 'GenerarMovimientosCuotasArriendo'
        else
            spGenerarMovimientosCuotasArriendo.ProcedureName := 'PREFAC_GenerarMovimientosCuotasArriendo';

        spGenerarMovimientosCuotasArriendo.Parameters.Refresh;
        // FIN : TASK_086_MGO_20161121
        spGenerarMovimientosCuotasArriendo.Parameters.ParamByName('@FechaEmision').Value := ObtenerCiclosAFacturar.FieldByName('FechaEmision').AsDateTime;
        spGenerarMovimientosCuotasArriendo.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := ObtenerCiclosAFacturar.FieldByName('CodigoGrupoFacturacion').AsInteger;
        spGenerarMovimientosCuotasArriendo.Parameters.ParamByName('@CodigoConvenio').Value := NULL;
        spGenerarMovimientosCuotasArriendo.Parameters.ParamByName('@IndiceVehiculo').Value := NULL;
        spGenerarMovimientosCuotasArriendo.Parameters.ParamByName('@Usuario').Value := NULL;
        spGenerarMovimientosCuotasArriendo.ExecProc;
        FCuotasGeneradas := True;
        Result := 1;
    except
        on e: Exception do begin
            //MsgBox(e.Message, 'Error', MB_ICONSTOP);                          // TASK_097_MGO_20161221
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);             // TASK_097_MGO_20161221
            FCuotasGeneradas := False;
            Result := -1;
        end;
    end;
end;

end.
