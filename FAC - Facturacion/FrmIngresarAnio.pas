{-----------------------------------------------------------------------------
 Unit Name: FrmIngresarAnio
 Author:    ggomez
 Purpose: Permitir al operador ingresar un a�o.
 History:
-----------------------------------------------------------------------------}

unit FrmIngresarAnio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DmiCtrls, PeaProcs;

type
  TFormIngresarAnio = class(TForm)
    lbl_Titulo: TLabel;
    Bevel1: TBevel;
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    txt_Anio: TNumericEdit;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
  private
    { Private declarations }
    // Para mantener el a�o ingresado por el operador
    FAnioIngresado: AnsiString;
  public
    { Public declarations }
    function Inicializar: Boolean;
    function ObtenerAnioIngresado: AnsiString;
  end;

var
  FormIngresarAnio: TFormIngresarAnio;

implementation

{$R *.dfm}

procedure TFormIngresarAnio.btn_CancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TFormIngresarAnio.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_ANIO_INVALIDO = 'El a�o ingresado no es v�lido.';
    CAPTION_INGRESAR_ANIO = 'Ingresar A�o';
begin
    btn_Aceptar.SetFocus;
    (* Controlar que el a�o ingresado sea v�lido. *)
    if not ValidateControls([txt_Anio],
        [(Trim(txt_Anio.Text) <> EmptyStr) and (txt_Anio.ValueInt > 0)],
        CAPTION_INGRESAR_ANIO,
        [MSG_ANIO_INVALIDO]) then Exit;

    FAnioIngresado := txt_Anio.Text;
    Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormIngresarAnio.Inicializar
  Author:    ggomez
  Date:      10-Feb-2005
  Arguments: None
  Result:    Boolean
-----------------------------------------------------------------------------}
function TFormIngresarAnio.Inicializar: Boolean;
begin
    Result := True;
    try
        // Incializar la variable de retorno.
        FAnioIngresado := EmptyStr;

        (* Colocar en el control para ingresar el a�o, el a�o actual. *)
        txt_Anio.ValueInt := CurrentYear;

    except
        Result := False;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: TFormIngresarAnio.ObtenerAnioIngresado
  Author:    ggomez
  Date:      10-Feb-2005
  Arguments: None
  Result:    AnsiString. Retorna el valor de la variable FAnioIngresado.
-----------------------------------------------------------------------------}
function TFormIngresarAnio.ObtenerAnioIngresado: AnsiString;
begin
    Result := FAnioIngresado;
end;

end.
