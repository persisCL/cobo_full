unit ReporteDetalleViajes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppStrtch, ppRegion, ppClass, ppVar,
  ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppDBBDE, ppComm, ppRelatv,
  ppDBPipe, DB, ADODB, DMConnection, peaProcs;

type
  TfrmDetalleViajes = class(TForm)
    rbi_FacturacionDetallada: TRBInterface;
    rp_FacturacionDetallada: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppLabel21: TppLabel;
    ppLabel34: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel17: TppLabel;
    ppDBText12: TppDBText;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel23: TppLabel;
    ppDBText15: TppDBText;
    lApellidoNombreDet: TppLabel;
    ppDBText18: TppDBText;
    ppLabel25: TppLabel;
    ppLine2: TppLine;
    ppSystemVariable2: TppSystemVariable;
    ppDetailBand2: TppDetailBand;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText9: TppDBText;
    ppDBText8: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppLine6: TppLine;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppRegion3: TppRegion;
    ppLabel37: TppLabel;
    ppLabel38: TppLabel;
    ppLabel39: TppLabel;
    ppLabel40: TppLabel;
    ppLabel13: TppLabel;
    ppLabel22: TppLabel;
    ppLabel14: TppLabel;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppRegion4: TppRegion;
    ppLabel41: TppLabel;
    ppDBCalc2: TppDBCalc;
    plDetalle: TppDBPipeline;
    ObtenerFacturacionDetallada: TADOStoredProc;
    dsFacturacionDetallada: TDataSource;
    ppDatosCliente: TppDBPipeline;
    dsDatosCliente: TDataSource;
    ppDBText1: TppDBText;
    lDomicilio: TppLabel;
    ObtenerDatosCliente: TADOStoredProc;
    ppConcesionaria: TppField;
    ppNumeroViaje: TppField;
    ppFechaHoraInicio: TppField;
    ppImporte: TppField;
    ppPatente: TppField;
    ppCategoria: TppField;
    ppPuntosCobro: TppField;
    ObtenerDatosClienteCodigoPersona: TAutoIncField;
    ObtenerDatosClienteCodigoDocumento: TStringField;
    ObtenerDatosClienteNumeroDocumento: TStringField;
    ObtenerDatosClienteNumeroDocumentoFormateado: TStringField;
    ObtenerDatosClientePersoneria: TStringField;
    ObtenerDatosClientePersoneriaDescripcion: TStringField;
    ObtenerDatosClienteNombre: TStringField;
    ObtenerDatosClienteDescripcionDomicilio: TStringField;
    ObtenerDatosClienteCodigoPostal: TStringField;
    ObtenerDatosClienteDescripcionComuna: TStringField;
    ObtenerDatosClienteDescripcionRegion: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Execute(TipoComprobante: Char;
      CodigoCovenio, CodigoCliente: Integer; NumeroComprobante: Double; showInterface: Boolean; Titulo: AnsiString);
  end;

implementation

{$R *.dfm}


procedure TfrmDetalleViajes.Execute(TipoComprobante: Char;
      CodigoCovenio, CodigoCliente: Integer; NumeroComprobante: Double; showInterface: Boolean; Titulo: AnsiString);
var
    Cpostal: AnsiString;
begin
	ObtenerFacturacionDetallada.Close;
	ObtenerFacturacionDetallada.Parameters.ParamByName('@CodigoConvenio').Value := CodigoCovenio;
	ObtenerFacturacionDetallada.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
	ObtenerFacturacionDetallada.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
	ObtenerFacturacionDetallada.Open;
    ObtenerDatosCliente.close;
    ObtenerDatosCliente.Parameters.ParamByName('@CodigoCliente').value := CodigoCliente;
    ObtenerDatosCliente.open;
	if not ObtenerDatosCliente.IsEmpty then begin

        lApellidoNombreDet.Caption	:= ObtenerDatosCliente.FieldByName('Nombre').AsString;

        if trim(ObtenerDatosCliente.fieldByName('CodigoPostal').AsString) <> '' then
            Cpostal := '  (' + trim(ObtenerDatosCliente.fieldByName('CodigoPostal').asString) + ') '
        else
            Cpostal := '';
            
        lDomicilio.Caption := trim(ObtenerDatosCliente.fieldByName('DescripcionDomicilio').asString) + Cpostal;
		// Ejecutamos el reporte
		rbi_FacturacionDetallada.Execute(showInterface);
	end;
end;


end.
