{-----------------------------------------------------------------------------
 File Name: frmAnulacionNotasCredito.pas
 Author:    flamas
 Date Created: 07/06/2005
 Language: ES-AR
 Description:  Proceso de Anulaci�n de Notas de Cobro

 Revision 1:
     Author : ggomez
     Date : 23/01/2006
     Description : Al SP spAnularNotaCobro le sub� el CommandTimeOut a 180
        pues para una NK con muchos tr�nsitos daba TimeOut. ESTA ES UNA SOLUCI�N
        PROVISORIA.

 Revision 1
 Author:    jconcheyro
 Date Created: 27/07/2006
 Language: ES-AR
 Description:  Se le agregan decimales a los importes ya que son importantes
 para mantener el balance de la cuenta corriente

 Revision 2
 Author:    jconcheyro
 Date Created: 09/08/2006
 Language: ES-AR
 Description:  Se le agrega un checkbox que comanda un nuevo parametro de
 Anular comprobante que determinar el estado a pasar a los transitos

 Revision 3
 Author:    jconcheyro
 Date Created: 22/08/2006
 Language: ES-AR
 Description:  Se agrega como tipo de comprobante posible de anulaci�n las boletas

 Revision 4
 Author:    jconcheyro
 Date Created: 05/11/2006
 Language: ES-AR
 Description:  Se agrega funcionalidad de no poder acreditar el IVA mas all� de X dias
 dados por el parametro DIAS_ACREDITAR_IVA


 Revision 5
 Author:    jconcheyro
 Date Created: 21/12/2006
 Language: ES-AR
 Description:  Se pasa todo lo que era decimal a integer, se redondea el precio
 de los transitos seleccionados para el credito hacia arriba. Se elimina toda 
 posibilidad de que existan o procesen decimales.

 Revision 6
 Author:    nefernandez
 Date Created: 23/02/2007
 Language: ES-AR
 Description: Se pasa el parametro CodigoUsuario al SP AnularBoleta y
              AnularNotaCobro (para Auditoria)

 Revision 7
 Author:    jconcheyro
 Date Created: 09/04/2007
 Language: ES-AR
 Description:  Se pueden hacer ahora creditos a boletas de arriendo.

 Revision 8
 Author:    rharris
 Date Created: 08/04/2009
 Language: ES-CL
 Description:
    1.- Se agregan el NumeroProcesoFacturacion a los Comprobantes
    de Notas de Credito
    2.- Se inserta en NumeroProcesoFacturacion en la cola de env�o a DBNET

 Revision 9
 Author:    rharris
 Date Created: 18/06/2009
 Language: ES-CL
 Description: (Ref.:Facturacion Electronica)
    1.- Si se eligen todos los transitos para anular un concepto, y si la suma
        de los importes de esos transitos seleccionados da m�s que el total del
        MC anulado, entonces se deja el importe original del MC.

 Revision 10
 Author: rharris
 Date Created: 19/06/2009
 Language: ES-CL
 Description: (Ref.:Facturacion Electronica)
    1.- Se agrega validaci�n para para que el m�ximo importe anulado por concepto
        no supere el importe total para concepto del comprobante que se est� anulando

 Revision 11
 Author: rharris
 Date Created: 23/06/2009
 Language: ES-CL
 Description: (Ref.:Facturacion Electronica)
    1.- Se modifica el AnularComprobante(spAnularNotaCobro, spAnularBoleta)
        para que si se anula un comprobante no electr�nico imprima en el formato
        original.

 Revision 12
 Author: mbecerra
 Date: 11-Agosto-2009
 Description:	(Ref. SS 784)
        Se modifica para validar que la inserci�n en la Cola de DBNet fue
        exitosso.

Revision 13
Author      :   Nelson Droguett Sierra
Date        :   23-Agosto-2009
Description : El usuario gatillar� que el ajuste de sencillo actual (de la tabla convenio) pase al saldo del
             �ltimo comprobante del convenio, dejando el ajuste de sencillo actual consistentemente en cero.
             Si el comprobante estaba con estado pago deber� volver a estar impago.
            Se debe actualizar la generaci�n (manual) de notas de cr�dito para que recibida la marca respectiva
            (un check desde la pantalla) no realice ajustes de sencillo. De la misma forma no deber�n realizarse
            ajustes de sencillo si el convenio esta dado de baja o no tiene cuentas.
            La mejor forma de traspasar el ajuste actual (paso 2) es desde la misma nota de cr�dito con una marca
            �traspasar ajuste de sencillo actual� (habilitado siempre y cuando sea el �ltimo comprobante).

Revision 14
Author      :   Nelson Droguett Sierra
Date        :   15-Septiembre-2009
Description : No debe permitir hacer una nota de credito de cierre de convenio si el convenio tiene movimientos
             no facturados.


 Revision 15
 Author: Nelson Droguett Sierra
 Date: 17-Noviembre-2009
 Description:	(Ref. SS 847)
        Al pasar un concepto de peajes del periodo, modificar y seleccionarlos
        todos, llena una lista para almacenar ahi los peajes. Luego, si eliminan
        el concepto, esta lista no se limpiaba, por lo que a pesar de decir que
        no acreditan los peajes, los rebaja igual, p Revision 14

 Revision 16
 Author: vpaszkowicz
 Date: 17-Diciembre-2009
 Description:	(Ref. SS 847)
        Cuando se eliminan movimientos de peaje y existen mas de 1 en la tabla
        de memoria, Si se elimina solo uno de los movimientos agregarods para
        anular, se deberan borrar solo los transitos de ese movimiento.
ermitiendo facturarlos
        nuevamente. Lo que se hizo es que al eliminar un concepto que es de peajes,
        se limpia esta lista.

 Revision		: 17
 Author			: mbecerra
 Date			: 01-Junio-2010
 Description	:		(Ref. Fase II)
                    Los conceptos de peajes y peajes del per�odo se traen para
                    Multiconcesionaria.
-----------------------------------------------------------------------------}
unit frmAnulacionNotasCobroCierreConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, Validate,
  DateEdit, ExtCtrls, ppCtrls, ppStrtch, ppRichTx, ppPrnabl, ppClass, ppBands,
  ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppDB, ppDBPipe,
  DBClient, Frm_CargoNotaCredito, FrmConsultaTransitosComprobanteAnular,
  ppParameter, ppModule, raCodMod, ppSubRpt, ReporteCK, ReporteNC, ImgList,
  ConstParametrosGenerales, frmReporteNotaCreditoElectronica,Math;

const
	KEY_F9 = VK_F9;
type
    Tfrm_AnulacionNotasCobroCierreConvenio = class(TForm)
    gbBusqueda: TGroupBox;
    gbDatosComprobante: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    Label10: TLabel;
    lblNumeroComprobante: TLabel;
    edNumeroComprobante: TNumericEdit;
	btnGenerarOAnular: TButton;
	btnSalir: TButton;
	gbDocumentoOriginal: TGroupBox;
	spComprobantes: TADOStoredProc;
	spComprobantesNumeroConvenioFormateado: TStringField;
	spComprobantesNumeroConvenio: TStringField;
	spComprobantesCodigoConvenio: TIntegerField;
	spComprobantesTipoComprobante: TStringField;
    spComprobantesNumeroComprobante: TLargeintField;
    spComprobantesPeriodoInicial: TDateTimeField;
    spComprobantesPeriodoFinal: TDateTimeField;
	spComprobantesFechaEmision: TDateTimeField;
    spComprobantesFechaVencimiento: TDateTimeField;
    spComprobantesNombreCliente: TStringField;
    spComprobantesDomicilio: TStringField;
    spComprobantesComuna: TStringField;
	spComprobantesComentarios: TStringField;
    spComprobantesCodigoPostal: TStringField;
    spComprobantesSaldoPendiente: TStringField;
    spComprobantesAjusteSencilloAnterior: TStringField;
    spComprobantesAjusteSencilloActual: TStringField;
    spComprobantesTotalAPagar: TStringField;
    spComprobantesCodigoTipoMedioPago: TWordField;
    spComprobantesEstadoPago: TStringField;
    dblDocumentoOriginal: TDBListEx;
    gbNotaCredito: TGroupBox;
	btnModificarConcepto: TButton;
    btnEliminarConcepto: TButton;
    btnAgregarNuevoConcepto: TButton;
    Label2: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    lblTotalAPagar: TLabel;
	Label11: TLabel;
    lblEstado: TLabel;
	lblTotalNotaCredito: TLabel;
	lblImporteTotalNotaCredito: TLabel;
	lblTipoComprobante: TLabel;
	cbTipoComprobante: TVariantComboBox;
	dblNotaCredito: TDBListEx;
	btnBuscar: TButton;
	lblFechaVencimiento: TLabel;
	lblFecha: TLabel;
	cdsOriginal: TClientDataSet;
	dsOriginal: TDataSource;
	dsCredito: TDataSource;
	spObtenerCargosComprobanteAAnular: TADOStoredProc;
    //datos del clientDataSet
	cdsCredito: TClientDataSet;
	cdsCreditoCodigoConcepto: TIntegerField;
	cdsCreditoCodigoConvenio: TIntegerField;
	cdsCreditoImporte: TFloatField;
	cdsCreditoObservaciones: TStringField;
	cdsCreditoNroOriginal: TIntegerField;
	cdsCreditoDescriImporte: TStringField;
	cdsCreditoDescripcion: TStringField;
	cdsCreditoImporteOriginal: TFloatField;
	cdsCreditoNumeroMovimiento: TIntegerField;
	cdsCreditoCodigoConceptoOriginal: TIntegerField;
    cdsCreditoIndiceVehiculo: TStringField;
    //
	spObtenerConceptoQueAnula: TADOStoredProc;
    spAgregarMovimientoCuentaTemporal: TADOStoredProc;
    btnPasarConcepto: TButton;
	btnPasarTodos: TButton;
	spCargarTransitoEnTablaTemporal: TADOStoredProc;
    ppLineasCKDBPipeline: TppDBPipeline;
    dsLineasImpresionCK: TDataSource;
	spObtenerLineasImpresionCK: TADOStoredProc;
    spObtenerLineasImpresionCKDescripcion: TStringField;
    spObtenerLineasImpresionCKImporte: TLargeintField;
    spObtenerLineasImpresionCKDescImporte: TStringField;
    spObtenerLineasImpresionCKCodigoConcepto: TWordField;
    lblTotalCKsPreviasANKText: TLabel;
    lblTotalCKsPreviasANK: TLabel;
    lblMaximoTotalComprobanteCKText: TLabel;
    lblMaximoTotalComprobanteCK: TLabel;
    spLimpiarDatosTemporales: TADOStoredProc;
    spObtenerImporteTransitosAcreditados: TADOStoredProc;
    Img_Tilde: TImageList;
    chkPasarTransitoEstadoEspecial: TCheckBox;
    spAnularNotaCobro: TADOStoredProc;
    cbImpresorasFiscales: TVariantComboBox;
    lblImpresoraFiscal: TLabel;
    lblNumeroComprobanteFiscal: TLabel;
    edNumeroComprobanteFiscal: TNumericEdit;
    spChequearUsuarioConcurrenteAcreditando: TADOStoredProc;
    cdsCreditoImporteIVA: TFloatField;
    cdsCreditoPorcentajeIVA: TFloatField;
    spAnularBoleta: TADOStoredProc;
    cdsCreditoValorNeto: TFloatField;
    cdsCreditoDescriImporteIVA: TStringField;
    cdsOriginalConcepto: TIntegerField;
    cdsOriginalImporte: TFloatField;
    cdsOriginalDescripcion: TStringField;
    cdsOriginalDescriImporte: TStringField;
    cdsOriginalNumeroLinea: TIntegerField;
    cdsOriginalPasado: TStringField;
    cdsOriginalObservaciones: TStringField;
    cdsOriginalNumeroMovimiento: TIntegerField;
    cdsOriginalIndiceVehiculo: TStringField;
    cdsOriginalConceptoEspecial: TIntegerField;
    cdsOriginalSeleccionado: TBooleanField;
    cdsOriginalAfectoIVA: TBooleanField;
    cdsOriginalValorNeto: TFloatField;
    cdsOriginalImporteIVA: TFloatField;
    cdsOriginalDescriValorNeto: TStringField;
    cdsOriginalDescriImporteIVA: TStringField;
    cdsOriginalPorcentajeIVA: TFloatField;
    cdsOriginalDescriPorcentajeIVA: TStringField;
    cdsCreditoDescriPorcentajeIVA: TStringField;
    Label1: TLabel;
    lblValorRUT: TLabel;
    Label4: TLabel;
    lblValorNumeroConvenio: TLabel;
    edNumeroNotaCreditoFiscal: TNumericEdit;
    lblNumeroNotaCreditoFiscal: TLabel;
    spComprobantesNumeroComprobanteFiscal: TLargeintField;
    spComprobantesNumeroDocumento: TStringField;
    spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet: TADOStoredProc;
    StringField15: TStringField;
    StringField16: TStringField;
    IntegerField2: TIntegerField;
    StringField17: TStringField;
    LargeintField3: TLargeintField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    DateTimeField7: TDateTimeField;
    DateTimeField8: TDateTimeField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    WordField2: TWordField;
    StringField27: TStringField;
    LargeintField4: TLargeintField;
    StringField28: TStringField;
    spCrearProcesoFacturacion: TADOStoredProc;
    spObtenerNumeroTipoInternoComprobanteFiscal: TADOStoredProc;
    spObtenerImporteAnuladoConceptoComprobanteAAnular: TADOStoredProc;
    cbTraspasarAjusteSencilloActual: TCheckBox;
    cbNoRealizarAjusteSencillo: TCheckBox;
    spObtenerUltimoComprobanteConvenioConTotal: TADOStoredProc;
    spTraspasarUltimoAjusteSencilloConvenioAComprobante: TADOStoredProc;
    spObtenerConvenio: TADOStoredProc;
    spObtenerCuentas: TADOStoredProc;
    lblUltimoAjusteSencillo: TLabel;
    cbNoGenerarIntereses: TCheckBox;
    lblSaldoConvenio: TLabel;
    lblSugerida: TLabel;
    SPObtenerUltimosConsumos: TADOStoredProc;
    spObtenerTodosLosCodigoConcepto: TADOStoredProc;
    procedure cbTipoComprobanteChange(Sender: TObject);
    procedure dblDocumentoOriginalDblClick(Sender: TObject);
	procedure dblDocumentoOriginalDrawText(Sender: TCustomDBListEx;
	  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
	  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
	  var DefaultDraw: Boolean);
	procedure btnModificarConceptoClick(Sender: TObject);
	procedure btnAgregarNuevoConceptoClick(Sender: TObject);
	procedure btnEliminarConceptoClick(Sender: TObject);
	procedure btnPasarTodosClick(Sender: TObject);
	procedure btnPasarConceptoClick(Sender: TObject);
	procedure btnBuscarClick(Sender: TObject);
	procedure btnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
	procedure btnGenerarOAnularClick(Sender: TObject);
	procedure CargarTiposComprobantes;
	procedure PasarConcepto;
    function  ChequearConcurrencia : boolean;
    procedure cbTraspasarAjusteSencilloActualClick(Sender: TObject);
  private
    { Private declarations }
    FNumeroComprobante: Int64;
	FBotonBloqueadoPorError: boolean;
	{--------REV.15
	FCodigoConceptoPeaje : integer;
	FCodigoConceptoPeajePeriodoAnterior: integer;}
    FListaCodigoConceptoPeaje,
    FListaCodigoConceptoPeajeAnterior : TStringList;
    //---------- FIN REV.15
	FTotalComprobante:double;
	FTotalComprobanteNeto:double; //precio sin IVA de movimientos en boletas
    FMaximoTotalComprobanteCK: double;
    // Rev.13 / 02-Septiembre-2009 / Nelson Droguett Sierra -------------------------------------------------------
    FSaldoConvenio: double;
    // ------------------------------------------------------------------------------------------------------------
	FCodigoConvenio: integer;
	FListaTransitosAlCredito: TStringList;
    FIdSesion : integer;
    FFechaProceso: TDateTime ;
    FCantidadDiasCreditosConIVA: integer;
    FNoSePuedeAcreditarIVA: boolean;
	function  CargarComprobantes: Boolean;
	procedure LimpiarDatosCliente;
	procedure LimpiarDatosComprobante;
	procedure LimpiarDetalles;
	procedure AnularNotaCobro;
    procedure AnularBoleta;
	procedure CargarDetalleComprobante;
	procedure UpdateImporteTotalCredito;
	function SeleccionarTransitosAAnular(aNumeroMovimiento: integer; var SelectedAll:boolean): Int64;
    procedure LimpiarDatosTemporales;
    function ObtenerImporteRemanente(aCodigoConcepto: integer): double;
    //function LineaEsSeleccionable: boolean;
    function ObtenerIVA: Double;
    function ValidarNumeroFiscalNotaCredito:boolean;
    Function  TieneUltimosConsumosSinFacturar (CodigoConvenio : Integer): Boolean;
  public
	{ Public declarations }
	function Inicializar : Boolean; //overload; mbecerra---> no tiene sentido
    function LlenarCodigosConceptosDePeajes : boolean;			//REV.17
  end;

var
  frm_AnulacionNotasCobroCierreConvenio: Tfrm_AnulacionNotasCobroCierreConvenio;
  NumeroProcesoFacturacion : integer;
  NumeroComprobante: Integer;
  TipoComprobante: string;

implementation

{$R *.dfm}


{-----------------------------------------------------------------------------
  					LlenarCodigosConceptosDePeajes
  Author		: mbecerra
  Date			: 07/06/2005
  Description	: 			(Ref Fase II)
                	Llena las listas con todos los c�digos de conceptos de
                    peajes y peajes anteriores, para todas las concesionarias

                    1.-		No filtra si est�n activos o no, los carga todos
                	2.-		No importa indicar la concesionaria, pues se
                        	necesitan s�lo para efectos de
-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobroCierreConvenio.LlenarCodigosConceptosDePeajes;
resourcestring
	SQL_TIPO_CONCEPTO_PEAJE				= 'SELECT dbo.CONST_TIPO_CONCEPTO_PEAJES_PERIODO()';
	SQL_TIPO_CONCEPTO_PEAJE_ANTERIOR	= 'SELECT dbo.CONST_TIPO_CONCEPTO_PEAJES_PERIODO_ANTERIOR()';

var
	TipoConceptoPeaje, TipoConceptoPeajeAnterior : integer;
begin

    TipoConceptoPeaje			:= QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_CONCEPTO_PEAJE);
    TipoConceptoPeajeAnterior	:= QueryGetValueInt(DMConnections.BaseCAC, SQL_TIPO_CONCEPTO_PEAJE_ANTERIOR);

    FListaCodigoConceptoPeaje.Clear;
    FListaCodigoConceptoPeajeAnterior.Clear;

    //primero los peajes
    spObtenerTodosLosCodigoConcepto.Close;
    with spObtenerTodosLosCodigoConcepto do begin
        Parameters.ParamByName('@CodigoTipoConcepto').Value	:= TipoConceptoPeaje;
        Parameters.ParamByName('@SoloActivos').Value		:= 0;
        Open;
        while not Eof do begin
            FListaCodigoConceptoPeaje.Add(FieldByName('CodigoConcepto').AsString);
            Next;
        end;
    end;

    //ahora los peajes anteriores
    spObtenerTodosLosCodigoConcepto.Close;
    with spObtenerTodosLosCodigoConcepto do begin
        Parameters.ParamByName('@CodigoTipoConcepto').Value	:= TipoConceptoPeajeAnterior;
        Parameters.ParamByName('@SoloActivos').Value		:= 0;
        Open;
        while not Eof do begin
            FListaCodigoConceptoPeajeAnterior.Add(FieldByName('CodigoConcepto').AsString);
            Next;
        end;
    end;

    spObtenerTodosLosCodigoConcepto.Close;

end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 07/06/2005
  Description:  Inicializaci�n de Este Formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobroCierreConvenio.Inicializar: Boolean;
resourcestring
    MSG_ERROR_OBTENIENDO_PARAMETRO_DIAS_IVA = 'Error obteniendo par�metro que indica cantidad de d�as que se puede acreditar IVA';
    MSG_ERROR_EN_PARAMETRO_DIAS_IVA = 'El par�metro que indica cantidad de d�as que se puede acreditar IVA es inv�lido';
begin
    // Rev.13 / 30-Agosto-2009 / Nelson Droguett Sierra ------------------------------
    lblUltimoAjusteSencillo.Visible:=False;
    lblSaldoConvenio.Visible:=False;
    lblSugerida.Visible:=False;
    //cbNoGenerarIntereses.Enabled := ExisteAcceso('GENERAR_INTERESES');
    //--------------------------------------------------------------------------------
    Result := False;
	CenterForm(Self);
	LimpiarDatosCliente;
	lblEstado.Caption := '';
	CargarTiposComprobantes;
    cbTipoComprobanteChange(self);
    CargarImpresorasFiscales(DMConnections.BaseCAC, cbImpresorasFiscales, 0);
	LimpiarDetalles;
	 {REV.17
	FCodigoConceptoPeaje := QueryGetValueInt(DMConnections.BaseCAC,'SELECT DBO.CONST_CONCEPTO_PEAJE_PERIODO()');
	FCodigoConceptoPeajePeriodoAnterior := QueryGetValueInt(DMConnections.BaseCAC,'SELECT DBO.CONST_CONCEPTO_PEAJE_PERIODOS_ANTERIORES()');
    }

    FListaCodigoConceptoPeaje		 	:= TStringList.Create;
    FListaCodigoConceptoPeajeAnterior	:= TStringList.Create;
    LlenarCodigosConceptosDePeajes();
	//FIN REV.17

    FIdSesion :=  QueryGetValueInt(DMConnections.BaseCAC,'SELECT @@SPID');
    FFechaProceso := NowBase( DMConnections.BaseCAC ) ;
	FBotonBloqueadoPorError := False;
	FCodigoConvenio := 0;
	FListaTransitosAlCredito := TStringList.Create;
    try
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_ACREDITAR_IVA', FCantidadDiasCreditosConIVA);
        if FCantidadDiasCreditosConIVA <= 0 then begin
            MsgBoxErr(MSG_ERROR_EN_PARAMETRO_DIAS_IVA, caption, caption, MB_ICONERROR);
            Exit;
        end;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_OBTENIENDO_PARAMETRO_DIAS_IVA, e.Message, caption, MB_ICONERROR);
            Exit;
        end;
    end;
  	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobroCierreConvenio.LimpiarDetalles;
begin

	LimpiarDatosComprobante;
	btnGenerarOAnular.Enabled	:= False;
	//limpiamos la grilla inferior tambien
	cdsCredito.EmptyDataSet;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComprobantes
  Author:    flamas
  Date Created: 20/12/2004
  Description: Carga los comprobantes impagos de un Convenio determinado
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function Tfrm_AnulacionNotasCobroCierreConvenio.CargarComprobantes : boolean;
resourcestring
	MSG_YES 				= 'Si';
	MSG_NO 					= 'No';
	ERROR_GETTING_INVOICE	= 'Error obteniendo el Comprobante';
	MSG_NOT_PAID 			= 'No';
	MSG_INVOICE_VOIDED		= 'El comprobante se encuentra Anulado.'
								+ crlf + 'No se puede anular.';
	MSG_COMPROBANTE_INEXISTENTE		= 'El comprobante no existe.';
	//MSG_COMPROBANTE_CON_CUOTAS		= 'El comprobante tiene cuotas, no se puede acreditar por este medio'; rev7
    MSG_IVA_VENCIDO                = 'Han transcurrido m�s de %d d�as desde la emisi�n del documento de d�bito' + CRLF +
                                     'El IVA de esta Nota de Cr�dito no es acreditable para el SII';
var
	EstadoPago: string;

begin
	FListaTransitosAlCredito.Clear;
	Result := False;
    TipoComprobante := cbTipoComprobante.Value;

    if	(cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA) OR
    	(cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) then
    begin
        with spObtenerNumeroTipoInternoComprobanteFiscal do begin
            Parameters.ParamByName('@NumeroComprobanteFiscal').Value := edNumeroComprobanteFiscal.Value;
            Parameters.ParamByName('@TipoComprobanteFiscal').Value := TipoComprobante;
            ExecProc;
            NumeroComprobante := Parameters.ParamByName('@NumeroComprobante').Value;
            TipoComprobante := Parameters.ParamByName('@TipoComprobante').Value;
        end;
    end;

	spComprobantes.Close;
	spComprobantes.Parameters.ParamByName('@TipoComprobante').Value   := TipoComprobante;//cbTipoComprobante.Value;

    if cbTipoComprobante.Value = TC_BOLETA then begin
		spComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := edNumeroComprobanteFiscal.Text;
		spComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := cbImpresorasFiscales.Value;
		spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := null;
    end;

    if (cbTipoComprobante.Value = TC_NOTA_COBRO) then
    begin
        NumeroComprobante := edNumeroComprobante.ValueInt;
        spComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;//edNumeroComprobante.Text;
    end;

    if	(cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA) OR
    	(cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) then
    begin
        spComprobantes.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := null;
        spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
    end;


    if ChequearConcurrencia then Exit; //si otro usuario esta haciendo un credito de este comprobante

	try
		spComprobantes.Open;
		LimpiarDetalles;
		if not spComprobantes.IsEmpty then begin
			EstadoPago := spComprobantes.FieldByName('EstadoPago').AsString;
			lblNombre.Caption       := spComprobantes.FieldByName('NombreCliente').AsString ;
			lblDomicilio.Caption    := spComprobantes.FieldByName('Domicilio').AsString + ' ' + spComprobantes.FieldByName('Comuna').AsString ;
			lblFecha.Caption            := FormatDateTime('dd/mm/yyyy', spComprobantes.FieldByName('FechaEmision').AsDateTime);
			lblFechaVencimiento.Caption := FormatDateTime('dd/mm/yyyy', spComprobantes.FieldByName('FechaVencimiento').AsDateTime);
			lblEstado.Caption 		    := QueryGetValue(DMConnections.BaseCAC,Format('SELECT dbo.ObtenerDescripcionEstadoComprobante(%s)',
											[QuotedStr(EstadoPago)]));
			FNumeroComprobante 		    := spComprobantes.FieldByName('NumeroComprobante').AsInteger;
			FCodigoConvenio 			:= spComprobantes.FieldByName('CodigoConvenio').AsInteger;
            lblValorRUT.Caption         := spComprobantes.FieldByName('NumeroDocumento').AsString;
            lblValorNumeroConvenio.Caption := spComprobantes.FieldByName('NumeroConvenioFormateado').AsString;
			FBotonBloqueadoPorError:= False;
            if ((cbTipoComprobante.Value = TC_BOLETA) OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA)) and
                (NowBase(DMConnections.BaseCAC) - spComprobantes.FieldByName('FechaEmision').AsDateTime > FCantidadDiasCreditosConIVA) then
            begin
                FNoSePuedeAcreditarIVA := True; //no se puede acreditar el IVA
                //MsgBox(format(MSG_IVA_VENCIDO , [FCantidadDiasCreditosConIVA]));
            end else
                    FNoSePuedeAcreditarIVA := False;

            // por ahora no se pueden hacer creditos a boletas de arriendo
            // ahora se puede. rev7
//            if (cbTipoComprobante.Value = TC_BOLETA ) then begin
//                if QueryGetValueInt(DMConnections.BaseCAC,format('Select dbo.ComprobanteTieneCuotas(''%s'',%d)',[ cbTipoComprobante.Value , FNumeroComprobante ])) > 0 then begin
//        			MsgBox(MSG_COMPROBANTE_CON_CUOTAS, Caption, MB_ICONERROR);
//                    Exit;
//                end;
//            end;
			Result := True;
			CargarDetalleComprobante;
			if (EstadoPago = COMPROBANTE_ANULADO) then begin
				MsgBox(MSG_INVOICE_VOIDED, Caption, MB_ICONINFORMATION);
				FBotonBloqueadoPorError:= true;
                Exit;
			end; // else if



            // Rev.13 / 25-Agosto-2009 / Nelson Droguett Sierra
            // Obtenemos el ultimo comprobante del convenio
            // Si es el mismo seleccionado, se activa la opcion de poder traspasar el ajuste de sencillo del convenio
            // al saldo del comprobante
            With spObtenerUltimoComprobanteConvenioConTotal do
            begin
              Close;
              Parameters.Refresh;
              Parameters.ParamByName('@CodigoConvenio').Value := spComprobantes.FieldByName('CodigoConvenio').AsInteger;
              Open;
              cbTraspasarAjusteSencilloActual.Enabled := (FNumeroComprobante = FieldByName('NumeroComprobante').AsInteger);
            end;
            // ----------------------------------------------------------------------------------------------------


            // Rev.13 / 25-Agosto-2009 / Nelson Droguett Sierra
            // Si el convenio esta dado de baja, la Nota de Credito
            // debe salir forzosamente sin Ajuste de sencillo
            With spObtenerConvenio do
            begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroConvenio').Value :=  spObtenerUltimoComprobanteConvenioConTotal.FieldByName('NumeroConvenio').AsString;
                Open;
                if Not IsEmpty then
                begin
                  if FieldByName('CodigoEstadoConvenio').Value = ESTADO_CONVENIO_BAJA then
                  begin
                    cbNoRealizarAjusteSencillo.Checked:=True;
                    cbNoRealizarAjusteSencillo.Enabled:=False;
                  end;
                end;
            end;

            // Si el convenio no tiene cuentas, la Nota de Credito
            // debe salir forzosamente sin Ajuste de sencillo
            With spObtenerCuentas do
            begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value :=  spComprobantes.FieldByName('CodigoConvenio').AsInteger;
                Parameters.ParamByName('@TraerBaja').Value := 0;
                Open;
                if IsEmpty then
                begin
                  cbNoRealizarAjusteSencillo.Checked:=True;
                  cbNoRealizarAjusteSencillo.Enabled:=False;
                end;
            end;
            // ----------------------------------------------------------------------------------------------------

            // Rev.13 / 30-Agosto-2009 / Nelson Droguett Sierra
            lblUltimoAjusteSencillo.Visible:=True;
            lblUltimoAjusteSencillo.Caption:='Ultimo ajuste sencillo:'+
                            FormatearImporteConDecimales(DMConnections.BaseCAC, spObtenerConvenio.FieldByName('UltimoAjusteSencillo').AsFloat, True, False, True);

            FSaldoConvenio := spObtenerConvenio.FieldByName('SaldoConvenio').AsFloat+spObtenerConvenio.FieldByName('UltimoAjusteSencillo').AsFloat;
            lblSaldoConvenio.Visible:=True;
            lblSaldoConvenio.Caption := 'Saldo del Convenio   :'+
                            FormatearImporteConDecimales(DMConnections.BaseCAC, FSaldoConvenio,True, False, True);

            lblSugerida.Visible:=True;
            lblSugerida.Caption := 'Devoluci�n Sugerida :'+
                            FormatearImporteConDecimales(DMConnections.BaseCAC, FSaldoConvenio - FTotalComprobante, True, False, True);
            // ----------------------------------------------------------------------------------------------------
		end else begin
			MsgBox(MSG_COMPROBANTE_INEXISTENTE, Caption, MB_ICONERROR);
        end;
	except
		on e: exception do begin
			MsgBoxErr(ERROR_GETTING_INVOICE, e.message, caption, MB_ICONERROR);
		end;
	end;
end;



{-----------------------------------------------------------------------------
  Function Name: btnGenerarOAnularClick
  Author:    mlopez
  Date Created: 06/10/2005
  Description: Se amplio la funcionalidad para que permita tanto la anulacion
  de una Nota de Cobro o la mera Generacion de una Nota de Credito (CK)
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnGenerarOAnularClick(Sender: TObject);
begin
	if (cbTipoComprobante.Value = TC_NOTA_COBRO)
      OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA)
      OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA)
    then
        AnularNotaCobro;

    if cbTipoComprobante.Value = TC_BOLETA then
        AnularBoleta;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    flamas
  Date Created: 21/12/2004
  Description: si tocan la tecla F9 permito anular tambien
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobroCierreConvenio.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
    if (Key = KEY_F9) and btnGenerarOAnular.Enabled then btnGenerarOAnular.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 08/08/2005
  Description: Permito salir del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnSalirClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 08/08/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionNotasCobroCierreConvenio.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos
	FListaTransitosAlCredito.Free;
    FListaCodigoConceptoPeaje.Free;    			//REV.17
    FListaCodigoConceptoPeajeAnterior.Free;     //REV.17
	Action := caFree;
end;


procedure Tfrm_AnulacionNotasCobroCierreConvenio.AnularBoleta;
resourcestring
	MSG_ERROR 				  = 'Error';
	MSG_SUCCESS				  = 'El comprobante %d se proces� correctamente.'#10#13 + 'Se gener� la Nota de Cr�dito %d ';
	MSG_ERROR_VOIDING_INVOICE = 'Error generando la Nota de Cr�dito';
	MSG_CONFIRM				  = 'Desea generar la Nota de Cr�dito ?';
	MSG_CAPTION 			  = 'Generar Nota de Cr�dito a Boleta';
	MSG_IMPRIMIR              = 'Desea imprimir la Nota de Cr�dito ?';
var
	sDescError : string;
	nNotaCredito : Int64;
    NumeroFiscalCredito: int64;
    f: TReporteNotaCreditoElectronicaForm;
    //REV 11
	g: TfrmReporteNC;

begin
 // procedimiento para generar una NC en base a la boleta
	//consulto al operador si esta seguro de realizar la operacion

    //if not ValidarNumeroFiscalNotaCredito then Exit;

	if MsgBox(MSG_CONFIRM, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;

	try
		//mbecerra 24-Junio-2009 DMConnections.BaseCAC.BeginTrans;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spAnulacionNotaCobro');

		//ahora hay que crear los movimientos con los cargos que estan en el cdsCredito
		cdsCredito.First;
		while not cdsCredito.Eof do begin
			spAgregarMovimientoCuentaTemporal.Parameters.Refresh;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConvenio'      ).Value := FCodigoConvenio;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IndiceVehiculo'      ).Value := IIF(cdsCredito.FieldByName('IndiceVehiculo').Value = '' , null, cdsCredito.FieldByName('IndiceVehiculo').Value);
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaHora'           ).Value := FFechaProceso;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConcepto'      ).Value := cdsCredito.FieldByName('CodigoConcepto').AsInteger;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Importe'             ).Value := cdsCredito.FieldByName('Importe').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@ImporteIVA'          ).Value := cdsCredito.FieldByName('ImporteIVA').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@PorcentajeIVA'       ).Value := cdsCredito.FieldByName('PorcentajeIVA').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@EsPago'              ).Value := False;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Observaciones'       ).Value := cdsCredito.FieldByName('Observaciones').AsString;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroPromocion'     ).Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroFinanciamiento').Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IdSesion'            ).Value := FIdSesion ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@TipoComprobante'     ).Value := TipoComprobante; //cbTipoComprobante.Value;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroComprobante'   ).Value := FNumeroComprobante;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaProcesamiento'  ).Value := FFechaProceso;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Usuario'             ).Value := UsuarioSistema;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Seleccionado'        ).Value := False;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroMovimiento'    ).Value := 0;
			spAgregarMovimientoCuentaTemporal.ExecProc;
			cdsCredito.Next;
		end;

        // REV 8: agrego generacion de proceso de facturacion

        with spCrearProcesoFacturacion do begin
        	Parameters.ParamByName('@Operador').Value := UsuarioSistema;
			Parameters.ParamByName('@FacturacionManual').Value := True;
			ExecProc;
        	NumeroProcesoFacturacion := Parameters.ParamByName('@NumeroProcesoFacturacion').Value;
        end;

		with spAnularBoleta do begin
            Parameters.Refresh;
			Parameters.ParamByName('@TipoComprobante').Value := TC_BOLETA;
			Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
			//Parameters.ParamByName('@NumeroFiscalCredito').Value := NULL;//edNumeroNotaCreditoFiscal.ValueInt;
            Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            Parameters.ParamByName('@PasarAEstadoEspecial').Value := false;
            // REV 8
            Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
            Parameters.ParamByName('@NumeroNotaCredito').Value := NULL;
            Parameters.ParamByName('@DescripcionError').Value := NULL;
            Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value := NULL;
            //REV 11
            Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value := NULL;
			ExecProc;
			sDescError := Trim(Parameters.ParamByName('@DescripcionError').Value);
		end;

        LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos

		//Verifico si hubo error al anular
		if (sDescError <> '') then begin
			//informo que hubo un error al anular
			MsgBoxErr(MSG_ERROR, sDescError, MSG_ERROR, MB_ICONERROR);
		end else begin
            //REV 11: lo dejo en la cola de envio a DBNET si el comprobante es electronico
            if spAnularBoleta.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then begin
                //REV 8
				with spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet do begin
					Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
					ExecProc;
                end;
            end;

            //mbecerra: DMConnections.BaseCAC.CommitTrans;
            DMConnections.BaseCAC.Execute('COMMIT TRAN spAnulacionNotaCobro');

            nNotaCredito := spAnularBoleta.Parameters.ParamByName('@NumeroNotaCredito').Value;
            NumeroFiscalCredito := spAnularBoleta.Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value;//edNumeroNotaCreditoFiscal.ValueInt;
			//informo que se anulo con exito
			//MsgBox( Format(MSG_SUCCESS, [FNumeroComprobante, NumeroFiscalCredito]), Caption, MB_ICONINFORMATION);
            MsgBox( Format(MSG_SUCCESS, [edNumeroComprobanteFiscal.ValueInt, NumeroFiscalCredito]), Caption, MB_ICONINFORMATION);
            if (MsgBox( Format(MSG_IMPRIMIR,[FNumeroComprobante, NumeroFiscalCredito]), Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin
                //REV 11: lo dejo en la cola de envio a DBNET si el comprobante es electronico
                if spAnularBoleta.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then begin
                	f:= TReporteNotaCreditoElectronicaForm.Create(self);
                    try
                    	if not f.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO,nNotaCredito,sDescError) then
                        	ShowMessage( sDescError )
                        else
                              f.Ejecutar;
                    finally
                          if Assigned(f) then f.Release;
                    end;
                end
                else begin
                	g:= TfrmReporteNC.Create(self);
                    try
						if not g.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO,nNotaCredito,sDescError) then
								              ShowMessage( sDescError )
                        else
                              g.Ejecutar;
                    finally
                    	if Assigned(g) then g.Release;
                    end;

                end;
            end;
		end;

	except on E: Exception do begin
    		MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
			//mbecerra 24-Junio-2009 DMConnections.BaseCAC.RollbackTrans;
            DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobro');
    	end;
  	end;

	LimpiarDetalles;
	LimpiarDatosCliente;
	LimpiarDatosComprobante;
	edNumeroComprobante.Clear;


end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.AnularNotaCobro;
  {-----------------------------------------------------------------------------
	Function Name: TieneNKMedioPagoAutomatico
	Author:    lgisuk
	Date Created: 09/08/2005
	Description: Verifico si la nota de cobro tiene medio de pago autormatico
	Parameters: Sender: TObject
	Return Value: None
  -----------------------------------------------------------------------------}
  function TieneNKMedioPagoAutomatico(NumeroComprobante : int64) : Boolean;
  begin
	  Result := IIF( QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.TieneNKMedioPagoAutomatico ('+ inttostr(NumeroComprobante) +')') = 'True', True, False);
  end;

  {-----------------------------------------------------------------------------
	Function Name: TieneNKMedioPagoAutomatico
	Author:    lgisuk
	Date Created: 09/08/2005
	Description: Muestro un cartel con el estado de la nota de cobro
	Parameters: Sender: TObject
	Return Value: None
  -----------------------------------------------------------------------------}
  function MostrarEstadoNK (NumeroComprobante : int64) : Boolean;
  resourcestring
	  MSG_ERROR_SHOW_STATE = 'No se pudo mostrar el estado de la nota de cobro';
	  MSG_ERROR = 'Error';
  const
	  THE_DOCUMENT_HAS_AUTOMATIC_PAYMENT = 'El Comprobante Tiene Medio Pago Automatico!';
	  STATE_SENDING      = 'Estado Envio : ';
	  STATE_PAYMENT      = 'Estado Pago : ';
	  DATE_SENDING       = 'Fecha Envio : ';
	  FILE_NAME          = 'Nombre Archivo : ';
	  DESCRIPTION_REJECT = 'Descripci�n Rechazo : ';
	  DATE_REJECT        = 'Fecha Rechazo : ';
	  OBSERVATION_REJECT = 'Observaci�n Rechazo : ';
  var
	  SP: TAdoStoredProc;
	  FechaEnvio, FechaRechazo : AnsiString;
  begin
	  Result := False;
	  try
		  try
			  SP := TADOStoredProc.Create(nil);
			  SP.Connection := DMConnections.BaseCAC;
			  SP.ProcedureName := 'ObtenerEstadoNK';
			  SP.Parameters.Refresh;
			  SP.Parameters.ParamByName('@NumeroComprobante').Value:= NumeroComprobante;
			  SP.Open;
			  if not SP.Eof then begin
				  //Obtengo las fechas
				  FechaEnvio :=  sp.FieldByName('FechaEnvio').AsString;
				  FechaRechazo := sp.FieldByName('FechaRechazo').AsString;
				  //Informo el mensaje
				  MsgBox(THE_DOCUMENT_HAS_AUTOMATIC_PAYMENT + CRLF +
						' ' + CRLF +
						STATE_SENDING         + sp.FieldByName('EstadoEnvio').AsString   + CRLF +
						STATE_PAYMENT         + sp.FieldByName('EstadoPago').AsString    + CRLF +
						DATE_SENDING          + IIF(FechaEnvio = '', 'N/A', FechaEnvio)  + CRLF +
						FILE_NAME             + sp.FieldByName('NombreArchivo').AsString + CRLF +
						DESCRIPTION_REJECT    + sp.FieldByName('DescripcionRechazo').AsString + CRLF +
						DATE_REJECT           + IIF(FechaRechazo = '', 'N/A', FechaRechazo)   + CRLF +
						OBSERVATION_REJECT    + sp.FieldByName('ObservacionRechazo').AsString + CRLF +
						' ',
						Caption,
						MB_ICONWARNING);
			  end;
			  SP.Close;
			  result := true;
		  except
			  on E: Exception do begin
					MsgBoxErr(MSG_ERROR_SHOW_STATE, e.message, caption, MB_ICONERROR);
			  end;
		  end;
	  finally
		  FreeAndNil(SP);
	  end;
  end;

resourcestring
	MSG_ERROR 				  = 'Error';
	MSG_SUCCESS				  = 'El comprobante %d se proces� correctamente.'#10#13 + 'Se gener� la Nota de Cr�dito %d a Nota de Cobro';
	MSG_ERROR_VOIDING_INVOICE = 'Error generando la nota de cr�dito';
	MSG_CONFIRM				  = 'Desea generar la nota de cr�dito ?';
	MSG_CAPTION 			  = 'Anular Nota de Cobro';
	MSG_IMPRIMIR              = 'Desea imprimir la N�ta de Cr�dito ?';
    MSG_DBNET					= 'Error al insertar registro en la Cola de Env�o a DBNet';
var
	sDescError : string;
	nNotaCredito : Int64;
	index: integer;
    f: TReporteNotaCreditoElectronicaForm;
	g: TfrmReporteCK;
    NotaCreditoFiscal: int64;
begin
	//Verifico si la nota de cobro tenia medio de pago automatico vigente confirmado
	if TieneNKMedioPagoAutomatico(FNumeroComprobante) then begin
		//si es asi informo un cartel de advertencia
		MostrarEstadoNK (FNumeroComprobante);
	end;
	//consulto al operador si esta seguro de realizar la operacion
	if MsgBox(MSG_CONFIRM, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;
    if ChequearConcurrencia then Exit; //si otro usuario esta haciendo una CK de esta NK
	try
        // Rev.13 / 27-Agosto-2009 / Nelson Droguett Sierra --------------------------
        if cbTraspasarAjusteSencilloActual.Checked then
        begin
            DMConnections.BaseCAC.Execute('BEGIN TRAN trxTraspasoUltimoAjusteSencillo');
            With spTraspasarUltimoAjusteSencilloConvenioAComprobante do
            begin
                Close;
                PArameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value             := FCodigoConvenio;
                Parameters.ParamByName('@TipoUltimoComprobante').Value      := spObtenerUltimoComprobanteConvenioConTotal.FieldByName('TipoComprobante').AsString;
                Parameters.ParamByName('@NumeroUltimoComprobante').Value    := spObtenerUltimoComprobanteConvenioConTotal.FieldByName('NumeroComprobante').AsInteger;
                Parameters.ParamByName('@TipoComprobante').Value            := TipoComprobante;
                Parameters.ParamByName('@NumeroComprobante').Value          := NumeroComprobante;
                Parameters.ParamByName('@CodigoUsuario').Value              := UsuarioSistema;
                ExecProc;
            end;
            DMConnections.BaseCAC.Execute ('COMMIT TRAN trxTraspasoUltimoAjusteSencillo');
            // Obtengo el convenio nuevamente, para asi tener el nuevo saldo del convenio
            With spObtenerConvenio do
            begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroConvenio').Value :=  spObtenerUltimoComprobanteConvenioConTotal.FieldByName('NumeroConvenio').AsString;
                Open;
                if Not IsEmpty then
                begin
                    FSaldoConvenio := spObtenerConvenio.FieldByName('SaldoConvenio').AsFloat;
                end;
            end;

        end;
        //----------------------------------------------------------------------------


		//Anulo la nota de cobro
		//mbecerra 24-Junio-2009 DMConnections.BaseCAC.BeginTrans;
        DMConnections.BaseCAC.Execute('BEGIN TRAN spAnulacionNotaCobro');

		//Cargar tabla temporal de transitos
		for index := 0 to FListaTransitosAlCredito.Count -1 do begin
			spCargarTransitoEnTablaTemporal.Parameters.Refresh;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@IdSesion'          ).Value := FIdSesion ;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@TipoComprobante'   ).Value := TipoComprobante;//cbTipoComprobante.Value;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@NumeroComprobante' ).Value := NumeroComprobante;//edNumeroComprobante.Text;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@FechaProcesamiento').Value := FFechaProceso;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@Usuario'           ).Value := UsuarioSistema;
			spCargarTransitoEnTablaTemporal.Parameters.ParamByName('@NumCorrCA'         ).Value := StrToInt(FListaTransitosAlCredito.Names[index]);
			spCargarTransitoEnTablaTemporal.ExecProc;
		end;

		//ahora hay que crear los movimientos con los cargos que estan en el cdsCredito
		cdsCredito.First;
		while not cdsCredito.Eof do begin
			spAgregarMovimientoCuentaTemporal.Parameters.Refresh;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConvenio'      ).Value := FCodigoConvenio;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IndiceVehiculo'      ).Value := IIF(cdsCredito.FieldByName('IndiceVehiculo').Value = '' , null, cdsCredito.FieldByName('IndiceVehiculo').Value);
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaHora'           ).Value := NowBase(DMConnections.BaseCAC);
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConcepto'      ).Value := cdsCredito.FieldByName('CodigoConcepto').AsInteger;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Importe'             ).Value := cdsCredito.FieldByName('Importe').AsFloat * 100 ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@ImporteIVA'          ).Value := null ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@PorcentajeIVA'       ).Value := null ;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@EsPago'              ).Value := False;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Observaciones'       ).Value := cdsCredito.FieldByName('Observaciones').AsString;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroPromocion'     ).Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroFinanciamiento').Value := null;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IdSesion'            ).Value := FIdSesion ;
 			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@TipoComprobante'     ).Value := TipoComprobante;//cbTipoComprobante.Value;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroComprobante'   ).Value := NumeroComprobante;//edNumeroComprobante.Text;
			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaProcesamiento'  ).Value := FFechaProceso;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Usuario'             ).Value := UsuarioSistema;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Seleccionado'        ).Value := False;
            spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroMovimiento'    ).Value := 0;
			spAgregarMovimientoCuentaTemporal.ExecProc;
			cdsCredito.Next;
		end;

        //ahora se cargan los movimientos Seleccionados para volver a cargar al convenio para proxima facturacion
        cdsOriginal.First;
        while not cdsOriginal.Eof do begin
            if (cdsOriginal.FieldByName('Seleccionado').Value = True) then begin
				spAgregarMovimientoCuentaTemporal.Parameters.Refresh;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConvenio'      ).Value := FCodigoConvenio;
				spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IndiceVehiculo'      ).Value := IIF(cdsOriginal.FieldByName('IndiceVehiculo').Value = '' , null, cdsOriginal.FieldByName('IndiceVehiculo').Value);
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaHora'           ).Value := NowBase(DMConnections.BaseCAC);
    			spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@CodigoConcepto'      ).Value := cdsOriginal.FieldByName('Concepto').AsInteger;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Importe'             ).Value := cdsOriginal.FieldByName('Importe').AsFloat * 100 ;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@EsPago'              ).Value := False;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Observaciones'       ).Value := cdsOriginal.FieldByName('Observaciones').AsString;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroPromocion'     ).Value := null;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroFinanciamiento').Value := null;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@IdSesion'            ).Value := FIdSesion ;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@TipoComprobante'     ).Value := TipoComprobante; //cbTipoComprobante.Value;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroComprobante'   ).Value := NumeroComprobante; //edNumeroComprobante.Text;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@FechaProcesamiento'  ).Value := FFechaProceso;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Usuario'             ).Value := UsuarioSistema;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@Seleccionado'        ).Value := True;
                spAgregarMovimientoCuentaTemporal.Parameters.ParamByName('@NumeroMovimiento'    ).Value := cdsOriginal.FieldByName('NumeroMovimiento').AsInteger;
                spAgregarMovimientoCuentaTemporal.ExecProc;
            end;
			cdsOriginal.Next;
        end;
        // agrego generacion de proceso de facturacion
        with spCrearProcesoFacturacion do begin
        	Parameters.ParamByName('@Operador').Value := UsuarioSistema;
            Parameters.ParamByName('@FacturacionManual').Value := True;
            ExecProc;
        	NumeroProcesoFacturacion := Parameters.ParamByName('@NumeroProcesoFacturacion').Value;
        end;


		with spAnularNotaCobro do begin
            Parameters.Refresh;
            Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante; //TC_NOTA_COBRO;
            Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante; //FNumeroComprobante;
            Parameters.ParamByName('@PasarAEstadoEspecial').Value := chkPasarTransitoEstadoEspecial.Checked;
            // Revision 6
            Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            // REV 8
            Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
            Parameters.ParamByName('@NumeroNotaCredito').Value := NULL;
            Parameters.ParamByName('@DescripcionError').Value := NULL;
            //REV 11
            Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value := NULL;
            Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value := NULL;

            // Rev.13 / 27-Agosto-2009 / Nelson Droguett Sierra ------------------------------------------------
            Parameters.ParamByName('@GenerarAjusteSencillo').Value := NOT(cbNoRealizarAjusteSencillo.Checked);
            Parameters.ParamByName('@CalcularIntereses').Value := NOT(cbNoGenerarIntereses.Checked);
            //--------------------------------------------------------------------------------------------------
            ExecProc;
            sDescError := Trim(Parameters.ParamByName('@DescripcionError').Value);
		end;

    	LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos

		//Verifico si hubo error al anular
		if (sDescError <> '') then begin
        	//informo que hubo un error al anular
			MsgBoxErr(MSG_ERROR, sDescError, MSG_ERROR, MB_ICONERROR);
        	// si hay un error en el SP hace rollback
        	//mbecerra: 24-Junio-2009 DMConnections.BaseCAC.RollbackTrans;
            DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobro');
		end
        else begin
        	//REV 11: inserto en la cola si la Nota credito es fiscal (electronica)
            if spAnularNotaCobro.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then begin
            	//REV 8
		        with spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet do begin
                	Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
                	ExecProc;
                    //REV.12
                	Index := Parameters.ParamByName('@RETURN_VALUE').Value;
                    if Index < 0 then begin
        				MsgBox(MSG_DBNET, Caption, MB_ICONERROR);
                        DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobro');
                        Exit;
					end;
                    //FIN REV.12

            	end;
         	end;

			//mbecerra 24-Junio-2009 DMConnections.BaseCAC.CommitTrans;
            DMConnections.BaseCAC.Execute ('COMMIT TRAN spAnulacionNotaCobro');

        	nNotaCredito := spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCredito').Value;
        	NotaCreditoFiscal := spAnularNotaCobro.Parameters.ParamByName('@NumeroNotaCreditoFiscal').Value;

			 //informo que se anulo con exito
       		if (edNumeroComprobante.Enabled = true) then
            	MsgBox( Format(MSG_SUCCESS, [edNumeroComprobante.ValueInt, NotaCreditoFiscal]), Caption, MB_ICONINFORMATION)
       		else
            	MsgBox( Format(MSG_SUCCESS, [edNumeroComprobanteFiscal.ValueInt, NotaCreditoFiscal]), Caption, MB_ICONINFORMATION);

			if (MsgBox( Format(MSG_IMPRIMIR,[FNumeroComprobante, nNotaCredito]), Caption, MB_YESNO + MB_ICONQUESTION) = ID_YES) then begin
            	//REV 11: si la nota de credito es fiscal (electronica), imprimo con el formulario para comprobantes electronicos
				if spAnularNotaCobro.Parameters.ParamByName('@ComprobanteDebitoEsFiscal').Value = true then begin
                	f:= TReporteNotaCreditoElectronicaForm.Create(nil);
                    try
                    	if not f.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO_A_COBRO,nNotaCredito,sDescError) then
							ShowMessage( sDescError )
                      	else
                          	f.Ejecutar;
                    finally
                    	if Assigned(f) then f.Release;

                    end;

            	end
                else begin
					//Imprimo con el formulario de comprobantes no electr�nicos
                    g:= TfrmReporteCK.Create(nil);
                  	try
						if not g.Inicializar(DMConnections.BaseCAC,TC_NOTA_CREDITO_A_COBRO,nNotaCredito,sDescError) then
                    		ShowMessage( sDescError )
                    	else
                    		g.Ejecutar;
                  	finally
                    	if Assigned(g) then g.Release;
                    end;
            	end;
			end;
		end;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
			      //mbecerra 24-Junio-2009 DMConnections.BaseCAC.RollbackTrans;
                  DMConnections.BaseCAC.Execute('ROLLBACK TRAN spAnulacionNotaCobro');
        end;
    end;

	LimpiarDetalles;
	LimpiarDatosCliente;
	LimpiarDatosComprobante;
    if edNumeroComprobante.Enabled = true then
	    edNumeroComprobante.Clear
    else
        edNumeroComprobanteFiscal.Clear;
    chkPasarTransitoEstadoEspecial.Checked := false;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.LimpiarDatosCliente;
begin
	lblNombre.Caption       := EmptyStr;
	lblDomicilio.Caption    := EmptyStr;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.LimpiarDatosComprobante;
begin
	lblFecha.Caption            := EmptyStr;
	lblTotalAPagar.Caption      := EmptyStr;
	lblFechaVencimiento.Caption := EmptyStr;
	lblEstado.Caption           := EmptyStr;
    lblValorNumeroConvenio.Caption  := EmptyStr;
    lblValorRut.Caption             := EmptyStr;
    cdsOriginal.EmptyDataSet ;
    lblImporteTotalNotaCredito.Caption := EmptyStr;
    lblTotalCKsPreviasANK.Visible           := False;
    lblTotalCKsPreviasANKText.Visible       := False;
    lblMaximoTotalComprobanteCKText.Visible := False;
    lblMaximoTotalComprobanteCK.Visible     := False;
    // Rev.13 / 29-Agosto-2009 / Nelson Droguett Sierra ----------------------------------------------------------
    cbNoRealizarAjusteSencillo.Enabled      :=True;
    cbTraspasarAjusteSencilloActual.Enabled :=False;
    cbNoRealizarAjusteSencillo.Checked      :=True;
    cbTraspasarAjusteSencilloActual.Checked :=False;
    //-------------------------------------------------------------------------------------------------------------
    // Rev.14 / 15-Septiembre-2009 / Nelson Droguett Sierra -------------------------------------------------------
    lblUltimoAjusteSencillo.Visible:=True;
    lblUltimoAjusteSencillo.Caption:='Ultimo ajuste sencillo:'+
                    FormatearImporteConDecimales(DMConnections.BaseCAC, 0, True, False, True);

    lblSaldoConvenio.Visible:=True;
    lblSaldoConvenio.Caption := 'Saldo del Convenio   :'+
                    FormatearImporteConDecimales(DMConnections.BaseCAC, 0,True, False, True);

    lblSugerida.Visible:=True;
    lblSugerida.Caption := 'Devoluci�n Sugerida :'+
                    FormatearImporteConDecimales(DMConnections.BaseCAC, 0, True, False, True);
    //-------------------------------------------------------------------------------------------------------------
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.CargarTiposComprobantes;
begin
	cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_NOTA_COBRO),TC_NOTA_COBRO);
	cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_BOLETA),TC_BOLETA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_FACTURA_EXENTA),TC_FACTURA_EXENTA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_FACTURA_AFECTA),TC_FACTURA_AFECTA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_BOLETA_EXENTA),TC_BOLETA_EXENTA);
    cbTipoComprobante.Items.Add(DescriTipoComprobante(TC_BOLETA_AFECTA),TC_BOLETA_AFECTA);
	cbTipoComprobante.ItemIndex := 0;
    cbImpresorasFiscales.Enabled := False;
    edNumeroComprobanteFiscal.Enabled := False ;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnBuscarClick(Sender: TObject);
resourcestring
	MSG_NUMERO_INVALIDO = 'N�mero de comprobante inv�lido';
    // Rev.14 / 15-Septiembre-2009 / Nelson Droguett Sierra -------------------------------------------------------
    MSG_ULTIMOS_CONSUMOS_SIN_FACTURAR = 'El Convenio tiene �ltimos consumos sin facturar. No se puede hacer la Nota de Cr�dito por cierre de convenio.';
var
    // Rev.14 / 15-Septiembre-2009 / Nelson Droguett Sierra -------------------------------------------------------
    TieneSaldo : Boolean;
begin
    // Rev.13 / 29-Agosto-2009 / Nelson Droguett Sierra ----------------------------------------------------------
    cbNoRealizarAjusteSencillo.Enabled      :=True;
    cbTraspasarAjusteSencilloActual.Enabled :=True;
    cbNoRealizarAjusteSencillo.Checked      :=True;
    cbTraspasarAjusteSencilloActual.Checked :=False;
    Application.ProcessMessages;
    //-------------------------------------------------------------------------------------------------------------
    if FNumeroComprobante <> 0 then LimpiarDatosTemporales; //se limpian las tablas con los datos temporales de Movimientos y Transitos
    if cbTipoComprobante.Value = TC_NOTA_COBRO then
    	if not ValidateControls([edNumeroComprobante],[edNumeroComprobante.Text<>''],caption,[MSG_NUMERO_INVALIDO]) then  Exit;
    if cbTipoComprobante.Value = TC_BOLETA then
    	if not ValidateControls([edNumeroComprobanteFiscal],[edNumeroComprobanteFiscal.Text<>''],caption,[MSG_NUMERO_INVALIDO]) then  Exit;

    if (cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR (cbTipoComprobante.Value = TC_FACTURA_EXENTA)
        OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA) OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) then
    	if not ValidateControls([edNumeroComprobanteFiscal],[edNumeroComprobanteFiscal.Text<>''],caption,[MSG_NUMERO_INVALIDO]) then  Exit;

    if ChequearConcurrencia then Exit;

    CargarComprobantes;
    // Rev.14 / 15-Septiembre-2009 / Nelson Droguett Sierra -------------------------------------------------------
    if TieneUltimosConsumosSinFacturar(FCodigoConvenio) then
    begin
        MsgBox(MSG_ULTIMOS_CONSUMOS_SIN_FACTURAR, 'Nota de Cr�dito para Cierre Convenio', MB_ICONWARNING);
        LimpiarDetalles;
        LimpiarDatosCliente;
        LimpiarDatosComprobante;
        if edNumeroComprobante.Enabled = true then
            edNumeroComprobante.Clear
        else
            edNumeroComprobanteFiscal.Clear;
        chkPasarTransitoEstadoEspecial.Checked := false;
        cbTipoComprobante.SetFocus;
    end
    // -------------------------------------------------------------------------------------------------------------
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.CargarDetalleComprobante;
resourcestring
    MSG_IVA_VENCIDO_MAXIMO_CREDITO = 'El importe m�ximo del cr�dito deber� ser de %s por estar vencido el IVA' +  CRLF +
                                     'ya que han transcurrido mas de %d d�as desde la emisi�n de la boleta el d�a %s';

    MSG_IVA_VENCIDO_CREDITO_NEGADO = 'Han transcurrido mas de %d d�as desde la emisi�n de la boleta el d�a %s' +  CRLF +
                                     'y los cr�ditos previos a la boleta superan el neto. No se puede realizar otro cr�dito';

var
  xIndex: integer;
  TotalCKsPreviasANK: double;
  CantidadCKsPreviasANK: integer;
begin
	spObtenerCargosComprobanteAAnular.Close;
	spObtenerCargosComprobanteAAnular.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;//cbTipoComprobante.Value;
	spObtenerCargosComprobanteAAnular.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
	spObtenerCargosComprobanteAAnular.Open;
	xIndex := 1;
	FTotalComprobante := 0 ;
    FTotalComprobanteNeto := 0 ;
	cdsOriginal.EmptyDataSet;
	while not spObtenerCargosComprobanteAAnular.Eof do begin
		cdsOriginal.Append;
        cdsOriginal.FieldByName('Concepto').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('CodigoConcepto').AsString ;
        cdsOriginal.FieldByName('Importe').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat ;
        cdsOriginal.FieldByName('DescriImporte').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('DescriImporte').AsString ;

        if (cbTipoComprobante.Value = TC_BOLETA) OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA) OR  (cbTipoComprobante.Value = TC_BOLETA_AFECTA) then begin
        	cdsOriginal.FieldByName('ValorNeto').Value 			:= 	spObtenerCargosComprobanteAAnular.FieldByName('ValorNeto').AsFloat ;
            cdsOriginal.FieldByName('DescriValorNeto').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('DescriValorNeto').AsString ;
            cdsOriginal.FieldByName('ImporteIVA').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('ImporteIVA').AsFloat ;
            cdsOriginal.FieldByName('DescriImporteIVA').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('DescriImporteIVA').AsString ;

            cdsOriginal.FieldByName('PorcentajeIVA').Value 		:= 	spObtenerCargosComprobanteAAnular.FieldByName('PorcentajeIVA').AsFloat ;
              //el porcentaje es el que se levanta, si no se edita el registro sigue siendo el mismo.
              //esto puede ser malo o bueno al momento del cambio del IVA
              //si hiciera falta hacer hoy un credito con el mismo porcentaje de IVA del pasado, entonces esta bueno
              //si el cr�dito siempre debiera hacerse con el IVA nuevo, entonces los cargos deberian editarse para que tomen el nuevo IVA
        end;

        cdsOriginal.FieldByName('Descripcion').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('Descripcion').AsString ;
        cdsOriginal.FieldByName('Observaciones').Value 	:= 	spObtenerCargosComprobanteAAnular.FieldByName('Observaciones').AsString ;
        cdsOriginal.FieldByName('NumeroMovimiento').Value := spObtenerCargosComprobanteAAnular.FieldByName('NumeroMovimiento').AsInteger ;
        cdsOriginal.FieldByName('IndiceVehiculo').Value := IIF(spObtenerCargosComprobanteAAnular.FieldByName('IndiceVehiculo').IsNull, ' ' ,spObtenerCargosComprobanteAAnular.FieldByName('IndiceVehiculo').AsString ) ;
        cdsOriginal.FieldByName('NumeroLinea').Value 	:= 	xIndex ;
        cdsOriginal.FieldByName('Pasado').Value := 	'N';

        FTotalComprobante := FTotalComprobante + spObtenerCargosComprobanteAAnular.FieldByName('Importe').AsFloat;
        //if (cbTipoComprobante.Value = TC_BOLETA) and FNoSePuedeAcreditarIVA
        //then
        //FTotalComprobanteNeto := FTotalComprobanteNeto + spObtenerCargosComprobanteAAnular.FieldByName('ValorNeto').AsFloat;

        Inc(xIndex);
		spObtenerCargosComprobanteAAnular.Next;
	end;
    cdsOriginal.First;  //porque si no queda en eof a pesar de haber cargado filas, y los botones de agregar y modificar no funcionan bien

    TotalCKsPreviasANK      := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerImporteCanceladoDeCKaUnaNK(''%s'',%d)',[TipoComprobante,FNumeroComprobante])) / 100 ;
    CantidadCKsPreviasANK   := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerCantidadDeCKaUnaNK(''%s'',%d)',[TipoComprobante,FNumeroComprobante])) ;
    //if (cbTipoComprobante.Value = TC_BOLETA) and FNoSePuedeAcreditarIVA
    //then
    //    FMaximoTotalComprobanteCK  := FTotalComprobanteNeto - TotalCKsPreviasANK
    //else

    // Rev.13 / 28-Agosto-2009 / Nelson Droguett Sierra -----------------------------
    if cbTraspasarAjusteSencilloActual.Checked then
    begin
        FMaximoTotalComprobanteCK  := (FTotalComprobante - TotalCKsPreviasANK) + spObtenerUltimoComprobanteConvenioConTotal.FieldByName('AjusteSencilloActual').AsFloat ;
    end
    else
    begin
        FMaximoTotalComprobanteCK  := FTotalComprobante - TotalCKsPreviasANK ;
    end;
    //--------------------------------------------------------------------------------


    if FMaximoTotalComprobanteCK <= 0 then  //si ya no se puede acreditar el IVA y se habi�n hecho cr�ditos por un importe superior al neto de la boleta, quedar�a negativo
        FMaximoTotalComprobanteCK := 0;

    lblTotalCKsPreviasANK.Caption           := FormatearImporteConDecimales(DMConnections.BaseCAC, TotalCKsPreviasANK, True, False, True);
    lblTotalCKsPreviasANKText.Caption       := 'Total ' + IntToStr(CantidadCKsPreviasANK) + ' Cr�ditos previos :' ;
	lblTotalAPagar.Caption                  := FormatearImporteConDecimales(DMConnections.BaseCAC, FTotalComprobante, True, False, True);
    lblMaximoTotalComprobanteCK.Caption     := FormatearImporte(DMConnections.BaseCAC, Trunc(FMaximoTotalComprobanteCK));
    lblTotalCKsPreviasANK.Visible           := (CantidadCKsPreviasANK > 0);
    lblTotalCKsPreviasANKText.Visible       := (CantidadCKsPreviasANK > 0);
    lblMaximoTotalComprobanteCKText.Visible := (CantidadCKsPreviasANK > 0);
    lblMaximoTotalComprobanteCK.Visible     := (CantidadCKsPreviasANK > 0);
    {if FNoSePuedeAcreditarIVA then begin
        lblMaximoTotalComprobanteCKText.Visible := True;
        lblMaximoTotalComprobanteCK.Visible     := True;
        if FMaximoTotalComprobanteCK = 0 then MsgBox(format(MSG_IVA_VENCIDO_CREDITO_NEGADO , [FCantidadDiasCreditosConIVA, lblFecha.Caption]))
            else MsgBox(format(MSG_IVA_VENCIDO_MAXIMO_CREDITO , [lblMaximoTotalComprobanteCK.Caption, FCantidadDiasCreditosConIVA, lblFecha.Caption]));
    end;}
end;


procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnPasarConceptoClick(Sender: TObject);
begin
	PasarConcepto;
	UpdateImporteTotalCredito;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnPasarTodosClick(Sender: TObject);
begin
	cdsOriginal.First;
	while not cdsOriginal.Eof do begin
		PasarConcepto;
		cdsOriginal.Next;
	end;
	cdsOriginal.First;
	UpdateImporteTotalCredito;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnEliminarConceptoClick(Sender: TObject);
var
    Posicion, indice: integer;
    CodigoConceptoStr : string;		//REV.17
begin
    if cdsCredito.Eof then Exit;

    CodigoConceptoStr := cdsCredito.FieldByName('CodigoConceptoOriginal').AsString;  	//REV.17
    
    // Rev.15 / 17-Noviembre-2009 / Nelson Droguett Sierra. --------------------------------
    // Limpia la lista de los Transitos seleccionados, si el concepto eliminado es de peajes.
	{----------------REV.17
	if (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeaje) or
		(cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeajePeriodoAnterior) then
    ------------------}
    if	(FListaCodigoConceptoPeaje.IndexOf(CodigoConceptoStr) >= 0) or
    	(FListaCodigoConceptoPeajeAnterior.IndexOf(CodigoConceptoStr) >= 0) then
    begin
    // Rev.17 / 17-Diciembre-2009 /  vpaszkowicz. --------------------------------
    // Borra de la lista de los Transitos seleccionados los numcorrCa asociados al numero de movimiento
    // si el concepto eliminado es de peajes. El mov ya se estaba guardando en el StringList separado del NumCorrCa por el signo =
        //FListaTransitosAlCredito.Clear;
        for Indice := FListaTransitosAlCredito.Count - 1 downto 0 do begin
            Posicion := Pos('=', FListaTransitosAlCredito[indice]);
            If StrToInt(Copy(FListaTransitosAlCredito[indice], Posicion + 1, Length(FListaTransitosAlCredito[indice]) - Posicion)) = cdsCredito.FieldByName('NumeroMovimiento').Value then
                FListaTransitosAlCredito.Delete(Indice);
        end;
    end;
    //--------------------------------------------------------------------------------------
	if cdsCredito.FieldByName('NroOriginal').Value <> null then
		if cdsOriginal.FindKey([cdsCredito.FieldByName('NroOriginal').Value]) then begin
		cdsOriginal.Edit;
		cdsOriginal.FieldByName('Pasado').Value := 'N';
		cdsOriginal.Post;
	end;
	cdsCredito.Delete;
	UpdateImporteTotalCredito;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnAgregarNuevoConceptoClick(Sender: TObject);
var
  f: TfrmCargoNotaCredito;
begin
    if cdsOriginal.eof then Exit;
	f := TfrmCargoNotaCredito.Create(Application);
	f.Visible := False;
	if f.Inicializar(false ,false , false , QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO()') , ObtenerIVA , FFechaProceso, FNoSePuedeAcreditarIVA) then
    	if f.ShowModal = mrOK then begin
			cdsCredito.Append;
			cdsCredito.FieldByName('CodigoConcepto').Value 	:= f.cbConcepto.Value;
			cdsCredito.FieldByName('Descripcion').Value 	:= f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
			cdsCredito.FieldByName('DescriImporte').Value 	:= FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value + f.edImporteIVA.Value, True, False, True);
			cdsCredito.FieldByName('Importe').Value 		:= f.edImporte.Value + f.edImporteIVA.Value;
			cdsCredito.FieldByName('NroOriginal').Value 	:= 0;
			cdsCredito.FieldByName('Observaciones').Value 	:= f.edDetalle.Text;
			cdsCredito.FieldByName('ImporteOriginal').Value := 0; //el nuevo valor no puede superar el original
			cdsCredito.FieldByName('CodigoConceptoOriginal').Value 	:= 0;
            cdsCredito.FieldByName('ImporteIVA').Value      := f.edImporteIVA.Value;
            cdsCredito.FieldByName('ValorNeto').Value       := f.edImporte.Value ;
            cdsCredito.FieldByName('PorcentajeIVA').Value   := f.edPorcentajeIVA.Value;
            cdsCredito.FieldByName('DescriImporteIVA').Value      := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporteIVA.Value, True, False, True);
            cdsCredito.FieldByName('DescriValorNeto').Value       := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value , True, False, True);
            cdsCredito.FieldByName('DescriPorcentajeIVA').Value   := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edPorcentajeIVA.Value, True, False, True);
			cdsCredito.Post;
		end;
    f.Release;
	UpdateImporteTotalCredito;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.btnModificarConceptoClick(Sender: TObject);
var
  f: TfrmCargoNotaCredito;
  ImporteTransitosSeleccionados: int64;
  SelectedAll: boolean;
  CodigoConceptoStr : string;		//REV.17
begin

    if cdsCredito.Eof then Exit;

	CodigoConceptoStr := cdsCredito.FieldByName('CodigoConceptoOriginal').AsString;  	//REV.17
    {---------REV.17
	if (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeaje) or
		(cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeajePeriodoAnterior) then begin
    -----------}
    if	(FListaCodigoConceptoPeaje.IndexOf(CodigoConceptoStr) >= 0) or
    	(FListaCodigoConceptoPeajeAnterior.IndexOf(CodigoConceptoStr) >= 0) then begin
        
		cdsCredito.Edit;
		SelectedAll := False;
		//si de un movimientoCuenta se seleccionan todo los transitos, el importe del movimiento no va a coincidir con la
		//sumatoria de los importes de los transitos, por cuestiones de redondeo. Si se detecta que son todos los transitos,
        //entonces no se usa la suma de importes.
        //Hay que calcular la diferencia entre el importe del Movimiento de la NK y los Movimientos de las CK que lo fueron anulando
		ImporteTransitosSeleccionados:= SeleccionarTransitosAAnular(cdsCredito.FieldByName('NumeroMovimiento').Value , SelectedAll);
    //REV 9
		//if SelectedAll then
		//	cdsCredito.FieldByName('Importe').Value :=  cdsCredito.FieldByName('ImporteOriginal').Value - ObtenerImporteRemanente(cdsCredito.FieldByName('CodigoConceptoOriginal').Value)
		//else
    if ImporteTransitosSeleccionados <= cdsCredito.FieldByName('ImporteOriginal').Value then
        cdsCredito.FieldByName('Importe').Value := ImporteTransitosSeleccionados
    else
        cdsCredito.FieldByName('Importe').Value := cdsCredito.FieldByName('ImporteOriginal').Value;
    //Fin REV 9

		cdsCredito.FieldByName('DescriImporte').Value := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('Importe').Value, True, False, True);
		cdsCredito.Post;
	end else
	begin
		f := TfrmCargoNotaCredito.Create(Application);
		f.Visible := False;
		if f.Inicializar(false ,
                        false,
                        false,
                        cdsCredito.FieldByName('CodigoConceptoOriginal').Value = 0,
                        cdsCredito.FieldByName('CodigoConcepto').Value,
                        cdsCredito.FieldByName('Importe').Value,
                        cdsCredito.FieldByName('ImporteOriginal').Value,
                        cdsCredito.FieldByName('Observaciones').Value,
                        cdsCredito.FieldByName('Descripcion').Value,
                        QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO()') ,
                        ObtenerIVA ,
                        FFechaProceso,
                        FNoSePuedeAcreditarIVA) then begin
			if f.ShowModal = mrOK then begin
				cdsCredito.Edit;
				cdsCredito.FieldByName('DescriImporte').Value 	:= FormatearImporteConDecimales(DMConnections.BaseCAC,f.edImporte.Value + f.edImporteIVA.Value, True, False, True);
				cdsCredito.FieldByName('Importe').Value 		:= f.edImporte.Value + f.edImporteIVA.Value;
				cdsCredito.FieldByName('Observaciones').Value 	:= f.edDetalle.Text;
                cdsCredito.FieldByName('ImporteIVA').Value      := f.edImporteIVA.Value;
                cdsCredito.FieldByName('ValorNeto').Value       := f.edImporte.Value  ;
                cdsCredito.FieldByName('PorcentajeIVA').Value   := f.edPorcentajeIVA.Value;
                cdsCredito.FieldByName('DescriImporteIVA').Value      := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporteIVA.Value, True, False, True);
                cdsCredito.FieldByName('DescriValorNeto').Value       := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value , True, False, True);
                cdsCredito.FieldByName('DescriPorcentajeIVA').Value   := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edPorcentajeIVA.Value, True, False, True);
				cdsCredito.Post;
			end;
		end;
        f.Release;
	end;
	UpdateImporteTotalCredito;
end;

function Tfrm_AnulacionNotasCobroCierreConvenio.ObtenerImporteRemanente( aCodigoConcepto:integer ): double;
begin
//si se seleccionaron todos los transitos, entonces el precio del movimiento cuenta ser� la diferencia
//del movimiento de la NK menos la suma de los movimientos de las CK que los anularon.

    //se le pasa el Nro de NK y por ejemplo 1 por Peajes. el SP buscar el inverso
    spObtenerImporteTransitosAcreditados.Parameters.Refresh;
    spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;//edNumeroComprobante.Text;
    spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@CodigoConcepto').Value := aCodigoConcepto;
    spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@Importe').Value := null;
    spObtenerImporteTransitosAcreditados.ExecProc ;
    Result := spObtenerImporteTransitosAcreditados.Parameters.ParamByName('@Importe').Value ;
    Result := Result * (-1) / 100 ;
    //el store los devuelve como negativos, pero queda mas claro devolverlos como positivos, as� despues cuando se usa
    //la funcion se debe una resta del importe original del Movimiento y el resultado de esta funcion

end;

function Tfrm_AnulacionNotasCobroCierreConvenio.SeleccionarTransitosAAnular( aNumeroMovimiento : integer; var SelectedAll:boolean): int64;
var
	f2: TFormConsultaTransitosComprobanteAnular;
	ImporteTotal: integer;
begin
	ImporteTotal := 0;
	Result := ImporteTotal;
	f2 := TFormConsultaTransitosComprobanteAnular.Create(Application);
	f2.Visible := False;
	if f2.Inicializar(aNumeroMovimiento, True, FListaTransitosAlCredito) then begin
		f2.ShowModal;
		Result := f2.ImporteTotal;
		SelectedAll := f2.SelectedAll;
		f2.Release;
	end;
	//la lista contiene ahora los numCorrCA de los tr�nsitos a meter en el credito
end;

function Tfrm_AnulacionNotasCobroCierreConvenio.TieneUltimosConsumosSinFacturar(
  CodigoConvenio: Integer): Boolean;
resourcestring
    MSG_ERROR = 'Error al obtener si tiene ultimos consumos sin facturar';
var
    Total : INT64;
begin
    Result := False;
    try
        with spObtenerUltimosConsumos do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            Parameters.ParamByName('@IndiceVehiculo').Value := NULL;
            Open;
            //Inicializo
            Total := 0;
            //Sumo todos los ultimos consumos
            while not Eof do begin
                Total := Total + FieldByName('Importe').asVariant;
                Next;
            end;
            //si el total es positivo, entonces tiene ultimos consumos
            if Total > 0 then Result := True;
        end;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, Caption, MB_ICONSTOP);
        end;
    end;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.UpdateImporteTotalCredito;
resourcestring
	ERROR_IMPORTE_CREDITO_MAYOR_A_ORIGINAL = 'El importe del cr�dito a generar supera al importe del comprobante original o del saldo del convenio (%0f)';
var
  Bookmark: TBookmark;
  ImporteTotal: double;
begin
	Bookmark := cdsCredito.GetBookmark;
	cdsCredito.DisableControls;
	cdsCredito.First;
	ImporteTotal := 0;
	while not cdsCredito.eof do begin
		ImporteTotal := ImporteTotal + cdsCredito.FieldByName('Importe').AsFloat  ;
		cdsCredito.Next;
	end;
	lblImporteTotalNotaCredito.Caption := FormatearImporteConDecimales(DMConnections.BaseCAC, ImporteTotal, True, False, True);
	cdsCredito.GotoBookmark(Bookmark);
	cdsCredito.EnableControls;
	cdsCredito.FreeBookmark(BookMark);
	btnGenerarOAnular.Enabled := ((ImporteTotal <= Max(FMaximoTotalComprobanteCK,FSaldoConvenio))) and (ImporteTotal > 0) ;
	ValidateControls(
			[lblImporteTotalNotaCredito],
			[(ImporteTotal <= Max(FMaximoTotalComprobanteCK,FSaldoConvenio)) ],
			caption,
			[Format(ERROR_IMPORTE_CREDITO_MAYOR_A_ORIGINAL,[Max(FMaximoTotalComprobanteCK,FSaldoConvenio)])]) ;
	if FBotonBloqueadoPorError then
		btnGenerarOAnular.Enabled := False;


end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.PasarConcepto;
resourcestring
	ERROR_GETTING_CHARGE =  'Error grave. No se ha encontrado Concepto que anula al Concepto %s' + CRLF +
							            'Deber� configurarse el concepto con otro que lo anule.' + CRLF +
							            'Ya no podr� seguir trabajando con esta Nota de Cobro' ;
  MSG_ERROR         = 'Ha ocurrido un error ejecutado el SP ObtenerImporteAnuladoConceptoComprobanteAAnular';
  MSG_WNG_CONCEPTO  = 'El concepto ya fue anulado en su totalidad, no puede ser utilizado en una Nota de Cr�dito para este Comprobante';

var
  ImporteAnuladoConcepto : Int64;
  CodigoConceptoStr : string;			//REV.17
begin
	if cdsOriginal.FieldByName('Pasado').Value = 'N' then begin
		//En base al concepto original, encontrar su contraparte.
		spObtenerConceptoQueAnula.Parameters.ParamByName('@CodigoConcepto').Value := cdsOriginal.FieldByName('Concepto').Value;
		spObtenerConceptoQueAnula.Open ;
		if spObtenerConceptoQueAnula.Eof then begin
			MsgBoxErr(Format(ERROR_GETTING_CHARGE,[cdsOriginal.FieldByName('Descripcion').AsString] ), caption, caption, MB_ICONERROR);
			spObtenerConceptoQueAnula.Close ;
			FBotonBloqueadoPorError:= true;
			btnGenerarOAnular.Enabled := False;
			exit;
		end;
    //REV 10
    try
      spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.Refresh;
      spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@TipoComprobante').Value   := TipoComprobante;
      spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
      spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@CodigoConcepto').Value    := spObtenerConceptoQueAnula.FieldByName('CodigoConcepto').Value;
      spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@ImporteConcepto').Value   := null;
      spObtenerImporteAnuladoConceptoComprobanteAAnular.ExecProc;
      ImporteAnuladoConcepto := spObtenerImporteAnuladoConceptoComprobanteAAnular.Parameters.ParamByName('@ImporteConcepto').Value;
    except
			  on E: Exception do begin
					MsgBoxErr(MSG_ERROR, e.message, caption, MB_ICONERROR);
			  end;
    end;
    //REV 10
    if (cdsOriginal.FieldByName('Importe').Value <= ImporteAnuladoConcepto) then begin
			  MsgBox(MSG_WNG_CONCEPTO, caption, MB_ICONWARNING);
			  spObtenerConceptoQueAnula.Close ;
			  btnGenerarOAnular.Enabled := False;
    end else begin
		    cdsOriginal.Edit;
		    cdsOriginal.FieldByName('Pasado').Value := 'S';
		    cdsOriginal.Post;

		    cdsCredito.Append;
		    cdsCredito.FieldByName('CodigoConcepto').Value 		:= spObtenerConceptoQueAnula.FieldByName('CodigoConcepto').AsString ;
		    cdsCredito.FieldByName('Descripcion').Value 		:= spObtenerConceptoQueAnula.FieldByName('Descripcion').Value;
		    cdsCredito.FieldByName('CodigoConceptoOriginal').Value 	:= cdsOriginal.FieldByName('Concepto').Value;

		    {-----------REV.17
		    if (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeaje) or
			      (cdsCredito.FieldByName('CodigoConceptoOriginal').Value = FCodigoConceptoPeajePeriodoAnterior) then begin
            }

			CodigoConceptoStr := cdsCredito.FieldByName('CodigoConceptoOriginal').AsString;  	//REV.17
    		if	(FListaCodigoConceptoPeaje.IndexOf(CodigoConceptoStr) >= 0) or
		    	(FListaCodigoConceptoPeajeAnterior.IndexOf(CodigoConceptoStr) >= 0) then begin
			      //los conceptos de peaje los paso con importe cero, porque el importe depende de los transitos que se seleccionen
			      cdsCredito.FieldByName('DescriImporte').Value 		:= '';
			      cdsCredito.FieldByName('Importe').Value 			:= 0;
		    end else begin
			        //cdsCredito.FieldByName('DescriImporte').Value 		:= cdsOriginal.FieldByName('DescriImporte').Value;
			        //cdsCredito.FieldByName('Importe').Value 			:= cdsOriginal.FieldByName('Importe').Value;

              // REV 10: dejo como importe la diferencia entre el importe del concepto y lo anulado hasta ahora
              if (cdsOriginal.FieldByName('Importe').Value > ImporteAnuladoConcepto) then begin
    		          cdsCredito.FieldByName('Importe').Value := cdsOriginal.FieldByName('Importe').Value - ImporteAnuladoConcepto; //el nuevo valor no puede superar el original
                  cdsCredito.FieldByName('DescriImporte').Value := FormatearImporteConDecimales( DMConnections.BaseCAC , cdsCredito.FieldByName('Importe').Value, True, False, True);
              end else begin
                    cdsCredito.FieldByName('Importe').Value := 0;
                  cdsCredito.FieldByName('DescriImporte').Value := '';
              end;

        end;
        cdsCredito.FieldByName('NroOriginal').Value 		:= cdsOriginal.FieldByName('NumeroLinea').Value;
		    cdsCredito.FieldByName('Observaciones').Value 		:= cdsOriginal.FieldByName('Observaciones').Value;
		    cdsCredito.FieldByName('NumeroMovimiento').Value 	:= cdsOriginal.FieldByName('NumeroMovimiento').Value;
        // REV 10: dejo como importe original la diferencia entre el importe del concepto y lo anulado hasta ahora
        if cdsOriginal.FieldByName('Importe').Value > ImporteAnuladoConcepto then
    		      cdsCredito.FieldByName('ImporteOriginal').Value := cdsOriginal.FieldByName('Importe').Value - ImporteAnuladoConcepto //el nuevo valor no puede superar el original
        else
              cdsCredito.FieldByName('ImporteOriginal').Value := 0;

		    cdsCredito.FieldByName('IndiceVehiculo').Value 		:= cdsOriginal.FieldByName('IndiceVehiculo').Value;
        cdsCredito.FieldByName('ImporteIVA').Value          := cdsOriginal.FieldByName('ImporteIVA').Value;
        cdsCredito.FieldByName('ValorNeto').Value           := cdsOriginal.FieldByName('ValorNeto').Value;
        cdsCredito.FieldByName('PorcentajeIVA').Value       := cdsOriginal.FieldByName('PorcentajeIVA').Value;
        cdsCredito.FieldByName('DescriImporteIVA').Value      := cdsOriginal.FieldByName('DescriImporteIVA').Value;
        cdsCredito.FieldByName('DescriValorNeto').Value       := cdsOriginal.FieldByName('DescriValorNeto').Value;
        cdsCredito.FieldByName('DescriPorcentajeIVA').Value   := cdsOriginal.FieldByName('DescriPorcentajeIVA').Value;

        {if FNoSePuedeAcreditarIVA then begin
			      cdsCredito.FieldByName('DescriImporte').Value 		    := cdsOriginal.FieldByName('DescriValorNeto').Value;
			      cdsCredito.FieldByName('Importe').Value 			    := cdsOriginal.FieldByName('ValorNeto').Value;
            cdsCredito.FieldByName('ImporteOriginal').Value 	    := cdsOriginal.FieldByName('ValorNeto').Value;
            cdsCredito.FieldByName('ImporteIVA').Value              := 0;
            cdsCredito.FieldByName('ValorNeto').Value               := cdsOriginal.FieldByName('ValorNeto').Value;
            cdsCredito.FieldByName('PorcentajeIVA').Value           := 0;
            cdsCredito.FieldByName('DescriImporteIVA').Value        := '0';
            cdsCredito.FieldByName('DescriValorNeto').Value         := cdsOriginal.FieldByName('DescriValorNeto').Value;
            cdsCredito.FieldByName('DescriPorcentajeIVA').Value     := '0';
        end;}

		    cdsCredito.Post;
		    spObtenerConceptoQueAnula.Close;
        UpdateImporteTotalCredito;
    end;// else
	end;

end;

function Tfrm_AnulacionNotasCobroCierreConvenio.ChequearConcurrencia: boolean;
resourcestring
	MSG_NUMERO_INVALIDO = 'N�mero de comprobante inv�lido';
    ERROR_COMPROBANTE_SIENDO_ANULADO = 'El comprobante est� siendo anulado por el Usuario %s el d�a %s';
var
    OtroUsuario: string;
    FechaOtroUsuario: TDateTime;
begin
    //Chequeamos si otro usuario est� modificando este comprobante en la tabla de MovimientosTemporales

    spChequearUsuarioConcurrenteAcreditando.Parameters.Refresh;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@TipoComprobante').Value    := cbTipoComprobante.Value ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@NumeroComprobante').Value  := edNumeroComprobante.ValueInt ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@NumeroDeSesion').Value     := FIdSesion ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value        := null ;
    spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value   := null ;
    spChequearUsuarioConcurrenteAcreditando.ExecProc;


    if spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value <> null then
        OtroUsuario := spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@OtroUsuario').Value
    else OtroUsuario := '';

    if spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value <> null then
        FechaOtroUsuario := spChequearUsuarioConcurrenteAcreditando.Parameters.ParamByName('@FechaOtroUsuario').Value
    else FechaOtroUsuario := 0;

    Result := False;
    if OtroUsuario <> '' then begin
        MsgBoxErr(Format(ERROR_COMPROBANTE_SIENDO_ANULADO,[OtroUsuario,DateTimeToStr(FechaOtroUsuario)] ), caption, caption, MB_ICONERROR);
        Result := True;
        Exit;
    end;

end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.LimpiarDatosTemporales;
begin
    spLimpiarDatosTemporales.Parameters.ParamByName('@IdSesion').Value := FIdSesion ;
    spLimpiarDatosTemporales.ExecProc;
end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.cbTipoComprobanteChange(Sender: TObject);
var
  index: integer;
begin
    if (cbTipoComprobante.Value = TC_BOLETA) then begin
        edNumeroComprobanteFiscal.Enabled       := true;
        cbImpresorasFiscales.Enabled            := true;
        lblNumeroComprobanteFiscal.Enabled      := true;
        lblImpresoraFiscal.Enabled              := true;
        chkPasarTransitoEstadoEspecial.Visible  := false;
        edNumeroComprobante.Enabled             := false;
        lblNumeroComprobante.Enabled            := false;
        //lblNumeroNotaCreditoFiscal.Enabled  := cbTipoComprobante.Value = TC_BOLETA;
        //edNumeroNotaCreditoFiscal.Enabled   := cbTipoComprobante.Value = TC_BOLETA;
    end;

    //edNumeroComprobante.Enabled     := cbTipoComprobante.Value = TC_NOTA_COBRO;
    //lblNumeroComprobante.Enabled    := cbTipoComprobante.Value = TC_NOTA_COBRO;
    if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
        edNumeroComprobante.Enabled             := true;
        lblNumeroComprobante.Enabled            := true;
        chkPasarTransitoEstadoEspecial.Visible  := true;
        edNumeroComprobanteFiscal.Enabled       := false;
        lblNumeroComprobanteFiscal.Enabled      := false;
        cbImpresorasFiscales.Enabled            := false;
        lblImpresoraFiscal.Enabled              := false;
    end;

    if (cbTipoComprobante.Value = TC_FACTURA_EXENTA) OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA)
        OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA) OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA)
    then begin
        edNumeroComprobanteFiscal.Enabled       := true;
        lblNumeroComprobanteFiscal.Enabled      := true;
        chkPasarTransitoEstadoEspecial.Visible  := true;
        edNumeroComprobante.Enabled             := false;
        lblNumeroComprobante.Enabled            := false;
        cbImpresorasFiscales.Enabled            := false;
        lblImpresoraFiscal.Enabled              := false;
    end;

    // Mostrar u ocultar la columna de Transferir y AfectoIVA
    for index := 0 to dblDocumentoOriginal.Columns.Count - 1 do begin

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'Seleccionado' then begin
            dblDocumentoOriginal.Columns.Items[index].Width := 0;
            dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
        end;
//          No existe mas la columna seleccionado
//            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
//                dblDocumentoOriginal.Columns.Items[index].Width := 80;
//                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Transferir';
//            end else begin
//                dblDocumentoOriginal.Columns.Items[index].Width := 0;
//                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
//            end;

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'AfectoIVA' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblDocumentoOriginal.Columns.Items[index].Width := 0;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
            end else begin
                dblDocumentoOriginal.Columns.Items[index].Width := 80;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Afecto IVA';
            end;

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'DescriValorNeto' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblDocumentoOriginal.Columns.Items[index].Width := 0;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
            end else begin
                dblDocumentoOriginal.Columns.Items[index].Width := 90;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Valor Neto';
            end;

        if dblDocumentoOriginal.Columns.Items[index].FieldName = 'DescriImporteIVA' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblDocumentoOriginal.Columns.Items[index].Width := 0;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := '';
            end else begin
                dblDocumentoOriginal.Columns.Items[index].Width := 90;
                dblDocumentoOriginal.Columns.Items[index].Header.Caption := 'Importe IVA';
            end;

    end;

    for index := 0 to dblNotaCredito.Columns.Count - 1 do begin
        if dblNotaCredito.Columns.Items[index].FieldName = 'DescriValorNeto' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblNotaCredito.Columns.Items[index].Width := 0;
                dblNotaCredito.Columns.Items[index].Header.Caption := '';
            end else begin
                dblNotaCredito.Columns.Items[index].Width := 90;
                dblNotaCredito.Columns.Items[index].Header.Caption := 'Valor Neto';
            end;

        if dblNotaCredito.Columns.Items[index].FieldName = 'DescriImporteIVA' then
            if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
                dblNotaCredito.Columns.Items[index].Width := 0 ;
                dblNotaCredito.Columns.Items[index].Header.Caption := '';
            end else begin
                dblNotaCredito.Columns.Items[index].Width := 90;
                dblNotaCredito.Columns.Items[index].Header.Caption := 'Importe IVA';
             end;
    end;

    //chkPasarTransitoEstadoEspecial.Visible := cbTipoComprobante.Value = TC_NOTA_COBRO;

    if (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
        edNumeroComprobanteFiscal.Value := 0;
        cbImpresorasFiscales.ItemIndex := -1;
    end;
    if (cbTipoComprobante.Value = TC_BOLETA) then
        edNumeroComprobante.Value := 0;

    if (cbTipoComprobante.Value = TC_FACTURA_EXENTA)
        OR (cbTipoComprobante.Value = TC_FACTURA_AFECTA)
        OR (cbTipoComprobante.Value = TC_BOLETA_EXENTA)
        OR (cbTipoComprobante.Value = TC_BOLETA_AFECTA) then begin
        edNumeroComprobante.Value := 0;
        cbImpresorasFiscales.ItemIndex := -1;
    end;


	LimpiarDatosCliente;
	LimpiarDatosComprobante;
	LimpiarDetalles;

end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.cbTraspasarAjusteSencilloActualClick(
  Sender: TObject);
begin
    if cbTraspasarAjusteSencilloActual.Checked then
    begin
      cbNoRealizarAjusteSencillo.Checked:=cbTraspasarAjusteSencilloActual.Checked;
      cbNoRealizarAjusteSencillo.Enabled:=False;
    end
    else
    begin
      cbNoRealizarAjusteSencillo.Enabled:=True;
    end;
    CargarDetalleComprobante;
end;

function Tfrm_AnulacionNotasCobroCierreConvenio.ObtenerIVA: Double;
begin
    //se usa now porque la emision de los creditos en la base es getdate
    Result := QueryGetValueInt(DMConnections.BaseCAC,
                                'SELECT dbo.ObtenerIVA(''' + FormatDateTime('yyyymmdd',
                                NowBase(spComprobantes.Connection)) + ''')');
    Result := Result / 100;       // esto devuelve un 0.19 porque en la base dice 1900

end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.dblDocumentoOriginalDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
//var
//  OldColor: TColor;
//  bmp: TBitMap;
begin
	//estos codigos de cargos se van a crear de todas maneras como movimientos en el convenio sin numero de lote
	//les cambio el color para que el usuario lo sepa
    //Ahora le agregamos un check para marcar los que se pasan de nuevo al convenio sin numero de lote, o no


//    No existe mas la seleccion de cargos a devolver al convenio
//	OldColor := Sender.Canvas.Font.color ;
//	if LineaEsSeleccionable and (cbTipoComprobante.Value = TC_NOTA_COBRO) then begin
//		Sender.Canvas.Font.color := clred;
//	    with Sender do begin
//    		if (Column.FieldName = 'Seleccionado') then begin
//				Text := '';
//				Canvas.FillRect(Rect);
//				bmp := TBitMap.Create;
//				try
//					DefaultDraw := False;
//					if (cdsOriginal.FieldByName('Seleccionado').AsBoolean ) then begin
//						Img_Tilde.GetBitmap(0, Bmp);
//					end else begin
//	                    Img_Tilde.GetBitmap(1, Bmp);
//	                end;
//                    Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
//	            finally
//                    bmp.Free;
//                end;
//            end;
//        end;
//    end
//	else
//		Sender.Canvas.Font.color := OldColor;

end;

function Tfrm_AnulacionNotasCobroCierreConvenio.ValidarNumeroFiscalNotaCredito:boolean;
resourcestring
	MSG_ERROR_IMPRESORA_MANUAL_NO_ENCONTRADA = 'No se encontro la impresora manual en la n�mina de impresoras registradas';
    MSG_ERROR_NUMERO_FISCAL_CREDITO_REPETIDO = 'Ya existe una Nota de Cr�dito con ese N�mero Fiscal';
    MSG_ERROR_NUMERO_FISCAL_CREDITO_ERRONEO = 'Valor err�neo para N�mero Fiscal de Nota de Cr�dito';
var
    CodigoImpresoraFiscalManual: integer;
begin
    CodigoImpresoraFiscalManual := QueryGetValueInt(DMConnections.BaseCAC,'select dbo.ObtenerCodigoImpresoraManual()');
    Result := False;
    if CodigoImpresoraFiscalManual = 0 then begin
        MsgBoxErr(MSG_ERROR_IMPRESORA_MANUAL_NO_ENCONTRADA, Caption, Caption, 0);
        Exit;
    end;

    //if edNumeroNotaCreditoFiscal.ValueInt <= 0 then begin
    //    MsgBoxBalloon(MSG_ERROR_NUMERO_FISCAL_CREDITO_ERRONEO, Caption, MB_ICONSTOP, edNumeroNotaCreditoFiscal);
    //    Result := False;
    //    Exit;
    //end;

    //CodigoImpresoraFiscal puede ser la manual o una que exista o ninguna, en ese caso el parametro es cero
    //Si Result queda True es que no existe el comprobante
    //Result := QueryGetValue( DMConnections.BaseCAC, Format('SELECT dbo.ExisteComprobanteFiscal (''%s'', %d , %d )',
    //            	[TC_NOTA_CREDITO, edNumeroNotaCreditoFiscal.ValueInt, CodigoImpresoraFiscalManual] )) = 'False';

    //if not Result then MsgBoxBalloon(MSG_ERROR_NUMERO_FISCAL_CREDITO_REPETIDO, Caption, MB_ICONSTOP, edNumeroNotaCreditoFiscal);


end;

procedure Tfrm_AnulacionNotasCobroCierreConvenio.dblDocumentoOriginalDblClick(
  Sender: TObject);
begin
//    No existe mas seleccion de cargos para devolver al convenio
//	if cdsOriginal.IsEmpty then Exit;
//	dblDocumentoOriginal.Invalidate;
//	if not LineaEsSeleccionable then Exit;
//	with cdsOriginal do begin
//    	  Edit;
//        FieldByName('Seleccionado').AsBoolean := not FieldByName( 'Seleccionado' ).AsBoolean;
//        Post;
//	end;
//	dblDocumentoOriginal.Invalidate;
end;

end.
