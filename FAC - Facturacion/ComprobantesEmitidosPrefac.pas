{-----------------------------------------------------------------------------
 File Name: ComprobantesEmitidosPrefac.pas
 Description:   Permite buscar comprobantes de PreFacturaci�n pertenecientes
                a un Convenio.

 -----------------------------------------------------------------------------}

unit ComprobantesEmitidosPrefac;

interface

uses
	frmReporteBoletaFacturaElectronica,
    frmReporteNotaCreditoElectronica,
    frmReporteNotaDebitoElectronica,
    frmRptDetInfraccionesAnuladas,
    frmReporteCompDevDinero,
    Windows, Messages, SysUtils, Variants, Classes, Controls, Forms, Dialogs,
    Dateedit, Buttons, ExtCtrls, ADODB, VariantComboBox, DmiCtrls, UtilProc,
    UtilDB, util, DMConnection, BuscaClientes, peaTypes, peaProcs, DPSControls,
    ImagenesTransitos, MenuesCOntextuales, Navigator, ComCtrls, DB, ListBoxEx,
    DBListEx, StdCtrls, ReporteFactura, DBClient, Graphics,
    ConstParametrosGenerales,SeleccionarPreFacturacionImportar,                 // TASK_098_MGO_20161222
	TimeEdit, Validate, DateUtils, ReporteCK, ReporteNC, CSVUtils, CollPnl;

type
  TFormComprobantesEmitidosPrefac = class(TForm)
    dsFacturas: TDataSource;
    gbCliente: TGroupBox;
    Label1: TLabel;
    peNumeroDocumento: TPickEdit;
    spObtenerListaComprobantes: TADOStoredProc;
    Label3: TLabel;
    cbConvenios: TVariantComboBox;
    Detalle: TClientDataSet;
    dsDetalle: TDataSource;
    spObtenerDetalleConceptosComprobante: TADOStoredProc;
    tmrConsultaRUT: TTimer;
    spPREFAC_ProcesosFacturacion_SELECT: TADOStoredProc;
    lblverConvenio: TLabel;
    BTNBuscarComprobante: TButton;
    grpFiltros: TGroupBox;
    lblImporteDesde: TLabel;
    lblImporteHasta: TLabel;
    txtImporteDesde: TNumericEdit;
    txtImporteHasta: TNumericEdit;
    chkExcluidosAuto: TCheckBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lblDomicilio: TLabel;
    lbl3: TLabel;
    lblConcesionaria: TLabel;
    lblConvenio: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblNombre: TLabel;
    txtMuestraAleatoria: TNumericEdit;
    vcbMotivosNoFacturar: TVariantComboBox;
    grpProceso: TGroupBox;
    lbl7: TLabel;
    lblNumeroProcesoFacturacion: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblFechaHoraInicio: TLabel;
    lblFechaHoraFin: TLabel;
    lbl18: TLabel;
    lblOperador: TLabel;
    chkFacturables: TCheckBox;
    lbl23: TLabel;
    lbl24: TLabel;
    lbl25: TLabel;
    lbl26: TLabel;
    lbl27: TLabel;
    lblComprobantes: TLabel;
    lblIncluidos: TLabel;
    lblExcluidos: TLabel;
    lblMontoIncluidos: TLabel;
    lblMontoExcluidos: TLabel;
    grpParams: TGroupBox;
    lbl14: TLabel;
    lbl17: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    lblValIVA: TLabel;
    lblValUF: TLabel;
    lblDiasMinEmiFac: TLabel;
    lblMesForGenNK: TLabel;
    lbl10: TLabel;
    lblValMinNK: TLabel;
    lbl13: TLabel;
    lblValMinNKPagAut: TLabel;
    lblValMinDeuConv: TLabel;
    lbl11: TLabel;
    lblValMinNKBaja: TLabel;
    lbl12: TLabel;
    pnlListado: TPanel;
    lstComprobantes: TDBListEx;
    pnlAcciones: TPanel;
    lbl4: TLabel;
    lblResultado: TLabel;
    shp1: TShape;
    lbl19: TLabel;
    shp3: TShape;
    lbl21: TLabel;
    shp4: TShape;
    lbl22: TLabel;
    shp2: TShape;
    lbl20: TLabel;
    btnIncluirApartar: TButton;
    btnIncluirMasivo: TButton;
    btnApartarMasivo: TButton;
    pnlDetalle: TPanel;
    pgcDetalles: TPageControl;
    tsDetalleComprobante: TTabSheet;
    lstDetalle: TDBListEx;
    chkMostrarDetalle: TCheckBox;
    chkIncluidos: TCheckBox;
    chkExcluidosManual: TCheckBox;
    lbl28: TLabel;
    lbl29: TLabel;
    lblGrupoFacturacionDescri: TLabel;
    lblCicloAno: TLabel;
    btnLimpiar: TButton;
    spl1: TSplitter;
    clpnlProceso: TCollapsablePanel;
    lblExportarCSV: TLabel;
    dlgSaveCSV: TSaveDialog;
    spObtenerDomicilioFacturacion: TADOStoredProc;
	// INICIO : TASK_095_MGO_20161220
    tsTransitos: TTabSheet;
    lstTransitos: TDBListEx;
    dsTransitosPorComprobante: TDataSource;
    spObtenerTransitosPorComprobante: TADOStoredProc;
    pnl1: TPanel;
    lbl_TransitosDesde: TLabel;
    lbl_TransitosHasta: TLabel;
    lbl_Patente: TLabel;
    lblTransitosFechasRestablecer: TLabel;
    lblTransitosCSV: TLabel;
    btnTransitosFiltrar: TButton;
    date_TransitosDesde: TDateEdit;
    date_TransitosHasta: TDateEdit;
    edtTransitosPatente: TEdit;
    btnImportar: TButton;
    lbl30: TLabel;
    lblValMinImpAfe: TLabel;
	// FIN : TASK_095_MGO_20161220
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure cbConveniosChange(Sender: TObject);
    procedure lstComprobantesDrawText(Sender: TCustomDBListEx;
    Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
    var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
    var DefaultDraw: Boolean);
    procedure dsFacturasDataChange(Sender: TObject; Field: TField);
    procedure lstDetalleDrawText(Sender: TCustomDBListEx;
    Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
    var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
    var DefaultDraw: Boolean);
    procedure lstComprobantesColumnsImporteHeaderClick(Sender: TObject);   
    procedure lstComprobantesColumnsEstadoHeaderClick(Sender: TObject);
    procedure lstComprobantesColumnsHeaderClick(Sender: TObject);
    procedure tmrConsultaRUTTimer(Sender: TObject);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure lblverConvenioClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BTNBuscarComprobanteClick(Sender: TObject);
    procedure OrdenarColumna(Sender: TObject);
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure chkExcluidosAutoClick(Sender: TObject);
    procedure btnIncluirApartarClick(Sender: TObject);
    procedure btnIncluirMasivoClick(Sender: TObject);
    procedure btnApartarMasivoClick(Sender: TObject);
    procedure chkMostrarDetalleClick(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);
    procedure lstComprobantesColumnsImporteMesHeaderClick(Sender: TObject);
    procedure lblExportarCSVClick(Sender: TObject);
	// INICIO : TASK_095_MGO_20161220
    procedure lstTransitosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure lstTransitosCheckLink(Sender: TCustomDBListEx;
      Column: TDBListExColumn; var IsLink: Boolean);
    procedure lstTransitosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnTransitosFiltrarClick(Sender: TObject);
    procedure lblTransitosFechasRestablecerClick(Sender: TObject);
    procedure lblTransitosCSVClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
	// FIN : TASK_095_MGO_20161220

    protected
    { Protected declarations }
  private
    { Private declarations }
    FUltimaBusqueda : TBusquedaCliente;
    FConvenio : integer;
    FNumeroProcesoFacturacion,                                                  // TASK_094_MGO_20161219
    FCodigoGrupoFacturacion,                                                    // TASK_094_MGO_20161219
    FCiclo,                                                                     // TASK_094_MGO_20161219
    FAno: Integer;                                                              // TASK_094_MGO_20161219
    FEditable,                                                                  // TASK_095_MGO_20161220
    FTransitos: Boolean;                                                        // TASK_095_MGO_20161220

    //Filtro de Transitos
    FTransitosDesde, FTransitosHasta : TDateTime;
    FEstacionamientosDesde, FEstacionamientosHasta : TDateTime;
    FBoletaAfectaElectronica, FBoletaExentaElectronica,
    FFacturaAfectaElectronica, FFacturaExentaElectronica,
    FTipoNotaDeCobro, FNotaCreditoElectronica, FNotaDebitoElectronica,
    FUltimoNumeroDocumento: String;
    procedure CargarDatosProcesoPreFacturacion;
    procedure CargarComprobantes(Filtrar: Boolean);
    procedure MostrarDetalleNotaCobro;
    procedure MostrarDetalleOtrosComprobantes;
    procedure CargarTransitos;													// TASK_095_MGO_20161220
    procedure MostrarDomicilioFacturacion(CodigoConvenio : integer);
    procedure CargarMotivosNoFacturar;
    procedure IncluirConvenio(CodigoConvenio: Integer; Facturable: Boolean);
    procedure ApartarConvenio(CodigoConvenio: Integer; Facturable: Boolean);
    procedure HabilitarBotones;
  public
  { Public declarations }
    function Inicializar(NumeroProcesoFacturacion: Integer): Boolean;
  end;

resourcestring
    STR_INCLUIR = 'Incluir';
    STR_APARTAR = 'Excluir';              
    MSG_CONFIRM_CAPTION = 'Confirmaci�n';
  
var
  FormComprobantesEmitidosPrefac: TFormComprobantesEmitidosPrefac;

implementation

{$R *.dfm}
uses
    FrmInicioConsultaConvenio;

{-----------------------------------------------------------------------------
 Function/Procedure Name:  Inicializar
 Date: 19/04/2010
 Author: jjofre
 Description:  permite preparar e inicilizar todas las variables, funciones
                y stored involucrados para imprimir un comprobante
-----------------------------------------------------------------------------}
function TFormComprobantesEmitidosPrefac.Inicializar(NumeroProcesoFacturacion: Integer): Boolean;
resourcestring
	MSG_ALL_INVOICE_TYPES	= 'Todos';
    MSG_ERROR_INICIALIZAR	= 'Error al inicializar el formulario.';
    SQL_BOLETA_EXENTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_EXENTA()';
    SQL_BOLETA_AFECTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_AFECTA()';
    SQL_FACTURA_EXENTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_EXENTA()';
    SQL_FACTURA_AFECTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_AFECTA()';
    SQL_NOTA_COBRO			= 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_COBRO()';
    SQL_NOTA_CREDITO_ELECTRONICA = 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO_ELECTRONICA()';
    SQL_NOTA_DEBITO_ELECTRONICA  = 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_DEBITO_ELECTRONICA()';
    TIPO_NOTA_DEBITO		= 'Nota D�bito Electr�nica';
begin
    //cargar los tipos de documentos electr�nicos
    FFacturaAfectaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_FACTURA_AFECTA);
    FFacturaExentaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_FACTURA_EXENTA);
    FBoletaAfectaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_BOLETA_AFECTA);
    FBoletaExentaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_BOLETA_EXENTA);
    FTipoNotaDeCobro			:= QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_COBRO);
    FNotaCreditoElectronica     := QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_CREDITO_ELECTRONICA);
    FNotaDebitoElectronica		:= QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_DEBITO_ELECTRONICA);
    
    lblConvenio.Caption             := EmptyStr;
    lblConcesionaria.Caption        := EmptyStr;
    lblNombre.Caption               := EmptyStr;
    lblDomicilio.Caption            := EmptyStr;

	pgcDetalles.ActivePageIndex := 0;
    
    FNumeroProcesoFacturacion := NumeroProcesoFacturacion;
    clpnlProceso.Caption := Format(clpnlProceso.Caption, [NumeroProcesoFacturacion]);   // TASK_094_MGO_20161219
    CargarDatosProcesoPreFacturacion;
    CargarMotivosNoFacturar;
    CargarComprobantes(True);
    
    Result := True;
end;

procedure TFormComprobantesEmitidosPrefac.CargarDatosProcesoPreFacturacion;     
resourcestring
    MSG_ERROR_PROCESO = 'Error al obtener los datos del Proceso de Pre-Facturaci�n';
    STR_CICLO_ANO     = '%d - A�o %d';
//var                                                                           // TASK_094_MGO_20161219
//  PREFAC_ProcesosFacturacion_SELECT: TADOStoredProc;                          // TASK_094_MGO_20161219
begin
    { BEGIN : TASK_094_MGO_20161219
    PREFAC_ProcesosFacturacion_SELECT := TADOStoredProc.Create(Self);
    try
        try
            PREFAC_ProcesosFacturacion_SELECT.Connection := DMConnections.BaseCAC;
            PREFAC_ProcesosFacturacion_SELECT.ProcedureName := 'PREFAC_ProcesosFacturacion_SELECT';
            PREFAC_ProcesosFacturacion_SELECT.CommandTimeout := 30;

            PREFAC_ProcesosFacturacion_SELECT.Parameters.Refresh;
            PREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            PREFAC_ProcesosFacturacion_SELECT.Open;

            lblNumeroProcesoFacturacion.Caption := PREFAC_ProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsString;
            lblFechaHoraInicio.Caption          := PREFAC_ProcesosFacturacion_SELECT.FieldByName('FechaHoraInicio').AsString;
            lblFechaHoraFin.Caption             := PREFAC_ProcesosFacturacion_SELECT.FieldByName('FechaHoraFin').AsString;
            lblOperador.Caption                 := PREFAC_ProcesosFacturacion_SELECT.FieldByName('Operador').AsString;
            lblValMinNK.Caption                 := PREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinNK').AsString;
            lblValMinDeuConv.Caption            := PREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinDeuConv').AsString;
            lblValMinNKBaja.Caption             := PREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinNKBaja').AsString;
            lblMesForGenNK.Caption              := PREFAC_ProcesosFacturacion_SELECT.FieldByName('MesForGenNK').AsString;
            lblValMinNKPagAut.Caption           := PREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinNKPagAut').AsString;
            lblValUF.Caption                    := PREFAC_ProcesosFacturacion_SELECT.FieldByName('ValUF').AsString;
            lblValIVA.Caption                   := PREFAC_ProcesosFacturacion_SELECT.FieldByName('ValIVA').AsString;
            lblDiasMinEmiFac.Caption            := PREFAC_ProcesosFacturacion_SELECT.FieldByName('DiasMinEmiFac').AsString;
            lblGrupoFacturacionDescri.Caption   := PREFAC_ProcesosFacturacion_SELECT.FieldByName('GrupoFacturacionDescri').AsString;
            lblCicloAno.Caption                 := Format(STR_CICLO_ANO,
                                                        [PREFAC_ProcesosFacturacion_SELECT.FieldByName('Ciclo').AsInteger,
                                                        PREFAC_ProcesosFacturacion_SELECT.FieldByName('Ano').AsInteger]);

            FCodigoGrupoFacturacion             := PREFAC_ProcesosFacturacion_SELECT.FieldByName('CodigoGrupoFacturacion').AsInteger;
            FCiclo                              := PREFAC_ProcesosFacturacion_SELECT.FieldByName('Ciclo').AsInteger;
            FAno                                := PREFAC_ProcesosFacturacion_SELECT.FieldByName('Ano').AsInteger;

            lblComprobantes.Caption             := PREFAC_ProcesosFacturacion_SELECT.FieldByName('Comprobantes').AsString;
            lblIncluidos.Caption                := PREFAC_ProcesosFacturacion_SELECT.FieldByName('Incluidos').AsString;
            lblExcluidos.Caption                := PREFAC_ProcesosFacturacion_SELECT.FieldByName('Excluidos').AsString;
            lblMontoIncluidos.Caption           := PREFAC_ProcesosFacturacion_SELECT.FieldByName('MontoIncluidos').AsString;
            lblMontoExcluidos.Caption           := PREFAC_ProcesosFacturacion_SELECT.FieldByName('MontoExcluidos').AsString;

            FEditable                           := PREFAC_ProcesosFacturacion_SELECT.FieldByName('Editable').AsBoolean;

            PREFAC_ProcesosFacturacion_SELECT.Close;
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR_PROCESO, e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        PREFAC_ProcesosFacturacion_SELECT.Free;
    end;
    }
    try
        spPREFAC_ProcesosFacturacion_SELECT.Close;
        spPREFAC_ProcesosFacturacion_SELECT.Parameters.Refresh;
        spPREFAC_ProcesosFacturacion_SELECT.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
        spPREFAC_ProcesosFacturacion_SELECT.Open;

        lblNumeroProcesoFacturacion.Caption := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsString;
        lblFechaHoraInicio.Caption          := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('FechaHoraInicio').AsString;
        lblFechaHoraFin.Caption             := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('FechaHoraFin').AsString;
        lblOperador.Caption                 := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Operador').AsString;
        lblValMinNK.Caption                 := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinNK').AsString;
        lblValMinDeuConv.Caption            := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinDeuConv').AsString;
        lblValMinNKBaja.Caption             := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinNKBaja').AsString;
        lblMesForGenNK.Caption              := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('MesForGenNK').AsString;
        lblValMinNKPagAut.Caption           := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinNKPagAut').AsString;
        lblValMinImpAfe.Caption             := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ValMinImpAfe').AsString;        // TASK_115_MGO_20170119
        lblValUF.Caption                    := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ValUF').AsString;
        lblValIVA.Caption                   := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('ValIVA').AsString;
        lblDiasMinEmiFac.Caption            := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('DiasMinEmiFac').AsString;
        lblGrupoFacturacionDescri.Caption   := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('GrupoFacturacionDescri').AsString;
        lblCicloAno.Caption                 := Format(STR_CICLO_ANO,
                                                    [spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Ciclo').AsInteger,
                                                    spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Ano').AsInteger]);

        FCodigoGrupoFacturacion             := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('CodigoGrupoFacturacion').AsInteger;
        FCiclo                              := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Ciclo').AsInteger;
        FAno                                := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Ano').AsInteger;

        lblComprobantes.Caption             := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Comprobantes').AsString;
        lblIncluidos.Caption                := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Incluidos').AsString;
        lblExcluidos.Caption                := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Excluidos').AsString;
        lblMontoIncluidos.Caption           := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('MontoIncluidos').AsString;
        lblMontoExcluidos.Caption           := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('MontoExcluidos').AsString;

        FEditable                           := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Editable').AsBoolean;
        FTransitos                          := spPREFAC_ProcesosFacturacion_SELECT.FieldByName('Transitos').AsBoolean;  // TASK_095_MGO_20161220
    except
        on e: Exception do
            MsgBoxErr(MSG_ERROR_PROCESO, e.Message, 'Error', MB_ICONSTOP);
    end;
    // FIN : TASK_094_MGO_20161219

    HabilitarBotones;
end;

procedure TFormComprobantesEmitidosPrefac.CargarMotivosNoFacturar;
resourcestring
    MSG_ERROR_MOTIVOS = 'Error al obtener los motivos de apartado';
var
  MotivosNoFacturar_SELECT: TADOStoredProc;
begin
    MotivosNoFacturar_SELECT := TADOStoredProc.Create(Self);
    try
        try
            MotivosNoFacturar_SELECT.Connection := DMConnections.BaseCAC;
            MotivosNoFacturar_SELECT.ProcedureName := 'MotivosNoFacturar_SELECT';
            MotivosNoFacturar_SELECT.CommandTimeout := 30;
            MotivosNoFacturar_SELECT.Open;

            vcbMotivosNoFacturar.Items.Add('Todos los Motivos', -1);
            while not(MotivosNoFacturar_SELECT.Eof) do begin
                vcbMotivosNoFacturar.Items.Add(
                    MotivosNoFacturar_SELECT.FieldByName('Descripcion').AsString,
                    MotivosNoFacturar_SELECT.FieldByName('CodigoMotivo').AsInteger);
                MotivosNoFacturar_SELECT.Next;
            end;             
            vcbMotivosNoFacturar.ItemIndex := 0;
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR_MOTIVOS, e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        MotivosNoFacturar_SELECT.Free;
    end;
end;

procedure TFormComprobantesEmitidosPrefac.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
    if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
            tmrConsultaRUTTimer(nil);
        end;
    end;
    F.free;
end;

procedure TFormComprobantesEmitidosPrefac.cbConveniosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    Cargarcomprobantes(True);
end;

{-----------------------------------------------------------------------------
  Procedure: TFormComprobantesEmitidosPrefac.cbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidosPrefac.cbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;

procedure TFormComprobantesEmitidosPrefac.chkMostrarDetalleClick(
  Sender: TObject);
begin
    if not chkMostrarDetalle.Checked then begin
        Detalle.Close;
        Detalle.CreateDataSet;
        spObtenerTransitosPorComprobante.Close;                                 // TASK_095_MGO_20161220
    end else begin                                                              // TASK_095_MGO_20161220
        MostrarDetalleNotaCobro;                                                // TASK_095_MGO_20161220
        CargarTransitos;                                                        // TASK_095_MGO_20161220
    end;
end;

procedure TFormComprobantesEmitidosPrefac.chkExcluidosAutoClick(
  Sender: TObject);
begin
    vcbMotivosNoFacturar.Enabled := (chkExcluidosAuto.Checked or chkExcluidosManual.Checked);
end;

procedure TFormComprobantesEmitidosPrefac.HabilitarBotones;
begin                                                                          
    btnLimpiar.Enabled := (txtMuestraAleatoria.Text = '') and FEditable;
    btnIncluirApartar.Enabled := (txtMuestraAleatoria.Text = '') and FEditable;
    btnIncluirMasivo.Enabled := (txtMuestraAleatoria.Text = '') and FEditable;
    btnApartarMasivo.Enabled := (txtMuestraAleatoria.Text = '') and FEditable;
    btnImportar.Enabled := (txtMuestraAleatoria.Text = '') and FEditable;       // TASK_098_MGO_20161222

    tsTransitos.TabVisible := FTransitos;                                       // TASK_095_MGO_20161220
end;

procedure TFormComprobantesEmitidosPrefac.CargarComprobantes(Filtrar: Boolean);
resourcestring
    MSG_ERROR_SP = 'Se produjo un error al ejecutar el procedimiento almacenado ObtenerDatosComprobanteParaTimbreElectronico';
    MSG_NO_EXISTEN_COMPROBANTE='No se encontraron comprobantes pre-facturados';
    STR_RESULTADO = '%d resultados';
Var
    Persona: Integer;
    TipoComprobante: string;
    NumeroComprobante: int64;
  I: Integer;
begin
    spObtenerListaComprobantes.Close;
    if Filtrar then begin
        HabilitarBotones;

        spObtenerListaComprobantes.Parameters.Refresh;
        spObtenerListaComprobantes.Parameters.ParamByName('@NumeroDocumento').Value := peNumeroDocumento.Text;
        spObtenerListaComprobantes.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
        spObtenerListaComprobantes.Parameters.ParamByName('@ImporteDesde').Value := txtImporteDesde.ValueInt;
        spObtenerListaComprobantes.Parameters.ParamByName('@ImporteHasta').Value := txtImporteHasta.ValueInt;
        spObtenerListaComprobantes.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
        spObtenerListaComprobantes.Parameters.ParamByName('@MuestraAleatoria').Value := txtMuestraAleatoria.ValueInt;
        spObtenerListaComprobantes.Parameters.ParamByName('@Facturables').Value := IIf(chkFacturables.Checked, 1, 0);      
        spObtenerListaComprobantes.Parameters.ParamByName('@Incluidos').Value := IIf(chkIncluidos.Checked, 1, 0);
        spObtenerListaComprobantes.Parameters.ParamByName('@ExcluidosAuto').Value := IIf(chkExcluidosAuto.Checked, 1, 0);
        spObtenerListaComprobantes.Parameters.ParamByName('@ExcluidosManual').Value := IIf(chkExcluidosManual.Checked, 1, 0);
        if (chkExcluidosAuto.Checked or chkExcluidosManual.Checked) then
            spObtenerListaComprobantes.Parameters.ParamByName('@CodigoMotivo').Value := vcbMotivosNoFacturar.Value;
    end;
    spObtenerListaComprobantes.Open;

    FConvenio := cbConvenios.Value;
    Persona := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT CodigoCliente FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = %d', [FConvenio]));
    lblConvenio.Caption :=  QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.FormatearNumeroConvenio(dbo.ObtenerNumeroConvenio(%d))', [FConvenio]));
    lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)',[FConvenio]));
    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNombrePersona(%d)', [Persona]));
    lblverConvenio.Enabled := (cbConvenios.Value > 0);
    
    lblResultado.Caption := Format(STR_RESULTADO, [spObtenerListaComprobantes.RecordCount]);

    MostrarDomicilioFacturacion(FConvenio);

    for I := 0 to lstComprobantes.Columns.Count - 1 do
        TDBListExColumn(lstComprobantes.Columns[I]).Sorting := csNone;

    if spObtenerListaComprobantes.RecordCount > 0 then                          // TASK_095_MGO_20161220
        lblTransitosFechasRestablecerClick(lblTransitosFechasRestablecer);      // TASK_095_MGO_20161220
end;

procedure TFormComprobantesEmitidosPrefac.MostrarDomicilioFacturacion(CodigoConvenio : integer);
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
    with spObtenerDomicilioFacturacion, Parameters do begin
        // Mostramos el domicilio de Facturaci�n
        Close;
        ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        try
            ExecProc;
            lblDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value;
        except
            on e: exception do begin
                MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;


procedure TFormComprobantesEmitidosPrefac.lstComprobantesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
resourcestring
    MSG_EXCLUIDO = 'Excluido';
    MSG_APARTADO = 'Excluido';
    MSG_FACTURABLE = 'Facturable';
    MSG_INCLUIDO = 'Incluido';
begin
    if spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger = 2 then begin
        if Column = lstComprobantes.Columns[0] then Text := MSG_EXCLUIDO;
        Sender.Canvas.Font.Color := clRed;
    end;

    if spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger = 1 then begin
        if Column = lstComprobantes.Columns[0] then Text := MSG_APARTADO;
        Sender.Canvas.Font.Color := clMaroon;
    end;

    if spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger = 0 then
        if Column = lstComprobantes.Columns[0] then Text := MSG_FACTURABLE;

    if spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger = -1 then begin
        if Column = lstComprobantes.Columns[0] then Text := MSG_INCLUIDO;
        Sender.Canvas.Font.Color := clBlue;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dsFacturasDataChange
  Author:    (probably) ndonadio
  Date Created: UNKNOWN
  Description:
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidosPrefac.dsFacturasDataChange(Sender: TObject; Field: TField);
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
    ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';
    ERROR_GETTING_INVOICE_MESSAGES = 'Ocurri� un error obteniendo la lista de mensajes del comprobante.';
var 
    NumComprobante, 
    EstadoExcluido : Integer;
    TipoComprobante : string;
begin
    if chkMostrarDetalle.Checked then begin
        try
            Screen.Cursor := crHourglass;

            NumComprobante := iif(spObtenerListaComprobantes.fieldByName('NumeroComprobante').AsInteger > 0,
                                    spObtenerListaComprobantes.fieldByName('NumeroComprobante').AsInteger, 0);
            TipoComprobante := spObtenerListaComprobantes.fieldByName('TipoComprobante').AsString;

            // INICIO : TASK_095_MGO_20161220
            if ( TipoComprobante = TC_NOTA_COBRO ) then begin
                    MostrarDetalleNotaCobro;
                    CargarTransitos;
            end else
                MostrarDetalleOtrosComprobantes;
            // FIN : TASK_095_MGO_20161220

        finally
            Screen.Cursor := crDefault;
        end;
    end;

    EstadoExcluido := spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger;

    if EstadoExcluido < 1 then
        btnIncluirApartar.Caption := STR_APARTAR
    else
        btnIncluirApartar.Caption := STR_INCLUIR;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDetalleNotaCobro
  Author:    flamas
  Date Created: 02/02/2005
  Description: Muestra el Detalle de una Nota de Cobro
 -----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidosPrefac.MostrarDetalleNotaCobro;
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
    //ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';      // TASK_095_MGO_20161220
var
  PREFAC_ObtenerDetalleCuentaFacturaAImprimir: TADOStoredProc;
begin
    PREFAC_ObtenerDetalleCuentaFacturaAImprimir := TADOStoredProc.Create(Self);
    try
        try
            Detalle.Close;
        
            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Connection := DMConnections.BaseCAC;
            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.ProcedureName := 'PREFAC_ObtenerDetalleCuentaFacturaAImprimir';
            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.CommandTimeout := 30;

            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Parameters.Refresh;
            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Parameters.ParamByName('@TipoComprobante').Value := spObtenerListaComprobantes.fieldByName('TipoComprobante').AsString;
            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Parameters.ParamByName('@NumeroComprobante').Value := spObtenerListaComprobantes.fieldByName('NumeroComprobante').asInteger;
            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Open;

            Detalle.CreateDataSet;
            while not PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Eof do begin
                Detalle.Append;
                Detalle.FieldByName('Descripcion').Assign(PREFAC_ObtenerDetalleCuentaFacturaAImprimir.FieldByName('Descripcion'));
                Detalle.FieldByName('Importe').Assign(PREFAC_ObtenerDetalleCuentaFacturaAImprimir.FieldByName('DescriImporte'));
                Detalle.Post;
                PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Next;
            end;

            if not spObtenerListaComprobantes.IsEmpty then begin
                Detalle.Append;
                Detalle.FieldByName('Descripcion').AsString := 'TOTAL A PAGAR';
                Detalle.FieldByName('Importe').AsString := spObtenerListaComprobantes.FieldByName('DescriAPagar').AsString;
                Detalle.Post;
            end;
            
            Detalle.First;
        except
            on e: exception do begin
                msgBoxErr(ERROR_GETTING_INVOICE_DETAIL, e.Message, caption, MB_ICONSTOP);
            end;
        end;
    finally
        PREFAC_ObtenerDetalleCuentaFacturaAImprimir.Free;
    end;
end;

// INICIO : TASK_095_MGO_20161220
procedure TFormComprobantesEmitidosPrefac.CargarTransitos;
resourcestring
    ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';
begin
    if not tsTransitos.TabVisible then Exit;

    try
        spObtenerTransitosPorComprobante.Close;
        spObtenerTransitosPorComprobante.Parameters.Refresh;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@TipoComprobante').Value       := spObtenerListaComprobantes.FieldByName('TipoComprobante').AsString;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@NumeroComprobante').Value     := spObtenerListaComprobantes.FieldByName('NumeroComprobante').AsInteger;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@Cantidad').Value              := MaxInt;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@FechaInicial').Value          := date_TransitosDesde.Date;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@FechaFinal').Value            := date_TransitosHasta.Date;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@CodigoConcesionaria').Value   := Null;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@Patente').Value               := IIf(Trim(edtTransitosPatente.Text) <> EmptyStr, edtTransitosPatente.Text, Null);
        spObtenerTransitosPorComprobante.Open;
    except
        on e: Exception do
            MsgBoxErr(ERROR_GETTING_INVOICE_TRANSITS, e.Message, caption, MB_ICONSTOP);
    end;
end;          

procedure TFormComprobantesEmitidosPrefac.btnTransitosFiltrarClick(
  Sender: TObject);
begin
    CargarTransitos;
end;

procedure TFormComprobantesEmitidosPrefac.lblTransitosCSVClick(Sender: TObject);
resourcestring
    MSG_TITLE   = 'Exportar Tr�nsitos a CSV';
    MSG_SIN_RESULTADOS = 'No hay registros en el listado para exportar.';
    MSG_ERROR_REGISTROS   = 'Error al exportar los tr�nsitos.';
    MSG_SUCCESS = 'El archivo %s fue creado exitosamente.';
    MSG_CONFIRM = '�Desea exportar los tr�nsitos del listado a formato CSV?';
    STR_FILENAME_TRANSITOS = 'PreFacturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__TRANSITOS__RUT_%s__Convenio_%s.csv';
var
    FileBuffer, Error: String;
    NombreArchivo: String;
begin
    if lstTransitos.DataSource.DataSet.IsEmpty then begin
        MsgBox(MSG_SIN_RESULTADOS, MSG_TITLE, MB_ICONSTOP);
        Exit;
    end;

    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then Exit;

    try
        Screen.Cursor := crHourGlass;

        if DatasetToExcelCSV(lstTransitos.DataSource.DataSet, FileBuffer, Error) then begin
            dlgSaveCSV.Title := 'Guardar Tr�nsitos Como...';
            NombreArchivo := Format(STR_FILENAME_TRANSITOS,
                                [FNumeroProcesoFacturacion, FCodigoGrupoFacturacion, FCiclo, FAno,
                                Trim(spObtenerListaComprobantes.FieldByName('NumeroDocumento').AsString),
                                Trim(spObtenerListaComprobantes.FieldByName('NumeroConvenio').AsString)]);

            dlgSaveCSV.FileName := NombreArchivo;

            Screen.Cursor := crDefault;
            if dlgSaveCSV.Execute then begin
                StringToFile(FileBuffer, dlgSaveCSV.FileName);
                MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
            end;
        end else begin
            raise Exception.Create(Error);
        end;
    except
        on e: Exception do begin
            Screen.Cursor := crDefault;
            MsgBoxErr(MSG_ERROR_REGISTROS, e.Message, MSG_TITLE, MB_ICONERROR);
            Exit;
        end;
    end;
end;

procedure TFormComprobantesEmitidosPrefac.lblTransitosFechasRestablecerClick(
  Sender: TObject);
begin
    date_TransitosDesde.Date := spObtenerListaComprobantes.FieldByName('PeriodoInicial').AsDateTime;
    date_TransitosHasta.Date := spObtenerListaComprobantes.FieldByName('PeriodoFinal').AsDateTime;
end;
// END : TASK_095_MGO_20161220

{-----------------------------------------------------------------------------
  Function Name: MostrarDetalleOtrosComprobantes
  Author:    flamas
  Date Created: 02/02/2005
  Description: Muestra el Detalle de los Comprobantes distintos de Nota de Cobro
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidosPrefac.MostrarDetalleOtrosComprobantes;
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
begin
    try
        Detalle.Close;
        spObtenerDetalleConceptosComprobante.Close;
        spObtenerDetalleConceptosComprobante.Parameters.Refresh;
        spObtenerDetalleConceptosComprobante.Parameters.ParamByName('@TipoComprobante').Value := spObtenerListaComprobantes.fieldByName('TipoComprobante').AsString;
        spObtenerDetalleConceptosComprobante.Parameters.ParamByName('@NumeroComprobante').Value := spObtenerListaComprobantes.fieldByName('NumeroComprobante').asInteger;
        spObtenerDetalleConceptosComprobante.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;    // TASK_139_MGO_20170214
        spObtenerDetalleConceptosComprobante.Open;
        Detalle.CreateDataSet;
        while not spObtenerDetalleConceptosComprobante.Eof do begin
            Detalle.Append;
            Detalle.FieldByName('Descripcion').Assign(spObtenerDetalleConceptosComprobante.FieldByName('Descripcion'));
            Detalle.FieldByName('Importe').AsString := spObtenerDetalleConceptosComprobante.FieldByName('DescImporte').AsString;
            Detalle.Post;
            spObtenerDetalleConceptosComprobante.Next;
        end;
        spObtenerDetalleConceptosComprobante.Close;
        if not spObtenerListaComprobantes.IsEmpty then begin
        	Detalle.Append;
            if (spObtenerListaComprobantes.FieldByName('TipoComprobante').AsString = TC_NOTA_CREDITO_A_COBRO)  then begin 
                Detalle.FieldByName('Descripcion').AsString := 'TOTAL COMPROBANTE';
            	Detalle.FieldByName('Importe').AsString := spObtenerListaComprobantes.FieldByName('DescriTotal').AsString;
            end
            else begin
            	Detalle.FieldByName('Descripcion').AsString := 'TOTAL A PAGAR';
            	Detalle.FieldByName('Importe').AsString := spObtenerListaComprobantes.FieldByName('DescriAPagar').AsString;
            end;
        	Detalle.Post;
        end;
        Detalle.First;
    except
        on e: exception do begin
            msgBoxErr(ERROR_GETTING_INVOICE_DETAIL, e.Message, caption, MB_ICONSTOP);
        end;
    end;
end;

procedure TFormComprobantesEmitidosPrefac.lstDetalleDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Assigned(Column) then begin
        if Pos('TOTAL', UpperCase(Text)) <> 0 then
          lstDetalle.Canvas.Font.Style := [fsBold];
    end;
end;

// INICIO : TASK_095_MGO_20161220
procedure TFormComprobantesEmitidosPrefac.lstTransitosCheckLink(
  Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    if Column.FieldName = 'Imagen' then
        IsLInk := spObtenerTransitosPorComprobante.Active and
          (spObtenerTransitosPorComprobante.FieldByName('RegistrationAccessibility').AsInteger > 0)
    else
        IsLInk := false;
end;

procedure TFormComprobantesEmitidosPrefac.lstTransitosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column = lstTransitos.Columns[0] then
       Text := FormatDateTime('dd-mm-yyyy hh:nn', spObtenerTransitosPorComprobante.FieldByName('FechaHora').AsDateTime);

    if spObtenerTransitosPorComprobante.FieldByName('TransitoAnulado').AsString <> '' then
    	Sender.Canvas.Font.Color := clRed;
end;

procedure TFormComprobantesEmitidosPrefac.lstTransitosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    if Column.IsLink then
        MostrarVentanaImagen(Self,
          spObtenerTransitosPorComprobante.FieldByName('NumCorrCA').asInteger,
          spObtenerTransitosPorComprobante.FieldByName('FechaHora').asDateTime,
          spObtenerTransitosPorComprobante.FieldByName('EsItaliano').AsBoolean);
end;
// FIN : TASK_095_MGO_20161220

procedure TFormComprobantesEmitidosPrefac.lstComprobantesColumnsEstadoHeaderClick(Sender: TObject);
begin
   if NOT  spObtenerListaComprobantes.Active then Exit;
   if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   else TDBListExColumn(sender).Sorting := csAscending;
   spObtenerListaComprobantes.Sort := 'EstadoExcluido' + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;       

procedure TFormComprobantesEmitidosPrefac.lstComprobantesColumnsImporteHeaderClick(Sender: TObject);
begin
   if NOT  spObtenerListaComprobantes.Active then Exit;
   if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   else TDBListExColumn(sender).Sorting := csAscending;
   spObtenerListaComprobantes.Sort := 'TotalAPagar' + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

procedure TFormComprobantesEmitidosPrefac.lstComprobantesColumnsImporteMesHeaderClick(
  Sender: TObject);
begin
    if NOT  spObtenerListaComprobantes.Active then Exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;
    spObtenerListaComprobantes.Sort := 'TotalComprobante' + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

procedure TFormComprobantesEmitidosPrefac.lstComprobantesColumnsHeaderClick(Sender: TObject);
begin
   if NOT  spObtenerListaComprobantes.Active then Exit;
   if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   else TDBListExColumn(sender).Sorting := csAscending;
   spObtenerListaComprobantes.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 08/03/2005
  Description: Resetea el Timer para que se ejecute la consulta
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidosPrefac.peNumeroDocumentoChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
 	tmrConsultaRUT.Enabled := False;
	tmrConsultaRUT.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: tmrConsultaTimer
  Author:    flamas
  Date Created: 08/03/2005
  Description: Ejecuta la consulta
-----------------------------------------------------------------------------}
procedure TFormComprobantesEmitidosPrefac.tmrConsultaRUTTimer(Sender: TObject);
begin
	Screen.Cursor := crHourglass;
    try
		tmrConsultaRUT.Enabled := False;
        if peNumeroDocumento.Text = EmptyStr then
            cbConvenios.Clear;
        PeaProcs.CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, False, True, True);
        lblverConvenio.Enabled := (cbConvenios.Value > 0);
    finally
		Screen.Cursor := crDefault;
    end;
end;

procedure TFormComprobantesEmitidosPrefac.OrdenarColumna(Sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;

procedure TFormComprobantesEmitidosPrefac.BTNBuscarComprobanteClick(Sender: TObject);
begin
    CargarComprobantes(True);
end;

// INICIO : TASK_094_MGO_20161219
procedure TFormComprobantesEmitidosPrefac.lblExportarCSVClick(Sender: TObject);
resourcestring
    MSG_TITLE   = 'Exportar CSV';
    MSG_SIN_RESULTADOS = 'No hay registros en el listado para exportar.';
    MSG_ERROR_ENCABEZADO   = 'Error al exportar los datos del encabezado.';
    MSG_ERROR_REGISTROS   = 'Error al exportar los registros del listado.';     // TASK_125_MGO_20170125
    MSG_ERROR_DETALLE   = 'Error al exportar el detalle del listado.';          // TASK_125_MGO_20170125
    MSG_SUCCESS = 'El archivo %s fu� creado exitosamente.';
    MSG_CONFIRM = '�Desea exportar el encabezado y registros del listado a formato CSV?. Puede demorar unos minutos.';    // TASK_125_MGO_20170125   
    MSG_CONFIRM_DETALLE = '�Desea exportar adem�s el detalle de los comprobantes?. Este proceso puede demorar varios minutos.';   // TASK_139_MGO_20170214
    STR_FILENAME_ENCABEZADO = 'PreFacturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__ENCABEZADO.csv';
    STR_FILENAME_REGISTROS = 'PreFacturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__REGISTROS.csv';    
    STR_FILENAME_DETALLE = 'PreFacturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__DETALLE.csv';   // TASK_125_MGO_20170125
var
    FileBuffer, Error: String;
    NombreArchivo: String;      
    EstadoMostrar: Boolean;
    spPREFAC_GenerarReporteProcesoFacturacion: TADOStoredProc;                  // TASK_125_MGO_20170125
begin
    if lstComprobantes.DataSource.DataSet.IsEmpty then begin
        MsgBox(MSG_SIN_RESULTADOS, MSG_TITLE, MB_ICONSTOP);
        Exit;
    end;

    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then Exit;


    try
        Screen.Cursor := crHourGlass;

        if DatasetToExcelCSV(spPREFAC_ProcesosFacturacion_SELECT, FileBuffer, Error) then begin
            dlgSaveCSV.Title := 'Guardar Encabezado Como...';                   // TASK_095_MGO_20161220
            NombreArchivo := Format(STR_FILENAME_ENCABEZADO,
                                [FNumeroProcesoFacturacion,
                                FCodigoGrupoFacturacion,
                                FCiclo, FAno]);

            dlgSaveCSV.FileName := NombreArchivo;

            Screen.Cursor := crDefault;
            if dlgSaveCSV.Execute then begin
                StringToFile(FileBuffer, dlgSaveCSV.FileName);
                MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
            end;
        end else begin
            raise Exception.Create(Error);
        end;
    except
        on e: Exception do begin
            Screen.Cursor := crDefault;
            MsgBoxErr(MSG_ERROR_ENCABEZADO, e.Message, MSG_TITLE, MB_ICONERROR);
            Exit;
        end;
    end;

    try
        try
            Screen.Cursor := crHourGlass;
            EstadoMostrar := chkMostrarDetalle.Checked;
            chkMostrarDetalle.Checked := False;

            // INICIO : TASK_125_MGO_20170125
            spPREFAC_GenerarReporteProcesoFacturacion := TADOStoredProc.Create(Self);
            spPREFAC_GenerarReporteProcesoFacturacion.Connection := DMConnections.BaseCAC;
            spPREFAC_GenerarReporteProcesoFacturacion.ProcedureName := 'PREFAC_GenerarReporteProcesoFacturacion';
            spPREFAC_GenerarReporteProcesoFacturacion.CommandTimeout := 60;
            spPREFAC_GenerarReporteProcesoFacturacion.Parameters.Refresh;
            spPREFAC_GenerarReporteProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            spPREFAC_GenerarReporteProcesoFacturacion.Open;
            // FIN : TASK_125_MGO_20170125

            //if DatasetToExcelCSV(lstComprobantes.DataSource.DataSet, FileBuffer, Error) then begin        // TASK_125_MGO_20170125
            if DatasetToExcelCSV(spPREFAC_GenerarReporteProcesoFacturacion, FileBuffer, Error) then begin   // TASK_125_MGO_20170125
                dlgSaveCSV.Title := 'Guardar Registros Como...';
                NombreArchivo := Format(STR_FILENAME_REGISTROS,
                                    [FNumeroProcesoFacturacion,
                                    FCodigoGrupoFacturacion,
                                    FCiclo, FAno]);

                dlgSaveCSV.FileName := NombreArchivo;

                Screen.Cursor := crDefault;
                if dlgSaveCSV.Execute then begin
                    StringToFile(FileBuffer, dlgSaveCSV.FileName);
                    MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
                end;
            end else begin
                raise Exception.Create(Error);
            end;
        except
            on e: Exception do begin
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ERROR_REGISTROS, e.Message, MSG_TITLE, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        chkMostrarDetalle.Checked := EstadoMostrar;  
        spPREFAC_GenerarReporteProcesoFacturacion.Free;                         // TASK_125_MGO_20170125
    end;

    if MsgBox(MSG_CONFIRM_DETALLE, MSG_CONFIRM_CAPTION, MB_ICONQUESTION+MB_YESNO) = ID_NO then Exit;    // TASK_139_MGO_20170214

    // INICIO : TASK_125_MGO_20170125
    try
        try
            Screen.Cursor := crHourGlass;
            EstadoMostrar := chkMostrarDetalle.Checked;
            chkMostrarDetalle.Checked := False;

            spPREFAC_GenerarReporteProcesoFacturacion := TADOStoredProc.Create(Self);
            spPREFAC_GenerarReporteProcesoFacturacion.Connection := DMConnections.BaseCAC;
            spPREFAC_GenerarReporteProcesoFacturacion.ProcedureName := 'PREFAC_GenerarReporteProcesoFacturacionDetalle';
            spPREFAC_GenerarReporteProcesoFacturacion.CommandTimeout := 60;
            spPREFAC_GenerarReporteProcesoFacturacion.Parameters.Refresh;
            spPREFAC_GenerarReporteProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            spPREFAC_GenerarReporteProcesoFacturacion.Open;

            if DatasetToExcelCSV(spPREFAC_GenerarReporteProcesoFacturacion, FileBuffer, Error) then begin
                dlgSaveCSV.Title := 'Guardar Detalle Como...';
                NombreArchivo := Format(STR_FILENAME_DETALLE,
                                    [FNumeroProcesoFacturacion,
                                    FCodigoGrupoFacturacion,
                                    FCiclo, FAno]);

                dlgSaveCSV.FileName := NombreArchivo;

                Screen.Cursor := crDefault;
                if dlgSaveCSV.Execute then begin
                    StringToFile(FileBuffer, dlgSaveCSV.FileName);
                    MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
                end;
            end else begin
                raise Exception.Create(Error);
            end;
        except
            on e: Exception do begin
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ERROR_DETALLE, e.Message, MSG_TITLE, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        chkMostrarDetalle.Checked := EstadoMostrar;
        spPREFAC_GenerarReporteProcesoFacturacion.Free;
    end;              
    // FIN : TASK_125_MGO_20170125
end;
// FIN : TASK_094_MGO_20161219

procedure TFormComprobantesEmitidosPrefac.lblverConvenioClick(Sender: TObject);
var
	f: TFormInicioConsultaConvenio;
begin
    if FindFormOrCreate(TFormInicioConsultaConvenio, f) then
        f.Show
    else begin
        if f.Inicializar(FConvenio, -1, -1) then
            f.Show
        else
            f.Release;
    end;
end;

procedure TFormComprobantesEmitidosPrefac.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

// INICIO : TASK_098_MGO_20161222
procedure TFormComprobantesEmitidosPrefac.btnImportarClick(Sender: TObject);
resourcestring
    MSG_ERROR_IMPORTAR = 'Hubo un error importando los Convenios excluidos e incluidos';
    MSG_EXITO = 'Se importaron %d Convenios excluidos y %d Convenios incluidos';
    STR_CAPTION = 'Importar Convenios';
var
    NumeroProcesoFacturacionSeleccion,
    CantidadApartados,
    CantidadIncluidos: Integer;
    PREFAC_ConveniosApartadosIncluidos_Importar: TADOStoredProc;
    SeleccionarPreFacturacionImportar: TfrmSeleccionarPreFacturacionImportar;
begin
    Application.CreateForm(TfrmSeleccionarPreFacturacionImportar, SeleccionarPreFacturacionImportar);
    if SeleccionarPreFacturacionImportar.Inicializar(FCodigoGrupoFacturacion, FNumeroProcesoFacturacion) then begin
        if SeleccionarPreFacturacionImportar.ShowModal = mrOk then
            NumeroProcesoFacturacionSeleccion := SeleccionarPreFacturacionImportar.FNumeroProcesoFacturacionSel
        else Exit;
    end else begin
        SeleccionarPreFacturacionImportar.Release;
        Exit;
    end;

    PREFAC_ConveniosApartadosIncluidos_Importar := TADOStoredProc.Create(Self);
    try
        try
            PREFAC_ConveniosApartadosIncluidos_Importar.Connection := DMConnections.BaseCAC;
            PREFAC_ConveniosApartadosIncluidos_Importar.ProcedureName := 'PREFAC_ConveniosApartadosIncluidos_Importar';
            PREFAC_ConveniosApartadosIncluidos_Importar.CommandTimeout := 30;

            PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.Refresh;
            PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.ParamByName('@NumeroProcesoFacturacionSeleccion').Value := NumeroProcesoFacturacionSeleccion;
            PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            PREFAC_ConveniosApartadosIncluidos_Importar.ExecProc;

            if PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.ParamByName('@ErrorDescription').Value);

            CantidadApartados := PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.ParamByName('@CantidadApartados').Value;
            CantidadIncluidos := PREFAC_ConveniosApartadosIncluidos_Importar.Parameters.ParamByName('@CantidadIncluidos').Value;

            MsgBox(Format(MSG_EXITO, [CantidadApartados, CantidadIncluidos]), STR_CAPTION, MB_OK);
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR_IMPORTAR, e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        PREFAC_ConveniosApartadosIncluidos_Importar.Free;
    end;

    CargarComprobantes(False);
    CargarDatosProcesoPreFacturacion;
end;
// FIN : TASK_098_MGO_20161222

procedure TFormComprobantesEmitidosPrefac.btnIncluirApartarClick(
  Sender: TObject);
var
    Posicion: TBookmark;
begin
    if btnIncluirApartar.Caption = STR_INCLUIR then
        IncluirConvenio(spObtenerListaComprobantes.FieldByName('CodigoConvenio').AsInteger,
                        spObtenerListaComprobantes.FieldByName('Facturable').AsBoolean);

    if btnIncluirApartar.Caption = STR_APARTAR then begin
        ApartarConvenio(spObtenerListaComprobantes.FieldByName('CodigoConvenio').AsInteger,
                        spObtenerListaComprobantes.FieldByName('Facturable').AsBoolean);
    end;

    Posicion := spObtenerListaComprobantes.GetBookmark;
    CargarComprobantes(False);
    try
        spObtenerListaComprobantes.GotoBookmark(Posicion);
    except
    end;

    CargarDatosProcesoPreFacturacion;
end;

procedure TFormComprobantesEmitidosPrefac.btnIncluirMasivoClick(
  Sender: TObject);                      
resourcestring
    MSG_INCLUIR_MASIVO = '�Desea incluir en facturaci�n a los %d Convenios seleccionados?';
var
    EstadoMostrar: Boolean;
begin
    if MsgBox(Format(MSG_INCLUIR_MASIVO, [spObtenerListaComprobantes.RecordCount]),
        MSG_CONFIRM_CAPTION, MB_YESNO) = IDNO then
        Exit;
        
    EstadoMostrar := chkMostrarDetalle.Checked;
    chkMostrarDetalle.Checked := False;

    spObtenerListaComprobantes.First;
    while not spObtenerListaComprobantes.Eof do begin
        if (spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger = 1)
            or (spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger = 2) then
            IncluirConvenio(spObtenerListaComprobantes.FieldByName('CodigoConvenio').AsInteger,
                            spObtenerListaComprobantes.FieldByName('Facturable').AsBoolean);

        spObtenerListaComprobantes.Next;
    end;

    chkMostrarDetalle.Checked := EstadoMostrar;
    CargarComprobantes(False);
    CargarDatosProcesoPreFacturacion;
end;           

procedure TFormComprobantesEmitidosPrefac.btnApartarMasivoClick(
  Sender: TObject);
resourcestring
    MSG_APARTAR_MASIVO = '�Desea apartar de facturaci�n a los %d Convenios seleccionados?';
var
    EstadoMostrar: Boolean;
begin          
    if MsgBox(Format(MSG_APARTAR_MASIVO, [spObtenerListaComprobantes.RecordCount]),
        MSG_CONFIRM_CAPTION, MB_YESNO) = IDNO then
        Exit;

    EstadoMostrar := chkMostrarDetalle.Checked;
    chkMostrarDetalle.Checked := False;

    spObtenerListaComprobantes.First;
    while not spObtenerListaComprobantes.Eof do begin
        if spObtenerListaComprobantes.FieldByName('EstadoExcluido').AsInteger < 1 then
            ApartarConvenio(spObtenerListaComprobantes.FieldByName('CodigoConvenio').AsInteger,
                            spObtenerListaComprobantes.FieldByName('Facturable').AsBoolean);

        spObtenerListaComprobantes.Next;
    end;
                        
    chkMostrarDetalle.Checked := EstadoMostrar;
    CargarComprobantes(False);
    CargarDatosProcesoPreFacturacion;
end;

procedure TFormComprobantesEmitidosPrefac.IncluirConvenio(CodigoConvenio: Integer; Facturable: Boolean);
resourcestring
    MSG_ERROR_INCLUIR = 'Hubo un error incluyendo el Convenio de c�digo %d';
var
  PREFAC_ConveniosIncluidos_INSERT: TADOStoredProc;
begin
    PREFAC_ConveniosIncluidos_INSERT := TADOStoredProc.Create(Self);
    try
        try
            PREFAC_ConveniosIncluidos_INSERT.Connection := DMConnections.BaseCAC;
            PREFAC_ConveniosIncluidos_INSERT.ProcedureName := 'PREFAC_ConveniosIncluidos_INSERT';
            PREFAC_ConveniosIncluidos_INSERT.CommandTimeout := 30;

            PREFAC_ConveniosIncluidos_INSERT.Parameters.Refresh;
            PREFAC_ConveniosIncluidos_INSERT.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            PREFAC_ConveniosIncluidos_INSERT.Parameters.ParamByName('@Facturable').Value := Facturable;
            PREFAC_ConveniosIncluidos_INSERT.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            PREFAC_ConveniosIncluidos_INSERT.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            PREFAC_ConveniosIncluidos_INSERT.ExecProc;

            if PREFAC_ConveniosIncluidos_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(PREFAC_ConveniosIncluidos_INSERT.Parameters.ParamByName('@ErrorDescription').Value);
        except
            on e: Exception do
                MsgBoxErr(Format(MSG_ERROR_INCLUIR, [CodigoConvenio]), e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        PREFAC_ConveniosIncluidos_INSERT.Free;
    end;
end;   

procedure TFormComprobantesEmitidosPrefac.ApartarConvenio(CodigoConvenio: Integer; Facturable: Boolean);
resourcestring
    MSG_ERROR_APARTAR = 'Hubo un error apartando el Convenio de c�digo %d';
var
  PREFAC_ConveniosApartados_INSERT: TADOStoredProc;
begin
    PREFAC_ConveniosApartados_INSERT := TADOStoredProc.Create(Self);
    try
        try
            PREFAC_ConveniosApartados_INSERT.Connection := DMConnections.BaseCAC;
            PREFAC_ConveniosApartados_INSERT.ProcedureName := 'PREFAC_ConveniosApartados_INSERT';
            PREFAC_ConveniosApartados_INSERT.CommandTimeout := 30;

            PREFAC_ConveniosApartados_INSERT.Parameters.Refresh;
            PREFAC_ConveniosApartados_INSERT.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            PREFAC_ConveniosApartados_INSERT.Parameters.ParamByName('@Facturable').Value := Facturable;
            PREFAC_ConveniosApartados_INSERT.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            PREFAC_ConveniosApartados_INSERT.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            PREFAC_ConveniosApartados_INSERT.ExecProc;

            if PREFAC_ConveniosApartados_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(PREFAC_ConveniosApartados_INSERT.Parameters.ParamByName('@ErrorDescription').Value);
        except
            on e: Exception do
                MsgBoxErr(Format(MSG_ERROR_APARTAR, [CodigoConvenio]), e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        PREFAC_ConveniosApartados_INSERT.Free;
    end;
end;

procedure TFormComprobantesEmitidosPrefac.btnLimpiarClick(Sender: TObject);
resourcestring
    MSG_ERROR_LIMPIAR = 'Hubo un error restableciendo los Convenios excluidos e incluidos';
    MSG_CONFIRMACION = '�Desea restablecer todos los Convenios excluidos e incluidos manualmente a su estado original?';
    MSG_EXITO = 'Los convenios excluidos e incluidos se restablecieron exitosamente';
    STR_CAPTION = 'Restablecer Convenios';
var
    PREFAC_ConveniosApartadosIncluidos_DELETE: TADOStoredProc;
begin
    if MsgBox(MSG_CONFIRMACION, STR_CAPTION, MB_YESNO) = IDNO then
        Exit;    

    PREFAC_ConveniosApartadosIncluidos_DELETE := TADOStoredProc.Create(Self);
    try
        try
            PREFAC_ConveniosApartadosIncluidos_DELETE.Connection := DMConnections.BaseCAC;
            PREFAC_ConveniosApartadosIncluidos_DELETE.ProcedureName := 'PREFAC_ConveniosApartadosIncluidos_DELETE';
            PREFAC_ConveniosApartadosIncluidos_DELETE.CommandTimeout := 30;
            
            PREFAC_ConveniosApartadosIncluidos_DELETE.Parameters.Refresh;
            PREFAC_ConveniosApartadosIncluidos_DELETE.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            PREFAC_ConveniosApartadosIncluidos_DELETE.ExecProc;    

            if PREFAC_ConveniosApartadosIncluidos_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(PREFAC_ConveniosApartadosIncluidos_DELETE.Parameters.ParamByName('@ErrorDescription').Value);

            MsgBox(MSG_EXITO, STR_CAPTION, MB_OK);
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR_LIMPIAR, e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        PREFAC_ConveniosApartadosIncluidos_DELETE.Free;
    end;

    CargarComprobantes(False);
    CargarDatosProcesoPreFacturacion;
end;

end.
