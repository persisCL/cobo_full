{-------------------------------------------------------
    DTEControlDLL.pas

    Author: mbecerra
    Date: 05-Marzo-2009
    Description:	Unit de interface entre la DLL de la factura electr�nica de
                	la suite DBNet, y Delphi.

    	Notas:
        1.-		Hay dos maneras de usar la DLL en Delphi: est�tica o din�micamente.

                est�ticamente:
                				function MiDll (Msg : PChar) : integer; cdecl; external 'Mensaje.dll';


                din�micamente:
                            	handle := LoadLibrary(Mensaje.dll),
                                @dir := GetProcAddress (handle, MiDLL)
                                FreeLibrary(handle)


        2.-     La DLL de DBNET necesita la variable de ambiente EGATE_HOME

        3.-		Al usar la DLL de manera est�tica, la variable de ambiente debe estar
            	previamente configurada. De lo contrario se obtendr� un Access Violation.

                Esto conlleva el problema de tener que configurar previamente dicha
                variable en cada computador donde se use la aplicaci�n con la DLL,
                ya que al momento de iniciarse la aplicaci�n, la DLL lee esta variable
                de ambiente.

                Por lo tanto, si la DLL est� declarada de manera est�tica de nada servir�
                configurar una variable de ambiente desde DELPHI.

        4.-		Al usar de manera din�mica la DLL, podemos configurar la variable de ambiente
            	desde DELPHI, y luego cargar la DLL.

                Luego de pruebas con ambos m�todos, se ha optado por este �ltimo.

Revision : 1
    Author : pdominguez
    Date   : 16/11/2009
    Description : SS 845
        - Hacemos p�blica la declaraci�n de la variable hndDLLHandle.

Revision : 2
    Author : Nelson Droguett Sierra
    Date   : 04-Junio-2010
    Description : SS 892
        - Copiar solo los archivos necesarios de DBNet al equipo local cuando
        levanta la dll, ejecutando JORDAN
--------------------------------------------------------}
unit DTEControlDLL;

interface

    uses SysUtils, Windows, ConstParametrosGenerales, ADODB, Util, dialogs, ShellApi, Forms;

    {Tipos de datos retornados por las funciones de la DLL}
	type
    	RptaSolicitaFolioDll = record
			Folio : integer;
			Codigo: array[1..11] of Char; //[OK, ERR]
			Mensaje : array[1..101] of Char;
        end;

        RptaAccionFolioDll = record
        	Codigo : array[1..11] of Char;
            Mensaje : array[1..101] of Char;
        end;

        RptaSolicitaTedDll = record
			strTed : array[1..1001] of Char;
			strTms : array[1..31] of Char;
			Codigo : array[1..11] of Char;
        	Mensaje: array[1..101] of Char;
        end;


    //funciones existentes en la DLL

    { Declaraciones est�ticas

	function SolicitaFolio( folio : integer ) : RptaSolicitaFolioDll; cdecl; external 'DteControlDll.dll';
	function ConfirmaFolio( folio : integer ) : RptaAccionFolioDll; cdecl; external 'DteControlDll.dll';
	function ReversaFolio( folio : integer ) : RptaAccionFolioDll; cdecl; external 'DteControlDll.dll';
    function  SolicitaTed (	RutEmisor : integer; DigitoVerEmisor : PChar; TipoDocumento, Folio : integer;
   							FechaHoraEmision : PChar; Monto : double; GlosaItem1 : PChar;
                            RutReceptor : integer; DigitoVerReceptor,
                            RazonSocialReceptor, FechaHoraTimbre : PChar): RptaSolicitaTedDll; cdecl; external 'DteControlDll.dll';
	}


    //Declaraciones Din�micas
    TSolicitaTimbre = function	(	RutEmisor : integer; DigitoVerEmisor : PChar;
    								TipoDocumento, Folio : integer;
   									FechaHoraEmision : PChar; Monto : double;
                                    GlosaItem1 : PChar;
                            		RutReceptor : integer; DigitoVerReceptor,
                            		RazonSocialReceptor, FechaHoraTimbre : PChar
    							): RptaSolicitaTedDll; cdecl;


    //Funciones para ser invocada desde las aplicaciones que usen la DLL
    TTimbreArchivo = record
        Timbre  : string;         {string que contiene el timbre}
        Archivo : string;        {nombre del archivo de la forma EuuuuuuuuuTaaaFdddddddddd}
        Tms     : string;
        Codigo  : string;
        Mensaje : string;
    end;

    function CargarDLL : integer;
    function LiberarDLL : boolean;
    function ObtieneErrorAlCargarDLL(CodigoError : integer) : string;

    function ObtenerTimbreDBNet(RutEmisor : integer; DigVerEmisor : string; TipoDocumento, Folio : integer;
   							FechaHoraEmision : TDateTime; Monto : double; GlosaItem1 : string;
                            RutReceptor : integer; DigVerReceptor, RazonSocialReceptor: string;
                            FechaHoraTimbre : TDateTime; GenerarImagen : boolean; var TimbreArchivo : TTimbreArchivo ) : boolean;


    function ConfigurarVariable_EGATE_HOME(Conexion : TADOConnection; EsWeb : boolean; var MensajeError : string; EsJordan : boolean = False ) : Boolean;

    procedure CopyDirectory( src, dest : string );
    //Rev.2 / 04-Junio-2010 / Nelson Droguett Sierra
    function DelTree(DirName : string): Boolean;

    function GetRutaImg(): string;

var
	NombreParametro_EGATE,				//contiene el nombre del par�metro general
    PARAM_DIR_EGATE_HOME : string;		// esta variable indica la ruta ra�z donde
                                    	// est� la DLL. Se espera una estructura de la forma:
                                        //	--suite
                                        //		|-- bin
                                        //		|-- config
                                        //				|-- caf
                                        //				|-- par
                                        //		|-- in
                                        //				|--ted
                                        //		|-- log
                                        //		|-- out
                                        //				|-- html
                                        //
                                        //	* La variable debe apuntar a la carpeta ra�z "suite" (ejemplo: \\pino\op_test\suite)
                                        //		OJO: No sirve la direcci�n del tipo \\172.16.3.32\op_test\suite, pues la DLL da error.
                                        //	* La DLL debe estar en la carpeta bin
                                        //	* En la carpeta "in\ted" est�n los archivos "*.ted" que sirven de entrada para el PDF417
                                        //	* El PFD417, archivo JPG, queda en la carpeta "out\html"
                                        //
                                        //	* Es necesario borrar los archivos *.ted, *.jpg.
                                        //	* En la carpeta "log" existe un archivo DTEControl.log que se incrementa con cada
                                        //		llamada a la funci�n SolicitaTed.
                                        //		Si todo fue OK, este archivo deber�a borrarse
                                        //
                                        //
                                        // Los archivos generados tienen la forma:
                                        //		EuuuuuuuuuTaaaFdddddddddd.ext
                                        //
                                        //	donde:
                                        //			ext		: extensi�n (ted o jpg)
                                        //			E, T, F : constantes fijas
                                        //			uuuu	: rut emisor, nueve d�gitos fijos
                                        //			aaa		: tipo de documento: ejemplo: 039 = Boleta
                                        //			dddd	: folio o n�mero del documento de diez d�gitos
                                        //
                                        //
	hndDLLHandle: THandle;  //handle de la DLL // Rev. 1 (SS 845)
                                        
implementation
var
    SolicitaTimbre, GeneraImagen : TSolicitaTimbre;
    FuncionListaEnDLL : boolean;

{----------------------------------------------------------------------------
            	CargarDLL

	Author: mbecerra
    Date: 05-Marzo-2009
    Description:	(Ref: Facturaci�n Electr�nica)
                	Carga la DLL de DBNet en el programa.

                    Retorna:
                    	 0 = �xito
                        -1 = la variable PARAM_DIR_EGATE_HOME no est� configurada
                        -2 = no existe la carpeta indicada en PARAM_DIR_EGATE_HOME
                        -3 = no se encuentra la carpeta bin
                        -4 = no se encuentra el archivo DteControlDll.dll
                        -5 = no se pudo cargar la DLL
                        -6 = no se pudo cargar la funci�n SolicitaTed
                        -7 = no se pudo cargar la funcion GeneraImg
                        -8 = Otro Error general
--------------------------------------------------------------------------------}
function CargarDLL;
resourcestring
	DIR_BIN			= 'bin';
    FILE_DLL		= 'DteControlDll.dll';
    FUNCTION_DLL	= 'SolicitaTed';
    FUNCTION_IMG	= 'GeneraImg';
var
    ArchivoDLL : string;
begin
	FuncionListaEnDLL := False;

	Result := -1;
    if PARAM_DIR_EGATE_HOME = '' then Exit;

    Result := -2;
    if not DirectoryExists(PARAM_DIR_EGATE_HOME) then Exit;

    Result := -3;
    if not DirectoryExists(PARAM_DIR_EGATE_HOME + DIR_BIN) then Exit;

    Result := -4;
    ArchivoDLL := GoodDir(PARAM_DIR_EGATE_HOME + DIR_BIN) + FILE_DLL;
    if not FileExists(ArchivoDLL) then Exit;

    Result := -5;
    try
    	hndDLLHandle := LoadLibrary ( PChar(ArchivoDLL) );
		if hndDLLHandle <> 0 then begin
        	Result := -6;
    		@SolicitaTimbre := GetProcAddress(hndDLLHandle, PChar(FUNCTION_DLL));
        	if Addr(SolicitaTimbre) <> nil then begin
            	Result := -7;
            	@GeneraImagen := GetProcAddress(hndDLLHandle, PChar(FUNCTION_IMG));
                if Addr(SolicitaTimbre) <> nil then begin
            		Result := 0;
                	FuncionListaEnDLL := True;
                end;
        	end;
    	end;

    except
        Result := -8;
    end;
end;

{----------------------------------------------------------------------------
            	LiberarDLL

	Author: mbecerra
    Date: 05-Marzo-2009
    Description:	(Ref: Facturaci�n Electr�nica)
                	Carga la DLL de DBNet en el programa.
--------------------------------------------------------------------------------}
function LiberarDLL;
begin
	try
        if hndDLLHandle >0 then
           	FreeLibrary(hndDLLHandle);
        FuncionListaEnDLL := False;
    	Result := True;
    except
    	Result := False;
    end;
end;


{----------------------------------------------------------------------------
            	ObtieneErrorAlCargarDLL

	Author: mbecerra
    Date: 13-Marzo-2009
    Description:	(Ref: Facturaci�n Electr�nica)
                	retorna un string indicando el mensaje de error en la carga de la DLL.
--------------------------------------------------------------------------------}
function ObtieneErrorAlCargarDLL;
resourcestring
    MSG_MENOS_UNO		= 'La variable PARAM_DIR_EGATE_HOME no est� configurada. Utilice la funci�n: ConfigurarVariable_EGATE_HOME()';
    MSG_MENOS_DOS		= 'No se encuentra la carpeta %s, indicada en el par�metro general %s';
    MSG_MENOS_TRES		= 'No se encuentra la carpeta bin dentro de la carpeta %s';
    MSG_MENOS_CUATRO	= 'No se encuentra el archivo DteControlDll.dll en la carpeta %sbin';
	MSG_MENOS_CINCO		= 'No se pudo cargar la DLL en memoria';
    MSG_MENOS_SEIS		= 'No se pudo cargar la funci�n SolicitaTed en la DLL';
    MSG_MENOS_SIETE		= 'No se pudo cargar la funci�n GeneraImg en la DLL';
    MSG_MENOS_OCHO		= 'Error inesperado en la carga de la DLL';
    MSG_DESCONOCIDO		= 'Par�metro: %d Codigo error desconocido';
begin
    case CodigoError of
    	-1 : Result := MSG_MENOS_UNO;
        -2 : Result := Format(MSG_MENOS_DOS, [PARAM_DIR_EGATE_HOME, NombreParametro_EGATE]);
        -3 : Result := Format(MSG_MENOS_TRES, [PARAM_DIR_EGATE_HOME]);
        -4 : Result := Format(MSG_MENOS_CUATRO, [PARAM_DIR_EGATE_HOME]);
        -5 : Result := MSG_MENOS_CINCO;
        -6 : Result := MSG_MENOS_SEIS;
        -7 : Result := MSG_MENOS_SIETE;
        -8 : Result := MSG_MENOS_OCHO;
        else
        	Result := Format(MSG_DESCONOCIDO, [CodigoError]);
    end;
end;

{----------------------------------------------------------------------------
            	ObtenerTimbreDBNet

	Author: mbecerra
    Date: 04-Marzo-2009
    Description:	(Ref: Facturaci�n Electr�nica)
                	Esta funci�n es una interfaz entre Delphi y la funci�n SolicitaTed
                    de la DLL de DBNet.

                    En caso de �xito, el par�metro "TimbreArchivo" contiene el
                    string entregado por la DLL y el nombre del archivo de acuerdo al
                    formato EuuuuuuuuuTaaaFdddddddddd

                    Notas:
                    	1.- 	no se hacen validaciones sobre la correctitud de los
                    			par�metros, como TipoDocumento, Rut y Fechas, o largo de los strings.

                        2.-		No olvidar que la funci�n SolicitaTED depende de que
                                est� configurada en la sesi�n la variable de
                                ambiente EGATE_HOME.
                                Si no est� o est� mal configurada, arrojar� un error
                                de Access Violation
--------------------------------------------------------------------------------}
function ObtenerTimbreDBNet;
var
    Respuesta : RptaSolicitaTedDll;
begin
    Result := False;
    TimbreArchivo.Timbre := '';
    TimbreArchivo.Archivo := '';
    try
    	if FuncionListaEnDLL then begin
            if GenerarImagen then begin
            	Respuesta := GeneraImagen	(	RutEmisor, PChar(DigVerEmisor),
            									TipoDocumento, Folio,
                								PChar(FormatDateTime('yyyy-mm-dd', FechaHoraEmision)),
                								Monto, PChar(GlosaItem1),
                								RutReceptor, PChar(DigVerReceptor),
                								PChar(RazonSocialReceptor),
                								PChar(FormatDateTime('yyyy-mm-dd', FechaHoraTimbre) +
                								'T' + FormatDateTime('hh:nn:ss', FechaHoraTimbre) )
            								);

            end
            else begin
        		Respuesta := SolicitaTimbre	(	RutEmisor, PChar(DigVerEmisor),
            									TipoDocumento, Folio,
                								PChar(FormatDateTime('yyyy-mm-dd', FechaHoraEmision)),
                								Monto, PChar(GlosaItem1),
                								RutReceptor, PChar(DigVerReceptor),
                								PChar(RazonSocialReceptor),
                								PChar(FormatDateTime('yyyy-mm-dd', FechaHoraTimbre) +
                								'T' + FormatDateTime('hh:nn:ss', FechaHoraTimbre) )
            								);
            end;

            TimbreArchivo.Timbre := Respuesta.strTed;
            TimbreArchivo.Archivo := Format('E%.09dT%.03dF%.10d', [RutEmisor, TipoDocumento, Folio]);
            TimbreArchivo.Tms := Respuesta.strTms;
            TimbreArchivo.Codigo := Respuesta.Codigo;
            TimbreArchivo.Mensaje := Respuesta.Mensaje;
            Result := True;
        end;

    except
    	Result := False;
    end;

end;

{----------------------------------------------------------------------------
            	ConfigurarVariable_EGATE_HOME

	Author: mbecerra
    Date: 12-Marzo-2009
    Description:	(Ref: Facturaci�n Electr�nica)
                	Configura la variable de ambiente EGATE_HOME, con el par�metro
                    indicado en la tabla par�metros generales.
--------------------------------------------------------------------------------}
function ConfigurarVariable_EGATE_HOME;
resourcestring
    DIR_EGATE		 = 'DIR_TIMBRE_ELECTRONICO';
    DIR_WEB_EGATE	 = 'DIR_WEB_TIMBRE_ELECTRONICO';
    DIR_JORDAN_EGATE = 'DIR_JORDAN_TIMBRE_ELECTRONICO';
    NOMBRE_EGATE	 = 'EGATE_HOME';
    MSG_ERROR_PARAM	 = 'No se encontr� el par�metro general %s';
    MSG_ERROR_SETEN	 = 'No se pudo configurar la variable de ambiente';
    MSG_ERROR_FOLDER = 'No se pudo crear la carpeta local para la impres�n masiva';
var
	CarpetaSuite, Valor : string;
    origenSuite:string;
begin
	Result := False;
    MensajeError := '';
    PARAM_DIR_EGATE_HOME := '';

	{2009-07-08:	Permitir tomar la configuraci�n de la variable desde el
                	archivo install.INI o desde el Par�metro general}
    CarpetaSuite := InstallIni.ReadString('General', NOMBRE_EGATE, '');
    if CarpetaSuite = '' then begin 					//si no est�, tomarlo desde par�metros generales
        if EsJordan then NombreParametro_EGATE := DIR_JORDAN_EGATE
        else if EsWeb then NombreParametro_EGATE := DIR_WEB_EGATE
        else NombreParametro_EGATE := DIR_EGATE;

        if not ObtenerParametroGeneral(Conexion, NombreParametro_EGATE, CarpetaSuite) then begin
            MensajeError := Format(MSG_ERROR_PARAM, [DIR_EGATE]);
        end;
    end;

    if EsJordan then begin			//Copiar los archivos DBNet al disco Local

	    //Rev.2 / 04-Junio-2010 / Nelson Droguett Sierra
         Deltree(ExpandFileName(GoodDir(CarpetaSuite)+'..'));
       //if not(DirectoryExists(CarpetaSuite)) then
       //begin
         ObtenerParametroGeneral(Conexion, 'DIR_TIMBRE_ELECTRONICO', origenSuite);
         if ForceDirectories(GoodDir(CarpetaSuite)) then
         begin
             try
			   //Rev.2 / 04-Junio-2010 / Nelson Droguett Sierra
               ForceDirectories(GoodDir(CarpetaSuite)+'bin');
               ForceDirectories(GoodDir(CarpetaSuite)+'config\caf');
               ForceDirectories(GoodDir(CarpetaSuite)+'config\par');
               ForceDirectories(GoodDir(CarpetaSuite)+'log');
               ForceDirectories(GoodDir(CarpetaSuite)+'in\ted');
               ForceDirectories(GoodDir(CarpetaSuite)+'out\html');

               CopyDirectory(GoodDir(origenSuite)+'bin\',GoodDir(CarpetaSuite)+'bin\');
               CopyDirectory(GoodDir(origenSuite)+'config\',GoodDir(CarpetaSuite)+'config\');
               //FinRev.2
             except
               MensajeError := MSG_ERROR_FOLDER;
             end;
         end
         else
         begin
           MensajeError := MSG_ERROR_FOLDER;
         end
       //end;
    end;

    if MensajeError = '' then begin
    	if SetEnvironmentVariable(PChar(NOMBRE_EGATE), PChar(CarpetaSuite)) then begin
        	Valor := GetEnvironmentVariable(NOMBRE_EGATE);
            Result := (Valor = CarpetaSuite);
            if Result then PARAM_DIR_EGATE_HOME := GoodDir(Valor)
            else MensajeError := MSG_ERROR_SETEN;
        end
        else MensajeError := MSG_ERROR_SETEN;
    end;
end;

procedure CopyDirectory( src, dest : string );
var
  sts : Integer ;
  SR: TSearchRec;
begin
  sts := FindFirst( src + '*.*' , faAnyFile , SR );
  if sts = 0 then
    begin
      if ( SR.Name <> '.' ) and ( SR.Name <> '..' ) then
        begin
          //Put User Feedback here if desired
          Application.ProcessMessages;
          if pos('.', SR.Name) = 0 then
            begin
              {$I-}MkDir( dest + SR.Name ) ;{$I+}
             CopyDirectory( src + SR.Name + '\', dest +
                                    SR.Name + '\' ) ;
            end
          else
            copyfile( pchar(src + SR.Name), pchar(dest + sr.name),
                          true );
        end;
      while FindNext( SR ) = 0 do
        begin
          if ( SR.Name <> '.' ) and ( SR.Name <> '..' ) then
            begin
              //Put User Feedback here if desired
              Application.ProcessMessages;
              if Pos('.', SR.Name) = 0 then
                begin
                  {$I-}MkDir( dest + SR.Name );{$I+}
                  CopyDirectory( src + SR.Name + '\', dest + SR.Name
                                    + '\' ) ;
                end
              else
                copyfile( pchar(src + SR.Name), pchar(dest +
                              sr.name), true );
            end;
        end;
      FindClose( SR.FindHandle ) ;
    end ;
end;

//Rev.2 / 04-Junio-2010 / Nelson Droguett Sierra
function DelTree(DirName: string): Boolean;
var
  SHFileOpStruct : TSHFileOpStruct;
  DirBuf : array [0..255] of char;
begin
  try
   Fillchar(SHFileOpStruct,Sizeof(SHFileOpStruct),0) ;
   FillChar(DirBuf, Sizeof(DirBuf), 0 ) ;
   StrPCopy(DirBuf, DirName) ;
   with SHFileOpStruct do begin
    Wnd := 0;
    pFrom := @DirBuf;
    wFunc := FO_DELETE;
    fFlags := FOF_ALLOWUNDO;
    fFlags := fFlags or FOF_NOCONFIRMATION;
    fFlags := fFlags or FOF_SILENT;
   end;
    Result := (SHFileOperation(SHFileOpStruct) = 0) ;
   except
    Result := False;
  end;
end;
//FinRev.2

function GetRutaImg():string;
begin
    Result := PARAM_DIR_EGATE_HOME + 'out\html\';
end;

end.
