unit DatosEntrada;

interface

type TDatosEntrada = class
    private
        _RutEmisor: Integer;
        _DigitoVerEmisor : string;
        _TipoDocumento: Integer;
        _Folio: Integer;
        _FechaHoraEmision: TDateTime;
        _Monto: Double;
        _GlosaItem1: string;
        _RutReceptor: Integer;
        _DigitoVerReceptor: string;
        _RazonSocialReceptor: string;
        _FechaHoraTimbre: TDateTime;
    public
        property RutEmisor: Integer read _RutEmisor write _RutEmisor;
        property DigitoVerEmisor: string read _DigitoVerEmisor write _DigitoVerEmisor;
        property TipoDocumento: Integer read _TipoDocumento write _TipoDocumento;  
        property Folio: Integer read _Folio write _Folio;
        property FechaHoraEmision: TDateTime read _FechaHoraEmision write _FechaHoraEmision;
        property Monto: Double read _Monto write _Monto;
        property GlosaItem1: string read _GlosaItem1 write _GlosaItem1;
        property RutReceptor: Integer read _RutReceptor write _RutReceptor;
        property DigitoVerReceptor: string read _DigitoVerReceptor write _DigitoVerReceptor;
        property RazonSocialReceptor: string read _RazonSocialReceptor write _RazonSocialReceptor;
        property FechaHoraTimbre: TDateTime read _FechaHoraTimbre write _FechaHoraTimbre;
end;

implementation

end.
