﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WinForms;

namespace GeneradorReportes
{

    public class ReporteToPDF
    {

        public bool GenerarReportePDF(string xml)
        {
            try
            {
                LectorXml xmlReader = new LectorXml();

                Reporte reporte = (Reporte)xmlReader.XMLtoObject<Reporte>(xml, "Reporte");

                byte[] Archivo = CrearPdf(reporte);

                if (Archivo != null)
                {
                    string nombreArchivo = "documento" + reporte.DatosDocumento.NumeroDocumento + reporte.DatosDocumento.FechaEmision + ".pdf";
                    using (FileStream fs = new FileStream(nombreArchivo, FileMode.Create))
                    {
                        fs.Write(Archivo, 0, Archivo.Length);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;

                WriteLog("LogDllReporte.txt", " GenerarReportePDF: " + ex.Message);
            }

            return false;

        }
/*INICIO: TASK_043_JMA_20160718_Revisión de Web Service ObtenerBoleta */
        private void WriteLog(string archivo, string mensaje)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(mensaje);
            using (FileStream fs = new FileStream(archivo, FileMode.Append))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        }
/*TERMINO: TASK_043_JMA_20160718_Revisión de Web Service ObtenerBoleta */

        public String GenerarReporteBase64(string xml)
        {
            try
            {
                LectorXml xmlReader = new LectorXml();

                Reporte reporte = (Reporte)xmlReader.XMLtoObject<Reporte>(xml, "Reporte");
                                
                byte[] Archivo = CrearPdf(reporte);
                                
                if (Archivo != null)
                {
                    return Convert.ToBase64String(Archivo);
                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;

                throw new Exception(" GenerarReporteBase64: " + ex.Message);
            }

            return null;
        }

        private byte[] CrearPdf(Reporte reporte)
        {
            byte[] bytes = null;
            try
            {
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string filenameExtension = string.Empty;

                LocalReport report = CrearReporte(reporte);
                
                if (report != null)
                    bytes = report.Render("pdf");
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;

                throw new Exception(" CrearPdf: " + ex.Message);
            }
            return bytes;
        }

        private LocalReport CrearReporte(Reporte reporte)
        {
            LocalReport report = null;
            try
            {
                Assembly assembly = Assembly.GetCallingAssembly();
                Stream stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".Factura.rdlc");
                report = new LocalReport();
                report.LoadReportDefinition(stream);

                if (reporte.DatosDocumento == null)
                    throw new Exception(" CrearReporte: reporte.DatosDocumento = null");

                report.SetParameters(new ReportParameter("RUTConcesionaria", "R.U.T. " + reporte.DatosDocumento.RUTConcesionaria));
                report.SetParameters(new ReportParameter("TipoDocumento", reporte.DatosDocumento.TipoDocumento != null ? reporte.DatosDocumento.TipoDocumento.ToUpper() : ""));
                report.SetParameters(new ReportParameter("NumeroDocumento", "N° " + reporte.DatosDocumento.NumeroDocumento));

                if (reporte.DatosDocumento.TotalPagar == null)
                    throw new Exception(" CrearReporte: reporte.DatosDocumento.TotalPagar = null");


                report.SetParameters(new ReportParameter("FechaTotal", reporte.DatosDocumento.TotalPagar.Fecha));
                report.SetParameters(new ReportParameter("TotalPagar", reporte.DatosDocumento.TotalPagar.Monto));

                report.SetParameters(new ReportParameter("FechaEmision", reporte.DatosDocumento.FechaEmision));
                report.SetParameters(new ReportParameter("FechaVencimiento", reporte.DatosDocumento.FechaVencimiento));
                report.SetParameters(new ReportParameter("PeriodoFacturacion", (reporte.DatosDocumento.PeriodoFacturacion.Desde != null ? reporte.DatosDocumento.PeriodoFacturacion.Desde : "")
                    + " al " + (reporte.DatosDocumento.PeriodoFacturacion.Hasta != null ? reporte.DatosDocumento.PeriodoFacturacion.Hasta : "")));
                report.SetParameters(new ReportParameter("FechaUltimoPago", reporte.DatosDocumento.UltimoPago));
                report.SetParameters(new ReportParameter("CodigoInternoPago", reporte.DatosDocumento.CodigoInternoPago));
                report.SetParameters(new ReportParameter("InformacionImportante", reporte.DatosDocumento.InformacionImportante));

                if (reporte.DatosCliente == null)
                    throw new Exception(" CrearReporte: reporte.DatosCliente = null");

                report.SetParameters(new ReportParameter("NombreCliente", reporte.DatosCliente.NombreCompleto));
                report.SetParameters(new ReportParameter("DireccionLinea1", reporte.DatosCliente.DireccionLinea1));
                report.SetParameters(new ReportParameter("DireccionLinea2", reporte.DatosCliente.DireccionLinea2));
                report.SetParameters(new ReportParameter("NumeroConvenio", reporte.DatosCliente.NumeroConvenio));
                report.SetParameters(new ReportParameter("Linea1", reporte.DatosCliente.Linea1));
                report.SetParameters(new ReportParameter("Linea2", reporte.DatosCliente.Linea2));
                report.SetParameters(new ReportParameter("Linea3", reporte.DatosCliente.Linea3));
                report.SetParameters(new ReportParameter("Linea4", reporte.DatosCliente.Linea4));

                report.SetParameters(new ReportParameter("ParametroImgTimbre", reporte.TimbreElectronico));
                report.SetParameters(new ReportParameter("CodigoBarra", reporte.CodigoBarras));

                //SELLO------------------      

                if (reporte.DatosDocumento.TipoMedioPagoAutomatico == "PAT")
                    stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".Imagenes.sello_pat.jpg");
                else if (reporte.DatosDocumento.TipoMedioPagoAutomatico == "PAC")
                    stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".Imagenes.sello_pac.jpg");
                else
                    stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".Imagenes.sello_servipag.jpg");

                byte[] bytes = new byte[stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                report.SetParameters(new ReportParameter("ImgSello", Convert.ToBase64String(bytes)));
                //FIN SELLO--------------------

                List<ItemDetalleConsumo> ListaDetallesConsumos = new List<ItemDetalleConsumo>();
                if (reporte.DetalleConsumo.DetallesConsumos.Count > 11)
                    ListaDetallesConsumos.AddRange(reporte.DetalleConsumo.DetallesConsumos.GetRange(0, 10));
                else
                    ListaDetallesConsumos = reporte.DetalleConsumo.DetallesConsumos;

                List<ItemDetalleCuenta> ListaDetallesCuentas = new List<ItemDetalleCuenta>();
                if (reporte.DetalleCuenta.DetallesCuenta.Count > 23)
                    ListaDetallesCuentas.AddRange(reporte.DetalleCuenta.DetallesCuenta.GetRange(0, 22));
                else
                    ListaDetallesCuentas = reporte.DetalleCuenta.DetallesCuenta;

                List<ItemEstadoCuenta> ListaEstadosCuentas = new List<ItemEstadoCuenta>();
                if (reporte.EstadoCuenta.EstadosCuentas.Count > 10)
                    ListaEstadosCuentas.AddRange(reporte.EstadoCuenta.EstadosCuentas.GetRange(0, 10));
                else
                    ListaEstadosCuentas = reporte.EstadoCuenta.EstadosCuentas;

                List<ItemHistorico> ListaFaturas = new List<ItemHistorico>();
                if (reporte.FacturasUltimos12Meses.Historicos.Count > 12)
                    ListaFaturas.AddRange(reporte.FacturasUltimos12Meses.Historicos.GetRange(0, 11));
                else
                    ListaFaturas = reporte.FacturasUltimos12Meses.Historicos;

                if (ListaFaturas.Count > 0)
                    report.SetParameters(new ReportParameter("HideChart", "False"));


                report.DataSources.Add(new ReportDataSource("DsDetalleConsumo", ListaDetallesConsumos));
                report.DataSources.Add(new ReportDataSource("DsDetalleCuenta", ListaDetallesCuentas));
                report.DataSources.Add(new ReportDataSource("DsFacturasUltimos12Meses", ListaFaturas));
                report.DataSources.Add(new ReportDataSource("DsEstadoCuenta", ListaEstadosCuentas));

                report.SetParameters(new ReportParameter("TotalConsumo", reporte.DetalleConsumo.TotalConsumo));
                report.SetParameters(new ReportParameter("TotalCuenta", reporte.DetalleCuenta.TotalCuenta));

                report.Refresh();

            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                throw new Exception(" CrearReporte: " + ex.Message);
            }

            return report;

        }

        private string MaskedString(String value)
        {
            var pattern = "^(/d{3})(/d{3})(/d{3})$";
            var regExp = new Regex(pattern);
            return regExp.Match(value).ToString();
        }
    }
}
