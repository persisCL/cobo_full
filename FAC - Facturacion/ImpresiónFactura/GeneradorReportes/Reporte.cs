﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace GeneradorReportes
{   
    
    [XmlRoot("Reporte")]
    public class Reporte
    {        
        [XmlElement("DatosDocumento")]
        public DatosDocumento DatosDocumento { get; set; }
        
        [XmlElement("DatosCliente")]
        public DatosCliente DatosCliente { get; set; }

        [XmlElement("DetalleCuenta")]
        public DetalleCuenta DetalleCuenta { get; set; }   
             
        [XmlElement("DetalleConsumo")]
        public DetalleConsumo DetalleConsumo { get; set; }       

        [XmlElement("FacturasUltimos12Meses")]
        public FacturasUltimos12Meses FacturasUltimos12Meses { get; set; }

        [XmlElement("EstadoCuenta")]
        public EstadoCuenta EstadoCuenta { get; set; }
        
        [XmlElement("CodigoBarras")]
        public string CodigoBarras { get; set; }

        [XmlElement("TimbreElectronico")]
        public string TimbreElectronico { get; set; }
    }

    public class DatosDocumento
    {
        [XmlElement("RUTConcesionaria")]
        public String RUTConcesionaria { get; set; }
        [XmlElement("TipoDocumento")]
        public String TipoDocumento { get; set; }
        [XmlElement("NumeroDocumento")]
        public String NumeroDocumento { get; set; }
        [XmlElement("FechaEmision")]
        public string FechaEmision { get; set; }
        [XmlElement("FechaVencimiento")]
        public String FechaVencimiento { get; set; }
        [XmlElement("PeriodoFacturacion")]
        public PeriodoFacturacion PeriodoFacturacion { get; set; }
        [XmlElement("UltimoPago")]
        public string UltimoPago { get; set; }        
        [XmlElement("TotalPagar")]
        public TotalPagar TotalPagar { get; set; }
        [XmlElement("CodigoInternoPago")]
        public String CodigoInternoPago { get; set; }
        [XmlElement("InformacionImportante")]
        public String InformacionImportante { get; set; }
        [XmlElement("TipoMedioPagoAutomatico")]
        public String TipoMedioPagoAutomatico { get; set; }
    }

    public class TotalPagar
    {
        [XmlElement("Fecha")]
        public string Fecha { get; set; }
        [XmlElement("Monto")]
        public string Monto { get; set; }
    }

    public class PeriodoFacturacion
    {
        [XmlElement("Desde")]
        public string Desde { get; set; }
        [XmlElement("Hasta")]
        public string Hasta { get; set; }
    }

    public class DatosCliente
    {        
        [XmlElement("NombreCompleto")]
        public string NombreCompleto { get; set; }
        [XmlElement("DireccionLinea1")]
        public string DireccionLinea1 { get; set; }
        [XmlElement("DireccionLinea2")]
        public string DireccionLinea2 { get; set; }
        [XmlElement("NumeroConvenio")]
        public string NumeroConvenio { get; set; }
        [XmlElement("Linea1")]
        public string Linea1 { get; set; }
        [XmlElement("Linea2")]
        public string Linea2 { get; set; }
        [XmlElement("Linea3")]
        public string Linea3 { get; set; }
        [XmlElement("Linea4")]
        public string Linea4 { get; set; }
    }

    public class DetalleCuenta
    {
        [XmlElement("Total")]
        public String TotalCuenta { get; set; }
        [XmlArray("Items")]
        public List<ItemDetalleCuenta> DetallesCuenta { get; set; }                     
    }

    public class ItemDetalleCuenta
    {
        [XmlElement("Descripcion")]
        public string Descripcion { get; set; }
        [XmlElement("Monto")]
        public string Monto { get; set; }
    }

    public class DetalleConsumo
    {
        [XmlElement("Total")]
        public string TotalConsumo { get; set; }
        [XmlArray("Items")]
        public List<ItemDetalleConsumo> DetallesConsumos { get; set; }
    }

    public class ItemDetalleConsumo
    {
        [XmlElement("Patente")]
        public string Patente { get; set; }
        [XmlElement("Sistema")]
        public string Sistema { get; set; }        
        [XmlElement("KmPasadas")]
        public string KmPasadas { get; set; }
        [XmlElement("TarifaFueraPunta")]
        public string TarifaFueraPunta { get; set; }
        [XmlElement("TarifaPunta")]
        public string TarifaPunta { get; set; }
        [XmlElement("TarifaSaturacion")]
        public string TarifaSaturacion { get; set; }
        [XmlElement("TotalPatente")]
        public string TotalPatente { get; set; }
    }

    public class EstadoCuenta
    {
        [XmlArray("Items")]
        public List<ItemEstadoCuenta> EstadosCuentas { get; set; }        
    }

    public class ItemEstadoCuenta
    {
        [XmlElement("Descripcion")]
        public string Descripcion { get; set; }
        [XmlElement("Monto")]
        public string Monto { get; set; }
    }
       
    public class FacturasUltimos12Meses
    {
        [XmlArray("Items")]
        public List<ItemHistorico> Historicos { get; set; }
    }

    public class ItemHistorico
    {       
        [XmlElement("Mes")]
        public string Fecha { get; set; }
        [XmlElement("Monto")]
        public double Monto { get; set; }
    }
}
