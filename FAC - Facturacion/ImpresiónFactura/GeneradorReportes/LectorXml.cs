﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace GeneradorReportes
{
    public class LectorXml
    {
        public T XMLtoObject<T>(string xml, string root)
        {
            try
            {                
                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                if (xml.StartsWith(_byteOrderMarkUtf8))                    
                    xml = xml.Remove(0, _byteOrderMarkUtf8.Length - 1);   

                XmlSerializer serializer = new XmlSerializer(typeof(T), new XmlRootAttribute(root));

                using (TextReader reader = new StringReader(xml))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch(Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                throw new Exception(ex.Message);
            }  
        }

        public string LeerArchivo(string ruta)
        {
            try
            {                               
                FileStream leer = File.OpenRead(ruta);

                byte[] contenido = new byte[leer.Length];

                leer.Read(contenido, 0, (int)leer.Length);

                leer.Close();
               
                return Encoding.UTF8.GetString(contenido);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                throw new Exception(ex.Message);
            }
        }

        public string LeerRecurso(string resourseName)
        {
            try
            {         
                Assembly assembly = Assembly.GetCallingAssembly();
                Stream stream = assembly.GetManifestResourceStream(assembly.GetName().Name + "." + resourseName);

                byte[] contenido = new byte[stream.Length];

                stream.Read(contenido, 0, (int)stream.Length);                

                return Encoding.UTF8.GetString(contenido);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                throw new Exception(ex.Message);
            }
        }
    }  
}
