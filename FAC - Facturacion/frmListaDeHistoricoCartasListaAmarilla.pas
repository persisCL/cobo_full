{-----------------------------------------------------------------------------
 File Name      : frmListaDeHistoricoCartasListaAmarilla
 Author         : Claudio Quezada Ib��ez
 Date Created   : 25-Febrero-2013
 Language       : ES-CL
 Description    : Formulario que muestra el hist�rico de cartas enviadas
-----------------------------------------------------------------------------}
unit frmListaDeHistoricoCartasListaAmarilla;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, ExtCtrls, UtilProc, DB, ADODB, Util;

type
  TListaDeHistoricoCartasListaAmarillaForm = class(TForm)
    pnlFiltro: TPanel;
    lblConvenio: TLabel;
    lblEstadoConvenio: TLabel;
    lblDatoConvenio: TLabel;
    lblDatoEstadoConvenio: TLabel;
    grpHistorico: TGroupBox;
    dblHistoricoCartas: TDBListEx;
    pnlPie: TPanel;
    btnSalir: TButton;
    dsHistoricoCartas: TDataSource;
    spHistoricoCartas: TADOStoredProc;
    procedure ObtenerHistoricoDeCartas(IDConvenioInhabilitado : Integer);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblHistoricoCartasColumnsHeaderClick(Sender: TObject);
    procedure dblHistoricoCartasDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
  private
    { Private declarations }
    FColumnaAgrandada : Boolean;
  public
    { Public declarations }
    function Inicializar(IDConvenioInhabilitado : Integer; NumeroConvenio, Estado : string): boolean;
  end;

var
  ListaDeHistoricoCartasListaAmarillaForm: TListaDeHistoricoCartasListaAmarillaForm;

implementation

{$R *.dfm}

function TListaDeHistoricoCartasListaAmarillaForm.Inicializar;
resourcestring
    MSG_ERROR_SPID = 'Se ha producido un error determinando el Id de sesi�n de base de datos';
Var
	S: TSize;
begin
    CenterForm(self);

    lblDatoConvenio.Caption         :=  NumeroConvenio;
    lblDatoEstadoConvenio.Caption   :=  Estado;
    FColumnaAgrandada               :=  False;

    ObtenerHistoricoDeCartas(IDConvenioInhabilitado);

    Result := True;
end;

procedure TListaDeHistoricoCartasListaAmarillaForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TListaDeHistoricoCartasListaAmarillaForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

{*******************************************************************************
Function Name   :   ObtenerHistoricoDeCartas
Author          :   CQuezadaI
Date Created    :   25-Febrero-2012
Description     :   Obtiene los datos historicos de env�o de cartas
*******************************************************************************}
procedure TListaDeHistoricoCartasListaAmarillaForm.ObtenerHistoricoDeCartas(IDConvenioInhabilitado : Integer);
resourcestring
    ERROR_OBT_HIST_CARTAS = 'Error obteniendo el historial de cartas enviadas';
begin
    Screen.Cursor := crHourGlass;
    try
        try
            with spHistoricoCartas do begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value         := null;
                Parameters.ParamByName('@IDConvenioInhabilitado').Value := IDConvenioInhabilitado;
                Open;
            end;
        except
            on E: Exception do begin
                screen.Cursor := crDefault;
                MsgBoxErr(ERROR_OBT_HIST_CARTAS, e.message, caption, MB_ICONSTOP);
            end;
        end;
    finally
        dblHistoricoCartas.Invalidate;
        Screen.Cursor := crDefault;
    end;
end;

{---------------------------------------------------------------------------------
Function Name   : dblHistoricoDemandaColumnsHeaderClick
Author          : CQuezadaI
Date Created    : 25/02/2012
Description     : Ordena las columnas del dblListEx
Parameters      : Sender: TObject;
Return Value    : None
Firma           : SS_660_CQU_20121010
----------------------------------------------------------------------------------}
procedure TListaDeHistoricoCartasListaAmarillaForm.dblHistoricoCartasColumnsHeaderClick(Sender: TObject);
begin
	  if spHistoricoCartas.IsEmpty then Exit;

	  if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   	  else TDBListExColumn(sender).Sorting := csAscending;

	  spHistoricoCartas.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{---------------------------------------------------------------------------------
Function Name   : dblHistoricoCartasDrawText
Author          : CQuezadaI
Date Created    : 27/06/2013
Description     : Agranda la columna Observacion del dblListEx si supera los 500
Parameters      : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
                  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
                  var ItemWidth: Integer; var DefaultDraw: Boolean
Return Value    : None
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
procedure TListaDeHistoricoCartasListaAmarillaForm.dblHistoricoCartasDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
  if ((Column.FieldName = 'Observacion') and (Length(Text) > 200) and not FColumnaAgrandada)
  then begin
    Column.MinWidth   :=  500;
    Column.MaxWidth   :=  2500;
    Column.Width      :=  2000;
    FColumnaAgrandada :=  True;
  end;
end;

end.
