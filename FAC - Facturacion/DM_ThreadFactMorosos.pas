{ *******************************************************
    Author      : CQuezadaI
    Date        : 07-01-2013
    Description : Hilo con la ejecuci�n del proceso de facturaci�n masiva de morosos.
    Firma       : SS_660_20121010
******************************************************* }
unit DM_ThreadFactMorosos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Util, UtilDB, UtilProc, ADODB, DMConnection;

type
  TDMThreadFacturacionMorosos = class(TDataModule)
    FacturacionMasivaConveniosMorosos: TADOStoredProc;
	procedure DataModuleCreate(Sender: TObject);
  private
	{ Private declarations }
	Conn: TDMConnections;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDMThreadFacturacionMorosos.DataModuleCreate(Sender: TObject);
begin
    //Creamos el DMConnection
	Conn := TDMConnections.Create(Self);
    // Lo Configuramos para que sepa a d�nde conectarse
    Conn.SetUp;
	FacturacionMasivaConveniosMorosos.Connection := Conn.BaseCAC;
end;

end.
