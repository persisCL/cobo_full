object FrmPlanesComerciales: TFrmPlanesComerciales
  Left = 147
  Top = 155
  Width = 765
  Height = 518
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Planes Comerciales'
  Color = clBtnFace
  Constraints.MinHeight = 440
  Constraints.MinWidth = 570
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 445
    Width = 757
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 560
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 6
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 757
    Height = 234
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'65'#0'C'#243'digo        '
      
        #0'211'#0'Descripci'#243'n                                                ' +
        ' ')
    HScrollBar = True
    RefreshTime = 100
    Table = tblPlanesComerciales
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnProcess = DBList1Process
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 757
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object PageControl: TPageControl
    Left = 0
    Top = 267
    Width = 757
    Height = 178
    ActivePage = Tab_General
    Align = alBottom
    TabIndex = 0
    TabOrder = 3
    object Tab_General: TTabSheet
      Caption = 'General'
      Enabled = False
      object Label1: TLabel
        Left = 24
        Top = 47
        Width = 72
        Height = 13
        Caption = 'Descripci'#243'n:'
        FocusControl = txt_descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 25
        Top = 22
        Width = 39
        Height = 13
        Caption = 'C'#243'digo: '
        FocusControl = txt_CodigoPlanComercial
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 250
        Top = 76
        Width = 140
        Height = 13
        Caption = 'Importe Env'#237'o Domicilio:'
        FocusControl = neImporteEnvioDomicilio
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 24
        Top = 76
        Width = 91
        Height = 13
        Caption = 'Importe Detalle:'
        FocusControl = neImporteDetalle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_descripcion: TEdit
        Left = 116
        Top = 43
        Width = 369
        Height = 21
        Color = 16444382
        MaxLength = 60
        TabOrder = 1
      end
      object txt_CodigoPlanComercial: TNumericEdit
        Left = 117
        Top = 18
        Width = 96
        Height = 21
        TabStop = False
        Color = clBtnFace
        MaxLength = 4
        TabOrder = 0
        Decimals = 0
      end
      object neImporteDetalle: TNumericEdit
        Left = 116
        Top = 71
        Width = 96
        Height = 21
        Color = 16444382
        MaxLength = 6
        TabOrder = 2
        Decimals = 0
      end
      object neImporteEnvioDomicilio: TNumericEdit
        Left = 391
        Top = 71
        Width = 96
        Height = 21
        Color = 16444382
        MaxLength = 6
        TabOrder = 3
        Decimals = 0
      end
    end
    object tab_TipoPago: TTabSheet
      Caption = 'Tipos de Pago'
      ImageIndex = 2
      object clbTipoPago: TCheckListBox
        Left = 0
        Top = 0
        Width = 749
        Height = 150
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object Tab_Categorias: TTabSheet
      Caption = 'Categor'#237'as'
      ImageIndex = 3
      object clbCategorias: TCheckListBox
        Left = 0
        Top = 0
        Width = 749
        Height = 150
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object tab_AdhesionTipoCliente: TTabSheet
      Caption = 'Tipo Cliente - Tipo de Adhesi'#243'n'
      ImageIndex = 3
      object clbTipoAdhesionTipoCliente: TCheckListBox
        Left = 0
        Top = 0
        Width = 749
        Height = 150
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object TabObservaciones: TTabSheet
      Caption = 'Notas del Plan Comercial'
      ImageIndex = 4
      object MemoObservaciones: TMemo
        Left = 0
        Top = 0
        Width = 749
        Height = 150
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object ObtenerPlanComercialCategoria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    MarshalOptions = moMarshalModifiedOnly
    ProcedureName = 'ObtenerPlanComercialCategoria;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 282
    Top = 133
  end
  object tblPlanesComerciales: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'PlanesComerciales'
    Left = 104
    Top = 133
  end
  object tblPlanesComercialesCategorias: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'PlanesComercialesCategorias'
    Left = 136
    Top = 133
  end
  object EliminarPlanComercialTipoPago: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'PlanComercial'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM PlanesComercialesTipoPago'
      'WHERE PlanComercial = :PlanComercial')
    Left = 168
    Top = 101
  end
  object tblPlanesComercialesTipoPago: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'PlanesComercialesTipoPago'
    Left = 168
    Top = 133
  end
  object tblPlanesComercialesPosibles: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'PlanesComercialesPosibles'
    Left = 200
    Top = 133
  end
  object ObtenerPlanesComercialesPosibles: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPlanesComercialesPosibles;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 313
    Top = 133
  end
  object ObtenerPlanComercialTiposPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPlanComercialTiposPago;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 344
    Top = 133
  end
  object EliminarPlanComercialPosible: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'PLanComercial'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM PlanesComercialesPosibles'
      'WHERE PlanComercial = :PLanComercial')
    Left = 200
    Top = 101
  end
  object ELiminarPlanComercialCategoria: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'PlanComercial'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM PlanesComercialesCategorias'
      'WHERE PlanComercial = :PlanComercial')
    Left = 136
    Top = 101
  end
  object InsertarPlanComercial: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarPlanComercial;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ImporteHojaDetalle'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@ImporteEnvioDomicilio'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 1073741823
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 376
    Top = 133
  end
  object ActualizarPlanComercial: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'Descripcion'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 60
        Value = Null
      end
      item
        Name = 'ImporteHojaDetalle'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ImporteEnvioDomicilio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Observaciones'
        Attributes = [paNullable, paLong]
        DataType = ftMemo
        NumericScale = 255
        Precision = 255
        Size = 1073741823
        Value = Null
      end
      item
        Name = 'PlanComercial'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE PlanesComerciales'
      'SET Descripcion = :Descripcion,'
      '        ImporteHojaDetalle = :ImporteHojaDetalle,'
      '        ImporteEnvioDomicilio = :ImporteEnvioDomicilio,'
      '        Observaciones = :Observaciones'
      'WHERE'
      '        PlanComercial = :PlanComercial')
    Left = 248
    Top = 133
  end
end
