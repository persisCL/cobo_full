unit FinanciamientoDeDueda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, validate, Dateedit, Buttons, ExtCtrls, utilProc,
  DmiCtrls, util, ADODB, DMConnection, ComCtrls, BuscaClientes, navigator,
  ImgList, Grids, DBGrids, DB, peaTypes, peaProcs,
  UtilRB, ReporteDetalleViajes, DBCtrls, DBClient, DPSControls, ListBoxEx,
  DBListEx, UtilFacturacion;

type
  TFrmFinanciamientoDeuda = class(TForm)
    btnCancelar: TDPSButton;
    btnAceptar: TDPSButton;
    dsFacturas: TDataSource;
    dsDeudaPorCuenta: TDataSource;
    ObtenerDeudaCuenta: TADOStoredProc;
    ObtenerListaComprobantes: TADOStoredProc;
    ObtenerCliente: TADOStoredProc;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    gbFinanciamiento: TGroupBox;
    Label19: TLabel;
    neInteres: TNumericEdit;
    Label20: TLabel;
    Label22: TLabel;
    Label23: TLabel;
	neEntregaInicial: TNumericEdit;
    nePlazoPrimerCuota: TNumericEdit;
    neCantidadCuotas: TNumericEdit;
    gbPlanFinanciamiento: TGroupBox;
	PlanesFinanciamiento: TADOQuery;
    saPlanesFinanciamiento: TDataSource;
    cbReemplazarFinanciamientos: TCheckBox;
    btnCalcular: TDPSButton;
    Bevel1: TBevel;
    TabSheet1: TTabSheet;
    ClientDataSet: TClientDataSet;
    Label18: TLabel;
    Label29: TLabel;
    DBText2: TDBText;
    Label25: TLabel;
    DBText1: TDBText;
    Label26: TLabel;
    DBText3: TDBText;
    Label27: TLabel;
    DBText4: TDBText;
	Label28: TLabel;
    DBText5: TDBText;
    cbPlanesFinanciamiento: TComboBox;
    GenerarFinanciamiento: TADOStoredProc;
    ObtenerFinanciamientos: TADOStoredProc;
    Label2: TLabel;
    dsFinanciamientosVigentes: TDataSource;
	PagarComprobante: TADOStoredProc;
	CalcularCuotaFinanciamiento: TADOStoredProc;
    Label21: TLabel;
    lDeudaTotal: TLabel;
    Bevel2: TBevel;
    Label4: TLabel;
    lImporteInteres: TLabel;
    Label3: TLabel;
	lImporteCuota: TLabel;
    AgregarPagoComprobante: TADOStoredProc;
    dbgComprobantes: TDBListEx;
    dbgFinanciamiento: TDBListEx;
    dbgFinanciamientosActuales: TDBListEx;
	procedure btnAceptarClick(Sender: TObject);
	procedure cbPlanesFinanciamientoChange(Sender: TObject);
	procedure btnCalcularClick(Sender: TObject);
	procedure neInteresChange(Sender: TObject);
	procedure cbReemplazarFinanciamientosClick(Sender: TObject);
  private
	{ Private declarations }
	FCodigoCliente: Integer;
	FDeudaTotal: Double;
	procedure CargarDeuda;
	procedure InicializarCampos;
  public
	{ Public declarations }
	function inicializa(aCodigoCliente: Integer; aApellidoNombre: AnsiString): Boolean;
  end;

function GenerarFinanciamiento(aCodigoCliente: Integer; aApellidoNombre: AnsiString): Boolean;

implementation

{$R *.dfm}

function GenerarFinanciamiento(aCodigoCliente: Integer; aApellidoNombre: AnsiString): Boolean;
var F: TFrmFinanciamientoDeuda;
begin
	application.CreateForm(TFrmFinanciamientoDeuda, F);
	F.Inicializa(aCodigoCliente, aApellidoNombre);
	F.ShowModal;
	result := F.ModalResult = mrOk;
	F.Free;
end;

function TFrmFinanciamientoDeuda.inicializa(aCodigoCliente: Integer; aApellidoNombre: AnsiString): Boolean;
resourcestring
    CAPTION_FORM = 'Financiamiento de Deuda ( %s )';
    MSG_COL_TITLE_MONTO_ADEUDADO    = 'Monto Adeudado';
    MSG_COL_TITLE_DEUDA_CONVENIDA   = 'Deuda Convenida';
    MSG_COL_TITLE_IMPORTE_FINANCIADO = 'Importe Financiado';

begin
    with dbgFinanciamiento do begin
		Columns[0].Header.Caption := MSG_COL_TITLE_CUENTA;
        Columns[1].Header.Caption := MSG_COL_TITLE_DESC_CUENTA;
        Columns[2].Header.Caption := MSG_COL_TITLE_MONTO_ADEUDADO;
        Columns[3].Header.Caption := MSG_COL_TITLE_ENTREGA_INICIAL;
        Columns[4].Header.Caption := MSG_COL_TITLE_DEUDA_CONVENIDA;
        Columns[5].Header.Caption := MSG_COL_TITLE_INTERES;
        Columns[6].Header.Caption := MSG_COL_TITLE_IMPORTE_CUOTA;
    end;

    with dbgFinanciamientosActuales do begin
        Columns[0].Header.Caption := MSG_COL_TITLE_FECHA;
        Columns[1].Header.Caption := MSG_COL_TITLE_CUENTA;
        Columns[2].Header.Caption := MSG_COL_TITLE_DESC_CUENTA;
        Columns[3].Header.Caption := MSG_COL_TITLE_IMPORTE_FINANCIADO;
        Columns[4].Header.Caption := MSG_COL_TITLE_ENTREGA_INICIAL;
        Columns[5].Header.Caption := MSG_COL_TITLE_INTERES;
        Columns[6].Header.Caption := MSG_COL_TITLE_VIGENCIA;
        Columns[7].Header.Caption := MSG_COL_TITLE_CUOTAS;
		Columns[8].Header.Caption := MSG_COL_TITLE_IMPORTE_CUOTA;
    end;

    with dbgComprobantes do begin
        Columns[0].Header.Caption := MSG_COL_TITLE_COMPROBANTE;
        Columns[1].Header.Caption := MSG_COL_TITLE_FECHA;
        Columns[2].Header.Caption := MSG_COL_TITLE_FECHA_VENC;
        Columns[3].Header.Caption := MSG_COL_TITLE_IMPORTE;
        Columns[4].Header.Caption := MSG_COL_TITLE_CTE_AJUSTADO;
		Columns[5].Header.Caption := MSG_COL_TITLE_DEBITO;
    end;


	ClientDataSet.createDataSet;
	(ClientDataSet.FieldByName('Interes') as TFloatField).displayFormat := '#,##0.00';
	(ClientDataSet.FieldByName('EntregaInicial') as TFloatField).displayFormat := FORMATO_IMPORTE;
	(ClientDataSet.FieldByName('ImporteCuota') as TFloatField).displayFormat := FORMATO_IMPORTE;
	(ClientDataSet.FieldByName('DeudaConvenida') as TFloatField).displayFormat := FORMATO_IMPORTE;
	btnAceptar.Enabled := false;
	CargarPlanesFinanciamiento(DMConnections.BaseCAC, cbPlanesFinanciamiento, 1);

	FCodigoCliente := aCodigoCliente;
	Caption := Format(CAPTION_FORM, [trim(aApellidoNombre)]);
	cbPlanesFinanciamientoChange(cbPlanesFinanciamiento);
	update;

	InicializarCampos;
	// Cargamos la informacion del comprobante
	btnAceptar.Enabled := False;
	screen.Cursor := crHourGlass;
	try
		With ObtenerListaComprobantes do begin
			close;
			Parameters.ParamByName('@CodigoPersona').value := FCodigoCliente;
			Parameters.ParamByName('@Vencimiento').value := NowBase(DMConnections.BaseCAC);
			Parameters.ParamByName('@Estado').value := 'I';
			open;
		end;

		// Cargamos la deuda por cuenta
		CargarDeuda;

		// Cargamos los financiamiento de deuda actuales
		with ObtenerFinanciamientos do begin
			Parameters.ParamByName('@EstadoFinanciamiento').value := EF_VIGENTE;
			close;
			Parameters.paramByName('@CodigoPersona').value := FCodigoCliente;
			open;
			cbReemplazarFinanciamientos.enabled := not isEmpty;
		end;
		Result := True;
	except
		Result := False;
	end;
end;

procedure TFrmFinanciamientoDeuda.InicializarCampos;
begin
	FDeudaTotal := 0;
	lDeudaTotal.Caption := FormatFloat(FORMATO_IMPORTE, 0.00);
	lImporteInteres.Caption := FormatFloat(FORMATO_IMPORTE, 0.00);
	lImporteCuota.Caption := FormatFloat(FORMATO_IMPORTE, 0.00);
end;

Procedure TFrmFinanciamientoDeuda.CargarDeuda;
Begin
	screen.Cursor := crHourGlass;
	try
		with ObtenerDeudaCuenta do begin
			close;
			Parameters.paramByName('@CodigoPersona').value := FCodigoCliente;
			Parameters.paramByName('@ConsiderarFinanciamientos').value := cbReemplazarFinanciamientos.Checked;
			open;
			InicializarCampos;
			ClientDataSet.emptyDataSet;
			while not eof do begin
				ClientDataSet.AppendRecord([FieldByName('CodigoCuenta').asInteger,
											FieldByName('DescriCuenta').asString,
											FieldByName('DescriImporte').asString,
											FieldByName('Importe').asFloat,
											0, 0, 0, 0]);
				FDeudaTotal := FDeudaTotal + FieldByName('Importe').asFloat;
				next;
			end;
			first;
			lDeudaTotal.caption := formatFloat(FORMATO_IMPORTE,FDeudaTotal);
		end;
	finally
		screen.Cursor := crDefault;
	end;
end;


procedure TFrmFinanciamientoDeuda.btnAceptarClick(
  Sender: TObject);
resourcestring
    MSG_APLICAR_FINANCIAMIENTO = '�Est� seguro de aplicar el financiamiento calculado?';
    CAPTION_APLICAR_FINANCIAMIENTO = 'Aplicar Financiamiento';
    MSG_ERROR_APLICAR_FINANCIAMIENTO = 'No se pudo generar el financiamiento calculado.';
begin
	if MsgBox(MSG_APLICAR_FINANCIAMIENTO, CAPTION_APLICAR_FINANCIAMIENTO, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		screen.Cursor := crHourGlass;
		ObtenerListaComprobantes.DisableControls;
		ClientDataSet.disableControls;
		try
			try
				DMConnections.BaseCAC.BeginTrans;
   				// Generamos los financiamientos correspondientes
				with ClientDataSet do begin
					first;
					While not eof do begin
						GenerarFinanciamiento.Parameters.ParamByName('@CodigoCuenta').value :=
						  FieldByName('CodigoCuenta').asInteger;
						GenerarFinanciamiento.Parameters.ParamByName('@CodigoFinanciamiento').value :=
						  IVal(StrRight(cbPlanesFinanciamiento.Items[cbPlanesFinanciamiento.ItemIndex], 20));
						GenerarFinanciamiento.Parameters.ParamByName('@Importe').value :=
						  FieldByName('Importe').asFloat;
						GenerarFinanciamiento.Parameters.ParamByName('@EntregaInicial').value :=
						  FieldByName('EntregaInicial').asFloat;
						GenerarFinanciamiento.Parameters.ParamByName('@CantidadCuotas').value :=
						  trunc(neCantidadCuotas.Value);
						GenerarFinanciamiento.Parameters.ParamByName('@Interes').value :=
						  neInteres.value;
						GenerarFinanciamiento.Parameters.ParamByName('@FactorFrecuencia').value :=
						  PlanesFinanciamiento.FieldByName('FactorFrecuencia').asFloat;
						GenerarFinanciamiento.Parameters.ParamByName('@PlazoPrimerCuota').value :=
						  trunc(nePlazoPrimerCuota.value);
						GenerarFinanciamiento.Parameters.ParamByName('@ReemplazarFinanciamientos').value :=
						  cbReemplazarFinanciamientos.Checked;
						GenerarFinanciamiento.execPROC;
						Next;
					end;
				end;
				// Luego Cancelamos los comprobantes afectados por el financiamiento
				with ObtenerListaComprobantes do begin
					first;
					While not eof do begin
						PagarComprobante.Parameters.ParamByName('@TipoComprobante').value :=
						  FieldByName('TipoComprobante').asString;
						PagarComprobante.Parameters.ParamByName('@NumeroComprobante').value :=
						  FieldByName('NumeroComprobante').asFloat;
						PagarComprobante.Parameters.ParamByName('@Importe').value :=
						  FieldByName('ImporteTotal').asFloat;
						PagarComprobante.Parameters.ParamByName('@Estado').value := 'P';
						PagarComprobante.Parameters.ParamByName('@CodigoConcepto').value := 16;
						PagarComprobante.Parameters.ParamByName('@Descripcion').value := null;
						PagarComprobante.Parameters.ParamByName('@FechaHora').value := NowBase(DMConnections.BaseCAC);
						PagarComprobante.execPROC;
						AgregarPagoComprobante.Parameters.ParamByName('@NumeroTurno').Value := null;
						AgregarPagoComprobante.Parameters.ParamByName('@TipoComprobante').Value :=
						  FieldByName('TipoComprobante').asString;
						AgregarPagoComprobante.Parameters.ParamByName('@NumeroComprobante').Value :=
						  FieldByName('NumeroComprobante').asFloat;
						AgregarPagoComprobante.Parameters.ParamByName('@FormaPago').Value := 'RF';
						AgregarPagoComprobante.Parameters.ParamByName('@Monto').Value :=
						  FieldByName('ImporteTotal').asFloat;
						AgregarPagoComprobante.Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
						AgregarPagoComprobante.ExecProc;
						next;
					end;
				end;
				DMConnections.BaseCAC.CommitTrans;
			except
				On E: Exception do begin
					DMConnections.BaseCAC.RollbackTrans;
					MsgBoxErr(MSG_ERROR_APLICAR_FINANCIAMIENTO, e.message, CAPTION_APLICAR_FINANCIAMIENTO, MB_ICONSTOP);
				end;
			end;
		finally
			ClientDataSet.EnableControls;
			ObtenerListaComprobantes.EnableControls;
			screen.Cursor := crDefault;
		end;
	end else
		btnCancelar.Click;
end;

procedure TFrmFinanciamientoDeuda.cbPlanesFinanciamientoChange(Sender: TObject);
begin
	with PlanesFinanciamiento do begin
		Close;
		Parameters.ParamByName('CodigoFinanciamiento').Value :=
		  IVal(StrRight((Sender as TComboBox).Items[(Sender as TComboBox).ItemIndex], 20));
		Open;
		neInteres.value := FieldByName('Interes').asFloat;
		neEntregaInicial.value := FieldByName('PieMinimo').asFloat;
		neCantidadCuotas.value := FieldByName('CantidadCuotas').asInteger;
		nePlazoPrimerCuota.value := FieldByName('PlazoPrimerCuota').asInteger;
	end;
end;

procedure TFrmFinanciamientoDeuda.btnCalcularClick(Sender: TObject);
resourcestring
    CAPTION_VALIDAR_FINANCIAMIENTO  = 'Validar los Datos del Financiamiento';
    MSG_TASA_INTERES    = 'La tasa de inter�s aplicada debe ser mayor o igual a la especificada por el plan de financiamiento.';
    MSG_ENTREGA_INICIAL = 'La entrega inicial debe ser mayor o igual al valor especificado por el plan de financiamiento ' +
	  'y menor o igual a la deuda a financiar.';
    MSG_CANTIDAD_CUOTAS = 'La cantidad de cuotas debe ser menor o igual al valor especificado ' +
	  'por el plan de financiamiento y mayor que cero.';
    MSG_PLAZO_PRIMER_CUOTA = 'El plazo para facturar la primer cuota debe menor o igual al especificado ' +
	  'por el plan de financiamiento.';

var bm: TbookMark;
	ImporteInteres, ImporteCuota, EntregaInicial: Double;
begin
	if FDeudaTotal > 0 then begin

		if not ValidateControls([neInteres,neEntregaInicial,neCantidadCuotas,nePlazoPrimerCuota],
		  [neInteres.Value >= PlanesFinanciamiento.FieldByName('Interes').asFloat,
		  (neEntregaInicial.Value >= PlanesFinanciamiento.FieldByName('PieMinimo').asFloat) and
		  (neEntregaInicial.Value <= FDeudaTotal),
		  (trunc(neCantidadCuotas.Value) <= PlanesFinanciamiento.FieldByName('CantidadCuotas').asInteger) and
		  (trunc(neCantidadCuotas.Value) > 0),
		  (trunc(nePlazoPrimerCuota.Value) <= PlanesFinanciamiento.FieldByName('PlazoPrimerCuota').asInteger)],
		  CAPTION_VALIDAR_FINANCIAMIENTO,
		  [MSG_TASA_INTERES, MSG_ENTREGA_INICIAL, MSG_CANTIDAD_CUOTAS, MSG_PLAZO_PRIMER_CUOTA]) then exit;

		ImporteInteres := 0.00;
		ImporteCuota := 0.00;
		with ClientDataSet do begin
			screen.Cursor := crHourGlass;
			try
				EntregaInicial :=  neEntregaInicial.value * (FieldByName('Importe').asFloat / FDeudaTotal);
				disableControls;
				bm := getBookMark;
				first;
				While not eof do begin
					// Calculamos la cuota del financiamiento
					CalcularCuotaFinanciamiento.Parameters.ParamByName('@CantidadCuotas').value :=
					  trunc(neCantidadCuotas.value);
					CalcularCuotaFinanciamiento.Parameters.ParamByName('@FrecuenciaCuotas').value :=
					  PlanesFinanciamiento.FieldByName('FactorFrecuencia').asFloat;
					CalcularCuotaFinanciamiento.Parameters.ParamByName('@ImporteAFinanciar').value :=
					  FieldByName('Importe').asFloat - EntregaInicial;
					CalcularCuotaFinanciamiento.Parameters.ParamByName('@InteresAnual').value :=
					  neInteres.value;
					CalcularCuotaFinanciamiento.execProc;
					// Regitramos el calculo en la tabla temporal
					edit;
					FieldByName('ImporteCuota').asFloat :=
					  CalcularCuotaFinanciamiento.Parameters.ParamByName('@ImporteCuota').value;
					FieldByName('EntregaInicial').asFloat := EntregaInicial;
					FieldByName('DeudaConvenida').asFloat :=
					  FieldByName('Importe').asFloat - FieldByName('EntregaInicial').AsFloat;
					FieldByName('Interes').asFloat :=
					  CalcularCuotaFinanciamiento.Parameters.ParamByName('@ImportePorInteres').value;
					next;
					ImporteInteres := ImporteInteres + CalcularCuotaFinanciamiento.Parameters.ParamByName('@ImportePorInteres').value;
					ImporteCuota := ImporteCuota + CalcularCuotaFinanciamiento.Parameters.ParamByName('@ImporteCuota').value;
				end;
				gotoBookMark(bm);
				freeBookMark(bm);
			finally
				enableControls;
				screen.Cursor := crDefault;
			end;
		end;
		lImporteInteres.caption := formatFloat(FORMATO_IMPORTE, ImporteInteres);
		lImporteCuota.caption := formatFloat(FORMATO_IMPORTE, ImporteCuota);
		btnAceptar.Enabled := True;
	end;
end;

procedure TFrmFinanciamientoDeuda.neInteresChange(Sender: TObject);
begin
	btnAceptar.Enabled := False;
end;

procedure TFrmFinanciamientoDeuda.cbReemplazarFinanciamientosClick(Sender: TObject);
begin
    CargarDeuda;
    btnAceptar.Enabled := False;
end;


end.
