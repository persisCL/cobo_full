object TipificacionMasivaForm: TTipificacionMasivaForm
  Left = 0
  Top = 0
  Caption = 'TipificacionMasivaForm'
  ClientHeight = 324
  ClientWidth = 636
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Arial'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 14
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 636
    Height = 129
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 17
      Width = 374
      Height = 16
      Caption = 'Ingrese Proceso de Facturaci'#243'n al cual Tipificar y Numerar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 146
      Height = 14
      Caption = 'N'#250'mero Proceso Facturaci'#243'n :'
    end
    object lblTotal: TLabel
      Left = 392
      Top = 96
      Width = 33
      Height = 14
      Caption = 'lblTotal'
    end
    object btnBuscar: TSpeedButton
      Left = 327
      Top = 52
      Width = 23
      Height = 22
      Hint = 'Buscar Procesos de facturaci'#243'n con comprobantes sin Tipificar'
      Caption = '. . .'
      ParentShowHint = False
      ShowHint = True
      OnClick = btnBuscarClick
    end
    object btnComenzar: TButton
      Left = 359
      Top = 51
      Width = 75
      Height = 25
      Caption = 'Comenzar'
      TabOrder = 0
      OnClick = btnComenzarClick
    end
    object btnSalir: TButton
      Left = 552
      Top = 20
      Width = 75
      Height = 25
      Caption = 'Salir'
      TabOrder = 1
      OnClick = btnSalirClick
    end
    object pgbNumerados: TProgressBar
      Left = 16
      Top = 96
      Width = 354
      Height = 17
      TabOrder = 2
    end
    object btnDetener: TButton
      Left = 440
      Top = 51
      Width = 75
      Height = 25
      Caption = 'Detener'
      TabOrder = 3
      OnClick = btnDetenerClick
    end
    object cbxNumeroProcesoFacturacion: TComboBox
      Left = 168
      Top = 52
      Width = 153
      Height = 22
      ItemHeight = 14
      TabOrder = 4
      Text = 'cbxNumeroProcesoFacturacion'
      OnKeyPress = cbxNumeroProcesoFacturacionKeyPress
    end
  end
  object mmoLog: TMemo
    Left = 0
    Top = 129
    Width = 636
    Height = 195
    Align = alClient
    Lines.Strings = (
      'mmoLog')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object tmerProgreso: TTimer
    Enabled = False
    OnTimer = tmerProgresoTimer
    Left = 432
    Top = 8
  end
  object spCrearLeerBorrarLogTYNM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearLeerBorrarLogTYNM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Total'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@Progreso'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@Exitosos'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 504
    Top = 56
  end
  object spObtenerProcesosFacturacionSinTerminarTipificacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerProcesosFacturacionSinTerminarTipificacion'
    Parameters = <>
    Left = 536
    Top = 56
  end
  object tmerForm: TTimer
    Enabled = False
    OnTimer = tmerFormTimer
    Left = 472
    Top = 8
  end
end
