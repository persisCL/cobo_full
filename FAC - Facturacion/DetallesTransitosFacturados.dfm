object frmDetalleTransitosFacturados: TfrmDetalleTransitosFacturados
  Left = 80
  Top = 131
  BorderStyle = bsSingle
  Caption = 'Detalle de Transitos Facturados'
  ClientHeight = 553
  ClientWidth = 789
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    789
    553)
  PixelsPerInch = 96
  TextHeight = 13
  object btnSalir: TDPSButton
    Left = 708
    Top = 525
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 0
    OnClick = btnSalirClick
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 0
    Width = 781
    Height = 60
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos del Cliente'
    TabOrder = 1
    DesignSize = (
      781
      60)
    object LNumeroDocumento: TLabel
      Left = 98
      Top = 18
      Width = 71
      Height = 13
      AutoSize = False
      Caption = 'LNumeroDocumento'
    end
    object Label6: TLabel
      Left = 181
      Top = 18
      Width = 48
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lApellidoNombre: TLabel
      Left = 230
      Top = 18
      Width = 543
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = 'Apellido Cliente'
    end
    object LDomicilio: TLabel
      Left = 82
      Top = 39
      Width = 683
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = 'DomicilioCliente'
    end
    object Label8: TLabel
      Left = 16
      Top = 18
      Width = 78
      Height = 13
      Caption = 'N'#250'mero RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 16
      Top = 39
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 5
    Top = 64
    Width = 780
    Height = 41
    Caption = 'Comprobante N'#250'mero: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object dbtNroComprobante: TDBText
      Left = 137
      Top = 0
      Width = 95
      Height = 13
      AutoSize = True
      DataField = 'DescriComprobanteNumero'
      DataSource = NavWindowComprobantesEmitidos.dsFacturas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbtFechaEmision: TDBText
      Left = 114
      Top = 18
      Width = 121
      Height = 17
      DataField = 'DescriFechaHora'
      DataSource = NavWindowComprobantesEmitidos.dsFacturas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbtVencimiento: TDBText
      Left = 387
      Top = 18
      Width = 121
      Height = 17
      DataField = 'DescriVencimiento'
      DataSource = NavWindowComprobantesEmitidos.dsFacturas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbtImporte: TDBText
      Left = 656
      Top = 18
      Width = 113
      Height = 17
      DataField = 'DescriImporte'
      DataSource = NavWindowComprobantesEmitidos.dsFacturas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 18
      Width = 105
      Height = 13
      Caption = 'Fecha de Emisi'#243'n:'
    end
    object Label4: TLabel
      Left = 256
      Top = 18
      Width = 131
      Height = 13
      Caption = 'Fecha de Vencimiento:'
    end
    object Label5: TLabel
      Left = 600
      Top = 18
      Width = 47
      Height = 13
      Caption = 'Importe:'
    end
  end
  object DBListEx1: TDBListEx
    Left = 6
    Top = 152
    Width = 199
    Height = 369
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Tr'#225'nsito'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumCorrCA'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Pto.Cobro'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroPuntoCobro'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Importe'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescriImporte'
      end>
    DataSource = dsTransitosPorItem
    DragReorder = True
    ParentColor = False
    TabOrder = 3
    TabStop = True
  end
  object DPSPageControl1: TDPSPageControl
    Left = 208
    Top = 152
    Width = 361
    Height = 369
    ActivePage = tsTRX_Detalle
    OwnerDraw = True
    TabIndex = 0
    TabOrder = 4
    PageColor = clBtnFace
    Color = clBtnFace
    object tsTRX_Detalle: TTabSheet
      Caption = 'Detalle del Tr'#225'nsito'
      object Label13: TLabel
        Left = 8
        Top = 16
        Width = 65
        Height = 13
        Caption = 'Fecha  Hora :'
      end
      object Label14: TLabel
        Left = 8
        Top = 117
        Width = 44
        Height = 13
        Caption = 'Importe : '
      end
      object Label15: TLabel
        Left = 8
        Top = 40
        Width = 80
        Height = 13
        Caption = 'Punto de Cobro: '
      end
      object Label16: TLabel
        Left = 8
        Top = 214
        Width = 46
        Height = 13
        Caption = 'Patente : '
      end
      object Label18: TLabel
        Left = 8
        Top = 165
        Width = 54
        Height = 13
        Caption = 'Convenio : '
      end
      object Label20: TLabel
        Left = 8
        Top = 253
        Width = 54
        Height = 13
        Caption = 'Categoria : '
      end
      object Label21: TLabel
        Left = 8
        Top = 312
        Width = 59
        Height = 13
        Caption = 'Anomal'#237'as : '
      end
      object Label22: TLabel
        Left = 256
        Top = 312
        Width = 55
        Height = 13
        Caption = 'Im'#225'genes : '
      end
      object dbtFechaHoraTRX: TDBText
        Left = 96
        Top = 16
        Width = 249
        Height = 17
        DataField = 'FechaHora'
        DataSource = dsTransitosPorItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtPuntoCobroNumero: TDBText
        Left = 96
        Top = 40
        Width = 25
        Height = 17
        Alignment = taRightJustify
        DataField = 'NumeroPuntoCobro'
        DataSource = dsTransitosPorItem
      end
      object dbtPuntoCobroDescripcion: TDBText
        Left = 155
        Top = 40
        Width = 194
        Height = 17
        DataField = 'DescPtoCobro'
        DataSource = dsTransitosPorItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtPuntoCobroSentido: TDBText
        Left = 124
        Top = 40
        Width = 25
        Height = 17
        DataField = 'Sentido'
        DataSource = dsTransitosPorItem
      end
      object dbtImporteTRX: TDBText
        Left = 96
        Top = 117
        Width = 249
        Height = 17
        DataField = 'DescriImporte'
        DataSource = dsTransitosPorItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtConvenio: TDBText
        Left = 96
        Top = 165
        Width = 249
        Height = 17
        DataField = 'NumeroConvenioFormateado'
        DataSource = dsTransitosPorItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtPatente: TDBText
        Left = 96
        Top = 214
        Width = 249
        Height = 17
        DataField = 'Patente'
        DataSource = dsTransitosPorItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtCategoria: TDBText
        Left = 96
        Top = 253
        Width = 25
        Height = 17
        DataField = 'Categoria'
        DataSource = dsTransitosPorItem
      end
      object lbAnomalias: TLabel
        Left = 72
        Top = 312
        Width = 16
        Height = 13
        Caption = 'NO'
      end
      object lbImagenes: TLabel
        Left = 312
        Top = 312
        Width = 16
        Height = 13
        Caption = 'NO'
      end
      object dbtDescripcionCategoria: TDBText
        Left = 128
        Top = 253
        Width = 217
        Height = 17
        DataField = 'DescCategoria'
        DataSource = dsTransitosPorItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object tsTRX_Anomalias: TTabSheet
      Caption = 'Anomalias'
      ImageIndex = 1
      TabVisible = False
      object Bevel1: TBevel
        Left = 2
        Top = 8
        Width = 350
        Height = 128
      end
      object Label30: TLabel
        Left = 0
        Top = 312
        Width = 68
        Height = 13
        Caption = 'Validado por:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsItalic]
        ParentFont = False
        Visible = False
      end
      object Label31: TLabel
        Left = 8
        Top = 328
        Width = 36
        Height = 13
        Caption = 'el d'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsItalic]
        ParentFont = False
        Transparent = True
        Visible = False
      end
      object Label23: TLabel
        Left = 264
        Top = 328
        Width = 86
        Height = 13
        Caption = 'REVALIDADO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Visible = False
      end
      object dbchkDiscCategoria: TDBCheckBox
        Left = 8
        Top = 15
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Discrepancia de Categor'#237'a'
        DataField = 'DiscrepanciaCategoriaVehiculo'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkDiscPatente: TDBCheckBox
        Left = 8
        Top = 39
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Discrepancia de Patente'
        DataField = 'DiscrepanciaPatenteTag'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 1
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkTAGInhabilitadoRobo: TDBCheckBox
        Left = 184
        Top = 87
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'TAG Inhabilitado por Robo'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 2
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkTAGInhabilitadoMora: TDBCheckBox
        Left = 184
        Top = 111
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'TAG Inhabilitado por Mora'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 3
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkInfraccion: TDBCheckBox
        Left = 8
        Top = 63
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Infracci'#243'n'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 4
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkTrxInfractor: TDBCheckBox
        Left = 8
        Top = 87
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Tr'#225'nsito Infractor'
        DataField = 'Infractor'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 5
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object DBCheckBox7: TDBCheckBox
        Left = 8
        Top = 111
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Tr'#225'nsito elegido por Azar'
        DataField = 'ElegidoPorAzar'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 6
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkTAGilegal: TDBCheckBox
        Left = 184
        Top = 63
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'TAG ilegal'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 7
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkTAGPocoProbable: TDBCheckBox
        Left = 184
        Top = 39
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'TAG Poco Probable'
        DataField = 'TagPocoProbable'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 8
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dbchkTAGNoDetectado: TDBCheckBox
        Left = 184
        Top = 15
        Width = 161
        Height = 17
        Alignment = taLeftJustify
        Caption = 'TAG NO Detectado'
        DataField = 'TagInexistente'
        DataSource = dsAnomalias
        ReadOnly = True
        TabOrder = 9
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 144
        Width = 353
        Height = 65
        Caption = 'Patente'
        TabOrder = 10
        object dbtPatenteDetectada: TDBText
          Left = 76
          Top = 20
          Width = 65
          Height = 17
          DataField = 'PatenteDetectada'
          DataSource = dsAnomalias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbtPatenteValidada: TDBText
          Left = 279
          Top = 20
          Width = 65
          Height = 17
          DataField = 'PatenteValidada'
          DataSource = dsAnomalias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 8
          Top = 20
          Width = 53
          Height = 13
          Caption = 'Detectada:'
        end
        object Label26: TLabel
          Left = 223
          Top = 20
          Width = 47
          Height = 13
          Caption = 'Validada: '
        end
        object Label27: TLabel
          Left = 8
          Top = 40
          Width = 66
          Height = 13
          Caption = 'Confiabilidad: '
        end
        object dbtConfiabilidadPatenteDetectada: TDBText
          Left = 80
          Top = 40
          Width = 65
          Height = 17
          DataField = 'ConfiabilidadPatenteDetectada'
          DataSource = dsAnomalias
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 216
        Width = 353
        Height = 89
        Caption = 'Categoria'
        TabOrder = 11
        object Label28: TLabel
          Left = 8
          Top = 20
          Width = 53
          Height = 13
          Caption = 'Detectada:'
        end
        object Label29: TLabel
          Left = 8
          Top = 52
          Width = 47
          Height = 13
          Caption = 'Validada: '
        end
        object dbtCategoriaDetectada: TDBText
          Left = 76
          Top = 20
          Width = 65
          Height = 17
          DataField = 'CategoriaDetectada'
          DataSource = dsAnomalias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbtCategoriaValidada: TDBText
          Left = 79
          Top = 52
          Width = 65
          Height = 17
          DataField = 'CategoriaValidada'
          DataSource = dsAnomalias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbtDescriCategoriaDetectada: TDBText
          Left = 8
          Top = 35
          Width = 337
          Height = 17
          DataSource = dsAnomalias
          Visible = False
        end
        object dbtDescriCategoriaValidada: TDBText
          Left = 8
          Top = 67
          Width = 337
          Height = 17
          DataSource = dsAnomalias
          Visible = False
        end
      end
    end
    object tsTRX_Imagenes: TTabSheet
      Caption = 'Im'#225'genes'
      ImageIndex = 2
      TabVisible = False
      object SpeedButton1: TSpeedButton
        Left = 0
        Top = 316
        Width = 97
        Height = 25
        Caption = '< Im'#225'gen Anterior'
        Flat = True
      end
      object SpeedButton2: TSpeedButton
        Left = 256
        Top = 316
        Width = 97
        Height = 25
        Caption = 'Im'#225'gen Siguiente >'
        Flat = True
      end
      object Label12: TLabel
        Left = 104
        Top = 320
        Width = 145
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Im'#225'gen X de Y'
      end
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 353
        Height = 341
        Align = alClient
      end
    end
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 528
    Width = 193
    Height = 17
    Caption = 'Mostrar Im'#225'genes del Tr'#225'nsito'
    TabOrder = 5
    OnClick = CheckBox1Click
  end
  object Panel1: TPanel
    Left = 571
    Top = 152
    Width = 215
    Height = 369
    BevelInner = bvLowered
    BevelOuter = bvNone
    Caption = 'Datos del Cliente'
    Color = 14732467
    TabOrder = 6
    object Label1: TLabel
      Left = 177
      Top = 152
      Width = 32
      Height = 13
      Caption = 'Label1'
      Color = 14732467
      ParentColor = False
    end
    object Label2: TLabel
      Left = 5
      Top = 5
      Width = 80
      Height = 13
      Caption = 'Datos del Cliente'
      Transparent = True
    end
    object DPSPageControl2: TDPSPageControl
      Left = 4
      Top = 20
      Width = 206
      Height = 346
      ActivePage = tsDatosClientes_TAG
      MultiLine = True
      OwnerDraw = True
      RaggedRight = True
      TabIndex = 0
      TabOrder = 0
      PageColor = 14732467
      Color = 14732467
      object tsDatosClientes_TAG: TTabSheet
        Caption = 'Veh'#237'culo / TAG'
        object GroupBox6: TGroupBox
          Left = 0
          Top = 0
          Width = 197
          Height = 185
          Caption = 'Veh'#237'culo'
          TabOrder = 0
          object Label17: TLabel
            Left = 8
            Top = 40
            Width = 36
            Height = 13
            Caption = 'Marca :'
          end
          object Label19: TLabel
            Left = 8
            Top = 56
            Width = 41
            Height = 13
            Caption = 'Modelo :'
          end
          object Label24: TLabel
            Left = 8
            Top = 80
            Width = 25
            Height = 13
            Caption = 'A'#241'o :'
          end
          object Label32: TLabel
            Left = 8
            Top = 104
            Width = 27
            Height = 13
            Caption = 'Tipo :'
          end
          object Label33: TLabel
            Left = 8
            Top = 128
            Width = 51
            Height = 13
            Caption = 'Categoria :'
          end
          object Label34: TLabel
            Left = 8
            Top = 160
            Width = 54
            Height = 13
            Caption = 'Categoria : '
            Visible = False
          end
          object Label35: TLabel
            Left = 8
            Top = 144
            Width = 75
            Height = 13
            Caption = 'Tiene Acoplado'
            Visible = False
          end
          object Label36: TLabel
            Left = 80
            Top = 80
            Width = 33
            Height = 13
            Caption = 'Color : '
          end
          object DBText1: TDBText
            Left = 64
            Top = 40
            Width = 113
            Height = 17
            DataField = 'DescriMarca'
            DataSource = dsCuentas
          end
          object DBText2: TDBText
            Left = 64
            Top = 56
            Width = 129
            Height = 17
            DataField = 'Modelo'
            DataSource = dsCuentas
          end
          object DBText3: TDBText
            Left = 40
            Top = 80
            Width = 33
            Height = 17
            DataField = 'AnioVehiculo'
            DataSource = dsCuentas
          end
          object DBText4: TDBText
            Left = 112
            Top = 80
            Width = 73
            Height = 17
            DataField = 'DescriColor'
            DataSource = dsCuentas
          end
          object DBText5: TDBText
            Left = 48
            Top = 104
            Width = 145
            Height = 17
            DataField = 'DescriTipoVehiculo'
            DataSource = dsCuentas
          end
          object DBText6: TDBText
            Left = 64
            Top = 128
            Width = 121
            Height = 17
            DataField = 'DescriCategoriaVehiculo'
            DataSource = dsCuentas
          end
          object DBText7: TDBText
            Left = 64
            Top = 160
            Width = 121
            Height = 17
            DataField = 'DescriCategoriaAcoplado'
            DataSource = dsCuentas
          end
          object DBText13: TDBText
            Left = 2
            Top = 15
            Width = 193
            Height = 25
            Align = alTop
            Alignment = taCenter
            DataField = 'Patente'
            DataSource = dsCuentas
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
          end
        end
        object GroupBox7: TGroupBox
          Left = 1
          Top = 186
          Width = 195
          Height = 131
          Caption = 'TAG'
          TabOrder = 1
          object Label37: TLabel
            Left = 8
            Top = 16
            Width = 44
            Height = 13
            Caption = 'Modelo : '
          end
          object Label38: TLabel
            Left = 8
            Top = 32
            Width = 81
            Height = 13
            Caption = 'Fin de Garantia : '
          end
          object Label39: TLabel
            Left = 8
            Top = 56
            Width = 38
            Height = 13
            Caption = 'Estados'
          end
          object Label40: TLabel
            Left = 8
            Top = 72
            Width = 84
            Height = 13
            Caption = 'Funcionamiento : '
          end
          object Label41: TLabel
            Left = 8
            Top = 92
            Width = 53
            Height = 13
            Caption = 'Situaci'#243'n : '
          end
          object Label42: TLabel
            Left = 8
            Top = 112
            Width = 74
            Height = 13
            Caption = 'Conservaci'#243'n : '
          end
          object DBText8: TDBText
            Left = 88
            Top = 32
            Width = 105
            Height = 17
            DataField = 'FechaFinGarantiaMOPTT'
            DataSource = dsCuentas
          end
          object DBText9: TDBText
            Left = 56
            Top = 16
            Width = 129
            Height = 17
            DataField = 'ModeloTAG'
            DataSource = dsCuentas
          end
          object DBText10: TDBText
            Left = 92
            Top = 73
            Width = 95
            Height = 17
            DataField = 'EdoFuncionamiento'
            DataSource = dsCuentas
          end
          object DBText11: TDBText
            Left = 92
            Top = 94
            Width = 95
            Height = 17
            DataField = 'EdoSituacion'
            DataSource = dsCuentas
          end
          object DBText12: TDBText
            Left = 92
            Top = 113
            Width = 95
            Height = 17
            DataField = 'EdoConservacion'
            DataSource = dsCuentas
            Transparent = True
          end
        end
      end
      object tsDatosClientes_Cuenta: TTabSheet
        Caption = 'Cuenta'
        ImageIndex = 2
        object Label43: TLabel
          Left = 8
          Top = 16
          Width = 72
          Height = 13
          Caption = 'Fecha de Alta :'
        end
        object Label44: TLabel
          Left = 8
          Top = 64
          Width = 78
          Height = 13
          Caption = 'Fecha de Baja : '
        end
        object Label46: TLabel
          Left = 0
          Top = 298
          Width = 175
          Height = 20
          Align = alBottom
          Alignment = taCenter
          Caption = 'CUENTA SUSPENDIDA'
          Color = clRed
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInfoBk
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Visible = False
        end
        object DBText14: TDBText
          Left = 8
          Top = 40
          Width = 185
          Height = 17
          DataField = 'FechaAltaCuenta'
          DataSource = dsCuentas
        end
        object DBText15: TDBText
          Left = 8
          Top = 88
          Width = 185
          Height = 17
          DataField = 'FechaBajaCuenta'
          DataSource = dsCuentas
        end
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 6
    Top = 105
    Width = 779
    Height = 40
    Caption = 'Tr'#225'nsitos Facturados'
    TabOrder = 7
    object Label7: TLabel
      Left = 8
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Fecha: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 256
      Top = 16
      Width = 51
      Height = 13
      Caption = 'Importe: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 519
      Top = 16
      Width = 133
      Height = 13
      Caption = 'Cantidad de Tr'#225'nsitos: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dbtFechaTrxFacturados: TDBText
      Left = 56
      Top = 16
      Width = 185
      Height = 17
      DataField = 'Fecha'
      DataSource = NavWindowComprobantesEmitidos.dsFacturacionDetallada
    end
    object dbtImporteTRXFacturados: TDBText
      Left = 312
      Top = 16
      Width = 161
      Height = 17
      DataField = 'DescrImporte'
      DataSource = NavWindowComprobantesEmitidos.dsFacturacionDetallada
    end
    object dbtCantidadTRXFacturados: TDBText
      Left = 656
      Top = 16
      Width = 113
      Height = 17
      DataField = 'CantidadTransitos'
      DataSource = NavWindowComprobantesEmitidos.dsFacturacionDetallada
    end
  end
  object dsTransitosPorItem: TDataSource
    DataSet = spTransitosPorItem
    OnDataChange = dsTransitosPorItemDataChange
    Left = 247
    Top = 524
  end
  object dsAnomalias: TDataSource
    DataSet = tblAnomalias
    Left = 319
    Top = 524
  end
  object tblAnomalias: TADOTable
    Active = True
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'NumCorrCA'
    MasterFields = 'NumCorrCA'
    MasterSource = dsTransitosPorItem
    TableName = 'Anomalias'
    Left = 288
    Top = 524
  end
  object spTransitosPorItem: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTransitosPorItem;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@IDItemPeajeCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 214
    Top = 523
    object spTransitosPorItemNumCorrCA: TLargeintField
      FieldName = 'NumCorrCA'
    end
    object spTransitosPorItemNumeroPuntoCobro: TWordField
      FieldName = 'NumeroPuntoCobro'
    end
    object spTransitosPorItemFechaHora: TDateTimeField
      FieldName = 'FechaHora'
    end
    object spTransitosPorItemIDItemPeajeCuenta: TIntegerField
      FieldName = 'IDItemPeajeCuenta'
    end
    object spTransitosPorItemIDDetalleTransito: TIntegerField
      FieldName = 'IDDetalleTransito'
    end
    object spTransitosPorItemImporte: TBCDField
      FieldName = 'Importe'
      ReadOnly = True
      Precision = 24
      Size = 6
    end
    object spTransitosPorItemDescriImporte: TStringField
      FieldName = 'DescriImporte'
      ReadOnly = True
    end
    object spTransitosPorItemDescPtoCobro: TStringField
      FieldName = 'DescPtoCobro'
      FixedChar = True
      Size = 60
    end
    object spTransitosPorItemSentido: TStringField
      FieldName = 'Sentido'
      FixedChar = True
      Size = 1
    end
    object spTransitosPorItemCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object spTransitosPorItemNumeroConvenioFormateado: TStringField
      FieldName = 'NumeroConvenioFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object spTransitosPorItemIndiceVehiculo: TIntegerField
      FieldName = 'IndiceVehiculo'
    end
    object spTransitosPorItemPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object spTransitosPorItemCategoria: TWordField
      FieldName = 'Categoria'
    end
    object spTransitosPorItemDescCategoria: TStringField
      FieldName = 'DescCategoria'
      FixedChar = True
      Size = 60
    end
  end
  object dsCuentas: TDataSource
    DataSet = spCuentas
    OnDataChange = dsCuentasDataChange
    Left = 400
    Top = 523
  end
  object spCuentas: TADOStoredProc
    Active = True
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerVehiculosTAGS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 370
    Top = 522
    object spCuentasCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object spCuentasIndiceVehiculo: TIntegerField
      FieldName = 'IndiceVehiculo'
    end
    object spCuentasFechaAltaCuenta: TDateTimeField
      FieldName = 'FechaAltaCuenta'
    end
    object spCuentasFechaBajaCuenta: TDateTimeField
      FieldName = 'FechaBajaCuenta'
    end
    object spCuentasCodigoEstadoCuenta: TWordField
      FieldName = 'CodigoEstadoCuenta'
    end
    object spCuentasFechaAltaTAG: TDateTimeField
      FieldName = 'FechaAltaTAG'
    end
    object spCuentasFechaBajaTAG: TDateTimeField
      FieldName = 'FechaBajaTAG'
    end
    object spCuentasPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object spCuentasDescriMarca: TStringField
      FieldName = 'DescriMarca'
      Size = 30
    end
    object spCuentasModelo: TStringField
      FieldName = 'Modelo'
      Size = 50
    end
    object spCuentasAnioVehiculo: TSmallintField
      FieldName = 'AnioVehiculo'
    end
    object spCuentasDescriColor: TStringField
      FieldName = 'DescriColor'
      Size = 30
    end
    object spCuentasDescriTipoVehiculo: TStringField
      FieldName = 'DescriTipoVehiculo'
      Size = 40
    end
    object spCuentasTieneAcoplado: TStringField
      FieldName = 'TieneAcoplado'
      ReadOnly = True
      Size = 2
    end
    object spCuentasDescriCategoriaVehiculo: TStringField
      FieldName = 'DescriCategoriaVehiculo'
      FixedChar = True
      Size = 60
    end
    object spCuentasDescriCategoriaAcoplado: TStringField
      FieldName = 'DescriCategoriaAcoplado'
      FixedChar = True
      Size = 60
    end
    object spCuentasFechaFinGarantiaMOPTT: TDateTimeField
      FieldName = 'FechaFinGarantiaMOPTT'
    end
    object spCuentasModeloTAG: TStringField
      FieldName = 'ModeloTAG'
      Size = 50
    end
    object spCuentasEdoSituacion: TStringField
      FieldName = 'EdoSituacion'
      FixedChar = True
      Size = 50
    end
    object spCuentasEdoConservacion: TStringField
      FieldName = 'EdoConservacion'
      FixedChar = True
      Size = 50
    end
    object spCuentasEdoFuncionamiento: TStringField
      FieldName = 'EdoFuncionamiento'
      FixedChar = True
      Size = 50
    end
  end
end
