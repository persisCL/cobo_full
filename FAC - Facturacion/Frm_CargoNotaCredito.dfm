object frmCargoNotaCredito: TfrmCargoNotaCredito
  Left = 346
  Top = 189
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Ingresar / Modificar Concepto '
  ClientHeight = 229
  ClientWidth = 591
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    591
    229)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 577
    Height = 153
  end
  object lblConcepto: TLabel
    Left = 89
    Top = 52
    Width = 46
    Height = 13
    Caption = 'Concepto'
  end
  object lblObservaciones: TLabel
    Left = 64
    Top = 78
    Width = 71
    Height = 13
    Caption = 'Observaciones'
  end
  object lblImporte: TLabel
    Left = 89
    Top = 105
    Width = 50
    Height = 13
    Caption = 'Valor Neto'
  end
  object lblImporteIVA: TLabel
    Left = 249
    Top = 105
    Width = 17
    Height = 13
    Caption = 'IVA'
  end
  object lblPorcentajeIVA: TLabel
    Left = 96
    Top = 135
    Width = 71
    Height = 13
    Caption = 'Porcentaje IVA'
  end
  object lblTipoComprobante: TLabel
    Left = 33
    Top = 24
    Width = 102
    Height = 13
    Caption = 'Tipo de Comprobante'
  end
  object lblFecha: TLabel
    Left = 402
    Top = 24
    Width = 30
    Height = 13
    Caption = 'Fecha'
  end
  object lblTotal: TLabel
    Left = 382
    Top = 105
    Width = 24
    Height = 13
    Caption = 'Total'
  end
  object lblSignoNeto: TLabel
    Left = 155
    Top = 99
    Width = 8
    Height = 24
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblSignoIVA: TLabel
    Left = 285
    Top = 99
    Width = 8
    Height = 24
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblSignoTotal: TLabel
    Left = 425
    Top = 99
    Width = 8
    Height = 24
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cbConcepto: TVariantComboBox
    Left = 145
    Top = 48
    Width = 297
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 2
    OnChange = cbConceptChange
    Items = <>
  end
  object btnCancelar: TButton
    Left = 501
    Top = 196
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 10
    OnClick = btnCancelarClick
  end
  object btnAceptar: TButton
    Left = 414
    Top = 196
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 8
    OnClick = btnAceptarClick
  end
  object edImporte: TNumericEdit
    Left = 166
    Top = 101
    Width = 72
    Height = 21
    MaxLength = 11
    TabOrder = 5
    OnChange = edImporteChange
    OnKeyPress = edImporteKeyPress
  end
  object edDetalle: TEdit
    Left = 145
    Top = 74
    Width = 297
    Height = 21
    TabOrder = 4
  end
  object edImporteIVA: TNumericEdit
    Left = 294
    Top = 101
    Width = 82
    Height = 21
    Enabled = False
    MaxLength = 11
    TabOrder = 6
  end
  object edPorcentajeIVA: TNumericEdit
    Left = 177
    Top = 131
    Width = 41
    Height = 21
    Enabled = False
    MaxLength = 7
    TabOrder = 9
  end
  object cbTipoComprobante: TVariantComboBox
    Left = 145
    Top = 20
    Width = 200
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cbTipoComprobanteChange
    Items = <>
  end
  object edFecha: TDateEdit
    Left = 445
    Top = 20
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 1
    Date = -693594.000000000000000000
  end
  object btnABMConceptos: TButton
    Left = 454
    Top = 47
    Width = 82
    Height = 22
    Caption = '&Consultar'
    TabOrder = 3
    OnClick = btnABMConceptosClick
  end
  object edTotal: TNumericEdit
    Left = 434
    Top = 101
    Width = 87
    Height = 21
    Enabled = False
    MaxLength = 10
    TabOrder = 7
  end
  object spObtenerConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConceptosComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 386
    Top = 126
  end
end
