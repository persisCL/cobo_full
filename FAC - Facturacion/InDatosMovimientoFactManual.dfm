object FrmAgregarMovFactManual: TFrmAgregarMovFactManual
  Left = 266
  Top = 260
  BorderStyle = bsDialog
  Caption = 'Agregar Movimiento a Facturar'
  ClientHeight = 165
  ClientWidth = 459
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 459
    Height = 128
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 10
      Top = 40
      Width = 59
      Height = 13
      Caption = 'Concep&to:'
      FocusControl = cbConceptos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 100
      Width = 47
      Height = 13
      Caption = '&Importe:'
      FocusControl = neImporte
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 10
      Top = 70
      Width = 89
      Height = 13
      Caption = '&Observaciones:'
      FocusControl = eObservaciones
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 10
      Top = 12
      Width = 51
      Height = 13
      Caption = '&Cuentas:'
      FocusControl = cbCuentasCliente
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lConceptoImporte: TLabel
      Left = 232
      Top = 100
      Width = 83
      Height = 13
      Caption = 'lConceptoImporte'
    end
    object eObservaciones: TEdit
      Left = 120
      Top = 66
      Width = 328
      Height = 21
      TabOrder = 2
      OnChange = cbConceptosChange
    end
    object cbConceptos: TComboBox
      Left = 120
      Top = 36
      Width = 329
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbConceptosChange
      OnExit = cbConceptosExit
    end
    object neImporte: TNumericEdit
      Left = 120
      Top = 96
      Width = 97
      Height = 21
      Color = 16444382
      TabOrder = 3
      OnChange = cbConceptosChange
      Decimals = 0
    end
    object cbCuentasCliente: TComboBox
      Left = 120
      Top = 9
      Width = 329
      Height = 19
      Style = csOwnerDrawFixed
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object btnAceptar: TDPSButton
    Left = 296
    Top = 135
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancelar: TDPSButton
    Left = 376
    Top = 135
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
end
