unit Frm_Facturar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, CheckLst,
  ComCtrls, UtilRB, ppComm, ppRelatv, ppProd, ppClass, Db, DBTables, Util, UtilDB, PeaProcs, UtilProc,
  validate, DateEdit, ThreadFact, DmiCtrls, RBSetup, ADODB, DMConnection, variants, BuscaClientes,
  jpeg, peaTypes, reporteFactura, ReporteDetalleViajes, DPSControls;

type
  TEstadoFacturacion = (efNada, efFacturando, efImprimiendo);

  TFormFacturacion = class(TForm)
    PageControl: TPageControl;
    tab_facturar: TTabSheet;
    tab_imprimir: TTabSheet;
    tab_inicio: TTabSheet;
	Panel1: TPanel;
    btn_siguiente: TDPSButton;
    btn_cancelar: TDPSButton;
	Bevel2: TBevel;
	Label1: TLabel;
	Label2: TLabel;
    Label3: TLabel;
	rb_facturar: TRadioButton;
    rb_imprimir: TRadioButton;
    Bevel3: TBevel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Button3: TDPSButton;
	lbl_impresora: TLabel;
	Timer1: TTimer;
    btn_anterior: TDPSButton;
    tab_FacturarFin: TTabSheet;
    Bevel5: TBevel;
    Label12: TLabel;
    GroupBox2: TGroupBox;
	pb_facturar: TProgressBar;
    Label14: TLabel;
    Label15: TLabel;
    lbl_tiempofacttrans: TLabel;
    lbl_tiempofactrest: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    lbl_cuentasproc: TLabel;
    rb_soloclienteimp: TRadioButton;
    RadioButton2: TRadioButton;
    lbl_codigoclienteimp: TLabel;
    tab_ImprimirFin: TTabSheet;
    Label13: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    GroupBox3: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    lbl_tiempoimptrans: TLabel;
    lbl_tiempoimprest: TLabel;
    Label26: TLabel;
    lbl_cuentasimp: TLabel;
    pb_Imprimir: TProgressBar;
    rb_soloclientefact: TRadioButton;
    lbl_codigoclientefact: TLabel;
    rbFacturarGrupos: TRadioButton;
    ObtenerComprobantesAImprimir: TADOStoredProc;
    Label8: TLabel;
    ObtenerGruposFacturacionAFacturar: TADOStoredProc;
    ObtenerGruposConFacturasAImprimir: TADOStoredProc;
    spCrearProcesoFacturacion: TADOStoredProc;
    ActualizarProceso: TADOQuery;
    lFacturando: TLabel;
    ObtenerCantidadComprobantes: TADOQuery;
    EliminarProcesoFacturacion: TADOQuery;
    ActualizarAgendaFacturacion: TADOQuery;
    qryContarComprobantes: TADOQuery;
    ActualizarComprobanteImpreso: TADOQuery;
    ActualizarProcesoFacturacion: TADOQuery;
    Bevel1: TBevel;
    Bevel4: TBevel;
    Image: TImage;
    Bevel6: TBevel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    txt_fechalimite: TDateEdit;
    Label18: TLabel;
    peCodigoCliente: TPickEdit;
    nbGruposCliente: TNotebook;
    Label9: TLabel;
    ListaGruposFact: TCheckListBox;
    Label10: TLabel;
    lbCiclos: TListBox;
    Label24: TLabel;
    Label25: TLabel;
    nbImprimir: TNotebook;
    ListaGruposImpre: TCheckListBox;
    peCodigoClienteImp: TPickEdit;
    GroupBox4: TGroupBox;
    lNombreCompletoIMp: TLabel;
    lDocumentoIMp: TLabel;
    lDomicilioImp: TLabel;
    lComunaImp: TLabel;
    lRegionImp: TLabel;
    GroupBox5: TGroupBox;
    lNombreCompleto: TLabel;
    lDocumento: TLabel;
    lDomicilio: TLabel;
    lComuna: TLabel;
    lRegion: TLabel;
    ObtenerCuentasConDetalle: TADOQuery;
    ObtenerCliente: TADOStoredProc;
    CrearProcesoFacturacionGrupoFacturacion: TADOStoredProc;
    UpDateFechaFacturacion: TADOQuery;
    Label17: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
	procedure Timer1Timer(Sender: TObject);
    procedure btn_siguienteClick(Sender: TObject);
    procedure btn_anteriorClick(Sender: TObject);
	procedure Button3Click(Sender: TObject);
	procedure btn_cancelarClick(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure FormCreate(Sender: TObject);
	procedure rb_soloclienteimpClick(Sender: TObject);
	procedure RadioButton2Click(Sender: TObject);
    procedure rb_soloclientefactClick(Sender: TObject);
    procedure rbFacturarGruposClick(Sender: TObject);
    procedure peCodigoClienteButtonClick(Sender: TObject);
    procedure ListaGruposFactClick(Sender: TObject);
    procedure peCodigoClienteImpButtonClick(Sender: TObject);
    procedure peCodigoClienteImpChange(Sender: TObject);
    procedure peCodigoClienteChange(Sender: TObject);
  private
	{ Private declarations }
    FReporteFactura: TFormReporteFactura;
    FReporteDetalleViajes: TFrmDetalleViajes;
	FechaLimitePorCliente: TDateTime;
	FInicio: TDateTime;
	FCancelar: Boolean;
	FEstado: TEstadoFacturacion;
	FAntHecho, FClienteAnterior: Integer;
    FCodigoCliente, FCodigoClienteImp, FCuentasProcesadas, FNumeroProcesoFacturacion, FThreadsCorriendo, FTotalCuentas: Integer;
    FComprobantesProcresados, FTotalComprobantes: Integer;
    FNombre, FApellido,FApellidoMaterno, FNombreImp, FApellidoImp, FApellidoMaternoImp: AnsiString;
	procedure RevisarEstadoFacturacion;
	procedure RevisarEstadoImpresion;
	procedure Facturar;
	procedure Imprimir;
	procedure MostrarProgreso(Inicio: TDateTime; Hecho: Single;
	  lbl1, lbl2: TLabel; pb: TProgressBar);
	Function ContarCuentas: Integer;
    Function ContarComprobantes:Integer;
    Function CrearProcesoFacturacion: Integer;
	Function StringGruposFacturacion: AnsiString;
	Function ImprimirCliente(var ClienteAnterior: Integer): Boolean;
	procedure ImprimirComprobante(TipoComprobante: Char; CodigoCliente: LongInt; NumeroComprobante: Double);
	procedure ImprimirFacturacionDetallada(TipoComprobante: Char; Cuenta, CodigoCliente: Integer; NumeroComprobante: Double);
    procedure MostrarClienteImp(CodigoCLiente: Integer);
    procedure MostrarCliente(CodigoCLiente: Integer);
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FormFacturacion: TFormFacturacion;


implementation

resourcestring
    CAPTION_CANCELAR_FACTURACION = 'Cancelar Proceso de Facturaci�n';
    CAPTION_CANCELAR_IMPRESION   = 'Cancelar Proceso de Impresi�n';

{$R *.DFM}
function TFormFacturacion.Inicializa: Boolean;
Var
	cGrupo: AnsiString;
    i, CodigoGrupo: Integer;
    slCiclos: TStrings;

begin
    result				:= true;
	FCancelar  			:= False;
	FAntHecho  			:= 0;
	FClienteAnterior 	:= 0;

	PageControl.ActivePage := tab_Inicio;

    application.CreateForm(TFormReporteFactura,FReporteFactura);
    application.CreateForm(TfrmDetalleViajes,FReporteDetalleViajes);

	lbl_impresora.caption := FReporteFactura.getImpresora;
    fechaLimitePorCliente := now;

    FNombre := '';
	FApellido := '';
    FApellidoMaterno := '';
    FNombreImp := '';
    FApellidoImp := '';
    FApellidoMaternoImp := '';

	// Incializo controles para facturar
  	peCodigoCliente.enabled := False;
    txt_FechaLimite.Enabled := False;
	peCodigoCliente.Color := clBtnFace;
    txt_FechaLimite.Color := clBtnFace;
  	peCodigoCliente.value := 0;
    lNombreCompleto.Caption := '';
    lDocumento.Caption := '';
    lDomicilio.Caption := '';
    lComuna.Caption := '';
    lRegion.Caption := '';
    nbGruposCliente.ActivePage := 'Grupos';

	// Incializo controles para imprimir
	peCodigoClienteImp.enabled := False;
	peCodigoClienteImp.Color := clBtnFace;
	peCodigoClienteImp.value := 0;
	lNombreCompletoImp.Caption := '';
	lDocumentoImp.Caption := '';
	lDomicilioIMp.Caption := '';
	lComunaImp.Caption := '';
	lRegionImp.Caption := '';
	nbImprimir.ActivePage := 'Grupos';


	// Cargo los grupos a facturar con sus ciclos pendientes
	FEstado := efNada;
    try
        with ObtenerGruposFacturacionAFacturar do begin
			open;
			i := 0;
			if not isEmpty then begin
				While not Eof do begin
					CodigoGrupo := FieldByName('CodigoGrupoFacturacion').AsInteger;
					cGrupo := FieldByName('Descripcion').AsString + Space(200) +
								  FieldByName('CodigoGrupoFacturacion').AsString;
					slCiclos := TStringList.Create;
					ListaGruposFact.Items.AddObject(cGrupo, slCiclos);
					ListaGruposFact.Checked[i] := True;
					while (not Eof) and (CodigoGrupo = FieldByName('CodigoGrupoFacturacion').AsInteger) do begin
						slCiclos.Add(format('%10.10s - %2.2d',[formatDateTime(shortDateFormat, FieldByName('FechaFacturacion').asDateTime), FieldByName('ciclo').asInteger]));
						Next;
					end;
					inc(i);
				end;
				lbCiclos.items := TStrings(ListaGruposFact.Items.Objects[0]);
			end;
			Close;
		end;

		// Cargar lista de grupos de clientes a Imprimir
		with ObtenerGruposConFacturasAImprimir do begin
            open;
            While not Eof do begin
                cGrupo := FieldByName('Descripcion').AsString + Space(200) + FieldByName('CodigoGrupoFacturacion').AsString;
                ListaGruposImpre.Checked[ListaGruposImpre.Items.Add(cGrupo)] := True;
                Next;
            end;
            Close;
        end;
    except
        result := false;
    end;
end;

procedure TFormFacturacion.Timer1Timer(Sender: TObject);
resourcestring
    CAPTION_SIGUIENTE = '&Siguiente >';
    //CAPTION_FINALIZAR = '&Finalizar';
    CAPTION_FINALIZAR = '&Comenzar';
begin
	Timer1.Enabled := False;
	if PageControl.ActivePage = tab_Inicio then begin
		btn_siguiente.caption := CAPTION_SIGUIENTE;
		btn_anterior.enabled := False;
	end else if PageControl.ActivePage = tab_Facturar then begin
		btn_siguiente.caption := CAPTION_SIGUIENTE;
		btn_anterior.enabled := True;
	end else if PageControl.ActivePage = tab_FacturarFin then begin
		btn_siguiente.caption := CAPTION_FINALIZAR;
		btn_siguiente.enabled := (FEstado = efNada);
		btn_anterior.enabled := (FEstado = efNada);
		btn_cancelar.enabled := (FEstado in [efNada, efFacturando]) and not FCancelar;
	end else if PageControl.ActivePage = tab_Imprimir then begin
		btn_siguiente.caption := CAPTION_SIGUIENTE;
		btn_anterior.enabled := True;
	end else if PageControl.ActivePage = tab_ImprimirFin then begin
		btn_siguiente.caption := CAPTION_FINALIZAR;
		btn_siguiente.enabled := (FEstado = efNada);
		btn_anterior.enabled := (FEstado = efNada);
		btn_cancelar.enabled := (FEstado in [efNada, efImprimiendo]) and not FCancelar;
	end;
	case FEstado of
		efFacturando:
			RevisarEstadoFacturacion;
		efImprimiendo:
			RevisarEstadoImpresion;
	end;
	Timer1.Enabled := True;
end;

procedure TFormFacturacion.btn_siguienteClick(Sender: TObject);
begin
	if PageControl.ActivePage = tab_Inicio then begin
		if rb_facturar.checked then
			PageControl.ActivePage := tab_Facturar
		else if rb_imprimir.checked then begin
			PageControl.ActivePage := tab_Imprimir
		end;
	end else if PageControl.ActivePage = tab_Facturar then begin
		PageControl.ActivePage := tab_FacturarFin;
	end else if PageControl.ActivePage = tab_FacturarFin then begin
		Facturar;
	end else if PageControl.ActivePage = tab_Imprimir then begin
		PageControl.ActivePage := tab_ImprimirFin;
	end else if PageControl.ActivePage = tab_ImprimirFin then begin
		Imprimir;
	end;
end;

procedure TFormFacturacion.btn_anteriorClick(Sender: TObject);
begin
	if PageControl.ActivePage = tab_Facturar then begin
		PageControl.ActivePage := tab_Inicio
	end else if PageControl.ActivePage = tab_FacturarFin then begin
		PageControl.ActivePage := tab_Facturar
	end else if PageControl.ActivePage = tab_Imprimir then begin
		PageControl.ActivePage := tab_Inicio
	end else if PageControl.ActivePage = tab_ImprimirFin then begin
		PageControl.ActivePage := tab_Imprimir
	end;
end;

procedure TFormFacturacion.Button3Click(Sender: TObject);
begin
    lbl_impresora.caption := FReporteFactura.ConfigurarImpresora;
end;

procedure TFormFacturacion.Facturar;
resourcestring
    CAPTION_PREPARANDO = 'Preparando la Facturaci�n';
    CAPTION_FACTURANDO = 'Facturaci�n en Progreso';
Var
	i, Grupo: Integer;
	FechaFacturacion, Vencimiento: TDateTime;
    listaCiclos: TStrings;
begin
	lFacturando.caption := CAPTION_PREPARANDO;
	update;
	// Inicializaci�n de variables
	FCuentasProcesadas := 0;
	FThreadsCorriendo := 0;
	FEstado := efFacturando;
	FInicio := Now;

	// Contamos las cuentas (para despu�s poder medir el progreso)
	FTotalCuentas := ContarCuentas;
	// Creamos el proceso de facturacion que vamos a comenzar
	if FTotalCuentas <> 0 then begin
	 	FNumeroProcesoFacturacion := CrearProcesoFacturacion;
		// Creamos los Threads
		Screen.Cursor := crHourGlass;
		lFacturando.Caption := CAPTION_FACTURANDO;
		if rb_soloclienteFact.Checked then begin
   			TThreadFacturacion.Create(Trunc(peCodigoCliente.value), 0, 40,
    		  txt_FechaLimite.Date + 1, FNumeroProcesoFacturacion,
	    	  FCuentasProcesadas, FThreadsCorriendo, FCancelar);
        end else begin
    		for i := 0 to ListaGruposFact.Items.Count - 1 do begin
				if ListaGruposFact.Checked[i] then begin
					listaCiclos := (ListaGruposFact.Items.Objects[i] as TStrings);
					fechaFacturacion := strToDateTime(strLeft(listaCiclos[0], 10));
					Grupo := IVal(StrRight(ListaGruposFact.Items[i], 20));
					TThreadFacturacion.Create(0, Grupo, 40, FechaFacturacion,
					  FNumeroProcesoFacturacion, FCuentasProcesadas, FThreadsCorriendo, FCancelar);
	    		end;
		    end;
    	end;
    end else
		lFacturando.Caption := '';
	Screen.Cursor := crDefault;
end;

procedure TFormFacturacion.Imprimir;
//Var
//	Config: TRBConfig;
begin
	Screen.Cursor := crHourGlass;
	// Inicializaci�n de variables
	FTotalComprobantes := 0;
	FEstado := efImprimiendo;
{	Config := RBSetup.LoadConfig(rp_Factura.Name);
	RBSetup.SaveConfig(rp_FacturacionDetallada.Name, Config);    }
	// Contamos las cuentas (para despu�s poder medir el progreso)
	FTotalComprobantes := ContarComprobantes;
	FInicio := Now;
	Screen.Cursor := crDefault;
end;

procedure TFormFacturacion.btn_cancelarClick(Sender: TObject);
resourcestring
    MSG_CANCELAR_FACTURACION = '�Est� seguro de que desea cancelar la Facturaci�n?';
    MSG_CANCELAR_IMPRESION   = '�Est� seguro de que desea cancelar la Impresi�n?';
begin
	case FEstado Of
		efNada:
			Close;
		efFacturando:
			if MsgBox(MSG_CANCELAR_FACTURACION, CAPTION_CANCELAR_FACTURACION, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
			  FCancelar := True;
              RevisarEstadoFacturacion;
            end;
		efImprimiendo:
			if MsgBox(MSG_CANCELAR_IMPRESION, CAPTION_CANCELAR_IMPRESION, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
			    FCancelar := True;
                RevisarEstadoImpresion;
            end;
	end;
end;

procedure TFormFacturacion.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := (FEstado = efNada);
end;

procedure TFormFacturacion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    FReporteDetalleViajes.free;
    FReporteFactura.free;
	Action := caFree;
end;

procedure TFormFacturacion.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;

procedure TFormFacturacion.MostrarProgreso(Inicio: TDateTime;
  Hecho: Single; lbl1, lbl2: TLabel; pb: TProgressBar);
resourcestring
    MSG_CALCULANDO = 'Calculando Tiempos de Procesamiento';
Var
	Insumido: TDateTime;
begin
	Insumido := Now - Inicio;
	lbl1.caption := FormatDateTime('hh:nn:ss', Insumido);
	if (Insumido < 3 / SecsPerDay) or (Hecho = 0) then begin
		lbl2.caption := MSG_CALCULANDO;
	end else begin
		lbl2.caption := FormatDateTime('hh:nn:ss', Insumido / Hecho * (1 - Hecho));
	end;
	pb.position := Round(Hecho * 100);
end;

function TFormFacturacion.ContarCuentas: Integer;
begin
	Screen.Cursor := crHourGlass;
	if (FEstado = efImprimiendo) and rb_soloclienteImp.checked then begin
		Result := QueryGetValueInt(DMConnections.BaseCAC,
        		  'SELECT Count(*) FROM Cuentas ' +
            	  'WHERE TipoPago <> ''PR'' AND CodigoCliente = ' +
		          FloatToStr(peCodigoClienteImp.Value));
	end else if (FEstado = efFacturando) and rb_soloclienteFact.checked then begin
        Result := QueryGetValueInt(DMConnections.BaseCAC,
                  'SELECT Count(*) FROM Cuentas ' +
                  'WHERE TipoPago <> ''PR'' AND CodigoCLiente = ' +
                  FloatToStr(peCodigoCliente.Value));
	end else begin
		Result := QueryGetValueInt(DMConnections.BaseCAC,
        		  'SELECT Count(*) FROM Cuentas ' +
                  'WHERE CodigoGrupoFacturacion IN (' +
	              StringGruposFacturacion + ') AND TipoPago <> ''PR''');
	end;
	Screen.Cursor := crDefault;
end;

Function TFormFacturacion.ContarComprobantes: Integer;
begin
	if rb_soloclienteimp.Checked then begin
		// Imprimir las cuentas de un cliente
		Result := QueryGetValueInt(DMConnections.BaseCAC, Format(
		  'SELECT COUNT(*) FROM Cuentas WHERE CodigoCliente = %d',
		  [Trunc(peCodigoClienteImp.Value)]));
	end else begin
		// Imprimir Ciertos grupos
		with QryContarComprobantes do begin
			Close;
			Parameters.ParamByName('GruposFacturacion').value := StringGruposFacturacion;
			Open;
			Result := FieldByName('CantComprobantes').AsInteger;
			Close;
		end;
	end;
end;

function TFormFacturacion.StringGruposFacturacion: AnsiString;
Var
	i: Integer;
begin
	Result := '';
	if FEstado = efFacturando then begin
		for i := 0 to ListaGruposFact.Items.Count - 1 do begin
			if ListaGruposFact.Checked[i] then Result := Result +
			  Trim(StrRight(ListaGruposFact.Items[i], 20)) + ',';
		end;
	end else if FEstado = efImprimiendo then begin
		for i := 0 to ListaGruposImpre.Items.Count - 1 do begin
			if ListaGruposImpre.Checked[i] then Result := Result +
			  Trim(StrRight(ListaGruposImpre.Items[i], 20)) + ',';
		end;
	end;
	if Result = '' then
		Result := 'NULL'
	else begin
		SetLength(Result, Length(Result) - 1);
	end;
end;

procedure TFormFacturacion.RevisarEstadoFacturacion;
var i, Dias: Integer;
	Horas, Min: Word;

    procedure calcularDiasHorasMin(FechaHoraEntrada,FechaHoraSalida:TDateTime; var Dias:Integer;var Horas,Min:Word);
    var Sec,MSec:Word;
        CantidadHoras : TDateTime;
    begin
        Dias := DateTimeToTimeStamp(FechaHoraSalida).date - DateTimeToTimeStamp(FechaHoraEntrada).date;
        if DateTimeToTimeStamp(FechaHoraSalida).time < DateTimeToTimeStamp(FechaHoraEntrada).time then
            dias := dias - 1;
        CantidadHoras := (24 - FechaHoraentrada) + FechaHoraSalida;
        DecodeTime(CantidadHoras,Horas,Min,Sec,MSec);
    end;

resourcestring
    // Mensajes de informaci�n en el form FormFacturacion
    CAPTION_FINALIZANDO = 'Finalizando Facturaci�n';

    // Mensajes sobre MsgBox
    CAPTION_FACTURACION_FINALIZADA = 'Facturaci�n Finalizada';
    MSG_FACTURACION_FINALIZADA     = 'La Facturaci�n se completo con �xito.';
    MSG_FACTURACION_CANCELADA      = 'Facturaci�n Cancelada. Si lo desea, puede reanudar la Facturaci�n m�s tarde.';

begin
	if FThreadsCorriendo = 0 then begin
	    // Se termin� de Facturar calculamos la cantidad de facturas, de cuentas y el tiempo del proceso
        lFacturando.Caption := CAPTION_FINALIZANDO;
        update;
        lFacturando.Caption := '';
		if FCancelar then begin
			MsgBox(MSG_FACTURACION_CANCELADA, CAPTION_CANCELAR_FACTURACION, MB_ICONWARNING);
		end else begin
            // Si la facturacion fue por grupo marcamos como completa
            // la facturaci�n del ciclo para los grupos facturados
            ObtenerCantidadComprobantes.Close;
            ObtenerCantidadComprobantes.Parameters.ParamByName('NumeroProcesoFacturacion').value := FNumeroProcesoFacturacion;
            ObtenerCantidadComprobantes.open;
            // Actualizamos el proceso de facturacion
            calcularDiasHorasMin(FInicio,Now,Dias,Horas,Min);
            ActualizarProceso.Parameters.ParamByName('Duracion').value := Dias * 24 * 60 + Horas * 60 + min;
            ActualizarProceso.Parameters.ParamByName('CantCuentasFacturadas').value := FCuentasProcesadas;
            ActualizarProceso.Parameters.ParamByName('CantFacturas').value := ObtenerCantidadComprobantes.FieldByName('CantFacturas').AsInteger;
            ActualizarProceso.Parameters.ParamByName('NumeroProcesoFacturacion').value := FNumeroProcesoFacturacion;
            ActualizarProceso.ExecSQL;
            ObtenerCantidadComprobantes.close;

            if not rb_soloclienteFact.checked then
                // Por cada grupo actualizamos la agenda de facturaci�n
                for i := 0 to ListaGruposFact.Items.Count - 1 do
					if ListaGruposFact.Checked[i] then begin
						// Actualizo la agenda de facturacion
						ActualizarAgendaFacturacion.Close;
						ActualizarAgendaFacturacion.Parameters.ParamByName('CodigoGrupoFacturacion').value :=
							IVal(StrRight(ListaGruposFact.Items[i], 20));
						ActualizarAgendaFacturacion.Parameters.ParamByName('FechaCorte').value :=
							now;
						ActualizarAgendaFacturacion.ExecSQL;
                        // Modifico la fecha de facturacion si el grupo es el 10 (Grupo Quincenal con pocas 1000 cuentas)

                 {      UpDateFechaFacturacion.Parameters.ParamByName('Fecha').Value := fechaFacturacion;
                        UpDateFechaFacturacion.Parameters.ParamByName('FechaVencimiento').Value := fechaFacturacion + 30;
                        UpDateFechaFacturacion.Parameters.ParamByName('Grupo').Value := Grupo;
                        UpDateFechaFacturacion.execSQL; }
					end;
			MsgBox(MSG_FACTURACION_FINALIZADA, CAPTION_FACTURACION_FINALIZADA, MB_ICONINFORMATION);
		end;
		FEstado := efNada;
		Close;
	end else begin
		if FAntHecho <> FCuentasProcesadas then begin
            if FTotalCuentas = 0 then FTotalCuentas := 1;
			MostrarProgreso(FInicio, FCuentasProcesadas / FTotalCuentas,
			  lbl_tiempofacttrans, lbl_tiempofactrest, pb_Facturar);
			lbl_cuentasproc.Caption := IntToStr(FCuentasProcesadas);
			FAntHecho := FCuentasProcesadas;
		end;
	end;
end;

procedure TFormFacturacion.rb_soloclienteimpClick(Sender: TObject);
begin
    peCodigoClienteImp.value := FCodigoClienteImp;
	peCodigoClienteImp.enabled := true;
	peCodigoClienteImp.Color := clWindow;
    nbImprimir.ActivePage := 'Cliente';
end;

procedure TFormFacturacion.RadioButton2Click(Sender: TObject);
begin
    FCodigoClienteImp := trunc(peCodigoClienteImp.value);
    peCodigoClienteImp.value := 0;
	peCodigoClienteImp.enabled := False;
	peCodigoClienteImp.Color := clBtnFace;
	nbImprimir.ActivePage := 'Grupos';
end;

procedure TFormFacturacion.RevisarEstadoImpresion;
resourcestring
    MSG_IMPRESION_CANCELADA     = 'Impresi�n Cancelada. Si lo desea, puede reanudar la Impresi�n m�s tarde.';
    MSG_IMPRESION_FINALIZADA    = 'La Impresi�n se ha completado con �xito.';
    CAPTION_IMPRESION_FINALIZADA = 'Impresi�n Finalizada';
begin
	// Si cancelaron finalizo la impresion
	if FCancelar then begin
		// Para finalizar la impresion verificamos si la impresion realizada
    	// cubre todos los comprobantes de un proceso
        ActualizarProcesoFacturacion.ExecSQL;
		MsgBox(MSG_IMPRESION_CANCELADA, CAPTION_CANCELAR_IMPRESION, MB_ICONWARNING);
		FEstado := efNada;
		Close;
	end;
	// Intentamos Imprimir todos los comprobantes pendientes de un cliente
    // Si retorna cero es por que no hay nada mas para imprimir
	if ImprimirCliente(FClienteAnterior) then begin
		Inc(FComprobantesProcresados);
        if FTotalComprobantes = 0 then
            FTotalComprobantes := 1;
		MostrarProgreso(FInicio, FComprobantesProcresados / FTotalComprobantes,
		  lbl_tiempoimptrans, lbl_tiempoimprest, pb_Imprimir);
		lbl_cuentasimp.Caption := IntToStr(FComprobantesProcresados);
	end else begin
		ActualizarProcesoFacturacion.ExecSQL;
		MsgBox(MSG_IMPRESION_FINALIZADA, CAPTION_IMPRESION_FINALIZADA, MB_ICONINFORMATION);
		FEstado := efNada;
		Close;
	end;
end;

function TFormFacturacion.ImprimirCliente(var ClienteAnterior: Integer): Boolean;
var tipoComprobante: AnsiString;
begin
	// Ejecutamos el StoredProc que nos devuelve los comprobantes de este cliente
	with ObtenerComprobantesAImprimir do begin
		Parameters.ParamByName('@ClienteAnterior').Value := ClienteAnterior;
		Parameters.ParamByName('@GruposFacturacion').Value := StringGruposFacturacion;
		Parameters.ParamByName('@CodigoCliente').Value := peCodigoClienteImp.value;
		Open;
		// Si retorna cero registro es por no hay nada m�s para imprimir.
		if Eof then begin
			Close;
			Result := False;
			Exit;
		end;
		// Vamos imprimiendo cada comprobante con su facturaci�n detallada.
	    while not Eof do begin
            tipoComprobante := FieldByName('TipoComprobante').AsString;
            if tipoComprobante = TC_FACTURA then begin
				ImprimirComprobante(tipoComprobante[1],
                                    FieldByName('CodigoCliente').asInteger,
                                    FieldByName('NumeroComprobante').asFloat);
				// Obtener las cuentas facturadas en este comprobante que requieren detalle
				// e imprimir el detalle para cada una
                ObtenerCuentasConDetalle.close;
                ObtenerCuentasConDetalle.Parameters.ParamByName('TipoComprobante').value := TipoComprobante;
                ObtenerCuentasConDetalle.Parameters.ParamByName('NumeroComprobante').value := FieldByName('NumeroComprobante').asFloat;
                ObtenerCuentasConDetalle.open;
                ObtenerCuentasConDetalle.first;
                While not ObtenerCuentasConDetalle.eof do begin
                    ImprimirFacturacionDetallada(TipoComprobante[1],
					  ObtenerCuentasConDetalle.FieldByName('CodigoCuenta').AsInteger,
                      FieldByName('CodigoCliente').asInteger,
					  FieldByName('NumeroComprobante').AsFloat);
                    ObtenerCuentasConDetalle.Next;
                end;
				ClienteAnterior := FieldByName('CodigoCliente').AsInteger;
            end;
    		Next;
	    end;
    	Close;
	    Result := True;
    end;
end;

procedure TFormFacturacion.ImprimirFacturacionDetallada(TipoComprobante: Char; Cuenta, CodigoCliente: Integer; NumeroComprobante: Double);
begin
    FReporteDetalleViajes.rbi_FacturacionDetallada.setConfig(FReporteFactura.rbi_Factura.GetConfig);
    FReporteDetalleViajes.Execute(TipoComprobante, Cuenta, CodigoCliente, NumeroComprobante, False, MSG_DETALLE_VIAJES);
end;

procedure TFormFacturacion.ImprimirComprobante(TipoComprobante: Char; CodigoCliente: Integer; NumeroComprobante: Double);
begin
    FReporteFactura.Execute(TipoComprobante, CodigoCliente, NumeroComprobante, false, MSG_DOCUMENTO_DEMOSTRACION);
end;


procedure TFormFacturacion.rb_soloclientefactClick(Sender: TObject);
begin
	// Habilito el campo codigo cliente y el campo fecha
    peCodigoCliente.value := FCodigoCliente;
    txt_FechaLimite.Date := FechaLimitePorCliente;
	peCodigoCliente.enabled := True;
    txt_FechaLimite.Enabled := True;
	peCodigoCliente.Color := clWindow;
    txt_FechaLimite.Color := clWindow;
	// Cambio de pagina
    nbGruposCliente.ActivePage := 'Cliente';
end;

procedure TFormFacturacion.rbFacturarGruposClick(Sender: TObject);
begin
	// Guardo las fecha del cliente y asigno la fecha de agenda
	FechaLimitePorCliente := txt_FechaLimite.date;
	// Habilito el campo fecha y el campo codigo cliente
	peCodigoCliente.enabled := False;
	txt_FechaLimite.Enabled := False;
	peCodigoCliente.Color	:= clBtnFace;
	txt_FechaLimite.Color	:= clBtnFace;
	FCodigoCliente			:= trunc(peCodigoCliente.value);
	peCodigoCliente.value	:= 0;
	// Cambio de pagina
    nbGruposCliente.ActivePage := 'Grupos';
end;

function TFormFacturacion.CrearProcesoFacturacion: Integer;
var
    i: Integer;
    listaCiclos: TStrings;
begin
    with spCrearProcesoFacturacion do begin
        Parameters.ParamByName('@Operador').value := UsuarioSistema;
        execProc;
        result := Parameters.paramByName('@NumeroProcesoFacturacion').value;

        if rb_soloclientefact.Checked then begin
            CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@NumeroProcesoFacturacion').value :=
              result;
            CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@CodigoGrupoFacturacion').value :=
                QueryGetValueInt(DMConnections.BaseCAC,
                'select codigoGrupoFacturacion from cuentas where cuentas.codigocliente = ' +
                intToStr(trunc(peCodigoCliente.Value)));
            CrearProcesoFacturacionGrupoFacturacion.execPROC;
        end;

        // Agregamos los grupos a facturar
        if rbFacturarGrupos.Checked then begin
			for i := 0 to ListaGruposFact.Items.Count - 1 do begin
				if ListaGruposFact.checked[i] then begin
					CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@NumeroProcesoFacturacion').value :=
					  result;
					CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@CodigoGrupoFacturacion').value :=
                      IVal(StrRight(ListaGruposFact.Items[i], 20));
                    listaCiclos := (ListaGruposFact.Items.Objects[i] as TStrings);
        			CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@FechaInicial').value :=
                      strToDateTime(strLeft(listaCiclos[ListaCiclos.count - 1], 10)) - 30;
					CrearProcesoFacturacionGrupoFacturacion.Parameters.paramByName('@FechaFinal').value :=
                      strToDateTime(strLeft(ListaCiclos[0], 10));
                    CrearProcesoFacturacionGrupoFacturacion.execPROC;
                end;
            end;
        end;
    end;
end;

procedure TFormFacturacion.peCodigoClienteButtonClick(Sender: TObject);
Var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, F);
	if f.Inicializa('', '', FApellido, FApellidoMaterno, FNombre, True)
       and (f.ShowModal = mrOk) then begin
		peCodigoCliente.value := f.CodigoCliente;
        mostrarCliente(f.CodigoCliente);
	end;
	f.Release;
end;

procedure TFormFacturacion.ListaGruposFactClick(Sender: TObject);
begin
    lbCiclos.items := TStrings((Sender as TCheckListBox).Items.Objects[(Sender as TCheckListBox).ItemIndex]);
end;

procedure TFormFacturacion.peCodigoClienteImpButtonClick(Sender: TObject);
Var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, F);
	if f.Inicializa('', '', FApellidoImp, FApellidoMaternoImp, FNombreImp, True)
        and (f.ShowModal = mrOk) then begin
		peCodigoClienteImp.value := f.CodigoCliente;
        MostrarClienteImp(f.CodigoCliente);
	end;
	f.Release;
end;

procedure TFormFacturacion.peCodigoClienteImpChange(Sender: TObject);
begin
    if activeControl = Sender then
        MostrarClienteImp(trunc((Sender as TPickEdit).Value));
end;

procedure TFormFacturacion.peCodigoClienteChange(Sender: TObject);
begin
    if activeControl = Sender then
        MostrarCliente(trunc((Sender as TPickEdit).Value));
end;

procedure TFormFacturacion.MostrarClienteImp(CodigoCLiente: Integer);
begin
    with ObtenerCliente do begin
        Parameters.ParamByName('@CodigoCliente').value := CodigoCliente;
        open;
        FApellidoImp := ObtenerCliente.fieldByName('Apellido').asString;
        FApellidoMaternoImp := ObtenerCliente.fieldByName('ApellidoMaterno').asString;
        FNombreImp := ObtenerCliente.fieldByName('Nombre').asString;
        if not eof then begin

            lNombreCompletoImp.Caption := ArmaNombreCompleto(FApellidoImp, FApellidoMaternoImp, FNombreImp);

            lDocumentoImp.Caption := format('%s - %s',[trim(ObtenerCliente.fieldByName('CodigoDocumento').asString),
                                                       trim(ObtenerCliente.fieldByName('NumeroDocumento').asString)]);
            //si no cargo el domicilio de Documentacion, usamos el domicilio Personal

            if Trim(ObtenerCliente.FieldByName('calleDocumentos').Value) <> '' then begin
                lDomicilioImp.Caption := ArmarDomicilioSimple(
                                            Trim(ObtenerCliente.FieldByName('calleDocumentos').Value),
                                            Trim(ObtenerCliente.FieldByName('numeroDocumentos').Value),
                                            Trim(ObtenerCliente.FieldByName('detalleDocumentos').Value));

                lComunaImp.Caption := BuscarDescripcionComuna(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionDocumentos').Value),
                                        trim(ObtenerCliente.FieldByName('comunaDocumentos').Value));

                lRegionImp.Caption := BuscarDescripcionRegion(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionDocumentos').Value));
            end
            else begin
                //Utilizo el domicilio Personal
                lDomicilioImp.Caption := ArmarDomicilioSimple(
                                            Trim(ObtenerCliente.FieldByName('callePersonal').Value),
                                            Trim(ObtenerCliente.FieldByName('numeroPersonal').Value),
                                            Trim(ObtenerCliente.FieldByName('detallePersonal').Value));

                lComunaImp.Caption := BuscarDescripcionComuna(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionPersonal').Value),
                                        trim(ObtenerCliente.FieldByName('comunaPersonal').Value));

                lRegionImp.Caption := BuscarDescripcionRegion(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionPersonal').Value));
            end;
        end else begin
            lNombreCompletoImp.Caption := '';
            lDocumentoImp.Caption := '';
            lDomicilioImp.Caption := '';
            lComunaImp.Caption := '';
            lRegionImp.Caption := '';
        end;
        close;
    end;
end;

procedure TFormFacturacion.MostrarCliente(CodigoCLiente: Integer);
begin
    with ObtenerCliente do begin
        Parameters.ParamByName('@CodigoCliente').value := CodigoCliente;
        open;
        FApellido := ObtenerCliente.fieldByName('Apellido').asString;
        FApellidoMaterno := ObtenerCliente.fieldByName('ApellidoMaterno').asString;
        FNombre := ObtenerCliente.fieldByName('Nombre').asString;
        if not eof then begin
            lNombreCompleto.Caption := ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre);

            lDocumento.Caption := format('%s - %s',[trim(ObtenerCliente.fieldByName('CodigoDocumento').asString),
                                                    trim(ObtenerCliente.fieldByName('NumeroDocumento').asString)]);

            if Trim(ObtenerCliente.FieldByName('calleDocumentos').Value) <> '' then begin
                lDomicilio.Caption := ArmarDomicilioSimple(
                                            Trim(ObtenerCliente.FieldByName('calleDocumentos').Value),
                                            Trim(ObtenerCliente.FieldByName('numeroDocumentos').Value),
                                            Trim(ObtenerCliente.FieldByName('detalleDocumentos').Value));

                lComuna.Caption := BuscarDescripcionComuna(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionDocumentos').Value),
                                        trim(ObtenerCliente.FieldByName('comunaDocumentos').Value));

                lRegion.Caption := BuscarDescripcionRegion(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionDocumentos').Value));
            end
            else begin
                lDomicilio.Caption := ArmarDomicilioSimple(
                                            Trim(ObtenerCliente.FieldByName('callePersonal').Value),
                                            Trim(ObtenerCliente.FieldByName('numeroPersonal').Value),
                                            Trim(ObtenerCliente.FieldByName('detallePersonal').Value));

                lComuna.Caption := BuscarDescripcionComuna(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionPersonal').Value),
                                        trim(ObtenerCliente.FieldByName('comunaPersonal').Value));

                lRegion.Caption := BuscarDescripcionRegion(DMConnections.BaseCAC,
                                        trim(ObtenerCliente.FieldByName('CodigoPais').Value),
                                        trim(ObtenerCliente.FieldByName('regionPersonal').Value));
            end;
        end else begin
            lNombreCompleto.Caption := '';
            lDocumento.Caption := '';
            lDomicilio.Caption := '';
            lComuna.Caption := '';
            lRegion.Caption := '';
        end;
        close;
    end;
end;

end.
