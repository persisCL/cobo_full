object IngresoEspecialSDForm: TIngresoEspecialSDForm
  Left = 0
  Top = 0
  Caption = 'IngresoEspecialSDForm'
  ClientHeight = 391
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lblTipoFiscal: TLabel
    Left = 64
    Top = 141
    Width = 121
    Height = 13
    Caption = 'Tipo Combrobante Fiscal:'
  end
  object lblNumeroFiscal: TLabel
    Left = 47
    Top = 172
    Width = 138
    Height = 13
    Caption = 'N'#250'mero Comprobante Fiscal:'
  end
  object Label1: TLabel
    Left = 47
    Top = 24
    Width = 312
    Height = 18
    Caption = 'Ingreso Especial Comprobante SD - Pagado'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lblObservacion: TLabel
    Left = 121
    Top = 287
    Width = 64
    Height = 13
    Caption = 'Observaci'#243'n:'
  end
  object lblAfecto: TLabel
    Left = 116
    Top = 236
    Width = 69
    Height = 13
    Caption = 'Monto Afecto:'
  end
  object lblExento: TLabel
    Left = 114
    Top = 204
    Width = 71
    Height = 13
    Caption = 'Monto Exento:'
  end
  object lbl1: TLabel
    Left = 108
    Top = 78
    Width = 77
    Height = 13
    Caption = 'RUT del Cliente:'
  end
  object lbl2: TLabel
    Left = 80
    Top = 110
    Width = 107
    Height = 13
    Caption = 'Convenios del Cliente:'
  end
  object lblIVA: TLabel
    Left = 349
    Top = 236
    Width = 21
    Height = 13
    Caption = 'IVA:'
  end
  object Label2: TLabel
    Left = 138
    Top = 255
    Width = 47
    Height = 13
    Caption = '( sin IVA )'
  end
  object lblSaldoLabel: TLabel
    Left = 436
    Top = 110
    Width = 34
    Height = 13
    Caption = 'Saldo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblSaldoConvenio: TLabel
    Left = 476
    Top = 110
    Width = 96
    Height = 13
    Caption = 'lblSaldoConvenio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object vcbTipoComprobanteFiscal: TVariantComboBox
    Left = 191
    Top = 137
    Width = 218
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 3
    Items = <>
  end
  object edtObservacion: TEdit
    Left = 191
    Top = 283
    Width = 466
    Height = 21
    MaxLength = 100
    TabOrder = 8
    Text = 'edtObservacion'
  end
  object neNumeroComprobanteFiscal: TNumericEdit
    Left = 191
    Top = 168
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object neMontoExento: TNumericEdit
    Left = 191
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object neMontoAfecto: TNumericEdit
    Left = 191
    Top = 232
    Width = 121
    Height = 21
    TabOrder = 6
    OnChange = neMontoAfectoChange
  end
  object btnCancelar: TButton
    Left = 188
    Top = 349
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 9
    OnClick = btnCancelarClick
  end
  object btnAceptar: TButton
    Left = 343
    Top = 349
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 10
    OnClick = btnAceptarClick
  end
  object peRUTCliente: TPickEdit
    Left = 191
    Top = 74
    Width = 218
    Height = 21
    CharCase = ecUpperCase
    Color = 16444382
    Enabled = True
    TabOrder = 0
    OnChange = peRUTClienteChange
    EditorStyle = bteTextEdit
    OnButtonClick = peRUTClienteButtonClick
  end
  object btnBuscar: TButton
    Left = 416
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 1
    OnClick = btnBuscarClick
  end
  object vcbConveniosCliente: TVariantComboBox
    Left = 191
    Top = 107
    Width = 218
    Height = 19
    Style = vcsOwnerDrawFixed
    Color = 16444382
    ItemHeight = 13
    TabOrder = 2
    OnChange = vcbConveniosClienteChange
    OnDrawItem = vcbConveniosClienteDrawItem
    Items = <>
  end
  object neMontoIVA: TNumericEdit
    Left = 376
    Top = 232
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object spGeneracionEspecialComprobanteSaldoInicialDeudor: TADOStoredProc
    ProcedureName = 'GeneracionEspecialComprobanteSaldoInicialDeudor'
    Parameters = <>
    Left = 584
    Top = 88
  end
end
