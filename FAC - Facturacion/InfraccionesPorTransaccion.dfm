object FrmTransitosInfractores: TFrmTransitosInfractores
  Left = 33
  Top = 103
  Width = 906
  Height = 600
  Anchors = []
  Caption = 'Tratamiento de Infracciones'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    898
    566)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 915
    Height = 509
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      915
      509)
    object gbViajes: TGroupBox
      Left = 6
      Top = 57
      Width = 889
      Height = 444
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Lista de Viajes'
      TabOrder = 1
      DesignSize = (
        889
        444)
      object Label3: TLabel
        Left = 477
        Top = 24
        Width = 102
        Height = 13
        AutoSize = False
        Caption = 'Imagen Veh'#237'culo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 475
        Top = 331
        Width = 95
        Height = 13
        AutoSize = False
        Caption = 'Imagen Patente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 15
        Top = 222
        Width = 94
        Height = 13
        Caption = 'Detalle de Tr'#225'nsitos'
      end
      object dbgTransitos: TDPSGrid
        Left = 10
        Top = 22
        Width = 454
        Height = 163
        DataSource = dsTransitos
        Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgMultiSelect]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyUp = dbgTransitosKeyUp
        OnMouseUp = dbgTransitosMouseUp
        Columns = <
          item
            Expanded = False
            FieldName = 'Patente'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fecha'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Juzgado'
            Width = 200
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Estado'
            Title.Alignment = taCenter
            Width = 80
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 477
        Top = 48
        Width = 401
        Height = 273
        Anchors = [akLeft, akTop, akRight]
        BevelOuter = bvLowered
        TabOrder = 3
        object ImagenVehiculo: TImagePlus
          Left = 1
          Top = 1
          Width = 399
          Height = 271
          HorzScrollBar.Style = ssHotTrack
          HorzScrollBar.Visible = False
          VertScrollBar.Visible = False
          Align = alClient
          Stretch = True
          Scale = 1.000000000000000000
        end
      end
      object Panel3: TPanel
        Left = 477
        Top = 353
        Width = 122
        Height = 78
        BevelOuter = bvLowered
        TabOrder = 4
        object imgPatente: TImagePlus
          Left = 1
          Top = 1
          Width = 120
          Height = 75
          HorzScrollBar.Style = ssHotTrack
          Align = alTop
          Scale = 1.000000000000000000
        end
      end
      object dblDetalle: TDBListEx
        Left = 10
        Top = 240
        Width = 454
        Height = 191
        Anchors = [akLeft, akTop, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Hora'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Hora'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Punto de Cobro'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescriPuntosCobro'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Categor'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Categoria'
          end>
        DataSource = dsDetalle
        DragReorder = True
        ParentColor = False
        TabOrder = 5
        TabStop = True
        OnClick = dblDetalleClick
        OnKeyUp = dblDetalleKeyUp
      end
      object btnMarcarTodo: TButton
        Left = 240
        Top = 194
        Width = 107
        Height = 25
        Caption = '&Marcar Todo'
        TabOrder = 0
        OnClick = btnMarcarTodoClick
      end
      object btnInvertirSeleccion: TButton
        Left = 352
        Top = 194
        Width = 107
        Height = 25
        Caption = 'Invertir Selecci'#243'n'
        TabOrder = 1
        OnClick = btnInvertirSeleccionClick
      end
    end
    object gpBusqueda: TGroupBox
      Left = 6
      Top = 1
      Width = 889
      Height = 56
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Par'#225'metros de b'#250'squeda'
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 26
        Width = 61
        Height = 13
        Caption = 'Fecha Inicio:'
      end
      object Label2: TLabel
        Left = 192
        Top = 26
        Width = 50
        Height = 13
        Caption = 'Fecha Fin:'
      end
      object txtFechaInicio: TDateEdit
        Left = 88
        Top = 23
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object txtFechaFin: TDateEdit
        Left = 264
        Top = 23
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 1
        Date = -693594.000000000000000000
      end
      object btnFiltrar: TButton
        Left = 382
        Top = 20
        Width = 75
        Height = 25
        Caption = '&Filtrar'
        Default = True
        TabOrder = 2
        OnClick = btnFiltrarClick
      end
    end
  end
  object ProgressBar: TProgressBar
    Left = 8
    Top = 520
    Width = 241
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 3
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 547
    Width = 898
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 200
      end
      item
        Style = psOwnerDraw
        Width = 50
      end
      item
        Bevel = pbNone
        Width = 50
      end>
    SimplePanel = False
    OnDrawPanel = StatusBarDrawPanel
  end
  object btnSalir: TButton
    Left = 814
    Top = 513
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnSalirClick
  end
  object btnGenerarInfracciones: TButton
    Left = 718
    Top = 513
    Width = 81
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Aceptar'
    TabOrder = 1
    OnClick = btnGenerarInfraccionesClick
  end
  object Imagenes: TImageList
    Left = 872
    Top = 408
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object ObtenerTransitosAnomalosInfraccionJuzgado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTransitosAnomalosInfraccionJuzgado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaInicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFin'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Prepared = True
    Left = 376
    Top = 515
    object ObtenerTransitosAnomalosInfraccionJuzgadoCodigoJuzgado: TIntegerField
      FieldName = 'CodigoJuzgado'
    end
    object ObtenerTransitosAnomalosInfraccionJuzgadoFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object ObtenerTransitosAnomalosInfraccionJuzgadoJuzgado: TStringField
      FieldName = 'Juzgado'
      FixedChar = True
      Size = 50
    end
    object ObtenerTransitosAnomalosInfraccionJuzgadoPatenteValidada: TStringField
      FieldName = 'PatenteValidada'
      FixedChar = True
      Size = 10
    end
  end
  object dsTransitos: TDataSource
    DataSet = cdsObtenerTransitosAnomalosInfraccionJuzgado
    OnDataChange = dsTransitosDataChange
    Left = 312
    Top = 515
  end
  object ActualizarInfraccionesTransitos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesTransitos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Fecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Juzgado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 264
    Top = 515
  end
  object dsDetalle: TDataSource
    DataSet = ObtenerTransitosAnomalosInfraccion
    Left = 436
    Top = 515
  end
  object ObtenerTransitosAnomalosInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTransitosAnomalosInfraccion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Fecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Prepared = True
    Left = 468
    Top = 515
    object ObtenerTransitosAnomalosInfraccionCodigoConcesionaria: TWordField
      FieldName = 'CodigoConcesionaria'
    end
    object ObtenerTransitosAnomalosInfraccionNumeroViaje: TIntegerField
      FieldName = 'NumeroViaje'
    end
    object ObtenerTransitosAnomalosInfraccionFechaHora: TDateTimeField
      FieldName = 'FechaHora'
    end
    object ObtenerTransitosAnomalosInfraccionHora: TStringField
      FieldName = 'Hora'
      ReadOnly = True
      Size = 5
    end
    object ObtenerTransitosAnomalosInfraccionNumeroPuntoCobro: TWordField
      FieldName = 'NumeroPuntoCobro'
    end
    object ObtenerTransitosAnomalosInfraccionDescriPuntosCobro: TStringField
      FieldName = 'DescriPuntosCobro'
      FixedChar = True
      Size = 60
    end
    object ObtenerTransitosAnomalosInfraccionCategoria: TStringField
      FieldName = 'Categoria'
      FixedChar = True
      Size = 60
    end
    object ObtenerTransitosAnomalosInfraccionPatenteValidada: TStringField
      FieldName = 'PatenteValidada'
      FixedChar = True
      Size = 10
    end
  end
  object PopupMenu: TPopupMenu
    OnPopup = PopupMenuPopup
    Left = 508
    Top = 515
    object mnu_Aceptar: TMenuItem
      Caption = 'Aceptar'
      OnClick = mnu_AceptarClick
    end
    object mnu_Rechazar: TMenuItem
      Caption = 'Rechazar'
      OnClick = mnu_RechazarClick
    end
    object mnu_SinEstado: TMenuItem
      Caption = 'Sin Estado'
      OnClick = mnu_SinEstadoClick
    end
  end
  object cdsObtenerTransitosAnomalosInfraccionJuzgado: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Patente'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Fecha'
        DataType = ftDateTime
      end
      item
        Name = 'Juzgado'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Estado'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CodigoJuzgado'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 344
    Top = 515
  end
end
