{

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

}
unit GrabacionMedioPago;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DeclTag, DeclHard, StdCtrls, ExtCtrls, Util, UtilProc, PeaTypes,
  PeaProcs, Db, DBTables, UtilDB, DmiCtrls, DPSControls;

type
  TGrabarMedioPagoForm = class(TForm)
	LIC: TTagReader;
	Label1: TLabel;
	lbl_numerotag: TLabel;
	txt_numerotag: TEdit;
	Image1: TImage;
	Image2: TImage;
	Shape1: TShape;
	Shape2: TShape;
	lbl_ContextMark: TLabel;
    GBListas: TGroupBox;
    CBNegra: TCheckBox;
    CBGris: TCheckBox;
    CBAmarilla: TCheckBox;
    CBVerde: TCheckBox;
    btn_accion: TDPSButton;
    cb_contextMark: TComboBox;
	procedure LICTagConnected(Sender: TObject; LID: Cardinal;
	  AppList: TApplicationList);
	procedure btn_accionClick(Sender: TObject);
	procedure FormKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	FContextMark: Integer;
	Data: TMPData;
	FNumeroTAG: AnsiString;
	EnCodificacionNormal : Boolean;
	function VerificarTagEnMaestro(NumeroTag: AnsiString; ContextMrk: Integer):Boolean;
	function VerificarTagEnAsignados (NumeroTag: AnsiString; ContextMrk: Integer):Boolean;
  public
	{ Public declarations }
	function Inicializa(MPData: TMPData; ConAntena:Boolean; Descripcion: AnsiString;
                        TipoCodificacion: integer = CODIFICACION_ASIGNACION;
                        CodificacionNormal: Boolean = True): Boolean;
    procedure AccionControl;
    procedure AccionAsignacion;
    procedure AccionDevolucion;
	property NumeroTAG: AnsiString Read FNumeroTag;
	property ContextMark: Integer read FContextMark;
  end;

var
  GrabarMedioPagoForm: TGrabarMedioPagoForm;

implementation

uses DMConnection;

{$R *.DFM}

resourcestring
    MSG_NO_EXISTE_TAG = 'El TAG %d-%s no se encuentra en el Maestro de TAGs.';
    MSG_TAG_ASIGNADO  = 'El TAG %d-%s se encuentra actualmente asignado.';
    CAPTION_VALIDAR_TAG = 'Validar TAG';

function TGrabarMedioPagoForm.Inicializa(MPData: TMPData; ConAntena:Boolean;
                                Descripcion: AnsiString;
                                TipoCodificacion: integer = CODIFICACION_ASIGNACION;
                                CodificacionNormal: Boolean = True): Boolean;
resourcestring
    CAPTION_FORM            = 'Codificaci�n de TAGs  -  %s';
    MSG_ESPERA              = 'Inicializando Lectores. Aguarde por favor ...';
    MSG_SOLICITUD           = 'Por Favor, Coloque el TAG Dentro del Dispositivo de Codificaci�n Correspondiente.' +
      ' Si Desea Cancelar la Operaci�n, Presione el Bot�n "Cancelar".';
    MSG_INFORMACION         = 'Este modo no utiliza la antena de codificaci�n de TAGs, ingrese el n�mero de TAG en el campo' +
      ' correspondiente y presione el bot�n "Codificar" o bien ESC para cancelar.';
    MSG_ERROR_INICIALIZAR    = 'Error al Inicializar el Dispositivo de Codificaci�n.';
    CAPTION_ERROR_INICIALIZAR = 'Inicializar Dispositivo de Codificaci�n';
    CAPTION_BOTON_CODIFICAR  = 'Codificar';

begin
    //*** Por ahora no esta terminado el uso de TipoCodificacion, puede
    //variar el uso de la pantalla segun sea para control, asignacion o devolucion
	Data := MPData;
	btn_accion.Tag := 0;
	btn_accion.cancel := true;
	Label1.Caption := MSG_ESPERA;
	Label1.Update;
    { INICIO : 20160315 MGO
	LIC.DLL := ApplicationIni.ReadString('Bibliotecas', 'LIC', LIC.DLL);
	}
    LIC.DLL := InstallIni.ReadString('Bibliotecas', 'LIC', LIC.DLL);
    // FIN : 20160315 MGO
	Self.Caption := Format(CAPTION_FORM,[Descripcion]);
	EnCodificacionNormal := CodificacionNormal;
    CargarContextMarks(DMCOnnections.BaseCAC, cb_ContextMark);
	if ConAntena then begin
		try
			Screen.Cursor := crHourGlass;
			try
				LIC.Active := True;
				Result := True;
				Screen.Cursor := crDefault;
				Label1.Caption := MSG_SOLICITUD;
			except
				on e: Exception do begin
					MsgBoxErr(MSG_ERROR_INICIALIZAR, e.message, CAPTION_ERROR_INICIALIZAR, MB_ICONSTOP);
					Result := false;
                    close;
				end;
			end;
		finally
			Screen.Cursor := crDefault;
		end;
	end else begin
		// No hay Antena
		btn_accion.ModalResult := mrOK;
		Screen.Cursor := crDefault;
		lbl_ContextMark.Visible := true;
		lbl_numerotag.Visible := true;
		txt_numerotag.Visible := true;
		btn_accion.Caption := CAPTION_BOTON_CODIFICAR;
		btn_accion.Tag:= 1;
		btn_accion.Cancel := false;
        ActiveControl := cb_ContextMark;
		Label1.Caption := MSG_INFORMACION;
		Result := true;
	end;
end;

procedure TGrabarMedioPagoForm.LICTagConnected(Sender: TObject; LID: Cardinal;
  AppList: TApplicationList);

	Procedure LiberarTag(LID: DWORD; Ok: Boolean);
	var
		Sonido: Integer;
	begin
		// Liberamos en tag, haci�ndole antes un Beep
		if Ok then Sonido := 0 else Sonido := 2;
		LIC.Ejecutar(LID, 0, 10, @Sonido, SizeOf(Sonido), nil, 0, '');
		LIC.Liberar(LID);
	end;

resourcestring
    MSG_ERROR_CONCESIONARIA = 'El TAG no es de esta Concecionaria.';
    MSG_ERROR_GRABACION     = 'Error al Codificar el TAG.';
    MSG_ERROR_LECTURA       = 'Error al Leer el TAG.';
    CAPTION_CODIFICAR_TAG   = 'Codificar TAG';
Var
	DataTemp: TMPData;
	Elemento: Integer;
	DescriError: AnsiString;
begin
(* Pablo	// El TAG que se acaba de conectar es el que voy a grabar.
	Elemento := EncontrarElementoTag(AppList, FContextMark);
	if Elemento <= 0 then begin
		MsgBox(MSG_ERROR_CONCESIONARIA, CAPTION_VALIDAR_TAG, MB_ICONSTOP);
		LiberarTag(LID, False);
		Exit;
	end;
	// Primero Leemos para sacar el Nro. de Serie del Tag
	if not LeerTag(LIC, LID, Elemento, [], DataTemp, FNumeroTAG, DescriError) then begin
		MsgBoxErr(MSG_ERROR_LECTURA, DescriError, CAPTION_VALIDAR_TAG, MB_ICONSTOP);
		LiberarTag(LID, False);
		FNumeroTAG := '';
		Exit;
	end;

	if EnCodificacionNormal then begin
		// Verificamos  que exista en el maestro..
		if  not VerificarTagEnMaestro(FNumeroTAG, FContextMark) then begin
			LiberarTag(LID, False);
			MsgBox(format(MSG_NO_EXISTE_TAG, [FContextMark, trim(FNumeroTAG)]), CAPTION_VALIDAR_TAG, MB_ICONSTOP);
			FNumeroTAG := '';
			Exit;
		end;

		// Verificamos ademas que el Tag no este asignado
		if  VerificarTagEnAsignados(FNumeroTAG, FContextMark) then begin
			LiberarTag(LID, False);
			MsgBox(format(MSG_TAG_ASIGNADO, [FContextMark, trim(FNumeroTAG)]), CAPTION_VALIDAR_TAG, MB_ICONSTOP);
			FNumeroTAG := '';
			Exit;
		end;

		// Ahora le grabamos los Datos
		if not GrabarTag(LIC, LID, Elemento, [mpStatic, mpReadWrite, mpHistory,
		  mpRefill], Data, DescriError) then begin
			MsgBoxErr(MSG_ERROR_GRABACION ,DescriError, CAPTION_CODIFICAR_TAG, MB_ICONSTOP);
			LiberarTag(LID, False);
			FNumeroTAG :='';
			Exit;
		end;
	end;
	LiberarTag(LID, True);
	// Anduvo Todo Ok
	ModalResult := mrOk;
*)
end;


procedure TGrabarMedioPagoForm.btn_accionClick(Sender: TObject);
begin
    //Separar las acciones posibles, AccionAsignacion, AccionControl, AccionDevolucion
    if (sender as TButton).Tag = 1 then begin
        AccionAsignacion;
    end;
end;

function TGrabarMedioPagoForm.VerificarTagEnAsignados(NumeroTag: AnsiString; ContextMrk: Integer): Boolean;
begin
	//Verificamos que el tag que se va a codificar no este asignado
	Result := (QueryGetValueInt(DMConnections.BaseCac,
		'SELECT 1 FROM TagsAsignados WHERE CONTRACTSERIALNUMBER = ' + NumeroTag +
		' AND CONTEXTMARK = ' + IntToStr(ContextMrk))) = 1;
end;

function TGrabarMedioPagoForm.VerificarTagEnMaestro(NumeroTag: AnsiString; ContextMrk: Integer): Boolean;
begin
	//Verificamos que el tag que se va a codificar exista en el maestro de tags
	Result := (QueryGetValueInt(DMConnections.BaseCac,
		'SELECT 1 FROM MaestroTags WITH(INDEX = PK_MAESTROTAG) WHERE CONTRACTSERIALNUMBER = ' + NumeroTag +
		' AND CONTEXTMARK = ' + IntToStr(ContextMrk))) = 1;
end;
procedure TGrabarMedioPagoForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key = VK_ESCAPE then begin
        self.ModalResult := mrCANCEL;
        Release;
    end;
end;

procedure TGrabarMedioPagoForm.FormCreate(Sender: TObject);
Var
    Rgn: HRgn;
    AltoCaption: Integer;
begin
    AltoCaption := Height - ClientHeight;
    Rgn := CreateRoundRectRgn(2, AltoCaption, Width - 2, Height - 4, 20, 20);
    SetWindowRgn(Handle, Rgn, False);
end;

procedure TGrabarMedioPagoForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TGrabarMedioPagoForm.AccionAsignacion;
resourcestring
    MSG_INGRESAR_CONTEXT_MARK = 'Debe elegir el Context Mark para continuar.';
    MSG_INGRESAR_NUMERO_TAG   = 'Debe ingresar un N�mero de TAG para continuar.';
begin
    if not ValidateControls(
      [cb_ContextMark, txt_numerotag],
      [Trim(cb_contextMark.text) <> '', (txt_numerotag.Text <> '') ],
      CAPTION_VALIDAR_TAG,
      [MSG_INGRESAR_CONTEXT_MARK, MSG_INGRESAR_NUMERO_TAG]) then Exit;

    FContextMark := Ival(Trim(Copy(cb_contextMark.Text,201,100)));
    FNumeroTAG := Trim(txt_numeroTag.Text);

    // Verificamos  que exista en el maestro y que no este asignado
    if  not VerificarTagEnMaestro(FNumeroTAG, FContextMark) then begin
        MsgBox(format(MSG_NO_EXISTE_TAG, [FContextMark, FNumeroTag]), CAPTION_VALIDAR_TAG, MB_ICONSTOP);
        FNumeroTAG := '';
        Exit;
    end;
    if VerificarTagEnAsignados(FNumeroTAG, FContextMark) then begin
        MsgBox(format(MSG_TAG_ASIGNADO, [FContextMark, FNumeroTag]), CAPTION_VALIDAR_TAG, MB_ICONSTOP);
        FNumeroTAG := '';
        Exit;
    end;
	btn_accion.ModalResult := mrOK;
end;


procedure TGrabarMedioPagoForm.AccionControl;
begin

end;

procedure TGrabarMedioPagoForm.AccionDevolucion;
begin

end;

end.
