unit frmIncrementoCuotaArriendo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, DmiCtrls, VariantComboBox, ExtCtrls,
  DB, ADODB, DMConnection, PeaProcs, PeaTypes, UtilProc, Util ;

type
  TIncrementoCuotaArriendoForm = class(TForm)
    btnAgregarCuotas: TButton;
    btnBuscar: TButton;
    btnSalir: TButton;
    cbImpresorasFiscales: TVariantComboBox;
    dblDetalleCuotas: TDBListEx;
    dsDetalleCuotas: TDataSource;
    edCantidadCuotasAgregar: TNumericEdit;
    edNumeroFiscalBoleta: TNumericEdit;
    lblApellido: TLabel;
    lblApellidoTxt: TLabel;
    lblCantCuotasAgrega: TLabel;
    lblCantCuotasTotales: TLabel;
    lblConvenioTxt: TLabel;
    lblCuotasGeneradas: TLabel;
    lblCuotasGeneradasTxt: TLabel;
    lblCuotasTotalTxt: TLabel;
    lblFechaCreacionTxt: TLabel;
    lblFechaEmision: TLabel;
    lblFechaEmisionTxt: TLabel;
    lblFechaVencimiento: TLabel;
    lblFechaVencimientoTxt: TLabel;
    lblImpresoraFiscal: TLabel;
    lblNombre: TLabel;
    lblNombreTxt: TLabel;
    lblNumeroBoleta: TLabel;
    lblNumeroConvenio: TLabel;
    lblRUT: TLabel;
    lblRutTxt: TLabel;
    lblTag: TLabel;
    lblTeleviaTxt: TLabel;
    lblTotalComprobante: TLabel;
    lblTotalComprobanteTxt: TLabel;
    lblUsuario: TLabel;
    lblUsuarioTxt: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    spComprobantesCuotasCrearCuota: TADOStoredProc;
    spObtenerCabeceraComprobanteCuotas: TADOStoredProc;
    spObtenerDetalleComprobanteCuotas: TADOStoredProc;
    spObtenerDetalleComprobanteCuotasIdComprobanteCuota: TLargeintField;
    spObtenerDetalleComprobanteCuotasNumeroCuota: TIntegerField;
    spObtenerDetalleComprobanteCuotasTipoComprobanteCuota: TStringField;
    spObtenerDetalleComprobanteCuotasNumeroComprobanteCuota: TLargeintField;
    spObtenerDetalleComprobanteCuotasFechaEmisionCuota: TDateTimeField;
    spObtenerDetalleComprobanteCuotasFechaVencimientoCuota: TDateTimeField;
    spObtenerDetalleComprobanteCuotasFechaGeneracionCuota: TDateTimeField;
    spObtenerDetalleComprobanteCuotasUsuarioGeneracionCuota: TStringField;
    spObtenerDetalleComprobanteCuotasModoGeneracionCuota: TIntegerField;
    spObtenerDetalleComprobanteCuotasImporteCuota: TStringField;
    spObtenerDetalleComprobanteCuotasFechaHoraCreacion: TDateTimeField;
    spObtenerDetalleComprobanteCuotasUsuarioCreacion: TStringField;
    spObtenerDetalleComprobanteCuotasFechaHoraModificacion: TDateTimeField;
    spObtenerDetalleComprobanteCuotasUsuarioModificacion: TStringField;
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarCuotasClick(Sender: TObject);
  private
    FCantidadCuotasTotales: integer;
    FCantidadCuotasGeneradas: integer;
    FNumeroBoleta: integer;
    FNumeroComprobanteDeuda: integer;
    procedure CargarCabecera;
    procedure CargarDetalle;
    procedure LimpiarDatos;
    { Private declarations }
  public
    { Public declarations }
    function Inicializar:boolean;
  end;

var
  IncrementoCuotaArriendoForm: TIncrementoCuotaArriendoForm;

implementation



{$R *.dfm}

{ TIncrementoCuotaArriendoForm }


function TIncrementoCuotaArriendoForm.Inicializar: boolean;
begin
    Result := True;
    CenterForm(Self);
    LimpiarDatos;
    CargarImpresorasFiscales(DMConnections.BaseCAC, cbImpresorasFiscales, 0);

end;


procedure TIncrementoCuotaArriendoForm.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_BOLETA_NO_ENCONTRADA = 'No se encuentra el comprobante ingresado';
begin
    spObtenerCabeceraComprobanteCuotas.Parameters.ParamByName('@TipoComprobante').Value     := TC_BOLETA;
    spObtenerCabeceraComprobanteCuotas.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := edNumeroFiscalBoleta.ValueInt ;
    spObtenerCabeceraComprobanteCuotas.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := cbImpresorasFiscales.Value;
    spObtenerCabeceraComprobanteCuotas.Open;
    FNumeroBoleta := edNumeroFiscalBoleta.ValueInt ;
    if spObtenerCabeceraComprobanteCuotas.eof then begin
        spObtenerCabeceraComprobanteCuotas.Close;
        MsgBox(MSG_BOLETA_NO_ENCONTRADA, Caption);
        LimpiarDatos;
        Exit;
    end;
    CargarCabecera;
end;

procedure TIncrementoCuotaArriendoForm.btnAgregarCuotasClick(Sender: TObject);
resourceString
    MSG_ERROR_LIMITE_CUOTAS = 'El m�ximo de cuotas a generar es de %d y el m�nimo es 1' ;
    MSG_CONFIRMA_AGREGADO_CUOTAS = 'Confirma agregar %d cuotas a la Boleta n�mero %d ?';
    MSG_ERROR_GENERANDO = 'Se ha producido un error generando las cuotas';
var
    CuotasRemanentes: integer;
    index: integer;
begin
    CuotasRemanentes := FCantidadCuotasTotales - FCantidadCuotasGeneradas;
    if (edCantidadCuotasAgregar.ValueInt > CuotasRemanentes) or (edCantidadCuotasAgregar.ValueInt <= 0 ) then begin
        MsgBoxBalloon(format(MSG_ERROR_LIMITE_CUOTAS, [CuotasRemanentes]), Caption , MB_ICONSTOP, edCantidadCuotasAgregar);
        Exit;
    end;
	if MsgBox(format(MSG_CONFIRMA_AGREGADO_CUOTAS , [edCantidadCuotasAgregar.ValueInt, FNumeroBoleta]), Caption, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try
            for index := 1 to edCantidadCuotasAgregar.ValueInt do
            begin
                DMConnections.BaseCAC.BeginTrans ;
                spComprobantesCuotasCrearCuota.Parameters.ParamByName('@TipoComprobanteDeuda').Value   := TC_COMPROBANTE_DEUDA ;
                spComprobantesCuotasCrearCuota.Parameters.ParamByName('@NumeroComprobanteDeuda').Value := FNumeroComprobanteDeuda;
                spComprobantesCuotasCrearCuota.Parameters.ParamByName('@CrearObligatorio').Value       := 1; //aunque haya otra cuota generada este mes
                spComprobantesCuotasCrearCuota.Parameters.ParamByName('@UsuarioCreacionCuota').Value   := UsuarioSistema ;
                spComprobantesCuotasCrearCuota.Parameters.ParamByName('@FechaEmision').Value           := NowBase(spComprobantesCuotasCrearCuota.Connection) ;

                spComprobantesCuotasCrearCuota.ExecProc;

                DMConnections.BaseCAC.CommitTrans;
            end;
        except
			on E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
				MsgBoxErr(MSG_ERROR_GENERANDO, e.message, Caption, MB_ICONSTOP);
			end;
        end;
        btnBuscarClick(self); //asi se actualiza la grilla con los nuevos datos
    end;
end;




procedure TIncrementoCuotaArriendoForm.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TIncrementoCuotaArriendoForm.CargarCabecera;
begin
    lblApellidoTxt.Caption        := trim(spObtenerCabeceraComprobanteCuotas.FieldByName('Apellido').AsString) + ' ' + trim(spObtenerCabeceraComprobanteCuotas.FieldByName('ApellidoMaterno').AsString);
    lblConvenioTxt.Caption        := spObtenerCabeceraComprobanteCuotas.FieldByName('NumeroConvenio').AsString;
    lblCuotasGeneradasTxt.Caption := spObtenerCabeceraComprobanteCuotas.FieldByName('CantidadCuotasGeneradas').AsString;
    lblCuotasTotalTxt.Caption     := spObtenerCabeceraComprobanteCuotas.FieldByName('CantidadCuotasTotales').AsString;
    lblFechaCreacionTxt.Caption   := DateTimeToStr(spObtenerCabeceraComprobanteCuotas.FieldByName('FechaHoraCreacion').AsDateTime);
    lblFechaEmisionTxt.Caption    := DateTimeToStr(spObtenerCabeceraComprobanteCuotas.FieldByName('FechaEmision').AsDateTime);
    lblFechaVencimientoTxt.Caption:= DateTimeToStr(spObtenerCabeceraComprobanteCuotas.FieldByName('FechaVencimiento').AsDateTime);
    lblNombreTxt.Caption          := trim(spObtenerCabeceraComprobanteCuotas.FieldByName('Nombre').AsString);
    lblRutTxt.Caption             := spObtenerCabeceraComprobanteCuotas.FieldByName('NumeroDocumento').AsString;;
    lblTeleviaTxt.Caption         := spObtenerCabeceraComprobanteCuotas.FieldByName('Etiqueta').AsString;
    lblTotalComprobanteTxt.Caption:= spObtenerCabeceraComprobanteCuotas.FieldByName('TotalComprobante').AsString;
    lblUsuarioTxt.Caption         := spObtenerCabeceraComprobanteCuotas.FieldByName('UsuarioCreacion').AsString;

    FCantidadCuotasTotales      := spObtenerCabeceraComprobanteCuotas.FieldByName('CantidadCuotasTotales').AsInteger;
    FCantidadCuotasGeneradas    := spObtenerCabeceraComprobanteCuotas.FieldByName('CantidadCuotasGeneradas').AsInteger;
    FNumeroComprobanteDeuda     := spObtenerCabeceraComprobanteCuotas.FieldByName('NumeroComprobanteDeuda').AsInteger;



    CargarDetalle;
end;

procedure TIncrementoCuotaArriendoForm.CargarDetalle;
begin
    spObtenerDetalleComprobanteCuotas.Close;
    spObtenerDetalleComprobanteCuotas.Parameters.ParamByName('@IdComprobanteCuota').Value := spObtenerCabeceraComprobanteCuotas.FieldByName('IdComprobanteCuota').AsInteger;
    spObtenerDetalleComprobanteCuotas.Open;
    spObtenerCabeceraComprobanteCuotas.Close;
end;



procedure TIncrementoCuotaArriendoForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TIncrementoCuotaArriendoForm.LimpiarDatos;
begin
    lblApellidoTxt.Caption        := EmptyStr;
    lblConvenioTxt.Caption        := EmptyStr;
    lblCuotasGeneradasTxt.Caption := EmptyStr;
    lblCuotasTotalTxt.Caption     := EmptyStr;
    lblFechaCreacionTxt.Caption   := EmptyStr;
    lblFechaEmisionTxt.Caption    := EmptyStr;
    lblFechaVencimientoTxt.Caption:= EmptyStr;
    lblNombreTxt.Caption          := EmptyStr;
    lblRutTxt.Caption             := EmptyStr;
    lblTeleviaTxt.Caption         := EmptyStr;
    lblTotalComprobanteTxt.Caption:= EmptyStr;
    lblUsuarioTxt.Caption         := EmptyStr;
    edCantidadCuotasAgregar.Clear;
end;


end.
