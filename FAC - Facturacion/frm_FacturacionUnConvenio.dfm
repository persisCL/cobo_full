object frmFacturacionUnConvenio: TfrmFacturacionUnConvenio
  Left = 177
  Top = 114
  Anchors = []
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Facturaci'#243'n de un Convenio Especifico '
  ClientHeight = 569
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    640
    569)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox4: TGroupBox
    Left = 6
    Top = 1
    Width = 627
    Height = 308
    Caption = ' Datos del Comprobante '
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 22
      Width = 78
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object Label3: TLabel
      Left = 8
      Top = 49
      Width = 105
      Height = 13
      Caption = 'Convenios del Cliente:'
    end
    object Label2: TLabel
      Left = 8
      Top = 78
      Width = 76
      Height = 13
      Caption = 'Fecha de Corte:'
    end
    object Label4: TLabel
      Left = 214
      Top = 78
      Width = 87
      Height = 13
      Caption = 'Fecha de Emisi'#243'n:'
    end
    object Label5: TLabel
      Left = 409
      Top = 78
      Width = 109
      Height = 13
      Caption = 'Fecha de Vencimiento:'
    end
    object lMedioPago: TLabel
      Left = 152
      Top = 143
      Width = 72
      Height = 13
      Caption = 'Medio de Pago'
    end
    object Label6: TLabel
      Left = 8
      Top = 143
      Width = 131
      Height = 13
      Caption = 'Medio de Pago Autom'#225'tico:'
    end
    object Bevel1: TBevel
      Left = 4
      Top = 132
      Width = 617
      Height = 18
      Shape = bsTopLine
    end
    object Label7: TLabel
      Left = 8
      Top = 165
      Width = 122
      Height = 13
      Caption = 'Detalle del Comprobante: '
    end
    object Label8: TLabel
      Left = 347
      Top = 19
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object peRUTCliente: TPickEdit
      Left = 117
      Top = 18
      Width = 139
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = True
      TabOrder = 0
      OnChange = peRUTClienteChange
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object deFechaCorte: TDateEdit
      Left = 117
      Top = 74
      Width = 92
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 2
      OnChange = deFechaCorteChange
      OnExit = deFechaCorteExit
      Date = -693594.000000000000000000
    end
    object deFechaEmision: TDateEdit
      Left = 307
      Top = 74
      Width = 92
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 3
      OnChange = deFechaEmisionChange
      OnExit = deFechaEmisionExit
      Date = -693594.000000000000000000
    end
    object deFechaVencimiento: TDateEdit
      Left = 526
      Top = 74
      Width = 92
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 4
      OnExit = deFechaVencimientoExit
      Date = -693594.000000000000000000
    end
    object cbConveniosCliente: TVariantComboBox
      Left = 119
      Top = 45
      Width = 209
      Height = 19
      Style = vcsOwnerDrawFixed
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbConveniosClienteChange
      OnDrawItem = cbConveniosClienteDrawItem
      Items = <>
    end
    object DBListEx1: TDBListEx
      Left = 3
      Top = 182
      Width = 609
      Height = 121
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 56
          Header.Caption = 'Facturar'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Seleccionado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 280
          Header.Caption = 'Descripci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'IMPORTE'
        end>
      DataSource = dsInformeFactura
      DragReorder = True
      ParentColor = False
      PopupMenu = PopFacturacionManual
      TabOrder = 5
      TabStop = True
      OnDblClick = DBListEx1DblClick
      OnDrawText = DBListEx1DrawText
      OnKeyPress = DBListEx1KeyPress
    end
    object NumeroConvenio: TEdit
      Left = 458
      Top = 15
      Width = 160
      Height = 21
      Color = 16444382
      TabOrder = 6
      OnChange = NumeroConvenioChange
    end
    object chkFacturarCompleto: TCheckBox
      Left = 8
      Top = 106
      Width = 264
      Height = 17
      Caption = 'Facturar el convenio completo, sin ajuste sencillo'
      TabOrder = 7
    end
    object chkFacturarSinIntereses: TCheckBox
      Left = 280
      Top = 106
      Width = 169
      Height = 17
      Caption = 'Facturar sin generar intereses'
      TabOrder = 8
    end
  end
  object GroupBox5: TGroupBox
    Left = 6
    Top = 401
    Width = 628
    Height = 120
    Caption = ' Datos del Cliente '
    TabOrder = 1
    object lNombreCompleto: TLabel
      Left = 81
      Top = 24
      Width = 83
      Height = 13
      Caption = 'lNombreCompleto'
    end
    object lDomicilio: TLabel
      Left = 81
      Top = 48
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComuna: TLabel
      Left = 81
      Top = 72
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegion: TLabel
      Left = 81
      Top = 96
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label31: TLabel
      Left = 14
      Top = 24
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Apellido:'
    end
    object Label33: TLabel
      Left = 14
      Top = 48
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label34: TLabel
      Left = 14
      Top = 72
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label35: TLabel
      Left = 14
      Top = 96
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 310
    Width = 629
    Height = 90
    Caption = ' Domicilio del Comprobante '
    TabOrder = 4
    object lDomicilioFacturacion: TLabel
      Left = 81
      Top = 20
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComunaFacturacion: TLabel
      Left = 81
      Top = 44
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegionFacturacion: TLabel
      Left = 81
      Top = 68
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label12: TLabel
      Left = 14
      Top = 20
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label13: TLabel
      Left = 14
      Top = 44
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label14: TLabel
      Left = 14
      Top = 68
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
  end
  object btnSalir: TButton
    Left = 526
    Top = 537
    Width = 102
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btnSalirClick
  end
  object btnFacturar: TButton
    Left = 417
    Top = 537
    Width = 102
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Facturar'
    Default = True
    TabOrder = 2
    OnClick = btnFacturarClick
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 222
    Top = 412
  end
  object ObtenerDatosMedioPagoAutomaticoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerDatosMedioPagoAutomaticoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 254
    Top = 412
  end
  object GenerarMovimientosTransitos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'GenerarMovimientosTransitos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConvenioAnterior'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@GrupoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NoGenerarAjusteSencillo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NoGenerarIntereses'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ConveniosProcesados'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 384
    Top = 424
  end
  object QryObtenerDomicilioFacturacion: TADOQuery
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    Parameters = <
      item
        Name = 'CodigoDomicilio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select '
      '  CodigoPais, '
      '  CodigoRegion,'
      '  CodigoComuna,'
      '  CodigoCalle,'
      '  Numero, '
      '  Detalle,'
      '  CalleDesnormalizada'
      'from '
      '  Domicilios'
      'where '
      '  CodigoDomicilio = :CodigoDomicilio')
    Left = 286
    Top = 412
  end
  object ObtenerUltimosConsumos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'ObtenerUltimosConsumos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 160
    Top = 208
  end
  object dsInformeFactura: TDataSource
    DataSet = cdConceptos
    Left = 128
    Top = 208
  end
  object spObtenerComprobanteFacturado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerComprobanteFacturado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 440
    Top = 320
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 486
    Top = 448
  end
  object tmrConsulta: TTimer
    Enabled = False
    OnTimer = tmrConsultaTimer
    Left = 454
    Top = 390
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 514
    Top = 377
  end
  object spObtenerDatosFacturacionDetallada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerDatosFacturacionDetallada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 98
    Top = 521
  end
  object spCrearProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'CrearProcesoFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FacturacionManual'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 552
    Top = 320
  end
  object cdConceptos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Importe'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 160
    Top = 240
  end
  object ilImages: TImageList
    Left = 128
    Top = 240
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000BABBBC00B4B5B700B5B6B800B5B6B800B5B6B800B5B6B800B5B6B800B5B6
      B800B4B5B700F7F7F80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000525558008F90930093949600939496009091920093949600939496009394
      960051545700F5F5F50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000052555800F5F6F60000000000000000009C9C9B0000000000000000000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E800B6B7B900B7B8B900B7B8
      B9003F424600B0B2B3006E6F70006D6E6F00494A4B007D7E7D00BDBCBC000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CDCDCE0023616700148D9500148D
      95000E868D00148D94000E878E00148C94001C585E00CFD0D100474645000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0000F5
      FF0000F5FF0000F5FF0000F5FF0000F5FF00176D7400616161006F6F6E000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0000F5
      FF0002DDE50002DFE80000F5FF0000F5FF0012686F009A9B9C00000000000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF00089F
      A4000990950009969B0006B5BC0000F5FF00196F7500CACACB00959594000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0004C5
      CC0003D3DB0009969B000A8D910000F5FF00166C7200626262009F9F9E000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0005C2
      CA000E67690007A6AC0001E7F10000F5FF00186E7500CECFD000FEFEFE000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0007A6
      AC0008A3A9000992970007AEB40000F5FF0020767D00CECFD000000000000000
      000054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0000F5
      FF0004CBD30003D4DC0000F2FC0000F5FF001F757B00B7B8BA00E2E2E300E2E2
      E30054565A00F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0000F5
      FF0000F5FF0000F5FF0000F5FF0000F5FF00156B720055575C0066686C006668
      6C0067696C00F0F0F10000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECF0020787F0000F5FF0000F5
      FF0000F5FF0000F5FF0000F5FF0000F5FF0021777E00CECFD000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CDCDCE002C585E00256C7200256C
      7200256C7200256C7200256C7200256C72002C575D00CFD0D100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F1F1F100DBDCDC00DBDCDC00DBDC
      DC00DBDCDC00DBDCDC00DBDCDC00DBDCDC00DBDCDC00F2F2F200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00F003FFFF00000000F003FFFF00000000
      F373FFFF000000000013FFFF000000000013FFFF000000000013FFFF00000000
      0033FFFF000000000013FFFF000000000013FFFF000000000013FFFF00000000
      0033FFFF000000000003FFFF000000000003FFFF00000000003FFFFF00000000
      003FFFFF00000000003FFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object PopFacturacionManual: TPopupMenu
    OnPopup = PopFacturacionManualPopup
    Left = 232
    Top = 256
    object MnuSeleccionarTodo: TMenuItem
      Caption = 'Seleccionar Todo'
      Hint = 'Selecciona todos los conceptos de la grilla'
      OnClick = MnuSeleccionarTodoClick
    end
    object MnuDeSeleccionarTodo: TMenuItem
      Caption = 'Deseleccionar Todo'
      Hint = 'Remueve todas las selecciones de la grilla'
      OnClick = MnuDeSeleccionarTodoClick
    end
  end
  object SpCargarConceptosAFacturarFacturadorManual: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CargarConceptosAFacturarFacturadorManual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 240
    Top = 528
  end
  object SPActualizarFechaHoraFinProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFechaHoraFinProcesoFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 272
    Top = 528
  end
  object spActualizarFechaImpresionComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFechaImpresionComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 344
    Top = 528
  end
  object spGenerarTablaTrabajoInteresesPorFacturar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 0
    ProcedureName = 'GenerarTablaTrabajoInteresesPorFacturar'
    Parameters = <
      item
        Name = '@CodigoGrupoFacturacion'
        DataType = ftSmallint
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@FechaEmision'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        DataType = ftInteger
        Value = Null
      end>
    Left = 345
    Top = 425
  end
  object spLlenarDetalleConsumoComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 0
    ProcedureName = 'LlenarDetalleConsumoComprobantes'
    Parameters = <
      item
        Name = '@NumeroProcesoFacturacion'
        DataType = ftInteger
        Value = Null
      end>
    Left = 377
    Top = 529
  end
end
