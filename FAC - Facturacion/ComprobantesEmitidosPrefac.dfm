object FormComprobantesEmitidosPrefac: TFormComprobantesEmitidosPrefac
  Left = 62
  Top = 76
  Anchors = []
  Caption = 'Comprobantes Emitidos PreFacturaci'#243'n'
  ClientHeight = 705
  ClientWidth = 1217
  Color = clBtnFace
  Constraints.MinHeight = 700
  Constraints.MinWidth = 1230
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlListado: TPanel
    Left = 0
    Top = 242
    Width = 1217
    Height = 463
    Align = alClient
    TabOrder = 0
    object spl1: TSplitter
      Left = 1
      Top = 311
      Width = 1215
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      Beveled = True
      Color = clBtnFace
      ParentColor = False
      ExplicitTop = 307
    end
    object lstComprobantes: TDBListEx
      Left = 1
      Top = 34
      Width = 1215
      Height = 277
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsEstadoHeaderClick
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Motivo exclusi'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'DescriMotivosNoFacturar'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'FechaEmision'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Comprobante'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'DescriComprobante'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'RUT'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'NumeroConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = #218'ltimo Corte'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaUltimoCorte'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Emisi'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'FechaEmision'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Vencimiento'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsHeaderClick
          FieldName = 'FechaVencimiento'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Width = 120
          Header.Caption = 'Total a Pagar'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsImporteHeaderClick
          FieldName = 'DescriAPagar'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Total Comprobante'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          OnHeaderClick = lstComprobantesColumnsImporteMesHeaderClick
          FieldName = 'DescriTotal'
        end>
      DataSource = dsFacturas
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = lstComprobantesDrawText
    end
    object pnlAcciones: TPanel
      Left = 1
      Top = 1
      Width = 1215
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        1215
        33)
      object lbl4: TLabel
        Left = 12
        Top = 12
        Width = 130
        Height = 13
        Caption = 'Lista de Comprobantes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblResultado: TLabel
        Left = 154
        Top = 12
        Width = 48
        Height = 13
        Caption = 'Resultado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object shp1: TShape
        Left = 284
        Top = 12
        Width = 11
        Height = 13
        Hint = 
          'Se generar'#225' en facturaci'#243'n por cumplir con todos los par'#225'metros ' +
          'configurados'
        Anchors = [akTop, akRight]
        Brush.Color = clBlack
        ParentShowHint = False
        ShowHint = True
      end
      object lbl19: TLabel
        Left = 301
        Top = 12
        Width = 50
        Height = 13
        Hint = 
          'Se generar'#225' en facturaci'#243'n por cumplir con todos los par'#225'metros ' +
          'configurados'
        Anchors = [akTop, akRight]
        Caption = 'Facturable'
        ParentShowHint = False
        ShowHint = True
      end
      object shp3: TShape
        Left = 363
        Top = 12
        Width = 11
        Height = 13
        Hint = 
          'NO se generar'#225' en facturaci'#243'n por NO cumplir con los par'#225'metros ' +
          'configurados'
        Anchors = [akTop, akRight]
        Brush.Color = clRed
        ParentShowHint = False
        ShowHint = True
      end
      object lbl21: TLabel
        Left = 380
        Top = 12
        Width = 124
        Height = 13
        Hint = 
          'NO se generar'#225' en facturaci'#243'n por NO cumplir con los par'#225'metros ' +
          'configurados'
        Anchors = [akTop, akRight]
        Caption = 'Excluido autom'#225'ticamente'
        ParentShowHint = False
        ShowHint = True
      end
      object shp4: TShape
        Left = 518
        Top = 12
        Width = 11
        Height = 13
        Hint = 
          'NO se generar'#225' en facturaci'#243'n por haber sido APARTADO manualment' +
          'e, pero cumple con los par'#225'metros configurados'
        Anchors = [akTop, akRight]
        Brush.Color = clMaroon
        ParentShowHint = False
        ShowHint = True
      end
      object lbl22: TLabel
        Left = 535
        Top = 12
        Width = 106
        Height = 13
        Hint = 
          'NO se generar'#225' en facturaci'#243'n por haber sido APARTADO manualment' +
          'e, pero cumple con los par'#225'metros configurados'
        Anchors = [akTop, akRight]
        Caption = 'Excluido manualmente'
        ParentShowHint = False
        ShowHint = True
      end
      object shp2: TShape
        Left = 655
        Top = 12
        Width = 11
        Height = 13
        Hint = 
          'Se generar'#225' en facturaci'#243'n por haber sido INCLUIDO manualmente, ' +
          'pero NO cumple con los par'#225'metros configurados'
        Anchors = [akTop, akRight]
        Brush.Color = clBlue
        ParentShowHint = False
        ShowHint = True
      end
      object lbl20: TLabel
        Left = 672
        Top = 12
        Width = 103
        Height = 13
        Hint = 
          'Se generar'#225' en facturaci'#243'n por haber sido INCLUIDO manualmente, ' +
          'pero NO cumple con los par'#225'metros configurados'
        Anchors = [akTop, akRight]
        Caption = 'Incluido manualmente'
        ParentShowHint = False
        ShowHint = True
      end
      object btnIncluirApartar: TButton
        Left = 781
        Top = 4
        Width = 83
        Height = 25
        Align = alCustom
        Anchors = [akTop, akRight]
        Caption = 'Excluir'
        TabOrder = 0
        OnClick = btnIncluirApartarClick
      end
      object btnIncluirMasivo: TButton
        Left = 870
        Top = 4
        Width = 84
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Incluir Masivo'
        TabOrder = 1
        OnClick = btnIncluirMasivoClick
      end
      object btnApartarMasivo: TButton
        Left = 960
        Top = 4
        Width = 91
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Excluir Masivo'
        TabOrder = 2
        OnClick = btnApartarMasivoClick
      end
      object btnLimpiar: TButton
        Left = 1138
        Top = 4
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Restablecer'
        TabOrder = 4
        OnClick = btnLimpiarClick
      end
      object btnImportar: TButton
        Left = 1057
        Top = 4
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Importar...'
        TabOrder = 3
        OnClick = btnImportarClick
      end
    end
    object pnlDetalle: TPanel
      Left = 1
      Top = 316
      Width = 1215
      Height = 146
      Align = alBottom
      TabOrder = 2
      DesignSize = (
        1215
        146)
      object pgcDetalles: TPageControl
        Left = 0
        Top = -1
        Width = 1213
        Height = 146
        ActivePage = tsTransitos
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object tsDetalleComprobante: TTabSheet
          Caption = '&Detalle Comprobante'
          object lstDetalle: TDBListEx
            Left = 0
            Top = 0
            Width = 1205
            Height = 118
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 250
                Header.Caption = 'Concepto del Cargo'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Descripcion'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 90
                Header.Caption = 'Monto'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'Importe'
              end>
            DataSource = dsDetalle
            DragReorder = True
            ParentColor = False
            TabOrder = 0
            TabStop = True
            OnDrawText = lstDetalleDrawText
          end
        end
        object tsTransitos: TTabSheet
          Caption = '&Tr'#225'nsitos'
          ImageIndex = 1
          object lstTransitos: TDBListEx
            Left = 0
            Top = 27
            Width = 1205
            Height = 91
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 120
                Header.Caption = 'Fecha / Hora'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'FechaHora'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 350
                Header.Caption = 'Punto de Cobro'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DescriPtoCobro'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Patente'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Patente'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Categor'#237'a'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DescriCategoria'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 100
                Header.Caption = 'TAG'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Etiqueta'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 100
                Header.Caption = 'Tarifa'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'TipoHorarioDesc'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Width = 100
                Header.Caption = 'Monto'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DescriImporte'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = [fsUnderline]
                Width = 60
                Header.Caption = 'Imagen'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = True
                FieldName = 'Imagen'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 80
                Header.Caption = 'Anulado'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'TransitoAnulado'
              end>
            DataSource = dsTransitosPorComprobante
            DragReorder = True
            ParentColor = False
            TabOrder = 0
            TabStop = True
            OnCheckLink = lstTransitosCheckLink
            OnDrawText = lstTransitosDrawText
            OnLinkClick = lstTransitosLinkClick
          end
          object pnl1: TPanel
            Left = 0
            Top = 0
            Width = 1205
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            DesignSize = (
              1205
              27)
            object lbl_TransitosDesde: TLabel
              Left = 9
              Top = 6
              Width = 34
              Height = 13
              Caption = 'Desde:'
            end
            object lbl_TransitosHasta: TLabel
              Left = 145
              Top = 6
              Width = 31
              Height = 13
              Caption = 'Hasta:'
            end
            object lbl_Patente: TLabel
              Left = 377
              Top = 6
              Width = 40
              Height = 13
              Caption = 'Patente:'
            end
            object lblTransitosFechasRestablecer: TLabel
              Left = 278
              Top = 6
              Width = 93
              Height = 13
              Cursor = crHandPoint
              Hint = 'Haga Click ac'#225' para reestablecer las fechas por defecto'
              Caption = 'Fechas por defecto'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsUnderline]
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              OnClick = lblTransitosFechasRestablecerClick
            end
            object lblTransitosCSV: TLabel
              Left = 1046
              Top = 6
              Width = 63
              Height = 13
              Cursor = crHandPoint
              Hint = 'Exportar esta consulta a formato CSV'
              Anchors = [akTop, akRight]
              Caption = 'Exportar CSV'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHotLight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsUnderline]
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              OnClick = lblTransitosCSVClick
            end
            object btnTransitosFiltrar: TButton
              Left = 1126
              Top = 1
              Width = 75
              Height = 24
              Anchors = [akTop, akRight]
              Caption = 'Filtrar'
              Default = True
              TabOrder = 3
              OnClick = btnTransitosFiltrarClick
            end
            object date_TransitosDesde: TDateEdit
              Left = 49
              Top = 3
              Width = 90
              Height = 21
              AutoSelect = False
              TabOrder = 0
              Date = -693594.000000000000000000
            end
            object date_TransitosHasta: TDateEdit
              Left = 182
              Top = 3
              Width = 90
              Height = 21
              AutoSelect = False
              TabOrder = 1
              Date = -693594.000000000000000000
            end
            object edtTransitosPatente: TEdit
              Left = 423
              Top = 3
              Width = 81
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 10
              TabOrder = 2
            end
          end
        end
      end
      object chkMostrarDetalle: TCheckBox
        Left = 183
        Top = 1
        Width = 308
        Height = 17
        Caption = 'Mostrar detalle de comprobantes (relentiza la navegaci'#243'n)'
        TabOrder = 1
        OnClick = chkMostrarDetalleClick
      end
    end
  end
  object clpnlProceso: TCollapsablePanel
    Left = 0
    Top = 0
    Width = 1217
    Height = 242
    Align = alTop
    Animated = False
    Direction = cpdDown
    Caption = 'Proceso de Pre-Facturaci'#243'n N'#176' %d'
    BarColorStart = 14071199
    BarColorEnd = 10646097
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    InternalSize = 219
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    DesignSize = (
      1217
      242)
    object gbCliente: TGroupBox
      Left = 2
      Top = 116
      Width = 599
      Height = 101
      Caption = ' Por Cliente / Cuenta '
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 78
        Height = 13
        Caption = 'N'#250'mero de RUT'
      end
      object Label3: TLabel
        Left = 209
        Top = 17
        Width = 45
        Height = 13
        Caption = 'Convenio'
      end
      object lblverConvenio: TLabel
        Left = 427
        Top = 35
        Width = 73
        Height = 13
        Cursor = crHandPoint
        Caption = 'Ver Convenio...'
        Color = clBtnFace
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentColor = False
        ParentFont = False
        Transparent = True
        OnClick = lblverConvenioClick
      end
      object lblDomicilio: TLabel
        Left = 71
        Top = 78
        Width = 74
        Height = 13
        Caption = 'DomicilioCliente'
      end
      object lbl3: TLabel
        Left = 9
        Top = 78
        Width = 56
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblConcesionaria: TLabel
        Left = 522
        Top = 10
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concesionaria'
      end
      object lblConvenio: TLabel
        Left = 72
        Top = 59
        Width = 100
        Height = 13
        Caption = 'N'#250'mero de Convenio'
      end
      object lbl5: TLabel
        Left = 8
        Top = 59
        Width = 58
        Height = 13
        Caption = 'Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl6: TLabel
        Left = 208
        Top = 59
        Width = 48
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNombre: TLabel
        Left = 273
        Top = 59
        Width = 72
        Height = 13
        Caption = 'Apellido Cliente'
      end
      object peNumeroDocumento: TPickEdit
        Left = 8
        Top = 32
        Width = 192
        Height = 21
        CharCase = ecUpperCase
        Enabled = True
        TabOrder = 0
        OnChange = peNumeroDocumentoChange
        EditorStyle = bteTextEdit
        OnButtonClick = peNumeroDocumentoButtonClick
      end
      object cbConvenios: TVariantComboBox
        Left = 208
        Top = 32
        Width = 203
        Height = 19
        Style = vcsOwnerDrawFixed
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbConveniosChange
        OnDrawItem = cbConveniosDrawItem
        Items = <>
      end
    end
    object grpProceso: TGroupBox
      Left = 1
      Top = 0
      Width = 600
      Height = 117
      Caption = 'Proceso de Pre-Facturaci'#243'n'
      TabOrder = 2
      object lbl7: TLabel
        Left = 8
        Top = 17
        Width = 116
        Height = 13
        Caption = 'N'#250'mero de Proceso:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNumeroProcesoFacturacion: TLabel
        Left = 130
        Top = 17
        Width = 142
        Height = 13
        Caption = 'lblNumeroProcesoFacturacion'
      end
      object lbl8: TLabel
        Left = 8
        Top = 36
        Width = 36
        Height = 13
        Caption = 'Inicio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl9: TLabel
        Left = 167
        Top = 36
        Width = 22
        Height = 13
        Caption = 'Fin:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblFechaHoraInicio: TLabel
        Left = 50
        Top = 36
        Width = 88
        Height = 13
        Caption = 'lblFechaHoraInicio'
      end
      object lblFechaHoraFin: TLabel
        Left = 195
        Top = 36
        Width = 77
        Height = 13
        Caption = 'lblFechaHoraFin'
      end
      object lbl18: TLabel
        Left = 8
        Top = 93
        Width = 57
        Height = 13
        Caption = 'Operador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblOperador: TLabel
        Left = 71
        Top = 93
        Width = 54
        Height = 13
        Caption = 'lblOperador'
      end
      object lbl23: TLabel
        Left = 320
        Top = 36
        Width = 110
        Height = 13
        Caption = 'Cantidad Incluidos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl24: TLabel
        Left = 320
        Top = 55
        Width = 113
        Height = 13
        Caption = 'Cantidad Excluidos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl25: TLabel
        Left = 320
        Top = 74
        Width = 95
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'Monto Incluidos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
      end
      object lbl26: TLabel
        Left = 320
        Top = 93
        Width = 98
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'Monto Excluidos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
      end
      object lbl27: TLabel
        Left = 320
        Top = 17
        Width = 85
        Height = 13
        Caption = 'Comprobantes:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComprobantes: TLabel
        Left = 439
        Top = 17
        Width = 78
        Height = 13
        Caption = 'lblComprobantes'
      end
      object lblIncluidos: TLabel
        Left = 439
        Top = 36
        Width = 52
        Height = 13
        Caption = 'lblIncluidos'
      end
      object lblExcluidos: TLabel
        Left = 439
        Top = 55
        Width = 55
        Height = 13
        Caption = 'lblExcluidos'
      end
      object lblMontoIncluidos: TLabel
        Left = 439
        Top = 74
        Width = 82
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'lblMontoIncluidos'
        ParentShowHint = False
        ShowHint = True
      end
      object lblMontoExcluidos: TLabel
        Left = 439
        Top = 93
        Width = 85
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'lblMontoExcluidos'
        ParentShowHint = False
        ShowHint = True
      end
      object lbl28: TLabel
        Left = 8
        Top = 55
        Width = 39
        Height = 13
        Caption = 'Grupo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl29: TLabel
        Left = 8
        Top = 74
        Width = 33
        Height = 13
        Caption = 'Ciclo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblGrupoFacturacionDescri: TLabel
        Left = 50
        Top = 55
        Width = 125
        Height = 13
        Caption = 'lblGrupoFacturacionDescri'
      end
      object lblCicloAno: TLabel
        Left = 50
        Top = 74
        Width = 52
        Height = 13
        Caption = 'lblCicloAno'
      end
    end
    object grpParams: TGroupBox
      Left = 607
      Top = 0
      Width = 610
      Height = 117
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Par'#225'metros Utilizados'
      TabOrder = 3
      object lbl14: TLabel
        Left = 326
        Top = 17
        Width = 101
        Height = 13
        Caption = 'Meses Forzar NK:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl17: TLabel
        Left = 326
        Top = 36
        Width = 116
        Height = 13
        Caption = 'D'#237'as M'#237'n. entre NK:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl15: TLabel
        Left = 326
        Top = 74
        Width = 54
        Height = 13
        Caption = 'Valor UF:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl16: TLabel
        Left = 326
        Top = 55
        Width = 58
        Height = 13
        Caption = 'Valor IVA:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValIVA: TLabel
        Left = 448
        Top = 55
        Width = 42
        Height = 13
        Caption = 'lblValIVA'
      end
      object lblValUF: TLabel
        Left = 448
        Top = 74
        Width = 39
        Height = 13
        Caption = 'lblValUF'
      end
      object lblDiasMinEmiFac: TLabel
        Left = 448
        Top = 36
        Width = 83
        Height = 13
        Caption = 'lblDiasMinEmiFac'
      end
      object lblMesForGenNK: TLabel
        Left = 448
        Top = 17
        Width = 80
        Height = 13
        Caption = 'lblMesForGenNK'
      end
      object lbl10: TLabel
        Left = 11
        Top = 17
        Width = 85
        Height = 13
        Caption = 'Valor M'#237'n. NK:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinNK: TLabel
        Left = 202
        Top = 17
        Width = 57
        Height = 13
        Caption = 'lblValMinNK'
      end
      object lbl13: TLabel
        Left = 11
        Top = 36
        Width = 185
        Height = 13
        Caption = 'Valor M'#237'n. NK Pago Autom'#225'tico:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinNKPagAut: TLabel
        Left = 202
        Top = 36
        Width = 92
        Height = 13
        Caption = 'lblValMinNKPagAut'
      end
      object lblValMinDeuConv: TLabel
        Left = 202
        Top = 55
        Width = 87
        Height = 13
        Caption = 'lblValMinDeuConv'
      end
      object lbl11: TLabel
        Left = 11
        Top = 55
        Width = 162
        Height = 13
        Caption = 'Valor M'#237'n. Deuda Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinNKBaja: TLabel
        Left = 202
        Top = 74
        Width = 78
        Height = 13
        Caption = 'lblValMinNKBaja'
      end
      object lbl12: TLabel
        Left = 11
        Top = 74
        Width = 171
        Height = 13
        Caption = 'Valor M'#237'n. NK Convenio Baja:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl30: TLabel
        Left = 11
        Top = 93
        Width = 151
        Height = 13
        Caption = 'Valor M'#237'n. Importe Afecto:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValMinImpAfe: TLabel
        Left = 202
        Top = 93
        Width = 75
        Height = 13
        Caption = 'lblValMinImpAfe'
      end
    end
    object grpFiltros: TGroupBox
      Left = 607
      Top = 116
      Width = 610
      Height = 101
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Filtros'
      TabOrder = 4
      DesignSize = (
        610
        101)
      object lblImporteDesde: TLabel
        Left = 129
        Top = 16
        Width = 64
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'Monto Desde'
        ParentShowHint = False
        ShowHint = True
      end
      object lblImporteHasta: TLabel
        Left = 244
        Top = 16
        Width = 61
        Height = 13
        Hint = 'Sobre Total a Pagar'
        Caption = 'Monto Hasta'
        ParentShowHint = False
        ShowHint = True
      end
      object lbl1: TLabel
        Left = 9
        Top = 16
        Width = 81
        Height = 13
        Caption = 'Muestra aleatoria'
      end
      object lbl2: TLabel
        Left = 244
        Top = 59
        Width = 94
        Height = 13
        Caption = 'Motivo de exclusi'#243'n'
      end
      object lblExportarCSV: TLabel
        Left = 533
        Top = 10
        Width = 63
        Height = 13
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = 'Exportar CSV'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblExportarCSVClick
      end
      object BTNBuscarComprobante: TButton
        Left = 533
        Top = 73
        Width = 66
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Buscar'
        Default = True
        TabOrder = 8
        OnClick = BTNBuscarComprobanteClick
      end
      object txtImporteDesde: TNumericEdit
        Left = 129
        Top = 32
        Width = 96
        Height = 21
        TabOrder = 1
      end
      object txtImporteHasta: TNumericEdit
        Left = 243
        Top = 32
        Width = 100
        Height = 21
        TabOrder = 2
      end
      object chkExcluidosAuto: TCheckBox
        Left = 9
        Top = 77
        Width = 97
        Height = 17
        Caption = 'Excluidos auto.'
        Checked = True
        State = cbChecked
        TabOrder = 5
        OnClick = chkExcluidosAutoClick
      end
      object txtMuestraAleatoria: TNumericEdit
        Left = 9
        Top = 32
        Width = 88
        Height = 21
        TabOrder = 0
      end
      object vcbMotivosNoFacturar: TVariantComboBox
        Left = 244
        Top = 74
        Width = 217
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 7
        Items = <>
      end
      object chkFacturables: TCheckBox
        Left = 9
        Top = 58
        Width = 97
        Height = 17
        Caption = 'Facturables'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
      object chkIncluidos: TCheckBox
        Left = 129
        Top = 58
        Width = 97
        Height = 17
        Caption = 'Incluidos manual'
        Checked = True
        State = cbChecked
        TabOrder = 4
      end
      object chkExcluidosManual: TCheckBox
        Left = 129
        Top = 77
        Width = 106
        Height = 17
        Caption = 'Excluidos manual'
        Checked = True
        State = cbChecked
        TabOrder = 6
        OnClick = chkExcluidosAutoClick
      end
    end
  end
  object dsFacturas: TDataSource
    DataSet = spObtenerListaComprobantes
    OnDataChange = dsFacturasDataChange
    Left = 456
    Top = 384
  end
  object spObtenerListaComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'PREFAC_ObtenerListaComprobantes'
    Parameters = <>
    Left = 456
    Top = 328
  end
  object Detalle: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Importe'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 232
    Top = 642
  end
  object dsDetalle: TDataSource
    DataSet = Detalle
    Left = 272
    Top = 650
  end
  object spObtenerDetalleConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'PREFAC_ObtenerDetalleConceptosComprobante'
    Parameters = <>
    Left = 104
    Top = 634
  end
  object tmrConsultaRUT: TTimer
    Enabled = False
    OnTimer = tmrConsultaRUTTimer
    Left = 542
    Top = 158
  end
  object spPREFAC_ProcesosFacturacion_SELECT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'PREFAC_ProcesosFacturacion_SELECT'
    Parameters = <>
    Left = 154
    Top = 329
  end
  object dlgSaveCSV: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 'Archivos Comma Separated Value|*.csv|Todos los Archivos|*.*'
    InitialDir = '.'
    Title = 'Guardar Como...'
    Left = 1064
    Top = 128
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 418
    Top = 169
  end
  object dsTransitosPorComprobante: TDataSource
    DataSet = spObtenerTransitosPorComprobante
    Left = 624
    Top = 634
  end
  object spObtenerTransitosPorComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'PREFAC_ObtenerTransitosPorComprobante'
    Parameters = <>
    Left = 720
    Top = 650
  end
end
