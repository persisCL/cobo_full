object frmSeleccionarPreFacturacionImportar: TfrmSeleccionarPreFacturacionImportar
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Seleccionar Pre-Facturaci'#243'n a Importar'
  ClientHeight = 281
  ClientWidth = 666
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lstProcesos: TDBListEx
    Left = 0
    Top = 29
    Width = 666
    Height = 214
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'C'#243'digo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroProcesoFacturacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'Ciclo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Ciclo'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'A'#241'o'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Ano'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Comprobantes'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Comprobantes'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Excluidos Manuales'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ExcluidosManuales'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Incluidos Manuales'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'IncluidosManuales'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha Hora'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraInicio'
      end>
    DataSource = dsProcesos
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    ExplicitTop = 0
    ExplicitHeight = 373
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 666
    Height = 29
    Align = alTop
    TabOrder = 1
    object lbl1: TLabel
      Left = 8
      Top = 8
      Width = 538
      Height = 13
      Caption = 
        'Seleccione el proceso de Pre-Facturaci'#243'n del cual desea importar' +
        ' la selecci'#243'n de Convenios excluidos e incluidos:'
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 243
    Width = 666
    Height = 38
    Align = alBottom
    TabOrder = 2
    object btnCancelar: TButton
      Left = 584
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
    object btnAceptar: TButton
      Left = 503
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      Default = True
      TabOrder = 1
      OnClick = btnAceptarClick
    end
  end
  object spPREFAC_ProcesosFacturacion_SELECT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PREFAC_ProcesosFacturacion_SELECT'
    Parameters = <>
    Left = 216
    Top = 104
  end
  object dsProcesos: TDataSource
    DataSet = spPREFAC_ProcesosFacturacion_SELECT
    Left = 392
    Top = 120
  end
end
