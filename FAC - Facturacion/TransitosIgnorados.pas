unit TransitosIgnorados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, DbList, Db, DBTables, ComCtrls, Util, UtilProc, UtilDB,
  PeaTypes, PeaProcs, DMConnection, ADODB, uImagenExt, ImgTypes, 
  ImgProcs, ImagePlus, DPSControls;

type
  TFrmTransitosIgnorados = class(TForm)
    Lista: TDBList;
    Panel3: TPanel;
    Panel4: TPanel;
	spTotal: TStatusBar;
    Panel7: TPanel;
    Label1: TLabel;
    cbValidador: TComboBox;
    Panel5: TPanel;
    Panel6: TPanel;
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
	Panel1: TPanel;
    ObtenerUsuariosConPermiso: TADOStoredProc;
    ObtenerTransitosIgnorados: TADOStoredProc;
    Imagen: TImagePlus;
    procedure cbValidadorChange(Sender: TObject);
    procedure ListaClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure ListaRefresh(Sender: TObject);
    procedure ListaDblClick(Sender: TObject);
    procedure ImagenPictureChange(Sender: TObject);
  private
	{ Private declarations } 
	FRevalida: Boolean;
	FNumeroTransito, FPuntoCobro: LongInt;
	FConcesionaria: Byte;
  public
    { Public declarations }
	function Inicializa(FUsuarioRevalida: Boolean): Boolean;
	property Concesionaria:Byte read FConcesionaria;
	property PuntoCobro:LongInt read FPuntoCobro;
    property NumeroTransito:LongInt read FNumeroTransito;
  end;

var
  FrmTransitosIgnorados: TFrmTransitosIgnorados;

implementation

{$R *.DFM}

function TFrmTransitosIgnorados.Inicializa(FUsuarioRevalida: Boolean): Boolean;
resourceString
	MSG_VALIDADOR = 'Cualquier Validador';
begin
	Result := False;
	FRevalida     := FUsuarioRevalida;
	// Cargar los usuarios que podr�an haber ignorado alg�n tr�nsito (permiso de validar)
	cbValidador.Items.Clear;
	cbValidador.Items.Add(MSG_VALIDADOR);
	ObtenerUsuariosConPermiso.Parameters.ParamByName('@CodigoSistema').Value := SYS_VALIDACION;
	ObtenerUsuariosConPermiso.Parameters.ParamByName('@Funcion').Value       := 'mnu_validar';
	if not OpenTables([ObtenerUsuariosConPermiso]) then Exit;
	while not ObtenerUsuariosConPermiso.EOF do begin
		cbValidador.Items.Add(
		  PadR(ObtenerUsuariosConPermiso.FieldByName('Nombre').AsString, 100, ' ')
		  + ObtenerUsuariosConPermiso.FieldByName('CodigoUsuario').AsString
		);
		ObtenerUsuariosConPermiso.Next;
	end;
	ObtenerUsuariosConPermiso.Close;
	cbValidador.ItemIndex := 0;
	cbValidadorChange(nil);
	Result := True;
end;

procedure TFrmTransitosIgnorados.cbValidadorChange(Sender: TObject);
begin
    try
    	ObtenerTransitosIgnorados.Close;
		if cbValidador.ItemIndex = 0 then
			ObtenerTransitosIgnorados.Parameters.ParamByName('@Validador').Value := ''
		else begin
			ObtenerTransitosIgnorados.Parameters.ParamByName('@Validador').Value :=
			  Trim(Copy(cbValidador.Text, 101, 20));
		end;
		ObtenerTransitosIgnorados.Parameters.ParamByName('@IncluirReValidacion').Value := FRevalida;
        if not OpenTables([ObtenerTransitosIgnorados]) then Exit;
    except
    	on e: Exception do begin
	        MsgBox('Error: ' + e.Message, 'Error', MB_OK);
        end;
    end;
    Lista.Reload;
end;

procedure TFrmTransitosIgnorados.ListaClick(Sender: TObject);
var
	i: TTipoImagen; 
	Error: TTipoErrorImg;
	ImagenActual: TBitmap;
	DescriError: AnsiString;
	Concesionaria, PuntoCobro, NumeroTransito: Integer;
begin
	// Cargamos la foto
	ImagenActual   := TBitmap.Create;
	Concesionaria  := ObtenerTransitosIgnorados.FieldByName('CodigoConcesionaria').AsInteger;
	PuntoCobro     := ObtenerTransitosIgnorados.FieldByName('NumeroPuntoCobro').AsInteger;
	NumeroTransito := ObtenerTransitosIgnorados.FieldByName('NumeroTransito').AsInteger;
	for i := Low(TTipoImagen) to High(TTipoImagen) do begin
		if (i <> tiDesconocida) and ObtenerImagenTransitoPendiente(DMConnections.BaseCOP, Concesionaria, PuntoCobro, NumeroTransito, 
		  i, ImagenActual, DescriError, Error) then begin
			Imagen.Picture.Assign(ImagenActual);
			Break;
		end else begin
    		Imagen.Picture := nil;
		end;
    end;
    ImagenActual.free;
end;

procedure TFrmTransitosIgnorados.btnAceptarClick(Sender: TObject);
begin
	// Valores a retornar
	FConcesionaria 	:= ObtenerTransitosIgnorados.FieldByName('CodigoConcesionaria').AsInteger;
	FPuntoCobro 	:= ObtenerTransitosIgnorados.FieldByName('NumeroPuntoCobro').AsInteger;
    FNumeroTransito := ObtenerTransitosIgnorados.FieldByName('NumeroTransito').AsInteger;
end;

procedure TFrmTransitosIgnorados.ListaDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
resourceString
	MSG_SI = '  Si'; 
	MSG_NO = '  No';
begin
	with sender.Canvas, Tabla do begin
		FillRect(Rect);
		Inc(Rect.Top);
		TextOut(Cols[0], Rect.Top, ' ' 
		  + FieldByName('NumeroPuntoCobro').AsString + ' - '
		  + FieldByName('DescriPuntoCobro').AsString);
		TextOut(Cols[1], Rect.Top, ' ' 
		  + (FormatShortDate(FieldByName('FechaHora').AsDateTime) + ' '
		  + FormatTime(FieldByName('FechaHora').AsDateTime)));
		TextOut(Cols[2], Rect.Top, ' ' 
		  + FieldByName('ContextMark').AsString
		  + ' - ' + FieldByName('ContractSerialNumber').AsString);
		TextOut(Cols[3], Rect.Top, ' ' 
		  + FieldByName('CantidadIgnorado').AsString);
		if FieldByName('Estado').AsInteger = 6 then 
		  TextOut(Cols[4], Rect.Top, MSG_SI)
		else TextOut(Cols[4], Rect.Top, MSG_NO);
    end;
end;

procedure TFrmTransitosIgnorados.ListaRefresh(Sender: TObject);
resourceString
	MSG_CANTIDAD = 'Total:  %s  tr�nsito(s).';
begin
	spTotal.SimpleText := Format(MSG_CANTIDAD, [IntToStr(ObtenerTransitosIgnorados.RecordCount)]);
end;

procedure TFrmTransitosIgnorados.ListaDblClick(Sender: TObject);
begin
	btnAceptar.Click();
end;

procedure TFrmTransitosIgnorados.ImagenPictureChange(Sender: TObject);
begin
    TImageExt(Sender).visible := (TImageExt(Sender).Picture.Bitmap.Width > 0);
end;

end.
