object FormComprobantesEmitidos: TFormComprobantesEmitidos
  Left = 62
  Top = 76
  Anchors = []
  Caption = 'Comprobantes Emitidos'
  ClientHeight = 535
  ClientWidth = 1115
  Color = clBtnFace
  Constraints.MinHeight = 562
  Constraints.MinWidth = 860
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    1115
    535)
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 6
    Top = 186
    Width = 108
    Height = 13
    Caption = 'Lista de Comprobantes'
  end
  object gbDatosCliente: TGroupBox
    Left = 4
    Top = 64
    Width = 1151
    Height = 109
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos del Cliente'
    TabOrder = 2
    DesignSize = (
      1151
      109)
    object Label6: TLabel
      Left = 17
      Top = 70
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 144
      Top = 70
      Width = 72
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Apellido Cliente'
    end
    object lblDomicilio: TLabel
      Left = 144
      Top = 89
      Width = 74
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 17
      Top = 89
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 17
      Top = 16
      Width = 123
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblConvenio: TLabel
      Left = 144
      Top = 16
      Width = 100
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'N'#250'mero de Convenio'
    end
    object lblRUT: TLabel
      Left = 144
      Top = 52
      Width = 72
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Numero de Rut'
    end
    object Label11: TLabel
      Left = 17
      Top = 52
      Width = 96
      Height = 13
      Caption = 'N'#250'mero de RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblverConvenio: TLabel
      Left = 309
      Top = 16
      Width = 73
      Height = 13
      Cursor = crHandPoint
      Caption = 'Ver Convenio...'
      Color = clBtnFace
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentColor = False
      ParentFont = False
      Transparent = True
      OnClick = lblverConvenioClick
    end
    object Label8: TLabel
      Left = 17
      Top = 34
      Width = 85
      Height = 13
      Caption = 'Concesionaria:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblConcesionaria: TLabel
      Left = 144
      Top = 34
      Width = 67
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Concesionaria'
    end
  end
  object gbCliente: TGroupBox
    Left = 4
    Top = 2
    Width = 421
    Height = 56
    Caption = ' Por Cliente / Cuenta '
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 16
      Width = 78
      Height = 13
      Caption = 'N'#250'mero de RUT'
    end
    object Label3: TLabel
      Left = 209
      Top = 17
      Width = 45
      Height = 13
      Caption = 'Convenio'
    end
    object peNumeroDocumento: TPickEdit
      Left = 10
      Top = 29
      Width = 192
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnChange = peNumeroDocumentoChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenios: TVariantComboBox
      Left = 208
      Top = 30
      Width = 203
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbConveniosChange
      OnDrawItem = cbConveniosDrawItem
      Items = <
        item
          Caption = 'Todos los Convenios'
          Value = 0
        end>
    end
  end
  object gbComprobante: TGroupBox
    Left = 429
    Top = 2
    Width = 718
    Height = 56
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Por Comprobante '
    TabOrder = 1
    DesignSize = (
      718
      56)
    object Label5: TLabel
      Left = 7
      Top = 15
      Width = 21
      Height = 13
      Caption = 'Tipo'
    end
    object Label2: TLabel
      Left = 313
      Top = 15
      Width = 37
      Height = 13
      Caption = 'N'#250'mero'
    end
    object lblImpresoraFiscal: TLabel
      Left = 135
      Top = 15
      Width = 76
      Height = 13
      Caption = 'Impresora Fiscal'
      Visible = False
    end
    object cbTipoComprobante: TVariantComboBox
      Left = 7
      Top = 29
      Width = 297
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTipoComprobanteChange
      Items = <>
    end
    object neNumeroComprobante: TNumericEdit
      Left = 312
      Top = 29
      Width = 288
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 2
    end
    object cbImpresorasFiscales: TVariantComboBox
      Left = 135
      Top = 29
      Width = 169
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Visible = False
      OnChange = cbTipoComprobanteChange
      Items = <>
    end
  end
  object dblComprobantes: TDBListEx
    Left = 8
    Top = 205
    Width = 1150
    Height = 125
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Fecha'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblComprobantesColumns0HeaderClick
        FieldName = 'FechaEmision'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Comprobante Fiscal'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ComprobanteFiscal'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 175
        Header.Caption = 'Comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblComprobantesColumns0HeaderClick
        FieldName = 'DescriComprobante'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Vencimiento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblComprobantesColumns0HeaderClick
        FieldName = 'FechaVencimiento'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Periodo Inicial'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'PeriodoInicial'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Periodo Final'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'PeriodoFinal'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Width = 100
        Header.Caption = 'Total comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblComprobantesColumns0HeaderClick
        FieldName = 'DescriTotal'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Total a pagar'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblComprobantesColumns0HeaderClick
        FieldName = 'DescriAPagar'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Width = 100
        Header.Caption = 'Pagado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblComprobantesColumns0HeaderClick
        FieldName = 'DescriEstado'
      end>
    DataSource = dsFacturas
    DragReorder = True
    ParentColor = False
    TabOrder = 4
    TabStop = True
    OnContextPopup = dblComprobantesContextPopup
    OnDrawText = dblComprobantesDrawText
  end
  object pgDetalles: TPageControl
    Left = 0
    Top = 335
    Width = 1115
    Height = 200
    ActivePage = tsTransacciones
    Align = alBottom
    TabOrder = 5
    object tsDetalleComprobante: TTabSheet
      Caption = '&Detalle Comprobante'
      object dblDetalle: TDBListEx
        Left = 0
        Top = 0
        Width = 1107
        Height = 172
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 250
            Header.Caption = 'Concepto del Cargo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'Importe'
          end>
        DataSource = dsDetalle
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDrawText = dblDetalleDrawText
      end
    end
    object tsTransacciones: TTabSheet
      Caption = '&Transacciones'
      ImageIndex = 1
      object dblTransitos: TDBListEx
        Left = 0
        Top = 33
        Width = 1107
        Height = 139
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 155
            Header.Caption = 'Fecha / Hora'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'FechaHora'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'NombreCorto'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'P. Cobro'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'DescriPtoCobro'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'Patente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Categor'#237'a Interurbana'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'DescriCategoria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Categor'#237'a Urbana'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CategoriaUrbana'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Telev'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'Etiqueta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'Tipo Horario'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'TipoHorarioDesc'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Kms.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'Kms'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Width = 80
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblTransitosColumns0HeaderClick
            FieldName = 'DescriImporte'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsUnderline]
            Width = 60
            Header.Caption = 'Imagen'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Imagen'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Anulado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'TransitoAnulado'
          end>
        DataSource = dsTransitosPorComprobante
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnContextPopup = dblTransitosContextPopup
        OnCheckLink = dblTransitosCheckLink
        OnDrawText = dblTransitosDrawText
        OnLinkClick = dblTransitosLinkClick
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1107
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          1107
          33)
        object lbl_TransitosDesde: TLabel
          Left = 2
          Top = 9
          Width = 34
          Height = 13
          Caption = 'Desde:'
        end
        object lbl_TransitosHasta: TLabel
          Left = 136
          Top = 9
          Width = 31
          Height = 13
          Caption = 'Hasta:'
        end
        object lbl_Patente: TLabel
          Left = 367
          Top = 9
          Width = 40
          Height = 13
          Caption = 'Patente:'
        end
        object lbl_TransitosReestablecer: TLabel
          Left = 269
          Top = 9
          Width = 93
          Height = 13
          Cursor = crHandPoint
          Hint = 'Haga Click ac'#225' para reestablecer las fechas por defecto'
          Caption = 'Fechas por defecto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsUnderline]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Visible = False
          OnClick = lbl_TransitosReestablecerClick
        end
        object lblCSV: TLabel
          Left = 624
          Top = 10
          Width = 62
          Height = 13
          Cursor = crHandPoint
          Hint = 'Exportar esta consulta a formato CSV'
          Caption = 'Formato CSV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = lblCSVClick
        end
        object btnTransitosFiltrar: TButton
          Left = 1028
          Top = 2
          Width = 75
          Height = 25
          Anchors = [akTop, akRight, akBottom]
          Caption = 'Filtrar'
          Default = True
          TabOrder = 3
          OnClick = btnTransitosFiltrarClick
        end
        object de_TransitosDesde: TDateEdit
          Left = 39
          Top = 5
          Width = 90
          Height = 21
          AutoSelect = False
          TabOrder = 0
          OnChange = de_TransitosDesdeChange
          Date = -693594.000000000000000000
        end
        object de_TransitosHasta: TDateEdit
          Left = 173
          Top = 6
          Width = 90
          Height = 21
          AutoSelect = False
          TabOrder = 1
          OnChange = de_TransitosHastaChange
          Date = -693594.000000000000000000
        end
        object txt_Patente: TEdit
          Left = 411
          Top = 5
          Width = 120
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 10
          TabOrder = 2
        end
      end
    end
    object tsMensajes: TTabSheet
      Caption = '&Mensajes'
      ImageIndex = 2
      object DBListEx1: TDBListEx
        Left = 0
        Top = 0
        Width = 1107
        Height = 172
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 650
            Header.Caption = 'Mensaje'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Texto'
          end>
        DataSource = dsMensajes
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnContextPopup = dblTransitosContextPopup
        OnCheckLink = dblTransitosCheckLink
        OnLinkClick = dblTransitosLinkClick
      end
    end
  end
  object btnReimprimir: TButton
    Left = 762
    Top = 176
    Width = 171
    Height = 24
    Anchors = [akTop, akRight]
    Caption = 'Reimprimir Comprobante'
    Enabled = False
    TabOrder = 3
    OnClick = btnReimprimirClick
  end
  object btnImprimirFacturacionDetallada: TButton
    Left = 936
    Top = 176
    Width = 175
    Height = 24
    Anchors = [akTop, akRight]
    Caption = 'Imprimir Detalle de Transacciones'
    Enabled = False
    TabOrder = 6
    OnClick = btnImprimirFacturacionDetalladaClick
  end
  object BTNBuscarComprobante: TButton
    Left = 1041
    Top = 29
    Width = 66
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Buscar'
    Default = True
    TabOrder = 7
    OnClick = BTNBuscarComprobanteClick
  end
  object dsFacturas: TDataSource
    DataSet = spObtenerListaComprobantes
    OnDataChange = dsFacturasDataChange
    Left = 72
    Top = 264
  end
  object dsTransitosPorComprobante: TDataSource
    DataSet = ObtenerTransitosPorComprobante
    Left = 168
    Top = 441
  end
  object ObtenerTransitosPorComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTransitosPorComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = -6
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 200
    Top = 440
  end
  object spObtenerListaComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = spObtenerListaComprobantesAfterOpen
    AfterClose = spObtenerListaComprobantesAfterClose
    AfterScroll = spObtenerListaComprobantesAfterScroll
    ProcedureName = 'ObtenerListaComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoImpresoraFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 104
    Top = 264
  end
  object Detalle: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Importe'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 56
    Top = 504
  end
  object dsDetalle: TDataSource
    DataSet = Detalle
    Left = 24
    Top = 504
  end
  object spListadoCargos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleCuentaFacturaAImprimir'
    Parameters = <>
    Left = 56
    Top = 440
  end
  object dsMensajes: TDataSource
    DataSet = spMensajes
    Left = 168
    Top = 472
  end
  object spObtenerDetalleConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleConceptosComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 56
    Top = 472
  end
  object tmrConsultaRUT: TTimer
    Enabled = False
    OnTimer = tmrConsultaRUTTimer
    Left = 254
    Top = 174
  end
  object spMensajes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMensajeComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 200
    Top = 472
    object spMensajesTexto: TStringField
      FieldName = 'Texto'
      Size = 255
    end
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 170
    Top = 177
  end
  object spObtenerNumeroTipoComprobanteInterno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = spObtenerListaComprobantesAfterOpen
    AfterClose = spObtenerListaComprobantesAfterClose
    AfterScroll = spObtenerListaComprobantesAfterScroll
    ProcedureName = 'ObtenerNumeroTipoComprobanteInterno'
    Parameters = <>
    Left = 142
    Top = 264
  end
  object spObtenerNumeroTipoInternoComprobanteFiscal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 360
    ProcedureName = 'ObtenerNumeroTipoInternoComprobanteFiscal'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 2
        Value = Null
      end>
    Left = 258
    Top = 450
    object StringField1: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object LargeintField1: TLargeintField
      FieldName = 'Importe'
    end
    object StringField2: TStringField
      FieldName = 'DescImporte'
      Size = 50
    end
    object WordField1: TWordField
      FieldName = 'CodigoConcepto'
    end
  end
  object spCobrar_o_DescontarReimpresion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'Cobrar_o_DescontarReimpresion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 552
    Top = 96
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 824
    Top = 488
  end
  object spObtenerEstacionamientos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEstacionamientosComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 416
    Top = 456
  end
  object dsEstacionamientos: TDataSource
    DataSet = spObtenerEstacionamientos
    Left = 448
    Top = 456
  end
  object spObtenerFechasTransitosPorComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFechasTransitosPorComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 216
    Top = 264
  end
  object sdGuardarCSV: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 'Archivos Comma Separated Value|*.csv|Todos los Archivos|*.*'
    InitialDir = '.'
    Title = 'Guardar Como...'
    Left = 616
    Top = 552
  end
  object spObtenerFechasEstacionamientosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFechasEstacionamientosComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 280
    Top = 264
  end
  object spObtenerTiposComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposComprobantes'
    Parameters = <>
    Left = 488
    Top = 40
  end
end
