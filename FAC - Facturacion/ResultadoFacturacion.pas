{-----------------------------------------------------------------------------
 File Name: ResultadoFacturacion.pas
 Description:   Permite buscar comprobantes de PreFacturaci�n pertenecientes
                a un Convenio.

 -----------------------------------------------------------------------------}

unit ResultadoFacturacion;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Controls, Forms, Dialogs,
    Dateedit, Buttons, ExtCtrls, ADODB, VariantComboBox, DmiCtrls, UtilProc,
    UtilDB, util, DMConnection, BuscaClientes, peaTypes, peaProcs, DPSControls,
    ImagenesTransitos, MenuesCOntextuales, Navigator, ComCtrls, DB, ListBoxEx,
    DBListEx, StdCtrls, ReporteFactura, DBClient, Graphics,
    ConstParametrosGenerales, TimeEdit, Validate, DateUtils, ReporteCK, ReporteNC, CSVUtils, CollPnl;

type
  TFormResultadoFacturacion = class(TForm)
    dsFacturas: TDataSource;
    gbCliente: TGroupBox;
    Label1: TLabel;
    peNumeroDocumento: TPickEdit;
    spObtenerComprobantesResultado: TADOStoredProc;
    Label3: TLabel;
    cbConvenios: TVariantComboBox;
    Detalle: TClientDataSet;
    dsDetalle: TDataSource;
    spObtenerDetalleConceptosComprobante: TADOStoredProc;
    tmrConsultaRUT: TTimer;
    spProcesosFacturacion_SELECT: TADOStoredProc;
    lblverConvenio: TLabel;
    BTNBuscarComprobante: TButton;
    grpFiltros: TGroupBox;
    lblImporteDesde: TLabel;
    lblImporteHasta: TLabel;
    txtImporteDesde: TNumericEdit;
    txtImporteHasta: TNumericEdit;
    lbl1: TLabel;
    lblDomicilio: TLabel;
    lbl3: TLabel;
    lblConcesionaria: TLabel;
    lblConvenio: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblNombre: TLabel;
    txtMuestraAleatoria: TNumericEdit;
    grpProceso: TGroupBox;
    lbl7: TLabel;
    lblNumeroProcesoFacturacion: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblFechaHoraInicio: TLabel;
    lblFechaHoraFin: TLabel;
    lbl18: TLabel;
    lblOperador: TLabel;
    lbl27: TLabel;
    lblComprobantes: TLabel;
    grpParams: TGroupBox;
    lbl14: TLabel;
    lbl17: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    lblValIVA: TLabel;
    lblValUF: TLabel;
    lblDiasMinEmiFac: TLabel;
    lblMesForGenNK: TLabel;
    lbl10: TLabel;
    lblValMinNK: TLabel;
    lbl13: TLabel;
    lblValMinNKPagAut: TLabel;
    lblValMinDeuConv: TLabel;
    lbl11: TLabel;
    lblValMinNKBaja: TLabel;
    lbl12: TLabel;
    pnlListado: TPanel;
    lstComprobantes: TDBListEx;
    pnlAcciones: TPanel;
    lbl4: TLabel;
    lblResultado: TLabel;
    pnlDetalle: TPanel;
    pgcDetalles: TPageControl;
    tsDetalleComprobante: TTabSheet;
    lstDetalle: TDBListEx;
    chkMostrarDetalle: TCheckBox;
    lbl28: TLabel;
    lbl29: TLabel;
    lblGrupoFacturacionDescri: TLabel;
    lblCicloAno: TLabel;
    spl1: TSplitter;
    clpnlProceso: TCollapsablePanel;
    lblExportarCSV: TLabel;
    dlgSaveCSV: TSaveDialog;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    tsTransitos: TTabSheet;
    lstTransitos: TDBListEx;
    dsTransitosPorComprobante: TDataSource;
    spObtenerTransitosPorComprobante: TADOStoredProc;
    pnl1: TPanel;
    lbl_TransitosDesde: TLabel;
    lbl_TransitosHasta: TLabel;
    lbl_Patente: TLabel;
    lblTransitosFechasRestablecer: TLabel;
    lblTransitosCSV: TLabel;
    btnTransitosFiltrar: TButton;
    date_TransitosDesde: TDateEdit;
    date_TransitosHasta: TDateEdit;
    edtTransitosPatente: TEdit;
    lbl2: TLabel;
    lblMontoTotal: TLabel;
    lbl19: TLabel;
    lblValMinImpAfe: TLabel;
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure cbConveniosChange(Sender: TObject);
    procedure dsFacturasDataChange(Sender: TObject; Field: TField);
    procedure lstDetalleDrawText(Sender: TCustomDBListEx;
    Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
    var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
    var DefaultDraw: Boolean);
    procedure lstComprobantesColumnsImporteHeaderClick(Sender: TObject);
    procedure lstComprobantesColumnsHeaderClick(Sender: TObject);
    procedure tmrConsultaRUTTimer(Sender: TObject);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure lblverConvenioClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BTNBuscarComprobanteClick(Sender: TObject);
    procedure OrdenarColumna(Sender: TObject);
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure chkMostrarDetalleClick(Sender: TObject);
    procedure lstComprobantesColumnsImporteMesHeaderClick(Sender: TObject);
    procedure lblExportarCSVClick(Sender: TObject);
    procedure lstTransitosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure lstTransitosCheckLink(Sender: TCustomDBListEx;
      Column: TDBListExColumn; var IsLink: Boolean);
    procedure lstTransitosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnTransitosFiltrarClick(Sender: TObject);
    procedure lblTransitosFechasRestablecerClick(Sender: TObject);
    procedure lblTransitosCSVClick(Sender: TObject);

    protected
    { Protected declarations }
  private
    { Private declarations }
    FUltimaBusqueda : TBusquedaCliente;
    FConvenio : integer;
    FNumeroProcesoFacturacion,
    FCodigoGrupoFacturacion,
    FCiclo,
    FAno: Integer;
    FEditable,
    FTransitos: Boolean;

    FTransitosDesde, FTransitosHasta : TDateTime;
    FEstacionamientosDesde, FEstacionamientosHasta : TDateTime;
    FBoletaAfectaElectronica, FBoletaExentaElectronica,
    FFacturaAfectaElectronica, FFacturaExentaElectronica,
    FTipoNotaDeCobro, FNotaCreditoElectronica, FNotaDebitoElectronica,
    FUltimoNumeroDocumento: String;
    procedure CargarDatosProcesoFacturacion;
    procedure CargarComprobantes(Filtrar: Boolean);
    procedure MostrarDetalleNotaCobro;
    procedure MostrarDetalleOtrosComprobantes;
    procedure CargarTransitos;
    procedure MostrarDomicilioFacturacion(CodigoConvenio : integer);
  public
  { Public declarations }
    function Inicializar(NumeroProcesoFacturacion: Integer): Boolean;
  end;

resourcestring
    STR_INCLUIR = 'Incluir';
    STR_APARTAR = 'Excluir';              
    MSG_CONFIRM_CAPTION = 'Confirmaci�n';
  
var
  FormResultadoFacturacion: TFormResultadoFacturacion;

implementation

{$R *.dfm}
uses
    FrmInicioConsultaConvenio;

function TFormResultadoFacturacion.Inicializar(NumeroProcesoFacturacion: Integer): Boolean;
resourcestring
	MSG_ALL_INVOICE_TYPES	= 'Todos';
    MSG_ERROR_INICIALIZAR	= 'Error al inicializar el formulario.';
    SQL_BOLETA_EXENTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_EXENTA()';
    SQL_BOLETA_AFECTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_AFECTA()';
    SQL_FACTURA_EXENTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_EXENTA()';
    SQL_FACTURA_AFECTA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_AFECTA()';
    SQL_NOTA_COBRO			= 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_COBRO()';
    SQL_NOTA_CREDITO_ELECTRONICA = 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_CREDITO_ELECTRONICA()';
    SQL_NOTA_DEBITO_ELECTRONICA  = 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_DEBITO_ELECTRONICA()';
    TIPO_NOTA_DEBITO		= 'Nota D�bito Electr�nica';
begin
    //cargar los tipos de documentos electr�nicos
    FFacturaAfectaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_FACTURA_AFECTA);
    FFacturaExentaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_FACTURA_EXENTA);
    FBoletaAfectaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_BOLETA_AFECTA);
    FBoletaExentaElectronica	:= QueryGetValue(DMConnections.BaseCAC, SQL_BOLETA_EXENTA);
    FTipoNotaDeCobro			:= QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_COBRO);
    FNotaCreditoElectronica     := QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_CREDITO_ELECTRONICA);
    FNotaDebitoElectronica		:= QueryGetValue(DMConnections.BaseCAC, SQL_NOTA_DEBITO_ELECTRONICA);
    
    lblConvenio.Caption             := EmptyStr;
    lblConcesionaria.Caption        := EmptyStr;
    lblNombre.Caption               := EmptyStr;
    lblDomicilio.Caption            := EmptyStr;

	pgcDetalles.ActivePageIndex := 0;
    
    FNumeroProcesoFacturacion := NumeroProcesoFacturacion;
    clpnlProceso.Caption := Format(clpnlProceso.Caption, [NumeroProcesoFacturacion]);
    CargarDatosProcesoFacturacion;
    CargarComprobantes(True);
    
    Result := True;
end;

procedure TFormResultadoFacturacion.CargarDatosProcesoFacturacion;     
resourcestring
    MSG_ERROR_PROCESO = 'Error al obtener los datos del Proceso de Facturaci�n';
    STR_CICLO_ANO     = '%d - A�o %d';
begin
    try
        spProcesosFacturacion_SELECT.Close;
        spProcesosFacturacion_SELECT.Parameters.Refresh;
        spProcesosFacturacion_SELECT.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
        spProcesosFacturacion_SELECT.Open;

        lblNumeroProcesoFacturacion.Caption := spProcesosFacturacion_SELECT.FieldByName('NumeroProcesoFacturacion').AsString;
        lblFechaHoraInicio.Caption          := spProcesosFacturacion_SELECT.FieldByName('FechaHoraInicio').AsString;
        lblFechaHoraFin.Caption             := spProcesosFacturacion_SELECT.FieldByName('FechaHoraFin').AsString;
        lblOperador.Caption                 := spProcesosFacturacion_SELECT.FieldByName('Operador').AsString;
        lblValMinNK.Caption                 := spProcesosFacturacion_SELECT.FieldByName('ValMinNK').AsString;
        lblValMinDeuConv.Caption            := spProcesosFacturacion_SELECT.FieldByName('ValMinDeuConv').AsString;
        lblValMinNKBaja.Caption             := spProcesosFacturacion_SELECT.FieldByName('ValMinNKBaja').AsString;
        lblMesForGenNK.Caption              := spProcesosFacturacion_SELECT.FieldByName('MesForGenNK').AsString;
        lblValMinNKPagAut.Caption           := spProcesosFacturacion_SELECT.FieldByName('ValMinNKPagAut').AsString;
        lblValMinImpAfe.Caption             := spProcesosFacturacion_SELECT.FieldByName('ValMinImpAfe').AsString;   // TASK_115_MGO_20170119
        lblValUF.Caption                    := spProcesosFacturacion_SELECT.FieldByName('ValUF').AsString;
        lblValIVA.Caption                   := spProcesosFacturacion_SELECT.FieldByName('ValIVA').AsString;
        lblDiasMinEmiFac.Caption            := spProcesosFacturacion_SELECT.FieldByName('DiasMinEmiFac').AsString;
        lblGrupoFacturacionDescri.Caption   := spProcesosFacturacion_SELECT.FieldByName('GrupoFacturacionDescri').AsString;
        lblCicloAno.Caption                 := Format(STR_CICLO_ANO,
                                                    [spProcesosFacturacion_SELECT.FieldByName('Ciclo').AsInteger,
                                                    spProcesosFacturacion_SELECT.FieldByName('Ano').AsInteger]);

        FCodigoGrupoFacturacion             := spProcesosFacturacion_SELECT.FieldByName('CodigoGrupoFacturacion').AsInteger;
        FCiclo                              := spProcesosFacturacion_SELECT.FieldByName('Ciclo').AsInteger;
        FAno                                := spProcesosFacturacion_SELECT.FieldByName('Ano').AsInteger;

        lblComprobantes.Caption             := spProcesosFacturacion_SELECT.FieldByName('Comprobantes').AsString;
        lblMontoTotal.Caption               := spProcesosFacturacion_SELECT.FieldByName('MontoTotal').AsString;
    except
        on e: Exception do
            MsgBoxErr(MSG_ERROR_PROCESO, e.Message, 'Error', MB_ICONSTOP);
    end;
end;

procedure TFormResultadoFacturacion.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
    if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
            tmrConsultaRUTTimer(nil);
        end;
    end;
    F.free;
end;

procedure TFormResultadoFacturacion.cbConveniosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    Cargarcomprobantes(True);
end;

procedure TFormResultadoFacturacion.cbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin
     ItemComboBoxColor(Control, Index, Rect, State, clred, true);
  end
  else begin
     ItemComboBoxColor(Control, Index, Rect, State);
  end;
end;

procedure TFormResultadoFacturacion.chkMostrarDetalleClick(
  Sender: TObject);
begin
    if not chkMostrarDetalle.Checked then begin
        Detalle.Close;
        Detalle.CreateDataSet;
        spObtenerTransitosPorComprobante.Close;
    end else begin
        MostrarDetalleNotaCobro;
        CargarTransitos;
    end;
end;

procedure TFormResultadoFacturacion.CargarComprobantes(Filtrar: Boolean);
resourcestring
    MSG_ERROR_SP = 'Se produjo un error al ejecutar el procedimiento almacenado ObtenerDatosComprobanteParaTimbreElectronico';
    MSG_NO_EXISTEN_COMPROBANTE='No se encontraron comprobantes facturados';
    STR_RESULTADO = '%d resultados';
Var
    Persona: Integer;
    TipoComprobante: string;
    NumeroComprobante: int64;
  I: Integer;
begin
    spObtenerComprobantesResultado.Close;
    if Filtrar then begin
        spObtenerComprobantesResultado.Parameters.Refresh;
        spObtenerComprobantesResultado.Parameters.ParamByName('@NumeroDocumento').Value := peNumeroDocumento.Text;
        spObtenerComprobantesResultado.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
        spObtenerComprobantesResultado.Parameters.ParamByName('@ImporteDesde').Value := txtImporteDesde.ValueInt;
        spObtenerComprobantesResultado.Parameters.ParamByName('@ImporteHasta').Value := txtImporteHasta.ValueInt;
        spObtenerComprobantesResultado.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
        spObtenerComprobantesResultado.Parameters.ParamByName('@MuestraAleatoria').Value := txtMuestraAleatoria.ValueInt;
    end;
    spObtenerComprobantesResultado.Open;

    FConvenio := cbConvenios.Value;
    Persona := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT CodigoCliente FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = %d', [FConvenio]));
    lblConvenio.Caption :=  QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.FormatearNumeroConvenio(dbo.ObtenerNumeroConvenio(%d))', [FConvenio]));
    lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)',[FConvenio]));
    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNombrePersona(%d)', [Persona]));
    lblverConvenio.Enabled := (cbConvenios.Value > 0);
    
    lblResultado.Caption := Format(STR_RESULTADO, [spObtenerComprobantesResultado.RecordCount]);

    MostrarDomicilioFacturacion(FConvenio);

    for I := 0 to lstComprobantes.Columns.Count - 1 do
        TDBListExColumn(lstComprobantes.Columns[I]).Sorting := csNone;

    if spObtenerComprobantesResultado.RecordCount > 0 then
        lblTransitosFechasRestablecerClick(lblTransitosFechasRestablecer);
end;

procedure TFormResultadoFacturacion.MostrarDomicilioFacturacion(CodigoConvenio : integer);
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
    with spObtenerDomicilioFacturacion, Parameters do begin
        // Mostramos el domicilio de Facturaci�n
        Close;
        ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        try
            ExecProc;
            lblDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value;
        except
            on e: exception do begin
                MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

procedure TFormResultadoFacturacion.dsFacturasDataChange(Sender: TObject; Field: TField);
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
    ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';
    ERROR_GETTING_INVOICE_MESSAGES = 'Ocurri� un error obteniendo la lista de mensajes del comprobante.';
var
    TipoComprobante : string;
begin
    if chkMostrarDetalle.Checked then begin
        try
            Screen.Cursor := crHourglass;
            TipoComprobante := spObtenerComprobantesResultado.fieldByName('TipoComprobante').AsString;

            if ( TipoComprobante = TC_NOTA_COBRO ) then begin
                    MostrarDetalleNotaCobro;
                    CargarTransitos;
            end else
                MostrarDetalleOtrosComprobantes;

        finally
            Screen.Cursor := crDefault;
        end;
    end;
end;

procedure TFormResultadoFacturacion.MostrarDetalleNotaCobro;
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
var
  spObtenerDetalleCuentaFacturaAImprimir: TADOStoredProc;
begin
    spObtenerDetalleCuentaFacturaAImprimir := TADOStoredProc.Create(Self);
    try
        try
            Detalle.Close;
        
            spObtenerDetalleCuentaFacturaAImprimir.Connection := DMConnections.BaseCAC;
            spObtenerDetalleCuentaFacturaAImprimir.ProcedureName := 'ObtenerDetalleCuentaFacturaAImprimir';
            spObtenerDetalleCuentaFacturaAImprimir.CommandTimeout := 30;

            spObtenerDetalleCuentaFacturaAImprimir.Parameters.Refresh;
            spObtenerDetalleCuentaFacturaAImprimir.Parameters.ParamByName('@TipoComprobante').Value := spObtenerComprobantesResultado.fieldByName('TipoComprobante').AsString;
            spObtenerDetalleCuentaFacturaAImprimir.Parameters.ParamByName('@NumeroComprobante').Value := spObtenerComprobantesResultado.fieldByName('NumeroComprobante').asInteger;
            spObtenerDetalleCuentaFacturaAImprimir.Open;

            Detalle.CreateDataSet;
            while not spObtenerDetalleCuentaFacturaAImprimir.Eof do begin
                Detalle.Append;
                Detalle.FieldByName('Descripcion').Assign(spObtenerDetalleCuentaFacturaAImprimir.FieldByName('Descripcion'));
                Detalle.FieldByName('Importe').Assign(spObtenerDetalleCuentaFacturaAImprimir.FieldByName('DescriImporte'));
                Detalle.Post;
                spObtenerDetalleCuentaFacturaAImprimir.Next;
            end;

            if not spObtenerComprobantesResultado.IsEmpty then begin
                Detalle.Append;
                Detalle.FieldByName('Descripcion').AsString := 'TOTAL A PAGAR';
                Detalle.FieldByName('Importe').AsString := spObtenerComprobantesResultado.FieldByName('DescriAPagar').AsString;
                Detalle.Post;
            end;
            
            Detalle.First;
        except
            on e: exception do begin
                msgBoxErr(ERROR_GETTING_INVOICE_DETAIL, e.Message, caption, MB_ICONSTOP);
            end;
        end;
    finally
        spObtenerDetalleCuentaFacturaAImprimir.Free;
    end;
end;

procedure TFormResultadoFacturacion.CargarTransitos;
resourcestring
    ERROR_GETTING_INVOICE_TRANSITS = 'Ocurri� un error obteniendo la lista de tr�nsitos del comprobante.';
begin
    if not tsTransitos.TabVisible then Exit;

    try
        spObtenerTransitosPorComprobante.Close;
        spObtenerTransitosPorComprobante.Parameters.Refresh;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@TipoComprobante').Value       := spObtenerComprobantesResultado.FieldByName('TipoComprobante').AsString;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@NumeroComprobante').Value     := spObtenerComprobantesResultado.FieldByName('NumeroComprobante').AsInteger;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@Cantidad').Value              := MaxInt;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@FechaInicial').Value          := date_TransitosDesde.Date;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@FechaFinal').Value            := date_TransitosHasta.Date;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@CodigoConcesionaria').Value   := Null;
        spObtenerTransitosPorComprobante.Parameters.ParamByName('@Patente').Value               := IIf(Trim(edtTransitosPatente.Text) <> EmptyStr, edtTransitosPatente.Text, Null);
        spObtenerTransitosPorComprobante.Open;
    except
        on e: Exception do
            MsgBoxErr(ERROR_GETTING_INVOICE_TRANSITS, e.Message, caption, MB_ICONSTOP);
    end;
end;          

procedure TFormResultadoFacturacion.btnTransitosFiltrarClick(
  Sender: TObject);
begin
    CargarTransitos;
end;

procedure TFormResultadoFacturacion.lblTransitosCSVClick(Sender: TObject);
resourcestring
    MSG_TITLE   = 'Exportar Tr�nsitos a CSV';
    MSG_SIN_RESULTADOS = 'No hay registros en el listado para exportar.';
    MSG_ERROR_REGISTROS   = 'Error al exportar los tr�nsitos.';
    MSG_SUCCESS = 'El archivo %s fue creado exitosamente.';
    MSG_CONFIRM = '�Desea exportar los tr�nsitos del listado a formato CSV?';
    STR_FILENAME_TRANSITOS = 'Facturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__TRANSITOS__RUT_%s__Convenio_%s.csv';
var
    FileBuffer, Error: String;
    NombreArchivo: String;
begin
    if lstTransitos.DataSource.DataSet.IsEmpty then begin
        MsgBox(MSG_SIN_RESULTADOS, MSG_TITLE, MB_ICONSTOP);
        Exit;
    end;

    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then Exit;

    try
        Screen.Cursor := crHourGlass;

        if DatasetToExcelCSV(lstTransitos.DataSource.DataSet, FileBuffer, Error) then begin
            dlgSaveCSV.Title := 'Guardar Tr�nsitos Como...';
            NombreArchivo := Format(STR_FILENAME_TRANSITOS,
                                [FNumeroProcesoFacturacion, FCodigoGrupoFacturacion, FCiclo, FAno,
                                Trim(spObtenerComprobantesResultado.FieldByName('NumeroDocumento').AsString),
                                Trim(spObtenerComprobantesResultado.FieldByName('NumeroConvenio').AsString)]);

            dlgSaveCSV.FileName := NombreArchivo;

            Screen.Cursor := crDefault;
            if dlgSaveCSV.Execute then begin
                StringToFile(FileBuffer, dlgSaveCSV.FileName);
                MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
            end;
        end else begin
            raise Exception.Create(Error);
        end;
    except
        on e: Exception do begin
            Screen.Cursor := crDefault;
            MsgBoxErr(MSG_ERROR_REGISTROS, e.Message, MSG_TITLE, MB_ICONERROR);
            Exit;
        end;
    end;
end;

procedure TFormResultadoFacturacion.lblTransitosFechasRestablecerClick(
  Sender: TObject);
begin
    date_TransitosDesde.Date := spObtenerComprobantesResultado.FieldByName('PeriodoInicial').AsDateTime;
    date_TransitosHasta.Date := spObtenerComprobantesResultado.FieldByName('PeriodoFinal').AsDateTime;
end;

procedure TFormResultadoFacturacion.MostrarDetalleOtrosComprobantes;
resourcestring
    ERROR_GETTING_INVOICE_DETAIL = 'Ocurri� un error obteniendo el detalle del comprobante.';
begin
    try
        Detalle.Close;
        spObtenerDetalleConceptosComprobante.Close;
        spObtenerDetalleConceptosComprobante.Parameters.Refresh;
        spObtenerDetalleConceptosComprobante.Parameters.ParamByName('@TipoComprobante').Value := spObtenerComprobantesResultado.fieldByName('TipoComprobante').AsString;
        spObtenerDetalleConceptosComprobante.Parameters.ParamByName('@NumeroComprobante').Value := spObtenerComprobantesResultado.fieldByName('NumeroComprobante').asInteger;
        spObtenerDetalleConceptosComprobante.Open;
        Detalle.CreateDataSet;
        while not spObtenerDetalleConceptosComprobante.Eof do begin
            Detalle.Append;
            Detalle.FieldByName('Descripcion').Assign(spObtenerDetalleConceptosComprobante.FieldByName('Descripcion'));
            Detalle.FieldByName('Importe').AsString := spObtenerDetalleConceptosComprobante.FieldByName('DescImporte').AsString;
            Detalle.Post;
            spObtenerDetalleConceptosComprobante.Next;
        end;
        spObtenerDetalleConceptosComprobante.Close;
        if not spObtenerComprobantesResultado.IsEmpty then begin
        	Detalle.Append;
            if (spObtenerComprobantesResultado.FieldByName('TipoComprobante').AsString = TC_NOTA_CREDITO_A_COBRO)  then begin 
                Detalle.FieldByName('Descripcion').AsString := 'TOTAL COMPROBANTE';
            	Detalle.FieldByName('Importe').AsString := spObtenerComprobantesResultado.FieldByName('DescriTotal').AsString;
            end
            else begin
            	Detalle.FieldByName('Descripcion').AsString := 'TOTAL A PAGAR';
            	Detalle.FieldByName('Importe').AsString := spObtenerComprobantesResultado.FieldByName('DescriAPagar').AsString;
            end;
        	Detalle.Post;
        end;
        Detalle.First;
    except
        on e: exception do begin
            msgBoxErr(ERROR_GETTING_INVOICE_DETAIL, e.Message, caption, MB_ICONSTOP);
        end;
    end;
end;

procedure TFormResultadoFacturacion.lstDetalleDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Assigned(Column) then begin
        if Pos('TOTAL', UpperCase(Text)) <> 0 then
          lstDetalle.Canvas.Font.Style := [fsBold];
    end;
end;

procedure TFormResultadoFacturacion.lstTransitosCheckLink(
  Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    if Column.FieldName = 'Imagen' then
        IsLInk := spObtenerTransitosPorComprobante.Active and
          (spObtenerTransitosPorComprobante.FieldByName('RegistrationAccessibility').AsInteger > 0)
    else
        IsLInk := false;
end;

procedure TFormResultadoFacturacion.lstTransitosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column = lstTransitos.Columns[0] then
       Text := FormatDateTime('dd-mm-yyyy hh:nn', spObtenerTransitosPorComprobante.FieldByName('FechaHora').AsDateTime);

    if spObtenerTransitosPorComprobante.FieldByName('TransitoAnulado').AsString <> '' then
    	Sender.Canvas.Font.Color := clRed;
end;

procedure TFormResultadoFacturacion.lstTransitosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    if Column.IsLink then
        MostrarVentanaImagen(Self,
          spObtenerTransitosPorComprobante.FieldByName('NumCorrCA').asInteger,
          spObtenerTransitosPorComprobante.FieldByName('FechaHora').asDateTime,
          False);
end;

procedure TFormResultadoFacturacion.lstComprobantesColumnsImporteHeaderClick(Sender: TObject);
begin
   if NOT  spObtenerComprobantesResultado.Active then Exit;
   if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   else TDBListExColumn(sender).Sorting := csAscending;
   spObtenerComprobantesResultado.Sort := 'TotalAPagar' + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

procedure TFormResultadoFacturacion.lstComprobantesColumnsImporteMesHeaderClick(
  Sender: TObject);
begin
    if NOT  spObtenerComprobantesResultado.Active then Exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;
    spObtenerComprobantesResultado.Sort := 'TotalComprobante' + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

procedure TFormResultadoFacturacion.lstComprobantesColumnsHeaderClick(Sender: TObject);
begin
   if NOT  spObtenerComprobantesResultado.Active then Exit;
   if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
   else TDBListExColumn(sender).Sorting := csAscending;
   spObtenerComprobantesResultado.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

procedure TFormResultadoFacturacion.peNumeroDocumentoChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
 	tmrConsultaRUT.Enabled := False;
	tmrConsultaRUT.Enabled := True;
end;

procedure TFormResultadoFacturacion.tmrConsultaRUTTimer(Sender: TObject);
begin
	Screen.Cursor := crHourglass;
    try
		tmrConsultaRUT.Enabled := False;
        if peNumeroDocumento.Text = EmptyStr then
            cbConvenios.Clear;
        PeaProcs.CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, False, True, True);
        lblverConvenio.Enabled := (cbConvenios.Value > 0);
    finally
		Screen.Cursor := crDefault;
    end;
end;

procedure TFormResultadoFacturacion.OrdenarColumna(Sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;

procedure TFormResultadoFacturacion.BTNBuscarComprobanteClick(Sender: TObject);
begin
    CargarComprobantes(True);
end;

procedure TFormResultadoFacturacion.lblExportarCSVClick(Sender: TObject);
resourcestring
    MSG_TITLE   = 'Exportar CSV';
    MSG_SIN_RESULTADOS = 'No hay registros en el listado para exportar.';
    MSG_ERROR_ENCABEZADO   = 'Error al exportar los datos del encabezado.';
    MSG_ERROR_REGISTROS   = 'Error al exportar los registros del listado.';     // TASK_125_MGO_20170125
    MSG_ERROR_DETALLE   = 'Error al exportar el detalle del listado.';          // TASK_125_MGO_20170125
    MSG_SUCCESS = 'El archivo %s fue creado exitosamente.';
    MSG_CONFIRM = '�Desea exportar el encabezado y registros del listado a formato CSV?. Puede demorar unos minutos.'; // TASK_125_MGO_20170125
    STR_FILENAME_ENCABEZADO = 'Facturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__ENCABEZADO.csv';
    STR_FILENAME_REGISTROS = 'Facturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__REGISTROS.csv';    
    STR_FILENAME_DETALLE = 'Facturacion_%d__Grupo_%d__Ciclo_%d__Anio_%d__DETALLE.csv';  // TASK_125_MGO_20170125
var
    FileBuffer, Error: String;
    NombreArchivo: String;      
    EstadoMostrar: Boolean;
    spGenerarReporteProcesoFacturacion: TADOStoredProc;                         // TASK_125_MGO_20170125
begin
    if lstComprobantes.DataSource.DataSet.IsEmpty then begin
        MsgBox(MSG_SIN_RESULTADOS, MSG_TITLE, MB_ICONSTOP);
        Exit;
    end;

    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then Exit;

    try
        Screen.Cursor := crHourGlass;
        if DatasetToExcelCSV(spProcesosFacturacion_SELECT, FileBuffer, Error) then begin
            dlgSaveCSV.Title := 'Guardar Encabezado Como...';
            NombreArchivo := Format(STR_FILENAME_ENCABEZADO,
                                [FNumeroProcesoFacturacion,
                                FCodigoGrupoFacturacion,
                                FCiclo, FAno]);

            dlgSaveCSV.FileName := NombreArchivo;

            Screen.Cursor := crDefault;
            if dlgSaveCSV.Execute then begin
                StringToFile(FileBuffer, dlgSaveCSV.FileName);
                MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
            end;
        end else begin
            raise Exception.Create(Error);
        end;
    except
        on e: Exception do begin
            Screen.Cursor := crDefault;
            MsgBoxErr(MSG_ERROR_ENCABEZADO, e.Message, MSG_TITLE, MB_ICONERROR);
            Exit;
        end;
    end;

    try
        try
            Screen.Cursor := crHourGlass;
            EstadoMostrar := chkMostrarDetalle.Checked;
            chkMostrarDetalle.Checked := False;

            // INICIO : TASK_125_MGO_20170125
            spGenerarReporteProcesoFacturacion := TADOStoredProc.Create(Self);
            spGenerarReporteProcesoFacturacion.Connection := DMConnections.BaseCAC;
            spGenerarReporteProcesoFacturacion.ProcedureName := 'GenerarReporteProcesoFacturacion';
            spGenerarReporteProcesoFacturacion.CommandTimeout := 60;
            spGenerarReporteProcesoFacturacion.Parameters.Refresh;
            spGenerarReporteProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            spGenerarReporteProcesoFacturacion.Open;
            // FIN : TASK_125_MGO_20170125

            //if DatasetToExcelCSV(lstComprobantes.DataSource.DataSet, FileBuffer, Error) then begin    // TASK_125_MGO_20170125
            if DatasetToExcelCSV(spGenerarReporteProcesoFacturacion, FileBuffer, Error) then begin      // TASK_125_MGO_20170125
                dlgSaveCSV.Title := 'Guardar Registros Como...';
                NombreArchivo := Format(STR_FILENAME_REGISTROS,
                                    [FNumeroProcesoFacturacion,
                                    FCodigoGrupoFacturacion,
                                    FCiclo, FAno]);

                dlgSaveCSV.FileName := NombreArchivo;

                Screen.Cursor := crDefault;
                if dlgSaveCSV.Execute then begin
                    StringToFile(FileBuffer, dlgSaveCSV.FileName);
                    MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
                end;
            end else begin
                raise Exception.Create(Error);
            end;
        except
            on e: Exception do begin
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ERROR_REGISTROS, e.Message, MSG_TITLE, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        chkMostrarDetalle.Checked := EstadoMostrar;
        spGenerarReporteProcesoFacturacion.Free;                                // TASK_125_MGO_20170125
    end;

    // INICIO : TASK_125_MGO_20170125
    try
        try
            Screen.Cursor := crHourGlass;
            EstadoMostrar := chkMostrarDetalle.Checked;
            chkMostrarDetalle.Checked := False;

            spGenerarReporteProcesoFacturacion := TADOStoredProc.Create(Self);
            spGenerarReporteProcesoFacturacion.Connection := DMConnections.BaseCAC;
            spGenerarReporteProcesoFacturacion.ProcedureName := 'GenerarReporteProcesoFacturacionDetalle';
            spGenerarReporteProcesoFacturacion.CommandTimeout := 60;
            spGenerarReporteProcesoFacturacion.Parameters.Refresh;
            spGenerarReporteProcesoFacturacion.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProcesoFacturacion;
            spGenerarReporteProcesoFacturacion.Open;

            if DatasetToExcelCSV(spGenerarReporteProcesoFacturacion, FileBuffer, Error) then begin
                dlgSaveCSV.Title := 'Guardar Detalle Como...';
                NombreArchivo := Format(STR_FILENAME_DETALLE,
                                    [FNumeroProcesoFacturacion,
                                    FCodigoGrupoFacturacion,
                                    FCiclo, FAno]);

                dlgSaveCSV.FileName := NombreArchivo;

                Screen.Cursor := crDefault;
                if dlgSaveCSV.Execute then begin
                    StringToFile(FileBuffer, dlgSaveCSV.FileName);
                    MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
                end;
            end else begin
                raise Exception.Create(Error);
            end;
        except
            on e: Exception do begin
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ERROR_DETALLE, e.Message, MSG_TITLE, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        chkMostrarDetalle.Checked := EstadoMostrar;
        spGenerarReporteProcesoFacturacion.Free;
    end;
    // FIN : TASK_125_MGO_20170125
end;

procedure TFormResultadoFacturacion.lblverConvenioClick(Sender: TObject);
var
	f: TFormInicioConsultaConvenio;
begin
    if FindFormOrCreate(TFormInicioConsultaConvenio, f) then
        f.Show
    else begin
        if f.Inicializar(FConvenio, -1, -1) then
            f.Show
        else
            f.Release;
    end;
end;

procedure TFormResultadoFacturacion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;           

end.
