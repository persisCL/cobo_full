{-------------------------------------------------------------------------------
 File Name      : frm_ImprimirNotaCobroInfraccion.pas
 Author         : CQuezada
 Date Created   : 10/12/2012
 Language       : ES-CL
 Firma          : SS_660_CQU_20121010
 Description    : Permite seleccionar el proceso de facturaci�n generado en la facturaci�n de morosos
                  para imprimir las notas de cobro que �ste tiene asociadas.
 -------------------------------------------------------------------------------}
unit frm_SeleccionarProcesosMorosos;

interface

uses
  DMConnection, PeaTypes, PeaProcs, Util, UtilProc, Windows, Messages, SysUtils,
  Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ListBoxEx,
  DBListEx, DB, ADODB, VariantComboBox, UtilDB, SysUtilsCN, Validate, DateEdit,
  DmiCtrls, frmReporteBoletaFacturaElectronica, frmReporteFacturacionDetallada,
  frmRptDetInfraccionesAnuladas, frm_ImprimirNotaCobroMorosos;

type
  Tform_SeleccionarProcesosMorosos = class(TForm)
    dblNotasCobroInfraccion: TDBListEx;
    lblSeleccione: TLabel;
    btnCerrar: TButton;
    spObtenerProcesosFacturacionMorosos: TADOStoredProc;
    dsObtenerProcesosFacturacionMorosos: TDataSource;
    grpFiltro: TGroupBox;
    lbl_TransitosDesde: TLabel;
    lbl_TransitosHasta: TLabel;
    deFechaDesde: TDateEdit;
    deFechaHasta: TDateEdit;
    btnBuscar: TButton;
    lblProcesoFacturacion: TLabel;
    txtNumeroProceso: TNumericEdit;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCerrarClick(Sender: TObject);
    procedure OrdenarPorColumna(Sender: TObject);
    procedure dblNotasCobroInfraccionDblClick(Sender: TObject);
    procedure dblNotasCobroInfraccionDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
  private
    { Private declarations }
    procedure LiberarMemoria;
  public
    { Public declarations }
    function Inicializar : Boolean;
  end;

var
  form_SeleccionarProcesosMorosos: Tform_SeleccionarProcesosMorosos;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name : Inicializar
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Inicializo este formulario
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
function Tform_SeleccionarProcesosMorosos.Inicializar: Boolean;
begin
    deFechaDesde.Date := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC) - 30;
    deFechaHasta.Date := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name : OrdenarPorColumna
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Ordena los datos de la TDBListEx de acuerdo a la columna que seleccione el usuario
-----------------------------------------------------------------------------}
procedure Tform_SeleccionarProcesosMorosos.OrdenarPorColumna(Sender: TObject);
begin
	if not spObtenerProcesosFacturacionMorosos.Active then Exit
    else
        OrdenarGridPorColumna(	TDBListExColumn(Sender).Columns.List,
                                TDBListExColumn(Sender),
                                TDBListExColumn(Sender).FieldName);
end;

{-----------------------------------------------------------------------------
  Function Name : btnBuscarClick
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Permito obtener las Notas de Cobro Infractoras a imprimir
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_SeleccionarProcesosMorosos.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_ERROR_EMPTY_DATA    = 'No hay datos de acuerdo a las opciones seleccionadas';
    MSG_ERROR_FILTRO        = 'Probla en el filtro';
    MSG_ERROR               = 'Ha ocurrido un error';
begin

    if not ValidateControls(
    [deFechaDesde],
    [deFechaDesde.Date <= deFechaHasta.Date],
    MSG_ERROR_FILTRO,
    [MSG_RANGO_FECHA_INCORRECTO]
    ) then Exit;

    try
        with spObtenerProcesosFacturacionMorosos do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@FechaInicio'). Value := deFechaDesde.Date;
            Parameters.ParamByName('@FechaFin'). Value := deFechaHasta.Date;
            if txtNumeroProceso.Value > 0 then
                Parameters.ParamByName('@NumeroProceso'). Value := txtNumeroProceso.ValueInt;
            Open;
        end;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.message, caption, MB_ICONSTOP);
        end;
    end;

    if spObtenerProcesosFacturacionMorosos.IsEmpty then begin
        MsgBox(MSG_ERROR_EMPTY_DATA, Caption, MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCerrarClick
  Author: lgisuk
  Date Created:  20/03/06
  Description:  Permito cerrar el formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_SeleccionarProcesosMorosos.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name : FormClose
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Libero el formulario de memoria
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_SeleccionarProcesosMorosos.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    spObtenerProcesosFacturacionMorosos.Close;
    Action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name : dblNotasCobroInfraccionDblClick
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Se abrir� la ventana con las facturas del proceso seleccionado
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_SeleccionarProcesosMorosos.dblNotasCobroInfraccionDblClick(Sender: TObject);
var
    f : Tform_ImprimirNotaCobroMorosos;
begin
    LiberarMemoria;
    // Abrir la ventana con las facturas a imprimir
    f := Tform_ImprimirNotaCobroMorosos.Create(nil);
    if (f.Inicializar(spObtenerProcesosFacturacionMorosos.FieldByName('NumeroProcesoFacturacion').AsInteger)) then
        f.ShowModal;
    if Assigned(f) then FreeAndNil(f);
end;

{-----------------------------------------------------------------------------
  Function Name : dblNotasCobroInfraccionDrawText
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Dar� formato a la fecha que se despliega
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_SeleccionarProcesosMorosos.dblNotasCobroInfraccionDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if (Column.FieldName = 'FechaHoraInicio') then
        Text := FormatDateTime('dd/mm/yyyy', spObtenerProcesosFacturacionMorosos.FieldByName('FechaHoraInicio').AsDateTime);
    if (Column.FieldName = 'FechaHoraFin')  then
        Text := FormatDateTime('dd/mm/yyyy', spObtenerProcesosFacturacionMorosos.FieldByName('FechaHoraFin').AsDateTime);
end;

{-----------------------------------------------------------------------------
  Function Name : LiberarMemoria
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Libera la memoria
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_SeleccionarProcesosMorosos.LiberarMemoria;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
end;

end.
