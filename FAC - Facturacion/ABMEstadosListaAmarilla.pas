{-----------------------------------------------------------------------------
 File Name      : ABMEstadosListaAmarilla
 Author         : Claudio Quezada Ib��ez
 Date Created   : 19-Febrero-2013
 Language       : ES-CL
 Firma          : SS_660_CQU_20121010
 Description    : Alta-Baja-Modificaci�n de la tabla EstadosConveniosListaAmarilla

 Firma          : SS_660_CQU_20130813
 Fecha          : 20-08-2013
 Description    : Deshabilita los botenes Alta y Baja del ABM. (.dfm) 
-----------------------------------------------------------------------------}
unit ABMEstadosListaAmarilla;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, VariantComboBox, Variants,
  BuscaTab;

type
  TFormEstadosListaAmarilla = class(TForm)
    AbmToolbar1: TAbmToolbar;
    dblEstados: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txtDescripcionEstados: TEdit;
    Notebook: TNotebook;
    tblEstados: TADOTable;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    txtCodigoEstado: TNumericEdit;
    lblCodigoEstado: TLabel;
    //qryEstados: TADOQuery;
    procedure btnCancelarClick(Sender: TObject);
    procedure dblEstadosClick(Sender: TObject);
    procedure dblEstadosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblEstadosEdit(Sender: TObject);
    procedure dblEstadosRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure dblEstadosDelete(Sender: TObject);
    procedure dblEstadosInsert(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure tblEstadosAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
	function Inicializa: boolean;
  end;

var
  FormEstadosListaAmarilla: TFormEstadosListaAmarilla;

implementation

resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el estado seleccionado?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar el estado seleccionado.';
    MSG_DELETE_CAPTION		= 'Eliminar Estado';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del estado seleccionado.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Estado';
    MSG_AGREGAR_CAPTION	    = 'Agregar Estado';
    MSG_DESCRIPCION         = 'Debe ingresar la Descripci�n';
    MSG_DATOS_RELACIONADOS  = 'El Estado no puede ser eliminado ya que se encuentra relacionado';
    MSG_CODIGO_VACIO        = 'Debe ingresar un C�digo';
    MSG_CODIGO_RANGO        = 'El c�digo ingresado est� fuera del rango permitido';
{$R *.DFM}
            
function TFormEstadosListaAmarilla.Inicializa(): boolean;
begin
    FormStyle := fsMDIChild;
	if not OpenTables([tblEstados]) then
		Result := False
	else begin
       	Notebook.PageIndex := 0;
		Result := True;
        tblEstados.Sort := 'Estado';
		dblEstados.Reload;
	end;
end;

procedure TFormEstadosListaAmarilla.btnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormEstadosListaAmarilla.dblEstadosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
        txtCodigoEstado.Value := FieldByName('Estado').AsInteger;
		txtDescripcionEstados.text	:= FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormEstadosListaAmarilla.dblEstadosDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, Tabla.FieldByName('Estado').AsString);
   		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFormEstadosListaAmarilla.dblEstadosEdit(Sender: TObject);
begin
	dblEstados.Enabled   := False;
    dblEstados.Estado    := modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    txtDescripcionEstados.setFocus;
end;

procedure TFormEstadosListaAmarilla.dblEstadosRefresh(Sender: TObject);
begin
	 if dblEstados.Empty then Limpiar_Campos();
end;

procedure TFormEstadosListaAmarilla.Limpiar_Campos();
begin
	txtDescripcionEstados.Clear;
    txtCodigoEstado.Clear;
end;

procedure TFormEstadosListaAmarilla.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormEstadosListaAmarilla.dblEstadosDelete(Sender: TObject);
var
    Consulta : string;
    Estado, Resultado : Integer;
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            Estado := (Sender AS TDbList).Table.FieldByName('Estado').AsInteger;
            Consulta := 'SELECT COUNT(Estado) FROM ConveniosInhabilitadosListaAmarilla WHERE Estado = ' + IntToStr(Estado);
            Resultado := QueryGetValueInt(DMConnections.BaseCAC, Consulta);

            if (Resultado = 0) then
    			(Sender AS TDbList).Table.Delete
            else
                MsgBoxErr(MSG_DELETE_ERROR, MSG_DATOS_RELACIONADOS, MSG_DELETE_CAPTION, MB_ICONSTOP);
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		dblEstados.Reload;
	end;
	dblEstados.Estado    := Normal;
	dblEstados.Enabled   := True;
 	Notebook.PageIndex      := 0;
	Screen.Cursor           := crDefault;
end;

procedure TFormEstadosListaAmarilla.dblEstadosInsert(Sender: TObject);
begin
	Limpiar_Campos;
    groupb.Enabled          := True;
	dblEstados.Enabled      := False;
	Notebook.PageIndex      := 1;
    txtCodigoEstado.Enabled := True;
    txtCodigoEstado.Color   := $00FAEBDE;
    txtCodigoEstado.SetFocus;
end;

procedure TFormEstadosListaAmarilla.btnAceptarClick(Sender: TObject);
var
    MensajeError,
    TituloMensaje        : string;
begin
    if dblEstados.Estado = Alta then begin
        dblEstados.Table.Append;
        TituloMensaje := MSG_AGREGAR_CAPTION;
    end else begin
        dblEstados.Table.Edit;
        TituloMensaje :=  MSG_ACTUALIZAR_CAPTION
    end;

    if not ValidateControls(
        [txtCodigoEstado, txtCodigoEstado, txtDescripcionEstados],
        [txtCodigoEstado.Text <> '',(txtCodigoEstado.Value > 0) and (txtCodigoEstado.Value <= 255),Trim(txtDescripcionEstados.text) <> ''],
        TituloMensaje,
        [MSG_CODIGO_VACIO, MSG_CODIGO_RANGO, MSG_DESCRIPCION]) then exit;
    Screen.Cursor := crHourGlass;
	With dblEstados.Table do begin
		Try
			if txtCodigoEstado.Value <= 0 then
            	FieldByName('Estado').Clear
            else
				FieldByName('Estado').AsInteger := StrToInt(txtCodigoEstado.Text);

			if Trim(txtDescripcionEstados.text) = '' then
            	FieldByName('Descripcion').Clear
            else
				FieldByName('Descripcion').AsString	 := Trim(txtDescripcionEstados.text);
			Post;
            txtCodigoEstado.Color   := clBtnFace;
            txtCodigoEstado.Enabled := False;
		except
			On E: Exception do begin
                Screen.Cursor := crDefault;
				Cancel;
                MensajeError := E.Message;
                if (AnsiPos('UNIQUE KEY', MensajeError) <> 0) then
                    MensajeError := 'El Estado ingresado ya existe';
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, MensajeError, TituloMensaje, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor := crDefault;
end;

procedure TFormEstadosListaAmarilla.FormShow(Sender: TObject);
begin
   	dblEstados.Reload;
end;

procedure TFormEstadosListaAmarilla.tblEstadosAfterOpen(DataSet: TDataSet);
begin
    tblEstados.DisableControls;
    tblEstados.Sort := 'Estado';
    tblEstados.EnableControls
end;

procedure TFormEstadosListaAmarilla.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormEstadosListaAmarilla.btnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormEstadosListaAmarilla.Volver_Campos;
begin
	dblEstados.Estado      := Normal;
	dblEstados.Enabled     := True;
    dblEstados.Reload;
	dblEstados.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

end.
