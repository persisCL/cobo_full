object FormIngresarAnio: TFormIngresarAnio
  Left = 345
  Top = 275
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Ingresar a'#241'o'
  ClientHeight = 193
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 4
    Top = 4
    Width = 393
    Height = 153
  end
  object lbl_Titulo: TLabel
    Left = 16
    Top = 21
    Width = 370
    Height = 20
    Alignment = taCenter
    AutoSize = False
    Caption = 
      'Por favor, ingrese el a'#241'o para el nuevo calendario de facturaci'#243 +
      'n.'
    WordWrap = True
  end
  object btn_Aceptar: TDPSButton
    Left = 246
    Top = 162
    Caption = '&Aceptar'
    TabOrder = 1
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TDPSButton
    Left = 322
    Top = 162
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = btn_CancelarClick
  end
  object txt_Anio: TNumericEdit
    Left = 162
    Top = 62
    Width = 77
    Height = 21
    MaxLength = 4
    TabOrder = 0
    Decimals = 0
  end
end
