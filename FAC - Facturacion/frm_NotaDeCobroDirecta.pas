{-------------------------------------------------------------------------------
 File Name: frm_NotaDeCobroDirecta.pas
 Author:  mlopez
 Date Created: 01/06/2005
 Language: ES-AR
 Description: Proceso de Emision de Nota de Cobro Directa

 Revision 1
 Author: nefernandez
 Date Created: 09/03/2007
 Language: ES-AR
 Description: Se pasa el parametro CodigoUsuario al SP CrearNotaCobroNQ
              (para Auditoria)

 Revision 2
 Author: mbecerra
 Date: 27-Agosto-2008
 Description:	Se modifica para que la lectura de los proveedores sea desde la tabla
            	ProveedoresDayPass, en vez de la tabla ClientesNQ

 Revision 3
 Author: mbecerra
 Date: 15-Junio-2009
 Description:	(Ref. Facturaci�n Electronica)
                Se modifica la funcion Delphi CrearNotaCobroNQ para poder
                capturar correctamente el Error en la ejecuci�n del
                stored CrearNotaCobroNQ.

                Adicionalmente, se quita la transaccionalidad y se trapasa al stored.

        06-Julio-2009
                Se vuelve el stored atr�s...por lo tanto se devuelve la transaccionalidad
                a programa.

        31-Julio-2009
			1.-		Se modifica nuevamente el stored con los cambios del 15-Junio.
            2.-		Adem�s, la NQ ahora se transforma en documento Electr�nico
            3.-		No hay convenio asociado a la NQ
            4.-		La impresi�n electr�nica se ajusta a este nuevo documento interno.
            5.-		Se cambian los CheckBox por RadioButtons
            
Firma       : SS-985-NDR-20110920
Description : Se debe generar la facturacion de nota de cobro directa por concesionaria.
-------------------------------------------------------------------------------}
unit frm_NotaDeCobroDirecta;

interface

uses
  //Emision Nota de Cobro Directa
  DMConnection,
  Util,
  UtilDB,
  UtilProc,
  PeaProcs,
  Peatypes,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Validate, DateEdit, ExtCtrls, VariantComboBox;

type
  Tform_NotaDeCobroDirecta = class(TForm)
    spCrearNotaCobroNQ: TADOStoredProc;
    deFechaHasta: TDateEdit;
    btnFacturar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    btnCancelar: TButton;
    deFechaEmision: TDateEdit;
    Label3: TLabel;
    Label4: TLabel;
    cbNombreClienteNQ: TVariantComboBox;
    ExisteComprobanteMasNuevo: TADOStoredProc;
    rbtnPDU: TRadioButton;
    rbtnBHTU: TRadioButton;
    Label5: TLabel;
    cbConcesionaria: TVariantComboBox;
    cbConcesionariaFac: TVariantComboBox;
    Label6: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnFacturarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    function  GenerarNotasDeCobroNQ(var DescriError : String) : Boolean;
    function  CrearNotaCobroNQ(FechaHasta : TDateTime; TipoDayPass : String; var NumeroComprobante : Integer; var DescriError : string) : Boolean;
    procedure CargarClientesNQ(Conn : TADOConnection; Combo : TVariantComboBox);
  public
    { Public declarations }
    function Inicializar : Boolean;
  end;

var
  form_NotaDeCobroDirecta : Tform_NotaDeCobroDirecta;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author: mlopez
  Date Created: 01/06/2005
  Description: Inicializo este formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tform_NotaDeCobroDirecta.Inicializar : Boolean;
begin
    CargarClientesNQ(DMConnections.BaseCAC, cbNombreClienteNQ);
    CargarConcesionariasReclamo(cbConcesionaria);                               //SS-985-NDR-20110920
    CargarConcesionariasReclamo(cbConcesionariaFac);                            //SS-985-NDR-20110920
    deFechaHasta.Date := NowBase(DMConnections.BaseCAC);
    deFechaEmision.Date := NowBase(DMConnections.BaseCAC);
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name:  CargarClientesNQ
  Author:  mlopez
  Date Created: 01/06/2005
  Description:  Carga en una lista los clientes a los que se les puede emitir
                una nota de cobro directa.
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_NotaDeCobroDirecta.CargarClientesNQ;
var
  	I : Integer;
  	Qry : TADOQuery;
    S : AnsiString;
begin
    Qry := TAdoQuery.Create(nil);
    try
        Qry.Connection := Conn;
        {Revisi�n 2
        S := 'SELECT CodigoClienteNQ, '
                + '(     (case when Apellido is not null then Apellido + space(1) else '''' end)'
                    +   '+ (case when ApellidoMaterno is not null then ApellidoMaterno + space(1) else '''' end)'
                    +   '+ (case when Nombre is not null then Nombre else '''' end)'
                + ') AS Nombre FROM ClientesNQ WITH (NOLOCK)';}

        S := 'SELECT * FROM ProveedoresDayPass WITH (nolock) ORDER BY Descripcion';
        Qry.SQL.Text := S;
        Qry.Open;
        Combo.Clear;
        I := -1;
        {Revision 2
        if TieneItemNinguno then begin
            Combo.Items.Add(SIN_ESPECIFICAR, 0);
            I := 0;
        end;}
        while not Qry.Eof do begin
          Combo.Items.Add(	Qry.FieldByName('Descripcion').AsString + Space(200) +  {descripci�n}
          					Qry.FieldByName('NumeroDocumento').AsString,            {Rut}
                            Qry.FieldByName('CodigoProveedorDayPass').AsInteger);   {codigoProveedor}

          Qry.Next;
        end;
        {revision 2
        if (Combo.Items.Count > 0) and (I = -1) then I := 0;
        Combo.ItemIndex := I;}
    finally
        Qry.Close;
        Qry.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name:  GenerarNotasDeCobroNQ
  Author: mlopez
  Date Created: 01/06/2005
  Description:  Selecciona si tiene que crear una nota de cobro PDU o BHTU
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tform_NotaDeCobroDirecta.GenerarNotasDeCobroNQ(var DescriError : String) : Boolean;
var
    NumeroCoprobante : Integer;
    TipoDayPass : string;			//REV.3
begin
    DescriError := '';

    {REV. 3
    Result := False;
    DMConnections.BaseCAC.BeginTrans;
    try
        if cbPDU.Checked then begin
            CrearNotaCobroNQ(deFechaHasta.Date, TIPO_DAYPASS_NORMAL, NumeroCoprobante);
        end;
        if cbBHTU.Checked then begin
            CrearNotaCobroNQ(deFechaHasta.Date, TIPO_DAYPASS_TARDIO, NumeroCoprobante);
        end;

        DMConnections.BaseCAC.CommitTrans;
        Result := True;

    except on e : Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            DescriError := e.Message;
        end;
    end;}

    if rbtnPDU.Checked then TipoDayPass := TIPO_DAYPASS_NORMAL
    else TipoDayPass := TIPO_DAYPASS_TARDIO;

    Result := CrearNotaCobroNQ(deFechaHasta.Date, TipoDayPass, NumeroCoprobante, DescriError);

end;

{-----------------------------------------------------------------------------
  Function Name: CrearNotaCobroNQ
  Author:  mlopez
  Date Created: 01/06/2005
  Description: El sp CrearNotaCobroNQ, determina seg�n el valor de @TipoDayPass,
               Si debe generar una Nota de Cobro Directa para PDU o una Nota de
               Cobro Directa para BHTU.
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tform_NotaDeCobroDirecta.CrearNotaCobroNQ;
begin
    Screen.Cursor := crHourGlass;
    //REV.3 try
    //REV.2 DMConnections.BaseCAC.Execute('BEGIN TRAN spCrearNotaCobroNQ');
    DescriError := '';

    Result := False;
        try
            with spCrearNotaCobroNQ do begin
            	Parameters.Refresh;
                Parameters.ParamByName('@CodigoClienteNQ').Value    := cbNombreClienteNQ.Value;
                Parameters.ParamByName('@FechaHasta').Value         := FechaHasta;
                Parameters.ParamByName('@TipoDayPass').Value        := TipoDayPass;
                Parameters.ParamByName('@FechaEmision').Value       := deFechaEmision.Date;
                // Revision 1
                Parameters.ParamByName('@CodigoUsuario').Value      := UsuarioSistema;
                Parameters.ParamByName('@NumeroComprobante').Value  := Null;
                Parameters.ParamByName('@CodigoConcesionaria').Value  := cbConcesionaria.Value;   //SS-985-NDR-20110920
                Parameters.ParamByName('@CodigoConcesionariaFac').Value  := 1; //cbConcesionariaFac.Value;   //SS-985-NDR-20110920
                CommandTimeOut := 2500;
                ExecProc;
                Result := True;
            end;
        //REV.3 except
            //REV.3 raise;
        except on e:exception do begin
                //REV3 MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
                DescriError := e.Message;
        	end;
        end;
    //REV.3 finally

    {REV.2
    if Result then DMConnections.BaseCAC.Execute('COMMIT TRAN spCrearNotaCobroNQ')
    else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spCrearNotaCobroNQ');
    }

    Screen.Cursor := crDefault;
    //REV.3    spCrearNotaCobroNQ.Close;
    //REV.3 end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnFacturarClick
  Author: mlopez
  Date Created: 01/06/2005
  Description: Proceso que crea la nota de cobro NQ
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
  Revision 1:
    Author : ggomez
    Date : 16/03/2006
    Description : Debido a que se agreg� el Tipo de Comprobante para la facturaci�n
        de Notas de Cobro Directas para BHTU, agregu� la verificaci�n de si
        existe comprobante m�s nuevo para ambos tipos.

  Revision 2:
  Author: mbecerra
  Date: 03-septiembre-2008
  Descripci�n:	 Se modifica para agregar el par�metro adicional @RutProveedorDayPass
				al SP ExisteComprobanteMasNuevo


  Revision 3
  Author: mbecerra
  Date: 12-Nov-2008
  Description:	Se agrega una validaci�n, SS 748, mediante la cual la fecha de emisi�n de la
                Nota de cobro no puede ser posterior al d�a de hoy.
-----------------------------------------------------------------------------}
procedure Tform_NotaDeCobroDirecta.btnFacturarClick(Sender: TObject);
resourcestring
    MSG_ERROR_PROVEEDOR			= 'Debe seleccionar un Proveedor DayPass';
    MSG_ERROR_CONCESIONARIA     = 'Debe seleccionar una concesionaria';                                //SS-985-NDR-20110920
    MSG_ERROR_FECHA_HASTA		= 'La Fecha Hasta no es v�lida';
    MSG_ERROR_FECHA_SUPERIOR	= 'La fecha de emisi�n debe ser superior a la fecha de corte';
    MSG_ERROR_TIPO_DAYPASS		= 'Debe seleccionar al menos un tipo de pase diario';
    MSG_ERROR_EXISTE_PDU_NUEVO	= 'No es posible emitir el comprobante para PDU porque ya existen comprobantes posteriores a la fecha indicada.';
    MSG_ERROR_EXISTE_BHTU_NUEVO	= 'No es posible emitir el comprobante para BHTU porque ya existen comprobantes posteriores a la fecha indicada.';
    MSG_ERROR_CREAR_NQ			= 'Error al crear la Nota de Cobro Electr�nica';
    MSG_NQ_OK					= 'La(s) Nota(s) de Cobro Directa se generaron correctamente';
    MSG_ERROR					= 'Error';
    //Revision 3
    MSG_ERROR_EN_FECHA			= 'No puede generar una nota de cobro con fecha de emisi�n posterior a la fecha de hoy';

Const
    STR_WANT_TO_BEGIN_THE_INVOICING = 'Desea comenzar la facturaci�n?';
var
    Error : String;
    FechaDeHoy : TDateTime;
begin
    //Validamos los datos y tipos de pases diarios
    if not ValidateControls(	[   cbNombreClienteNQ,
                                    cbConcesionaria,                                //SS-985-NDR-20110920
                                    deFechaHasta,
                                    deFechaEmision,
                                    rbtnPDU//,
                                    //cbConcesionariaFac                              //SS-985-NDR-20110920
                                ],
      							[cbNombreClienteNQ.ItemIndex >= 0,
                                    cbConcesionaria.ItemIndex > 0,                  //SS-985-NDR-20110920
       							UtilProc.IsValidDate(DateToStr(deFechaHasta.Date)),
       							DeFechaHasta.Date <= deFechaEmision.Date,
                                    rbtnPDU.Checked or rbtnBHTU.Checked//,
                                    //cbConcesionariaFac.ItemIndex>0                  //SS-985-NDR-20110920
                                ],
                                MSG_ERROR,
                                [   MSG_ERROR_PROVEEDOR,
                                    MSG_ERROR_CONCESIONARIA,                        //SS-985-NDR-20110920
                                    MSG_ERROR_FECHA_HASTA,
                                    MSG_ERROR_FECHA_SUPERIOR,
                                    MSG_ERROR_TIPO_DAYPASS//,
                                    //MSG_ERROR_CONCESIONARIA                        //SS-985-NDR-20110920
                               ]
                            ) then Exit;

    {Revision 3: valido la fecha de emisi�n}
	FechaDeHoy := Trunc(QueryGetValueDateTime(DMConnections.BaseCAC, 'SELECT GETDATE()'));
    if not ValidateControls([deFechaEmision], [deFechaEmision.Date < FechaDeHoy + 1],
    						Caption, [MSG_ERROR_EN_FECHA]) then Exit;

     //Verificar si hay una Nota de Cobro Directa para PDU o BHTU m�s Nueva.
    ExisteComprobanteMasNuevo.Parameters.ParamByName('@FechaEmision').Value := deFechaEmision.Date;
    ExisteComprobanteMasNuevo.Parameters.ParamByName('@RutProveedorDayPass').Value := Trim(StrRight(cbNombreClienteNQ.Text,200));
    ExisteComprobanteMasNuevo.Parameters.ParamByName('@Return_Value').Value := null;
    ExisteComprobanteMasNuevo.CommandTimeout := 500;

    if rbtnPDU.Checked then ExisteComprobanteMasNuevo.Parameters.ParamByName('@TipoComprobante').Value := TC_NOTA_COBRO_DIRECTA
    else ExisteComprobanteMasNuevo.Parameters.ParamByName('@TipoComprobante').Value := TC_NOTA_COBRO_DIRECTA_BHTU;

    ExisteComprobanteMasNuevo.ExecProc;
    if ExisteComprobanteMasNuevo.Parameters.ParamByName('@Return_Value').Value then begin
        if rbtnPDU.Checked then MsgBox(MSG_ERROR_EXISTE_PDU_NUEVO, Caption, MB_ICONEXCLAMATION)
        else MsgBox(MSG_ERROR_EXISTE_BHTU_NUEVO, Caption, MB_ICONEXCLAMATION);

        Exit;
    end;

    //Consulto al operador si desea comenzar la facturaci�n
    if MsgBox(STR_WANT_TO_BEGIN_THE_INVOICING, Caption, MB_ICONWARNING or MB_YESNO) = IDNO then begin
        Exit;
    end;

    //Generamos la Nota de Cobro
    if not GenerarNotasDeCobroNQ(Error) then begin
        //Informo que hubo un error al crear la nota de cobro
        MsgBoxErr(MSG_ERROR_CREAR_NQ, Error, MSG_ERROR , MB_ICONERROR);
    end else begin
        //Informo que la nota de cobro se creo con exito
        MsgBox(MSG_NQ_OK, Caption, MB_ICONINFORMATION);
    end;        
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author: lgisuk
  Date Created:  20/03/06
  Description:  Permito cerrar el formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_NotaDeCobroDirecta.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author: lgisuk
  Date Created: 20/03/06
  Description: Libero el formulario de memoria
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure Tform_NotaDeCobroDirecta.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;


end.
