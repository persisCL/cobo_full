object FormCuentasContables: TFormCuentasContables
  Left = 241
  Top = 174
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Cuentas Contables'
  ClientHeight = 362
  ClientWidth = 584
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 323
    Width = 584
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 262
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object Lista: TAbmList
    Left = 0
    Top = 33
    Width = 584
    Height = 156
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'126'#0'C'#243'digo Cuenta.               '
      #0'123'#0'Cuenta Externa.             '
      #0'67'#0'Descripci'#243'n.')
    HScrollBar = True
    RefreshTime = 10
    Table = CuentasContables
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaClick
    OnProcess = ListaProcess
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
    OnInsert = ListaInsert
    OnDelete = ListaDelete
    OnEdit = ListaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 584
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object GroupB: TPanel
    Left = 0
    Top = 189
    Width = 584
    Height = 134
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object lblNombre: TLabel
      Left = 9
      Top = 85
      Width = 48
      Height = 13
      Caption = '&Nombre:'
      FocusControl = edNombre
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 9
      Top = 22
      Width = 91
      Height = 13
      Caption = '&C'#243'digo cuenta: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 10
      Top = 54
      Width = 95
      Height = 13
      Caption = 'C&uenta externa: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edNombre: TEdit
      Left = 108
      Top = 81
      Width = 315
      Height = 21
      MaxLength = 50
      TabOrder = 2
    end
    object neCodigoCuentaContable: TNumericEdit
      Left = 110
      Top = 18
      Width = 121
      Height = 21
      Color = clSkyBlue
      Enabled = False
      MaxLength = 12
      TabOrder = 0
    end
    object neCodigoExternoCuentaContable: TNumericEdit
      Left = 109
      Top = 50
      Width = 121
      Height = 21
      MaxLength = 12
      TabOrder = 1
    end
  end
  object CuentasContables: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'CuentasContables'
    Left = 316
    Top = 75
    object CuentasContablesCodigoCuentaContable: TAutoIncField
      FieldName = 'CodigoCuentaContable'
      ReadOnly = True
    end
    object CuentasContablesCodigoExternoCuentaContable: TLargeintField
      FieldName = 'CodigoExternoCuentaContable'
    end
    object CuentasContablesNombre: TStringField
      FieldName = 'Nombre'
      Size = 50
    end
    object CuentasContablesFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object CuentasContablesUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object CuentasContablesFechaHoraActualizacion: TDateTimeField
      FieldName = 'FechaHoraActualizacion'
    end
    object CuentasContablesUsuarioActualizacion: TStringField
      FieldName = 'UsuarioActualizacion'
      FixedChar = True
    end
  end
end
