object frmDetalleViajes: TfrmDetalleViajes
  Left = 544
  Top = 253
  Width = 182
  Height = 107
  Caption = 'ReporteDetalle'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object rbi_FacturacionDetallada: TRBInterface
    Report = rp_FacturacionDetallada
    Caption = 'Configurar Impresi'#243'n de Facturas'
    OrderIndex = 0
    Left = 8
    Top = 40
  end
  object rp_FacturacionDetallada: TppReport
    AutoStop = False
    DataPipeline = plDetalle
    PassSetting = psTwoPass
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 0
    PrinterSetup.mmMarginLeft = 0
    PrinterSetup.mmMarginRight = 0
    PrinterSetup.mmMarginTop = 0
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Template.FileName = 'C:\TEMP\aaa.rtm'
    DeviceType = 'Screen'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    Left = 40
    Top = 40
    Version = '9.02'
    mmColumnWidth = 0
    DataPipelineName = 'plDetalle'
    object ppHeaderBand2: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 40217
      mmPrintPosition = 0
      object ppLabel21: TppLabel
        UserName = 'Label4'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Fecha:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 180182
        mmTop = 6350
        mmWidth = 10848
        BandType = 0
      end
      object ppLabel34: TppLabel
        UserName = 'Label1'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Detalle de Viajes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 16
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 6773
        mmLeft = 83615
        mmTop = 794
        mmWidth = 44704
        BandType = 0
      end
      object ppSystemVariable1: TppSystemVariable
        UserName = 'SystemVariable1'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        VarType = vtPageSetDesc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4233
        mmLeft = 180182
        mmTop = 1588
        mmWidth = 29898
        BandType = 0
      end
      object ppLabel17: TppLabel
        UserName = 'Label17'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'N'#250'mero de RUT:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4022
        mmLeft = 1058
        mmTop = 16669
        mmWidth = 26670
        BandType = 0
      end
      object ppDBText12: TppDBText
        UserName = 'DBText12'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'NumeroDocumentoFormateado'
        DataPipeline = ppDatosCliente
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppDatosCliente'
        mmHeight = 4233
        mmLeft = 29104
        mmTop = 16669
        mmWidth = 55033
        BandType = 0
      end
      object ppLabel19: TppLabel
        UserName = 'Label2'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Apellido y Nombres o Raz'#243'n Social:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 23019
        mmWidth = 56356
        BandType = 0
      end
      object ppLabel20: TppLabel
        UserName = 'Label20'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4233
        mmLeft = 1058
        mmTop = 28311
        mmWidth = 15346
        BandType = 0
      end
      object ppLabel23: TppLabel
        UserName = 'Label23'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Comuna:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 1058
        mmTop = 33338
        mmWidth = 14288
        BandType = 0
      end
      object ppDBText15: TppDBText
        UserName = 'DBText102'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'DescripcionComuna'
        DataPipeline = ppDatosCliente
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppDatosCliente'
        mmHeight = 4233
        mmLeft = 19050
        mmTop = 33338
        mmWidth = 46567
        BandType = 0
      end
      object lApellidoNombreDet: TppLabel
        UserName = 'lApellidoNombre1'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lApellidoNombre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 60061
        mmTop = 23019
        mmWidth = 28448
        BandType = 0
      end
      object ppDBText18: TppDBText
        UserName = 'DBText18'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'DescripcionRegion'
        DataPipeline = ppDatosCliente
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppDatosCliente'
        mmHeight = 4233
        mmLeft = 86784
        mmTop = 33338
        mmWidth = 42333
        BandType = 0
      end
      object ppLabel25: TppLabel
        UserName = 'Label25'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Regi'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 73554
        mmTop = 33338
        mmWidth = 12171
        BandType = 0
      end
      object ppLine2: TppLine
        UserName = 'Line2'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        ParentWidth = True
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 0
        mmTop = 14552
        mmWidth = 210000
        BandType = 0
      end
      object ppSystemVariable2: TppSystemVariable
        UserName = 'SystemVariable2'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DisplayFormat = 'dd/mm/yyyy'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 4064
        mmLeft = 192352
        mmTop = 6615
        mmWidth = 17611
        BandType = 0
      end
      object lDomicilio: TppLabel
        UserName = 'Label3'
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'lDomicilio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 19050
        mmTop = 28310
        mmWidth = 137319
        BandType = 0
      end
    end
    object ppDetailBand2: TppDetailBand
      mmBottomOffset = 0
      mmHeight = 4763
      mmPrintPosition = 0
      object ppDBText4: TppDBText
        UserName = 'DBText2'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'NumeroViaje'
        DataPipeline = plDetalle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        ParentDataPipeline = False
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 3969
        mmLeft = 3175
        mmTop = 265
        mmWidth = 20108
        BandType = 4
      end
      object ppDBText5: TppDBText
        UserName = 'DBText1'
        AutoSize = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'FechaHoraInicio'
        DataPipeline = plDetalle
        DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 26458
        mmTop = 265
        mmWidth = 24606
        BandType = 4
      end
      object ppDBText6: TppDBText
        UserName = 'DBText3'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Importe'
        DataPipeline = plDetalle
        DisplayFormat = '#######0;(#######0)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 3969
        mmLeft = 189971
        mmTop = 265
        mmWidth = 17198
        BandType = 4
      end
      object ppDBText7: TppDBText
        UserName = 'DBText4'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Patente'
        DataPipeline = plDetalle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 61648
        mmTop = 265
        mmWidth = 17198
        BandType = 4
      end
      object ppDBText9: TppDBText
        UserName = 'DBText9'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'PuntosCobro'
        DataPipeline = plDetalle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 125942
        mmTop = 265
        mmWidth = 56621
        BandType = 4
      end
      object ppDBText8: TppDBText
        UserName = 'DBText8'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Categoria'
        DataPipeline = plDetalle
        DisplayFormat = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 80169
        mmTop = 265
        mmWidth = 17198
        BandType = 4
      end
      object ppDBText1: TppDBText
        UserName = 'DBText5'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Concesionaria'
        DataPipeline = plDetalle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'plDetalle'
        mmHeight = 4233
        mmLeft = 98425
        mmTop = 265
        mmWidth = 26723
        BandType = 4
      end
    end
    object ppFooterBand2: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 15610
      mmPrintPosition = 0
      object ppLine6: TppLine
        UserName = 'Line3'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        ParentWidth = True
        Weight = 0.750000000000000000
        mmHeight = 3175
        mmLeft = 0
        mmTop = 0
        mmWidth = 210000
        BandType = 8
      end
    end
    object ppGroup2: TppGroup
      BreakType = btCustomField
      KeepTogether = True
      OutlineSettings.CreateNode = True
      UserName = 'Group1'
      mmNewColumnThreshold = 0
      mmNewPageThreshold = 0
      DataPipelineName = ''
      object ppGroupHeaderBand2: TppGroupHeaderBand
        mmBottomOffset = 0
        mmHeight = 6615
        mmPrintPosition = 0
        object ppRegion3: TppRegion
          UserName = 'Region1'
          Brush.Color = 15263976
          ParentHeight = True
          ParentWidth = True
          mmHeight = 6615
          mmLeft = 0
          mmTop = 0
          mmWidth = 210000
          BandType = 3
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          object ppLabel37: TppLabel
            UserName = 'Label5'
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Nro. de Viaje'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 1323
            mmTop = 1323
            mmWidth = 21960
            BandType = 3
            GroupNo = 0
          end
          object ppLabel38: TppLabel
            UserName = 'Label6'
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Fecha / Hora Inicio'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 26723
            mmTop = 1323
            mmWidth = 31750
            BandType = 3
            GroupNo = 0
          end
          object ppLabel39: TppLabel
            UserName = 'Label7'
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Veh'#237'culo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 61913
            mmTop = 1323
            mmWidth = 14817
            BandType = 3
            GroupNo = 0
          end
          object ppLabel40: TppLabel
            UserName = 'Label8'
            AutoSize = False
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Importe'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taRightJustified
            Transparent = True
            mmHeight = 4233
            mmLeft = 185473
            mmTop = 1323
            mmWidth = 22225
            BandType = 3
            GroupNo = 0
          end
          object ppLabel13: TppLabel
            UserName = 'Label13'
            AutoSize = False
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Categor'#237'a'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taRightJustified
            Transparent = True
            mmHeight = 4233
            mmLeft = 79904
            mmTop = 1323
            mmWidth = 17727
            BandType = 3
            GroupNo = 0
          end
          object ppLabel22: TppLabel
            UserName = 'Label22'
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Puntos de Cobro Recorridos'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 126207
            mmTop = 1323
            mmWidth = 48419
            BandType = 3
            GroupNo = 0
          end
          object ppLabel14: TppLabel
            UserName = 'Label14'
            AutoSize = False
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Concesionaria'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taCentered
            Transparent = True
            mmHeight = 4233
            mmLeft = 98425
            mmTop = 1323
            mmWidth = 26988
            BandType = 3
            GroupNo = 0
          end
        end
      end
      object ppGroupFooterBand2: TppGroupFooterBand
        mmBottomOffset = 0
        mmHeight = 6350
        mmPrintPosition = 0
        object ppRegion4: TppRegion
          UserName = 'Region2'
          Brush.Color = 15329769
          ParentWidth = True
          mmHeight = 6350
          mmLeft = 0
          mmTop = 0
          mmWidth = 210000
          BandType = 5
          GroupNo = 0
          mmBottomOffset = 0
          mmOverFlowOffset = 0
          mmStopPosition = 0
          object ppLabel41: TppLabel
            UserName = 'Label10'
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            Caption = 'Total:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            Transparent = True
            mmHeight = 4233
            mmLeft = 170392
            mmTop = 1058
            mmWidth = 9525
            BandType = 5
            GroupNo = 0
          end
          object ppDBCalc2: TppDBCalc
            UserName = 'DBCalc1'
            Border.BorderPositions = []
            Border.Color = clBlack
            Border.Style = psSolid
            Border.Visible = False
            Border.Weight = 1.000000000000000000
            DataField = 'Importe'
            DataPipeline = plDetalle
            DisplayFormat = '#######0;(#######0)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Name = 'Arial'
            Font.Size = 10
            Font.Style = [fsBold]
            TextAlignment = taRightJustified
            Transparent = True
            DataPipelineName = 'plDetalle'
            mmHeight = 4233
            mmLeft = 185209
            mmTop = 1058
            mmWidth = 22225
            BandType = 5
            GroupNo = 0
          end
        end
      end
    end
  end
  object plDetalle: TppDBPipeline
    DataSource = dsFacturacionDetallada
    OpenDataSource = False
    UserName = 'plDetalle'
    Left = 72
    Top = 40
    object ppConcesionaria: TppField
      FieldAlias = 'Concesionaria'
      FieldName = 'Concesionaria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object ppNumeroViaje: TppField
      FieldAlias = 'NumeroViaje'
      FieldName = 'NumeroViaje'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object ppFechaHoraInicio: TppField
      FieldAlias = 'FechaHoraInicio'
      FieldName = 'FechaHoraInicio'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object ppImporte: TppField
      FieldAlias = 'Importe'
      FieldName = 'Importe'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object ppPatente: TppField
      FieldAlias = 'Patente'
      FieldName = 'Patente'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object ppCategoria: TppField
      FieldAlias = 'Categoria'
      FieldName = 'Categoria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
    object ppPuntosCobro: TppField
      FieldAlias = 'PuntosCobro'
      FieldName = 'PuntosCobro'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 6
      Searchable = False
      Sortable = False
    end
  end
  object ObtenerFacturacionDetallada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerFacturacionDetallada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end>
    Left = 136
    Top = 40
  end
  object dsFacturacionDetallada: TDataSource
    DataSet = ObtenerFacturacionDetallada
    Left = 104
    Top = 40
  end
  object ppDatosCliente: TppDBPipeline
    DataSource = dsDatosCliente
    OpenDataSource = False
    UserName = 'DatosCliente'
    Left = 72
    Top = 8
  end
  object dsDatosCliente: TDataSource
    DataSet = ObtenerDatosCliente
    Left = 104
    Top = 8
  end
  object ObtenerDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 8
    object ObtenerDatosClienteCodigoPersona: TAutoIncField
      FieldName = 'CodigoPersona'
      ReadOnly = True
    end
    object ObtenerDatosClienteCodigoDocumento: TStringField
      FieldName = 'CodigoDocumento'
      FixedChar = True
      Size = 4
    end
    object ObtenerDatosClienteNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object ObtenerDatosClienteNumeroDocumentoFormateado: TStringField
      FieldName = 'NumeroDocumentoFormateado'
      ReadOnly = True
      FixedChar = True
      Size = 12
    end
    object ObtenerDatosClientePersoneria: TStringField
      FieldName = 'Personeria'
      FixedChar = True
      Size = 1
    end
    object ObtenerDatosClientePersoneriaDescripcion: TStringField
      FieldName = 'PersoneriaDescripcion'
      ReadOnly = True
      Size = 100
    end
    object ObtenerDatosClienteNombre: TStringField
      FieldName = 'Nombre'
      ReadOnly = True
      Size = 403
    end
    object ObtenerDatosClienteDescripcionDomicilio: TStringField
      FieldName = 'DescripcionDomicilio'
      ReadOnly = True
      Size = 255
    end
    object ObtenerDatosClienteCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      Size = 10
    end
    object ObtenerDatosClienteDescripcionComuna: TStringField
      FieldName = 'DescripcionComuna'
      ReadOnly = True
      FixedChar = True
      Size = 100
    end
    object ObtenerDatosClienteDescripcionRegion: TStringField
      FieldName = 'DescripcionRegion'
      ReadOnly = True
      FixedChar = True
      Size = 100
    end
  end
end
