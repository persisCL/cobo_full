object VentanaConveniosEnLegales: TVentanaConveniosEnLegales
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Cambio Estado Masivo'
  ClientHeight = 200
  ClientWidth = 290
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 5
    Top = 3
    Width = 278
    Height = 52
    Caption = 
      'Los convenios seleccionados fueron cambiados de estado exitosame' +
      'nte, entre ellos se detect'#243' que los convenios listados a continu' +
      'aci'#243'n estaban en Carpetas Legales por lo cual fueron sacados de ' +
      'Carpetas Legales.'
    WordWrap = True
  end
  object lstConvenios: TListBox
    Left = 5
    Top = 60
    Width = 279
    Height = 101
    ItemHeight = 13
    TabOrder = 0
  end
  object btnCerrar: TButton
    Left = 98
    Top = 167
    Width = 97
    Height = 25
    Align = alCustom
    Caption = 'Cerrar'
    TabOrder = 1
    OnClick = btnCerrarClick
  end
end
