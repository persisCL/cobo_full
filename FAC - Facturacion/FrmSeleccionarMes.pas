unit FrmSeleccionarMes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls,
  UtilProc, ExtCtrls, VariantComboBox;

type
  TFormSeleccionarMes = class(TForm)
    lbl_Titulo: TLabel;
    Bevel1: TBevel;
    cb_Mes: TVariantComboBox;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
  private
    { Private declarations }
    // Para mantener el mes seleccionado por el operador
    FMesSeleccionado: ShortInt;
    function ObtenerMesStr(NroMes: Word): AnsiString;
  public
    { Public declarations }
    function Inicializar(Mes: Word): Boolean;
    function ObtenerMesSeleccionado: ShortInt;
  end;

var
  FormSeleccionarMes: TFormSeleccionarMes;

implementation

{$R *.dfm}

procedure TFormSeleccionarMes.btn_CancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TFormSeleccionarMes.btn_AceptarClick(Sender: TObject);
begin
    FMesSeleccionado := cb_Mes.Value;
    Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormSeleccionarMes.Inicializar
  Author:    ggomez
  Date:      26-Ene-2005
  Arguments: Mes: Word. Mes a partir del cual se llena el combo cb_Mes.
  Result:    Boolean
-----------------------------------------------------------------------------}
function TFormSeleccionarMes.Inicializar(Mes: Word): Boolean;
var
    i: Integer;
begin
    Result := False;

    if not Mes in [1..12] then Exit;

    FMesSeleccionado := -1;

    (* Llenar el combo con los meses a partir del mes pasado como par�metro. *)
    cb_Mes.Items.Clear;
    for i := Mes to 12 do begin
        cb_Mes.Items.Add(ObtenerMesStr(i), i);
    end;
    cb_Mes.ItemIndex := 0;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormSeleccionarMes.ObtenerMesStr
  Author:    ggomez
  Date:      26-Ene-2005
  Arguments: NroMes: Word
  Result:    AnsiString
  Description: Dado un N� de mes retorna el nombre del mes. Si no es un N� de
    mes v�lido, retorna -1.
-----------------------------------------------------------------------------}
function TFormSeleccionarMes.ObtenerMesStr(NroMes: Word): AnsiString;
resourcestring
    MSG_ENERO = 'Enero';
    MSG_FEBRERO = 'Febrero';
    MSG_MARZO = 'Marzo';
    MSG_ABRIL = 'Abril';
    MSG_MAYO = 'Mayo';
    MSG_JUNIO = 'Junio';
    MSG_JULIO = 'Julio';
    MSG_AGOSTO = 'Agosto';
    MSG_SEPTIEMBRE = 'Septiembre';
    MSG_OCTUBRE = 'Octubre';
    MSG_NOVIEMBRE = 'Noviembre';
    MSG_DICIEMBRE = 'Diciembre';
    MSG_DESCONOCIDO = '---';
begin
    case NroMes of
        1: Result := MSG_ENERO;
        2: Result := MSG_FEBRERO;
        3: Result := MSG_MARZO;
        4: Result := MSG_ABRIL;
        5: Result := MSG_MAYO;
        6: Result := MSG_JUNIO;
        7: Result := MSG_JULIO;
        8: Result := MSG_AGOSTO;
        9: Result := MSG_SEPTIEMBRE;
        10: Result := MSG_OCTUBRE;
        11: Result := MSG_NOVIEMBRE;
        12: Result := MSG_DICIEMBRE;
        else Result := MSG_DESCONOCIDO;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: TFormSeleccionarMes.ObtenerMesSeleccionado
  Author:    ggomez
  Date:      26-Ene-2005
  Arguments: None
  Result:    Word. Retorna l valor de la variable FMesSeleccionado.
-----------------------------------------------------------------------------}
function TFormSeleccionarMes.ObtenerMesSeleccionado: ShortInt;
begin
    Result := FMesSeleccionado;
end;

end.
