{-------------------------------------------------------------------------------
    File Name      : frm_ImprimirNotaCobroMorosos.pas
    Author         : CQuezada
    Date Created   : 10/12/2012
    Language       : ES-CL
    Firma          : SS_660_CQU_20121010
    Description    : Permite imprimir las facturas asociadas al Proceso de Facturaci�n seleccionado
 -------------------------------------------------------------------------------}
unit frm_ImprimirNotaCobroMorosos;

interface

uses
  DMConnection, PeaTypes, PeaProcs, Util, UtilProc, Windows, Messages, SysUtils,
  Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ListBoxEx,
  DBListEx, DB, ADODB, VariantComboBox, UtilDB, SysUtilsCN,
  frmReporteBoletaFacturaElectronica, ReporteFactura,
  frmRptDetInfraccionesAnuladas, ExtCtrls, frmReporteFacturacionDetallada;

type
  Tform_ImprimirNotaCobroMorosos = class(TForm)
    spObtenerNotasCobroPorProcesoFacturacion: TADOStoredProc;
    dsObtenerNotasCobroPorProcesoFacturacion: TDataSource;
    pnlSuperior: TPanel;
    pnlCentral: TPanel;
    pnlInferior: TPanel;
    lblMensaje: TLabel;
    dblNotasCobroMorosos: TDBListEx;
    btnImprimirComprobante: TButton;
    btnImprimirDetalle: TButton;
    btnCerrar: TButton;
    procedure btnImprimirDetalleClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCerrarClick(Sender: TObject);
    procedure btnImprimirComprobanteClick(Sender: TObject);
    procedure OrdenarPorColumna(Sender: TObject);
    procedure spObtenerNotasCobroPorProcesoFacturacionAfterScroll(DataSet: TDataSet);
    procedure dblNotasCobroMorososDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
  private
    { Private declarations }
    TipoComprobante : string;
    NroComprobante : int64;
    procedure LiberarMemoria;
  public
    { Public declarations }
    function Inicializar(NumeroProceso : Int64) : Boolean;
  end;

var
  form_ImprimirNotaCobroMorosos: Tform_ImprimirNotaCobroMorosos;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name : spObtenerNotasCobroPorProcesoFacturacionAfterScroll
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Inicializo el formulario
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
function Tform_ImprimirNotaCobroMorosos.Inicializar;
resourcestring
    MSG_TITULO  =   'No se pudo iniciar %s';
    MSG_VACIO   =   'No se encontraron datos para el Numero de Proceso de Facturaci�n indicado (%d)';
begin
    btnImprimirComprobante.Enabled := False;
    btnImprimirDetalle.Enabled     := False;

    try
        Result := True;
        with spObtenerNotasCobroPorProcesoFacturacion do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroProceso').Value := NumeroProceso;
            Open;
            if IsEmpty then begin
                MsgBox(Format(MSG_VACIO, [NumeroProceso]),
                    Format(MSG_TITULO, [caption]),
                    MB_ICONQUESTION + MB_OK);
                Result := False;
                btnCerrar.Click;
            end else
                btnImprimirComprobante.Enabled := True;
        end;
    except
        Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : OrdenarPorColumna
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Ordena los datos de la TDBListEx de acuerdo a la columna que seleccione el usuario
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.OrdenarPorColumna(Sender: TObject);
begin
	if not spObtenerNotasCobroPorProcesoFacturacion.Active then Exit
    else
        OrdenarGridPorColumna(	TDBListExColumn(Sender).Columns.List,
                                TDBListExColumn(Sender),
                                TDBListExColumn(Sender).FieldName);
end;

{-----------------------------------------------------------------------------
  Function Name : spObtenerNotasCobroPorProcesoFacturacionAfterScroll
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Habilito o Deshabilito el bot�n de detalle
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.spObtenerNotasCobroPorProcesoFacturacionAfterScroll(DataSet: TDataSet);
begin
    if spObtenerNotasCobroPorProcesoFacturacion.Active then
        btnImprimirDetalle.Enabled := (spObtenerNotasCobroPorProcesoFacturacion.FieldByName('EstadoPago').AsString <> 'A');
end;

{-----------------------------------------------------------------------------
  Function Name : btnImprimirComprobanteClick
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Imprimo la Boleta
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.btnImprimirComprobanteClick(Sender: TObject);
var
    f : TReporteBoletaFacturaElectronicaForm;
    sError : string;
begin
    Screen.Cursor := crHourGlass;
    try
        LiberarMemoria;

        f := TReporteBoletaFacturaElectronicaForm.Create(nil);

        TipoComprobante := spObtenerNotasCobroPorProcesoFacturacion.FieldByName('TipoComprobante').AsString;
        NroComprobante  := spObtenerNotasCobroPorProcesoFacturacion.FieldByName('NumeroComprobante').AsInteger;

        if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, sError, True) then begin
            f.Ejecutar;
        end;

        if Assigned(f) then FreeAndNil(f);
    except
        on e : exception do begin
            MsgBoxErr('Error', e.Message, Caption, MB_ICONSTOP);
        end;
    end;

    Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name : btnImprimirDetalleClick
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Imprime el detalle de la NK
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.btnImprimirDetalleClick(Sender: TObject);
resourcestring
	MSG_ERROR_GETTING_DETAIL 	= 'Error imprimiendo Facturaci�n Detallada';
var
    ErrorNormal, ErrorInfraccion, Error : String;
    detInf : TRptDetInfraccionesAnuladas;
    detNor : TformReporteFacturacionDetallada;
begin
    try
        TipoComprobante := spObtenerNotasCobroPorProcesoFacturacion.FieldByName('TipoComprobante').AsString;
        NroComprobante  := spObtenerNotasCobroPorProcesoFacturacion.FieldByName('NumeroComprobante').AsInteger;

        detNor := TFormReporteFacturacionDetallada.Create(nil);
        detInf := TRptDetInfraccionesAnuladas.Create(nil);
        try
            if detNor.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, ErrorNormal) then begin
                if not detNor.Ejecutar then MsgBox(Trim(ErrorNormal), Caption, MB_ICONERROR )
                else if not detInf.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, ErrorInfraccion)
                    then MsgBox(Trim(ErrorInfraccion), Caption, MB_ICONERROR );

            end else MsgBox(MSG_ERROR_GETTING_DETAIL, Caption, MB_ICONERROR);

        finally
            if Assigned(detNor) then FreeAndNil(detNor);
            if Assigned(detInf) then FreeAndNil(detInf);
        end;
    except
        on e : exception do begin
            MsgBoxErr(MSG_ERROR_GETTING_DETAIL, e.Message, Caption, MB_ICONSTOP);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : btnCerrarClick
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Cierra el formulario
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name : FormClose
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Cierra el SP y libera la memoria
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    spObtenerNotasCobroPorProcesoFacturacion.Close;
    Action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name : dblNotasCobroMorososDrawText
  Author        : CQuezada
  Date Created  : 11-12-2012
  Description   : Formatea la fecha de emisi�n
  Firma         : SS_660_CQU_20121010
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.dblNotasCobroMorososDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if (Column.FieldName = 'FechaEmision') then
        Text := FormatDateTime('dd/mm/yyyy', spObtenerNotasCobroPorProcesoFacturacion.FieldByName('FechaEmision').AsDateTime);
end;

{-----------------------------------------------------------------------------
  Function Name : LiberarMemoria
  Author        : CQuezada
  Date Created  : 10/12/2012
  Description   : Libera la memoria
  Parameters    :
  Return Value  : Boolean
-----------------------------------------------------------------------------}
procedure Tform_ImprimirNotaCobroMorosos.LiberarMemoria;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
end;

end.
