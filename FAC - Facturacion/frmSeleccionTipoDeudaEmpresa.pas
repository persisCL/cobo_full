unit frmSeleccionTipoDeudaEmpresa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, ExtCtrls, DMConnection,RStrings,utildb,UtilProc,ADODB, PeaProcs;



type
  TSeleccionTipoDeudaEmpresaForm = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    lblMensaje: TLabel;
    lblEmpresaRecaudadora: TLabel;
    cbEmpresaRecaudadora: TVariantComboBox;
    cbTipoDeuda: TVariantComboBox;
    lbltipodeuda: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    function Inicializar(): Boolean;
    { Public declarations }
  end;

var
  SeleccionTipoDeudaEmpresaForm: TSeleccionTipoDeudaEmpresaForm;

implementation

{$R *.dfm}

procedure TSeleccionTipoDeudaEmpresaForm.btnAceptarClick(Sender: TObject);
ResourceString
  MSG_VALIDAR_CAPTION 		      = 'Validar datos';
	MSG_ACTUALIZAR_CAPTION		    = 'Empresa Recaudadora/ Tipo de Deuda';
  MSG_EMPRESARECAUDADORA_ERROR	= 'Debe seleccionar una Empresa Recaudadora';
  MSG_TIPODEUDA_ERROR		        = 'Debe Seleccionar un Tipo de Deuda';
begin


   	if (cbEmpresaRecaudadora.Value = 0) then begin
      MsgBoxBalloon( MSG_EMPRESARECAUDADORA_ERROR, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbEmpresaRecaudadora);
      Exit;
    end;
   	if (cbTipoDeuda.Value = 0) then begin
      MsgBoxBalloon( MSG_TIPODEUDA_ERROR, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbTipoDeuda);
      Exit;
    end;
    ModalResult := mrOK;
end;

procedure TSeleccionTipoDeudaEmpresaForm.btnCancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TSeleccionTipoDeudaEmpresaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author: mpiazza
  Date Created: 05/06/2009
  Description: Inicializa el formulario
  Parameters:
  Return Boolean

  Revision 1
    Author: mpiazza
    Date: 05/06/2009
    Description: SS 803
           - se le a�ade un parametro de filtro por campo judicial,
           True: trae solo empresas judiciales
           False: trae solo empresas NO judiciales
-----------------------------------------------------------------------------}
function TSeleccionTipoDeudaEmpresaForm.Inicializar: Boolean;
begin


  CargarTipoDeuda(DMConnections.BaseCAC,cbTipoDeuda,false);
  CargarEmpresaRecaudacionJudicial(DMConnections.BaseCAC,cbEmpresaRecaudadora,false,0);

end;

end.
