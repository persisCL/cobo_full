{********************************** File Header ********************************
File Name   : ArbolFAQ
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 08-Mar-2004
Language    : ES-AR
Description : Arbol FAQ
--------------------------------------------------------------------------------
Date		: 26-Abr-2004
Author		: Castro, Ra�l <rcastro@dpsautomation.com>
Description : Redise�o de estructura de �rbol para el manejo de D&D

Revision 2:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Autor       :   CQuezadaI
Fecha       :   17 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se agrega un mensaje cuando no se ha seleccionado la ruta para el nuevo Arbol.
*******************************************************************************}

unit ArbolFAQ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, DB, ADODB, Grids, DBGrids, DBCtrls, Mask, UtilProc,
  ExtCtrls, Buttons, DPSControls, RStrings, utildb,
  Menus, DPSGrid;

type
  TCodigo = Class(TObject)
  private
	FCodigoRef : Integer;
  public
	property CodigoRef: Integer read FCodigoRef write FCodigoRef;
  end;

  TfrmArbolFAQ = class(TForm)
	dsPreguntasFAQ: TDataSource;
	splArbol: TSplitter;
	ObtenerPreguntasFAQ: TADOStoredProc;
	panDerecho: TPanel;
	panPyR: TPanel;
	qryPreguntas: TADOQuery;
	dsPreguntas: TDataSource;
	dsTitulos: TDataSource;
	qryTitulos: TADOQuery;
	dsSubTitulos: TDataSource;
	qrySubTitulos: TADOQuery;
	panArbol: TPanel;
	ArbolFAQ: TTreeView;
	Splitter3: TSplitter;
	panListaPreguntas: TPanel;
	panABM: TPanel;
	panABArbol: TPanel;
	grdTitulos: TDPSGrid;
	Respuesta: TDBRichEdit;
	grdSubTitulos: TDPSGrid;
	grdPreguntas: TDPSGrid;
	ObtenerArbolFAQ: TADOStoredProc;
	spEliminarDeArbolFAQ: TADOStoredProc;
    spActualizarArbolFAQ: TADOStoredProc;
	grdFuentes: TDPSGrid;
	dsFuentes: TDataSource;
	qryFuentes: TADOQuery;
	qryTitulosCodigoTitulo: TIntegerField;
	qryTitulosTitulo: TStringField;
	qryTitulosCantidad: TIntegerField;
    spLimpiarOrdenFAQ: TADOStoredProc;
	spBorrarOrdenFAQ: TADOStoredProc;
    btnAgregarEnArbol: TButton;
    btnEliminarDeArbol: TButton;
    btnABMSubTitulos: TButton;
    btnABMPreguntas: TButton;
    btnABMTitulos: TButton;
	procedure ArbolFAQClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	function CrearArbol: Boolean;
	function EliminarArbol: Boolean;
	procedure btnABMTitulosClick(Sender: TObject);
	procedure btnABMSubTitulosClick(Sender: TObject);
	procedure EditarTitulos(Sender: TObject; Codigo: Integer);
	procedure EditarSubTitulos(Sender: TObject; Codigo: Integer);
	procedure EditarPreguntas(Sender: TObject; Codigo: Integer);
	procedure btnABMPreguntasClick(Sender: TObject);
	procedure btnEliminarDeArbolClick(Sender: TObject);
	procedure btnAgregarEnArbolClick(Sender: TObject);
	procedure ArbolFAQDragDrop(Sender, Source: TObject; X, Y: Integer);
	procedure ArbolFAQDragOver(Sender, Source: TObject; X, Y: Integer;
	  State: TDragState; var Accept: Boolean);
	function GrabarArbolEnArchivo (MoverNodos: Boolean; NodoNuevaPosicion: TTreeNode): Boolean;
    procedure grdPreguntasKeyPress(Sender: TObject; var Key: Char);
	procedure grdSubTitulosKeyPress(Sender: TObject; var Key: Char);
    procedure grdTitulosKeyPress(Sender: TObject; var Key: Char);
    procedure grdTitulosDblClick(Sender: TObject);
	procedure grdSubTitulosDblClick(Sender: TObject);
	procedure grdPreguntasDblClick(Sender: TObject);
  private
	{ Private declarations }
	function CodigoFuente (Nodo: TTreeNode): Integer;
	function CodigoTitulo (Nodo: TTreeNode): Integer;
	function CodigoSubTitulo (Nodo: TTreeNode): Integer;
	function CodigoPregunta (Nodo: TTreeNode): Integer;
  public
	{ Public declarations }
	function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  frmArbolFAQ: TfrmArbolFAQ;

implementation

uses  ABMPreguntasFAQ, ABMSubTitulosFAQ, ABMTitulosFAQ,
  frmMensaje3botones;

{$R *.dfm}

function TfrmArbolFAQ.Inicializar(MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
	Result := False;
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	if not OpenTables([qryFuentes, qryTitulos,qrySubTitulos,qryPreguntas]) then exit;
	result := CrearArbol And GrabarArbolEnArchivo (True, nil);
	if not Result then MsgBox(MSG_ERROR_NO_HAY_FAQ, self.Caption)
end;

function TfrmArbolFAQ.CrearArbol: Boolean;
var
	CodigoFuente,
	CodigoTitulo,
	CodigoSubTitulo,
	CodigoPregunta: Integer;

	SubNodo1,
	SubNodo2,
	SubNodo3: TTreeNode;

	Codigo : TCodigo;

begin
	EliminarArbol;
	ObtenerPreguntasFAQ.Close;
   	Screen.Cursor := crHourGlass;

	With ObtenerArbolFAQ Do Begin
		Open;

		qryFuentes.First;
		While Not qryFuentes.EoF Do Begin
			CodigoFuente := qryFuentes.FieldByName('CodigoFuenteSolicitud').AsInteger;
			Filter := 'CodigoFuenteSolicitud = ' + IntToStr (CodigoFuente);

			// Ra�z
			Codigo := TCodigo.Create;
			Codigo.CodigoRef := CodigoFuente;
			SubNodo1 := ArbolFAQ.Items.AddChildObject(nil, Trim(qryFuentes.FieldByName('Fuente').AsString), Codigo);

			// Nivel 1
			First;
			While (Not EoF) And (CodigoFuente = FieldByName('CodigoFuenteSolicitud').AsInteger) Do Begin
				CodigoTitulo := FieldByName('CodigoTitulo').AsInteger;

				Codigo := TCodigo.Create;
				Codigo.CodigoRef := CodigoTitulo;
				SubNodo2 := ArbolFAQ.Items.AddChildObject(SubNodo1, Trim(FieldByName('Titulo').AsString), Codigo);

				// Nivel 2
				While (Not EoF) And (CodigoTitulo = FieldByName('CodigoTitulo').AsInteger) Do Begin
					CodigoSubTitulo := FieldByName('CodigoSubTitulo').AsInteger;

					Codigo := TCodigo.Create;
					Codigo.CodigoRef := CodigoSubTitulo;
					SubNodo3 := ArbolFAQ.Items.AddChildObject(SubNodo2, Trim(FieldByName('SubTitulo').AsString), Codigo);

					// Nivel 3
					While (Not EoF) And	(CodigoSubTitulo = FieldByName('CodigoSubTitulo').AsInteger) Do Begin
						CodigoPregunta := FieldByName('CodigoPregunta').AsInteger;

						Codigo := TCodigo.Create;
						Codigo.CodigoRef := CodigoPregunta;

						ArbolFAQ.Items.AddChildObject(SubNodo3, Trim(FieldByName('Pregunta').AsString), Codigo);

						Next
					End
				End
			End;

			qryFuentes.Next
		End;
		qryFuentes.First;

		Close
	End;

   	Screen.Cursor := crDefault;

	Result := ArbolFAQ.Items.GetFirstNode <> nil;
	ArbolFAQ.FullExpand;
end;

function TfrmArbolFAQ.CodigoFuente (Nodo: TTreeNode): Integer;
begin
    case Nodo.level of
        0 : Result := TCodigo(Nodo.Data).CodigoRef;
        1 : Result := TCodigo(Nodo.Parent.Data).CodigoRef;
        2 : Result := TCodigo(Nodo.Parent.Parent.Data).CodigoRef;
        3 : Result := TCodigo(Nodo.Parent.Parent.Parent.Data).CodigoRef;
        else result := -1;
    end;
end;

function TfrmArbolFAQ.CodigoTitulo (Nodo: TTreeNode): Integer;
begin
    case Nodo.level of
        0: Result := -1;
    	1: Result := TCodigo(Nodo.Data).CodigoRef;
        2: Result := TCodigo(Nodo.Parent.Data).CodigoRef;
        3: Result := TCodigo(Nodo.Parent.Parent.Data).CodigoRef;
        else result := -1;
    end;
end;

function TfrmArbolFAQ.CodigoSubTitulo (Nodo: TTreeNode): Integer;
begin
    case Nodo.level of
        0: Result := -1;
    	1: Result := -1;
        2: Result := TCodigo(Nodo.Data).CodigoRef;
        3: Result := TCodigo(Nodo.Parent.Data).CodigoRef;
        else result := -1;
    end;
end;

function TfrmArbolFAQ.CodigoPregunta (Nodo: TTreeNode): Integer;
begin
    case Nodo.level of
        0: Result := -1;
    	1: Result := -1;
        2: Result := -1;
        3: Result := TCodigo(Nodo.Data).CodigoRef;
        else result := -1;
    end;
end;

procedure TfrmArbolFAQ.ArbolFAQClick(Sender: TObject);
begin
  	ObtenerPreguntasFAQ.Close;
	if Assigned(ArbolFAQ.Selected) and (ArbolFAQ.Selected.Level = 3) then begin
		ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoFuenteSolicitud').value 	:= CodigoFuente (ArbolFAQ.Selected);
		ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoTitulo').value 			:= CodigoTitulo (ArbolFAQ.Selected);
		ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoSubTitulo').value 		:= CodigoSubTitulo (ArbolFAQ.Selected);
		ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoPregunta').value 		:= CodigoPregunta (ArbolFAQ.Selected);
		ObtenerPreguntasFAQ.Open;
	end;
end;

procedure TfrmArbolFAQ.FormClose(Sender: TObject; var Action: TCloseAction);

begin
	EliminarArbol;

	qryPreguntas.Close;
	qryTitulos.Close;
	qrySubTitulos.Close;

	action := caFree;
end;

function TfrmArbolFAQ.EliminarArbol: Boolean;
var
	ItemArbol: TTreeNode;
begin
	// Primer nodo del �rbol
	ItemArbol := ArbolFAQ.Items.GetFirstNode;

	// Recorro hasta el final
	while ItemArbol <> nil do begin
		TCodigo(ItemArbol.Data).Free;

		// Pr�ximo nodo
		ItemArbol := ItemArbol.GetNext
	end;

	ArbolFAQ.Items.Clear;
	Result := True
end;

procedure TfrmArbolFAQ.EditarTitulos(Sender: TObject; Codigo: Integer);
var
	f: TfrmABMTitulosFAQ;
begin
	f := TfrmABMTitulosFAQ.Create (Self);
	f.Inicializar(Codigo);
	f.ShowModal;
	f.Release;

	qryTitulos.Close;
	qryTitulos.Open;
	CrearArbol;
end;

procedure TfrmArbolFAQ.EditarSubTitulos(Sender: TObject; Codigo: Integer);
var
	f: TfrmABMSubTitulosFAQ;
begin
	f := TfrmABMSubTitulosFAQ.Create (Self);
	f.Inicializar(Codigo);
	f.ShowModal;
	f.Release;

	qrySubTitulos.Close;
	qrySubTitulos.Open;
	CrearArbol;
end;

procedure TfrmArbolFAQ.EditarPreguntas(Sender: TObject; Codigo: Integer);
var
	f: TfrmABMPreguntasFAQ;
begin
	f := TfrmABMPreguntasFAQ.Create (Self);
	f.Inicializar(Codigo);
	f.ShowModal;
	f.Release;

	qryPreguntas.Close;
	qryPreguntas.Open;
	CrearArbol;
end;

procedure TfrmArbolFAQ.btnABMTitulosClick(Sender: TObject);
begin
	EditarTitulos(Sender, 0);
end;

procedure TfrmArbolFAQ.btnABMSubTitulosClick(Sender: TObject);
begin
	EditarSubTitulos(Sender, 0);
end;

procedure TfrmArbolFAQ.btnABMPreguntasClick(Sender: TObject);
begin
	EditarPreguntas(Sender, 0);
end;

procedure TfrmArbolFAQ.btnEliminarDeArbolClick(Sender: TObject);
var
	Item1,
	Item2,
	Item3: TTreeNode;
begin
	if Not Assigned(ArbolFAQ.Selected) then exit;

	TCodigo(ArbolFAQ.Selected.Data).Free;

	Item1 := ArbolFAQ.Selected.GetFirstChild;
	while Item1 <> nil do begin
		TCodigo(Item1.Data).Free;

		Item2 := Item1.GetFirstChild;
		while Item2 <> nil do begin
			TCodigo(Item2.Data).Free;

			Item3 := Item2.GetFirstChild;
			while Item3 <> nil do begin
				TCodigo(Item3.Data).Free;

				Item3 := Item3.GetNextSibling
			end;

			Item2.DeleteChildren;
			Item2 := Item2.GetNextSibling
		end;

		Item1.DeleteChildren;
		Item1 := Item1.GetNextSibling
	end;

	ArbolFAQ.Selected.DeleteChildren;
	ArbolFAQ.Selected.Delete;

	GrabarArbolEnArchivo (True, ArbolFAQ.Items.GetFirstNode);
	CrearArbol;
end;

procedure TfrmArbolFAQ.btnAgregarEnArbolClick(Sender: TObject);
resourcestring                                                                                            // SS_1147_CQU_20140714
    MSG_ARBOL = 'Para crear un �rbol primero debe seleccionar una Fuente, Titulo, Subtitulo y preg�nta.'; // SS_1147_CQU_20140714
var
	f,t,st,p : Integer;
begin
	Screen.Cursor := crHourGlass;

	If (grdFuentes.SelectedRows.Count > 0) And (grdTitulos.SelectedRows.Count > 0) And (grdSubTitulos.SelectedRows.Count > 0) And (grdPreguntas.SelectedRows.Count > 0) Then Begin
		For f := 0 To grdFuentes.SelectedRows.Count-1 Do Begin
			grdFuentes.DataSource.DataSet.GotoBookmark(pointer (grdFuentes.SelectedRows.Items[f]));

			For t := 0 To grdTitulos.SelectedRows.Count-1 Do Begin
				grdTitulos.DataSource.DataSet.GotoBookmark(pointer (grdTitulos.SelectedRows.Items[t]));

				For st := 0 To grdSubTitulos.SelectedRows.Count-1 Do Begin
					grdSubTitulos.DataSource.DataSet.GotoBookmark(pointer(grdSubTitulos.SelectedRows.Items[st]));

					For p := 0 To grdPreguntas.SelectedRows.Count-1 Do Begin
						grdPreguntas.DataSource.DataSet.GotoBookmark(pointer(grdPreguntas.SelectedRows.Items[p]));

						try
							with spActualizarArbolFAQ, Parameters do begin
								ParamByName('@CodigoFuente').Value		:= grdFuentes.DataSource.DataSet.FieldByName ('CodigoFuenteSolicitud').AsInteger;
								ParamByName('@CodigoTitulo').Value		:= grdTitulos.DataSource.DataSet.FieldByName ('CodigoTitulo').AsInteger;
								ParamByName('@CodigoSubtitulo').Value	:= grdSubTitulos.DataSource.DataSet.FieldByName ('CodigoSubTitulo').AsInteger;
								ParamByName('@CodigoPregunta').Value	:= grdPreguntas.DataSource.DataSet.FieldByName ('CodigoPregunta').AsInteger;
								ParamByName('@Orden').Value				:= 0;

								ExecProc
							end;
						except
							On E: EDataBaseError do
						end
					End
				End
			End
		End;

		// Deselecciono todo
		grdFuentes.SelectedRows.Clear;
		grdTitulos.SelectedRows.Clear;
		grdSubTitulos.SelectedRows.Clear;
		grdPreguntas.SelectedRows.Clear;

		CrearArbol;
	//End;                                                              // SS_1147_CQU_20140714
    end else MsgBox(MSG_ARBOL, Caption, MB_ICONINFORMATION + MB_OK);    // SS_1147_CQU_20140714

	Screen.Cursor := crDefault;
end;

procedure MoverNodoEnArbol (Arbol : TTreeView; NodoOrigen, NodoDestino : TTreeNode);

	procedure MoverNodo (NodoFijo, NodoOrigen, NodoDestino : TTreeNode);
	var
		NodoNuevo : TTreeNode;
	begin
		if (NodoDestino = nil) or (NodoOrigen = nil) then Exit;

		{ Creo un nuevo hijo }
		if nodoorigen.Level = nododestino.Level then
			NodoNuevo := Arbol.Items.AddChild (NodoDestino.Parent, NodoOrigen.Text)
		else
			NodoNuevo := Arbol.Items.AddChildFirst (NodoDestino, NodoOrigen.Text);
		NodoNuevo.Data := NodoOrigen.Data;

		{ Si el nodo origen tiene hijos, primero los muevo }
		if (NodoOrigen.HasChildren) then MoverNodo (NodoFijo, NodoOrigen.GetFirstChild, NodoNuevo );

		{ Move all hermanos, unless at original level }
		if (NodoFijo <> NodoOrigen) then MoverNodo (NodoFijo, NodoOrigen.GetNextSibling, NodoDestino);
	end;

begin
	{ Copio nodo origen y sus hijos }
	MoverNodo (NodoOrigen, NodoOrigen, NodoDestino);

	{ Borrar nodo origen }
	NodoOrigen.Delete;
end;

procedure TfrmArbolFAQ.ArbolFAQDragDrop(Sender, Source: TObject; X,Y: Integer);

	function YaExisteNodo (Nodo: TTreeNode; Referencia: String): Boolean;
	var
		NodoHermano : TTreeNode;
	begin
		if (Nodo = nil) then begin
			Result := False;
			Exit;
		end;

		if Nodo.Text = Referencia then begin
			// ya existe
			Result := true;
			Exit;
		end;

		// reviso los nodos hermanos previos
		NodoHermano := Nodo.GetPrevSibling;
		while NodoHermano <> nil do begin
			if NodoHermano.Text = Referencia then begin
				// ya existe
				Result := True;
				Exit
			end;

			NodoHermano := NodoHermano.GetPrevSibling
		end;

		// reviso los nodos hermanos posteriores
		NodoHermano := Nodo.GetNextSibling;
		while NodoHermano <> nil do begin
			if NodoHermano.Text = Referencia then begin
				// ya existe
				Result := True;
				Exit
			end;

			NodoHermano := NodoHermano.GetNextSibling
		end;

	   Result := False
	end;

var
	NodoInicial, NodoDestino, NewRaiz, NodoOrigen, Item1, Item2, Item3, NewItem1, NewItem2: TTreeNode;
	Respuesta: Integer;
	f3b : TfrmMsg3Botones;
	TipoNodo: String;
    codigo: TCodigo;
begin
    NodoInicial := nil; 
	with ArbolFAQ do begin
     	// d�nde voy a insertar
        NodoDestino := GetNodeAt (X, Y);

        // qu� voy a mover
        NodoOrigen := Selected;

        // no hay drag-drop
        // drag-drop sobre si mismo
        if (NodoDestino = nil) or (NodoDestino = NodoOrigen) then begin
        	EndDrag (false);
            Exit;
        end;

        // drag-drop en distinto nivel que no sea del tipo padre
        if (NodoDestino.Level < NodoOrigen.Level-1) then begin
        	ShowMessage('S�lo puede moverse a un nivel igual o inmediato superior');
            EndDrag (false);
            Exit;
        end;

        // drag-drop a un hijo
        if (NodoDestino <> NodoOrigen.Parent) And (NodoDestino.Level > NodoOrigen.Level) then begin
            ShowMessage('No puede moverse a un nivel inferior');
            EndDrag (false);
            Exit;
        end;

        // nodo ya existe en el destino
        if YaExisteNodo (NodoDestino.GetFirstChild, NodoOrigen.Text) then begin
            ShowMessage('El �tem ya existe en el nivel d�nde se quiere copiar/mover');
            EndDrag (false);
            Exit;
        end;

        Respuesta := mrOk;
        if (NodoDestino <> NodoOrigen.Parent) And (NodoDestino.Parent <> NodoOrigen.Parent) then begin
            case NodoOrigen.Level of
                0: TipoNodo:= 'Fuente';
                1: TipoNodo:= 'T�tulo';
                2: TipoNodo:= 'Subt�tulo';
                3: TipoNodo:= 'Pregunta/Respuesta';
            end;

            f3b := TfrmMsg3Botones.Create(Self);
            f3b.Inicializar('Confirmaci�n', TipoNodo + #13 + 'Qu� acci�n desea realizar?', 'Copiar', 'Mover', 'Cancelar', mrAll, mrOk, mrCancel);
            Respuesta := f3b.ShowModal;
            f3b.Release
        end;

        if (Respuesta <> mrCancel) then begin
            if (Respuesta = mrAll) then begin
                // Copiar el �rbol
                if (ArbolFAQ.Selected.Level = NodoDestino.Level) then begin
                    NodoDestino := NodoDestino.parent;
                end;

                Codigo := TCodigo.Create;
                Codigo.CodigoRef := TCodigo(NodoOrigen.data).CodigoRef;
                newRaiz := ArbolFAQ.Items.AddChildObject(NodoDestino, NodoOrigen.Text, Codigo);
                NodoInicial := NewRaiz;

                Item1 := NodoOrigen.GetFirstChild;
                while Item1 <> nil do begin
                    Codigo := TCodigo.Create;
                    Codigo.CodigoRef := TCodigo(Item1.data).CodigoRef;
                    NewItem1 := ArbolFAQ.Items.AddChildObject(NewRaiz, Item1.Text, Codigo);

                    Item2 := Item1.GetFirstChild;
                    while Item2 <> nil do begin
                        Codigo := TCodigo.Create;
                        Codigo.CodigoRef := TCodigo(Item2.data).CodigoRef;
                        NewItem2 := ArbolFAQ.Items.AddChildObject(NewItem1, Item2.Text, Codigo);

                        Item3 := Item2.GetFirstChild;
                        while Item3 <> nil do begin
                            Codigo := TCodigo.Create;
                            Codigo.CodigoRef := TCodigo(Item3.data).CodigoRef;
                            ArbolFAQ.Items.AddChildObject(NewItem2, Item3.Text, Codigo);

                            Item3 := Item3.GetNextSibling
                        end;

                        Item2 := Item2.GetNextSibling
                    end;

                    Item1 := Item1.GetNextSibling
                end;
            end else begin
                // Movemos un subarbol
                NodoInicial := NodoOrigen;
             	if (ArbolFAQ.Selected.Level - 1 = NodoDestino.Level) then begin
                    ArbolFAQ.Selected.MoveTo(NodoDestino, naAddChildFirst)
                end else begin
	                ArbolFAQ.Selected.MoveTo(NodoDestino, naInsert);
                end;
           end;
    	end
    end;

    if Respuesta <> mrCancel then
        GrabarArbolEnArchivo(Respuesta = mrOk, NodoInicial);
end;

function TfrmArbolFAQ.GrabarArbolEnArchivo (MoverNodos: Boolean; NodoNuevaPosicion: TTreeNode): Boolean;

var
	Orden : integer;
	ItemArbol: TTreeNode;
    aCodigoFuente, aCodigoTitulo, aCodigoSubTitulo, aCodigoPregunta: INteger;

begin
	Orden := 0;

	// poner orden en 0 si no es copia
//	If MoverNodos Then
    spLimpiarOrdenFAQ.ExecProc;

	// Primer nodo del �rbol
	ItemArbol := ArbolFAQ.Items.GetFirstNode;

	// Recorro hasta el final
	while ItemArbol <> nil do begin
		If ItemArbol.Level = 3 then begin
			Inc (Orden, 50);

			// actualizar
			With spActualizarArbolFAQ, Parameters do begin
				ParamByName('@CodigoFuente').value 		:= CodigoFuente (ItemArbol);
				ParamByName('@CodigoTitulo').value 	 	:= CodigoTitulo (ItemArbol);
				ParamByName('@CodigoSubTitulo').value 	:= CodigoSubTitulo (ItemArbol);
				ParamByName('@CodigoPregunta').value 	:= CodigoPregunta (ItemArbol);
				ParamByName('@Orden').Value				:= Orden;

				ExecProc
			end
		end;

		// Pr�ximo nodo
		ItemArbol := ItemArbol.GetNext
	end;

	// borrar los que tienen orden 0
	spBorrarOrdenFAQ.ExecProc;

	// reconstruir arbol
    if NodoNuevaPosicion <> nil then begin
        aCodigoFuente    := CodigoFuente(NodoNuevaPosicion);
        aCodigoTitulo    := CodigoTitulo(NodoNuevaPosicion);
        aCodigoSubTitulo := CodigoSubtitulo(NodoNuevaPosicion);
        aCodigoPregunta  := CodigoPregunta(NodoNuevaPosicion);
    end else begin
        aCodigoFuente := -1;
        aCodigoTitulo := -1;
        aCodigoSubTitulo := -1;
        aCodigoPregunta := -1;
    end;

    // Creamos el arbol
	CrearArbol;
   	ItemArbol := ArbolFAQ.Items.GetFirstNode;

	// Busco el nodo que tenia seleccionado
    if NodoNuevaPosicion <> nil then begin
        ArbolFAQ.FullCollapse;
    	while ItemArbol <> nil do begin
            if (CodigoFuente(ItemArbol) = aCodigoFuente) and
    	          (CodigoTitulo(ItemArbol) = aCodigoTitulo) and
        	      (CodigoSubTitulo(ItemArbol) = aCodigoSubtitulo) and
            	  (CodigoPregunta(ItemArbol) = aCodigoPregunta) then begin
                Break;
            end;

	    	// Pr�ximo nodo
		    ItemArbol := ItemArbol.GetNext;
    	end;
    end;

    if ItemArbol <> nil then begin
        ArbolFAQ.Select(ItemArbol);
        ArbolFAQ.Selected.Expand(True);
    end;

	Result := True
end;

procedure TfrmArbolFAQ.ArbolFAQDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
	Accept := (Sender is TTreeView) and (TTreeView(Sender) = TTreeView(Source))
end;

procedure TfrmArbolFAQ.grdPreguntasKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key = #13 then grdPreguntasDblClick(Sender);
end;

procedure TfrmArbolFAQ.grdSubTitulosKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key = #13 then grdSubTitulosDblClick(Sender);
end;

procedure TfrmArbolFAQ.grdTitulosKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then grdTitulosDblClick(Sender);
end;

procedure TfrmArbolFAQ.grdTitulosDblClick(Sender: TObject);
begin
	EditarTitulos(Sender, qryTitulos.FieldByName('CodigoTitulo').AsInteger);
end;

procedure TfrmArbolFAQ.grdSubTitulosDblClick(Sender: TObject);
begin
	EditarSubTitulos(Sender, qrySubTitulos.FieldByName('CodigoSubTitulo').AsInteger);
end;

procedure TfrmArbolFAQ.grdPreguntasDblClick(Sender: TObject);
begin
	EditarPreguntas(Sender, qryPreguntas.FieldByName('CodigoPregunta').AsInteger);
end;

end.







