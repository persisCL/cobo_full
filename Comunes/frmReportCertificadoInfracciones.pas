{********************************** Unit Header ********************************
File Name : frmReportCertificadoInfracciones.pas
Author : pdominguez
Date Created: 08/07/2009
Language : ES-AR
Description : SS 784
    - Imprime el Certificado de Inrfacciones.

Revision : 1
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS 828
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            dsInfracciones: TDataSource,
            ppDBPlInfracciones: TppDBPipeline,
            ppSubReport1: TppSubReport,
            ArchivoPlantillaAnexo: String,
            LineasDetalleCertificado: Integer,
            TAG_PATENTE_N,
            TAG_FECHA_N,
            TAG_FEC_ANUL_N,
            PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO,
            CANTIDAD_LINEAS_DETALLE_CERTIFICADO

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializar,
            ImprimirCertificado,
            ppSummaryBand1BeforePrint

Revision : 2
    Author : pdominguez
    Date   : 04/11/2009
    Description : SS 719
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            - Se a�ade el objeto spAgregarAHistoricoCertificadosInfraccionesEmitidos: TADOStoredProc
            - Se a�ade el objeto spAgregarAHistoricoCertificadosInfraccionesEmitidosDetalle: TADOStoredProc
            - Se mueven los resourceStrings de los procedimientos Inicializar y
            ImprimirCertificado, para que sean globales en vez de locales
            - Se a�aden las variables FPatente, FTratamiento, FClienteCertificado,
            FRelacion, FNumeroDocumentoCertificado, FFecha, FFechaDesde,
            FFechaHasta, FConexionDB, FUsuario, FPersoneria, FSexo y FCodigoVerificacion.
            - Se a�aden las constantes PLANTILLA_CERTIFICADO_INFRACCIONES_WEB,
            PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO_WEB y
            CANTIDAD_LINEAS_DETALLE_CERTIFICADO_WEB.

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializar
            ImprimirCertificado
            ppSummaryBand1BeforePrint
            ImprimirCertificadoWEB
            FormateRUTCertificado
            ppCertificadoInfraccionesBeforePrint
            ExportarReporteAPDF
            AuditarCertificado

Revision : 3
    Author : pdominguez
    Date   : 17/12/2009
    Description : SS 719
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ppCertificadoInfraccionesBeforePrint

Revision : 4
    Author : pdominguez
    Date   : 04/07/2010
    Description : Infractores Fase 2
        - Se a�adieron/modificaron los siguientes objetos/variables:
            FCodigoConcesionaria

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializar,
            AuditarCertificado

Revision : 5
    Author : pdominguez
    Date   : 05/07/2010
    Description : Infractores Fase 2
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ImprimirCertificadoWEB

Firma   : PAR00114-NDR-20110901
        El certificado de infracciones debe ser multiconcesionaria (logo, firma, plantilla)

Firma       :   SS_660D_CQU_20140806
Descripcion :   Se agrega nuevo certificado para infracciones por morosidad (Lista Amarilla)
                Se agrega nuevo par�metro que indica el tipo de certificado a imprimir
                Se agergan nuevas constantes para que indican los par�metros generales para morosos

Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


*******************************************************************************}
unit frmReportCertificadoInfracciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppPrnabl, ppClass, ppStrtch, ppRichTx, ppBands, ppCache, ppComm,
  ppRelatv, ppProd, ppReport, UtilRB, jpeg, ppCtrls, ADODb, ppRegion, DBClient,
  ppDB, ppDBPipe, ppSubRpt, DB, PeaTypes, TXRB, ppDevice, GIFImg, ppVar, UtilDB,           //PAR00114-NDR-20110901
  ppPDFDevice, ppParameter;

type
  TReportCertificadoInfraccionesForm = class(TForm)
    RBInterface1: TRBInterface;
    ppCertificadoInfracciones: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppRichText1: TppRichText;
    ppImage1: TppImage;
    ppSummaryBand1: TppSummaryBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppHeaderBand2: TppHeaderBand;
    ppImage2: TppImage;
    ppDBText1: TppDBText;
    ppDBPlInfracciones: TppDBPipeline;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    dsInfracciones: TDataSource;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppShape4: TppShape;
    ppShape5: TppShape;
    ppShape6: TppShape;
    ppRichText2: TppRichText;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppShape3: TppShape;
    spAgregarAHistoricoCertificadosInfraccionesEmitidos: TADOStoredProc;
    ppImgFirmayTimbre: TppImage;
    spAgregarAHistoricoCertificadosInfraccionesEmitidosDetalle: TADOStoredProc;
    ppFooterBand1: TppFooterBand;
    ppFooterBand2: TppFooterBand;
    pplblCodigoVerificador: TppLabel;
    pplblCodigoVerificador2: TppLabel;
    procedure ppSummaryBand1BeforePrint(Sender: TObject);
    procedure ppCertificadoInfraccionesBeforePrint(Sender: TObject);
  private
    { Private declarations }
    FArchivoPlantilla,
    FArchivoPlantillaAnexo: String;  // Rev. 1 (SS 828)
    FArchivoImagenLogo,FArchivoImagenFirma: String;                                        //PAR00114-NDR-20110901
    FLineasDetalleCertificado: Integer; // Rev. 1 (SS 828)
    // Rev. 2 (SS 719)
    FPatente,
    FTratamiento,
    FClienteCertificado,
    FRelacion,
    FNumeroDocumentoCertificado,
    FUsuario,
    FPersoneria,
    FSexo: String;
    FFecha,
    FFechaDesde,
    FFechaHasta: TDateTime;
    FConexionDB: TADOConnection;
    FCodigoVerificacion: String;
    // Fin Rev. 2 (SS 719)
    FCodigoConcesionaria: Integer; // Rev. 4 (Infractores Fase 2)
    FNombreCorto: String;                                                       //PAR00114-NDR-20110901
    FListaAmarilla : Boolean; // SS_660D_CQU_20140806
    function FormateaRUTCertificado(RUT: String): String;
    function ExportarReporteAPDF(Reporte: TppReport; var Resultado, Error: string): boolean;
    Procedure AuditarCertificado(var DescError: String);
  public
    { Public declarations }
    //function Inicializar(Conexion: TADOConnection; var DescError: String; Fecha, FechaDesde, FechaHasta: TDateTime; Tratamiento, ClienteCertificado, NumeroRut, Relacion, Patente: String; cdsInfracciones: TClientDataSet; Usuario, Personeria, Sexo: String; CodigoConcesionaria: Integer = 1): Boolean;                                // SS_660D_CQU_20140806
    function Inicializar(Conexion: TADOConnection; var DescError: String; Fecha, FechaDesde, FechaHasta: TDateTime; Tratamiento, ClienteCertificado, NumeroRut, Relacion, Patente: String; cdsInfracciones: TClientDataSet; Usuario, Personeria, Sexo: String; CodigoConcesionaria: Integer = 1; ListaAmarilla : Boolean = False): Boolean; // SS_660D_CQU_20140806
    procedure ImprimirCertificado;
    procedure ImprimirCertificadoWEB(Conexion: TADOConnection; DatosDetalle: TADOStoredProc; Fecha, FechaDesde, FechaHasta: TDateTime; Tratamiento, ClienteCertificado, NumeroRut, Relacion, Patente, CodigoVerificador: String; ReImprimir: Boolean; var Plantilla, DescError: String; Usuario, RutaPlantillas: String; CodigoConcesionaria: Integer = 1);
  end;

var
  ReportCertificadoInfraccionesForm: TReportCertificadoInfraccionesForm;

const
    TAG_DIA  = 'DIA';
    TAG_MES  = 'MES';
    TAG_ANIO = 'ANIO';
    TAG_PATENTE = 'PATENTE';
    TAG_TRATAMIENTO = 'TRATAMIENTO';
    TAG_CLIENTE_CERTIFICADO = 'CLIENTE_CERTIFICADO';
    TAG_NUMERO_RUT = 'NUMERO_RUT';
    TAG_RELACION   = 'RELACION';
    TAG_DIA_DESDE  = 'DIA_DESDE';
    TAG_MES_DESDE  = 'MES_DESDE';
    TAG_ANIO_DESDE = 'ANIO_DESDE';
    TAG_DIA_HASTA  = 'DIA_HASTA';
    TAG_MES_HASTA  = 'MES_HASTA';
    TAG_ANIO_HASTA = 'ANIO_HASTA';
    // Rev. 1 (SS 828)
    TAG_PATENTE_N  = 'PAT_%d';
    TAG_FECHA_N    = 'FEC_%d';
    TAG_FEC_ANUL_N = 'FEC2_%d';
    // Fin Rev. 1 (SS 828)


    PLANT_CERTIF_INFRAC             = 'PLANT_CERTIF_INFRAC_';                    //PAR00114-NDR-20110901
    PLANT_CERTIF_INFRACC_WEB        = 'PLANT_CERTIF_INFRACC_WEB_';               //PAR00114-NDR-20110901
    PLANT_CERTIF_INFRACC_ANEXO      = 'PLANT_CERTIF_INFRACC_ANEXO_';             //PAR00114-NDR-20110901
    PLANT_CERTIF_INFRACC_ANEXO_WEB  = 'PLANT_CERTIF_INFRACC_ANEXO_WEB_';         //PAR00114-NDR-20110901
    CANT_LINEAS_CERTIF              = 'CANT_LINEAS_CERTIF_';                     //PAR00114-NDR-20110901
    CANT_LINEAS_CERTIF_WEB          = 'CANT_LINEAS_CERTIF_WEB_';                 //PAR00114-NDR-20110901
    TOP_FIRMA_CERTIF_WEB            = 'TOP_FIRMA_CERTIF_WEB_';                   //PAR00114-NDR-20110901
    IMAGEN_LOGO_CERTIFICADO         = 'IMAGEN_LOGO_CERTIFICADO_';                //PAR00114-NDR-20110901
    IMAGEN_FIRMA_CERTIFICADO        = 'IMAGEN_FIRMA_CERTIFICADO_';               //PAR00114-NDR-20110901
    IMAGEN_LOGO_CERTIFICADO_WEB     = 'IMAGEN_LOGO_CERTIFICADO_WEB_';            //PAR00114-NDR-20110901
    IMAGEN_FIRMA_CERTIFICADO_WEB    = 'IMAGEN_FIRMA_CERTIFICADO_WEB_';           //PAR00114-NDR-20110901

    PLANT_CERT_INF_LA               = 'PLANT_CERT_INF_LA_';                     // SS_660D_CQU_20140806
    PLANT_CERT_INF_WEB_LA           = 'PLANT_CERT_INF_WEB_LA_';                 // SS_660D_CQU_20140806
    PLANT_CERT_INF_ANEXO_LA         = 'PLANT_CERT_INF_ANEXO_LA_';               // SS_660D_CQU_20140806
    PLANT_CERT_INF_ANEXO_WEB_LA     = 'PLANT_CERT_INF_ANEXO_WEB_LA_';           // SS_660D_CQU_20140806
    CANT_LINEAS_CERT_LA             = 'CANT_LINEAS_CERT_LA_';                   // SS_660D_CQU_20140806
    CANT_LINEAS_CERT_WEB_LA         = 'CANT_LINEAS_CERT_WEB_LA_';               // SS_660D_CQU_20140806
    TOP_FIRMA_CERT_WEB_LA           = 'TOP_FIRMA_CERT_WEB_LA_';                 // SS_660D_CQU_20140806
    IMAGEN_FIRMA_CERT_LA            = 'IMAGEN_FIRMA_CERT_LA_';                  // SS_660D_CQU_20140806
    IMAGEN_FIRMA_CERT_WEB_LA        = 'IMAGEN_FIRMA_CERT_WEB_LA_';              // SS_660D_CQU_20140806
    IMAGEN_LOGO_CERT_LA             = 'IMAGEN_LOGO_CERT_LA_';                   // SS_660D_CQU_20140806
    IMAGEN_LOGO_CERT_WEB_LA         = 'IMAGEN_LOGO_CERT_WEB_LA_';               // SS_660D_CQU_20140806



implementation

{$R *.dfm}

Uses ConstParametrosGenerales, Util, UtilHTTP, UtilProc;


// Rev. 2 (SS 719)
resourcestring
        MSG_NO_EXISTE_PLANTILLA = 'La plantilla del Certificado de Inrfacciones NO Existe. '  + CRLF + CRLF + 'Archivo: %s';
        MSG_PARAMETRO_NO_DEFINIDO = 'El par�metro General "%s" para la plantilla del Certificado de Infracciones NO est� Definido.';
        MSG_PARAMETRO_NO_VALIDO = 'El par�metro General "%s" para la plantilla del Certificado de Infracciones NO puede ser Cero.';
        MSG_ERROR_INESPERADO_INICIALIZAR = 'Se produjo un Error al inicializar el Reporte. Error: %s';
        MSG_ERROR_INESPERADO_IMPRIMIR = 'Se produjo un Error al imprimir el Reporte. Error: %s';
        MSG_ERROR_CAPTION = ' Impresi�n Certificado Infracciones';
        MSG_ERROR_IMAGEN_LOGO = ' No existe el archivo con la im�gen del Logo para el certificado.' + CRLF + CRLF + 'Par�metro General: %s' + CRLF + 'Valor: %s';            //PAR00114-NDR-20110901
        MSG_ERROR_IMAGEN_FIRMA = ' No existe el archivo con la im�gen de la firma para el certificado' + CRLF + CRLF + 'Par�metro General: %s' + CRLF + 'Valor: %s';        //PAR00114-NDR-20110901

// Fin Rev. 2 (SS 719)

{******************************* Procedure Header ******************************
Procedure Name: Inicializar
Author : pdominguez
Date Created : 08/07/2009
Parameters : Conexion: TADOConnection; var DescError: String; Fecha, FechaDesde,
    FechaHasta: TDateTime; Tratamiento, ClienteCertificado, NumeroRut, Relacion,
    Patente: String; cdsInfracciones: TClientDataSet; Usuario, Personeria, Sexo: String
Description : SS 784
    - Inicializa el Reporte, cargando los par�metros necesarios.

Revision : 1
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS 828
        - Se a�ade la carga de los par�metros PLANTILLA_CERTIFICADO_INFRACCIONES_ANEXO y
        CANTIDAD_LINEAS_DETALLE_CERTIFICADO.

Revision : 2
    Author : pdominguez
    Date   : 04/11/2009
    Description : SS 719
        - Se mueven los resourceStrings del procedimiento para que sean globales
        en vez de locales.
        - Se a�aden los par�metros Fecha, FechaDesde, FechaHasta, Tratamiento,
        ClienteCertificado, NumeroRut, Relacion, Patente, cdsInfracciones,
        Usuario, Personeria, Sexo.
        - Se asignan los valores a la variables FFecha, FFechaDesde, FFechaHasta
        FTratamiento, FClienteCertificado, FNumeroDocumentoCertificado, FRelacion,
        FPatente, FUsuario, FPersoneria, FSexo y FConexionDB para tenerlos disponible
        durante todo el proceso de impresi�n del reporte.
        - Se pasa en las llamadas y/o asignaciones correspondientes la variable
        FConexion en vez del par�metro Conexion.
        - Se a�ade la auditor�a de la impresi�n del Certificado.

Revision : 4
    Author : pdominguez
    Date   : 04/07/2010
    Description : Infractores Fase 2
        - Se a�ade el par�metro CodigoConcesionaria.
*******************************************************************************}
function TReportCertificadoInfraccionesForm.Inicializar(
    Conexion: TADOConnection; var DescError: String; Fecha, FechaDesde, FechaHasta: TDateTime;
    Tratamiento, ClienteCertificado, NumeroRut, Relacion, Patente: String; cdsInfracciones: TClientDataSet;
    //Usuario, Personeria, Sexo: String; CodigoConcesionaria: Integer = 1): Boolean;                        		// SS_660D_CQU_20140806
    Usuario, Personeria, Sexo: String; CodigoConcesionaria: Integer = 1; ListaAmarilla : Boolean = False): Boolean; // SS_660D_CQU_20140806
begin
    Try
        DescError := '';
        FArchivoPlantilla := '';
        FArchivoPlantillaAnexo := '';    // Rev. 1 (SS 828)
        FLineasDetalleCertificado := 0;  // Rev. 1 (SS 828)
        // Rev. 2 (SS 719)
        FConexionDB := Conexion;
        dsInfracciones.DataSet := cdsInfracciones;
        FFecha := Fecha;
        FFechaDesde := FechaDesde;
        FFechaHasta := FechaHasta;
        FTratamiento := Tratamiento;
        FClienteCertificado := Trim(ClienteCertificado);
        FNumeroDocumentoCertificado := Trim(NumeroRut);
        FRelacion := Relacion;
        FPatente := Patente;
        FUsuario := Usuario;
        FPersoneria := Personeria;
        FSexo := Sexo;
        FCodigoConcesionaria := CodigoConcesionaria; // Rev. 4 (Infractores Fase 2)
        FNombreCorto := QueryGetValue(Conexion, Format('SELECT NombreCorto FROM Concesionarias  WITH (NOLOCK) WHERE CodigoConcesionaria = %d ' , [FCodigoConcesionaria]));              //PAR00114-NDR-20110901
        FListaAmarilla := ListaAmarilla;  // SS_660D_CQU_20140806

        if not FListaAmarilla then begin                                                                                             // SS_660D_CQU_20140806

            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERTIF_INFRAC)+Trim(FNombreCorto),FArchivoPlantilla);                     //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERTIF_INFRACC_ANEXO)+Trim(FNombreCorto),FArchivoPlantillaAnexo);         //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(CANT_LINEAS_CERTIF)+Trim(FNombreCorto), FLineasDetalleCertificado);             //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_LOGO_CERTIFICADO)+Trim(FNombreCorto),FArchivoImagenLogo);                //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_FIRMA_CERTIFICADO)+Trim(FNombreCorto), FArchivoImagenFirma);             //PAR00114-NDR-20110901



            // Fin Rev. 2 (SS 719)
            if Trim(FArchivoPlantilla) <> '' then begin                                                                                         //PAR00114-NDR-20110901
                if Not FileExists(FArchivoPlantilla) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantilla])                      //PAR00114-NDR-20110901
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERTIF_INFRAC)+Trim(FNombreCorto)]);                             //PAR00114-NDR-20110901
                                                                                                                                                //PAR00114-NDR-20110901
            // Rev. 1 (SS 828)                                                                                                                  //PAR00114-NDR-20110901
            if Trim(FArchivoPlantillaAnexo) <> '' then begin                                                                                    //PAR00114-NDR-20110901
                if Not FileExists(FArchivoPlantillaAnexo) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantillaAnexo])            //PAR00114-NDR-20110901
            end else begin                                                                                                                      //PAR00114-NDR-20110901
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                                        //PAR00114-NDR-20110901
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERTIF_INFRACC_ANEXO)+Trim(FNombreCorto)]);               //PAR00114-NDR-20110901
            end;                                                                                                                                //PAR00114-NDR-20110901
                                                                                                                                                //PAR00114-NDR-20110901
            if FLineasDetalleCertificado = 0 then begin                                                                                         //PAR00114-NDR-20110901
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                                        //PAR00114-NDR-20110901
                DescError := DescError + Format(MSG_PARAMETRO_NO_VALIDO,[Trim(CANT_LINEAS_CERTIF)+Trim(FNombreCorto)]);                         //PAR00114-NDR-20110901
            end;                                                                                                                                //PAR00114-NDR-20110901
                                                                                                                                                //PAR00114-NDR-20110901
            if Trim(FArchivoImagenLogo) <> '' then begin                                                                                        //PAR00114-NDR-20110901
                if Not FileExists(FArchivoImagenLogo) then begin
                    if DescError <> '' then DescError := DescError + #13#10 + #13#10;
                    DescError := DescError + Format(MSG_ERROR_IMAGEN_LOGO,[Trim(IMAGEN_LOGO_CERTIFICADO)+Trim(FNombreCorto), FArchivoImagenLogo])                      //PAR00114-NDR-20110901
                end;
            end else begin
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_LOGO_CERTIFICADO)+Trim(FNombreCorto)]);                         //PAR00114-NDR-20110901
            end;
                                                                                                                                                //PAR00114-NDR-20110901
            if Trim(FArchivoImagenFirma) <> '' then begin                                                                                       //PAR00114-NDR-20110901
                if Not FileExists(FArchivoImagenFirma) then begin
                    if DescError <> '' then DescError := DescError + #13#10 + #13#10;
                    DescError := DescError + Format(MSG_ERROR_IMAGEN_FIRMA,[Trim(IMAGEN_FIRMA_CERTIFICADO)+Trim(FNombreCorto), FArchivoImagenFirma])                   //PAR00114-NDR-20110901
                end;
            end else begin
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_FIRMA_CERTIFICADO)+Trim(FNombreCorto)]);                        //PAR00114-NDR-20110901
            end;                                                                                                                                       //PAR00114-NDR-20110901
        end else begin                                                                                                                              // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERT_INF_LA)+Trim(FNombreCorto),FArchivoPlantilla);                                      // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERT_INF_ANEXO_LA)+Trim(FNombreCorto),FArchivoPlantillaAnexo);                           // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(CANT_LINEAS_CERT_LA)+Trim(FNombreCorto), FLineasDetalleCertificado);                           // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_LOGO_CERT_LA)+Trim(FNombreCorto),FArchivoImagenLogo);                                   // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_FIRMA_CERT_LA)+Trim(FNombreCorto), FArchivoImagenFirma);                                // SS_660D_CQU_20140806

            if Trim(FArchivoPlantilla) <> '' then begin                                                                                             // SS_660D_CQU_20140806
                if Not FileExists(FArchivoPlantilla) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantilla])                          // SS_660D_CQU_20140806
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERT_INF_LA)+Trim(FNombreCorto)]);                                   // SS_660D_CQU_20140806
                                                                                                                                                    // SS_660D_CQU_20140806
            if Trim(FArchivoPlantillaAnexo) <> '' then begin                                                                                        // SS_660D_CQU_20140806
                if Not FileExists(FArchivoPlantillaAnexo) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantillaAnexo])                // SS_660D_CQU_20140806
            end else begin                                                                                                                          // SS_660D_CQU_20140806
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                                   // SS_660D_CQU_20140806
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERT_INF_ANEXO_LA)+Trim(FNombreCorto)]);                      // SS_660D_CQU_20140806
            end;                                                                                                                                    // SS_660D_CQU_20140806
                                                                                                                                                    // SS_660D_CQU_20140806
            if FLineasDetalleCertificado = 0 then begin                                                                                             // SS_660D_CQU_20140806
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                                   // SS_660D_CQU_20140806
                DescError := DescError + Format(MSG_PARAMETRO_NO_VALIDO,[Trim(CANT_LINEAS_CERT_LA)+Trim(FNombreCorto)]);                            // SS_660D_CQU_20140806
            end;                                                                                                                                    // SS_660D_CQU_20140806
                                                                                                                                                    // SS_660D_CQU_20140806
            if Trim(FArchivoImagenLogo) <> '' then begin                                                                                            // SS_660D_CQU_20140806
                if Not FileExists(FArchivoImagenLogo) then begin                                                                                    // SS_660D_CQU_20140806
                    if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                               // SS_660D_CQU_20140806
                    DescError := DescError + Format(MSG_ERROR_IMAGEN_LOGO,[Trim(IMAGEN_LOGO_CERT_LA)+Trim(FNombreCorto), FArchivoImagenLogo])       // SS_660D_CQU_20140806
                end;                                                                                                                                // SS_660D_CQU_20140806
            end else begin                                                                                                                          // SS_660D_CQU_20140806
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                                   // SS_660D_CQU_20140806
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_LOGO_CERT_LA)+Trim(FNombreCorto)]);                          // SS_660D_CQU_20140806
            end;                                                                                                                                    // SS_660D_CQU_20140806
                                                                                                                                                    // SS_660D_CQU_20140806
            if Trim(FArchivoImagenFirma) <> '' then begin                                                                                           // SS_660D_CQU_20140806
                if Not FileExists(FArchivoImagenFirma) then begin                                                                                   // SS_660D_CQU_20140806
                    if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                               // SS_660D_CQU_20140806
                    DescError := DescError + Format(MSG_ERROR_IMAGEN_FIRMA,[Trim(IMAGEN_FIRMA_CERT_LA)+Trim(FNombreCorto), FArchivoImagenFirma])    // SS_660D_CQU_20140806
                end;                                                                                                                                // SS_660D_CQU_20140806
            end else begin                                                                                                                          // SS_660D_CQU_20140806
                if DescError <> '' then DescError := DescError + #13#10 + #13#10;                                                                   // SS_660D_CQU_20140806
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_FIRMA_CERT_LA)+Trim(FNombreCorto)]);                         // SS_660D_CQU_20140806
            end;                                                                                                                                    // SS_660D_CQU_20140806
        end;                                                                                                                                        // SS_660D_CQU_20140806
        // Fin Rev. 1 (SS 828)
        // Rev. 2 (SS 719)
        spAgregarAHistoricoCertificadosInfraccionesEmitidos.Connection := FConexionDB;
        spAgregarAHistoricoCertificadosInfraccionesEmitidosDetalle.Connection := FConexionDB;

        AuditarCertificado(DescError);
        // fin Rev. 2 (SS 719)
        Result := DescError = '';
    Except
        On E:Exception do begin
            DescError := Format(MSG_ERROR_INESPERADO_INICIALIZAR,[E.Message]);
            Result := False;
        end;
    End;
end;

{******************************* Procedure Header ******************************
Procedure Name: ImprimirCertificado
Author : pdominguez
Date Created : 08/07/2009
Parameters :
Description : SS 784
    - Prepara y lanza la impresi�n del Certificado de infracciones.

Revision : 1
    Author : pdominguez
    Date   : 21/09/2009
    Description : SS 828
        - Se a�ade el par�metro cdsInfracciones: TClientDataSet.
        - Se a�ade la impresi�n de los campos Patente, Fecha y FechaAnulacion,
        almacenados en el objeto cdsInfracciones.

Revision : 2
    Author : pdominguez
    Date   : 04/11/2009
    Description : SS 719
        - Se mueven los resourceStrings del procedimiento para que sean globales
        en vez de locales.
        - Se mueve el c�digo de incializaci�n del reporte al evento BeforePrint
        para compatibilizaci�n con la impresi�n desde la WEB.
        - Se mueven todos los par�metros a la funci�n Inicializar.
*******************************************************************************}
procedure TReportCertificadoInfraccionesForm.ImprimirCertificado;
    var
        Veces: Integer;
begin
    try
        ppImage1.Picture.LoadFromFile(FArchivoImagenLogo);                      //PAR00114-NDR-20110901
        ppImage2.Picture.LoadFromFile(FArchivoImagenLogo);                      //PAR00114-NDR-20110901
        RBInterface1.Execute(True);
    except
        on E:Exception do begin
            MsgBox(Format(MSG_ERROR_INESPERADO_IMPRIMIR,[E.Message]), MSG_ERROR_CAPTION, MB_ICONSTOP);
        end;
    end;
end;

{******************************* Procedure Header ******************************
Procedure Name: ppSummaryBand1BeforePrint
Author : pdominguez
Date Created : 21/09/2009
Parameters : Sender: TObject
Description : SS 828
    - Habilitamos o deshabilitamos la banda Summary, la cual contiene el reporte
    del anexo. Imprimiremos el anexo siempre que los registros de detalle superen
    las l�neas de detalle de la hoja principal del certificado.

Revision : 1
    Author : pdominguez
    Date   : 05/10/2009
    Description : SS 719
        - Llamamos directamente a la propiedad RecordCount del Dataset en vez
        de llamarla a traves de la clase TClientDataSet, ya que ahora la clase
        del DataSet asignado varia.
*******************************************************************************}
procedure TReportCertificadoInfraccionesForm.ppSummaryBand1BeforePrint(Sender: TObject);
begin
    ppSummaryBand1.Visible := dsInfracciones.DataSet.RecordCount > FLineasDetalleCertificado;
end;

{******************************* Procedure Header ******************************
Procedure Name: ImprimirCertificadoWEB
Author : pdominguez
Date Created : 29/09/2009
Parameters : Conexion: TADOConnection; Fecha, FechaDesde, FechaHasta: TDateTime;
    Tratamiento, ClienteCertificado, NumeroRut, Relacion, Patente: String;
    var Plantilla, DescError, Usuario, RutaPlantillas: String
Description : SS 719
    - Imprime el certificado de Infracciones en formato PDF para la WEB.

Revision : 5
    Author : pdominguez
    Date   : 05/07/2010
    Description : Infractores Fase 2
        - Se a�ade el par�metro CodigoConcesionaria.
*******************************************************************************}
procedure TReportCertificadoInfraccionesForm.ImprimirCertificadoWEB(
    Conexion: TADOConnection; DatosDetalle: TADOStoredProc; Fecha, FechaDesde, FechaHasta: TDateTime; Tratamiento,
    ClienteCertificado, NumeroRut, Relacion, Patente, CodigoVerificador: String; ReImprimir: Boolean; var Plantilla,
    DescError: String; Usuario, RutaPlantillas: String; CodigoConcesionaria: Integer = 1);
    var
        Veces,
        TOPFirmayTimbreLectura: Integer;
        TOPFirmayTimbre: Currency;
begin
    Try
        DescError := '';
        FArchivoPlantilla := '';
        FArchivoPlantillaAnexo := '';
        FLineasDetalleCertificado := 0;
        FConexionDB := Conexion;

        FFecha := Fecha;
        FFechaDesde := FechaDesde;
        FFechaHasta := FechaHasta;
        FTratamiento := Tratamiento;
        FClienteCertificado := Trim(ClienteCertificado);
        FNumeroDocumentoCertificado := Trim(NumeroRut);
        FRelacion := Relacion;
        FPatente := Patente;
        FUsuario := Usuario;
        FPersoneria := '';
        FSexo := '';
        FCodigoConcesionaria := CodigoConcesionaria; // Rev. 5 (Infractores Fase 2)
        FNombreCorto := QueryGetValue(Conexion, Format('SELECT NombreCorto FROM Concesionarias  WITH (NOLOCK) WHERE CodigoConcesionaria = %d ' , [FCodigoConcesionaria]));    //PAR00114-NDR-20110901

        if not FListaAmarilla then begin                                                                                                                               // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERTIF_INFRACC_WEB)+Trim(FNombreCorto),FArchivoPlantilla);                                                  //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERTIF_INFRACC_ANEXO_WEB)+Trim(FNombreCorto),FArchivoPlantillaAnexo);                                       //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(CANT_LINEAS_CERTIF_WEB)+Trim(FNombreCorto), FLineasDetalleCertificado);                                           //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(TOP_FIRMA_CERTIF_WEB)+Trim(FNombreCorto), TOPFirmayTimbreLectura);                                                //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_LOGO_CERTIFICADO_WEB)+Trim(FNombreCorto), FArchivoImagenLogo);                                             //PAR00114-NDR-20110901
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_FIRMA_CERTIFICADO_WEB)+Trim(FNombreCorto), FArchivoImagenFirma);                                           //PAR00114-NDR-20110901

            FArchivoPlantilla := RutaPlantillas + FArchivoPlantilla;
            FArchivoPlantillaAnexo := RutaPlantillas + FArchivoPlantillaAnexo;

            if Trim(FArchivoPlantilla) <> '' then begin
                if Not FileExists(FArchivoPlantilla) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantilla])
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERTIF_INFRACC_WEB)+Trim(FNombreCorto)]);                                               //PAR00114-NDR-20110901

            if Trim(FArchivoPlantillaAnexo) <> '' then begin
                if Not FileExists(FArchivoPlantillaAnexo) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantillaAnexo])
            end else begin
                if DescError <> '' then DescError := DescError + #13#10;
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERTIF_INFRACC_ANEXO_WEB)+Trim(FNombreCorto)]);                                 //PAR00114-NDR-20110901
            end;

            if FLineasDetalleCertificado = 0 then begin
                if DescError <> '' then DescError := DescError + #13#10;
                DescError := DescError + Format(MSG_PARAMETRO_NO_VALIDO,[Trim(CANT_LINEAS_CERTIF_WEB)+Trim(FNombreCorto)]);                                           //PAR00114-NDR-20110901
            end;

            if TOPFirmayTimbreLectura = 0 then begin
                if DescError <> '' then DescError := DescError + #13#10;
                DescError := DescError + Format(MSG_PARAMETRO_NO_VALIDO,[Trim(TOP_FIRMA_CERTIF_WEB)+Trim(FNombreCorto)]);                                             //PAR00114-NDR-20110901
            end;

            if Trim(FArchivoImagenLogo) <> '' then begin                                                                                                              //PAR00114-NDR-20110901
                if Not FileExists(FArchivoImagenLogo) then DescError := Format(MSG_ERROR_IMAGEN_LOGO,[FArchivoImagenLogo])                                            //PAR00114-NDR-20110901
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_LOGO_CERTIFICADO_WEB)+Trim(FNombreCorto)]);                                           //PAR00114-NDR-20110901
                                                                                                                                                                      //PAR00114-NDR-20110901
            if Trim(FArchivoImagenFirma) <> '' then begin                                                                                                             //PAR00114-NDR-20110901
                if Not FileExists(FArchivoImagenFirma) then DescError := Format(MSG_ERROR_IMAGEN_FIRMA,[FArchivoImagenFirma])                                         //PAR00114-NDR-20110901
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_FIRMA_CERTIFICADO_WEB)+Trim(FNombreCorto)]);                                          //PAR00114-NDR-20110901
        end else begin                                                                                                                                                // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERT_INF_WEB_LA)+Trim(FNombreCorto),FArchivoPlantilla);                                                    // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(PLANT_CERT_INF_ANEXO_WEB_LA)+Trim(FNombreCorto),FArchivoPlantillaAnexo);                                         // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(CANT_LINEAS_CERT_WEB_LA)+Trim(FNombreCorto), FLineasDetalleCertificado);                                         // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(TOP_FIRMA_CERT_WEB_LA)+Trim(FNombreCorto), TOPFirmayTimbreLectura);                                              // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_LOGO_CERT_WEB_LA)+Trim(FNombreCorto), FArchivoImagenLogo);                                                // SS_660D_CQU_20140806
            ObtenerParametroGeneral(FConexionDB,Trim(IMAGEN_FIRMA_CERT_WEB_LA)+Trim(FNombreCorto), FArchivoImagenFirma);                                              // SS_660D_CQU_20140806
                                                                                                                                                                      // SS_660D_CQU_20140806
            FArchivoPlantilla := RutaPlantillas + FArchivoPlantilla;                                                                                                  // SS_660D_CQU_20140806
            FArchivoPlantillaAnexo := RutaPlantillas + FArchivoPlantillaAnexo;                                                                                        // SS_660D_CQU_20140806
                                                                                                                                                                      // SS_660D_CQU_20140806
            if Trim(FArchivoPlantilla) <> '' then begin                                                                                                               // SS_660D_CQU_20140806
                if Not FileExists(FArchivoPlantilla) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantilla])                                            // SS_660D_CQU_20140806
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERT_INF_WEB_LA)+Trim(FNombreCorto)]);                                                 // SS_660D_CQU_20140806
                                                                                                                                                                      // SS_660D_CQU_20140806
            if Trim(FArchivoPlantillaAnexo) <> '' then begin                                                                                                          // SS_660D_CQU_20140806
                if Not FileExists(FArchivoPlantillaAnexo) then DescError := Format(MSG_NO_EXISTE_PLANTILLA,[FArchivoPlantillaAnexo])                                  // SS_660D_CQU_20140806
            end else begin                                                                                                                                            // SS_660D_CQU_20140806
                if DescError <> '' then DescError := DescError + #13#10;                                                                                              // SS_660D_CQU_20140806
                DescError := DescError + Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(PLANT_CERT_INF_ANEXO_WEB_LA)+Trim(FNombreCorto)]);                                    // SS_660D_CQU_20140806
            end;                                                                                                                                                      // SS_660D_CQU_20140806
                                                                                                                                                                      // SS_660D_CQU_20140806
            if FLineasDetalleCertificado = 0 then begin                                                                                                               // SS_660D_CQU_20140806
                if DescError <> '' then DescError := DescError + #13#10;                                                                                              // SS_660D_CQU_20140806
                DescError := DescError + Format(MSG_PARAMETRO_NO_VALIDO,[Trim(CANT_LINEAS_CERT_WEB_LA)+Trim(FNombreCorto)]);                                          // SS_660D_CQU_20140806
            end;                                                                                                                                                      // SS_660D_CQU_20140806
                                                                                                                                                                      // SS_660D_CQU_20140806
            if TOPFirmayTimbreLectura = 0 then begin                                                                                                                  // SS_660D_CQU_20140806
                if DescError <> '' then DescError := DescError + #13#10;                                                                                              // SS_660D_CQU_20140806
                DescError := DescError + Format(MSG_PARAMETRO_NO_VALIDO,[Trim(TOP_FIRMA_CERT_WEB_LA)+Trim(FNombreCorto)]);                                            // SS_660D_CQU_20140806
            end;                                                                                                                                                      // SS_660D_CQU_20140806
                                                                                                                                                                      // SS_660D_CQU_20140806
            if Trim(FArchivoImagenLogo) <> '' then begin                                                                                                              // SS_660D_CQU_20140806
                if Not FileExists(FArchivoImagenLogo) then DescError := Format(MSG_ERROR_IMAGEN_LOGO,[FArchivoImagenLogo])                                            // SS_660D_CQU_20140806
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_LOGO_CERT_WEB_LA)+Trim(FNombreCorto)]);                                               // SS_660D_CQU_20140806
                                                                                                                                                                      // SS_660D_CQU_20140806
            if Trim(FArchivoImagenFirma) <> '' then begin                                                                                                             // SS_660D_CQU_20140806
                if Not FileExists(FArchivoImagenFirma) then DescError := Format(MSG_ERROR_IMAGEN_FIRMA,[FArchivoImagenFirma])                                         // SS_660D_CQU_20140806
            end else DescError := Format(MSG_PARAMETRO_NO_DEFINIDO,[Trim(IMAGEN_FIRMA_CERT_WEB_LA)+Trim(FNombreCorto)]);                                              // SS_660D_CQU_20140806
        end;                                                                                                                                                          // SS_660D_CQU_20140806

        If DescError = '' then begin
            spAgregarAHistoricoCertificadosInfraccionesEmitidos.Connection := FConexionDB;
            spAgregarAHistoricoCertificadosInfraccionesEmitidosDetalle.Connection := FConexionDB;

            ppImage1.Picture.LoadFromFile(FArchivoImagenLogo);                                                   //PAR00114-NDR-20110901
            ppImage2.Picture.LoadFromFile(FArchivoImagenLogo);                                                   //PAR00114-NDR-20110901
            ppImgFirmayTimbre.Picture.LoadFromFile(FArchivoImagenFirma);                                         //PAR00114-NDR-20110901
            ppImgFirmayTimbre.Visible := True;
            ppImgFirmayTimbre.Transparent := True;
            TOPFirmayTimbre := TOPFirmayTimbreLectura;
            ppImgFirmayTimbre.Top := (TOPFirmayTimbre / 25);

            dsInfracciones.DataSet := DatosDetalle;

            if ReImprimir then
                FCodigoVerificacion := CodigoVerificador
            else AuditarCertificado(DescError);

            If DescError = '' then ExportarReporteAPDF(ppCertificadoInfracciones, Plantilla, DescError);
        end;
    Except
        On E:Exception do begin
            DescError := Format(MSG_ERROR_INESPERADO_IMPRIMIR,[E.Message]);
        end;
    End;
end;

{******************************** Function Header ******************************
Procedure Name: FormateaRUTCertificado
Author : pdominguez
Date Created : 02/10/2009
Parameters : RUT: String
Return Value : String
Description : SS 719
    - Formatea el N�mero de RUT.
*******************************************************************************}
function TReportCertificadoInfraccionesForm.FormateaRUTCertificado(RUT: String): String;
begin
    Result := StrLeft(RUT, Length(RUT) - 1)+ '-' + StrRight(RUT,1);
end;

{******************************* Procedure Header ******************************
Procedure Name: ppCertificadoInfraccionesBeforePrint
Author : pdominguez
Date Created : 02/10/2009
Parameters : Sender: TObject
Description : SS 719
    - Se Mueve el C�digo de inicializaci�n del reporte del procedimiento ImprimirCertificado,
    por compatibilizaci�n del reporte para la impresi�n en PDF desde la WEB.

Revision : 1
    Author : pdominguez
    Date   : 17/12/2009
    Description : SS 719
        - Se implementa la funci�n DevolverFechaFormateada para evitar los problemas
        con la configuraci�n regional.
*******************************************************************************}
procedure TReportCertificadoInfraccionesForm.ppCertificadoInfraccionesBeforePrint(Sender: TObject);
    var
        Veces: Integer;

    function DevolverFechaFormateada(Formato: string; Fecha: TDateTime): string;
        const
            CONST_MESES: Array[1..12] of String = ('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        var
            LocalFormatSettings: TFormatSettings;
            Veces: Integer;
    begin
        GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT,LocalFormatSettings);
        with LocalFormatSettings do begin
            ShortDateFormat := 'dd/mm/yyyy';
            For Veces := 1 to 12 do
                LongMonthNames[Veces] := CONST_MESES[Veces]
        end;
        DateTimeToString(Result,Formato ,Fecha, LocalFormatSettings);
    end;
begin
    pplblCodigoVerificador.Caption :=
        'C�digo Verificador: ' +
        PadL(FCodigoVerificacion,9,'0');

    with ppRichText1 do begin
        LoadFromFile(FArchivoPlantilla);

        // Rev. 1 (SS 719)
        RichText := ReplaceTag(RichText, TAG_DIA,DevolverFechaFormateada('dd',FFecha));
        RichText := ReplaceTag(RichText, TAG_MES,DevolverFechaFormateada('mmmm',FFecha));
        RichText := ReplaceTag(RichText, TAG_ANIO,DevolverFechaFormateada('yyyy',FFecha));
        RichText := ReplaceTag(RichText, TAG_DIA_DESDE,DevolverFechaFormateada('dd',FFechaDesde));
        RichText := ReplaceTag(RichText, TAG_MES_DESDE,DevolverFechaFormateada('mmmm',FFechaDesde));
        RichText := ReplaceTag(RichText, TAG_ANIO_DESDE,DevolverFechaFormateada('yyyy',FFechaDesde));
        RichText := ReplaceTag(RichText, TAG_DIA_HASTA,DevolverFechaFormateada('dd',FFechaHasta));
        RichText := ReplaceTag(RichText, TAG_MES_HASTA,DevolverFechaFormateada('mmmm',FFechaHasta));
        RichText := ReplaceTag(RichText, TAG_ANIO_HASTA,DevolverFechaFormateada('yyyy',FFechaHasta));
        // Fin Rev. 1 (SS 719)
        RichText := ReplaceTag(RichText, TAG_TRATAMIENTO,FTratamiento);
        RichText := ReplaceTag(RichText, TAG_CLIENTE_CERTIFICADO,FClienteCertificado);
        RichText := ReplaceTag(RichText, TAG_NUMERO_RUT,FormateaRUTCertificado(FNumeroDocumentoCertificado));
        RichText := ReplaceTag(RichText, TAG_RELACION,FRelacion);
        RichText := ReplaceTag(RichText, TAG_PATENTE,FPatente);

        dsInfracciones.DataSet.First;
        // Asignamos los datos a la rejilla del certificado
        for Veces := 1 to FLineasDetalleCertificado do begin
            RichText := ReplaceTag(RichText, Format(TAG_PATENTE_N,[Veces]),iif(not dsInfracciones.DataSet.Eof, dsInfracciones.DataSet.FieldByName('Patente').AsString, ''));
            RichText := ReplaceTag(RichText, Format(TAG_FECHA_N,[Veces]),iif(not dsInfracciones.DataSet.Eof, DevolverFechaFormateada('dd/mm/yyyy', dsInfracciones.DataSet.FieldByName('FechaInfraccion').AsDateTime), '')); // Rev. 1 (SS 719)
            RichText := ReplaceTag(RichText, Format(TAG_FEC_ANUL_N,[Veces]),iif(not dsInfracciones.DataSet.Eof, DevolverFechaFormateada('dd/mm/yyyy', dsInfracciones.DataSet.FieldByName('FechaAnulacion').AsDateTime), '')); // Rev. 1 (SS 719)

            if not dsInfracciones.DataSet.Eof then dsInfracciones.DataSet.Next;
        end;
    end;

    // Si tenemos m�s registros que los previstos en la plantilla del certificado, asignamos los
    // datos en el Anexo, pues lo imprimiremos.
    if dsInfracciones.DataSet.RecordCount > FLineasDetalleCertificado then
        with ppRichText2 do begin
            LoadFromFile(FArchivoPlantillaAnexo);
            // Rev. 1 (SS 719)
            RichText := ReplaceTag(RichText, TAG_DIA,DevolverFechaFormateada('dd',FFecha));
            RichText := ReplaceTag(RichText, TAG_MES,DevolverFechaFormateada('mmmm',FFecha));
            RichText := ReplaceTag(RichText, TAG_ANIO,DevolverFechaFormateada('yyyy',FFecha));
            // Fin Rev. 1 (SS 719)
            RichText := ReplaceTag(RichText, TAG_TRATAMIENTO,FTratamiento);
            RichText := ReplaceTag(RichText, TAG_CLIENTE_CERTIFICADO,FClienteCertificado);
            RichText := ReplaceTag(RichText, TAG_NUMERO_RUT,FormateaRUTCertificado(FNumeroDocumentoCertificado));
            RichText := ReplaceTag(RichText, TAG_RELACION,FRelacion);
            RichText := ReplaceTag(RichText, TAG_PATENTE,FPatente);

            pplblCodigoVerificador2.Caption := pplblCodigoVerificador.Caption;
        end;
end;

{******************************** Function Header ******************************
Procedure Name: ExportarReporteAPDF
Author : pdominguez
Date Created : 02/10/2009
Parameters : Reporte: TppReport; var Resultado, Error: string
Return Value : boolean
Description : SS 719
    - Exporta el reporte pasado a PDF.
*******************************************************************************}
function TReportCertificadoInfraccionesForm.ExportarReporteAPDF(Reporte: TppReport; var Resultado, Error: string): boolean;
    var
        Exportador: TppPDFDevice;
        Salida: TmemoryStream;
        Salida2: TStringStream;
begin
    try
        try
            Salida := TmemoryStream.Create;
            Salida2 := TStringStream.Create('');
            Error := '';
            Exportador := TppPDFDevice.Create(nil);

            TppPDFDevice(Exportador).PDFSettings.OptimizeImageExport := False;
            TppPDFDevice(Exportador).PDFSettings.ScaleImages := False;
            TppPDFDevice(Exportador).FileName := GetTempDir + FNumeroDocumentoCertificado + '.pdf';

//            Exportador.PrintToStream := True;
  //          Exportador.ReportStream := Salida;

            Exportador.Publisher := Reporte.Publisher;
            Exportador.Reset;
            Reporte.PrintToDevices;

            Salida.LoadFromFile(TppPDFDevice(Exportador).FileName);

            Salida.Position := 0;
            Salida.SaveToStream(Salida2);
            Resultado := Salida2.DataString;

            if FileExists(TppPDFDevice(Exportador).FileName) then DeleteFile(TppPDFDevice(Exportador).FileName);
            
            Result := true;

        except
            on E:Exception do begin
                Result := false;
                Error := '[ExportarReporte] ' + e.message;
            end;
        end;
    finally
        if Assigned(Exportador) then FreeAndNil(Exportador);
        if Assigned(Salida) then FreeAndNil(Salida);
        if Assigned(Salida2) then FreeAndNil(Salida2);
    end;
end;

{******************************* Procedure Header ******************************
Procedure Name: AuditarCertificado
Author : pdominguez
Date Created : 03/11/2009
Parameters : var DescError: String
Description : SS 719
    - Audita la impresi�n del Certificado.

Revision 4
    Author: pdominguez
    Date  : 04/07/2010
    Description: Infractores Fase 2
        - Se a�ade la asignaci�n al par�metro @CodigoConcesionaria
*******************************************************************************}
procedure TReportCertificadoInfraccionesForm.AuditarCertificado(var DescError: String);
begin
    try
        with spAgregarAHistoricoCertificadosInfraccionesEmitidos do begin
            Parameters.ParamByName('@Patente').Value := FPatente;
            Parameters.ParamByName('@FechaDesde').Value := FFechaDesde;
            Parameters.ParamByName('@FechaHasta').Value := FFechaHasta;
            Parameters.ParamByName('@NumeroDocumento').Value := FNumeroDocumentoCertificado;
            Parameters.ParamByName('@usuario').Value := FUsuario;
            Parameters.ParamByName('@Personeria').Value := iif(Trim(FPersoneria) <> '', FPersoneria, NULL);
            Parameters.ParamByName('@Sexo').Value := iif(Trim(FSexo) <> '', FSexo, NULL);
            Parameters.ParamByName('@NombreCertificado').Value := FClienteCertificado;
            Parameters.ParamByName('@FechaCertificado').Value := FFecha;
            Parameters.ParamByName('@CodigoVerificador').Value := NULL;
            Parameters.ParamByName('@CodigoVerificadorDV').Value := NULL;
            Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
            ExecProc;
            FCodigoVerificacion :=
                IntToStr(Parameters.ParamByName('@CodigoVerificador').Value) +
                IntToStr(Parameters.ParamByName('@CodigoVerificadorDV').Value);
        end;

        while not dsInfracciones.DataSet.Eof do begin
            with spAgregarAHistoricoCertificadosInfraccionesEmitidosDetalle do begin
                Parameters.ParamByName('@IDHistoricoCertificado').Value :=
                    spAgregarAHistoricoCertificadosInfraccionesEmitidos.Parameters.ParamByName('@IDHistoricoCertificado').Value;
                Parameters.ParamByName('@FechaTransito').Value := dsInfracciones.DataSet.FieldByName('FechaInfraccion').AsDateTime;
                Parameters.ParamByName('@FechaPago').Value := dsInfracciones.DataSet.FieldByName('FechaAnulacion').AsDateTime;
                ExecProc;
           end;
           dsInfracciones.DataSet.Next;
        end;

        dsInfracciones.DataSet.First;
    except
        On E:Exception do DescError := E.Message;
    end;
end;

end.
