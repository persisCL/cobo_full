{********************************* File Header ********************************
File Name   : FrmBuscaCalle.pas
Author      : dcalani
Date Created: 24/02/2004
Language    : ES-AR
Description : Permite buscar una calle por multiples campos
*******************************************************************************}

unit FrmBuscaCalle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,UtilProc, ExtCtrls, DPSControls,ADODB, DB,Util,utildb,
  DmiCtrls, ListBoxEx, DBListEx;

ResourceString
    MSG_ERROR_CAPTION_BUSQUEDA = 'No se puede realizar la busqueda.';
    MSG_ERROR_BUSQUEDA         = 'Error al carga el formulario de busqueda.';
    CAPTION_FORM = 'Buscador de Calles de ';

type
  TFormBuscaCalle = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    pnl_botones: TPanel;
    btn_Salir: TDPSButton;
    btn_Aceptar: TDPSButton;
    cbRegiones: TComboBox;
    cbComunas: TComboBox;
    Label1: TLabel;
    lbl_Comuna: TLabel;
    Label4: TLabel;
    txt_nombre: TEdit;
    dbl_Calles: TDBListEx;
    DSCalle: TDataSource;
    lbl_No_Encontro: TLabel;
    BuscarCalles: TADOStoredProc;
    procedure CambioEstadoTabla(DataSet: TDataSet);

    procedure btn_SalirClick(Sender: TObject);
    procedure cbRegionesChange(Sender: TObject);
    procedure cbComunasChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_BuscarClick(Sender: TObject);
    procedure dbl_CallesDblClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure cbRegionesEnter(Sender: TObject);
    procedure cbComunasEnter(Sender: TObject);
    procedure cbCiudadesChange(Sender: TObject);
    procedure txt_nombreChange(Sender: TObject);
  private
    { Private declarations }
    PropiedadCodigoPais:String;

    FCodigoRegionAnterior: AnsiString;
    FCodigoComunaAnterior: AnsiString;

    function GetCalle:String;
    function GetAlias:String;
    function GetCodigoCalle:Integer;
    function GetCodigoTipoCalle:Integer;
    function GetCodigoPais:String;
    function GetCodigoRegion:String;
    function GetCodigoComuna:String;
    function GetNumeroDesde:integer;
    function GetNumeroHasta:integer;
    function GetCodigoOriginalGEO:String;
    function DarRegion:String;
    function DarComuna:String;
    procedure BuscarCalle;

  public
    { Public declarations }
    function inicializar(Pais:String;Region:String='';Comuna:String='';Numero:String='';Descripcion:String=''):boolean;
    property Descripcion:String read GetCalle;
    property Alias:String read GetAlias;
    property CodigoCalle:Integer read GetCodigoCalle;
    property CodigoTipoCalle:Integer read GetCodigoTipoCalle;
    property CodigoPais:String read GetCodigoPais;
    property CodigoRegion:String read GetCodigoRegion;
    property CodigoComuna:String read GetCodigoComuna;
    property NumeroDesde:integer read GetNumeroDesde;
    property NumeroHasta:integer read GetNumeroHasta;
    property CodigoOriginalGEO:String read GetCodigoOriginalGEO;
  end;

var
  FormBuscaCalle: TFormBuscaCalle;

implementation

uses PeaProcs, PeaTypes, DMConnection;

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: TFormBuscaCalle.inicializa
Author       : dcalani
Date Created : 03/03/2004
Description  : Carga todos los datos en los combos, devuelve TRUE si cargo todo bien, caso contrario FALSE
Parameters   : Pais:String;Region:String='';Comuna:String='';Ciudad:String='';Numero:integer=0;Descripcion:String=''
Return Value : boolean
*******************************************************************************}
function TFormBuscaCalle.inicializar(Pais:String;Region:String='';Comuna:String='';Numero:String='';Descripcion:String=''):boolean;
begin
    result:=true;
    try
        Region:=trim(Region);
        Comuna:=trim(Comuna);
        pais:=trim(pais);

        PropiedadCodigoPais:=Pais;
        self.Caption:=CAPTION_FORM+QueryGetValue(DMConnections.BaseCAC,format('select dbo.ObtenerDescripcionPais(''%s'')',[Pais]));

        FCodigoRegionAnterior           := Region;
        FCodigoComunaAnterior           := Comuna;
        CargarRegiones(DMConnections.BaseCAC, cbRegiones, Pais, Region);
        CargarComunas(DMConnections.BaseCAC, cbComunas, PropiedadCodigoPais, Region, Comuna, true);

        if trim(Descripcion) <> '' then txt_nombre.Text:=Descripcion;
    except
        On E: Exception do begin
            result:=false;
            MsgBoxErr(MSG_ERROR_BUSQUEDA, E.Message, MSG_ERROR_CAPTION_BUSQUEDA, MB_ICONSTOP);
        end;
    end;
end;

{******************************** Function Header *******************************
  Function Name: TFormBuscaCalle.btn_SalirClick
  Author:        Malisia, Fabio Nazareno <fmalisia@dpsautomation.com>
  Date Created:  18-Mar-2004
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Cierra el formulario.
********************************************************************************}
procedure TFormBuscaCalle.btn_SalirClick(Sender: TObject);
begin
    close;
end;

{******************************** Function Header *******************************
  Function Name: TFormBuscaCalle.cbRegionesChange
  Author:        Malisia, Fabio Nazareno <fmalisia@dpsautomation.com>
  Date Created:  18-Mar-2004
  Parameters:    Sender: TObject
  Return Value:  None
  Description:
********************************************************************************}
procedure TFormBuscaCalle.cbRegionesChange(Sender: TObject);
begin
    if (FCodigoRegionAnterior <> DarRegion) or (DarRegion = SIN_ESPECIFICAR) then begin
        CargarComunas(DMConnections.BaseCAC, cbComunas, PropiedadCodigoPais, DarRegion, DarComuna, true);
        cbComunas.OnChange(cbComunas);
    end;
    FCodigoRegionAnterior   := DarRegion;
    FCodigoComunaAnterior   := DarComuna;
end;

{******************************** Function Header *******************************
  Function Name: TFormBuscaCalle.cbComunasChange
  Author:        Malisia, Fabio Nazareno <fmalisia@dpsautomation.com>
  Date Created:  18-Mar-2004
  Parameters:    Sender: TObject
  Return Value:  None
  Description:
********************************************************************************}
procedure TFormBuscaCalle.cbComunasChange(Sender: TObject);
begin
    if (FCodigoComunaAnterior <> DarComuna) or (DarComuna=SIN_ESPECIFICAR) then begin
        BuscarCalle;
    end;
    FCodigoComunaAnterior   := DarComuna;
end;


procedure TFormBuscaCalle.FormShow(Sender: TObject);
begin
    txt_nombre.setfocus;
    txt_nombre.SelStart:=length(txt_nombre.Text);
    txt_nombre.SelLength:=0;
end;

procedure TFormBuscaCalle.btn_BuscarClick(Sender: TObject);
begin
    BuscarCalle;
end;

procedure TFormBuscaCalle.dbl_CallesDblClick(Sender: TObject);
begin
    if btn_Aceptar.Enabled then btn_Aceptar.Click;
end;

{******************************** Function Header ******************************
Function Name: TFormBuscaCalle.CambioEstadoTabla
Author       : dcalani
Date Created : 03/03/2004
Description  : Deacuerdo a que si la busqueda devuelve alguna calle, se habilita o no el boton de "aceptar"  
Parameters   : DataSet: TDataSet
Return Value : None
*******************************************************************************}
procedure TFormBuscaCalle.CambioEstadoTabla(DataSet: TDataSet);
begin
    if BuscarCalles.Active then begin
        btn_Aceptar.Enabled:=BuscarCalles.RecordCount<>0;
        lbl_No_Encontro.Visible:=not(btn_Aceptar.Enabled);
    end else begin
        btn_Aceptar.Enabled:=false;
        lbl_No_Encontro.Visible:=false;
    end;
end;

procedure TFormBuscaCalle.btn_AceptarClick(Sender: TObject);
begin
    ModalResult:=mrOk;
end;

function TFormBuscaCalle.GetCalle:String;
begin
    if BuscarCalles.Active then Result:=Trim(BuscarCalles.FieldByName('Descripcion').AsString)
    else Result:='';
end;

function TFormBuscaCalle.GetAlias:String;
begin
    if BuscarCalles.Active then Result:=Trim(BuscarCalles.FieldByName('Alias').AsString)
    else Result:='';
end;

function TFormBuscaCalle.GetCodigoCalle:Integer;
begin
    if BuscarCalles.Active then Result:=StrToInt(Trim(BuscarCalles.FieldByName('CodigoCalle').AsString))
    else Result:=0;
end;

function TFormBuscaCalle.GetCodigoTipoCalle:integer;
begin
    if BuscarCalles.Active then Result:=StrToInt(Trim(BuscarCalles.FieldByName('CodigoTipoCalle').AsString))
    else Result:=0;
end;

function TFormBuscaCalle.GetCodigoPais:String;
begin
    if BuscarCalles.Active then Result:=Trim(BuscarCalles.FieldByName('CodigoPais').AsString)
    else Result:='';
end;

function TFormBuscaCalle.GetCodigoRegion:String;
begin
    if BuscarCalles.Active then Result:=Trim(BuscarCalles.FieldByName('CodigoRegion').AsString)
    else Result:='';
end;

function TFormBuscaCalle.GetCodigoComuna:String;
begin
    if BuscarCalles.Active then Result:=Trim(BuscarCalles.FieldByName('CodigoComuna').AsString)
    else Result:='';
end;

function TFormBuscaCalle.GetNumeroDesde:integer;
begin
    if BuscarCalles.Active then Result:=StrToInt(Trim(BuscarCalles.FieldByName('NumeroDesde').AsString))
    else Result:=0;
end;

function TFormBuscaCalle.GetNumeroHasta:integer;
begin
    if BuscarCalles.Active then Result:=StrToInt(Trim(BuscarCalles.FieldByName('NumeroHasta').AsString))
    else Result:=0;
end;

function TFormBuscaCalle.GetCodigoOriginalGEO:String;
begin
    if BuscarCalles.Active then Result:=Trim(BuscarCalles.FieldByName('CodigoOriginalGEO').AsString)
    else Result:='';
end;


procedure TFormBuscaCalle.cbRegionesEnter(Sender: TObject);
begin
    FCodigoRegionAnterior := DarRegion; //self.CodigoRegion;
end;

procedure TFormBuscaCalle.cbComunasEnter(Sender: TObject);
begin
    FCodigoComunaAnterior := DarComuna; //self.CodigoComuna;
end;

procedure TFormBuscaCalle.cbCiudadesChange(Sender: TObject);
begin
    cbComunas.Enabled       := False;
    cbComunas.Color         := clBtnFace;
    BuscarCalle;
end;

function TFormBuscaCalle.DarRegion:String;
begin
    result:=Trim(StrRight(cbRegiones.Text,3));
end;

function TFormBuscaCalle.DarComuna:String;
begin
    result:=Trim(StrRight(cbComunas.Text,3))
end;

{******************************** Function Header ******************************
Function Name: TFormBuscaCalle.BuscarCalle
Author       : dcalani
Date Created : 03/03/2004
Description  : Realiza la busqueda propiamente dicha, por lo menos,
                tiene que tener cargada la region y la comuna
Parameters   : None
Return Value : None
*******************************************************************************}
procedure TFormBuscaCalle.BuscarCalle;
begin
    if ((DarRegion=REGION_SANTIAGO) and (DarRegion<>'')) or
       (DarRegion<>REGION_SANTIAGO) then begin

        try
            with BuscarCalles do begin
                close;
                Parameters.ParamByName('@CodigoPais').Value:=PropiedadCodigoPais;
                Parameters.ParamByName('@CodigoRegion').Value:=iif(DarRegion='',NULL,DarRegion);
                Parameters.ParamByName('@CodigoComuna').Value:=iif(DarComuna='',NULL,DarComuna);
                Parameters.ParamByName('@Descripcion').Value:=iif(trim(txt_nombre.Text)<>'',txt_nombre.Text,NULL);
                Open;
            end;
        Except
            On E: Exception do begin
                BuscarCalles.close;
                MsgBoxErr(MSG_ERROR_CAPTION_BUSQUEDA, E.Message, MSG_ERROR_CAPTION_BUSQUEDA, MB_ICONSTOP);
            end;
        end;
    end;
end;

procedure TFormBuscaCalle.txt_nombreChange(Sender: TObject);
begin
    BuscarCalle;
end;

end.

