{ -----------------------------------------------------------------------------
Autor           :   CQuezadaI
Firma           :   SS_1245_CQU_20150224
Fecha           :   24-feb-2015
Descripción     :   Se agrega la opbtención del parámetro general REFINAN_FACTOR_IMPORTE_SUJERIDO
                    y se agrega la constante REFINANCIACION_FACTOR_IMPORTE_SUJERIDO para guardar
                    su valor.

Firma           : SS_1410_MCA_20151027
Descripcion     : se agrega una nueva forma de pago para refinanciacion.
 -----------------------------------------------------------------------------}
unit CobranzasResources;

interface

    const
        { Funciones de Base de Datos }
        SQL_REFINANCIACION_ESTADO_CUOTA_ANULADA              = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_ANULADA()';
        SQL_REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA     = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA()';
        SQL_REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO()';
        SQL_REFINANCIACION_ESTADO_CUOTA_DEPOSITADO	         = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_DEPOSITADO()';
        SQL_REFINANCIACION_ESTADO_CUOTA_EN_COBRANZA	         = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_EN_COBRANZA()';
        SQL_REFINANCIACION_ESTADO_CUOTA_MORA_PACTADA	     = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_MORA_PACTADA()';
        SQL_REFINANCIACION_ESTADO_CUOTA_PAGADA               = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_PAGADA()';
        SQL_REFINANCIACION_ESTADO_CUOTA_PENDIENTE	         = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_PENDIENTE()';
        SQL_REFINANCIACION_ESTADO_CUOTA_PROTESTADO	         = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_CUOTA_PROTESTADO()';

        SQL_REFINANCIACION_ESTADO_CUOTA_DESCRIPCION          = 'SELECT Descripcion FROM RefinanciacionEstadosCuotas WHERE CodigoEstadoCuota = %d';

        SQL_REFINANCIACION_ESTADO_EN_TRAMITE = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_EN_TRAMITE()';
        SQL_REFINANCIACION_ESTADO_ACEPTADA   = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_ACEPTADA()';
        SQL_REFINANCIACION_ESTADO_TERMINADA  = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_TERMINADA()';
        SQL_REFINANCIACION_ESTADO_VIGENTE    = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_VIGENTE()';
        SQL_REFINANCIACION_ESTADO_ANULADA    = 'SELECT dbo.CONST_REFINANCIACION_ESTADO_ANULADA()';
        SQL_REFINANCIACION_FACTOR_IMPORTE_SUJERIDO          = 'SELECT dbo.ObtenerParametroGeneralInt(''REFINAN_FACTOR_IMPORTE_SUJERIDO'')'; // SS_1245_CQU_20150224

        SQL_FORMA_PAGO_AVENIMIENTO         = 'SELECT dbo.CONST_FORMA_PAGO_AVENIMIENTO()';
        SQL_FORMA_PAGO_CHEQUE              = 'SELECT dbo.CONST_FORMA_PAGO_CHEQUE()';
        SQL_FORMA_PAGO_DEBITO_CUENTA       = 'SELECT dbo.CONST_FORMA_PAGO_DEBITO_CUENTA()';
        SQL_FORMA_PAGO_EFECTIVO            = 'SELECT dbo.CONST_FORMA_PAGO_EFECTIVO()';
        SQL_FORMA_PAGO_PAGARE              = 'SELECT dbo.CONST_FORMA_PAGO_PAGARE()';
        SQL_FORMA_PAGO_TARJETA_CREDITO     = 'SELECT dbo.CONST_FORMA_PAGO_TARJETA_CREDITO()';
        SQL_FORMA_PAGO_TARJETA_DEBITO      = 'SELECT dbo.CONST_FORMA_PAGO_TARJETA_DEBITO()';
//        SQL_FORMA_PAGO_TRASPASO            = 'SELECT dbo.CONST_FORMA_PAGO_TRASPASO()';
        SQL_FORMA_PAGO_VALE_VISTA          = 'SELECT dbo.CONST_FORMA_PAGO_VALE_VISTA()';
        SQL_FORMA_PAGO_DEPOSITO_ANTICIPADO = 'SELECT dbo.CONST_FORMA_PAGO_DEPOSITO_ANTICIPADO()';   // SS_637_PDO_20110503
        SQL_FORMA_PAGO_FONDO_NO_RECUPERABLE = 'SELECT dbo.CONST_FORMA_PAGO_FONDO_NO_RECUPERABLE()';   // SS_1410_MCA_20151027
        SQL_FORMA_PAGO_CASTIGO              = 'SELECT dbo.CONST_FORMA_PAGO_CASTIGO()'; // TASK_020_GLE_20161121

        SQL_CANAL_PAGO_AVENIMIENTO  = 'SELECT dbo.CONST_CANAL_PAGO_AVENIMIENTO()';
        SQL_CANAL_PAGO_REPACTACION  = 'SELECT dbo.CONST_CANAL_PAGO_REPACTACION()';

        SQL_CODIGO_BANCO_SANTANDER = 'SELECT dbo.CONST_CODIGO_BANCO_SANTANDER()';

        { Constantes para la pantalla de entrada de Datos frmRefinanciacionEntrarEditarCuotas}
        TIPO_ENTRADA_DATOS_CUOTA             = 1;
        TIPO_ENTRADA_DATOS_PAGO_CUOTA        = 2;
        TIPO_ENTRADA_DATOS_PAGO_COMPROBANTES = 3;

        PAGINA_ENTRADA_DATOS_NO_DEFINIDA       = 0;
        PAGINA_ENTRADA_DATOS_EFECTIVO          = 1;
        PAGINA_ENTRADA_DATOS_TARJETA           = 2;
        PAGINA_ENTRADA_DATOS_CHEQUE            = 3;
        PAGINA_ENTRADA_DATOS_VALE_VISTA        = 4;
        PAGINA_ENTRADA_DATOS_DEBITO_CUENTA_PAC = 5;
        PAGINA_ENTRADA_DATOS_DEBITO_CUENTA_PAT = 6;
        PAGINA_ENTRADA_DATOS_TARJETA_CREDITO   = 7;
        PAGINA_ENTRADA_DATOS_TARJETA_DEBITO    = 8;
        PAGINA_ENTRADA_DATOS_PAGARE            = 9;
        PAGINA_ENTRADA_DATOS_EFECTIVO_CAMBIO   = 11;        
        PAGINA_ENTRADA_DATOS_SELECCIONE        = 12;
        PAGINA_ENTRADA_DATOS_CASTIGO           = 13; // TASK_020_GLE_20161121

    var
        { pseudoConstantes que almacenarán los valores de las funciones en BD }
        REFINANCIACION_ESTADO_CUOTA_ANULADA,
        REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA,
        REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO,
        REFINANCIACION_ESTADO_CUOTA_DEPOSITADO,
        REFINANCIACION_ESTADO_CUOTA_EN_COBRANZA,
        REFINANCIACION_ESTADO_CUOTA_MORA_PACTADA,
        REFINANCIACION_ESTADO_CUOTA_PAGADA,
        REFINANCIACION_ESTADO_CUOTA_PENDIENTE,
        REFINANCIACION_ESTADO_CUOTA_PROTESTADO: Byte;

        REFINANCIACION_ESTADO_CUOTA_PENDIENTE_DESCRIPCION,
        REFINANCIACION_ESTADO_CUOTA_ANULADA_DESCRIPCION,
        REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO_DESCRIPCION: string[30];

        REFINANCIACION_ESTADO_EN_TRAMITE,
        REFINANCIACION_ESTADO_ACEPTADA,
        REFINANCIACION_ESTADO_TERMINADA,
        REFINANCIACION_ESTADO_VIGENTE,
        REFINANCIACION_ESTADO_ANULADA: Byte;

        FORMA_PAGO_AVENIMIENTO,
        FORMA_PAGO_CHEQUE,
        FORMA_PAGO_DEBITO_CUENTA,
        FORMA_PAGO_EFECTIVO,
        FORMA_PAGO_PAGARE,
        FORMA_PAGO_TARJETA_CREDITO,
        FORMA_PAGO_TARJETA_DEBITO,
//        FORMA_PAGO_TRASPASO,
        FORMA_PAGO_FONDO_NO_RECUPERABLE,                                        //SS_1410_MCA_20151027
        FORMA_PAGO_VALE_VISTA,
        FORMA_PAGO_DEPOSITO_ANTICIPADO: Byte;  // SS_637_PDO_20110503
        FORMA_PAGO_CASTIGO: Byte; // TASK_020_GLE_20161121

        CANAL_PAGO_AVENIMIENTO,
        CANAL_PAGO_REPACTACION: Byte;

        CODIGO_BANCO_SANTANDER: Byte;
        REFINANCIACION_FACTOR_IMPORTE_SUJERIDO : Integer;  // SS_1245_CQU_20150224
        { Variables de Control de Carga de pseudoConstantes }
        ConstantesCobranzasRefinanciacionCargadas,
        ConstantesCobranzasCanalesFormasPagoCargadas: Boolean;

    { Procedimientos de carga de pseudoConstantes }
    procedure CargarConstantesCobranzasRefinanciacion;
    procedure CargarConstantesCobranzasCanalesFormasPago;

implementation

uses
    // Delphi
    SysUtils,

    // Proyecto CN
    SysUtilsCN,
    frmMuestraMensaje,

    // A Revisar
    DMConnection, UtilDB;

procedure CargarConstantesCobranzasRefinanciacion;
    resourcestring
        ERROR_TITULO_CARGAR_CONSTANTES_COBRANZAS  = 'Carga de Constantes de Cobranzas - Refinanciación';
        ERROR_MENSAJE_CARGAR_CONSTANTES_COBRANZAS = 'Se Produjo un Error al Cargar las Constantes de Cobranzas - Refinanciación. Error: %s';

        MSG_CARGANDO_ESTADOS_CUOTAS_REFINANCIACION = 'Cargando Constantes Estados Cuotas Refinanciación ...';
        MSG_CARGANDO_ESTADOS_REFINANCIACION        = 'Cargando Constantes Estados Refinanciación ...';
begin
    if not ConstantesCobranzasRefinanciacionCargadas then try
        CambiarEstadoCursor(CURSOR_RELOJ);
        try
            TPanelMensajesForm.MuestraMensaje(MSG_CARGANDO_ESTADOS_REFINANCIACION, False);

            REFINANCIACION_ESTADO_EN_TRAMITE := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_EN_TRAMITE);
            REFINANCIACION_ESTADO_ACEPTADA   := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_ACEPTADA);
            REFINANCIACION_ESTADO_TERMINADA  := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_TERMINADA);
            REFINANCIACION_ESTADO_VIGENTE    := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_VIGENTE);
            REFINANCIACION_ESTADO_ANULADA    := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_ANULADA);

            TPanelMensajesForm.MuestraMensaje(MSG_CARGANDO_ESTADOS_CUOTAS_REFINANCIACION, False);

            REFINANCIACION_ESTADO_CUOTA_ANULADA              := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_ANULADA);
            REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA     := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA);
            REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO);
            REFINANCIACION_ESTADO_CUOTA_DEPOSITADO           := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_DEPOSITADO);
            REFINANCIACION_ESTADO_CUOTA_EN_COBRANZA          := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_EN_COBRANZA);
            REFINANCIACION_ESTADO_CUOTA_MORA_PACTADA         := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_MORA_PACTADA);
            REFINANCIACION_ESTADO_CUOTA_PAGADA               := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_PAGADA);
            REFINANCIACION_ESTADO_CUOTA_PENDIENTE            := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_PENDIENTE);
            REFINANCIACION_ESTADO_CUOTA_PROTESTADO           := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_ESTADO_CUOTA_PROTESTADO);

            REFINANCIACION_ESTADO_CUOTA_PENDIENTE_DESCRIPCION := QueryGetValue(DMConnections.BaseCAC, Format(SQL_REFINANCIACION_ESTADO_CUOTA_DESCRIPCION, [REFINANCIACION_ESTADO_CUOTA_PENDIENTE]));
            REFINANCIACION_ESTADO_CUOTA_ANULADA_DESCRIPCION   := QueryGetValue(DMConnections.BaseCAC, Format(SQL_REFINANCIACION_ESTADO_CUOTA_DESCRIPCION, [REFINANCIACION_ESTADO_CUOTA_ANULADA]));

            REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO_DESCRIPCION   := QueryGetValue(DMConnections.BaseCAC, Format(SQL_REFINANCIACION_ESTADO_CUOTA_DESCRIPCION, [REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO]));
            REFINANCIACION_FACTOR_IMPORTE_SUJERIDO           := QueryGetValueInt(DMConnections.BaseCAC, SQL_REFINANCIACION_FACTOR_IMPORTE_SUJERIDO);    // SS_1245_CQU_20150224

            CODIGO_BANCO_SANTANDER := QueryGetValueInt(DMConnections.BaseCAC, SQL_CODIGO_BANCO_SANTANDER);

            ConstantesCobranzasRefinanciacionCargadas := True;
        except
            on e: exception do begin
                raise EErrorExceptionCN.Create(
                            ERROR_TITULO_CARGAR_CONSTANTES_COBRANZAS,
                            Format(ERROR_MENSAJE_CARGAR_CONSTANTES_COBRANZAS, [e.Message]));
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure CargarConstantesCobranzasCanalesFormasPago;
    resourcestring
        ERROR_TITULO_CARGAR_CONSTANTES_COBRANZAS  = 'Carga de Constantes de Cobranzas - Canales y Formas de Pago';
        ERROR_MENSAJE_CARGAR_CONSTANTES_COBRANZAS = 'Se Produjo un Error al Cargar las Constantes de Cobranzas - Canales y Formas de Pago. Error: %s';

        MSG_CARGANDO_FORMAS_DE_PAGO               = 'Cargando Constantes de Formas de Pago ...';
        MSG_CARGANDO_CANALES_DE_PAGO              = 'Cargando Constantes de Canales de Pago ...';
begin
    if not ConstantesCobranzasCanalesFormasPagoCargadas then try
        CambiarEstadoCursor(CURSOR_RELOJ);
        try
            TPanelMensajesForm.MuestraMensaje(MSG_CARGANDO_CANALES_DE_PAGO, False);

            CANAL_PAGO_AVENIMIENTO     := QueryGetValueInt(DMConnections.BaseCAC, SQL_CANAL_PAGO_AVENIMIENTO);
            CANAL_PAGO_REPACTACION     := QueryGetValueInt(DMConnections.BaseCAC, SQL_CANAL_PAGO_REPACTACION);

            TPanelMensajesForm.MuestraMensaje(MSG_CARGANDO_FORMAS_DE_PAGO, False);

            FORMA_PAGO_AVENIMIENTO         := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_AVENIMIENTO);
            FORMA_PAGO_CHEQUE              := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_CHEQUE);
            FORMA_PAGO_DEBITO_CUENTA       := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_DEBITO_CUENTA);
            FORMA_PAGO_EFECTIVO            := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_EFECTIVO);
            FORMA_PAGO_PAGARE              := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_PAGARE);
            FORMA_PAGO_TARJETA_CREDITO     := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_TARJETA_CREDITO);
            FORMA_PAGO_TARJETA_DEBITO      := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_TARJETA_DEBITO);
//            FORMA_PAGO_TRASPASO            := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_TRASPASO);
            FORMA_PAGO_VALE_VISTA          := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_VALE_VISTA);
            FORMA_PAGO_DEPOSITO_ANTICIPADO := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_DEPOSITO_ANTICIPADO);  // SS_637_PDO_20110503
            FORMA_PAGO_FONDO_NO_RECUPERABLE := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_FONDO_NO_RECUPERABLE);			//SS_1410_MCA_20151027
            FORMA_PAGO_CASTIGO             := 14; //QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_CASTIGO); // TASK_020_GLE_20161121
            ConstantesCobranzasCanalesFormasPagoCargadas := True;
        except
            on e: exception do begin
                raise EErrorExceptionCN.Create(
                            ERROR_TITULO_CARGAR_CONSTANTES_COBRANZAS,
                            Format(ERROR_MENSAJE_CARGAR_CONSTANTES_COBRANZAS, [e.Message]));
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

initialization
   ConstantesCobranzasRefinanciacionCargadas    := False;
   ConstantesCobranzasCanalesFormasPagoCargadas := False;

finalization

end.
