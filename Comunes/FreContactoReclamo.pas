{-------------------------------------------------------------------------------
 File Name: FreContactoReclamo.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Seccion con la informaci�n de la persona que reclama
			  y los datos de como desea ser contactado
Revision 1
Author: JConcheyro
Date : 14/1/2005
Descripcion: Daba un AV cuando se ingresaba a facturacion/consultas/ComprobantesEmitidos
y seleccionando un comprobante y se pedia cargar un reclamo. El metodo SetConvenio setea el numero
de documento y cuando quiere obtener el nombre del cliente , el txtNumeroDocumentoChange ya le habia cerrado
la consulta.

Revision 2
Author: jconcheyro
Date : 03/5/2006
Descripcion: Haciendo un reclamo por transitos desde Cartola a un RUT con varios convenios
el frmReclamoTransito terminaba apuntando a un convenio que no era el que se hab�a pedido
porque al setear por ultima vez el rut pisaba el codigo de convenio actual con el pirmero de la
lista de convenios de ese rut. SS 181

Revision 3
Author: mpiazza
Date : 19/01/2009
Descripcion: mantener el dato de telefono y mail incluso si la opcion sea no contactar el cliente
Se modifica GetTelefono y GetEmail SS 689

Revision 4
Author: mpiazza
Date : 09/02/2009
Descripcion: se agrega validacion de codigo de area.

Revision 5
Date        : 25/02/2009
Author      : mpiazza
Description :  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
              los bloqueos de tablas en la lectura

Revision :
                  Author : vpaszkowicz
                  Date : 17/03/2009
                  Description :Preparo el m�todo para obtener los datos de la
                  persona de 2 maneras: 
                  1)si abro un reclamo desde el CAC
                  Nuevo Reclamo. Para esto el usuario setear� el n�mero de do-
                  cumento en el edit y activar� el OnChange.
                  2)Vengo de un men� en donde tengo el c�digo de convenio selec-
                  cionado, entonces le proporciono el codigoCliente para obtener
                  datos de la persona.
                  Deshabilito el OnChange del rut cuando me voy del edit.

Firma       :  SS_1120_MVI_20130820
Descripcion :  Se cambia el TComboBox por un TVariantComboBox para el NumeroConvenio.
               Se crea el procedimiento txtnumeroconvenioDrawItem para pintar los Convenios de baja.

Firma       : SS_1156_NDR_20140203
Descripcion :  Se valida que sea obligatorio elegir un convenio

-------------------------------------------------------------------------------}
unit FreContactoReclamo;

interface

uses
  //FrecontactoReclamo
  DMConnection,
  Util,
  Utildb,
  UtilProc,
  PeaProcs,
  RStrings,
  BuscaClientes,             //Busca Clientes
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DmiCtrls, VariantComboBox,DB, ComCtrls, ADODB,
  PeaTypes;                                                                         //SS_1120_MVI_20130820

type
  TFrameContactoReclamo = class(TFrame)
    gbUsuario: TGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    txtNombre: TEdit;
    gbContacto: TGroupBox;
    rbTelefono: TRadioButton;
    rbEmail: TRadioButton;
    rbNoContactar: TRadioButton;
    cbCodigoArea: TComboBox;
    txtEmail: TEdit;
    txtTelefono: TEdit;
    LNumeroConvenio: TLabel;
    SPObtenerConveniosxNumeroDocumento: TADOStoredProc;
    lblEnviarMail: TLabel;
    GBPersona: TGroupBox;
	Label3: TLabel;
    TXTNombrePersona: TEdit;
    spObtenerDatosPersonaReclamo: TADOStoredProc;
    lblVer: TLabel;
    RBcarta: TRadioButton;
    txtNumeroDocumento: TPickEdit;
    TxtNumeroConvenio: TVariantComboBox;
    btnNuevo: TButton;                                                    //SS_1120_MVI_20130820
    procedure CambioContactar(Sender: TObject);
    procedure txtNumeroDocumentoChange(Sender: TObject);
    procedure TxtNumeroConvenioChange(Sender: TObject);
    procedure lblEnviarMailClick(Sender: TObject);
    procedure lblEnviarMailMouseEnter(Sender: TObject);
    procedure lblEnviarMailMouseLeave(Sender: TObject);
    procedure lblVerClick(Sender: TObject);
    procedure lblVerMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
	procedure txtNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure txtNumeroDocumentoButtonClick(Sender: TObject);
    procedure txtEmailChange(Sender: TObject);
    procedure txtNumeroDocumentoEnter(Sender: TObject);
    procedure txtNumeroDocumentoExit(Sender: TObject);
    procedure TxtNumeroConvenioDrawItem(Control: TWinControl; Index: Integer;             //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);
  private
    { Private declarations }
    FPatente: AnsiString;
    FCodigoPersona :integer;
    FVehiculo: Integer;
    FContactoReclamo: Boolean;
    FQContactoReclamo: SmallInt;
    procedure SetConvenio(const Value: Integer);
    procedure SetVehiculo(const Value: Integer);
    procedure SetPatente(const Value: AnsiString);
    function GetCodigoDocumento: AnsiString;
    procedure SetCodigoDocumento(const Value: AnsiString);
    function GetNumeroDocumento: AnsiString;
    procedure SetNumeroDocumento(const Value: AnsiString);
    function GetCodigoArea: Integer;
    procedure SetCodigoArea(const Value: Integer);
    function GetTelefono: AnsiString;
    procedure SetTelefono(const Value: AnsiString);
    function GetEmail: AnsiString;
    procedure SetEmail(const Value: AnsiString);
    function GetNombre: AnsiString;
	procedure SetNombre(const Value: AnsiString);
    function GetNombrePersona: AnsiString;
    procedure SetNombrePersona(const Value: AnsiString);
    function GetTipoContacto: AnsiString;
    procedure SetTipoContacto(const Value: AnsiString);
  public
    { Public declarations }
    FConvenio: Integer;
    FRut: string;
    FUltimaBusqueda: TBusquedaCliente; //busca el cliente y devuelve el rut
    procedure ObtenerDatosPersona(NumeroDocumento:string; CodigoPersona: integer = -1);     // TASK_88_PAN_20170215
    function Inicializar: Boolean; overload;
    function Inicializar(CodigoConvenio: Integer): Boolean; overload;
    function Inicializar(CodigoConvenio, IndiceVehiculo: Integer): Boolean; overload;
    function Inicializar(Patente: AnsiString): Boolean; overload;
    Function Deshabilitar: Boolean;
    function Validar: Boolean;
    function ValidoConvenio: Boolean;
	procedure LoadFromDataset(DS: TDataset);
    //
    property CodigoConvenio: Integer read FConvenio write SetConvenio;
    property IndiceVehiculo: Integer read FVehiculo write SetVehiculo;
    property Patente: AnsiString read FPatente write SetPatente;
    property CodigoDocumento: AnsiString read GetCodigoDocumento write SetCodigoDocumento;
    property NumeroDocumento: AnsiString read GetNumeroDocumento write SetNumeroDocumento;
    property Nombre: AnsiString read GetNombre write SetNombre;
    property NombrePersona: AnsiString read GetNombrepersona write SetNombrePersona;
    property CodigoArea: Integer read GetCodigoArea write SetCodigoArea;
    property Telefono: AnsiString read GetTelefono write SetTelefono;
    property Email: AnsiString read GetEmail write SetEmail;
    property TipoContacto: AnsiString Read GetTipoContacto write SetTipoContacto;
  end;

implementation

{$R *.dfm}

uses Navigator,                 //Navigator
     FrmInicioConsultaConvenio; //Cartola

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.Inicializar: Boolean;
begin
    FCodigoPersona := 0;
    CargarCodigosAreaTelefono(DMConnections.BaseCAC, cbCodigoArea, 2);
    CambioContactar(nil);
    txtNumeroDocumentoChange(nil);
    txtEmailChange(nil);
    FContactoReclamo:= False;  FQContactoReclamo:= 0;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:
  Description:
  Parameters: CodigoConvenio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.Inicializar(CodigoConvenio: Integer): Boolean;
begin
    Self.CodigoConvenio := CodigoConvenio;
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:
  Description:
  Parameters: Patente: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.Inicializar(Patente: AnsiString): Boolean;
begin
    Patente := Patente;
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:
  Description:
  Parameters: CodigoConvenio, IndiceVehiculo: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.Inicializar(CodigoConvenio, IndiceVehiculo: Integer): Boolean;
begin
    Self.CodigoConvenio := CodigoConvenio;
    Self.IndiceVehiculo := IndiceVehiculo;
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: Deshabilitar
  Author:    lgisuk
  Date Created: 16/06/2005
  Description: Deshabilito los componentes del frame
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFrameContactoReclamo.Deshabilitar: Boolean;
begin
    //Identificacion del usuario
    txtnumerodocumento.Enabled:= False;
	txtnombre.Enabled         := False;
    txtnumeroconvenio.Enabled := False;

    //Identificaci�n de la persona que llama
    txtnombrepersona.Enabled  := False;

    //Informacion de Contacto
    rbnocontactar.Enabled     := False;
    rbtelefono.Enabled        := False;
    cbcodigoarea.Enabled      := False;
    txttelefono.Enabled       := False;
    rbemail.Enabled           := False;
    txtemail.Enabled          := False;
    rbcarta.Enabled           := False;
    Result:=true;
end;

{-----------------------------------------------------------------------------
  Function Name: CambioContactar
  Author:
  Date Created:
  Description:  define por que medio sera contactado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.CambioContactar(Sender: TObject);
begin
    if rbTelefono.Checked then begin
        cbCodigoArea.Enabled := True;
        cbCodigoArea.TabStop := True;
        cbCodigoArea.Color := clWindow;
        txtTelefono.ReadOnly := False;
        txtTelefono.TabStop := True;
        txtTelefono.Color := clWindow;
    end else begin
        cbCodigoArea.Enabled := False;
		cbCodigoArea.TabStop := False;
        cbCodigoArea.Color := clBtnFace;
        txtTelefono.ReadOnly := True;
        txtTelefono.TabStop := False;
        txtTelefono.Color := clBtnFace;
    end;
    if rbEmail.Checked then begin
        txtEmail.ReadOnly := False;
        txtEmail.TabStop := True;
        txtEmail.Color := clWindow;
    end else begin
        txtEmail.ReadOnly := True;
        txtEmail.TabStop := False;
        txtEmail.Color := clBtnFace;
    end;
end;

function TFrameContactoReclamo.ValidoConvenio: Boolean;
resourcestring
	MSG_SELECCIONAR_CONVENIO = 'Debe seleccionar un convenio';                    //SS_1156_NDR_20140203
	STR_ERROR = 'Error';
var
    Error:string;
begin
    Result := False;


   //BEGIN : SS_1156_NDR_20140203 ------------------------------------------------------
   if txtNumeroConvenio.text=SELECCIONAR then
   begin
		MsgBoxBalloon(MSG_SELECCIONAR_CONVENIO, STR_ERROR, MB_ICONSTOP, txtNumeroConvenio);
		txtNumeroConvenio.SetFocus;
        Exit;
   end;
   //END : SS_1156_NDR_20140203 ------------------------------------------------------

   Result := True;
end;
{-----------------------------------------------------------------------------
  Function Name: Validar
  Author:
  Date Created:
  Description:  Valida los datos Ingresados al form
  Parameters: None
  Return Value: Boolean

  Revision 1
    Author: mpiazza
    Date : 09/02/2009
    Descripcion: se agrega validacion de codigo de area.
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.Validar: Boolean;


      {-----------------------------------------------------------------------
        Function Name: EsNumeroTelefonoValido
        Author:    lgisuk
        Date Created: 21/04/2005
        Description: valida si es un telefono valido
        Parameters: CodigoArea:Integer; Telefono:AnsiString;
        Return Value: Boolean
      ------------------------------------------------------------------------}
	  function EsNumeroTelefonoValido(CodigoArea:Integer; Telefono:AnsiString;
              Var DescripError:String):Boolean;
      var
          SP: TADOStoredProc;
      begin
          SP := TADOStoredProc.Create(nil);
          try
              try
                  SP.Connection := DMConnections.BaseCAC;
                  with SP, Parameters do begin
                      ProcedureName := 'ValidarTelefonoCorrecto';
                      CreateParameter('@CodigoArea', ftInteger, pdInput, 0, CodigoArea);
                      CreateParameter('@Telefono',  ftString, pdInput, 255, Telefono);
                  end;

                  sp.Open;

                  result:=sp.FieldByName('Valido').AsInteger=1;
                  DescripError:=sp.FieldByName('DescripcionError').AsString;
              except
                  result:= false;
                  DescripError:=SIN_ESPECIFICAR;
              end;

          finally
              //listo
              SP.Free;
          end;
      end;


resourcestring
	MSG_INDICAR_NOMBRE = 'Debe indicar nombre y apellido'; // para poder contactar al usuario
	MSG_INDICAR_TELEFONO = 'Debe indicar un n�mero de tel�fono';
    MSG_INDICAR_TELEFONO_VALIDO = 'Debe indicar un n�mero de tel�fono valido';
	MSG_INDICAR_EMAIL = 'Debe indicar una direcci�n de correo electr�nico';
	MSG_INDICAR_EMAIL_VALIDO = 'La direcci�n de correo no es v�lida';
  	MSG_INDICAR_TELEFONO_AREA_VALIDO = 'Debe Seleccionar un c�digo de �rea';
	MSG_SELECCIONAR_CONVENIO = 'Debe seleccionar un convenio';                    //SS_1156_NDR_20140203
	STR_ERROR = 'Error';
var
    Error:string;
begin
    Result := False;

    //valido que carguen el nombre
    if gbUsuario.Visible and {not rbNoContactar.Checked and} (Trim(txtNombre.Text) = '') then begin
		MsgBoxBalloon(MSG_INDICAR_NOMBRE, STR_ERROR, MB_ICONSTOP, txtNombre);
		txtNombre.SetFocus;
        Exit;
    end;

    //valido que carguen un telefono
    if rbTelefono.Checked and (Trim(txtTelefono.Text) = '') then begin
		MsgBoxBalloon(MSG_INDICAR_TELEFONO, STR_ERROR, MB_ICONSTOP, txtTelefono);
		txtTelefono.SetFocus;
        Exit;
    end;

    //valido que exita un codigo de area seleccionado
    if rbTelefono.Checked and (cbcodigoarea.text = '') then begin
		MsgBoxBalloon(MSG_INDICAR_TELEFONO_AREA_VALIDO, STR_ERROR, MB_ICONSTOP, cbcodigoarea);
		cbcodigoarea.SetFocus;
        Exit;
    end;

    //valido que sea un numero de telefono valido
    if rbTelefono.Checked and (not esNumeroTelefonoValido( iif( cbcodigoarea.text = '' , 0 , strtoint(stringreplace(copy(cbcodigoarea.text,1,3),' ','',[rfReplaceAll])) )  , txtTelefono.Text, Error )) then begin
		MsgBoxBalloon(MSG_INDICAR_TELEFONO_VALIDO, STR_ERROR, MB_ICONSTOP, txtTelefono);
		txtTelefono.SetFocus;
        Exit;
    end;

    //Valido que carguen un email
    if rbEmail.Checked and (Trim(txtEmail.Text) = '') then begin
		MsgBoxBalloon(MSG_INDICAR_EMAIL, STR_ERROR, MB_ICONSTOP, txtEmail);
		txtEmail.SetFocus;
		Exit;
    end;

    //verifico que el email sea valido
	if rbEmail.Checked and not IsValidEMailList(txtEmail.Text) then begin
		MsgBoxBalloon(MSG_INDICAR_EMAIL_VALIDO, STR_ERROR, MB_ICONSTOP, txtEmail);
		txtEmail.SetFocus;
        Exit;
    end;


    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: LoadFromDataset
  Author:
  Date Created:
  Description: Cargo los datos de un dataset
  Parameters: DS: TDataset
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.LoadFromDataset(DS: TDataset);
begin
    NumeroDocumento := Trim(DS.FieldByName('NumeroDocumento').AsString);
    Nombre := Trim(DS.FieldByName('ContactoNombre').AsString);
    NombrePersona := Trim(DS.FieldByName('NombrePersona').AsString);
    CodigoConvenio := DS.FieldByName('CodigoConvenio').AsInteger;
    CodigoArea := DS.FieldByName('ContactoCodigoArea').AsInteger;
    Telefono := Trim(DS.FieldByName('ContactoTelefono').AsString);
    Email := Trim(DS.FieldByName('ContactoEmail').AsString);
    TipoContacto := DS.FieldByName('TipoContacto').AsString
end;


{-----------------------------------------------------------------------------
  Function Name: SetConvenio
  Author:    lgisuk
  Date Created: 29/03/2005
  Description: Muestro los Datos que identifican al Usuario
  Parameters: const Value: Integer
  Return Value: None
  Revision 1
  Author : JConcheyro
  Date : 14/1/2005
  Description: Elimino la carga del nombre, porque se encarga de eso el txtNumeroDocumentoChange
  y la linea eliminada estaba dando un AV.
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetConvenio(const Value: Integer);
var
    SQLPersona,SQLNumeroConvenio:string;
    //sCodigoPersona:string;
    //CodigoPersona: Variant;
begin
    FConvenio := Value;
    if FConvenio > 0 then begin
        //obtengo el codigo de persona
        SQLPersona:='Select dbo.obtenercodigocliente ( '+inttostr(codigoconvenio) + ' )';
        FCodigoPersona := QueryGetValueInt(DMConnections.BaseCAC, SQLPersona);
		//obtengo el numero de convenio
		//Revision 2, obtenemos el convenio formateado
 //		SQLNumeroConvenio := 'Select dbo.formatearNumeroConvenio(dbo.obtenernumeroconvenio('+inttostr(codigoconvenio)+'))';	//SS_1120_MVI_20130820

 //		txtnumeroconvenio.text := QueryGetValue(DMConnections.BaseCAC,SQLNumeroConvenio);                                   //SS_1120_MVI_20130820
      txtnumeroconvenio.ItemIndex := txtnumeroconvenio.Items.IndexOfValue(FConvenio);                                       //SS_1120_MVI_20130820
		//Aca hay que traigo el resto de los datos de ese convenio
		{with spobtenerdatospersonareclamo do begin
            close;
            //cargo los parametros
            with parameters do begin
                Refresh;
                CodigoPersona:= iif(sCodigoPersona <> '', scodigopersona, '0');
                ParamByName('@CodigoPersona').value:= CodigoPersona;
            end;
            //abro la consulta
            try
                open;
                //obtengo rut y nombre
                txtnumerodocumento.text:=fieldbyname('Rut').asstring;
                //  Revision 1  elimina    txtnombre.text:=fieldbyname('Nombre').asstring;
				close;
            except
            end;
        end;}
        ObtenerDatosPersona('', FCodigoPersona);
        //inicialmente completo con el nombre del titular del convenio
        if txtnombrepersona.Text = '' then txtnombrepersona.text := txtnombre.text;
        //Inhabilito los campos
        txtnumerodocumento.Enabled:=false;
        txtnombre.Enabled:=false;
        txtnumeroconvenio.Enabled:=false;
    end;
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TFrameContactoReclamo.txtnumeroconvenioDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.TxtNumeroConvenioDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, txtnumeroconvenio.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                    

end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------

{-----------------------------------------------------------------------------
  Function Name: txtNumeroDocumentoButtonClick
  Author:    lgisuk
  Date Created: 09/06/2005
  Description: busca al cliente y devuelve su rut
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.txtNumeroDocumentoButtonClick(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
			txtNumeroDocumento.Text := Trim(f.Persona.NumeroDocumento);
            txtNombre.Text:=Trim(f.Persona.Apellido) + ' ' + Trim(f.Persona.ApellidoMaterno)+' , ' +Trim(f.Persona.Nombre);
	   	end;
	end;
    f.Release;
end;

//cargo los datos de una persona
Procedure TFrameContactoReclamo.ObtenerDatosPersona(NumeroDocumento:string; CodigoPersona: integer = -1);

    {******************************** Function Header ******************************
    Function Name: EsNumeroDocumentoValido
    Author :
    Date Created :
    Description : valido no traer a nadie con el primer cero
    Parameters : NumeroDocumento:String
    Return Value : boolean

    Revision 1:
        Author : ggomez
        Date : 15/12/2005
        Description : Modifiqu� el c�digo para que llame a la funci�n ValidarRUT.
    *******************************************************************************}

    Function EsNumeroDocumentoValido(NumeroDocumento:String):boolean;
    begin
        Result := ValidarRUT(DMConnections.BaseCAC, NumeroDocumento);
    end;

var SQLPersona:string;
    Completo,CodigoArea,Telefono: string;
begin
    Inc(FQContactoReclamo);                                                      // TASK_88_PAN_20170215
    if FQContactoReclamo = 1 then FContactoReclamo:= (CodigoPersona > 0);        // TASK_88_PAN_20170215
    txtnombre.Text := '';
    txttelefono.text := '';
    txtemail.text := '';
    cbCodigoArea.ItemIndex := 0;
    //if FContactoReclamo then txtNumeroDocumento.Text:=
    if CodigoPersona = -1 then begin
        if stringreplace(numerodocumento,' ','',[rfReplaceAll]) = '' then exit;
        if not EsNumeroDocumentoValido( NumeroDocumento ) then begin                        // TASK_88_PAN_20170215
          btnNuevo.Enabled:= ((txtNombre.Text = '') and (txtNumeroDocumento.Text <> ''));    // TASK_88_PAN_20170215
          exit;                                                                             // TASK_88_PAN_20170215
        end;
        SQLPersona:= 'select dbo.ObtenerCodigoPersonaRUT ( '''+ numerodocumento +''' )';
        FCodigoPersona := QueryGetValueint(DMConnections.BaseCAC, SQLPersona);
    end
    else FCodigoPersona := CodigoPersona;
    with spobtenerdatospersonareclamo do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoPersona').value:= FCodigoPersona;
        try
            open;
            //obtengo el nombre
            FRut:= fieldbyname('rut').asstring;
            if (Trim(txtNumeroDocumento.Text) = '') and not Assigned(txtNumeroDocumento.OnChange) then
                txtNumeroDocumento.Text := FRut;
            txtnombre.text := fieldbyname('nombre').asstring;
            //obtengo el telefono completo
            Completo := fieldbyname('telefono').asstring;
            if completo <> '' then begin
                //obtengo el codigo de area
                CodigoArea:=ParseParamByNumber(Completo,1,'-');
                //cargo los codigos de area
                CargarCodigosAreaTelefono(DMConnections.BaseCAC,
                                          cbCodigoArea,
                                          StrToInt(CodigoArea)
                                          );
                //Obtengo el telefono
                Telefono:=ParseParamByNumber(Completo,2,'-');
                txttelefono.text:=Telefono;
            end;
            //obtengo el mail
            txtemail.text:=fieldbyname('Email').asstring;
            close;
        except
        end;
    end;
    TxtNumeroConvenio.Enabled:= (txtNombre.Text <> '');     // TASK_88_PAN_20170215
    TXTNombrePersona.Enabled:= (txtNombre.Text <> '');      // TASK_88_PAN_20170215
    gbContacto.Enabled:= (txtNombre.Text <> '');            // TASK_88_PAN_20170215
    btnNuevo.Enabled:= ((txtNombre.Text = '') and (txtNumeroDocumento.Text <> ''))    // TASK_88_PAN_20170215}
end;


{-----------------------------------------------------------------------------
  Function Name: txtNumeroDocumentoChange
  Author:    lgisuk
  Date Created: 29/03/2005
  Description:  obtengo el nombre y los convenios que corresponden a ese rut
                los cargo en un combobox
  Parameters: Sender: TObject
  Return Value: None

  Revision 1:
      Author : ggomez
      Date : 23/02/2006
      Description : Agregu� que si se ingres� un N�mero de Documento, se ejecut�
        el evento Change de txtNumeroConvenio, para que se sete� la variable
        FCodigoConvenio. Esto lo hice pues los reclamos se almacenaban sin el
        CodigoConvenio si nunca se ejecutaba ese evento.

-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.txtNumeroDocumentoChange(Sender: TObject);
    //cargo los convenios de una persona
    Procedure CargarConvenios(NumeroDocumento:string);
    begin
        TxtNumeroConvenio.text:='';
        TxtNumeroConvenio.Clear;
        //si hay parametreos
//        with SPObtenerConveniosxNumeroDocumento.Parameters do begin                                              //SS_1120_MVI_20130820
//            refresh;                                                                                             //SS_1120_MVI_20130820
//            parambyname('@NumeroDocumento').value:= NumeroDocumento;                                             //SS_1120_MVI_20130820
//        end;                                                                                                     //SS_1120_MVI_20130820
//        //ejecuto la consulta                                                                                    //SS_1120_MVI_20130820
//        try                                                                                                      //SS_1120_MVI_20130820
//            with SPObtenerConveniosxNumeroDocumento do begin                                                     //SS_1120_MVI_20130820
//                Close;                                                                                           //SS_1120_MVI_20130820
//                Open;                                                                                            //SS_1120_MVI_20130820
//                //asigo el primero                                                                               //SS_1120_MVI_20130820
//                if not eof then begin                                                                            //SS_1120_MVI_20130820
//                    TxtNumeroConvenio.text:= trim(fieldbyname('numeroconvenio').asstring);                       //SS_1120_MVI_20130820
//                end;                                                                                             //SS_1120_MVI_20130820
//                //cargo el combo                                                                                 //SS_1120_MVI_20130820
//                while not eof do begin                                                                           //SS_1120_MVI_20130820
//                    TxtNumeroConvenio.Items.Add(trim(fieldbyname('numeroconvenio').asstring));                   //SS_1120_MVI_20130820
//                    next;                                                                                        //SS_1120_MVI_20130820
//                end;                                                                                             //SS_1120_MVI_20130820
//                close;                                                                                           //SS_1120_MVI_20130820
//            end;                                                                                                 //SS_1120_MVI_20130820
//        except                                                                                                   //SS_1120_MVI_20130820
//            on e: Exception do begin                                                                             //SS_1120_MVI_20130820
//                //el stored procedure podria fallar                                                              //SS_1120_MVI_20130820
//                MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR);                                         //SS_1120_MVI_20130820
//            end;                                                                                                 //SS_1120_MVI_20130820
//        end;                                                                                                     //SS_1120_MVI_20130820
                                                                                                                   //SS_1120_MVI_20130820
      CargarConveniosRUT(DMCOnnections.BaseCAC,TxtNumeroConvenio, 'RUT', NumeroDocumento, False, 0, True, True);   //SS_1120_MVI_20130820
      btnNuevo.Enabled:= ((txtNombre.Text = '') and (txtNumeroDocumento.Text <> ''))    // TASK_88_PAN_20170215
    end;

var
    FPersonaAnterior: integer;
begin
    FPersonaAnterior := FCodigoPersona;
	ObtenerDatosPersona(txtNumeroDocumento.text);
    //Revision 2 pongo el if
	if FPersonaAnterior <> FCodigoPersona then Cargarconvenios(txtNumeroDocumento.text);
	//habilito el link si hay un numero de convenio
	lblver.Enabled:= (txtnumeroconvenio.Text <> '');
    if TxtNumeroConvenio.Text <> EmptyStr then begin
        TxtNumeroConvenioChange(txtNumeroDocumento);
    end; //if
end;

procedure TFrameContactoReclamo.txtNumeroDocumentoEnter(Sender: TObject);
begin
    txtNumeroDocumento.OnChange := txtNumeroDocumentoChange;
end;

procedure TFrameContactoReclamo.txtNumeroDocumentoExit(Sender: TObject);
begin
    if FContactoReclamo then
      txtNumeroDocumento.OnChange := Nil
    else
      txtNumeroDocumentoChange(sender);
end;

{-----------------------------------------------------------------------------
  Function Name: TxtNumeroConvenioChange
  Author:    lgisuk
  Date Created: 29/03/2005
  Description: si es un numero de convenio valido, asigno el codigo de convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.TxtNumeroConvenioChange(Sender: TObject);
var
    NumeroConvenio: String;
    SQLConvenio: String;
    Convenio: Integer;
begin
//    //habilito el link si hay un numero de convenio                                                 //SS_1120_MVI_20130820
//    lblver.Enabled:= (txtnumeroconvenio.Text <> '');                                                //SS_1120_MVI_20130820
//    //Obtengo el codigo de convenio                                                                 //SS_1120_MVI_20130820
//    NumeroConvenio  := StringReplace(TXTnumeroconvenio.Text, '-' , EmptyStr, [rfReplaceAll]);       //SS_1120_MVI_20130820
//    SQLConvenio     := 'SELECT dbo.obtenercodigoconvenio ( ''' + NumeroConvenio + ''' )';           //SS_1120_MVI_20130820
//    Convenio        := QueryGetValueint(DMConnections.BaseCAC, SQLConvenio);                        //SS_1120_MVI_20130820
//    if Convenio <> 0 then begin                                                                     //SS_1120_MVI_20130820
//        FConvenio := Convenio;                                                                      //SS_1120_MVI_20130820
//    end;                                                                                            //SS_1120_MVI_20130820
                                                                                                      //SS_1120_MVI_20130820
    if txtnumeroconvenio.items.count > 0 then begin                                                   //SS_1120_MVI_20130820
        FConvenio := txtnumeroconvenio.value;                                                         //SS_1120_MVI_20130820
    end;                                                                                              //SS_1120_MVI_20130820
end;

{-----------------------------------------------------------------------------
  Function Name: SetVehiculo
  Author:
  Date Created:
  Description:
  Parameters: const Value: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetVehiculo(const Value: Integer);
begin
    FVehiculo := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: SetPatente
  Author:
  Date Created:
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetPatente(const Value: AnsiString);
begin
    FPatente := Value;
    if FPatente <> '' then gbUsuario.Visible := False;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoDocumento
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetCodigoDocumento: AnsiString;
begin
    if Trim(txtNumeroDocumento.Text) = '' then Result := ''
      else Result := 'RUT';
end;

{-----------------------------------------------------------------------------
  Function Name: SetCodigoDocumento
  Author:
  Date Created:
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetCodigoDocumento(const Value: AnsiString);
begin

end;

{-----------------------------------------------------------------------------
  Function Name: GetNumeroDocumento
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetNumeroDocumento: AnsiString;
begin
    Result := Trim(txtNumeroDocumento.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: SetNumeroDocumento
  Author:
  Date Created:
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetNumeroDocumento(const Value: AnsiString);
begin
    txtNumeroDocumento.Text := Trim(Value);
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoArea
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: Integer

  Revision 1
  Author: mpiazza
  Date : 19/01/2009
  Descripcion: mantener el dato de telefono y mail incluso si la opcion sea no contactar el cliente
  Se comenta el codigo devolviendo el valor sin validad SS 689
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetCodigoArea: Integer;
begin
   if (cbCodigoArea.ItemIndex <> -1) and (Telefono <> '')
        then Result := IVal(cbCodigoArea.Text)
        else Result := 0;
end;

{-----------------------------------------------------------------------------
  Function Name: SetCodigoArea
  Author:
  Date Created:
  Description:
  Parameters: const Value: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetCodigoArea(const Value: Integer);
Var
    i: Integer;
begin
    cbCodigoArea.ItemIndex := -1;
    for i := 0 to cbCodigoArea.Items.Count - 1 do begin
        if IVal(Copy(cbCodigoArea.items[i], 1, 5)) = Value then begin
            cbCodigoArea.ItemIndex := i;
            break;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetTelefono
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: AnsiString

  Revision 1
  Author: mpiazza
  Date : 19/01/2009
  Descripcion: mantener el dato de telefono y mail incluso si la opcion sea no contactar el cliente
  Se comenta el codigo devolviendo el valor sin validad SS 689
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetTelefono: AnsiString;
begin
Result := Trim(txtTelefono.Text)
{

    if rbTelefono.Checked then Result := Trim(txtTelefono.Text)
      else Result := '';
}
end;

{-----------------------------------------------------------------------------
  Function Name: SetTelefono
  Author:
  Date Created:
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetTelefono(const Value: AnsiString);
begin
    txtTelefono.Text := Trim(Value);
end;

{-----------------------------------------------------------------------------
  Function Name: GetEmail
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: AnsiString

  Revision 1
  Author: mpiazza
  Date : 19/01/2009
  Descripcion: mantener el dato de telefono y mail incluso si la opcion sea no contactar el cliente
  Se comenta el codigo devolviendo el valor sin validad SS 689
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetEmail: AnsiString;
begin
Result := Trim(txtEmail.Text)
{
    if rbEmail.Checked then Result := Trim(txtEmail.Text)
      else Result := '';
}
end;

{-----------------------------------------------------------------------------
  Function Name: SetEmail
  Author:
  Date Created:
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetEmail(const Value: AnsiString);
begin
    txtEmail.Text := Trim(Value);
end;

{-----------------------------------------------------------------------------
  Function Name: GetNombre
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetNombre: AnsiString;
begin
    Result := Trim(txtNombre.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: SetNombre
  Author:
  Date Created:
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetNombre(const Value: AnsiString);
begin
    txtNombre.Text := Trim(Value);
end;

{-----------------------------------------------------------------------------
  Function Name: GetNombrePersona
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetNombrePersona: AnsiString;
begin
    Result := Trim(txtNombrePersona.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: GetTipoContacto
  Author:    lgisuk
  Date Created: 13/06/2005
  Description: obtengo el tipo de contacto
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameContactoReclamo.GetTipoContacto: AnsiString;
var TipoContacto:AnsiString;
begin
    if rbnocontactar.Checked = true then TipoContacto:='N';
    if rbtelefono.Checked    = true then TipoContacto:='T';
    if rbemail.Checked       = true then TipoContacto:='E';
    if rbcarta.Checked       = true then TipoContacto:='C';
    Result := TipoContacto;
end;

{-----------------------------------------------------------------------------
  Function Name: SetTipoContacto
  Author:    lgisuk
  Date Created: 14/06/2005
  Description: asigno el tipo de contacto
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetTipoContacto(const Value: AnsiString);
begin
    if Value = 'N' then rbnocontactar.Checked := true;
    if Value = 'T' then rbtelefono.Checked    := true;
    if Value = 'E' then rbemail.Checked       := true;
    if Value = 'C' then rbcarta.Checked       := true;
end;


{-----------------------------------------------------------------------------
  Function Name: SetNombre
  Author:
  Date Created:
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.SetNombrePersona(const Value: AnsiString);
begin
    txtNombrePersona.Text := Trim(Value);
end;

{-----------------------------------------------------------------------------
  Function Name: lblEnviarMailClick
  Author:    dcalani
  Date Created: 21/04/2005
  Description: abre la ventana para enviar un email
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.lblEnviarMailClick(Sender: TObject);
begin
    if (lblEnviarMail.Cursor = crHandPoint) and (not InvocarCorreoPredeterminado(txtEmail, txtEmail.Text, '', '')) then
        MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, MSG_CAPTION_ENVIO, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: lblEnviarMailMouseEnter
  Author:    dcalani
  Date Created: 21/04/2005
  Description:  cambia el estado del cursor
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.lblEnviarMailMouseEnter(Sender: TObject);
begin
    if trim(txtEmail.Text) = '' then lblEnviarMail.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: lblEnviarMailMouseLeave
  Author:    dcalani
  Date Created: 21/04/2005
  Description:  cambia el estado del cursor
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.lblEnviarMailMouseLeave(Sender: TObject);
begin
    lblEnviarMail.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: txtNumeroDocumentoKeyPress
  Author:    lgisuk
  Date Created: 06/06/2005
  Description: solo permito escribir numeros
  Parameters: Sender: TObject;var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.txtNumeroDocumentoKeyPress(Sender: TObject;var Key: Char);

     //bloquea los valores que no sean numeros
     Procedure numeros(var key:char);
     begin
        if not (key in ['0'..'9','K','k','.',',','-',#8 ]) then begin
           key:=#0;
        end;
     end;

begin
    //solo permito escribir numeros
    numeros(key);
end;

{-----------------------------------------------------------------------------
  Function Name: lblVerMouseMove
  Author:    lgisuk
  Date Created: 06/06/2005
  Description: permito hacer click si hay numero de rut
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.lblVerMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if txtnumeroconvenio.Text = '' then lblver.Cursor :=crDefault	else lblver.Cursor :=crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: lblVerClick
  Author:    lgisuk
  Date Created: 06/06/2005
  Description: Abro la cartola desde el reclamo
  Parameters: Sender: TObject
  Return Value: None

Revision 1:
    Author : mpiazza
    Date : 04/02/2009
    Description : SS-689 En el link de ir a convenio, si el formulario
    esta en memoria lo cierro y lo vuelvo a crear con los nuevos datos
    solicitados perdiendo la seleccion anterior

Revision 2:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.lblVerClick(Sender: TObject);
resourcestring
    MSG_TITLE      = 'Gesti�n de Reclamos';
    MSG_CONVENIO   = 'No se pueden mostrar los datos del cliente porque'+ CRLF +
                     'no existe ese numero de convenio!';
    MSG_ERROR      = 'Error al abrir la Cartola';
var
    NumeroConvenio: String;
    CodigoConvenio: Integer;
	f: TFormInicioConsultaConvenio;
begin
    if txtNombre.Text  = EmptyStr then Exit
    else begin
    
        //si no hay numero de convenio salgo
        if txtnumeroconvenio.Text = EmptyStr then Exit;
        try
            //Obtengo el codigodeconvenio
     //       NumeroConvenio  := StringReplace(TXTnumeroconvenio.Text, '-', EmptyStr, [rfReplaceAll]);                                                                                                              //SS_1120_MVI_20130820
     //       CodigoConvenio  := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoConvenio from convenio  WITH (NOLOCK) where numeroconvenio = ''' + NumeroConvenio + '''');//+inttostr(CodigoPersona));       //SS_1120_MVI_20130820
              CodigoConvenio  := txtnumeroconvenio.value;                                                                                                                                                           //SS_1120_MVI_20130820
            //si no existe ningun convenio para esa persona salgo
            if (CodigoConvenio = 0) then begin
                //no existe ningun convenio con ese numero de convenio
                MsgBoxBalloon(MSG_CONVENIO, MSG_TITLE, MB_ICONSTOP , txtNumeroConvenio);
                txtNumeroConvenio.SetFocus;
                Exit;
            end;

            (* Abrir la cartola.
            Si ya existe una cartola para el RUT, mostrar la cartola existente, sino
            crear otra cartola. *)
            //Revision 3
            if FindFormOrCreate(TFormInicioConsultaConvenio, f) then begin
              f.Close;
              f.Free;
              f := nil;
              FindFormOrCreate(TFormInicioConsultaConvenio, f);
            end;
            if f.Inicializar(CodigoConvenio, 0, 0) then begin
                f.Show;
                f.IrAConvenio(CodigoConvenio);
            end
            else begin
                f.Release;
                Exit;
            end;
        except
            on E: Exception do begin
                 msgboxerr(MSG_ERROR,e.Message,MSG_TITLE,0);
            end;
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: txtEmailChange
  Author:    lgisuk
  Date Created: 29/06/2005
  Description: Habilito el link si hay un mail
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameContactoReclamo.txtEmailChange(Sender: TObject);
begin
    //habilito el link si hay un mail
    lblenviarmail.Enabled:= (txtEmail.Text <> '');
end;

end.
