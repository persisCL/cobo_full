object frmIngresarComprobantes: TfrmIngresarComprobantes
  Left = 205
  Top = 125
  Anchors = []
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Ingresar Comprobantes'
  ClientHeight = 509
  ClientWidth = 664
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    664
    509)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 7
    Top = 463
    Width = 354
    Height = 13
    Caption = 
      'Si desea incluir un '#237'tem en el comprobante haga doble click sobr' +
      'e el mismo'
  end
  object lbl_EstadoEmisionBoleta: TLabel
    Left = 208
    Top = 481
    Width = 143
    Height = 13
    Alignment = taCenter
    Caption = 'Estado emisi'#243'n de boleta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object gbDatosConvenio: TGroupBox
    Left = 6
    Top = 1
    Width = 652
    Height = 77
    Caption = ' Datos del Convenio '
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 22
      Width = 78
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object Label3: TLabel
      Left = 12
      Top = 49
      Width = 105
      Height = 13
      Caption = 'Convenios del Cliente:'
    end
    object Label7: TLabel
      Left = 9
      Top = 305
      Width = 122
      Height = 13
      Caption = 'Detalle del Comprobante: '
    end
    object peRUTCliente: TPickEdit
      Left = 127
      Top = 18
      Width = 139
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = True
      TabOrder = 0
      OnChange = peRUTClienteChange
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object cbConveniosCliente: TVariantComboBox
      Left = 127
      Top = 45
      Width = 218
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbConveniosClienteChange
      Items = <>
    end
    object btnBuscarRUT: TButton
      Left = 272
      Top = 16
      Width = 73
      Height = 25
      Caption = 'Buscar'
      TabOrder = 1
      OnClick = btnBuscarRUTClick
    end
  end
  object gbDatosCliente: TGroupBox
    Left = 6
    Top = 176
    Width = 652
    Height = 100
    Caption = ' Datos del Cliente '
    TabOrder = 2
    object lNombreCompleto: TLabel
      Left = 81
      Top = 19
      Width = 83
      Height = 13
      Caption = 'lNombreCompleto'
    end
    object lDomicilio: TLabel
      Left = 81
      Top = 50
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComuna: TLabel
      Left = 81
      Top = 65
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegion: TLabel
      Left = 81
      Top = 81
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label31: TLabel
      Left = 14
      Top = 19
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Apellido:'
    end
    object Label33: TLabel
      Left = 14
      Top = 50
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label34: TLabel
      Left = 14
      Top = 65
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label35: TLabel
      Left = 14
      Top = 81
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
    object lblRUT: TLabel
      Left = 14
      Top = 34
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'RUT :'
    end
    object lblRUTRUT: TLabel
      Left = 81
      Top = 34
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'lRUT'
    end
  end
  object gbItemsPendientes: TGroupBox
    Left = 5
    Top = 278
    Width = 651
    Height = 182
    Caption = ' Items Pendientes '
    TabOrder = 3
    DesignSize = (
      651
      182)
    object dbItemsPendientes: TDBListEx
      Left = 13
      Top = 21
      Width = 545
      Height = 157
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 25
          Header.Caption = ' '
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Marcado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 25
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'EstaImpreso'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 110
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHora'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 140
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Concepto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 160
          Header.Caption = 'Detalle'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Observaciones'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescImporte'
        end>
      DataSource = dsObtenerPendienteFacturacion
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbItemsPendientesDblClick
      OnDrawText = dbItemsPendientesDrawText
    end
    object btnAgregarItem: TButton
      Left = 556
      Top = 17
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Agregar'
      Enabled = False
      TabOrder = 1
      OnClick = btnAgregarItemClick
    end
    object btnModificarItem: TButton
      Left = 556
      Top = 42
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Modificar'
      Enabled = False
      TabOrder = 2
      OnClick = btnModificarItemClick
    end
    object btnEliminarItem: TButton
      Left = 556
      Top = 67
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Eliminar'
      Enabled = False
      TabOrder = 3
      OnClick = btnEliminarItemClick
    end
  end
  object gbDatosComprobante: TGroupBox
    Left = 6
    Top = 79
    Width = 652
    Height = 90
    Caption = ' Datos del Comprobante '
    TabOrder = 1
    object lbl_NroComprobante: TLabel
      Left = 12
      Top = 42
      Width = 112
      Height = 13
      AutoSize = False
      Caption = 'N'#250'mero Comprobante:'
    end
    object lbl_FechaEmision: TLabel
      Left = 393
      Top = 18
      Width = 87
      Height = 13
      AutoSize = False
      Caption = 'Fecha Emisi'#243'n:'
    end
    object lblFechaVencimiento: TLabel
      Left = 393
      Top = 42
      Width = 99
      Height = 13
      AutoSize = False
      Caption = 'Fecha Vencimiento:'
      Visible = False
    end
    object Label5: TLabel
      Left = 393
      Top = 64
      Width = 56
      Height = 13
      AutoSize = False
      Caption = 'Total :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 12
      Top = 18
      Width = 100
      Height = 13
      Caption = 'Tipos Comprobantes:'
    end
    object lbl_Importe: TLabel
      Left = 497
      Top = 64
      Width = 133
      Height = 13
      AutoSize = False
    end
    object lblNumeroTelevia: TLabel
      Left = 12
      Top = 64
      Width = 112
      Height = 13
      AutoSize = False
      Caption = 'N'#250'mero Telev'#237'a:'
      Visible = False
    end
    object lblCuotas: TLabel
      Left = 280
      Top = 64
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Cuotas :'
      Visible = False
    end
    object edFechaEmision: TDateEdit
      Left = 497
      Top = 14
      Width = 145
      Height = 21
      AutoSelect = False
      TabOrder = 2
      OnChange = edNumeroComprobanteChange
      Date = -693594.000000000000000000
    end
    object edNumeroComprobante: TNumericEdit
      Left = 118
      Top = 38
      Width = 145
      Height = 21
      TabOrder = 3
      OnChange = edNumeroComprobanteChange
    end
    object edFechaVencimiento: TDateEdit
      Left = 497
      Top = 38
      Width = 145
      Height = 21
      AutoSelect = False
      TabOrder = 4
      Visible = False
      OnChange = edNumeroComprobanteChange
      Date = -693594.000000000000000000
    end
    object cbTiposComprobantes: TVariantComboBox
      Left = 118
      Top = 15
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTiposComprobantesChange
      Items = <>
    end
    object chkBoletaManual: TCheckBox
      Left = 280
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Boleta Manual'
      TabOrder = 1
      Visible = False
      OnClick = chkBoletaManualClick
    end
    object edCantidadCuotas: TNumericEdit
      Left = 329
      Top = 61
      Width = 26
      Height = 21
      MaxLength = 2
      TabOrder = 6
      Visible = False
      OnExit = edCantidadCuotasExit
    end
    object edNumeroTelevia: TEdit
      Left = 118
      Top = 61
      Width = 145
      Height = 21
      MaxLength = 11
      TabOrder = 5
      Visible = False
      OnExit = edNumeroTeleviaExit
    end
    object CbSoporte: TCheckBox
      Left = 43
      Top = 86
      Width = 223
      Height = 17
      Caption = 'Agregar Concepto de Soporte de Telev'#237'a'
      TabOrder = 7
      OnClick = CbSoporteClick
    end
  end
  object btnSalir: TButton
    Left = 557
    Top = 467
    Width = 96
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 5
    OnClick = btnSalirClick
  end
  object nb_Botones: TNotebook
    Left = 434
    Top = 464
    Width = 118
    Height = 32
    Anchors = [akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    PageIndex = 1
    ParentFont = False
    TabOrder = 4
    object TPage
      Left = 0
      Top = 0
      Caption = 'PAGE_REGISTRAR'
      DesignSize = (
        118
        32)
      object btn_Continuar: TButton
        Left = 21
        Top = 3
        Width = 96
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Continuar'
        TabOrder = 0
        OnClick = btn_ContinuarClick
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'PAGE_CONTINUAR'
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        118
        32)
      object btnRegistrar: TButton
        Left = 21
        Top = 3
        Width = 96
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Registrar'
        Default = True
        Enabled = False
        TabOrder = 0
        OnClick = btnRegistrarClick
      end
    end
  end
  object spObtenerMovimientosAFacturar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMovimientosAFacturar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 79
    Top = 329
  end
  object dsObtenerPendienteFacturacion: TDataSource
    DataSet = cdsObtenerPendienteFacturacion
    Left = 46
    Top = 328
  end
  object cdsObtenerPendienteFacturacion: TClientDataSet
    Active = True
    Aggregates = <>
    AutoCalcFields = False
    FieldDefs = <
      item
        Name = 'Marcado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'NumeroMovimiento'
        DataType = ftInteger
      end
      item
        Name = 'FechaHora'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftWord
      end
      item
        Name = 'Concepto'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescImporte'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ImporteCtvs'
        DataType = ftLargeint
      end
      item
        Name = 'Nuevo'
        DataType = ftBoolean
      end
      item
        Name = 'Cantidad'
        DataType = ftFloat
      end
      item
        Name = 'PorcentajeIVA'
        DataType = ftFloat
      end
      item
        Name = 'EstaImpreso'
        DataType = ftBoolean
      end
      item
        Name = 'ImporteIVA'
        DataType = ftFloat
      end
      item
        Name = 'ValorNeto'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterScroll = cdsObtenerPendienteFacturacionAfterScroll
    OnCalcFields = cdsObtenerPendienteFacturacionCalcFields
    Left = 389
    Top = 314
    Data = {
      640100009619E0BD01000000180000000F0000000000030000006401074D6172
      6361646F02000300000000000E436F6469676F436F6E76656E696F0400010000
      000000104E756D65726F4D6F76696D69656E746F040001000000000009466563
      6861486F726108000800000000000E436F6469676F436F6E636570746F020002
      000000000008436F6E636570746F010049000000010005574944544802000200
      32000D4F62736572766163696F6E657301004900000001000557494454480200
      02003C000B44657363496D706F72746501004900000001000557494454480200
      020014000B496D706F727465437476730800010000000000054E7565766F0200
      0300000000000843616E746964616408000400000000000D506F7263656E7461
      6A6549564108000400000000000B45737461496D707265736F02000300000000
      000A496D706F72746549564108000400000000000956616C6F724E65746F0800
      0400000000000000}
  end
  object Imagenes: TImageList
    Left = 80
    Top = 372
    Bitmap = {
      494C010103000400080010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000878787FF8787
      87FF808080FF808080FF808080FF777777FF777777FF777777FF777777FF8080
      80FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFCD
      B2FFFFDEB2FFFFDEB2FFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7FFFFFFFFFF7777
      77FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFCD
      B2FFFFFFFFFFFFB2D8FFFFFFFFFFC7C7C7FF007F24FFC7C7C7FFFFFFFFFF7777
      77FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFCD
      B2FFFFFFFFFFFFDEB2FFFFDEB2FF007F24FF007F24FF007F24FFF7F7F7FFA7A7
      A7FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFCD
      B2FFFFB2D8FFFFFFFFFF007F24FF007F24FF00B233FF007F24FF007F24FFC7C7
      C7FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFDE
      B2FF878787FFC7C7C7FF00B233FF00B233FFC7C7C7FF00B233FF007F24FF007F
      24FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFCD
      B2FFFFFFFFFFFFCDB2FFFFCDB2FFFFCDB2FFFFFFFFFFFFFFFFFF00B233FF007F
      24FF007F24FF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFCD
      B2FF878787FF808080FF808080FF808080FF777777FF808080FFFFFFFFFF00B2
      33FF007F24FF007F24FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFCD
      B2FFFFB2D8FFFFFFFFFFFFDEB2FFFFFFFFFFFFB2D8FFFFFFFFFFF7F7F7FF7777
      77FF00B233FF007F24FF007F24FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFDE
      B2FF979797FF979797FF979797FF979797FF878787FF979797FFFFFFFFFF7777
      77FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000A7A7A7FFFFB2
      D8FFFFDEB2FFFFFFFFFFFFCDB2FFFFFFFFFFFFDEB2FFF7F7F7FFF7F7F7FF7777
      77FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000878787FFA7A7
      A7FF979797FF979797FF979797FF979797FF878787FF808080FF777777FF7777
      77FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF000080018001FFFF0000
      80018001C00F000080018001C00F000084018001C00F000080018001C00F0000
      80018001C00F000080018001C00F000080018001C007000080018001C0030000
      80018001C001000080018001C00F000080018001C00F000080018001C00F0000
      80018001FFFF0000FFFFFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 334
    Top = 379
  end
  object spAgregarMovimientoCuentaIVA: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarMovimientoCuentaIVA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteIVA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcentajeIVA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 146
    Top = 385
  end
  object spCrearLoteFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearLoteFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Total'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 240
    Top = 378
  end
  object spCrearComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@PeriodoInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PeriodoFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@TipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Calle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DetalleDomicilio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@TotalComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@SaldoAnterior'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAPagar'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@AjusteSencilloAnterior'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AjusteSencilloActual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCuenta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaVencimientoTarjeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoEmisorTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Inserto1'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Inserto2'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Inserto3'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Inserto4'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ImprDetallada'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@EnvioElectronico'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoImpresoraFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 271
    Top = 378
  end
  object spActualizarLoteFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarLoteFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioModificacion'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 20
        Size = -1
        Value = Null
      end>
    Left = 302
    Top = 379
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 382
    Top = 379
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 274
    Top = 329
  end
  object ImpresoraFiscal: TFiscalPrinter
    Active = False
    DLL = 'TMT88IIIFCH.dll'
    ID = 1
    PollingMode = pmSinglethreaded
    Left = 416
    Top = 464
  end
  object spObtenerDetalleConceptoMovimiento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleConceptoMovimiento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 382
    Top = 411
  end
  object spCrearComprobanteCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CrearComprobanteCuotas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadCuotasTotales'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 414
    Top = 411
  end
  object PopupGrilla: TPopupMenu
    Left = 528
    Top = 278
    object Marcarcomoimpreso1: TMenuItem
      Caption = 'Marcar como impreso'
      OnClick = Marcarcomoimpreso1Click
    end
  end
  object spReaplicarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ReaplicarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 496
    Top = 352
  end
end
