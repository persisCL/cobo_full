{********************************** Unit Header ********************************

    17-Abril-2009
    Author: mbecerra
                            Archivo renombrado a frmFacturacionManual




File Name : frm_FacturacionUnConvenio.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
	Author : ggomez
	Date : 22/03/2006
	Description : Coloqu� los CommandTimeout de los SP y TQuery a 300 segundos,
		ya que par aun convenio que tiene muchas cuentas daba timeout al
		facturarlo manualmente.

Revision 2:
	Author : jconcheyro
	Date : 21/04/2006
	Description : Se eliminaron las llamadas a EliminarLotesSinComprobantes
		ya que si estaba corriendo un proceso masivo y se lanzaba uno manual, arruinaba
		los comprobantes del masivo

Revision 3:
    Author : ggomez
    Date : 26/04/2006
    Description : Quit� el if que se hac�a en la impresi�n del comporobante. Lo
        hice ya que si el operador respond�a que NO quer�a imprimir, mostraba un
        mensaje de error que no deb�a mostrar. Esto lo hac�a ya que el m�todo
        ImprimirComprobante, retorna False si el operador responde que no quiere
        imprimir.

Revision 3:
	Author : jconcheyro
	Date : 30/11/2006
	Description : Se valida que la fecha de corte m�nima posible sea la de inicio de cobranza.

Revision 4:
    Author : FSandi
    Date : 12/03/2007
    Description : Se modific� el formulario para que los datos de la grilla se obtengan de un
                ClientDataSet. Ahora la pantalla permite marcar los conceptos que se desean
                Facturar.

Revision 5:
    Author : nefernandez
    Date : 16/03/2007
    Description : Se pasa el parametro Usuario al SP "GenerarMovimientosTransitos" -
                para auditoria. Tambien se reemplazo el uso de un TADOQuery por la
                llamada a un SP, para actualizar la FechaHora de impresion del
                Comprobante



    Revision : 7
        Author : vpaszkowicz
        Date : 08/07/2008
        Description : Quito la posibilidad de desmarcar conceptos relacionados
        a peajes.

    Revision : 8
        Author : rharris
        Date : 21/01/2009
        Description : (Ref.:SS 627)
                Se modifica el proceso de facturacion para que:
                    1.- utilice tablas de trabajo para el calculo de intereses
                    2.- calculo los consumos de los movimientos prefacturados (masivamente)


    Revision 9
    Author: mbecerra
    Date: 03-Marzo-2009
    Description:	(Ref Facturacion Electr�nica)
                    Una vez creada exitosamente la NK, se agrega lo siguiente:
                    	* Tipificaci�n y Numeraci�n
                        * Timbrado del documento
                        * Inserci�n en las bases de datos de DBNet
                        * Aplicaci�n del Comprobante y actualizaci�n del Saldo del Convenio
                        * Nueva Impresi�n, incluyendo el Timbre PDF417

                    * Se reordena el evento OnClick del bot�n btnFacturar,
                      y se simplifica el c�digo, quit�ndole los Try excesivos

                    * Se cambia el procedimiento LlenarDetalleConsumoComprobantes
                      transform�ndolo en una funci�n

        31-Marzo-2009
                	* La impresi�n de NK se hace con el nuevo formulario
                      frmReporteBoletaFacturaElectr�nica

                    * Antes de imprimir, hay que solicitar al usuario si desea boleta o factura


        24-Abril-2009
                	* Todos los conceptos aparecen inicialmente "sin seleccionar"

                    * Adem�s, cuando se selecciona o desmarca un concepto cuyo
                      campo "AfectaFechaCorte" es True, entonces se seleccionan
                      o desmarcan autom�ticamente el resto de los conceptos
                      que tengan el mismo campo en true.

                    * Se permite marcar y desmarcar cualquier item

        11-Mayo-2009
                	* Se modifica el stored ObtenerUltimosConsumos para que retorne
                      un campo que indique si el consumo es Prefacturado o no.
                      Si es Prefacturado, no se visualiza (ni mucho menos se factura)
                      en esta ventana.

                	* Se modifica para que al seleccionar el convenio, se
                      obtengan los movimientoscuentas asociados a intereses.

                      Lo anterior significa que al facturar, se debe invocar al stored
                      GenerarMovimientosTransitos con el parametro NOGenerarIntereses = 1

        13-Mayo-2009
                	* Se realizan las siguientes modificaciones al facturador manual,
                      en pro de facturar Infracciones:

                        1.-		Se agrega el par�metro "Tipo de documento" para
                        		referenciar el contexto de lo que se est� facturando:
                                NK o Infracciones

                        2.-		El documento interno a generar sigue siendo NK

                        3.-		Se cambia la funci�n ObtenerResumenAFacturar
                            	para invocar a dos stored diferentes:
                                    * ObtenerUltimosConsumos
                                    * ObtenerMovimientosInfractor

                        4.-		Se quita la llamada a la funci�n ValidarConsumos

                        5.-		Se quita la llamada a constantes que identifican
                            	los movimientosCuenta asociados a Infracciones,
                                en vez de eso se utilizar� la nueva columna
                                IncluirNotaInfractor

                        5.-		En virtud de estos cambios, se deshacen las
                            	versiones 1.4 a 1.10  (TeamCVS) manteniendo las
                                cambios de las versiones 1.7 y 1.9.

        19-Mayo-2009
                    * Se reemplaza el Timer por un bot�n Buscar, de modo de ser invocado
                      desde otro formulario y para darle al operador la tranquilidad
                      de ingresar el Rut Completo.

                    * Se agrega nueva variable FMostrarMensajeFinal de modo de
                      desplegar o no el mensaje "Desea facturar otro convenio?".


        28-Mayo-2009
                	* Se agrega que actualice el TotalAPagar, FechaEmision y
                      FechaVencimiento del Comprobante, una vez que finalice la
                      Tipificaci�n

                    * 01-Junio-2009: Facturaci�n manual no es necesario actualizar
                      el TotalAPagar, s�lo en la Tipificaci�n Masiva.

        01-Junio-2009
                	* Se modifica el uso del stored CargarConceptosAFacturarFacturadorManual
                      para que efectivamente cargue los conceptos a facturar, y no
                      los conceptos que el operador NO seleccion�.

                    * Por ende se modifica el stored GenerarMovimientosTransitos
                      para que utilice esta funcionalidad.

        03-Junio-2009
                	* Se corrige un IF que imprim�a el documento electr�nico
                      si no era NotaInfractor.
                      NOTA: el documento electronico siempre debe imprimirse

                    * Se cambia el stored ObtenerComprobanteFacturado por
                      ObtenerComprobantePreFacturado, pues el nombre conlleva a
                      confusi�n.

        17-Junio-2009
                	* Se llama a la funci�n function CargarDatosIntereses
                      antes de invocar al stored GenerarMovimientosInteres
                      al seleccionar un convenio.

                    * Como el stored GenerarMovimientosInteres hace un JOIN por
                      CodigoConvenio, no usa el par�metro NumeroProcesofacturacion
                      de la funci�n CargarDatosIntereses, se le pasar� un cero (0).

        29-Junio-2009
                	* Se modifica la llamada a la funci�n ImprimirDetalleInfraccionesAnuladas
                      para que al invocar a la funci�n Inicializar del formulario
                      frmRptDetInfraccionesAnuladas obtenga la descripci�n
                      del Error, si lo hay

                    * Si el documento de Contexto es una Nota Infractor, entonces
                      deben aparecer seleccionados todos los conceptos

                    * Si el documento de conexto es una Nota Infractor, entonces
                      no importa si tiene un concepto o m�s en la ventana,
                      debe seleccionarlos todos

        11-Agosto-2009
                	* Se agrega una validaci�n adicional para ver si hubo error
                      al insertar el NumeroProcesoFacturacion en la cola de env�o a DBNet

        02-Noviembre-2009
                	* Se agrega un ROLLBACK si hay un error inesperado en
                      la facturaci�n manual.

Revision 10
    Author: jjofre
    Date: 13-08-2010
    Description:	(Ref SS 690 Semaforos)
               -Se modificaron/a�adieron los siguientes Procedimientos/Funciones
                    MostrarMensajeTipoClienteSistema
                    btnBuscarClick
                    peRutCliente
                    peRUTClienteButtonClick

	Revision	: 10
	Author		: mbecerra
    Date		: 01-Junio-2010
    Description	:	(Ref Fase II)
                	Se quitan las variables y llamadas a las funciones
                    CONST_CONCEPTO PEAJES

    Revision    : 11
    Author      : pdominguez
    Date        : 02/06/2010
    Description : Infractores Fase 2
        - Se a�aden los siguientes objetos:
            spActualizarInfraccionesFacturadas: TADOStoredProc.

        - Se modificaron los siguientes procedimientos/funciones
            btnFacturarClick,
            Inicializar

    Firma: SS_964_PDO_20110531

    Descripci�n: SS 964 - Despliegue de Ventana de Facturaci�n
        - Se adaptan las funcionalidades, para que cuando el formulario sea llamado
        en el modo TC_FACTURACION_INMEDIATA, los conceptos de Facturaci�n Inmediata,
        salgan directamente seleccionados.

    Firma       :   SS_1045_CQU_20120604
    Description :   Si el m�todo de facturaci�n es por Rut (fFacturacionInfracciones)
                    no se deben desplegar los mensajes ya que ser� autom�tica.
                    Se agregan propiedades que permitan mantener
                    los valores elegidos por el usuario al momento de imprimir
                    Pedro agrega cambios para esta SS

    Firma       :   SS_1006_1015_CQU_20120903
    Description :   Se modifica la funci�n EsFechaHabil quitando la validaci�n en ella,
                    agregando la funci�n EsDiaHabil desde PeaProcs.pas para realizarla
                    ya que en �sta �ltima se valida que laboral y considera los feriados.

    Firma       : SS_1006_10015_NDR_20120928
    Description : Se usa la funcion QueryGetDateTimeValue de SysUtilsCN para llamar a dbo.ObtenerUltimaFechaEmisionNotasCobro()

    Firma       :   SS_660_CQU_20121010
    Description :   Se agrega el sp spLlenarDetalleConsumoComprobantesMorosos para llenar el detalle de consumo de las infracciones

    Firma       :   SS_660_CQU_20130604
    Description :   Se agrega llamada a TPanelMensajesForm para desplegar el estado del proceso cuando est� oculto (SS_1045)

    Author		: mvillarroel
    Date		: 07-Julio-2013
    Firma   : SS_660_MVI_20130711
    Description	:		(Ref: SS 660)
                    Se agrega validaci�n para indicar si un Convenio est� en Lista Amarilla

    Autor       :   CQuezadaI
    Fecha       :   23-07-2013
    Firma       :   SS_660_CQU_20130711
    Descripcion :   Se alinea el Label de Lista Amarilla
                    Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla

    Firma       : SS_660_MVI_20130729
    Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

    Firma       : SS_1120_MVI_20130809
    Descripcion : Se agrega texto "Convenio Sin Cuenta" en el combo que muestra los convenios.

    Firma       : SS_660_CQU_20130816
    Descripcion : Se modifica el m�todo peRUTClienteChange para que limpie el label que indica si est� en lista amarilla

    Firma       : SS_1120_MVI_20130820
    Descripcion : Se cambia la forma en la que se llena el combo cbConveniosCliente, para utilizar el procedimeinto generico CargarConveniosRUT.
                  Se modifica el procedimiento cbConveniosClienteDrawItem para que llame al procedimiento generico ItemComboBoxColor.
                  Se comenta el procedimiento ItemComboBoxColor, ya que con el cambio no se utilizara.
                  Se aprovecha de limpiar el label de lista amarilla una vez que se factura y se contin�a el proceso con otros convenios
                  y se valida que se consulte si est� en Lista Amarilla solo cuando existan convenios en el Combo cbConveniosCliente.

    Firma       :   SS_660_MVI_20130909
    Descripcion :   Se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                    Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.

    Firma       :   SS_1045_CQU_20131120
    Descripcion :   Finalizada la facturaci�n de infracciones se deben anular los tr�nsitos, dicho proceso se demora por lo cual
                    se agrega una evaluaci�n para determinar si agrandar el TimeOut del sp que realiza el proceso o no.
                    Adiconalmente se agregaron nuevos mensajes cuando se ejecuta el proceso antes mencionado.

    Firma       :   SS_660_CQU_20140108
    Descripcion :   Si el convenio est� en lista amarilla Y el formulario est� visible se deber� facturar
                    por el sp FacturacionMasivaConveniosMorosos, esto es para facturar las infracciones por morosidad (SS_660).
                    Si el formulario est� oculto se deber� seguir el proceso normal (SS_1045)

    Firma		:	SS_1200_MCA_20140707
    Descripcion	:	Error de desbordamiento operacional al tipificar y enumerar

    Author      :   Claudio Quezada Ib��ez
    Date        :   06-Agosto-2014
    Firma       :   SS_660E_CQU_20140806
    Descripcion :   Se sobreescribe la funci�n Inicializar agreg�ndole un par�metro que permita indicar si s�lo se facturar�n
                    infracciones de lista amarilla (Tipo 2).

    Firma       :   SS_1147_NDR_20141216
    Descripcion :   Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

    Firma       :   SS_1147_CQU_20150116
    Descripcion :   se reemplaza constante COD_CONCESIONARIA por Codigo Concesionaria Nativa

    Author      :   Claudio Quezada Ib��ez
    Fecha       :   06/10/2015
    Firma       :   SS_1246_CQU_20151006
    Descripcion :   Se pidi� quitar el cobro por reimpresi�n de comprobantes en TODAS partes.

    Firma       : SS_1408_MCA_20151027
    Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar
*******************************************************************************}
unit frmFacturacionManual;

interface

uses
	//ReporteFactura,    ya no se usa
  frmReporteBoletaFacturaElectronica, frmReporteFacturacionDetallada,
  frmRptDetInfraccionesAnuladas,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Dateedit, ExtCtrls, utilProc, DmiCtrls, util, DBCtrls,
  ImgList, ADODB, DMConnection, ComCtrls, BuscaClientes, Utildb, DB,
  DPSControls, VariantComboBox, constParametrosGenerales, Validate,
  peaTypes, PeaProcs, dateUtils, ListBoxEx, DBListEx, DBClient, Menus // ; // SS_1045_CQU_20120604
  ,PeaProcsCN, ImprimirWO,                                                 // SS_1045_CQU_20120604
  frmMuestraMensaje,                                                        // SS_660_CQU_20130604
  SysUtilsCN;                                                               //SS_1006_10015_NDR_20120928

type
  TFacturacionManualForm = class(TForm)
    ObtenerCLiente: TADOStoredProc;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    peRUTCliente: TPickEdit;
    GroupBox5: TGroupBox;
    lNombreCompleto: TLabel;
    lDomicilio: TLabel;
    lComuna: TLabel;
    lRegion: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label2: TLabel;
    deFechaCorte: TDateEdit;
    deFechaEmision: TDateEdit;
    Label4: TLabel;
    Label5: TLabel;
    deFechaVencimiento: TDateEdit;
    cbConveniosCliente: TVariantComboBox;
    lMedioPago: TLabel;
    ObtenerDatosMedioPagoAutomaticoConvenio: TADOStoredProc;
    Label6: TLabel;
    GroupBox1: TGroupBox;
    lDomicilioFacturacion: TLabel;
    lComunaFacturacion: TLabel;
    lRegionFacturacion: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    GenerarMovimientosTransitos: TADOStoredProc;
    Bevel1: TBevel;
    Label7: TLabel;
    DBListEx1: TDBListEx;
    ObtenerUltimosConsumos: TADOStoredProc;
    dsInformeFactura: TDataSource;
    spObtenerComprobantePreFacturado: TADOStoredProc;
    Label8: TLabel;
    NumeroConvenio: TEdit;
    spObtenerConveniosCliente: TADOStoredProc;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    spObtenerDatosFacturacionDetallada: TADOStoredProc;
    spCrearProcesoFacturacion: TADOStoredProc;
    chkFacturarCompleto: TCheckBox;
    chkFacturarSinIntereses: TCheckBox;
    ilImages: TImageList;
    PopFacturacionManual: TPopupMenu;
    MnuSeleccionarTodo: TMenuItem;
    MnuDeSeleccionarTodo: TMenuItem;
    SpCargarConceptosAFacturarFacturadorManual: TADOStoredProc;
    SPActualizarFechaHoraFinProcesoFacturacion: TADOStoredProc;
    spActualizarFechaImpresionComprobantes: TADOStoredProc;
    spGenerarTablaTrabajoInteresesPorFacturar: TADOStoredProc;
    spLlenarDetalleConsumoComprobantes: TADOStoredProc;
    Panel1: TPanel;
    btnFacturar: TButton;
    btnSalir: TButton;
    Label9: TLabel;
    vcbTipoDocumentoDeseado: TVariantComboBox;
    spTipificarYNumerarComprobante: TADOStoredProc;
    spActualizarSaldoConvenio: TADOStoredProc;
    spReaplicarConvenio: TADOStoredProc;
    spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet: TADOStoredProc;
    cdConceptos: TClientDataSet;
    spCobrar_o_DescontarReimpresion: TADOStoredProc;
    spGenerarMovimientosInteres: TADOStoredProc;
    btnBuscar: TButton;
    spActualizarInfraccionesFacturadas: TADOStoredProc;
    spLlenarDetalleConsumoComprobantesMorosos: TADOStoredProc;  // SS_660_CQU_20121010
    lblEnListaAmarilla: TLabel;									//SS_660_MVI_20130711
    spObtenerCantTransitosPorInfraccion: TADOStoredProc;        // SS_1045_CQU_20131120
    btnCalcular: TButton;                                       // TASK_137_MGO_20170213
    procedure cbConveniosClienteDrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure btnSalirClick(Sender: TObject);
    procedure btnFacturarClick(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure deFechaCorteExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure deFechaVencimientoExit(Sender: TObject);
    procedure deFechaEmisionChange(Sender: TObject);
    procedure deFechaEmisionExit(Sender: TObject);
    procedure deFechaCorteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NumeroConvenioChange(Sender: TObject);
    procedure DBListEx1DblClick(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure PopFacturacionManualPopup(Sender: TObject);
    procedure MnuSeleccionarTodoClick(Sender: TObject);
    procedure MnuDeSeleccionarTodoClick(Sender: TObject);
    procedure DBListEx1KeyPress(Sender: TObject; var Key: Char);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnCalcularClick(Sender: TObject);                                // TASK_137_MGO_20170213
  private
    { Private declarations }
    FTipoDocumentoContexto : string;
    FMostrarMensajeFinal : boolean;
	FUltimaBusqueda : TBusquedaCliente;
    FDiasVencimiento: integer;
	FFechaCorteMaximaPosible: TDateTime;
	{ Revision 9 : FCancelar: Boolean;}
    // Para mantener los convenios dados de baja, para luego saber cuales hay
    // que colorear en el combo de Convenios.
    FListaConveniosBaja: TStringList;
    FFechaCorteMininaPosible: TDateTime;
    // Inicio Bloque SS_1045_CQU_20120604
    FHecho, FOpcion1, FOpcion2 : Boolean;
    FRespuesta : TRespuesta;
    // Fin Bloque SS_1045_CQU_20120604
    FSoloListaAmarilla : Boolean;           // SS_660E_CQU_20140806
    FCat1, FCat2, FCat3, FCat4 : Integer;   // SS_660E_CQU_20140806
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;              //SS_1147_NDR_20141216
    FConcesionariaNativa: Integer;                                              // SS_1147_CQU_20150116
    function ValidarConsumos: Boolean;
    function EsFechaHabil(aFecha: TDateTime): Boolean;
    procedure ObtenerResumenAFacturar(Fecha: TDateTime);
    procedure ClearCaptions;
    procedure CargarDataset;
	procedure MostrarDomicilioFacturacion;
    Function ImprimirComprobante(TipoComprobante:string; NroComprobante: int64): Boolean;
    Function ImprimirDetalleInfraccionesAnuladas(TipoComprobante:string; NroComprobante: int64): Boolean;
    Function ImprimirFacturacionDetallada(TipoComprobante:string; NroComprobante: int64) : boolean;
//  procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor);                     //SS_1120_MVI_20130809
    procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor; Bold: Boolean);      //SS_1120_MVI_20130809
    function CargarDatosIntereses(CodigoConvenio: Integer; FechaEmision: TDatetime; NumeroProcesoFacturacion: Integer): Integer;
    //Revision 9 procedure LlenarDetalleConsumoComprobantes (NumeroProcesoFacturacion: Integer);
    function LlenarDetalleConsumoComprobantes (NumeroProcesoFacturacion: Integer) : boolean;
    procedure MostrarMensajeTipoClienteSistema(NumeroDocumento: String);
    procedure LiberarMemoria;       // SS_1017_PDO_20120514
  public
    { Public declarations }

    //REV.9
    function Inicializar(TipoDocumentoContexto : string; MostrarMensajeFinal : Boolean = True) : Boolean; overload;
    function Inicializar(MostrarMensajeFinal : Boolean; TipoDocumentoContexto : string;  SoloListaAmarilla : integer) : Boolean;  overload; // SS_660E_CQU_20140806
    function TipificarYNumerar(TipoComprobante, TipoComprobanteFiscalDeseado : string; NumeroComprobante, SaldoComprobante, CodigoConvenio : int64) : boolean;
    procedure MarcarTodos(Marcar : boolean);
    procedure GenerarMovimientosPorIntereses;
    // Inicio Bloque SS_1045_CQU_20120604
    property Hecho : Boolean read FHecho write FHecho;
    property Opcion1 : Boolean read FOpcion1 write FOpcion1;
    property Opcion2 : Boolean read FOpcion2 write FOpcion2;
    property Respuesta : TRespuesta read FRespuesta write FRespuesta;
    // Fin Bloque SS_1045_CQU_20120604
  end;


implementation

{$R *.dfm}

{REV.9
const
    CONCEPTO_PEAJES_PERIODO = 1;
    CONCEPTO_PEAJES_OTROS_PERIODOS = 2;}

resourcestring
    MSG_CLIENTE                         = 'Debe especificarse un cliente y el convenio a facturar.';
    MSG_COMVENIO                        = 'Debe especificarse un convenio a facturar.';
    MSG_FECHA_CORTE_NO_EXISTE           = 'Debe especificarse una fecha de corte.';
    MSG_FECHA_EMISION_NO_EXISTE         = 'Debe especificarse una fecha de emisi�n.';
    MSG_FECHA_VENCIMIENTO_NO_EXISTE     = 'Debe especificarse una fecha de vencimiento.';
    MSG_FECHA_CORTE_INCORRECTA          = 'La fecha de corte no puede ser mayor a la fecha actual.';
    MSG_FECHA_VENCIMIENTO_INCORRECTA    = 'La fecha de vencimiento no puede ser menor o igual a la fecha de emisi�n.';
    MSG_FECHA_EMISION_INCORRECTA        = 'La fecha de emisi�n no puede ser menor a la fecha de corte.';
    MSG_FECHA_EMISION_ANTERIOR_A_ULTIMA	= 'La fecha de emisi�n no puede ser anterior a la fecha de emisi�n '#10#13 +
    										'de la �ltima Nota de Cobro Emitida ';
    MSG_FECHA_VENCIMIENTO_NO_HABIL      = 'La fecha de vencimiento debe corresponder a un d�a laborable.';
    MSG_FECHA_EMISION_NO_HABIL          = 'La fecha de emisi�n debe corresponder a un d�a laborable.';

//Revision 4
var
    //REV.10 CodigoPeajes, CodigoPeajesAnteriores,
    CodigoEstadoFacturacionElectronica : Integer;
//Fin Revision 4


{******************************** Function Header ******************************
Function Name: EsFechaHabil
Author       : fmalisia
Date Created : 22-11-2004
Description  : Verifica si una fecha corresponde a un dia laborable
               (Distinto de Sabado y Domingo)
Parameters   : aFecha: TDateTime
Return Value : Boolean
*******************************************************************************}
function TFacturacionManualForm.EsFechaHabil(aFecha: TDateTime): Boolean;
begin
        //result := not ((DayOfTheWeek(aFecha) = daySaturday) or (DayOfTheWeek(aFecha) = daySunday));   // SS_1006_1015_CQU_20120903
        result := EsDiaHabil(DMConnections.BaseCAC, aFecha);                                            // SS_1006_1015_CQU_20120903
end;

{
    Function Name: Inicializar
    Parameters : TipoDocumentoContexto : string; MostrarMensajeFinal : Boolean = True
    Return Value: Boolean
    Author : fmalisia
    Date Created : 24-11-2004

    Description : Inicializaci�n de Este Formulario.

    Revision : 11
        Author: pdominguez
        Date: 05/06/2010
        Description: Infractores Fase 2
            - Se a�ade la visualizaci�n en el Caption 'Facturaci�n Infracciones' cuando
            se est�n facturando conceptos Infractores en el documento.
}
//function TFacturacionManualForm.Inicializar;                                                                                          // SS_660E_CQU_20140806
function TFacturacionManualForm.Inicializar (TipoDocumentoContexto : string; MostrarMensajeFinal : Boolean = True) : Boolean; // SS_660E_CQU_20140806
Resourcestring
    ERROR_CANNOT_INITIALIZE				= 'No se puede inicializar';
    ERROR_CANT_LOAD_INITIAL_DATE		= 'No puedo obtener el Parametro General: FECHA_INICIO_COBRANZA';
    ERROR_OBTENIENDO_CONSTANTES_PEAJE	= 'Ocurri� un error al obtener las constantes de peajes de periodos anteriores y peajes de periodo actual';
    MSG_SQL								= 'SELECT dbo.CONST_ESTADO_FACTURACION_ELECTRONICA_TERMINADA()';

    //Rev 9
    MSG_SQL_BO	= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA()';
    MSG_SQL_FC	= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA()';
    CAPTION_BOLETA	= 'Boleta';
    CAPTION_FACTURA = 'Factura';
    CAPTION_AUTOMAT	= '(Autom�tico)';

    MSG_CAPTION_INFRACCIONES = ' - Facturaci�n Infracciones';
var
    TipoBoleta, TipoFactura : string;
begin

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;
    FConcesionariaNativa := ObtenerCodigoConcesionariaNativa;                                               // SS_1147_CQU_20150116

    try
    	FTipoDocumentoContexto	:= TipoDocumentoContexto;			//REV.9
        FMostrarMensajeFinal	:= MostrarMensajeFinal;				//REV.9
        CenterForm(Self);
        //REV.9 Se cambia Visible por Enabled
        //chkFacturarSinIntereses.Visible := ExisteAcceso('GENERAR_INTERESES');
        chkFacturarSinIntereses.Enabled	:= ExisteAcceso('GENERAR_INTERESES');
        deFechaEmision.Enabled			:= ExisteAcceso('FECHA_EMISION_FACTURACION_MANUAL'); //Revision 1
        ClearCaptions;
		//FFechaCorteMaximaPosible := NowBase(DMConnections.BaseCAC);                                                //SS_1006_10015_NDR_20120928
        FFechaCorteMaximaPosible := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC); //QueryGetDateTimeValue(DMConnections.BaseCAC, 'SELECT GETDATE()');                 //SS_1006_10015_NDR_20120928
        Result := ObtenerParametroGeneral(DMCOnnections.BaseCAC, DIAS_VENCIMIENTO_COMPROBANTE, FDiasVencimiento);
        if result then begin
            //deFechaEmision.Date := NowBase(DMConnections.BaseCAC);                                                 //SS_1006_10015_NDR_20120928
            deFechaEmision.Date := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);//QueryGetDateTimeValue(DMConnections.BaseCAC, 'SELECT GETDATE()');                 //SS_1006_10015_NDR_20120928
            deFechaCorte.Date := FFechaCorteMaximaPosible;
            deFechaVencimiento.Date := deFechaEmision.Date + FDiasVencimiento;
        end;
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'FECHA_INICIO_COBRANZA', FFechaCorteMininaPosible) then begin
            MsgBoxErr(ERROR_CANNOT_INITIALIZE, ERROR_CANT_LOAD_INITIAL_DATE, Caption, MB_ICONSTOP);
            Result := False; // SS_1045_CQU_20120604
            Exit;
        end;


        //Rev 9
        //Cargar los tipos de documentos
        TipoBoleta				:= QueryGetValue(DMConnections.BaseCAC, MSG_SQL_BO);
        TipoFactura				:= QueryGetValue(DMConnections.BaseCAC, MSG_SQL_FC);
        vcbTipoDocumentoDeseado.Items.Clear;
        vcbTipoDocumentoDeseado.Items.Add(CAPTION_AUTOMAT, EmptyStr);
        vcbTipoDocumentoDeseado.Items.Add(CAPTION_BOLETA, TipoBoleta);
        vcbTipoDocumentoDeseado.Items.Add(CAPTION_FACTURA, TipoFactura);
        vcbTipoDocumentoDeseado.ItemIndex := 0;

        //Constante de fin de tipificaci�n
        CodigoEstadoFacturacionElectronica := QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL);

        if TipoDocumentoContexto = TC_NOTA_COBRO_INFRACCIONES then
            Caption := Caption + MSG_CAPTION_INFRACCIONES;

        // Inicio Bloque SS_1045_CQU_20120604
        FHecho := False;
        FOpcion1 := False;
        FOpcion2 := False;
        FillChar(FRespuesta, SizeOf(FRespuesta), 0);
        // Fin Bloque SS_1045_CQU_20120604

    except
        on e : Exception do begin
            if not Visible then TPanelMensajesForm.OcultaPanel;                                            // SS_660_CQU_20130604
            MsgBoxErr('No se pudo cargar la Facturaci�n Manual', e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;

 lblEnListaAmarilla.Visible:=False;                                                    //SS_660_MVI_20130711

//Comentado en REV.10
//Revision 4
//    try
//      CodigoPeajes := QueryGetValueInt(DMConnections.BaseCAC, 'Select Dbo.CONST_CONCEPTO_PEAJE_PERIODO ()');
//      CodigoPeajesAnteriores := QueryGetValueInt(DMConnections.BaseCAC, 'Select Dbo.CONST_CONCEPTO_PEAJE_PERIODOS_ANTERIORES ()');
//    except
//        on e: Exception do begin
//            msgBoxErr(ERROR_OBTENIENDO_CONSTANTES_PEAJE, e.Message, caption, MB_ICONSTOP);
//        end;
//    end;
//Fin Revision 4
end;

function TFacturacionManualForm.Inicializar(MostrarMensajeFinal : Boolean;TipoDocumentoContexto : string;  SoloListaAmarilla : integer) : Boolean;      // SS_660E_CQU_20140806
var                                                                                                                                                     // SS_660E_CQU_20140806
    ConceptosInfraLA : TADOStoredProc;                                                                                                                  // SS_660E_CQU_20140806
    Resultado : Boolean;                                                                                                                                // SS_660E_CQU_20140806
begin                                                                                                                                                   // SS_660E_CQU_20140806
    Resultado := False;                                                                                                                                 // SS_660E_CQU_20140806
    try                                                                                                                                                 // SS_660E_CQU_20140806
        FSoloListaAmarilla := (SoloListaAmarilla = 1);                                                                                                  // SS_660E_CQU_20140806
        try                                                                                                                                             // SS_660E_CQU_20140806
            FConcesionariaNativa := ObtenerCodigoConcesionariaNativa;                                                                                   // SS_1147_CQU_20150116
            ConceptosInfraLA := TADOStoredProc.Create(nil);                                                                                             // SS_660E_CQU_20140806
            ConceptosInfraLA.Connection := DMConnections.BaseCAC;                                                                                       // SS_660E_CQU_20140806
            ConceptosInfraLA.ProcedureName:= 'ObtenerConceptosInfractoresConcesionaria';                                                                // SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.Refresh;                                                                                                        // SS_660E_CQU_20140806
            //ConceptosInfraLA.Parameters.ParamByName('@CodigoConcesionaria').Value := COD_CONCESIONARIA;                                               // SS_1147_CQU_20150116// SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConcesionaria').Value := FConcesionariaNativa;                                              // SS_1147_CQU_20150116
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoPrejudicial').Value := Null;                                             // SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraGastosCobranza').Value := Null;                                                // SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraIntereses').Value := Null;                                                     // SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat1').Value := Null;                                           // SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat2').Value := Null;                                           // SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat3').Value := Null;                                           // SS_660E_CQU_20140806
            ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat4').Value := Null;                                           // SS_660E_CQU_20140806
            ConceptosInfraLA.ExecProc;                                                                                                                  // SS_660E_CQU_20140806
            FCat1 := ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat1').Value;                                          // SS_660E_CQU_20140806
            FCat2 := ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat2').Value;                                          // SS_660E_CQU_20140806
            FCat3 := ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat3').Value;                                          // SS_660E_CQU_20140806
            FCat4 := ConceptosInfraLA.Parameters.ParamByName('@CodigoConceptoInfraBoletoMorosidadCat4').Value;                                          // SS_660E_CQU_20140806
            ConceptosInfraLA.Close;                                                                                                                     // SS_660E_CQU_20140806
            ConceptosInfraLA := nil;                                                                                                                    // SS_660E_CQU_20140806
            Resultado := Inicializar(TipoDocumentoContexto, MostrarMensajeFinal);                                                                       // SS_660E_CQU_20140806
        except                                                                                                                                          // SS_660E_CQU_20140806
            on E: Exception do begin                                                                                                                    // SS_660E_CQU_20140806
                if not Visible then TPanelMensajesForm.OcultaPanel;
                MsgBoxErr('No se pudo cargar la Facturaci�n Manual', e.Message, Caption, MB_ICONSTOP);
                Result := False;                                                                                                                                                        // SS_660E_CQU_20140806
            end;                                                                                                                                        // SS_660E_CQU_20140806
        end;                                                                                                                                            // SS_660E_CQU_20140806
    finally                                                                                                                                             // SS_660E_CQU_20140806
        Result := Resultado;                                                                                                                            // SS_660E_CQU_20140806
    end;                                                                                                                                                // SS_660E_CQU_20140806
end;                                                                                                                                                    // SS_660E_CQU_20140806

{---------------------------------------------------------------------
            	GenerarMovimientosPorIntereses

 Author: mbecerra
 Date: 11-Mayo-2009
 Description:	(Ref. Facturaci�n Electr�nica)
                Esta funci�n invoca al stored GenerarMovimientosInteres
                para el convenio seleccionado

        17-Junio-2009:
            	Previamente debe llamar al stored: CargarConceptosAFacturarFacturadorManual
                ya que GenerarMovimientosInteres usa la tabla
                tmp_ConveniosConInteresesPorFacturar
---------------------------------------------------------------------------}
procedure TFacturacionManualForm.GenerarMovimientosPorIntereses;
resourcestring
    MSG_ERROR	= 'Error al generar intereses para el convenio %s';
var
	FechaHoy : TDateTime;
    CodigoConvenio : integer;
begin
	if cbConveniosCliente.ItemIndex >= 0 then begin
    	FechaHoy := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);				// SS_1045_CQU_20120604
        CodigoConvenio := cbConveniosCliente.Value;
        if CargarDatosIntereses(CodigoConvenio, FechaHoy, 0) > 0 then begin       // 17-Junio-2009
	    	spGenerarMovimientosInteres.Close;
	    	with spGenerarMovimientosInteres do begin
	        	Parameters.ParamByName('@FechaEmision').Value	:= FechaHoy;
	        	Parameters.ParamByName('@Usuario').Value		:= UsuarioSistema;
	        	Parameters.ParamByName('@CodigoConvenio').Value	:= CodigoConvenio;
                ExecProc;
	        	if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then  begin
	          		MsgBox(MSG_ERROR, Caption, MB_ICONEXCLAMATION);
	        	end
	        	else begin
	        		chkFacturarSinIntereses.Checked := True;
	            	chkFacturarSinIntereses.Enabled := False;
	        	end;
            end;
    	end;
    end;
end;

{---------------------------------------------------------------------
            	TipificarYNumerar

 Author: mbecerra
 Date: 04-Marzo-2009
 Description:	(Ref. Facturaci�n Electr�nica)
                Esta funci�n retorna verdadero si logra Tipificar, Numerar,
                Actualizar saldo del Convenio y Aplicar el comprobante

                Retorna Falso en caso contrario
---------------------------------------------------------------------------}
function TFacturacionManualForm.TipificarYNumerar;
resourcestring
	MSG_ERROR		= 'Ocurri� un error al ';
    MSG_ERROR_2		= 'Ocurri� el siguiente error al %s: %s';
    MSG_TIPIFICAR	= 'Tipificar y Numerar Comprobante';
    MSG_SALDO		= 'Actualizar Saldo del Convenio';
    MSG_ESTADO		= 'Actualizar Estado Electr�nico del comprobante';
    MSG_FINALIZAR	= 'Finalizar electr�nicamente la generaci�n del comprobante';
    SQL_UPDATE_TIPI	= 'UPDATE Comprobantes SET CodigoEstadoFacturacionElectronica = %d ' +
    				  'WHERE TipoComprobante = ''%s'' AND NumeroComprobante = %d';
var
    DescError, FaseProceso : string;
begin
	Result := True;
    try
    	spTipificarYNumerarComprobante.Close;
        spActualizarSaldoConvenio.Close;
        spReaplicarConvenio.Close;
    	{Tipificar y Numerar}
        FaseProceso := MSG_TIPIFICAR;
    	with spTipificarYNumerarComprobante do begin
    		Parameters.ParamByName('@TipoComprobante').Value			:= TipoComprobante;
        	Parameters.ParamByName('@NumeroComprobante').Value			:= NumeroComprobante;
        	Parameters.ParamByName('@DescripcionError').Value			:= '';
            if TipoComprobanteFiscalDeseado = EmptyStr then Parameters.ParamByName('@TipoComprobanteFiscal').Value := NULL
            else Parameters.ParamByName('@TipoComprobanteFiscal').Value := TipoComprobanteFiscalDeseado;

        	Parameters.ParamByName('@NumeroComprobanteFiscal').Value	:= NULL;
            ExecProc;
            DescError := Parameters.ParamByName('@DescripcionError').Value;
            if DescError <> '' then begin
                MsgBox(Format(MSG_ERROR_2, [MSG_TIPIFICAR, DescError]), Caption, MB_ICONERROR );
                Result := False;
            end
    	end;

        //Actualizar Saldo del Convenio
        if Result then begin
            FaseProceso := MSG_SALDO;
            with spActualizarSaldoConvenio do begin
                Parameters.ParamByName('@CodigoConvenio').Value		:= CodigoConvenio;
                Parameters.ParamByName('@TipoComprobante').Value	:= TipoComprobante;
                Parameters.ParamByName('@SaldoComprobante').Value	:= SaldoComprobante;
                ExecProc;
            end;
        end;


         //actualizar el campo estado del comprobante
        if Result then begin
        	QueryExecute(DMConnections.BaseCAC, Format(SQL_UPDATE_TIPI, [	CodigoEstadoFacturacionElectronica,
        																	TipoComprobante,
                	                                                        NumeroComprobante]));
        end;

        //Reaplicar el Convenio
        if Result then begin
            FaseProceso := MSG_FINALIZAR;
            with spReaplicarConvenio do begin
                Parameters.ParamByName('@CodigoConvenio').Value	:= CodigoConvenio;
                Parameters.ParamByName('@CodigoUsuario').Value	:= UsuarioSistema;
                ExecProc;
            end;
        end;

    	//Actualizar TotalAPagar, FechaEmision y FechaVencimiento del Comprobante

    except on e:exception do begin
            MsgBoxErr(MSG_ERROR + FaseProceso, e.Message, Caption, MB_ICONERROR);
            Result := False;
    	end;
    end;

end;

{-----------------------------------------------------------------------
            	MarcarTodos

Author: mbecerra
Date: 24-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Marca o Desmarca todos los conceptos cuyo campo
                AfectaFechaCorte est� encendido
-----------------------------------------------------------------------}
procedure TFacturacionManualForm.MarcarTodos;
var
  book : TBookMark;
begin
    with cdConceptos do begin
        book := GetBookmark;
        DisableControls;
        First;
        while not EOF do begin
        	if FieldByName('AfectaFechaCorte').AsBoolean then begin
            	Edit;
            	FieldByName('Seleccionado').AsBoolean := Marcar;
            	Post;
        	end;

        	Next;
        end;
        GotoBookmark(book);
        EnableControls;
    end;

end;

procedure TFacturacionManualForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
                    	btnBuscarClick
Author       : mbecerra
Date Created : 19-Mayo-2009
Description  : Busca los datos para el Rut Indicado

*******************************************************************************}
procedure TFacturacionManualForm.btnBuscarClick(Sender: TObject);
var
    DescripcionCalle: AnsiString;
    sNumeroConvenio: string;
    nIndex : integer;
    CodigoCliente: Integer;
    EsConvenioSinCuenta: Boolean;                                                   //SS_1120_MVI_20130809
begin
    //lista para guardar los valores (�ndices) de los convenios dados de baja
    if FListaConveniosBaja <> Nil then begin
        FreeAndNil(FListaConveniosBaja);
    end;
    FListaConveniosBaja := TStringList.Create;

    try
		Screen.Cursor := crHourglass;
    	sNumeroConvenio := trim(NumeroConvenio.Text);
		ClearCaptions;
		cbConveniosCliente.Items.clear;

		NumeroConvenio.Text := '';

		ObtenerUltimosConsumos.Close;
        if (length(peRUTCliente.Text) < 7) then begin
            Exit;
        end;

        // Obtenemos el cliente seleccionado
        with ObtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value := NULL;
            Parameters.ParamByName('@CodigoDocumento').value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').value := trim(PadL(peRUTCLiente.Text, 9, '0'));
            open;
        end;
        CodigoCliente:= obtenerCliente.fieldbyname('CodigoCliente').AsInteger;

        if CodigoCliente <> 0 then                                              //SS_1408_MCA_20151027
                EstaClienteConMensajeEspecial(DMCOnnections.BaseCAC, trim(PadL(peRUTCLiente.Text, 9, '0')));  //SS_1408_MCA_20151027
        // Obtenemos la lista de convenios para el cliente seleccionado

        if sNumeroConvenio = '' then sNumeroConvenio := '0';

        CargarConveniosRUT(DMCOnnections.BaseCAC,cbConveniosCliente, 'RUT',
        PadL(peRUTCLiente.Text, 9, '0' ), False, StrToInt(sNumeroConvenio) );
//END:SS_1120_MVI_20130820 -----------------------------------------------------
        cbConveniosClienteChange(cbConveniosCliente);

        with ObtenerCliente do begin
            // Mostramos el nombre completo del cliente
            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString), Trim(FieldByName('Nombre').asString))
            else
                lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString));

            // Mostramos el domicilio Personal
            DescripcionCalle := ObtenerDescripCalle(DMConnections.BaseCAC,
              PAIS_CHILE,
                  trim(ObtenerCliente.FieldByName('CodigoRegion').asString),
                  trim(ObtenerCliente.FieldByName('CodigoComuna').asString),
                  ObtenerCliente.FieldByName('CodigoCalle').asInteger,
                  trim(ObtenerCliente.FieldByName('CalleDesnormalizada').asString));
            lDomicilio.Caption := ArmarDomicilioSimple(DMConnections.BaseCAC, DescripcionCalle,
              Trim(ObtenerCliente.FieldByName('Numero').asString), 0, '',
              Trim(ObtenerCliente.FieldByName('detalle').asString));
            lComuna.Caption := BuscarDescripcionComuna(DMConnections.BaseCAC,
              PAIS_CHILE,
              trim(ObtenerCliente.FieldByName('Codigoregion').asString),
              trim(ObtenerCliente.FieldByName('CodigoComuna').asString));
            lRegion.Caption := BuscarDescripcionRegion(DMConnections.BaseCAC,
              PAIS_CHILE,
              trim(ObtenerCliente.FieldByName('CodigoRegion').asString));
        end;

        if CodigoCliente <> 0 then begin
            MostrarMensajeTipoClienteSistema(Trim(peRUTCliente.Text));
        end;


    finally
		Screen.Cursor := crDefault;
    end;

end;

// INICIO : TASK_137_MGO_20170213
procedure TFacturacionManualForm.btnCalcularClick(Sender: TObject);
begin
    ObtenerResumenAFacturar(deFechaCorte.Date);
end;
// FIN : TASK_137_MGO_20170213

{
    Procedure Name: btnFacturarClick
    Parameters : Sender: TObject
    Author : fmalisia
    Date Created : 24-11-2004

    Description : Ejecuta el proceso de facturacion para un convenio.

    Revision : 11
        Author: pdominguez
        Date: 01/06/2010
        Description: Infractores Fase 2
            - Se a�ade la actualizaci�n de las infracciones incluidas en el comprobante,
            en el caso de ser una Nota Infractor
}
procedure TFacturacionManualForm.btnFacturarClick(Sender: TObject);
resourcestring
	MSG_FALTA_TIPO_DOC					= 'No ha indicado el tipo de documento Fiscal deseado';
    ERROR_INVOICING     				= 'Error ejecutando el proceso de facturaci�n.';
    ERROR_INVOICE_NOT_GENERATED 		= 'El comprobante no ha sido generado';
    ERROR_NOITEM_BILLING        		= 'Proceso de Facturaci�n no realizado.';
    ERROR_NOITEM_BILLING_MSG    		= 'No existen items ni ajuste para facturar, o el importe de la deuda del cliente supera el m�ximo permitido para facturaci�n';
    ERROR_PRINTING      				= 'Error al intentar imprimir el comprobante';
    MSG_NO_INVOICE      				= 'El comprobante no ha sido emitido.';
  	MSG_INVOICING_OK    				= 'Proceso de facturaci�n finalizado con �xito.';
  	MSG_INVOICE_AGAIN   				= 'Desea realizar la facturaci�n para otro convenio? ';
  	MSG_ASK_GENERATE    				= 'Desea generar el comprobante?' + CRLF + CRLF +
    										'N�mero de Convenio: %s' + CRLF +
                 							'Fecha de Corte: %s' + CRLF +
                 							'Fecha de Vencimiento: %s';
  	MSG_ASK_PRINT       				= 'Desea imprimir el comprobante?';
    MSG_ASK_CREATE_INVOICE				= 'Si emite una Nota de Cobro con fecha futura, luego no '#10#13 +
    										'podr� emitir Notas de Cobro con fechas anteriores a �sta.'#10#13 +
                    						'Desea emitirla de todos modos ?';
	MSG_CANCEL_INVOICING				= 'La emisi�n del comprobante ha sido cancelada por el usuario.';
    MSG_ASK_USE_BACKGROUND_IMAGE		= 'Incluir Imagen de Fondo?';
    MSG_ERROR_GET_LAST_BILL_DATE		= 'Ha ocurrido un error al obtener la �ltima Fecha de Emisi�n de Nota de Cobro.';
    MSG_ERROR_CREATE_BILLING_PROCESS	= 'Ha ocurrido un error al crear el Proceso de Facturaci�n.';
    MSG_ERROR_CREATE_INVOICE			= 'Ha ocurrido un error al crear el Comprobante.';
    MSG_ERROR_UPDATE_BILLING_PROCESS	= 'Ha ocurrido un error al actualizar la Fecha de Finalizaci�n del Proceso de Facturaci�n.';
    MSG_ERROR_GET_INVOICE_NUMBER		= 'Ha ocurrido un error al obtener el N� del Comprobante generado. No se puede imprimir el Comprobante.';
    MSG_ERROR_FECHA_CORTE				= 'La fecha de corte m�nima debe ser %s%';
    MSG_ERROR_PARCIAL_PROCESS			= 'Ha ocurrido un error al realizar el proceso de Facturaci�n Parcial';
    MSG_ERROR_DETALLE_COMPROBANTES		= 'No se pudo generar el DetalleComprobante';
    MSG_ERROR_COLA_DBNET				= 'No se pudo insertar el registro en la cola de env�o a DBNet';
    MSG_ERROR_ACTUALIZANDO_INFRACCIONES = 'Se produjo un error al actualizar las Infracciones incluidas en el Comprobante'; // Rev. 11 (Infractores Fase 2)

    //Comprobantes Obligatorios Nota Infractor
    MSG_FALTA_ITEM_INFR					= 	'Debe seleccionar todos los items de infracci�n !';
    // Inicio Bloque SS_1045_CQU_20120604 (PDO)
    MSG_ERROR_BEGIN_TRANS               = 'NO se pudo iniciar la Transacci�n para generar el Comprobante.';
    MSG_ERROR_ROLLBACK_TRANS            = 'Se produjo un Error en el proceso. Se ejecut� ROLLBACK. ' + CRLF + CRLF + 'Error: %s.' + CRLF + 'P�ngase en contacto con Sistemas e informe del Error en Pantalla.';
    MSG_ERROR_COMPROBANTE_PREFACTURADO  = 'No se pudo obtener los datos del Comprobante Pre-Facturado.';
    MSG_ERROR_CALCULO_INTERESES         = 'Error al llenar tabla tmp_ConveniosConInteresesPorFacturar.';
    MSG_ERROR_TIPIFICAR_Y_NUMERAR       = 'No se pudo Tipificar y Numerar el Comprobante.';
    MSG_CAPTION_ERROR_INESPERADO        = 'Error Inesperado.';
    MSG_ERROR_INESPERADO                = 'Se produjo un Error inesperado.' + CRLF + CRLF + 'Error: %s' + CRLF + 'Error Message: %s' + CRLF + 'P�ngase en contacto con Sistemas e informe del Error en Pantalla.';
    MSG_ERROR_INESPERADO_ROLLBACK       = 'Se produjo un Error inesperado. Se Ejecut� Rollback.' + CRLF + CRLF + 'Error: %s' + CRLF + 'Error Message: %s' + CRLF + 'P�ngase en contacto con Sistemas e informe del Error en Pantalla.';
    MSG_ERROR_IPRIMIR_INFRACCIONES      = 'No se pudieron imprimir las Infracciones Facturadas en el Comprobante.';
    // Fin Bloque SS_1045_CQU_20120604 (PDO)
    MSG_ERROR_DATOS_INFRACCION_FACTURADA= 'Se produjo un error al calcular la cantidad de datos de infracciones facturadas';                                    // SS_1045_CQU_20131120
    MSG_PANEL_INFRACCIONES_DEMORA       = 'Anulando Infracciones Facturadas, ser�n anuladas %d infracciones con %d tr�nsitos asociados, espere por favor...';   // SS_1045_CQU_20131120
    MSG_PANEL_INFRACCIONES              = 'Anulando Infracciones Facturadas, espere por favor...';                                                              // SS_1045_CQU_20131120
var
	NumeroProcesoFacturacion : integer;
    DescripcionError : string;
    dUltimaFechaEmision : TDateTime;
    i, CantidadMovimientos, CodigoConcepto : Integer; //Revision 3
    CargaInteresesOK    : Integer;
    HuboError : boolean;
    //REV.9
    CantidadSeleccionados : integer;
    MensajeError : string;
    EnTransaccion : boolean;

    ADOConnectionWrite,                 // SS_1045_CQU_20120604 (PDO)
    ADOConnectionRead: TADOConnection;  // SS_1045_CQU_20120604 (PDO)
    TotalInfracciones,                      // SS_1045_CQU_20131120
    TotalTransitosInfracciones : Integer;   // SS_1045_CQU_20131120
    MensajePanel : string;                  // SS_1045_CQU_20131120
begin
	// Obtiene la �ltima Fecha de Emisi�n
	try
        dUltimaFechaEmision := QueryGetDateTimeValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerUltimaFechaEmisionNotasCobro()');
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_GET_LAST_BILL_DATE, E.Message, Caption, MB_ICONSTOP);
            Exit;
        end;
    end; // except

    // Validamos que esten los datos completos y sean v�lidos.
    if ((FTipoDocumentoContexto = TC_NOTA_COBRO) or (FTipoDocumentoContexto = TC_FACTURACION_INMEDIATA) or (FTipoDocumentoContexto = TC_FACTURACION_POR_BAJA)) and ( not ValidateCOntrols(        // SS_964_PDO_20110531
              [peRUTCliente,
              cbConveniosCliente,
              deFechaCorte,
              deFechaEmision,
              deFechaVencimiento,
              deFechaCorte,
              deFechaEmision,
              deFechaEmision,
              deFechaVencimiento,
              deFechaVencimiento,
              deFechaCorte,
              vcbTipoDocumentoDeseado],
              [(trim(peRUTCliente.text) <> '') and (ObtenerCliente.recordCount > 0),
              (cbConveniosCliente.Items.count > 0),
              deFechaCorte.Date <> nulldate,
              deFechaEmision.date <> nulldate,
              deFechaVencimiento.date <> nulldate,
              deFechaCorte.Date <= FFechaCorteMaximaPosible,
              deFechaEmision.date >= deFechaCorte.Date,
              deFechaEmision.date >= dUltimaFechaEmision,
              deFechaVencimiento.Date > deFechaEmision.Date,
              EsFechaHabil(deFechaVencimiento.date),
              deFechaCorte.Date >= FFechaCorteMininaPosible,
              vcbTipoDocumentoDeseado.ItemIndex >= 0],
              caption,
              [MSG_CLIENTE,
              MSG_COMVENIO,
              MSG_FECHA_CORTE_NO_EXISTE,
              MSG_FECHA_EMISION_NO_EXISTE,
              MSG_FECHA_VENCIMIENTO_NO_EXISTE,
              MSG_FECHA_CORTE_INCORRECTA,
              MSG_FECHA_EMISION_INCORRECTA,
              MSG_FECHA_EMISION_ANTERIOR_A_ULTIMA + FormatDateTime( '(dd/mm/yyyy)', dUltimaFechaEmision),
              MSG_FECHA_VENCIMIENTO_INCORRECTA,
              MSG_FECHA_VENCIMIENTO_NO_HABIL,
              format(MSG_ERROR_FECHA_CORTE , [DateTimeToStr(FFechaCorteMininaPosible)]),
              MSG_FALTA_TIPO_DOC
              ]) ) then begin

          Exit;
    end;

	if ((FTipoDocumentoContexto = TC_NOTA_COBRO) or (FTipoDocumentoContexto = TC_FACTURACION_INMEDIATA) or (FTipoDocumentoContexto = TC_FACTURACION_POR_BAJA)) and (deFechaEmision.Date > Date) then begin     // SS_964_PDO_20110531
    	if (MsgBox(MSG_ASK_CREATE_INVOICE, caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES) then begin
        	deFechaEmision.SetFocus;
			Exit;
        end;
    end;

    if FTipoDocumentoContexto = TC_NOTA_COBRO_INFRACCIONES then begin
        if not ValidateControls([peRUTCliente, cbConveniosCliente, deFechaEmision, deFechaVencimiento],
        						[peRUTCliente.Text <> '', cbConveniosCliente.ItemIndex >= 0,
                                deFechaEmision.Date > 0, deFechaVencimiento.Date > 0],
                                Caption,
                                [ MSG_CLIENTE, MSG_COMVENIO, MSG_FECHA_EMISION_NO_EXISTE,
                                MSG_FECHA_VENCIMIENTO_NO_EXISTE]) then Exit;

        //Validar que tenga los items obligatorios
        {--------> Cambio del 29-Junio-2009:
            Si el documento de contexto es Infracci�n, debe facturar todos los
            conceptos que tenga desplegados en la ventana
        }
        CantidadSeleccionados := 0;
        cdConceptos.DisableControls;
        cdConceptos.First;
        while not cdConceptos.Eof  do begin
        	if	cdConceptos.FieldByName('Seleccionado').AsBoolean then begin
                Inc(CantidadSeleccionados);
            end;
            cdConceptos.Next;
        end;

        cdConceptos.EnableControls;
        if CantidadSeleccionados <> cdConceptos.RecordCount then begin
        	MsgBox(MSG_FALTA_ITEM_INFR, Caption, MB_ICONEXCLAMATION);
            Exit;
        end;


    end;

    // Generamos el comprobante
    // Inicio Bloque SS_1045_CQU_20120604
    //if MsgBox(Format(MSG_ASK_GENERATE, [cbConveniosCliente.Items[cbConveniosCliente.ItemIndex].Caption,
    //                DateToStr(deFechaCorte.date), DateToStr(deFechaVencimiento.date)]),
    //                Caption, MB_YESNO + MB_ICONQUESTION) = IDNO then Exit;
    if Visible then begin
        if MsgBox(Format(MSG_ASK_GENERATE, [cbConveniosCliente.Items[cbConveniosCliente.ItemIndex].Caption,
                            DateToStr(deFechaCorte.date), DateToStr(deFechaVencimiento.date)]),
                            Caption, MB_YESNO + MB_ICONQUESTION) = IDNO then Exit;
    end;
    // Fin Bloque SS_1045_CQU_20120604
    try
        // M�todo agregado por PDO SS_1045_CQU_20120604
        //DMConnections.BaseCAC.Execute('BEGIN TRAN spFacturacionManual');                  // SS_1045_CQU_20120604 (PDO)
    	//EnTransaccion := True;                                                            // SS_1045_CQU_20120604 (PDO)
        if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;   // SS_1045_CQU_20120604 (PDO)

        if not DMConnections.BaseCAC.InTransaction then begin                               // SS_1045_CQU_20120604 (PDO)
            raise Exception.Create(MSG_ERROR_BEGIN_TRANS);                                  // SS_1045_CQU_20120604 (PDO)
        end;                                                                                // SS_1045_CQU_20120604 (PDO)

        {obtener correlativo proceso de facturaci�n}
        MensajeError := MSG_ERROR_CREATE_BILLING_PROCESS;
        with spCrearProcesoFacturacion do begin
        	Parameters.ParamByName('@Operador').Value := UsuarioSistema;
            Parameters.ParamByName('@FacturacionManual').Value := True;
            ExecProc;
        	NumeroProcesoFacturacion := Parameters.ParamByName('@NumeroProcesoFacturacion').Value;
        end;

        //Agregar los conceptos que se escogieron para facturar
        //dentro de la tabla ConceptosAFacturarFacturadorManual

        //Revision 4
        MensajeError := MSG_ERROR_PARCIAL_PROCESS;
        CdConceptos.Edit;
        CantidadMovimientos := CdConceptos.RecordCount;
        CdConceptos.First;
        for i := 0 to CantidadMovimientos -1 do begin
        	if {REV.9 not} cdConceptos.FieldByName('Seleccionado').AsBoolean then begin
            	CodigoConcepto := cdConceptos.FieldByName('CodigoConcepto').AsInteger;
                SPCargarConceptosAFacturarFacturadorManual.Parameters.ParamByName('@NumeroProcesoFacturacion').Value:=NumeroProcesoFacturacion;
                SPCargarConceptosAFacturarFacturadorManual.Parameters.ParamByName('@CodigoConcepto').Value:=CodigoConcepto;
                SPCargarConceptosAFacturarFacturadorManual.ExecProc;
            end;
            CdConceptos.Next;
        end;
        //FIN Revision 4

        if not Visible then TPanelMensajesForm.MuestraMensaje('Cargando intereses...'); // SS_660_CQU_20130604
        //Cargar tabla de trabajo para calcular intereses
        CargaInteresesOK := CargarDatosIntereses(	StrToInt(cbConveniosCliente.Value),
        											deFechaEmision.date, NumeroProcesoFacturacion);
        HuboError := (CargaInteresesOK < 0);

        if not HuboError then begin
        	MensajeError := MSG_ERROR_CREATE_INVOICE;
        	DescripcionError := EmptyStr;
            //if lblEnListaAmarilla.Visible and Visible then                                        // SS_660_CQU_20140108
            if FSoloListaAmarilla and Visible then                                                  // SS_660E_CQU_20140806 // SS_660_CQU_20140108
                GenerarMovimientosTransitos.ProcedureName := 'FacturacionMasivaConveniosMorosos'    // SS_660_CQU_20140108
            else                                                                                    // SS_660_CQU_20140108
                GenerarMovimientosTransitos.ProcedureName := 'GenerarMovimientosTransitos';         // SS_660_CQU_20140108

            with GenerarMovimientosTransitos do begin
            	Parameters.Refresh;
            	Parameters.ParamByname('@CodigoConvenio').Value     		:= StrToInt(cbConveniosCliente.Value);
                //Parameters.ParamByname('@ConvenioAnterior').Value   		:= 0;                                   // TASK_142_MGO_20170220
                Parameters.ParamByname('@ConvenioAnterior').Value   		:= StrToInt(cbConveniosCliente.Value);  // TASK_142_MGO_20170220
                Parameters.ParamByname('@GrupoFacturacion').Value   		:= 0;
                Parameters.ParamByname('@FechaCorte').Value         		:= deFechaCorte.date;
                Parameters.ParamByname('@FechaEmision').Value       		:= deFechaEmision.date;
                Parameters.ParamByname('@FechaVencimiento').Value			:= deFechaVencimiento.Date;
                Parameters.ParamByname('@NumeroProcesoFacturacion').Value	:= NumeroProcesoFacturacion;
                Parameters.ParamByname('@NoGenerarAjusteSencillo').Value	:= chkFacturarCompleto.Checked;
                Parameters.ParamByname('@NoGenerarIntereses').Value			:= chkFacturarSinIntereses.Checked;
                Parameters.ParamByname('@ConveniosProcesados').Value		:= 0;
                Parameters.ParamByname('@DescripcionError').Value			:= '';
                // Revision 5
                Parameters.ParamByname('@CodigoUsuario').Value				:= UsuarioSistema;
                ExecProc;
                DescripcionError := Parameters.ParamByname('@DescripcionError').Value;
            end;

            HuboError := (DescripcionError <> '');
        end;

        if not HuboError then begin
            if not Visible then TPanelMensajesForm.MuestraMensaje('Generando detalle de consumo...'); // SS_660_CQU_20130604
        	// una vez creado el comprobante sin error
            //se genera el detalle de consumos de este
            //desde la tabla DetalleConsumoMovimientos
            HuboError := not LlenarDetalleConsumoComprobantes(NumeroProcesoFacturacion);
            if HuboError then  MsgBox(MSG_ERROR_DETALLE_COMPROBANTES, Caption, MB_ICONERROR);

        end;

        if not HuboError then begin
            //Obtener el comprobante recientemente creado}
            MensajeError := MSG_ERROR_GET_INVOICE_NUMBER;
            spObtenerComprobantePreFacturado.Close;
            with spObtenerComprobantePreFacturado do begin
            	Parameters.Refresh;
            	Parameters.ParamByName('@CodigoConvenio').Value 			:= StrToInt(cbConveniosCliente.Value);
                Parameters.ParamByName('@NumeroProcesoFacturacion').Value 	:= NumeroProcesoFacturacion;
                Open;

                if not IsEmpty then //Revision 9
                	HuboError := not TipificarYNumerar(	FieldByName('TipoComprobante').AsString,
                                                        vcbTipoDocumentoDeseado.Value,
                    									FieldByName('NumeroComprobante').AsInteger,
                                                        //FieldByName('Saldo').AsInteger,				//SS_1200_MCA_20140707
                                                        Trunc(FieldByName('Saldo').AsFloat),			//SS_1200_MCA_20140707
                                                        cbConveniosCliente.Value )
                else begin
                    HuboError := True;
                    MsgBoxErr(ERROR_NOITEM_BILLING, ERROR_NOITEM_BILLING_MSG, caption, MB_ICONERROR);
                end;
            end;
        end;

        //Indicar fecha y hora de fin del proceso
        if not HuboError then begin
        	MensajeError := MSG_ERROR_UPDATE_BILLING_PROCESS;
            with SPActualizarFechaHoraFinProcesoFacturacion do begin
            	Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
                ExecProc;
            end;
        end;

        //generar un registro en la Cola de env�o a DBNet
        // 11-Agosto-2009: La inserci�n NO PUEDE fallar
        if not HuboError then begin
        	MensajeError := MSG_ERROR_COLA_DBNET;
        	with spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet do begin
        		Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
            	ExecProc;
                i := Parameters.ParamByName('@RETURN_VALUE').Value;
                if i < 0 then begin
                	HuboError := True;
                    MsgBox(MensajeError, Caption, MB_ICONERROR);
                end;

            end;
        end;

        // Rev. 11 (Infractores Fase 2)
        if not HuboError then begin
            MensajeError                := MSG_ERROR_DATOS_INFRACCION_FACTURADA;                                                                    // SS_1045_CQU_20131120
            TotalInfracciones           := 0;                                                                                                       // SS_1045_CQU_20131120
            TotalTransitosInfracciones  := 0;                                                                                                       // SS_1045_CQU_20131120
            with spObtenerCantTransitosPorInfraccion do begin                                                                                       // SS_1045_CQU_20131120
                Parameters.Refresh;                                                                                                                 // SS_1045_CQU_20131120
                Parameters.ParamByName('@NumeroComprobante').Value := spObtenerComprobantePreFacturado.FieldByName('NumeroComprobante').AsInteger;  // SS_1045_CQU_20131120
                Parameters.ParamByName('@TipoComprobante').Value   := spObtenerComprobantePreFacturado.FieldByName('TipoComprobante').AsString;     // SS_1045_CQU_20131120
                Parameters.ParamByName('@TotalInfracciones').Value := null;                                                                         // SS_1045_CQU_20131120
                Parameters.ParamByName('@TotalTransitosInfracciones').Value := null;                                                                // SS_1045_CQU_20131120
                ExecProc;                                                                                                                           // SS_1045_CQU_20131120
                // Obtengo los valores Devueltos                                                                                                    // SS_1045_CQU_20131120
                TotalInfracciones           := Parameters.ParamByName('@TotalInfracciones').Value;                                                  // SS_1045_CQU_20131120
                TotalTransitosInfracciones  := Parameters.ParamByName('@TotalTransitosInfracciones').Value;                                         // SS_1045_CQU_20131120
            end;                                                                                                                                    // SS_1045_CQU_20131120
            // Evaluo y coloco el mensaje que corresponda.                                                                                          // SS_1045_CQU_20131120
            if TotalTransitosInfracciones > 2000 then begin                                                                                         // SS_1045_CQU_20131120
                // Como se demora mucho para grandes cantidades de datos, modifico el TimeOut                                                       // SS_1045_CQU_20131120
                spActualizarInfraccionesFacturadas.CommandTimeout := 0;                                                                             // SS_1045_CQU_20131120
                MensajePanel := Format(MSG_PANEL_INFRACCIONES_DEMORA, [TotalInfracciones, TotalTransitosInfracciones]);                             // SS_1045_CQU_20131120
            end else                                                                                                                                // SS_1045_CQU_20131120
                MensajePanel := MSG_PANEL_INFRACCIONES;                                                                                             // SS_1045_CQU_20131120

            MensajeError := MSG_ERROR_ACTUALIZANDO_INFRACCIONES;
            if not Visible then TPanelMensajesForm.MuestraMensaje(MensajePanel);                                                                    // SS_1045_CQU_20131120
            with spActualizarInfraccionesFacturadas do begin
                if Active then Close;
                Parameters.Refresh;

                Parameters.ParamByName('@NumeroComprobante').Value := spObtenerComprobantePreFacturado.FieldByName('NumeroComprobante').AsInteger;
                Parameters.ParamByName('@TipoComprobante').Value   := spObtenerComprobantePreFacturado.FieldByName('TipoComprobante').AsString;
                Parameters.ParamByName('@CodigoUsuario').Value     := UsuarioSistema;
                ExecProc;
            end;
        end;

        // Fin Rev. 11 (Infractores Fase 2)

        //confirmar o deshacer la transacci�n
        //if not HuboError then DMConnections.BaseCAC.Execute('COMMIT TRAN spFacturacionManual') //REV.9 DMConnections.BaseCAC.CommitTrans // SS_1045_CQU_20120604 (PDO)
        //else DMConnections.BaseCAC.Execute('ROLLBACK TRAN spFacturacionManual'); //REV.9 DMConnections.BaseCAC.RollbackTrans;            // SS_1045_CQU_20120604 (PDO)

        if not HuboError then DMConnections.BaseCAC.CommitTrans // SS_1045_CQU_20120604 (PDO)
        else DMConnections.BaseCAC.RollbackTrans;               // SS_1045_CQU_20120604 (PDO)

        //EnTransaccion := False;                               // SS_1045_CQU_20120604 (PDO)

        //imprimir s�lo si el comprobante fue exitosamente generado
        if not HuboError then begin
            MensajeError := ERROR_PRINTING;
        	with spObtenerComprobantePreFacturado do begin
                LiberarMemoria;		// SS_1017_PDO_20120514
              if not Visible then TPanelMensajesForm.MuestraMensaje('Preparando impresi�n del comprobante generado...'); // SS_660_CQU_20130604
            	//Nota: mbecerra    SIEMPRE DEBE IMPRIMIRSE EL DOCUMENTO ELECTRONICO
            	HuboError := not ImprimirComprobante(	FieldByName('TipoComprobante').AsString ,
                										FieldByName('NumeroComprobante').AsInteger );

                if Hecho and not Visible then                                  // SS_1045_CQU_20120604
                    HuboError := IIf(Respuesta[0] = mrCancel, True, False);    // SS_1045_CQU_20120604

              	if (not HuboError) and (FTipoDocumentoContexto = TC_NOTA_COBRO_INFRACCIONES) then begin
              		ImprimirDetalleInfraccionesAnuladas(	FieldByName('TipoComprobante').AsString ,
                    										FieldByName('NumeroComprobante').AsInteger );
              	end;
            end;
        end;

    except on e:exception do begin
          if not Visible then TPanelMensajesForm.OcultaPanel;   // SS_660_CQU_20130604
        	MsgBoxErr(MensajeError, e.Message, Caption, MB_ICONERROR);
            //if EnTransaccion then DMConnections.BaseCAC.Execute('ROLLBACK TRAN spFacturacionManual'); // SS_1045_CQU_20120604 (PDO)
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;            // SS_1045_CQU_20120604 (PDO)
    	end;
    end;


    if FMostrarMensajeFinal and (MsgBox(MSG_INVOICE_AGAIN, caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin
    	peRutCliente.setFocus;
    	deFechaEmision.Date := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);	//SS_1045_CQU_20120604
    	deFechaCorte.Date := FFechaCorteMaximaPosible;
    	deFechaVencimiento.Date := deFechaEmision.Date + FDiasVencimiento;
    	peRutCliente.Text := '';
        //--------------------MBE Cambios por fase 2
        peRUTCliente.OnChange(peRUTCliente);		//fase 2 para que limpie los campos
      	//btnFacturar.Enabled := False;
    	//Revision 4
    	//cdConceptos.Edit;
    	//cdConceptos.EmptyDataSet;
    	//FIN Revision 4
        //---------------------MBE Fin cambios Fase 2
      lblEnListaAmarilla.Visible := False;                                   //SS_1120_MVI_20130820
      lblEnListaAmarilla.Caption := EmptyStr;                                //SS_1120_MVI_20130820

    end
    else Close;
end;



{******************************** Function Header ******************************
Function Name: cbConveniosClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la lista de convenios se refrescan
               la lista de items de la factura a realizar.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.cbConveniosClienteChange(Sender: TObject);
resourcestring
    ERROR_DATOS_MEDIO_PAGO = 'Error calculando el tipo de medio de pago';
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(''%s'',GETDATE() )';        // SS_660_CQU_20130711    //SS_660_MVI_20130711
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(''%s'',GETDATE(), NULL )';  //SS_660_MVI_20130729  // SS_660_CQU_20130711
    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%s) ';                                                //SS_660_MVI_20130729
var                                                                                                                                         //SS_660_MVI_20130729
    EstadoListaAmarilla : String;                                                                                                           //SS_660_MVI_20130729
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
    //REV.9
    chkFacturarSinIntereses.Enabled := True;
    chkFacturarSinIntereses.Checked := False;

	//Revision 4
    cdConceptos.Edit;
    cdConceptos.EmptyDataSet;
	//FIN Revision 4

    // Obtenemos la descripci�n del medio de pago
    lMedioPago.Caption := '';

    //lblEnListaAmarilla.Color:=clYellow;                                                                                              //SS_660_MVI_20130729    //SS_660_MVI_20130711
    //lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                                                           //SS_660_MVI_20130729    //SS_660_MVI_20130711
    //                                                    Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                      //SS_660_MVI_20130729        //SS_660_MVI_20130711
    //                                                            [   cbConveniosCliente.value                                         //SS_660_MVI_20130729               //SS_660_MVI_20130711
    //                                                            ]                                                                    //SS_660_MVI_20130729        //SS_660_MVI_20130711
    //                                                          )                                                                      //SS_660_MVI_20130729        //SS_660_MVI_20130711
    //                                                 )='True';                                                                       //SS_660_MVI_20130729        //SS_660_MVI_20130711

   if cbConveniosCliente.Items.Count > 0 then  begin                                                                                   //SS_1120_MVI_20130820

    EstadoListaAmarilla:= QueryGetValue(   DMConnections.BaseCAC,                                                                        //SS_660_MVI_20130729
                                             Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                     [   cbConveniosCliente.Value                                                        //SS_660_MVI_20130729
                                                     ]                                                                                   //SS_660_MVI_20130729
                                                   )                                                                                     //SS_660_MVI_20130729
                                                   );                                                                                    //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
//    if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
    if EstadoListaAmarilla <> '' then begin                                                                                              //SS_660_MVI_20130909  //SS_660_MVI_20130729
    lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
    lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    end                                                                                                                                  //SS_660_MVI_20130729
    else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    end;                                                                                                                                 //SS_660_MVI_20130729

   end;                                                                                                 //SS_1120_MVI_20130820

    try
        ObtenerDatosMedioPagoAutomaticoConvenio.close;
        ObtenerDatosMedioPagoAutomaticoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := cbConveniosCliente.value;
        ObtenerDatosMedioPagoAutomaticoConvenio.open;

        if (ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger = TPA_PAT) then
            lMedioPago.Caption := TPA_PAT_DESC
        else
            if (ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger = TPA_PAC) then
                lMedioPago.Caption := TPA_PAC_DESC
            else
                if (ObtenerDatosMedioPagoAutomaticoConvenio.FieldByName('TipoMedioPago').AsInteger = TPA_NINGUNO) then
                    lMedioPago.Caption := TPA_NINGUNO_DESC;
    except
        on e: exception do begin
            msgBoxErr(ERROR_DATOS_MEDIO_PAGO, e.Message, caption, MB_ICONSTOP);
        end;
    end;

 	// Muestra el Domicilio de Facturacion
    MostrarDomicilioFacturacion;

    //REV. 9
    GenerarMovimientosPorIntereses();

    // Obtenemos el resumen a Facturar para la fecha de corte actual
    ObtenerResumenAFacturar(deFechaCorte.date);

    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

{******************************** Function Header ******************************
Function Name: peRUTClienteButtonClick
Author       : fmalisia
Date Created : 24-11-2004
Description  : Permite buscar un cliente por medio del form FormBuscaClientes.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    application.createForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peRUTCliente.Text :=  F.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
            btnBuscar.Click;
        end;

        if F.Persona.CodigoPersona <> 0 then begin
            MostrarMensajeTipoClienteSistema(Trim(peRUTCliente.Text));
        end ;
    end;
    F.free;
end;


{******************************** Function Header ******************************
Function Name: peRUTClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia el RUT del cliente se cargan todos sus datos asociados
               (Lista de convenios, nombre, domicilio y detalle de la factura a realizar).
Parameters   : Sender: TObject
Return Value : None

Revision 1:
Author: Fsandi
Date: 31-05-2007
Description: Se limpian los check de ajuste e intereses cuando se cambia el rut
*******************************************************************************}
procedure TFacturacionManualForm.peRUTClienteChange(Sender: TObject);
begin
  //Revision 4
  cdConceptos.Edit;
  cdConceptos.EmptyDataSet;
  chkFacturarCompleto.Checked := False;			//revision 1
  chkFacturarSinIntereses.Checked := False;		//revision 1
  chkFacturarSinIntereses.Enabled := True; 		//REV.9
  //---------------MBE 26-Julio-2010----Cambios fase 2
  cbConveniosCliente.Clear;
  NumeroConvenio.Text := '';
  vcbTipoDocumentoDeseado.ItemIndex := 0;

  //FIN Revision 4
  //if (activeControl <> sender) then Exit;

  //-------------------------MBE 26-Julio-2010
    lblEnListaAmarilla.Visible := False;    // SS_660_CQU_20130816
    lblEnListaAmarilla.Caption := EmptyStr; // SS_660_CQU_20130816

end;

//Reviison 4
procedure TFacturacionManualForm.PopFacturacionManualPopup(Sender: TObject);
begin
    CdConceptos.Edit;
    if CdConceptos.RecordCount > 0 then begin
        MnuSeleccionarTodo.Enabled := True;
        MnuDeSeleccionarTodo.Enabled := True;
    end else begin
        MnuSeleccionarTodo.Enabled := False;
        MnuDeSeleccionarTodo.Enabled := False;
    end;
end;
//Fin Revision 4

{******************************** Function Header ******************************
Function Name: deFechaCorteExit
Author       : fmalisia
Date Created : 24-11-2004
Description  : Al dejar el control de fecha de corte se valida si es correcta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.deFechaCorteExit(Sender: TObject);
begin
	if deFechaCorte.Date = nulldate then begin
		MsgBoxBalloon(MSG_FECHA_CORTE_NO_EXISTE, caption, MB_ICONSTOP, deFechaCorte);
        exit;
	end;
	if deFechaCorte.Date > FFechaCorteMaximaPosible then begin
		MsgBoxBalloon(MSG_FECHA_CORTE_INCORRECTA, caption, MB_ICONSTOP, deFechaCorte);
	end;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author :
Date Created :
Description :
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None

Revision : 1
    Author : ggomez
    Date : 30/11/2006
    Description : Agregu� la liberaci�n de la lista de convenios dados de baja.
*******************************************************************************}
procedure TFacturacionManualForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if FListaConveniosBaja <> Nil then begin
        FreeAndNil(FListaConveniosBaja);
    end;

	Action := caFree;
  CdConceptos.Close;
end;

{******************************** Function Header ******************************
Function Name: deFechaVencimientoExit
Author       : fmalisia
Date Created : 24-11-2004
Description  : Al dejar el control de fecha de vencimiento se valida que sea correcta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.deFechaVencimientoExit(
  Sender: TObject);
begin
	if deFechaVencimiento.Date = nulldate then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_NO_EXISTE, caption, MB_ICONSTOP, deFechaVencimiento);
        exit;
	end;
	if deFechaCorte.Date > deFechaVencimiento.Date then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_INCORRECTA, caption, MB_ICONSTOP, deFechaVencimiento);
        exit;
	end;
	if not EsFechaHabil(deFechaVencimiento.date) then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_NO_HABIL, caption, MB_ICONSTOP, deFechaVencimiento);
	end;
end;

{******************************** Function Header ******************************
Function Name: deFechaEmisionChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la fecha de emisi�n se actualiza la de vencimiento en funci�n de esta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.deFechaEmisionChange(Sender: TObject);
begin
    deFechaVencimiento.Date := deFechaEmision.Date + FDiasVencimiento;
end;

{******************************** Function Header ******************************
Function Name: deFechaEmisionExit
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando se dejka el control de fecha de emisi�n se valida si es correcta.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.deFechaEmisionExit(Sender: TObject);
begin
	if deFechaEmision.Date = nulldate then begin
		MsgBoxBalloon(MSG_FECHA_EMISION_NO_EXISTE, caption, MB_ICONSTOP, deFechaEmision);
        exit;
	end;
	if deFechaEmision.Date < deFechaCorte.Date then begin
		MsgBoxBalloon(MSG_FECHA_EMISION_INCORRECTA, caption, MB_ICONSTOP, deFechaEmision);
        exit;
	end;
    {if not EsFechaHabil(deFechaEmision.date) then begin
		MsgBoxBalloon(MSG_FECHA_EMISION_NO_HABIL, caption, MB_ICONSTOP, deFechaEmision);
	end;}
end;

{******************************** Function Header ******************************
Function Name: deFechaCorteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la fecha de corte se actualizan los items a facturar.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.deFechaCorteChange(Sender: TObject);
var
    fecha: TDateTime;
begin
    Fecha := deFechaCorte.date;
    ObtenerResumenAFacturar(Fecha);
end;

procedure TFacturacionManualForm.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;


{******************************** Function Header ******************************
Function Name: ObtenerResumenAFacturar
Author       : fmalisia
Date Created : 24-11-2004
Description  : Obtiene el detalle de la factura resultado.
Parameters   : Fecha: TDateTime
Return Value : None
*******************************************************************************}
procedure TFacturacionManualForm.ObtenerResumenAFacturar(Fecha: TDateTime);
resourcestring
    ERROR_INFORME_FACTURA = 'Error obteniendo el resumen a facturar.';
begin
    try
        ObtenerUltimosConsumos.Close;
        with ObtenerUltimosConsumos do begin
            if (FTipoDocumentoContexto = TC_NOTA_COBRO) or (FTipoDocumentoContexto = TC_FACTURACION_INMEDIATA) or (FTipoDocumentoContexto = TC_FACTURACION_POR_BAJA) then begin      // SS_964_PDO_20110531
                ProcedureName := 'ObtenerUltimosConsumos';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConveniosCliente.Value <> null, cbConveniosCliente.Value, 0);
                Parameters.ParamByName('@IndiceVehiculo').Value := null;
                Parameters.ParamByName('@FechaCorte').Value := iif((Fecha = nulldate) or (year(fecha) < 1970) or (year(fecha) > 2079), null, fecha);
            end
            else if FTipoDocumentoContexto = TC_NOTA_COBRO_INFRACCIONES then begin
                ProcedureName := 'ObtenerMovimientosInfractor';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConveniosCliente.Value <> null, cbConveniosCliente.Value, 0);
            end;

            Open;
            //Revision 4
            CargarDataSet;
            //Fin de Revision 4
            //REV.9 btnFacturar.Enabled := ValidarConsumos;//not ObtenerUltimosConsumos.IsEmpty;
        end;

    except
        on e: Exception do begin
            msgBoxErr(ERROR_INFORME_FACTURA, e.Message, caption, MB_ICONSTOP);
        end;
    end;
end;


procedure TFacturacionManualForm.ClearCaptions;
begin
    lNombreCompleto.Caption         := '';
    lMedioPago.Caption              := '';
    lDomicilio.Caption              := '';
    lComuna.Caption                 := '';
    lRegion.Caption                 := '';
    lDomicilioFacturacion.Caption   := '';
    lComunaFacturacion.Caption      := '';
    lRegionFacturacion.Caption      := '';
end;

//Revision 4
procedure TFacturacionManualForm.CargarDataset;
resourcestring                                                                                                          // SS_660E_CQU_20140806
    FILTRO_INFRACTORES  = 'CodigoConcepto = %s OR CodigoConcepto = %s OR CodigoConcepto = %s OR CodigoConcepto = %s';   // SS_660E_CQU_20140806
var
	Seleccionado : boolean;		//REV.9 	29-Junio-2009
    Filtrado : Boolean;                                                                                                 // SS_660E_CQU_20140806
begin
    if not cdconceptos.Active then cdConceptos.CreateDataSet;
    cdConceptos.Open;
    cdConceptos.Edit;
    cdConceptos.EmptyDataSet;
    if FSoloListaAmarilla and not ObtenerUltimosConsumos.Eof then begin                 // SS_660E_CQU_20140806
        ObtenerUltimosConsumos.Filter := Format(FILTRO_INFRACTORES, [IntToStr(FCat1),   // SS_660E_CQU_20140806
                                                                    IntToStr(FCat2),    // SS_660E_CQU_20140806
                                                                    IntToStr(FCat3),    // SS_660E_CQU_20140806
                                                                    IntToStr(FCat4)]);  // SS_660E_CQU_20140806
        ObtenerUltimosConsumos.Filtered := True;                                        // SS_660E_CQU_20140806
        Filtrado := True;                                                               // SS_660E_CQU_20140806
    end;                                                                                // SS_660E_CQU_20140806

    while not ObtenerUltimosConsumos.Eof do begin
        // Insertamos nuestro registro

        Seleccionado :=                                                                                                                             // SS_964_PDO_20110531
            (FTipoDocumentoContexto = TC_NOTA_COBRO_INFRACCIONES) //REV.9                                                                           // SS_964_PDO_20110531
                or ((FTipoDocumentoContexto = TC_FACTURACION_INMEDIATA) and ObtenerUltimosConsumos.FieldByName('FacturacionInmediata').AsBoolean)   // SS_964_PDO_20110531
                or (FTipoDocumentoContexto = TC_FACTURACION_POR_BAJA);                                                                              // SS_964_PDO_20110531

        if ObtenerUltimosConsumos.FieldByName('EsPrefacturado').AsInteger = 0 then begin 	//REV.9 11-Mayo-2009
        	cdConceptos.AppendRecord(	[	{REV.9 True} Seleccionado,
            								ObtenerUltimosConsumos.FieldByName('CONCEPTO').AsString,
                                            ObtenerUltimosConsumos.FieldByName('DescImporte').AsString,
                                            ObtenerUltimosConsumos.FieldByName('CODIGOCONCEPTO').AsInteger,
                                            ObtenerUltimosConsumos.FieldByName('AfectaFechaCorte').AsBoolean,
                                            ObtenerUltimosConsumos.FieldByName('FacturacionInmediata').AsBoolean                                    // SS_964_PDO_20110531
            							]);
        end;

        ObtenerUltimosConsumos.Next;
    end;
end;

{******************************** Function Header ******************************
Function Name: DBListEx1DblClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None
Revision :1
    Author : vpaszkowicz
    Date : 08/07/2008
    Description : Le quito la posiblitdad de desfiltrar cualquier concepto que
    sea de peajes.
*******************************************************************************}
procedure TFacturacionManualForm.DBListEx1DblClick(Sender: TObject);
var
    CodigoConcepto : Integer;

    //Dado que los peajes se manejan por duo (actuales y anteriores),
    //Este procedimiento marca el otro concepto de peaje cuando hay uno marcado

    
    {comentada en REV.10
    procedure MarcarDuoPeajes(Marcado : Integer);
    var
        i, CantidadRegistros, RegAMarcar : integer;
    begin
        RegAMarcar := 0;
        if Marcado = CodigoPeajes then RegAMarcar := CodigoPeajesAnteriores;
        if Marcado = CodigoPeajesAnteriores then RegAMarcar := CodigoPeajes;
        CantidadRegistros := CdConceptos.RecordCount;
        CdConceptos.First;
        For i := 0 to CantidadRegistros do begin
            if (cdConceptos.FieldByName('CodigoConcepto').AsInteger = RegAMarcar) then begin
                CdConceptos.Edit;
                CdConceptos.FieldByName('Seleccionado').AsBoolean := not (cdConceptos.FieldByName('Seleccionado').AsBoolean);
                Break;
            end;
            CdConceptos.Next;
        end;
    end;

    Fin REV.10
    }

begin
    //cdConceptos.Edit;
    CodigoConcepto := cdConceptos.FieldByName('CodigoConcepto').AsInteger;
    if ((cdConceptos.RecordCount > 0) {REV.9 and not (CodigoConcepto in [CONCEPTO_PEAJES_PERIODO, CONCEPTO_PEAJES_OTROS_PERIODOS])}) then begin
        cdConceptos.Edit;
        cdConceptos.FieldByName('Seleccionado').AsBoolean := not (cdConceptos.FieldByName('Seleccionado').AsBoolean);
        //Verificamos, si el item que se esta marcando es un peaje anterior o un peaje del periodo
        //entonces tambien se marca el otro, usando el procedimiento MarcarDuoPeajes

        //Comentado en Rev.1
        {if ((CodigoConcepto = CodigoPeajes) or
         (CodigoConcepto = CodigoPeajesAnteriores)) then begin
            MarcarDuoPeajes (CodigoConcepto);
        end;}
        //cdConceptos.Edit;
        cdConceptos.Post;
        if cdConceptos.FieldByName('AfectaFechaCorte').AsBoolean then
        	MarcarTodos(cdConceptos.FieldByName('Seleccionado').AsBoolean);
    end;
end;


procedure TFacturacionManualForm.DBListEx1DrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    cdConceptos.Edit;
    if cdConceptos.RecordCount>0 then begin
        if Column.FieldName = 'Seleccionado' then begin
              if cdConceptos.FieldByName('Seleccionado').AsBoolean = True then begin
                  ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
              end else if cdConceptos.FieldByName('Seleccionado').AsBoolean = False then begin
                  ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
              end;
              DefaultDraw := False;
        end;
    end else begin
        ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
    end;
end;
procedure TFacturacionManualForm.DBListEx1KeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key = #32  then  DBListEx1DblClick(Self);
end;

//FIN Revision 4

{-----------------------------------------------------------------------------
            	ImprimirComprobante

Author: mbecerra
Date: 31-Marzo-2009
Description:	(Ref. Factura Electronica)
            	Las NK antiguads se imprimen con el mismo nuevo
                reporte de boletas y facturas electr�nicas.
-----------------------------------------------------------------------------}
Function TFacturacionManualForm.ImprimirComprobante;
resourcestring
    MSG_ERROR_COBRAR	= 'No se actualiz� el campo "ReImpresi�n" para el comprobante';

var
    f : TReporteBoletaFacturaElectronicaForm;
    DescError : string;
begin
    Result := False;

    //Application.CreateForm(TReporteBoletaFacturaElectronicaForm,f);   // SS_1017_PDO_20120514
    f := TReporteBoletaFacturaElectronicaForm.create(nil);              // SS_1017_PDO_20120514

    try
        if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, DescError, True) then begin
        // Inicio Bloque SS_1045_CQU_20120604
    		//if f.Ejecutar(True) then begin
                if not Visible then f.Visible := False;		// SS_1045_CQU_20120604
                f.Hecho := Hecho;
                f.Opcion1 := Opcion1;
                f.Opcion2 := Opcion2;
                f.Respuesta := Respuesta;
                if not Visible then TPanelMensajesForm.OcultaPanel;  // SS_660_CQU_20130604
            if f.Ejecutar() then begin
                    Hecho := f.Hecho;
                    Opcion1 := f.Opcion1;
                    Opcion2 := f.Opcion2;
                    Respuesta := f.Respuesta;
        // Fin Bloque SS_1045_CQU_20120604
            	// Hago que se marque como Impreso el Comprobante...
                {with spActualizarFechaImpresionComprobantes do begin
                	Parameters.ParamByname('@TipoComprobante').Value := TipoComprobante;
            		Parameters.ParamByname('@NumeroComprobante').Value := NroComprobante;
            		Parameters.ParamByname('@CodigoUsuario').Value := UsuarioSistema;
            		ExecProc;
                end;}




                //spCobrar_o_DescontarReimpresion.Close;                                    // SS_1246_CQU_20151006
                //with spCobrar_o_DescontarReimpresion do begin                             // SS_1246_CQU_20151006
                //	Parameters.Refresh;                                                     // SS_1246_CQU_20151006
                //    Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;  // SS_1246_CQU_20151006
                //	Parameters.ParamByName('@NumeroComprobante').Value := NroComprobante;   // SS_1246_CQU_20151006
                //	Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;       // SS_1246_CQU_20151006
                //	ExecProc;                                                               // SS_1246_CQU_20151006
                //	if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin         // SS_1246_CQU_20151006
                //		MsgBox(MSG_ERROR_COBRAR, Caption, MB_ICONEXCLAMATION);              // SS_1246_CQU_20151006
                //	end;                                                                    // SS_1246_CQU_20151006

                //end;                                                                      // SS_1246_CQU_20151006
              if not Visible then TPanelMensajesForm.MuestraMensaje('Imprimiendo comprobante...');  // SS_660_CQU_20130604
            	ImprimirFacturacionDetallada(TipoComprobante, NroComprobante);
                Result := True;
            end else begin // Inicio Bloque SS_1045_CQU_20120604
                try
                    Hecho := f.Hecho;
                    Opcion1 := f.Opcion1;
                    Opcion2 := f.Opcion2;
                    Respuesta := f.Respuesta;
                finally

                end;

            end; // Fin Bloque SS_1045_CQU_20120604
        end
        else MsgBox( DescError, Caption, MB_ICONERROR );

	finally
        //f.Release;                            // SS_1017_PDO_20120514
        if Assigned(f) then FreeAndNil(f);      // SS_1017_PDO_20120514
        if not Visible and not Result then TPanelMensajesForm.OcultaPanel;  // SS_660_CQU_20130604
    end;
end;

function TFacturacionManualForm.ImprimirDetalleInfraccionesAnuladas(
  TipoComprobante: string; NroComprobante: int64): Boolean;
resourcestring
    MSG_ERROR_COBRAR	= 'No se actualiz� el campo "ReImpresi�n" para el comprobante';

var
    f : TRptDetInfraccionesAnuladas;
    DescError : string;
    MostrarVentana : Boolean;       // SS_1045_CQU_20120604
begin
    Result := False;

    Application.CreateForm(TRptDetInfraccionesAnuladas,f);      // SS_1017_PDO_20120514
    f := TRptDetInfraccionesAnuladas.Create(nil);               // SS_1017_PDO_20120514

    try
        if Hecho and not Visible then	// SS_1045_CQU_20120604
            MostrarVentana := False		// SS_1045_CQU_20120604
        else							// SS_1045_CQU_20120604
            MostrarVentana := True;		// SS_1045_CQU_20120604
        //if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, DescError) then begin  //REV.9       		// SS_1045_CQU_20120604
        if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, DescError, False, MostrarVentana) then begin	// SS_1045_CQU_20120604
			//REV.9 f.Release;
			//spCobrar_o_DescontarReimpresion.Close;                                    // SS_1246_CQU_20151006
			//with spCobrar_o_DescontarReimpresion do begin                             // SS_1246_CQU_20151006
  			//	Parameters.Refresh;                                                     // SS_1246_CQU_20151006
  			//	Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;    // SS_1246_CQU_20151006
  			//	Parameters.ParamByName('@NumeroComprobante').Value := NroComprobante;   // SS_1246_CQU_20151006
  			//	Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;       // SS_1246_CQU_20151006
  			//	ExecProc;                                                               // SS_1246_CQU_20151006
  			//	if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin         // SS_1246_CQU_20151006
      		//		MsgBox(MSG_ERROR_COBRAR, Caption, MB_ICONEXCLAMATION);              // SS_1246_CQU_20151006
  			//	end;                                                                    // SS_1246_CQU_20151006

			//end;                                                                      // SS_1246_CQU_20151006

			Result := True;
        end
        else MsgBox( DescError, Caption, MB_ICONERROR );

	finally
        //f.Release;                            // SS_1017_PDO_20120514
        if Assigned(f) then FreeAndNil(f);      // SS_1017_PDO_20120514
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: ImprimirFacturacionDetallada
  Author:    flamas
  Date Created: 29/03/2005
  Description: Imprime la facturaci�n Detallada si corresponde
  Parameters: TipoComprobante:string; NroComprobante: int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFacturacionManualForm.ImprimirFacturacionDetallada(TipoComprobante:string; NroComprobante: int64): Boolean;
resourcestring
	MSG_ERROR_GETTING_DETAIL 	= 'Error obteniendo datos de Facturaci�n Detallada';
var
    f: TformReporteFacturacionDetallada;
    Error:String;
begin
    result := True;
	with spObtenerDatosFacturacionDetallada, Parameters do begin
        Close;
    	ParamByName('@TipoComprobante').Value := TipoComprobante;
        ParamByName('@NumeroComprobante').Value := NroComprobante;
        try
        	Open;
            if (FieldByName('ImpresionDetallada').AsBoolean) then begin

                // Lanza el reporte de Facturacion detallada...
                Screen.Cursor := crHourGlass;
                Application.CreateForm(TformreportefacturacionDetallada,f);
                try
                    // Inicio Bloque SS_1045_CQU_20120604
                    //if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, Error, True) then
                    //    result := f.Ejecutar;
                    if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, Error, True) then begin
                        if Hecho and not Visible then
                            //    Result := Opcion2			// SS_1045_CQU_20120604
                            //else							// SS_1045_CQU_20120604
                            //    result := f.Ejecutar;		// SS_1045_CQU_20120604
                            f.MostrarConfirmacion := False;	// SS_1045_CQU_20120604
                        result := f.Ejecutar;
                    end;
                    // Fin Bloque SS_1045_CQU_20120604
                finally
                    f.Release;
                    Screen.Cursor := crDefault;
                end;
            end;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_DETAIL, e.Message, Caption, MB_ICONSTOP);
            	result := False;
            end;
        end;
	end;
end;

procedure TFacturacionManualForm.NumeroConvenioChange(Sender: TObject);
var
	Documento : String;
begin
	if (activeControl = sender) then begin
		// voy buscando el cliente que tenga este convenio, si existe pongo su RUT en el buscador
		Documento := Trim( QueryGetValue (DMConnections.BaseCAC, 'SELECT ISNULL (NumeroDocumento, '''') FROM Personas  WITH (NOLOCK) WHERE CodigoPersona = (SELECT CodigoCliente FROM Convenio  WITH (NOLOCK) WHERE NumeroConvenio = ' + QuotedStr(NumeroConvenio.Text) + ')'));

		if Documento <> '' then peRUTCliente.setFocus;
		peRUTCliente.Text := Documento
	end
end;

//Revision 4
procedure TFacturacionManualForm.MnuDeSeleccionarTodoClick(Sender: TObject);
var
    i, CantidadMovimientos : Integer;
begin
    CantidadMovimientos := CdConceptos.RecordCount;
    CdConceptos.First;
    For i:=0 to CantidadMovimientos -1 do begin
        //REV.9
        //if not cdConceptos.FieldByName('CodigoConcepto').asInteger in [CONCEPTO_PEAJES_PERIODO, CONCEPTO_PEAJES_OTROS_PERIODOS] then begin
            CdConceptos.Edit;
            cdConceptos.FieldByName('Seleccionado').AsBoolean := False;
            CdConceptos.Next;
        //REV.9
        //end;
    end;
end;

procedure TFacturacionManualForm.MnuSeleccionarTodoClick(Sender: TObject);
var
i, CantidadMovimientos : Integer;

begin

    CantidadMovimientos := CdConceptos.RecordCount;
    CdConceptos.First;
    For i:=0 to CantidadMovimientos -1 do begin
        CdConceptos.Edit;
        cdConceptos.FieldByName('Seleccionado').AsBoolean := True;
        CdConceptos.Next;
    end;
end;

//FIN Revision 4
procedure TFacturacionManualForm.MostrarDomicilioFacturacion;
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := cbConveniosCliente.value;
        try
        	ExecProc;
    		lDomicilioFacturacion.Caption	:= ParamByName('@Domicilio').Value;
 			lComunaFacturacion.Caption		:= ParamByName('@Comuna').Value;
            lRegionFacturacion.Caption 		:= ParamByName('@Region').Value;
        except
        	on e: exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

{*******************************************************************************
Revision : 1
    Author : nefernandez
    Date : 23/08/2007
    Description : SS 548: Cambio del AsInteger a AsVariant para el campo IMPORTE
*******************************************************************************}
function TFacturacionManualForm.ValidarConsumos: Boolean;
    function IncluirEnNotaCobro(CodigoConcepto: integer):Boolean;
    var
        Q: AnsiString;
    begin
        Q := QueryGetValue(DMCOnnections.BaseCAC,
                   Format( ' SELECT IncluirNotaCobro FROM ConceptosMovimiento WITH(NOLOCK) '+
                           ' WHERE CodigoConcepto = %d ',[CodigoConcepto]));
        Result := (TRIM(Q) = 'True')
    end;
var
    Suma: int64;
begin
    Result := False;
//    if ObtenerUltimosConsumos.IsEmpty then Exit;
    if cbConveniosCliente.Text = EmptyStr then Exit;
    with ObtenerUltimosConsumos do begin
        DisableControls;
        First;
        Suma := 0;
        while NOT Eof do begin
            if IncluirEnNotaCobro(FieldByName('CodigoConcepto').AsInteger) then
                        Suma := Suma + FieldByName('IMPORTE').AsVariant;
            Next;
        end;
        EnableControls;
    end;
    Result := (Suma >= 0);
end;

{******************************** Function Header ******************************
Function Name: cbConveniosClienteDrawItem
Author :
Date Created :
Description :
Parameters : Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState
Return Value : None

Revision : 1
    Author : ggomez
    Date : 30/11/2006
    Description :
        - Modifiqu� para que use la propiedad privada FListaConveniosBaja.
        - Modifiqu� para que no use la posici�n del item para saber si un convenio
        est� dado de baja, en su lugar ahora se usa el Codigo de Convenio para
        saber si est� entre los convenios dados de baja.
*******************************************************************************}
{                                                                                                                                               //SS_1120_MVI_20130809
procedure TFacturacionManualForm.cbConveniosClienteDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);         //SS_1120_MVI_20130809
var                                                                                                                                             //SS_1120_MVI_20130809
    CodigoConvenio: AnsiString;                                                                                                                 //SS_1120_MVI_20130809
begin                                                                                                                                           //SS_1120_MVI_20130809
    CodigoConvenio := cbConveniosCliente.Items[Index].Value;                                                                                    //SS_1120_MVI_20130809
    // Si el Convenio est� en la lista de los convenios dados de baja, entonces                                                                 //SS_1120_MVI_20130809
    // colorearlo de manera diferente a los que NO est�n de baja.                                                                               //SS_1120_MVI_20130809
    if FListaConveniosBaja.IndexOf(CodigoConvenio) <> -1 then begin                                                                             //SS_1120_MVI_20130809
        ItemCBColor(Control, Index, Rect, cl3DLight, clRed)                                                                                     //SS_1120_MVI_20130809
    end else begin                                                                                                                              //SS_1120_MVI_20130809
        ItemCBColor(Control, Index, Rect, $00FAEBDE, clBlack);                                                                                  //SS_1120_MVI_20130809
    end;                                                                                                                                        //SS_1120_MVI_20130809
end;                                                                                                                                            //SS_1120_MVI_20130809
}                                                                                                                                               //SS_1120_MVI_20130809
                                                                                                                                                //SS_1120_MVI_20130809
procedure TFacturacionManualForm.cbConveniosClienteDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);         //SS_1120_MVI_20130809
//BEGIN:SS_1120_MVI_20130820 -----------------------------------------------------
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConveniosCliente.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;

//	const                                                                                                                                         //SS_1120_MVI_20130809
//    	cSQLEsConvenioDeBaja = 'SELECT dbo.EsConvenioDeBaja(%d)';                                                                                 //SS_1120_MVI_20130809
//    var                                                                                                                                         //SS_1120_MVI_20130809
//    	CodigoConvenio: Integer;                                                                                                                  //SS_1120_MVI_20130809
//        FontColor,                                                                                                                              //SS_1120_MVI_20130809
//        BackGroundColor: TColor;                                                                                                                //SS_1120_MVI_20130809
//        FontBold: Boolean;                                                                                                                      //SS_1120_MVI_20130809
//begin                                                                                                                                           //SS_1120_MVI_20130809
//	CodigoConvenio := cbConveniosCliente.Items[Index].Value;                                                                                      //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//	if SysUtilsCN.QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLEsConvenioDeBaja,[CodigoConvenio])) then begin                           //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//        BackGroundColor := clWhite;                                                                                                             //SS_1120_MVI_20130809
//        FontColor 		:= clRed;                                                                                                                 //SS_1120_MVI_20130809
//	    FontBold 		:= True;                                                                                                                      //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//    	if (Control as TVariantComboBox).DroppedDown then begin                                                                                   //SS_1120_MVI_20130809
//            if (odselected in State) then begin                                                                                                 //SS_1120_MVI_20130809
//	        	BackGroundColor := clHighlight;                                                                                                     //SS_1120_MVI_20130809
//		        FontColor 		:= clWhite;                                                                                                           //SS_1120_MVI_20130809
//            end;                                                                                                                                //SS_1120_MVI_20130809
//        end;                                                                                                                                    //SS_1120_MVI_20130809
//    end                                                                                                                                         //SS_1120_MVI_20130809
//    else begin                                                                                                                                  //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//        BackGroundColor := clWhite;                                                                                                             //SS_1120_MVI_20130809
//        FontColor		:= clBlack;                                                                                                                 //SS_1120_MVI_20130809
//        FontBold 		:= False;                                                                                                                   //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//    	if (Control as TVariantComboBox).DroppedDown then begin                                                                                   //SS_1120_MVI_20130809
//            if (odselected in State) then begin                                                                                                 //SS_1120_MVI_20130809
//	        	BackGroundColor := clHighlight;                                                                                                     //SS_1120_MVI_20130809
//		        FontColor 		:= clWhite;                                                                                                           //SS_1120_MVI_20130809
//	        end;                                                                                                                                  //SS_1120_MVI_20130809
//        end;                                                                                                                                    //SS_1120_MVI_20130809
//    end;                                                                                                                                        //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//	ItemCBColor(Control, Index, Rect, BackGroundColor, FontColor, FontBold);                                                                      //SS_1120_MVI_20130809
//end;                                                                                                                                            //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//                                                                                                                                                //SS_1120_MVI_20130809
//END:SS_1120_MVI_20130820 -----------------------------------------------------                                                              
{-----------------------------------------------------------------------------
  Procedure Name: ItemCBColor
  Author:    ddiaz
  Date Created: 18/09/2006
  Description: Pinta en el combobox los convenios con distinto solor seg�n el estado (Vigrnte o Baja)
                SS 0341
-----------------------------------------------------------------------------}
{                                                                                                                                                 //SS_1120_MVI_20130809
procedure TFacturacionManualForm.ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor);                        //SS_1120_MVI_20130809
begin                                                                                                                                             //SS_1120_MVI_20130809
    with (cmbBox as TVariantComboBox) do begin                                                                                                    //SS_1120_MVI_20130809
        Canvas.Brush.color := ColorFondo;                                                                                                         //SS_1120_MVI_20130809
        Canvas.FillRect(R);                                                                                                                       //SS_1120_MVI_20130809
        Canvas.Font.Color := ColorTexto;                                                                                                          //SS_1120_MVI_20130809
        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption);                                                                                        //SS_1120_MVI_20130809
   end;                                                                                                                                           //SS_1120_MVI_20130809
end;                                                                                                                                              //SS_1120_MVI_20130809
}                                                                                                                                                 //SS_1120_MVI_20130809
procedure TFacturacionManualForm.ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor; Bold: Boolean);         //SS_1120_MVI_20130809
begin                                                                                                                                             //SS_1120_MVI_20130809
    with (cmbBox as TVariantComboBox) do begin                                                                                                    //SS_1120_MVI_20130809
        Canvas.Brush.color := ColorFondo;                                                                                                         //SS_1120_MVI_20130809
        Canvas.FillRect(R);                                                                                                                       //SS_1120_MVI_20130809
        Canvas.Font.Color := ColorTexto;                                                                                                          //SS_1120_MVI_20130809
        if Bold then begin                                                                                                                        //SS_1120_MVI_20130809
        	Canvas.Font.Style := Canvas.Font.Style + [fsBold]                                                                                       //SS_1120_MVI_20130809
        end                                                                                                                                       //SS_1120_MVI_20130809
        else begin                                                                                                                                //SS_1120_MVI_20130809
        	Canvas.Font.Style := Canvas.Font.Style - [fsBold]                                                                                       //SS_1120_MVI_20130809
        end;                                                                                                                                      //SS_1120_MVI_20130809
                                                                                                                                                  //SS_1120_MVI_20130809
        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption + Util.iif(Bold, ' (Baja)', ''));                                                        //SS_1120_MVI_20130809
   end;                                                                                                                                           //SS_1120_MVI_20130809
end;                                                                                                                                              //SS_1120_MVI_20130809
                                                                                                                                                  //SS_1120_MVI_20130809

{*******************************************************************************
    Procedure Name: CargarDatosIntereses
    Author : rharris
    Date : 24/12/2008
    Description : (SS 627) se calculan los intereses desde tablas de trabajo
*******************************************************************************}
function TFacturacionManualForm.CargarDatosIntereses;
resourcestring
    MSG_TEMP	= 'Error al llenar tabla tmp_ConveniosConInteresesPorFacturar';
begin
	try
    	spGenerarTablaTrabajoInteresesPorFacturar.Close;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.Refresh;
		//spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@CodigoGrupoFacturacion').Value := NULL;
		spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@FechaEmision').Value := FechaEmision;
        spGenerarTablaTrabajoInteresesPorFacturar.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
		spGenerarTablaTrabajoInteresesPorFacturar.ExecProc;
		Result := 1;
	except on e: Exception do begin
			MsgBoxErr(MSG_TEMP, e.Message, caption, MB_ICONSTOP);
			Result := -1;
		end;
	end;
end;

{*******************************************************************************
    Procedure Name: LlenarDetalleConsumoComprobantes
    Author : rharris
    Date : 24/12/2008
    Description : (SS 627) se calculan los detalle de consumo del comprobante
    desde los movimientos prefacturados
*******************************************************************************}
function TFacturacionManualForm.LlenarDetalleConsumoComprobantes;
resourcestring
	MSG_ERROR = 'Ocurri� un error al llenar DetalleConsumoComprobantes';
begin
  	try
		//spLlenarDetalleConsumoComprobantes.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;             // SS_660_CQU_20121010
		//spLlenarDetalleConsumoComprobantes.ExecProc;                                                                                          // SS_660_CQU_20121010
        if FTipoDocumentoContexto <> TC_NOTA_COBRO_INFRACCIONES then begin                                                                      // SS_660_CQU_20121010
            //spLlenarDetalleConsumoComprobantes.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;         // SS_660_CQU_20140108  // SS_660_CQU_20121010
		    //spLlenarDetalleConsumoComprobantes.ExecProc;                                                                                      // SS_660_CQU_20140108  // SS_660_CQU_20121010
            if lblEnListaAmarilla.Visible and Visible then begin                                                                                // SS_660_CQU_20140108
                spLlenarDetalleConsumoComprobantesMorosos.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;// SS_660_CQU_20140108
		        spLlenarDetalleConsumoComprobantesMorosos.ExecProc;                                                                             // SS_660_CQU_20140108
            end else begin                                                                                                                      // SS_660_CQU_20140108
                spLlenarDetalleConsumoComprobantes.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;       // SS_660_CQU_20140108
	    	    spLlenarDetalleConsumoComprobantes.ExecProc;                                                                                    // SS_660_CQU_20140108
            end;                                                                                                                                // SS_660_CQU_20140108
        end else begin                                                                                                                          // SS_660_CQU_20121010
            spLlenarDetalleConsumoComprobantesMorosos.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NumeroProcesoFacturacion;    // SS_660_CQU_20121010
		    spLlenarDetalleConsumoComprobantesMorosos.ExecProc;                                                                                 // SS_660_CQU_20121010
        end;                                                                                                                                    // SS_660_CQU_20121010
                                                                                                                                                
        Result := True;
	except on e: exception do begin
			MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONSTOP);
            Result := False;
		end;
	end;

end;

procedure TFacturacionManualForm.MostrarMensajeTipoClienteSistema(
  NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeTipoCliente(DMCOnnections.BaseCAC, NumeroDocumento, SistemaActual);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

// SS_1017_PDO_20120514 Inicio bloque
procedure TFacturacionManualForm.LiberarMemoria;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
end;
// SS_1017_PDO_20120514 Fin bloque
end.


