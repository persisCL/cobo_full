{-------------------------------------------------------------------------------
 File Name: FrmReclamoTransito.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos

 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 24-Junio-2010
 Description	: (Ref.Fase 2) Agregar concesionaria y subtipo de reclamo

 Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos. 
 -------------------------------------------------------------------------------}
unit FrmReclamoTransito;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  FreContactoReclamo,             //Frame contacto
  FreCompromisoOrdenServicio,     //Frame compromiso
  FreSolucionOrdenServicio,       //Frame solucion
  FreHistoriaOrdenServicio,       //Frame Historia
  FreFuenteReclamoOrdenServicio,  //Frame Fuente Reclamo
  FreNumeroOrdenServicio,         //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,      //Frame Respuesta Orden Servicio 
  ImagenesTransitos,              //Muestra la Ventana con la imagen
  FrmTransitosReclamadosAcciones, //Acciones que se hicieron a los transitos al cerrar el reclamo
  OrdenesServicio,                //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Menus,
  FreConcesionariaReclamoOrdenServicio{, FreSubTipoReclamoOrdenServicio};       // _PAN_

type
  TFormReclamoTransito = class(TForm)
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
    txtDetalle: TMemo;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio;
    Label3: TLabel;
    DataSource: TDataSource;
    DBLTransitos: TDBListEx;
    lnCheck: TImageList;
    spObtenerOrdenServicio: TADOStoredProc;
    spObtenerTransitosReclamo: TADOStoredProc;
    PopupResolver: TPopupMenu;
    mnu_Resolver: TMenuItem;
    Mnu_Pendiente: TMenuItem;
    Mnu_Aceptado: TMenuItem;
    Mnu_Denegado: TMenuItem;
    Mnu_Comun: TMenuItem;
    PAbajo: TPanel;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    ilCamara: TImageList;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
{    FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;}     // _PAN_
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBLTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure DBLTransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
    procedure DBLTransitosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    procedure Mnu_ComunClick(Sender: TObject);
  private
    { Private declarations }
    FTipoOrdenServicio : Integer;
    FCodigoOrdenServicio : Integer;
    FListaTransitos : TStringList;
    FEditando : Boolean; //Indica si estoy editando o no
    Function  GetCodigoOrdenServicio : Integer;
    Function  ListaTransitosInclude(Lista : TStringList; Valor : String) : Boolean;
    Function  ListaTransitosExclude(Lista : TStringList; Valor : String) : Boolean;
    Function  ListaTransitosIn(Lista : TStringList ; Valor : String) : Boolean;
    Function  ListaTransitosEmpresaIn(Valor : String; Var Resolucion : Integer) : Boolean;
    Function  ValidarEstadoTransitos : Boolean;
  public
    { Public declarations }
    Function Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer; ListaTransitos : TStringList ) : Boolean;
    Function InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio : Integer): Boolean;
    Property CodigoOrdenServicio: Integer Read GetCodigoOrdenServicio;
  end;

var
  FormReclamoTransito : TFormReclamoTransito;
  FListaTransitosCliente : TStringList; //Guarda los transitos que el usuario no rechaza
  FListaTransitosEmpresa : TStringList; //Guarda la resolucion de la empresa sobre cada transito
  FListaTransitosSeleccionados : TStringList; //Guarda la resolucion de la empresa sobre cada transito

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransito.Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer; ListaTransitos: TStringList ) : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerDatosTransitos
      Author:  lgisuk
      Date Created: 06/02/2006
      Description:  Cargo la grilla de reclamos
      Parameters:
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerDatosTransitos(Transitos : TStringList) : Boolean;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerEnumeracion
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Obtengo la Enumeraci�n
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        function ObtenerEnumeracion(Lista : TStringList) : String;
        var
            Enum: AnsiString;
            i: Integer;
        begin
            Enum := '';
            for i:= 0 to Lista.Count - 1 do begin
                //Enum := Enum + Lista.Strings[i] + ',';
                Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
            end;
            Delete(Enum, Length(Enum), 1);
            Result := Enum;
        end;

    begin
        with SPObtenerTransitosReclamo do begin
            Close;
            Parameters.ParamByName('@CodigoOrdenServicio').Value := NULL;
            Parameters.ParamByName('@Enumeracion').Value := ObtenerEnumeracion(ListaTransitos);
            Open;
        end;
        Result := True;
    end;

Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo Transito';
    MSG_ERROR = 'Error';
Const
    STR_RECLAMO = 'Reclamo de Transito - ';
Var
	  Sz: TSize;
begin
    FTipoOrdenServicio := TipoOrdenServicio;
    try
        //si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaTransitos <> nil then begin
            FListaTransitos := ListaTransitos;
            //cargo la grilla con los transitos
            ObtenerDatosTransitos(ListaTransitos);
            //obtengo el codigo de convenio
            CodigoConvenio:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCodigoConvenioTransito (' + spObtenerTransitosReclamo.FieldByName('NumCorrCa').AsString + ')');
            //la empresa aun no resolvio nada es la primera vez
            FListaTransitosEmpresa.Clear;
        end else begin
            //Creo la lista la voy a cargar una consulta
            FListaTransitos := TStringList.Create;
        end;

        //Centro el form
        CenterForm(Self);
        //ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);
        //titulo
        Caption := STR_RECLAMO + QueryGetValue(DMConnections.BaseCAC, Format(
                   'SELECT dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
                   [TipoOrdenServicio]));
        PageControl.ActivePageIndex := 0;
        ActiveControl := txtDetalle;
        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(10) and
                                FrameSolucionOrdenServicio1.Inicializar and
                                        FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 24-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar{ and        // _PAN_
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar};             // _PAN_
                                        //---------------------------------------------------------------        

        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    FrameNumeroOrdenServicio1.Visible := False;
    //No estoy Editando
    FEditando := False;
    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso := ObtenerFechaComprometidaOS(now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, FrameCompromisoOrdenServicio1.Prioridad);
    AddOSForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:  DDeMarco
  Date Created:
  Description: Modifico Reclamo que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoTransito.InicializarEditando(CodigoOrdenServicio: Integer): Boolean;

   {-----------------------------------------------------------------------------
      Function Name: ReadOnly
      Author:  lgisuk
      Date Created: 06/02/2006
      Description:  Cambia al formulario al modo solo lectura
      Parameters:
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Procedure ReadOnly;
    Const
        STR_EXIT = 'Salir';
    begin
        dbltransitos.PopupMenu := Nil;
        //Comunes
        FrameContactoReclamo1.Deshabilitar;
        FrameCompromisoOrdenServicio1.Enabled := False;
        FrameSolucionordenServicio1.Enabled := False;
        FrameRespuestaOrdenServicio1.Deshabilitar;
        TxtDetalle.ReadOnly := True;
        TxtDetalleSolucion.ReadOnly := True;
        CKRetenerOrden.Visible := False;
        BtnCancelar.Caption := STR_EXIT;
        AceptarBtn.Visible := False;
        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra ----------------------------------------------------------
    	FrameConcesionariaReclamoOrdenServicio1.Enabled:=False;
{        FrameSubTipoReclamoOrdenServicio1.Enabled:=False;}                     // _PAN_
    	//FinRev.1-------------------------------------------------------------------------------------------------
    end;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerDatosTransitos
      Author:  lgisuk
      Date Created: 06/02/2006
      Description:  Obtiene los datos de los transitos
      Parameters:
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerDatosTransitos (CodigoOrdenServicio : Integer) : Boolean;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerTransitos
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Obtiene los transitos que pertencen al reclamo
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        Function ObtenerTransitos (CodigoOrdenServicio : Integer) : Boolean;
        begin
           FListaTransitos.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              first;
              while not eof do begin
                  FListaTransitos.Add(FieldByName('NumCorrCa').AsString);
                  next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerTransitosAceptadosCliente
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Arma una lista de los transitos aceptados por el cliente
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        Function ObtenerTransitosAceptadosCliente(CodigoOrdenServicio : Integer) : Boolean;
        var
            NumCorrCa : String;
            Rechaza : Boolean;
        begin
           FListaTransitosCliente.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              First;
              while not eof do begin
                  NumCorrCa := FieldByName('NumCorrCa').AsString;
                  Rechaza := QueryGetValue(DMConnections.BaseCAC,'select dbo.ObtenerRechazaOSTransito('+ IntToStr(CodigoOrdenServicio) + ',' + NumCorrCa + ')'  ) = 'True';
                  if not Rechaza then ListaTransitosInclude(FListaTransitosCliente, NumCorrCa);
                  Next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerResolucionEmpresa
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Arma una lista de los transitos aceptados por la empresa
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        Function ObtenerResolucionEmpresa(CodigoOrdenServicio : Integer) : Boolean;
        var
            NumCorrCA : String;
            CodigoResolucion : String;
            Bloque : String;
        begin
           FListaTransitosEmpresa.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              First;
              while not Eof do begin
                  NumCorrCA := FieldByName('NumCorrCa').AsString;
                  CodigoResolucion := FieldByName('CodigoResolucionReclamoTransito').AsString;
                  CodigoResolucion := IIF( CodigoResolucion = '' , '1' , CodigoResolucion);
                  Bloque := NumCorrCa + ';' + CodigoResolucion;
                  if CodigoResolucion <> '1' then ListaTransitosInclude(FListaTransitosEmpresa,Bloque);
                  next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

    begin

        with SpObtenerTransitosReclamo do begin
            Close;
            Parameters.ParamByName('@CodigoOrdenServicio').value := CodigoOrdenServicio;
            Parameters.ParamByName('@Enumeracion').value := NULL;
            Open;
        end;

        ObtenerTransitos (CodigoOrdenServicio);
        ObtenerTransitosAceptadosCliente (CodigoOrdenServicio);
        ObtenerResolucionEmpresa (CodigoOrdenServicio);

        Result := True;
    end;

var
    Estado : String;
begin
    FCodigoOrdenServicio := CodigoOrdenServicio;
    spObtenerOrdenServicio.Parameters.Refresh;
    spObtenerOrdenServicio.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    Result := OpenTables([spObtenerOrdenServicio]);
    if not Result then Exit;

    // Cargamos los datos
    Result := Inicializar(spObtenerOrdenServicio.FieldByName('TipoOrdenServicio').AsInteger, 0, nil);

    txtDetalle.Text := spObtenerOrdenServicio.FieldByName('ObservacionesSolicitante').AsString;
    txtDetalleSolucion.Text := spObtenerOrdenServicio.FieldByName('ObservacionesEjecutante').AsString;

    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra --------------------------------------------------------------
    FrameConcesionariaReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
{    FrameSubTipoReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);}    // _PAN_
    //FinRev.1------------------------------------------------------------------------------------------------------

    //cargo la grilla con los transitos objetados
    ObtenerDatosTransitos(CodigoOrdenServicio);

    //obtengo la historia para esa orden de servicio
    FrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio);

    //obtengo la fuente del reclamo
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //Obtengo el Estado de la OS para saber si permito editar o read only
    Estado := spObtenerOrdenServicio.FieldByName('Estado').AsString;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;

    // Listo
    spObtenerOrdenServicio.Close;

    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(IntToStr(CodigoOrdenServicio));
    //por Default no retengo la orden de servicio
    CKRetenerOrden.Checked := False;
    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;
    //Estoy Editando
    FEditando := True;
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:  DDeMarco
  Date Created:
  Description: Soluciono un Reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoTransito.InicializarSolucionando(CodigoOrdenServicio: Integer): Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.ActivePageIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
function TFormReclamoTransito.GetCodigoOrdenServicio: integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosInclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: agrego un valor a la lista de transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransito.ListaTransitosInclude(Lista : TStringList; Valor : String) : Boolean;
begin
    Result := False;
    try
        if not ListaTransitosIn(Lista, Valor) then Lista.Add(Valor);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosExclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: quito un valor a la lista de Transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransito.ListaTransitosExclude(Lista:tstringlist;valor:string):boolean;
var
    Index:integer;
begin
    Result := False;
    try
        Index := Lista.IndexOf(valor);
        if Index <> -1 then Lista.Delete(index);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosIn
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: Devuelve si un valor esta en la lista transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransito.ListaTransitosIn(Lista:tstringlist;valor:string):boolean;
begin
    Result := not (Lista.IndexOf(valor) = -1);
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosEmpresaIn
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: busca un transito, se fija si esta y trae su resolucion
  Parameters: valor:string; var Resolucion:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransito.ListaTransitosEmpresaIn(valor:string; var Resolucion : Integer) : Boolean;
var
    I : Integer;
begin
    Result := False;
    Resolucion := 1;
    I := 0;
    while I <= FListaTransitosEmpresa.Count-1 do
    begin
        if ParseParamByNumber(FListaTransitosEmpresa.Strings[i],1,';') = Valor then begin
            Resolucion := StrToInt(ParseParamByNumber(FListaTransitosEmpresa.Strings[i],2,';'));
            I := FListaTransitosEmpresa.Count;
            Result := True;
        end;
        inc(i);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosDrawText
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  Author:    lgisuk
  Date Created: 04/12/2006
  Description: Ahora se permite la selecci�n r�pida de m�ltiples tr�nsitos
                para luego aceptar el reclamo.
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.DBLTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 29/06/2005
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer):boolean;
    var
        bmp: TBitMap;
    begin
        bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            bmp.Free;
        end;
    end;

var
    I : Integer;
    NumCorrCA : String;
    Resolucion : Integer;
begin

    //FechaHora
    if (Column.DisplayName = 'Fecha / Hora') then begin
        if Text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerTransitosReclamo.FieldByName('FechaHora').AsDateTime);
    end;

    //muestra si el cliente rechazo un transito o no
  	if (Column.DisplayName = 'Usuario Rechaza') then begin
        DefaultDraw := False;
        //verifica si esta seleccionado o no
        I := iif(ListaTransitosIn(FListaTransitosCliente,spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring), 0, 1);
        //Dibujo el icono correspondiente
        DibujarBitmap(dblTransitos, LnCheck, I);
  	end;

    //Muestra la resolucion de la empresa con respecto al transito
  	if (Column.DisplayName = 'Resoluci�n') then begin
        //Obtengo el numero de transito
        NumCorrCA := spObtenerTransitosReclamo.fieldbyname('NumCorrCa').asstring;
        //Busco el transito. Si esta me devuelve su resolucion actual.
        if not ListaTransitosEmpresaIn(NumCorrCa, Resolucion) then begin
            Text := 'Pendiente';
        end else begin
            if resolucion = 1 then text := 'Pendiente';
            if resolucion = 2 then text := 'Aceptado';
            if resolucion = 3 then text := 'Denegado';
        end;
  	end;

    //si el transito tiene imagen le crea un link para que se pueda acceder a verla
    if (Column.DisplayName = 'Im�gen') then begin
        DefaultDraw := False;
        //Muestro el dibujito de una camara cuando hay una foto del transito
        I := iif(spObtenerTransitosReclamo.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos, ilCamara, I);
    end;

    //muestra si el cliente rechazo un transito o no
  	if (Column.DisplayName = 'Seleccionado') then begin  //Agregado en Revision 1
        DefaultDraw := False;
        //verifica si esta seleccionado o no
        I := iif(ListaTransitosIn(FListaTransitosSeleccionados,spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring), 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dblTransitos, LnCheck, I);
        //Text := IIF(I = 0, 'SI', 'NO');
  	end;

end;


{-----------------------------------------------------------------------------
  Function Name: DBLTransitosCheckLink
  Author:    lgisuk
  Date Created: 18/05/2005
  Description: habilito el link solo si hay imagen
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.DBLTransitosCheckLink(Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    //permito hacer click en el link o no
    if (Column.DisplayName = 'Im�gen') then begin
        Column.IsLink := (spObtenerTransitosReclamo['RegistrationAccessibility'] > 0);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosLinkClick
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: permito rechazar o aceptar un transito
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  Author:    lgisuk
  Date Created: 04/12/2006
  Description: Ahora se permite la selecci�n r�pida de m�ltiples tr�nsitos
                para luego aceptar el reclamo.
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.DBLTransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Resourcestring
    MSG_TITLE   = 'Sistema de Reclamos';
    MSG_CONFIRM = 'Desea cambiar la opci�n que eligio el usuario?';
Var
  	Numcorrca: string;
    RealizarCambio:boolean;
    Estado: string;
begin

    //Mostrar Imagen del Transito
    if (Column.DisplayName = 'Im�gen') then begin
        //Si hay imagen
        if spObtenerTransitosReclamo.FieldByName('RegistrationAccessibility').AsInteger > 0 then begin
            //La muestra
            //MostrarVentanaImagen(Self,spObtenerTransitosReclamo['NumCorrCA'], spObtenerTransitosReclamo['FechaHora']);    // SS_1091_CQU_20130516
            MostrarVentanaImagen(Self,                                                                                      // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['NumCorrCA'],                                                                     // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['FechaHora'],                                                                     // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['EsItaliano']);                                                                   // SS_1091_CQU_20130516
        end;
    end;

    if (Column.DisplayName = 'Usuario Rechaza') then begin
        //Selecciono/desselecciono transito  cliente
        if Column.IsLink = false then Exit;
    end;

    //Si la OS ya esta cerrada evito que hagan click en el link
    Estado :=FrameSolucionOrdenServicio1.cbEstado.value;
    if ((Estado = 'C') or (Estado = 'T')) then Exit;

    if (Column.DisplayName = 'Usuario Rechaza') then begin
        //no realizar cambio hasta  confirmacion operador
        RealizarCambio:= False;
        // Si es un reclamo nuevo
        if feditando = false then begin
            RealizarCambio:= true;
        end;
        //si se esta modificando
        if fEditando = true then begin
           //confirmo si desea cambiar la opcion que eligio el usuario
           if MsgBox(MSG_CONFIRM, MSG_TITLE, 1) = 1 then begin
                RealizarCambio:= true;
           end;
        end;
        //realizo el cambio en la opcion del cliente
        if realizarcambio then begin
            NumCorrCa := spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring;
            if ListaTransitosin(FListaTransitosCliente,Numcorrca) then ListaTransitosExclude(FListaTransitosCliente,Numcorrca)
            else ListaTransitosInclude(FListaTransitosCliente,Numcorrca);
            Sender.Invalidate;
       end;
    end;

    if (Column.DisplayName = 'Seleccionado') then begin       //Agregado en Revision 1
            NumCorrCa := spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring;
            if ListaTransitosin(FListaTransitosSeleccionados,Numcorrca) then ListaTransitosExclude(FListaTransitosSeleccionados,Numcorrca)
            else ListaTransitosInclude(FListaTransitosSeleccionados,Numcorrca);
            Sender.Invalidate;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosContextPopup
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: habilito el menu contextual solo si hay registros
  Parameters: Sender: TObject;MousePos: TPoint; var Handled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.DBLTransitosContextPopup(Sender: TObject;MousePos: TPoint; var Handled: Boolean);
begin
  	if not spObtenerTransitosReclamo.IsEmpty then begin
        Handled := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Mnu_ComunClick
  Author:    lgisuk
  Date Created: 19/05/2005
  Description:  Permite cambiar la Resolucion de la empresa
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  Author:    lgisuk
  Date Created: 04/12/2006
  Description: Ahora se permite la selecci�n r�pida de m�ltiples tr�nsitos
                para luego aceptar el reclamo.
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.Mnu_ComunClick(Sender: TObject);
var
  	NumCorrCa: String;
    CodigoResolucionActual : String;
    CodigoResolucionAnterior : Integer;
begin
    //Obtengo codigo de Resolucion Actual
    CodigoResolucionActual := IntToStr((Sender as TMenuItem).Tag);

    //Recorro los transitos
    with spObtenerTransitosReclamo do begin
        DisableControls;
        first;
        while not eof do begin

            //si estan entre los seleccionados
            if ListaTransitosIn(FListaTransitosSeleccionados, fieldbyname('NumCorrCa').asstring) then begin

    //Obtengo numero de transito
    NumCorrCa := spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring;
    //Obtengo Resolucion Anterior porque la necesito para localizar el registro
    ListaTransitosEmpresaIn(Numcorrca, CodigoResolucionAnterior);
    //Elimino Resolucion Anterior porque solo puede existir una
    ListaTransitosExclude(FListaTransitosEmpresa, Numcorrca + ';' + IntToStr(CodigoResolucionAnterior));
    //Inserto Nueva Resolucion
    ListaTransitosInclude(FListaTransitosEmpresa, Numcorrca + ';' + CodigoResolucionActual);

            end;

            Next;
        end;
        EnableControls;
    end;

    //Borro la lista de seleccionados porque ya realice la accion
    FListaTransitosSeleccionados.Clear;

    //Refresco la vista
    dbltransitos.Invalidate;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarEstadoTransitos
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: valido que todos los transitos incluidos en el reclamo
               hallan sido analizados y resueltos por la empresa
               antes de dar por finalizado el reclamo
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransito.ValidarEstadoTransitos : Boolean;
var CodigoResolucion : Integer;
begin
    Result := True;
    //si el sp no esta abierto salgo
    if spObtenerTransitosReclamo.Active = False then Exit;
    with spObtenerTransitosReclamo do begin
        DisableControls;
        First;
        while not eof do begin
            ListaTransitosEmpresaIn(fieldbyname('NumCorrCa').asstring, CodigoResolucion);
            //basta que uno este sin resolver no permito Guardar
            if CodigoResolucion = 1 then begin
                Result := False;
            end;
            Next;
        end;
        EnableControls;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: registra el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  Valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar : Boolean;
    resourcestring
        MSG_ERROR_DET       = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE    = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA     = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                              'debe ser menor a la Fecha Comprometida con el Cliente';
        MSG_ERROR_TRANSITOS = 'Todos los Tr�nsitos deben estar resueltos para cerrar el reclamo';
    begin
        Result := False;

        //Validamos fuente de reclamo
        if FEditando = False then begin
            if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            Result := True;
            Exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = false then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //valido que todos los transitos esten resueltos
        if (FrameSolucionOrdenServicio1.Estado = 'T') then begin
            if not ValidarEstadoTransitos then begin
                PageControl.ActivePageIndex := 0;
                MsgBoxBalloon(MSG_ERROR_TRANSITOS, STR_ERROR, MB_ICONSTOP, dbltransitos);
                dbltransitos.SetFocus;
                Exit;
            end;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            txtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;


        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802

        Result := True;
    end;

    {-----------------------------------------------------------------------------
      Function Name: InformarTransitosReclamadosAcciones
      Author:    lgisuk
      Date Created: 05/07/2005
      Description: Informo los transitos reclamados
      Parameters: ListaTransitos:tstringlist
      Return Value: None
    -----------------------------------------------------------------------------}
    Procedure InformarTransitosReclamadosAcciones(ListaTransitos : TStringList);
    Var
        F :  TFormTransitosReclamadosAcciones;
    begin
         Application.CreateForm(TFormTransitosReclamadosAcciones, F);
         if F.Inicializar(ListaTransitos) then F.ShowModal;
    end;
resourcestring
    MSG_PROCESS_ERROR = 'Error al procesar los transitos reclamados';
    MSG_ERROR = 'Error';
Var
    OS : Integer;
    OK : Boolean;
    CodigoResolucion : Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    //Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra----------------------------------
    DMConnections.BaseCAC.BeginTrans;
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.Text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaBefore,       // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaAfter,        // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteBefore,  // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteAfter    // _PAN_
                                   {FrameSubTipoReclamoOrdenServicio1}                              // _PAN_
                                   );
    //Rev.1---------------------------------------------------------------------------

    if OS <= 0 then begin
        //Cancelo la transaccion y salgo
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    //Especifico de Transitos
    try
        OK := True;
        with spObtenerTransitosReclamo do begin
            DisableControls;
            first;
            while not eof do begin
                ListaTransitosEmpresaIn(fieldbyname('NumCorrCa').asstring, CodigoResolucion);
                //Guardo cada transito en la bd
                if GuardarOrdenServicioTransito(OS, FieldByName('NumCorrCa').AsString, not (ListaTransitosIn(FListaTransitosCliente,FieldByName('NumCorrCa').AsString)), CodigoResolucion ) = False then Ok := False;
                next;
            end;
            EnableControls;
        end;
        if not OK then begin
            //Cancelo la transaccion y salgo
            DMConnections.BaseCAC.RollbackTrans;
            Exit;
        end;
    except
        on E : Exception do begin
            //Cancelo la transaccion, Informo la situacion al operador y salgo
            DMConnections.BaseCAC.RollbackTrans;
            MsgBoxErr(MSG_PROCESS_ERROR, E.message, MSG_ERROR, MB_ICONSTOP);
            Exit;
        end;
    end;

    //En el caso que se acepte el reclamo del cliente
    //se pone el transito en estado no facturable
    if FrameSolucionOrdenServicio1.Estado = 'T' then begin
        //Actualizo el estado de los transitos reclamados
        if not ActualizarTransitosReclamosAceptados(OS) then begin
            //Cancelo la transaccion y salgo
            DMConnections.BaseCAC.RollbackTrans;
            Exit;
        end;
    end;

    //Listo
    DMConnections.BaseCAC.CommitTrans;

    //Muestro un cartel con las acciones realizadas sobre
    //los transitos reclamados
    if FrameSolucionOrdenServicio1.Estado = 'T' then begin
        InformarTransitosReclamadosAcciones(FListaTransitos);
    end;

    //Si es un reclamo nuevo
    if FEditando = False then begin
        //Informo al operador el numero de Reclamo que se genero
        InformarNumeroReclamo(inttostr(OS));
        DesbloquearOrdenServicio(OS);
    end;

    Close;

end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 12/04/2005
  Description:  remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.FormDestroy(Sender: TObject);
begin
    RemoveOSform(self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: desbloqueo la orden de servicio antes de salir
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransito.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //Si se esta modificando un reclamo
    if feditando = true then begin
        //Verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = false then begin

            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //Borro la lista
    FListaTransitosCliente.Clear;
    FListaTransitosSeleccionados.Clear;
    //Lo libero de memoria
    Action := caFree;
end;

initialization
    FListaTransitosCliente := TStringList.Create;
    FListaTransitosEmpresa := TStringList.Create;
    FListaTransitosSeleccionados := TStringList.Create;
finalization
    FreeAndNil(FListaTransitosCliente);
    FreeAndNil(FListaTransitosEmpresa);
    FreeAndNil(FListaTransitosSeleccionados);
end.
