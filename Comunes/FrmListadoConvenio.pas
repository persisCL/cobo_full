{********************************** Unit Header ********************************
File Name : FrmListadoConvenio.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision : 1
    Author : pdominguez
    Date : 06/08/2009
    Description : SS 733
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            btnImprimirClick


    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
*******************************************************************************}
unit FrmListadoConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx,UtilProc,DmConnection,Peatypes,
  DB, ADODB, DmiCtrls, Validate, DateEdit,peaprocs,util,UtilDB,RStrings,
  VariantComboBox, frmVisorDocumentos, Convenios, frmImprimirConvenio,
  ConstParametrosGenerales, DBClient;

type
    TOnClickRegSeleccionadoConsulta = procedure (NumeroConvenio: String; CodigoConvenio: Integer) of object;
    TOnClickRegSeleccionadoModif = procedure (NumeroConvenio: String; CodigoConvenio: Integer; Fuente: Integer; Administrador: Boolean) of object;

type
  TFormListadoConvenios = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    dbl_Convenio: TDBListEx;
    ObtenerListadoConvenio: TADOStoredProc;
    DS_ListadoConvenio: TDataSource;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    txt_CantVehiculos: TNumericEdit;
    Label5: TLabel;
    cb_Personeria: TComboBox;
    Label6: TLabel;
    Label7: TLabel;
    txt_Apellido: TEdit;
    txt_rut: TEdit;
    Label8: TLabel;
    txt_Patente: TEdit;
    Label9: TLabel;
    cb_EstadoConvenio: TComboBox;
    txt_NumeroConvenio: TEdit;
    lbl_Mostar: TLabel;
    Label10: TLabel;
    cb_Concesionaria: TComboBox;
    txt_Top: TNumericEdit;
    Label11: TLabel;
    Notebook: TNotebook;
    lbl_PagoAutomatico: TLabel;
    cb_PagoAutomatico: TVariantComboBox;
    Label12: TLabel;
    txt_Tag: TEdit;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    btn_Ver: TButton;
    btn_modificar: TButton;
    btn_VerDocumento: TButton;
    btnImprimir: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure ObtenerListadoConvenioAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure dbl_SolicitudColumns0HeaderClick(Sender: TObject);
    procedure btn_VerClick(Sender: TObject);
    procedure dbl_ConvenioDblClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure txt_rutKeyPress(Sender: TObject; var Key: Char);
    procedure txt_ApellidoKeyPress(Sender: TObject; var Key: Char);
    procedure btn_modificarClick(Sender: TObject);
    procedure ObtenerListadoConvenioAfterScroll(DataSet: TDataSet);
    procedure txt_TopExit(Sender: TObject);
    procedure btn_VerDocumentoClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
    SoloConsulta: boolean;
    FPuntoEntrega: integer;
    FPuntoVenta: integer;
    FFuente: integer;
    FAdministrador:Boolean;
    FOnClickRegSeleccionadoConsulta: TOnClickRegSeleccionadoConsulta;
    FOnClickRegSeleccionadoModif: TOnClickRegSeleccionadoModif;
    FCodigoConcesionariaNativa: Integer;					//SS_1147_MCA_20140408
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function BuscarColumna(Nombre:String;IniciarDesde:integer):Integer;
    procedure CargarConcesionarias(Conn: TADOConnection; Combo: TComboBox; CodigoConcesionarias:Integer = 0; TieneItemNinguno: boolean = false);
    function GetOnClickRegSeleccionadoModif: TOnClickRegSeleccionadoModif;
    function GetOnClickRegSeleccionadoConsulta: TOnClickRegSeleccionadoConsulta;
    procedure SetOnClickRegSeleccionadoConsulta(const Value: TOnClickRegSeleccionadoConsulta);
    procedure SetOnClickRegSeleccionadoModif(const Value: TOnClickRegSeleccionadoModif);

  public
    { Public declarations }
    property PuntoEntrega: integer read FPuntoEntrega;
    property PuntoVenta: integer read FPuntoVenta;
    property Fuente: integer read FFuente;
    property Administrador:Boolean read FAdministrador;

    function Inicializar(aCaption: TCaption; MDIChild: Boolean; EsSoloConsulta: boolean; PuntoEntrega: integer = -1; PuntoVenta: integer = -1; Fuente: integer = FUENTE_CALLCENTER; Administrador:Boolean = False): Boolean;
    procedure Refrescar;
    property OnClickRegSeleccionadoConsulta: TOnClickRegSeleccionadoConsulta read GetOnClickRegSeleccionadoConsulta write SetOnClickRegSeleccionadoConsulta;
    property OnClickRegSeleccionadoModif: TOnClickRegSeleccionadoModif read GetOnClickRegSeleccionadoModif write SetOnClickRegSeleccionadoModif;
  end;

  TFormListadoConveniosAdministrador = class(TFormListadoConvenios)
      public function Inicializar(aCaption: TCaption; MDIChild: Boolean; EsSoloConsulta: boolean; PuntoEntrega: integer = -1; PuntoVenta: integer = -1; Fuente: integer = FUENTE_CALLCENTER): Boolean;
  end;

  TFormListadoConveniosOperador = class(TFormListadoConvenios)
      public function Inicializar(aCaption: TCaption; MDIChild: Boolean; EsSoloConsulta: boolean; PuntoEntrega: integer = -1; PuntoVenta: integer = -1; Fuente: integer = FUENTE_CALLCENTER): Boolean;
  end;

  TFormListadoConveniosConsulta = Class(TFormListadoConvenios)
    public function Inicializar(aCaption: TCaption; MDIChild: Boolean): Boolean;
  end;

var
  FormListadoConvenios: TFormListadoConvenios;

implementation

{$R *.dfm}

procedure TFormListadoConvenios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

function TFormListadoConvenios.Inicializar(aCaption: TCaption; MDIChild: Boolean; EsSoloConsulta: boolean; PuntoEntrega: integer = -1; PuntoVenta: integer = -1; Fuente: integer = FUENTE_CALLCENTER; Administrador:Boolean = False): Boolean;
resourcestring
    MSG_DOBLE_CLICK_SOLICITUD_CONSULTA = '<Doble click para ver el detalle del convenio>';
    STR_LISTADO_CONVENIO = 'Listado de Convenio';
Var S: TSize;
begin
    try

        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;

        Update;
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;

        Caption := Caption + ' - ' + aCaption;
        SoloConsulta := EsSoloConsulta;
        dbl_Convenio.Hint := MSG_DOBLE_CLICK_SOLICITUD_CONSULTA;

        FPuntoEntrega:= PuntoEntrega;
        FPuntoVenta:= PuntoVenta;
        FFuente := Fuente;
        FAdministrador := Administrador;

        btn_Limpiar.Click;
        FCodigoConcesionariaNativa:= ObtenerCodigoConcesionariaNativa;							//SS_1147_MCA_20140408
        //CargarConcesionarias(DMConnections.BaseCAC, cb_Concesionaria, CODIGO_CN, true);       //SS_1147_MCA_20140408
        CargarConcesionarias(DMConnections.BaseCAC, cb_Concesionaria, FCodigoConcesionariaNativa, true);         //SS_1147_MCA_20140408
//        CargarMediosPagoAutomatico(cb_PagoAutomatico,-1, False, True);

        //*** 27-04-2004: Guillermo. No mostrar solicitudes al inicio y tampoco la fecha de hoy en el filtro desde
        //txt_FechaDesde.Date:=Now;
        txt_FechaHasta.Date:=Now;
        {with ObtenerSolicitudContacto do begin
            close;
            Parameters.ParamByName('@FechaHoraCreacion').Value:=now;
            Parameters.ParamByName('@FechaHoraFin').Value:=now;
            Parameters.ParamByName('@OrderBy').Value:=DarOrderBy;
            open;
        end;
        }

        Notebook.PageIndex := Ord(not EsSoloConsulta);
        Result := True;
    except
        on e: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_INICIALIZACION,[STR_LISTADO_CONVENIO]),e.Message, Caption, MB_ICONSTOP);
    		Result := false;
        end;

    end;
end;

procedure TFormListadoConvenios.BtnSalirClick(Sender: TObject);
begin
    close;
end;

{******************************** Function Header ******************************
Function Name: Btn_FiltrarClick
Author :
Date Created :
Description :
Parameters :
Return Value :

Revision 1:
    Auhtor: Fsandi
    Date: 01/12/2006
    Description: Filtra los convenios recibiendo los parametros necesarios para una
    busqueda, se corrigieron los errores producidos por Timeout del SP

*******************************************************************************}
procedure TFormListadoConvenios.btn_FiltrarClick(Sender: TObject);
    function BuscarConvenios(var DescriError: AnsiString): Boolean;

      function DarEstadoConvenio:Integer;
      begin
          result:=strtoint(iif(trim(cb_EstadoConvenio.Text)='','0',StrRight(cb_EstadoConvenio.Text,10)));
      end;

      function DarConcesionarias:Integer;
      begin
          result:=strtoint(iif(trim(StrRight(cb_Concesionaria.Text,3))='','0',StrRight(cb_Concesionaria.Text,3)));
      end;

      function DarPersoneria:String;
      begin
          result:=StrRight(iif(trim(cb_Personeria.Text) = '','0',trim(cb_Personeria.Text)),1);
      end;


      function DarMedioPago:Variant;
      begin
          result:=cb_PagoAutomatico.Value;
      end;

    begin
        Result := False;
        ObtenerListadoConvenio.Close;
        DescriError := EmptyStr;
        try
            ObtenerListadoConvenio.Parameters.ParamByName('@FechaHoraDesde').Value := iif(txt_FechaDesde.Date=nulldate,null,txt_FechaDesde.Date);
            ObtenerListadoConvenio.Parameters.ParamByName('@FechaHoraHasta').Value := iif(txt_FechaHasta.Date=nulldate,null,txt_FechaHasta.Date);
            ObtenerListadoConvenio.Parameters.ParamByName('@CodigoEstadoConvenio').Value := iif(DarEstadoConvenio=0,NULL,DarEstadoConvenio);
            ObtenerListadoConvenio.Parameters.ParamByName('@Concesionarias').Value := iif(DarConcesionarias=0,NULL,DarConcesionarias);
            ObtenerListadoConvenio.Parameters.ParamByName('@NumeroConvenio').Value := iif(Trim(txt_NumeroConvenio.Text)='',null,Trim(txt_NumeroConvenio.Text));
            ObtenerListadoConvenio.Parameters.ParamByName('@CantidadVehiculos').Value := iif(Trim(txt_CantVehiculos.Text)='',null,txt_CantVehiculos.ValueInt);
            ObtenerListadoConvenio.Parameters.ParamByName('@Personeria').Value := iif(DarPersoneria='0',null,DarPersoneria);
            ObtenerListadoConvenio.Parameters.ParamByName('@RUT').Value := iif(Trim(txt_rut.Text)='',NULL,trim(txt_rut.Text));
            ObtenerListadoConvenio.Parameters.ParamByName('@Apellido').Value := iif(Trim(txt_Apellido.Text)='',NULL,Trim(txt_Apellido.Text));
            ObtenerListadoConvenio.Parameters.ParamByName('@Patente').Value := iif(Trim(txt_Patente.Text)='',NULL,Trim(txt_Patente.Text));
            ObtenerListadoConvenio.Parameters.ParamByName('@TOP').Value := txt_Top.Text;
            ObtenerListadoConvenio.Parameters.ParamByName('@TipoMedioPagoAutomatico').Value := DarMedioPago;
            ObtenerListadoConvenio.Parameters.ParamByName('@NroEtiqueta').Value := iif(Trim(txt_Tag.Text) = '', Null, Trim(txt_Tag.Text));
            ObtenerListadoConvenio.Open;
            Result := True;
        except
            on e: Exception do begin
                DescriError := e.Message;
            end;
        end;
    end;
var
    Error: String;
ResourceString
    MSG_RESULT_SEARCH = 'No se ha encontrado registros para la b�squeda realizada';
    MSG_ATENTION      = 'Atenci�n';
    MSG_ERRORBUSQUEDA = 'No se pudo completar el Filtrado de Convenios';
    STR_CONVENIOFILTRADO = 'Listado de Convenios';

    function DarTipoSolicitud:Integer;
    begin
        result:=strtoint(iif(trim(cb_EstadoConvenio.Text)='','0',StrRight(cb_EstadoConvenio.Text,10)));
    end;

begin
    if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA,MSG_CAPTION_LISTSOLICITUD,MB_ICONSTOP,txt_FechaDesde);
        exit;
    end;

    if txt_Top.Value >= 10000 then begin
        MsgBoxBalloon(MSG_VALIDAR_TOP_LISTA_CONVENIO,MSG_CAPTION_LISTSOLICITUD,MB_ICONSTOP,txt_Top);
        exit;
    end;
    if not BuscarConvenios(Error) then begin
        MsgBoxErr(Format(MSG_ERRORBUSQUEDA,[STR_CONVENIOFILTRADO]), Error, MSG_CAPTION_LISTSOLICITUD, MB_ICONSTOP);
    end else begin
        if ObtenerListadoConvenio.IsEmpty then begin
            MsgBox(MSG_RESULT_SEARCH, MSG_ATENTION, MB_ICONINFORMATION);
        end else begin
            dbl_Convenio.Columns[2].Sorting := csDescending;
            dbl_Convenio.Columns[2].OnHeaderClick(dbl_Convenio.Columns[2]);
        end;
    end;
end;

procedure TFormListadoConvenios.Refrescar;
var
    Book: TBookmark;
begin
    Book := ObtenerListadoConvenio.GetBookmark;
    btn_Filtrar.click;

    if ObtenerListadoConvenio.RecordCount > 0 then
        ObtenerListadoConvenio.GotoBookmark(Book);

    ObtenerListadoConvenio.FreeBookmark(Book);

end;

procedure TFormListadoConvenios.ObtenerListadoConvenioAfterOpen(
  DataSet: TDataSet);
resourcestring
    STR_CANT_CONVENIOS = 'Cantidad de Convenios: %d.';
var
    CantRegistros: Integer;
begin

    CantRegistros:=0;
    if ObtenerListadoConvenio.Active then begin
        CantRegistros := ObtenerListadoConvenio.RecordCount;
        ObtenerListadoConvenio.First;
    end;
    lbl_Mostar.Caption := Format(STR_CANT_CONVENIOS ,[CantRegistros]);

//    btnImprimir.Enabled := btnImprimir.Visible and (ObtenerListadoConvenio.Active) and (ObtenerListadoConvenio.RecordCount>0);
    btn_Ver.Enabled := (ObtenerListadoConvenio.Active) and (ObtenerListadoConvenio.RecordCount>0);
    btn_modificar.Enabled := (ObtenerListadoConvenio.Active) and (ObtenerListadoConvenio.RecordCount>0);
    btn_VerDocumento.Enabled := (ObtenerListadoConvenio.Active) and (ObtenerListadoConvenio.RecordCount>0) and (ObtenerListadoConvenio.fieldbyname('TieneImagenScan').AsInteger = 1);
    if (ObtenerListadoConvenio.Active) then
        btn_modificar.Caption := iif(ObtenerListadoConvenio.FieldByName('CodigoEstadoConvenio').AsInteger = ESTADO_CONVENIO_VIGENTE, '&Modificar', 'Con&sultar');

end;

procedure TFormListadoConvenios.FormShow(Sender: TObject);
begin
    txt_FechaDesde.SetFocus;
    btn_Ver.Enabled          :=False;
    btnImprimir.Enabled      := False;
    btn_modificar.Enabled    :=False;
    btn_VerDocumento.Enabled := False;
end;

procedure TFormListadoConvenios.dbl_SolicitudColumns0HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;
    if ObtenerListadoConvenio.Active then begin
        ObtenerListadoConvenio.Sort := TDBListExColumn(sender).FieldName  + ' ' + iif(TDBListExColumn(sender).Sorting=csAscending ,'ASC','DESC');
        ObtenerListadoConvenioAfterScroll(ObtenerListadoConvenio);
    end;
end;


function TFormListadoConvenios.BuscarColumna(Nombre:String;IniciarDesde:integer):Integer;
begin
    if IniciarDesde>dbl_Convenio.Columns.Count-1 then begin result:=-1; exit; end;
    if dbl_Convenio.Columns[IniciarDesde].FieldName=Nombre then result:=IniciarDesde
    else Result:=BuscarColumna(Nombre,IniciarDesde+1);
end;


procedure TFormListadoConvenios.btn_VerClick(Sender: TObject);
begin
    if assigned(Self.OnClickRegSeleccionadoConsulta) then self.OnClickRegSeleccionadoConsulta(ObtenerListadoConvenio.FieldByName('NumeroConvenio').AsString, ObtenerListadoConvenio.FieldByName('CodigoConvenio').AsInteger);
end;

procedure TFormListadoConvenios.dbl_ConvenioDblClick(Sender: TObject);
begin
    if Notebook.PageIndex = 0 then
        btn_Ver.Click
    else
        if btn_modificar.Enabled then btn_modificar.Click;
end;

procedure TFormListadoConvenios.btn_LimpiarClick(Sender: TObject);
begin
    CargarPersoneria(cb_Personeria,SIN_ESPECIFICAR,false,true);
    CargarEstadosConvenio(DMConnections.BaseCAC,cb_EstadoConvenio,0, true);
    CargarConcesionarias(DMConnections.BaseCAC,cb_Concesionaria,0,true);
    CargarMediosPagoAutomatico(cb_PagoAutomatico,-1,False, True);
    txt_NumeroConvenio.Clear;
    txt_FechaDesde.Clear;
    txt_FechaHasta.Clear;
    txt_CantVehiculos.Clear;
    txt_Apellido.Clear;
    txt_rut.Clear;
    txt_Patente.clear;
    ObtenerListadoConvenio.close;
    txt_FechaDesde.SetFocus;
    lbl_Mostar.Caption:='';
    txt_Tag.Clear;
    txt_Top.Value := 100;
end;

procedure TFormListadoConvenios.txt_rutKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key := UpCase(KEY);
    if not (Key  in ['0'..'9','K','k', #8,'%']) then
       Key := #0
end;

procedure TFormListadoConvenios.txt_ApellidoKeyPress(Sender: TObject;
  var Key: Char);
begin
    key:=UpCase(Key);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarConcesionarias
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoConcesionarias:Integer = 0; TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormListadoConvenios.CargarConcesionarias(Conn: TADOConnection; Combo: TComboBox; CodigoConcesionarias:Integer = 0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        try
            s := '';
            Qry.Connection := Conn;
            s := 'SELECT * FROM Concesionarias WITH (NOLOCK) ORDER BY Descripcion';
            Qry.SQL.Text := s;
            Qry.Open;
            Combo.Clear;
            i := -1;
            if TieneItemNinguno then begin
                Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + ' ');
                i := 0;
            end;
            While not Qry.Eof do begin
                Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
                  Space(200) + Trim(Qry.FieldByName('CodigoConcesionaria').AsString));
                if Qry.FieldByName('CodigoConcesionaria').AsInteger = CodigoConcesionarias then
                  i := Combo.Items.Count - 1;
                Qry.Next;
            end;
            if (Combo.Items.Count > 0) and (i = -1) then i := 0;
            Combo.ItemIndex := i;

        except
             on E: exception do MsgBoxErr( caption, e.message, caption, MB_ICONSTOP);
        end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure TFormListadoConvenios.btn_modificarClick(Sender: TObject);
begin
    if not ObtenerListadoConvenio.Active then Exit;
    if Assigned(Self.OnClickRegSeleccionadoModif) then begin
        self.OnClickRegSeleccionadoModif(ObtenerListadoConvenio.FieldByName('NumeroConvenio').AsString, ObtenerListadoConvenio.FieldByName('CodigoConvenio').AsInteger, Fuente, Administrador);
        Update;
        Refrescar;
    end;
end;

procedure TFormListadoConvenios.ObtenerListadoConvenioAfterScroll(
  DataSet: TDataSet);
begin
    if not DataSet.Active then
        Exit;
    btn_modificar.Caption := iif(DataSet.FieldByName('CodigoEstadoConvenio').AsInteger = ESTADO_CONVENIO_VIGENTE, '&Modificar', 'Con&sultar');
//    btnImprimir.Enabled := btn_modificar.Visible and ObtenerListadoConvenio.Active and (ObtenerListadoConvenio.RecordCount > 0) and (ObtenerListadoConvenio.fieldbyname('CodigoEstadoConvenio').AsInteger <> ESTADO_CONVENIO_BAJA);
    btn_VerDocumento.Enabled := ObtenerListadoConvenio.fieldbyname('TieneImagenScan').AsInteger = 1;
end;

procedure TFormListadoConvenios.txt_TopExit(Sender: TObject);
resourcestring
    MSG_INVALID_NUMBER = 'N�mero no V�lido.';
begin
    if txt_Top.Value < 1 then begin
        MsgBoxBalloon(MSG_INVALID_NUMBER,Caption,MB_ICONSTOP,txt_Top);
        txt_Top.SetFocus;
    end;
end;

function TFormListadoConvenios.GetOnClickRegSeleccionadoModif: TOnClickRegSeleccionadoModif;
begin
    result := FOnClickRegSeleccionadoModif;
end;

procedure TFormListadoConvenios.SetOnClickRegSeleccionadoModif(
  const Value: TOnClickRegSeleccionadoModif);
begin
    FOnClickRegSeleccionadoModif := Value;
end;

function TFormListadoConvenios.GetOnClickRegSeleccionadoConsulta: TOnClickRegSeleccionadoConsulta;
begin
    Result := FOnClickRegSeleccionadoConsulta;
end;

procedure TFormListadoConvenios.SetOnClickRegSeleccionadoConsulta(
  const Value: TOnClickRegSeleccionadoConsulta);
begin
    FOnClickRegSeleccionadoConsulta := Value;
end;

{ TFormListadoConveniosAdministrador }

function TFormListadoConveniosAdministrador.Inicializar(aCaption: TCaption; MDIChild,
  EsSoloConsulta: boolean; PuntoEntrega, PuntoVenta,
  Fuente: integer): Boolean;
begin
    Result :=  inherited Inicializar(aCaption, MDIChild, EsSoloConsulta , PuntoEntrega, PuntoVenta, Fuente, True);
end;

{ TFormListadoConveniosOperador }

function TFormListadoConveniosOperador.Inicializar(aCaption: TCaption; MDIChild,
  EsSoloConsulta: boolean; PuntoEntrega, PuntoVenta,
  Fuente: integer): Boolean;
begin
    Result :=  inherited Inicializar(aCaption, MDIChild, EsSoloConsulta , PuntoEntrega, PuntoVenta, Fuente, False);
end;

{ TFormListadoConveniosConsulta }

function TFormListadoConveniosConsulta.Inicializar(aCaption: TCaption; MDIChild: Boolean): Boolean;
begin
    Result := inherited Inicializar(aCaption, MDIChild, True);
end;

procedure TFormListadoConvenios.btn_VerDocumentoClick(Sender: TObject);
var
    f: TformVisorDocumentos;
begin
    if FindFormOrCreate(TformVisorDocumentos, f) then
		f.Show
	else begin
		if not f.Inicializar(btn_VerDocumento.Caption, ObtenerListadoConvenio.fieldbyname('CodigoConvenio').AsInteger, True) then f.Release;
	end;
end;

{******************************** Procedure Header *****************************
Procedure Name: btnImprimirClick
Author :
Date Created :
Parameters : Sender: TObject
Description :

Revision : 1
    Author : pdominguez
    Date : 06/08/2009
    Description : SS 733
        - Se cambia la llamada a la impresi�n del Estado del Convenio al nuevo
        procedimiento.
*******************************************************************************}
procedure TFormListadoConvenios.btnImprimirClick(Sender: TObject);
resourcestring
    MSG_ERROR_PARAM = 'No existe el parametro CAN_COPIAS_ESTADO_CONVENIO';
var
    dcConvenio: TDatosConvenio;
    MotivoCancelacion: TMotivoCancelacion;
    Form : TFormImprimirConvenio;
    CantCopias: Integer;
begin
{    Form := TFormImprimirConvenio.Create(nil);
    MotivoCancelacion := OK;
    dcConvenio := TDatosConvenio.Create(Trim(ObtenerListadoConvenio.FieldByName('NumeroConvenio').AsString), FPuntoVenta, FPuntoEntrega);
    try
        if ObtenerParametroGeneral(DMConnections.BaseCAC, CAN_COPIAS_ESTADO_CONVENIO, CantCopias) then
            dcConvenio.Imprimir(IMPRIMIR_CONVENIO, MotivoCancelacion, CantCopias, Form, dcConvenio.Cuentas)
        else
            MsgBox(MSG_ERROR_PARAM, Caption, MB_ICONSTOP);
    finally
        dcConvenio.Free;
        form.Free;
    end;}

    try
        dcConvenio := TDatosConvenio.Create(Trim(ObtenerListadoConvenio.FieldByName('NumeroConvenio').AsString), FPuntoVenta, FPuntoEntrega);
        Form := TFormImprimirConvenio.Create(nil);
        Form.Inicializar(dcConvenio);
        Form.ImprimirEstadoConvenio;
    finally
        dcConvenio.Free;
        form.Free;
    end;
end;

end.
