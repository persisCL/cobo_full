{********************************** File Header ********************************
File Name   : frmFAQs
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 08-Mar-2004
Language    : ES-AR
Description : Visualiza �rbol FAQ
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 28-Abr-2004
Author		: Castro, Ra�l <rcastro@dpsautomation.com>
Description : Uso de CollapsablePanel para visualizaci�n de Preguntas & Respuestas
*******************************************************************************}

unit frmFAQ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, DB, ADODB, Grids, DBGrids, DBCtrls, Mask, UtilProc,
  ExtCtrls, Buttons, DPSControls, DmiCtrls, RStrings, dbcgrids, CollPnl, util,math,
  ImgList;

type
  TCodigo = Class(TObject)
  private
	FCodigoRef : Integer;
  public
	property CodigoRef: Integer read FCodigoRef write FCodigoRef;
  end;

  TfrmFAQs = class(TForm)
	Panel3: TPanel;
	Splitter2: TSplitter;
	Panel1: TPanel;
	ArbolFAQ: TTreeView;
    ActualizarEstadisticaFAQ: TADOStoredProc;
	ObtenerArbolFAQ: TADOStoredProc;
    dsPreguntasFAQ: TDataSource;
    ObtenerPreguntasFAQ: TADOStoredProc;
    sbPreguntas: TScrollBox;
    Panel2: TPanel;
    spbExpandir: TSpeedButton;
    spbContraer: TSpeedButton;
    Panel4: TPanel;
    btnExpandirArbol: TSpeedButton;
    btnContaerArbol: TSpeedButton;
    ImageList: TImageList;
	procedure LimpiarPanelPreguntas(Sender: TObject);
	procedure ArbolFAQClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	function CrearArbol: Boolean;
	function EliminarArbol: Boolean;
    procedure spbExpandirClick(Sender: TObject);
	procedure spbContraerClick(Sender: TObject);
	procedure btnExpandirArbolClick(Sender: TObject);
	procedure btnContaerArbolClick(Sender: TObject);
  private
	{ Private declarations }
    procedure ActualizarEstadistica(sender: TObject);
  public
	{ Public declarations }
	function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  frmFAQs: TfrmFAQs;

implementation

uses DMConnection;

{$R *.dfm}

function TfrmFAQs.CrearArbol: Boolean;
var
	CodigoTitulo,
	CodigoSubTitulo: Integer;

	Nodo1 : TTreeNode;

	Codigo : TCodigo;

begin
	EliminarArbol;
	LimpiarPanelPreguntas(nil);
	ObtenerPreguntasFAQ.Close;

	With ObtenerArbolFAQ Do Begin
		// Nivel 1
		Open;
		While (Not EoF) Do Begin
			CodigoTitulo := FieldByName('CodigoTitulo').AsInteger;

			Codigo := TCodigo.Create;
			Codigo.CodigoRef := CodigoTitulo;
			Nodo1 := ArbolFAQ.Items.AddChildObject(Nil, Trim(FieldByName('Titulo').AsString), Codigo);

			// Nivel 2
			While (Not EoF) And (CodigoTitulo = FieldByName('CodigoTitulo').AsInteger) Do Begin
				CodigoSubTitulo := FieldByName('CodigoSubTitulo').AsInteger;

				Codigo := TCodigo.Create;
				Codigo.CodigoRef := CodigoSubTitulo;
				ArbolFAQ.Items.AddChildObject(Nodo1, Trim(FieldByName('SubTitulo').AsString), Codigo);

				// Nivel 3
				While (Not EoF) And	(CodigoSubTitulo = FieldByName('CodigoSubTitulo').AsInteger) Do Next
			End
		End;

		Close
	End;

	ArbolFAQ.FullExpand;
	Result := ArbolFAQ.Items.GetFirstNode <> nil;
end;

function TfrmFAQs.Inicializar(MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	result := CrearArbol;
    ArbolFAQ.Select(ArbolFAQ.TopItem);
    ArbolFAQ.OnClick(ArbolFAQ);

	if not result then
		MsgBox(MSG_ERROR_NO_HAY_FAQ, self.Caption);
end;

procedure TfrmFAQs.LimpiarPanelPreguntas(Sender: TObject);
var
	cp: TCollapsablePanel;
begin
	while sbPreguntas.ControlCount > 0 do begin
		cp := (sbPreguntas.Controls[0] as TCollapsablePanel);
		sbPreguntas.RemoveControl(cp);
		cp.free;
	end
end;

procedure TfrmFAQs.ArbolFAQClick(Sender: TObject);
resourcestring
    STR_ACTUALIZAR_EST = 'Actualizar Estad�stica';
var
	cp: TCollapsablePanel;
	reRespuesta: TRichEdit;
	stream: TADOBlobStream;
    pnl: TPanel;
    btn: TSpeedButton;
    bmp: TBitMap;
begin
	ObtenerPreguntasFAQ.Close;

	if Assigned(ArbolFAQ.Selected) then begin
		If ArbolFAQ.Selected.Level <> 0 then begin
			ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoTitulo').value :=  TCodigo(ArbolFAQ.Selected.Parent.Data).CodigoRef;
			ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoSubTitulo').value := TCodigo(ArbolFAQ.Selected.Data).CodigoRef
		end else begin
			ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoTitulo').value :=  TCodigo(ArbolFAQ.Selected.Data).CodigoRef;
			ObtenerPreguntasFAQ.Parameters.ParamByName('@CodigoSubTitulo').value := null;
		end;

		ObtenerPreguntasFAQ.open
	end;

	sbPreguntas.Visible := False;
	LimpiarPanelPreguntas (Sender);

	while not ObtenerPreguntasFAQ.eof do begin
		// Creamos el Collapsed Panel
		cp := TCollapsablePanel.Create(nil);
		cp.TitleFont.Color := clBlack;
		cp.Style := cpsWinXP;
		cp.Animated := False;
		cp.Caption := trim(ObtenerPreguntasFAQ.FieldByName('Pregunta').AsString);

		// Creamos el RichEdit
		reRespuesta := TRichEdit.Create(nil);
		reRespuesta.align := alCLient;
		reRespuesta.Color := clWindow;
		reRespuesta.ScrollBars := ssVertical;
   		reRespuesta.BorderStyle := bsNone;
		reRespuesta.ReadOnly := true;

		// Insertamos y configuramos el RichEdit
		cp.InsertControl(reRespuesta);

		// Insertamos y configuramos el Collapsed Panel
		sbPreguntas.InsertControl(cp);
        cp.Color := clWindow;
		cp.BarColorStart := clWindow;
		cp.BarColorEnd := clGradientInactiveCaption;
        cp.ArrowLocation := cpaLeftBottom;

		// Finalmente asignamos el texto al RichEdit
		Stream := TADOBlobStream.Create(TBlobField(ObtenerPreguntasFAQ.FieldByName('Respuesta')), bmRead);
		reRespuesta.Lines.LoadFromStream(Stream);
		Stream.Free;
        cp.Align := alTop;
		cp.Height := max(min(100, reRespuesta.Lines.Count * 16 + 24), 48);

		btn := TSpeedButton.create(nil);
        btn.Hint := STR_ACTUALIZAR_EST;
        btn.Tag := ObtenerPreguntasFAQ.FieldByName('CodigoPregunta').AsInteger;
        btn.ShowHint := true;

        pnl := TPanel.create(nil);
        pnl.color := reRespuesta.Color;
        pnl.BevelOuter := bvNone;
        pnl.Caption := '';
        pnl.Width := 26;
        pnl.InsertControl(btn);
        btn.Align := alNone;

        bmp := TBitMap.Create;
        imageList.GetBitmap(0, bmp);
        btn.glyph.Assign(bmp);
        bmp.free;
        btn.setBounds(2, 0, 20, 20);
        btn.onClick := ActualizarEstadistica;

        cp.InsertControl(pnl);
        pnl.Align := alLeft;
   		cp.Open := False;

		// Siguiente
		ObtenerPreguntasFAQ.next;
	end;
	sbPreguntas.Visible := True;
end;

procedure TfrmFAQs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	EliminarArbol;
	action := caFree;
end;

function TfrmFAQs.EliminarArbol: Boolean;
var
	ItemArbol: TTreeNode;
begin
	// Primer nodo del �rbol
	ItemArbol := ArbolFAQ.Items.GetFirstNode;

	// Recorro hasta el final
	while ItemArbol <> nil do begin
		// Es nodo con info, libero la memoria asociada a ella
		TCodigo(ItemArbol.data).Free;

		// Pr�ximo nodo
		ItemArbol := ItemArbol.GetNext
	end;

	// Borro los items del �rbol
	ArbolFAQ.Items.Clear;

	Result := True
end;

procedure TfrmFAQs.spbExpandirClick(Sender: TObject);
var
	i: Integer;
begin
	for i := 0 to sbPreguntas.ControlCount-1 do
		(sbPreguntas.Controls[i] as TCollapsablePanel).Open := True
end;

procedure TfrmFAQs.spbContraerClick(Sender: TObject);
var
	i: Integer;
begin
	for i := 0 to sbPreguntas.ControlCount-1 do
		(sbPreguntas.Controls[i] as TCollapsablePanel).Open := False
end;

procedure TfrmFAQs.btnExpandirArbolClick(Sender: TObject);
begin
	ArbolFAQ.FullExpand;
end;

procedure TfrmFAQs.btnContaerArbolClick(Sender: TObject);
begin
	ArbolFAQ.FullCollapse;
end;

procedure TFrmFAQs.ActualizarEstadistica(sender: TObject);
begin
	With ActualizarEstadisticaFAQ, Parameters do Begin
		ParamByName('@CodigoPregunta').Value := TButton(Sender).Tag;
		ExecProc;
	End;
end;

end.

