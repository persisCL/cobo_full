unit FrmFecha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, PeaProcs;

type
  TformFecha = class(TForm)
    lblFecha: TLabel;
    edtFecha: TDateEdit;
    btnAceptar: TButton;
    btnCancelar: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(labelFecha: string; Fecha: TDateTime):Boolean;
  end;

var
  formFecha: TformFecha;

implementation

{$R *.dfm}

function TformFecha.Inicializar(labelFecha: string; Fecha: TDateTime): Boolean;
begin
    lblFecha.Caption := labelFecha;

    edtFecha.Date := Fecha;

    Result := true;
end;

procedure TformFecha.btnAceptarClick(Sender: TObject);
begin
    if not ValidateControls([edtFecha],
                     [edtFecha.Text <> ''],
                     'Atenci�n',
                     ['Indique Fecha']) then
                     Exit;

    ModalResult:= mrOk;

end;

procedure TformFecha.btnCancelarClick(Sender: TObject);
begin
Close;
end;

end.
