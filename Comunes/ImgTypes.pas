unit ImgTypes;

interface

Uses
	Windows, PeaTypes, Types;

type
	// Posici�n del vehiculo en que fueron tomadas las fotos. Si estas cambian, modificar
	// "CodigoImagenTransito/Patente", que son las posiciones de im�genes que se almacenan.
	TTipoImagen = (
                    tiFrontal, tiPosterior, tiOverview1, tiOverview2, tiOverview3, tiFrontal2, tiPosterior2, tiDesconocida,
                    tiFrontalSPC1, tiFrontalSPC2, tiContextoSPC1, tiContextoSPC2, tiContextoSPC3, tiContextoSPC4, tiContextoSPC5, tiContextoSPC6);
    TEstadoTAG = (etSinProblemas, etNoVisible, etMalUbicado, etDesmontado);

	// Errores posibles al Obtener Imagen
	TTipoErrorImg = (teSinError,
      teObtenerPath,
      teParametrosMal,
      teArchivoNoExiste,
      teAbrirArchivo,
      teLeerArchivo,
      teImagenNoExiste,
      teCargarImagen,
      teFormatoImagen,
      teGrabarArchivo);

// const
//	CodigoImagenTransito: Array[TTipoImagen] of Integer = (0, 1, 2, 3, 4, 5, 6, 7);
//	CodigoImagenPatente: Array[TTipoImagen] of Integer  = (0, 6, 2, 3, 4, 5, 6, 7);
	//  0: Nunca se almacen� ese tipo de Imagen.
	// -n: Se almacen�, pero ya no se almacena. Es decir, se puede ubicar la imagen
	//      en la posicion n, pero no grabarla.
	//  n: Se almacena (y recupera) en la posici�n n.

const
    CC_PATENTE_CAT_INCORRECTA  	= 1;
    CC_PATENTE_INCORRECTA       = 2;
    CC_CAT_INCORRECTA           = 3;
    CC_SIN_PROBLEMAS            = 4;

const
    EP_SIN_PROBLEMAS            = 1;
    EP_MAL_UBICADA              = 2;
    EP_DOBLADA                  = 3;
    EP_INCLINADA                = 4;
    EP_CARACTERES_PARTIDOS      = 5;
    EP_SUCIA                    = 6;
    EP_OBSTRUIDA                = 7;
    EP_FORMATO_DIFERENTE        = 8;

// Estructura almacenamiento:
// 	Tr�nsitos: 				Concesionaria/PuntoCobro/Fecha/archivos de 0.IMG a 9.IMG
//  Patentes: 				00..99/archivos de 000..900.IMG a 099..999.IMG

resourcestring
	IMG_FRONTAL                 = 'Imagen Frontal';
	IMG_POSTERIOR               = 'Imagen Posterior';
	IMG_GENERAL1                = 'Imagen General 1';
	IMG_GENERAL2                = 'Imagen General 2';
	IMG_GENERAL3                = 'Imagen General 3';
	IMG_FRONTAL2                = 'Imagen frontal 2';
	IMG_POSTERIOR2              = 'Imagen posterior 2';
	IMG_DESCONOCIDA             = 'Imagen desconocida';
	IMG_FRONTAL_SPC1            = 'Imagen Frontal 1 SPC';
	IMG_FRONTAL_SPC2            = 'Imagen Frontal 2 SPC';
    IMG_CONTEXTO_SPC1           = 'Imagen Contexto 1 SPC';
    IMG_CONTEXTO_SPC2           = 'Imagen Contexto 2 SPC';
    IMG_CONTEXTO_SPC3           = 'Imagen Contexto 3 SPC';
    IMG_CONTEXTO_SPC4           = 'Imagen Contexto 4 SPC';
    IMG_CONTEXTO_SPC5           = 'Imagen Contexto 5 SPC';
    IMG_CONTEXTO_SPC6           = 'Imagen Contexto 6 SPC';

const
	DescripcionImagen: Array[TTipoImagen] of String = (
	  (IMG_FRONTAL),
	  (IMG_POSTERIOR),
	  (IMG_GENERAL1),
	  (IMG_GENERAL2),
	  (IMG_GENERAL3),
	  (IMG_FRONTAL2),
	  (IMG_POSTERIOR2),
	  (IMG_DESCONOCIDA),
	  (IMG_FRONTAL_SPC1),
	  (IMG_FRONTAL_SPC2),
	  (IMG_CONTEXTO_SPC1),
	  (IMG_CONTEXTO_SPC2),
	  (IMG_CONTEXTO_SPC3),
	  (IMG_CONTEXTO_SPC4),
	  (IMG_CONTEXTO_SPC5),
	  (IMG_CONTEXTO_SPC6)
	);
	SufijoImagen: Array[TTipoImagen] of String[6] = (
	  ('VRF1'),
	  ('VRR1'),
	  ('VDC1'),
	  ('VDC2'),
	  ('VDC3'),
	  ('VRF2'),
	  ('VRR2'),
	  (''),
	  ('VRF1'),
	  ('VRF2'),
	  ('VDC1'),
	  ('VDC2'),
	  ('VDC3'),
	  ('VDC4'),
	  ('VDC5'),
	  ('VDC6'));

//    SufijoImagenVS: Array[TTipoImagen] of String[2] = (
//	  ('.1.'),
//	  (''),
//      (''),
//      (''),
//      (''),
//      ('.2.'),
//      (''),
//      (''));


	RegistrationAccessibilityImagen: Array[TTipoImagen] of Integer = (
	  1,
	  2,
	  4,
	  8,
	  16,
	  32,
	  64,
	  0,
      256,
      512,
      1024,
      2048,
      4096,
      8192,
      16384,
      32768);

    DescripcionError: Array[TTipoErrorImg] of AnsiString = (
    'No existe Error.',
    'No se puede obtener el directorio del archivo IMG.',
    'Existe un par�metro incorrecto.',
    'El archivo IMG no existe.',
    'No se puede abrir el archivo IMG.',
    'Error intentado leer en el archivo IMG.',
    'La imagen no existe.',
    'Error intentado cargar la imagen.',
    'El formato de la imagen es incorrecto.',
    'Error intentado grabar en el archivo IMG.');

Type
    (* La primera componente es para el Punto de Cobro,
    la segunda es para el Sentido del tramo,
    la tercera es para indicar el �rea a pintar para cada punto de cobro,
    la cuarta es para la coordenada. (1: X, 2: Y). *)
    TTramosPuntosCobro = array[1..9, 1..2, 1..5, 1..2] of Integer;
    TTramosOchoATres = array[1..5, 1..2] of Integer;
    TTramosTresADos = array[1..6, 1..2] of Integer;
    TTramosTresAOcho = array[1..1, 1..2] of Integer;
    TCoordenadasPuntosCobro = array[1..9, 1..2] of Integer;

const TramosPuntosCobro: TTramosPuntosCobro =
    (
     (
      ((312, 52), (294, 52), (283, 52), (0, 0), (0, 0)),  // Punto de cobro 1 Sentido Izquierda
      ((312, 60), (295, 60), (0, 0), (0, 0), (0, 0))      // Punto de cobro 1 Sentido Derecha
     ),
     (
      ((270, 52), (236, 52), (216, 52), (218, 52), (0, 0)),  // Punto de cobro 2 Sentido Izquierda
      ((245, 60), (268, 60), (283, 60), (0, 0), (0, 0))   // Punto de cobro 2 Sentido Derecha
     ),
     (
      ((205, 52), (185, 52), (0, 0), (0, 0), (0, 0)),     // Punto de cobro 3 Sentido Izquierda
      ((203, 60), (185, 60), (0, 0), (0, 0), (0, 0))     // Punto de cobro 3 Sentido Derecha
     ),
     (
      ((0, 0), (0, 0), (0, 0), (0, 0), (0, 0)),           // Punto de cobro 4 Sentido Izquierda
      ((178, 60), (168, 60), (154, 60), (0, 0), (0, 0))  // Punto de cobro 4 Sentido Derecha
     ),
     (
      ((147, 52), (122, 52), (97, 52), (0, 0), (0, 0)),   // Punto de cobro 5 Sentido Izquierda
      ((150, 60), (146, 60), (124, 60), (0, 0), (0, 0))   // Punto de cobro 5 Sentido Derecha
     ),
     (
      ((76, 52), (42, 52), (37, 52), (0, 0), (0, 0)),     // Punto de cobro 6 Sentido Izquierda
      ((43, 60), (72, 60), (97, 60), (0, 0), (0, 0))     // Punto de cobro 6 Sentido Derecha
     ),
     (
      ((306, 97), (288, 97), (278, 97), (0, 0), (0, 0)),  // Punto de cobro 7 Sentido Izquierda
      ((287, 105), (308, 105), (0, 0), (0, 0), (0, 0))   // Punto de cobro 7 Sentido Derecha
     ),
     (
      ((270, 97), (241, 79), (0, 0), (0, 0), (0, 0)),        // Punto de cobro 8 Sentido Izquierda
      ((235, 84), (268, 105), (278, 105), (0, 0), (0, 0))   // Punto de cobro 8 Sentido Derecha
     ),
     (
      ((32, 52), (16, 52), (0, 0), (0, 0), (0, 0)),        // Punto de cobro 9 Sentido Izquierda
      ((16, 60), (31, 60), (37, 60), (0, 0), (0, 0))      // Punto de cobro 9 Sentido Derecha
     )
    );

    TramosTresAOcho: TTramosTresAOcho = ((213, 61));
    TramosOchoATres: TTramosOchoATres = ((224, 62), (221, 59), (219, 57), (218, 56), (216, 53));
    TramosTresADos: TTramosTresADos = ((212, 61), (216, 61), (218, 60), (222, 60), (227, 60), (224, 62));

    // Coordenadas de los indicadores del punto de cobro actual
    CoordenadasPuntosCobro: TCoordenadasPuntosCobro = ((287, 16), (238, 16), (176, 16), (148, 75), (122, 16), (31, 77), (279, 117), (238, 117), (6, 16));


Type
	// Archivos de Almacenamiento de im�genes
	TItemHeader = packed record
        TipoImagen: TTipoImagen;
		PosicionInicial: Integer;
		BytesDatos: Integer;
		BytesEstructuraAddicional: Byte;
	end;

	// Viajes
 (*	TDirViajesHeader = packed record
		NumCorrCA: Integer;
	end;

 	TImageFileHeader = packed record
		Firma:		   Array[1..16] of Char;
		CantTransitos: Integer;
		TiposImagen:   Integer;
		OffsetSgte:	   Integer;
	end;
    *)
	// Patentes
	TDirPlatesHeader = packed record
		Patente: String[10];
	end;

	TImagePlateHeader = packed record
		Firma:		  Array[1..16] of Char;
		CantPatentes: Integer;
		TiposImagen:  Integer;
		OffsetSgte:	  Integer;
	end;

type
    TCornerLPN = Packed record
       X: Word;
       Y: Word;
    end;

	// Datos que se encuentran al final de los archivos de imagen
	TDataImageVR = packed record                           	// Total =  116 bytes
		TipoImagen: 	  Word;								//  2 bytes
		PointID: 		  Word;								//  2 bytes
		SegmentoID:  	  Word;								//  2 bytes
		SensorSN: 		  packed array[0..69] of char;		// 70 bytes
		Camera:   		  Word;								//  2 bytes
		PosicionVehiculo: Word;                 		   	//  2 bytes
		PosicionPatente:  Word;								//  2 bytes
	 	FechaHora: 		  TUnixDateTime; 					//  8 bytes
		OCR: 			  String[10];						// 11 bytes
		Confiabilidad:    String[10];						// 11 bytes
        UpperLeftLPN:     TCornerLPN;
        UpperRigthLPN:    TCornerLPN;
        LowerRigthLPN:    TCornerLPN;
        LowerLeftLPN:     TCornerLPN;
		Checksum: 		  DWORD;							//  4 bytes
	end;

	TCoordVehiculo = packed record 								// Total = 14 bytes
		RegistrationID: Cardinal;							//  4 bytes
		VehicleID:		Word;								//  2 bytes
		Largo:		  	Word; 								//  2 bytes
		Ancho: 			Word;								//  2 bytes
		FrontX: 		SmallInt;							//  2 bytes
		FrontY:         SmallInt;							//  2 bytes
	end;

	TDataOverview = packed record                       	// Total = 42 + (14 * 24)
		TipoImagen:    	Word;								//  2 bytes
		FactorEscala:  	Word; 								//  2 bytes
		upperLeftX:     Smallint;                       	//  2 bytes
		upperLeftY: 	Smallint;							//  2 bytes
		PointID: 	   	Word;								//  2 bytes
		SegmentoID:    	Word;								//  2 bytes
		FechaHora:     	DWORD;								//  4 bytes
		TiempoCaptura:	DWORD;								//  4 bytes
		VDCSensor:		Word;								//  2 bytes
		VDCX:			Smallint;							//  2 bytes
		Actual:			TCoordVehiculo;						// 14 bytes
		Vehiculos:		packed array[1..24] of TCoordVehiculo;
		Checksum: 		Cardinal;							//  4 bytes
	end;

	// Datos que guardamos
	TDescriAuto = record
		NumCorrCO:    LongInt;
		Validado: 	  Boolean;
		TAGAsignado:  Boolean;
		ContextMark:  BYTE;
		SerialNumber: Double;
		Categoria:	  BYTE;
		Patente:	  AnsiString;
		Sugerido:	  Boolean;
	end;


    TDataImage = packed record
        case integer of
            0: (DataOverView: TDataOverView);
            1: (DataImageVR: TDataImageVR);
    end;
	TDatosAutos =  Array[0..40] of TDescriAuto;
	TEstadoTransito = (etPendiente, etValidado);
	
const
	BYTES_EXTRAS_VR		 		= SizeOf(TDataImageVR);
	BYTES_EXTRAS_OVERVIEW 		= SizeOf(TDataOverview);

implementation

end.
