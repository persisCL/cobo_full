unit LoginValuesEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CategoryButtons, Util, StdCtrls, ExtCtrls, DmiCtrls, DB, ADODB;

type
  TfrmLoginValuesEditor = class(TForm)
    lblSection: TLabel;
    lblkey: TLabel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Shape1: TShape;
    Bevel1: TBevel;
    txtValue: THistoryEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAceptarClick(Sender: TObject);
  private
    FItem: TCategoryButtons;
    FSection, FEntry, FKey, FValue: String;
  public
    function Inicializar(Item: TCategoryButtons): Boolean;
  end;

var
  frmLoginValuesEditor: TfrmLoginValuesEditor;

implementation

{$R *.dfm}

{ TfrmLoginValuesEditor }

function TfrmLoginValuesEditor.Inicializar(Item: TCategoryButtons): Boolean;
begin
    FItem := Item;
    txtValue.Clear;
    FSection := Item.SelectedItem.Category.Caption;
    FEntry := Item.SelectedItem.Caption;
    FKey := Trim(ParseParamByNumber(FEntry, 1, '='));
    FValue := Trim(ParseParamByNumber(FEntry, 2, '='));
    lblSection.Caption := '[' + FSection + ']';
    lblKey.Caption := FKey + ':';
    txtValue.Text := FValue;
    Result := True;
end;

procedure TfrmLoginValuesEditor.btnAceptarClick(Sender: TObject);
const
    SEP_CHAR = '=';
begin
    FItem.SelectedItem.Caption := FKey + SEP_CHAR + Trim(txtValue.Text);
    InstallIni.WriteString(FSection, FKey, Trim(txtValue.Text));
    txtValue.SaveHistory;
    Close;
end;

procedure TfrmLoginValuesEditor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.

