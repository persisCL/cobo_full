{-----------------------------------------------------------------------------
  Function Name: frmUltimaFactura
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit frmUltimaFactura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ppViewr, StdCtrls, Buttons, ExtCtrls, ToolWin, ComCtrls, Util, ppComm,
  ppRelatv, ppProd, ppArchiv, UtilProc, RBPreviewPrint, RBSetup, RBSpool,
  Menus, ppReport, ppClass, ResourceStrings, Cursores, ppTypes, JPeg, Clipbrd,
  ImgList, Mapi, ShellApi, CollPnl, DMIGDI, navigator, DMConnection,
  Utildb, UtilRB;

type
  TExPPViewer = class(TPPViewer)
  protected
	function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
	function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
  end;

  TFormUltimaFactura = class(TNavWindowFrm)
    ControlBar1: TControlBar;
    ToolBar1: TToolBar;
    Panel4: TPanel;
    btn_cerrar: TSpeedButton;
    btn_copiar: TSpeedButton;
    btn_imprimir: TSpeedButton;
	btn_open: TSpeedButton;
    btn_save: TSpeedButton;
	cb_Escala: TComboBox;
	Panel3: TPanel;
	btn_pg_up: TSpeedButton;
    btn_pg_dn: TSpeedButton;
	Reader: TppArchiveReader;
	OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    PopupMenu1: TPopupMenu;
    Cerrar1: TMenuItem;
    N1: TMenuItem;
    Zoom1: TMenuItem;
    AnchodePgina1: TMenuItem;
    PginaEntera1: TMenuItem;
    Guardarcomo1: TMenuItem;
    Abrir1: TMenuItem;
	Imprimir1: TMenuItem;
    Copiar1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    mnu_zoom010: TMenuItem;
    mnu_zoom025: TMenuItem;
    mnu_zoom050: TMenuItem;
	mnu_zoom075: TMenuItem;
    mnu_zoom100: TMenuItem;
    mnu_zoom200: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    Enviara1: TMenuItem;
    mnu_email: TMenuItem;
    mnu_desktop: TMenuItem;
    mnu_mydocuments: TMenuItem;
    ImageList1: TImageList;
    pnlBarras: TPanel;
	pnlViewer: TPanel;
    Panel6: TPanel;
    Panel5: TPanel;
    Panel2: TPanel;
    Panel7: TPanel;
    cpTareas: TCollapsablePanel;
    cpResumen: TCollapsablePanel;
    Panel8: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    img_desktop: TImage;
    img_mydocs: TImage;
    img_openmydocs: TImage;
    img_mail: TImage;
    img_Save: TImage;
    Label6: TLabel;
    lblTamanio: TLabel;
    Label8: TLabel;
    lblPaginas: TLabel;
    Label10: TLabel;
    lblTitulo: TLabel;
    lblFecha: TLabel;
    Label9: TLabel;
	procedure cb_EscalaClick(Sender: TObject);
	procedure cb_EscalaExit(Sender: TObject);
	procedure cb_EscalaKeyPress(Sender: TObject; var Key: Char);
	procedure FormKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure btn_cerrarClick(Sender: TObject);
	procedure btn_imprimirClick(Sender: TObject);
	procedure btn_openClick(Sender: TObject);
	procedure btn_saveClick(Sender: TObject);
	procedure btn_copiarClick(Sender: TObject);
	procedure AnchodePgina1Click(Sender: TObject);
	procedure PginaEntera1Click(Sender: TObject);
	procedure ViewerPageChange(Sender: TObject);
	procedure btn_pg_upClick(Sender: TObject);
	procedure btn_pg_dnClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure mnu_zoomClick(Sender: TObject);
	procedure mnu_desktopClick(Sender: TObject);
	procedure mnu_mydocumentsClick(Sender: TObject);
	procedure mnu_emailClick(Sender: TObject);
    procedure Label1MouseEnter(Sender: TObject);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label5Click(Sender: TObject);
  private
	LastWheel: Int64;
	Viewer: TPPViewer;
	FDragPoint: TPoint;
  	FArchiveFile: AnsiString;
	FReportName: AnsiString;
	FReportTitle: AnsiString;
    FEmail, FEmailDefault, FNameDefault: AnsiString;
    FCodigoCliente: integer;
	{ Private declarations }
	Procedure MouseWheelDown;
	Procedure MouseWheelUp;
	function  GetSaveFileName: AnsiString;
	procedure DoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
	procedure DoMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
	procedure DoMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  public
	{ Public declarations }
	constructor Create(AOwner: TComponent); override;
	function Inicializa(ArchiveFile, ReportName, ReportTitle: AnsiString; CodigoCliente: integer): Boolean; override;
	Procedure RefreshData; override;
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(ArchiveFile, ReportName, ReportTitle: AnsiString; CodigoCliente: integer): AnsiString;
  end;

var
  FormUltimaFactura: TFormUltimaFactura;

function SendMailMAPI(const Subject, Body, FileName, SenderName, SenderEMail, RecepientName, RecepientEMail: String) : Integer;

implementation


function SendMailMAPI(const Subject, Body, FileName, SenderName, SenderEMail, RecepientName, RecepientEMail: String) : Integer;
var
	message: TMapiMessage;
	lpSender,lpRecepient: TMapiRecipDesc;
	FileAttach: TMapiFileDesc;
	SM: TFNMapiSendMail;
	MAPIModule: HModule;
begin
	FillChar(message, SizeOf(message), 0);

	with message do begin

		if (Subject<>'') then lpszSubject := PChar(Subject);
	    if (Body<>'') then lpszNoteText := PChar(Body);

		if (SenderEMail<>'') then begin
			lpSender.ulRecipClass := MAPI_ORIG;
			if (SenderName='') then lpSender.lpszName := PChar(SenderEMail)
				else lpSender.lpszName := PChar(SenderName);
	    	lpSender.lpszAddress := PChar('SMTP:'+SenderEMail);
	    	lpSender.ulReserved := 0;
    	  	lpSender.ulEIDSize := 0;
      		lpSender.lpEntryID := nil;
	     	lpOriginator := @lpSender;
    	end;

		if (RecepientEMail<>'') then begin
			lpRecepient.ulRecipClass := MAPI_TO;
			if (RecepientName='') then lpRecepient.lpszName := PChar(RecepientEMail)
				else lpRecepient.lpszName := PChar(RecepientName);
			lpRecepient.lpszAddress := PChar('SMTP:'+RecepientEMail);
	        lpRecepient.ulReserved := 0;
			lpRecepient.ulEIDSize := 0;
			lpRecepient.lpEntryID := nil;
			nRecipCount := 1;
			lpRecips := @lpRecepient;
    	end else begin
	      lpRecips := nil
    	end;

		if (FileName='') then begin
			nFileCount := 0;
			lpFiles := nil;
		end else begin
			FillChar(FileAttach, SizeOf(FileAttach), 0);
      		FileAttach.nPosition := Cardinal($FFFFFFFF);
      		FileAttach.lpszPathName := PChar(FileName);
      		nFileCount := 1;
      		lpFiles := @FileAttach;
		end;
    end;

	MAPIModule := LoadLibrary(PChar(MAPIDLL));
	if MAPIModule=0 then
    	Result := -1
    else begin
        try
            @SM := GetProcAddress(MAPIModule, 'MAPISendMail');
            if @SM<>nil then Result := SM(0, Application.Handle, message, MAPI_DIALOG or  MAPI_LOGON_UI, 0)
                else Result := 1
        finally
            FreeLibrary(MAPIModule);
        end;
	end;
end;


{ TExPPViewer }

function TExPPViewer.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
	TFormUltimaFactura(Owner).MouseWheelDown;
	Result := True;
end;

function TExPPViewer.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
	TFormUltimaFactura(Owner).MouseWheelUp;
	Result := True;
end;

{$R *.DFM}

{ TNavWindowDatosCliente }
class function TFormUltimaFactura.CreateBookmark(ArchiveFile, ReportName, ReportTitle: AnsiString; CodigoCliente: integer): AnsiString;
begin
	Result := ArchiveFile+ ';' + ReportName + ';' + ReportTitle + ';' + IntToSTr(CodigoCliente);
end;

function TFormUltimaFactura.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Comprobante Factura: %18.0n';
begin
	Bookmark	:= CreateBookMark(Reader.ArchiveFileName, FReportName, FReportTitle, FCodigoCliente);
	Description	:= 'Comprobante de Factura';
	Result		:= True;
end;

function TFormUltimaFactura.GotoBookmark(Bookmark: AnsiString): Boolean;
begin
	FArchiveFile			:= ParseParamByNumber(Bookmark, 1,';');
	FReportName 			:= ParseParamByNumber(Bookmark, 2,';');
	FReportTitle 			:= ParseParamByNumber(Bookmark, 3,';');
	FCodigoCliente 			:= Ival(ParseParamByNumber(Bookmark, 4,';'));
	if Trim(FReportTitle) = '' then FReportTitle := 'Untitled';
	Caption := FReportTitle + ' - ' + MSG_PREVIEW;
	Result := inicializa(FArchiveFile, FReportName, FReportTitle, FCodigoCliente);
end;

procedure TFormUltimaFactura.cb_EscalaClick(Sender: TObject);
begin
	Viewer.SetFocus;
end;

procedure TFormUltimaFactura.cb_EscalaExit(Sender: TObject);
Var
	s: AnsiString;
begin
	if cb_Escala.Text = cb_Escala.Items[cb_Escala.Items.Count - 2] then
		Viewer.ZoomSetting := zsPageWidth
	else if cb_Escala.Text = cb_Escala.Items[cb_Escala.Items.Count - 1] then
		Viewer.ZoomSetting := zsWholePage
	else begin
		s := StringReplace(cb_Escala.Text, '%', '', [rfReplaceAll]);
		if IVal(s) < 10 then s := '10';
		if IVal(s) > 500 then s := '500';
		Viewer.ZoomSetting := zsPercentage;
		Viewer.ZoomPercentage := IVal(s);
		cb_Escala.Text := IntToStr(Viewer.ZoomPercentage) + '%';
	end;
end;

procedure TFormUltimaFactura.cb_EscalaKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then begin
		Viewer.SetFocus;
		Key := #0;
	end;
end;

procedure TFormUltimaFactura.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
Var
	nPage: Integer;
begin
	if ActiveControl = Viewer then begin
		nPage := Reader.ArchivePageNo;
		case Key of
			VK_NEXT:
				if Viewer.ScrollBox.VertScrollBar.Position >=
				  (Viewer.ScrollBox.VertScrollBar.Range - Viewer.Height) then begin
					if nPage >= Reader.ArchivePageCount then begin
						Beep;
					end else begin
						Viewer.NextPage;
						Viewer.ScrollBox.VertScrollBar.Position := 0;
					end;
				end else begin
					Viewer.ScrollBox.VertScrollBar.Position :=
					  Viewer.ScrollBox.VertScrollBar.Position + Viewer.Height - 10;
				end;
			VK_PRIOR:
				if Viewer.ScrollBox.VertScrollBar.Position = 0 then begin
					if nPage <= 1 then begin
						Beep;
					end else begin
						Viewer.PriorPage;
						Viewer.ScrollBox.VertScrollBar.Position := 10000;
					end;
				end else begin
					Viewer.ScrollBox.VertScrollBar.Position :=
					  Viewer.ScrollBox.VertScrollBar.Position - Viewer.Height + 10;
				end;
			VK_HOME:
				if ssCtrl in Shift then begin
					Viewer.FirstPage;
					Viewer.ScrollBox.VertScrollBar.Position := 0;
				end;
			VK_END:
				if ssCtrl in Shift then begin
					Viewer.LastPage;
					Viewer.ScrollBox.VertScrollBar.Position := 10000;
				end;
		end;
	end;
end;

procedure TFormUltimaFactura.btn_cerrarClick(Sender: TObject);
begin
	Close;
end;

constructor TFormUltimaFactura.Create(AOwner: TComponent);
Var
	S: TSize;
	X, Y: Integer;
	MyDocs: AnsiString;
begin
	inherited Create(AOwner);
	Viewer := TExPPViewer.Create(Self);
	Viewer.Parent := pnlViewer;
	Viewer.Align := alClient;
	Viewer.BevelOuter := bvNone;
	Viewer.PageColor := clWindow;
	Viewer.PopupMenu := PopupMenu1;
	Viewer.Report := Reader;
	Viewer.ZoomPercentage := 100;
	Viewer.ZoomSetting := zsPageWidth;
	Viewer.OnPageChange := ViewerPageChange;
	Viewer.TabStop := True;
	Viewer.ScrollBox.VertScrollBar.Increment := 50;
	Viewer.PaintBox.OnMouseDown := DoMouseDown;
	Viewer.PaintBox.OnMouseMove := DoMouseMove;
	Viewer.PaintBox.OnMouseUp   := DoMouseUp;
	Viewer.PaintBox.Cursor      := crHandOpen;
	ActiveControl := Viewer;

	if (Application.MainForm <> nil) and (Application.MainForm.FormStyle = fsMDIForm) then begin
		// El Form principal es MDI. Si tiene alguna ventana hija, y est�
		// maximizada, entonces esta ventana debe estar maximizada tambi�n.
		// De lo contrario, tomamos estado normal, pero ocupamos toda el �rea
		// disponible de la ventana principal.
		FormStyle := fsMDIChild;
		if (Application.MainForm.MDIChildCount = 0) or
		  (Application.MainForm.MDIChildren[0].WindowState <> wsMaximized) then begin
			S := GetFormClientSize(Application.MainForm);
			SetBounds(0, 0, S.cx, S.cy);
		end;
	end else begin
		x := Screen.Width;
		y := Screen.Height;
		SetBounds(x div 40, y div 40, Trunc(x * 0.95), Trunc(y * 0.95));
	end;
	MyDocs := GetMyDocumentsDir;
	While StrRight(MyDocs, 1) = '\' do SetLength(MyDocs, Length(MyDocs) - 1);
	mnu_MyDocuments.Caption := '&' + ExtractFileName(MyDocs);
end;

procedure TFormUltimaFactura.btn_imprimirClick(Sender: TObject);
Var
	f: TRBPrevPrintForm;
begin
	Application.CreateForm(TRBPrevPrintForm, f);
	if f.Inicializa(FReportName) and (f.ShowModal = mrOk) then begin
		TRBSpooler.Create(Reader.ArchiveFileName, FReportTitle, f.Config);
	end;
	f.Release;
end;

procedure TFormUltimaFactura.btn_openClick(Sender: TObject);
begin
	if OpenDialog.Execute then begin
		Reader.ArchiveFileName := OpenDialog.FileName;
		Reader.PrintToDevices;
	end;
end;

procedure TFormUltimaFactura.btn_saveClick(Sender: TObject);

	function TryExt(FileName, NewExt: AnsiString): AnsiString;
	Var
		Ext: AnsiString;
	begin
		Ext := ExtractFileExt(FileName);
		if Ext = '' then Result := ChangeFileExt(FileName, NewExt)
		  else Result := FileName;
	end;

begin
	if not SaveDialog.Execute then Exit;
	case SaveDialog.FilterIndex of
		1: SaveDialog.FileName := TryExt(SaveDialog.FileName, '.rpt');
		2: SaveDialog.FileName := TryExt(SaveDialog.FileName, '.txt');
		3: SaveDialog.FileName := TryExt(SaveDialog.FileName, '.xls');
		4: SaveDialog.FileName := TryExt(SaveDialog.FileName, '.rtf');
		5: SaveDialog.FileName := TryExt(SaveDialog.FileName, '.pdf');
	end;
	case SaveDialog.FilterIndex of
		1: ConvertArchive(Reader.ArchiveFileName, SaveDialog.FileName, dtArchive);
		2: ConvertArchive(Reader.ArchiveFileName, SaveDialog.FileName, dtTextFile);
		3: ConvertArchive(Reader.ArchiveFileName, SaveDialog.FileName, dtExcelFile);
		4: ConvertArchive(Reader.ArchiveFileName, SaveDialog.FileName, dtRTFFile);
		5: ConvertArchive(Reader.ArchiveFileName, SaveDialog.FileName, dtPDFFile);
		else MsgBox('Formato de archivo desconocido', 'Guardar', MB_ICONSTOP);
	end;
end;

procedure TFormUltimaFactura.btn_copiarClick(Sender: TObject);
Var
	JPeg: TJPegImage;
	Arch: AnsiString;
begin
	Screen.Cursor := crHourGlass;
	Arch := GetTempDir + 'IMG0001.jpg';
	if ConvertArchive(Reader.ArchiveFileName, GetTempDir + '1.jpg', dtGraphicFile,
	  IntToStr(Reader.ArchivePageNo)) then begin
		JPeg := TJPegImage.Create;
		try
			JPeg.LoadFromFile(Arch);
			Clipboard.Assign(JPeg);
		except
			on e: exception do MsgBox('Error al copiar: ' + e.message,
			  'Error', MB_ICONSTOP);			
		end;
		JPeg.Free;
	end else begin
		MsgBox('Error al copiar', 'Error', MB_ICONSTOP);
	end;
	DeleteFile(Arch);
	Screen.Cursor := crDefault;
end;

procedure TFormUltimaFactura.AnchodePgina1Click(Sender: TObject);
begin
	cb_escala.ItemIndex := cb_escala.Items.count - 2;
	cb_escala.OnExit(cb_escala);
end;

procedure TFormUltimaFactura.PginaEntera1Click(Sender: TObject);
begin
	cb_escala.ItemIndex := cb_escala.Items.count - 1;
	cb_escala.OnExit(cb_escala);
end;

procedure TFormUltimaFactura.ViewerPageChange(Sender: TObject);
begin
	if assigned(viewer.report) then begin
		btn_pg_up.enabled := Reader.ArchivePageNo > 1;
		btn_pg_dn.enabled := Reader.ArchivePageNo < Reader.ArchivePageCount;
	end;
end;

procedure TFormUltimaFactura.btn_pg_upClick(Sender: TObject);
begin
	Viewer.PriorPage;
	Viewer.ScrollBox.VertScrollBar.Position := 0;
end;

procedure TFormUltimaFactura.btn_pg_dnClick(Sender: TObject);
begin
	Viewer.NextPage;
	Viewer.ScrollBox.VertScrollBar.Position := 0;
end;

procedure TFormUltimaFactura.FormCreate(Sender: TObject);
begin
	cb_escala.Clear;
	cb_escala.Items.Add('500%');
	cb_escala.Items.Add('200%');
	cb_escala.Items.Add('150%');
	cb_escala.Items.Add('100%');
	cb_escala.Items.Add('75%');
	cb_escala.Items.Add('50%');
	cb_escala.Items.Add('25%');
	cb_escala.Items.Add('10%');
	cb_escala.Items.Add(MSG_ANCHO_PAGINA);
	cb_escala.Items.Add(MSG_PAGINA_ENTERA);
	{ Ancho de P�gina }
	cb_escala.ItemIndex := cb_escala.Items.Count - 2;
end;

procedure TFormUltimaFactura.mnu_zoomClick(Sender: TObject);
begin
	cb_Escala.Text := IntToStr(IVal(StrRight(TComponent(Sender).Name, 3))) + '%';
	cb_EscalaExit(cb_Escala);
end;
{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:
  Date Created:  /  /
  Description:
  Parameters: ArchiveFile, ReportName, ReportTitle: AnsiString; CodigoCliente: integer
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormUltimaFactura.Inicializa(ArchiveFile, ReportName, ReportTitle: AnsiString; CodigoCliente: integer): Boolean;
begin
	Update;
	Result := Inherited Inicializa;

	if not Result then Exit;

    if not FileExists(FArchiveFile) then begin
    	Result := False;
    	Exit;
	end;

	Reader.ArchiveFileName	:= FArchiveFile;
//    Reader.PrintToDevices;
    FEmailDefault	:= installini.ReadString('Correo','sender','');
    FNameDefault	:= installini.ReadString('Correo','sendername','');
    FEmail		 	:= Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT ISNULL (Valor, '''') FROM MediosComunicacion  WITH (NOLOCK) INNER JOIN PersonasMediosComunicacion  WITH (NOLOCK) ON '+
                                            'MediosComunicacion.CodigoMedioComunicacion = PersonasMediosComunicacion.CodigoMedioComunicacion ' +
                                            'WHERE CodigoTipoMedioContacto = dbo.CONST_MEDIOS_CONTACTO_EMAIL() AND ' +
                                            ' MediosComunicacion.ACTIVO = 1 AND CodigoPersona = ' + intToStr(CodigoCliente)));

	if Trim(ReportTitle) = '' then FReportTitle := 'Untitled';
	Caption := FReportTitle + ' - ' + MSG_PREVIEW;
	Reader.PrintToDevices;
	ImageList1.GetBitmap(0, img_mail.Picture.Bitmap);
	ImageList1.GetBitmap(1, img_desktop.Picture.Bitmap);
	ImageList1.GetBitmap(2, img_mydocs.Picture.Bitmap);
	ImageList1.GetBitmap(2, img_openmydocs.Picture.Bitmap);
	lblTitulo.Caption  := FReportTitle;
	lblFecha.Caption   := DateTimeToStr(FileDateToDateTime(FileAge(FArchiveFile)));
	lblTamanio.Caption := IntToStr(GetFileSize(FArchiveFile) div 1024) + ' Kb.';
	lblPaginas.Caption := IntToStr(Reader.ArchivePageCount);
	// Paneles
	pnlBarras.Color         := AlphaColor(clHighlight, clWhite, 200);
	cpTareas.BarColorStart  := AlphaColor(clHighlight, clWhite, 30);
	cpTareas.BarColorEnd    := AlphaColor(clHighlight, clBlack, 30);
	cpResumen.BarColorStart := cpTareas.BarColorStart;
	cpResumen.BarColorEnd   := cpTareas.BarColorEnd;
	// Listo
	Result := True;
end;

procedure TFormUltimaFactura.mnu_desktopClick(Sender: TObject);
Var
	Str: TSHFileOpStruct;
	FFrom, FTo: AnsiString; 
begin
	FFrom 	   := Reader.ArchiveFileName + #0;
	FTo   	   := GoodDir(GetDesktopDir) + GetSaveFileName + #0;
	Str.Wnd    := Handle;
	Str.wFunc  := FO_COPY;
	Str.pFrom  := PChar(FFrom);
	Str.pTo    := PChar(FTo);
	Str.fFlags := FOF_ALLOWUNDO;
	SHFileOperation(Str);
end;

procedure TFormUltimaFactura.mnu_mydocumentsClick(Sender: TObject);
Var
	Str: TSHFileOpStruct;
	FFrom, FTo: AnsiString; 
begin
	FFrom 	   := Reader.ArchiveFileName + #0;
	FTo   	   := GoodDir(GetMyDocumentsDir) + GetSaveFileName + #0;
	Str.Wnd    := Handle;
	Str.wFunc  := FO_COPY;
	Str.pFrom  := PChar(FFrom);
	Str.pTo    := PChar(FTo);
	Str.fFlags := FOF_ALLOWUNDO;
	SHFileOperation(Str);
end;

procedure TFormUltimaFactura.mnu_emailClick(Sender: TObject);
Var
//	Msg: TMapiMessage;
//	Attach: TMapiFileDesc;
	Subject, Body, PathName: AnsiString;
    NuevoArchivo: AnsiString;
begin
	Screen.Cursor := crHourGlass;
	PathName := GetTempDir + FReportTitle + '.rpt';
	RegisterWorkFile(PathName);
	FileCopy(Reader.ArchiveFileName, PathName);

	Subject := FReportTitle;
	Body := 'Se adjunta la �ltima factura ';//"' + FReportTitle + '.rpt"';

    NuevoArchivo := ChangeFileExt(PathName, '.pdf');
    if FileExists(NuevoArchivo) then DeleteFile(NuevoArchivo);
   	ConvertArchive(PathName, NuevoArchivo, dtPDFFile);

	if not SendMailMAPI(
    	Subject,
        Body,
        NuevoArchivo,
        FNameDefault, //SenderName,
        FEmailDefault,
        '',//RecepientName,
        FEmail//RecepientEMail
        ) = 0 then begin
        MsgBox('Error al enviar el mensaje', 'Enviar a destinatario de correo', MB_ICONSTOP)
	end;
(*	// Attachment
	FillChar(Attach, SizeOf(Attach), 0);
	Attach.nPosition := $FFFFFFFF;
	Attach.lpszPathName := PChar(PathName);
	// Mail
	FillChar(Msg, SizeOf(Msg), 0);
	Msg.lpszSubject := PChar(Subject);
	Msg.lpszNoteText := PChar(Body);
	Msg.nFileCount := 1;
	Msg.lpFiles := @Attach;
	// Enviamos el mail
	if MAPISendMail(0, Application.Handle, Msg, MAPI_DIALOG or MAPI_LOGON_UI,
	  0) <> SUCCESS_SUCCESS then begin
		MsgBox('Error al enviar el mensaje', 'Enviar a destinatario de correo',
		  MB_ICONSTOP);
	end;                 *)
	Screen.Cursor := crDefault;
end;

procedure TFormUltimaFactura.MouseWheelDown;
Var
	Ant: Integer;
begin
	Ant := Viewer.ScrollBox.VertScrollBar.Position;
	Viewer.ScrollBox.VertScrollBar.Position := Viewer.ScrollBox.VertScrollBar.Position +
	  Viewer.ScrollBox.VertScrollBar.Increment;
	if Viewer.ScrollBox.VertScrollBar.Position = Ant then begin
		// Ya est�bamos abajo a tope
		if LastWheel = 0 then
			LastWheel := GetMSCounter
		else if (GetMSCounter > LastWheel + 300) and btn_pg_dn.Enabled then begin
			btn_pg_dn.Click;
		end;
	end else begin
		LastWheel := 0;
	end;
end;

procedure TFormUltimaFactura.MouseWheelUp;
Var
	Ant: Integer;
begin
	Ant := Viewer.ScrollBox.VertScrollBar.Position;
	Viewer.ScrollBox.VertScrollBar.Position := Viewer.ScrollBox.VertScrollBar.Position -
	  Viewer.ScrollBox.VertScrollBar.Increment;
	if Viewer.ScrollBox.VertScrollBar.Position = Ant then begin
		// Ya est�bamos arriba a tope
		if LastWheel = 0 then
			LastWheel := GetMSCounter
		else if (GetMSCounter > LastWheel + 300) and btn_pg_up.Enabled then begin
			btn_pg_up.Click;
			Viewer.ScrollBox.VertScrollBar.Position := Viewer.ScrollBox.VertScrollBar.Range -
			  Viewer.ScrollBox.ClientHeight;
		end;
	end else begin
		LastWheel := 0;
	end;
end;

procedure TFormUltimaFactura.DoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	if Button <> mbLeft then Exit;
	GetCursorPos(FDragPoint);
	Screen.Cursor := crHandClosed;
end;

procedure TFormUltimaFactura.DoMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
Var
	P: TPoint;
	Ant: Integer;
begin
	if GetCapture <> Viewer.PaintBox.Parent.Handle then Exit;
	GetCursorPos(P);
	// Movemos la ventana horizontalmente
	Ant := Viewer.ScrollBox.HorzScrollBar.Position;
	Viewer.ScrollBox.HorzScrollBar.Position := Viewer.ScrollBox.HorzScrollBar.Position - P.X + FDragPoint.X;
	if Ant <> Viewer.ScrollBox.HorzScrollBar.Position then FDragPoint.X := P.X;
	// Movemos la ventana verticalmente
	Ant := Viewer.ScrollBox.VertScrollBar.Position;
	Viewer.ScrollBox.VertScrollBar.Position := Viewer.ScrollBox.VertScrollBar.Position - P.Y + FDragPoint.Y;
	if Ant <> Viewer.ScrollBox.VertScrollBar.Position then FDragPoint.Y := P.Y;
end;

procedure TFormUltimaFactura.DoMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	Screen.Cursor := crDefault;
end;

procedure TFormUltimaFactura.Label1MouseEnter(Sender: TObject);
begin
	with TLabel(Sender) do begin
		Font.Style := [fsUnderline];
//		Font.Color := clMenuHighlight;
		Font.Color := clMenu;
	end;
end;

procedure TFormUltimaFactura.Label1MouseLeave(Sender: TObject);
begin
	with TLabel(Sender) do begin
		Font.Style := [];
		Font.Color := clBlack;
	end;
end;

function TFormUltimaFactura.GetSaveFileName: AnsiString;
begin
	Result := ExtractFileName(Reader.ArchiveFileName);
	if AnsiCompareText(ExtractFileExt(Result), '.tmp') = 0 then
	  Result := FReportTitle + '.rpt';
end;

procedure TFormUltimaFactura.Label5Click(Sender: TObject);
begin
	Run('Explorer "' + GetMyDocumentsDir + '"');
end;

procedure TFormUltimaFactura.RefreshData;
begin
  inherited;
end;

end.

