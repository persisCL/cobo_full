{-----------------------------------------------------------------------------
 File Name: RVMBind.pas
 Author:    gcasais
 Date Created: 11/04/2005
 Language: ES-AR
 Description: Bind Unit para el XML del RNVM
-----------------------------------------------------------------------------}
unit RVMBind;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLCertxmlType = interface;
  IXMLPatenteType = interface;
  IXMLAutoType = interface;
  IXMLConSeguroType = interface;
  IXMLSinSeguroType = interface;
  IXMLAmoType = interface;
  IXMLDireType = interface;
  IXMLAmorepType = interface;
  IXMLAnotacion1Type = interface;
  IXMLTituloSoliType = interface;
  IXMLSolicitudType = interface;
  IXMLSinSolicitudesType = interface;
  IXMLSubinscripcionType = interface;
  IXMLSubinscripcionTypeList = interface;
  IXMLRepType = interface;
  IXMLAmoAntType = interface;
  IXMLAmoCOMType = interface;

{ IXMLCertxmlType }

  IXMLCertxmlType = interface(IXMLNode)
    ['{8341C1CB-C1A9-499C-A3DC-A00931604D58}']
    { Property Accessors }
    function Get_Patente: IXMLPatenteType;
    function Get_Auto: IXMLAutoType;
    function Get_ConSeguro: IXMLConSeguroType;
    function Get_SinSeguro: IXMLSinSeguroType;
    function Get_Amo: IXMLAmoType;
    function Get_Dire: IXMLDireType;
    function Get_Amorep: IXMLAmorepType;
    function Get_TituloAnot: WideString;
    function Get_Anotacion1: IXMLAnotacion1Type;
    function Get_TituloSoli: IXMLTituloSoliType;
    function Get_Solicitud: IXMLSolicitudType;
    function Get_SinSolicitudes: IXMLSinSolicitudesType;
    function Get_TituloSub: WideString;
    function Get_Subinscripcion: IXMLSubinscripcionTypeList;
    function Get_TituloAmos: WideString;
    function Get_Rep: IXMLRepType;
    function Get_AmoAnt: IXMLAmoAntType;
    function Get_AmoCOM: IXMLAmoCOMType;
    procedure Set_TituloAnot(Value: WideString);
    procedure Set_TituloSub(Value: WideString);
    procedure Set_TituloAmos(Value: WideString);
    { Methods & Properties }
    property Patente: IXMLPatenteType read Get_Patente;
    property Auto: IXMLAutoType read Get_Auto;
    property ConSeguro: IXMLConSeguroType read Get_ConSeguro;
    property SinSeguro: IXMLSinSeguroType read Get_SinSeguro;
    property Amo: IXMLAmoType read Get_Amo;
    property Dire: IXMLDireType read Get_Dire;
    property Amorep: IXMLAmorepType read Get_Amorep;
    property TituloAnot: WideString read Get_TituloAnot write Set_TituloAnot;
    property Anotacion1: IXMLAnotacion1Type read Get_Anotacion1;
    property TituloSoli: IXMLTituloSoliType read Get_TituloSoli;
    property Solicitud: IXMLSolicitudType read Get_Solicitud;
    property SinSolicitudes: IXMLSinSolicitudesType read Get_SinSolicitudes;
    property TituloSub: WideString read Get_TituloSub write Set_TituloSub;
    property Subinscripcion: IXMLSubinscripcionTypeList read Get_Subinscripcion;
    property TituloAmos: WideString read Get_TituloAmos write Set_TituloAmos;
    property Rep: IXMLRepType read Get_Rep;
    property AmoAnt: IXMLAmoAntType read Get_AmoAnt;
    property AmoCOM: IXMLAmoCOMType read Get_AmoCOM;
  end;

{ IXMLPatenteType }

  IXMLPatenteType = interface(IXMLNode)
    ['{C085DE2F-9A2F-42E6-ACEC-7A0797D9B346}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
  end;

{ IXMLAutoType }

  IXMLAutoType = interface(IXMLNode)
    ['{D70553DE-5E0F-4F05-9B94-27DF27E2E8D9}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    function Get_Fieldname5: WideString;
    function Get_Fieldname6: WideString;
    function Get_Fieldname7: WideString;
    function Get_Fieldname8: WideString;
    function Get_Fieldname9: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    procedure Set_Fieldname5(Value: WideString);
    procedure Set_Fieldname6(Value: WideString);
    procedure Set_Fieldname7(Value: WideString);
    procedure Set_Fieldname8(Value: WideString);
    procedure Set_Fieldname9(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
    property Fieldname4: WideString read Get_Fieldname4 write Set_Fieldname4;
    property Fieldname5: WideString read Get_Fieldname5 write Set_Fieldname5;
    property Fieldname6: WideString read Get_Fieldname6 write Set_Fieldname6;
    property Fieldname7: WideString read Get_Fieldname7 write Set_Fieldname7;
    property Fieldname8: WideString read Get_Fieldname8 write Set_Fieldname8;
    property Fieldname9: WideString read Get_Fieldname9 write Set_Fieldname9;
  end;

{ IXMLConSeguroType }

  IXMLConSeguroType = interface(IXMLNode)
    ['{FDC134CC-1C5A-45D7-8C44-265614A2D286}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
  end;

{ IXMLSinSeguroType }

  IXMLSinSeguroType = interface(IXMLNode)
    ['{96A1F37C-4E12-4452-9C1E-B49C57DB2BBA}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
  end;

{ IXMLAmoType }

  IXMLAmoType = interface(IXMLNode)
    ['{59749E2A-018D-44A8-9B58-63DB8D3F5348}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
    property Fieldname4: WideString read Get_Fieldname4 write Set_Fieldname4;
  end;

{ IXMLDireType }

  IXMLDireType = interface(IXMLNode)
    ['{15EEAAA0-AA75-491A-9C77-570A0A4552A9}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    function Get_Fieldname5: WideString;
    function Get_Fieldname6: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    procedure Set_Fieldname5(Value: WideString);
    procedure Set_Fieldname6(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
    property Fieldname4: WideString read Get_Fieldname4 write Set_Fieldname4;
    property Fieldname5: WideString read Get_Fieldname5 write Set_Fieldname5;
    property Fieldname6: WideString read Get_Fieldname6 write Set_Fieldname6;
  end;

{ IXMLAmorepType }

  IXMLAmorepType = interface(IXMLNode)
    ['{27BF5EED-6D1F-4810-9A20-A4A1519C62E8}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
    property Fieldname4: WideString read Get_Fieldname4 write Set_Fieldname4;
  end;

{ IXMLAnotacion1Type }

  IXMLAnotacion1Type = interface(IXMLNode)
    ['{71186F7D-68AA-4824-9F75-496A0AED6594}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    function Get_Fieldname5: WideString;
    function Get_Fieldname6: WideString;
    function Get_Fieldname7: WideString;
    function Get_Fieldname8: WideString;
    function Get_Fieldname9: WideString;
    function Get_Fieldname10: WideString;
    function Get_Fieldname11: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    procedure Set_Fieldname5(Value: WideString);
    procedure Set_Fieldname6(Value: WideString);
    procedure Set_Fieldname7(Value: WideString);
    procedure Set_Fieldname8(Value: WideString);
    procedure Set_Fieldname9(Value: WideString);
    procedure Set_Fieldname10(Value: WideString);
    procedure Set_Fieldname11(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
    property Fieldname4: WideString read Get_Fieldname4 write Set_Fieldname4;
    property Fieldname5: WideString read Get_Fieldname5 write Set_Fieldname5;
    property Fieldname6: WideString read Get_Fieldname6 write Set_Fieldname6;
    property Fieldname7: WideString read Get_Fieldname7 write Set_Fieldname7;
    property Fieldname8: WideString read Get_Fieldname8 write Set_Fieldname8;
    property Fieldname9: WideString read Get_Fieldname9 write Set_Fieldname9;
    property Fieldname10: WideString read Get_Fieldname10 write Set_Fieldname10;
    property Fieldname11: WideString read Get_Fieldname11 write Set_Fieldname11;
  end;

{ IXMLTituloSoliType }

  IXMLTituloSoliType = interface(IXMLNode)
    ['{35BBCB5A-6927-4B69-BEE6-B84E07479B2F}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
  end;

{ IXMLSolicitudType }

  IXMLSolicitudType = interface(IXMLNode)
    ['{F2C55AB1-8D62-4FA2-BE44-505F56044B2E}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
  end;

{ IXMLSinSolicitudesType }

  IXMLSinSolicitudesType = interface(IXMLNode)
    ['{82DAE516-036E-46D6-9C8D-16107FFD6B55}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
  end;

{ IXMLSubinscripcionType }

  IXMLSubinscripcionType = interface(IXMLNode)
    ['{11B56CFB-6FE3-4DA1-86CF-05AEED8AF1E1}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
  end;

{ IXMLSubinscripcionTypeList }

  IXMLSubinscripcionTypeList = interface(IXMLNodeCollection)
    ['{53B11386-D87C-4EB8-9F04-884FDB3C0336}']
    { Methods & Properties }
    function Add: IXMLSubinscripcionType;
    function Insert(const Index: Integer): IXMLSubinscripcionType;
    function Get_Item(Index: Integer): IXMLSubinscripcionType;
    property Items[Index: Integer]: IXMLSubinscripcionType read Get_Item; default;
  end;

{ IXMLRepType }

  IXMLRepType = interface(IXMLNode)
    ['{FC1BAF7B-D967-4732-B1F1-FED1BB5A92EC}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
  end;

{ IXMLAmoAntType }

  IXMLAmoAntType = interface(IXMLNode)
    ['{F09F2424-1739-44DB-B9E7-C14209B96465}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
  end;

{ IXMLAmoCOMType }

  IXMLAmoCOMType = interface(IXMLNode)
    ['{C1ED6BB4-A9FA-4F1C-8FCB-7DE2C3AC6820}']
    { Property Accessors }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    { Methods & Properties }
    property Fieldname1: WideString read Get_Fieldname1 write Set_Fieldname1;
    property Fieldname2: WideString read Get_Fieldname2 write Set_Fieldname2;
    property Fieldname3: WideString read Get_Fieldname3 write Set_Fieldname3;
    property Fieldname4: WideString read Get_Fieldname4 write Set_Fieldname4;
  end;

{ Forward Decls }

  TXMLCertxmlType = class;
  TXMLPatenteType = class;
  TXMLAutoType = class;
  TXMLConSeguroType = class;
  TXMLSinSeguroType = class;
  TXMLAmoType = class;
  TXMLDireType = class;
  TXMLAmorepType = class;
  TXMLAnotacion1Type = class;
  TXMLTituloSoliType = class;
  TXMLSolicitudType = class;
  TXMLSinSolicitudesType = class;
  TXMLSubinscripcionType = class;
  TXMLSubinscripcionTypeList = class;
  TXMLRepType = class;
  TXMLAmoAntType = class;
  TXMLAmoCOMType = class;

{ TXMLCertxmlType }

  TXMLCertxmlType = class(TXMLNode, IXMLCertxmlType)
  private
    FSubinscripcion: IXMLSubinscripcionTypeList;
  protected
    { IXMLCertxmlType }
    function Get_Patente: IXMLPatenteType;
    function Get_Auto: IXMLAutoType;
    function Get_ConSeguro: IXMLConSeguroType;
    function Get_SinSeguro: IXMLSinSeguroType;
    function Get_Amo: IXMLAmoType;
    function Get_Dire: IXMLDireType;
    function Get_Amorep: IXMLAmorepType;
    function Get_TituloAnot: WideString;
    function Get_Anotacion1: IXMLAnotacion1Type;
    function Get_TituloSoli: IXMLTituloSoliType;
    function Get_Solicitud: IXMLSolicitudType;
    function Get_SinSolicitudes: IXMLSinSolicitudesType;
    function Get_TituloSub: WideString;
    function Get_Subinscripcion: IXMLSubinscripcionTypeList;
    function Get_TituloAmos: WideString;
    function Get_Rep: IXMLRepType;
    function Get_AmoAnt: IXMLAmoAntType;
    function Get_AmoCOM: IXMLAmoCOMType;
    procedure Set_TituloAnot(Value: WideString);
    procedure Set_TituloSub(Value: WideString);
    procedure Set_TituloAmos(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPatenteType }

  TXMLPatenteType = class(TXMLNode, IXMLPatenteType)
  protected
    { IXMLPatenteType }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
  end;

{ TXMLAutoType }

  TXMLAutoType = class(TXMLNode, IXMLAutoType)
  protected
    { IXMLAutoType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    function Get_Fieldname5: WideString;
    function Get_Fieldname6: WideString;
    function Get_Fieldname7: WideString;
    function Get_Fieldname8: WideString;
    function Get_Fieldname9: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    procedure Set_Fieldname5(Value: WideString);
    procedure Set_Fieldname6(Value: WideString);
    procedure Set_Fieldname7(Value: WideString);
    procedure Set_Fieldname8(Value: WideString);
    procedure Set_Fieldname9(Value: WideString);
  end;

{ TXMLConSeguroType }

  TXMLConSeguroType = class(TXMLNode, IXMLConSeguroType)
  protected
    { IXMLConSeguroType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
  end;

{ TXMLSinSeguroType }

  TXMLSinSeguroType = class(TXMLNode, IXMLSinSeguroType)
  protected
    { IXMLSinSeguroType }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
  end;

{ TXMLAmoType }

  TXMLAmoType = class(TXMLNode, IXMLAmoType)
  protected
    { IXMLAmoType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
  end;

{ TXMLDireType }

  TXMLDireType = class(TXMLNode, IXMLDireType)
  protected
    { IXMLDireType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    function Get_Fieldname5: WideString;
    function Get_Fieldname6: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    procedure Set_Fieldname5(Value: WideString);
    procedure Set_Fieldname6(Value: WideString);
  end;

{ TXMLAmorepType }

  TXMLAmorepType = class(TXMLNode, IXMLAmorepType)
  protected
    { IXMLAmorepType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
  end;

{ TXMLAnotacion1Type }

  TXMLAnotacion1Type = class(TXMLNode, IXMLAnotacion1Type)
  protected
    { IXMLAnotacion1Type }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    function Get_Fieldname5: WideString;
    function Get_Fieldname6: WideString;
    function Get_Fieldname7: WideString;
    function Get_Fieldname8: WideString;
    function Get_Fieldname9: WideString;
    function Get_Fieldname10: WideString;
    function Get_Fieldname11: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
    procedure Set_Fieldname5(Value: WideString);
    procedure Set_Fieldname6(Value: WideString);
    procedure Set_Fieldname7(Value: WideString);
    procedure Set_Fieldname8(Value: WideString);
    procedure Set_Fieldname9(Value: WideString);
    procedure Set_Fieldname10(Value: WideString);
    procedure Set_Fieldname11(Value: WideString);
  end;

{ TXMLTituloSoliType }

  TXMLTituloSoliType = class(TXMLNode, IXMLTituloSoliType)
  protected
    { IXMLTituloSoliType }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
  end;

{ TXMLSolicitudType }

  TXMLSolicitudType = class(TXMLNode, IXMLSolicitudType)
  protected
    { IXMLSolicitudType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
  end;

{ TXMLSinSolicitudesType }

  TXMLSinSolicitudesType = class(TXMLNode, IXMLSinSolicitudesType)
  protected
    { IXMLSinSolicitudesType }
    function Get_Fieldname1: WideString;
    procedure Set_Fieldname1(Value: WideString);
  end;

{ TXMLSubinscripcionType }

  TXMLSubinscripcionType = class(TXMLNode, IXMLSubinscripcionType)
  protected
    { IXMLSubinscripcionType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
  end;

{ TXMLSubinscripcionTypeList }

  TXMLSubinscripcionTypeList = class(TXMLNodeCollection, IXMLSubinscripcionTypeList)
  protected
    { IXMLSubinscripcionTypeList }
    function Add: IXMLSubinscripcionType;
    function Insert(const Index: Integer): IXMLSubinscripcionType;
    function Get_Item(Index: Integer): IXMLSubinscripcionType;
  end;

{ TXMLRepType }

  TXMLRepType = class(TXMLNode, IXMLRepType)
  protected
    { IXMLRepType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
  end;

{ TXMLAmoAntType }

  TXMLAmoAntType = class(TXMLNode, IXMLAmoAntType)
  protected
    { IXMLAmoAntType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
  end;

{ TXMLAmoCOMType }

  TXMLAmoCOMType = class(TXMLNode, IXMLAmoCOMType)
  protected
    { IXMLAmoCOMType }
    function Get_Fieldname1: WideString;
    function Get_Fieldname2: WideString;
    function Get_Fieldname3: WideString;
    function Get_Fieldname4: WideString;
    procedure Set_Fieldname1(Value: WideString);
    procedure Set_Fieldname2(Value: WideString);
    procedure Set_Fieldname3(Value: WideString);
    procedure Set_Fieldname4(Value: WideString);
  end;

{ Global Functions }

function Getcertxml(Doc: IXMLDocument): IXMLCertxmlType;
function Loadcertxml(const FileName: WideString): IXMLCertxmlType;
function Newcertxml: IXMLCertxmlType;

implementation

{ Global Functions }

function Getcertxml(Doc: IXMLDocument): IXMLCertxmlType;
begin
  Result := Doc.GetDocBinding('certxml', TXMLCertxmlType) as IXMLCertxmlType;
end;
function Loadcertxml(const FileName: WideString): IXMLCertxmlType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('certxml', TXMLCertxmlType) as IXMLCertxmlType;
end;

function Newcertxml: IXMLCertxmlType;
begin
  Result := NewXMLDocument.GetDocBinding('certxml', TXMLCertxmlType) as IXMLCertxmlType;
end;

{ TXMLCertxmlType }

procedure TXMLCertxmlType.AfterConstruction;
begin
  RegisterChildNode('Patente', TXMLPatenteType);
  RegisterChildNode('Auto', TXMLAutoType);
  RegisterChildNode('ConSeguro', TXMLConSeguroType);
  RegisterChildNode('SinSeguro', TXMLSinSeguroType);
  RegisterChildNode('amo', TXMLAmoType);
  RegisterChildNode('dire', TXMLDireType);
  RegisterChildNode('amorep', TXMLAmorepType);
  RegisterChildNode('anotacion1', TXMLAnotacion1Type);
  RegisterChildNode('TituloSoli', TXMLTituloSoliType);
  RegisterChildNode('solicitud', TXMLSolicitudType);
  RegisterChildNode('sinSolicitudes', TXMLSinSolicitudesType);
  RegisterChildNode('Subinscripcion', TXMLSubinscripcionType);
  RegisterChildNode('rep', TXMLRepType);
  RegisterChildNode('amoAnt', TXMLAmoAntType);
  RegisterChildNode('amoCOM', TXMLAmoCOMType);
  FSubinscripcion := CreateCollection(TXMLSubinscripcionTypeList, IXMLSubinscripcionType, 'Subinscripcion') as IXMLSubinscripcionTypeList;
  inherited;
end;

function TXMLCertxmlType.Get_Patente: IXMLPatenteType;
begin
  Result := ChildNodes['Patente'] as IXMLPatenteType;
end;

function TXMLCertxmlType.Get_Auto: IXMLAutoType;
begin
  Result := ChildNodes['Auto'] as IXMLAutoType;
end;

function TXMLCertxmlType.Get_ConSeguro: IXMLConSeguroType;
begin
  Result := ChildNodes['ConSeguro'] as IXMLConSeguroType;
end;

function TXMLCertxmlType.Get_SinSeguro: IXMLSinSeguroType;
begin
  Result := ChildNodes['SinSeguro'] as IXMLSinSeguroType;
end;

function TXMLCertxmlType.Get_Amo: IXMLAmoType;
begin
  Result := ChildNodes['amo'] as IXMLAmoType;
end;

function TXMLCertxmlType.Get_Dire: IXMLDireType;
begin
  Result := ChildNodes['dire'] as IXMLDireType;
end;

function TXMLCertxmlType.Get_Amorep: IXMLAmorepType;
begin
  Result := ChildNodes['amorep'] as IXMLAmorepType;
end;

function TXMLCertxmlType.Get_TituloAnot: WideString;
begin
  Result := ChildNodes['TituloAnot'].Text;
end;

procedure TXMLCertxmlType.Set_TituloAnot(Value: WideString);
begin
  ChildNodes['TituloAnot'].NodeValue := Value;
end;

function TXMLCertxmlType.Get_Anotacion1: IXMLAnotacion1Type;
begin
  Result := ChildNodes['anotacion1'] as IXMLAnotacion1Type;
end;

function TXMLCertxmlType.Get_TituloSoli: IXMLTituloSoliType;
begin
  Result := ChildNodes['TituloSoli'] as IXMLTituloSoliType;
end;

function TXMLCertxmlType.Get_Solicitud: IXMLSolicitudType;
begin
  Result := ChildNodes['solicitud'] as IXMLSolicitudType;
end;

function TXMLCertxmlType.Get_SinSolicitudes: IXMLSinSolicitudesType;
begin
  Result := ChildNodes['sinSolicitudes'] as IXMLSinSolicitudesType;
end;

function TXMLCertxmlType.Get_TituloSub: WideString;
begin
  Result := ChildNodes['TituloSub'].Text;
end;

procedure TXMLCertxmlType.Set_TituloSub(Value: WideString);
begin
  ChildNodes['TituloSub'].NodeValue := Value;
end;

function TXMLCertxmlType.Get_Subinscripcion: IXMLSubinscripcionTypeList;
begin
  Result := FSubinscripcion;
end;

function TXMLCertxmlType.Get_TituloAmos: WideString;
begin
  Result := ChildNodes['TituloAmos'].Text;
end;

procedure TXMLCertxmlType.Set_TituloAmos(Value: WideString);
begin
  ChildNodes['TituloAmos'].NodeValue := Value;
end;

function TXMLCertxmlType.Get_Rep: IXMLRepType;
begin
  Result := ChildNodes['rep'] as IXMLRepType;
end;

function TXMLCertxmlType.Get_AmoAnt: IXMLAmoAntType;
begin
  Result := ChildNodes['amoAnt'] as IXMLAmoAntType;
end;

function TXMLCertxmlType.Get_AmoCOM: IXMLAmoCOMType;
begin
  Result := ChildNodes['amoCOM'] as IXMLAmoCOMType;
end;

{ TXMLPatenteType }

function TXMLPatenteType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLPatenteType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

{ TXMLAutoType }

function TXMLAutoType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLAutoType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLAutoType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLAutoType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname4: WideString;
begin
  Result := ChildNodes['fieldname4'].Text;
end;

procedure TXMLAutoType.Set_Fieldname4(Value: WideString);
begin
  ChildNodes['fieldname4'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname5: WideString;
begin
  Result := ChildNodes['fieldname5'].Text;
end;

procedure TXMLAutoType.Set_Fieldname5(Value: WideString);
begin
  ChildNodes['fieldname5'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname6: WideString;
begin
  Result := ChildNodes['fieldname6'].Text;
end;

procedure TXMLAutoType.Set_Fieldname6(Value: WideString);
begin
  ChildNodes['fieldname6'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname7: WideString;
begin
  Result := ChildNodes['fieldname7'].Text;
end;

procedure TXMLAutoType.Set_Fieldname7(Value: WideString);
begin
  ChildNodes['fieldname7'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname8: WideString;
begin
  Result := ChildNodes['fieldname8'].Text;
end;

procedure TXMLAutoType.Set_Fieldname8(Value: WideString);
begin
  ChildNodes['fieldname8'].NodeValue := Value;
end;

function TXMLAutoType.Get_Fieldname9: WideString;
begin
  Result := ChildNodes['fieldname9'].Text;
end;

procedure TXMLAutoType.Set_Fieldname9(Value: WideString);
begin
  ChildNodes['fieldname9'].NodeValue := Value;
end;

{ TXMLConSeguroType }

function TXMLConSeguroType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLConSeguroType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLConSeguroType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLConSeguroType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLConSeguroType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLConSeguroType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

{ TXMLSinSeguroType }

function TXMLSinSeguroType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLSinSeguroType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

{ TXMLAmoType }

function TXMLAmoType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLAmoType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLAmoType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLAmoType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLAmoType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLAmoType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

function TXMLAmoType.Get_Fieldname4: WideString;
begin
  Result := ChildNodes['fieldname4'].Text;
end;

procedure TXMLAmoType.Set_Fieldname4(Value: WideString);
begin
  ChildNodes['fieldname4'].NodeValue := Value;
end;

{ TXMLDireType }

function TXMLDireType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLDireType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLDireType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLDireType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLDireType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLDireType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

function TXMLDireType.Get_Fieldname4: WideString;
begin
  Result := ChildNodes['fieldname4'].Text;
end;

procedure TXMLDireType.Set_Fieldname4(Value: WideString);
begin
  ChildNodes['fieldname4'].NodeValue := Value;
end;

function TXMLDireType.Get_Fieldname5: WideString;
begin
  Result := ChildNodes['fieldname5'].Text;
end;

procedure TXMLDireType.Set_Fieldname5(Value: WideString);
begin
  ChildNodes['fieldname5'].NodeValue := Value;
end;

function TXMLDireType.Get_Fieldname6: WideString;
begin
  Result := ChildNodes['fieldname6'].Text;
end;

procedure TXMLDireType.Set_Fieldname6(Value: WideString);
begin
  ChildNodes['fieldname6'].NodeValue := Value;
end;

{ TXMLAmorepType }

function TXMLAmorepType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLAmorepType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLAmorepType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLAmorepType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLAmorepType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLAmorepType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

function TXMLAmorepType.Get_Fieldname4: WideString;
begin
  Result := ChildNodes['fieldname4'].Text;
end;

procedure TXMLAmorepType.Set_Fieldname4(Value: WideString);
begin
  ChildNodes['fieldname4'].NodeValue := Value;
end;

{ TXMLAnotacion1Type }

function TXMLAnotacion1Type.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname4: WideString;
begin
  Result := ChildNodes['fieldname4'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname4(Value: WideString);
begin
  ChildNodes['fieldname4'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname5: WideString;
begin
  Result := ChildNodes['fieldname5'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname5(Value: WideString);
begin
  ChildNodes['fieldname5'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname6: WideString;
begin
  Result := ChildNodes['fieldname6'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname6(Value: WideString);
begin
  ChildNodes['fieldname6'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname7: WideString;
begin
  Result := ChildNodes['fieldname7'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname7(Value: WideString);
begin
  ChildNodes['fieldname7'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname8: WideString;
begin
  Result := ChildNodes['fieldname8'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname8(Value: WideString);
begin
  ChildNodes['fieldname8'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname9: WideString;
begin
  Result := ChildNodes['fieldname9'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname9(Value: WideString);
begin
  ChildNodes['fieldname9'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname10: WideString;
begin
  Result := ChildNodes['fieldname10'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname10(Value: WideString);
begin
  ChildNodes['fieldname10'].NodeValue := Value;
end;

function TXMLAnotacion1Type.Get_Fieldname11: WideString;
begin
  Result := ChildNodes['fieldname11'].Text;
end;

procedure TXMLAnotacion1Type.Set_Fieldname11(Value: WideString);
begin
  ChildNodes['fieldname11'].NodeValue := Value;
end;

{ TXMLTituloSoliType }

function TXMLTituloSoliType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLTituloSoliType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

{ TXMLSolicitudType }

function TXMLSolicitudType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLSolicitudType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLSolicitudType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLSolicitudType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

{ TXMLSinSolicitudesType }

function TXMLSinSolicitudesType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLSinSolicitudesType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

{ TXMLSubinscripcionType }

function TXMLSubinscripcionType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLSubinscripcionType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLSubinscripcionType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLSubinscripcionType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLSubinscripcionType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLSubinscripcionType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

{ TXMLSubinscripcionTypeList }

function TXMLSubinscripcionTypeList.Add: IXMLSubinscripcionType;
begin
  Result := AddItem(-1) as IXMLSubinscripcionType;
end;

function TXMLSubinscripcionTypeList.Insert(const Index: Integer): IXMLSubinscripcionType;
begin
  Result := AddItem(Index) as IXMLSubinscripcionType;
end;

function TXMLSubinscripcionTypeList.Get_Item(Index: Integer): IXMLSubinscripcionType;
begin
  Result := List[Index] as IXMLSubinscripcionType;
end;

{ TXMLRepType }

function TXMLRepType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLRepType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLRepType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLRepType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLRepType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLRepType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

{ TXMLAmoAntType }

function TXMLAmoAntType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLAmoAntType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLAmoAntType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLAmoAntType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

{ TXMLAmoCOMType }

function TXMLAmoCOMType.Get_Fieldname1: WideString;
begin
  Result := ChildNodes['fieldname1'].Text;
end;

procedure TXMLAmoCOMType.Set_Fieldname1(Value: WideString);
begin
  ChildNodes['fieldname1'].NodeValue := Value;
end;

function TXMLAmoCOMType.Get_Fieldname2: WideString;
begin
  Result := ChildNodes['fieldname2'].Text;
end;

procedure TXMLAmoCOMType.Set_Fieldname2(Value: WideString);
begin
  ChildNodes['fieldname2'].NodeValue := Value;
end;

function TXMLAmoCOMType.Get_Fieldname3: WideString;
begin
  Result := ChildNodes['fieldname3'].Text;
end;

procedure TXMLAmoCOMType.Set_Fieldname3(Value: WideString);
begin
  ChildNodes['fieldname3'].NodeValue := Value;
end;

function TXMLAmoCOMType.Get_Fieldname4: WideString;
begin
  Result := ChildNodes['fieldname4'].Text;
end;

procedure TXMLAmoCOMType.Set_Fieldname4(Value: WideString);
begin
  ChildNodes['fieldname4'].NodeValue := Value;
end;

end.
