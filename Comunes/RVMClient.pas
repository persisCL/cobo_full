{-----------------------------------------------------------------------------
 File Name: RVMClient.pas
 Author:    gcasais
 Date Created: 22/02/2005
 Language: ES-AR
 Description: Componente Cliente para conexi�n al RNVM

 Revision 1
 Author: nefernandez
 Date: 20/06/2007
 Language: ES-AR
 Description : Le agregu� un par�metro a la funci�n RequestLPData, para
 retornar el string del XML de respuesta

 Revision 2
 Author: lcanteros
 Date: 12/06/2008
 Language: ES-AR
 Description : se reemplazan los caracteres inv�lidos no soportados por el
 sistema de inspecci�n fiscal en el metodo RequestLPData (SS 704)
-----------------------------------------------------------------------------}
unit RVMClient;

interface

uses
  Windows, Messages, SysUtils, Classes, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, XMLDoc, XMLIntf, XMLDOM, RVMTypes,
  RVMBind, Util, UtilProc, IdAuthentication , IdAuthenticationManager, IdURI;
Const
    CLIENT_ID = 'DPS Automation S.A. RNVM Client 1.1';
type
  TRVMClient = class(TObject)
  private
    FUserName: AnsiString;
    FPassword: AnsiString;
    FServiceID: AnsiString;
    FServiceURL: AnsiString;
    FSaveDirectory: AnsiString;
    FSaveFileName: AnsiString;
    FServicePort: Integer;
    FHttpClient: TIdHTTP;
    FUseProxy: Boolean;
    FProxyServer: AnsiString;
    FProxyServerPort: Integer;
    FLicensePlateExists: Boolean;
    function GetUserName: String;
    procedure SetUsername(const Value: String);
    function GetPassWord: String;
    procedure SetPassWord(const Value: String);
    function GetServiceID: String;
    procedure SetServiceID(const Value: String);
    function GetServiceURL: String;
    procedure SetServiceURL(const Value: String);
    function GetServicePort: Integer;
    procedure SetServicePort(const Value: Integer);
    function GetSaveDirectory: String;
    procedure SetSaveDirectory(const Value: String);
    function GetSaveFileName: String;
    procedure SetSaveFileName(const Value: String);
    function GetProxyServer: String;
    procedure SetProxyServer(const Value: String);
    function GetProxyServerPort: Integer;
    procedure SetProxyServerPort(const Value: Integer);
    function GetLicensePlateExists: Boolean;
  protected
    procedure ConfigureClient;
    function FillData(RVMResp: TXMLDocument): TRVMInformation;
  public
    constructor Create;
    destructor  Destroy; override;
    function RequestLPData(LicensePlate: String; var RVMInformation: TRVMInformation; var ErrorText: String; var RespuestaRNVM: WideString):Boolean;
  published
    property Username: String read GetUserName write SetUsername;
    property Password: String read GetPassWord write SetPassWord;
    property ServiceID: String read GetServiceID write SetServiceID;
    property ServiceURL: String read GetServiceURL write SetServiceURL;
    property ServicePort: Integer read GetServicePort write SetServicePort;
    property SaveDirectory: String read GetSaveDirectory write SetSaveDirectory;
    property FileName: String read GetSaveFileName write SetSaveFileName;
    property RequestUseProxy: Boolean read FUseproxy write FUseProxy;
    property RequestProxyServer: String read GetProxyServer write SetProxyServer;
    property RequestProxyServerPort: Integer read GetProxyServerPort write SetProxyServerPort;
    property LicensePlateExists: Boolean read GetLicensePlateExists;
  end;


implementation

uses PeaProcs;

{ TRVMClient }
{-----------------------------------------------------------------------------
  Function Name: ConfigureClient
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TRVMClient.ConfigureClient;
begin
    FHttpClient.Request.BasicAuthentication := True;
    FHttpClient.Request.Username := UserName;
    FHttpClient.Request.Password := Password;
    FHttpClient.Request.Host := ServiceUrl;
    FHttpClient.Request.ContentType := 'application/x-www-form-urlencoded';
    if RequestUseProxy then begin
        FHttpClient.ProxyParams.ProxyServer := RequestProxyServer; // CN Proxy: '172.16.1.125';
        FHttpClient.ProxyParams.ProxyPort   := RequestProxyServerPort; //CN Proxy Port: 8080
    end else begin
        FHttpClient.ProxyParams.ProxyServer := '';
        FHttpClient.ProxyParams.ProxyPort   := 0;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: Create
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
constructor TRVMClient.Create;
begin
    FHttpClient := TIdHTTP.Create;
end;
{-----------------------------------------------------------------------------
  Function Name: Destroy
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
destructor TRVMClient.Destroy;
begin
    FreeAndNil(FHttpClient);
    inherited;
end;
{-----------------------------------------------------------------------------
  Function Name: GetPassWord
  Author:    gcasais
  Date Created: 25/02/2005
  Description:
  Parameters: None
  Return Value: String
-----------------------------------------------------------------------------}
function TRVMClient.GetPassWord: String;
begin
    Result := FPassWord;
end;

function TRVMClient.GetServiceID: String;
begin
    Result := FServiceID;
end;

function TRVMClient.GetServicePort: Integer;
begin
    Result := FServicePort;
end;

function TRVMClient.GetServiceURL: String;
begin
    Result := FServiceURL;
end;

function TRVMClient.GetUserName: String;
begin
    Result := FUserName;
end;

{-----------------------------------------------------------------------------
  Function Name: RequestLPData
  Author:    gcasais
  Date Created: 22/02/2005
  Description: Obtiene la informacion del RNVM para una patente dada
  Parameters: LicensePlate: String
  Return Value: Boolean
  Revision : 1
    Author : lcanteros
    Date : 12/06/2008
    Description : se eliminan caracteres no soportados por el sistema de inspecci�n
    fiscal (SS 704).
  Revision : 2
    Author : nefernandez
    Date : 22/08/2008
    Description : SS 704: No se estaban reemplazando los caracteres no soportados, porque
    en el string de respuesta del RVNM dichos caracteres vienen en formato HTML (por ejemplo
    el ap�strofe viene indicado como &apos;). Entonces lo que se hizo fue despues de escribir el
    XML a disco (esta acci�n hace la conversi�n autom�tica del "&apos;" al caracter correspondiente),
    leer el contenido del archivo y se ejecutar la rutina de reemplazo de caracteres no validos;
    y finalmente el resultado se guarda en la base de datos.
-----------------------------------------------------------------------------}
function TRVMClient.RequestLPData(LicensePlate: String; var RVMInformation: TRVMInformation; var ErrorText: String; var RespuestaRNVM: WideString):Boolean;
resourcestring
    MSG_ERROR = 'Error';
    SAVE_ERROR = 'Error guardando el XML de respuesta ';
    XML_INIT_ERROR = 'Error Activando el Contenedor XML';
    ERROR_SENDING_REQUEST = 'Error Sending Request';
    ERROR_NO_LP = 'Debe especificarse una patente para realizar una consulta';
    ERROR_LP_UNKNOWN = 'Consulta realizada pero la base de datos del RNVM no conoce la patente %s';
var
    AskXML: String;
    Response, ResponseCorregida: TStringStream;
    RespXML: TXMLDocument;
    TempString: TStringList;
    Buff, BuffOriginal: String;
const
    HARD_XML = '<srcei><datos><user>%s</user><tipo>RVMDIR</tipo><param>%s</param></datos></srcei>';
begin
    Result := False;
    ErrorText := '';
    RespuestaRNVM := '';
    FLicensePlateExists := False;
    if Trim(LicensePlate) = '' then begin
        ErrorText := ERROR_NO_LP;
        Exit;
    end;
    //Preparamos la pregunta y la respuesta
    LicensePlate := UpperCase(LicensePlate);
    TempString  := TStringList.Create;
    Response    := TStringStream.Create('');
    ConfigureClient;
    AskXML := Format(HARD_XML, [ServiceID, LicensePlate]);
    TempString.Add(AskXML);
    RespXML := TXMLDocument.Create(nil);
    RespXML.DOMVendor := GetDOMVendor('MSXML');
    RespXML.Options := RespXML.Options + [doNodeAutoIndent];
    try
        try
            try
                RespXML.Active := True;
            except
                on e: exception do begin
                    ErrorText:= XML_INIT_ERROR + Space(1) +  e.Message;
                    Exit;
                end;
            end;
            // Mandamos la pregunta
            FHttpClient.Post(ServiceURL, TempString, Response);
            // Esta es una manganeta para determinar si la patente
            // por la que se pregunto no existe en la BD del RVM
            // debido a que no existe un mensaje de
            // de respuesta standard para este problema.
            // Por lo tanto si en la respuesta no existe
            // la patente que estoy buscando asumimos
            // que no existe en el RNVM
            Buff:= Response.DataString;
            if Pos(LicensePlate, Buff) <= 0 then begin
               // Todo anduvo bien hasta ac� pero la patente no existe
               // No seguimos adelante.
               ErrorText := Format(ERROR_LP_UNKNOWN, [LicensePlate]);
               // Revision 1: Se toma como respuesta el string del Response
               ReemplazarCaracteresNoSoportados(Buff);
               RespuestaRNVM := Buff;
               Result := True;
               Exit;
            end;
            // Encontramos la patente en la respuesta
            // Seguimos adelante para extraer los datos
            FLicensePlateExists := True;
            BuffOriginal := Buff;
            Buff := '';
            // Seguimos cargando la respuesta a un Doc XML
            RespXML.LoadFromStream(Response);
            // Revision 1: Se obtiene el string del XML
            ///RespuestaRNVM := RespXML.XML.GetText;
            // Guardamos el XML de respuesta que recibimos a disco si podemos
            // Si no podemos seguimos adelante pero dejamos el texto del error
            // para que alguien lo muestre
            if DirectoryExists(SaveDirectory) then begin
                try
                    // se graba la respuesta original sin correci�n
                    RespXML.SaveToFile(SaveDirectory + FileName);
                except
                    on e: Exception do begin
                        ErrorText := SAVE_ERROR + Space(1) + SaveDirectory +
                          Space(1) + FileName + Space(1) + e.Message;
                    end;
                end;
            end;
            // se reemplazan los caracteres no soportados por el sistema de inspeccion fiscal
            //Lo parseamos..con la correci�n de caracteres
            //Revision 2
            Buff := FileToString(SaveDirectory + FileName);
            ReemplazarCaracteresNoSoportados(Buff);
            ResponseCorregida := TStringStream.Create(Buff);
            RespXML.XML.Clear;
            RespXML.LoadFromStream(ResponseCorregida);
            // obtengo la respuesta corregida
            RespuestaRNVM := RespXML.XML.GetText;
            RVMInformation := FillData(RespXML);
            //Listo!
            Result := True;
        except
            on e: Exception do begin
                ErrorText := ERROR_SENDING_REQUEST + Space(1) + e.Message;
            end;
        end;
    finally
        FreeAndNil(TempString);
        FreeAndNil(Response);
        if ResponseCorregida <> nil then FreeAndNil(ResponseCorregida);
    end;
end;


procedure TRVMClient.SetPassWord(const Value: String);
begin
    if Value = FPassWord then Exit;
    FPassWord := Value;
end;

procedure TRVMClient.SetServiceID(const Value: String);
begin
    if Value  = FServiceID then Exit;
    FServiceID := Value;
end;

procedure TRVMClient.SetServicePort(const Value: Integer);
begin
    if Value = FServicePort then Exit;
    FServicePort := Value;
end;

procedure TRVMClient.SetServiceURL(const Value: String);
begin
    if Value = FServiceURL then Exit;
    FServiceURL := Value;
end;

procedure TRVMClient.SetUsername(const Value: String);
begin
    if Value = FUserName then exit;
    FUsername := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: FillData
  Author:    gcasais
  Date Created: 22/02/2005
  Description: Convierte la respuesta en formato XML al record con la data
               Para devolverlo como respuesta 
  Parameters: RVMResponse: TXMLDocument
  Return Value: TRVMInformation
-----------------------------------------------------------------------------}
function TRVMClient.FillData(RVMResp: TXMLDocument): TRVMInformation;
    // Esta funci�n reemplaza un caracter indicado por otro
    // Si se encuentra en un string
    function Reemplazar(Texto, Que, Por: String) : String;
    begin
        Result := StringReplace(Texto, Que, UpperCase(Por), [rfReplaceAll, rfIgnoreCase]);
    end;
var
    Doc: IXMLCertxmlType;
begin
    if not RVMResp.Active then RVMResp.Active := True;
    //Hacemos el bind al documento (DOM)
    Doc := GetCertXML(RVMResp);
    //Llenamos nuestro record
    // Datos del veh�culo
    try
        Result.RequestedLicensePlate                := Doc.Patente.Fieldname1;
        Result.VehicleInformation.Category          := Doc.Auto.Fieldname1;
        Result.VehicleInformation.Manufacturer      := Doc.Auto.Fieldname2;
        Result.VehicleInformation.Model             := Doc.Auto.Fieldname3;
        Result.VehicleInformation.Color             := Doc.Auto.Fieldname4;
        Result.VehicleInformation.EngineID          := Doc.Auto.fieldname5;
        Result.VehicleInformation.ChassisID         := Doc.Auto.Fieldname6;
        Result.VehicleInformation.Serial            := Doc.Auto.Fieldname7;
        Result.VehicleInformation.VINNumber         := Doc.Auto.Fieldname8;
        Result.VehicleInformation.Year              := Doc.Auto.Fieldname9;
    except
        raise exception.Create('Error obteniendo los datos del veh�culo');
    end;
    // Datos del seguro, si los hubiera
    try
        Result.InsuranceInformation.Entity          := Doc.ConSeguro.Fieldname1;
        Result.InsuranceInformation.ContractID      := Doc.ConSeguro.Fieldname2;
        Result.InsuranceInformation.ExpirationDate  := Doc.ConSeguro.Fieldname3;
    except
        on e: Exception do begin
            Result.InsuranceInformation.Entity  := 'No existen datos referidos al seguro';
        end;
    end;
    // Datos del titular
    try
        Result.OwnerInformation.OwnerID1            := Doc.Amo.Fieldname1;
        Result.OwnerInformation.OwnerName1          := Reemplazar(Doc.Amo.Fieldname2, '@', '�');
    except
        Result.OwnerInformation.OwnerID1 := 'No se pudieron obtener todos los datos del titular';
    end;
    // Datos del domicilio
    try
        Result.AddressInformation.Street     := Reemplazar(Doc.Dire.Fieldname1, '@', '�');
        Result.AddressInformation.DoorNumber := Doc.Dire.FieldName2;
        Result.AddressInformation.Letter     := Reemplazar(Doc.Dire.Fieldname3, '@', '�');
        Result.AddressInformation.OtherInfo  := Reemplazar(Doc.Dire.Fieldname4, '@', '�');
        Result.AddressInformation.Commune    := Reemplazar(Doc.Dire.Fieldname5, '@', '�');
        Result.AddressInformation.Field6     := Reemplazar(Doc.Dire.Fieldname6, '@', '�');
    except
        on e: Exception do begin
            Result.AddressInformation.Street := 'No se pudieron obtener los datos del domicilio' + CRLF +
              e.Message;
        end;
    end;

    // Registracion del Vehiculo en el Registro Civil
    try
        Result.OwnerRegistration.AcquisitionDate    := Doc.Amorep.Fieldname1;
        Result.OwnerRegistration.Registration       := Doc.Amorep.Fieldname2;
        Result.OwnerRegistration.RegistrationNumber := Doc.Amorep.Fieldname3;
        Result.OwnerRegistration.RegistrationDate   := Doc.Amorep.Fieldname4;
    except
        //Como no es relevante...
    end;


    // Anotaciones del RVM Para este veh�culo
    try
        Result.VehicleNotes := doc.TituloAnot;
    except
        Result.VehicleNotes := 'No hay anotaciones para el t�tulo de este veh�culo';
    end;

    // Detalle de las anotaciones, si las hubiera.
    try
        Result.DomainConstraints.Notas[1]:= Doc.Anotacion1.Fieldname1;
        Result.DomainConstraints.Notas[2]:= Doc.Anotacion1.Fieldname2;
        Result.DomainConstraints.Notas[3]:= Doc.Anotacion1.Fieldname3;
        Result.DomainConstraints.Notas[4]:= Doc.Anotacion1.Fieldname4;
        Result.DomainConstraints.Notas[5]:= Doc.Anotacion1.Fieldname5;
        Result.DomainConstraints.Notas[6]:= Doc.Anotacion1.Fieldname6;
        Result.DomainConstraints.Notas[7]:= Doc.Anotacion1.Fieldname7;
        Result.DomainConstraints.Notas[8]:= Doc.Anotacion1.Fieldname8;
        Result.DomainConstraints.Notas[9]:= Doc.Anotacion1.Fieldname9;
        Result.DomainConstraints.Notas[10]:= Doc.Anotacion1.Fieldname10;
        Result.DomainConstraints.Notas[11]:= Doc.Anotacion1.Fieldname11;

    except
        //Como no es relevante...
    end;

    try
        Result.DomainRequests.Nota1                 := doc.SinSolicitudes.Fieldname1;
        Result.DomainRequests.Nota2                 := doc.TituloAmos;
        Result.OthersOwnersInformation.OwnerID1     := doc.AmoAnt.Fieldname1;
        Result.OthersOwnersInformation.OwnerName1   := doc.AmoAnt.Fieldname2;
    except
        // No es relevante
    end;

    try
        Result.OwnerInformationCOM.Rut1     := Doc.AmoCOM.Fieldname1;
        Result.OwnerInformationCOM.Nombre1  := Doc.AmoCOM.Fieldname2;
        Result.OwnerInformationCOM.Rut2     := Doc.AmoCOM.Fieldname3;
        Result.OwnerInformationCOM.Nombre2  := Doc.AmoCOM.Fieldname4;
    except
        // No es relevante
    end;
end;

function TRVMClient.GetSaveDirectory: String;
begin
    Result := GoodDir(FSaveDirectory);
end;

procedure TRVMClient.SetSaveDirectory(const Value: String);
begin
    if Value = FSaveDirectory then Exit;
    FSaveDirectory := Value;
end;

function TRVMClient.GetSaveFileName: String;
begin
    Result := FSaveFilename;
end;

procedure TRVMClient.SetSaveFileName(const Value: String);
begin
    if Value = FSaveFileName then Exit;
    FSaveFileName := Value;
end;

function TRVMClient.GetProxyServer: String;
begin
    Result := FProxyServer;
end;

procedure TRVMClient.SetProxyServer(const Value: String);
begin
    if Value = FProxyServer then Exit;
    FProxyServer := Trim(Value);
end;

function TRVMClient.GetProxyServerPort: Integer;
begin
    Result := FProxyServerPort;
end;

procedure TRVMClient.SetProxyServerPort(const Value: Integer);
begin
    if Value <= 0 then begin
        raise Exception.Create('El port debe ser mayor que cero');
        Exit;
    end;
    if Value = FProxyServerPort then Exit;
    FProxyServerPort := Value;
end;

function TRVMClient.GetLicensePlateExists: Boolean;
begin
    Result := FLicensePlateExists;
end;

end.
