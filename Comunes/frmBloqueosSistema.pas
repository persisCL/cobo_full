unit frmBloqueosSistema;

interface

uses
  DMConnection, frmMuestraMensaje,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, ExtCtrls, DB, ADODB;

type
  TBloqueosSistemaForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Image1: TImage;
    DBListEx1: TDBListEx;
    btnAceptar: TButton;
    Image2: TImage;
    spObtenerBloqueo: TADOStoredProc;
    dsObtenerBloqueo: TDataSource;
    GroupBox2: TGroupBox;
    memInfoBloqueo: TMemo;
    spEliminarBloqueo: TADOStoredProc;
    procedure spObtenerBloqueoAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    class function Inicializar(pObjeto, pClave, pUsuario: String; var pIDBloqueo: Integer): Integer;
  end;

var
  BloqueosSistemaForm: TBloqueosSistemaForm;

implementation

{$R *.dfm}

procedure TBloqueosSistemaForm.FormShow(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

class function TBloqueosSistemaForm.Inicializar(pObjeto, pClave, pUsuario: String; var pIDBloqueo: Integer): Integer;
begin
    try
        try
            Result := 0;

            if not Assigned(BloqueosSistemaForm) then begin
                BloqueosSistemaForm := TBloqueosSistemaForm.Create(Application);
            end;

            with BloqueosSistemaForm do begin
                case pIDBloqueo of
                    0: begin // Bloquear Objeto
                        with spObtenerBloqueo do begin
                            if Active then Close;
                            Parameters.Refresh;

                            Parameters.ParamByName('@Objeto').Value    := pObjeto;
                            Parameters.ParamByName('@Clave').Value     := pClave;
                            Parameters.ParamByName('@Usuario').Value   := pUsuario;
                            Parameters.ParamByName('@IDBloqueo').Value := 0;

                            Open;

                            if (Parameters.ParamByName('@IDBloqueo').Value <> 0) then begin
//                            if (Parameters.ParamByName('@IDBloqueo').Value <> 0) and (FieldByName('Usuario').AsString <> pUsuario) then begin
                                ShowModal;
                                Result := 1;
                            end
                            else pIDBloqueo := FieldByName('IDBloqueo').AsInteger;
                        end;

                    end;
                else // Desbloquear Objeto
                    with spEliminarBloqueo do begin
                        if Active then Close;
                        Parameters.Refresh;

                        Parameters.ParamByName('@IDBloqueo').Value := pIDBloqueo;
                        ExecProc;

                        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                            raise Exception.Create(Format('No se pudo desbloquear el ID %d de la tabla de Bloqueos.',[pIDBloqueo]));
                        end;
                    end;
                end;
            end;
        except
            on e: Exception do begin
                Result := 2;
                Raise;
            end;
        end;
    finally
        if Assigned(BloqueosSistemaForm) then begin
            with BloqueosSistemaForm do begin
                if spObtenerBloqueo.Active then spObtenerBloqueo.Close;
            end;
        end;
    end;
end;

procedure TBloqueosSistemaForm.spObtenerBloqueoAfterScroll(DataSet: TDataSet);
begin
    memInfoBloqueo.Clear;
    memInfoBloqueo.Text := spObtenerBloqueo.FieldByName('InfoBloqueo').AsString;
end;

end.
