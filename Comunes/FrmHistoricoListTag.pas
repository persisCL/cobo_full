{-------------------------------------------------------------------------------
 File Name: frmRptHistoricoListTag.pas
 Author: aganc
 Date Created:  16/5/06
 Language: ES-AR
 Description: Reporte de pedidos de actualizacion de ActionList

Revision 1:
    Author : ggomez
    Date : 20/07/2006
    Description : Quit� la funcionalidad de Cancelar un Env�o. Esto fue aceptado
        por CN.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------}
unit FrmHistoricoListTag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,DMConnection, DB, ADODB, Grids, DBGrids, StdCtrls, DPSControls,
  ExtCtrls, Validate,UtilProc, DateEdit, Menus, ListBoxEx, DBListEx,util,
  ImgList, PeaTypes, PeaProcs;
             
type
  TfrmHistoricoListTagForm = class(TForm)
    dsspObtenerHistoricoPedidosEnvioActionList: TDataSource;
    pnlFiltros: TPanel;
    txtFechaDesde: TDateEdit;
    txtFechaHasta: TDateEdit;
    txtEtiquetaInicial: TEdit;
    spObtenerHistoricoPedidosEnvioActionList: TADOStoredProc;
    txtUsuario: TEdit;
    lblFiltroTelevia: TLabel;
    lblFiltroUsuario: TLabel;
    lblFiltroFechaDesde: TLabel;
    lblFiltroFechaHasta: TLabel;
    btnBuscar: TButton;
    dblHistoricoPedidoEnvioActionList: TDBListEx;
    Img_Tilde: TImageList;
    pnlBottom: TPanel;
    btnSalir: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblHistoricoPedidoEnvioActionListDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);

  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    { Public declarations }
    function Inicializar(txtCaption: String; MDIChild: Boolean; Etiqueta: AnsiString = ''): Boolean;
  end;

var
  frmHistoricoListTagForm: TfrmHistoricoListTagForm;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    aganc
  Date Created: 29/05/2006
  Description: Inicializacion de este formulario
  Parameters: txtCaption: String; MDIChild: boolean
  Return Value: Boolean

  Revision 1:
      Author : ggomez
      Date : 05/07/2006
      Description : Agregu� el par�metro Etiqueta para poder llamar a este form
        con una Etiqueta para que se habra el form con los hist�ricos del Telev�a
        pasado como par�metro.

-----------------------------------------------------------------------------}
function TfrmHistoricoListTagForm.Inicializar(txtCaption: String;
    MDIChild: Boolean; Etiqueta: AnsiString = ''): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
		Constraints.MinWidth := 800;
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);

    if Etiqueta <> EmptyStr then begin
        // Filtrar por Telev�a
        txtEtiquetaInicial.Text := Etiqueta;
        btnBuscarClick(Self);
    end;
	Result := true;
end;


{******************************** Function Header ******************************
Function Name: BTCerrarClick
Author : aganc
Date Created : 16/05/2006
Description : cierra el formulario
Parameters : Sender: TObject
Return Value :
*******************************************************************************}
procedure TfrmHistoricoListTagForm.btnSalirClick(Sender: TObject);
begin
    Self.Close;
end;
{******************************** Function Header ******************************
Function Name: BTBuscarClick
Author : aganc
Date Created : 16/05/2006
Description : llama al sp  ObtenerHistoricoPedidosEnvioActionList
si fija si hay parametros para restringir la busqueda y dicho caso lo pasa
sino pasa null y el sp resuelve la consulta dinamicamente
Parameters : none
Return Value : none
*******************************************************************************}
procedure TfrmHistoricoListTagForm.btnBuscarClick(Sender: TObject);
resourceString
    MSG_IS_NOT_VALID_DATE = 'La fecha ingresada debe ser mayor a 01/01/1950';
    MSG_ERROR_OBTENER_HISTORICO = 'Ha ocurrido un error al obtener los datos del Hist�rico.';
begin
    try
        spObtenerHistoricoPedidosEnvioActionList.close;
        if txtFechaDesde.Date <> Nulldate then begin
            if txtFechaDesde.Date < EncodeDate(1950, 1, 1) then begin
              MsgBoxBalloon(MSG_IS_NOT_VALID_DATE, Self.Caption, MB_ICONSTOP, txtFechaDesde);
              Exit;
            end;
        end;

        if txtFechaHasta.Date <> Nulldate then begin
            if txtFechaHasta.Date < EncodeDate(1950, 1, 1) then begin
                MsgBoxBalloon(MSG_IS_NOT_VALID_DATE, Self.Caption, MB_ICONSTOP, txtFechaHasta);
                Exit;
            end;
        end;

        if txtEtiquetaInicial.Text <> '' then begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@Etiqueta').Value := txtEtiquetaInicial.Text;
        end else begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@Etiqueta').Value := Null;
        end;

        if txtUsuario.Text <> '' then begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@Usuario').Value := txtUsuario.Text;
        end else begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@Usuario').Value := Null;
        end;

        if txtFechaDesde.Date > 0  then begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@FechaDesde').Value := FormatDateTime('yyyymmdd', txtFechaDesde.Date);
        end else begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@FechaDesde').Value := Null;
        end;

        if txtFechaHasta.Date > 0 then begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@FechaHasta').Value := FormatDateTime('yyyymmdd', txtFechaHasta.Date + 1);
        end else begin
            spObtenerHistoricoPedidosEnvioActionList.Parameters.ParamByName('@FechaHasta').Value := Null;
        end;

        spObtenerHistoricoPedidosEnvioActionList.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_OBTENER_HISTORICO, e.Message, Self.Caption, MB_ICONSTOP);
            Cursor := crDefault;
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListEx1DrawText
  Author:    aganc
  Date Created: 29/05/2006
  Description:  dibuja un bitmap en las columnas de los campos boleanos
  con un tilde o en blanco respectivamente de su valor
  Parameters: Sender: TCustomDBListEx; var Action: TCloseAction
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmHistoricoListTagForm.dblHistoricoPedidoEnvioActionListDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
var
  bmp: TBitMap;
begin
    with Sender do  begin
        if (spObtenerHistoricoPedidosEnvioActionList.FindField(Column.FieldName).DataType = ftBoolean) then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
		      	DefaultDraw := False;
            try
                if (spObtenerHistoricoPedidosEnvioActionList.FindField(Column.FieldName).AsBoolean) then begin
                    Img_Tilde.GetBitmap(0, Bmp);
                end else begin
                    Img_Tilde.GetBitmap(1, Bmp);
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    aganc
  Date Created: 29/05/2006
  Description:  libero el form de la memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmHistoricoListTagForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
