object frmExplorarInfraciones: TfrmExplorarInfraciones
  Left = 80
  Top = 158
  Caption = 'Explorador de Infracciones'
  ClientHeight = 451
  ClientWidth = 1230
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    1230
    451)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlDefaultFilter: TPanel
    Left = 0
    Top = 0
    Width = 1230
    Height = 129
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 991
    object gbFiltros: TGroupBox
      Left = 0
      Top = 0
      Width = 1230
      Height = 129
      Align = alClient
      Caption = ' Filtros '
      TabOrder = 0
      ExplicitWidth = 991
      DesignSize = (
        1230
        129)
      object lblCapPatente: TLabel
        Left = 51
        Top = 47
        Width = 40
        Height = 13
        Caption = 'Patente:'
      end
      object Label1: TLabel
        Left = 205
        Top = 47
        Width = 128
        Height = 13
        Caption = 'Fecha de Infracci'#243'n:  entre'
      end
      object Label2: TLabel
        Left = 436
        Top = 47
        Width = 17
        Height = 13
        Caption = '  y  '
      end
      object Label3: TLabel
        Left = 55
        Top = 75
        Width = 36
        Height = 13
        Caption = 'Estado:'
      end
      object Label4: TLabel
        Left = 565
        Top = 75
        Width = 100
        Height = 13
        Caption = 'Motivo de Anulaci'#243'n:'
      end
      object Label5: TLabel
        Left = 580
        Top = 47
        Width = 85
        Height = 13
        Caption = '&Infracciones <= a:'
      end
      object lblConcesionaria: TLabel
        Left = 21
        Top = 20
        Width = 70
        Height = 13
        Caption = 'Concesionaria:'
      end
      object Label6: TLabel
        Left = 302
        Top = 75
        Width = 48
        Height = 13
        Caption = 'Estado IF:'
      end
      object txtPatente: TEdit
        Left = 97
        Top = 44
        Width = 81
        Height = 21
        AutoSize = False
        MaxLength = 10
        TabOrder = 1
      end
      object txtDesde: TDateEdit
        Left = 341
        Top = 44
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
      object txtHasta: TDateEdit
        Left = 458
        Top = 44
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 3
        Date = -693594.000000000000000000
      end
      object cbEstadoInterno: TVariantComboBox
        Left = 97
        Top = 72
        Width = 190
        Height = 22
        Style = vcsOwnerDrawVariable
        ItemHeight = 16
        TabOrder = 5
        OnChange = cbEstadoInternoChange
        Items = <>
      end
      object cbMotivoAnulacion: TVariantComboBox
        Left = 671
        Top = 71
        Width = 258
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 7
        Items = <>
      end
      object txtTop: TNumericEdit
        Left = 671
        Top = 44
        Width = 57
        Height = 21
        MaxLength = 5
        TabOrder = 4
        Value = 100.000000000000000000
      end
      object btnFiltrar: TButton
        Left = 1066
        Top = 15
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Flitrar'
        Default = True
        TabOrder = 9
        OnClick = btnFiltrarClick
        ExplicitLeft = 827
      end
      object btnLimpiar: TButton
        Left = 1147
        Top = 15
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 10
        OnClick = btnLimpiarClick
        ExplicitLeft = 908
      end
      object chbNoIncluidasXML: TCheckBox
        Left = 703
        Top = 101
        Width = 226
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Ver Solo Infracciones No Incluidas en XML:'
        TabOrder = 8
      end
      object cbConcesionaria: TVariantComboBox
        Left = 97
        Top = 17
        Width = 191
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        Items = <>
      end
      object cbEstadoExterno: TVariantComboBox
        Left = 358
        Top = 72
        Width = 190
        Height = 22
        Style = vcsOwnerDrawVariable
        ItemHeight = 16
        TabOrder = 6
        OnChange = cbEstadoInternoChange
        Items = <>
      end
    end
  end
  object btnEditar: TButton
    Left = 1027
    Top = 418
    Width = 114
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Editar Infracci'#243'n'
    Enabled = False
    TabOrder = 1
    OnClick = btnEditarClick
    ExplicitLeft = 788
  end
  object btnCerrar: TButton
    Left = 1147
    Top = 418
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnCerrarClick
    ExplicitLeft = 908
  end
  object dbInfracciones: TDBListEx
    Left = 0
    Top = 129
    Width = 1230
    Height = 280
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'C'#243'digo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'CodigoInfraccion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 85
        Header.Caption = 'Concesionaria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Concesionaria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Tipo Infraccion'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'TipoInfraccion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 70
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'Patente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Fecha'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'FechaInfraccion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 125
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'EstadoInterno'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 125
        Header.Caption = 'Estado IF'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'EstadoExterno'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 95
        Header.Caption = 'Fec. Anulaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaAnulacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 180
        Header.Caption = 'Motivo Anulaci'#243'n Infracci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'MotivoAnulacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 125
        Header.Caption = 'Usuario Anulaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CodigoUsuarioAnulacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Respuesta RNVM'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'TuvoRespuestaRNVM'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Cantidad de Consultas'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'CantidadConsultasRNVM'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Nombre'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Nombre'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Documento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroDocumento'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 70
        Header.Caption = 'Existe Carta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'HayCarta'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dbInfraccionesColumns0HeaderClick
        FieldName = 'CodigoUsuario'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Motivo No Incluido XML'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'MotivoInfraccionNoIncluida'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        Header.Caption = 'Codigo Carta Inf.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CodigoCartaAInfractor'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'Disco'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Disco'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 65
        Header.Caption = 'Archivo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Archivo'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Nombre Archivo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreArchivo'
      end>
    DataSource = dsExplorarInfracciones
    DragReorder = True
    ParentColor = False
    TabOrder = 3
    TabStop = True
    OnDblClick = dbInfraccionesDblClick
    ExplicitTop = 124
    ExplicitWidth = 991
  end
  object btnExportarCSV: TButton
    Left = 8
    Top = 418
    Width = 83
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'E&xportar CSV'
    TabOrder = 4
    OnClick = btnExportarCSVClick
  end
  object spExplorarInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ExplorarInfracciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Desde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Hasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEstadoExterno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstadoInterno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMotivoAnulacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EnviadoXML'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TOP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 71
    Top = 186
  end
  object dsExplorarInfracciones: TDataSource
    DataSet = spExplorarInfracciones
    Left = 111
    Top = 201
  end
  object spObtenerEstadoExternoInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEstadoExternoInfraccion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstadoExterno'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 103
    Top = 306
  end
  object spObtenerEstadoInternoInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEstadoInternoInfraccion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstadoInterno'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 216
    Top = 328
  end
  object dlgSaveCSV: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 'Archivos Comma Separated Value|*.csv|Todos los Archivos|*.*'
    InitialDir = '.'
    Title = 'Guardar Como...'
    Left = 112
    Top = 408
  end
end
