object ReporteNotaCreditoElectronicaForm: TReporteNotaCreditoElectronicaForm
  Left = 0
  Top = 0
  Caption = 'ReporteNotaCreditoElectronica'
  ClientHeight = 300
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object rbiCK: TRBInterface
    Report = rptCK
    Caption = 'Impresion de Nota de Credito Electr'#243'nica'
    OrderIndex = 0
    Left = 120
    Top = 28
  end
  object ppCK: TppDBPipeline
    DataSource = sdCK
    UserName = 'ppCK'
    Left = 240
    Top = 166
  end
  object spComprobantes: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = 'ObtenerEncabezadoCKAImprimir;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 88
    Top = 133
  end
  object sdCK: TDataSource
    DataSet = spComprobantes
    Left = 241
    Top = 117
  end
  object ppLineasCKDBPipeline: TppDBPipeline
    DataSource = dsLineasImpresionCK
    UserName = 'LineasCKDBPipeline'
    Left = 440
    Top = 216
  end
  object dsLineasImpresionCK: TDataSource
    DataSet = cdsDetalle
    Left = 440
    Top = 168
  end
  object spObtenerLineasImpresionCK: TADOStoredProc
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerLineasImpresionNotaCreditoElectronica;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 399
    Top = 15
    object spObtenerLineasImpresionCKDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object spObtenerLineasImpresionCKImporte: TLargeintField
      FieldName = 'Importe'
    end
    object spObtenerLineasImpresionCKDescImporte: TStringField
      FieldName = 'DescImporte'
      Size = 50
    end
    object spObtenerLineasImpresionCKCodigoConcepto: TWordField
      FieldName = 'CodigoConcepto'
    end
  end
  object spObtenerDatosComprobanteParaTimbreElectronico: TADOStoredProc
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerDatosComprobanteParaTimbreElectronico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RutEmisor'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 76496130
      end
      item
        Name = '@DVRutEmisor'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1
        Value = '7'
      end
      item
        Name = '@CodigoTipoDocumentoElectronico'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@TotalComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@DescripcionPrimerItem'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 35
        Value = Null
      end
      item
        Name = '@RutReceptor'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DVRutReceptor'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1
        Value = Null
      end
      item
        Name = '@RazonSocialONombrePersona'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = '  '
      end
      item
        Name = '@FechaTimbre'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 82
    Top = 250
    object StringField1: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object LargeintField1: TLargeintField
      FieldName = 'Importe'
    end
    object StringField2: TStringField
      FieldName = 'DescImporte'
      Size = 50
    end
    object WordField1: TWordField
      FieldName = 'CodigoConcepto'
    end
  end
  object rptCK: TppReport
    AutoStop = False
    DataPipeline = ppCK
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 2540
    PrinterSetup.mmMarginLeft = 2540
    PrinterSetup.mmMarginRight = 2540
    PrinterSetup.mmMarginTop = 2540
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Units = utMillimeters
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 64
    Top = 32
    Version = '12.04'
    mmColumnWidth = 0
    DataPipelineName = 'ppCK'
    object ppDetailBand3: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 291500
      mmPrintPosition = 0
      object ppImageFondo: TppImage
        UserName = 'ImageFondo'
        AlignHorizontal = ahCenter
        AlignVertical = avCenter
        MaintainAspectRatio = False
        Stretch = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        mmHeight = 291042
        mmLeft = 0
        mmTop = 0
        mmWidth = 205052
        BandType = 4
      end
      object ppShape1: TppShape
        UserName = 'Shape1'
        Brush.Style = bsClear
        Gradient.EndColor = clWhite
        Gradient.StartColor = clWhite
        Gradient.Style = gsNone
        Pen.Color = clRed
        Pen.Width = 2
        mmHeight = 30163
        mmLeft = 114300
        mmTop = 7938
        mmWidth = 77523
        BandType = 4
      end
      object ppRutConcesionaria: TppLabel
        UserName = 'RutConcesionaria'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clRed
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'RutConcesionaria'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4995
        mmLeft = 116152
        mmTop = 10054
        mmWidth = 73554
        BandType = 4
      end
      object ppTipoDocumentoElectronico: TppLabel
        UserName = 'TipoDocumentoElectronico'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clRed
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'TipoDocumentoElectronico'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 11906
        mmLeft = 116152
        mmTop = 16933
        mmWidth = 73554
        BandType = 4
      end
      object ppLabel1: TppLabel
        UserName = 'Label1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clRed
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'N'#176
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4995
        mmLeft = 144463
        mmTop = 30427
        mmWidth = 4741
        BandType = 4
      end
      object ppDBText3: TppDBText
        UserName = 'DBText3'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clRed
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'NumeroComprobanteFiscal'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 12
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 4995
        mmLeft = 150548
        mmTop = 30427
        mmWidth = 39158
        BandType = 4
      end
      object ppdbtNombre: TppDBText
        UserName = 'dbtNombre'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'NombreCliente'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 4233
        mmLeft = 13494
        mmTop = 64558
        mmWidth = 84667
        BandType = 4
      end
      object ppdbtDir1: TppDBText
        UserName = 'dbtDir1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Domicilio'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        WordWrap = True
        DataPipelineName = 'ppCK'
        mmHeight = 9790
        mmLeft = 13494
        mmTop = 69850
        mmWidth = 84667
        BandType = 4
      end
      object ppdbtDir2: TppDBText
        UserName = 'dbtDir2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'Comuna'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 4233
        mmLeft = 13494
        mmTop = 79904
        mmWidth = 84667
        BandType = 4
      end
      object ppDBText4: TppDBText
        UserName = 'DBText4'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'FechaEmision'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Name = 'Microsoft Sans Serif'
        Font.Size = 9
        Font.Style = [fsBold]
        ParentDataPipeline = False
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 3598
        mmLeft = 160073
        mmTop = 58473
        mmWidth = 28310
        BandType = 4
      end
      object ppDBText1: TppDBText
        UserName = 'DBText1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'NumeroConvenioFormateado'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 3598
        mmLeft = 13494
        mmTop = 112184
        mmWidth = 51065
        BandType = 4
      end
      object ppDBText11: TppDBText
        UserName = 'DBText11'
        HyperlinkColor = clBlue
        AutoSize = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        DataField = 'RutCliente'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 3598
        mmLeft = 22490
        mmTop = 116681
        mmWidth = 18256
        BandType = 4
      end
      object ppDBText12: TppDBText
        UserName = 'DBText12'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        DataField = 'GiroCliente'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 3598
        mmLeft = 23019
        mmTop = 121973
        mmWidth = 72231
        BandType = 4
      end
      object ppLabel5: TppLabel
        UserName = 'Label5'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Giro:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        mmHeight = 3598
        mmLeft = 13494
        mmTop = 121973
        mmWidth = 6900
        BandType = 4
      end
      object ppDBText5: TppDBText
        UserName = 'DBText5'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'DescripcionComprobanteQueAnula'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 4191
        mmLeft = 12171
        mmTop = 140229
        mmWidth = 38629
        BandType = 4
      end
      object ppDBText7: TppDBText
        UserName = 'DBText7'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'ComprobanteQueAnula'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 4022
        mmLeft = 51858
        mmTop = 140494
        mmWidth = 47361
        BandType = 4
      end
      object ppLabel3: TppLabel
        UserName = 'Label3'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Fecha Emisi'#243'n'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 4191
        mmLeft = 12171
        mmTop = 144198
        mmWidth = 38365
        BandType = 4
      end
      object ppDBText6: TppDBText
        UserName = 'DBText6'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'FechaEmisionComprobanteQueAnula'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        DataPipelineName = 'ppCK'
        mmHeight = 3969
        mmLeft = 51858
        mmTop = 144463
        mmWidth = 47361
        BandType = 4
      end
      object ppLabel8: TppLabel
        UserName = 'Label8'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Motivo Nota Cr'#233'dito Electr'#243'nica'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taRightJustified
        Transparent = True
        WordWrap = True
        mmHeight = 8467
        mmLeft = 12171
        mmTop = 148167
        mmWidth = 38100
        BandType = 4
      end
      object ppDBText10: TppDBText
        UserName = 'DBText10'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        DataField = 'MotivoAnulacion'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        WordWrap = True
        DataPipelineName = 'ppCK'
        mmHeight = 8467
        mmLeft = 51858
        mmTop = 148167
        mmWidth = 47361
        BandType = 4
      end
      object ppImage2: TppImage
        UserName = 'ImgTimbre1'
        AlignHorizontal = ahCenter
        AlignVertical = avCenter
        MaintainAspectRatio = False
        Stretch = True
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        mmHeight = 30427
        mmLeft = 11906
        mmTop = 161661
        mmWidth = 87577
        BandType = 4
      end
      object ppResolucionSII: TppLabel
        UserName = 'ResolucionSII'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'ResolucionSII'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 7
        Font.Style = []
        TextAlignment = taCentered
        Transparent = True
        WordWrap = True
        mmHeight = 7144
        mmLeft = 14023
        mmTop = 192617
        mmWidth = 83873
        BandType = 4
      end
      object pplblUltimoAjuste: TppLabel
        UserName = 'Label6'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'Su '#250'ltimo ajuste sencillo es: $'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        Visible = False
        mmHeight = 3704
        mmLeft = 109273
        mmTop = 194734
        mmWidth = 44979
        BandType = 4
      end
      object ppSubReport1: TppSubReport
        UserName = 'SubReport1'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ParentWidth = False
        TraverseAllData = False
        DataPipelineName = 'ppLineasCKDBPipeline'
        mmHeight = 76994
        mmLeft = 109273
        mmTop = 82815
        mmWidth = 84138
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport1: TppChildReport
          AutoStop = False
          DataPipeline = ppLineasCKDBPipeline
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 2540
          PrinterSetup.mmMarginLeft = 2540
          PrinterSetup.mmMarginRight = 2540
          PrinterSetup.mmMarginTop = 2540
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Units = utMillimeters
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppLineasCKDBPipeline'
          object ppDetailBand2: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object ppDBText8: TppDBText
              UserName = 'DBText8'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'Descripcion'
              DataPipeline = ppLineasCKDBPipeline
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppLineasCKDBPipeline'
              mmHeight = 3704
              mmLeft = 0
              mmTop = 265
              mmWidth = 55298
              BandType = 4
            end
            object ppDBText9: TppDBText
              UserName = 'DBText9'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'DescImporte'
              DataPipeline = ppLineasCKDBPipeline
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'ppLineasCKDBPipeline'
              mmHeight = 3704
              mmLeft = 56621
              mmTop = 265
              mmWidth = 20638
              BandType = 4
            end
          end
          object raCodeModule1: TraCodeModule
            ProgramStream = {00}
          end
        end
      end
      object ppDBText2: TppDBText
        UserName = 'DBText2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        DataField = 'UltimoAjusteConvenio'
        DataPipeline = ppCK
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        ParentDataPipeline = False
        Transparent = True
        Visible = False
        DataPipelineName = 'ppCK'
        mmHeight = 3704
        mmLeft = 155840
        mmTop = 194734
        mmWidth = 34131
        BandType = 4
      end
      object ppLabel10: TppLabel
        UserName = 'Label10'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'RUT:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 9
        Font.Style = []
        Transparent = True
        mmHeight = 3704
        mmLeft = 13494
        mmTop = 116681
        mmWidth = 7408
        BandType = 4
      end
      object ppLabel2: TppLabel
        UserName = 'Label2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = ':'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Microsoft Sans Serif'
        Font.Size = 9
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 50536
        mmTop = 140494
        mmWidth = 1058
        BandType = 4
      end
      object ppLabel4: TppLabel
        UserName = 'Label4'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = ':'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Microsoft Sans Serif'
        Font.Size = 9
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 50536
        mmTop = 144463
        mmWidth = 1058
        BandType = 4
      end
      object ppLabel9: TppLabel
        UserName = 'Label9'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = ':'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Name = 'Microsoft Sans Serif'
        Font.Size = 9
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 3704
        mmLeft = 50536
        mmTop = 148167
        mmWidth = 1058
        BandType = 4
      end
      object ppLabel6: TppLabel
        UserName = 'RutConcesionaria1'
        HyperlinkColor = clBlue
        AutoSize = False
        Border.BorderPositions = []
        Border.Color = clRed
        Border.Style = psSolid
        Border.Visible = False
        Caption = 'S.I.I. - SANTIAGO ORIENTE'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4191
        mmLeft = 116152
        mmTop = 38894
        mmWidth = 73554
        BandType = 4
      end
      object plbl1: TppLabel
        UserName = 'plbl1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Caption = 
          'Ruta del Maipo Sociedad Concesionaria S.A.'#13#10'Ruta 5 Sur Km 54, Pa' +
          'ine, Regi'#243'n Metropolitana'#13#10'Call Center 600 252 5000 www.rutamaip' +
          'o.cl'#13#10'Giro: Dise'#241'o, Construcci'#243'n, Mantenci'#243'n, Explotaci'#243'n y Oper' +
          'aci'#243'n'#13#10'por Concesi'#243'n de Obra P'#250'blica Fiscal denominada Ruta 5'#13#10'T' +
          'ramo Santiago - Talca y Acceso Sur a Santiago'
        Ellipsis = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 6
        Font.Style = []
        Transparent = True
        WordWrap = True
        mmHeight = 17992
        mmLeft = 13494
        mmTop = 28310
        mmWidth = 61648
        BandType = 4
      end
      object ppSubReport2: TppSubReport
        UserName = 'SubReport2'
        ExpandAll = False
        NewPrintJob = False
        OutlineSettings.CreateNode = True
        ParentWidth = False
        TraverseAllData = False
        DataPipelineName = 'ppDBTotalPipeline'
        mmHeight = 14023
        mmLeft = 109273
        mmTop = 169863
        mmWidth = 84138
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport2: TppChildReport
          AutoStop = False
          DataPipeline = ppDBTotalPipeline
          PrinterSetup.BinName = 'Default'
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 2540
          PrinterSetup.mmMarginLeft = 2540
          PrinterSetup.mmMarginRight = 2540
          PrinterSetup.mmMarginTop = 2540
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Units = utMillimeters
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppDBTotalPipeline'
          object ppDetailBand1: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 5027
            mmPrintPosition = 0
            object ppDBText13: TppDBText
              UserName = 'DBText8'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'Descripcion'
              DataPipeline = ppDBTotalPipeline
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppDBTotalPipeline'
              mmHeight = 3704
              mmLeft = 0
              mmTop = 265
              mmWidth = 55298
              BandType = 4
            end
            object ppDBText14: TppDBText
              UserName = 'DBText9'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'DescImporte'
              DataPipeline = ppDBTotalPipeline
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 9
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'ppDBTotalPipeline'
              mmHeight = 3704
              mmLeft = 56621
              mmTop = 265
              mmWidth = 20638
              BandType = 4
            end
          end
          object raCodeModule2: TraCodeModule
            ProgramStream = {00}
          end
        end
      end
    end
    object raCodeModule3: TraCodeModule
      ProgramStream = {00}
    end
    object ppParameterList2: TppParameterList
    end
  end
  object spObtenerMaestroConcesionaria: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = 'ObtenerMaestroConcesionaria'
    Parameters = <>
    Left = 88
    Top = 194
  end
  object ppDBTotalPipeline: TppDBPipeline
    DataSource = dsTotal
    UserName = 'LineasTotalPipeline'
    Left = 336
    Top = 216
  end
  object cdsDetalle: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescImporte'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 440
    Top = 112
    Data = {
      5A0000009619E0BD0100000018000000020000000000030000005A000B446573
      6372697063696F6E01004900000001000557494454480200020032000B446573
      63496D706F72746501004900000001000557494454480200020032000000}
  end
  object cdsTotal: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescImporte'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Orden'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 336
    Top = 112
    Data = {
      680000009619E0BD01000000180000000300000000000300000068000B446573
      6372697063696F6E01004900000001000557494454480200020032000B446573
      63496D706F7274650100490000000100055749445448020002003200054F7264
      656E02000100000000000000}
  end
  object dsTotal: TDataSource
    DataSet = cdsTotal
    Left = 336
    Top = 168
  end
end
