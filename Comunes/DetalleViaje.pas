unit DetalleViaje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, DmiCtrls, validate, Dateedit, DbList,
  DB, ADODB, UtilDB, Grids, DBGrids, DPSGrid, Util, DBTables, UtilProc,
  DMConnection, peaprocs, PeaTypes, ComCtrls,
  math, DateUtils, Navigator, ListBoxEx, DBListEx, DPSControls, VariantComboBox,
  BuscaClientes, utilFacturacion, ComprobantesEmitidos;

type
  TNavWindowTransitosPorConvenio = class(TNavWindowFrm)
    ObtenerTransitosConvenio: TADOStoredProc;
    dsTransitos: TDataSource;
    GroupBox2: TGroupBox;
    GroupBox1: TGroupBox;
    lb_CodigoCliente: TLabel;
    peRUTCliente: TPickEdit;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    cbRangosPredefinidos: TComboBox;
    Label4: TLabel;
    deFechaInicial: TDateEdit;
    Label7: TLabel;
    deFechaFinal: TDateEdit;
    DPSButton1: TDPSButton;
    dblTRansitos: TDBListEx;
    cbVehiculos: TVariantComboBox;
    GroupBox3: TGroupBox;
    lNombreCompleto: TLabel;
    Label6: TLabel;
    LDomicilioCompleto: TLabel;
    Label10: TLabel;
    cbConveniosCliente: TVariantComboBox;
    ObtenerCLiente: TADOStoredProc;
    qryConvenios: TADOQuery;
	procedure btn_cerrarClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure ActualizarBusqueda();
    procedure peRUTClienteChange(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure DPSButton1Click(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure deFechaInicialChange(Sender: TObject);
    procedure cbRangosPredefinidosChange(Sender: TObject);
    procedure cbVehiculosChange(Sender: TObject);
  private
    Procedure ClearCaptions;
    procedure CargarCliente;
    function DateToDateSQL(aDate: TDateTime): Variant; 
  public
	{ Public declarations }
	Procedure RefreshData; override;
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
    class function CreateBookmark(RUTCliente: AnsiString; IndexConvenio, IndexVehiculo, IndexRangoFecha: Integer;
      FechaInicial, FechaFinal: TDateTime): AnsiString;
	function Inicializa: Boolean; override;
	function FindFirst(SearchText: AnsiString): Boolean; override;
  end;

var
  NavWindowTransitosPorConvenio: TNavWindowTransitosPorConvenio;



implementation

{$R *.dfm}

{ TNavWindowDatosCliente }
class function TNavWindowTransitosPorConvenio.CreateBookmark(RUTCliente: AnsiString; IndexConvenio, IndexVehiculo, IndexRangoFecha: Integer;
  FechaInicial, FechaFinal: TDateTime): AnsiString;
begin
	Result := format('%s;%d;%d;%d;%s;%s',[Trim(RUTCliente), IndexConvenio, IndexVehiculo,
      IndexRangoFecha, DateTimeToStr(FechaInicial), DateTimeToStr(FechaFinal)]);
end;

function TNavWindowTransitosPorConvenio.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Transacciones del RUT: %s';
    MSG_SIN_RUT = '( No Seleccionado )';
begin
	Bookmark := CreateBookMark(peRUTCliente.Text, cbConveniosCliente.ItemIndex, cbVehiculos.ItemIndex,
      cbRangosPredefinidos.ItemIndex, deFechaInicial.Date, deFechaFinal.Date);
    if trim(peRUTCLiente.text) = '' then begin
        Description := format(MSG_BOOKMARK, [MSG_SIN_RUT]);
    end else begin
        Description := format(MSG_BOOKMARK, [trim(peRUTCLiente.text)]);
    end;
	Result := True;
end;

function TNavWindowTransitosPorConvenio.GotoBookmark(Bookmark: AnsiString): Boolean;
var
    RUTCliente: AnsiString;
    IndexConvenio, IndexVehiculo, IndexRangoFecha: Integer;
    FechaInicial, FechaFinal: TDateTime;
begin
    result := true;
	try
        // Cargamos los valores del BookMark en variables locales.
		RUTCliente      := Trim(ParseParamByNumber(Bookmark, 1, ';'));
        IndexConvenio   := IVal(ParseParamByNumber(Bookmark, 2, ';'));
        IndexVehiculo   := IVal(ParseParamByNumber(Bookmark, 3, ';'));
		IndexRangoFecha := IVal(ParseParamByNumber(Bookmark, 4, ';'));
        try
            FechaINicial := StrToDateTime(ParseParamByNumber(Bookmark, 5, ';'));
        except
            FechaInicial := nulldate;
        end;
        try
            FechaFInal  := StrToDateTime(ParseParamByNumber(Bookmark, 6, ';'));
        except
            FechaFinal  := nulldate;
        end;

        // Cargamos el RUT del cliente seg�n bookmark
        peRUTCliente.Text               := RUTCliente;
        // Obtenemos el cliente en funci�n del RUT
        CargarCliente;
        // Actualizamos el convenio y el vehiculo en funci�n del bookmark
        cbConveniosCliente.ItemIndex    := IndexConvenio;
        cbVehiculos.ItemIndex           := IndexVehiculo;
        // Actualizamos el rango de fecha en funci�n del bookmark
        cbRangosPredefinidos.ItemIndex  := IndexRangoFecha;
        deFechaInicial.date         := FechaInicial;
        deFechaFinal.date           := FechaFinal;

        // Ejecutamos la consulta
        ActualizarBusqueda;
	except
		result := False;
	end;
end;

function TNavWindowTransitosPorConvenio.Inicializa: Boolean;
resourcestring
    MSG_TODAS_TRANSACCIONES = 'Todas las Transacciones';
    MSG_RANGO_PERSONALIZADO = 'Rango de Fecha Personalizado';
begin
	Update;
	Result := Inherited Inicializa;
	if not Result then Exit;
    CargarFechasParaReportes(cbRangosPredefinidos);
    cbRangosPredefinidos.Items.Insert(0, MSG_TODAS_TRANSACCIONES);
    cbRangosPredefinidos.Items.Insert(1, MSG_RANGO_PERSONALIZADO);
    cbRangosPredefinidos.ItemIndex := 0;
end;

procedure TNavWindowTransitosPorConvenio.btn_cerrarClick(Sender: TObject);
begin
	Close;
end;

procedure TNavWindowTransitosPorConvenio.RefreshData;
begin
	inherited;
end;

procedure TNavWindowTransitosPorConvenio.FormCreate(Sender: TObject);
begin
	Update;
end;

function TNavWindowTransitosPorConvenio.FindFirst(SearchText: AnsiString): Boolean;
begin
	result := true;
end;

procedure TNavWindowTransitosPorConvenio.ActualizarBusqueda();
resourcestring
    ERROR_OBTENER_TRANSACCIONES = 'Error obteniendo la lista de transacciones.';
begin
	screen.Cursor := crhourGlass;
	try
        try
    		ObtenerTransitosConvenio.Close;
	    	With ObtenerTransitosConvenio.Parameters do begin
		    	ParamByName('@CodigoConvenio').value    := cbConveniosCliente.value;
			    ParamByName('@IndiceVehiculo').value    := iif(cbVehiculos.Value = 0, null, cbVehiculos.Value);
    			ParamByName('@FechaHoraInicial').value  := DateToDateSQL(deFechaInicial.Date);
	    		ParamByName('@FechaHoraFinal').value    := DateToDateSQL(deFechaFinal.Date);
    	    end;
    		ObtenerTransitosConvenio.Open;
        except
            on e: exception do begin
                msgBoxErr(ERROR_OBTENER_TRANSACCIONES, e.Message, caption, MB_ICONSTOP);;
            end;
        end;
	finally
		ObtenerTransitosConvenio.enableControls;
		screen.Cursor := crDefault;
	end;
end;


procedure TNavWindowTransitosPorConvenio.peRUTClienteChange(Sender: TObject);
begin
    inherited;
    if (activeControl = sender) then begin
        if (length((Sender as TPickEdit).Text) < 7) then begin
            Exit;
        end;
        CargarCliente;
    end;
end;

procedure TNavWindowTransitosPorConvenio.ClearCaptions;
begin
    lNombreCompleto.Caption      := '';
    lDomicilioCompleto.Caption   := '';
end;

procedure TNavWindowTransitosPorConvenio.cbConveniosClienteChange(Sender: TObject);
resourcestring
    MSG_TODAS_CUENTAS = 'Todas las cuentas';
begin
    inherited;
    CargarCuentasConvenio(DMCOnnections.BaseCAC, cbVehiculos, cbConveniosCliente.Value);
    cbVehiculos.Items.InsertItem(0, MSG_TODAS_CUENTAS, null) ;
    cbVehiculos.ItemIndex := 0;
    cbVehiculosChange(cbVehiculos);
end;

procedure TNavWindowTransitosPorConvenio.DPSButton1Click(Sender: TObject);
begin
    inherited;
    Close;
end;

procedure TNavWindowTransitosPorConvenio.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    application.createForm(TFormBuscaClientes,F);
	if F.Inicializa(TIPO_DOCUMENTO_RUT, trim(peRUTCliente.Text), '', '', '') then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            peRUTCliente.Text :=  F.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
        end;
    end;
    F.free;
end;

procedure TNavWindowTransitosPorConvenio.deFechaInicialChange(Sender: TObject);
begin
    inherited;
    if (sender as TDateEdit).Focused then
        cbRangosPredefinidos.ItemIndex := 1;
    ActualizarBusqueda;
end;

procedure TNavWindowTransitosPorConvenio.cbRangosPredefinidosChange(
  Sender: TObject);
begin
    inherited;
    if cbRangosPredefinidos.ItemIndex = 1 then exit;
    if cbRangosPredefinidos.ItemIndex = 0 then begin
        deFechaInicial.Date := nulldate;
        deFechaFInal.Date := nullDate;
    end else begin
        deFechaInicial.Date := EncodeDate(
          Ival(Copy(strLeft(strRight(cbRangosPredefinidos.text,16),8), 0, 4)),
          Ival(Copy(strLeft(strRight(cbRangosPredefinidos.text,16),8), 5, 2)),
          Ival(Copy(strLeft(strRight(cbRangosPredefinidos.text,16),8), 7, 2)));
        deFechaFInal.Date := EncodeDate(
          Ival(Copy(strRight(cbRangosPredefinidos.text,8), 0, 4)),
          Ival(Copy(strRight(cbRangosPredefinidos.text,8), 5, 2)),
          Ival(Copy(strRight(cbRangosPredefinidos.text,8), 7, 2)));
    end;
end;

procedure TNavWindowTransitosPorConvenio.cbVehiculosChange(Sender: TObject);
begin
    inherited;
    ActualizarBusqueda;
end;

function TNavWindowTransitosPorConvenio.DateToDateSQL(aDate: TDateTime): Variant;
begin
    result := iif((aDate = nulldate) or (year(aDate) < 1900) or (year(aDate) > 2070), null, aDate);
end;

procedure TNavWindowTransitosPorConvenio.CargarCliente;
var
    DescripcionCalle: ANsiString;
begin
    ClearCaptions;
    cbConveniosCliente.Items.clear;
    cbVehiculos.Items.Clear;
    ObtenerTransitosConvenio.close;
    // Obtenemos el cliente seleccionado
    with obtenerCliente do begin
        Close;
        Parameters.ParamByName('@CodigoCliente').Value := NULL;
        Parameters.ParamByName('@CodigoDocumento').value := TIPO_DOCUMENTO_RUT;
        Parameters.ParamByName('@NumeroDocumento').value := trim(peRUTCLiente.Text);
        open;
    end;

    // Obtenemos la lista de convenios para el cliente seleccionado
    With qryConvenios do begin
        Parameters.ParamByName('CodigoCliente').Value := obtenerCliente.fieldbyname('CodigoCliente').AsInteger;
        open;
        First;
        while not qryConvenios.Eof do begin
            cbConveniosCliente.Items.Add(qryConvenios.fieldbyname('NumeroConvenio').AsString,
              trim(qryConvenios.fieldbyname('CodigoConvenio').AsString));
            qryConvenios.Next;
        end;
        close;
    end;
    cbConveniosCliente.ItemIndex := 0;
    cbConveniosClienteChange(cbConveniosCliente);

    with ObtenerCliente do begin
        // Mostramos el nombre completo del cliente
        if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
            lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(FieldByName('Apellido').asString),
              Trim(FieldByName('ApellidoMaterno').asString), Trim(FieldByName('Nombre').asString))
        else
            lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(FieldByName('Apellido').asString),
              Trim(FieldByName('ApellidoMaterno').asString));

        // Mostramos el domicilio Personal
        if not (FieldByName('CodigoDomicilio').IsNull) then
            lDomicilioCompleto.Caption := ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC,
              FieldByName('CodigoCliente').AsInteger, FieldByName('CodigoDomicilio').AsInteger)
        else
            lDomicilioCompleto.Caption := MSG_NO_ENCONTRO_DOMICILIO;

    end;

end;

end.
