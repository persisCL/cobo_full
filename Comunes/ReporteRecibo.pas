{-----------------------------------------------------------------------------
  Unit:      Reporte Recibo
  Author:    gcasais
  Date:      26-Abr-2005
------------------------------------------------------------------------------
  IMPORTANTE!!!!!
  Para que el reporte se imprima con el tama�o adecuado, la fuente utilizada
  debe ser FontB11 de tama�o 9. Esta es una fuente que tiene el driver de la
  impresora Epson Tm-T88 III Receipt. Por lo tanto, al recompilar esta unit se
  debe tener instalada la fuente.
-----------------------------------------------------------------------------



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

firma       :  SS_1147_MCA_20150226
Descripcion : se corrige problema al querer imprimir el reporte.

Firma       : SS_1147_MCA_20150422
Descripcion : se modifica el pie del reporte de la impresion de la boleta de pago.
-----------------------------------------------------------------------------}
unit ReporteRecibo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppStrtch, ppRegion, ppClass, ppVar,
  ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppDBBDE, ppComm, ppRelatv,
  ppDBPipe, DB, ADODB, peaTypes, DMConnection, RBSetup,
  pptypes, peaprocs, ExtCtrls, jpeg, ppBarCod, ppParameter, ppSubRpt,
  ppModule, raCodMod, UtilProc, RStrings, Util, UtilDB, ConstParametrosGenerales,
  Printers;

type
    TformRecibo = class(TForm)
    dsDatosRecibo: TDataSource;
  	rbi_Recibo: TRBInterface;
  	ppDatosRecibo: TppDBPipeline;
  	spObtenerDatosRecibo: TADOStoredProc;
    rp_ReciboOriginal: TppReport;
    ppShape23: TppShape;
    ppLabel34: TppLabel;
    ppDBText18: TppDBText;
  	ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppDBText20: TppDBText;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppLabel47: TppLabel;
  	ppLabel1: TppLabel;
    ppDBText27: TppDBText;
    ppDBText1: TppDBText;
    ppDBText28: TppDBText;
  	ppLine1: TppLine;
  	ppLine2: TppLine;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppLabel3: TppLabel;
  	ppLabel4: TppLabel;
  	ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
  	ppLabel9: TppLabel;
    ppShape2: TppShape;
  	ppLabel10: TppLabel;
  	ppImage1: TppImage;
    ppLabel11: TppLabel;
    ppLine3: TppLine;
    ppShape4: TppShape;
  	ppShape3: TppShape;
    ppLabel13: TppLabel;
    ppDBText9: TppDBText;
    ppLine4: TppLine;
    ppLabel2: TppLabel;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
  	ppTitleBand1: TppTitleBand;
    ppDetailBand1: TppDetailBand;
  	ppSummaryBand1: TppSummaryBand;
    ppLabel8: TppLabel;
    dsMediosPago: TDataSource;
    spObtenerMediosPago: TADOStoredProc;
  	ppregionCheque: TppRegion;
  	ppDBText6: TppDBText;
  	ppDBText8: TppDBText;
    ppDBText10: TppDBText;
    ppLabel12: TppLabel;
  	ppLabel14: TppLabel;
    ppLabel15: TppLabel;
  	ppDBText2: TppDBText;
    ppLabel16: TppLabel;
    ppDBText11: TppDBText;
    ppRegionTarjeta: TppRegion;
    ppLabel29: TppLabel;
  	ppDBText29: TppDBText;
    ppDBText21: TppDBText;
    ppLabel22: TppLabel;
    ppLabel17: TppLabel;
    ppLabel19: TppLabel;
    ppDBText12: TppDBText;
    ppMedioPago: TppDBPipeline;
    rp_Recibo: TppReport;
    ppHeaderBand1: TppHeaderBand;
    pptxt_Domicilio: TppDBText;
    pplbl_Concesionaria: TppLabel;
    pplbl_RUTConcesionaria: TppLabel;
    pplbl_DireccionConcesionaria: TppLabel;
    pptxt_NumeroDocumento: TppDBText;
    pptxt_DescripcionComuna: TppDBText;
    pptxt_DescripcionRegion: TppDBText;
    pplbl_LocalidadConcesionaria: TppLabel;
    pptxt_Nombre: TppDBText;
    pptxt_NumeroRecibo: TppDBText;
    pplbl_Nombre: TppLabel;
    pplbl_NumeroRUT: TppLabel;
    pplbl_Domicilio: TppLabel;
    pplbl_Comuna: TppLabel;
    pplbl_Region: TppLabel;
    pplbl_Convenio: TppLabel;
    pplbl_DatosCliente: TppLabel;
    pplbl_NumeroRecibo: TppLabel;
    pplbl_Fecha: TppLabel;
    pptxt_Fecha: TppDBText;
  	ppDetailBand3: TppDetailBand;
    pptxt_TipoComprobante: TppDBText;
    pptxt_ImporteCancelado: TppDBText;
    pptxt_NumeroComprobanteCancelado: TppDBText;
  	ppFooterBand1: TppFooterBand;
  	ppSummaryBand3: TppSummaryBand;
    pplbl_TotalCancelado: TppLabel;
  	ppParameterList1: TppParameterList;
    pplbl_FirmaConcesionaria: TppLabel;
  	ppLogo: TppImage;
    ppDomicilioMuestra: TppLabel;
    pplblLineaDatosCliente: TppLabel;
    pplbl_LineaTotalCancelado: TppLabel;
    pplbl_LineaFirmaConcesionaria: TppLabel;
    txt_ImportePagado: TppDBText;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    pptxt_NumeroConvenio: TppDBText;
    ppLabel18: TppLabel;
    ppDBText13: TppDBText;
    spObtenerMaestroConcesionaria: TADOStoredProc;
	  procedure rbi_ReciboExecute(Sender: TObject; var Cancelled: Boolean);
  private
	  { Private declarations }
	  FNumeroRecibo : Integer;
  public
	  { Public declarations }
	  function Inicializar (Logo: TBitmap; NumeroRecibo: Integer; MostrarConfigurarImpresora: Boolean): Boolean;
  end;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Procedure: Inicializar
  Author:    gcasais
  Date:      26-Abr-2005
  Comments:  Inicializaci�n de este formulario
  Result:    None

Revision 1:
    Author : ggomez
    Date : 22/06/2006
    Description :
        - Correg� ortograf�a del resourcestring.
        - Agregu� comentarios.
        - Agregu� nolock al query que obtiene la Fecha de Creaci�n.
        - Modifiqu� para que informe de los errores en forma m�s detallada y
        usando el MsgBoxErr.
------------------------------------------------------------------------------}
function TformRecibo.Inicializar(Logo: TBitmap; NumeroRecibo: Integer; MostrarConfigurarImpresora: Boolean): Boolean;
resourcestring
    MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE = 'Error al obtener datos de configuraci�n del reporte.'
                                                + CRLF + 'Consulte al Administrador de Sistemas';
    MSG_ERROR_OBTENER_FECHA_CORTE_RUT = 'Ha ocurrido un error al obtener la fecha desde la cual la concesionaria cambio el RUT.';
    MSG_ERROR_OBTENER_RUT_ANTERIOR  = 'Ha ocurrido un error al obtener el RUT Anterior de la concesionaria.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_RUT                         = 'RUT: ';
var
    Cancelar: boolean;
    FechaCreacion , FechaDeCorte : TDateTime;
    Rut : String;
    sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;                                      //SS_1147_NDR_20140710
begin
    //Obtengo el numero de recibo
    FNumeroRecibo := NumeroRecibo;
    //REVISION :1
    // Obtener la Fecha de Creaci�n del Recibo.
    try
        FechaCreacion := QueryGetValueDateTime(spObtenerDatosRecibo.Connection,
                ' select FechaCreacion '+
                ' from Comprobantes with (nolock)' +
                ' where (NumeroComprobante = ''' + IntToStr(NumeroRecibo) + ''')' +
                    ' and (TipoComprobante = ''' + TC_RECIBO + ''')');

    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end;

    // Obtener la fecha desde cuando la concesionaria tiene un nuevo RUT.
    try
        ObtenerParametroGeneral(spObtenerDatosRecibo.Connection, 'FECHA_DE_CORTE_RUT', FechaDeCorte);
    except
        on E: Exception do begin
            // Mensja de error
            MsgBoxErr(MSG_ERROR_OBTENER_FECHA_CORTE_RUT, E.Message, Self.Caption, MB_ICONSTOP);
            // Como ocurri� un error, salir con False.
            Result := False;
            Exit;
        end;
    end; // except

    // Si la Fecha de Creaci�n del Recibo es anterior a la Fecha desde cuando la
    // concesionaria tiene nuevo RUT, entonces imprimir el RUT Anterior, sino
    // imprimir el RUT Nuevo.
    if ( FechaCreacion < FechaDeCorte )then begin
        try
            ObtenerParametroGeneral(spObtenerDatosRecibo.Connection, 'RUT_EMPRESA_ANTERIOR', Rut);
        except
            on E: Exception do begin
                // Mensja de error
                MsgBoxErr(MSG_ERROR_OBTENER_RUT_ANTERIOR, E.Message, Self.Caption, MB_ICONSTOP);
                // Como ocurri� un error, salir con False.
                Result := False;
                Exit;
            end;
        end; // except
    end else begin
        try
            ObtenerParametroGeneral(spObtenerDatosRecibo.Connection, 'RUT_EMPRESA', Rut);
        except
            on E: Exception do begin
                // Mensja de error
                MsgBoxErr(MSG_ERROR_OBTENER_RUT, E.Message, Self.Caption, MB_ICONSTOP);
                // Como ocurri� un error, salir con False.
                Result := False;
                Exit;
            end;
        end; // except
    end;
    pplbl_RUTConcesionaria.Caption := MSG_RUT + Rut;

    //FIN REVISION :1

    //si hay un logo
    if Assigned(Logo) then begin
        //lo muestra
        ppLogo.Picture.Bitmap := Logo;
    end else begin
        //oculta la imagen
        ppLogo.Picture := nil;
        ppLogo.Visible := False;
    end;

    with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
    begin                                                                                               //SS_1147_NDR_20140710
      Close;                                                                                            //SS_1147_NDR_20140710
      Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
      Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
      //ExecProc;                                                                                       //SS_1147_MCA_20150226 //SS_1147_NDR_20140710
      Open;                                                                                             //SS_1147_MCA_20150226
      sRazonSocial  := UpperCase(FieldByName('RazonSocialConcesionaria').AsString);                     //SS_1147_NDR_20140710
      sRut          := FormatFloat('#.#',FieldByName('RutConcesionaria').AsInteger) + '-' +             //SS_1147_NDR_20140710
                       FieldByName('DigitoVerificadorConcesionaria').AsString;                          //SS_1147_NDR_20140710
      sDireccion    := FieldByName('DireccionConcesionaria').AsString;                                  //SS_1147_NDR_20140710
      sLocalidad    := FieldByName('Comuna').AsString + '  ' +                                          //SS_1147_NDR_20140710
                       FieldByName('Region').AsString+ '  ' +                                           //SS_1147_NDR_20140710
                       FieldByName('Pais').AsString;                                                    //SS_1147_NDR_20140710
                                                                                                        //SS_1147_NDR_20140710
      pplbl_Concesionaria.Caption          := sRazonSocial;                                             //SS_1147_NDR_20140710
      pplbl_RUTConcesionaria.Caption       := sRut;                                                     //SS_1147_NDR_20140710
      pplbl_DireccionConcesionaria.Caption := sDireccion;                                               //SS_1147_NDR_20140710
      pplbl_LocalidadConcesionaria.Caption := sLocalidad;                                               //SS_1147_NDR_20140710
      pplbl_FirmaConcesionaria.Caption := 'p. ' + ObtenerNombreConcesionariaNativa();
    end;                                                                                                //SS_1147_NDR_20140710

    //Permito configurar la impresora antes de generar el reporte
    if not MostrarConfigurarImpresora then rbi_ReciboExecute(nil, Cancelar);
    //Genero el Reporte
    Result := rbi_Recibo.Execute(MostrarConfigurarImpresora);
end;

{-----------------------------------------------------------------------------
  Procedure: rbi_ReciboExecute
  Author:    gcasais
  Date:      26-Abr-2005
  Comments:  Inicializaci�n de este formulario
  Result:    None
------------------------------------------------------------------------------}
procedure TformRecibo.rbi_ReciboExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_ERROR_AL_IMPRIMIR_RECIBO = 'Error al imprimir el recibo';
    MSG_ERROR_OBTENER_DATOS_RECIBO = 'Error al obtener los Datos del Recibo';
    MSG_ERROR_OBTENER_DATOS_MEDIO_PAGO = 'Error al obtener los datos del Medio de Pago';
begin
    Screen.Cursor := crHourGlass;

    try

        //Obtengo los datos del recibo
        try
            with spObtenerDatosRecibo do begin
                Close;
                CommandTimeout := 500;
                Parameters.ParamByName('@NumeroRecibo').Value := FNumeroRecibo;
                Open;
            end;
        except
            on E: Exception do begin
                //informo que hubo un error al obtener los datos del recibo
                MsgBoxErr(MSG_ERROR_OBTENER_DATOS_RECIBO, E.Message, MSG_ERROR_AL_IMPRIMIR_RECIBO, MB_ICONERROR );
                Cancelled := True;
                Exit;
            end;
        end;

        //Obtengo los datos del medio de pago
        try
            with spObtenerMediosPago do begin
                Close;
                CommandTimeout := 500;
                Parameters.ParamByName('@NumeroRecibo').Value := FNumeroRecibo;
                Open;
                Case FieldByName('CodigoTipoMedioPago').asInteger of
                  0..3:
                    begin
                      ppRegionCheque.Visible := False;
                      ppRegionTarjeta.Visible := False;
                    end;
                  4   :
                    begin
                      ppRegionCheque.Visible := True;
                      ppRegionTarjeta.Visible := False;
                    end;
                  5, 6:
                    begin
                      ppRegionCheque.Visible := False;
                      ppRegionTarjeta.Visible := True;
                    end;
                end;
            end;
        except
            on E: Exception do begin
                //informo que hubo un error al obtener los datos del medio de pago
                MsgBoxErr(MSG_ERROR_OBTENER_DATOS_MEDIO_PAGO , E.Message, MSG_ERROR_AL_IMPRIMIR_RECIBO, MB_ICONERROR );
                Cancelled := True;
                Exit;
            end;
        end;

        //Ajusta la posici�n del domicilio
        ppDomicilioMuestra.Visible := True;
        ppDomicilioMuestra.Caption := pptxt_Domicilio.Text;
        (* Si el Domicilio no entra en una l�nea, aumentar el height del
        componente del domicilio y bajar el resto de los controles. *)
        if ppDomicilioMuestra.Width > pptxt_Domicilio.Width then begin
            //Bajar los labels
            pplbl_Comuna.Top := pplbl_Comuna.Top + pptxt_Domicilio.Height;
            pplbl_Region.Top := pplbl_Region.Top + pptxt_Domicilio.Height;
            pplbl_Convenio.Top := pplbl_Convenio.Top + pptxt_Domicilio.Height;
            //Bajar los dbText
            pptxt_DescripcionComuna.Top := pplbl_Comuna.Top;
            pptxt_DescripcionRegion.Top := pplbl_Region.Top;
            pptxt_NumeroConvenio.Top := pplbl_Convenio.Top;
            //Aumentar el height del dbText del domicilio
            pptxt_Domicilio.Height := pptxt_Domicilio.Height * 2;
            pptxt_Domicilio.WordWrap := True;
            pptxt_Domicilio.AutoSize := False;
        end;
        ppDomicilioMuestra.Visible := False;
    finally
        Screen.Cursor := crDefault;
    end;
end;


end.

