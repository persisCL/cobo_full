{********************************** File Header ********************************
File Name: DB_CRUDCommonProcs.pas
Author: aunanue
Date Created: 01/03/2017
Description: Esta unit contiene rutinas comunes para el manejo de base de datos y para la creaci�n de ABMs.
*******************************************************************************}
unit DB_CRUDCommonProcs;

interface

uses Forms, StdCtrls, Controls, ADODB, DB, VariantComboBox, Graphics, PeaTypes, DMConnection,
     DBClient;

function FindFocusLabelText(OwnerForm: TForm; Control: TWinControl; const EndingChar: string = '.') : String;
procedure AddParameter(Params: TParameters; const ParamName: WideString;
            FieldType: TFieldType; ParamDirection: TParameterDirection;
            Value: OleVariant; Size: Integer = 0);
procedure AddParameter_Codigo(const NombreCampoCodigo: string; Params: ADODB.TParameters; Value: Integer);
procedure AddParameter_Nombre(Params: ADODB.TParameters; const Value: String);
procedure AddParameter_Descripcion(Params: ADODB.TParameters; const Value: String; IsMemo: Boolean = false);
procedure AddParameter_CodigoUsuario(Params: ADODB.TParameters);
procedure AddParameter_Resultado(Params: ADODB.TParameters);
procedure AddParameter_CodigoTelefono(Params: ADODB.TParameters; Value: Integer);
procedure AddParameter_Telefono(Params: ADODB.TParameters; CodigoTelefono: integer;
                                CodigoArea: SmallInt;  NumeroTelefono: String;
                                HorarioDesde, HorarioHasta: TTime; GenerarHistorico: Boolean);
procedure AddParameter_CodigoEmail(Params: ADODB.TParameters; Value: Integer);
procedure AddParameter_EmailContacto(Params: ADODB.TParameters; CodigoEmail: integer;
                             Email: String; GenerarHistorico: Boolean);
procedure AddParameter_CodigoDomicilio(Params: ADODB.TParameters; Value: Integer; IsInOutputParam: boolean = false);
procedure AddParameter_Domicilio(Params: ADODB.TParameters; Value: TDatosDomicilio);
procedure FillVariantItemsFromStoredProc(DBConnection: TADOConnection; StoredProcName: string;
                                         TheItems: TVariantItems; const FieldName_ID: string;
                                         const FieldName_Description: string;
                                         Parameters: array of string; ParamValues: array of variant;
                                         const CaptionForError: string; AddSelectText: boolean);
procedure FillVariantComboBox(TheComboBox: TVariantComboBox; DataSet: TCustomADODataSet;
                           const FieldName_ID: string; const FieldName_Description: string;
                           const CaptionForError: string; AddSelectText: boolean); deprecated;
function CreateDataSetFromStoredProcedure(DBConnection: TADOConnection; StoredProcName: string;
                                           Dataset: TClientDataSet;
                                           Parameters: array of string;
                                           ParamValues: array of variant;
                                           CaptionForError: string): boolean;
function FloatToStrWithDecimalPoint(const Value: Extended; const Format: String = '0.##'): String;
function GetColorForInternalExternalRecord(condition: boolean; State: TOwnerDrawState): TColor;

implementation

uses SysUtils, Util, UtilProc, RStrings, Windows, TypInfo, Variants;

resourcestring
    SIN_ESPECIFICAR = '(Sin especificar)';
    SELECCIONAR = '(Seleccionar)';
    TODOS = '<Todos>';

{-----------------------------------------------------------------------------
  Function Name: ValueToTFieldType
  Author:    aunanue
  Date:      09-03-2017
  Description: Obtiene el TFieldType de un valor
-----------------------------------------------------------------------------}
function ValueToTFieldType(value: Variant) : TFieldType;
begin
   Result := VarTypeToDataType(VarType(Value));
end;

{-----------------------------------------------------------------------------
  Function Name: FindFocusLabelText
  Author:    aunanue
  Date:      09-03-2017
  Description: Buscar el texto asociado a un label que tiene como FocusControl el "Control" pasado como par�metro.
  Parameters: Control: El Edit (o similar) que es FocusControl del Label a buscar.
  Return Value: Boolean
-----------------------------------------------------------------------------}
function FindFocusLabelText(OwnerForm: TForm; Control: TWinControl; const EndingChar: string = '.') : String;
var
  i: Integer;
  TheControl: TLabel;
begin
    Result := ' ';
    for i := 0 to OwnerForm.ComponentCount - 1 do begin
        if not (OwnerForm.Components[i] is TLabel) then
            continue;
        //
        TheControl := OwnerForm.Components[i] as TLabel;
        if (TheControl <> nil) and (TheControl.FocusControl = Control) then begin
            Result := StringReplace(TheControl.Caption, '&', '', [rfReplaceAll]);
            Result := StringReplace(Result, ':', '', [rfReplaceAll]) + EndingChar;
        end;
    end;
end;

{$REGION 'AddParameter' }
{-----------------------------------------------------------------------------
  Function Name: AddParameter
  Author:    aunanue
  Date:      09-03-2017
  Description: Agrega un par�metro a una colecci�n de par�metros
  Return Value: NA
-----------------------------------------------------------------------------}
procedure AddParameter(Params: TParameters; const ParamName: WideString;
            FieldType: TFieldType; ParamDirection: TParameterDirection;
            Value: OleVariant; Size: Integer = 0);
begin
    if (Params.FindParam(ParamName) = nil) then
        Params.CreateParameter(ParamName, FieldType, ParamDirection, Size, Value);
    //
    Params.FindParam(ParamName).Value := Value;
end;

procedure AddParameter_Codigo(const NombreCampoCodigo: string; Params: ADODB.TParameters; Value: Integer);
begin
    AddParameter(Params, '@' + NombreCampoCodigo, ftInteger, pdInput, Value);
end;

procedure AddParameter_Nombre(Params: ADODB.TParameters; const Value: String);
begin
    AddParameter(Params, '@Nombre', ftString, pdInput, Value, 255);
end;

procedure AddParameter_Descripcion(Params: ADODB.TParameters; const Value: String; IsMemo: Boolean = false);
begin
    if IsMemo then    
        AddParameter(Params, '@Descripcion', ftMemo, pdInput, Value, 0)
    else
        AddParameter(Params, '@Descripcion', ftString, pdInput, Value, 255);
end;

procedure AddParameter_CodigoUsuario(Params: ADODB.TParameters);
begin
    AddParameter(Params, '@CodigoUsuario', ftString, pdInput, UsuarioSistema, 20);
end;

procedure AddParameter_Resultado(Params: ADODB.TParameters);
begin
    AddParameter(Params, '@Resultado', ftInteger, pdOutput, 0);
end;

procedure AddParameter_CodigoTelefono(Params: ADODB.TParameters; Value: Integer);
begin
    AddParameter(Params, '@CodigoTelefono', ftInteger, pdInput, Value);
end;

procedure AddParameter_Telefono(Params: ADODB.TParameters; CodigoTelefono: integer;
                                CodigoArea: SmallInt;  NumeroTelefono: String;
                                HorarioDesde, HorarioHasta: TTime; GenerarHistorico: Boolean);
begin
    AddParameter_CodigoTelefono(Params, CodigoTelefono);
    AddParameter(Params, '@CodigoArea', ftSmallint, pdInput, CodigoArea);
    AddParameter(Params, '@NumeroTelefono', ftString, pdInput, NumeroTelefono, 15);
    AddParameter(Params, '@HorarioDesde', ftTime, pdInput, HorarioDesde);
    AddParameter(Params, '@HorarioHasta', ftTime, pdInput, HorarioHasta);
    AddParameter(Params, '@GenerarHistorico', ftBoolean, pdInput, GenerarHistorico);
    AddParameter_CodigoUsuario(Params);
    AddParameter_Resultado(Params);
end;

procedure AddParameter_CodigoEmail(Params: ADODB.TParameters; Value: Integer);
begin
    AddParameter(Params, '@CodigoEmail', ftInteger, pdInput, Value);
end;

procedure AddParameter_EmailContacto(Params: ADODB.TParameters; CodigoEmail: integer;
                             Email: String; GenerarHistorico: Boolean);
begin
    AddParameter_CodigoEmail(Params, CodigoEmail);
    AddParameter(Params, '@Email', ftString, pdInput, Email, 255);
    AddParameter(Params, '@GenerarHistorico', ftBoolean, pdInput, GenerarHistorico);
    AddParameter_CodigoUsuario(Params);
    AddParameter_Resultado(Params);
end;

procedure AddParameter_CodigoDomicilio(Params: ADODB.TParameters; Value: Integer; IsInOutputParam: boolean = false);
begin
    AddParameter(Params, '@CodigoDomicilio', ftInteger, iif(IsInOutputParam, pdInputOutput, pdInput), Value);
end;

procedure AddParameter_Domicilio(Params: ADODB.TParameters; Value: TDatosDomicilio);
begin
    AddParameter(Params, '@CodigoTipoDomicilio', ftInteger, pdInput, Value.CodigoTipoDomicilio);
    AddParameter(Params, '@CodigoPais', ftString, pdInput, Value.CodigoPais, 3);
    AddParameter(Params, '@CodigoRegion', ftString, pdInput, Value.CodigoRegion, 3);
    AddParameter(Params, '@CodigoComuna', ftString, pdInput, Value.CodigoComuna, 3);
    AddParameter(Params, '@DescripcionCiudad', ftString, pdInput, Value.DescripcionCiudad, 50);
    AddParameter(Params, '@CodigoCalle', ftInteger, pdInput, Value.CodigoCalle);
    AddParameter(Params, '@CodigoSegmento', ftInteger, pdInput, Value.CodigoSegmento);
    AddParameter(Params, '@CalleDesnormalizada', ftString, pdInput, Value.CalleDesnormalizada, 100);
    AddParameter(Params, '@Numero', ftString, pdInput, Value.NumeroCalleSTR, 10);
    AddParameter(Params, '@Piso', ftInteger, pdInput, Value.Piso);
    AddParameter(Params, '@Depto', ftString, pdInput, Value.Dpto, 10);
    AddParameter(Params, '@CodigoPostal', ftString, pdInput, Value.CodigoPostal, 10);
    AddParameter(Params, '@Detalle', ftString, pdInput, Value.Detalle, 100);
    AddParameter_CodigoDomicilio(Params, Value.CodigoDomicilio, true);
end;

{$ENDREGION}

{-----------------------------------------------------------------------------
  Function Name: FillVariantComboBox
  Author:    aunanue
  Date:      09-03-2017
  Description: Procedure privado para llenar un VariantItems con los datos obtenidos en el Dataset.
-----------------------------------------------------------------------------}
procedure FillVariantItems(TheItems: TVariantItems; DataSet: TCustomADODataSet;
                           const FieldName_ID: string; const FieldName_Description: string;
                           const CaptionForError: string; AddSelectText: boolean);
begin
    DataSet.Open;
    try
        try
        DataSet.First;
        TheItems.Clear;
        if AddSelectText then
            TheItems.Add(SELECCIONAR, -1);
        //
        while not DataSet.Eof do begin
            TheItems.Add(
                DataSet.FieldByName(FieldName_Description).AsString,
                DataSet.FieldByName(FieldName_ID).AsInteger);
            DataSet.Next;
        end;
        except
            on E: Exception do
                MsgBoxErr(Format(MSG_CAPTION_OBTENER,[CaptionForError]), e.message, CaptionForError, MB_ICONSTOP);
        end;
    finally
        DataSet.Close;
    end;
end;

{-----------------------------------------------------------------------------
Author: aunanue. Este m�todo est� DEPRECATED, usar FillVariantItemsFromStoredProc en su lugar.
-----------------------------------------------------------------------------}
procedure FillVariantComboBox(TheComboBox: TVariantComboBox; DataSet: TCustomADODataSet;
                           const FieldName_ID: string; const FieldName_Description: string;
                           const CaptionForError: string; AddSelectText: boolean);
begin
    FillVariantItems(TheComboBox.Items, DataSet, FieldName_ID, FieldName_Description,
                     CaptionForError, AddSelectText);
end;

{-----------------------------------------------------------------------------
  Function Name: FillVariantItemsFromStoredProc
  Author:    aunanue
  Date:      09-03-2017
  Description: LLena un VariantItems con los datos obtenidos desde un stored procedure. Generalmente
               son los Items de un VariantComboBox.
-----------------------------------------------------------------------------}
procedure FillVariantItemsFromStoredProc(DBConnection: TADOConnection; StoredProcName: string; TheItems: TVariantItems;
                           const FieldName_ID: string; const FieldName_Description: string;
                           Parameters: array of string; ParamValues: array of variant;
                           const CaptionForError: string; AddSelectText: boolean);
var
    StoredProc: TADOStoredProc;
    i, ParamSize: Integer;
    StrValue: String;
begin
    StoredProc := TADOStoredProc.Create(nil);
    try
        if Length(Parameters) <> Length(ParamValues) then begin
            MsgBoxErr(Format(MSG_CAPTION_OBTENER,[CaptionForError]), 'Par�metros inv�lidos para: ' + StoredProcName, CaptionForError, MB_ICONSTOP);
            Exit;
        end;
        //
        StoredProc.ProcedureName := StoredProcName;
        StoredProc.Connection := DBConnection;
        //
        for i := 0 to Length(Parameters) -1 do begin
            if ValueToTFieldType(ParamValues[i]) = ftString then begin
                StrValue := String(ParamValues[i]);
                ParamSize := Length(StrValue);
            end else
                ParamSize := 0;
            //
            AddParameter(StoredProc.Parameters, Parameters[i], ValueToTFieldType(ParamValues[i]), pdInput, ParamValues[i], ParamSize);
        end;

       FillVariantItems(TheItems, StoredProc, FieldName_ID, FieldName_Description, CaptionForError,
                        AddSelectText);
    finally
        FreeAndNil(StoredProc);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CreateDataSetFromStoredProcedure
  Author:    aunanue
  Date:      09-03-2017
  Description: Crea un Dataset en base al resultado de un StoredProc.
               Todos los campos que se devuelven del Store tienen que existir en el Dataset!!!
-----------------------------------------------------------------------------}
function CreateDataSetFromStoredProcedure(DBConnection: TADOConnection; StoredProcName: string;
                                           Dataset: TClientDataSet;
                                           Parameters: array of string;
                                           ParamValues: array of variant;
                                           CaptionForError: string): boolean;
var
    StoredProc: TADOStoredProc;
    i, ParamSize: Integer;
    StrValue: String;
    TheField: TField;
begin
    Result := False;
    StoredProc := TADOStoredProc.Create(nil);
    Dataset.DisableControls;
    try
        try
            if Length(Parameters) <> Length(ParamValues) then begin
                MsgBoxErr(Format(MSG_CAPTION_OBTENER,[CaptionForError]), 'Par�metros inv�lidos para: ' + StoredProcName, CaptionForError, MB_ICONSTOP);
                Exit;
            end;
            StoredProc.Connection := DBConnection;
            StoredProc.ProcedureName := StoredProcName;

            for i := 0 to Length(Parameters) -1 do begin
                if ValueToTFieldType(ParamValues[i]) = ftString then begin
                    StrValue := String(ParamValues[i]);
                    ParamSize := Length(StrValue);
                end else
                    ParamSize := 0;
                //
                AddParameter(StoredProc.Parameters, Parameters[i], ValueToTFieldType(ParamValues[i]), pdInput, ParamValues[i], ParamSize);
            end;

            StoredProc.Open;
            StoredProc.First;
            Dataset.CreateDataSet;
            while not StoredProc.Eof do begin
                Dataset.Insert;
                for i := 0 to StoredProc.FieldCount - 1 do begin
                    //si existe el campo le asigno el valor
                    TheField := Dataset.FindField(StoredProc.Fields[i].FieldName);
                    if Assigned(TheField) then
                        Dataset.FieldByName(StoredProc.Fields[i].FieldName).Value := StoredProc.Fields[i].Value;
                    //
                end;
                Dataset.Post;
                //
                StoredProc.Next;
            end;
            Dataset.First;
            Result := True;
        except on e: Exception do
            MsgBoxErr(Format(MSG_CAPTION_OBTENER,[CaptionForError]), e.message, CaptionForError, MB_ICONSTOP);
        end;
    finally
        if Assigned(StoredProc) then
            StoredProc.Close;
        //
        Dataset.EnableControls;
        FreeAndNil(StoredProc);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FloatToStrWithDecimalPoint
  Author:    aunanue
  Date:      09-03-2017
  Description: Formatea un Float o int para que se muestre como decimal con separador decimal.
-----------------------------------------------------------------------------}
function FloatToStrWithDecimalPoint(const Value: Extended; const Format: String = '0.##'): String;
    //
    function OccurrencesOfChar(const ContentString: string;
      const CharToCount: char): integer;
    var
        C: Char;
    begin
        Result := 0;
        for C in ContentString do
            if C = CharToCount then
                Inc(Result);
    end;
    //
var
    myFormatSettings: TFormatSettings;
begin
    GetLocaleFormatSettings(GetThreadLocale, myFormatSettings);
    myFormatSettings.DecimalSeparator := '.';
    Result := FormatFloat(Format, Value, myFormatSettings);
    if Pos('.', Result) <= 0 then begin
        Result := Result + '.' + StringOfChar('0', OccurrencesOfChar(Format, '#'));
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetColorForInternalExternalRecord
  Author:    aunanue
  Date:      09-03-2017
  Description: Devuelve el color a usar en el DBListDatosDrawItem para cuando se tienen
               Registros internos/externos.
-----------------------------------------------------------------------------}
function GetColorForInternalExternalRecord(condition: boolean;State: TOwnerDrawState): TColor;
begin
    if condition then begin
        if odSelected in State then begin
            Result := clWindow;
		end else begin
            Result := clBlue;
        end;
    end else begin
        if odSelected in State then begin
            Result := clWindow;
		end else begin
            Result := clWindowText;
        end;
    end;
end;

end.
