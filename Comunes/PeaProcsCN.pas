{
    File Name   : PeaProcsCN
    Author      : pdominguez
    Date Created: 14/06/2010
    Language    : ES-AR
    Description : Procedimientos y Funciones propietarias Proyecto CN.

    Fecha           : 29/08/2012
    Firma           : SS_1006_1015_CQU_20120829
    Description     : Se agregan funciones para la obtenci�n del Co�digo de los M�dulos.

    Fecha           : 11/10/2013
    Firma           : SS_1138_CQU_20131009
    Descripcion     : Se agrega la funci�n ObtenerTipoImagenPorRegistratioAccesibility
                      que devuelve el TTipoImagen seg�n un RegistrationAccessibility indicado.
}
unit PeaProcsCN;

interface

Uses
    Windows, Classes, SysUtils, Forms,  Graphics, Controls, ComCtrls,

	MsgBoxCN, SysUtilsCN, Util,
    frmMuestraMensaje, ADODB, UtilDB, RStrings,
    ImgTypes,   // SS_1138_CQU_20131009
    ppReport, ppPDFDevice;

function ShowMsgBoxCN(Titulo, Mensaje: String; Estilo: Integer; Propietario: TComponent): Integer; overload;
function ShowMsgBoxCN(e: Exception; Propietario: TComponent): Integer; overload;
function ValidateControlsCN(Controles: Array of TControl; Condiciones: Array of Boolean; Titulo: AnsiString; Mensajes: Array of String; Propietario: TComponent; Silencioso: Boolean = False): Boolean;

function ArmarNombrePersonaCN(pConexionADO: TADOConnection; pPersoneria, pNombre, pApellido, pApellidoMaterno: String): String;
function FormatearRUTCAC(pRUT: string): string;

function ExportarReporteAPDF(pReporte: TppReport; pNombreArchivo: string): Boolean;

function MesNombre(Fecha: TDateTime): String;


//SS_1011_MBE_20111125_1400
    function NowBaseCN(Database:TADOConnection):TDateTime;

function ObtenerCodigoModuloTransaccionesRechazadasOC(DataBase : TADOConnection): Integer; //SS_1006_1015_CQU_20120829
function ObtenerCodigoModuloGenerarArchivoIvaOC(DataBase : TADOConnection): Integer;       //SS_1006_1015_CQU_20120829
function ObtenerCodigoModuloSalidaFoliosFacturacionOC(DataBase : TADOConnection): Integer; //SS_1006_1015_CQU_20120829
function ObtenerCodigoModuloSalidaFoliosPagosOC(DataBase : TADOConnection): Integer;       //SS_1006_1015_CQU_20120829

function ObtenerTipoImagenPorRegistratioAccesibility(RegistrationAccessisbility : Integer) : TTipoImagen;   // SS_1138_CQU_20131009

implementation

var
    fMsgBoxCN: TfrmMsgBoxCN;

ResourceString
    rsPregunta        = ' Se Requiere una Respuesta del Usuario';
    rsAviso           = ' Aviso del Sistema';
    rsInformacion     = ' Informaci�n del Sistema';
    rsError           = ' Error del Sistema ';
    rsErrorInesperado = ' Error Inesperado del Sistema';
    rsBotonSI         = 'S�';
    rsBotonNO         = 'No';

function ShowMsgBoxCN(Titulo, Mensaje: String; Estilo: Integer; Propietario: TComponent): Integer;
begin
    try
//        fMsgBoxCN := TfrmMsgBoxCN.Create(Propietario);
        fMsgBoxCN := TfrmMsgBoxCN.Create(Application.MainForm);

        with fMsgBoxCN do begin
            lblTitulo.Caption := Titulo;
            EstiloForm := Estilo;

            with imgMsgBoxCN.Picture.Icon do begin
                case Estilo of
                    MB_ICONWARNING: begin
                        Handle := LoadIcon(0, IDI_EXCLAMATION);
                        Caption := rsAviso;
                    end;
                    MB_ICONSTOP: begin
                        Handle := LoadIcon(0, IDI_HAND);
                        Caption := rsError;
                    end;
                    MB_ICONQUESTION, MB_ICONQUESTION + MB_YESNO: begin
                        Handle := LoadIcon(0, IDI_QUESTION);
                        Caption := rsPregunta;
                        btnAceptar.Left := btnAceptar.Left - (btnAceptar.Width + 3);
                        btnAceptar.Default := False;
                        btnCancelar.Visible := True;
                        btnCancelar.Default := True;

                        if Estilo = MB_ICONQUESTION + MB_YESNO then begin
                            btnAceptar.Caption  := rsBotonSI;
                            btnCancelar.Caption := rsBotonNO;
                        end;
                    end;
                    MB_ICONINFORMATION: begin
                        Handle := LoadIcon(0, IDI_ASTERISK);
                        Caption := rsInformacion;
                    end;
                end;
        	end;

            MensajeAMostrar := Trim(Mensaje);
            meMensajeAMostrar.Text := MensajeAMostrar;
            AjustaAlturaMsgBoxCN;
            TPanelMensajesForm.OcultaPanel;

            if not Application.Active then begin
                BringWindowToTop(Application.Handle);
            end;
            
            Result := ShowModal;
        end;
    finally
        if Assigned(fMsgBoxCN) then FreeAndNil(fMsgBoxCN);
    end;
end;

function ShowMsgBoxCN(e: Exception; Propietario: TComponent): Integer; overload;
begin
    try
//        fMsgBoxCN := TfrmMsgBoxCN.Create(Propietario);
        fMsgBoxCN := TfrmMsgBoxCN.Create(Application.MainForm);

        with fMsgBoxCN do begin

            if e is EGeneralExceptionCN then
                with EGeneralExceptionCN(e) do begin

                    if e is EQuestionExceptionCN then begin
                        Caption := rsPregunta;
                        btnAceptar.Left := btnAceptar.Left - (btnAceptar.Width + 3);
                        btnAceptar.Default := False;
                        btnCancelar.Visible := True;
                        btnCancelar.Default := True;
                    end
                    else if e is EErrorExceptionCN then
                        Caption := rsError
                    else if e is EWarningExceptionCN then
                        Caption := rsAviso
                    else if e is EInfoExceptionCN then
                        Caption := rsInformacion;

                    lblTitulo.Caption := EGeneralExceptionCN(e).Title;

                    with imgMsgBoxCN.Picture.Icon do begin
                        case Style of
                            MB_ICONINFORMATION:
                                Handle := LoadIcon(0, IDI_ASTERISK);
                            MB_ICONQUESTION:
                                Handle := LoadIcon(0, IDI_QUESTION);
                            MB_ICONERROR:
                                Handle := LoadIcon(0, IDI_HAND);
                            MB_ICONWARNING:
                                Handle := LoadIcon(0, IDI_EXCLAMATION);
                        end;
                    end;

                    MensajeAMostrar := Trim(Message);
                    meMensajeAMostrar.Lines.Text := MensajeAMostrar;
                end
            else begin
                Caption := rsErrorInesperado;
                lblTitulo.Caption := e.ClassName;
                imgMsgBoxCN.Picture.Icon.Handle := LoadIcon(0, IDI_HAND);
                MensajeAMostrar := Trim(e.Message);
                meMensajeAMostrar.Text := MensajeAMostrar;
            end;

            AjustaAlturaMsgBoxCN;
            TPanelMensajesForm.OcultaPanel;
            Result := ShowModal;
        end;
    finally
        if Assigned(fMsgBoxCN) then FreeAndNil(fMsgBoxCN);
    end;
end;

function ValidateControlsCN(Controles: Array of TControl; Condiciones: Array of Boolean; Titulo: AnsiString; Mensajes: Array of String; Propietario: TComponent; Silencioso: Boolean = False): Boolean;
    var
	    vVeces      : Integer;
        vErrores    : TStringList;
        vFocoSeteado: Boolean;

	procedure ActivarTab(Control: TControl);
	    var
		    vControl: TControl;
	begin
		vControl := Control.Parent;
		While vControl <> nil do begin
			if vControl is TTabSheet then TTabSheet(vControl).PageControl.ActivePage := TTabSheet(vControl);
			vControl := vControl.Parent;
		end;
	end;
begin
    try
        Result       := False;
        vErrores     := TStringList.Create;
        vFocoSeteado := False;

	    for vVeces := Low(Controles) to High(Controles) do
		    if not Condiciones[vVeces] then begin
                vErrores.Add(Mensajes[vVeces]);
                if not Silencioso then begin
                    if not vFocoSeteado then begin
                        if Controles[vVeces] is TWinControl then begin
                            ActivarTab(Controles[vVeces]);
                            if TWinControl(Controles[vVeces]).Enabled and TWinControl(Controles[vVeces]).Visible then begin
                                TWinControl(Controles[vVeces]).SetFocus;

                                vFocoSeteado := True;
                            end;
                        end;
                    end;
                end;
            end;

        if vErrores.Count > 0 then begin
            if not Silencioso then ShowMsgBoxCN(Titulo, vErrores.Text, MB_ICONWARNING, Propietario)
        end
        else Result := True;
        
    finally
        if Assigned(vErrores) then FreeAndNil(vErrores);
    end;
end;

function ArmarNombrePersonaCN(pConexionADO: TADOConnection; pPersoneria, pNombre, pApellido, pApellidoMaterno: String): String;
    const
        cSQL = 'SELECT dbo.ArmarNombrePersona(%s, %s, %s,%s)';
begin

    Result :=
        Trim(
            QueryGetValue(
                pConexionADO,
                Format(
                    cSQL,
                    [QuotedStr(Trim(pPersoneria)), QuotedStr(Trim(pApellido)), QuotedStr(Trim(pApellidoMaterno)), QuotedStr(Trim(pNombre))])));
end;

{------------------------------------------------------------
        NowBaseCN
        
Author      : mbecerra
Date        : 25-Nov-2011
Description : Devuelve la fecha y hora de la Base de Datos
-------------------------------------------------------------}
function NowBaseCN(Database:TADOConnection) : TDateTime;
resourcestring
    cSQL = 'SELECT GETDATE()';
    
begin
    Result := QueryGetValueDateTime(Database, cSQL);
end;


function FormatearRUTCAC(pRUT: string): string;
    var
        Digito: string[01];
        RUT: Extended;
    	CadenaRUT: string;
    const
    	cFormatoRUT = '%12.0n-%s';
begin
    try
        CadenaRUT := Trim(pRUT);
    	Digito    := Copy(CadenaRUT, Length(CadenaRUT), 1);
        RUT	      := StrToInt(Copy(CadenaRUT, 1, Length(CadenaRUT) - 1));

        Result    := StringReplace(Trim(Format(cFormatoRUT, [RUT, Digito])), ',', '.', [rfReplaceAll]);
    except
    	on e: Exception do begin
            raise Exception.Create(e.Message);
        end;
    end;
end;


function ExportarReporteAPDF(pReporte: TppReport; pNombreArchivo: string): Boolean;
    var
        Exportador: TppPDFDevice;
begin
	try
    	Result := False;

    	try
            Exportador := TppPDFDevice.Create(nil);                                                                                                                         // SS_995_PDO_20111006
                                                                                                                                                                            // SS_995_PDO_20111006
            TppPDFDevice(Exportador).PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
            TppPDFDevice(Exportador).PDFSettings.ScaleImages := False;                                                                                                      // SS_995_PDO_20111006
            TppPDFDevice(Exportador).FileName := pNombreArchivo;                                                                                                             // SS_995_PDO_20111006
                                                                                                                                                                            // SS_995_PDO_20111006
            Exportador.Publisher := pReporte.Publisher;                                                                                                                      // SS_995_PDO_20111006
            Exportador.Reset;                                                                                                                                               // SS_995_PDO_20111006
            pReporte.PrintToDevices;

            Result := True;
        except
        	on e: Exception do begin
            	raise Exception.Create(e.message);
            end;
        end;
    finally
    	if Assigned(Exportador) then FreeAndNil(Exportador);
    end;
end;

function MesNombre(Fecha: TDateTime): String;
resourcestring
    STR_ENERO      = 'Enero';
    STR_FEBRERO    = 'Febrero';
    STR_MARZO      = 'Marzo';
    STR_ABRIL      = 'Abril';
    STR_MAYO       = 'Mayo';
    STR_JUNIO      = 'Junio';
    STR_JULIO      = 'Julio';
    STR_AGOSTO     = 'Agosto';
    STR_SEPTIEMBRE = 'Septiembre';
    STR_OCTUBRE    = 'Octubre';
    STR_NOVIEMBRE  = 'Noviembre';
    STR_DICIEMBRE  = 'Diciembre';
begin
    Case Month(Fecha) of
        1: result   := STR_ENERO;
        2: result   := STR_FEBRERO;
        3: result   := STR_MARZO;
        4: result   := STR_ABRIL;
        5: result   := STR_MAYO;
        6: result   := STR_JUNIO;
        7: result   := STR_JULIO;
        8: result   := STR_AGOSTO;
        9: result   := STR_SEPTIEMBRE;
        10: result  := STR_OCTUBRE;
        11: result  := STR_NOVIEMBRE;
        12: result  := STR_DICIEMBRE;
        else result := STR_ERROR;
    end;
end;

{------------------------------------------------------------
        ObtenerCodigoModuloTransaccionesRechazadasOC

Author      : CQuezada
Date        : 29-08-2012
Firma       : SS_1006_1015_CQU_20120829
Description : Devuelve el c�digo del m�dulo
              "Interfaz Saliente Archivos Transacciones
              Rechazadas Otras Concesionarias"
-------------------------------------------------------------}
function ObtenerCodigoModuloTransaccionesRechazadasOC(DataBase : TADOConnection): Integer;
const
    CONST_CODIGO_MODULO_INTERFAZ_TRANSACCIONES_RECHAZADAS = 'SELECT dbo.CONST_CODIGO_MODULO_INTERFAZ_TRANSACCIONES_RECHAZADAS()';
begin
    try
        Result := QueryGetValueInt(DataBase, CONST_CODIGO_MODULO_INTERFAZ_TRANSACCIONES_RECHAZADAS);
    except
        on e: Exception do begin
            raise Exception.Create(e.message);
        end;
    end;
end;

{------------------------------------------------------------
        ObtenerCodigoModuloGenerarArchivoIvaOC

Author      : CQuezada
Date        : 29-08-2012
Firma       : SS_1006_1015_CQU_20120829
Description : Devuelve el c�digo del m�dulo de
              "Interfaz Saliente IVA Otras Concesionarias"
-------------------------------------------------------------}
function ObtenerCodigoModuloGenerarArchivoIvaOC(DataBase : TADOConnection): Integer;
const
    cSQL_CONST_CODIGO_MODULO_INTERFAZ_IVA_OTRAS_CONCESIONARIAS = 'SELECT dbo.CONST_CODIGO_MODULO_INTERFAZ_IVA_OTRAS_CONCESIONARIAS()';
begin
    try
        Result := QueryGetValueInt(DataBase, cSQL_CONST_CODIGO_MODULO_INTERFAZ_IVA_OTRAS_CONCESIONARIAS);
    except
        on e: Exception do begin
            raise Exception.Create(e.message);
        end;
    end;
end;

{------------------------------------------------------------
        ObtenerCodigoModuloSalidaFoliosFacturacionOC

Author      : CQuezada
Date        : 29-08-2012
Firma       : SS_1006_1015_CQU_20120829
Description : Devuelve el c�digo del m�dulo de Archivos
              Transacciones Rechazadas Otras Concesionarias
-------------------------------------------------------------}
function ObtenerCodigoModuloSalidaFoliosFacturacionOC(DataBase : TADOConnection): Integer;
const
    cSQL_CONST_CODIGO_MODULO_INTERFAZ_FOLIO_OTRAS_CONCESIONARIAS = 'SELECT dbo.CONST_CODIGO_MODULO_INTERFAZ_FOLIO_OTRAS_CONCESIONARIAS()';
begin
    try
        Result := QueryGetValueInt(DataBase, cSQL_CONST_CODIGO_MODULO_INTERFAZ_FOLIO_OTRAS_CONCESIONARIAS);
    except
        on e: Exception do begin
            raise Exception.Create(e.message);
        end;
    end;
end;

{------------------------------------------------------------
        ObtenerCodigoModuloSalidaFoliosPagosOC

Author      : CQuezada
Date        : 29-08-2012
Firma       : SS_1006_1015_CQU_20120829
Description : Devuelve el c�digo del m�dulo
              Archivos Pagos Otras Concesionarias
-------------------------------------------------------------}
function ObtenerCodigoModuloSalidaFoliosPagosOC(DataBase : TADOConnection): Integer;
const
    cSQL_CONST_CODIGO_MODULO_INTERFAZ_PAGOS_OTRAS_CONCESIONARIAS = 'SELECT dbo.CONST_CODIGO_MODULO_INTERFAZ_PAGOS_OTRAS_CONCESIONARIAS()';
begin
    try
        Result := QueryGetValueInt(DataBase, cSQL_CONST_CODIGO_MODULO_INTERFAZ_PAGOS_OTRAS_CONCESIONARIAS);
    except
        on e: Exception do begin
            raise Exception.Create(e.message);
        end;
    end;
end;

{------------------------------------------------------------
        ObtenerTipoImagenPorRegistratioAccesibility

Author      : CQuezada
Date        : 11-10-2013
Firma       : SS_1138_CQU_20131009
Description : Devuelve el tipo de imagen (TTIpoImagen) correspondiente al RegistrationAccessisbility
-------------------------------------------------------------}
function ObtenerTipoImagenPorRegistratioAccesibility(RegistrationAccessisbility : Integer) : TTipoImagen;
var
    I : Integer;
begin
    // tiFrontal = 0, tiPosterior = 1, tiOverview1 = 2, tiOverview2 = 3, tiOverview3 = 4, tiFrontal2 = 5, tiPosterior2 = 6, tiDesconocida = 7
    for I := 0 to  Integer(High(TTipoImagen)) do begin
        if RegistrationAccessibilityImagen[TTipoImagen(I)] = RegistrationAccessisbility then begin
            Result := TTipoImagen(I);
            Break;
        end;
    end;
end;

end.
