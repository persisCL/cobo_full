object frmLoginValuesEditor: TfrmLoginValuesEditor
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Editar Configuraci'#243'n'
  ClientHeight = 144
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 417
    Height = 97
  end
  object lblSection: TLabel
    Left = 24
    Top = 14
    Width = 46
    Height = 14
    Caption = 'Section'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblkey: TLabel
    Left = 24
    Top = 40
    Width = 20
    Height = 14
    Caption = 'Key'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Shape1: TShape
    Left = 23
    Top = 33
    Width = 394
    Height = 1
  end
  object btnAceptar: TButton
    Left = 264
    Top = 112
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 344
    Top = 112
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object txtValue: THistoryEdit
    Left = 24
    Top = 64
    Width = 345
    Height = 21
    TabOrder = 2
    Text = 'txtValue'
  end
end
