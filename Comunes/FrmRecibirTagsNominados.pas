{-----------------------------------------------------------------------------
 File Name: FrmRecibirTagsNominados
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 25/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura dentro del DFM

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

-----------------------------------------------------------------------------}
unit FrmRecibirTagsNominados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ADODB, Provider, DB, DBClient,
  Grids, DBGrids, DMConnection, UtilProc, PeaTypes, UtilDB, Util, RStrings, PeaProcs,
  VariantComboBox, FrmObservacionesGeneral, ValEdit, StrUtils;

type
  TGuiasDespacho = record
    CodigoGuiaDespacho: integer;
    NumeroGuiaDespacho: string[20];
    CodigoAlmacenOrigen: integer;
    CodigoAlmacenDestino: integer;
    CodigoTransportista: integer;
    Estado: String[1];
    FechaHoraEnvio: TDateTime;
    FechaHoraRecepcion: TDateTime;
	FechaHoraAceptacionRechazo: TDateTime;
    Observacion: string;
  end;

  TFormRecibirTagsNominados = class(TForm)
    pnlTop: TPanel;
    dsRecibirTags: TDataSource;
    pnlBottom: TPanel;
    lblGuiaDespacho: TLabel;
    ActualizarStockTagsRecibidos: TADOStoredProc;
    lblTransportista: TLabel;
    cbTransportistas: TVariantComboBox;
    lblAlmacenDestino: TLabel;
    cbGuiaDespacho: TComboBox;
    ObtenerDatosGuiaDespacho: TADOStoredProc;
    ActualizarEstadoGuiaDespacho: TADOStoredProc;
	txtAlmacenDestino: TEdit;
    ObtenerGuiaDespachoCategorias: TADOStoredProc;
    cdsTAGs: TClientDataSet;
    dsTAGs: TDataSource;
    vleRecibidos: TValueListEditor;
    vleIngresados: TValueListEditor;
    spVerificarTAGEnArchivos: TADOStoredProc;
    spObtenerDatosRangoTAGsTransporte: TADOStoredProc;
    PasarAMaestroTAGs: TADOStoredProc;
    RegistrarMovimientoStockNominado: TADOStoredProc;
    spObtenerDatosPuntoEntrega: TADOStoredProc;
    spRegistrarMovimientoStock: TADOStoredProc;
    dbgRecibirTags: TDBGrid;
    ObtenerDatosAlmacen: TADOStoredProc;
    cdsRecibirTags: TClientDataSet;
    dbgRecibidos: TDBGrid;
    Qry_Imporar: TADOQuery;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnObservacion: TButton;
    btnBorrarLinea: TButton;
    btn_Importar: TButton;
	procedure btnAceptarClick(Sender: TObject);
    procedure dbgRecibirTagsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsRecibirTagsAfterInsert(DataSet: TDataSet);
    procedure btnObservacionClick(Sender: TObject);
    procedure cdsTAGsNewRecord(DataSet: TDataSet);
	function ValidarCategoria (Categoria: ANSIString): Boolean;
	function ValidarCantidadTAGsEnLinea (TAGInicial, TAGFinal: DWORD; CantidadTAGS: LongInt; Categoria: Integer): Boolean;
    procedure cdsTAGsBeforePost(DataSet: TDataSet);
	procedure ActualizarIngresados (Categoria: ANSIString; Cantidad: LongInt);
	function ValidarCantidadTAGs: Boolean;
	function ValidarRangosSuperpuestos(CdsRangos: TClientDataSet; ConMsg: Boolean=False): Boolean;
    procedure dbgRecibirTagsExit(Sender: TObject);
	function ValidarExistenciaTAG (TextoCaption: ANSIString; Conn: TADOConnection; CodigoTAG : DWORD; StrTipo: ANSIString; ConMsg: Boolean): Byte;
	procedure CalcularTotalesIngresados;
    procedure cbGuiaDespachoChange(Sender: TObject);
    procedure cbTransportistasChange(Sender: TObject);
    procedure btnBorrarLineaClick(Sender: TObject);
    procedure btn_ImportarClick(Sender: TObject);
    procedure cdsTAGsAfterScroll(DataSet: TDataSet);
  private
    FGuiasDespacho: TGuiasDespacho;
    FUsuario: Ansistring;
    FCodigoPuntoEntrega: integer;
    FNoAgregar: boolean;
    FAlmacenNominado: boolean;
	TAGInicial,
    TAGFinal: DWORD;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    function Inicializar(Caption: TCaption; Usuario: AnsiString; CodigoPuntoEntrega: integer = -1; EnPuntoEntrega: boolean = False; CodigoAlmacen: integer = -1): boolean;
  end;

var
  FormRecibirTagsNominados: TFormRecibirTagsNominados;

implementation

{$R *.dfm}

function TFormRecibirTagsNominados.Inicializar(Caption: TCaption; Usuario: AnsiString; CodigoPuntoEntrega: integer = -1; EnPuntoEntrega: boolean = False; CodigoAlmacen: integer = -1): boolean;
resourcestring
	MSG_NO_ENCUENTRA_ID_ALMACEN = 'No se encuentra la identificaci�n del almac�n.';
	MSG_PUNTO_ENTREGA_MAL_ASIGNADO = 'El Punto de Entrega est� mal asignado';
begin
    result := False;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    self.Caption := AnsiReplaceStr(Caption, '&', '');
    FUsuario := Usuario;
    FCodigoPuntoEntrega := CodigoPuntoEntrega;
    FGuiasDespacho.Observacion := '';
    FNoAgregar:= False;
    FGuiasDespacho.CodigoAlmacenDestino := CodigoAlmacen;

    //btn_Importar.Visible:= (InstallIni.ReadInteger('General', 'SoloImpresora', 1) = 0);
    btn_Importar.Visible:= False;

    if EnPuntoEntrega then begin
        if (FCodigoPuntoEntrega < 1) then begin
            Msgbox(MSG_ERROR_PUNTO_ENTREGA, self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end
    else begin
        if (FGuiasDespacho.CodigoAlmacenDestino < 1) then begin
			Msgbox(MSG_NO_ENCUENTRA_ID_ALMACEN, self.Caption, MB_ICONSTOP);
			Exit;
		end;
	end;
	try
		FAlmacenNominado := false;
		if CodigoPuntoEntrega <> -1 then begin
			with spObtenerDatosPuntoEntrega, Parameters do begin
				ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
				ParamByName('@NombreAlmacenDestino').Value := null;
				ParamByName('@CodigoAlmacenDestino').Value := null;
				ParamByName('@SoloNominado').Value := null;
				ExecProc;

				if ParamByName('@NombreAlmacenDestino').value = Null then begin
					MsgBox(MSG_PUNTO_ENTREGA_MAL_ASIGNADO, self.Caption, MB_ICONSTOP);
					Exit;
				end;
				txtAlmacenDestino.Text 	:= ParamByName('@NombreAlmacenDestino').value;
				FGuiasDespacho.CodigoAlmacenDestino 	:= ParamByName('@CodigoAlmacenDestino').value;
				FAlmacenNominado 		:= ParamByName('@SoloNominado').value;
				Close
			end
		end
		else begin
			with ObtenerDatosAlmacen, Parameters do begin
				ParamByName('@CodigoAlmacen').Value := FGuiasDespacho.CodigoAlmacenDestino;
				Open;


				txtAlmacenDestino.Text 	:= Trim(ObtenerDatosAlmacen.FieldByName('Descripcion').AsString);
				FGuiasDespacho.CodigoAlmacenDestino 	:= ObtenerDatosAlmacen.FieldByName('CodigoAlmacen').AsInteger;
				FAlmacenNominado 		:= ObtenerDatosAlmacen.FieldByName('SoloNominado').AsBoolean;
				Close
            end
        end;

        CargarTransportistas(DMConnections.BaseCAC, cbTransportistas, 1);
        if FCodigoPuntoEntrega > 0 then
            CargarGuiaDespacho(DMConnections.BaseCAC,cbGuiaDespacho, FGuiasDespacho.CodigoAlmacenDestino, True, cbTransportistas.Value, FGuiasDespacho.CodigoAlmacenDestino)
        else
            CargarGuiaDespachoOrigen(DMConnections.BaseCAC,cbGuiaDespacho, FGuiasDespacho.CodigoAlmacenDestino, True, cbTransportistas.Value, FGuiasDespacho.CodigoAlmacenDestino);
	except
		on e: Exception do begin
			Screen.Cursor := crDefault;
			MsgBox(MSG_ERROR_RECIBIR_TELEVIAS_GUARDAR + #13#10 +
                    E.Message, self.Caption, MB_ICONSTOP);
			Exit;
		end;
	end;

	btnBorrarLinea.Enabled := False;
	btnObservacion.Enabled := False;
	btnAceptar.Enabled := False;

    result := True;
end;

procedure TFormRecibirTagsNominados.CalcularTotalesIngresados;
begin
    vleRecibidos.Strings.Clear;
    vleIngresados.Strings.Clear;

	with cdsRecibirTags do begin
    	First;
		while not EoF do begin
			vleRecibidos.InsertRow (FieldByName('Categoria').AsString, FieldByName('CantidadTAGsRecibidos').AsString, true);
			vleIngresados.InsertRow (FieldByName('Categoria').AsString, '0', true);
            Next
        end
    end;

	with cdsTAGs do begin
    	First;
        while not EoF do begin
		    ActualizarIngresados (FieldByName('CategoriaTAGs').AsString, FieldByName('CantidadTAGs').AsInteger);
            Next
        end
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormRecibirTagsNominados.btnAceptarClick(Sender: TObject);
	var
    TodoOk, TodoCero, Diferencias: Boolean;
    CantidadNoNominada: longint;
	idxCategoria, CodigoAlmacenTransportista: Integer;
resourcestring
	MSG_ASIGNAR_GUIA_DESPACHO = 'Debe seleccionar una gu�a de despacho.';
	MSG_ASIGNAR_CANTIDAD = 'Debe asignar alguna cantidad.';
	MSG_DETALLAR_OBSERVACION_DIFERENCIAS = 'Debe detallar una observaci�n por las diferencias de cantidades.';
begin
	if (not cdsTAGs.Active) or (not cdsRecibirTAGs.Active) then begin
		MsgBoxBalloon(MSG_ASIGNAR_GUIA_DESPACHO, self.Caption, MB_ICONSTOP, cbGuiaDespacho);
		Exit;
	end;

	if cdsTAGs.State in [dsEdit, dsInsert] then cdsTAGs.Post;
	if cdsRecibirTAGs.State in [dsEdit, dsInsert] then cdsRecibirTAGs.Post;
	if cdsTAGs.IsEmpty then Exit;

	CalcularTotalesIngresados;

	if (cbTransportistas.ItemIndex < 1) then begin
		MsgBoxBalloon(MSG_ERROR_TRANSPORTISTA_SELECCIONAR,self.Caption,MB_ICONSTOP,cbTransportistas);
		Exit;
	end;

	if (cbGuiaDespacho.ItemIndex < 1) then begin
		MsgBoxBalloon(MSG_VALIDAR_GUIA_DESPACHO,self.Caption,MB_ICONSTOP,cbGuiaDespacho);
		Exit;
	end;
	if not ValidarRangosSuperpuestos(cdsTAGs, true) then exit;
	if not ValidarCantidadTAGs then exit;

    cdsRecibirTags.DisableControls;
    cdsRecibirTags.First;
    TodoCero:= not (cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger > 0);
    Diferencias := cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger < cdsRecibirTags.FieldByName('CantidadTags').AsInteger;
    while not cdsRecibirTags.Eof do begin
        TodoCero := (TodoCero and (not (cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger > 0)));
        Diferencias := Diferencias or (cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger < cdsRecibirTags.FieldByName('CantidadTags').AsInteger);
        if (cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger < cdsRecibirTags.FieldByName('CantidadTags').AsInteger) and
           (Trim(FGuiasDespacho.Observacion) = '') then begin
			MsgBoxBalloon(MSG_DETALLAR_OBSERVACION_DIFERENCIAS, self.Caption, MB_ICONSTOP, btnObservacion);
			cdsRecibirTags.EnableControls;
            Exit;
        end;
        cdsRecibirTags.Next;
    end;
    cdsRecibirTags.EnableControls;
    if TodoCero then begin
		MsgBoxBalloon(MSG_ASIGNAR_CANTIDAD, self.Caption, MB_ICONSTOP, dbgRecibirTags);
        Exit;
    end;

	Screen.Cursor := crHourGlass;
    TodoOk:= False;
    DMConnections.BaseCAC.BeginTrans;

    try
        try
	        CodigoAlmacenTransportista := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT AlmacenAsociado FROM MaestroTransportistas  WITH (NOLOCK) WHERE CodigoTransportista = ''' + inttostr(cbTransportistas.Items[cbTransportistas.ItemIndex].Value) + '''');

        	//Para Almacen Nominado
            if not cdsTAGs.IsEmpty then begin
                cdsTAGs.First;
                while not cdsTAGs.Eof do begin
                    with spRegistrarMovimientoStock, Parameters do begin
                    	Close;

	                    ParamByName('@CodigoOperacion').Value 			:= CONST_RECIBIR_TAG_DE_TRANSPORTE;
	                    ParamByName('@TipoMovimiento').Value 			:= CONST_MOVIMIENTO_NOMINADO;
	                    ParamByName('@CodigoEmbalaje').Value 			:= NULL;

    	                ParamByName('@NumeroInicialTAG').Value 			:= EtiquetaToSerialNumber(PadL(trim(cdsTAGs.FieldByName('NumeroInicialTAG').AsString), 11, '0'));
        	            ParamByName('@NumeroFinalTAG').Value 			:= EtiquetaToSerialNumber(PadL(trim(cdsTAGs.FieldByName('NumeroFinalTAG').AsString), 11, '0'));

            	        ParamByName('@CategoriaTAG').Value 				:= cdsTAGs.FieldByName('CategoriaTAGs').AsInteger;
            	        ParamByName('@Cantidad').Value 					:= cdsTAGs.FieldByName('CantidadTAGs').AsInteger;
    	                ParamByName('@EstadoControl').Value 			:= Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_CONTROL_CALIDAD_ACEPTADO()'));
	                    ParamByName('@EstadoSituacion').Value 			:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_DISPONIBLE()');

	                    ParamByName('@CodigoAlmacenOrigen').Value 		:= CodigoAlmacenTransportista;
                        ParamByName('@CodigoAlmacenDestino').Value      := FGuiasDespacho.CodigoAlmacenDestino;
//    	                ParamByName('@CodigoAlmacenDestino').Value 		:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoAlmacenRecepcion FROM PuntosEntrega WHERE CodigoPuntoEntrega = ''' + inttostr(FCodigoPuntoEntrega) + '''');

        	            ParamByName('@GuiaDespacho').Value 				:= FGuiasDespacho.CodigoGuiaDespacho;
            	        ParamByName('@Usuario').Value 					:= FUsuario;

                	    ParamByName('@FechaHoraEvento').Value 			:= NowBase(DMConnections.BaseCAC);
                    	ParamByName('@MotivoEntrega').Value 			:= null;
	                    ParamByName('@ConstanciaCarabinero').Value 		:= null;
    	                ParamByName('@FechaHoraConstancia').Value 		:= null;
        	            ParamByName('@DescripcionMotivoRegistro').Value := null;
            	        ParamByName('@CodigoComunicacion').Value 		:= null;
	                    ExecProc
                    end;

                    cdsTAGs.Next;
                end;
            end
            else begin
                cdsRecibirTags.First;

                while not cdsRecibirTags.Eof do begin
                    CantidadNoNominada := cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger;
                    if (cdsRecibirTags.RecordCount > 0) then begin
                        if vleIngresados.FindRow(Trim(cdsRecibirTags.FieldByName('Categoria').AsString), idxCategoria) then begin
                            CantidadNoNominada := CantidadNoNominada  - strtoint(vleIngresados.Cells[1, idxCategoria]);
                        end;
                    end;

                    with spRegistrarMovimientoStock, Parameters do begin
                    	Close;

                        if CantidadNoNominada <> 0 then begin
                            ParamByName('@CodigoOperacion').Value 			:= CONST_RECIBIR_TAG_DE_TRANSPORTE;
                            ParamByName('@TipoMovimiento').Value 			:= CONST_MOVIMIENTO_NO_NOMINADO;
                            ParamByName('@CodigoEmbalaje').Value 			:= NULL;

                            ParamByName('@NumeroInicialTAG').Value 			:= NULL;
                            ParamByName('@NumeroFinalTAG').Value 			:= NULL;

                            ParamByName('@CategoriaTAG').Value 				:= cdsRecibirTags.FieldByName('Categoria').AsInteger;
                            ParamByName('@Cantidad').Value 					:= CantidadNoNominada;
                            ParamByName('@EstadoControl').Value 			:= NULL;
                            ParamByName('@EstadoSituacion').Value 			:= NULL;

                            ParamByName('@CodigoAlmacenOrigen').Value 		:= CodigoAlmacenTransportista;
                            ParamByName('@CodigoAlmacenDestino').Value      := FGuiasDespacho.CodigoAlmacenDestino;
    //    	                ParamByName('@CodigoAlmacenDestino').Value 		:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoAlmacenRecepcion FROM PuntosEntrega WHERE CodigoPuntoEntrega = ''' + inttostr(FCodigoPuntoEntrega) + '''');

                            ParamByName('@GuiaDespacho').Value 				:= FGuiasDespacho.CodigoGuiaDespacho;
                            ParamByName('@Usuario').Value 					:= FUsuario;

                            ParamByName('@FechaHoraEvento').Value 			:= NowBase(DMConnections.BaseCAC);
                            ParamByName('@MotivoEntrega').Value 			:= null;
                            ParamByName('@ConstanciaCarabinero').Value 		:= null;
                            ParamByName('@FechaHoraConstancia').Value 		:= null;
                            ParamByName('@DescripcionMotivoRegistro').Value := null;
                            ParamByName('@CodigoComunicacion').Value 		:= null;

                            ExecProc
                        end;
                    end;

                    cdsRecibirTags.Next;
                end;
            end;

            //Actualizo el Estado de la Guia de Despacho
            ActualizarEstadoGuiaDespacho.Close;
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@CodigoGuiaDespacho').Value := FGuiasDespacho.CodigoGuiaDespacho;
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@FechaHoraRecepcion').Value := NowBase(DMConnections.BaseCAC);
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@FechaAceptacionRechazo').Value := NULL;
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Observacion').Value := iif(Trim(FGuiasDespacho.Observacion) = '', NULL, FGuiasDespacho.Observacion);
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Estado').Value := iif(Diferencias, ESTADO_ACEPTADO_CON_OBSERVACIONES, ESTADO_ACEPTADO);
            ActualizarEstadoGuiaDespacho.ExecProc;

            TodoOk:= True;
        except
		    on e: Exception do begin
			    Screen.Cursor := crDefault;

                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ERROR_RECIBIR_TELEVIAS_GUARDAR, e.message, self.caption, MB_ICONSTOP);

			    Exit;
    		end;
        end;
    finally
        Screen.Cursor := crDefault;
        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;
            MsgBox(MSG_CAPTION_RECEPCION, self.Caption, MB_ICONINFORMATION);
            ModalResult := mrOk;
        end
    end;
end;

procedure TFormRecibirTagsNominados.dbgRecibirTagsKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
end;

procedure TFormRecibirTagsNominados.cdsRecibirTagsAfterInsert(
  DataSet: TDataSet);
begin
    if (not FNoAgregar) and (Trim(DataSet.FieldByName('Descripcion').AsString) = '') then begin
        DataSet.Delete;
        btnAceptar.SetFocus;
    end;
end;

procedure TFormRecibirTagsNominados.btnObservacionClick(Sender: TObject);
var
    f: TFormObservacionesGeneral;
begin
    if (cbTransportistas.ItemIndex < 1) then begin
        MsgBoxBalloon(MSG_ERROR_TRANSPORTISTA_SELECCIONAR,self.Caption,MB_ICONSTOP,cbTransportistas);
        Exit;
    end;

    if (cbGuiaDespacho.ItemIndex < 1) then begin
        MsgBoxBalloon(MSG_VALIDAR_GUIA_DESPACHO,self.Caption,MB_ICONSTOP,cbGuiaDespacho);
        Exit;
    end;

(*
    if (btnObservacion.Caption = '&' + STR_OBSERVACION) then begin
        Application.CreateForm(TFormObservacionesGeneral, f);
        f.Inicializa(FGuiasDespacho.Observacion, False);
        f.ShowModal;
        FGuiasDespacho.Observacion := f.Observaciones;
        f.Release;
        Exit;
    end;
*)

    Application.CreateForm(TFormObservacionesGeneral, f);
    f.Inicializa(FGuiasDespacho.Observacion, False);
    f.ShowModal;
    FGuiasDespacho.Observacion := f.Observaciones;
    f.Release;
    Exit;

(*
    //Actualizo el Estado de la Guia de Despacho
    ActualizarEstadoGuiaDespacho.Close;
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@CodigoGuiaDespacho').Value := FGuiasDespacho.CodigoGuiaDespacho;
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@FechaHoraRecepcion').Value := iif(FCodigoPuntoEntrega<>-1, NowBase(DMConnections.BaseCAC), NULL);
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Estado').Value := ESTADO_RECHAZADO;
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Observacion').Value := Observacion;
    ActualizarEstadoGuiaDespacho.ExecProc;
*)
end;

procedure TFormRecibirTagsNominados.cdsTAGsNewRecord(DataSet: TDataSet);
begin
    cdsTAGs.FieldByName ('NumeroInicialTAG').AsFloat:= 0;
    cdsTAGs.FieldByName ('NumeroFinalTAG').AsFloat 	:= 0;
    cdsTAGs.FieldByName ('CantidadTAGs').AsInteger 	:= 0;
//    cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger := cdsEntrega.FieldByName ('Categoria').AsInteger;
    cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger := 1;
(*
    cdsTAGs.FieldByName ('NumeroInicialTAG').AsFloat:= 0;
    cdsTAGs.FieldByName ('NumeroFinalTAG').AsFloat 	:= 0;
    cdsTAGs.FieldByName ('CantidadTAGs').AsInteger 	:= 0;
    cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger := cdsRecibirTags.FieldByName ('Categoria').AsInteger;
*)
end;

function TFormRecibirTagsNominados.ValidarCantidadTAGs: Boolean;
var
	idxCategoria: Integer;
begin
	result := true;
    cdsRecibirTags.first;

    for idxCategoria := 1 to vleRecibidos.RowCount-1 do begin
    	case FAlmacenNominado of
    		true: result := result and (StrToInt(vleRecibidos.Cells[1, idxCategoria]) = StrToInt (vleIngresados.Cells[1, idxCategoria]));
//            else result := result and (StrToInt(vleRecibidos.Cells[1, idxCategoria]) >= StrToInt (vleIngresados.Cells[1, idxCategoria]));
            else result := result and ((StrToInt (vleIngresados.Cells[1, idxCategoria])=0) And (cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger <= cdsRecibirTags.FieldByName('CantidadTags').AsInteger)) or
										((StrToInt(vleRecibidos.Cells[1, idxCategoria]) = StrToInt (vleIngresados.Cells[1, idxCategoria])) And (cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger <= cdsRecibirTags.FieldByName('CantidadTags').AsInteger))
        end;
	    if not result then begin
            MsgBox(iif(FAlmacenNominado, MSG_ERROR_TOTAL_INGRESADO_NOMINADO, Format (MSG_ERROR_TOTAL_INGRESADO,[IntToStr(idxCategoria)])), self.Caption, MB_OK + MB_ICONSTOP);
            Exit;
        end;
       cdsRecibirTags.Next;
    end;
end;

function TFormRecibirTagsNominados.ValidarRangosSuperpuestos(CdsRangos: TClientDataSet; ConMsg: Boolean=False): Boolean;
var
	Actual: Integer;
    bm: TBookmark;
    TAGError,
    NumeroInicialTAG,
    NumeroFinalTAG: DWORD;
begin
    TAGError := 0;
    result := true;
    cursor := crHourGlass;
    cdsRangos.disableControls;
    try
        with cdsRangos do begin
            first;
            while not eof do begin
                // Tomamos el registro actual para no verificar
                Actual := RecNo;
                NumeroInicialTAG := EtiquetaToSerialNumber(PadL(FieldByName('NumeroInicialTAG').asString,11,'0'));
                NumeroFinalTAG := EtiquetaToSerialNumber(PadL(FieldByName('NumeroFinalTAG').asString,11,'0'));
                bm := GetBookMark;
                try
                    // Recorremos el datase verificando el n�mero
                    first;
                    while not eof do begin
                        if Actual = RecNo then
                            next
                        else begin
                            if ((NumeroInicialTAG >= EtiquetaToSerialNumber(PadL(FieldByName('NumeroInicialTAG').asString,11,'0'))) and
                              (NumeroInicialTAG <= EtiquetaToSerialNumber(PadL(FieldByName('NumeroFinalTAG').asString,11,'0')))) then begin
                              	TAGError := NumeroInicialTAG;
                                result := false;
                                break;
                            end;

                            if ((NumeroFinalTAG >= EtiquetaToSerialNumber(PadL(FieldByName('NumeroInicialTAG').asString,11,'0'))) and
                              (NumeroFinalTAG <= EtiquetaToSerialNumber(PadL(FieldByName('NumeroFinalTAG').asString,11,'0')))) then begin
                              	TAGError := NumeroFinalTAG;
                                result := false;
                                break;
                            end;
                        next;
                        end;
                    end;
                    GotoBookMark(bm);
                    next;
                finally
                    FreeBookMark(bm);
                end;
            end;
        end;
    finally
        cdsRangos.EnableControls;
        cursor := crDefault;
    end;

	if ConMsg and (not result) then MsgBox(SerialNumberToEtiqueta (IntToStr(TAGError)) + ' - ' + MSG_ERROR_RANGO_SUPERPUESTO, self.Caption, MB_OK + MB_ICONSTOP);
end;

procedure TFormRecibirTagsNominados.dbgRecibirTagsExit(Sender: TObject);
begin
    if cdsRecibirTAGs.State in [dsEdit, dsInsert] then cdsRecibirTAGs.Post;
end;

function TFormRecibirTagsNominados.ValidarExistenciaTAG (TextoCaption: ANSIString; Conn: TADOConnection; CodigoTAG : DWORD; StrTipo: ANSIString; ConMsg: Boolean): Byte;
begin
    with spVerificarTAGEnArchivos, Parameters do begin
        ParamByName('@ContractSerialNumber').Value := CodigoTAG;
        ExecProc;

       	result := ParamByName('@RETURN_VALUE').Value;
        if ConMsg and (result = TAG_NO_EXISTENTE) then MsgBox(StrTipo + MSG_ERROR_TAG_NO_EXISTE_EN_LISTA, TextoCaption, MB_OK + MB_ICONSTOP);

        Close;
    end
end;

function TFormRecibirTagsNominados.ValidarCategoria (Categoria: ANSIString): Boolean;
var
	idxCategoria: Integer;
begin
    result := vleRecibidos.FindRow(Categoria, idxCategoria);
    if not result then MsgBox(MSG_ERROR_CATEGORIA_FUERA_DE_LISTA, self.Caption, MB_OK + MB_ICONSTOP);
end;

function TFormRecibirTagsNominados.ValidarCantidadTAGsEnLinea (TAGInicial, TAGFinal: DWORD; CantidadTAGS: LongInt; Categoria: Integer): Boolean;
begin
   	result := CantidadTAGs > 0;
    if not result then begin
    	MsgBox(MSG_ERROR_CANTIDAD_ES_CERO, self.Caption, MB_OK + MB_ICONSTOP);
        Exit
    end;

    with spObtenerDatosRangoTAGsTransporte, Parameters do begin
        Close;
        ParamByName('@TAGInicial').Value 		:= TAGInicial;
        ParamByName('@TAGFinal').Value  		:= TAGFinal;
        ParamByName('@CategoriaTAG').Value		:= Categoria;
        ParamByName('@CodigoTransportista').Value:= cbTransportistas.Items[cbTransportistas.ItemIndex].Value;
        Open;

        result := FieldByName('CantidadTAGsCategoria').AsInteger = CantidadTAGS;

        Close
    end;

    if not result then begin
    	MsgBox(MSG_ERROR_CANTIDAD_TAGS_CATEGORIA, self.Caption, MB_OK + MB_ICONSTOP);
        Exit
    end;
end;

procedure TFormRecibirTagsNominados.cdsTAGsBeforePost(DataSet: TDataSet);

begin
	if (cdsTAGs.FieldByName ('NumeroInicialTAG').AsFloat <> 0) and
       (cdsTAGs.FieldByName ('NumeroFinalTAG').AsFloat = 0) and
       (cdsTAGs.FieldByName ('CantidadTAGs').AsFloat = 0) and
       (cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger = 1) then begin
        cdsTAGs.FieldByName ('NumeroFinalTAG').AsFloat 	:= cdsTAGs.FieldByName ('NumeroInicialTAG').AsFloat;
        cdsTAGs.FieldByName ('CantidadTAGs').AsInteger 	:= 1;
        cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger := 1;
    end;

	TAGInicial := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroInicialTAG').AsString, 11, '0'));
   	TAGFinal  := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroFinalTAG').AsString, 11, '0'));

	if not ValidarEtiqueta(self.caption, 'Telev�a Inicial', DataSet.FieldByName('NumeroInicialTAG').AsString, DAR_MENSAJE) then abort;
	if not ValidarEtiqueta(self.caption, 'Telev�a Final', DataSet.FieldByName('NumeroFinalTAG').AsString, DAR_MENSAJE) then abort;
	if not ValidarRangosTAGs(self.caption, TAGInicial, TAGFinal, true, DAR_MENSAJE) then abort;
	if not ValidarCategoria (DataSet.FieldByName('CategoriaTAGs').AsString) then abort;
	if not ValidarCategoriaTAG(self.caption, 'Telev�a Inicial', TAGInicial, DataSet.FieldByName('CategoriaTAGs').AsInteger, DAR_MENSAJE) then abort;
	if not ValidarCategoriaTAG(self.caption, 'Telev�a Final', TAGFinal, DataSet.FieldByName('CategoriaTAGs').AsInteger, DAR_MENSAJE) then abort;
	if not ValidarCantidadTAGsEnLinea (TAGInicial, TAGFinal, DataSet.FieldByName('CantidadTAGs').AsInteger, DataSet.FieldByName('CategoriaTAGs').AsInteger) then abort;
   	if ValidarExistenciaTAG (self.caption, DMConnections.BaseCAC, TAGInicial, 'Telev�a Inicial', DAR_MENSAJE) = TAG_NO_EXISTENTE then abort;
	if ValidarexistenciaTAG (self.caption, DMConnections.BaseCAC, TAGFinal, 'Telev�a Final', DAR_MENSAJE) = TAG_NO_EXISTENTE then abort;
end;

procedure TFormRecibirTagsNominados.ActualizarIngresados (Categoria: ANSIString; Cantidad: LongInt);
var
    idxCategoria: Integer;
begin
	vleIngresados.FindRow(Categoria, idxCategoria);
	vleIngresados.Cells[1, idxCategoria] := IntToStr(StrToInt(vleIngresados.Cells[1, idxCategoria]) + Cantidad)
end;

procedure TFormRecibirTagsNominados.cbGuiaDespachoChange(Sender: TObject);
var
	CategoriaAnterior: integer;
begin
	try
		if (cbTransportistas.ItemIndex < 1) or (cbGuiaDespacho.ItemIndex < 1) then Exit;

		ObtenerDatosGuiaDespacho.Close;
		ObtenerDatosGuiaDespacho.Parameters.ParamByName('@CodigoTransportista').Value := cbTransportistas.Items[cbTransportistas.ItemIndex].Value;
		ObtenerDatosGuiaDespacho.Parameters.ParamByName('@NumeroGuiaDespacho').Value := Trim(cbGuiaDespacho.Text);
		ObtenerDatosGuiaDespacho.Parameters.ParamByName('@CodigoGuiaDespacho').Value := Ival(StrRight(cbGuiaDespacho.Text, 10));
//        ObtenerDatosGuiaDespacho.Parameters.ParamByName('@Estado').Value := FGuiasDespacho.Estado[1];
		ObtenerDatosGuiaDespacho.Open;

(*
		if FCodigoPuntoEntrega<>-1 then begin
			CodigoAlmacenDestino := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoAlmacenRecepcion FROM PuntosEntrega WHERE CodigoPuntoEntrega = ''' + inttostr(FCodigoPuntoEntrega) + '''');

			if (ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenDestino').AsInteger <> CodigoAlmacenDestino) then begin
				MsgBox(MSG_ERROR_RECEPCION_TAGS, self.Caption, MB_ICONSTOP);
				Exit;
			end;

		end else begin
			txtAlmacenDestino.Text := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MaestroAlmacenes WHERE CodigoAlmacen = ''' + ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenDestino').AsString + ''''));
			FGuiasDespacho.Observacion := ObtenerDatosGuiaDespacho.FieldByName('Observacion').AsString;
			FGuiasDespacho.CodigoAlmacenOrigen := ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenOrigen').AsInteger;
			FGuiasDespacho.CodigoAlmacenDestino := ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenDestino').AsInteger;
		end;
*)
		FGuiasDespacho.CodigoGuiaDespacho := ObtenerDatosGuiaDespacho.FieldByName('CodigoGuiaDespacho').AsInteger;
		FGuiasDespacho.Observacion := ObtenerDatosGuiaDespacho.FieldByName('Observacion').AsString;
		FGuiasDespacho.Estado[1] := ObtenerDatosGuiaDespacho.FieldByName('Estado').AsString[1];

		ObtenerGuiaDespachoCategorias.Close;
		ObtenerGuiaDespachoCategorias.Parameters.ParamByName('@CodigoGuiaDespacho').Value := ObtenerDatosGuiaDespacho.FieldByName('CodigoGuiaDespacho').AsInteger;
		ObtenerGuiaDespachoCategorias.Open;

		cdsRecibirTags.Close;
		cdsRecibirTags.CreateDataSet;
		cdsRecibirTags.Open;

		cdsTAGs.Close;
		cdsTAGs.CreateDataSet;
		cdsTAGs.Open;

		ObtenerGuiaDespachoCategorias.First;
		CategoriaAnterior := -1;
		while not ObtenerGuiaDespachoCategorias.Eof do begin
(*
			cdsRecibirTags.AppendRecord([ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsInteger,
			ObtenerGuiaDespachoCategorias.FieldByName('Descripcion').AsString,
			ObtenerGuiaDespachoCategorias.FieldByName('Cantidad').AsInteger,
			ObtenerGuiaDespachoCategorias.FieldByName('CantidadTagsRecibidos').AsInteger]);
*)
			vleRecibidos.InsertRow (ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsString, '0', true);
			FNoAgregar := True;
			if (FGuiasDespacho.Estado[1] = ESTADO_ACEPTADO_CON_OBSERVACIONES) and (CategoriaAnterior = ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsInteger) then
				cdsRecibirTags.Edit
			else begin
				if CategoriaAnterior = ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsInteger then
					cdsRecibirTags.Edit
				else
					cdsRecibirTags.Append;
			end;

			CategoriaAnterior :=  ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsInteger;

			cdsRecibirTags.FieldByName('Categoria').AsInteger := ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsInteger;
			cdsRecibirTags.FieldByName('Descripcion').AsString := ObtenerGuiaDespachoCategorias.FieldByName('Descripcion').AsString;
			if (FGuiasDespacho.Estado[1] = ESTADO_ACEPTADO_CON_OBSERVACIONES) and (cdsRecibirTags.State in [dsEdit]) then
				if ObtenerGuiaDespachoCategorias.FieldByName('CodigoAlmacenOrigen').AsInteger = FGuiasDespacho.CodigoAlmacenDestino then
					cdsRecibirTags.FieldByName('CantidadTags').AsInteger := cdsRecibirTags.FieldByName('CantidadTags').AsInteger + ObtenerGuiaDespachoCategorias.FieldByName('Cantidad').AsInteger
				else
					cdsRecibirTags.FieldByName('CantidadTags').AsInteger := cdsRecibirTags.FieldByName('CantidadTags').AsInteger - ObtenerGuiaDespachoCategorias.FieldByName('Cantidad').AsInteger
			else begin
				if (cdsRecibirTags.State in [dsEdit]) then
					cdsRecibirTags.FieldByName('CantidadTags').AsInteger := cdsRecibirTags.FieldByName('CantidadTags').AsInteger + ObtenerGuiaDespachoCategorias.FieldByName('Cantidad').AsInteger
				else
					cdsRecibirTags.FieldByName('CantidadTags').AsInteger := ObtenerGuiaDespachoCategorias.FieldByName('Cantidad').AsInteger;
			end;
			cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger := ObtenerGuiaDespachoCategorias.FieldByName('CantidadTagsRecibidos').AsInteger;
			cdsRecibirTags.Post;

			ObtenerGuiaDespachoCategorias.Next;
		end;

		ObtenerGuiaDespachoCategorias.Close;

		FNoAgregar:= False;
		cdsRecibirTags.First;
	except
		on e: Exception do begin
			Screen.Cursor := crDefault;
			MsgBox(MSG_ERROR_APERTURA + #13#10 +
					E.Message, self.Caption, MB_ICONSTOP);
			Exit;
		end;
	end;
end;

procedure TFormRecibirTagsNominados.cbTransportistasChange(
  Sender: TObject);
begin
    if cdsRecibirTags.Active then
        cdsRecibirTags.EmptyDataSet;
    if cdsTAGs.Active then
        cdsTAGs.EmptyDataSet;
    if FCodigoPuntoEntrega > 0 then
        CargarGuiaDespacho(DMConnections.BaseCAC,cbGuiaDespacho, FGuiasDespacho.CodigoAlmacenDestino, True, cbTransportistas.Value, FGuiasDespacho.CodigoAlmacenDestino)
    else
        CargarGuiaDespachoOrigen(DMConnections.BaseCAC,cbGuiaDespacho, FGuiasDespacho.CodigoAlmacenDestino, True, cbTransportistas.Value, FGuiasDespacho.CodigoAlmacenDestino);
end;

procedure TFormRecibirTagsNominados.btnBorrarLineaClick(Sender: TObject);
begin
    if (cdsTAGs.Active) and (cdsTAGs.RecordCount > 0) then
        cdsTAGs.Delete;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_ImportarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormRecibirTagsNominados.btn_ImportarClick(Sender: TObject);
var
	cant:Integer;
begin
	try
    	try
        	cant:=0;
            Qry_Imporar.close;
            Qry_Imporar.SQL.Clear;
            //{ INICIO : 20160315 MGO
            Qry_Imporar.SQL.add('SELECT rtrim(ltrim(tag)) as tag,RTRIM(LTRIM(TAGFinal)) as TAGFinal, pos , Cantidad FROM Tabla_Import_TAG  WITH (NOLOCK) where POS = '+ApplicationIni.ReadString('General', 'PuntoEntrega', '-1'));
            {
            Qry_Imporar.SQL.add('SELECT rtrim(ltrim(tag)) as tag,RTRIM(LTRIM(TAGFinal)) as TAGFinal, pos , Cantidad FROM Tabla_Import_TAG  WITH (NOLOCK) where POS = '+InstallIni.ReadString('General', 'PuntoEntrega', '-1'));
            }
            // FIN : 20160315 MGO
            Qry_Imporar.open;
            Qry_Imporar.First;
            with Qry_Imporar do begin
                while not Eof do begin
                    cdsTAGs.Append;
                    cdsTAGs.FieldByName('NumeroInicialTAG').AsFloat:=Qry_Imporar.FieldByName('Tag').AsFloat;
	                    if trim(Qry_Imporar.FieldByName('TAGFinal').AsString)='' then begin
	                    cdsTAGs.FieldByName('NumeroFinalTAG').AsFloat:=Qry_Imporar.FieldByName('Tag').AsFloat;
                        cdsTAGs.FieldByName('CantidadTAGs').AsInteger:=1;
                        inc(cant);
                    end else begin
	                    cdsTAGs.FieldByName('NumeroFinalTAG').AsFloat:=Qry_Imporar.FieldByName('TAGFinal').AsFloat;
                        cdsTAGs.FieldByName('CantidadTAGs').AsInteger:=Qry_Imporar.FieldByName('Cantidad').AsInteger;
                        cant:=cant+Qry_Imporar.FieldByName('Cantidad').AsInteger;
                    end;
					cdsTAGs.FieldByName('CategoriaTAGs').AsInteger:=1;

					cdsTAGs.Post;
                    Next;
                end;
                cdsRecibirTags.Edit;
				cdsRecibirTags.FieldByName('CantidadTagsRecibidos').AsInteger:=cant;
				cdsRecibirTags.Post;
            end;
        except
            on E: exception do begin
				MsgBoxErr(STR_ERROR, e.message, STR_ERROR, MB_ICONSTOP);
            end;
        end;
    finally
    	Qry_Imporar.close;
    end;
end;

procedure TFormRecibirTagsNominados.cdsTAGsAfterScroll(DataSet: TDataSet);
begin
	btnBorrarLinea.Enabled := not cdsTAGs.IsEmpty;
	btnObservacion.Enabled := not cdsTAGs.IsEmpty;
	btnAceptar.Enabled := not cdsTAGs.IsEmpty;
end;

end.







