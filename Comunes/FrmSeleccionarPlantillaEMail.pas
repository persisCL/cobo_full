unit FrmSeleccionarPlantillaEMail;
{*******************************************************************************
    Firma : TASK_124_CFU_20170320
    Author : Claudio Fuentes
    Date : 21/03/2017
    Description : Unidad para seleccionar la plantilla de email a utilizar
*******************************************************************************}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, frmImprimirConvenio, RStrings, PeaProcs,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Convenios,  ConstParametrosGenerales, Util, UtilProc,
  ADODB, SysUtilsCN, PeaProcsCN, frmReporteContratoAdhesionPA, Peatypes, RBSetUp, PRinters, UtilDB,
  DB;
type
  TFormSeleccionarPlantillaEMail = class(TForm)
    gb_Convenio: TGroupBox;
    sb_Convenio: TScrollBox;
    Panel10: TPanel;
    btn_Cancelar: TButton;
    btn_ok: TButton;
    rgPlantillas: TRadioGroup;
    spObtenerPlantillasEMAIL: TADOStoredProc;
    procedure btn_okClick(Sender: TObject);

  private

  public
  	ListaCodigosPlantillas: TStringList;
    function Inicializar(): Boolean;
  end;

var
  FormSeleccionarPlantillaEMail: TFormSeleccionarPlantillaEMail;

implementation

uses DMConnection;

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}

{ TFrmSeleccionarReimpresion }

function TFormSeleccionarPlantillaEMail.Inicializar(): Boolean;
begin
	ListaCodigosPlantillas := TStringList.Create;
	with spObtenerPlantillasEMAIL do begin
        if Active then
        	Close;
        Open;
        if spObtenerPlantillasEMAIL.RecordCount > 20 then begin
            Width					:= 1200;
            rgPlantillas.Columns	:= 3;
        end
        else if spObtenerPlantillasEMAIL.RecordCount > 10 then begin
            Width					:= 800;
            rgPlantillas.Columns	:= 2;
        end
        else begin
            Width					:= 400;
            rgPlantillas.Columns	:= 1;
        end;

        while not Eof do begin
        	rgPlantillas.Items.Add(FieldByName('Asunto').AsString);
            ListaCodigosPlantillas.Add(FieldByName('CodigoPlantilla').AsString);
        	Next;
        	end;
        Close;
    end;
    Result := True;
end;

procedure TFormSeleccionarPlantillaEMail.btn_okClick(Sender: TObject);
resourcestring
	MSG_SIN_SELECCION = 'No ha seleccionado ninguna plantilla';
begin
    try
        if rgPlantillas.ItemIndex < 0 then begin
        	MsgBox(MSG_SIN_SELECCION, Caption, MB_ICONERROR);
            Exit;
        end;

        ModalResult := mrOk;
    finally
    end;
end;

end.
