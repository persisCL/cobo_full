object frmConfirmarImpresion: TfrmConfirmarImpresion
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Confirmar Impresi'#243'n'
  ClientHeight = 87
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object txtEstado: TLabel
    Left = -34
    Top = 12
    Width = 462
    Height = 21
    Alignment = taCenter
    AutoSize = False
    Caption = 'Desea Imprimir los Cambios?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnConfirmar: TButton
    Left = 50
    Top = 50
    Width = 133
    Height = 25
    Caption = 'Confir&ma Impresi'#243'n'
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancelar: TButton
    Left = 229
    Top = 50
    Width = 133
    Height = 25
    Caption = '&Contin'#250'a sin Imprimir'
    Default = True
    ModalResult = 2
    TabOrder = 1
  end
end
