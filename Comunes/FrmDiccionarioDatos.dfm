object FormDiccionarioDatos: TFormDiccionarioDatos
  Left = 129
  Top = 37
  Width = 781
  Height = 541
  Caption = 'Diccionario de Datos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    773
    507)
  PixelsPerInch = 96
  TextHeight = 13
  object TreeView: TTreeView
    Left = 0
    Top = 42
    Width = 773
    Height = 425
    Anchors = [akLeft, akTop, akRight, akBottom]
    Indent = 19
    ReadOnly = True
    TabOrder = 0
    OnChange = TreeViewChange
  end
  object Panel2: TPanel
    Left = 0
    Top = 468
    Width = 773
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 451
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 111
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
        end
      end
    end
  end
  object pnlEncabezado: TPanel
    Left = 0
    Top = 0
    Width = 773
    Height = 42
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 42
      Height = 13
      Caption = 'Servidor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 312
      Top = 16
      Width = 73
      Height = 13
      Caption = 'Base de Datos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtServidor: THistoryEdit
      Left = 57
      Top = 12
      Width = 233
      Height = 21
      TabOrder = 0
    end
    object txtBase: THistoryEdit
      Left = 391
      Top = 12
      Width = 210
      Height = 21
      TabOrder = 1
    end
    object BtnVer: TButton
      Left = 608
      Top = 10
      Width = 70
      Height = 25
      Caption = '&Ver'
      TabOrder = 2
      OnClick = BtnVerClick
    end
  end
  object qry_Campos: TADOQuery
    Connection = DMConnInformes.BaseInformes
    CursorType = ctOpenForwardOnly
    Parameters = <
      item
        Name = 'Tabla'
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 128
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      'CONVERT (varchar (25), COLUMN_NAME) AS Columna, '
      'CONVERT (varchar (15), DATA_TYPE) AS Tipo, '
      'CASE '
      
        '     WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL THEN CONVERT (var' +
        'char (10), CHARACTER_MAXIMUM_LENGTH)'
      
        '     ELSE CONVERT (varchar (7), ISNULL (NUMERIC_PRECISION, 0)) +' +
        ' '#39'.'#39' + CONVERT (varchar (2), ISNULL (NUMERIC_SCALE, 0))'
      'END AS Longitud,'
      'CONVERT (varchar (10), LOWER (IS_NULLABLE)) AS Nulos,'
      
        'CONVERT (varchar (1024), ISNULL (dbo.ObtenerDescripcionCampo (TA' +
        'BLE_NAME, COLUMN_NAME), '#39#39')) AS Descripcion'
      'FROM INFORMATION_SCHEMA.COLUMNS  WITH (NOLOCK) '
      'WHERE'
      'TABLE_NAME = :Tabla'
      'ORDER BY Columna')
    Left = 734
    Top = 11
  end
  object qry_Tablas: TADOQuery
    Connection = DMConnInformes.BaseInformes
    CursorType = ctOpenForwardOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      'TABLE_NAME AS Tabla,'
      
        'CONVERT (varchar (1024), ISNULL (dbo.ObtenerDescripcionTabla (TA' +
        'BLE_NAME), '#39#39')) AS Descripcion'
      'FROM INFORMATION_SCHEMA.TABLES  WITH (NOLOCK) '
      'WHERE '
      'TABLE_TYPE = '#39'BASE TABLE'#39
      ''
      'ORDER BY Tabla')
    Left = 702
    Top = 11
  end
end
