unit frmMuestraPDF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, AcroPDFLib_TLB, StdCtrls, ExtCtrls, Util, frmMuestraMensaje;

type
  TMuestraPDFForm = class(TForm)
    pnlAcciones: TPanel;
    btnSalir: TButton;
    Panel1: TPanel;
    AcroPDF1: TAcroPDF;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure MuestraPDF(pFicheroPDF, pTituloVentana: string; pFormMDIChild: boolean = False; pFormShowModal: Boolean = False);
  end;

var
  MuestraPDFForm: TMuestraPDFForm;

implementation

{$R *.dfm}

class procedure TMuestraPDFForm.MuestraPDF(pFicheroPDF, pTituloVentana: string; pFormMDIChild: boolean = False; pFormShowModal: Boolean = False);
    var
        FormMuestraPDF: TMuestraPDFForm;
        FormSize: TSize;

    procedure CargarPDF;
    begin
        with FormMuestraPDF.AcroPDF1 do begin
            Realign;
            Refresh;
            src := pFicheroPDF;
            setCurrentPage(1);
            setShowToolbar(True);
        end;
    end;

begin
    FormMuestraPDF := TMuestraPDFForm.Create(Application);
    with FormMuestraPDF do begin
        FormStyle    := iif(pFormMDIChild, fsMDIChild, fsNormal);
        Caption      := pTituloVentana;


        if (not pFormMDIChild) and pFormShowModal then begin
            BorderIcons := BorderIcons - [biMinimize];
            Height      := Application.MainForm.Height - 50;
            Width       := Application.MainForm.Width - 50;
            Position    := poMainFormCenter;

            CargarPDF;
            ShowModal;
        end
        else begin
            SetBounds(0, 0, Application.MainForm.Width - 25, Application.MainForm.Height - 25);

            CargarPDF;
            Show;
        end;
    end;
end;

procedure TMuestraPDFForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TMuestraPDFForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TMuestraPDFForm.FormCreate(Sender: TObject);
begin
    FFormState := FFormState - [fsVisible];
end;

procedure TMuestraPDFForm.FormShow(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

end.
