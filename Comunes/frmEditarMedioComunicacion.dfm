object formEditarMedioComunicacion: TformEditarMedioComunicacion
  Left = 198
  Top = 191
  BorderStyle = bsDialog
  Caption = 'Editar Medio de Comunicaci'#243'n'
  ClientHeight = 190
  ClientWidth = 586
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 12
    Top = 8
    Width = 561
    Height = 137
  end
  object Label1: TLabel
    Left = 50
    Top = 19
    Width = 61
    Height = 13
    Caption = 'Tipo Dato:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 50
    Top = 119
    Width = 74
    Height = 13
    Caption = 'Observaciones:'
  end
  object pnlInferior: TPanel
    Left = 0
    Top = 152
    Width = 586
    Height = 38
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      586
      38)
    object BtnAceptar: TButton
      Left = 421
      Top = 7
      Width = 79
      Height = 26
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = BtnAceptarClick
    end
    object BtnCancelar: TButton
      Left = 501
      Top = 7
      Width = 79
      Height = 26
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = BtnCancelarClick
    end
  end
  object cbTipoMedioContacto: TVariantComboBox
    Left = 155
    Top = 15
    Width = 282
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 0
    OnChange = cbTipoMedioContactoChange
    Items = <>
  end
  object Notebook: TNotebook
    Left = 45
    Top = 42
    Width = 516
    Height = 71
    TabOrder = 1
    object TPage
      Left = 0
      Top = 0
      Caption = 'Telefono'
      inline FreTelefono: TFrameTelefono
        Left = 7
        Top = 8
        Width = 506
        Height = 57
        TabOrder = 0
        inherited lblDesde: TLabel
          Left = 5
          Top = 33
          Width = 37
          Caption = 'Horario:'
        end
        inherited lblHasta: TLabel
          Left = 168
          Top = 33
          Width = 6
          Caption = 'a'
        end
        inherited txtTelefono: TEdit
          Left = 173
        end
        inherited cbTipoTelefono: TVariantComboBox
          Left = 336
          Visible = False
        end
        inherited cbDesde: TComboBox
          Left = 103
          Top = 27
        end
        inherited cbHasta: TComboBox
          Left = 184
          Top = 28
        end
        inherited cbCodigosArea: TComboBox
          Left = 104
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'EMail'
      object Label5: TLabel
        Left = 8
        Top = 10
        Width = 35
        Height = 13
        Caption = 'Email:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object eMail: TEdit
        Left = 111
        Top = 6
        Width = 369
        Height = 21
        Color = 16444382
        MaxLength = 255
        TabOrder = 0
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Domicilio'
      object Label6: TLabel
        Left = 8
        Top = 11
        Width = 56
        Height = 13
        Caption = 'Domicilio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object sbtnClear: TSpeedButton
        Left = 397
        Top = 6
        Width = 23
        Height = 23
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
          3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
          03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
          33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
          0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
          3333333337FFF7F3333333333000003333333333377777333333}
        NumGlyphs = 2
        OnClick = sbtnClearClick
      end
      object bteDetalle: TBuscaTabEdit
        Left = 110
        Top = 7
        Width = 283
        Height = 21
        Color = 16444382
        Enabled = True
        MaxLength = 255
        ReadOnly = True
        TabOrder = 0
        OnEnter = IrAlInicio
        OnExit = IrAlInicio
        Decimals = 0
        EditorStyle = bteTextEdit
        BuscaTabla = btDomicilios
      end
    end
  end
  object txtObservaciones: TEdit
    Left = 155
    Top = 115
    Width = 354
    Height = 21
    MaxLength = 100
    TabOrder = 2
  end
  object btDomicilios: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = cdsDomicilios
    OnProcess = btDomiciliosProcess
    OnSelect = btDomiciliosSelect
    Left = 452
    Top = 125
  end
  object ObtenerDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 84
    Top = 160
  end
  object dsDomicilios: TDataSource
    DataSet = cdsDomicilios
    Left = 112
    Top = 160
  end
  object cdsDomicilios: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TipoDomicilio'
        DataType = ftSmallint
      end
      item
        Name = 'DescriTipoDomicilio'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DescripcionCiudad'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoCalle'
        DataType = ftInteger
      end
      item
        Name = 'Numero'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Piso'
        DataType = ftSmallint
      end
      item
        Name = 'Dpto'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'CalleDesnormalizada'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPostal'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DomicilioEntrega'
        DataType = ftBoolean
      end
      item
        Name = 'CalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'CalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'Normalizado'
        DataType = ftInteger
      end
      item
        Name = 'DireccionCompleta'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'UbicacionGeografica'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspDomicilios'
    StoreDefs = True
    Left = 140
    Top = 160
    object cdsDomiciliosCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object cdsDomiciliosCodigoPersona: TIntegerField
      FieldName = 'CodigoPersona'
    end
    object cdsDomiciliosTipoDomicilio: TSmallintField
      FieldName = 'TipoDomicilio'
    end
    object cdsDomiciliosDescriTipoDomicilio: TStringField
      FieldName = 'DescriTipoDomicilio'
      Size = 50
    end
    object cdsDomiciliosCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriPais: TStringField
      FieldName = 'DescriPais'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriRegion: TStringField
      FieldName = 'DescriRegion'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriComuna: TStringField
      FieldName = 'DescriComuna'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosDescripcionCiudad: TStringField
      FieldName = 'DescripcionCiudad'
      Size = 50
    end
    object cdsDomiciliosCodigoCalle: TIntegerField
      FieldName = 'CodigoCalle'
    end
    object cdsDomiciliosNumero: TStringField
      FieldName = 'Numero'
      FixedChar = True
      Size = 10
    end
    object cdsDomiciliosPiso: TSmallintField
      FieldName = 'Piso'
    end
    object cdsDomiciliosDpto: TStringField
      FieldName = 'Dpto'
      Size = 4
    end
    object cdsDomiciliosCalleDesnormalizada: TStringField
      FieldName = 'CalleDesnormalizada'
      Size = 50
    end
    object cdsDomiciliosDetalle: TStringField
      FieldName = 'Detalle'
      Size = 50
    end
    object cdsDomiciliosCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      Size = 10
    end
    object cdsDomiciliosDomicilioEntrega: TBooleanField
      FieldName = 'DomicilioEntrega'
    end
    object cdsDomiciliosCalleRelacionadaUno: TIntegerField
      FieldName = 'CalleRelacionadaUno'
    end
    object cdsDomiciliosNumeroCalleRelacionadaUno: TIntegerField
      FieldName = 'NumeroCalleRelacionadaUno'
    end
    object cdsDomiciliosCalleRelacionadaDos: TIntegerField
      FieldName = 'CalleRelacionadaDos'
    end
    object cdsDomiciliosNumeroCalleRelacionadaDos: TIntegerField
      FieldName = 'NumeroCalleRelacionadaDos'
    end
    object cdsDomiciliosNormalizado: TIntegerField
      FieldName = 'Normalizado'
    end
    object cdsDomiciliosDireccionCompleta: TStringField
      FieldName = 'DireccionCompleta'
      Size = 200
    end
    object cdsDomiciliosUbicacionGeografica: TStringField
      FieldName = 'UbicacionGeografica'
      Size = 100
    end
  end
  object dspDomicilios: TDataSetProvider
    DataSet = ObtenerDomiciliosPersona
    Constraints = True
    Left = 168
    Top = 160
  end
end
