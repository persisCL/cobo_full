unit FreDatosPersonaConsulta;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Util,PeaProcs,PeaTypes,DmConnection,RStrings,UtilProc, StdCtrls, DB,
  ADODB;

type
  TFrameDatosPersonaConsulta = class(TFrame)
    lblDescNombre: TLabel;
    lblEmail: TLabel;
    lblRutRun: TLabel;
    txtDocumento: TEdit;
    lblNombrePersoneria: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lblDescTelefono: TLabel;
    lblDescDomicilio: TLabel;
    procedure txtDocumentoKeyPress(Sender: TObject; var Key: Char);
  private
    aux: TDatosPersonales;
    function GetPersoneria: AnsiString;
    procedure SetPersoneria(const Value: AnsiString);
  private
    function GetRegistrodeDatos: TDatosPersonales;
    procedure SetRegistrodeDatos(const Value: TDatosPersonales);
    property Personeria:AnsiString read GetPersoneria write SetPersoneria;
    { Private declarations }
  public
    { Public declarations }
    property RegistrodeDatos:TDatosPersonales read GetRegistrodeDatos write SetRegistrodeDatos;
  end;

implementation

{$R *.dfm}

function TFrameDatosPersonaConsulta.GetPersoneria: AnsiString;
begin

end;

function TFrameDatosPersonaConsulta.GetRegistrodeDatos: TDatosPersonales;
begin

end;

procedure TFrameDatosPersonaConsulta.SetPersoneria(
  const Value: AnsiString);
begin

end;

procedure TFrameDatosPersonaConsulta.SetRegistrodeDatos(
  const Value: TDatosPersonales);
begin
    self.Personeria:=Value.Personeria;
    if self.Personeria=PERSONERIA_FISICA then begin
        lblNombrePersoneria.Caption := PERSONERIA_FISICA_DESC +':';
    end else begin
        lblNombrePersoneria.Caption := PERSONERIA_JURIDICA_DESC +':';
    end;
    lblDescNombre.Caption := ArmarNombrePersona(Value.Personeria,Value.Nombre,Value.Apellido,Value.ApellidoMaterno);
    //AGREGAR DOMICILIO
    //AGREGAR TELEFONO
    //AGREGAR EMAIL
    Aux:=Value;
    txtDocumento.Text:=Value.NumeroDocumento;
end;

procedure TFrameDatosPersonaConsulta.txtDocumentoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9','K','k', #8]) then
       Key := #0;
end;

end.
