unit frmPatentesConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, StdCtrls, CheckLst, UtilProc, Buttons;

type
  TFormPatentesConvenio = class(TForm)
    chklstPatentesConvenio: TCheckListBox;
    Panel1: TPanel;
    spObtenerPatentesConvenioDescuento: TADOStoredProc;
    btnAplicarDescuentos: TBitBtn;
    btnCancelar: TBitBtn;
    procedure btnAplicarDescuentosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
	ListaPatentes: TStringList;
	function Inicializa(CodigoConvenio: Integer): Boolean;
  end;

var
  FormPatentesConvenio: TFormPatentesConvenio;

implementation

{$R *.dfm}

procedure TFormPatentesConvenio.btnAplicarDescuentosClick(Sender: TObject);
var
	i: Integer;
begin
	for i := 0 to chklstPatentesConvenio.Items.Count - 1 do begin
    	if chklstPatentesConvenio.Checked[i] then
        	ListaPatentes.Add(chklstPatentesConvenio.Items[i]);
    	end;
end;

procedure TFormPatentesConvenio.FormCreate(Sender: TObject);
begin
	ListaPatentes := TStringList.Create;
end;

function TFormPatentesConvenio.Inicializa(CodigoConvenio: Integer): Boolean;
resourcestring
    CAPTION_GET_PATENTES		= 'Obtener Patentes';
	MSG_CANNOT_GET_PATENTES		= 'No se pudieron obtener las patentes';
var
	i: Integer;
begin
    try
        with spObtenerPatentesConvenioDescuento, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value	:= CodigoConvenio;
            Open;
            i := 0;
            while not EOF do begin
            	chklstPatentesConvenio.Items.Add(FieldByName('Patente').AsString);
                if FieldByName('CodigoDescuento').IsNull then
                	chklstPatentesConvenio.Checked[i] := False
                else
                	chklstPatentesConvenio.Checked[i] := True;
                inc(i);
            	Next;
                end;
            Close;
        end;
    except
    	on E:Exception do begin
        	MsgBoxErr(CAPTION_GET_PATENTES, e.message, CAPTION_GET_PATENTES, MB_ICONSTOP);
			Exit;
        end;
    end;
    result := True;
end;


end.
