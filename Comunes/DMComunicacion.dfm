object DMComunicaciones: TDMComunicaciones
  OldCreateOrder = False
  Height = 225
  Width = 347
  object spCierraLogComunicaciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CerrarLogComunicacioes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 72
  end
  object spAgregarLogComunicacioes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarLogComunicaciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
      end
      item
        Name = '@CodUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
      end
      item
        Name = '@CodComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end>
    Left = 72
    Top = 16
  end
end
