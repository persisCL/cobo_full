{********************************** File Header ********************************
File Name   : FrmRptInformeUsuario.pas
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura dentro del DFM

    Revision : 2
        Author : vpaszkowicz
        Date : 27/04/2009
        Description : SS 744
			Agrego usuario y contrase�a para reportes.

Description : Se sobrecarga el m�todo Inicializa, para que obtenga la base de datos
              de donde se obtendr�n los par�metros generales.
Firma       : PAR00133_COPAMB_ALA_20110801

Firma       : SS_979_ALA_20110914
Description : Se quita Cifrado por defecto de la base de datos por que es un error.


Firma       : SS_NDR_1147_20140813
Descripcion : La fila de "Rango del Filtro" ponia todo como fecha

Firma       : SS_1147ZZZ_NDR_20150413
Descripcion : Correccion a observaciones de la migracion

Etiqueta    : TASK_054_MGO_20160728
Descripci�n : Se arregla falta de Result en Inicializar
*******************************************************************************}

unit FrmRptInformeUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppBands, ppCtrls, ppClass, ppVar, ppPrnabl, ppCache, ppProd,
  ppReport, DB, UtilRB, ppDB, ppComm, ppRelatv, ppDBPipe, ADODB, UtilProc,
  FrmRptInformeUsuarioConfig, Util, UtilDB, PeaTypes, ppTypes, TeEngine,
  Series, ExtCtrls, TeeProcs, Chart, ppMemo, DbChart, ppStrtch, ppRegion, DateUtils,
  ConstParametrosGenerales, utilReportes, ppModule, daDataModule,
  ppParameter, Printers;

const	// Rev. 2 (SS 744)
    CONST_USUARIO_INFORMES_STANDARD = 'usr_informes';	// Rev. 2 (SS 744)

resourcestring
    MSG_ERROR_GENERANDO_REPORTE = 'Error generando reporte, se cancela el proceso';
    TXT_ATENCION = 'Atenci�n';

type
  TFormRptInformeUsuario = class(TForm)
	rbiReporte: TRBInterface;
    rptReporteUsuario: TppReport;
    DBPipelineDiseniar: TppDBPipeline;
	dsReporte: TDataSource;
    dbcReporte: TDBChart;
    ppParameterList1: TppParameterList;
    ppHeaderBand1: TppHeaderBand;
    ppLine1: TppLine;
    pplblUsuario: TppLabel;
    ppLabel14: TppLabel;
    labFiltro: TppLabel;
    ppTitulo: TppLabel;
    imgLogo: TppImage;
    txtTextoLogo: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppLabel1: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppShape1: TppShape;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppShape2: TppShape;
    imgGrafico: TppImage;
    BaseInformes: TADOConnection;
    qryReporte: TADOQuery;
	procedure rbiReporteSelect(Sender: TObject);
	procedure rptReporteUsuarioBeforePrint(Sender: TObject);
	procedure rptReporteUsuarioAfterPrint(Sender: TObject);
	procedure rbiReporteExecute(Sender: TObject; var Cancelled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BeforePrintDiseno(Sender: TObject);
  private
	{ Private declarations }
	FnParam: integer;
	FParams: TParamsReporte;
	FCodigoConsulta: integer;
    FTipoGrafico: AnsiString;
	FCampoDatosGrafico: AnsiString;
	FCampoValoresGrafico: AnsiString;
    FServidor: AnsiString;
    FBasedeDatos: AnsiString;
    FTimeOutConsultas: integer;
    FUsuario: string;	// Rev. 2 (SS 744)
    FContrasenia: string;	// Rev. 2 (SS 744)
    FConnectionDatosBase : TADOConnection;          //PAR00133_COPAMB_ALA_20110801
	Procedure ArmarReporte();
	Procedure ArmarGrafico();
    function ConectarServer(Server, DataBase: string; UserName, Password: string): boolean;	// Rev. 2 (SS 744)
  public
    { Public declarations }
   	Function Inicializar(Codigo: integer; Nombre: AnsiString; ConsultaSQL: AnsiString; nParam: integer; TipoGrafico: AnsiString; CampoDatosGrafico: AnsiString; CampoValoresGrafico: AnsiString): Boolean; overload; //PAR00133_COPAMB_ALA_20110801
    Function Inicializar(Codigo: integer; Nombre: AnsiString; ConsultaSQL: AnsiString; nParam: integer; TipoGrafico: AnsiString; CampoDatosGrafico: AnsiString; CampoValoresGrafico: AnsiString; ConnectionBaseDatos : TADOConnection): Boolean; overload; //PAR00133_COPAMB_ALA_20110801

  end;

var
  FormRptInformeUsuario: TFormRptInformeUsuario;
  spParametrosReporte: TADOStoredProc;
  
implementation

uses
	DMConnection, PeaProcs, EncriptaRijandel;	// Rev. 2 (SS 744)

{$R *.dfm}

function TFormRptInformeUsuario.Inicializar(Codigo: integer; Nombre: AnsiString; ConsultaSQL: AnsiString; nParam: integer; TipoGrafico: AnsiString; CampoDatosGrafico: AnsiString; CampoValoresGrafico: AnsiString): Boolean;
begin
    Result := Inicializar(Codigo, Nombre, ConsultaSQL, nParam, TipoGrafico, CampoDatosGrafico, CampoValoresGrafico, DMConnections.BaseCAC); //PAR00133_COPAMB_ALA_20110801
end;

//INICIO BLOQUE //PAR00133_COPAMB_ALA_20110801
function TFormRptInformeUsuario.Inicializar(Codigo: integer; Nombre,
  ConsultaSQL: AnsiString; nParam: integer; TipoGrafico, CampoDatosGrafico,
  CampoValoresGrafico: AnsiString;
  ConnectionBaseDatos: TADOConnection): Boolean;
resourcestring
    MSG_ERROR = 'Ha ocurrido un error generando el reporte.';
Var
	i: integer;
	Resultado: boolean;
    FRootReportes: AnsiString;
    spDatosReporte: TADOStoredProc;
begin
    // INICIO : TASK_054_MGO_20160728
    Result := True;
    Cursor := crHourGlass;
    try
    FCodigoConsulta := Codigo;
    FnParam := nParam;
	FTipoGrafico := Trim(TipoGrafico);
	FCampoDatosGrafico := Trim(CampoDatosGrafico);
	FCampoValoresGrafico := Trim(CampoValoresGrafico);
	ppTitulo.Caption := Trim(Nombre);
    qryReporte.SQL.Text := Trim(ConsultaSQL);
	rbiReporte.Caption := Trim(Nombre);
    spParametrosReporte:= TADOStoredProc.Create(nil);
    spParametrosReporte.Connection := DMConnections.BaseCAC;
    spParametrosReporte.ProcedureName := 'ObtenerVariablesConsulta';
    spParametrosReporte.Parameters.Refresh;
	spParametrosReporte.parameters.ParamByName('@CodigoConsulta').value := FCodigoConsulta;

        FConnectionDatosBase := ConnectionBaseDatos;

        ObtenerTiempoConsultaReporte(FConnectionDatosBase, FTimeOutConsultas);
    qryReporte.CommandTimeout := FTimeOutConsultas;
        ObtenerParametroGeneral(FConnectionDatosBase, ROOT_TEMPLATE_REPORTES, FRootReportes);
    FRootReportes := Trim(FRootReportes);
    if FileExists(GoodDir(FRootReportes) + IntToStr(Codigo) + '.rtm') then begin
      rptReporteUsuario.Template.FileName := GoodDir(FRootReportes) + IntToStr(Codigo) + '.rtm';
      rptReporteUsuario.Template.LoadFromFile;
      rptReporteUsuario.NoDataBehaviors := [ndMessageDialog, ndBlankReport];
      rptReporteUsuario.BeforePrint := BeforePrintDiseno;
    end;

    try
        spParametrosReporte.Open;
        if spParametrosReporte.RecordCount>0 then
        begin
            i := 0;
            spParametrosReporte.First;
            While not spParametrosReporte.Eof do begin
                case UpperCase(spParametrosReporte.FieldByName('Tipo').AsString)[1] of
                    'D': FParams[i] := FormatDateTime('yyyymmdd', date);
                    'H': FParams[i] := FormatDateTime('yyyymmdd hh:nn:ss', NowBase(FConnectionDatosBase));              //PAR00133_COPAMB_ALA_20110801
                    'L': FParams[i] := '';
                    else FParams[i] := NULL
                end;
                Inc(i);
                spParametrosReporte.Next;
            end;
            while (i <= MAX_CANT_PARAMETROS_REPORTE) do
            begin
                FParams[i] := NULL;
                Inc(i);
            end;
                //Result := True;                                                   // TASK_054_MGO_20160728
        end;
    finally
        spParametrosReporte.Close;
    end;

    // guardo los datos del server y base de datos

    spDatosReporte := TADOStoredProc.Create(nil);
    try
        spDatosReporte.Connection := DMConnections.BaseCAC;
        spDatosReporte.ProcedureName := 'ObtenerConsultas';
        spDatosReporte.Parameters.Refresh;
        spDatosReporte.parameters.ParamByName('@CodigoConsulta').value := FCodigoConsulta;
        spDatosReporte.Open;
        if spDatosReporte.RecordCount>0 then
        begin
            spDatosReporte.First;

            FServidor    := TrimRight(spDatosReporte.FieldByName('Servidor').AsString);
            FBasedeDatos := TrimRight(spDatosReporte.FieldByName('BasedeDatos').AsString);
            FUsuario := TrimRight(spDatosReporte.FieldByName('UsuarioBaseDatos').AsString);
            FContrasenia := TrimRight(spDatosReporte.FieldByName('Password').AsString);
        end;     
    finally
        spDatosReporte.Free;
    end;

    if (FContrasenia <> '') then
        FContrasenia := Descifrar(FContrasenia, CONTRASENIA_REPORTE);
    if (FUsuario = '') then
        FUsuario := CONST_USUARIO_INFORMES_STANDARD;
    except
        on e: Exception do begin
            Cursor := crDefault;
            MsgBoxErr(MSG_ERROR, e.Message, Self.Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;          
    Cursor := crDefault;
    // FIN : TASK_054_MGO_20160728
end;

procedure TFormRptInformeUsuario.rbiReporteSelect(Sender: TObject);
Var
    f: TFormRptInformeUsuarioConfig;
begin
	Application.CreateForm(TFormRptInformeUsuarioConfig, f);
	if f.Inicializar(FCodigoConsulta, FParams, FTipoGrafico, FConnectionDatosBase) then f.ShowModal;                    //PAR00133_COPAMB_ALA_20110801
    if (f.MostrarGrafico = False) then FTipoGrafico := SIN_GRAFICO;
	f.Release;
end;

procedure TFormRptInformeUsuario.rptReporteUsuarioBeforePrint(Sender: TObject);
resourcestring
	MSG_ERROR_EJECUTANDO_CONSULTA = 'Error ejecutando la consulta.';
begin
	// Ejecutamos la consulta.
	try
		qryReporte.Open;
		ArmarReporte;
	except
		on e: exception do begin
            MsgBoxErr(MSG_ERROR_EJECUTANDO_CONSULTA, e.Message,
			  Self.Caption, MB_ICONSTOP);
			Exit;
		end;
	end;
end;

procedure TFormRptInformeUsuario.ArmarReporte;
resourcestring
	MSG_CONSULTA_DEMASIADO_GRANDE = 'El tama�o total de los campos de la consulta excede el tama�o de la p�gina.';
const
	CAMPO_AVG = '_AVG';
	CAMPO_CNT = '_CNT';
	CAMPO_MAX = '_MAX';
	CAMPO_MIN = '_MIN';
	CAMPO_SUM = '_SUM';
	TAMANIO_CARACTER = 0.08;
	MAX_CANT_CARACTERES = 35;
	DIST_SEPARACION_CAMPOS = 0.025;
	DESCRIPCION_CAMPO_SUM = 'SUMA';
	DESCRIPCION_CAMPO_MAX = 'MAXIMO';
	DESCRIPCION_CAMPO_MIN = 'MINIMO';
	DESCRIPCION_CAMPO_AVG = 'PROMEDIO';
	DESCRIPCION_CAMPO_CNT = 'CANTIDAD';
Var
	miCampo: TppDBText;
	i, nParam: integer;
	miCampoMemo: TppDBMemo;
	miCampoResumen: TppDBCalc;
	TipoFuncionResumen: TppDBCalcType;
	miEtiqueta, miEtiquetaResumen: TppLabel;
	GenerarResumen, MostrarGrupoResumen: boolean;
	NombreCampo, TipoResumen, RutaLogo: AnsiString;
	TamanioCampo, TamanioRealCampo, DistIzquierda: Single;
	AlineacionCampo : TAlignment;
	EstilosCampo : TFontStyles;
    Day, month, year, hour, minute, second: word;
    valor, s: Ansistring;
    spFormatoColumnas: TADOStoredProc;
begin
	Screen.Cursor := crHourGlass;
	// Coloca el logo del reporte.
    ObtenerParametroGeneral(FConnectionDatosBase, LOGO_REPORTES, RutaLogo);     //PAR00133_COPAMB_ALA_20110801
	try
    if imgLogo=nil then imgLogo := TppImage.Create(Self);                       //SS_1147ZZZ_NDR_20150413
		imgLogo.Picture.LoadFromFile(Trim(RutaLogo));
	except
		On E: Exception do begin
			Screen.Cursor := crDefault;
			MsgBox(E.message, Self.Caption, MB_ICONSTOP);
			Screen.Cursor := crHourGlass;
		end;
	end;
	// Coloca texto del logo del reporte.

    ObtenerParametroGeneral(FConnectionDatosBase, TEXTO_LOGO_REPORTES, valor);  //PAR00133_COPAMB_ALA_20110801
	txtTextoLogo.Text := valor;

	// Coloca los filtros seleccionados s�lo si no es multiselect.
    
    try
        spParametrosReporte.Open;
        if spParametrosReporte.RecordCount>0 then
        begin
            spParametrosReporte.First;
            if (spParametrosReporte.Eof = True) then ppLabel14.Caption := '';
            nParam := 0;
            While not spParametrosReporte.Eof do begin
                If UpperCase(spParametrosReporte.FieldByName('Tipo').AsString)[1] <> 'L' then
            begin
               valor := FParams[nParam];
               if spParametrosReporte.FieldByName('Tipo').AsString[1] = 'H' then
               begin
                 year := strToInt(copy(Valor, 1 , 4));
                 month := strToINt(copy(Valor, 5 , 2));
                 day := strToINt(copy(Valor, 7 , 2));
                 hour := strToInt(copy(Valor, 10 , 2));
                 minute := strToINt(copy(Valor, 13 , 2));
                 Second := strToINt(copy(Valor, 16 , 2));
                 S := FormatDateTime('dd/mm/yyyy hh:nn:ss', EncodeDateTime(year, month, day, hour, minute, second, 0));
               end
               else if spParametrosReporte.FieldByName('Tipo').AsString[1] = 'D' then
               begin
                 year := strToInt(copy(Valor, 1 , 4));
                 month := strToINt(copy(Valor, 5 , 2));
                 day := strToINt(copy(Valor, 7 , 2));
                 S := FormatDateTime('dd/mm/yyyy', EncodeDate(year, month, day));
               end
               else                                                                     //SS_NDR_1147_20140813
               begin                                                                    //SS_NDR_1147_20140813
                 S:=valor;                                                              //SS_NDR_1147_20140813
               end;                                                                     //SS_NDR_1147_20140813
                 labFiltro.Text := labFiltro.Text + Trim(spParametrosReporte.FieldByName('Descripcion').AsString) + ': ' + S;
                     if (nParam+1 < FnParam) then
                  labFiltro.Text := labFiltro.Text + '   -   ';                         //SS_NDR_1147_20140813
                end;
                Inc(nParam);

                spParametrosReporte.Next;
            end;
        end;
    finally
        spParametrosReporte.Close;
    end;

	// Arma los campos.
	DistIzquierda := DIST_SEPARACION_CAMPOS * 6;
	MostrarGrupoResumen := False;
//    qryReporte.Open; Lo Abre    rptReporteUsuarioBeforePrint
    if qryReporte.RecordCount>0 then
    begin
        qryReporte.First;
        for i := 0 to qryReporte.FieldCount - 1 do begin
            // Estilo por defecto
            EstilosCampo := [];

            // Alineaci�n por defecto
            case qryReporte.Fields[i].DataType of
            ftString: AlineacionCampo := taLeftJustify;
            ftDate, ftTime, ftDateTime: AlineacionCampo := taCenter;
            else AlineacionCampo := taRightJustify;
            end;

            // Calcula el tama�o del campo por defecto
            case qryReporte.Fields[i].DataType of
                ftString: TamanioCampo := (TAMANIO_CARACTER * qryReporte.Fields[i].Size);
                ftBoolean: TamanioCampo := (TAMANIO_CARACTER * 5);
                ftInteger: TamanioCampo := (TAMANIO_CARACTER * 10);
                ftDate, ftTime, ftDateTime: TamanioCampo := (TAMANIO_CARACTER * 19);
                else TamanioCampo := (TAMANIO_CARACTER * 20);
            end;

            spFormatoColumnas := TADOStoredProc.Create(nil);
            try
                // Define valores de Alineaci�n, Estilo y Ancho en base a lo especificado por dise�o
                spFormatoColumnas.Connection := DMConnections.BaseCAC;
                spFormatoColumnas.ProcedureName := 'ObtenerConsultasColumnas';
                spFormatoColumnas.Parameters.Refresh;
                spFormatoColumnas.Parameters.ParamByName ('@CodigoColumna').Value := i + 1;
                spFormatoColumnas.Parameters.ParamByName ('@CodigoConsulta').Value := FCodigoConsulta;
                spFormatoColumnas.Open;
                if spFormatoColumnas.RecordCount>0 then
                begin
                    // Alineaci�n
                    case UpperCase (spFormatoColumnas.FieldByName ('Alineacion').AsString)[1] of
                        'I': AlineacionCampo := taLeftJustify;
                        'C': AlineacionCampo := taCenter;
                        'D': AlineacionCampo := taRightJustify;
                    end;
                    // Estilo
                    case UpperCase (spFormatoColumnas.FieldByName ('Estilo').AsString)[1] of
                        'N': EstilosCampo := [];
                        'I': EstilosCampo := [fsItalic];
                        'B': If Trim(spFormatoColumnas.FieldByName ('Estilo').AsString) = 'B' then EstilosCampo := [fsBold]
                             else EstilosCampo := [fsBold, fsItalic];
                    end;
                    TamanioCampo := TAMANIO_CARACTER * spFormatoColumnas.FieldByName ('AnchoColumna').AsInteger;
                end;
            finally
                spFormatoColumnas.Free;
            end;

            // Arma el nombre del campo y de la funci�n de resumen.
            TipoResumen := Copy(qryReporte.Fields[i].FieldName, (Length(qryReporte.Fields[i].FieldName) - 3), 4);
            TipoFuncionResumen := dcSum;
            if ((TipoResumen = CAMPO_AVG) or (TipoResumen = CAMPO_CNT) or (TipoResumen = CAMPO_MAX) or
              (TipoResumen = CAMPO_MIN) or (TipoResumen = CAMPO_SUM)) then
            begin
                NombreCampo := Copy(qryReporte.Fields[i].FieldName, 0, (Length(qryReporte.Fields[i].FieldName) - 4));
                if (TipoResumen = CAMPO_AVG) then TipoFuncionResumen := dcAverage
                else if (TipoResumen = CAMPO_CNT) then TipoFuncionResumen := dcCount
                else if (TipoResumen = CAMPO_MAX) then TipoFuncionResumen := dcMaximum
                else if (TipoResumen = CAMPO_MIN) then TipoFuncionResumen := dcMinimum;
                GenerarResumen := True;
                MostrarGrupoResumen := True;
            end
            else
            begin
                NombreCampo := qryReporte.Fields[i].FieldName;
                GenerarResumen := False;
            end;

            TamanioRealCampo := TamanioCampo;
            // Si el nombre del campo es m�s grande que el tama�o predeterminado, lo cambia.
            if (TAMANIO_CARACTER * (Length(NombreCampo)) > TamanioCampo) then TamanioCampo := (TAMANIO_CARACTER * Length(NombreCampo));
            if (TamanioCampo > (TAMANIO_CARACTER * MAX_CANT_CARACTERES)) then TamanioCampo := (TAMANIO_CARACTER * MAX_CANT_CARACTERES);

            // Controla el tama�o total del reporte.
            if ((DistIzquierda + TamanioCampo) > rptReporteUsuario.PrinterSetup.PrintableWidth) then
            begin
              ppGroupFooterBand1.Visible := MostrarGrupoResumen;
              Screen.Cursor := crDefault;
              MsgBox(MSG_CONSULTA_DEMASIADO_GRANDE, Self.Caption, MB_ICONINFORMATION);
              Break;
            end;

            // Crea los t�tulos de los campos.
            miEtiqueta := TppLabel.Create(Self);
            miEtiqueta.AutoSize := false;
            miEtiqueta.Font.Name := 'Arial';
            miEtiqueta.Font.Size := 9;
            miEtiqueta.Alignment := AlineacionCampo;
            miEtiqueta.Font.Style := [fsBold];
            miEtiqueta.Band := ppGroupHeaderBand1;
            miEtiqueta.Name := 'Etiqueta' + IntToStr(i);
            miEtiqueta.Top := 0.0417;
            miEtiqueta.Left := DistIzquierda;
            miEtiqueta.Height := 0.16;
            miEtiqueta.Caption := NombreCampo;
            miEtiqueta.Transparent := True;
            // Controla el tama�o del �ltimo campo.
            miEtiqueta.Width := IIf(((DistIzquierda + TamanioCampo) > rptReporteUsuario.PrinterSetup.PrintableWidth),
                              (TamanioCampo - ((DistIzquierda + TamanioCampo) - rptReporteUsuario.PrinterSetup.PrintableWidth)),
                              TamanioCampo);
            // Crea los campos.
            if (TamanioRealCampo > TamanioCampo) then
            begin
              miCampoMemo := TppDBMemo.Create(Self);
              miCampoMemo.Alignment := miEtiqueta.Alignment;
              miCampoMemo.AutoSize := miEtiqueta.Autosize;
              miCampoMemo.Font.Name := miEtiqueta.Font.Name;
              miCampoMemo.Font.Size := miEtiqueta.Font.Size;
              miCampoMemo.Font.Style:= EstilosCampo;
              miCampoMemo.Band := ppDetailBand1;
              miCampoMemo.Name := 'Campo' + IntToStr(i);
              miCampoMemo.Top := miEtiqueta.Top;
              miCampoMemo.Left := miEtiqueta.Left;
              miCampoMemo.Height := miEtiqueta.Height;
              miCampoMemo.DataField := qryReporte.Fields[i].FieldName;
              miCampoMemo.Transparent := miEtiqueta.Transparent;
              miCampoMemo.DataPipeline := DBPipelineDiseniar;
              miCampoMemo.WordWrap := True;
              miCampoMemo.Width := miEtiqueta.Width;
              miCampoMemo.Stretch := True;
            end
            else
            begin
              miCampo := TppDBText.Create(Self);
              miCampo.Alignment := miEtiqueta.Alignment;
              miCampo.AutoSize := miEtiqueta.Autosize;
              miCampo.Font.Name := miEtiqueta.Font.Name;
              miCampo.Font.Size := miEtiqueta.Font.Size;
              miCampo.Font.Style:= EstilosCampo;
              miCampo.Band := ppDetailBand1;
              miCampo.Name := 'Campo' + IntToStr(i);
              miCampo.Top := miEtiqueta.Top;
              miCampo.Left := miEtiqueta.Left;
              miCampo.Height := miEtiqueta.Height;
              miCampo.DataField := qryReporte.Fields[i].FieldName;
              miCampo.Transparent := miEtiqueta.Transparent;
              miCampo.DataPipeline := DBPipelineDiseniar;
              miCampo.WordWrap := True;
              miCampo.Width := miEtiqueta.Width;
            end;

            // Crea los campos de resumen de grupo.
            if (GenerarResumen = True) then
            begin
                if (qryReporte.Fields[i].DataType = ftString) then
                begin
                    miEtiquetaResumen := TppLabel.Create(Self);
                    miEtiquetaResumen.Alignment := AlineacionCampo;
                    miEtiquetaResumen.AutoSize := miEtiqueta.AutoSize;
                    miEtiquetaResumen.Font.Name := miEtiqueta.Font.Name;
                    miEtiquetaResumen.Font.Size := miEtiqueta.Font.Size;
                    miEtiquetaResumen.Band := ppGroupFooterBand1;
                    miEtiquetaResumen.Name := 'CampoResumen' + IntToStr(i);
                    miEtiquetaResumen.Top := miEtiqueta.Top;
                    miEtiquetaResumen.Left := miEtiqueta.Left;
                    miEtiquetaResumen.Height := miEtiqueta.Height;
                    if (TipoResumen = CAMPO_AVG) then miEtiquetaResumen.Caption := DESCRIPCION_CAMPO_AVG
                    else if (TipoResumen = CAMPO_CNT) then miEtiquetaResumen.Caption := DESCRIPCION_CAMPO_CNT
                    else if (TipoResumen = CAMPO_MAX) then miEtiquetaResumen.Caption := DESCRIPCION_CAMPO_MAX
                    else if (TipoResumen = CAMPO_MIN) then miEtiquetaResumen.Caption := DESCRIPCION_CAMPO_MIN
                    else miEtiquetaResumen.Caption := DESCRIPCION_CAMPO_SUM;
                    miEtiquetaResumen.Transparent := True;
                    miEtiquetaResumen.Width := miEtiqueta.Width;
                end
                else
                begin
                    miCampoResumen := TppDBCalc.Create(Self);
                    miCampoResumen.Alignment := AlineacionCampo;
                    miCampoResumen.AutoSize := miEtiqueta.Autosize;
                    miCampoResumen.Font.Name := miEtiqueta.Font.Name;
                    miCampoResumen.Font.Size := miEtiqueta.Font.Size;
                    miCampoResumen.Band := ppGroupFooterBand1;
                    miCampoResumen.Name := 'CampoResumen' + IntToStr(i);
                    miCampoResumen.Top := miEtiqueta.Top;
                    miCampoResumen.Left := miEtiqueta.Left;
                    miCampoResumen.Height := miEtiqueta.Height;
                    miCampoResumen.DataField := qryReporte.Fields[i].FieldName;
                    miCampoResumen.Transparent := miEtiqueta.Transparent;
                    miCampoResumen.DataPipeline := DBPipelineDiseniar;
                    miCampoResumen.WordWrap := True;
                    miCampoResumen.Width := miEtiqueta.Width;
                    miCampoResumen.DBCalcType := TipoFuncionResumen;
                end;
            end;
            // Guarda la distancia del campo hasta el margen.
            DistIzquierda := (DistIzquierda + miEtiqueta.Width) + DIST_SEPARACION_CAMPOS;
        end;
    end;
//    qryReporte.Close; Lo Cierra     rptReporteUsuarioAfterPrint

	If (MostrarGrupoResumen = False) then
	begin
		If (FTipoGrafico = SIN_GRAFICO) then
		begin
			ppGroupFooterBand1.Visible := False;
		end
		else
		begin
			ppShape2.Visible := False;
			imgGrafico.Top := (imgGrafico.Top - 0.25);
			ArmarGrafico;
		end;
	end
	else
	begin
		If (FTipoGrafico = SIN_GRAFICO) then
		begin
			imgGrafico.Visible := False;
			ppGroupFooterBand1.Height := 0.25;
		end
		else
		begin
			ArmarGrafico;
		end;
	end;

	Screen.Cursor := crDefault;
end;

procedure TFormRptInformeUsuario.ArmarGrafico();
var
	Img: AnsiString;
	NewSeries: TChartSeries;
    SeriesClass: TChartSeriesClass;
begin;
    // Armo el gr�fico del reporte, lo guardo en un archivo para cargarlos en
	// el reporte y luego lo borro.
    Img := GetTempDir + TempFile + '.wmf';
	If (FTipoGrafico = GRAFICO_BARRAS) then SeriesClass := TBarSeries
	else If (FTipoGrafico = GRAFICO_TORTA) then SeriesClass := TPieSeries
    else SeriesClass := TLineSeries;
	NewSeries := SeriesClass.Create (Self);
    NewSeries.ParentChart := dbcReporte;
    qryReporte.First;
    qryReporte.DisableControls;
    while not (qryReporte.Eof = True) do
    begin
        try
            dbcReporte.Series[0].Add(qryReporte.FieldByName(FCampoValoresGrafico).AsFloat, qryReporte.FieldByName(FCampoDatosGrafico).AsString);
		except
        end;
        qryReporte.Next;
    end;
	qryReporte.EnableControls;
	dbcReporte.SaveToMetafile(Img);
    imgGrafico.Picture.LoadFromFile(Img);
    DeleteFile(Img);
end;

procedure TFormRptInformeUsuario.rptReporteUsuarioAfterPrint(Sender: TObject);
begin
	 qryReporte.Close;
end;

procedure TFormRptInformeUsuario.rbiReporteExecute(Sender: TObject; var Cancelled: Boolean);
var
    s: AnsiString;
resourcestring
	MSG_PARAMETROS_VACIOS = 'Debe completar los valores de los par�metros para poder continuar.';
	CONST_MSG_SIN_DATOS = 'No existen datos para la consulta';
	CONST_MSG_ERROR_CONSULTA = 'Error en la consulta';
begin

    try
        spParametrosReporte.Open;
        if not UtilReportes.ArmarConsultaSQL(FParams,spParametrosReporte, qryReporte) then begin
            Cancelled := True;
            MsgBox(MSG_PARAMETROS_VACIOS, Self.Caption, MB_ICONINFORMATION);
            Exit;
        end;
    finally
        spParametrosReporte.Close;
    end;

	if (FnParam <= 0) then rbiReporte.OnSelect := nil;
    S := qryReporte.SQL.Text;
    // armo tambien la cadena de conexion
    if not ConectarServer(FServidor, FBasedeDatos, FUsuario, FContrasenia) then begin	// Rev. 2 (SS 744)
        Cancelled := True;
        MsgBox(MSG_ERROR_GENERANDO_REPORTE, TXT_ATENCION, MB_ICONINFORMATION);
        Exit;
    end else begin
        qryReporte.Connection := BaseInformes;
    end;
    
    try
        qryReporte.Open;
	except
		on e: exception do begin
            Cancelled := True;
			MsgBoxErr('Error en la consulta', e.Message,
			  Self.Caption, MB_ICONSTOP);
			Exit;
		end;
	end;
    qryReporte.SQL.Text := S;
end;

procedure TFormRptInformeUsuario.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if spParametrosReporte.State = dsBrowse then
    	spParametrosReporte.Close;
    BaseInformes.Close;
end;

procedure TFormRptInformeUsuario.BeforePrintDiseno(Sender: TObject);
var
    nParam: integer;
    valor, S: Ansistring;
    Day, month, year, hour, minute, second: word;
begin
	// Coloca los filtros seleccionados s�lo si no es multiselect.
    spParametrosReporte.Open;
    if spParametrosReporte.RecordCount>0 then
    begin
        spParametrosReporte.First;
        nParam := 0;
        While not spParametrosReporte.Eof do begin
            If UpperCase(spParametrosReporte.FieldByName('Tipo').AsString)[1] <> 'L' then begin
                valor := FParams[nParam];
                if spParametrosReporte.FieldByName('Tipo').AsString[1] = 'H' then begin
                    year := strToInt(copy(Valor, 1 , 4));
                    month := strToINt(copy(Valor, 5 , 2));
                    day := strToINt(copy(Valor, 7 , 2));
                    hour := strToInt(copy(Valor, 10 , 2));
                    minute := strToINt(copy(Valor, 13 , 2));
                    Second := strToINt(copy(Valor, 16 , 2));
                    S := FormatDateTime('dd/mm/yyyy hh:nn:ss', EncodeDateTime(year, month, day, hour, minute, second, 0));
                end
                else
                    if spParametrosReporte.FieldByName('Tipo').AsString[1] = 'D' then begin
                        year := strToInt(copy(Valor, 1 , 4));
                        month := strToINt(copy(Valor, 5 , 2));
                        day := strToINt(copy(Valor, 7 , 2));
                        S := FormatDateTime('dd/mm/yyyy', EncodeDate(year, month, day));
                    end
                    else S:= Trim(Valor);

                if assigned(labFiltro) then begin
                    labFiltro.Text := labFiltro.Text + Trim(spParametrosReporte.FieldByName('Descripcion').AsString)
                                      + ': ' + S;
                    if (nParam+1 < FnParam) then labFiltro.Text := labFiltro.Text + ' - ';
                end;

            end;
            Inc(nParam);

            spParametrosReporte.Next;
        end;
    end;
    spParametrosReporte.Close;
end;

function TFormRptInformeUsuario.ConectarServer(Server, DataBase: string; UserName, Password: string): boolean;	// Rev. 2 (SS 744)
resourcestring
    MSG_ERROR_CONNECTION_INFORMES = 'Error de conexi�n con la base de datos';
    TXT_ERROR = 'Error';
// Rev. 2 (SS 744)
var
    Autenticacion: string;
// Fin Rev. 2 (SS 744)
begin
    Result := False;
    BaseInformes.Connected        := False;
	BaseInformes.KeepConnection   := True;
	// Rev. 2 (SS 744)
    Autenticacion := UserName;
    if Password <> EmptyStr then
         Autenticacion := Autenticacion + '; Password=' + Password;
    Autenticacion := Autenticacion + ';';    
	// Fin Rev. 2 (SS 744)
	BaseInformes.ConnectionString :=
	  'Provider=SQLOLEDB.1;User ID='+ Autenticacion +	// Rev. 2 (SS 744)
	  'Initial Catalog=' + DataBase + ';' +
	  'Data Source=' +
		Server + ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID='   + GetMachineName();
    try
        BaseInformes.Open;
        Result := True;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_CONNECTION_INFORMES, e.Message, TXT_ERROR, MB_ICONEXCLAMATION);
        end;
    end;
end;


end.

