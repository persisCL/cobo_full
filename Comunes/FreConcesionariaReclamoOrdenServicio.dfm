object FrameConcesionariaReclamoOrdenServicio: TFrameConcesionariaReclamoOrdenServicio
  Left = 0
  Top = 0
  Width = 236
  Height = 61
  Anchors = [akTop, akRight]
  TabOrder = 0
  Visible = False
  object GBConcesionaria: TGroupBox
    Left = 3
    Top = 2
    Width = 235
    Height = 55
    Caption = 'Concesionaria'
    TabOrder = 0
    Visible = False
    object lblConcesionaria: TLabel
      Left = 7
      Top = 23
      Width = 71
      Height = 13
      Caption = 'Concesionaria:'
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 85
      Top = 19
      Width = 141
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
end
