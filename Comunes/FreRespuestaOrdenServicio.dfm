object FrameRespuestaOrdenServicio: TFrameRespuestaOrdenServicio
  Left = 0
  Top = 0
  Width = 841
  Height = 476
  TabOrder = 0
  object LRespuestadelaConcesionaria: TLabel
    Left = 0
    Top = 60
    Width = 841
    Height = 13
    Align = alTop
    Caption = '  Respuesta de la Concesionaria'
    ExplicitWidth = 153
  end
  object LComentariosDelCliente: TLabel
    Left = 0
    Top = 161
    Width = 841
    Height = 13
    Align = alTop
    Caption = '  Comentarios del Cliente'
    ExplicitWidth = 119
  end
  object PContactarCliente: TPanel
    Left = 0
    Top = 0
    Width = 841
    Height = 60
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 393
    object cknotificado: TCheckBox
      Left = 7
      Top = 8
      Width = 177
      Height = 17
      Caption = 'Qued'#243' Notificado'
      TabOrder = 0
    end
    object ckconforme: TCheckBox
      Left = 7
      Top = 32
      Width = 233
      Height = 17
      Caption = 'Estuvo de acuerdo con la Resoluci'#243'n'
      TabOrder = 1
    end
  end
  object txtDetalleRespuesta: TMemo
    Left = 0
    Top = 73
    Width = 841
    Height = 88
    Align = alTop
    TabOrder = 1
    OnChange = txtDetalleRespuestaChange
    ExplicitWidth = 393
  end
  object TxtComentariosdelCliente: TMemo
    Left = 0
    Top = 174
    Width = 841
    Height = 302
    Align = alClient
    TabOrder = 2
    OnChange = TxtComentariosdelClienteChange
    ExplicitWidth = 393
    ExplicitHeight = 42
  end
end
