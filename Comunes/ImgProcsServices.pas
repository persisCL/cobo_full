unit ImgProcsServices;

interface

Uses
	Classes, Windows, Messages, SysUtils, DB, DBTables, JPeg, JpegPlus, ADODB,
    StdCtrls, Util, Graphics, Variants, ImgTypes, SyncObjs, AdminIMG, utilImg,
    WinSvc, ConstParametrosGenerales, DateUtils,
    StrUtils;                                                                                                                                                            //PAR00133_COPAMB_ALA_20110727

// Funciones para administrar imagenes de referencia
function ObtenerImagenPatente(ImagePlatePath,Patente, NombreCorto: AnsiString; TipoImg: TTipoImagen;                                                                      ///SS-377-NDR-20110607
  var Imagen: TJPEGPlusImage; var DataImage: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                                                 ///SS-377-NDR-20110607

function AlmacenarImagenPatente(ImagePlatePath, ImagePath, Patente, NombreCorto: AnsiString; NumCorrCO: Int64;                                                            ///SS-377-NDR-20110607
  TipoImg: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                                                                                  ///SS-377-NDR-20110607


// Funciones para administrar imagenes de tr�nsitos pendientes
procedure ObtenerPathFileName(DirRaiz: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen; var Path: AnsiString; var NombreArchivo: AnsiString);
function ArmarPathTransitoPendiente(DirRaiz: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var NombreArchivo: AnsiString): Boolean;
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var JPG12: TJPEGPlusImage; var DataImage: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; Overload;
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var Imagen: TBitmap; var DatosImg: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; Overload;
function AlmacenarImagenTransitoPendiente(PathBase, ArchivoJPGOrigen: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;

// Funciones para administrar el traspaso de imagenes desde la estructura de transitos pendientes a transitos
function CopiarImagenTransitoPendiente(ADOConn: TADOConnection; ImagePathSource, ImagePathDest, NombreCorto: AnsiString; NumCorrCO, NumCorrCA: Int64;                                              ///SS-377-NDR-20110607
  FechaTransito: TDateTime; TipoImg: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                                                        ///SS-377-NDR-20110607

function ArmarPathPadreTransitos(DirRaiz: AnsiString; NumCorrCA: Int64; Fecha: TDateTime; var ArchivoPadre: AnsiString): Boolean; forward;                                //SS-377-NDR-20110607
function ArmarPathPadrePatentes(DirRaiz: AnsiString; NombreCorto:AnsiString; Patente: AnsiString; var ArchivoPadre: AnsiString): Boolean; forward;                        //SS-377-NDR-20110607
function ArmarPathPadreTransitosNuevoFormato(ADOConn: TADOConnection; DirRaiz, NombreCorto: AnsiString; NumCorrCA: Int64; Fecha: TDateTime; var ArchivoPadre: AnsiString): Boolean; forward; Overload;       //SS-377-NDR-20110607
function ArmarPathPadreTransitosNuevoFormato(DirRaiz, NombreCorto: AnsiString; NumCorrCA: Int64; Fecha: TDateTime; var ArchivoPadre: AnsiString): Boolean; forward; Overload;      //SS-377-NDR-20110607
function ArmarPathImagenTVM(PathRaiz, NombreImagen, NombreCorto : AnsiString; var PathRetorno : AnsiString) : Boolean;                                                    //PAR00133_COPAMB_ALA_20110727


implementation

type
	// Paths
	TItemPath = record
		Viajes: String;
	end;

	TPaths = record
		Transitos: Array[1..20] of TItemPath;
		Patentes: String;
        Pendientes: String;
	end;

Var
	TimeoutPath: Int64;
    FPathImagenes: TPaths;

// Declaracion de funciones locales
function CargarDatosPath(Conn: TADOConnection; var PathImagenes: TPaths): Boolean; forward;
//

//****************************************************************************//


function CargarDatosPath(Conn: TADOConnection; var PathImagenes: TPaths): Boolean;
Const
	TIMEOUT_PATH = 120000; // 2'
var
	Qry: TADOQuery;
	CritSect: TCriticalSection;
	PtoCobro, i: Integer;
begin
	// Cargar para cada Concesionaria/PuntoCobro los datos del Path.
	// S�lo esta funci�n usa la variable FPathImagenes.
	CritSect := TCriticalSection.Create;
	try
      	if (GetMSCounter < TimeoutPath) then begin
			PathImagenes := FPathImagenes;
			Result := True;
		end else begin
            // Primero inicializamos la variable.
            FPathImagenes.Patentes := '';
            FPathImagenes.Pendientes := '';
            for i := 1 to 20 do begin
                FPathImagenes.transitos[i].Viajes := '';
            end;
			Qry := TAdoQuery.Create(nil);
			try
				try
					Qry.Connection := Conn;
					Qry.SQL.Text := 'SELECT * FROM PuntosCobro WITH (NOLOCK) ';
					Qry.Open;
					While not Qry.Eof do begin
						PtoCobro := Qry.FieldByName('NumeroPuntoCobro').AsInteger;
						PathImagenes.Transitos[PtoCobro].Viajes := Trim(Qry.FieldByName('PathFotosViajes').AsString);
						Qry.Next;
					end;
					//

                    ObtenerParametroGeneral(Conn, 'Dir_Imagenes_Transitos_Pendientes', PathImagenes.Pendientes);
                    ObtenerParametroGeneral(Conn, 'Dir_Imagenes_Patentes', PathImagenes.Patentes);

					// Actualizar la variable que usamos como "cache"
					FPathImagenes := PathImagenes;
					TimeoutPath   := GetMSCounter + TIMEOUT_PATH;
					Result := True;
				except
					Result := False;
				end;
			finally
				Qry.Close;
				Qry.Free;
			end;
		end;
	finally
		CritSect.Free;
	end;
end;

function ArmarPathPadreTransitos(DirRaiz: AnsiString; NumCorrCA: int64; Fecha: TDateTime;
  var ArchivoPadre: AnsiString): Boolean;
var
	Nivel1, Nivel2: AnsiString;
begin
	try
		// Armamos el Path completo con la dir del directorio de la Concesionaria/PuntoCobro
		// y sgte nivel.
		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');
		DateTimeToString(Nivel1, 'yyyy-mm-dd', Fecha);
		Nivel2		  := IntToStr(NumCorrCA MOD 1000);
		ArchivoPadre  := format('%s\%s\%s.IMG', [DirRaiz, Nivel1, Nivel2]);
		//
		Result := True;
	except
		Result := False;
	end;
end;

function ArmarPathPadreTransitosNuevoFormato(ADOConn: TADOConnection; DirRaiz, NombreCorto: AnsiString; NumCorrCA: Int64; Fecha: TDateTime; var ArchivoPadre: AnsiString): Boolean; //SS-377-NDR-20110607
var                                                                                                                       //SS-377-NDR-20110607
	Nivel0, Nivel1, Nivel2, Nivel3: AnsiString;                                                                           //SS-377-NDR-20110607
    DirectorioImagenes: string;
const
    DIR_IMAGENES_TRANSITOS = 'DIR_IMAGENES_TRANSITOS_%s';
begin                                                                                                                     //SS-377-NDR-20110607
    try                                                                                                                   //SS-377-NDR-20110607
        Result := False;

        if not ObtenerParametroGeneral(
                  ADOConn,
                  Format(DIR_IMAGENES_TRANSITOS,[FormatDateTime('MM',Fecha)]),
                  DirectorioImagenes) then Exit;

        // Armamos el Path completo con la dir del directorio de la Concesionaria/PuntoCobro                              //SS-377-NDR-20110607
		// y sgte nivel.                                                                                                  //SS-377-NDR-20110607
//		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');                                         //SS-377-NDR-20110607
//		DateTimeToString(Nivel0, 'mm', Fecha);                                                                            //SS-377-NDR-20110607
		DateTimeToString(Nivel1, 'yyyy-mm-dd', Fecha);                                                                    //SS-377-NDR-20110607
		Nivel2		  := Trim(NombreCorto);                                                                               //SS-377-NDR-20110607
		Nivel3		  := IntToStr(NumCorrCA MOD 1000);                                                                    //SS-377-NDR-20110607
        // Si se ha iniciado la Migraci�n devuelve s�lo la ruta, sino la ruta completa del Archivo Padre.                 //SS-377-NDR-20110607
//        ArchivoPadre := format('%s%s\%s\%s\%s\', [DirRaiz, Nivel0, Nivel1, Nivel2, Nivel3]);                            //SS-377-NDR-20110607
        ArchivoPadre := format('%s\%s\%s\%s\', [DirectorioImagenes, Nivel1, Nivel2, Nivel3]);
		//                                                                                                                //SS-377-NDR-20110607
		Result := True;                                                                                                   //SS-377-NDR-20110607
	except                                                                                                                //SS-377-NDR-20110607
		Result := False;                                                                                                  //SS-377-NDR-20110607
	end;                                                                                                                  //SS-377-NDR-20110607
end;

function ArmarPathPadreTransitosNuevoFormato(DirRaiz, NombreCorto: AnsiString; NumCorrCA: int64; Fecha: TDateTime;        //SS-377-NDR-20110607
  var ArchivoPadre: AnsiString): Boolean;                                                                                 //SS-377-NDR-20110607
var                                                                                                                       //SS-377-NDR-20110607
	Nivel0, Nivel1, Nivel2, Nivel3: AnsiString;                                                                           //SS-377-NDR-20110607
begin                                                                                                                     //SS-377-NDR-20110607
    try                                                                                                                   //SS-377-NDR-20110607
        // Armamos el Path completo con la dir del directorio de la Concesionaria/PuntoCobro                              //SS-377-NDR-20110607
		// y sgte nivel.                                                                                                  //SS-377-NDR-20110607
		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');                                         //SS-377-NDR-20110607
		DateTimeToString(Nivel0, 'mm', Fecha);                                                                            //SS-377-NDR-20110607
		DateTimeToString(Nivel1, 'yyyy-mm-dd', Fecha);                                                                    //SS-377-NDR-20110607
		Nivel2		  := Trim(NombreCorto);                                                                               //SS-377-NDR-20110607
		Nivel3		  := IntToStr(NumCorrCA MOD 1000);                                                                    //SS-377-NDR-20110607
        // Si se ha iniciado la Migraci�n devuelve s�lo la ruta, sino la ruta completa del Archivo Padre.                 //SS-377-NDR-20110607
        ArchivoPadre := format('%s%s\%s\%s\%s\', [DirRaiz, Nivel0, Nivel1, Nivel2, Nivel3]);                              //SS-377-NDR-20110607
		//                                                                                                                //SS-377-NDR-20110607
		Result := True;                                                                                                   //SS-377-NDR-20110607
	except                                                                                                                //SS-377-NDR-20110607
		Result := False;                                                                                                  //SS-377-NDR-20110607
	end;                                                                                                                  //SS-377-NDR-20110607
end;                                                                                                                      //SS-377-NDR-20110607
                                                                                                              //SS-377-NDR-20110607


function ArmarPathPadrePatentes(DirRaiz: AnsiString; NombreCorto:AnsiString; Patente: AnsiString; var ArchivoPadre: AnsiString): Boolean;

  procedure ObtenerNumerosPatente(Patente: AnsiString; var NumerosPatente: LongInt);
  var
      i: Integer;
      numPatente: AnsiString;
  begin
      // Saca los n�mero de la patente, por orden de aparici�n (izq. a der.)
      numPatente := '';
      for i := 1 to Length(Patente) do begin
          if (Ord(Patente[i]) >= Ord('0')) and (Ord(Patente[i]) <= Ord('9')) then
            numPatente := numPatente + Patente[i];
      end;
      NumerosPatente := IVal(numPatente);
  end;

var
	NumeroPat: Integer;
begin
	try
		// Armamos el Path completo
		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');
		ObtenerNumerosPatente(Patente, NumeroPat);
		ArchivoPadre := GoodDir(DirRaiz) + Trim(NombreCorto) + '\' + IntToStr(NumeroPat MOD 100) + '\' + IntToStr(NumeroPat MOD 1000);     //SS-377-NDR-20110607
		//
		Result := True;
	except
		Result := False;
	end;
end;



function CargarImagenPendientes(HFile: Integer; var JPG12: TJpegPlusImage; var DataImage: TDataImage;
  var DescriError: AnsiString): Boolean;
var
	Buffer: PChar;
	ArchiAux: AnsiString;
	H, Tamanio: Integer;
begin
	// Si tenemos la seguridad que las fotos son J12, directamente podemos cargar
	// un TPicture. Pero en las demos, seguramente tenemos jpegs, entonces, probamos.
	Result := False;
    try
        // Generamos un archivo temporal
        ArchiAux := GetTempDir + TempFile + '.j12';
        H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);
        if H = -1 then raise exception.Create('Error al abrir archivo.');
        FileSeek(H, 0, 0);
        Tamanio := FileSeek(HFile, 0, 2);
        FileSeek(HFile, 0, 0);
        Buffer  := AllocMem(Tamanio);
        FileRead(HFile, Buffer^, Tamanio);
        FileWrite(H, Buffer^, Tamanio);
        FileClose(H);
        FreeMem(Buffer);
        // Obtenemos la imagen
        if not ObtenerImagen(ArchiAux, JPG12, DataImage, DescriError) then begin
            DeleteFile(ArchiAux);
            result := false;
        end else begin
            Result := True;
        end;
    except
        on e: exception do begin
            DescriError := e.Message;
        end;
    end;
end;

function CargarOverViewPendientes(HFile: Integer; var Imagen: TBitMap; var DataImage: TDataImage;
  var DescriError: AnsiString): Boolean;
var
	Buffer: PChar;
	ArchiAux: AnsiString;
	H, BytesLeer, Tamanio: Integer;
    jpegAux : TJPegImage;
begin
	jpegAux := TjpegImage.Create;
	Result := False;
	try
		try
			ArchiAux := GetTempDir + TempFile + '.j12';
			H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);
			if H = -1 then raise exception.Create('Error al abrir archivo.');
			FileSeek(H, 0, 0);
			Tamanio := FileSeek(HFile, 0, 2);
			FileSeek(HFile, 0, 0);
			BytesLeer := Tamanio - BYTES_EXTRAS_OVERVIEW;
			Buffer    := AllocMem(BytesLeer);
			FileRead(HFile, Buffer^, BytesLeer);
			FileWrite(H, Buffer^, BytesLeer);
			FileClose(H);
			// Retornar Imagen
			try
				jpegAux.LoadFromFile(ArchiAux);
                Imagen.Assign(jpegAux);
			except
			end;
			FreeMem(Buffer);
			DeleteFile(ArchiAux);
			// Cargar los datos extras
			FileSeek(HFile, Tamanio - BYTES_EXTRAS_OVERVIEW, 0);
			FileRead(HFile, DataImage.DataOverView, BYTES_EXTRAS_OVERVIEW);
			//
			Result := True;
		except
			on e: exception do begin
				DescriError := e.Message;
				Result := False;
			end;
		end;
	finally
		jpegAux.Free;
	end;
end;





function CopiarImagenTransitoPendiente(ADOConn: TADOConnection; ImagePathSource, ImagePathDest, NombreCorto: AnsiString; NumCorrCO, NumCorrCA: Int64;     //SS-377-NDR-20110607
  FechaTransito: TDateTime; TipoImg: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;               //SS-377-NDR-20110607

 (*   Function ImagenEsJPEG(archivo: AnsiString): Boolean;
    begin
        result := true;
        // Cargamos la imagen en Buffer desde el Archivo IMG
        if DirItems[i*7 + j].PosicionInicial = 0 then begin
            inc(j);
            continue;
        end;
        FileSeek(HFilePadre, DirItems[i*7 + j].PosicionInicial, 0);
        Buffer := AllocMem(DirItems[i*7 + j].BytesDatos + DirItems[i*7 + j].BytesEstructuraAddicional);
        FileRead(Archivo, Buffer^, DirItems[i*7 + j].BytesDatos + DirItems[i*7 + j].BytesEstructuraAddicional);
        Tamanio := FileRead(HFilePadre, Buffer^, DirItems[i*7 + j].BytesDatos);
    end;
    *)

var
	ArchivoTransito, Path, ArchivoPadre: AnsiString;
begin
	// La imagen original est� en pendientes.
	// La imagen debe pasar a la estructura de almacenamiento final (agrupadas en un mismo archivo)
	Result := False;
    try
        // Path del Archivo a Copiar
        ObtenerPathFileName(ImagePathSource, NumCorrCO, TipoImg, Path, ArchivoTransito);
        // Verificamos si el archivo fuente existe
        if not FileExists(ArchivoTransito) then begin
            Error := teImagenNoExiste;
            raise exception.Create('No se encuentra la Imagen del tr�nsito.');
        end;
        //Rev.4 / 07-Junio-2011 / Nelson Droguett Sierra----------------------------------------------------------------------------
        // Path del Archivo Padre.                                                                                                   //SS-377-NDR-20110607
        //if not ArmarPathPadreTransitos(ImagePathDest, NumCorrCA, FechaTransito, ArchivoPadre) then begin                           //SS-377-NDR-20110607
        //    Error := teObtenerPath;                                                                                                //SS-377-NDR-20110607
        //    raise exception.Create('No se pudo obtener el Path del Archivo Destino.');                                             //SS-377-NDR-20110607
        //end;                                                                                                                       //SS-377-NDR-20110607
        if not ArmarPathPadreTransitosNuevoFormato(ADOConn, ImagePathDest, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin    //SS-377-NDR-20110607
            Error := teObtenerPath;                                                                                                  //SS-377-NDR-20110607
            raise exception.Create('No se pudo obtener el Path del Archivo Destino.');                                               //SS-377-NDR-20110607
        end;                                                                                                                         //SS-377-NDR-20110607
                                                                                                                                     //SS-377-NDR-20110607
        // Almacenamos la imagen en el Archivo IMG                                                                                   //SS-377-NDR-20110607
        //if not AlmacenarImagenIMGTransito(ArchivoTransito, ArchivoPadre, NumCorrCA, TipoImg, DescriError) then begin               //SS-377-NDR-20110607
        //    Error := teGrabarArchivo;                                                                                              //SS-377-NDR-20110607
        //    raise exception.Create('No se pudo crear la imagen del tr�nsito.' + DescriError);                                      //SS-377-NDR-20110607
        //end;                                                                                                                       //SS-377-NDR-20110607
        if not AlmacenarImagenIMGTransitoNuevoFormato(ArchivoTransito, ArchivoPadre, NumCorrCA, TipoImg, DescriError) then begin     //SS-377-NDR-20110607
             Error := teGrabarArchivo;                                                                                               //SS-377-NDR-20110607
             raise exception.Create('No se pudo crear la imagen del tr�nsito.' + DescriError);                                       //SS-377-NDR-20110607
        end;                                                                                                                         //SS-377-NDR-20110607
        // Listo                                                                                                                     //SS-377-NDR-20110607
        //FinRev.4-------------------------------------------------------------------------------------------------------------------
        Result := True;
    except
        on e: Exception do begin
            DescriError := 'Copiar Imagen: ' + e.Message;
        end;
    end;
end;

function AlmacenarImagenPatente(ImagePlatePath, ImagePath, Patente, NombreCorto: AnsiString;                         //SS-377-NDR-20110607
  NumCorrCO: Int64; TipoImg: TTipoImagen;                                                                            //SS-377-NDR-20110607
  var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                                                   //SS-377-NDR-20110607

var
	ArchivoTransito, ArchivoPadre: AnsiString;
begin
	// La imagen original est� en pendientes y debe pasar a la estructura de almacenamiento de Referencia.
	Result := False;
    try
        // Validaciones
        if (length(Patente) = 0) then raise exception.Create('Patente Nula.');
        // Path del Archivo de Pendientes
        if not ArmarPathTransitoPendiente(ImagePath, NumCorrCO, TipoImg, ArchivoTransito) then begin
            Error := teObtenerPath;
            raise exception.Create('No se pudo obtener el Path de la imagen Origen.');
        end;
        //
        if not FileExists(ArchivoTransito) then begin
            Error := teImagenNoExiste;
            raise exception.Create('No se encuentra la Imagen del tr�nsito.');
        end;
        // Path del Archivo Padre.
        if not ArmarPathPadrePatentes(ImagePlatePath,NombreCorto, Patente, ArchivoPadre) then begin                              //SS-377-NDR-20110607
            Error := teObtenerPath;                                                                                              //SS-377-NDR-20110607
            raise exception.Create('No se pudo obtener el Path del archivo Destino.');                                           //SS-377-NDR-20110607
        end;                                                                                                                     //SS-377-NDR-20110607
        //if not AlmacenarImagenIMGPatente(ArchivoTransito, ArchivoPadre, Patente, TipoImg,                                      //SS-377-NDR-20110607
        //  DescriError) then begin                                                                                              //SS-377-NDR-20110607
        //    Error := teGrabarArchivo;                                                                                          //SS-377-NDR-20110607
        //    raise exception.Create(DescriError);                                                                               //SS-377-NDR-20110607
        //end;                                                                                                                   //SS-377-NDR-20110607
        if not AlmacenarImagenIMGPatenteNuevoFormato(ArchivoTransito, ArchivoPadre, Patente, TipoImg, DescriError) then begin    //SS-377-NDR-20110607
            Error := teGrabarArchivo;                                                                                            //SS-377-NDR-20110607
            raise exception.Create(DescriError);                                                                                 //SS-377-NDR-20110607
        end;                                                                                                                     //SS-377-NDR-20110607

        // Listo
        Result := True;
    except
        on e: Exception do begin
            DescriError := 'Copiar Imagen: ' + e.Message;
        end;
    end;
end;


function ObtenerImagenPatente(ImagePlatePath, Patente, NombreCorto: AnsiString; TipoImg: TTipoImagen;                                     //SS-377-NDR-20110607
  var Imagen: TJPEGPLusIMage; var DataIMage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                 //SS-377-NDR-20110607
ResourceString                                                                                                                            //SS-377-NDR-20110607
   MSG_ERROR_PATH_NEW = 'No se pudo obtener el Path del Archivo.';                                                                        //SS-377-NDR-20110607
var
	ArchivoPadre: AnsiString;
begin
    Result := False;
    try
        if not ArmarPathPadrePatentes(ImagePlatePath, NombreCorto, Patente, ArchivoPadre) then begin                                      //SS-377-NDR-20110607
            DescriError := MSG_ERROR_PATH_NEW;                                                                                            //SS-377-NDR-20110607
            Error := teObtenerPath;
            Exit;
        end;
        // Obtenemos la imagen desde la estructura
        //if not ObtenerImagenIMGPatente(ArchivoPadre, Patente, TipoImg, Imagen, DataIMage, DescriError, error) then begin               //SS-377-NDR-20110607
        if not ObtenerImagenIMGPatenteNuevoFormato(ArchivoPadre, Patente, TipoImg, Imagen, DataIMage, DescriError, error) then begin     //SS-377-NDR-20110607
            Error := teCargarImagen;
            Exit;
        end;
        Result := True;
    except
        on e:Exception do begin
            Result := False;
            DescriError := 'Recuperar Imagen: ' + e.Message;
            Error := teCargarImagen;
        end;
    end;
end;


//***** Lectura - Tr�nsitosPendientes ****************************************//
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString;
  NumCorrCO: Int64; TipoImagen: TTipoImagen;
  var JPG12: TJPEGPlusImage; var DataImage: TDataImage; var DescriError: AnsiString;
  var Error: TTipoErrorImg): Boolean;
var
	HFilePend: Integer;
    NomArchivo: AnsiString;
begin
	Result := False;
	// Par�metros Ok?
	if not Assigned(JPG12) then begin
		DescriError := 'El par�metro Imagen (TBitmap) no puede ser nil';
		Exit;
	end;
	if not ArmarPathTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, NomArchivo) then begin
		DescriError := 'No se pudo obtener el Path de la imagen';
		Error := teObtenerPath;
		Exit;
	end;
	// La imagen existe?
	if not FileExists(NomArchivo) then begin
		DescriError := 'La imagen no existe';
		Error := teImagenNoExiste;
		Exit;
	end;
	HFilePend := -1;
	try
		HFilePend := FileOpenRetry(NomArchivo, fmOpenRead or fmShareDenyWrite);
		if HFilePend = -1 then
		  raise exception.Create('Error al abrir archivo');
		if not CargarImagenPendientes(HFilePend, JPG12, DataImage, DescriError) then
		  raise exception.Create(DescriError);
		Result := True;
	except
	end;
	if HFilePend > 0 then FileClose(HFilePend);
end;

function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen;
  var Imagen: TBitmap; var DatosImg: TDataImage;
  var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	HFilePend: Integer;
	NomArchivo: AnsiString;
begin
	Result := False;
    Error := teObtenerPath;
	// Par�metros Ok?
	if not Assigned(Imagen) then begin
		DescriError := 'El par�metro Imagen (TBitmap) no puede ser nil';
		Exit;
	end;
	// El path existe o pudo ser creado?
	if not ArmarPathTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, NomArchivo) then begin
		DescriError := 'No se pudo obtener el Path de la imagen';
		Error := teObtenerPath;
		Exit;
	end;
	// La imagen existe?
	if not FileExists(NomArchivo) then begin
		DescriError := 'La imagen no existe';
		Error := teImagenNoExiste;
		Exit;
	end;
	HFilePend := -1;
	try
		HFilePend := FileOpenRetry(NomArchivo, fmOpenRead or fmShareDenyWrite);
		if HFilePend = -1 then
		  raise exception.Create('Error al abrir archivo');
		if not CargarOverViewPendientes(HFilePend, Imagen, DatosImg, DescriError) then
		  raise exception.Create(DescriError);
		Result := True;
	except
	end;
	if HFilePend > 0 then FileClose(HFilePend);
end;

procedure ObtenerPathFileName(DirRaiz: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen; var Path: AnsiString; var NombreArchivo: AnsiString);
begin
	Path          := GoodDir(DirRaiz) + IntToStr(NumCorrCO mod 1000);
	NombreArchivo := GoodDir(Path) + IntToStr(NumCorrCO) + '-' + SufijoImagen[TipoImagen] + '.jpg';
end;

function ArmarPathTransitoPendiente(DirRaiz: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen; var NombreArchivo: AnsiString): Boolean;
var
	Path: AnsiString;
begin
	// Armamos el path completo con la raiz del directorio de Pendientes que viene
	// como par�metro, el grupo del Tr�nsito (N� Tr�nsito Mod 1000),
	// y el nombre del archivo con el  N� Tr�nsito y la Ubicacion de la foto.
	// Si el Directorio no existe, lo creamos.
    ObtenerPathFileName(DirRaiz, NumCorrCO, TipoImagen, Path, NombreArchivo);
	try
		Result := DirectoryExists(Path) or ForceDirectories(Path);
	except
		Result := False;
	end;
end;


{ Mueve el Archivo ArchivoJPGOrigen al directorio de Pendientes. }
function AlmacenarImagenTransitoPendiente(PathBase, ArchivoJPGOrigen: AnsiString;
  NumCorrCO: Int64; TipoImagen: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	ArchivoDestino: AnsiString;
begin
	try
		if not FileExists(ArchivoJPGOrigen) then begin
			Error := teImagenNoExiste;
			raise exception.Create('No existe archivo de Origen');
		end;
		// Armar el Path completo donde guardar el archivo
		if not ArmarPathTransitoPendiente(PathBase, NumCorrCO, TipoImagen, ArchivoDestino) then begin
			Error := teObtenerPath;
			raise exception.Create('No se pudo obtener el Path de la imagen en Pendientes');
		end;
		// Copiar
		if not FileCopy(ArchivoJPGOrigen, ArchivoDestino) then begin
			Error := teGrabarArchivo;
			raise exception.Create('No se pudo copiar el archivo al directorio de Pendientes');
		end;
		Result := True;
	except
		on e: exception do begin
			DescriError := e.Message;
			Result := False;
		end;
	end;
end;


function ObtenerPathTransitoPendiente(Conn: TADOConnection; Concesionaria, PuntoCobro,
  NumeroTransito: Integer; TipoImagen: TTipoImagen; var ArchivoTransito: AnsiString; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	PathImagenes: TPaths;
	DatosImg: TDataOverview;
	DirRaiz, NomArchivo: AnsiString;
begin
	Result := False;
	CargarDatosPath(Conn, PathImagenes);
	FillChar(DatosImg, Sizeof(DatosImg), 0);
	// El path existe o pudo ser creado?
	DirRaiz := PathImagenes.Pendientes;
	if not ArmarPathTransitoPendiente(DirRaiz, NumeroTransito, TipoImagen, NomArchivo) then begin
		DescriError := 'No se pudo obtener el Path de la imagen';
		Error := teObtenerPath;
		Exit;
	end;
	ArchivoTransito := NomArchivo;
	Result := True;
end;

{---------------------------------------------------------------------------------
Function Name: ArmarPathImagenTVM
Author       : alabra
Date Created : 27-07-2011
Description  : Arma el path donde se debe encontrar la imagen de TVM.
----------------------------------------------------------------------------------}
function ArmarPathImagenTVM(PathRaiz, NombreImagen, NombreCorto : AnsiString; var PathRetorno : AnsiString) : Boolean;
var
    Fecha   : AnsiString;
    Mes     : AnsiString;
begin
    try
        Mes     := AnsiMidStr(NombreImagen, 5, 2);
        // yyyy-mm-dd
        Fecha   := AnsiMidStr(NombreImagen, 0, 4) + '-' + AnsiMidStr(NombreImagen, 5, 2) + '-' + AnsiMidStr(NombreImagen, 7, 2);

        PathRetorno := format('%s%s\%s\%s\%s\', [PathRaiz, Mes, Fecha, NombreCorto, 'TVM']);
        Result := True;
    except
        Result := False;
    end;
end;

end.

