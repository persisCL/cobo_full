{********************************** File Header ********************************
File Name : ExportarToExcel.pas
Author : LOR
Date Created: 07/05/2015
Language : ES-CL
Description : Exportar DataSET a formato EXCEL
*******************************************************************************}
unit ExportarToExcel;

interface

uses ComObj, Controls, DB, Dialogs, Forms,StdCtrls, Util,UtilProc, SysUtils, Windows;

    function FormatFieldToExcel(Campo: TField): string;
    function ExportToCsv(Dataset: TDataset; lnewFileBuffer: Boolean; var FileBuffer: string; var Error: string; cCampos: String): boolean;
    function SaveToCSV(DataSet: TDataset; PrefijoNombreArchivo: string): boolean;

    implementation

    function FormatFieldToExcel(Campo: TField): string;
      const
      DELIMITADOR_EXCEL = '"';
      SEPARADOR_EXCEL = ';';
    begin
        result := '';
        if Campo.IsNull then exit;
        case Campo.DataType of
            ftUnknown, ftString, ftFixedChar, ftWideString, ftBytes, ftVarBytes, ftLargeint, ftVariant, ftGuid, ftMemo:
                result := result + '=' + DELIMITADOR_EXCEL + StringReplace(StringReplace(stringreplace(Campo.AsString, DELIMITADOR_EXCEL, DELIMITADOR_EXCEL + DELIMITADOR_EXCEL, [rfReplaceAll]), #13, ' ', [rfReplaceAll]), #10, ' ', [rfReplaceAll])  + DELIMITADOR_EXCEL;

            ftSmallint, ftInteger, ftWord, ftAutoInc, ftFloat, ftCurrency, ftBCD, ftFMTBcd:
                result := result + '=' + DELIMITADOR_EXCEL +  Campo.AsString + DELIMITADOR_EXCEL;

            ftBoolean:
                result := result + Campo.AsString;

//            ftFloat, ftCurrency, ftBCD, ftFMTBcd:
//                result := result + Campo.AsString;

            ftDate:
                result := result + FormatDateTime('dd/mm/yyyy', Campo.AsDateTime);

            ftTime:
                result := result + FormatDateTime('hh:nn:ss', Campo.AsDateTime);

            ftDateTime, ftTimeStamp:
                begin
                    if trunc(Campo.AsDateTime) = 0 then begin
                        // es una hora
                        result := result + FormatDateTime('hh:nn:ss', Campo.AsDateTime);
                    end else if frac(Campo.AsDateTime) = 0 then begin
                        // es una fecha
                        result := result + FormatDateTime('dd/mm/yyyy', Campo.AsDateTime);
                    end else begin
                        // es una fecha completa
                        result := result + FormatDateTime('dd/mm/yyyy hh:nn:ss', Campo.AsDateTime);
                    end;
                end;

            ftBlob, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob, ftInterface, ftIDispatch:
                result := result + 'Tipo de campo no soportado';
        end;
    end;

    function ExportToCsv(Dataset: TDataset; lnewFileBuffer: Boolean; var FileBuffer: string; var Error: string; cCampos: String): boolean;
    var
        i: integer;
      const
      DELIMITADOR_EXCEL = '"';
      SEPARADOR_EXCEL = ';';
    begin
        result := false;
        try
            if lnewFileBuffer then FileBuffer := '' else FileBuffer := FileBuffer + CRLF;

            with Dataset do begin
                // Pongo las cabeceras
                for i := 0 to FieldCount - 1 do begin
                  if cCampos <> '' then begin
                    if Pos(Fields[i].FieldName,cCampos) > 0 then begin
                        FileBuffer := FileBuffer + AnsiQuotedStr(Fields[i].DisplayLabel, DELIMITADOR_EXCEL) + SEPARADOR_EXCEL;
                    end;
                  end else begin
                    FileBuffer := FileBuffer + AnsiQuotedStr(Fields[i].FieldName, DELIMITADOR_EXCEL) + SEPARADOR_EXCEL;
                  end;
                end;
                //FileBuffer := FileBuffer + AnsiQuotedStr(Fields[FieldCount - 1].FieldName, DELIMITADOR_EXCEL) + CRLF;
                FileBuffer := FileBuffer + CRLF;

                // Insertamos los datos
                First;
                while not eof do begin
                    for i := 0 to FieldCount - 1 do begin
                        if cCampos <> '' then begin
                          if Pos(Fields[i].FieldName, cCampos) > 0 then begin
                            FileBuffer := FileBuffer + FormatFieldToExcel(Fields[i]) + SEPARADOR_EXCEL;
                          end;
                        end else begin
                          FileBuffer := FileBuffer + FormatFieldToExcel(Fields[i]) + SEPARADOR_EXCEL;
                        end;
                    end;
                    //FileBuffer := FileBuffer + FormatearCampoExcel(Fields[FieldCount - 1]) + CRLF;
                    FileBuffer := FileBuffer + CRLF;
                    next;
                end;
            end;
            result := true;
        except
            on e: exception do begin
                raise Exception.Create(e.Message);
            end;
        end;
    end;

    function SaveToCSV(DataSet: TDataset; PrefijoNombreArchivo: string): boolean;
    var
        sdGuardarCSV: TSaveDialog;
        FileBuffer, Error:  String;

    Const
        MSG_TITLE   = 'Exportar CSV';
        MSG_SUCCESS = 'El archivo %s fu� creado exitosamente';
        MSG_EOF = 'No existen datos';
        MSG_TITLE_EMPTY   = 'Atenci�n';

        begin
                try
                      result := false;
                      try
                          sdGuardarCSV := TSaveDialog.Create(nil);
                          sdGuardarCSV.DefaultExt := '*.csv';
                          sdGuardarCSV.Title:= 'Guardar como...';
                          sdGuardarCSV.Filter:= 'Archivos Comma Separated Value|*.csv|Todos|*.*';

                          if not DataSet.IsEmpty  then begin

                            if ExportToCSV(DataSet, True, FileBuffer, Error, '') then begin

                              sdGuardarCSV.FileName := PrefijoNombreArchivo + '_' + StringReplace(DateTimeToStr(Now),':','-',[rfReplaceAll]);
                              if sdGuardarCSV.Execute then begin
                                StringToFile(FileBuffer, sdGuardarCSV.FileName);
                                MsgBox(Format(MSG_SUCCESS, [sdGuardarCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
                              end
                              else
                              	Exit;

                            end;

                            result := true;

                          end else begin
                            MsgBox(Format(MSG_EOF, [sdGuardarCSV.FileName]), MSG_TITLE_EMPTY, MB_ICONINFORMATION);
                          end;

                          FreeAndNil(sdGuardarCSV);
                      except
                          on e: exception do begin
                              raise Exception.Create(e.Message);
                          end;
                      end;

                finally
                      if Assigned(sdGuardarCSV) then begin
                          FreeAndNil(sdGuardarCSV);
                      end;
                end;
        end;
end.






