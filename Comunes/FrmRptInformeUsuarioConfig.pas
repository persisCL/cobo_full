{********************************** File Header ********************************
File Name   : FrmRptInformeUsuarioConfig.pas
Author      :
Date Created:
Language    : ES-AR
Description : 
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura en el DFM

Descripcion : Se sobrecarga el metodo Inicializar para incluir la base de datos
              donde se debe conectar la query qry_Parametros.
Firma       : PAR00133_COPAMB_ALA_20110801
*******************************************************************************}
unit FrmRptInformeUsuarioConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables, UtilDB, Util, UtilProc, DateEdit, DmiCtrls, DMConnection,
  ADODB, Variants, PeaTypes, ExtCtrls, DPSControls, StrUtils, Validate,
  TimeEdit, CheckLst, VariantComboBox, utilReportes, DBClient;

type
  TTodos = record
    NombreControl: string;
    EstadoTodos: boolean;
  end;

  TFormRptInformeUsuarioConfig = class(TForm)
    //qry_Parametros: TADOQuery;    //20160629_MGO
	bvlParametros: TBevel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    spVariables: TADOStoredProc;	//20160629_MGO
	procedure btnAceptarClick(Sender: TObject);
  private
	{ Private declarations }
	PParams: ^TParamsReporte;
	FCodigoConsulta: Integer;
    TodosClick: array of TTodos;
    FcsParametros: TClientDataSet;
    procedure GuardarParametros;
    procedure GuardarParametrosDiseno(FcsParametros: TClientDataSet);
    procedure doOnclickCheck (Sender: TObject);
  public
	{ Public declarations }
	MostrarGrafico: boolean;
	function Inicializar(CodigoConsulta: Integer; var Params: TParamsReporte; TipoGrafico: AnsiString): Boolean; overload;
    function Inicializar(CodigoConsulta: Integer; var Params: TParamsReporte; TipoGrafico: AnsiString; ConnectionBaseDatos: TADOConnection): Boolean; overload;     //PAR00133_COPAMB_ALA_20110801
	function Inicializar(csParametros: TClientDataSet; var Params: TParamsReporte; TipoGrafico: AnsiString): Boolean; overload;
  end;

Var
  FormRptInformeUsuarioConfig: TFormRptInformeUsuarioConfig;

implementation

{$R *.DFM}
function TFormRptInformeUsuarioConfig.Inicializar(CodigoConsulta: Integer;
  var Params: TParamsReporte; TipoGrafico: AnsiString;
  ConnectionBaseDatos: TADOConnection): Boolean;
begin
    { INICIO : 20160629_MGO
    qry_Parametros.Connection := ConnectionBaseDatos;
    }
    spVariables.Connection := ConnectionBaseDatos;
    // FIN : 20160629_MGO
    Result := Inicializar(CodigoConsulta, Params, TipoGrafico);
end;

function TFormRptInformeUsuarioConfig.Inicializar(CodigoConsulta: Integer; var Params: TParamsReporte; TipoGrafico: AnsiString): Boolean;
Var
	ED: TEdit;
	LB: TLabel;
	CB: TVariantCheckListBox;
	DE: TDateEdit;
	TE: TTimeEdit;
	CKB: TCheckBox;
	Valor: Variant;
	QryCB: TADOQuery;
	NE: TNumericEdit;
	i, DistIzquierda, y, nParam, CantidadCB: integer;
	S : String;
    day, year, month, hour, minute, second: word;

begin
	FCodigoConsulta := CodigoConsulta;
	PParams := @Params;
	MostrarGrafico := False;
    { INICIO : 20160629_MGO
	qry_Parametros.parameters.ParamByName('CodigoConsulta').value := CodigoConsulta;
	Result := OpenTables([qry_Parametros]);
    }
    spVariables.parameters.Refresh;
    spVariables.parameters.ParamByName('@CodigoConsulta').value := CodigoConsulta;
	Result := OpenTables([spVariables]);
    // FIN : 20160629_MGO
	if not Result then Exit;
	 // Carga las etiquetas de los campos.
	DistIzquierda := 100;
	y := 20;
    CantidadCB:= 0;
	{ INICIO : 20160629_MGO
	While not qry_Parametros.Eof do begin

		LB := TLabel.Create(Self);
		LB.Caption := Trim(MidStr(qry_Parametros.FieldByName('Descripcion').AsString, 1, 20)) + ':';
		LB.Top := y + 3;
		LB.Left := 20;
		LB.Parent := Self;

		Inc(y, iif(UpperCase(qry_Parametros.FieldByName('Tipo').AsString)[1] = 'L', 100 * 3 div 4, 25));

        if UpperCase(qry_Parametros.FieldByName('Tipo').AsString)[1] = 'L' then begin
            bvlParametros.height := bvlParametros.height + 100 * 3 div 4;
        end else begin
            bvlParametros.height := bvlParametros.height + 25;
        end;

		if ((LB.Left + LB.Width) > DistIzquierda) then begin
			DistIzquierda := (LB.Left + LB.Width) + 10;
		end;
		qry_Parametros.Next;
	end;
    }
    While not spVariables.Eof do begin
    	LB := TLabel.Create(Self);
		LB.Caption := Trim(MidStr(spVariables.FieldByName('Descripcion').AsString, 1, 20)) + ':';
		LB.Top := y + 3;
		LB.Left := 20;
		LB.Parent := Self;

		Inc(y, iif(UpperCase(spVariables.FieldByName('Tipo').AsString)[1] = 'L', 100 * 3 div 4, 25));

        if UpperCase(spVariables.FieldByName('Tipo').AsString)[1] = 'L' then begin
            bvlParametros.height := bvlParametros.height + 100 * 3 div 4;
        end else begin
            bvlParametros.height := bvlParametros.height + 25;
        end;

		if ((LB.Left + LB.Width) > DistIzquierda) then begin
			DistIzquierda := (LB.Left + LB.Width) + 10;
		end;
		spVariables.Next;
	end;
    // FIN : 20160629_MGO

	if not (TipoGrafico = SIN_GRAFICO) then
	begin
		LB := TLabel.Create(Self);
		LB.Caption := 'Gr�fico';
		LB.Top := y + 3;
		LB.Left := 20;
		LB.Parent := Self;
		bvlParametros.height := bvlParametros.height + 25;
		if ((LB.Left + LB.Width) > DistIzquierda) then begin
			DistIzquierda := (LB.Left + LB.Width) + 10;
		end;
	end;
	// Carga los par�metros.
	//qry_Parametros.First;    //20160629_MGO
	spVariables.First;	//20160629_MGO
	y := 20;
	nParam := 0;
	//While not qry_Parametros.Eof do begin     //20160629_MGO
	While not spVariables.Eof do begin	//20160629_MGO
		if Params[nParam] <> NULL then Valor := Params[nParam] else Valor := NULL;

		//case UpperCase(qry_Parametros.FieldByName('Tipo').AsString)[1] of //20160629_MGO
		case UpperCase(spVariables.FieldByName('Tipo').AsString)[1] of	//20160629_MGO
			'D':
				begin
					DE := TDateEdit.Create(Self);
					//DE.Name := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString);  //20160629_MGO
					DE.Name := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString);	//20160629_MGO
					DE.AllowEmpty := True;
					DE.SetBounds(DistIzquierda, y, 90, DE.Height);
					DE.Parent := Self;
					if VarIsNull(Valor) or (Valor = Unassigned) then DE.Date := Date
                    else begin
                        year := strToInt(copy(Valor, 1 , 4));
                        month := strToINt(copy(Valor, 5 , 2));
                        day := strToINt(copy(Valor, 7 , 2));
                        DE.Date := EncodeDate(year, month, day);
                    end;
				end;
			'H':
				begin
                    (* Agregamos la fecha para la variable FechaHora*)
                    DE := TDateEdit.Create(Self);
					//DE.Name := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString) + '_F';   //20160629_MGO
					DE.Name := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString) + '_F';	//20160629_MGO
					DE.AllowEmpty := True;
					DE.SetBounds(DistIzquierda, y, 90, DE.Height);
					DE.Parent := Self;
					if VarIsNull(Valor) or (Valor = Unassigned) then DE.Date := Date
                    else begin
                        year := strToInt(copy(Valor, 1 , 4));
                        month := strToINt(copy(Valor, 5 , 2));
                        day := strToINt(copy(Valor, 7 , 2));
                        DE.Date := EncodeDate(year, month, day);
                    end;

                    (* AGregamos la hora para la variable FechaHora*)
					TE := TTimeEdit.Create(Self);
					//TE.Name := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString) + '_H';  //20160629_MGO
					TE.Name := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString) + '_H';	//20160629_MGO
					TE.AllowEmpty := True;
					TE.SetBounds(DE.Left + DE.width + 10, y, 90, TE.Height);
					TE.Parent := Self;
					if VarIsNull(Valor) or (Valor = Unassigned) then TE.Time  := Time
                    else begin
                        hour := strToInt(copy(Valor, 10 , 2));
                        minute := strToINt(copy(Valor, 13 , 2));
                        Second := strToINt(copy(Valor, 16 , 2));
                        TE.Time := EncodeTime(Hour, minute, second, 0);
                    end;
				end;
			'L':
				begin
					CB := TVariantCheckListBox.Create(Self);
                    CB.OnClickCheck := DoOnclickCheck;
					//CB.Name := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString);   //20160629_MGO
					CB.Name := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString);	//20160629_MGO
					CB.SetBounds(DistIzquierda, y, 280, CB.Height * 3 div 4);
					CB.Parent := Self;
					QryCB := TAdoQuery.Create(nil);
                    CantidadCB := CantidadCB + 1;
                    SetLength(TodosClick, CantidadCB);
                    TodosClick[CantidadCB-1].NombreControl := CB.Name;
					try
						QryCB.Connection := DMConnections.BaseCAC;
						//QryCB.SQL.Text := Trim(qry_Parametros.FieldByName('ConsultaLista').AsString);   //20160629_MGO
						QryCB.SQL.Text := Trim(spVariables.FieldByName('ConsultaLista').AsString);	//20160629_MGO
						QryCB.Open;

						CB.Clear;
						CB.Items.Add('Todos', 'Todos');
						While not QryCB.Eof do begin
							if QryCB.FieldCount = 1 then
								S := QryCB.Fields[0].AsString
							else
								S := QryCB.Fields[1].AsString;
							CB.Items.Add(Trim(QryCB.Fields[0].AsString), iif(QryCB.Fields[0].DataType = ftInteger, S, ''''+S+''''));

							if not VarIsNull(Valor) and (Pos(S, Valor) > 0) then begin
                                CB.Checked[CB.count - 1] := true;
							end;

    					    QryCB.Next;
                        end;


                        (* Actualizamos los items chekeados *)
                        if ('*' = Valor) then begin
                            CB.Checked[0] := true;
                        end;

                        TodosClick[CantidadCB-1].EstadoTodos := CB.Checked[0];

                        QryCB.first;
                        i := 1;
                        While not QryCB.Eof do begin
							if QryCB.FieldCount = 1 then
								S := QryCB.Fields[0].AsString
							else
								S := QryCB.Fields[1].AsString;

							if (not VarIsNull(Valor) and (Pos(S, Valor) > 0)) or CB.Checked[0] then begin
                                CB.Checked[i] := true;
							end;

							QryCB.Next;
                            inc(i);
                        end;

						CB.ItemIndex := 0;
					finally
						QryCB.Close;
						QryCB.Free;
					end;

					Inc(y, CB.Height - 22);
				end;
			'N':
				begin  
					NE := TNumericEdit.Create(Self);
					//NE.Name := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString);     //20160629_MGO
					NE.Name := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString);	//20160629_MGO
					NE.SetBounds(DistIzquierda, y, 90, Ne.Height);
					NE.Parent := Self;
					if VarIsNull(Valor) then NE.Value := 0 else NE.Value := StrToInt(Valor);
				end;
			else
				begin
					ED := TEdit.Create(Self);
					//ED.Name := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString);    //20160629_MGO
					ED.Name := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString);	//20160629_MGO
					ED.SetBounds(DistIzquierda, y, 280, ED.Height);
					ED.Parent := Self;
					if VarIsNull(Valor) then ED.Text := '' else ED.Text := Valor;
				end;
		end;
		Inc(nParam);
		Inc(y, 25);
		//qry_Parametros.Next;        //20160629_MGO
	    spVariables.Next;	//20160629_MGO
	end;
	if not (TipoGrafico = SIN_GRAFICO) then
	begin
		CKB := TCheckBox.Create(Self);
		CKB.Name := 'var_Grafico';
		CKB.SetBounds(DistIzquierda, y, 300, CKB.Height);
		CKB.Parent := Self;
		CKB.Caption := '';
		CKB.Checked := True;
	end;
	// Acomodamos los dem�s controles
	ClientHeight := ((bvlParametros.Top * 3) + bvlParametros.Height) + btnAceptar.Height;
	CenterForm(Self);
	// Listo
	Result := True;
end;

procedure TFormRptInformeUsuarioConfig.GuardarParametros;
Var
	i,
	item,
	nParam: integer;
	Lista,
	fecha, hora, ControlName: AnsiString;
begin
	//qry_Parametros.First;	//20160629_MGO
	spVariables.First;	//20160629_MGO
	nParam := 0;
	//While not qry_Parametros.Eof do begin	//20160629_MGO
	While not spVariables.Eof do begin	//20160629_MGO
		Lista := '';
        { INICIO : 20160629_MGO
   		ControlName := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString);
		case UpperCase(qry_Parametros.FieldByName('Tipo').AsString)[1] of
			'D': if not (TDateEdit(FindComponent(ControlName)).Date = NULLDATE) then
                PParams[nParam] := FormatDateTime('yyyymmdd', TDateEdit(FindComponent(ControlName)).Date);
			'H': begin
               		ControlName := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString) + '_F' ;
                    if not (TDateEdit(FindComponent(ControlName)).Date = NULLDATE) then
                        fecha := FormatDateTime('yyyymmdd', TDateEdit(FindComponent(ControlName)).Date)
                    else
                        fecha := '';
                    ControlName := 'var_' + Trim(qry_Parametros.FieldByName('CodigoVariable').AsString) + '_H' ;
                    if not (TTimeEdit(FindComponent(ControlName)).Time = NULLDATE) then
                        hora := FormatDateTime('hh:nn:ss', TTimeEdit(FindComponent(ControlName)).Time)
                    else
                        hora := '';
                    PParams[nParam] := trim(fecha + ' ' + hora);
                 end;
			'L': if not (TComboBox(FindComponent(ControlName)).Text = '') 		then begin
				with TVariantCheckListBox(FindComponent(ControlName)) Do Begin
					i := 0;
					for item := 0 to Items.Count-1 do
						if checked[item] then begin
							Lista := Lista + Items[item].Value + '*';
							inc(i)
						end;

					if (i = Items.Count) Or (checked[0]) Then Lista := '*'
				end;

				PParams[nParam] := Lista
			end;
			'N': if not (TNumericEdit(FindComponent(ControlName)).Text = '') 	then PParams[nParam] := TNumericEdit(FindComponent(ControlName)).Text;
			else if not (TEdit(FindComponent(ControlName)).Text = '') 			then PParams[nParam] := TEdit(FindComponent(ControlName)).Text;
		end;
		Inc(nParam);
		qry_Parametros.Next;
        }
	    ControlName := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString);
		case UpperCase(spVariables.FieldByName('Tipo').AsString)[1] of
			'D': if not (TDateEdit(FindComponent(ControlName)).Date = NULLDATE) then
                PParams[nParam] := FormatDateTime('yyyymmdd', TDateEdit(FindComponent(ControlName)).Date);
			'H': begin
               		ControlName := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString) + '_F' ;
                    if not (TDateEdit(FindComponent(ControlName)).Date = NULLDATE) then
                        fecha := FormatDateTime('yyyymmdd', TDateEdit(FindComponent(ControlName)).Date)
                    else
                        fecha := '';
                    ControlName := 'var_' + Trim(spVariables.FieldByName('CodigoVariable').AsString) + '_H' ;
                    if not (TTimeEdit(FindComponent(ControlName)).Time = NULLDATE) then
                        hora := FormatDateTime('hh:nn:ss', TTimeEdit(FindComponent(ControlName)).Time)
                    else
                        hora := '';
                    PParams[nParam] := trim(fecha + ' ' + hora);
                 end;
			'L': if not (TComboBox(FindComponent(ControlName)).Text = '') 		then begin
				with TVariantCheckListBox(FindComponent(ControlName)) Do Begin
					i := 0;
					for item := 0 to Items.Count-1 do
						if checked[item] then begin
							Lista := Lista + Items[item].Value + '*';
							inc(i)
						end;

					if (i = Items.Count) Or (checked[0]) Then Lista := '*'
				end;

				PParams[nParam] := Lista
			end;
			'N': if not (TNumericEdit(FindComponent(ControlName)).Text = '') 	then PParams[nParam] := TNumericEdit(FindComponent(ControlName)).Text;
			else if not (TEdit(FindComponent(ControlName)).Text = '') 			then PParams[nParam] := TEdit(FindComponent(ControlName)).Text;
		end;
		Inc(nParam);
		spVariables.Next;
        // FIN : 20160629_MGO
	end;
end;

procedure TFormRptInformeUsuarioConfig.btnAceptarClick(Sender: TObject);
var
    i: integer;
begin
	for i := 0 to (Self.ControlCount - 1) do
	begin
        if (Self.Controls[i].Name = 'var_Grafico') then
        begin
			MostrarGrafico := TCheckBox(Self.Controls[i]).Checked;
            break;
        end;
    end;
    //if qry_Parametros.Active then	//20160629_MGO
    if spVariables.Active then	//20160629_MGO
        GuardarParametros
    else
        GuardarParametrosDiseno(FcsParametros);
	ModalResult := mrOk
end;

procedure TFormRptInformeUsuarioConfig.doOnclickCheck(Sender: TObject);
var
    i, j, k: integer;
begin


    for j:=0 to Length(TodosClick) - 1 do
        if (((Trim(TodosClick[j].NombreControl) = Trim((TVariantCheckListBox(Sender).Name)))
            and (TVariantCheckListBox(Sender).Checked[0] <> TodosClick[j].EstadoTodos))) then begin
            if (TVariantCheckListBox(Sender).Checked[0]) then begin
                for i:= 1 to TVariantCheckListBox(Sender).Count - 1  do
                    TVariantCheckListBox(Sender).Checked [i]:= True;
                TodosClick[j].EstadoTodos :=  True;
            end
            else begin
                for i:= 1 to TVariantCheckListBox(Sender).Count - 1  do
                    TVariantCheckListBox(Sender).Checked [i]:= False;
                    TodosClick[j].EstadoTodos :=  False;
            end;
        end
        else
            if (Trim(TodosClick[j].NombreControl) = Trim(TVariantCheckListBox(Sender).Name)) then
                if (TVariantCheckListBox(Sender).Checked[0]) then begin
                    i:= 0;
                    while (i < TVariantCheckListBox(Sender).Count) and (TVariantCheckListBox(Sender).Checked [i]) do
                        i := i + 1;
                    if (i < TVariantCheckListBox(Sender).Count) then begin
                        TVariantCheckListBox(Sender).Checked[0] := False;
                        for k:=0 to Length(TodosClick) - 1 do
                            if (Trim(TodosClick[k].NombreControl) = Trim(TVariantCheckListBox(Sender).Name)) then
                                TodosClick[k].EstadoTodos :=  False;
                    end;
                end;

end;

function TFormRptInformeUsuarioConfig.Inicializar(
  csParametros: TClientDataSet; var Params: TParamsReporte;
  TipoGrafico: AnsiString): Boolean;
Var
	ED: TEdit;
	LB: TLabel;
	CB: TVariantCheckListBox;
	DE: TDateEdit;
	TE: TTimeEdit;
	CKB: TCheckBox;
	Valor: Variant;
	QryCB: TADOQuery;
	NE: TNumericEdit;
	i, DistIzquierda, y, nParam, CantidadCB: integer;
	S : String;
    day, year, month, hour, minute, second: word;

begin
    FcsParametros := csParametros;
	PParams := @Params;
	MostrarGrafico := False;
	 // Carga las etiquetas de los campos.
	DistIzquierda := 100;
	y := 20;
    CantidadCB:= 0;
    csParametros.First;
	While not csParametros.Eof do begin

		LB := TLabel.Create(Self);
		LB.Caption := Trim(MidStr(csParametros.FieldByName('Descripcion').AsString, 1, 15)) + ':';
		LB.Top := y + 3;
		LB.Left := 20;
		LB.Parent := Self;

		Inc(y, iif(UpperCase(csParametros.FieldByName('Tipo').AsString)[1] = 'L', 100 * 3 div 4, 25));

        if UpperCase(csParametros.FieldByName('Tipo').AsString)[1] = 'L' then begin
            bvlParametros.height := bvlParametros.height + 100 * 3 div 4;
        end else begin
            bvlParametros.height := bvlParametros.height + 25;
        end;

		if ((LB.Left + LB.Width) > DistIzquierda) then begin
			DistIzquierda := (LB.Left + LB.Width) + 10;
		end;
		csParametros.Next;
	end;
	if not (TipoGrafico = SIN_GRAFICO) then
	begin
		LB := TLabel.Create(Self);
		LB.Caption := 'Gr�fico';
		LB.Top := y + 3;
		LB.Left := 20;
		LB.Parent := Self;
		bvlParametros.height := bvlParametros.height + 25;
		if ((LB.Left + LB.Width) > DistIzquierda) then begin
			DistIzquierda := (LB.Left + LB.Width) + 10;
		end;
	end;
	// Carga los par�metros.
	csParametros.First;
	y := 20;
	nParam := 0;
	While not csParametros.Eof do begin
		if Params[nParam] <> NULL then Valor := Params[nParam] else Valor := NULL;

		case UpperCase(csParametros.FieldByName('Tipo').AsString)[1] of
			'D':
				begin
					DE := TDateEdit.Create(Self);
					DE.Name := 'var_' + Trim(csParametros.FieldByName('CodigoVariable').AsString);
					DE.AllowEmpty := True;
					DE.SetBounds(DistIzquierda, y, 90, DE.Height);
					DE.Parent := Self;
					if VarIsNull(Valor) or (Valor = Unassigned) then DE.Date := Date
                    else begin
                        year := strToInt(copy(Valor, 1 , 4));
                        month := strToINt(copy(Valor, 5 , 2));
                        day := strToINt(copy(Valor, 7 , 2));
                        DE.Date := EncodeDate(year, month, day);
                    end;
				end;
			'H':
				begin
                    (* Agregamos la fecha para la variable FechaHora*)
                    DE := TDateEdit.Create(Self);
					DE.Name := 'var_' + Trim(csParametros.FieldByName('CodigoVariable').AsString) + '_F';
					DE.AllowEmpty := True;
					DE.SetBounds(DistIzquierda, y, 90, DE.Height);
					DE.Parent := Self;
					if VarIsNull(Valor) or (Valor = Unassigned) then DE.Date := Date
                    else begin
                        year := strToInt(copy(Valor, 1 , 4));
                        month := strToINt(copy(Valor, 5 , 2));
                        day := strToINt(copy(Valor, 7 , 2));
                        DE.Date := EncodeDate(year, month, day);
                    end;

                    (* AGregamos la hora para la variable FechaHora*)
					TE := TTimeEdit.Create(Self);
					TE.Name := 'var_' + Trim(csParametros.FieldByName('CodigoVariable').AsString) + '_H';
					TE.AllowEmpty := True;
					TE.SetBounds(DE.Left + DE.width + 10, y, 90, TE.Height);
					TE.Parent := Self;
					if VarIsNull(Valor) or (Valor = Unassigned) then TE.Time  := Time
                    else begin
                        hour := strToInt(copy(Valor, 10 , 2));
                        minute := strToINt(copy(Valor, 13 , 2));
                        Second := strToINt(copy(Valor, 16 , 2));
                        TE.Time := EncodeTime(Hour, minute, second, 0);
                    end;
				end;
			'L':
				begin
					CB := TVariantCheckListBox.Create(Self);
                    CB.OnClickCheck := DoOnclickCheck;
					CB.Name := 'var_' + Trim(csParametros.FieldByName('CodigoVariable').AsString);
					CB.SetBounds(DistIzquierda, y, 280, CB.Height * 3 div 4);
					CB.Parent := Self;
					QryCB := TAdoQuery.Create(nil);
                    CantidadCB := CantidadCB + 1;
                    SetLength(TodosClick, CantidadCB);
                    TodosClick[CantidadCB-1].NombreControl := CB.Name;
					try
						QryCB.Connection := DMConnections.BaseCAC;
						QryCB.SQL.Text := Trim(csParametros.FieldByName('ConsultaLista').AsString);
						QryCB.Open;

						CB.Clear;
						CB.Items.Add('Todos', 'Todos');
						While not QryCB.Eof do begin
							if QryCB.FieldCount = 1 then
								S := QryCB.Fields[0].AsString
							else
								S := QryCB.Fields[1].AsString;
							CB.Items.Add(Trim(QryCB.Fields[0].AsString), iif(QryCB.Fields[0].DataType = ftInteger, S, ''''+S+''''));

							if not VarIsNull(Valor) and (Pos(S, Valor) > 0) then begin
                                CB.Checked[CB.count - 1] := true;
							end;

    					    QryCB.Next;
                        end;


                        (* Actualizamos los items chekeados *)
                        if ('*' = Valor) then begin
                            CB.Checked[0] := true;
                        end;

                        TodosClick[CantidadCB-1].EstadoTodos := CB.Checked[0];

                        QryCB.first;
                        i := 1;
                        While not QryCB.Eof do begin
							if QryCB.FieldCount = 1 then
								S := QryCB.Fields[0].AsString
							else
								S := QryCB.Fields[1].AsString;

							if (not VarIsNull(Valor) and (Pos(S, Valor) > 0)) or CB.Checked[0] then begin
                                CB.Checked[i] := true;
							end;

							QryCB.Next;
                            inc(i);
                        end;

						CB.ItemIndex := 0;
					finally
						QryCB.Close;
						QryCB.Free;
					end;

					Inc(y, CB.Height - 22);
				end;
			'N':
				begin  
					NE := TNumericEdit.Create(Self);
					NE.Name := 'var_' + Trim(csParametros.FieldByName('CodigoVariable').AsString);
					NE.SetBounds(DistIzquierda, y, 90, Ne.Height);
					NE.Parent := Self;
					if VarIsNull(Valor) then NE.Value := 0 else NE.Value := StrToInt(Valor);
				end;
			else
				begin
					ED := TEdit.Create(Self);
					ED.Name := 'var_' + Trim(csParametros.FieldByName('CodigoVariable').AsString);
					ED.SetBounds(DistIzquierda, y, 280, ED.Height);
					ED.Parent := Self;
					if VarIsNull(Valor) then ED.Text := '' else ED.Text := Valor;
				end;
		end;
		Inc(nParam);
		Inc(y, 25);
		csParametros.Next;
	end;
	if not (TipoGrafico = SIN_GRAFICO) then
	begin
		CKB := TCheckBox.Create(Self);
		CKB.Name := 'var_Grafico';
		CKB.SetBounds(DistIzquierda, y, 300, CKB.Height);
		CKB.Parent := Self;
		CKB.Caption := '';
		CKB.Checked := True;
	end;
	// Acomodamos los dem�s controles
	ClientHeight := ((bvlParametros.Top * 3) + bvlParametros.Height) + btnAceptar.Height;
	CenterForm(Self);
	// Listo
	Result := True;
end;

procedure TFormRptInformeUsuarioConfig.GuardarParametrosDiseno(
  FcsParametros: TClientDataSet);
Var
	i,
	item,
	nParam: integer;
	Lista,
	fecha, hora, ControlName: AnsiString;
begin
	FcsParametros.First;
	nParam := 0;
	While not FcsParametros.Eof do begin
		Lista := '';
   		ControlName := 'var_' + Trim(FcsParametros.FieldByName('CodigoVariable').AsString);
		case UpperCase(FcsParametros.FieldByName('Tipo').AsString)[1] of
			'D': if not (TDateEdit(FindComponent(ControlName)).Date = NULLDATE) then
                PParams[nParam] := FormatDateTime('yyyymmdd', TDateEdit(FindComponent(ControlName)).Date);
			'H': begin
               		ControlName := 'var_' + Trim(FcsParametros.FieldByName('CodigoVariable').AsString) + '_F' ;
                    if not (TDateEdit(FindComponent(ControlName)).Date = NULLDATE) then
                        fecha := FormatDateTime('yyyymmdd', TDateEdit(FindComponent(ControlName)).Date)
                    else
                        fecha := '';
                    ControlName := 'var_' + Trim(FcsParametros.FieldByName('CodigoVariable').AsString) + '_H' ;
                    if not (TTimeEdit(FindComponent(ControlName)).Time = NULLDATE) then
                        hora := FormatDateTime('hh:nn:ss', TTimeEdit(FindComponent(ControlName)).Time)
                    else
                        hora := '';
                    PParams[nParam] := trim(fecha + ' ' + hora);
                 end;
			'L': if not (TComboBox(FindComponent(ControlName)).Text = '') 		then begin
				with TVariantCheckListBox(FindComponent(ControlName)) Do Begin
					i := 0;
					for item := 0 to Items.Count-1 do
						if checked[item] then begin
							Lista := Lista + Items[item].Value + '*';
							inc(i)
						end;

					if (i = Items.Count) Or (checked[0]) Then Lista := '*'
				end;

				PParams[nParam] := Lista
			end;
			'N': if not (TNumericEdit(FindComponent(ControlName)).Text = '') 	then PParams[nParam] := TNumericEdit(FindComponent(ControlName)).Text;
			else if not (TEdit(FindComponent(ControlName)).Text = '') 			then PParams[nParam] := TEdit(FindComponent(ControlName)).Text;
		end;
		Inc(nParam);
		FcsParametros.Next;
	end;
end;
end.
