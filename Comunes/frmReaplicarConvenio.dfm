object formReaplicarConvenio: TformReaplicarConvenio
  Left = 211
  Top = 464
  Anchors = []
  BorderStyle = bsDialog
  Caption = 'Reaplicaci'#243'n de Convenio'
  ClientHeight = 183
  ClientWidth = 664
  Color = clBtnFace
  Constraints.MinWidth = 670
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  DesignSize = (
    664
    183)
  PixelsPerInch = 96
  TextHeight = 13
  object gbPorCliente: TGroupBox
    Left = 6
    Top = 0
    Width = 653
    Height = 65
    Anchors = [akLeft, akTop, akRight]
    Caption = ' B'#250'squeda Convenio '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 12
      Top = 14
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object lbl_NumeroConvenio: TLabel
      Left = 184
      Top = 14
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object pe_NumeroDocumento: TPickEdit
      Left = 12
      Top = 28
      Width = 145
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnChange = pe_NumeroDocumentoChange
      EditorStyle = bteTextEdit
      OnButtonClick = pe_NumeroDocumentoButtonClick
    end
    object cb_Convenios: TVariantComboBox
      Left = 184
      Top = 28
      Width = 209
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
      OnChange = cb_ConveniosChange
      OnDrawItem = cb_ConveniosDrawItem
      Items = <>
    end
  end
  object gb_DatosCliente: TGroupBox
    Left = 5
    Top = 72
    Width = 654
    Height = 70
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Datos del Cliente '
    TabOrder = 1
    DesignSize = (
      654
      70)
    object Label6: TLabel
      Left = 17
      Top = 32
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 91
      Top = 31
      Width = 72
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Apellido Cliente'
    end
    object lblDomicilio: TLabel
      Left = 90
      Top = 49
      Width = 74
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 17
      Top = 49
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 17
      Top = 15
      Width = 31
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRUT: TLabel
      Left = 91
      Top = 14
      Width = 23
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'RUT'
    end
    object lblSaldoDebitos: TLabel
      Left = 473
      Top = 15
      Width = 86
      Height = 13
      Caption = 'Saldo d'#233'bitos :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblSaldoDebitosImporte: TLabel
      Left = 571
      Top = 14
      Width = 6
      Height = 13
      Caption = '0'
    end
    object lblSaldoCreditos: TLabel
      Left = 473
      Top = 31
      Width = 79
      Height = 13
      Caption = 'Saldo pagos :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblSaldoCreditosImporte: TLabel
      Left = 571
      Top = 31
      Width = 6
      Height = 13
      Caption = '0'
    end
  end
  object btnReaplicar: TButton
    Left = 5
    Top = 152
    Width = 196
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Reaplicar Convenio (F9)'
    Default = True
    Enabled = False
    TabOrder = 2
    OnClick = btnReaplicarClick
  end
  object btn_Salir: TButton
    Left = 584
    Top = 152
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 3
    OnClick = btn_SalirClick
  end
  object sp_ObtenerConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end>
    Left = 556
    Top = 19
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 590
    Top = 19
  end
  object spReaplicarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ReaplicarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 468
    Top = 19
  end
  object spObtenerDatosReemisionConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosReemisionConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SaldoDebitos'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 10
        Value = '0'
      end
      item
        Name = '@SaldoCreditos'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 10
        Value = '0'
      end>
    Left = 508
    Top = 19
  end
end
