{********************************** Unit Header ********************************
File Name : ImgProcs.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 3:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
Revision    : 4
Date        : 07-Junio-2011
Author      : Nelson Droguett Sierra
Description :  (Ref.Fase2) Las imagenes se transfieren como imagenes individuales.
                Ya no se usar� el formato "archivo padre".
                Al pasar, quedaran en una carpeta con la siguiente estructura
                DIR_IMAGENES_PARAMETROMM             (MM es el mes)
                    |
                    FECHA TRANSITO                   (YYYY-MM-DD)
                        |
                        CONCESIONARIA                (Nombre Corto)
                            |
                            MOD 1000 NUMCORRCA
                                |
                                ARCHIVOIMAGEN.JPG
                Ej : \IMAGENESCAC12\2011-12-01\AMB\MODNUMCORRCA\*.JPG (no padre)
       Modificaciones debidas al cambio en el formato de almacenamiento
       de las imagenes. (Se agrega como parametro el nombre corto de la concesionaria)
            - Funci�n ObtenerImagenPatente: se a�adi� el par�metro NombreCorto.
            - Funci�n ObtenerImagenTransito: se a�adi� el par�metro NombreCorto.
            - Funci�n CopiarImagenTransitoPendiente: Se modific� para almacenar la imagen
            en el formato que corresponda seg�n el estado de la Migraci�n.se a�adi� el par�metro NombreCorto.
       Funciones Nuevas:
            - Funci�n ArmarPathPadreTransitosNuevoFormato.
            - Funci�n AlmacenarImagenPatente.
Firma       : SS-377-NDR-20110607

Firma       : PAR00133_COPAMB_ALA_20110727
Description : Se agrega funcion que retorna la ruta donde se encuentra la im�gen
             de TVM (Ticket de Venta Manual)

Autor       :   CQuezadaI
Firma       :   SS_1091_CQU_20131001
Descripcion :   Se realiza Overload de la funci�n ObtenerImagenPatente
                para que obtenga la imagen de referencia de Porticos Italianos
                OJO: la imagen de referencia puede ser Italiana o Kapsch y no es,
                NO ES, dependiente del portico ni del transito en evaluaci�n.
                Se modifica la funci�n ObtenerImagenPatente para que devuelva
                True s�lo si DescriError es vac�o, se hizo as� ya que la variable
                no se limpia.

Autor       :   Mcabello
Firma       :   SS_1132_MCA_20131004
Descripcion :   Se cambian los mensajes para identificar archivo o NumCorrCO en caso de error

Autor       :   CQuezadaI
Firma       :   SS_1135_CQU_20131030
Descripcion :   Se agrega nueva funci�n llamada ArmarPathPadreTransitosInfractor la cual
                armar� la ruta de la imagen infractora desde la ruta que se le env�e
                y si no se le env�a usar� la del Parametro DIR_IMAGENES_TRANSITOS_INFRACTORES
                Eso se hizo as� ya que ArmarPathPadreTransitosNuevoFormato SIEMPRE usa la ruta
                de los tr�nsitos y dicha ruta puede ser borrada, cosa que NO PUEDE pasar
                con las infracciones ya que estas por contrato se deben guardar para siempre.
                Se agregan nuevas funciones ObtenerImagenTransitoInfractor y su overload
                para obtener las infracciones desde donde corresponde ya que
                ObtenerImagenTransito busca la imagen en el directorio de tr�nsitos y no de infracciones.

Firma		: SS_1163_NDR_20140206
Descripcion	: Agregar 4 puntos de cobro italianos al monitor de puntos de cobro.

Firma		: SS_1147W_CQU_20141215
Descripcion	: Se agrega overload de la funci�n ObtenerImagenTransito
			  este overload contempla la recepci�n de un TObject en vez de una imagen
			  luego, seg�n sea el caso, carga la imagen Italiana o Kapsch y retorna
			  el objeto para ser CASTEADO al tipo que corresponda, adicionalmente
			  devuelve el booleano EsKapsch que indica si la imagen Es Kapsch o no.
			  Esto se hace a ra�z de la llegada de ortos p�rticos (ni Kapsch ni Italianos)

Firma		: SS_1261_CQU_20150506
Descripcion	: Se agrega validaci�n al tama�o del archivo y devuelve el error teLeerArchivo

Fecha       :   01-06-2015
Autor       :   CQuezadaI
Firma       :   SS_1288_CQU_20150601
Descripcion :   Se replica el codigo de la SS_1261_CQU_20150506 agregando validaci�n al tama�o del archivo.

Firma		: SS_1374A_CQU_20150924
Descripcion	: La funcionalidad AjustarTama�oImagen implementada por SS_1382_MCA_20150928
              se cambia para ac� ya que es com�n a todo el sistema

Firma		: SS_1416_CQU_20151123
Descripcion	: Se modifica el mensaje de error cuando no puede cargar la imagen, ahora se indica el NumCorrCO
*******************************************************************************}

unit ImgProcs;

interface

Uses
	Classes, Windows, Messages, SysUtils, DB, DBTables, JPeg, JpegPlus, ADODB,
    StdCtrls, Util, Graphics, Variants, ImgTypes, SyncObjs, AdminIMG, utilImg,
    WinSvc, ConstParametrosGenerales, DateUtils,
    StrUtils,
    ImagePlus,// SS_1374A_CQU_20150924                                                                                                                                                           //PAR00133_COPAMB_ALA_20110727
	DMConnection;

// Funciones para administrar imagenes de referencia
function ObtenerImagenPatente(ImagePlatePath,Patente, NombreCorto: AnsiString; TipoImg: TTipoImagen;                                                                      ///SS-377-NDR-20110607
  var Imagen: TJPEGPlusImage; var DataImage: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;  overload;                                    // SS_1091_CQU_20131001 ///SS-377-NDR-20110607

function ObtenerImagenPatente(ImagePlatePath,Patente, NombreCorto: AnsiString; TipoImg: TTipoImagen;                                                                    // SS_1091_CQU_20131001
  var Imagen: TBitmap; var DataImage: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;                                            // SS_1091_CQU_20131001

function AlmacenarImagenPatente(ImagePlatePath, ImagePath, Patente, NombreCorto: AnsiString; NumCorrCO: Int64;                                                            ///SS-377-NDR-20110607
  TipoImg: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                                                                                  ///SS-377-NDR-20110607

function ObtenerImagenTransito(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;									//	SS_1147W_CQU_20141215
    var Imagen: TObject; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg; var EsKapsch : boolean): Boolean; overload;					//	SS_1147W_CQU_20141215

// Funciones para administrar imagenes de transitos
function ObtenerImagenTransito(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;                                   ///SS-377-NDR-20110607
    var Imagen: TBitmap; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;                                            ///SS-377-NDR-20110607

function ObtenerImagenTransito(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;                                   ///SS-377-NDR-20110607
    var Imagen: TJPEGPlusImage; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;                                     ///SS-377-NDR-20110607

// Funciones para administrar imagenes de transitos infractores (Infracciones)
function ObtenerImagenTransitoInfractor(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;                          // SS_1135_CQU_20131030
    var Imagen: TBitmap; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;                                            // SS_1135_CQU_20131030

function ObtenerImagenTransitoInfractor(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;                          // SS_1135_CQU_20131030
    var Imagen: TJPEGPlusImage; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;                                     // SS_1135_CQU_20131030

// Funciones para administrar imagenes de tr�nsitos pendientes
procedure ObtenerPathFileName(DirRaiz: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen; var Path: AnsiString; var NombreArchivo: AnsiString);
function ArmarPathTransitoPendiente(DirRaiz: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var NombreArchivo: AnsiString): Boolean;
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var JPG12: TJPEGPlusImage; var DataImage: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; Overload;
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var Imagen: TBitmap; var DatosImg: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; Overload;
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var Imagen: TObject; var DatosImg: TDataImage; var DescriError: AnsiString; var Error: TTipoErrorImg; var EsKapsch : boolean): Boolean; Overload; //	SS_1147V_CQU_20141219
function AlmacenarImagenTransitoPendiente(PathBase, ArchivoJPGOrigen: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;

// Funciones para administrar el traspaso de imagenes desde la estructura de transitos pendientes a transitos
function CopiarImagenTransitoPendiente(ImagePathSource, ImagePathDest, NombreCorto: AnsiString; NumCorrCO, NumCorrCA: Int64;                                              ///SS-377-NDR-20110607
  FechaTransito: TDateTime; TipoImg: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                                                        ///SS-377-NDR-20110607

function ArmarPathPadreTransitos(DirRaiz: AnsiString; NumCorrCA: Int64; Fecha: TDateTime; var ArchivoPadre: AnsiString): Boolean; forward;                                //SS-377-NDR-20110607
function ArmarPathPadrePatentes(DirRaiz: AnsiString; NombreCorto:AnsiString; Patente: AnsiString; var ArchivoPadre: AnsiString): Boolean; forward;                        //SS-377-NDR-20110607
function ArmarPathPadreTransitosNuevoFormato(DirRaiz, NombreCorto: AnsiString; NumCorrCA: Int64; Fecha: TDateTime; var ArchivoPadre: AnsiString): Boolean; forward;       //SS-377-NDR-20110607
function ArmarPathImagenTVM(PathRaiz, NombreImagen, NombreCorto : AnsiString; var PathRetorno : AnsiString) : Boolean;                                                    //PAR00133_COPAMB_ALA_20110727
function ArmarPathPadreTransitosInfractor(DirRaiz, NombreCorto: AnsiString; NumCorrCA: Int64; Fecha: TDateTime; var ArchivoPadre: AnsiString): Boolean; forward;          // SS_1135_CQU_20131030
procedure AjustarTama�oTImagePlus(var ImagenAmpliar : TImagePlus); // SS_1374A_CQU_20150924
implementation

type
	// Paths
	TItemPath = record
		Viajes: String;
	end;

	TPaths = record
		Transitos: Array[1..20] of TItemPath;
		Patentes: String;
        Pendientes: String;
	end;

Var
	TimeoutPath: Int64;
    FPathImagenes: TPaths;

// Declaracion de funciones locales
function CargarDatosPath(Conn: TADOConnection; var PathImagenes: TPaths): Boolean; forward;
//

//****************************************************************************//


function CargarDatosPath(Conn: TADOConnection; var PathImagenes: TPaths): Boolean;
Const
	TIMEOUT_PATH = 120000; // 2'
var
	Qry: TADOQuery;
	CritSect: TCriticalSection;
	PtoCobro, i: Integer;
begin
	// Cargar para cada Concesionaria/PuntoCobro los datos del Path.
	// S�lo esta funci�n usa la variable FPathImagenes.
	CritSect := TCriticalSection.Create;
	try
      if (GetMSCounter < TimeoutPath) then
      begin
        PathImagenes := FPathImagenes;
        Result := True;
  	  end
      else
      begin
        // Primero inicializamos la variable.
        FPathImagenes.Patentes := '';
        FPathImagenes.Pendientes := '';
        for i := 1 to 30 do                                                       //SS_1163_NDR_20140206
        begin
          FPathImagenes.transitos[i].Viajes := '';
        end;
        Qry := TAdoQuery.Create(nil);
        try
          try
            Qry.Connection := Conn;
            Qry.SQL.Text := 'SELECT * FROM PuntosCobro WITH (NOLOCK) ';
            Qry.Open;
            While not Qry.Eof do
            begin
              PtoCobro := Qry.FieldByName('NumeroPuntoCobro').AsInteger;
              PathImagenes.Transitos[PtoCobro].Viajes := Trim(Qry.FieldByName('PathFotosViajes').AsString);
              Qry.Next;
            end;

            //
            ObtenerParametroGeneral(Conn, 'Dir_Imagenes_Transitos_Pendientes', PathImagenes.Pendientes);
            ObtenerParametroGeneral(Conn, 'Dir_Imagenes_Patentes', PathImagenes.Patentes);

            // Actualizar la variable que usamos como "cache"
            FPathImagenes := PathImagenes;
            TimeoutPath   := GetMSCounter + TIMEOUT_PATH;
            Result := True;
          except
            Result := False;
          end;
        finally
		  Qry.Close;
		  Qry.Free;
		end;
      end;
	finally
	  CritSect.Free;
	end;
end;

function ArmarPathPadreTransitos(DirRaiz: AnsiString; NumCorrCA: int64; Fecha: TDateTime;
  var ArchivoPadre: AnsiString): Boolean;
var
	Nivel1, Nivel2: AnsiString;
begin
	try
		// Armamos el Path completo con la dir del directorio de la Concesionaria/PuntoCobro
		// y sgte nivel.
		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');
		DateTimeToString(Nivel1, 'yyyy-mm-dd', Fecha);
		Nivel2		  := IntToStr(NumCorrCA MOD 1000);
		ArchivoPadre  := format('%s\%s\%s.IMG', [DirRaiz, Nivel1, Nivel2]);
		//
		Result := True;
	except
		Result := False;
	end;
end;
{
function ArmarPathPadreTransitosNuevoFormato(DirRaiz, NombreCorto: AnsiString; NumCorrCA: int64; Fecha: TDateTime;        //SS-377-NDR-20110607
  var ArchivoPadre: AnsiString): Boolean;                                                                                 //SS-377-NDR-20110607
var                                                                                                                       //SS-377-NDR-20110607
	Nivel0, Nivel1, Nivel2, Nivel3: AnsiString;                                                                           //SS-377-NDR-20110607
begin                                                                                                                     //SS-377-NDR-20110607
    try                                                                                                                   //SS-377-NDR-20110607
        // Armamos el Path completo con la dir del directorio de la Concesionaria/PuntoCobro                              //SS-377-NDR-20110607
		// y sgte nivel.                                                                                                  //SS-377-NDR-20110607
		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');                                         //SS-377-NDR-20110607
		DateTimeToString(Nivel0, 'mm', Fecha);                                                                            //SS-377-NDR-20110607
		DateTimeToString(Nivel1, 'yyyy-mm-dd', Fecha);                                                                    //SS-377-NDR-20110607
		Nivel2		  := Trim(NombreCorto);                                                                               //SS-377-NDR-20110607
		Nivel3		  := IntToStr(NumCorrCA MOD 1000);                                                                    //SS-377-NDR-20110607
        // Si se ha iniciado la Migraci�n devuelve s�lo la ruta, sino la ruta completa del Archivo Padre.                 //SS-377-NDR-20110607
        ArchivoPadre := format('%s%s\%s\%s\%s\', [DirRaiz, Nivel0, Nivel1, Nivel2, Nivel3]);                              //SS-377-NDR-20110607
		//                                                                                                                //SS-377-NDR-20110607
		Result := True;                                                                                                   //SS-377-NDR-20110607
	except                                                                                                                //SS-377-NDR-20110607
		Result := False;                                                                                                  //SS-377-NDR-20110607
	end;                                                                                                                  //SS-377-NDR-20110607
end;
}

function ArmarPathPadreTransitosNuevoFormato(DirRaiz, NombreCorto: AnsiString; NumCorrCA: int64; Fecha: TDateTime;        //SS-377-NDR-20110607
  var ArchivoPadre: AnsiString): Boolean;                                                                                 //SS-377-NDR-20110607
var                                                                                                                       //SS-377-NDR-20110607
	Nivel0, Nivel1, Nivel2, Nivel3: AnsiString;                                                                           //SS-377-NDR-20110607
    DirectorioImagenes: string;
const
    DIR_IMAGENES_TRANSITOS = 'DIR_IMAGENES_TRANSITOS_%s';
begin                                                                                                                     //SS-377-NDR-20110607
    try                                                                                                                   //SS-377-NDR-20110607
        Result := False;

        if not ObtenerParametroGeneral(
                  DMConnections.BaseCAC,
                  Format(DIR_IMAGENES_TRANSITOS,[FormatDateTime('MM',Fecha)]),
                  DirectorioImagenes) then Exit;

        // Armamos el Path completo con la dir del directorio de la Concesionaria/PuntoCobro                              //SS-377-NDR-20110607
		// y sgte nivel.                                                                                                  //SS-377-NDR-20110607
//		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');                                         //SS-377-NDR-20110607
//		DateTimeToString(Nivel0, 'mm', Fecha);                                                                            //SS-377-NDR-20110607
		DateTimeToString(Nivel1, 'yyyy-mm-dd', Fecha);                                                                    //SS-377-NDR-20110607
		Nivel2		  := Trim(NombreCorto);                                                                               //SS-377-NDR-20110607
		Nivel3		  := IntToStr(NumCorrCA MOD 1000);                                                                    //SS-377-NDR-20110607
        // Si se ha iniciado la Migraci�n devuelve s�lo la ruta, sino la ruta completa del Archivo Padre.                 //SS-377-NDR-20110607
//        ArchivoPadre := format('%s%s\%s\%s\%s\', [DirRaiz, Nivel0, Nivel1, Nivel2, Nivel3]);                            //SS-377-NDR-20110607
        ArchivoPadre := format('%s\%s\%s\%s\', [DirectorioImagenes, Nivel1, Nivel2, Nivel3]);
		//                                                                                                                //SS-377-NDR-20110607
		Result := True;                                                                                                   //SS-377-NDR-20110607
	except                                                                                                                //SS-377-NDR-20110607
		Result := False;                                                                                                  //SS-377-NDR-20110607
	end;                                                                                                                  //SS-377-NDR-20110607
end;                                                                                                                      //SS-377-NDR-20110607


function ArmarPathPadrePatentes(DirRaiz: AnsiString; NombreCorto:AnsiString; Patente: AnsiString; var ArchivoPadre: AnsiString): Boolean;

  procedure ObtenerNumerosPatente(Patente: AnsiString; var NumerosPatente: LongInt);
  var
      i: Integer;
      numPatente: AnsiString;
  begin
      // Saca los n�mero de la patente, por orden de aparici�n (izq. a der.)
      numPatente := '';
      for i := 1 to Length(Patente) do begin
          if (Ord(Patente[i]) >= Ord('0')) and (Ord(Patente[i]) <= Ord('9')) then
            numPatente := numPatente + Patente[i];
      end;
      NumerosPatente := IVal(numPatente);
  end;

var
	NumeroPat: Integer;
begin
	try
		// Armamos el Path completo
		if length(DirRaiz) = 0 then raise Exception.Create('Faltan Datos Path.');
		ObtenerNumerosPatente(Patente, NumeroPat);
		ArchivoPadre := GoodDir(DirRaiz) + Trim(NombreCorto) + '\' + IntToStr(NumeroPat MOD 100) + '\' + IntToStr(NumeroPat MOD 1000);     //SS-377-NDR-20110607
		//
		Result := True;
	except
		Result := False;
	end;
end;



function CargarImagenPendientes(HFile: Integer; var JPG12: TJpegPlusImage; var DataImage: TDataImage;
  var DescriError: AnsiString): Boolean; overload;                  //SS_1163_NDR_20140206
var
	Buffer: PChar;
	ArchiAux: AnsiString;
	H, Tamanio: Integer;
begin
	// Si tenemos la seguridad que las fotos son J12, directamente podemos cargar
	// un TPicture. Pero en las demos, seguramente tenemos jpegs, entonces, probamos.
	Result := False;
    try
        // Generamos un archivo temporal
        ArchiAux := GetTempDir + TempFile + '.j12';
        H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);
        if H = -1 then raise exception.Create('Error al abrir archivo.');
        FileSeek(H, 0, 0);
        Tamanio := FileSeek(HFile, 0, 2);
        FileSeek(HFile, 0, 0);
        Buffer  := AllocMem(Tamanio);
        FileRead(HFile, Buffer^, Tamanio);
        FileWrite(H, Buffer^, Tamanio);
        FileClose(H);
        FreeMem(Buffer);
        // Obtenemos la imagen
        if not ObtenerImagen(ArchiAux, JPG12, DataImage, DescriError) then begin
            DeleteFile(ArchiAux);
            result := false;
        end else begin
            Result := True;
        end;
    except
        on e: exception do begin
            DescriError := e.Message;
        end;
    end;
end;


//BEGIN : SS_1163_NDR_20140206 ---------------------------------------------------------------------
function CargarImagenPendientes(HFile: Integer; var Imagen: TBitMap; var DataImage: TDataImage;
  var DescriError: AnsiString): Boolean; overload;
var
	Buffer: PChar;
	ArchiAux: AnsiString;
	H, Tamanio: Integer;
begin
	// Si tenemos la seguridad que las fotos son J12, directamente podemos cargar
	// un TPicture. Pero en las demos, seguramente tenemos jpegs, entonces, probamos.
	Result := False;
    try
        // Generamos un archivo temporal
        ArchiAux := GetTempDir + TempFile + '.j12';
        H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);
        if H = -1 then raise exception.Create('Error al abrir archivo.');
        FileSeek(H, 0, 0);
        Tamanio := FileSeek(HFile, 0, 2);
        FileSeek(HFile, 0, 0);
        Buffer  := AllocMem(Tamanio);
        FileRead(HFile, Buffer^, Tamanio);
        FileWrite(H, Buffer^, Tamanio);
        FileClose(H);
        FreeMem(Buffer);
        // Obtenemos la imagen
        if not ObtenerImagen(ArchiAux, Imagen, DataImage, DescriError) then begin
            DeleteFile(ArchiAux);
            result := false;
        end else begin
            Result := True;
        end;
    except
        on e: exception do begin
            DescriError := e.Message;
        end;
    end;
end;
//END : SS_1163_NDR_20140206 ---------------------------------------------------------------------


function CargarOverViewPendientes(HFile: Integer; var Imagen: TBitMap; var DataImage: TDataImage;
  var DescriError: AnsiString): Boolean;
var
	Buffer: PChar;
	ArchiAux: AnsiString;
	H, BytesLeer, Tamanio: Integer;
    jpegAux : TJPegImage;
begin
	jpegAux := TjpegImage.Create;
	Result := False;
	try
		try
			ArchiAux := GetTempDir + TempFile + '.j12';
			H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);
			if H = -1 then raise exception.Create('Error al abrir archivo.');
			FileSeek(H, 0, 0);
			Tamanio := FileSeek(HFile, 0, 2);
			FileSeek(HFile, 0, 0);
			BytesLeer := Tamanio - BYTES_EXTRAS_OVERVIEW;
			Buffer    := AllocMem(BytesLeer);
			FileRead(HFile, Buffer^, BytesLeer);
			FileWrite(H, Buffer^, BytesLeer);
			FileClose(H);
			// Retornar Imagen
			try
				jpegAux.LoadFromFile(ArchiAux);
                Imagen.Assign(jpegAux);
			except
			end;
			FreeMem(Buffer);
			DeleteFile(ArchiAux);
			// Cargar los datos extras
			FileSeek(HFile, Tamanio - BYTES_EXTRAS_OVERVIEW, 0);
			FileRead(HFile, DataImage.DataOverView, BYTES_EXTRAS_OVERVIEW);
			//
			Result := True;
		except
			on e: exception do begin
				DescriError := e.Message;
				Result := False;
			end;
		end;
	finally
		jpegAux.Free;
	end;
end;





function CopiarImagenTransitoPendiente(ImagePathSource, ImagePathDest, NombreCorto: AnsiString; NumCorrCO, NumCorrCA: Int64;     //SS-377-NDR-20110607
  FechaTransito: TDateTime; TipoImg: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;               //SS-377-NDR-20110607

 (*   Function ImagenEsJPEG(archivo: AnsiString): Boolean;
    begin
        result := true;
        // Cargamos la imagen en Buffer desde el Archivo IMG
        if DirItems[i*7 + j].PosicionInicial = 0 then begin
            inc(j);
            continue;
        end;
        FileSeek(HFilePadre, DirItems[i*7 + j].PosicionInicial, 0);
        Buffer := AllocMem(DirItems[i*7 + j].BytesDatos + DirItems[i*7 + j].BytesEstructuraAddicional);
        FileRead(Archivo, Buffer^, DirItems[i*7 + j].BytesDatos + DirItems[i*7 + j].BytesEstructuraAddicional);
        Tamanio := FileRead(HFilePadre, Buffer^, DirItems[i*7 + j].BytesDatos);
    end;
    *)

var
	ArchivoTransito, Path, ArchivoPadre: AnsiString;
begin
	// La imagen original est� en pendientes.
	// La imagen debe pasar a la estructura de almacenamiento final (agrupadas en un mismo archivo)
	Result := False;
    try
        // Path del Archivo a Copiar
        ObtenerPathFileName(ImagePathSource, NumCorrCO, TipoImg, Path, ArchivoTransito);
        // Verificamos si el archivo fuente existe
        if not FileExists(ArchivoTransito) then begin
            Error := teImagenNoExiste;
            raise exception.Create('No se encuentra la Imagen del tr�nsito.');
        end;
        if GetFileSize(ArchivoTransito) <= 0 then begin        // SS_1288_CQU_20150601
            DescriError := 'Tama�o del archivo incorrecto';    // SS_1288_CQU_20150601
            Error := teLeerArchivo;                            // SS_1288_CQU_20150601
            raise exception.Create(DescriError);               // SS_1288_CQU_20150601
        end;                                                   // SS_1288_CQU_20150601
        //Rev.4 / 07-Junio-2011 / Nelson Droguett Sierra----------------------------------------------------------------------------
        // Path del Archivo Padre.                                                                                                   //SS-377-NDR-20110607
        //if not ArmarPathPadreTransitos(ImagePathDest, NumCorrCA, FechaTransito, ArchivoPadre) then begin                           //SS-377-NDR-20110607
        //    Error := teObtenerPath;                                                                                                //SS-377-NDR-20110607
        //    raise exception.Create('No se pudo obtener el Path del Archivo Destino.');                                             //SS-377-NDR-20110607
        //end;                                                                                                                       //SS-377-NDR-20110607
        if not ArmarPathPadreTransitosNuevoFormato(ImagePathDest, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin    //SS-377-NDR-20110607
            Error := teObtenerPath;                                                                                                  //SS-377-NDR-20110607
            raise exception.Create('No se pudo obtener el Path del Archivo Destino.');                                               //SS-377-NDR-20110607
        end;                                                                                                                         //SS-377-NDR-20110607
                                                                                                                                     //SS-377-NDR-20110607
        // Almacenamos la imagen en el Archivo IMG                                                                                   //SS-377-NDR-20110607
        //if not AlmacenarImagenIMGTransito(ArchivoTransito, ArchivoPadre, NumCorrCA, TipoImg, DescriError) then begin               //SS-377-NDR-20110607
        //    Error := teGrabarArchivo;                                                                                              //SS-377-NDR-20110607
        //    raise exception.Create('No se pudo crear la imagen del tr�nsito.' + DescriError);                                      //SS-377-NDR-20110607
        //end;                                                                                                                       //SS-377-NDR-20110607
        if not AlmacenarImagenIMGTransitoNuevoFormato(ArchivoTransito, ArchivoPadre, NumCorrCA, TipoImg, DescriError) then begin     //SS-377-NDR-20110607
             Error := teGrabarArchivo;                                                                                               //SS-377-NDR-20110607
             raise exception.Create('No se pudo crear la imagen del tr�nsito.' + DescriError);                                       //SS-377-NDR-20110607
        end;                                                                                                                         //SS-377-NDR-20110607
        // Listo                                                                                                                     //SS-377-NDR-20110607
        //FinRev.4-------------------------------------------------------------------------------------------------------------------
        Result := True;
    except
        on e: Exception do begin
            DescriError := 'Copiar Imagen: ' + e.Message;
        end;
    end;
end;

function ObtenerImagenTransito(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;                 //SS-377-NDR-20110607
    var Imagen: TJPEGPlusImage; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;                   //SS-377-NDR-20110607
var                                                                                                                                                     //SS-377-NDR-20110607
	ArchivoPadre: AnsiString;                                                                                                                           //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
    function ObtenerDeFormatoNuevo: Boolean;                                                                                                            //SS-377-NDR-20110607
    begin                                                                                                                                               //SS-377-NDR-20110607
        Result := False;                                                                                                                                //SS-377-NDR-20110607
        // Path del Archivo Padre.                                                                                                                      //SS-377-NDR-20110607
        if not ArmarPathPadreTransitosNuevoFormato(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin                        //SS-377-NDR-20110607
            DescriError := DescripcionError[teObtenerPath];                                                                                             //SS-377-NDR-20110607
            Error := teObtenerPath;                                                                                                                     //SS-377-NDR-20110607
            Exit;                                                                                                                                       //SS-377-NDR-20110607
        end;                                                                                                                                            //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        // Para salvar el problema de los seg en la transf de imagenes, buscamos en el dia del transito y en el dia anterior                            //SS-377-NDR-20110607
        if (Hour(FechaTransito) = 0) and (Minute(FechaTransito) = 0) then begin                                                                         //SS-377-NDR-20110607
            // Caso a salvar                                                                                                                            //SS-377-NDR-20110607
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                               //SS-377-NDR-20110607
              Imagen, DataImage, DescriError, Error) then begin                                                                                         //SS-377-NDR-20110607
                  // Si la imagen se se encontro pero no se pudo cargar por algun error nos vamos                                                       //SS-377-NDR-20110607
                  if Error in [teParametrosMal, teCargarImagen, teFormatoImagen] then exit;                                                             //SS-377-NDR-20110607
                  FechaTransito := incDay(FechaTransito, -1);                                                                                           //SS-377-NDR-20110607
                  // Recalculo el archivo, pero con la fecha anterior al transito                                                                       //SS-377-NDR-20110607
                  if not ArmarPathPadreTransitosNuevoFormato(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin              //SS-377-NDR-20110607
                      DescriError := DescripcionError[teObtenerPath];                                                                                   //SS-377-NDR-20110607
                      Error := teObtenerPath;                                                                                                           //SS-377-NDR-20110607
                      Exit;                                                                                                                             //SS-377-NDR-20110607
                  end;                                                                                                                                  //SS-377-NDR-20110607
                  if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                         //SS-377-NDR-20110607
                    Imagen, DataImage, DescriError, error) then begin                                                                                   //SS-377-NDR-20110607
                      Exit;                                                                                                                             //SS-377-NDR-20110607
                  end;                                                                                                                                  //SS-377-NDR-20110607
            end;                                                                                                                                        //SS-377-NDR-20110607
        end else begin                                                                                                                                  //SS-377-NDR-20110607
            // Caso Normal                                                                                                                              //SS-377-NDR-20110607
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                               //SS-377-NDR-20110607
              Imagen, DataImage, DescriError, error) then Exit;                                                                                         //SS-377-NDR-20110607
            end;                                                                                                                                        //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        Result := True;                                                                                                                                 //SS-377-NDR-20110607
    end;                                                                                                                                                //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
begin                                                                                                                                                   //SS-377-NDR-20110607
    Result := False;                                                                                                                                    //SS-377-NDR-20110607
    DescriError := '';                                                                                                                                  //SS-377-NDR-20110607
    Error := teSinError;                                                                                                                                //SS-377-NDR-20110607
    try                                                                                                                                                 //SS-377-NDR-20110607
        // Verificar Par�metro                                                                                                                         //SS-377-NDR-20110607
        if not Assigned(Imagen) then begin                                                                                                              //SS-377-NDR-20110607
            Error := teParametrosMal;                                                                                                                   //SS-377-NDR-20110607
            DescriError := DescripcionError[teParametrosMal];                                                                                           //SS-377-NDR-20110607
            Exit;                                                                                                                                       //SS-377-NDR-20110607
        end;                                                                                                                                            //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        if not ObtenerDeFormatoNuevo then Exit;                                                                                                         //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        Result := True;                                                                                                                                 //SS-377-NDR-20110607
    except                                                                                                                                              //SS-377-NDR-20110607
        on e:Exception do begin                                                                                                                         //SS-377-NDR-20110607
            Result := False;                                                                                                                            //SS-377-NDR-20110607
            DescriError := e.Message;                                                                                                                   //SS-377-NDR-20110607
            Error       := teCargarImagen;                                                                                                              //SS-377-NDR-20110607
        end;                                                                                                                                            //SS-377-NDR-20110607
    end;                                                                                                                                                //SS-377-NDR-20110607
end;                                                                                                                                                    //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
function ObtenerImagenTransito(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;                 //SS-377-NDR-20110607
    var Imagen: TBitmap; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;                          //SS-377-NDR-20110607
var                                                                                                                                                     //SS-377-NDR-20110607
	ArchivoPadre: AnsiString;                                                                                                                           //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
    function ObtenerDeFormatoNuevo: Boolean;                                                                                                            //SS-377-NDR-20110607
    begin                                                                                                                                               //SS-377-NDR-20110607
        Result := False;                                                                                                                                //SS-377-NDR-20110607
        // Path del Archivo Padre.                                                                                                                      //SS-377-NDR-20110607
        if not ArmarPathPadreTransitosNuevoFormato(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin                        //SS-377-NDR-20110607
            DescriError := DescripcionError[teObtenerPath];                                                                                             //SS-377-NDR-20110607
            Error := teObtenerPath;                                                                                                                     //SS-377-NDR-20110607
            Exit;                                                                                                                                       //SS-377-NDR-20110607
        end;                                                                                                                                            //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        // Para salvar el problema de los seg en la transf de imagenes, buscamos en el dia del transito y en el dia anterior                            //SS-377-NDR-20110607
        if (Hour(FechaTransito) = 0) and (Minute(FechaTransito) = 0) then begin                                                                         //SS-377-NDR-20110607
            // Caso a salvar                                                                                                                            //SS-377-NDR-20110607
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                               //SS-377-NDR-20110607
              Imagen, DataImage, DescriError, Error) then begin                                                                                         //SS-377-NDR-20110607
                  // Si la imagen se se encontro pero no se pudo cargar por algun error nos vamos                                                       //SS-377-NDR-20110607
                  if Error in [teParametrosMal, teCargarImagen, teFormatoImagen] then exit;                                                             //SS-377-NDR-20110607
                  FechaTransito := incDay(FechaTransito, -1);                                                                                           //SS-377-NDR-20110607
                  // Recalculo el archivo, pero con la fecha anterior al transito                                                                       //SS-377-NDR-20110607
                  if not ArmarPathPadreTransitosNuevoFormato(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin              //SS-377-NDR-20110607
                      DescriError := DescripcionError[teObtenerPath];                                                                                   //SS-377-NDR-20110607
                      Error := teObtenerPath;                                                                                                           //SS-377-NDR-20110607
                      Exit;                                                                                                                             //SS-377-NDR-20110607
                  end;                                                                                                                                  //SS-377-NDR-20110607
                  if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                         //SS-377-NDR-20110607
                    Imagen, DataImage, DescriError, error) then begin                                                                                   //SS-377-NDR-20110607
                      Exit;                                                                                                                             //SS-377-NDR-20110607
                  end;                                                                                                                                  //SS-377-NDR-20110607
            end;                                                                                                                                        //SS-377-NDR-20110607
        end else begin                                                                                                                                  //SS-377-NDR-20110607
            // Caso Normal                                                                                                                              //SS-377-NDR-20110607
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                               //SS-377-NDR-20110607
              Imagen, DataImage, DescriError, error) then Exit;                                                                                         //SS-377-NDR-20110607
            end;                                                                                                                                        //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        Result := True;                                                                                                                                 //SS-377-NDR-20110607
    end;                                                                                                                                                //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
begin                                                                                                                                                   //SS-377-NDR-20110607
    Result := False;                                                                                                                                    //SS-377-NDR-20110607
    DescriError := '';                                                                                                                                  //SS-377-NDR-20110607
    Error := teSinError;                                                                                                                                //SS-377-NDR-20110607
    try                                                                                                                                                 //SS-377-NDR-20110607
        // Verificar Par�metro                                                                                                                         //SS-377-NDR-20110607
        if not Assigned(Imagen) then begin                                                                                                              //SS-377-NDR-20110607
            Error := teParametrosMal;                                                                                                                   //SS-377-NDR-20110607
            DescriError := DescripcionError[teParametrosMal];                                                                                           //SS-377-NDR-20110607
            Exit;                                                                                                                                       //SS-377-NDR-20110607
        end;                                                                                                                                            //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        if not ObtenerDeFormatoNuevo then Exit;                                                                                                         //SS-377-NDR-20110607
                                                                                                                                                        //SS-377-NDR-20110607
        Result := True;                                                                                                                                 //SS-377-NDR-20110607
    except                                                                                                                                              //SS-377-NDR-20110607
        on e:Exception do begin                                                                                                                         //SS-377-NDR-20110607
            Result := False;                                                                                                                            //SS-377-NDR-20110607
            DescriError := e.Message;                                                                                                                   //SS-377-NDR-20110607
            Error       := teCargarImagen;                                                                                                              //SS-377-NDR-20110607
        end;                                                                                                                                            //SS-377-NDR-20110607
    end;                                                                                                                                                //SS-377-NDR-20110607
end;                                                                                                                                                    //SS-377-NDR-20110607

{-----------------------------------------------------------------------------
  Function Name	: ObtenerImagenTransito
  Author		: Claudio Quezada Ib��ez
  Date Created	: 15/12/2014
  Firma         : SS_1147W_CQU_20141215
  Description	: Carga la imagen del tr�nsito seg�n el tipo (JPeg12 o BMP)
				  Mediante una variable indica si es Kapsch o no
  Parameters	: ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime;
				  TipoImg: TTipoImagen; var Imagen: TObject; var DataImage: TDataIMage;
				  var DescriError: AnsiString; var Error: TTipoErrorImg; var EsKapsch : boolean
  Return Value	: Boolean
-----------------------------------------------------------------------------}
function ObtenerImagenTransito(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;					//	SS_1147W_CQU_20141215
		var Imagen: TObject; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg;											//	SS_1147W_CQU_20141215
		var EsKapsch : boolean): Boolean; overload;																										//	SS_1147W_CQU_20141215
var																																						//	SS_1147W_CQU_20141215
    Resultado       : Boolean;																															//	SS_1147W_CQU_20141215
    ImagenNoKapsch  : TBitmap;																															//	SS_1147W_CQU_20141215
    ImagenKapsch    : TJPEGPlusImage;																													//	SS_1147W_CQU_20141215
begin																																					//	SS_1147W_CQU_20141215
    try																																					//	SS_1147W_CQU_20141215
        try																																				//	SS_1147W_CQU_20141215
            ImagenNoKapsch  := TBitmap.Create;																											//	SS_1147W_CQU_20141215
            ImagenKapsch    := TJPEGPlusImage.Create;																									//	SS_1147W_CQU_20141215
            if ObtenerImagenTransito(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, TipoImg,														//	SS_1147W_CQU_20141215
									ImagenKapsch, DataImage, DescriError, Error) then																	//	SS_1147W_CQU_20141215
            begin																																		//	SS_1147W_CQU_20141215
                Imagen := (ImagenKapsch as TObject);																									//	SS_1147W_CQU_20141215
                EsKapsch := True;																														//	SS_1147W_CQU_20141215
                Resultado := True;																														//	SS_1147W_CQU_20141215
            end else if ObtenerImagenTransito(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, TipoImg,												//	SS_1147W_CQU_20141215
									ImagenNoKapsch, DataImage, DescriError, Error) then																	//	SS_1147W_CQU_20141215
            begin																																		//	SS_1147W_CQU_20141215
                Imagen := (ImagenNoKapsch as TObject);																									//	SS_1147W_CQU_20141215
                EsKapsch := False;																														//	SS_1147W_CQU_20141215
                Resultado := True;																														//	SS_1147W_CQU_20141215
            end else																																	//	SS_1147W_CQU_20141215
                Resultado := False;																														//	SS_1147W_CQU_20141215
        except																																			//	SS_1147W_CQU_20141215
            on e:Exception do begin																														//	SS_1147W_CQU_20141215
                Resultado := False;																														//	SS_1147W_CQU_20141215
                DescriError := e.Message;																												//	SS_1147W_CQU_20141215
                Error       := teCargarImagen;																											//	SS_1147W_CQU_20141215
            end;																																		//	SS_1147W_CQU_20141215
        end;																																			//	SS_1147W_CQU_20141215
    finally																																				//	SS_1147W_CQU_20141215
        Result := Resultado;																															//	SS_1147W_CQU_20141215
    end;																																				//	SS_1147W_CQU_20141215
end;																																					//	SS_1147W_CQU_20141215

function AlmacenarImagenPatente(ImagePlatePath, ImagePath, Patente, NombreCorto: AnsiString;                         //SS-377-NDR-20110607
  NumCorrCO: Int64; TipoImg: TTipoImagen;                                                                            //SS-377-NDR-20110607
  var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;                                                   //SS-377-NDR-20110607

var
	ArchivoTransito, ArchivoPadre: AnsiString;
begin
	// La imagen original est� en pendientes y debe pasar a la estructura de almacenamiento de Referencia.
	Result := False;
    try
        // Validaciones
        if (length(Patente) = 0) then raise exception.Create('Patente Nula.');
        // Path del Archivo de Pendientes
        if not ArmarPathTransitoPendiente(ImagePath, NumCorrCO, TipoImg, ArchivoTransito) then begin
            Error := teObtenerPath;
            raise exception.Create('No se pudo obtener el Path de la imagen Origen.');
        end;
        //
        if not FileExists(ArchivoTransito) then begin
            Error := teImagenNoExiste;
            raise exception.Create('No se encuentra la Imagen del tr�nsito.');
        end;
        if GetFileSize(ArchivoTransito) <= 0 then begin                                                                         // SS_1288_CQU_20150601
            DescriError := 'Tama�o del archivo incorrecto';                                                                     // SS_1288_CQU_20150601
            Error := teLeerArchivo;                                                                                             // SS_1288_CQU_20150601
            raise exception.Create(DescriError);                                                                                // SS_1288_CQU_20150601
        end;                                                                                                                    // SS_1288_CQU_20150601
        // Path del Archivo Padre.
        if not ArmarPathPadrePatentes(ImagePlatePath,NombreCorto, Patente, ArchivoPadre) then begin                              //SS-377-NDR-20110607
            Error := teObtenerPath;                                                                                              //SS-377-NDR-20110607
            raise exception.Create('No se pudo obtener el Path del archivo Destino.');                                           //SS-377-NDR-20110607
        end;                                                                                                                     //SS-377-NDR-20110607
        //if not AlmacenarImagenIMGPatente(ArchivoTransito, ArchivoPadre, Patente, TipoImg,                                      //SS-377-NDR-20110607
        //  DescriError) then begin                                                                                              //SS-377-NDR-20110607
        //    Error := teGrabarArchivo;                                                                                          //SS-377-NDR-20110607
        //    raise exception.Create(DescriError);                                                                               //SS-377-NDR-20110607
        //end;                                                                                                                   //SS-377-NDR-20110607
        if not AlmacenarImagenIMGPatenteNuevoFormato(ArchivoTransito, ArchivoPadre, Patente, TipoImg, DescriError) then begin    //SS-377-NDR-20110607
            Error := teGrabarArchivo;                                                                                            //SS-377-NDR-20110607
            raise exception.Create(DescriError);                                                                                 //SS-377-NDR-20110607
        end;                                                                                                                     //SS-377-NDR-20110607

        // Listo
        Result := True;
    except
        on e: Exception do begin
            DescriError := 'Copiar Imagen: ' + e.Message;
        end;
    end;
end;


function ObtenerImagenPatente(ImagePlatePath, Patente, NombreCorto: AnsiString; TipoImg: TTipoImagen;                                     //SS-377-NDR-20110607
  var Imagen: TJPEGPLusIMage; var DataIMage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;       //SS_1091_CQU_20131001    //SS-377-NDR-20110607
ResourceString                                                                                                                            //SS-377-NDR-20110607
   MSG_ERROR_PATH_NEW = 'No se pudo obtener el Path del Archivo.';                                                                        //SS-377-NDR-20110607
var
	ArchivoPadre: AnsiString;
begin
    Result := False;
    try
        if not ArmarPathPadrePatentes(ImagePlatePath, NombreCorto, Patente, ArchivoPadre) then begin                                      //SS-377-NDR-20110607
            DescriError := MSG_ERROR_PATH_NEW;                                                                                            //SS-377-NDR-20110607
            Error := teObtenerPath;
            Exit;
        end;
        // Obtenemos la imagen desde la estructura
        //if not ObtenerImagenIMGPatente(ArchivoPadre, Patente, TipoImg, Imagen, DataIMage, DescriError, error) then begin               //SS-377-NDR-20110607
        if not ObtenerImagenIMGPatenteNuevoFormato(ArchivoPadre, Patente, TipoImg, Imagen, DataIMage, DescriError, error) then begin     //SS-377-NDR-20110607
            Error := teCargarImagen;
            Exit;
        end;
        //Result := True;                       // SS_1091_CQU_20131001
        if not (DescriError <> EmptyStr) then   // SS_1091_CQU_20131001
            Result := True;                     // SS_1091_CQU_20131001
    except
        on e:Exception do begin
            Result := False;
            DescriError := 'Recuperar Imagen: ' + e.Message;
            Error := teCargarImagen;
        end;
    end;
end;

{---------------------------------------------------------------------------------                                              // SS_1091_CQU_20131001
Function Name   : ObtenerImagenPatente : Overload                                                                               // SS_1091_CQU_20131001
Author          : CQuezadaI                                                                                                     // SS_1091_CQU_20131001
Date Created    : 01-10-2013                                                                                                    // SS_1091_CQU_20131001
Firma           : SS_1091_CQU_20131001                                                                                          // SS_1091_CQU_20131001
Description     : Sobrecarga para recibir un TBitmap y poder obtener imagenes italianas                                         // SS_1091_CQU_20131001
----------------------------------------------------------------------------------}                                             // SS_1091_CQU_20131001
function ObtenerImagenPatente(ImagePlatePath, Patente, NombreCorto: AnsiString; TipoImg: TTipoImagen;                           // SS_1091_CQU_20131001
  var Imagen: TBitmap; var DataIMage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;    // SS_1091_CQU_20131001
ResourceString                                                                                                                  // SS_1091_CQU_20131001
   MSG_ERROR_PATH_NEW = 'No se pudo obtener el Path del Archivo.';                                                              // SS_1091_CQU_20131001
var                                                                                                                             // SS_1091_CQU_20131001
	ArchivoPadre: AnsiString;                                                                                                   // SS_1091_CQU_20131001
begin                                                                                                                           // SS_1091_CQU_20131001
    Result := False;                                                                                                            // SS_1091_CQU_20131001
    try                                                                                                                         // SS_1091_CQU_20131001
        if not ArmarPathPadrePatentes(ImagePlatePath, NombreCorto, Patente, ArchivoPadre) then begin                            // SS_1091_CQU_20131001
            DescriError := MSG_ERROR_PATH_NEW;                                                                                  // SS_1091_CQU_20131001
            Error := teObtenerPath;                                                                                             // SS_1091_CQU_20131001
            Exit;                                                                                                               // SS_1091_CQU_20131001
        end;                                                                                                                    // SS_1091_CQU_20131001
        // Obtenemos la imagen desde la estructura                                                                              // SS_1091_CQU_20131001
        if not ObtenerImagenIMGPatenteNuevoFormato(ArchivoPadre, Patente, TipoImg, Imagen                                       // SS_1091_CQU_20131001
            ,DataImage, DescriError, error) then                                                                                // SS_1091_CQU_20131001
        begin                                                                                                                   // SS_1091_CQU_20131001
            Error := teCargarImagen;                                                                                            // SS_1091_CQU_20131001
            Exit;                                                                                                               // SS_1091_CQU_20131001
        end;                                                                                                                    // SS_1091_CQU_20131001
        if not (DescriError <> EmptyStr) then                                                                                   // SS_1091_CQU_20131001
            Result := True;                                                                                                     // SS_1091_CQU_20131001
    except                                                                                                                      // SS_1091_CQU_20131001
        on e:Exception do begin                                                                                                 // SS_1091_CQU_20131001
            Result := False;                                                                                                    // SS_1091_CQU_20131001
            DescriError := 'Recuperar Imagen: ' + e.Message;                                                                    // SS_1091_CQU_20131001
            Error := teCargarImagen;                                                                                            // SS_1091_CQU_20131001
        end;                                                                                                                    // SS_1091_CQU_20131001
    end;                                                                                                                        // SS_1091_CQU_20131001
end;                                                                                                                            // SS_1091_CQU_20131001

//***** Lectura - Tr�nsitosPendientes ****************************************//
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString;
  NumCorrCO: Int64; TipoImagen: TTipoImagen;
  var JPG12: TJPEGPlusImage; var DataImage: TDataImage; var DescriError: AnsiString;
  var Error: TTipoErrorImg): Boolean;
const                                                                                          // SS_1416_CQU_20151123
    MSG_DATOS_TRANSITO  =   'Datos del Tr�nsito, NumCorrCA: %d' + CRLF;                        // SS_1416_CQU_20151123
var
	HFilePend: Integer;
    NomArchivo: AnsiString;
begin
	Result := False;
	// Par�metros Ok?
	if not Assigned(JPG12) then begin
		DescriError := 'El par�metro Imagen (TBitmap) no puede ser nil';
        DescriError := DescriError + CRLF + Format(MSG_DATOS_TRANSITO, [NumCorrCO]);           // SS_1416_CQU_20151123
		Exit;
	end;
	if not ArmarPathTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, NomArchivo) then begin
		DescriError := 'No se pudo obtener el Path de la imagen';
        DescriError := DescriError + CRLF + Format(MSG_DATOS_TRANSITO, [NumCorrCO]);           // SS_1416_CQU_20151123
		Error := teObtenerPath;
		Exit;
	end;
	// La imagen existe?
	if not FileExists(NomArchivo) then begin
		DescriError := 'La imagen no existe';
        DescriError := DescriError + CRLF + Format(MSG_DATOS_TRANSITO, [NumCorrCO]);           // SS_1416_CQU_20151123
		Error := teImagenNoExiste;
		Exit;
	end;

    if GetFileSize(NomArchivo) <= 0 then begin             // SS_1261_CQU_20150506
		DescriError := 'Tama�o del archivo incorrecto';    // SS_1261_CQU_20150506
		Error := teLeerArchivo;                            // SS_1261_CQU_20150506
		Exit;                                              // SS_1261_CQU_20150506
    end;                                                   // SS_1261_CQU_20150506

	HFilePend := -1;
	try
		HFilePend := FileOpenRetry(NomArchivo, fmOpenRead or fmShareDenyWrite);
		if HFilePend = -1 then
		  raise exception.Create('Error al abrir archivo');
		if not CargarImagenPendientes(HFilePend, JPG12, DataImage, DescriError) then
		  raise exception.Create(DescriError);
		Result := True;
	except
	end;
	if HFilePend > 0 then FileClose(HFilePend);
end;

function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen;
  var Imagen: TBitmap; var DatosImg: TDataImage;
  var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
const                                                                                   // SS_1416_CQU_20151123
    MSG_DATOS_TRANSITO  =   'Datos del Tr�nsito, NumCorrCA: %d' + CRLF;                 // SS_1416_CQU_20151123
var
	HFilePend: Integer;
	NomArchivo: AnsiString;
begin
	Result := False;
    Error := teObtenerPath;
	// Par�metros Ok?
	if not Assigned(Imagen) then begin
		DescriError := 'El par�metro Imagen (TBitmap) no puede ser nil';
        DescriError := DescriError + CRLF + Format(MSG_DATOS_TRANSITO, [NumCorrCO]);           // SS_1416_CQU_20151123
		Exit;
	end;
	// El path existe o pudo ser creado?
	if not ArmarPathTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, NomArchivo) then begin
		DescriError := 'No se pudo obtener el Path de la imagen';
        DescriError := DescriError + CRLF + Format(MSG_DATOS_TRANSITO, [NumCorrCO]);           // SS_1416_CQU_20151123
		Error := teObtenerPath;
		Exit;
	end;
	// La imagen existe?
	if not FileExists(NomArchivo) then begin
		DescriError := 'La imagen no existe';
        DescriError := DescriError + CRLF + Format(MSG_DATOS_TRANSITO, [NumCorrCO]);           // SS_1416_CQU_20151123
		Error := teImagenNoExiste;
		Exit;
	end;

    if GetFileSize(NomArchivo) <= 0 then begin             // SS_1261_CQU_20150506
		DescriError := 'Tama�o del archivo incorrecto';    // SS_1261_CQU_20150506
		Error := teLeerArchivo;                            // SS_1261_CQU_20150506
		Exit;                                              // SS_1261_CQU_20150506
    end;                                                   // SS_1261_CQU_20150506

	HFilePend := -1;
	try
		HFilePend := FileOpenRetry(NomArchivo, fmOpenRead or fmShareDenyWrite);
		if HFilePend = -1 then
		  raise exception.Create('Error al abrir archivo');

		//BEGIN: SS_1163_NDR_20140206 ---------------------------------------------------------
	    if TipoImagen=tiFrontal then
	    begin
	      if not CargarImagenPendientes(HFilePend, Imagen, DatosImg, DescriError) then
	        raise exception.Create(DescriError);
	    end
	    else if ((TipoImagen=tiOverview1) or (TipoImagen=tiOverview2) or (TipoImagen=tiOverview3)) then
	    begin
	        if not CargarOverViewPendientes(HFilePend, Imagen, DatosImg, DescriError) then
	          raise exception.Create(DescriError);
	    end
	    else if ((TipoImagen=tiFrontal2) or (TipoImagen=tiPosterior) or (TipoImagen=tiPosterior2)) then
	    begin
	        if not CargarOverViewPendientes(HFilePend, Imagen, DatosImg, DescriError) then
	          raise exception.Create(DescriError);
	    end;
		//END: SS_1163_NDR_20140206 ---------------------------------------------------------

		Result := True;
	except
	end;
	if HFilePend > 0 then FileClose(HFilePend);
end;

{-----------------------------------------------------------------------------
  Function Name	: ObtenerImagenTransitoPendiente
  Author		: Claudio Quezada Ib��ez
  Date Created	: 19/12/2014
  Firma         : SS_1147V_CQU_20141219
  Description	: Carga la imagen del tr�nsito seg�n el tipo (JPeg12 o BMP)
				  Mediante una variable indica si es Kapsch o no
  Parameters	: ImagePath: AnsiString; NumCorrCO: Int64; TipoImagen: TTipoImagen;
                  var Imagen: TObject; var DatosImg: TDataImage; var DescriError: AnsiString;
                  var Error: TTipoErrorImg; var EsKapsch : boolean
  Return Value	: Boolean
-----------------------------------------------------------------------------}
function ObtenerImagenTransitoPendiente(ImagePath: AnsiString; NumCorrCO: Int64;                                                                        //	SS_1147V_CQU_20141219
        TipoImagen: TTipoImagen; var Imagen: TObject; var DatosImg: TDataImage;                                                                         //	SS_1147V_CQU_20141219
        var DescriError: AnsiString; var Error: TTipoErrorImg; var EsKapsch : boolean): Boolean; Overload;                                              //	SS_1147V_CQU_20141219
var																																						//	SS_1147V_CQU_20141219
    Resultado       : Boolean;																															//	SS_1147V_CQU_20141219
    ImagenNoKapsch  : TBitmap;																															//	SS_1147V_CQU_20141219
    ImagenKapsch    : TJPEGPlusImage;																													//	SS_1147V_CQU_20141219
begin                                                                                                  													//	SS_1147V_CQU_20141219
    try																																					//	SS_1147V_CQU_20141219
        try																																				//	SS_1147V_CQU_20141219
            ImagenNoKapsch  := TBitmap.Create;																											//	SS_1147V_CQU_20141219
            ImagenKapsch    := TJPEGPlusImage.Create;																									//	SS_1147V_CQU_20141219
            if ObtenerImagenTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, ImagenKapsch, DatosImg, DescriError, Error) then				        //	SS_1147V_CQU_20141219
            begin																																		//	SS_1147V_CQU_20141219
                Imagen := (ImagenKapsch as TObject);																									//	SS_1147V_CQU_20141219
                EsKapsch := True;																														//	SS_1147V_CQU_20141219
                Resultado := True;																														//	SS_1147V_CQU_20141219
            end else if ObtenerImagenTransitoPendiente(ImagePath, NumCorrCO, TipoImagen, ImagenNoKapsch, DatosImg, DescriError, Error) then	            //	SS_1147V_CQU_20141219
            begin																																		//	SS_1147V_CQU_20141219
                Imagen := (ImagenNoKapsch as TObject);																									//	SS_1147V_CQU_20141219
                EsKapsch := False;																														//	SS_1147V_CQU_20141219
                Resultado := True;																														//	SS_1147V_CQU_20141219
            end else																																	//	SS_1147V_CQU_20141219
                Resultado := False;																														//	SS_1147V_CQU_20141219
        except																																			//	SS_1147V_CQU_20141219
            on e:Exception do begin																														//	SS_1147V_CQU_20141219
                Resultado := False;																														//	SS_1147V_CQU_20141219
                DescriError := e.Message;																												//	SS_1147V_CQU_20141219
                Error       := teCargarImagen;																											//	SS_1147V_CQU_20141219
            end;																																		//	SS_1147V_CQU_20141219
        end;																																			//	SS_1147V_CQU_20141219
    finally																																				//	SS_1147V_CQU_20141219
        Result := Resultado;																															//	SS_1147V_CQU_20141219
    end;																																				//	SS_1147V_CQU_20141219
end;                                                                                                   													//	SS_1147V_CQU_20141219

procedure ObtenerPathFileName(DirRaiz: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen; var Path: AnsiString; var NombreArchivo: AnsiString);
begin
	Path          := GoodDir(DirRaiz) + IntToStr(NumCorrCO mod 1000);
	NombreArchivo := GoodDir(Path) + IntToStr(NumCorrCO) + '-' + SufijoImagen[TipoImagen] + '.jpg';
end;

function ArmarPathTransitoPendiente(DirRaiz: AnsiString; NumCorrCO: Int64;
  TipoImagen: TTipoImagen; var NombreArchivo: AnsiString): Boolean;
var
	Path: AnsiString;
begin
	// Armamos el path completo con la raiz del directorio de Pendientes que viene
	// como par�metro, el grupo del Tr�nsito (N� Tr�nsito Mod 1000),
	// y el nombre del archivo con el  N� Tr�nsito y la Ubicacion de la foto.
	// Si el Directorio no existe, lo creamos.
    ObtenerPathFileName(DirRaiz, NumCorrCO, TipoImagen, Path, NombreArchivo);
	try
		Result := DirectoryExists(Path) or ForceDirectories(Path);
	except
		Result := False;
	end;
end;


{ Mueve el Archivo ArchivoJPGOrigen al directorio de Pendientes. }
function AlmacenarImagenTransitoPendiente(PathBase, ArchivoJPGOrigen: AnsiString;
  NumCorrCO: Int64; TipoImagen: TTipoImagen; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	ArchivoDestino: AnsiString;
begin
	try
		if not FileExists(ArchivoJPGOrigen) then begin
			Error := teImagenNoExiste;
            //raise exception.Create('No existe archivo de Origen');            // SS_1132_MCA_20131004
			raise exception.Create(Format('No existe archivo de Origen %s - NumCorrCO: %d', [ArchivoJPGOrigen, NumCorrCO])); // SS_1132_MCA_20131004
		end;
        if GetFileSize(ArchivoJPGOrigen) <= 0 then begin                                            // SS_1288_CQU_20150601
            DescriError := Format('Tama�o del archivo incorrecto - NumCorrCO: %d', [NumCorrCO]);    // SS_1288_CQU_20150601
            Error := teLeerArchivo;                                                                 // SS_1288_CQU_20150601
            raise exception.Create(DescriError);                                                    // SS_1288_CQU_20150601
        end;                                                                                        // SS_1288_CQU_20150601
		// Armar el Path completo donde guardar el archivo
		if not ArmarPathTransitoPendiente(PathBase, NumCorrCO, TipoImagen, ArchivoDestino) then begin
			Error := teObtenerPath;
            //raise exception.Create('No se pudo obtener el Path de la imagen en Pendientes');          // SS_1132_MCA_20131004
			raise exception.Create(Format('No se pudo obtener el Path de la imagen en Pendientes %s - NumCorrCO: %d', [PathBase, NumCorrCO]));      // SS_1132_MCA_20131004
		end;
		// Copiar
		if not FileCopy(ArchivoJPGOrigen, ArchivoDestino) then begin
			Error := teGrabarArchivo;
            //raise exception.Create('No se pudo copiar el archivo al directorio de Pendientes');       // SS_1132_MCA_20131004
			raise exception.Create(Format('No se pudo copiar el archivo al directorio de Pendientes %s - NumCorrCO: %d', [ArchivoDestino, NumCorrCO])); // SS_1132_MCA_20131004
		end;
		Result := True;
	except
		on e: exception do begin
			DescriError := e.Message;
			Result := False;
		end;
	end;
end;


function ObtenerPathTransitoPendiente(Conn: TADOConnection; Concesionaria, PuntoCobro,
  NumeroTransito: Integer; TipoImagen: TTipoImagen; var ArchivoTransito: AnsiString; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	PathImagenes: TPaths;
	DatosImg: TDataOverview;
	DirRaiz, NomArchivo: AnsiString;
begin
	Result := False;
	CargarDatosPath(Conn, PathImagenes);
	FillChar(DatosImg, Sizeof(DatosImg), 0);
	// El path existe o pudo ser creado?
	DirRaiz := PathImagenes.Pendientes;
	if not ArmarPathTransitoPendiente(DirRaiz, NumeroTransito, TipoImagen, NomArchivo) then begin
		DescriError := 'No se pudo obtener el Path de la imagen';
		Error := teObtenerPath;
		Exit;
	end;
	ArchivoTransito := NomArchivo;
	Result := True;
end;

{---------------------------------------------------------------------------------
Function Name: ArmarPathImagenTVM
Author       : alabra
Date Created : 27-07-2011
Description  : Arma el path donde se debe encontrar la imagen de TVM.
----------------------------------------------------------------------------------}
{
function ArmarPathImagenTVM(PathRaiz, NombreImagen, NombreCorto : AnsiString; var PathRetorno : AnsiString) : Boolean;
var
    Fecha   : AnsiString;
    Mes     : AnsiString;
begin
    try
        Mes     := AnsiMidStr(NombreImagen, 5, 2);
        // yyyy-mm-dd
        Fecha   := AnsiMidStr(NombreImagen, 0, 4) + '-' + AnsiMidStr(NombreImagen, 5, 2) + '-' + AnsiMidStr(NombreImagen, 7, 2);

        PathRetorno := format('%s%s\%s\%s\%s\', [PathRaiz, Mes, Fecha, NombreCorto, 'TVM']);
        Result := True;
    except
        Result := False;
    end;
end;
}

function ArmarPathImagenTVM(PathRaiz, NombreImagen, NombreCorto : AnsiString; var PathRetorno : AnsiString) : Boolean;
var
    Fecha   : AnsiString;
    Mes     : AnsiString;
    DirectorioImagenes : String;
const
    DIR_IMAGENES_TRANSITOS = 'DIR_IMAGENES_TRANSITOS_%s';
begin
    try
        Result := False;

        Mes     := AnsiMidStr(NombreImagen, 5, 2);

        if not ObtenerParametroGeneral(
                  DMConnections.BaseCAC,
                  Format(DIR_IMAGENES_TRANSITOS,[Mes]),
                  DirectorioImagenes) then Exit;


        // yyyy-mm-dd
        Fecha   := AnsiMidStr(NombreImagen, 0, 4) + '-' + AnsiMidStr(NombreImagen, 5, 2) + '-' + AnsiMidStr(NombreImagen, 7, 2);

        PathRetorno := format('%s\%s\%s\%s\', [DirectorioImagenes, Fecha, NombreCorto, 'TVM']);
        Result := True;
    except
        Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name	: ArmarPathPadreTransitosInfractor
  Author		: Claudio Quezada Ib��ez
  Date Created	: 30/10/2013
  Firma         : SS_1135_CQU_20131030
  Description	: Genera la ruta para obtener imagenes de infracciones
  Parameters	: DirRaiz, NombreCorto: AnsiString; NumCorrCA: int64; Fecha: TDateTime; var ArchivoPadre: AnsiString
  Return Value	: Boolean
-----------------------------------------------------------------------------}
function ArmarPathPadreTransitosInfractor(DirRaiz, NombreCorto: AnsiString; NumCorrCA: int64; Fecha: TDateTime;
  var ArchivoPadre: AnsiString): Boolean;
var
	Nivel1, Nivel2: AnsiString;
    DirectorioImagenes: string;
const
    DIR_IMAGENES_TRANSITOS_INFRACTORES = 'DIR_IMAGENES_TRANSITOS_INFRACTORES';
begin
    try
        Result := False;
        if length(DirRaiz) = 0 then begin
            if not ObtenerParametroGeneral(
                      DMConnections.BaseCAC,
                      DIR_IMAGENES_TRANSITOS_INFRACTORES,
                      DirectorioImagenes) then Exit;
        end else
            DirectorioImagenes := DirRaiz;

        // Armamos el Path completo con la dir del directorio de la Concesionaria/PuntoCobro y sgte nivel.                                                                                                  //SS-377-NDR-20110607
		DateTimeToString(Nivel1, 'yyyy-mm-dd', Fecha);
		Nivel2		  := IntToStr(NumCorrCA MOD 1000);
        // Si se ha iniciado la Migraci�n devuelve s�lo la ruta, sino la ruta completa del Archivo Padre.
        ArchivoPadre := format('%s\%s\%s\', [DirectorioImagenes, Nivel1, Nivel2]);

		Result := True;
	except
		Result := False;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name	: ObtenerImagenTransitoInfractor
  Author		: Claudio Quezada Ib��ez
  Date Created	: 30/10/2013
  Firma         : SS_1135_CQU_20131030
  Description	: Genera la ruta para obtener imagenes de infracciones
  Parameters	: ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64;
                  FechaTransito: TDateTime; TipoImg: TTipoImagen; var Imagen: TBitmap;
                  var DataImage: TDataIMage; var DescriError: AnsiString;
                  var Error: TTipoErrorImg
  Return Value	: Boolean
-----------------------------------------------------------------------------}
function ObtenerImagenTransitoInfractor(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;    
    var Imagen: TJPEGPlusImage; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;               
var                                                                                                                                                 
	ArchivoPadre: AnsiString;
                                                                                                                                                    
    function ObtenerInfraccion: Boolean;
    begin                                                                                                                                           
        Result := False;
        // Path del Archivo Padre.
        if not ArmarPathPadreTransitosInfractor(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin
            DescriError := DescripcionError[teObtenerPath];
            Error := teObtenerPath;                                                                                                                 
            Exit;                                                                                                                                   
        end;                                                                                                                                        
                                                                                                                                                    
        // Para salvar el problema de los seg en la transf de imagenes, buscamos en el dia del transito y en el dia anterior                        
        if (Hour(FechaTransito) = 0) and (Minute(FechaTransito) = 0) then begin
            // Caso a salvar                                                                                                                        
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                           
              Imagen, DataImage, DescriError, Error) then begin                                                                                     
                  // Si la imagen se se encontro pero no se pudo cargar por algun error nos vamos                                                   
                  if Error in [teParametrosMal, teCargarImagen, teFormatoImagen] then exit;                                                         
                  FechaTransito := incDay(FechaTransito, -1);                                                                                       
                  // Recalculo el archivo, pero con la fecha anterior al transito                                                                   
                  if not ArmarPathPadreTransitosInfractor(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin
                      DescriError := DescripcionError[teObtenerPath];
                      Error := teObtenerPath;                                                                                                       
                      Exit;                                                                                                                         
                  end;                                                                                                                              
                  if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,
                    Imagen, DataImage, DescriError, error) then begin                                                                               
                      Exit;                                                                                                                         
                  end;                                                                                                                              
            end;                                                                                                                                    
        end else begin                                                                                                                              
            // Caso Normal                                                                                                                          
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                           
              Imagen, DataImage, DescriError, error) then Exit;                                                                                     
            end;                                                                                                                                    
                                                                                                                                                    
        Result := True;                                                                                                                             
    end;                                                                                                                                            
                                                                                                                                                    
begin                                                                                                                                               
    Result := False;
    DescriError := '';                                                                                                                              
    Error := teSinError;                                                                                                                            
    try                                                                                                                                             
        // Verificar Par�metro                                                                                                                     
        if not Assigned(Imagen) then begin                                                                                                          
            Error := teParametrosMal;                                                                                                               
            DescriError := DescripcionError[teParametrosMal];                                                                                       
            Exit;                                                                                                                                   
        end;                                                                                                                                        
                                                                                                                                                    
        if not ObtenerInfraccion then Exit;
                                                                                                                                                    
        Result := True;                                                                                                                             
    except                                                                                                                                          
        on e:Exception do begin                                                                                                                     
            Result := False;                                                                                                                        
            DescriError := e.Message;                                                                                                               
            Error       := teCargarImagen;                                                                                                          
        end;                                                                                                                                        
    end;                                                                                                                                            
end;

{-----------------------------------------------------------------------------
  Function Name	: ObtenerImagenTransitoInfractor
  Author		: Claudio Quezada Ib��ez
  Date Created	: 30/10/2013
  Firma         : SS_1135_CQU_20131030
  Description	: Genera la ruta para obtener imagenes de infracciones
  Parameters	: ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64;
                  FechaTransito: TDateTime; TipoImg: TTipoImagen; var Imagen: TBitmap;
                  var DataImage: TDataIMage; var DescriError: AnsiString;
                  var Error: TTipoErrorImg
  Return Value	: Boolean
-----------------------------------------------------------------------------}
function ObtenerImagenTransitoInfractor(ImagePathNFI, NombreCorto: AnsiString; NumCorrCA: Int64; FechaTransito: TDateTime; TipoImg: TTipoImagen;
    var Imagen: TBitmap; var DataImage: TDataIMage; var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean; overload;
var
	ArchivoPadre: AnsiString;

    function ObtenerInfraccion: Boolean;
    begin
        Result := False;                                                                                                                                
        // Path del Archivo Padre.                                                                                                                      
        if not ArmarPathPadreTransitosInfractor(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin
            DescriError := DescripcionError[teObtenerPath];                                                                                             
            Error := teObtenerPath;                                                                                                                     
            Exit;                                                                                                                                       
        end;                                                                                                                                            
                                                                                                                                                        
        // Para salvar el problema de los seg en la transf de imagenes, buscamos en el dia del transito y en el dia anterior                            
        if (Hour(FechaTransito) = 0) and (Minute(FechaTransito) = 0) then begin                                                                         
            // Caso a salvar                                                                                                                            
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                               
              Imagen, DataImage, DescriError, Error) then begin                                                                                         
                  // Si la imagen se se encontro pero no se pudo cargar por algun error nos vamos                                                       
                  if Error in [teParametrosMal, teCargarImagen, teFormatoImagen] then exit;                                                             
                  FechaTransito := incDay(FechaTransito, -1);                                                                                           
                  // Recalculo el archivo, pero con la fecha anterior al transito                                                                       
                  if not ArmarPathPadreTransitosInfractor(ImagePathNFI, NombreCorto, NumCorrCA, FechaTransito, ArchivoPadre) then begin
                      DescriError := DescripcionError[teObtenerPath];                                                                                   
                      Error := teObtenerPath;                                                                                                           
                      Exit;                                                                                                                             
                  end;
                  if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                         
                    Imagen, DataImage, DescriError, error) then begin                                                                                   
                      Exit;                                                                                                                             
                  end;                                                                                                                                  
            end;                                                                                                                                        
        end else begin                                                                                                                                  
            // Caso Normal                                                                                                                              
            if not ObtenerImagenIMGTransitoNuevoFormato(ArchivoPadre, NumCorrCA, TipoImg,                                                               
              Imagen, DataImage, DescriError, error) then Exit;                                                                                         
            end;                                                                                                                                        
                                                                                                                                                        
        Result := True;                                                                                                                                 
    end;                                                                                                                                                
                                                                                                                                                        
begin                                                                                                                                                   
    Result := False;
    DescriError := '';                                                                                                                                  
    Error := teSinError;                                                                                                                                
    try                                                                                                                                                 
        // Verificar Par�metro                                                                                                                         
        if not Assigned(Imagen) then begin                                                                                                              
            Error := teParametrosMal;                                                                                                                   
            DescriError := DescripcionError[teParametrosMal];                                                                                           
            Exit;                                                                                                                                       
        end;                                                                                                                                            
                                                                                                                                                        
        if not ObtenerInfraccion then Exit;
                                                                                                                                                        
        Result := True;                                                                                                                                 
    except                                                                                                                                              
        on e:Exception do begin                                                                                                                         
            Result := False;                                                                                                                            
            DescriError := e.Message;                                                                                                                   
            Error       := teCargarImagen;                                                                                                              
        end;                                                                                                                                            
    end;                                                                                                                                                
end;                                                                                                                                                    

{-----------------------------------------------------------------------------
  Function Name	: AjustarTama�oImagenPlus
  Author		: Claudio Quezada Ib��ez
  Date Created	: 29/09/2015
  Firma         : SS_1374A_CQU_20150924 (originalmente SS_1382_MCA_20150928)
  Description	: Redimensiona la imagen al interior de un TImagePlus
  Parameters	: var ImagenAmpliar : TImagePlus
-----------------------------------------------------------------------------}
procedure AjustarTama�oTImagePlus(var ImagenAmpliar : TImagePlus);
var
    WidthObjImg : Integer;
begin
    WidthObjImg := ImagenAmpliar.Width;
    if WidthObjImg >= 2048 then begin
        if ImagenAmpliar.Picture.Width >= 2048 then
            ImagenAmpliar.Scale := 0.5
        else begin
            if (ImagenAmpliar.Picture.Width >= 1024) and (ImagenAmpliar.Picture.Width < 2048) then
                ImagenAmpliar.Scale := 1
            else
                ImagenAmpliar.Scale := 1.5;
        end;
    end
    else begin
        if (WidthObjImg >= 1024) and  (WidthObjImg < 2048) then  begin
            if ImagenAmpliar.Picture.Width >= 2048 then
                ImagenAmpliar.Scale := 0.35
            else begin
                if (ImagenAmpliar.Picture.Width >= 1024) and (ImagenAmpliar.Picture.Width < 2048) then
                    ImagenAmpliar.Scale := 0.9
                else
                    ImagenAmpliar.Scale := 1.5;
            end;
        end
        else begin
            if ImagenAmpliar.Picture.Width >= 2048 then
                ImagenAmpliar.Scale := 0.23
            else begin
                if (ImagenAmpliar.Picture.Width >= 1024) and (ImagenAmpliar.Picture.Width < 2048) then
                    ImagenAmpliar.Scale := 0.6
                else
                    ImagenAmpliar.Scale := 0.9;
            end;
        end;
    end;
end;

end.

