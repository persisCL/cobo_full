object FormRecibirTags: TFormRecibirTags
  Left = 334
  Top = 239
  BorderStyle = bsDialog
  Caption = 'Recibir Telev'#237'as'
  ClientHeight = 312
  ClientWidth = 422
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 422
    Height = 64
    Align = alTop
    TabOrder = 0
    object lblGuiaDespacho: TLabel
      Left = 6
      Top = 38
      Width = 94
      Height = 13
      Caption = '&Gu'#237'a de Despacho:'
      FocusControl = cbGuiaDespacho
    end
    object lblTransportista: TLabel
      Left = 6
      Top = 10
      Width = 64
      Height = 13
      Caption = '&Transportista:'
    end
    object lblAlmacenDestino: TLabel
      Left = 284
      Top = 8
      Width = 83
      Height = 13
      Caption = 'Almac'#233'n Destino:'
    end
    object cbTransportistas: TVariantComboBox
      Left = 103
      Top = 6
      Width = 133
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTransportistasChange
      Items = <>
    end
    object cbGuiaDespacho: TComboBox
      Left = 103
      Top = 34
      Width = 133
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbTransportistasChange
    end
    object txtAlmacenDestino: TEdit
      Left = 258
      Top = 26
      Width = 147
      Height = 21
      Cursor = crArrow
      ReadOnly = True
      TabOrder = 2
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 266
    Width = 422
    Height = 46
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      422
      46)
    object btnAceptar: TDPSButton
      Left = 160
      Top = 11
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TDPSButton
      Left = 339
      Top = 11
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
    end
    object btnObservacion: TDPSButton
      Left = 241
      Top = 11
      Width = 94
      Anchors = [akRight, akBottom]
      Caption = '&Observaci'#243'n'
      TabOrder = 2
      OnClick = btnObservacionClick
    end
  end
  object dbgRecibirTags: TDBGrid
    Left = 0
    Top = 64
    Width = 422
    Height = 202
    Align = alClient
    DataSource = dsRecibirTags
    Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyDown = dbgRecibirTagsKeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Caption = 'Categor'#237'a'
        Width = 300
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CantidadTags'
        Title.Alignment = taCenter
        Title.Caption = 'Cantidad Telev'#237'as'
        Width = 100
        Visible = True
      end>
  end
  object dsRecibirTags: TDataSource
    DataSet = cdsRecibirTags
    Left = 88
    Top = 152
  end
  object ActualizarStockTagsRecibidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarStockTagsRecibidos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 368
    Top = 152
  end
  object ObtenerDatosGuiaDespacho: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosGuiaDespacho'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 156
    Top = 185
  end
  object ActualizarEstadoGuiaDespacho: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadoGuiaDespacho'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Observacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@FechaAceptacionRechazo'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraRecepcion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraEnvio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 368
    Top = 184
  end
  object ObtenerGuiaDespachoCategorias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerGuiaDespachoCategorias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 155
    Top = 152
  end
  object cdsRecibirTags: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Categoria'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CantidadTags'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 123
    Top = 152
  end
end
