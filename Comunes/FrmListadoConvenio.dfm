object FormListadoConvenios: TFormListadoConvenios
  Left = 60
  Top = 89
  Caption = 'Listado de Convenios'
  ClientHeight = 508
  ClientWidth = 850
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 809
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 850
    Height = 125
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 850
      Height = 125
      Align = alClient
      Caption = 'Filtros'
      TabOrder = 0
      DesignSize = (
        850
        125)
      object Label1: TLabel
        Left = 11
        Top = 24
        Width = 131
        Height = 13
        Caption = 'Fecha de Creacion:  &desde:'
        FocusControl = txt_FechaDesde
      end
      object Label2: TLabel
        Left = 248
        Top = 24
        Width = 29
        Height = 13
        Caption = '&hasta:'
        FocusControl = txt_FechaHasta
      end
      object Label3: TLabel
        Left = 386
        Top = 23
        Width = 88
        Height = 13
        Caption = 'N'#250'mero Convenio:'
      end
      object Label4: TLabel
        Left = 595
        Top = 24
        Width = 75
        Height = 13
        Caption = '&Veh'#237'culos >= a:'
        FocusControl = txt_CantVehiculos
      end
      object Label5: TLabel
        Left = 11
        Top = 51
        Width = 55
        Height = 13
        Caption = 'Personer&'#237'a:'
        FocusControl = cb_Personeria
      end
      object Label6: TLabel
        Left = 251
        Top = 51
        Width = 26
        Height = 13
        Caption = '&RUT:'
        FocusControl = txt_rut
      end
      object Label7: TLabel
        Left = 380
        Top = 51
        Width = 66
        Height = 13
        Caption = '&Apellido / RS:'
        FocusControl = txt_Apellido
      end
      object Label8: TLabel
        Left = 595
        Top = 51
        Width = 40
        Height = 13
        Caption = '&Patente:'
        FocusControl = txt_Patente
      end
      object Label9: TLabel
        Left = 11
        Top = 76
        Width = 84
        Height = 13
        Caption = 'Estado &Convenio:'
        FocusControl = cb_EstadoConvenio
      end
      object Label10: TLabel
        Left = 251
        Top = 76
        Width = 70
        Height = 13
        Caption = 'C&oncesionaria:'
        FocusControl = cb_Concesionaria
      end
      object Label11: TLabel
        Left = 599
        Top = 76
        Width = 42
        Height = 13
        Caption = 'Limitar a:'
      end
      object lbl_PagoAutomatico: TLabel
        Left = 10
        Top = 104
        Width = 92
        Height = 13
        Caption = 'F. Pago autom'#225'tico'
        Color = clBtnFace
        FocusControl = cb_PagoAutomatico
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label12: TLabel
        Left = 251
        Top = 104
        Width = 58
        Height = 13
        Caption = 'Nro Televia:'
        FocusControl = cb_Concesionaria
      end
      object txt_FechaDesde: TDateEdit
        Left = 148
        Top = 19
        Width = 91
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 284
        Top = 19
        Width = 91
        Height = 21
        AutoSelect = False
        TabOrder = 1
        Date = 36526.000000000000000000
      end
      object txt_CantVehiculos: TNumericEdit
        Left = 676
        Top = 17
        Width = 33
        Height = 21
        TabOrder = 3
      end
      object cb_Personeria: TComboBox
        Left = 114
        Top = 46
        Width = 127
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 4
      end
      object txt_Apellido: TEdit
        Left = 451
        Top = 44
        Width = 133
        Height = 21
        MaxLength = 60
        TabOrder = 6
        OnKeyPress = txt_ApellidoKeyPress
      end
      object txt_rut: TEdit
        Left = 284
        Top = 46
        Width = 91
        Height = 21
        Hint = 'N'#250'mero de documento'
        CharCase = ecUpperCase
        MaxLength = 9
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnKeyPress = txt_rutKeyPress
      end
      object txt_Patente: TEdit
        Left = 656
        Top = 43
        Width = 56
        Height = 21
        MaxLength = 10
        TabOrder = 7
        OnKeyPress = txt_ApellidoKeyPress
      end
      object cb_EstadoConvenio: TComboBox
        Left = 113
        Top = 72
        Width = 128
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 8
      end
      object txt_NumeroConvenio: TEdit
        Left = 476
        Top = 17
        Width = 109
        Height = 21
        MaxLength = 30
        TabOrder = 2
      end
      object cb_Concesionaria: TComboBox
        Left = 328
        Top = 72
        Width = 257
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 9
      end
      object txt_Top: TNumericEdit
        Left = 655
        Top = 72
        Width = 57
        Height = 21
        MaxLength = 4
        TabOrder = 10
        OnExit = txt_TopExit
      end
      object cb_PagoAutomatico: TVariantComboBox
        Left = 113
        Top = 98
        Width = 128
        Height = 21
        Hint = 'Indique si utiliza una forma de pago autom'#225'tica'
        Style = vcsDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        Items = <>
      end
      object txt_Tag: TEdit
        Left = 328
        Top = 98
        Width = 121
        Height = 21
        MaxLength = 11
        TabOrder = 12
        Text = 'txt_Tag'
      end
      object btn_Filtrar: TButton
        Left = 766
        Top = 16
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Filtrar'
        Default = True
        TabOrder = 13
        OnClick = btn_FiltrarClick
      end
      object btn_Limpiar: TButton
        Left = 766
        Top = 48
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 14
        OnClick = btn_LimpiarClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 125
    Width = 850
    Height = 342
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object dbl_Convenio: TDBListEx
      Left = 0
      Top = 0
      Width = 850
      Height = 342
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 115
          Header.Caption = 'Nro. Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'NumeroConvenio'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'RUT'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'RUT'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 300
          Header.Caption = 'Nombre Completo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'Nombre'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 90
          Header.Caption = 'Cant. Veh'#237'culos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'CantidadVehiculos'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 85
          Header.Caption = 'Fecha Alta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaAlta'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 90
          Header.Caption = 'Fecha Modificaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'FechaActualizacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 85
          Header.Caption = 'Fecha Baja'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaBaja'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 50
          Header.Caption = 'Personer'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'PersoneriaDescrip'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 130
          Header.Caption = 'Medio de Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'TipoMedioPagoAutomatico'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'DescripcionEstadoConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo de Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionTipoConvenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Convenio de Facturaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroConvenioFacturacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'RUT de Facturaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'RUTClienteFacturacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripicionConcesionarias'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Usuario Ultima Mod.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'CodigoUsuarioActualizacion'
        end>
      DataSource = DS_ListadoConvenio
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbl_ConvenioDblClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 467
    Width = 850
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 850
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        850
        41)
      object lbl_Mostar: TLabel
        Left = 8
        Top = 16
        Width = 282
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lbl_Mostar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Notebook: TNotebook
        Left = 565
        Top = 4
        Width = 139
        Height = 33
        Anchors = [akRight, akBottom]
        PageIndex = 1
        TabOrder = 2
        object TPage
          Left = 0
          Top = 0
          Caption = 'Ver Detalle'
          DesignSize = (
            139
            33)
          object btn_Ver: TButton
            Left = 6
            Top = 2
            Width = 130
            Height = 26
            Hint = 'Detalle de la solicitud'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Ver Detalle - Imprimir'
            TabOrder = 0
            OnClick = btn_VerClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Modificacion'
          DesignSize = (
            139
            33)
          object btn_modificar: TButton
            Left = 8
            Top = 3
            Width = 127
            Height = 26
            Hint = 'Detalle de la solicitud'
            Anchors = [akRight, akBottom]
            Cancel = True
            Caption = '&Modificar'
            TabOrder = 0
            OnClick = btn_modificarClick
          end
        end
      end
      object BtnSalir: TButton
        Left = 707
        Top = 7
        Width = 130
        Height = 26
        Hint = 'Salir del visualizador de Solicitudes'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 3
        OnClick = BtnSalirClick
      end
      object btn_VerDocumento: TButton
        Left = 433
        Top = 7
        Width = 130
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = 'Ver &Imagenes Docs.'
        TabOrder = 1
        OnClick = btn_VerDocumentoClick
      end
      object btnImprimir: TButton
        Left = 297
        Top = 7
        Width = 130
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = 'Imprimir Estado Convenio'
        TabOrder = 0
        OnClick = btnImprimirClick
      end
    end
  end
  object ObtenerListadoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    AfterOpen = ObtenerListadoConvenioAfterOpen
    AfterClose = ObtenerListadoConvenioAfterOpen
    AfterScroll = ObtenerListadoConvenioAfterScroll
    ProcedureName = 'ObtenerListadoConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHoraDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoEstadoConvenio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadVehiculos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Concesionarias'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TOP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NroEtiqueta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 152
    Top = 168
  end
  object DS_ListadoConvenio: TDataSource
    DataSet = ObtenerListadoConvenio
    Left = 280
    Top = 168
  end
end
