object FormCerrarCuenta: TFormCerrarCuenta
  Left = 133
  Top = 163
  Width = 768
  Height = 450
  Caption = 'Cerrar Cuenta'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 9
    Width = 35
    Height = 13
    Caption = 'Cliente:'
  end
  object lblCliente: TLabel
    Left = 81
    Top = 9
    Width = 258
    Height = 13
    AutoSize = False
    Caption = 'lblCliente'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    Left = 11
    Top = 49
    Width = 37
    Height = 13
    Caption = 'Cuenta:'
  end
  object lblCuenta: TLabel
    Left = 80
    Top = 49
    Width = 54
    Height = 13
    Caption = 'lblCuenta'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label3: TLabel
    Left = 352
    Top = 10
    Width = 45
    Height = 13
    Caption = 'Tel'#233'fono:'
  end
  object lblTelefono: TLabel
    Left = 430
    Top = 10
    Width = 64
    Height = 13
    Caption = 'lblTelefono'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label4: TLabel
    Left = 12
    Top = 117
    Width = 74
    Height = 13
    Caption = 'Observaciones:'
  end
  object Label5: TLabel
    Left = 11
    Top = 69
    Width = 28
    Height = 13
    Caption = 'Pago:'
  end
  object lblTipoPago: TLabel
    Left = 80
    Top = 69
    Width = 43
    Height = 13
    Caption = 'lblPago'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object label12: TLabel
    Left = 352
    Top = 69
    Width = 46
    Height = 13
    Caption = 'Veh'#237'culo:'
  end
  object lblVehiculo: TLabel
    Left = 430
    Top = 69
    Width = 63
    Height = 13
    Caption = 'lblVehiculo'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label6: TLabel
    Left = 352
    Top = 49
    Width = 73
    Height = 13
    Caption = 'Plan Comercial:'
  end
  object lblPlanComercial: TLabel
    Left = 430
    Top = 49
    Width = 94
    Height = 13
    Caption = 'lblPlanComercial'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label7: TLabel
    Left = 12
    Top = 29
    Width = 58
    Height = 13
    Caption = 'Documento:'
  end
  object lblDocumento: TLabel
    Left = 80
    Top = 29
    Width = 78
    Height = 13
    Caption = 'lblDocumento'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label8: TLabel
    Left = 12
    Top = 89
    Width = 30
    Height = 13
    Caption = 'Saldo:'
  end
  object lblSaldo: TLabel
    Left = 81
    Top = 89
    Width = 46
    Height = 13
    Caption = 'lblSaldo'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object memoObs: TMemo
    Left = 12
    Top = 134
    Width = 737
    Height = 247
    Enabled = False
    TabOrder = 0
  end
  object btnCerrarCuenta: TDPSButton
    Left = 679
    Top = 389
    Caption = '&Cerrar Cuenta'
    TabOrder = 1
    OnClick = btnCerrarCuentaClick
  end
  object btnCancelar: TDPSButton
    Left = 599
    Top = 389
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object ObtenerDetallesCuentaCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDetallesCuentaCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 148
  end
  object ObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 280
    Top = 200
  end
end
