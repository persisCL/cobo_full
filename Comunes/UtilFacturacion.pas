{-----------------------------------------------------------------------------
 File Name: UtilFacturacion
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 25/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

Revision  : 2
Date : 07/04/2009
Author : Nelson Droguett Sierra
Description : Se le agregaron las propiedades TipoComprobanteFiscal y
              NumeroComprobanteFiscal al objeto TComprobante

Revision  : 3
Date : 03/06/2010
Author : pdominguez
Description : Infractores Fase 2
    - Se a�ade a la Clase TComprobante la propiedad TieneConceptosInfractor: Boolean
-----------------------------------------------------------------------------}
unit UtilFacturacion;

interface

Uses
	Classes, Windows, Messages, SysUtils, PeaTypes, Menus, Util, UtilProc, DB,
	DBTables, UtilDB, Graphics, WinSock, Forms, Controls, JPeg, ADODB, StdCtrls,
    Variants, ComCtrls, StrUtils, dialogs, DMConnection, Math, VariantComboBox,
    DateUtils, RStrings, Contnrs;

procedure CargarConceptosExternos(Conn: TADOConnection; Combo: TComboBox; CodigoConcepto: Integer = 0);
procedure CargarCuentasDebito(Conn: TADOConnection; Combo: TComboBox; CodigoCliente: Integer; TipoPago: AnsiString = ''; TipoDebitoAutomatico: AnsiString = ''; CodigoDebitoAutomatico: Integer = 0);
procedure CargarPlanesComerciales(Conn: TADOConnection; Combo: TComboBox; CodigoPlanComercial:AnsiString = '');
procedure CargarTiposCliente(Conn: TADOConnection; Combo: TComboBox; TipoCliente: Integer = 0);
procedure CargarPlanesComercialesPosibles(Conn: TADOConnection; Combo: TComboBox; TipoCliente, TipoAdhesion: Integer; PlanComercial: Integer = 0);
procedure CargarFacturas(Conn: TADOConnection; Combo: TComboBox; CodigoCliente: LongInt; NumeroFactura: Integer = 0);
procedure CargarPlanesFinanciamiento(Conn: TADOConnection; Combo: TComboBox; CodigoFinanciamiento: Integer = 0);
procedure CargarGruposFacturacion (Conn: TADOConnection; Combo: TComboBox; CodigoGrupo:AnsiString = '');
procedure CargarDebitosAutomaticos(Conn: TADOConnection; Combo: TComboBox; CodigoCliente:Integer; CodigoDebito:Integer = 0);
procedure CargarCuentasConvenio(Conn: TADOConnection; Combo: TVariantComboBox; CodigoConvenio: Integer; bAgregarTodos : boolean );

   
type
  TComprobante = Class(TObject)
	CodigoCliente: integer;
    TipoComprobante: AnsiString;
    NumeroComprobante: Int64;
    FechaEmision: TDateTime;
    FechaVencimiento: TDateTime;
    TotalComprobante: Double;
    TotalAPagar: Double;
    TotalPagos: Double;
    Pagado : Boolean;
    EstadoDebito : string;
    DescriEstadoPago: AnsiString;
    UltimoComprobante : Int64;
    // Rev.2 / 07-04-2009 / Nelson Droguett Sierra
    TipoComprobanteFiscal: AnsiString;
    NumeroComprobanteFiscal: Int64;
    TieneConceptosInfractor: Boolean; // Rev. 3 (Infractores Fase 2)
  end;

  TListaComprobante = Class(TObjectList)
  	constructor create;
  end;

  TNotaCredito = Class(TObject)
	CodigoCliente: integer;
    TipoComprobante: AnsiString;
    NumeroComprobante: Int64;
    FechaEmision: TDateTime;
    FechaVencimiento: TDateTime;
    TotalComprobante: Double;
    TotalAPagar: Double;
    Pagado : boolean;
    EstadoDebito : string;
    PagoAutorizado : boolean;
    Autorizo : string;
  	FechaAutorizacion : TDateTime; 
    UltimoComprobante : Int64;
  end;

implementation

{********************************** File Header ********************************
File Name : UtilFacturacion.pas
Author : flamas
Date Created: 08/07/2005
Language : ES-AR
Description : Constructor de la Clase TListaComprobante
*******************************************************************************}
constructor TListaComprobante.Create;
begin
	inherited;

    // Setea el Owned Objects en True para que libere
    // la memoria al hacer el Free de la lista
    OwnsObjects := True;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarConceptosExternos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoConcepto: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarConceptosExternos(Conn: TADOConnection; Combo: TComboBox; CodigoConcepto: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'select * from ConceptosMovimiento  WITH (NOLOCK) where Interno <> 1';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoConcepto').AsString);
			if Qry.FieldByName('CodigoConcepto').AsInteger = CodigoConcepto then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarCuentasDebito
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoCliente: Integer;
              TipoPago: AnsiString = '';
              TipoDebitoAutomatico: AnsiString = '';
              CodigoDebitoAutomatico: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCuentasDebito(Conn: TADOConnection; Combo: TComboBox; CodigoCliente: Integer; TipoPago: AnsiString = ''; TipoDebitoAutomatico: AnsiString = ''; CodigoDebitoAutomatico: Integer = 0);
resourcestring
    MSG_CUENTAS_PAGO_MANUAL = 'Cuentas de pago manual';
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TADOQuery.Create(nil);
	i := 0;
	with Qry do begin
		try
			Connection := Conn;
			// Agregar cuentas de debito manual
			SQL.add(Trim(format('SELECT TipoPago FROM Cuentas  WITH (NOLOCK)  ' +
			  'WHERE TipoPago = ''PM'' AND CodigoCliente = %d', [CodigoCliente])));
			Open;
			Combo.Clear;
			if not isEmpty then begin
				combo.items.Add(MSG_CUENTAS_PAGO_MANUAL + Space(200) + 'PM');
				if TipoPago = TP_POSPAGO_MANUAL then
					i := 0;
			end;
			close;
			SQL.Clear;
			SQL.Add(format('select distinct TarjetasCredito.Descripcion, dbo.OcultarTarjeta(DebitoAutomatico.CuentaDebito) AS CuentaDebito, ' +
			  'DebitoAutomatico.CodigoDebitoAutomatico from DebitoAutomatico  WITH (NOLOCK) , TarjetasCredito  WITH (NOLOCK) where DebitoAutomatico.CodigoEntidad = TarjetasCredito.CodigoTarjeta ' +
			  'and DebitoAutomatico.TipoDebito = ''TC'' and DebitoAutomatico.CodigoCliente = %d',[CodigoCliente]));
			Open;
			while not eof do begin
				Combo.Items.Add(format('%-s - %-s', [trim(FieldByName('Descripcion').asString),
				  trim(FieldByName('CuentaDebito').asString)]) + Space(200) +
				  FieldByName('CodigoDebitoAutomatico').AsString);
				if (TipoPago <> TP_POSPAGO_MANUAL) and (TipoDebitoAutomatico = DA_TARJETA_CREDITO) and
				  (FieldByName('CodigoDebitoAutomatico').AsInteger = CodigoDebitoAutomatico) then
					i := Combo.Items.Count - 1;
				next;
			end;
			close;
			SQL.Clear;
			SQL.Add(format('select distinct Bancos.Descripcion, DebitoAutomatico.CuentaDebito, DebitoAutomatico.CodigoDebitoAutomatico ' +
			  ' from DebitoAutomatico WITH (NOLOCK) , Bancos  WITH (NOLOCK) where DebitoAutomatico.CodigoEntidad = Bancos.CodigoBanco ' +
			  ' and DebitoAutomatico.TipoDebito = ''CB'' and DebitoAutomatico.CodigoCliente = %d',[CodigoCliente]));
			open;
			while not eof do begin
				combo.Items.Add(format('%-s - %-s', [trim(FieldByName('Descripcion').asString),
				  trim(FieldByName('CuentaDebito').asString)]) + Space(200) +
				  FieldByName('CodigoDebitoAutomatico').AsString);
				if (TipoPago <> TP_POSPAGO_MANUAL) and (TipoDebitoAutomatico = DA_CUENTA_BANCARIA) and
				  (FieldByName('CodigoDebitoAutomatico').AsInteger = CodigoDebitoAutomatico) then
					i := Combo.Items.Count - 1;
				next;
			end;
			close;
			combo.itemIndex := i;
		finally
			Combo.ItemIndex := 0;
			close;
			free;
		end;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarPlanesComerciales
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo:
              TComboBox; CodigoPlanComercial:AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarPlanesComerciales(Conn: TADOConnection; Combo: TComboBox; CodigoPlanComercial:AnsiString = '');
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM PLANESCOMERCIALES WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('PlanComercial').AsString);
			if Trim(Qry.FieldByName('PlanComercial').AsString) = Trim(CodigoPlanComercial) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarTiposCliente
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; TipoCliente: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposCliente(Conn: TADOConnection; Combo: TComboBox; TipoCliente: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposCliente WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('TipoCliente').AsString);
			if Qry.FieldByName('TipoCliente').AsInteger = TipoCliente then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPlanesComercialesPosibles
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              TipoCliente, TipoAdhesion: Integer;
              PlanComercial: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarPlanesComercialesPosibles(Conn: TADOConnection; Combo: TComboBox;
  TipoCliente, TipoAdhesion: Integer; PlanComercial: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := Format(
		  'SELECT * FROM PlanesComerciales  WITH (NOLOCK) ' +
		  'WHERE EXISTS(SELECT 1 FROM PlanesComercialesPosibles  WITH (NOLOCK) WHERE ' +
		  'TipoCliente = %d AND TipoAdhesion = %d ' +
		  'AND PlanComercial = PlanesComerciales.PlanComercial)',
		  [TipoCliente, TipoAdhesion]);
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('PlanComercial').AsString);
			if Qry.FieldByName('PlanComercial').AsInteger = PlanComercial then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarFacturas(Conn: TADOConnection; Combo: TComboBox; CodigoCliente: Integer; NumeroFactura: Integer = 0);
resourcestring
    MSG_FACTURAS = '%.0f - Venc.: %s Importe: %s';
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'EXECUTE ObtenerListaComprobantes ' + IntToStr(CodigoCliente);
		Qry.Open;
		Combo.Clear;
		i := -1;
		Qry.Last;
		While not Qry.Bof do begin
			if Qry.FieldByName('TipoComprobante').AsString = 'F' then begin
				Combo.Items.Add(Format(MSG_FACTURAS,[Qry.FieldByName('NumeroComprobante').AsFloat,
				  FormatShortDate(Qry.FieldByName('Vencimiento').AsDateTime),
                  FormatFloat(FORMATO_IMPORTE, Qry.FieldByName('ImporteTotal').AsFloat)]) + Space(200) +
				  Qry.FieldByName('NumeroComprobante').AsString);
				if Qry.FieldByName('NumeroComprobante').AsInteger = NumeroFactura then
				  i := Combo.Items.Count - 1;
			end;
			Qry.Prior;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPlanesFinanciamiento
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoFinanciamiento: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
Procedure CargarPlanesFinanciamiento(Conn: TADOConnection; Combo: TComboBox; CodigoFinanciamiento: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM PlanesFinanciamiento WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoFinanciamiento').AsString);
			if Qry.FieldByName('CodigoFinanciamiento').AsInteger = CodigoFinanciamiento then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarGruposFacturacion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;CodigoGrupo:AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarGruposFacturacion (Conn: TADOConnection; Combo: TComboBox;CodigoGrupo:AnsiString = '');
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text :=
        ' select  * ' +
        ' from Gruposfacturacion  WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
            Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
            Space(200) + Qry.FieldByName('CodigoGrupoFacturacion').AsString);
            if Trim(Qry.FieldByName('CodigoGrupoFacturacion').AsString) = Trim(CodigoGrupo) then
                i := Combo.Items.Count - 1;
            Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarDebitosAutomaticos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoCliente:Integer; CodigoDebito:Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarDebitosAutomaticos(Conn: TADOConnection; Combo: TComboBox; CodigoCliente:Integer; CodigoDebito:Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text :=
        ' select CuentaDebito, TipoDebito, CodigoDebitoAutomatico AS CodigoDebito, ' +
        ' dbo.ObtenerDescripcionDebitoAutomatico(CodigoCliente, CodigoDebitoAutomatico) AS DescripcionDebito ' +
        ' from DebitoAutomatico  WITH (NOLOCK) where codigocliente = :Cliente';
        Qry.Parameters.ParamByName('Cliente').Value := CodigoCliente;
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
            Combo.Items.Add(Qry.FieldByName('DescripcionDebito').AsString +
			  Space(200) + Qry.FieldByName('CodigoDebito').AsString);
              if Qry.FieldByName('CodigoDebito').AsInteger = CodigoDebito then
			       i := Combo.Items.Count - 1;
		    Qry.Next;
        end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarConveniosCliente(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCliente: LongInt );
Var
	Qry		: TADOQuery;
    FOldSel : Variant;
    nSelIdx : integer;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT dbo.FormatearNumeroConvenio(NumeroConvenio) as NumeroConvenio, CodigoConvenio ' +
        				'FROM Convenio WITH (NOLOCK) WHERE Convenio.CodigoCliente = ' + inttostr(CodigoCliente);
		Qry.Open;

    	FOldSel := Combo.Value;
		Combo.Clear;
        nSelIdx := 0;
        while not Qry.Eof do begin
            Combo.Items.Add(Qry.fieldbyname('NumeroConvenio').AsString,
              trim(Qry.fieldbyname('CodigoConvenio').AsString));
            if ( Qry.fieldbyname('CodigoConvenio').Value = FOldSel ) then nSelIdx := Combo.Items.Count - 1;
            Qry.Next;
        end;
        Combo.ItemIndex := nSelIdx;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCuentasConvenio
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox;
              CodigoConvenio: Integer; bAgregarTodos : boolean
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCuentasConvenio(Conn: TADOConnection; Combo: TVariantComboBox; CodigoConvenio: Integer; bAgregarTodos : boolean );
Var
	Qry     : TADOQuery;
    FOldSel : Variant;
    nSelIdx : integer;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        Qry.SQL.Text := 'SELECT dbo.SerialNumbertoEtiqueta(ContractSerialNumber) AS Etiqueta, Patente, IndiceVehiculo ' +
                        'FROM ' +
                            'Cuentas  WITH (NOLOCK) INNER JOIN MaestroVehiculos  WITH (NOLOCK) on Cuentas.CodigoVehiculo = MaestroVehiculos.CodigoVehiculo ' +
                        'WHERE ' +
                            'FechaBajaCuenta IS NULL ' +
                            'AND FechaBajaTag IS NULL ' +
                            'AND CodigoConvenio = '  + inttostr(CodigoConvenio) + ' ' +
                        'ORDER BY Patente';
		Qry.Open;
    	FOldSel := Combo.Value;
		Combo.Clear;
        nSelIdx := 0;

        if bAgregarTodos then Combo.Items.Add( 'Todos los veh�culos', null );

		While not Qry.Eof do begin
			Combo.Items.Add(trim(Qry.FieldByName('Patente').AsString) + ' - ' +
			  trim(Qry.FieldByName('Etiqueta').AsString),
              Qry.FieldByName('IndiceVehiculo').AsInteger);
            if ( Qry.FieldByName('IndiceVehiculo').AsInteger = FOldSel ) then nSelIdx := Combo.Items.Count - 1;
			Qry.Next;
		end;
        Combo.ItemIndex := nSelIdx;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


end.
