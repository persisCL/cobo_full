object FramePersona: TFramePersona
  Left = 0
  Top = 0
  Width = 817
  Height = 186
  TabOrder = 0
  TabStop = True
  object lbl_Peroneria: TLabel
    Left = 6
    Top = 13
    Width = 67
    Height = 13
    Caption = 'Personer'#237'a:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblApellido: TLabel
    Left = 6
    Top = 62
    Width = 50
    Height = 13
    Caption = 'Apellido:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblNombre: TLabel
    Left = 6
    Top = 88
    Width = 48
    Height = 13
    Caption = 'Nombre:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 113
    Width = 73
    Height = 13
    Caption = 'Pa'#237's de Origen:'
  end
  object Label10: TLabel
    Left = 8
    Top = 138
    Width = 48
    Height = 13
    Caption = 'Actividad:'
  end
  object lblApellidoMaterno: TLabel
    Left = 409
    Top = 63
    Width = 84
    Height = 13
    Caption = 'Apellido Materno:'
  end
  object Label11: TLabel
    Left = 409
    Top = 114
    Width = 82
    Height = 13
    Caption = 'Situaci'#243'n de IVA:'
  end
  object lblSexo: TLabel
    Left = 409
    Top = 88
    Width = 33
    Height = 13
    Caption = 'Sexo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblFechaNacCreacion: TLabel
    Left = 8
    Top = 163
    Width = 88
    Height = 13
    Caption = 'Fecha Nacimiento:'
  end
  object lblRUT: TLabel
    Left = 6
    Top = 38
    Width = 31
    Height = 13
    Caption = 'RUT:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblRazonSocial: TLabel
    Left = 409
    Top = 38
    Width = 80
    Height = 13
    Caption = 'Raz'#243'n Social:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblMail: TLabel
    Left = 409
    Top = 140
    Width = 93
    Height = 13
    Caption = 'Direcci'#243'n de E-Mail:'
  end
  object cb_Personeria: TComboBox
    Left = 119
    Top = 9
    Width = 117
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 0
    TabOrder = 0
    OnChange = cb_PersoneriaChange
  end
  object cbActividades: TComboBox
    Left = 119
    Top = 134
    Width = 278
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 10
  end
  object cbLugarNacimiento: TComboBox
    Left = 119
    Top = 109
    Width = 278
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 8
    OnChange = cbLugarNacimientoChange
  end
  object txtNombre: TEdit
    Left = 119
    Top = 84
    Width = 278
    Height = 21
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 30
    TabOrder = 6
    OnExit = txtNombreExit
  end
  object txtApellido: TEdit
    Left = 119
    Top = 59
    Width = 278
    Height = 21
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 30
    TabOrder = 4
    OnExit = txtApellidoExit
  end
  object btn_Buscar: TBitBtn
    Left = 244
    Top = 6
    Width = 77
    Height = 25
    Caption = '&Buscar'
    TabOrder = 1
    Visible = False
    OnClick = btn_BuscarClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33033333333333333F7F3333333333333000333333333333F777333333333333
      000333333333333F777333333333333000333333333333F77733333333333300
      033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
      33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
      3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
      33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
      333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
      333333773FF77333333333370007333333333333777333333333}
    NumGlyphs = 2
  end
  object txtFechaNacimiento: TDateEdit
    Left = 119
    Top = 159
    Width = 101
    Height = 21
    AutoSelect = False
    TabOrder = 12
    Date = -693594.000000000000000000
  end
  object cb_Sexo: TComboBox
    Left = 524
    Top = 84
    Width = 117
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 7
    Items.Strings = (
      '(Ninguno)'
      
        'Masculino                                                       ' +
        '                      M'
      
        'Femenino                                                        ' +
        '                      F')
  end
  object cbSituacionIVA: TComboBox
    Left = 524
    Top = 109
    Width = 278
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 9
  end
  object txtApellidoMaterno: TEdit
    Left = 524
    Top = 59
    Width = 278
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 5
    OnExit = txtApellidoMaternoExit
  end
  object txt_Documento: TEdit
    Left = 119
    Top = 34
    Width = 117
    Height = 21
    Hint = 'N'#250'mero de documento'
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 9
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnChange = txt_DocumentoChange
    OnKeyPress = txt_DocumentoKeyPress
  end
  object txtRazonSocial: TEdit
    Left = 524
    Top = 34
    Width = 277
    Height = 21
    Hint = 'Raz'#243'n Social'
    CharCase = ecUpperCase
    Color = 16444382
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnExit = txtRazonSocialExit
  end
  object txt_email: TEdit
    Left = 524
    Top = 134
    Width = 278
    Height = 21
    MaxLength = 30
    TabOrder = 11
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 324
    Top = 6
  end
end
