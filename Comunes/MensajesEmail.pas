{********************************** File Header ********************************
Revision 1
Author: nefernandez
Date: 02/02/2009
Description: SS 781: Se agregan los campos NombreArchivo y EliminarArchivo al registro
de Mensaje.

Revision 2
Author: Nelson Droguett Sierra
Date: 09/03/2009
Description: Se cambia el simbolo de parseo de parametros , de %1%

Revision 3
Author: Nelson Droguett Sierra
Date: 18-Agosto-2010
Description: Se agrega la firma, el bit de archivo adjunto de la plantilla, el contenttype
             y los reintentos al objeto TMensaje


    Firma       :   SS_1354_MBE_20150731
    Description :   Se agrega que devuelva el codigo de plantilla, para descubrir
					los errores de 'String de parametros malformados' (Comunes\MensajesEmail.pas)

Etiqueta    :   20160530 MGO
Descripci�n :   Se agrega filtro y parseo espec�fico para Correspondencia

*******************************************************************************}
unit MensajesEmail;

interface

uses sysutils, dmMensaje, util, variants, StrUtils;

type
    TMensaje = record
        Para: string;
        Asunto: string;
        Mensaje: string;
        CodigoConvenio: integer;
        FechaCreacion: tdatetime;
        FechaEnvio: tdatetime;
        Enviado: boolean;
        NombreArchivo: string;      //Revision 1
        EliminarArchivo: boolean;   //Revision 1
        Firma: string;              //Revision 3
        Reintentos: integer;        //Revision 3
        ArchivoAdjunto: boolean;    //Revision 3
        ContentType: string;
        EmailSender: string;        //SS_1075_MCO_20130131
        ClaveSender: string;        //SS_1075_MCO_20130131
        NameSender : string;
        SMTP: string;
        Port: Integer;
    end;

function ObtenerMensaje(dm: TDMMensajes; CodigoMensaje: integer; var Mensaje: TMensaje; var Error: string): boolean;
// INICIO : 20160530 MGO
function ObtenerCorrespondencia(dm: TDMMensajes; CodigoMensaje: integer; var Mensaje: TMensaje; var Error: string): boolean;
// FIN : 20160530 MGO
function ImportarMensajesDesdeArchivo(dm: TDMMensajes; Archivo: string; var Error: string): boolean;
function ProcesarParametros(CodigoPlantilla : integer; Plantilla, Parametros: string): string;              // SS_1354_MBE_20150731

implementation

function DeQuote(texto: string): string;
begin
    result := trim(Texto);
    if Texto = '' then exit;
    if result[1] = '"' then result := copy(result, 2, length(result));
    if result[length(result)] = '"' then result := copy(result, 1, length(result) - 1);
end;

function ImportarLinea(dm: TDMMensajes; Linea: string; var Error: string): boolean;
var
    pospc,
    CodigoPlantilla,
    CodigoConvenio,
    CodigoFirma: integer;
    Email,
    Parametros: string;
begin
    result := true;
    // si es una linea en blanco...
    if trim(Linea) = '' then exit;
    // o si es un comentario...
    if trim(Linea)[1] = '#' then exit;

    result := false;
    // me fijo que contenga los 3 parametros obligatorios: Plantilla, firma y email
    CodigoPlantilla := StrToIntDef(ParseParamByNumber(Linea, 1, ';'), -1);
    if CodigoPlantilla = -1 then begin
        Error := 'Codigo de plantilla inv�lido';
        exit;
    end;

    CodigoFirma := StrToIntDef(ParseParamByNumber(Linea, 2, ';'), -1);
    if CodigoFirma = -1 then begin
        Error := 'Codigo de firma inv�lido';
        exit;
    end;

    Email := Dequote(ParseParamByNumber(Linea, 3, ';'));
    if not IsValidEMail(Email) then begin
        Error := 'Email inv�lido';
        exit;
    end;

    CodigoConvenio := StrToIntDef(ParseParamByNumber(Linea, 4, ';'), -1);

    // los parametros es todo lo que sobrepase el tercer punto y coma
    parametros := '';

    // busco el primer punto y coma
    pospc := pos(';', Linea);
    // corto hasta el primer punto y coma
    Linea := copy(linea, pospc + 1, length(linea));

    // busco el segundo punto y coma
    pospc := pos(';', Linea);
    // corto hasta el segundo punto y coma
    Linea := copy(linea, pospc + 1, length(linea));

    // busco el tercer punto y coma
    pospc := pos(';', Linea);
    // a partir de aca no es obligatorio, asi que tengo que ver si esta el parametro
    if pospc <> 0 then begin
        // corto hasta el tercer punto y coma
        Linea := copy(linea, pospc + 1, length(linea));

        // busco el cuarto punto y coma
        pospc := pos(';', Linea);
        if pospc <> 0 then begin
            // si habia parametros...
            // corto hasta el cuarto punto y coma
            Parametros := copy(linea, pospc + 1, length(linea));
        end;
    end;

    try
        with dm.spagregarmensaje do begin
            parameters.ParamByName('@Email').value := Email;
            parameters.ParamByName('@CodigoPlantilla').value := CodigoPlantilla;
            parameters.ParamByName('@CodigoFirma').value := CodigoFirma;
            parameters.ParamByName('@CodigoConvenio').value := iif(CodigoConvenio = -1, NULL, CodigoConvenio);
            parameters.ParamByName('@Parametros').value := Parametros;

            execproc;
        end;
    except
        on e: exception do begin
            error := e.Message;
            exit;
        end;
    end;

    result := true;
end;


function ImportarMensajesDesdeArchivo(dm: TDMMensajes; Archivo: string; var Error: string): boolean;
var
    ArchivoAImportar,
    ArchivoErrores : text;
    LineaAProcesar: string;
begin
    result := false;
    assignfile(ArchivoAImportar, Archivo);
    assignfile(ArchivoErrores, Archivo + '.err');
    try
        reset(ArchivoAImportar);
        rewrite(ArchivoErrores);

        while not eof(ArchivoAImportar) do begin
            Readln(ArchivoAImportar, LineaAProcesar);
            if not ImportarLinea(dm, LineaAProcesar, Error) then begin
                writeln(ArchivoErrores, '# Error: ' + Error);
                writeln(ArchivoErrores, LineaAProcesar);
                writeln(ArchivoErrores);
            end;
        end;


        result := true;
    except
        on e: exception do begin
            Error := e.Message;
        end;
    end;
    close(ArchivoAImportar);
    close(ArchivoErrores);
end;

function ProcesarParametros(CodigoPlantilla: integer; Plantilla, Parametros: string): string;           // SS_1354_MBE_20150731
var
    i: integer;
    p: integer;
    Etiqueta: string;
    EsperarComilla: boolean;
begin
    result := Plantilla;
    i := 1;
    Parametros := trim(Parametros);
    Etiqueta := '';
    EsperarComilla := false;

    while Parametros <> '' do begin
        if Parametros[1] = '"' then begin
            // el parametro esta encomillado
            EsperarComilla := true;
            Parametros := copy(Parametros, 2, length(Parametros));
        end;

        // busco el siguiente separador
        p := pos(iif(EsperarComilla, '"', ';'), Parametros);
        if EsperarComilla and (Parametros[p + 1] = '"') then begin
            // era una doble comilla, escapada para indicarnos una sola
            Etiqueta := Etiqueta + copy(Parametros, 1, p - 1);
            Parametros := copy(Parametros, p + 2, length(Parametros));
            // sigo buscando el final
            continue;
        end;

        if p = 0 then begin
            // si no encotramos el final, cargamos hasta lo ultimo
            Etiqueta := Etiqueta + copy(Parametros, 1, length(Parametros));
            Parametros := '';
        end else begin
            Etiqueta := Etiqueta + copy(Parametros, 1, p - 1);
            Parametros := copy(Parametros, p + iif(EsperarComilla, 1, 0), length(Parametros));
        end;
        result := StringReplace(result, '%' + inttostr(i) + '%', Etiqueta, [rfReplaceAll]);
        // Rev.2 / 09-03-2009 / Nelson Droguett Sierra.
        result := StringReplace(result, '${' + trim(inttostr(i)) + '};', Etiqueta, [rfReplaceAll]);

        inc(i);

        Parametros := trim(Parametros);

        if EsperarComilla and (Parametros <> '') and (Parametros[1] <> ';') then begin                                        // SS_1354_MBE_20150731
            raise(exception.Create('String de par�metros malformados. C�digo Plantilla: ' + IntToStr(CodigoPlantilla)));      // SS_1354_MBE_20150731
        end;                                                                                                                  // SS_1354_MBE_20150731

        // saco el punto y coma del principio
        Parametros := copy(Parametros, 2, length(Parametros));
        Parametros := trim(Parametros);

        EsperarComilla := false;
        Etiqueta := '';
    end;
end;

function ObtenerMensaje(dm: TDMMensajes; CodigoMensaje: integer; var Mensaje: TMensaje; var Error: string): boolean;
begin
    result := false;
    try
        with dm.spObtenerMensaje do begin
            dm.spObtenerMensaje.Close;
            parameters.ParamByName('@CodigoMensaje').value := CodigoMensaje;
            dm.spObtenerMensaje.Open;
            if eof then begin
                Error := 'El mensaje no existe';
            end else begin
                Mensaje.SMTP := fieldbyname('SMTP_Sender').AsString;
                Mensaje.Port := fieldbyname('Port_Sender').AsInteger;

                Mensaje.Para := fieldbyname('Email').AsString;
                Mensaje.Asunto := fieldbyname('Asunto').AsString;
                Mensaje.Mensaje := fieldbyname('Plantilla').AsString ; //+ CRLF + fieldbyname('Firma').AsString;
                Mensaje.NameSender := FieldByName('Name_Sender').AsString;
                Mensaje.EmailSender := FieldByName('Email_Sender').AsString;
                Mensaje.ClaveSender := FieldByName('Clave_Sender').AsString;
                Mensaje.Mensaje := ProcesarParametros(FieldByName('CodigoPlantilla').AsInteger, Mensaje.Mensaje, fieldbyname('Parametros').AsString);
                if fieldbyname('CodigoConvenio').IsNull then begin
                    Mensaje.CodigoConvenio := 0;
                end else begin
                    Mensaje.CodigoConvenio := fieldbyname('CodigoConvenio').AsInteger;
                end;

                if fieldbyname('FechaEnvio').IsNull then begin
                    Mensaje.FechaEnvio := NULLDATE;
                end else begin
                    Mensaje.FechaEnvio := fieldbyname('FechaEnvio').AsDateTime;
                end;

                Mensaje.FechaCreacion := fieldbyname('FechaCreacion').AsDateTime;
                Mensaje.Enviado := fieldbyname('Enviado').AsBoolean;
                //Revision 1
                Mensaje.NombreArchivo := fieldbyname('NombreArchivo').AsString;
                Mensaje.EliminarArchivo := fieldbyname('EliminarArchivo').AsBoolean;
                //Rev.3 / 18-Agosto-2010 / Nelson Droguett Sierra ---------------------
                Mensaje.Firma := fieldbyname('Firma').AsString;
                Mensaje.Reintentos := fieldbyname('Reintentos').AsInteger;
                Mensaje.ArchivoAdjunto := FieldByName('ArchivoAdjunto').AsBoolean;
                Mensaje.ContentType := IfThen(FieldByName('ContentType').AsInteger=1,'text/plain','text/html');
                result := true;
            end;
        end;
    except
        on e: exception do begin
            Error := e.Message;
        end;
    end;
end;

// INICIO : 20160530 MGO
function ObtenerCorrespondencia(dm: TDMMensajes; CodigoMensaje: integer; var Mensaje: TMensaje; var Error: string): boolean;
    function ProcesarEncabezado(Plantilla: String; CodigoMensaje, CodigoConvenio, CodigoPersona: Integer; FechaEnvio: TDateTime): String;
    resourcestring
        QRY_OBTENER_NUMERO_CONVENIO = 'SELECT RTRIM(dbo.ObtenerNumeroConvenio(%d))';
        STR_LOGO_INTERVIAL = 'Logo_Intervial_Maipo';
    var
        Calle, Numero, Piso, Dpto, Comuna, Region, Direccion: String;
        Apellido, ApellidoMaterno, Nombre, NombreCompleto, Titulo, RazonSocial: String;
        vLogo: String;
        NumeroConvenio: String;
    begin
        Result := Plantilla;

        with dm.spObtenerDomicilioConvenio do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            Open;

            if not IsEmpty then begin
                Calle := Trim(FieldByName('DescriCalle').AsString);
                if Calle = EmptyStr then Calle := Trim(FieldByName('CalleDesnormalizada').AsString);
                Numero := Trim(FieldByName('Numero').AsString);
                Piso := Trim(FieldByName('Piso').AsString);
                Dpto := Trim(FieldByName('Dpto').AsString);
                Comuna := Trim(FieldByName('DescriComuna').AsString);
                Region := Trim(FieldByName('DescriRegion').AsString);
            end;
        end;

        Direccion := Calle + ' ' + Numero;
        if (Piso <> EmptyStr) and (Piso <> '0') then
            Direccion := Direccion + ', piso ' + Piso;
        if Dpto <> EmptyStr then
            Direccion := Direccion + ', ' + Dpto;

        with dm.spObtenerDatosPersona do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            Open;

            if not IsEmpty then begin
                Apellido := Trim(FieldByName('Apellido').AsString);
                ApellidoMaterno := Trim(FieldByName('ApellidoMaterno').AsString);
                Nombre := Trim(FieldByName('Nombre').AsString);
                RazonSocial := Trim(FieldByName('Giro').AsString);
                Titulo := iif(Trim(FieldByName('Sexo').AsString) = 'F', 'Sra.', 'Sr.');
            end;
        end;

        with dm.spObtenerParametroGeneral do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@Nombre').Value := STR_LOGO_INTERVIAL;
            Parameters.ParamByName('@Valor').Value := Null;
            Parameters.ParamByName('@TipoParametro').Value := Null;
            ExecProc;

            vLogo := Parameters.ParamByName('@Valor').Value;
        end;

        NombreCompleto := Apellido;
        if ApellidoMaterno <> EmptyStr then
            NombreCompleto := NombreCompleto + ' ' + ApellidoMaterno;
        NombreCompleto := NombreCompleto + ', ' + Nombre;

        NumeroConvenio := dm.QueryGetValueString(Format(QRY_OBTENER_NUMERO_CONVENIO, [CodigoConvenio]));

        Result := StringReplace(Result, '%Logo_Intervial_Maipo%', vLogo, [rfIgnoreCase, rfReplaceAll]);
        Result := StringReplace(Result, '%CodigoMensaje%', IntToStr(CodigoMensaje), [rfReplaceAll]);
        Result := StringReplace(Result, '%CodigoConvenio%', NumeroConvenio, [rfReplaceAll]);
        Result := StringReplace(Result, '%NumeroConvenio%', NumeroConvenio, [rfReplaceAll]);
        Result := StringReplace(Result, '%FechaEnvio%', FormatDateTime('dd-mm-yyyy', FechaEnvio), [rfReplaceAll]);
        Result := StringReplace(Result, '%Direccion%', Direccion, [rfReplaceAll]);
        Result := StringReplace(Result, '%Comuna%', Comuna, [rfReplaceAll]);
        Result := StringReplace(Result, '%Ciudad%', Region, [rfReplaceAll]);
        Result := StringReplace(Result, '%Titulo%', Titulo, [rfReplaceAll]);
        Result := StringReplace(Result, '%NombreCompleto%', NombreCompleto, [rfReplaceAll]);
        Result := StringReplace(Result, '%RazonSocial%', RazonSocial, [rfReplaceAll]);
    end;
begin
    result := false;

    try
        with dm.spObtenerCorrespondencia do begin
            Close;
            Parameters.ParamByName('@CodigoMensaje').value := CodigoMensaje;
            Parameters.ParamByName('@ErrorDescription').value := Null;
            Open;
            if eof then begin
                Error := 'El mensaje no existe';
            end else begin
                Mensaje.Para := fieldbyname('Email').AsString;
                Mensaje.Asunto := fieldbyname('Asunto').AsString;
                Mensaje.EmailSender := FieldByName('Email_Sender').AsString;
                Mensaje.ClaveSender := FieldByName('Clave_Sender').AsString;
                if fieldbyname('CodigoConvenio').IsNull then begin
                    Mensaje.CodigoConvenio := 0;
                end else begin
                    Mensaje.CodigoConvenio := fieldbyname('CodigoConvenio').AsInteger;
                end;

                if fieldbyname('FechaEnvio').IsNull then begin
                    Mensaje.FechaEnvio := Now;
                end else begin
                    Mensaje.FechaEnvio := fieldbyname('FechaEnvio').AsDateTime;
                end;

                Mensaje.FechaCreacion := fieldbyname('FechaCreacion').AsDateTime;
                Mensaje.Enviado := fieldbyname('Enviado').AsBoolean;
                Mensaje.Firma := fieldbyname('Firma').AsString;
                Mensaje.Reintentos := fieldbyname('Reintentos').AsInteger;
                Mensaje.ArchivoAdjunto := False;
                Mensaje.ContentType := IfThen(FieldByName('ContentType').AsInteger=1,'text/plain','text/html');

                Mensaje.Mensaje := FieldByName('Plantilla').AsString;
                Mensaje.Mensaje := ProcesarParametros(FieldByName('CodigoPlantilla').AsInteger,
                                                        Mensaje.Mensaje,
                                                        fieldbyname('Parametros').AsString);       
                Mensaje.Mensaje := ProcesarEncabezado(Mensaje.Mensaje,
                                                        CodigoMensaje,
                                                        Fieldbyname('CodigoConvenio').AsInteger,
                                                        Fieldbyname('CodigoPersona').AsInteger,
                                                        Mensaje.FechaEnvio);

                Result := True;
            end;
        end;
    except
        on e: exception do begin
            Error := e.Message;
        end;
    end;
end;
// FIN : 20160530 MGO

end.
