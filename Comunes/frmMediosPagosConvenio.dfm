object FormMediosPagosConvenio: TFormMediosPagosConvenio
  Left = 169
  Top = 189
  BorderStyle = bsDialog
  Caption = 'Gestion de Datos de Medios de Pago'
  ClientHeight = 272
  ClientWidth = 777
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    777
    272)
  PixelsPerInch = 96
  TextHeight = 13
  object Notebook: TNotebook
    Left = 0
    Top = 0
    Width = 777
    Height = 97
    Align = alTop
    PageIndex = 1
    TabOrder = 0
    object TPage
      Left = 0
      Top = 0
      Caption = 'PAT'
      object gb_PAT: TGroupBox
        Left = 4
        Top = -1
        Width = 770
        Height = 89
        Caption = ' PAT '
        TabOrder = 0
        object lbl_FechaVencimiento: TLabel
          Left = 392
          Top = 48
          Width = 167
          Height = 13
          Caption = '&Fecha Vencimiento (MM/AA):'
          FocusControl = txt_FechaVencimiento
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl_EmisorTarjeta: TLabel
          Left = 392
          Top = 24
          Width = 104
          Height = 13
          Caption = '&Emisor de Tarjeta:'
          Color = clBtnFace
          FocusControl = cb_EmisoresTarjetasCreditos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lbl_TipoTarjeta: TLabel
          Left = 8
          Top = 24
          Width = 92
          Height = 13
          Caption = '&Tipo de Tarjeta:'
          Color = clBtnFace
          FocusControl = cb_TipoTarjeta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lbl_NumeroTarjetaCredito: TLabel
          Left = 8
          Top = 48
          Width = 136
          Height = 13
          Caption = '&N'#250'mero Tarjeta Cr'#233'dito:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txt_FechaVencimiento: TMaskEdit
          Left = 570
          Top = 43
          Width = 37
          Height = 21
          Hint = 'Fecha de Vencimiento de la Tarjeta de Cr'#233'dito'
          Color = 16444382
          EditMask = '00\/00;1; '
          MaxLength = 5
          TabOrder = 1
          Text = '00-  '
        end
        object cb_EmisoresTarjetasCreditos: TComboBox
          Left = 503
          Top = 19
          Width = 200
          Height = 21
          Hint = 'Emisor de la Tarjeta de Cr'#233'dito'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
          OnClick = cb_EmisoresTarjetasCreditosClick
        end
        object cb_TipoTarjeta: TComboBox
          Left = 153
          Top = 19
          Width = 200
          Height = 21
          Hint = 'Tipo de Tarjeta'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 2
          OnChange = cb_TipoTarjetaChange
          OnClick = cb_TipoTarjetaClick
          OnExit = cb_TipoTarjetaExit
        end
        object txt_NroTarjetaCredito: TEdit
          Left = 153
          Top = 43
          Width = 200
          Height = 21
          Hint = 'N'#250'mero de Tarjeta de Cr'#233'dito'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 20
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnChange = txt_NroTarjetaCreditoChange
          OnExit = txt_NroTarjetaCreditoExit
          OnKeyPress = txt_NroTarjetaCreditoKeyPress
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      HelpContext = 1
      Caption = 'PAC'
      object gb_PAC: TGroupBox
        Left = 3
        Top = 0
        Width = 770
        Height = 89
        Caption = ' PAC '
        TabOrder = 0
        object lbl_TipoCuenta: TLabel
          Left = 16
          Top = 24
          Width = 92
          Height = 13
          Caption = '&Tipo de Cuenta:'
          Color = clBtnFace
          FocusControl = cb_TipoCuenta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lbl_Banco: TLabel
          Left = 368
          Top = 24
          Width = 41
          Height = 13
          Caption = '&Banco:'
          Color = clBtnFace
          FocusControl = cb_Banco
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lbl_NumeroCuenta: TLabel
          Left = 16
          Top = 48
          Width = 92
          Height = 13
          Caption = '&N'#250'mero Cuenta:'
          Color = clBtnFace
          FocusControl = txt_NumeroCuenta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lbl_Sucursal: TLabel
          Left = 368
          Top = 48
          Width = 54
          Height = 13
          Caption = '&Sucursal:'
          Color = clBtnFace
          FocusControl = txt_Sucursal
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object cb_TipoCuenta: TComboBox
          Left = 115
          Top = 19
          Width = 200
          Height = 21
          Hint = 'Tipo de Cuenta'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
          OnClick = cb_TipoCuentaClick
        end
        object cb_Banco: TComboBox
          Left = 427
          Top = 19
          Width = 254
          Height = 21
          Hint = 'Nombre del Banco'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 1
          OnClick = cb_BancoClick
        end
        object txt_NumeroCuenta: TEdit
          Left = 115
          Top = 44
          Width = 198
          Height = 21
          Hint = 'N'#250'mero de Cuenta'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 30
          TabOrder = 2
          OnChange = txt_NumeroCuentaChange
          OnKeyPress = txt_NumeroCuentaKeyPress
        end
        object txt_Sucursal: TEdit
          Left = 427
          Top = 44
          Width = 254
          Height = 21
          Hint = 'Nombre de la Sucursal'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 30
          TabOrder = 3
        end
      end
    end
  end
  object gb_DatosPersonales: TGroupBox
    Left = 3
    Top = 94
    Width = 771
    Height = 113
    Caption = ' Datos del Mandante '
    TabOrder = 2
    object lbl_PersoneriaMandante: TLabel
      Left = 16
      Top = 24
      Width = 67
      Height = 13
      Caption = 'Personer'#237'a:'
      FocusControl = cbPersoneria
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRutRun: TLabel
      Left = 299
      Top = 24
      Width = 31
      Height = 13
      Caption = 'RUT:'
      FocusControl = txtDocumento
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnDatosConvenio: TSpeedButton
      Left = 456
      Top = 20
      Width = 22
      Height = 19
      Hint = 'Tomar datos del convenio'
      Glyph.Data = {
        AA040000424DAA04000000000000360000002800000013000000130000000100
        1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9EC840000840000840000840000840000840000840000840000840000D8E9
        ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
        D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
        FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
        0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
        EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
        0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
        FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
        840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
        00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
        E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
        ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
        0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
        00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
        0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
        EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9EC000000}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnDatosConvenioClick
    end
    object Pc_Datos: TPageControl
      Left = 2
      Top = 41
      Width = 763
      Height = 33
      ActivePage = Tab_Fisica
      Style = tsFlatButtons
      TabOrder = 2
      OnEnter = Pc_DatosEnter
      object Tab_Juridica: TTabSheet
        Caption = 'Tab_Juridica'
        TabVisible = False
        object lbl_RazonSocial: TLabel
          Left = 10
          Top = 6
          Width = 80
          Height = 13
          Caption = 'Raz'#243'n Social:'
          FocusControl = txt_RazonSocial
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txt_RazonSocial: TEdit
          Left = 109
          Top = 2
          Width = 364
          Height = 21
          Hint = 'Raz'#243'n Social'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 60
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnKeyPress = txt_NroTarjetaCreditoKeyPress
        end
      end
      object Tab_Fisica: TTabSheet
        Caption = 'Tab_Fisica'
        ImageIndex = 1
        TabVisible = False
        object lbl_ApellidoMaternoMandante: TLabel
          Left = 502
          Top = 5
          Width = 82
          Height = 13
          Caption = 'Apellido Materno:'
          FocusControl = txtApellidoMaterno
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbl_ApellidoMandante: TLabel
          Left = 274
          Top = 5
          Width = 50
          Height = 13
          Caption = 'Apellido:'
          FocusControl = txtApellido
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl_NombreMandante: TLabel
          Left = 10
          Top = 5
          Width = 48
          Height = 13
          Caption = 'Nombre:'
          FocusControl = txtNombre
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txtApellidoMaterno: TEdit
          Left = 589
          Top = 1
          Width = 166
          Height = 21
          Hint = 'Apellido materno'
          CharCase = ecUpperCase
          MaxLength = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnKeyPress = txt_NroTarjetaCreditoKeyPress
        end
        object txtApellido: TEdit
          Left = 332
          Top = 1
          Width = 155
          Height = 21
          Hint = 'Apellido paterno'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 60
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnKeyPress = txt_NroTarjetaCreditoKeyPress
        end
        object txtNombre: TEdit
          Left = 109
          Top = 1
          Width = 155
          Height = 21
          Hint = 'Nombre'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 60
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnKeyPress = txt_NroTarjetaCreditoKeyPress
        end
      end
    end
    object cbPersoneria: TComboBox
      Left = 115
      Top = 20
      Width = 158
      Height = 21
      Hint = 'Tipo de Personer'#237'a'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = cbPersoneriaChange
      Items.Strings = (
        'F'#237'sica                          F'
        'Jur'#237'dica                       J')
    end
    object txtDocumento: TEdit
      Left = 338
      Top = 20
      Width = 105
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = txtDocumentoChange
      OnKeyDown = txtDocumentoKeyDown
    end
    inline FrameTelefono: TFrameTelefono
      Left = 3
      Top = 72
      Width = 625
      Height = 27
      TabOrder = 3
      TabStop = True
      ExplicitLeft = 3
      ExplicitTop = 72
      ExplicitWidth = 625
      ExplicitHeight = 27
      inherited lblTelefono: TLabel
        Left = 16
        Top = 7
        Width = 52
        Caption = 'Tel'#233'fono:'
        ExplicitLeft = 16
        ExplicitTop = 7
        ExplicitWidth = 52
      end
      inherited lblDesde: TLabel
        Left = 408
        ExplicitLeft = 408
      end
      inherited lblHasta: TLabel
        Left = 511
        ExplicitLeft = 511
      end
      inherited txtTelefono: TEdit
        Left = 169
        Width = 98
        ExplicitLeft = 169
        ExplicitWidth = 98
      end
      inherited cbTipoTelefono: TVariantComboBox
        Left = 270
        ExplicitLeft = 270
      end
      inherited cbDesde: TComboBox
        Left = 445
        ExplicitLeft = 445
      end
      inherited cbHasta: TComboBox
        Left = 547
        ExplicitLeft = 547
      end
      inherited cbCodigosArea: TComboBox
        Left = 112
        Hint = 'C'#243'digo de '#193'rea'
        ExplicitLeft = 112
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 212
    Width = 777
    Height = 28
    Align = alBottom
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    object Me_Mensaje: TLabel
      Left = 0
      Top = 0
      Width = 777
      Height = 28
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = 
        'Este Mandato, para entrar en vigencia, deber'#225' ser firmado en ori' +
        'ginal por el Titular de la Cuenta/Tarjeta.'
      Layout = tlCenter
    end
  end
  object pnlBotton: TPanel
    Left = 0
    Top = 240
    Width = 777
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object btnAceptar: TButton
      Left = 570
      Top = 4
      Width = 95
      Height = 25
      Hint = 'Guardar Datos'
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnSalir: TButton
      Left = 675
      Top = 4
      Width = 92
      Height = 25
      Hint = 'Salir de la Carga'
      Cancel = True
      Caption = '&Salir'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object pnl_Confirmacion: TPanel
    Left = 7
    Top = 65
    Width = 761
    Height = 19
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 1
    object lbl_EstadoTipoMedioPago: TLabel
      Left = 272
      Top = 4
      Width = 200
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Confirmado / No Confirmado'
      Color = clBtnFace
      FocusControl = cb_Banco
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object chk_ConfirmarTipoMedioPago: TCheckBox
      Left = 12
      Top = 1
      Width = 137
      Height = 17
      Caption = 'Forzar Confirmaci'#243'n'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = chk_ConfirmarTipoMedioPagoClick
    end
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona'
    Parameters = <>
    Left = 575
    Top = 65534
  end
  object ObtenerMedioComunicacionPricipalPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMedioComunicacionPricipalPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 608
    Top = 65534
  end
  object VerificarMedioPagoAutomatico: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarMedioPagoAutomatico'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@TipoMedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenioEditado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoCovenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 728
    Top = 152
  end
  object qry_prefijos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTipoTarjetaCredito'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        '  TarjetasCreditoPrefijos.Prefijo, TarjetasCreditoTipos.ValidarP' +
        'refijo'
      'FROM'
      '  TarjetasCreditoPrefijos  WITH (NOLOCK),'
      '  TarjetasCreditoTipos  WITH (NOLOCK)'
      'WHERE'
      
        '  TarjetasCreditoPrefijos.CodigoTipoTarjetaCredito = TarjetasCre' +
        'ditoTipos.CodigoTipoTarjetaCredito'
      ''
      
        'AND TarjetasCreditoPrefijos.CodigoTipoTarjetaCredito = :CodigoTi' +
        'poTarjetaCredito')
    Left = 428
    Top = 180
  end
end
