object FNumeroReclamoAsignado: TFNumeroReclamoAsignado
  Left = 359
  Top = 247
  BorderIcons = [biSystemMenu]
  Caption = 'Sistema de Casos'
  ClientHeight = 131
  ClientWidth = 275
  Color = clBtnFace
  Constraints.MaxHeight = 169
  Constraints.MaxWidth = 291
  Constraints.MinHeight = 169
  Constraints.MinWidth = 291
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Ltitulo: TLabel
    Left = 0
    Top = 30
    Width = 275
    Height = 13
    Align = alTop
    Alignment = taCenter
    Caption = 'Se le asign'#243' el N'#250'mero de Caso'
    ExplicitWidth = 151
  end
  object LNumero: TLabel
    Left = 0
    Top = 52
    Width = 275
    Height = 20
    Align = alTop
    Alignment = taCenter
    Caption = '00000'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitWidth = 51
  end
  object AceptarBTN: TButton
    Left = 104
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    ModalResult = 1
    TabOrder = 0
    OnClick = AceptarBTNClick
  end
  object P1: TPanel
    Left = 0
    Top = 0
    Width = 275
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
  end
  object P2: TPanel
    Left = 0
    Top = 43
    Width = 275
    Height = 9
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
end
