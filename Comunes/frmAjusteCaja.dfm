object FormAjusteCaja: TFormAjusteCaja
  Left = 437
  Top = 228
  BorderStyle = bsDialog
  Caption = 'Ajuste de Monto en Caja'
  ClientHeight = 179
  ClientWidth = 346
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object bvl_Datos: TBevel
    Left = 8
    Top = 8
    Width = 322
    Height = 124
  end
  object lbl_Usuario: TLabel
    Left = 71
    Top = 51
    Width = 39
    Height = 13
    Caption = 'Usuario:'
  end
  object lbl_NumeroTurno: TLabel
    Left = 71
    Top = 76
    Width = 58
    Height = 13
    Caption = 'N'#176' de Turno'
  end
  object lbl_Encabezado: TLabel
    Left = 24
    Top = 24
    Width = 272
    Height = 13
    Caption = 'Ingrese los datos del turno para cambiar el monto en caja.'
  end
  object pnl_Botones: TPanel
    Left = 0
    Top = 148
    Width = 346
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      346
      31)
    object btn_Aceptar: TButton
      Left = 181
      Top = 0
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TButton
      Left = 262
      Top = 0
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object cb_Usuario: TComboBox
    Left = 136
    Top = 48
    Width = 130
    Height = 21
    Hint = 'Seleccione un usuario para poder seleccionar sus turnos'
    Style = csDropDownList
    ItemHeight = 0
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object edt_NumeroTurno: TBuscaTabEdit
    Left = 135
    Top = 75
    Width = 130
    Height = 21
    Hint = 'N'#250'mero del Turno para el cual obtener el informe'
    AutoSize = False
    Color = 16444382
    Enabled = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    EditorStyle = bteNumericEdit
    OnButtonClick = edt_NumeroTurnoButtonClick
    BuscaTabla = bsctbl_Turnos
  end
  object sp_ObtenerListaTurnos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListaTurnos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = ''
      end>
    Left = 20
    Top = 132
  end
  object bsctbl_Turnos: TBuscaTabla
    Caption = 'Turnos Cerrados'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = sp_ObtenerListaTurnos
    OnProcess = bsctbl_TurnosProcess
    OnSelect = bsctbl_TurnosSelect
    Left = 112
    Top = 132
  end
  object sp_EsTurnoCerrado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EsTurnoCerrado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ExisteTurno'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@EsCerrado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@UsuarioTurno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@PuntoVenta'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 60
        Value = Null
      end>
    Left = 62
    Top = 132
  end
end
