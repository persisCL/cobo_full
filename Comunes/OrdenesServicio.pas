{-------------------------------------------------------------------------------
 File Name: OrdenesServicio.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description:  Rutinas que utilizan las ordenes de servicio

 Revision : 1
     Author : dallegretti
     Date : 23/05/2008
     Description : Se le agreg� al stored ActualizarTransitosReclamosAceptados
                    el par�metro @Usuario.
 Revision 2 :
    Author : lcanteros
    Date : 21/07/2008
    Description : Modificaciones SS 327, se agrega los campos Usuario Creador y
    Numero de convenio en la lista de Reclamos recibidos.
 Revision 3 :
    Author : mpiazza
    Date : 26-01-2009
    Description : Modificaciones SS 689, Modificar el mensaje de lock de
    convenio MSG_LOCK_ERROR

  Revision 4:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

  Revision 5:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

  Revision 6:
    Date: 29-Junio-2010
    Author: Nelson Droguett Sierra
    Description:  Ref (Fase 2) Se agrega a los reclamos, la concesionaria y el SubTipo

Etiqueta	: 20160307 MGO
Descripci�n	: Se usa BaseBO_Master para SP
-------------------------------------------------------------------------------}
unit OrdenesServicio;

interface

Uses
    //Ordenes Servicio
    DMConnection,                //Coneccion a base de datos OP_CAC
    Util,                        //IIF
    UtilProc,                    //Mensajes
    UtilDB,                      //Rutinas para base de datos
    RStrings,                    //Resource Strings
    FreContactoReclamo,          //Frame contacto
    FreCompromisoOrdenServicio,  //Frame compromiso
    FreSolucionOrdenServicio,    //Frame Solucion
    FrmNumeroReclamoAsignado,    //Informa el numero de Reclamo Asignado
    //Rev.6 / 29-Junio-2010-----------------------------------------------------------
    FreConcesionariaReclamoOrdenServicio,
    //FreSubTipoReclamoOrdenServicio,       // _PAN_
	//FinRev.6------------------------------------------------------------------------
    //General
    Windows, SysUtils, Classes, Variants, Forms, AdoDB,VariantComboBox,Dialogs, DateUtils;

Type
    TOSNotifier = procedure of object;

    //Guardar Orden de Servicio
    //Rev.6 / 29-Junio-2010 / Nelson Droguett Sierra -----------------------------------
    function  GuardarOrdenServicioBase(CodigoOrdenServicio, TipoOrdenServicio, CodigoFuenteReclamo: Integer; DatosContacto: TFrameContactoReclamo; DatosCompromiso: TFrameCompromisoOrdenServicio; DatosSolucion: TFrameSolucionOrdenServicio; ObservacionesSolicitante: AnsiString; ObservacionesEjecutante: AnsiString; ObservacionesRespuesta: AnsiString; ObservacionesComentariosCliente: AnsiString; Notificado: Boolean; Conforme: Boolean;ConcesionariaReclamoOrdenServicio:TFrameConcesionariaReclamoOrdenServicio;  // _PAN_
              MD5txtDetalleRespuestaBefore, MD5txtDetalleRespuestaAfter, MD5TxtComentariosdelClienteBefore, MD5TxtComentariosdelClienteAfter: string {;SubTipoReclamoOrdenServicio:TFrameSubTipoReclamoOrdenServicio }): Integer;    // _PAN_
    function  GuardarOrdenServicioFacturacion(CodigoOrdenServicio: Integer;TipoComprobante: AnsiString; NumeroComprobante: Int64;FechaPago: TDateTime; LugarPago: AnsiString; SubtipoReclamo: Integer; NumeroRecibo:integer; CodigoResolucion:integer; RecibioComprobante, DireccionIncorrecta:boolean): Boolean;
    function  GuardarOrdenServicioCuenta(CodigoOrdenServicio: Integer; PatenteDesconocida, PatenteFaltante:string; FechaHoraDesconoce:tdatetime; CodigoMotivoDesconocePatente:integer; TipoComprobante: AnsiString; NumeroComprobante: Int64; ContextMark: Integer; ContractSerialNumber: Int64): Boolean;
    function  GuardarOrdenServicioTransito(CodigoOrdenServicio: Integer; NumCorrCA: String; ClienteRechaza:boolean; CodigoResolucionReclamoTransito:integer): Boolean;
    function  GuardarOrdenServicioDesconoceInfraccion(CodigoOrdenServicio: Integer;CodigoMotivoDesconoceInfraccion:integer; NumeroSerie:string; FechaCompra:Tdatetime; CodigoCategoria:integer; Patente: string; Etiqueta:string; CodigoConcesionaria:integer; NumeroConvenio:string): Boolean;
    function  GuardarOrdenServicioContactarCliente(CodigoOrdenServicio: Integer; Notificado, Conforme: boolean): Boolean;
    function  GuardarOrdenServicioEstacionamiento(CodigoOrdenServicio: Integer; NumCorrEstacionamiento: String; ClienteRechaza:boolean; CodigoResolucionReclamoEstacionamiento:integer): Boolean;

    //Actualizar
    function  ActualizarTransitosReclamosAceptados(CodigoOrdenServicio: Integer): Boolean;
    function  ActualizarEstacionamientosReclamosAceptados(CodigoOrdenServicio: Integer): Boolean;
    //Otras
    procedure EditarOrdenServicio(CodigoOrdenServicio: Integer);
    procedure SolucionarOrdenServicio(CodigoOrdenServicio: Integer);
    //Bloquear / Desbloquear OS
    function  BloquearOrdenServicio(CodigoOrdenServicio: Integer): Boolean;
    function  DesbloquearOrdenServicio(CodigoOrdenServicio: Integer): Boolean;
    function  DesbloquearOrdenServicioAdministrador(CodigoOrdenServicio: Integer): Boolean;
    //Obtener
    function  ObtenerDescripcionEstadoOS(Estado: AnsiString): AnsiString;
    function  ObtenerDescripcionPrioridadOS(Prioridad: Integer): AnsiString;
    function  ObtenerFechaComprometidaOS(Fecha: TDatetime; TipoOrdenServicio: integer; CodigoFuenteReclamo:integer; Prioridad: integer): Tdatetime;
    //otros
    procedure CargarFuentesReclamo(Combo:TVariantComboBox);
    Procedure InformarNumeroReclamo(NumeroReclamo:string);
    procedure CargarUsuarios(Combo:TVariantComboBox; AreaAtencion: Integer);
    //Permisos
    function  TienePermisoOrdenServicio(TipoOrdenServicio: Integer): Boolean;
    //Notify
    procedure AddOSNotification(OSProc: TOSNotifier);
    procedure RemoveOSNotification(OSProc: TOSNotifier);
    procedure NotifyOSChanged;
    //notify form
    procedure AddOSForm(OSForm: TForm);
    procedure RemoveOSForm(OSForm: TForm);


implementation

uses FrmReclamoGeneral,
     FrmReclamoFactura,
     FrmReclamoCuenta,
     FrmReclamoFaltaVehiculo,
     FrmReclamoTransito,
     FrmReclamoEstacionamiento,
     FrmReclamoTransitoDesconoceInfraccion,
     FrmReclamoContactarCliente;

resourcestring
    MSG_SAVE_ERROR_SERVICE_ORDER = 'Error guardando la orden de servicio:';
    MSG_LOCK_ERROR = 'No es posible bloquear esta orden de servicio para su edici�n. Ya esta siendo editada por el usuario ';
    MSG_ERROR_CAPTION = 'Bloquear Orden de Servicio';

Var
    GPermisos: TStringList;
    ProximaCargaPermisos: Int64 = 0;
    //Forms que seran notificados
    GOSNotifications: Array[1..100] of TOSNotifier; //Es una lista con los forms que desean ser notificados si
                                                    //cambia una orden de servicio en realidad apunta a la rutina
                                                    //en cada uno para actualizar la pantalla
    //Forms Abiertos
    GOSForms: Array[1..100] of TForm;               //Es una lista con forms abiertos para no volver a crearlos
                                                    //si existen

{-------------------------------------------------------------------------------------
  Function Name: GuardarOrdenServicioBase
  Author:   DDeMarco
  Date Created:
  Description: Permite guardar la informacion comun a todas las ordenes de servicio
  Parameters:
  Return Value: Integer
    Revision 1:
        Author : lcanteros
        Date : 21/07/2008
        Description : Se agrega el parametro UsuarioCreacion (SS 327)
-------------------------------------------------------------------------------------}
function GuardarOrdenServicioBase(CodigoOrdenServicio, TipoOrdenServicio, CodigoFuenteReclamo: Integer;
  DatosContacto: TFrameContactoReclamo; DatosCompromiso: TFrameCompromisoOrdenServicio;
  DatosSolucion: TFrameSolucionOrdenServicio; ObservacionesSolicitante: AnsiString; ObservacionesEjecutante: AnsiString;
  ObservacionesRespuesta: AnsiString; ObservacionesComentariosCliente: AnsiString; Notificado: Boolean; Conforme: Boolean;
  ConcesionariaReclamoOrdenServicio:TFrameConcesionariaReclamoOrdenServicio;                            // _PAN_
  MD5txtDetalleRespuestaBefore, MD5txtDetalleRespuestaAfter, MD5TxtComentariosdelClienteBefore, MD5TxtComentariosdelClienteAfter: string {;                          // _PAN_
  SubTipoReclamoOrdenServicio:TFrameSubTipoReclamoOrdenServicio}): Integer;                             // _PAN_
Var
    SP: TAdoStoredProc; ModificaHistorico: Boolean;
begin

    ModificaHistorico:= ((MD5txtDetalleRespuestaBefore <> MD5txtDetalleRespuestaAfter) or
        (MD5TxtComentariosdelClienteBefore <> MD5TxtComentariosdelClienteAfter));
    
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
	SP.ProcedureName := 'GuardarOrdenServicioBase';

	try
		result := 0;
		try
			SP.Parameters.Refresh;
			SP.Parameters.ParamByName('@TipoOrdenServicio').Value :=
			  TipoOrdenServicio;
			SP.Parameters.ParamByName('@CodigoConvenio').Value :=
			  iif(DatosContacto.CodigoConvenio = 0, NULL, DatosContacto.CodigoConvenio);
			SP.Parameters.ParamByName('@IndiceVehiculo').Value :=
			  iif(DatosContacto.IndiceVehiculo = 0, NULL, DatosContacto.IndiceVehiculo);
			SP.Parameters.ParamByName('@CodigoDocumento').Value :=
			  iif(DatosContacto.CodigoDocumento = '', NULL, DatosContacto.CodigoDocumento);
			SP.Parameters.ParamByName('@NumeroDocumento').Value :=
			  iif(DatosContacto.NumeroDocumento = '', NULL, DatosContacto.NumeroDocumento);
			SP.Parameters.ParamByName('@Patente').Value :=
			  iif(DatosContacto.Patente = '', NULL, DatosContacto.Patente);
			SP.Parameters.ParamByName('@Estado').Value :=
			  DatosSolucion.Estado;
			SP.Parameters.ParamByName('@Prioridad').Value :=
			  DatosCompromiso.Prioridad;
			SP.Parameters.ParamByName('@FechaCompromiso').Value :=
			  iif(DatosCompromiso.FechaCompromiso = NULLDATE, NULL, DatosCompromiso.FechaCompromiso);
			SP.Parameters.ParamByName('@ContactoNombre').Value :=
			  iif(DatosContacto.Nombre = '', NULL, DatosContacto.Nombre);
			SP.Parameters.ParamByName('@NombrePersona').Value :=
			  iif(DatosContacto.NombrePersona = '', NULL, DatosContacto.NombrePersona);
			SP.Parameters.ParamByName('@ContactoCodigoArea').Value :=
			  iif(DatosContacto.CodigoArea = 0, NULL, DatosContacto.CodigoArea);
			SP.Parameters.ParamByName('@ContactoTelefono').Value :=
			  iif(DatosContacto.Telefono = '', NULL, DatosContacto.Telefono);
			SP.Parameters.ParamByName('@ContactoEmail').Value :=
			  iif(DatosContacto.Email = '', NULL, DatosContacto.Email);
			SP.Parameters.ParamByName('@ObservacionesSolicitante').Value :=
			  iif(Trim(ObservacionesSolicitante) = '', NULL, ObservacionesSolicitante);
			SP.Parameters.ParamByName('@ObservacionesEjecutante').Value :=
			  iif(Trim(ObservacionesEjecutante) = '', NULL, ObservacionesEjecutante);
            SP.Parameters.ParamByName('@ObservacionesRespuesta').Value :=
			  iif(Trim(ObservacionesRespuesta) = '', NULL, ObservacionesRespuesta);
            SP.Parameters.ParamByName('@ObservacionesComentariosCliente').Value :=
			  iif(Trim(ObservacionesComentariosCliente) = '', NULL, ObservacionesComentariosCliente);
            SP.Parameters.ParamByName('@Notificado').Value := Notificado;
            SP.Parameters.ParamByName('@Conforme').Value := Conforme;
			SP.Parameters.ParamByName('@CodigoOrdenServicio').Value :=
			  iif(CodigoOrdenServicio = 0, NULL, CodigoOrdenServicio);
			SP.Parameters.ParamByName('@CodigoFuenteReclamo').Value :=
			  iif(CodigoFuenteReclamo = 0, NULL, CodigoFuenteReclamo);
			SP.Parameters.ParamByName('@FechaCompromisoInterna').Value :=
			  iif(DatosSolucion.FechaCompromiso = NULLDATE, NULL, DatosSolucion.FechaCompromiso);
			SP.Parameters.ParamByName('@CodigoEntidad').Value := NULL;                                                                      // _PAN_
			SP.Parameters.ParamByName('@CodigoAreaAtencionDeCaso').Value := iif(DatosSolucion.Entidad = 0, 0, DatosSolucion.Entidad);    // _PAN_
            SP.Parameters.ParamByName('@Responsable').Value := iif(Trim(DatosSolucion.RespUsuario) = '', NULL, DatosSolucion.RespUsuario);  // _PAN_

			SP.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            if (CodigoOrdenServicio = 0) then
                SP.Parameters.ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
            SP.Parameters.ParamByName('@TipoContacto').Value := DatosContacto.TipoContacto;

            //Rev.6 / 29-Junio-2010 / Nelson Droguett Sierra -------------------------
            SP.Parameters.ParamByName('@CodigoConcesionaria').Value := IIf(ConcesionariaReclamoOrdenServicio.ConcesionariaReclamo > 0, ConcesionariaReclamoOrdenServicio.ConcesionariaReclamo, NULL);
            SP.Parameters.ParamByName('@SubTipoOrdenServicio').Value := {IIf(SubTipoReclamoOrdenServicio.SubTipoReclamo > 0, SubTipoReclamoOrdenServicio.SubTipoReclamo, NULL); } 1;         // _PAN_
            //FinRev.6----------------------------------------------------------------
			SP.ExecProc;
			Result := SP.Parameters.ParamByName('@CodigoOrdenServicio').Value;
			NotifyOSChanged;
		except
			on e: exception do begin
				MsgBox(MSG_SAVE_ERROR_SERVICE_ORDER + CRLF + e.Message, STR_ERROR, MB_ICONSTOP);
				Result := 0;
			end;
		end;
	finally
		SP.Free;
	end
end;

{-----------------------------------------------------------------------------
  Function Name: GuardarOrdenServicioFacturacion
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer; TipoComprobante: AnsiString; NumeroComprobante: Int64; FechaPago: TDateTime; LugarPago: AnsiString; SubtipoReclamo: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function  GuardarOrdenServicioFacturacion(CodigoOrdenServicio: Integer;TipoComprobante: AnsiString; NumeroComprobante: Int64;FechaPago: TDateTime; LugarPago: AnsiString; SubtipoReclamo: Integer; NumeroRecibo:integer; CodigoResolucion:integer; RecibioComprobante, DireccionIncorrecta:boolean): Boolean;
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'GuardarOrdenServicioFacturacion';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        SP.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
        SP.Parameters.ParamByName('@FechaPago').Value := iif(FechaPago = NULLDATE, NULL, FechaPago);
        SP.Parameters.ParamByName('@LugarPago').Value := iif(Trim(LugarPago) = '', NULL, LugarPago);
        SP.Parameters.ParamByName('@Subtipo').Value := iif(SubtipoReclamo = 0, NULL, SubtipoReclamo);
        SP.Parameters.ParamByName('@NumeroRecibo').Value := iif(NumeroRecibo = 0, NULL, NumeroRecibo);
        SP.Parameters.ParamByName('@CodigoResolucion').Value := iif(CodigoResolucion = 0, NULL, CodigoResolucion);
        SP.Parameters.ParamByName('@RecibioComprobante').Value := RecibioComprobante;
        SP.Parameters.ParamByName('@DireccionIncorrecta').Value := DireccionIncorrecta;
        SP.ExecProc;
        Result := True;
        NotifyOSChanged;
    except
        on e: exception do begin
            MsgBox(MSG_SAVE_ERROR_SERVICE_ORDER + CRLF + e.Message, STR_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
    SP.Free;
end;

{-----------------------------------------------------------------------------
  Function Name: GuardarOrdenServicioCuenta
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer; PatenteDesconocida, PatenteFaltante, TipoComprobante: AnsiString; NumeroComprobante: Int64; ContextMark: Integer; ContractSerialNumber: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function GuardarOrdenServicioCuenta(CodigoOrdenServicio: Integer; PatenteDesconocida, PatenteFaltante:string; FechaHoraDesconoce:tdatetime; CodigoMotivoDesconocePatente:integer; TipoComprobante: AnsiString; NumeroComprobante: Int64; ContextMark: Integer; ContractSerialNumber: Int64): Boolean;
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'GuardarOrdenServicioCuenta';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@PatenteDesconocida').Value := iif(PatenteDesconocida = '', NULL, PatenteDesconocida);
        SP.Parameters.ParamByName('@PatenteFaltante').Value := iif(PatenteFaltante = '', NULL, PatenteFaltante);
        SP.Parameters.ParamByName('@FechaHoraDesconoce').Value := iif(FechaHoraDesconoce = NULLDATE, NULL, FechaHoraDesconoce);
        SP.Parameters.ParamByName('@CodigoMotivoDesconocePatente').Value := iif(CodigoMotivoDesconocePatente = 0, NULL, CodigoMotivoDesconocePatente);
        SP.Parameters.ParamByName('@TipoComprobante').Value := iif(TipoComprobante = '', NULL, TipoComprobante);
        SP.Parameters.ParamByName('@NumeroComprobante').Value := iif(NumeroComprobante = 0, NULL, NumeroComprobante);
        SP.Parameters.ParamByName('@ContextMark').Value := iif(ContextMark = 0, NULL, ContextMark);
        SP.Parameters.ParamByName('@ContractSerialNumber').Value := iif(ContractSerialNumber = 0, NULL, ContractSerialNumber);
        SP.ExecProc;
        Result := True;
        NotifyOSChanged;
    except
        on e: exception do begin
            MsgBox(MSG_SAVE_ERROR_SERVICE_ORDER + CRLF + e.Message, STR_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
    SP.Free;
end;


{-----------------------------------------------------------------------------
  Function Name: GuardarOrdenServiciotransito
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer; PatenteDesconocida, PatenteFaltante, TipoComprobante: AnsiString; NumeroComprobante: Int64; ContextMark: Integer; ContractSerialNumber: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function GuardarOrdenServicioTransito(CodigoOrdenServicio: Integer; NumCorrCA: String; ClienteRechaza:boolean; CodigoResolucionReclamoTransito:integer): Boolean;
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'GuardarOrdenServicioTransito';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@NumCorrCA').Value := NumCorrCA;
        SP.Parameters.ParamByName('@Rechaza').Value := ClienteRechaza;
        SP.Parameters.ParamByName('@CodigoResolucionReclamoTransito').Value := iif(CodigoResolucionReclamoTransito = 0, NULL, CodigoResolucionReclamoTransito);
        SP.ExecProc;
        Result := True;
        NotifyOSChanged;
    except
        on e: exception do begin
            MsgBox(MSG_SAVE_ERROR_SERVICE_ORDER + CRLF + e.Message, STR_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
    SP.Free;
end;


{-----------------------------------------------------------------------------
  Procedure: GuardarOrdenServicioDesconoceInfraccion
  Author:    lgisuk
  Date:      09-May-2005
  Arguments: CodigoOrdenServicio: Integer;CodigoMotivoDesconoceInfraccion:integer; NumeroSerie:string; FechaCompra:Tdatetime; CodigoCategoria:integer; Patente: string; Etiqueta:string; CodigoConcesionaria:integer; NumeroConvenio:string
  Result:    Boolean
-----------------------------------------------------------------------------}
function GuardarOrdenServicioDesconoceInfraccion(CodigoOrdenServicio: Integer;CodigoMotivoDesconoceInfraccion:integer; NumeroSerie:string; FechaCompra:Tdatetime; CodigoCategoria:integer; Patente: string; Etiqueta:string; CodigoConcesionaria:integer; NumeroConvenio:string): Boolean;
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'GuardarOrdenServicioDesconoceInfraccion';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@CodigoMotivoDesconoceInfraccion').value := CodigoMotivoDesconoceInfraccion;
        SP.Parameters.ParamByName('@NumeroSerie').Value := iif(NumeroSerie = '', NULL, NumeroSerie);
        SP.Parameters.ParamByName('@FechaCompra').Value := iif(FechaCompra = NULLDATE, NULL, FechaCompra);
        SP.Parameters.ParamByName('@CodigoCategoria').Value := iif(CodigoCategoria = 0, NULL, CodigoCategoria);
        SP.Parameters.ParamByName('@Patente').Value := iif(Patente = '', NULL, Patente);;
        SP.Parameters.ParamByName('@Etiqueta').Value := iif(Etiqueta = '', NULL, Etiqueta);
        SP.Parameters.ParamByName('@CodigoConcesionaria').Value := iif(CodigoConcesionaria = 0, NULL, CodigoConcesionaria);
        SP.Parameters.ParamByName('@Numeroconvenio').Value := iif(NumeroConvenio = '', NULL, NumeroConvenio);
        SP.ExecProc;
        Result := True;
        NotifyOSChanged;
    except
        on e: exception do begin
            MsgBox(MSG_SAVE_ERROR_SERVICE_ORDER + CRLF + e.Message, STR_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
    SP.Free;
end;


{-----------------------------------------------------------------------------
  Function Name: GuardarOrdenServicioContactarCliente
  Author:    lgisuk
  Date Created: 13/04/2005
  Description:
  Parameters: CodigoOrdenServicio: Integer;
  Return Value: Boolean
-----------------------------------------------------------------------------}
function GuardarOrdenServicioContactarCliente(CodigoOrdenServicio: Integer; Notificado, Conforme: boolean): Boolean;
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'GuardarOrdenServicioCliente';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@Notificado').Value := Notificado;
        SP.Parameters.ParamByName('@Conforme').Value := Conforme;
        SP.ExecProc;
        Result := True;
        NotifyOSChanged;
    except
        on e: exception do begin
            MsgBox(MSG_SAVE_ERROR_SERVICE_ORDER + CRLF + e.Message, STR_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
    SP.Free;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarTransitosReclamosAceptados
  Author:    lgisuk
  Date Created: 20/05/2005
  Description: En el caso que se acepte el reclamo del cliente
               se pone el transito en estado no facturable
  Parameters: CodigoOrdenServicio: Integer; Notificado, Conforme: boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function ActualizarTransitosReclamosAceptados(CodigoOrdenServicio: Integer): Boolean;
resourcestring
    MSG_SAVE_ERROR = 'Error al actualizar los transitos reclamados';
    MSG_ERROR = 'Error';
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'ActualizarTransitosReclamosAceptados';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        SP.ExecProc;
        Result := True;
    except
        on E : Exception do begin
            Result := False;
            MsgBoxErr(MSG_SAVE_ERROR, E.message, MSG_ERROR, MB_ICONSTOP);
        end;
    end;
    SP.Free;
end;

{-----------------------------------------------------------------------------
  Function Name: _EditarOrdenServicio
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer; Solucionar: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure _EditarOrdenServicio(CodigoOrdenServicio: Integer; Solucionar: Boolean);

    procedure EditarGeneral;
    Var
        f: TFormReclamoGeneral;
    begin
        Application.CreateForm(TFormReclamoGeneral, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.Show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

    procedure EditarFacturacion;
    Var
        f: TFormReclamoFactura;
    begin
        Application.CreateForm(TFormReclamoFactura, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

    procedure EditarCuenta;
    Var
        f: TFormReclamoCuenta;
    begin
        Application.CreateForm(TFormReclamoCuenta, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

    procedure EditarFaltaVehiculo;
    Var
        f: TFormReclamoFaltaVehiculo;
    begin
        Application.CreateForm(TFormReclamoFaltaVehiculo, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

    procedure EditarTransito;
    Var
        f: TFormReclamoTransito;
    begin
        Application.CreateForm(TFormReclamoTransito, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

    procedure EditarEstacionamiento;
    Var
        f: TFormReclamoEstacionamiento;
    begin
        Application.CreateForm(TFormReclamoEstacionamiento, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

    procedure EditarTransitoDesconoceInfraccion;
    Var
        f: TFormReclamoTransitoDesconoceInfraccion;
    begin
        Application.CreateForm(TFormReclamoTransitoDesconoceInfraccion, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

    procedure EditarContactarCliente;
    Var
        f: TFormReclamoContactarCliente;
    begin
        Application.CreateForm(TFormReclamoContactarCliente, f);
        if Solucionar then begin
            if f.InicializarSolucionando(CodigoOrdenServicio) then f.show;
        end else begin
            if f.InicializarEditando(CodigoOrdenServicio) then f.show;
        end;
    end;

var
    TipoOrdenServicio : Integer;
    I: Integer;
    F: TForm;
    CodigoOrden: Integer;
begin
    //si no esta bloqueada intento bloquearla
    if not BloquearOrdenServicio(CodigoOrdenServicio) then Exit;
    //reviso si ya esta creada en el array
    for I := Low(GOSForms) to High(GOSForms) do begin
        CodigoOrden := 0;
        F:= GOSForms[I];
        if Assigned(F) then begin
            //Obtengo el CodigoOrdenServicio del form
            if CodigoOrden = 0 then if (F is TFormReclamoGeneral)                     then CodigoOrden := (F as TFormReclamoGeneral).CodigoOrdenServicio;
            if CodigoOrden = 0 then if (F is TFormReclamoFactura)                     then CodigoOrden := (F as TFormReclamoFactura).CodigoOrdenServicio;
            if CodigoOrden = 0 then if (F is TFormReclamoCuenta)                      then CodigoOrden := (F as TFormReclamoCuenta).CodigoOrdenServicio;
            if CodigoOrden = 0 then if (F is TFormReclamoFaltaVehiculo)               then CodigoOrden := (F as TFormReclamoFaltaVehiculo).CodigoOrdenServicio;
            if CodigoOrden = 0 then if (F is TFormReclamoTransito)                    then CodigoOrden := (F as TFormReclamoTransito).CodigoOrdenServicio;
            if CodigoOrden = 0 then if (F is TFormReclamoTransitoDesconoceInfraccion) then CodigoOrden := (F as TFormReclamoTransitoDesconoceInfraccion).CodigoOrdenServicio;
            if CodigoOrden = 0 then if (F is TFormReclamoContactarCliente)            then CodigoOrden := (F as TFormReclamoContactarCliente).CodigoOrdenServicio;
            //si la OS ya esta en pantalla
            if CodigoOrden = CodigoOrdenServicio then begin
                //la traigo al frente sin crearla denuevo
                if F.WindowState = wsMiniMized then F.WindowState := wsNormal;
                F.show;
                //salgo para que no cree el form
                Exit;
            end;
        end;
    end;

    //obtengo el tipo de orden de servicio
    TipoOrdenServicio := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerTipoOrdenServicio ( %d ) ',[CodigoOrdenServicio]));
    //segun que tipo de orden de servicio es abro el formulario para ese tipo
    case TipoOrdenServicio of
        204, 205, 206, 208, 214, 215, 216: EditarFacturacion();
        211, 301, 302: EditarCuenta();
        212: EditarFaltaVehiculo();
        207: EditarTransito();
        217: EditarEstacionamiento();
        201: EditarTransitoDesconoceInfraccion();
        402: EditarGeneral(); //Da�o a vehiculos
        2001: EditarContactarCliente();
        else EditarGeneral;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: EditarOrdenServicio
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure EditarOrdenServicio(CodigoOrdenServicio: Integer);
begin
    _EditarOrdenServicio(CodigoOrdenServicio, False);
end;

{-----------------------------------------------------------------------------
  Function Name: SolucionarOrdenServicio
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure SolucionarOrdenServicio(CodigoOrdenServicio: Integer);
begin
    _EditarOrdenServicio(CodigoOrdenServicio, True);
end;


{-----------------------------------------------------------------------------
  Function Name: BloquearOrdenServicio
  Author: DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean

 Revision 1 :
    Author : mpiazza
    Date : 26-01-2009
    Description : Modificaciones SS 689, Modificar el mensaje de lock de
    convenio MSG_LOCK_ERROR

-----------------------------------------------------------------------------}
function BloquearOrdenServicio(CodigoOrdenServicio: Integer): Boolean;
Var
    SP          : TADOStoredProc;
    UsuarioLock : string; //R1
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'BloquearOrdenServicio';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    SP.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
    SP.Parameters.ParamByName('@Result').Value := NULL;
    SP.Parameters.ParamByName('@UsuarioLock').Value := NULL; //R1
    SP.ExecProc;
    Result := SP.Parameters.ParamByName('@Result').Value;
    UsuarioLock := iif(SP.Parameters.ParamByName('@UsuarioLock').Value = null, '', SP.Parameters.ParamByName('@UsuarioLock').Value); //R1
    SP.Free;
    if Result then
        NotifyOSChanged
    else begin
        MsgBox(MSG_LOCK_ERROR + UsuarioLock, MSG_ERROR_CAPTION, MB_ICONSTOP); //R1
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DesbloquearOrdenServicio
  Author:    DDeMarco
  Date Created:
  Description:
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function DesbloquearOrdenServicio(CodigoOrdenServicio: Integer): Boolean;
Var
    SP: TADOStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'DesbloquearOrdenServicio';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    SP.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
    SP.Parameters.ParamByName('@Result').Value := NULL;
    SP.ExecProc;
    Result := SP.Parameters.ParamByName('@Result').Value;
    SP.Free;
    if Result then
        NotifyOSChanged
    else begin
        MsgBox(MSG_LOCK_ERROR, MSG_ERROR_CAPTION, MB_ICONSTOP);
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: DesbloquearOrdenServicioAdministrador
  Author:    lgisuk
  Date Created: 06/04/2005
  Description: Permite Desbloquear Al Administrador Cualquier Orden de Servicio
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function DesbloquearOrdenServicioAdministrador(CodigoOrdenServicio: Integer): Boolean;
Var
    SP: TADOStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'DesbloquearOrdenServicioAdministrador';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    SP.Parameters.ParamByName('@Result').Value := NULL;
    SP.ExecProc;
    Result := SP.Parameters.ParamByName('@Result').Value;
    SP.Free;
    if Result then
        NotifyOSChanged
    else begin
        MsgBox(MSG_LOCK_ERROR, MSG_ERROR_CAPTION, MB_ICONSTOP);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DesbloquearOrdenesServicio
  Author:  DDeMarco
  Date Created:
  Description:
  Parameters: None
  Return Value: Boolean

  Revision 1:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura


-----------------------------------------------------------------------------}
function DesbloquearOrdenesServicio: Boolean;
resourcestring
    MSG_UNLOCK_SERVICE_ORDERS = 'Ud. est� trabajando en una o m�s �rdenes de servicio, '+
                                'lo que implica que los dem�s usuarios no pueden accederlas. ' +
                                '�Desea desbloquearlas antes de salir?';
    STR_SALIR                 = 'Salir';
    MSG_ERROR_UNLOCK          = 'Error al desbloquear �rdenes de servicio';
begin
    try
        if (UsuarioSistema = '') or (QueryGetValueInt(DMConnections.BaseCAC, Format(
          'SELECT COUNT(*) FROM OrdenesServicio  WITH (NOLOCK) WHERE Estado in (''P'',''I'',''E'') AND Usuario = ''%s''', //inclui pendientes internas y externas
          [UsuarioSistema])) = 0) then begin
            Result := True;
        end else begin
            case MsgBox(MSG_UNLOCK_SERVICE_ORDERS, STR_SALIR, MB_ICONWARNING
              or MB_YESNOCANCEL or MB_DEFBUTTON1) of
                ID_YES:
                    begin
                        QueryExecute(DMConnections.BaseCAC, Format(
                          'UPDATE OrdenesServicio SET Usuario = NULL WHERE ' +
                          'Estado in (''P'',''I'',''E'') AND Usuario = ''%s''', [UsuarioSistema])); //inclui pendientes internas y externas
                        Result := True;
                    end;
                ID_NO:
                    Result := True;
                else
                    Result := False;
            end;
        end;
    except
        on e: exception do begin
            MsgBox(e.message, MSG_ERROR_UNLOCK, MB_ICONSTOP);
            Result := True;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerDescripcionEstadoOS
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: Estado: AnsiString
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function ObtenerDescripcionEstadoOS(Estado: AnsiString): AnsiString;
resourcestring
    STR_PENDIENTE = 'Pendiente';
    STR_INTERNA   = 'An�lisis'; //'P.Resp.Int';
    STR_EXTERNA   = 'Respuesta';//'P.Resp.Ext';
    STR_TERMINADA = 'Resuelta';
    STR_CANCELADA = 'Cancelada';
    STR_ERROR     = '<Error>';
begin
	Estado := UpperCase(Estado);
	if Estado = 'P' then Result := STR_PENDIENTE
    else if Estado = 'I' then Result := STR_INTERNA
    else if Estado = 'E' then Result := STR_EXTERNA
	else if Estado = 'T' then Result := STR_TERMINADA
	else if Estado = 'C' then Result := STR_CANCELADA
	else Result := STR_ERROR;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerDescripcionPrioridadOS
  Author:  DDeMarco
  Date Created:
  Description:
  Parameters: Prioridad: Integer
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function ObtenerDescripcionPrioridadOS(Prioridad: Integer): AnsiString;
resourcestring
    STR_URGENTE = 'Muy Alta';
    STR_ALTA    = 'Alta';
    STR_NORMAL  = 'Media'; //'Normal';
    STR_BAJA    = 'Baja';
    STR_ERROR   = '<Error>';
begin
	case Prioridad of
		1: Result := STR_URGENTE;
		2: Result := STR_ALTA;
		3: Result := STR_NORMAL;
		4: Result := STR_BAJA;
		else Result := STR_ERROR;
	end;
end;



{-----------------------------------------------------------------------------
  Function Name: ObtenerFechaComprometidaOS
  Author:    lgisuk
  Date Created: 20/04/2005
  Description: obtengo una fecha comprometida en funci�n del tipodereclamo ,
               origen del reclamo y prioridad asignada
  Parameters: Prioridad: Integer
  Return Value: AnsiString
  Comments:
       Pongo la fecha como aaaammdd hh:nn:ss.sss que es standard y el SQL siempre la acepta
       para evitar error de conversion de fecha ya que se esta llamando una Fn con parametro datetime,
       pero se le est� pasando un string, con lo cual hace un casteo implicito.
       -- ndonadio
 -----------------------------------------------------------------------------}
function ObtenerFechaComprometidaOS(Fecha: TDatetime; TipoOrdenServicio: integer; CodigoFuenteReclamo:integer; Prioridad: integer): Tdatetime;
var
        stFecha: AnsiString;
begin
    result:=NULLDATE;

    try
        // Pongo la fecha como aaaammdd hh:nn:ss.sss que es standard y el SQL siempre la acepta
        stFecha := FormatDateTime('yyyymmdd hh:nn:ss.zzz',Fecha);
        result:=Querygetvaluedatetime(dmconnections.basecac,
                                       'SELECT dbo.ObtenerFechaComprometidaOS ('''+  stFecha +''''+','+
                                       inttostr(TipoOrdenServicio)+','+
                                       inttostr(CodigofuenteReclamo)+','+
                                       inttostr(prioridad)+
                                       ')'
                                     );


    except
        on e: exception do begin
         msgbox(e.Message);
        end;

    end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarFuentesReclamo
  Author:    lgisuk
  Date Created: 20/04/2005
  Description: cargo las fuentes del reclamo
  Parameters: Combo:TVariantComboBox
  Return Value: None
-----------------------------------------------------------------------------}
procedure CargarFuentesReclamo(Combo:TVariantComboBox);
const
    CONST_NINGUNO = '(Seleccionar)'; //'Ninguno'
var
    SP: TAdoStoredProc;
begin
    try
        Combo.Items.Add(CONST_NINGUNO, 0);
        try
            SP := TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'ObtenerFuentesReclamo';
            SP.Open;
            while not sp.Eof do begin
                Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('CodigoFuenteReclamo').asinteger);
                SP.Next;
            end;
            SP.Close;
            Combo.ItemIndex:=0;
        except
        end;
    finally
        FreeAndNil(SP);
    end;
end;

procedure CargarUsuarios(Combo:TVariantComboBox; AreaAtencion: Integer);
const
    CONST_NINGUNO = '(Seleccionar)'; //'Ninguno'
var
    SP: TAdoStoredProc;
begin
    try
        Combo.Items.Clear;
        Combo.Items.Add(CONST_NINGUNO, 0);
        try
            SP := TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'ObtenerUsuariosAreas';
            SP.Parameters.Refresh;
            SP.Parameters.ParamByName('@Area').value:= AreaAtencion;
            SP.Open;
            while not sp.Eof do begin
                Combo.Items.Add(SP.fieldbyname('NombreUsuario').AsString, SP.fieldbyname('CodigoUsuario').AsString);
                SP.Next;
            end;
            SP.Close;
            Combo.ItemIndex:=0;
        except
        end;
    finally
        FreeAndNil(SP);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: InformarNumeroReclamo
  Author:    lgisuk
  Date Created: 20/04/2005
  Description: informo numero de reclamo asignado
  Parameters: NumeroReclamo:string
  Return Value: None
-----------------------------------------------------------------------------}
Procedure InformarNumeroReclamo(NumeroReclamo:string);
var F:TfNumeroReclamoAsignado;
begin
    Application.CreateForm(TFNumeroReclamoAsignado, f);
    if f.Inicializar(NumeroReclamo) then f.ShowModal;
    f.Release;
end;



{-----------------------------------------------------------------------------
  Function Name: TienePermisoOrdenServicio
  Author:   DDeMarco
  Date Created:
  Description:
  Parameters: TipoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TienePermisoOrdenServicio(TipoOrdenServicio: Integer): Boolean;

    procedure CargarPermisos;
    Var
        SP: TADOStoredProc;
    begin
        if GetMSCounter < ProximaCargaPermisos then Exit;
        SP := TADOStoredProc.Create(nil);
        { INICIO  : 20160307 MGO
		SP.Connection := DMConnections.BaseCAC;
   		}
		SP.Connection := DMConnections.BaseBO_Master;
        // FIN : 20160307 MGO
		SP.ProcedureName := 'ObtenerPermisosUsuario';
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        SP.Parameters.ParamByName('@CodigoSistema').Value := 5;
        SP.Open;
        GPermisos.Clear;
        while not SP.Eof do begin
            GPermisos.Add(Trim(SP.FieldByName('Funcion').AsString));
            SP.Next;
        end;
        SP.Free;
        ProximaCargaPermisos := GetMSCounter + 30000;
    end;

begin
    CargarPermisos;
    Result := GPermisos.IndexOf(IntToStr(TipoOrdenServicio)) <> -1;
end;


{-----------------------------------------------------------------------------
  Function Name: AddOSNotification
  Author:  DDeMarco
  Date Created:
  Description:
  Parameters: OSProc: TOSNotifier
  Return Value: None
-----------------------------------------------------------------------------}
procedure AddOSNotification(OSProc: TOSNotifier);
Var
    i: Integer;
begin
    for i := Low(GOSNotifications) to High(GOSNotifications) do begin
        if not Assigned(GOSNotifications[i]) then begin
            GOSNotifications[i] := OSProc;
            break; //una vez que encuentra el primer lugar libre asigna y sale
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RemoveOSNotification
  Author:  DDeMarco
  Date Created:
  Description:
  Parameters: OSProc: TOSNotifier
  Return Value: None
-----------------------------------------------------------------------------}
procedure RemoveOSNotification(OSProc: TOSNotifier);
Var
    i: Integer;
begin
    for i := Low(GOSNotifications) to High(GOSNotifications) do
      if @GOSNotifications[i] = @OSProc then GOSNotifications[i] := nil;
end;

{-----------------------------------------------------------------------------
  Function Name: NotifyOSChanged
  Author:  DDeMarco
  Date Created:
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure NotifyOSChanged;
Var
    i: Integer;
begin
    for i := Low(GOSNotifications) to High(GOSNotifications) do
      if Assigned(GOSNotifications[i]) then GOSNotifications[i];
end;

{-----------------------------------------------------------------------------
  Function Name: AddOSForm
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: agrego un form a la lista de formularios
  Parameters: OSForm: TForm
  Return Value: None
-----------------------------------------------------------------------------}
procedure AddOSForm(OSForm: TForm);
Var
    i: Integer;
begin
    for i := Low(GOSForms) to High(GOSForms) do begin
        if not Assigned(GOSForms[i]) then begin
            GOSForms[i] := OSForm;
            break; //una vez que encuentra el primer lugar libre asigna y sale
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RemoveOSForm
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: elimino un form de la lista de formularios
  Parameters: OSForm: TForm
  Return Value: None
-----------------------------------------------------------------------------}
procedure RemoveOSForm(OSForm: TForm);
Var
    i: Integer;
    f: tform;
begin
    for i := Low(GOSForms) to High(GOSForms) do begin
      F:= GOSForms[i];
      if F = OSForm then begin
            GOSForms[i] := nil;
      end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: GuardarOrdenServicioEstacionamiento
  Author:   Nelson Droguett Sierra
  Date Created: 06-Abril-2011
  Description: Guarda reclamo de estacionamiento
-----------------------------------------------------------------------------}
function GuardarOrdenServicioEstacionamiento(CodigoOrdenServicio: Integer; NumCorrEstacionamiento: String; ClienteRechaza:boolean; CodigoResolucionReclamoEstacionamiento:integer): Boolean;
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'GuardarOrdenServicioEstacionamiento';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@NumCorrEstacionamiento').Value := NumCorrEstacionamiento;
        SP.Parameters.ParamByName('@Rechaza').Value := ClienteRechaza;
        SP.Parameters.ParamByName('@CodigoResolucionReclamoEstacionamiento').Value := iif(CodigoResolucionReclamoEstacionamiento = 0, NULL, CodigoResolucionReclamoEstacionamiento);
        SP.ExecProc;
        Result := True;
        NotifyOSChanged;
    except
        on e: exception do begin
            MsgBox(MSG_SAVE_ERROR_SERVICE_ORDER + CRLF + e.Message, STR_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
    SP.Free;
end;

function ActualizarEstacionamientosReclamosAceptados(CodigoOrdenServicio: Integer): Boolean;
resourcestring
    MSG_SAVE_ERROR = 'Error al actualizar los estacionamientos reclamados';
    MSG_ERROR = 'Error';
Var
    SP: TAdoStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := DMConnections.BaseCAC;
    SP.ProcedureName := 'ActualizarEstacionamientosReclamosAceptados';
    try
        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        SP.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        SP.ExecProc;
        Result := True;
    except
        on E : Exception do begin
            Result := False;
            MsgBoxErr(MSG_SAVE_ERROR, E.message, MSG_ERROR, MB_ICONSTOP);
        end;
    end;
    SP.Free;
end;

initialization
    GPermisos := TStringList.Create;
    FillChar(GOSNotifications, SizeOf(GOSNotifications), 0);
    FillChar(GOSForms, SizeOf(GOSForms), 0);
    AddTerminateProc(DesbloquearOrdenesServicio);
finalization
    FreeAndNil(GPermisos);
    FillChar(GOSNotifications, SizeOf(GOSNotifications), 0);
    FillChar(GOSForms, SizeOf(GOSForms), 0);
end.
