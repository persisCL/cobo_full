object FormEstacionamientosReclamadosAcciones: TFormEstacionamientosReclamadosAcciones
  Left = 379
  Top = 136
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Sistema de Reclamos'
  ClientHeight = 266
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MaxHeight = 300
  Constraints.MaxWidth = 600
  Constraints.MinHeight = 291
  Constraints.MinWidth = 598
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Ltitulo: TLabel
    Left = 160
    Top = 16
    Width = 290
    Height = 13
    Caption = 'Se realizaron acciones sobre los siguientes estacionamientos.'
  end
  object DBLEstacionamientos: TDBListEx
    Left = 4
    Top = 41
    Width = 583
    Height = 128
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Estacionamiento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'NumCorrEstacionamiento'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Entrada'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescripcionEntrada'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha / Hora Entrada'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraEntrada'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Salida'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescripcionSalida'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha / Hora Salida'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaHoraSalida'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Acci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Accion'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDrawText = DBLEstacionamientosDrawText
  end
  object btnCerrar: TButton
    Left = 251
    Top = 207
    Width = 75
    Height = 25
    Caption = '&Cerrar'
    Default = True
    TabOrder = 1
    OnClick = btnCerrarClick
  end
  object DataSource: TDataSource
    DataSet = spObtenerEstacionamientosReclamadosAcciones
    Left = 483
    Top = 72
  end
  object spObtenerEstacionamientosReclamadosAcciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEstacionamientosReclamadosAcciones;1'
    Parameters = <>
    Left = 449
    Top = 72
  end
end
