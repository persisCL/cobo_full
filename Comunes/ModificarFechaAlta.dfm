object frmModificarFechaAlta: TfrmModificarFechaAlta
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Modificaci'#243'n de Fecha de Alta'
  ClientHeight = 104
  ClientWidth = 264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GridPanel1: TGridPanel
    Left = 0
    Top = 0
    Width = 264
    Height = 104
    Align = alClient
    ColumnCollection = <
      item
        Value = 100.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = Panel1
        Row = 0
      end
      item
        Column = 0
        Control = GridPanel2
        Row = 1
      end>
    RowCollection = <
      item
        Value = 79.999360005274940000
      end
      item
        Value = 20.000639994725070000
      end>
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 262
      Height = 81
      Align = alClient
      TabOrder = 0
      object lblFechaAlta: TLabel
        Left = 16
        Top = 11
        Width = 76
        Height = 13
        Caption = 'Fecha de Alta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblHoraAlta: TLabel
        Left = 16
        Top = 44
        Width = 70
        Height = 13
        Caption = 'Hora de Alta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DtFechaAlta: TDateEdit
        Left = 112
        Top = 8
        Width = 137
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object DtHoraAlta: TTimeEdit
        Left = 112
        Top = 38
        Width = 137
        Height = 21
        AutoSelect = False
        TabOrder = 1
        AllowEmpty = False
        ShowSeconds = False
      end
    end
    object GridPanel2: TGridPanel
      Left = 1
      Top = 82
      Width = 262
      Height = 21
      Align = alClient
      ColumnCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = BtnAceptar
          Row = 0
        end
        item
          Column = 1
          Control = BtnCancelar
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 1
      object BtnAceptar: TButton
        Left = 1
        Top = 1
        Width = 130
        Height = 19
        Align = alClient
        Caption = '&Aceptar'
        TabOrder = 0
        OnClick = BtnAceptarClick
      end
      object BtnCancelar: TButton
        Left = 131
        Top = 1
        Width = 130
        Height = 19
        Align = alClient
        Caption = '&Cancelar'
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
end
