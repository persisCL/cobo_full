unit GrillaListado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbClient, StdCtrls, ExtCtrls, Grids, DBGrids, DB;

type
  TfrmGrilla = class(TForm)
    dbgrdListado: TDBGrid;
    pnlSuperior: TPanel;
    pnlInferior: TPanel;
    btnSalir: TButton;
    lblMensaje: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(datos: TClientDataSet; encabezados : array of string): boolean;
  end;

var
  frmGrilla: TfrmGrilla;

implementation

{$R *.dfm}

procedure TfrmGrilla.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmGrilla.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if Assigned(dbgrdListado.DataSource) then
    begin
        if Assigned(dbgrdListado.DataSource.DataSet) then
            dbgrdListado.DataSource.DataSet.Free;
        dbgrdListado.DataSource.Free;
    end;
    Action:= caFree;
end;

function TfrmGrilla.Inicializar(datos: TClientDataSet; encabezados: array of string):Boolean;
var
    i: Integer;
    Columna: TColumn;
begin

     Result:= False;

     dbgrdListado.Columns.Clear;
     for I := Low(encabezados) to High(encabezados) do
     begin
         Columna := dbgrdListado.Columns.Add;
         Columna.Title.Caption := encabezados[i];
         Columna.FieldName := encabezados[i];
         Columna.Width := 200;
         Columna.Free;
     end;            

     if Assigned(dbgrdListado.DataSource) then
         dbgrdListado.DataSource.Free;

     dbgrdListado.DataSource := TDataSource.Create(nil);
     dbgrdListado.DataSource.DataSet := datos;
     datos.First;

     Result:= True;
end;

end.
