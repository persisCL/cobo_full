{---------------------------------------------------------------
                    frmReporteNotaDebitoElectronica
Author: mbecerra
Date: 14-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
                Imprime la Nota de D�bito Electr�nica

Revision    : 1
Author      : Nelson Droguett Sierra
Date        : 01-Octubre-2009
Description : Se hacen cambios para que la impresion se adapte
                a la impresion de las notas de credito a CB y CQ.

Revision    : 2
Author      : Nelson Droguett Sierra
Date        : 03-Febrero-2010
Description : Se comento el "uses DMConnection" ya que provocaba un error de compilacion


 Revision 3
 Author: mbecerra
 Date: 03-Febrero-2010
 Description:	Se agrega una funci�n que borre el archivo JPG generado para el
            	Timbre electr�nico.

                Se agrega la llamada a esta funci�n en la funci�n Ejecutar, para que
                borre el archivo autom�ticamente cuando imprime el Reporte.

                Se crea esta funci�n ya que los servicios de Mailer no usan la
                funci�n Ejecutar. De esta manera se puede invocar despu�s de
                generar el archivo PDF.

----------------------------------------------------------------}
unit frmReporteNotaDebitoElectronica;

interface
                    
uses
	Util,
    UtilProc,
    ConstParametrosGenerales,
    //DMConnection,  ------------------ Rev.2 / 03-Febrero-2010 / Nelson Droguett Sierra
    DTEControlDLL,
    ImprimirWO,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ADODB, ppDBPipe, ppParameter, ppBands, ppModule, raCodMod,
  ppReport, ppStrtch, ppSubRpt, ppCtrls, ppPrnabl, ppClass, ppCache, ppComm,
  ppRelatv, ppProd, UtilRB, JPEG, StrUtils,
  // Rev.1 / 01-Octubre-2009 / Nelson Droguett Sierra.
  Peatypes;

type
  TReporteNotaDebitoElectronicaForm = class(TForm)
    rbiNotaDebitoElectronica: TRBInterface;
    ReporteNotaDebito: TppReport;
    ppParameterList1: TppParameterList;
    ppDBPipelineComprobantes: TppDBPipeline;
    spComprobantes: TADOStoredProc;
    dsComprobantes: TDataSource;
    ppDBPipelineDetalleCuenta: TppDBPipeline;
    ppLineasCKDBPipelineppField1: TppField;
    ppLineasCKDBPipelineppField2: TppField;
    ppLineasCKDBPipelineppField3: TppField;
    ppLineasCKDBPipelineppField4: TppField;
    dsDetalleCuenta: TDataSource;
    spObtenerDetalleCuentaFacturaAImprimir: TADOStoredProc;
    spObtenerDatosComprobanteParaTimbreElectronico: TADOStoredProc;
    ppHeaderBand1: TppHeaderBand;
    ppImagenFondo: TppImage;
    ppShape1: TppShape;
    ppRutConcesionaria: TppLabel;
    ppTipoDocumentoElectronico: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppdbtNombre: TppDBText;
    ppdbtDir1: TppDBText;
    ppdbtDir2: TppDBText;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule1: TraCodeModule;
    ppDBText4: TppDBText;
    ppImagenTimbre: TppImage;
    ppResolucionSII: TppLabel;
    ppFooterBand1: TppFooterBand;
    raCodeModule2: TraCodeModule;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    spObtenerComprobantesReferenciados: TADOStoredProc;
    dsObtenerComprobantesRelacionados: TDataSource;
    ppDBPipelineObtenerComprobantesRelacionados: TppDBPipeline;
    ppDetailBand3: TppDetailBand;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppNumeroDocumento: TppLabel;
    ppDBText3: TppDBText;
    ppDBText7: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel8: TppLabel;
    ppTitleBand2: TppTitleBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel1: TppLabel;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppLabel9: TppLabel;
    ppLabel5: TppLabel;
    ppDBText12: TppDBText;
  private
    { Private declarations }
    FGlosaResolucionSII,
    FTipoComprobante,
    FNombreDocumento, FRutaImagenTimbreElectronico,
    FRutaImagenDeFondo, FNombreImagenDeFondo : string;
    FPreparado, FIncluirFondo : boolean;
    FNumeroComprobante : int64;
    
  public
    { Public declarations }
    function Inicializar(Conexion : TADOConnection; TipoComprobante : string; NumeroComprobante : int64; IncluirFondo : boolean; EsWeb: boolean = False) : boolean;
    function Imprimir(MostrarDialogo: boolean) : boolean;
    function Preparar : boolean;

    //REV.2
    function EliminarImagenTimbreElectronico : boolean;
  end;

var
  ReporteNotaDebitoElectronicaForm: TReporteNotaDebitoElectronicaForm;

implementation

{$R *.dfm}

{--------------------------------------------------------
        	Inicializar

Author: mbecerra
Date: 14-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Inicializa los valores para la impresi�n de la nota de d�bito
                electr�nica
-------------------------------------------------------------------}
function TReporteNotaDebitoElectronicaForm.Inicializar;
resourcestring
	MSG_NO_ESTAN		=	'No existen todas las im�genes requeridas para ' + CRLF +
    						'poder imprimir una Nota de D�bito en el sistema';
	MSG_ERROR			= 'Ocurri� un error al inicializar la impresi�n de la nota de d�bito electr�nica';
    NOMBRE_NOTA_DEBITO	= 'NOTA DEBITO ELECTR�NICA';
    NOMBRE_NOTA_CREDITO	= 'NOTA CR�DITO ELECTR�NICA';
    MSG_CAPTION			= 'Impresi�n Nota D�bito Electr�nica';
    NOMBRE_ARCHIVO_ND	= 'IMAGEN_FONDO_NOTA_CREDITO_DEBITO';
    DIR_ARCHIVO_ND		= 'DIR_IMAGEN_FONDO_BF_ELECTRONICAS';
    DIR_WEB_ARCHIVO_ND	= 'DIR_WEB_IMAGEN_FONDO_ELECTRONICAS';

var
	CodErrorDLL : integer;
    MensajeError : string;
begin
	Result := False;
    Caption := MSG_CAPTION;
    try
    	FIncluirFondo := IncluirFondo;
        if Pos( TipoComprobante,    TC_NOTA_CREDITO + ','+
                                    TC_NOTA_CREDITO_A_COBRO + ','+
                                    TC_NOTA_CREDITO_A_NB + ',' +
                                    TC_NOTA_CREDITO_A_NQ) > 0 then
        	FNombreDocumento := NOMBRE_NOTA_CREDITO
        else
           	FNombreDocumento := NOMBRE_NOTA_DEBITO;

        FTipoComprobante := TipoComprobante;
        FNumeroComprobante := NumeroComprobante;

    	{cerrar y asiganr la conexi�n a los stored}
    	if spComprobantes.Active then spComprobantes.Close;
    	if spObtenerComprobantesReferenciados.Active then spObtenerComprobantesReferenciados.Close;
    	if spObtenerDetalleCuentaFacturaAImprimir.Active then spObtenerDetalleCuentaFacturaAImprimir.Close;
    	if spObtenerDatosComprobanteParaTimbreElectronico.Active then spObtenerDatosComprobanteParaTimbreElectronico.Close;

    	spComprobantes.Connection := Conexion;
    	spObtenerComprobantesReferenciados.Connection := Conexion;
    	spObtenerDetalleCuentaFacturaAImprimir.Connection := Conexion;
    	spObtenerDatosComprobanteParaTimbreElectronico.Connection := Conexion;


    	{ver si est� la carpeta donde reside la imagen de fondo}
        if EsWeb then ObtenerParametroGeneral(Conexion, DIR_WEB_ARCHIVO_ND, FRutaImagenDeFondo)
        else ObtenerParametroGeneral(Conexion, DIR_ARCHIVO_ND, FRutaImagenDeFondo);
        
    	{ver si est� la imagen de fondo}
    	ObtenerParametroGeneral(Conexion, NOMBRE_ARCHIVO_ND, FNombreImagenDeFondo);
    	FRutaImagenDeFondo := GoodDir(FRutaImagenDeFondo);
    	FNombreImagenDeFondo := FRutaImagenDeFondo + FNombreImagenDeFondo;
    	if FIncluirFondo and (not FileExists(FNombreImagenDeFondo)) then begin
        	MsgBox(MSG_NO_ESTAN, Caption, MB_ICONEXCLAMATION);
        	Exit;
    	end;

       //Determinar la carga de la DLL
        if not ConfigurarVariable_EGATE_HOME(Conexion, EsWeb, MensajeError) then begin
        	MsgBox(MensajeError, Caption, MB_ICONERROR);
            Exit;
        end;

        CodErrorDLL := CargarDLL();
        if CodErrorDLL < 0 then begin
        	MsgBox(ObtieneErrorAlCargarDLL(CodErrorDLL), Caption, MB_ICONERROR);
            Exit;
        end;

        if not ObtenerParametroGeneral(Conexion, 'RESOLUCION_SII_FACTURA_ELECTRONICA', FGlosaResolucionSII) then FGlosaResolucionSII := '<Sin Glosa>';
        ppResolucionSII.Caption := FGlosaResolucionSII;
        
        FPreparado := False;
        Result := True;
	except on e:exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
            Result := False;
    	end;
    end;
end;

{--------------------------------------------------------------------------
                    EliminarImagenTimbreElectronico

Author: mbecerra
Date: 03-Febrero-2010
Description:	Borra la imagen JPG del Timbre Electr�nico

            	Retorna verdadero si la imagen no existe despu�s de borrarla.
                Falso en caso contrario
----------------------------------------------------------------------------}
function TReporteNotaDebitoElectronicaForm.EliminarImagenTimbreElectronico;
begin
    DeleteFile(FRutaImagenTimbreElectronico);
    Result := not FileExists(FRutaImagenTimbreElectronico);

end;

{--------------------------------------------------------
        	Imprimir

Author: mbecerra
Date: 14-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Imprime la Nota de D�bito en pantalla
-------------------------------------------------------------------}
function TReporteNotaDebitoElectronicaForm.Imprimir;
begin
	if Preparar() then rbiNotaDebitoElectronica.Execute(MostrarDialogo);
    Result := True;

    //REV.2
    EliminarImagenTimbreElectronico();
end;

{--------------------------------------------------------
        	Preparar

Author: mbecerra
Date: 14-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Prepara la informaci�n a ser impresa
-------------------------------------------------------------------}
function TReporteNotaDebitoElectronicaForm.Preparar;
resourcestring
	MSG_ERROR_ND		= 'Ocurri� un error al preparar el reporte de la nota de d�bito electr�nica';
	MSG_ERROR_NC		= 'Ocurri� un error al preparar el reporte de la nota de cr�dito electr�nica';
    MSG_ERROR_TIMBRE	= 'No se pudo timbrar el documento electr�nico';
    MSG_ERROR_IMAGEN	= 'No se encontr� el archivo %s';


var
	Respuesta : TTimbreArchivo;
    RutEmisor: integer;
    DvEmisor : string;

begin
	try
		{datos del comprobante}
    	with spComprobantes do begin
    		Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        	Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        	Open;
    	end;

    	{detalle del comprobante}
    	with spObtenerDetalleCuentaFacturaAImprimir do begin
        	Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        	Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        	Open;
    	end;

        {Comprobantes referenciados}
        with spObtenerComprobantesReferenciados do begin
        	Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        	Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        	Open;
    	end;

        {datos del timbre electr�nico}
    	with spObtenerDatosComprobanteParaTimbreElectronico do begin
    		Parameters.Refresh;
        	Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
        	Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
        	ExecProc;
        	if ObtenerTimbreDBNet(	Parameters.ParamByName('@RutEmisor').Value,
        		                    Parameters.ParamByName('@DVRutEmisor').Value,
            		                Parameters.ParamByName('@CodigoTipoDocumentoElectronico').Value,
                    	            Parameters.ParamByName('@NumeroComprobanteFiscal').Value,
        							Parameters.ParamByName('@FechaEmision').Value,
                            		Parameters.ParamByName('@TotalComprobante').Value,
	                            	Parameters.ParamByName('@DescripcionPrimerItem').Value,
    	                        	Parameters.ParamByName('@RutReceptor').Value,
        	                    	Parameters.ParamByName('@DVRutReceptor').Value,
            	                	Parameters.ParamByName('@RazonSocialONombrePersona').Value,
                	            	Parameters.ParamByName('@FechaTimbre').Value,
                    	        	True, Respuesta
        						) then begin
	            FRutaImagenTimbreElectronico := PARAM_DIR_EGATE_HOME + 'out\html\' + Respuesta.Archivo + '.jpg';
    	        if FileExists(FRutaImagenTimbreElectronico) then begin
        	    	ppImagenTimbre.Picture.LoadFromFile(FRutaImagenTimbreElectronico);
            	end
            	else MsgBox(Format(MSG_ERROR_IMAGEN, [FRutaImagenTimbreElectronico]), Caption, MB_ICONERROR);

        	end
        	else MsgBox(MSG_ERROR_TIMBRE, Caption, MB_ICONERROR);
        end;

        ppTipoDocumentoElectronico.Caption := FNombreDocumento;
        ppNumeroDocumento.Caption := 'N� ' + Format('%.0n', [1.0 * spComprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger]);
    	RutEmisor := spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@RutEmisor').Value;
    	DVEmisor  := spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@DVRutEmisor').Value;
    	ppRutConcesionaria.Caption :=	'R.U.T. ' + Format('%.0n', [1.0 * RutEmisor]) + '-' + DvEmisor;
      ppRutConcesionaria.Caption := ReplaceText(ppRutConcesionaria.Caption,',','.');


        if FIncluirFondo then ppImagenFondo.Picture.LoadFromFile(FNombreImagenDeFondo);

        Result := True;
    except on e:exception do begin
            MsgBoxErr(IfThen(FTipoComprobante='NC',MSG_ERROR_NC,MSG_ERROR_ND), e.Message, Caption, MB_ICONERROR);
            Result := False;
    	end;

    end;

end;

end.
