object frmGrilla: TfrmGrilla
  Left = 0
  Top = 0
  Caption = 'frmGrilla'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dbgrdListado: TDBGrid
    Left = 0
    Top = 41
    Width = 635
    Height = 218
    Align = alClient
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object pnlSuperior: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 41
    Align = alTop
    TabOrder = 1
    object lblMensaje: TLabel
      Left = 16
      Top = 14
      Width = 50
      Height = 13
      Caption = 'lblMensaje'
    end
  end
  object pnlInferior: TPanel
    Left = 0
    Top = 259
    Width = 635
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      635
      41)
    object btnSalir: TButton
      Left = 280
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
end
