object FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio
  Left = 0
  Top = 0
  Width = 767
  Height = 380
  TabOrder = 0
  object DBLHistoria: TDBListEx
    Left = 0
    Top = 35
    Width = 767
    Height = 304
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'F. Modificaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'FechaHoraModificacion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'F. Compromiso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'FechaCompromiso'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'Usuario'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Prioridad'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'Prioridad'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'DescEstado'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 80
        Header.Caption = 'Responsable'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Responsable'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Comentario Cliente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ObservacionesComentariosCliente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Respuesta Concesionaria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ObservacionesRespuesta'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Comentario Ejecutante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ObservacionesEjecutante'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Comentario Solicitante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ObservacionesSolicitante'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDrawText = DBLHistoriaDrawText
    ExplicitWidth = 419
  end
  object Pencabezado: TPanel
    Left = 0
    Top = 0
    Width = 767
    Height = 35
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 419
    object Lmensaje: TLabel
      Left = 31
      Top = 12
      Width = 418
      Height = 47
      AutoSize = False
      Caption = 'Historia de los cambios que se produjeron'
      WordWrap = True
    end
    object Pizq: TPanel
      Left = 0
      Top = 6
      Width = 7
      Height = 29
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
    end
    object Pder: TPanel
      Left = 759
      Top = 6
      Width = 8
      Height = 29
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 411
    end
    object Parriva: TPanel
      Left = 0
      Top = 0
      Width = 767
      Height = 6
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitWidth = 419
    end
  end
  object Pabajo: TPanel
    Left = 0
    Top = 339
    Width = 767
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 419
  end
  object SpObtenerHistoriaOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'WS_ObtenerHistoriaOrdenServicio;1'
    Parameters = <>
    Left = 48
    Top = 120
  end
  object DataSource: TDataSource
    DataSet = SpObtenerHistoriaOrdenServicio
    Left = 144
    Top = 160
  end
end
