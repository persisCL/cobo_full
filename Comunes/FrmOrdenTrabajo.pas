unit FrmOrdenTrabajo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FreAltaOrdenServicio, ExtCtrls, ComCtrls, UtilDB, peaprocs, peatypes;

type
  TFormOrdenTrabajo = class(TForm)
    pnlBotones: TPanel;
    pnlDetalleOrdenServicio: TPanel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    FrameAltaOrdenServicio1: TFrameAltaOrdenServicio;
    Label1: TLabel;
    Label2: TLabel;
    sbMemo: TScrollBox;
    mmObservaciones: TMemo;
    cbTiposFallas: TComboBox;
    cbUbicaciones: TComboBox;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FrameAltaOrdenServicio1tvOrdenesServicioClick(
      Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Function Inicializar(OrdenServicio: integer): Boolean;
  end;

var
  FormOrdenTrabajo: TFormOrdenTrabajo;

implementation

uses DMConnection;

{$R *.dfm}

{ TFormSolicitudTrabajo }

function TFormOrdenTrabajo.Inicializar(OrdenServicio: integer): Boolean;
begin
    // Obtengo los datos tomados en la Orden de Servicio para generar la Nueva Orden
    // de Trabajo .
(*Pablo
    CargarTiposFallas(DMConnections.BaseCAC, cbTiposFallas,
    	QueryGetValueInt(DMConnections.BaseCAC,
        'SELECT CodigoTipoFalla FROM ordenesServicioSolicitudTrabajo WHERE CodigoOrdenServicio = ' +
        IntToStr(OrdenServicio)), false, False);
    CargarUbicacionesMantenimiento(DMConnections.BaseCAC, cbUbicaciones,
	    QueryGetValueInt(DMConnections.BaseCAC,
		'SELECT CodigoUbicacion FROM ordenesServicioSolicitudTrabajo WHERE CodigoOrdenServicio = ' +
        IntToStr(OrdenServicio)), False, False);
*)
    mmObservaciones.Text := QueryGetValue(DMConnections.BaseCAC,
    	'Select Observaciones FROM OrdenesServicio WHERE ' +
        ' CodigoOrdenServicio = '+ IntToStr(OrdenServicio));

	Result := FrameAltaOrdenServicio1.InicializarOrdenTrabajo(QueryGetValueInt(DMConnections.BaseCAC,
		'SELECT CodigoComunicacion FROM OrdenesServicio WHERE CodigoOrdenServicio = ' +
        IntToStr(OrdenServicio)));

end;

procedure TFormOrdenTrabajo.btnAceptarClick(Sender: TObject);
begin
	FrameAltaOrdenServicio1.Guardar;
	ModalResult := FrameAltaOrdenServicio1.Resultado;
    if ModalResult = mrOk then begin
        close;
        ModalResult := mrOk;
    end;
end;

procedure TFormOrdenTrabajo.btnCancelarClick(Sender: TObject);
begin
	ModalResult := FrameAltaOrdenServicio1.Resultado;
 	close;
end;

procedure TFormOrdenTrabajo.FrameAltaOrdenServicio1tvOrdenesServicioClick(
  Sender: TObject);
begin
    FrameAltaOrdenServicio1.tvOrdenesServicioClick(Sender);
end;

end.
