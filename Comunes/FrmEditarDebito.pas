unit FrmEditarDebito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Util, PeaTypes, PeaProcs, UtilDB,
  DPSControls;

type
  TFormEditarDebito = class(TForm)
	Label22: TLabel;
	lbl_entidad: TLabel;
	Label23: TLabel;
	cb_tipodebito: TComboBox;
	cb_entidaddebito: TComboBox;
	txt_cuentadebito: TEdit;
	Bevel1: TBevel;
	OPButton1: TDPSButton;
	OPButton2: TDPSButton;
    procedure OPButton1Click(Sender: TObject);
    procedure cb_tipodebitoChange(Sender: TObject);
  private
    function GetCuenta: AnsiString;
    function GetEntidad: Integer;
    function GetTipODebito: AnsiString;
    procedure SetCuenta(const Value: AnsiString);
	procedure SetEntidad(const Value: Integer);
    procedure SetTipoDebito(const Value: AnsiString);
	{ Private declarations }
  public
	{ Public declarations }
	property TipoDebito: AnsiString Read GetTipODebito Write SetTipoDebito;
	property CodigoEntidad: Integer Read GetEntidad Write SetEntidad;
	property CuentaDebito: AnsiString Read GetCuenta Write SetCuenta;
  end;

var
  FormEditarDebito: TFormEditarDebito;

implementation

uses DMConnection;

{$R *.dfm}

{ TFormEditarDebito }

function TFormEditarDebito.GetCuenta: AnsiString;
begin
	Result := txt_cuentadebito.Text;
end;

function TFormEditarDebito.GetEntidad: Integer;
begin
	Result := IVal(StrRight(cb_entidaddebito.Text, 20));
end;

function TFormEditarDebito.GetTipoDebito: AnsiString;
begin
	Result := iif(cb_tipodebito.ItemIndex = 0, DA_TARJETA_CREDITO, DA_CUENTA_BANCARIA);
end;

procedure TFormEditarDebito.SetCuenta(const Value: AnsiString);
begin
	txt_cuentadebito.Text := Trim(Value);
end;

procedure TFormEditarDebito.SetEntidad(const Value: Integer);
begin
	if cb_tipodebito.ItemIndex = 0 then begin
		CargarTiposTarjetasCreditoDebito(DMCOnnections.BaseCAC, cb_entidaddebito, DA_TARJETA_CREDITO, Value);
	end else begin
		CargarBancos(DMCOnnections.BaseCAC, cb_entidaddebito, Value);
	end;
end;

procedure TFormEditarDebito.SetTipoDebito(const Value: AnsiString);
begin
	if Value = DA_TARJETA_CREDITO then begin
		cb_tipodebito.ItemIndex := 0;
		CargarTiposTarjetasCreditoDebito(DMCOnnections.BaseCAC, cb_entidaddebito, DA_TARJETA_CREDITO);
	end else if Value = DA_CUENTA_BANCARIA then begin
		cb_tipodebito.ItemIndex := 1;
		CargarBancos(DMCOnnections.BaseCAC, cb_entidaddebito);
	end;
end;

procedure TFormEditarDebito.OPButton1Click(Sender: TObject);
resourcestring
    // Mensajes de validaci�n
    CAPTION_VALIDAR_DEBITO_AUTOMATICO = 'Validar Datos del Debito Autom�tico';
    MSG_TIPO_DEBITO       = 'Debe indicar el tipo de d�bito.';
    MSG_BANCO_TARJETA     = 'Debe indicar el banco o la tarjeta de cr�dito.';
    MSG_NUMERO_CUENTA     = 'Debe indicar n�mero de cuenta bancaria o tarjeta de cr�dito.';
begin
	if not ValidateControls(
	  [cb_tipodebito, cb_entidaddebito, txt_cuentadebito],
	  [cb_tipodebito.itemIndex >= 0,
	 cb_entidaddebito.Itemindex >= 0,
	 Trim(txt_cuentadebito.Text) <> ''],
	 CAPTION_VALIDAR_DEBITO_AUTOMATICO,
	 [MSG_TIPO_DEBITO, MSG_BANCO_TARJETA, MSG_NUMERO_CUENTA]) then Exit;
	ModalResult := mrOk;
end;

procedure TFormEditarDebito.cb_tipodebitoChange(Sender: TObject);
resourcestring
    CAPTION_TARJETA = 'Tarjeta:';
    CAPTION_BANCO   = 'Banco:';
begin
	TipoDebito := iif(cb_tipodebito.ItemIndex = 0, DA_TARJETA_CREDITO,
	  DA_CUENTA_BANCARIA);
	lbl_entidad.Caption := iif(TipoDebito = DA_TARJETA_CREDITO, CAPTION_TARJETA, CAPTION_BANCO);
end;

end.
