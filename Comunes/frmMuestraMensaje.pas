unit frmMuestraMensaje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TPanelMensajesForm = class(TForm)
    Panel1: TPanel;
    lbMensajes: TListBox;
    Label1: TLabel;
    imgMsgPanel: TImage;
    Image1: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure MuestraMensaje(Mensaje: string; Inicializar: Boolean = False);
    class procedure OcultaPanel;
  end;

var
  PanelMensajesForm: TPanelMensajesForm;

implementation

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}

class procedure TPanelMensajesForm.MuestraMensaje(Mensaje: string; Inicializar: Boolean = False);
begin
    if not Assigned(PanelMensajesForm) then begin
        PanelMensajesForm := TPanelMensajesForm.Create(Application.MainForm);
        {$IFDEF DESARROLLO} 
        	PanelMensajesForm.FormStyle := fsNormal;
		{$ENDIF}

//        Application.CreateForm(TPanelMensajesForm, PanelMensajesForm);
    end;

    with PanelMensajesForm do begin
        if Inicializar then lbMensajes.Clear;

        lbMensajes.ItemIndex := lbMensajes.Items.Add(Mensaje);
        if not PanelMensajesForm.Visible then begin
//            PanelMensajesForm.Top  := (Application.MainForm.Height - PanelMensajesForm.Height - 50) + Application.MainForm.Top;
//            PanelMensajesForm.left := (Application.MainForm.Width - PanelMensajesForm.Width - 25) + Application.MainForm.Left;

            PanelMensajesForm.Show;
        end else //;
            PanelMensajesForm.BringToFront; 
    end;
    Application.ProcessMessages;
end;

class procedure TPanelMensajesForm.OcultaPanel;
begin
    if Assigned(PanelMensajesForm) then begin
        {
        FreeAndNil(PanelMensajesForm);
        Application.ProcessMessages;
        }
        if PanelMensajesForm.Visible then begin
            PanelMensajesForm.Hide;
            Application.ProcessMessages;
        end;
    end;
end;

end.
