{********************************** Unit Header ********************************
File Name : CustomAssistance.pas
Author : gcasais
Date Created: 26/10/2005
Language : ES-AR
Description : Informaci�n de Soporte T�cnico para Costanera Norte Peaje
*******************************************************************************}
unit CustomAssistance;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, Util, ShellApi;

type
  TfrmLocalAssistance = class(TForm)
    Image1: TImage;
    lblChileText: TLabel;
    lblchile: TLabel;
    lblArgentinaText: TLabel;
    lblArgentina: TLabel;
    lblSalir: TLabel;
    procedure lblArgentinaClick(Sender: TObject);
    procedure lblSalirClick(Sender: TObject);
  private
    procedure EnviarMail(MailTo: Ansistring);
  public
    function Inicializar: Boolean;
  end;

var
  frmLocalAssistance: TfrmLocalAssistance;

implementation

{$R *.dfm}

{ TfrmLocalAssitance }

function TfrmLocalAssistance.Inicializar: Boolean;
begin
    Result := True;
end;

procedure TfrmLocalAssistance.lblSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmLocalAssistance.lblArgentinaClick(Sender: TObject);
begin
    EnviarMail((Sender as TLabel).Caption);
end;

procedure TfrmLocalAssistance.EnviarMail(MailTo: Ansistring);
resourcestring
    MSG_SUBJ = 'Solicitud de Asistencia T�cnica';
begin
    ShellExecute(0, 'open', PChar('mailto:' + MailTo + '?' + 'subject=' + MSG_SUBJ), nil, nil, SW_MAXIMIZE);
end;

end.

