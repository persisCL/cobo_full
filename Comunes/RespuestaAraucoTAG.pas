unit RespuestaAraucoTAG;

interface

uses InvokeRegistry, Types;

type
    /// <summary>Clase con el c�digo de mensaje y el mensaje de respuesta</summary>
    TMensaje = class(TRemotable)
        private
            FCodigoMensaje : Integer;
            FMensaje : string;
        published
            property CodigoMensaje : Integer read FCodigoMensaje write FCodigoMensaje;
            property Mensaje : string read FMensaje write FMensaje;
    end;

    /// <summary>Arreglo de tipo TMensaje el cual contendr� todos los mensajes s devolver con su respectivo c�digo</summary>
    TMensajes = array of TMensaje;

    /// <summary>Clase con los datos de la Patente que se recibir�n en el WebService</summary>
    TPatenteConsultar = class(TRemotable)
        private
            FPatente,
            FDigito : string;
            FEstadoPatente : TMensaje;
            function getMensaje : TMensaje;
            procedure setMensaje(const Value : TMensaje);
        published
            property Patente : string read FPatente write FPatente;
            property Digito : string read FDigito write FDigito;
            property EstadoPatente : TMensaje read getMensaje write setMensaje;
    end;

    /// <summary>Clase con los datos de la persona que se recibir�n en el WebService</summary>
    TPersona = class(TRemotable)
        private
            FRut,
            FNombre,
            FMail : string;
        published
            property Rut : string read FRut write FRut;
            property Nombre : string read FNombre write FNombre;
            property Mail : string read FMail write FMail;
    end;

    /// <summary>Arreglo de tipo TPatente el cual contendr� las patentes a devolver</summary>
    TPatentes = array of TPatenteConsultar;

    /// <summary>Clase Respuesta con la respuesta a enviar por el WebService</summary>
    TRespuesta = class(TRemotable)
        private
            FIDConsultaPAK : Integer;
            FPersona : TPersona;
            FPatentes : TPatentes;
            FPuedeAdherir : Boolean;
            FMensajeError : TMensajes;
            function getPersona : TPersona;
            procedure setPersona(const Value : TPersona);
        published
            constructor Create; overload;
            property ConsultaPAK : integer read FIDConsultaPAK write FIDConsultaPAK;
            property Persona : TPersona read getPersona write setPersona;
            property Patentes : TPatentes read FPatentes write FPatentes;
            property PuedeAdherir : Boolean read FPuedeAdherir write FPuedeAdherir;
            property Mensajes : TMensajes read FMensajeError write FMensajeError;
    end;

    /// <summary>Clase con la cabecera del WebService</summary>
    TAuth = class(TSOAPHeader)
        private
            FUsuario : string;
            Fpassword : string;
        published
            property UID : string read FUsuario write FUsuario;
            property PWD : string read Fpassword write Fpassword;
    end;

implementation

{$REGION 'TRespuesta'}

constructor TRespuesta.Create;
begin
    FPersona := TPersona.Create;
    SetLength(FPatentes, 1);
    SetLength(FMensajeError, 1);
end;

/// <summary>Devuelve la clase persona</summary>
function TRespuesta.getPersona;
begin
    if not Assigned(FPersona) then FPersona := TPersona.Create;
    Result := FPersona;
end;

/// <summary>Setea la Clase Persona</summary>
/// <param name="Value">Persona</param>
procedure TRespuesta.setPersona(const Value: TPersona);
begin
    FPersona := Value;
end;

{$ENDREGION}

{$REGION 'TPatenteConsultar'}
/// <summary>Devuelve la clase Mensaje</summary>
function TPatenteConsultar.getMensaje;
begin
    if not Assigned(FEstadoPatente) then FEstadoPatente := TMensaje.Create;
    Result := FEstadoPatente;
end;

/// <summary>Setea la Clase Mensaje</summary>
/// <param name="Value">Mensaje</param>
procedure TPatenteConsultar.setMensaje(const Value : TMensaje);
begin
    FEstadoPatente := Value;
end;
{$ENDREGION}

end.
