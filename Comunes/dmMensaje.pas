{

Etiqueta	: 20160530 MGO
Descripción : Se agregan spObtenerCorrespondencia, spObtenerDatosPersona y spObtenerDomicilioConvenio
				Se agrega QueryGetValueString

Etiqueta    : 20160530 MGO
Descripción : Se reemplaza Base específica por BaseOP_CAC de DMConnections
}

unit dmMensaje;

interface

uses
  SysUtils, Classes, DB, ADODB, util, utildb, eventlog, // 20160609 MGO
  DMConnection;                                         // 20160609 MGO

type
  TdmMensajes = class(TDataModule)
	//Base: TADOConnection; // 20160609 MGO
    spObtenerMensaje: TADOStoredProc;
    // INICIO : 20160530 MGO
    spObtenerCorrespondencia: TADOStoredProc;
    spObtenerDatosPersona: TADOStoredProc;
    spObtenerDomicilioConvenio: TADOStoredProc;
    // FIN : 20160530 MGO
    spMensajesSinEnviar: TADOStoredProc;
    spAgregarReintento: TADOStoredProc;
    spMarcarComoEnviado: TADOStoredProc;
    spAgregarMensaje: TADOStoredProc;
    spObtenerParametroGeneral: TADOStoredProc;
    { INICIO : 20160609 MGO
    procedure BaseWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BaseBeforeConnect(Sender: TObject);
    } // FIN : 20160609 MGO
  private
    { Private declarations }
  public
    { Public declarations }   
    // INICIO : 20160530 MGO
    function QueryGetValueString(QueryString: String): String;
    // FIN : 20160530 MGO
  end;

var
  dmMensajes: TdmMensajes;

implementation

{$R *.dfm}

{ INICIO : 20160609 MGO
function GetBase: string;
begin
    result := installini.ReadString('Database CAC', 'DatabaseName', 'OP_CAC');
end;

function GetServer: string;
begin
    result := installini.ReadString('Database CAC', 'Server', 'srv_database\dbcostanera');
end;


procedure TdmMensajes.BaseWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
Var
	DescriError: AnsiString;
begin
	if not SecureDatabase(Base, UserID, Password, DescriError, 'Parametros') then begin
        EventLogReportEvent(elError, 'Error al intentar asegurar la base de datos: ' + DescriError, '');
        EventStatus := esCancel;
	end;
end;

procedure TdmMensajes.BaseBeforeConnect(Sender: TObject);
begin
    Base.ConnectionString :=
      'Provider=SQLOLEDB.1;' +
      'User ID=sa;' +
      'Initial Catalog=' + GetBase + ';' +
      'Data Source=' + GetServer;
end;
} // FIN : 20160609 MGO

// INICIO : 20160530 MGO
function TdmMensajes.QueryGetValueString(QueryString: String): String;
begin
    { INICIO : 20160609 MGO
    Result := QueryGetValue(Base, QueryString);
    }
    Result := QueryGetValue(DMConnections.BaseCAC, QueryString);
    // FIN : 20160609 MGO
end;
// FIN : 20160530 MGO

end.
