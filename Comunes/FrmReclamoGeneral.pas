{-------------------------------------------------------------------------------
 File Name: FrmReclamoGeneral.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos

 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 29-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y SubTipoReclamo

  Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos
 -------------------------------------------------------------------------------}
unit FrmReclamoGeneral;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //Nulldate
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  PeaProcs,                                                                     // TASK_88_PAN_20170215
  PeaTypes,
  FreContactoReclamo,             //Frame contacto
  FreCompromisoOrdenServicio,     //Frame compromiso
  FreSolucionOrdenServicio,       //Frame solucion
  FreHistoriaOrdenServicio,       //Frame Historia
  FreFuenteReclamoOrdenServicio,  //Frame Fuente de Reclamo
  FreNumeroOrdenServicio,         //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,      //Frame Respuesta Orden Servicio
  OrdenesServicio,                //Registra la orden de  servicio
  ABMPersonas,                    // TASK_88_PAN_20170215
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, {FreSubTipoReclamoOrdenServicio,}                            // _PAN_
  FreConcesionariaReclamoOrdenServicio, DBClient;

type
  TFormReclamoGeneral = class(TForm)
    spObtenerOrdenServicio: TADOStoredProc;
    PAbajo: TPanel;
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
    Ldetallesdelreclamo: TLabel;
    txtDetalle: TMemo;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameHistoriaOrdenServicio1: TFrameHistoriaOrdenServicio;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
    TabSheetAsociados: TTabSheet;
    dsAsociados: TClientDataSet;
    lb_Asociados: TListBox;
    lb_Posibles: TListBox;
    btn_Agregar: TButton;
    btn_Quitar: TButton;
    spObtenerReclamosAsociados: TADOStoredProc;
    //FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;               // _PAN_
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FrameContactoReclamo1btnNuevoClick(Sender: TObject);
    procedure FrameContactoReclamo1txtNumeroDocumentoExit(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure btn_AgregarClick(Sender: TObject);
    procedure btn_QuitarClick(Sender: TObject);
    procedure FrameContactoReclamo1txtNumeroDocumentoButtonClick(
      Sender: TObject);
    procedure lb_PosiblesDblClick(Sender: TObject);
    procedure lb_AsociadosDblClick(Sender: TObject);              // TASK_88_PAN_20170215
  private
    { Private declarations }
    FTipoOrdenServicio : Integer;
    FCodigoOrdenServicio : Integer;
    FEditando : Boolean; //Indica si estoy editando o no
    Function GetCodigoOrdenServicio : Integer;
    procedure GetReclamosAsociados(Nrodocumento: string; CodigoOrdenServicio: integer);
    Function  GuardarReclamosAsociados(CodigoOrdenServicio: Integer): Boolean;
  public
    EsNuevo: Boolean;
    { Public declarations }
    Function Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0) : Boolean;
    Function InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
    Property CodigoOrdenServicio: Integer Read GetCodigoOrdenServicio;
  end;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoGeneral.Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0) : Boolean;
Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo General';
    MSG_ERROR = 'Error';
Const
    STR_RECLAMO = 'Reclamo general - ';
Var
  	Sz: TSize;
    IDPrioridad: Integer;                                                       // _PAA_
Begin
    EsNuevo:= False;
    FTipoOrdenServicio := TipoOrdenServicio;
    try
        //Centro el form
        CenterForm(Self);

        //Ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);

        //Titulo
        Caption := STR_RECLAMO + QueryGetValue(DMConnections.BaseCAC, Format(
                   'select dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
                   [TipoOrdenServicio]));

        IDPrioridad:= QueryGetValueInt(dmconnections.basecac, 'SELECT PriodidadPorDefecto FROM TiposOrdenServicio  WITH (NOLOCK) WHERE TipoOrdenServicio = '+IntToStr(FTipoOrdenServicio)+'');  //_PAA_
                          
        PageControl.ActivePageIndex := 0;

        ActiveControl := FrameContactoReclamo1.txtNumeroDocumento; //txtDetalle

        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(IDPrioridad) and     // _PAN_
                                FrameSolucionOrdenServicio1.Inicializar(FTipoOrdenServicio) and     // _PAN_
                                    FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar {and   // _PAN_
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar};        // _PAN_
                                        //---------------------------------------------------------------


        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //Termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    FrameNumeroOrdenServicio1.Visible := False;

    //No estoy Editando
    FEditando := False;

    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE
      then
      FrameCompromisoOrdenServicio1.FechaCompromiso:=ObtenerFechaComprometidaOS(now,
                                                                                    FTipoOrdenServicio,
                                                                                    FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                                                                    FrameCompromisoOrdenServicio1.Prioridad);
    AddOSForm(self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:  DDeMarco
  Date Created:
  Description: Modifico Reclamo que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoGeneral.InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;

    //form read Only
    Procedure ReadOnly;
    begin
        EsNuevo:= False;
        FrameContactoReclamo1.Deshabilitar;
        FrameCompromisoOrdenServicio1.Enabled := False;
        FrameSolucionordenServicio1.Enabled := False;
        FrameRespuestaOrdenServicio1.Deshabilitar;
        txtdetalle.ReadOnly := True;
        txtDetalleSolucion.ReadOnly := True;
        CKRetenerOrden.Visible := False;
        btncancelar.Caption := 'Salir';
        aceptarbtn.Visible := False;
    end;

var
    Estado: String;
begin
    //asigo el codigoordenservicio recibido a una varible global
    FCodigoOrdenServicio := CodigoOrdenServicio;
    //Asigno los parametros
    with spObtenerOrdenServicio.Parameters do begin
        ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    end;
    //Abro la consulta
    Result := OpenTables([spObtenerOrdenServicio]);
    //si no puede abrir la consulta sale
    if not Result then Exit;
    //Cargamos los datos
    Result := Inicializar(spObtenerOrdenServicio['TipoOrdenServicio']);
    //Obtengo el detalle, fuente y estado del reclamo
    with spObtenerOrdenServicio do begin
        txtDetalle.Text := FieldByName('ObservacionesSolicitante').AsString;
        txtDetalleSolucion.Text := FieldByName('ObservacionesEjecutante').AsString;
        Estado := FieldByName('Estado').AsString;
    end;
    //cargo los frames
    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameHistoriaOrdenServicio1.Inicializar(CodigoOrdenServicio);
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    //Rev.1 / 07-Julio-2010 / Nelson Droguett Sierra ----------------------------------
    FrameConcesionariaReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    //FrameSubTipoReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);        // _PAN_
    //FinRev.1-------------------------------------------------------------------------
    // Listo
    spObtenerOrdenServicio.Close;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;
    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(inttostr(CodigoOrdenServicio));
    //por Default no retengo la orden de servicio
    CKRetenerOrden.Checked := False;
    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;
    //Estoy Editando
    FEditando := True;
end;


{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:  DDeMarco
  Date Created:
  Description: Soluciono un Reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoGeneral.InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
begin
    EsNuevo:= False;
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.TabIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

procedure TFormReclamoGeneral.PageControlChange(Sender: TObject);
begin
  if PageControl.ActivePage.Name = 'TabSheetAsociados' then
  GetReclamosAsociados(FrameContactoReclamo1.txtNumeroDocumento.Text, FCodigoOrdenServicio);
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormReclamoGeneral.GetCodigoOrdenServicio: Integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.FormKeyPress(Sender: TObject; var Key: Char);
resourcestring
    MSG_CANCELAR_RECLAMO = '�Desea cancelar el reclamo?';
    MSG_CANCELAR_CAPTION = 'Cancelar reclamo';
begin
    if Key = #27 then
        if 	MsgBox(MSG_CANCELAR_RECLAMO, MSG_CANCELAR_CAPTION, MB_YESNO + MB_ICONQUESTION) = mrYes then  //TASK_020_JMA_20160616
            btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: registra el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar: Boolean;
    Resourcestring
        MSG_ERROR_DET    = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA  = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                           'debe ser menor a la Fecha Comprometida con el Cliente';
    begin
        Result := False;

        //Validamos fuente de reclamo
        if FEditando = False then begin
             if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            result:=true;
            exit;
        end;

        if EsNuevo then begin
          FrameContactoReclamo1.txtNumeroConvenio.Clear;
          FrameContactoReclamo1.txtNumeroConvenio.text:= '-';
          FrameContactoReclamo1.txtNumeroConvenio.ItemIndex:= 0
        end;

        if not FEditando then
            if not EsNuevo then
                //Validamos identificacion de usuario y contacto
                if not FrameContactoReclamo1.ValidoConvenio then Exit;

        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = false then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            txtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;

        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802

        Result := True;

    end;

Var
    OS: Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    // Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguet Sierra ----------------------------------
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaBefore,       // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaAfter,        // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteBefore,  // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteAfter    // _PAN_
                                   {FrameSubTipoReclamoOrdenServicio1}                              // _PAN_
                                   );
    //FinRev.1-------------------------------------------------------------------------
                                   
    if OS > 0 then begin

        //si es un reclamo nuevo
        if FEditando = False then begin

            //informo al operador el numero de Reclamo que se genero
            InformarNumeroReclamo(IntToStr(OS));
            DesbloquearOrdenServicio(OS);
        end;
        GuardarReclamosAsociados(OS);
        close;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.FormDestroy(Sender: TObject);
begin
    RemoveOSform(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: desbloqueo la orden de servicio antes de salir
               y libero el formulario de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //si se esta modificando un reclamo
    if FEditando = True then begin
        //verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = False then begin

            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //lo libero de memoria
    Action := caFree;
    EsNuevo:= False;
end;

procedure TFormReclamoGeneral.FrameContactoReclamo1btnNuevoClick(               // TASK_88_PAN_20170215
  Sender: TObject);                                                             // TASK_88_PAN_20170215
var NroRUT: string;   f: TfrmABMPersonas;                                       // TASK_88_PAN_20170215
begin
    if GNumeroTurno < 1 then begin
        MsgBox(MSG_ERROR_TURNO, 'Turno Cerrado', MB_ICONSTOP);
        Exit;
    end;



    EsNuevo:= True;                                                                          // TASK_88_PAN_20170215
    try
        NroRUT:= FrameContactoReclamo1.txtNumeroDocumento.Text;
        f :=  TfrmABMPersonas.Create(Self);
        //f.Inicializar(False);
        f.Inicio(True, NroRUT);
        if f.ShowModal = mrOk then begin
            NroRUT:= f.Persona.Datos.NumeroDocumento;
        end;

        //  NroRUT:= FrameContactoReclamo1.txtNumeroDocumento.Text;                       // TASK_88_PAN_20170215
          FrameContactoReclamo1.txtnumeroconvenio.text:= '0';                                                 // TASK_88_PAN_20170215
          FrameContactoReclamo1.ObtenerDatosPersona('0', -1);                                                  // TASK_88_PAN_20170215
          CargarConveniosRUT(DMCOnnections.BaseCAC,FrameContactoReclamo1.TxtNumeroConvenio, 'RUT', '0', False, 0, True, True); // TASK_88_PAN_20170215
          FrameContactoReclamo1.txtNumeroDocumentoChange(Sender);                       // TASK_88_PAN_20170215
          PageControl.ActivePageIndex := 0;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;

procedure TFormReclamoGeneral.FrameContactoReclamo1txtNumeroDocumentoButtonClick(
  Sender: TObject);
begin
  PageControl.ActivePageIndex := 0;
  FrameContactoReclamo1.txtNumeroDocumentoButtonClick(Sender);
end;

procedure TFormReclamoGeneral.FrameContactoReclamo1txtNumeroDocumentoExit(
  Sender: TObject);
begin
  FrameContactoReclamo1.txtNumeroDocumentoExit(Sender);                     // TASK_88_PAN_20170215

end;

procedure TFormReclamoGeneral.GetReclamosAsociados(Nrodocumento: string; CodigoOrdenServicio: integer);
begin
    with spObtenerReclamosAsociados do begin
      Close;
      Parameters.ParamByName('@NumeroDocumento').Value := Nrodocumento;
      Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
      Open;
      First;
    end;
    lb_Asociados.Clear;
    lb_Posibles.Clear;
    While not spObtenerReclamosAsociados.eof do begin
      if spObtenerReclamosAsociados.FieldByName('ParentID').AsInteger > 0
        then lb_Asociados.Items.Add(spObtenerReclamosAsociados.FieldByName('Data').AsString)
        else lb_Posibles.Items.Add(spObtenerReclamosAsociados.FieldByName('Data').AsString);
      spObtenerReclamosAsociados.Next;
    end;
end;

procedure TFormReclamoGeneral.btn_AgregarClick(Sender: TObject);
Var
  i: Integer;
begin
  i := 0;
  while i < lb_Posibles.Items.Count do
    if lb_Posibles.Selected[i] then begin
      lb_Asociados.Items.Add(lb_Posibles.items[i]);
      lb_Posibles.Items.Delete(i);
    end
    else Inc(i);
  //GetReclamosAsociados(FrameContactoReclamo1.txtNumeroDocumento.Text, FCodigoOrdenServicio);
end;

procedure TFormReclamoGeneral.btn_QuitarClick(Sender: TObject);
Var
  i: Integer;
begin
  i := 0;
  while i < lb_Asociados.Items.Count do
    if lb_Asociados.Selected[i] then begin
      lb_Posibles.Items.Add(lb_Asociados.items[i]);
      lb_Asociados.Items.Delete(i);
    end
    else Inc(i);
  //GetReclamosAsociados(FrameContactoReclamo1.txtNumeroDocumento.Text, FCodigoOrdenServicio);
end;

procedure TFormReclamoGeneral.lb_AsociadosDblClick(Sender: TObject);
begin
   btn_QuitarClick(Sender);
end;

procedure TFormReclamoGeneral.lb_PosiblesDblClick(Sender: TObject);
begin
   btn_AgregarClick(Sender);
end;

function TFormReclamoGeneral.GuardarReclamosAsociados(CodigoOrdenServicio: Integer): Boolean;
resourcestring
    MSG_SAVE_ERROR = 'Error al actualizar los reclamados asociados';
    MSG_ERROR = 'Error';
Var
    I: Integer; Asociado: string;
begin
    try
        for I := 0 to lb_Asociados.items.count - 1 do begin
          Asociado:= Trim(copy(lb_Asociados.items[I], 227, 10));
          QueryExecute(DMConnections.BaseCAC,
          'UPDATE OrdenesServicio SET ParentID = '+inttostr(CodigoOrdenServicio)+' '+
          'WHERE CodigoOrdenServicio = '+Asociado+'');
        end;

        for I := 0 to lb_Posibles.items.count - 1 do begin
          Asociado:= Trim(copy(lb_Posibles.items[I], 227, 10));
          QueryExecute(DMConnections.BaseCAC,
		  'UPDATE OrdenesServicio SET ParentID = null '+
          'WHERE CodigoOrdenServicio = '+Asociado+'');
        end;

        Result := True;
    except
        on E : Exception do begin
            Result := False;
            MsgBoxErr(MSG_SAVE_ERROR, E.message, MSG_ERROR, MB_ICONSTOP);
        end;
    end;
end;

end.
