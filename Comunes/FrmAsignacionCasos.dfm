object FrAsignacionCasos: TFrAsignacionCasos
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Asignaci'#243'n Masiva de Casos'
  ClientHeight = 95
  ClientWidth = 675
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_Area: TLabel
    Left = 22
    Top = 16
    Width = 72
    Height = 13
    Caption = 'Area Atencion:'
  end
  object lblUsuarios: TLabel
    Left = 307
    Top = 16
    Width = 45
    Height = 13
    Caption = 'Usuarios:'
  end
  object EEntidad: TVariantComboBox
    Left = 100
    Top = 8
    Width = 194
    Height = 21
    Style = vcsDropDownList
    DropDownCount = 10
    ItemHeight = 13
    TabOrder = 0
    OnSelect = EEntidadSelect
    Items = <
      item
        Caption = '(Seleccionar)'
        Value = '0'
      end>
  end
  object cbUsuarios: TVariantComboBox
    Left = 358
    Top = 8
    Width = 213
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items = <>
  end
  object chkBloquea: TCheckBox
    Left = 584
    Top = 8
    Width = 97
    Height = 17
    Caption = 'Bloquea'
    TabOrder = 2
  end
  object btnAceptar: TButton
    Left = 488
    Top = 61
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object btnSalir: TButton
    Left = 584
    Top = 61
    Width = 75
    Height = 25
    Caption = 'Salir'
    TabOrder = 4
    OnClick = btnSalirClick
  end
  object spObtenerAreasAtencionDeCasosTipoOrdenServicios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerAreasAtencionDeCasosTipoOrdenServicios;1'
    Parameters = <
      item
        Name = '@Tipo'
        DataType = ftInteger
        Value = Null
      end>
    Left = 144
  end
end
