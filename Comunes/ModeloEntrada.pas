unit ModeloEntrada;

interface

uses xmldom, XMLDoc, XMLIntf, Util, Variants;

type

{ Forward Decls }

  IXMLRNUTType = interface;
  IXMLContratoType = interface;
  IXMLContratoTypeList = interface;
  IXMLAccionType = interface;
  IXMLEntidadType = interface;
  IXMLEntidadTypeList = interface;
  IXMLAsociadaType = interface;
  IXMLAsociadaTypeList = interface;
  IXMLContactoType = interface;
  IXMLContactoTypeList = interface;
  IXMLDireccionType = interface;
  IXMLDireccionTypeList = interface;
  IXMLVehiculoType = interface;
  IXMLVehiculoTypeList = interface;
  IXMLTAGType = interface;
  IXMLTAGTypeList = interface;
  IXMLListaType = interface;
  IXMLListaTypeList = interface;
  IXMLTAGLType = interface;
  IXMLTAGLTypeList = interface;

{ IXMLRNUTType }

  IXMLRNUTType = interface(IXMLNode)
    ['{60B43DA9-52CF-4C72-821D-0DF01F3CEE00}']
    { Property Accessors }
    function Get_Contrato: IXMLContratoTypeList;
    function Get_Lista: IXMLListaTypeList;
    { Methods & Properties }
    property Contrato: IXMLContratoTypeList read Get_Contrato;
    property Lista: IXMLListaTypeList read Get_Lista;
  end;

{ IXMLContratoType }

  IXMLContratoType = interface(IXMLNode)
    ['{6BEE4606-37DA-4D27-A3F7-6D8DA438CFCF}']
    { Property Accessors }
    function Get_Concesionaria: Integer;
    function Get_Accion: IXMLAccionType;
    function Get_Codigo: String;
    function Get_Estado: Integer;
    function Get_Segundario: WideString;
    function Get_Entidad: IXMLEntidadTypeList;
    function Get_Direccion: IXMLDireccionTypeList;
    function Get_Vehiculo: IXMLVehiculoTypeList;
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_Codigo(Value: String);
    procedure Set_Estado(Value: Integer);
    procedure Set_Segundario(Value: WideString);
    { Methods & Properties }
    property Concesionaria: Integer read Get_Concesionaria write Set_Concesionaria;
    property Accion: IXMLAccionType read Get_Accion;
    property Codigo: string read Get_Codigo write Set_Codigo;
    property Estado: Integer read Get_Estado write Set_Estado;
    property Segundario: WideString read Get_Segundario write Set_Segundario;
    property Entidad: IXMLEntidadTypeList read Get_Entidad;
    property Direccion: IXMLDireccionTypeList read Get_Direccion;
    property Vehiculo: IXMLVehiculoTypeList read Get_Vehiculo;
  end;

{ IXMLContratoTypeList }

  IXMLContratoTypeList = interface(IXMLNodeCollection)
    ['{04EC6BC6-886A-4A06-8F03-EE22E09A7EA8}']
    { Methods & Properties }
    function Add: IXMLContratoType;
    function Insert(const Index: Integer): IXMLContratoType;
    function Get_Item(Index: Integer): IXMLContratoType;
    property Items[Index: Integer]: IXMLContratoType read Get_Item; default;
  end;

{ IXMLAccionType }

  IXMLAccionType = interface(IXMLNode)
    ['{7CF21EA1-99F3-45B0-BDCC-FD9E5A604FEB}']
    { Property Accessors }
    function Get_FechaAccion: WideString;
    procedure Set_FechaAccion(Value: WideString);
    { Methods & Properties }
    property FechaAccion: WideString read Get_FechaAccion write Set_FechaAccion;
  end;

{ IXMLEntidadType }

  IXMLEntidadType = interface(IXMLNode)
    ['{881079C2-8E7F-4A32-87CE-4FBD8CEF8F88}']
    { Property Accessors }
    function Get_Accion: Integer;
    function Get_RUT: WideString;
    function Get_DV: WideString;
    function Get_Tipo: Integer;
    function Get_Nombre: WideString;
    function Get_Apellidop: WideString;
    function Get_Apellidom: WideString;
    function Get_Fantasia: WideString;
    function Get_Giro: WideString;
    function Get_Rol: Integer;
    function Get_Asociada: IXMLAsociadaTypeList;
    function Get_Contacto: IXMLContactoTypeList;
    function Get_Direccion: IXMLDireccionTypeList;
    procedure Set_Accion(Value: Integer);
    procedure Set_RUT(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Tipo(Value: Integer);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Apellidop(Value: WideString);
    procedure Set_Apellidom(Value: WideString);
    procedure Set_Fantasia(Value: WideString);
    procedure Set_Giro(Value: WideString);
    procedure Set_Rol(Value: Integer);
    { Methods & Properties }
    property Accion: Integer read Get_Accion write Set_Accion;
    property RUT: WideString read Get_RUT write Set_RUT;
    property DV: WideString read Get_DV write Set_DV;
    property Tipo: Integer read Get_Tipo write Set_Tipo;
    property Nombre: WideString read Get_Nombre write Set_Nombre;
    property Apellidop: WideString read Get_Apellidop write Set_Apellidop;
    property Apellidom: WideString read Get_Apellidom write Set_Apellidom;
    property Fantasia: WideString read Get_Fantasia write Set_Fantasia;
    property Giro: WideString read Get_Giro write Set_Giro;
    property Rol: Integer read Get_Rol write Set_Rol;
    property Asociada: IXMLAsociadaTypeList read Get_Asociada;
    property Contacto: IXMLContactoTypeList read Get_Contacto;
    property Direccion: IXMLDireccionTypeList read Get_Direccion;
  end;

{ IXMLEntidadTypeList }

  IXMLEntidadTypeList = interface(IXMLNodeCollection)
    ['{6631138E-B252-4CBE-855E-EEA33EF8024C}']
    { Methods & Properties }
    function Add: IXMLEntidadType;
    function Insert(const Index: Integer): IXMLEntidadType;
    function Get_Item(Index: Integer): IXMLEntidadType;
    property Items[Index: Integer]: IXMLEntidadType read Get_Item; default;
  end;

{ IXMLAsociadaType }

  IXMLAsociadaType = interface(IXMLNode)
    ['{F9947F25-7EAE-4EBD-9BAD-63DA3EEEA546}']
    { Property Accessors }
    function Get_RUT: WideString;
    function Get_DV: WideString;
    function Get_Rol: Integer;
    procedure Set_RUT(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Rol(Value: Integer);
    { Methods & Properties }
    property RUT: WideString read Get_RUT write Set_RUT;
    property DV: WideString read Get_DV write Set_DV;
    property Rol: Integer read Get_Rol write Set_Rol;
  end;

{ IXMLAsociadaTypeList }

  IXMLAsociadaTypeList = interface(IXMLNodeCollection)
    ['{6627916F-0D4D-4F3D-BC90-352AC9C97C12}']
    { Methods & Properties }
    function Add: IXMLAsociadaType;
    function Insert(const Index: Integer): IXMLAsociadaType;
    function Get_Item(Index: Integer): IXMLAsociadaType;
    property Items[Index: Integer]: IXMLAsociadaType read Get_Item; default;
  end;

{ IXMLContactoType }

  IXMLContactoType = interface(IXMLNode)
    ['{18105E7E-288F-4988-B829-BC68CE0F076B}']
    { Property Accessors }
    function Get_Accion: Integer;
    function Get_Dato: WideString;
    function Get_Tipo: Integer;
    function Get_Observacion: WideString;
    procedure Set_Accion(Value: Integer);
    procedure Set_Dato(Value: WideString);
    procedure Set_Tipo(Value: Integer);
    procedure Set_Observacion(Value: WideString);
    { Methods & Properties }
    property Accion: Integer read Get_Accion write Set_Accion;
    property Dato: WideString read Get_Dato write Set_Dato;
    property Tipo: Integer read Get_Tipo write Set_Tipo;
    property Observacion: WideString read Get_Observacion write Set_Observacion;
  end;

{ IXMLContactoTypeList }

  IXMLContactoTypeList = interface(IXMLNodeCollection)
    ['{8E03CE84-847F-463F-B10E-9E07B2CC91BC}']
    { Methods & Properties }
    function Add: IXMLContactoType;
    function Insert(const Index: Integer): IXMLContactoType;
    function Get_Item(Index: Integer): IXMLContactoType;
    property Items[Index: Integer]: IXMLContactoType read Get_Item; default;
  end;

{ IXMLDireccionType }

  IXMLDireccionType = interface(IXMLNode)
    ['{B2344A48-004A-48E4-AF93-CED099661EEC}']
    { Property Accessors }
    function Get_Accion: Integer;
    function Get_Codigo: string;
    function Get_Tipo: Integer;
    function Get_Calle: WideString;
    function Get_Comuna: WideString;
    function Get_Altura: WideString;
    function Get_Depto: WideString;
    function Get_Zip: WideString;
    function Get_Comentario: WideString;
    procedure Set_Accion(Value: Integer);
    procedure Set_Codigo(Value: string);
    procedure Set_Tipo(Value: Integer);
    procedure Set_Calle(Value: WideString);
    procedure Set_Comuna(Value: WideString);
    procedure Set_Altura(Value: WideString);
    procedure Set_Depto(Value: WideString);
    procedure Set_Zip(Value: WideString);
    procedure Set_Comentario(Value: WideString);
    { Methods & Properties }
    property Accion: Integer read Get_Accion write Set_Accion;
    property Codigo: string read Get_Codigo write Set_Codigo;
    property Tipo: Integer read Get_Tipo write Set_Tipo;
    property Calle: WideString read Get_Calle write Set_Calle;
    property Comuna: WideString read Get_Comuna write Set_Comuna;
    property Altura: WideString read Get_Altura write Set_Altura;
    property Depto: WideString read Get_Depto write Set_Depto;
    property Zip: WideString read Get_Zip write Set_Zip;
    property Comentario: WideString read Get_Comentario write Set_Comentario;
  end;

{ IXMLDireccionTypeList }

  IXMLDireccionTypeList = interface(IXMLNodeCollection)
    ['{6D595FA9-A874-4FD0-A881-99616B27402C}']
    { Methods & Properties }
    function Add: IXMLDireccionType;
    function Insert(const Index: Integer): IXMLDireccionType;
    function Get_Item(Index: Integer): IXMLDireccionType;
    property Items[Index: Integer]: IXMLDireccionType read Get_Item; default;
  end;

{ IXMLVehiculoType }

  IXMLVehiculoType = interface(IXMLNode)
    ['{C06E8DD4-C747-465A-9836-882FF9570256}']
    { Property Accessors }
    function Get_Accion: IXMLAccionType;
    function Get_Patente: WideString;
    function Get_DV: WideString;
    function Get_Categoria: Integer;
    function Get_Catinturb: Integer;
    function Get_Marca: WideString;
    function Get_Modelo: WideString;
    function Get_Version: WideString;
    function Get_Color: WideString;
    function Get_Anio: Integer;
    function Get_TAG: IXMLTAGTypeList;
    procedure Set_Patente(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Categoria(Value: Integer);
    procedure Set_Catinturb(Value: Integer);
    procedure Set_Marca(Value: WideString);
    procedure Set_Modelo(Value: WideString);
    procedure Set_Version(Value: WideString);
    procedure Set_Color(Value: WideString);
    procedure Set_Anio(Value: Integer);
    { Methods & Properties }
    property Accion: IXMLAccionType read Get_Accion;
    property Patente: WideString read Get_Patente write Set_Patente;
    property DV: WideString read Get_DV write Set_DV;
    property Categoria: Integer read Get_Categoria write Set_Categoria;
    property Catinturb: Integer read Get_Catinturb write Set_Catinturb;
    property Marca: WideString read Get_Marca write Set_Marca;
    property Modelo: WideString read Get_Modelo write Set_Modelo;
    property Version: WideString read Get_Version write Set_Version;
    property Color: WideString read Get_Color write Set_Color;
    property Anio: Integer read Get_Anio write Set_Anio;
    property TAG: IXMLTAGTypeList read Get_TAG;
  end;

{ IXMLVehiculoTypeList }

  IXMLVehiculoTypeList = interface(IXMLNodeCollection)
    ['{C9A5A70A-3909-4978-929B-C0C7E21135F8}']
    { Methods & Properties }
    function Add: IXMLVehiculoType;
    function Insert(const Index: Integer): IXMLVehiculoType;
    function Get_Item(Index: Integer): IXMLVehiculoType;
    property Items[Index: Integer]: IXMLVehiculoType read Get_Item; default;
  end;

{ IXMLTAGType }

  IXMLTAGType = interface(IXMLNode)
    ['{AEC98CDB-23AA-4B7B-8A92-61ABB3C92CB8}']
    { Property Accessors }
    function Get_Accion: Integer;
    function Get_Serie: String;
    function Get_Fabricante: Integer;
    function Get_Caturb: Integer;
    function Get_Catinturb: Integer;
    function Get_Context: Integer;
    function Get_Contract: cardinal;
    function Get_Estado: Integer;
    function Get_Instal: WideString;
    procedure Set_Accion(Value: Integer);
    procedure Set_Serie(Value: String);
    procedure Set_Fabricante(Value: Integer);
    procedure Set_Caturb(Value: Integer);
    procedure Set_Catinturb(Value: Integer);
    procedure Set_Context(Value: Integer);
    procedure Set_Contract(Value: cardinal);
    procedure Set_Estado(Value: Integer);
    procedure Set_Instal(Value: WideString);
    { Methods & Properties }
    property Accion: Integer read Get_Accion write Set_Accion;
    property Serie: String read Get_Serie write Set_Serie;
    property Fabricante: Integer read Get_Fabricante write Set_Fabricante;
    property Caturb: Integer read Get_Caturb write Set_Caturb;
    property Catinturb: Integer read Get_Catinturb write Set_Catinturb;
    property Context: Integer read Get_Context write Set_Context;
    property Contract: cardinal read Get_Contract write Set_Contract;
    property Estado: Integer read Get_Estado write Set_Estado;
    property Instal: WideString read Get_Instal write Set_Instal;
  end;

{ IXMLTAGTypeList }

  IXMLTAGTypeList = interface(IXMLNodeCollection)
    ['{0FC2CFFC-3B3A-4E00-9C38-8487583247E5}']
    { Methods & Properties }
    function Add: IXMLTAGType;
    function Insert(const Index: Integer): IXMLTAGType;
    function Get_Item(Index: Integer): IXMLTAGType;
    property Items[Index: Integer]: IXMLTAGType read Get_Item; default;
  end;

{ IXMLListaType }

  IXMLListaType = interface(IXMLNode)
    ['{769A9DE6-9662-4121-9E7C-2BFD6BBD62BC}']
    { Property Accessors }
    function Get_Concesionaria: Integer;
    function Get_Accion: Integer;
    function Get_Color: Integer;
    function Get_Fechalista: WideString;
    function Get_TAGL: IXMLTAGLTypeList;
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_Accion(Value: Integer);
    procedure Set_Color(Value: Integer);
    procedure Set_Fechalista(Value: WideString);
    { Methods & Properties }
    property Concesionaria: Integer read Get_Concesionaria write Set_Concesionaria;
    property Accion: Integer read Get_Accion write Set_Accion;
    property Color: Integer read Get_Color write Set_Color;
    property Fechalista: WideString read Get_Fechalista write Set_Fechalista;
    property TAGL: IXMLTAGLTypeList read Get_TAGL;
  end;

{ IXMLListaTypeList }

  IXMLListaTypeList = interface(IXMLNodeCollection)
    ['{7BC9920B-18F7-4924-A3C7-B2CCC0F0CC7E}']
    { Methods & Properties }
    function Add: IXMLListaType;
    function Insert(const Index: Integer): IXMLListaType;
    function Get_Item(Index: Integer): IXMLListaType;
    property Items[Index: Integer]: IXMLListaType read Get_Item; default;
  end;

{ IXMLTAGLType }

  IXMLTAGLType = interface(IXMLNode)
    ['{6ED3ED62-AB2E-498B-A278-1E7AEEDCAFE2}']
    { Property Accessors }
    function Get_Serie: String;
    function Get_Fab: Integer;
    procedure Set_Serie(Value: String);
    procedure Set_Fab(Value: Integer);
    { Methods & Properties }
    property Serie: String read Get_Serie write Set_Serie;
    property Fab: Integer read Get_Fab write Set_Fab;
  end;

{ IXMLTAGLTypeList }

  IXMLTAGLTypeList = interface(IXMLNodeCollection)
    ['{4D66C713-D7B8-465C-A4D1-3073EDB64236}']
    { Methods & Properties }
    function Add: IXMLTAGLType;
    function Insert(const Index: Integer): IXMLTAGLType;
    function Get_Item(Index: Integer): IXMLTAGLType;
    property Items[Index: Integer]: IXMLTAGLType read Get_Item; default;
  end;

{ Forward Decls }

  TXMLRNUTType = class;
  TXMLContratoType = class;
  TXMLContratoTypeList = class;
  TXMLAccionType = class;
  TXMLEntidadType = class;
  TXMLEntidadTypeList = class;
  TXMLAsociadaType = class;
  TXMLAsociadaTypeList = class;
  TXMLContactoType = class;
  TXMLContactoTypeList = class;
  TXMLDireccionType = class;
  TXMLDireccionTypeList = class;
  TXMLVehiculoType = class;
  TXMLVehiculoTypeList = class;
  TXMLTAGType = class;
  TXMLTAGTypeList = class;
  TXMLListaType = class;
  TXMLListaTypeList = class;
  TXMLTAGLType = class;
  TXMLTAGLTypeList = class;

{ TXMLRNUTType }

  TXMLRNUTType = class(TXMLNode, IXMLRNUTType)
  private
    FContrato: IXMLContratoTypeList;
    FLista: IXMLListaTypeList;
  protected
    { IXMLRNUTType }
    function Get_Contrato: IXMLContratoTypeList;
    function Get_Lista: IXMLListaTypeList;
  public
    procedure AfterConstruction; override;
    destructor Destroy;override;
  end;

{ TXMLContratoType }

  TXMLContratoType = class(TXMLNode, IXMLContratoType)
  private
    FEntidad: IXMLEntidadTypeList;
    FDireccion: IXMLDireccionTypeList;
    FVehiculo: IXMLVehiculoTypeList;
  protected
    { IXMLContratoType }
    function Get_Concesionaria: Integer;
    function Get_Accion: IXMLAccionType;
    function Get_Codigo: string;
    function Get_Estado: Integer;
    function Get_Segundario: WideString;
    function Get_Entidad: IXMLEntidadTypeList;
    function Get_Direccion: IXMLDireccionTypeList;
    function Get_Vehiculo: IXMLVehiculoTypeList;
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_Codigo(Value: String);
    procedure Set_Estado(Value: Integer);
    procedure Set_Segundario(Value: WideString);
  public
    procedure AfterConstruction; override;
    destructor Destroy;override;
  end;

{ TXMLContratoTypeList }

  TXMLContratoTypeList = class(TXMLNodeCollection, IXMLContratoTypeList)
  protected
    { IXMLContratoTypeList }
    function Add: IXMLContratoType;
    function Insert(const Index: Integer): IXMLContratoType;
    function Get_Item(Index: Integer): IXMLContratoType;
  end;

{ TXMLAccionType }

  TXMLAccionType = class(TXMLNode, IXMLAccionType)
  protected
    { IXMLAccionType }
    function Get_FechaAccion: WideString;
    procedure Set_FechaAccion(Value: WideString);
  end;

{ TXMLEntidadType }

  TXMLEntidadType = class(TXMLNode, IXMLEntidadType)
  private
    FAsociada: IXMLAsociadaTypeList;
    FContacto: IXMLContactoTypeList;
    FDireccion: IXMLDireccionTypeList;
  protected
    { IXMLEntidadType }
    function Get_Accion: Integer;
    function Get_RUT: WideString;
    function Get_DV: WideString;
    function Get_Tipo: Integer;
    function Get_Nombre: WideString;
    function Get_Apellidop: WideString;
    function Get_Apellidom: WideString;
    function Get_Fantasia: WideString;
    function Get_Giro: WideString;
    function Get_Rol: Integer;
    function Get_Asociada: IXMLAsociadaTypeList;
    function Get_Contacto: IXMLContactoTypeList;
    function Get_Direccion: IXMLDireccionTypeList;
    procedure Set_Accion(Value: Integer);
    procedure Set_RUT(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Tipo(Value: Integer);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Apellidop(Value: WideString);
    procedure Set_Apellidom(Value: WideString);
    procedure Set_Fantasia(Value: WideString);
    procedure Set_Giro(Value: WideString);
    procedure Set_Rol(Value: Integer);
  public
    procedure AfterConstruction; override;
    destructor Destroy;override;
  end;

{ TXMLEntidadTypeList }

  TXMLEntidadTypeList = class(TXMLNodeCollection, IXMLEntidadTypeList)
  protected
    { IXMLEntidadTypeList }
    function Add: IXMLEntidadType;
    function Insert(const Index: Integer): IXMLEntidadType;
    function Get_Item(Index: Integer): IXMLEntidadType;
  end;

{ TXMLAsociadaType }

  TXMLAsociadaType = class(TXMLNode, IXMLAsociadaType)
  protected
    { IXMLAsociadaType }
    function Get_RUT: WideString;
    function Get_DV: WideString;
    function Get_Rol: Integer;
    procedure Set_RUT(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Rol(Value: Integer);
  end;

{ TXMLAsociadaTypeList }

  TXMLAsociadaTypeList = class(TXMLNodeCollection, IXMLAsociadaTypeList)
  protected
    { IXMLAsociadaTypeList }
    function Add: IXMLAsociadaType;
    function Insert(const Index: Integer): IXMLAsociadaType;
    function Get_Item(Index: Integer): IXMLAsociadaType;
  end;

{ TXMLContactoType }

  TXMLContactoType = class(TXMLNode, IXMLContactoType)
  protected
    { IXMLContactoType }
    function Get_Accion: Integer;
    function Get_Dato: WideString;
    function Get_Tipo: Integer;
    function Get_Observacion: WideString;
    procedure Set_Accion(Value: Integer);
    procedure Set_Dato(Value: WideString);
    procedure Set_Tipo(Value: Integer);
    procedure Set_Observacion(Value: WideString);
  end;

{ TXMLContactoTypeList }

  TXMLContactoTypeList = class(TXMLNodeCollection, IXMLContactoTypeList)
  protected
    { IXMLContactoTypeList }
    function Add: IXMLContactoType;
    function Insert(const Index: Integer): IXMLContactoType;
    function Get_Item(Index: Integer): IXMLContactoType;
  end;

{ TXMLDireccionType }

  TXMLDireccionType = class(TXMLNode, IXMLDireccionType)
  protected
    { IXMLDireccionType }
    function Get_Accion: Integer;
    function Get_Codigo: string;
    function Get_Tipo: Integer;
    function Get_Calle: WideString;
    function Get_Comuna: WideString;
    function Get_Altura: WideString;
    function Get_Depto: WideString;
    function Get_Zip: WideString;
    function Get_Comentario: WideString;
    procedure Set_Accion(Value: Integer);
    procedure Set_Codigo(Value: string);
    procedure Set_Tipo(Value: Integer);
    procedure Set_Calle(Value: WideString);
    procedure Set_Comuna(Value: WideString);
    procedure Set_Altura(Value: WideString);
    procedure Set_Depto(Value: WideString);
    procedure Set_Zip(Value: WideString);
    procedure Set_Comentario(Value: WideString);
  end;

{ TXMLDireccionTypeList }

  TXMLDireccionTypeList = class(TXMLNodeCollection, IXMLDireccionTypeList)
  protected
    { IXMLDireccionTypeList }
    function Add: IXMLDireccionType;
    function Insert(const Index: Integer): IXMLDireccionType;
    function Get_Item(Index: Integer): IXMLDireccionType;
  end;

{ TXMLVehiculoType }

  TXMLVehiculoType = class(TXMLNode, IXMLVehiculoType)
  private
    FTAG: IXMLTAGTypeList;
  protected
    { IXMLVehiculoType }
    function Get_Accion: IXMLAccionType;
    function Get_Patente: WideString;
    function Get_DV: WideString;
    function Get_Categoria: Integer;
    function Get_Catinturb: Integer;
    function Get_Marca: WideString;
    function Get_Modelo: WideString;
    function Get_Version: WideString;
    function Get_Color: WideString;
    function Get_Anio: Integer;
    function Get_TAG: IXMLTAGTypeList;
    procedure Set_Patente(Value: WideString);
    procedure Set_DV(Value: WideString);
    procedure Set_Categoria(Value: Integer);
    procedure Set_Catinturb(Value: Integer);
    procedure Set_Marca(Value: WideString);
    procedure Set_Modelo(Value: WideString);
    procedure Set_Version(Value: WideString);
    procedure Set_Color(Value: WideString);
    procedure Set_Anio(Value: Integer);
  public
    procedure AfterConstruction; override;
    destructor Destroy;override;
  end;

{ TXMLVehiculoTypeList }

  TXMLVehiculoTypeList = class(TXMLNodeCollection, IXMLVehiculoTypeList)
  protected
    { IXMLVehiculoTypeList }
    function Add: IXMLVehiculoType;
    function Insert(const Index: Integer): IXMLVehiculoType;
    function Get_Item(Index: Integer): IXMLVehiculoType;
  end;

{ TXMLTAGType }

  TXMLTAGType = class(TXMLNode, IXMLTAGType)
  protected
    { IXMLTAGType }
    function Get_Accion: Integer;
    function Get_Serie: String;
    function Get_Fabricante: Integer;
    function Get_Caturb: Integer;
    function Get_Catinturb: Integer;
    function Get_Context: Integer;
    function Get_Contract: cardinal;
    function Get_Estado: Integer;
    function Get_Instal: WideString;
    procedure Set_Accion(Value: Integer);
    procedure Set_Serie(Value: String);
    procedure Set_Fabricante(Value: Integer);
    procedure Set_Caturb(Value: Integer);
    procedure Set_Catinturb(Value: Integer);
    procedure Set_Context(Value: Integer);
    procedure Set_Contract(Value: cardinal);
    procedure Set_Estado(Value: Integer);
    procedure Set_Instal(Value: WideString);
  end;

{ TXMLTAGTypeList }

  TXMLTAGTypeList = class(TXMLNodeCollection, IXMLTAGTypeList)
  protected
    { IXMLTAGTypeList }
    function Add: IXMLTAGType;
    function Insert(const Index: Integer): IXMLTAGType;
    function Get_Item(Index: Integer): IXMLTAGType;
  end;

{ TXMLListaType }

  TXMLListaType = class(TXMLNode, IXMLListaType)
  private
    FTAGL: IXMLTAGLTypeList;
  protected
    { IXMLListaType }
    function Get_Concesionaria: Integer;
    function Get_Accion: Integer;
    function Get_Color: Integer;
    function Get_Fechalista: WideString;
    function Get_TAGL: IXMLTAGLTypeList;
    procedure Set_Concesionaria(Value: Integer);
    procedure Set_Accion(Value: Integer);
    procedure Set_Color(Value: Integer);
    procedure Set_Fechalista(Value: WideString);
  public
    procedure AfterConstruction; override;
    destructor Destroy;override;
  end;

{ TXMLListaTypeList }

  TXMLListaTypeList = class(TXMLNodeCollection, IXMLListaTypeList)
  protected
    { IXMLListaTypeList }
    function Add: IXMLListaType;
    function Insert(const Index: Integer): IXMLListaType;
    function Get_Item(Index: Integer): IXMLListaType;
  end;

{ TXMLTAGLType }

  TXMLTAGLType = class(TXMLNode, IXMLTAGLType)
  protected
    { IXMLTAGLType }
    function Get_Serie: String;
    function Get_Fab: Integer;
    procedure Set_Serie(Value: String);
    procedure Set_Fab(Value: Integer);
  end;

{ TXMLTAGLTypeList }

  TXMLTAGLTypeList = class(TXMLNodeCollection, IXMLTAGLTypeList)
  protected
    { IXMLTAGLTypeList }
    function Add: IXMLTAGLType;
    function Insert(const Index: Integer): IXMLTAGLType;
    function Get_Item(Index: Integer): IXMLTAGLType;
  end;

{ Global Functions }

function GetRNUT(Doc: IXMLDocument): IXMLRNUTType;
function LoadRNUT(const FileName: WideString): IXMLRNUTType;
function NewRNUT: IXMLRNUTType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetRNUT(Doc: IXMLDocument): IXMLRNUTType;
begin
  Result := Doc.GetDocBinding('RNUT', TXMLRNUTType, TargetNamespace) as IXMLRNUTType;
end;

function LoadRNUT(const FileName: WideString): IXMLRNUTType;
begin
    Result := LoadXMLDocument(FileName).GetDocBinding('RNUT', TXMLRNUTType, TargetNamespace) as IXMLRNUTType;
end;

function NewRNUT: IXMLRNUTType;
begin
  Result := NewXMLDocument.GetDocBinding('RNUT', TXMLRNUTType, TargetNamespace) as IXMLRNUTType;
end;

{ TXMLRNUTType }

procedure TXMLRNUTType.AfterConstruction;
begin
  RegisterChildNode('contrato', TXMLContratoType);
  RegisterChildNode('lista', TXMLListaType);
  FContrato := CreateCollection(TXMLContratoTypeList, IXMLContratoType, 'contrato') as IXMLContratoTypeList;
  FLista := CreateCollection(TXMLListaTypeList, IXMLListaType, 'lista') as IXMLListaTypeList;
  inherited;
end;

destructor TXMLRNUTType.Destroy;
begin
  FContrato.Clear;
  FLista.Clear;
  inherited;
end;

function TXMLRNUTType.Get_Contrato: IXMLContratoTypeList;
begin
  Result := FContrato;
end;

function TXMLRNUTType.Get_Lista: IXMLListaTypeList;
begin
  Result := FLista;
end;

{ TXMLContratoType }

procedure TXMLContratoType.AfterConstruction;
begin
  RegisterChildNode('accion', TXMLAccionType);
  RegisterChildNode('entidad', TXMLEntidadType);
  RegisterChildNode('direccion', TXMLDireccionType);
  RegisterChildNode('vehiculo', TXMLVehiculoType);
  FEntidad := CreateCollection(TXMLEntidadTypeList, IXMLEntidadType, 'entidad') as IXMLEntidadTypeList;
  FDireccion := CreateCollection(TXMLDireccionTypeList, IXMLDireccionType, 'direccion') as IXMLDireccionTypeList;
  FVehiculo := CreateCollection(TXMLVehiculoTypeList, IXMLVehiculoType, 'vehiculo') as IXMLVehiculoTypeList;
  inherited;
end;

function TXMLContratoType.Get_Concesionaria: Integer;
begin
  Result := iif(ChildNodes['concesionaria'].NodeValue = Null, 0, ChildNodes['concesionaria'].NodeValue);
end;

procedure TXMLContratoType.Set_Concesionaria(Value: Integer);
begin
  ChildNodes['concesionaria'].NodeValue := Value;
end;

function TXMLContratoType.Get_Accion: IXMLAccionType;
begin
  Result := ChildNodes['accion'] as IXMLAccionType;
end;

function TXMLContratoType.Get_Codigo: string;
begin
  Result := iif((ChildNodes['codigo'].NodeValue = Null), '', ChildNodes['codigo'].NodeValue);
end;

procedure TXMLContratoType.Set_Codigo(Value: String);
begin
  ChildNodes['codigo'].NodeValue := Value;
end;

function TXMLContratoType.Get_Estado: Integer;
begin
  Result := iif((ChildNodes['estado'].NodeValue = Null), 0, ChildNodes['estado'].NodeValue);
end;

procedure TXMLContratoType.Set_Estado(Value: Integer);
begin
  ChildNodes['estado'].NodeValue := Value;
end;

function TXMLContratoType.Get_Segundario: WideString;
begin
  Result := iif((ChildNodes['segundario'].Text = Null), '', ChildNodes['estado'].NodeValue);
end;

procedure TXMLContratoType.Set_Segundario(Value: WideString);
begin
  ChildNodes['segundario'].NodeValue := Value;
end;

function TXMLContratoType.Get_Entidad: IXMLEntidadTypeList;
begin
  Result := FEntidad;
end;

function TXMLContratoType.Get_Direccion: IXMLDireccionTypeList;
begin
  Result := FDireccion;
end;

function TXMLContratoType.Get_Vehiculo: IXMLVehiculoTypeList;
begin
  Result := FVehiculo;
end;

destructor TXMLContratoType.Destroy;
begin
    FEntidad.Clear;
    FDireccion.Clear;
    FVehiculo.Clear;
  inherited;
end;

{ TXMLContratoTypeList }

function TXMLContratoTypeList.Add: IXMLContratoType;
begin
  Result := AddItem(-1) as IXMLContratoType;
end;

function TXMLContratoTypeList.Insert(const Index: Integer): IXMLContratoType;
begin
  Result := AddItem(Index) as IXMLContratoType;
end;
function TXMLContratoTypeList.Get_Item(Index: Integer): IXMLContratoType;
begin
  Result := List[Index] as IXMLContratoType;
end;

{ TXMLAccionType }

function TXMLAccionType.Get_FechaAccion: WideString;
begin
    Result := iif((AttributeNodes['fechaAccion'].Text = Null), '', AttributeNodes['fechaAccion'].Text);
end;

procedure TXMLAccionType.Set_FechaAccion(Value: WideString);
begin
  SetAttribute('fechaAccion', Value);
end;

{ TXMLEntidadType }

procedure TXMLEntidadType.AfterConstruction;
begin
  RegisterChildNode('asociada', TXMLAsociadaType);
  RegisterChildNode('contacto', TXMLContactoType);
  RegisterChildNode('direccion', TXMLDireccionType);
  FAsociada := CreateCollection(TXMLAsociadaTypeList, IXMLAsociadaType, 'asociada') as IXMLAsociadaTypeList;
  FContacto := CreateCollection(TXMLContactoTypeList, IXMLContactoType, 'contacto') as IXMLContactoTypeList;
  FDireccion := CreateCollection(TXMLDireccionTypeList, IXMLDireccionType, 'direccion') as IXMLDireccionTypeList;
  inherited;
end;

function TXMLEntidadType.Get_Accion: Integer;
begin
  Result := iif((ChildNodes['accion'].NodeValue = Null), 0, ChildNodes['accion'].NodeValue);
end;

procedure TXMLEntidadType.Set_Accion(Value: Integer);
begin
  ChildNodes['accion'].NodeValue := Value;
end;

function TXMLEntidadType.Get_RUT: WideString;
begin
  Result := iif((ChildNodes['RUT'].Text = Null), '', ChildNodes['RUT'].Text);
end;

procedure TXMLEntidadType.Set_RUT(Value: WideString);
begin
  ChildNodes['RUT'].NodeValue := Value;
end;

function TXMLEntidadType.Get_DV: WideString;
begin
  Result := iif((ChildNodes['DV'].Text = Null),'', ChildNodes['DV'].Text);
end;

procedure TXMLEntidadType.Set_DV(Value: WideString);
begin
  ChildNodes['DV'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Tipo: Integer;
begin
  Result := iif((ChildNodes['tipo'].NodeValue = Null), 0, ChildNodes['tipo'].NodeValue);
end;

procedure TXMLEntidadType.Set_Tipo(Value: Integer);
begin
  ChildNodes['tipo'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Nombre: WideString;
begin
  Result := iif((ChildNodes['nombre'].Text = Null), '', ChildNodes['nombre'].Text);
end;

procedure TXMLEntidadType.Set_Nombre(Value: WideString);
begin
  ChildNodes['nombre'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Apellidop: WideString;
begin
  Result := iif((ChildNodes['apellidop'].Text = Null), '', ChildNodes['apellidop'].Text);
end;

procedure TXMLEntidadType.Set_Apellidop(Value: WideString);
begin
  ChildNodes['apellidop'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Apellidom: WideString;
begin
  Result := iif((ChildNodes['apellidom'].Text = Null), '', ChildNodes['apellidom'].Text);
end;

procedure TXMLEntidadType.Set_Apellidom(Value: WideString);
begin
  ChildNodes['apellidom'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Fantasia: WideString;
begin
  Result := iif((ChildNodes['fantasia'].Text = Null), '', ChildNodes['fantasia'].Text);
end;

procedure TXMLEntidadType.Set_Fantasia(Value: WideString);
begin
  ChildNodes['fantasia'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Giro: WideString;
begin
  Result := iif((ChildNodes['giro'].Text = Null), '', ChildNodes['giro'].Text);
end;

procedure TXMLEntidadType.Set_Giro(Value: WideString);
begin
  ChildNodes['giro'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Rol: Integer;
begin
  Result := iif((ChildNodes['rol'].NodeValue = Null),0, ChildNodes['rol'].NodeValue);
end;

procedure TXMLEntidadType.Set_Rol(Value: Integer);
begin
  ChildNodes['rol'].NodeValue := Value;
end;

function TXMLEntidadType.Get_Asociada: IXMLAsociadaTypeList;
begin
  Result := FAsociada;
end;

function TXMLEntidadType.Get_Contacto: IXMLContactoTypeList;
begin
  Result := FContacto;
end;

function TXMLEntidadType.Get_Direccion: IXMLDireccionTypeList;
begin
  Result := FDireccion;
end;

destructor TXMLEntidadType.Destroy;
begin
    FAsociada.Clear;
    FContacto.Clear;
    FDireccion.Clear;
  inherited;
end;

{ TXMLEntidadTypeList }

function TXMLEntidadTypeList.Add: IXMLEntidadType;
begin
  Result := AddItem(-1) as IXMLEntidadType;
end;

function TXMLEntidadTypeList.Insert(const Index: Integer): IXMLEntidadType;
begin
  Result := AddItem(Index) as IXMLEntidadType;
end;
function TXMLEntidadTypeList.Get_Item(Index: Integer): IXMLEntidadType;
begin
  Result := List[Index] as IXMLEntidadType;
end;

{ TXMLAsociadaType }

function TXMLAsociadaType.Get_RUT: WideString;
begin
  Result := iif((ChildNodes['RUT'].Text = Null), '', ChildNodes['RUT'].Text);
end;

procedure TXMLAsociadaType.Set_RUT(Value: WideString);
begin
  ChildNodes['RUT'].NodeValue := Value;
end;

function TXMLAsociadaType.Get_DV: WideString;
begin
  Result := iif((ChildNodes['DV'].Text = Null), '', ChildNodes['DV'].Text);
end;

procedure TXMLAsociadaType.Set_DV(Value: WideString);
begin
  ChildNodes['DV'].NodeValue := Value;
end;

function TXMLAsociadaType.Get_Rol: Integer;
begin
  Result := iif((ChildNodes['rol'].NodeValue = Null), 0, ChildNodes['rol'].NodeValue);
end;

procedure TXMLAsociadaType.Set_Rol(Value: Integer);
begin
  ChildNodes['rol'].NodeValue := Value;
end;

{ TXMLAsociadaTypeList }

function TXMLAsociadaTypeList.Add: IXMLAsociadaType;
begin
  Result := AddItem(-1) as IXMLAsociadaType;
end;

function TXMLAsociadaTypeList.Insert(const Index: Integer): IXMLAsociadaType;
begin
  Result := AddItem(Index) as IXMLAsociadaType;
end;
function TXMLAsociadaTypeList.Get_Item(Index: Integer): IXMLAsociadaType;
begin
  Result := List[Index] as IXMLAsociadaType;
end;

{ TXMLContactoType }

function TXMLContactoType.Get_Accion: Integer;
begin
  Result := iif((ChildNodes['accion'].NodeValue = Null), 0, ChildNodes['accion'].NodeValue);
end;

procedure TXMLContactoType.Set_Accion(Value: Integer);
begin
  ChildNodes['accion'].NodeValue := Value;
end;

function TXMLContactoType.Get_Dato: WideString;
begin
  Result := iif((ChildNodes['dato'].Text = Null), '', ChildNodes['dato'].Text);
end;

procedure TXMLContactoType.Set_Dato(Value: WideString);
begin
  ChildNodes['dato'].NodeValue := Value;
end;

function TXMLContactoType.Get_Tipo: Integer;
begin
  Result := iif((ChildNodes['tipo'].NodeValue = Null), 0, ChildNodes['tipo'].NodeValue);
end;

procedure TXMLContactoType.Set_Tipo(Value: Integer);
begin
  ChildNodes['tipo'].NodeValue := Value;
end;

function TXMLContactoType.Get_Observacion: WideString;
begin
  Result := iif((ChildNodes['observacion'].Text = Null), '', ChildNodes['observacion'].Text);
end;

procedure TXMLContactoType.Set_Observacion(Value: WideString);
begin
  ChildNodes['observacion'].NodeValue := Value;
end;

{ TXMLContactoTypeList }

function TXMLContactoTypeList.Add: IXMLContactoType;
begin
  Result := AddItem(-1) as IXMLContactoType;
end;

function TXMLContactoTypeList.Insert(const Index: Integer): IXMLContactoType;
begin
  Result := AddItem(Index) as IXMLContactoType;
end;
function TXMLContactoTypeList.Get_Item(Index: Integer): IXMLContactoType;
begin
  Result := List[Index] as IXMLContactoType;
end;

{ TXMLDireccionType }

function TXMLDireccionType.Get_Accion: Integer;
begin
  Result := iif((ChildNodes['accion'].NodeValue = Null), 0, ChildNodes['accion'].NodeValue);
end;

procedure TXMLDireccionType.Set_Accion(Value: Integer);
begin
  ChildNodes['accion'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Codigo: string;
begin
  Result := iif((ChildNodes['codigo'].NodeValue = Null), '', ChildNodes['codigo'].NodeValue);
end;

procedure TXMLDireccionType.Set_Codigo(Value: string);
begin
  ChildNodes['codigo'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Tipo: Integer;
begin
  Result := iif((ChildNodes['tipo'].NodeValue = Null), 0, ChildNodes['tipo'].NodeValue);
end;

procedure TXMLDireccionType.Set_Tipo(Value: Integer);
begin
  ChildNodes['tipo'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Calle: WideString;
begin
  Result := iif((ChildNodes['calle'].Text = Null), '', ChildNodes['calle'].Text);
end;

procedure TXMLDireccionType.Set_Calle(Value: WideString);
begin
  ChildNodes['calle'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Comuna: WideString;
begin
  Result := iif((ChildNodes['comuna'].Text = Null), '', ChildNodes['comuna'].Text);
end;

procedure TXMLDireccionType.Set_Comuna(Value: WideString);
begin
  ChildNodes['comuna'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Altura: WideString;
begin
  Result := iif((ChildNodes['altura'].Text = Null), '', ChildNodes['altura'].Text);
end;

procedure TXMLDireccionType.Set_Altura(Value: WideString);
begin
  ChildNodes['altura'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Depto: WideString;
begin
  Result := iif((ChildNodes['depto'].Text = Null), '', ChildNodes['depto'].Text);
end;

procedure TXMLDireccionType.Set_Depto(Value: WideString);
begin
  ChildNodes['depto'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Zip: WideString;
begin
  Result := iif((ChildNodes['zip'].Text = Null), '', ChildNodes['zip'].Text);
end;

procedure TXMLDireccionType.Set_Zip(Value: WideString);
begin
  ChildNodes['zip'].NodeValue := Value;
end;

function TXMLDireccionType.Get_Comentario: WideString;
begin
  Result := iif((ChildNodes['comentario'].Text = Null), '', ChildNodes['comentario'].Text);
end;

procedure TXMLDireccionType.Set_Comentario(Value: WideString);
begin
  ChildNodes['comentario'].NodeValue := Value;
end;

{ TXMLDireccionTypeList }

function TXMLDireccionTypeList.Add: IXMLDireccionType;
begin
  Result := AddItem(-1) as IXMLDireccionType;
end;

function TXMLDireccionTypeList.Insert(const Index: Integer): IXMLDireccionType;
begin
  Result := AddItem(Index) as IXMLDireccionType;
end;
function TXMLDireccionTypeList.Get_Item(Index: Integer): IXMLDireccionType;
begin
  Result := List[Index] as IXMLDireccionType;
end;

{ TXMLVehiculoType }

procedure TXMLVehiculoType.AfterConstruction;
begin
  RegisterChildNode('accion', TXMLAccionType);
  RegisterChildNode('TAG', TXMLTAGType);
  FTAG := CreateCollection(TXMLTAGTypeList, IXMLTAGType, 'TAG') as IXMLTAGTypeList;
  inherited;
end;

function TXMLVehiculoType.Get_Accion: IXMLAccionType;
begin
  Result := ChildNodes['accion'] as IXMLAccionType;
end;

function TXMLVehiculoType.Get_Patente: WideString;
begin
  Result := iif((ChildNodes['patente'].Text= Null), '', ChildNodes['patente'].Text);
end;

procedure TXMLVehiculoType.Set_Patente(Value: WideString);
begin
  ChildNodes['patente'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_DV: WideString;
begin
  Result := iif((ChildNodes['DV'].Text = Null), '', ChildNodes['DV'].Text);
end;

procedure TXMLVehiculoType.Set_DV(Value: WideString);
begin
  ChildNodes['DV'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Categoria: Integer;
begin
  Result := iif((ChildNodes['categoria'].NodeValue = Null), 0, ChildNodes['categoria'].NodeValue);
end;

procedure TXMLVehiculoType.Set_Categoria(Value: Integer);
begin
  ChildNodes['categoria'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Catinturb: Integer;
begin
  Result := iif((ChildNodes['catinturb'].NodeValue = Null), 0, ChildNodes['catinturb'].NodeValue);
end;

procedure TXMLVehiculoType.Set_Catinturb(Value: Integer);
begin
  ChildNodes['catinturb'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Marca: WideString;
begin
  Result := iif((ChildNodes['marca'].Text = Null), '', ChildNodes['marca'].Text);
end;

procedure TXMLVehiculoType.Set_Marca(Value: WideString);
begin
  ChildNodes['marca'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Modelo: WideString;
begin
  Result := iif((ChildNodes['modelo'].Text = Null), '', ChildNodes['modelo'].Text);
end;

procedure TXMLVehiculoType.Set_Modelo(Value: WideString);
begin
  ChildNodes['modelo'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Version: WideString;
begin
  Result := iif((ChildNodes['version'].Text = Null), '', ChildNodes['version'].Text);
end;

procedure TXMLVehiculoType.Set_Version(Value: WideString);
begin
  ChildNodes['version'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Color: WideString;
begin
  Result := iif((ChildNodes['color'].Text = Null), '', ChildNodes['color'].Text);
end;

procedure TXMLVehiculoType.Set_Color(Value: WideString);
begin
  ChildNodes['color'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_Anio: Integer;
begin
  Result := iif((ChildNodes['anio'].NodeValue = Null), 0, ChildNodes['anio'].NodeValue);
end;

procedure TXMLVehiculoType.Set_Anio(Value: Integer);
begin
  ChildNodes['anio'].NodeValue := Value;
end;

function TXMLVehiculoType.Get_TAG: IXMLTAGTypeList;
begin
  Result := FTAG;
end;

destructor TXMLVehiculoType.Destroy;
begin
   FTAG.Clear;
  inherited;
end;

{ TXMLVehiculoTypeList }

function TXMLVehiculoTypeList.Add: IXMLVehiculoType;
begin
  Result := AddItem(-1) as IXMLVehiculoType;
end;

function TXMLVehiculoTypeList.Insert(const Index: Integer): IXMLVehiculoType;
begin
  Result := AddItem(Index) as IXMLVehiculoType;
end;
function TXMLVehiculoTypeList.Get_Item(Index: Integer): IXMLVehiculoType;
begin
  Result := List[Index] as IXMLVehiculoType;
end;

{ TXMLTAGType }

function TXMLTAGType.Get_Accion: Integer;
begin
  Result := iif((ChildNodes['accion'].NodeValue = Null), 0, ChildNodes['accion'].NodeValue);
end;

procedure TXMLTAGType.Set_Accion(Value: Integer);
begin
  ChildNodes['accion'].NodeValue := Value;
end;

function TXMLTAGType.Get_Serie: String;
begin
  Result := iif((ChildNodes['serie'].NodeValue = Null), '', ChildNodes['serie'].NodeValue);
end;

procedure TXMLTAGType.Set_Serie(Value: String);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLTAGType.Get_Fabricante: Integer;
begin
  Result := iif((ChildNodes['fabricante'].NodeValue = Null), 0, ChildNodes['fabricante'].NodeValue);
end;

procedure TXMLTAGType.Set_Fabricante(Value: Integer);
begin
  ChildNodes['fabricante'].NodeValue := Value;
end;

function TXMLTAGType.Get_Caturb: Integer;
begin
  Result := iif((ChildNodes['caturb'].NodeValue = Null), 0, ChildNodes['caturb'].NodeValue);
end;

procedure TXMLTAGType.Set_Caturb(Value: Integer);
begin
  ChildNodes['caturb'].NodeValue := Value;
end;

function TXMLTAGType.Get_Catinturb: Integer;
begin
  Result := iif((ChildNodes['catinturb'].NodeValue = Null), 0, ChildNodes['catinturb'].NodeValue);
end;

procedure TXMLTAGType.Set_Catinturb(Value: Integer);
begin
  ChildNodes['catinturb'].NodeValue := Value;
end;

function TXMLTAGType.Get_Context: Integer;
begin
  Result := iif((ChildNodes['context'].NodeValue = Null), 0, ChildNodes['context'].NodeValue);
end;

procedure TXMLTAGType.Set_Context(Value: Integer);
begin
  ChildNodes['context'].NodeValue := Value;
end;

function TXMLTAGType.Get_Contract: cardinal;
begin
  Result := iif((ChildNodes['contract'].NodeValue = Null), 0, ChildNodes['contract'].NodeValue);
end;

procedure TXMLTAGType.Set_Contract(Value: cardinal);
begin
  ChildNodes['contract'].NodeValue := Value;
end;

function TXMLTAGType.Get_Estado: Integer;
begin
  Result := iif((ChildNodes['estado'].NodeValue = Null), 0, ChildNodes['estado'].NodeValue);
end;

procedure TXMLTAGType.Set_Estado(Value: Integer);
begin
  ChildNodes['estado'].NodeValue := Value;
end;

function TXMLTAGType.Get_Instal: WideString;
begin
  Result := iif((ChildNodes['instal'].Text = Null), '', ChildNodes['instal'].Text);
end;

procedure TXMLTAGType.Set_Instal(Value: WideString);
begin
  ChildNodes['instal'].NodeValue := Value;
end;

{ TXMLTAGTypeList }

function TXMLTAGTypeList.Add: IXMLTAGType;
begin
  Result := AddItem(-1) as IXMLTAGType;
end;

function TXMLTAGTypeList.Insert(const Index: Integer): IXMLTAGType;
begin
  Result := AddItem(Index) as IXMLTAGType;
end;
function TXMLTAGTypeList.Get_Item(Index: Integer): IXMLTAGType;
begin
  Result := List[Index] as IXMLTAGType;
end;

{ TXMLListaType }

procedure TXMLListaType.AfterConstruction;
begin
  RegisterChildNode('TAGL', TXMLTAGLType);
  FTAGL := CreateCollection(TXMLTAGLTypeList, IXMLTAGLType, 'TAGL') as IXMLTAGLTypeList;
  inherited;
end;

function TXMLListaType.Get_Concesionaria: Integer;
begin
  Result := iif((ChildNodes['concesionaria'].NodeValue = Null), 0, ChildNodes['concesionaria'].NodeValue);
end;

procedure TXMLListaType.Set_Concesionaria(Value: Integer);
begin
  ChildNodes['concesionaria'].NodeValue := Value;
end;

function TXMLListaType.Get_Accion: Integer;
begin
  Result := iif((ChildNodes['accion'].NodeValue = Null), 0,ChildNodes['accion'].NodeValue);
end;

procedure TXMLListaType.Set_Accion(Value: Integer);
begin
  ChildNodes['accion'].NodeValue := Value;
end;

function TXMLListaType.Get_Color: Integer;
begin
  Result := iif((ChildNodes['color'].NodeValue = Null), 0, ChildNodes['color'].NodeValue);
end;

procedure TXMLListaType.Set_Color(Value: Integer);
begin
  ChildNodes['color'].NodeValue := Value;
end;

function TXMLListaType.Get_Fechalista: WideString;
begin
  Result := iif((ChildNodes['fechalista'].Text = Null), '', ChildNodes['fechalista'].Text);
end;

procedure TXMLListaType.Set_Fechalista(Value: WideString);
begin
  ChildNodes['fechalista'].NodeValue := Value;
end;

function TXMLListaType.Get_TAGL: IXMLTAGLTypeList;
begin
  Result := FTAGL;
end;

destructor TXMLListaType.Destroy;
begin
    FTAGL.Clear;
  inherited;
end;

{ TXMLListaTypeList }

function TXMLListaTypeList.Add: IXMLListaType;
begin
  Result := AddItem(-1) as IXMLListaType;
end;

function TXMLListaTypeList.Insert(const Index: Integer): IXMLListaType;
begin
  Result := AddItem(Index) as IXMLListaType;
end;
function TXMLListaTypeList.Get_Item(Index: Integer): IXMLListaType;
begin
  Result := List[Index] as IXMLListaType;
end;

{ TXMLTAGLType }

function TXMLTAGLType.Get_Serie: string;
begin
  Result := iif((ChildNodes['serie'].NodeValue = Null), '', ChildNodes['serie'].NodeValue);
end;

procedure TXMLTAGLType.Set_Serie(Value: string);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLTAGLType.Get_Fab: Integer;
begin
  Result := iif((ChildNodes['fab'].NodeValue = Null), 0, ChildNodes['fab'].NodeValue);
end;

procedure TXMLTAGLType.Set_Fab(Value: Integer);
begin
  ChildNodes['fab'].NodeValue := Value;
end;

{ TXMLTAGLTypeList }

function TXMLTAGLTypeList.Add: IXMLTAGLType;
begin
  Result := AddItem(-1) as IXMLTAGLType;
end;

function TXMLTAGLTypeList.Insert(const Index: Integer): IXMLTAGLType;
begin
  Result := AddItem(Index) as IXMLTAGLType;
end;
function TXMLTAGLTypeList.Get_Item(Index: Integer): IXMLTAGLType;
begin
  Result := List[Index] as IXMLTAGLType;
end;
end.
