{-----------------------------------------------------------------------------
 File Name: RNVMConfig.pas
 Author:    gcasais
 Date Created: 11/04/2005
 Language: ES-AR
 Description: Configuración del servicio RNVM

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
                                     
Etiqueta    : 20160315 MGO
Descripción : Se utiliza InstallIni en vez de ApplicationIni

-----------------------------------------------------------------------------}

unit RNVMConfig;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DmiCtrls, Util, UtilProc;

type
  TfrmConfig = class(TForm)
    Bevel1: TBevel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    chkProxy: TCheckBox;
    txtServer: TEdit;
    lbl_ServerProxy: TLabel;
    lbl_ServerPort: TLabel;
    nePort: TNumericEdit;
    procedure btnAceptarClick(Sender: TObject);
    procedure chkProxyClick(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    function Inicializar:Boolean;
  end;

var
  frmConfig: TfrmConfig;

implementation

{$R *.dfm}

{ TfrmConfig }
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 11/04/2005
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmConfig.Inicializar: Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    { INICIO : 20160315 MGO
    chkProxy.Checked := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
    chkProxy.OnClick(chkProxy);
    txtServer.Text   := ApplicationIni.ReadString('RNVM', 'PROXYSERVER', '');
    nePort.ValueInt  := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
    }
    chkProxy.Checked := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
    chkProxy.OnClick(chkProxy);
    txtServer.Text   := InstallIni.ReadString('RNVM', 'PROXYSERVER', '');
    nePort.ValueInt  := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
    // FIN : 20160315 MGO
    Result := True;
end;
{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    gcasais
  Date Created: 11/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmConfig.btnAceptarClick(Sender: TObject);
begin
    { INICIO : 20160315 MGO
    ApplicationIni.WriteBool('RNVM', 'USEPROXY', chkProxy.Checked);
    ApplicationIni.WriteString('RNVM', 'PROXYSERVER', Trim(txtServer.Text));
    ApplicationIni.WriteInteger('RNVM', 'SERVERPORT', nePort.ValueInt);
    }
    InstallIni.WriteBool('RNVM', 'USEPROXY', chkProxy.Checked);
    InstallIni.WriteString('RNVM', 'PROXYSERVER', Trim(txtServer.Text));
    InstallIni.WriteInteger('RNVM', 'SERVERPORT', nePort.ValueInt);
    // FIN : 20160315 MGO
end;
{-----------------------------------------------------------------------------
  Function Name: chkproxyClick
  Author:    gcasais
  Date Created: 11/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmConfig.chkProxyClick(Sender: TObject);
begin
    if not chkproxy.Checked then begin
        txtServer.Text := '';
        nePort.ValueInt := 0;
    end;

    lbl_ServerProxy.Enabled := chkProxy.Checked;
    txtServer.Enabled := chkProxy.Checked;
    lbl_ServerPort.Enabled := chkProxy.Checked;
    nePort.Enabled := chkProxy.Checked;;
end;

end.
