unit FrmSeleccionarCalleRelacionada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, BuscaTab, DB, ADODB, StdCtrls, DPSControls, Buttons,
  DmiCtrls, DMConnection, Util, UtilProc, utildb, RStrings,
  Peaprocs, Peatypes, FrmBuscaCalle;

type
  TFormSeleccionarCalleRelacionada = class(TForm)
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
    Label2: TLabel;
    Label14: TLabel;
    cb_Regiones: TComboBox;
    Label15: TLabel;
    cb_Comunas: TComboBox;
    Label6: TLabel;
    cb_Calle: TComboBox;
    btn_buscaCalle: TSpeedButton;
    txt_numeroCalle: TEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure cb_RegionesChange(Sender: TObject);
    procedure cb_ComunasChange(Sender: TObject);
    procedure btn_buscaCalleClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCodigoCalle:   integer;
    FCodigoPais:    AnsiString;
    FCodigoRegion:  AnsiString;
    FCodigoComuna:  AnsiString;
    function GetDescripcionCalle: AnsiString;
    Function GetNumeroCalle: AnsiString;
    function GetRegion: AnsiString;
    function GetComuna: AnsiString;
    function GetCodigoCalle: integer;

  public
    { Public declarations }
    property CodigoCalle: integer read GetCodigoCalle;
    property NumeroCalle: AnsiString read getNumeroCalle;
    Property Pais:   AnsiString Read FCodigoPais;
    Property Region: AnsiString Read GetRegion;
    Property Comuna: AnsiString Read GetComuna;
    Property DescripcionCalle: AnsiString read GetDescripcionCalle;
    Function Inicializar(CodigoCalle: integer;
                        Numero, CodigoPais, CodigoRegion, CodigoComuna: AnsiString): Boolean; overload; //Utilizada para la edici�n.
    Function Inicializar(CodigoPais: AnsiString = PAIS_CHILE; CodigoRegion: AnsiString = REGION_SANTIAGO; CodigoComuna: AnsiString = ''): Boolean; overload; //Utilizada para el Alta
    Function ValidarSeleccionCalleRelacionada: boolean;
  end;

var
  FormSeleccionarCalleRelacionada: TFormSeleccionarCalleRelacionada;

implementation

{$R *.dfm}
Function TFormSeleccionarCalleRelacionada.Inicializar(CodigoCalle: integer;
                        Numero, CodigoPais, CodigoRegion, CodigoComuna: AnsiString): Boolean;
begin
    FCodigoPais             := CodigoPais;
    FCodigoRegion           := CodigoRegion;
    FCodigoComuna           := CodigoComuna;
    FCodigoCalle            := CodigoCalle;
    txt_NumeroCalle.Text    := Numero;

    Result := True;
end;
Function TFormSeleccionarCalleRelacionada.Inicializar(CodigoPais: AnsiString = PAIS_CHILE;
            CodigoRegion: AnsiString = REGION_SANTIAGO; CodigoComuna: AnsiString = ''): Boolean;
begin
    FCodigoPais             := CodigoPais;
    FCodigoRegion           := CodigoRegion;
    FCodigoComuna           := CodigoComuna;
    FCodigoCalle            := -1;
    txt_NumeroCalle.clear;
    Result                  := True;
end;

function TFormSeleccionarCalleRelacionada.GetDescripcionCalle: AnsiString;
begin
    Result := Trim(cb_Calle.Text);
end;

function TFormSeleccionarCalleRelacionada.GetNUmeroCalle: AnsiString;
begin
    Result := trim(txt_NumeroCalle.Text);
end;

procedure TFormSeleccionarCalleRelacionada.btnCancelarClick(
  Sender: TObject);
begin
    Close;
end;

procedure TFormSeleccionarCalleRelacionada.btnAceptarClick(
  Sender: TObject);
begin
    if not ValidarSeleccionCalleRelacionada then exit;
    ModalResult := mrOK;
end;

procedure TFormSeleccionarCalleRelacionada.cb_RegionesChange(
  Sender: TObject);
begin
    CargarComunas(DMConnections.BaseCAC,cb_Comunas,Self.Pais,Self.Region,'',true);
    FCodigoRegion:=self.Region;
    cb_Comunas.SetFocus;
end;

procedure TFormSeleccionarCalleRelacionada.cb_ComunasChange(
  Sender: TObject);
begin
    if (cb_Regiones.ItemIndex>0) and (cb_Comunas.ItemIndex>0) then
        CargarCallesComuna(DMConnections.BaseCAC,cb_calle,Self.Pais,self.Region,self.Comuna)
    else cb_Calle.Clear;
end;

function TFormSeleccionarCalleRelacionada.ValidarSeleccionCalleRelacionada: boolean;
begin
    result:=False;
    if ValidateControls([cb_Regiones,cb_Comunas,cb_Calle,txt_numeroCalle],
                            [cb_Regiones.ItemIndex>=0,cb_Comunas.ItemIndex>=0,trim(cb_Calle.Text)<>'',trim(txt_numeroCalle.Text)<>''],
                            self.Caption,
                            [format(MSG_VALIDAR_DEBE_LA,[FLD_REGION]),format(MSG_VALIDAR_DEBE_LA,[FLD_COMUNA]),format(MSG_VALIDAR_DEBE_LA,[FLD_CALLE]),format(MSG_VALIDAR_DEBE_EL,[FLD_NUMERO])]) then begin
            if Self.CodigoCalle>-1 then begin
                if QueryGetValueInt(DMConnections.BaseCAC,format('SELECT DBO.NumeroCalleValido(''%s'',''%s'',''%s'',%d,%d)',
                        [self.Pais, self.Region, self.comuna, self.codigoCalle,FormatearNumeroCalle(self.NumeroCalle)]))=1 then result:=True
                else begin
                    result:=False;
                    MsgBoxBalloon(MSG_ERROR_NUMERO_CALLE,MSG_CAPTION_DOMICILIO,MB_ICONSTOP,txt_numeroCalle);
                    txt_numeroCalle.SetFocus;
                end;
            end else begin
                MsgBoxBalloon(format(MSG_VALIDAR_DATO,[FLD_CALLE]),MSG_CAPTION_DOMICILIO,MB_ICONSTOP,cb_Calle);
                cb_Calle.SetFocus;
            end;
    end;
end;

procedure TFormSeleccionarCalleRelacionada.btn_buscaCalleClick(
  Sender: TObject);
var
    f:TFormBuscaCalle;
begin
    Application.CreateForm(TFormBuscaCalle,f);
    if (f.inicializar(self.Pais,self.Region,self.Comuna,self.Region)) and (f.ShowModal=mrOK) then begin
        CargarRegiones(DMConnections.BaseCAC,cb_Regiones,f.CodigoPais,f.CodigoRegion,true);
        CargarComunas(DMConnections.BaseCAC,cb_Comunas,f.CodigoPais,f.CodigoRegion,f.CodigoComuna,true);
        CargarCallesComuna(DMConnections.BaseCAC,cb_Calle,f.CodigoPais,f.CodigoRegion,f.CodigoComuna,f.CodigoCalle);
        txt_numeroCalle.SetFocus;
    end else cb_Calle.SetFocus;
    f.Release;
end;

function TFormSeleccionarCalleRelacionada.GetCodigoCalle: integer;
begin
    if (cb_calle.ItemIndex < 0) then result := -1
    else result:=(Integer(cb_calle.Items.Objects[cb_Calle.ItemIndex]) div 1000);

end;

function TFormSeleccionarCalleRelacionada.GetComuna: AnsiString;
begin
    Result := Trim(StrRight(cb_Comunas.Text, 10));
end;

function TFormSeleccionarCalleRelacionada.GetRegion: AnsiString;
begin
    Result := Trim(StrRight(cb_Regiones.Text, 10));
end;

procedure TFormSeleccionarCalleRelacionada.FormShow(Sender: TObject);
begin
    //CARGAR
    CargarRegiones(DMConnections.BaseCAC,cb_Regiones,FCodigoPais,FCodigoRegion,true);
    CargarComunas(DMConnections.BaseCAC,cb_Comunas,FCodigoPais,FCodigoRegion,FCodigoComuna,true);
    CargarCallesComuna(DMConnections.BaseCAC,cb_Calle,FCodigoPais,FCodigoRegion,FCodigoComuna,FCodigoCalle);
end;

end.
