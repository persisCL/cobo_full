{-------------------------------------------------------------------------------
 File Name: FreCompromisoOrdenServicio.pas
 Author: DdMarco
 Date Created:
 Language: ES-AR
 Description: Seccion con la información del compromiso asumido con el cliente
-------------------------------------------------------------------------------}
unit FreCompromisoOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Util, VariantComboBox, Validate, DateEdit, UtilProc, DB;

type
  TFrameCompromisoOrdenServicio = class(TFrame)
    GBCompromisoCliente: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    cbPrioridad: TVariantComboBox;
    txtFechaCompromiso: TDateEdit;
  private
    function GetPrioridad: Integer;
    procedure SetPrioridad(const Value: Integer);
    function GetFechaCompromiso: TDateTime;
    procedure SetFechaCompromiso(const Value: TDateTime);
    { Private declarations }
  public
    { Public declarations }
    function Inicializar: Boolean; overload;
    function Inicializar(Prioridad: Integer): Boolean; overload;
    function Inicializar(Prioridad: Integer; FechaCompromiso: TDateTime): Boolean; overload;
    function Validar: Boolean;
    procedure LoadFromDataset(DS: TDataset);
    //
    property Prioridad: Integer read GetPrioridad write SetPrioridad;
    property FechaCompromiso: TDateTime read GetFechaCompromiso write SetFechaCompromiso;
  end;

implementation

uses RStrings;

{$R *.dfm}

{ TFrameCompromisoOrdenServicio }

function TFrameCompromisoOrdenServicio.Inicializar: Boolean;
begin
{    cbprioridad.Value:=4;  //prioridad Baja}               // _PAA_
    Result := True;
end;

function TFrameCompromisoOrdenServicio.Inicializar(Prioridad: Integer): Boolean;
begin
    cbPrioridad.Value := Prioridad;
    Result := Inicializar;
end;

function TFrameCompromisoOrdenServicio.GetPrioridad: Integer;
begin
    Result := cbPrioridad.Value;
end;

function TFrameCompromisoOrdenServicio.Inicializar(Prioridad: Integer; FechaCompromiso: TDateTime): Boolean;
begin
    Self.Prioridad := Prioridad;
    Self.FechaCompromiso := FechaCompromiso;
    Result := Inicializar;
end;

procedure TFrameCompromisoOrdenServicio.SetPrioridad(const Value: Integer);
begin
    cbPrioridad.Value := Value;
end;

function TFrameCompromisoOrdenServicio.GetFechaCompromiso: TDateTime;
begin
    Result := txtFechaCompromiso.Date;
end;

procedure TFrameCompromisoOrdenServicio.SetFechaCompromiso(const Value: TDateTime);
begin
    txtFechaCompromiso.Date := Value;
end;

function TFrameCompromisoOrdenServicio.Validar: Boolean;
resourcestring
	MSG_ERROR_FECHA_COMPROMETIDA_INCORRECTA = 'La fecha comprometida no puede ser anterior a la actual';
begin
    Result := False;
    if (txtFechaCompromiso.Date <> NULLDATE) and (txtFechaCompromiso.Date < Date) then begin
		MsgBoxBalloon(MSG_ERROR_FECHA_COMPROMETIDA_INCORRECTA, STR_ERROR, MB_ICONSTOP, txtFechaCompromiso);
        txtFechaCompromiso.SetFocus;
        Exit;
    end;
    Result := True;
end;

procedure TFrameCompromisoOrdenServicio.LoadFromDataset(DS: TDataset);
begin
    if not DS.FieldByName('FechaCompromiso').IsNull then
      txtFechaCompromiso.Date := DS.FieldByName('FechaCompromiso').AsDateTime;
    cbPrioridad.Value := DS['Prioridad'];
end;

end.
