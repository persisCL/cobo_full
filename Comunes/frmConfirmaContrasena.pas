unit frmConfirmaContrasena;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, DB, ADODB, DmiCtrls, ExtCtrls, DmConnection, UtilProc, RStrings;

type
  TFormConfirmaContrasena = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    Image1: TImage;
    txt_password: TEdit;
    ValidarLogin: TADOStoredProc;
    lblusuario: TLabel;
    txtusuario: TLabel;
    btn_ok: TButton;
    btn_cancel: TButton;
    procedure btn_okClick(Sender: TObject);
    procedure txt_passwordChange(Sender: TObject);
  private
    FUsuarioSistema: Ansistring;
  public
    property Usuario: AnsiString read FUsuarioSistema;
    function Inicializar(CodigoUsuario: AnsiString): boolean;
  end;

var
  FormConfirmaContrasena: TFormConfirmaContrasena;

implementation

{$R *.dfm}

{ TFormConfirmaContrasena }

function TFormConfirmaContrasena.Inicializar(CodigoUsuario: AnsiString): Boolean;
begin
	FUsuarioSistema 		:= Trim(CodigoUsuario);
	txtusuario.Caption      := Trim(CodigoUsuario);
	Result 					:= True;
end;

procedure TFormConfirmaContrasena.btn_okClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	try
   		ValidarLogin.Parameters.Refresh;
		ValidarLogin.Parameters.ParamByName('@CodigoUsuario').Value  := FUsuarioSistema;
		ValidarLogin.Parameters.ParamByName('@Password').Value       := txt_password.text;
		ValidarLogin.Parameters.ParamByName('@NombreCompleto').Value := NULL;
		ValidarLogin.Parameters.ParamByName('@DescriError').Value 	 := NULL;
        ValidarLogin.Parameters.ParamByName('@PedirCambiarPassword').Value:= NULL;
		ValidarLogin.ExecProc;
		Screen.Cursor := crDefault;
	except
		on e: Exception do begin
			Screen.Cursor := crDefault;
			MsgBox(MSG_ERROR_ACCESO + #13#10 +
                    E.Message, MSG_CAPTION_INGRESE_CONTRASENA, MB_ICONSTOP);
			Exit;
		end;
	end;

    if ValidarLogin.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
  		MsgBox(MSG_ERROR_CONTRASENA, MSG_CAPTION_INGRESE_CONTRASENA, MB_ICONSTOP);
        txt_Password.Clear;
		Exit;
	end;

    ModalResult := mrOk;
end;

procedure TFormConfirmaContrasena.txt_passwordChange(Sender: TObject);
begin
	btn_ok.enabled := txt_password.text <> '';
end;

end.
