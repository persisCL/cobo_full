object FormNormalizarDomicilio: TFormNormalizarDomicilio
  Left = 259
  Top = 165
  Width = 696
  Height = 480
  Caption = 'Normalizador de Domicilios'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl_botones: TPanel
    Left = 0
    Top = 412
    Width = 688
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 688
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        688
        41)
      object btn_Normalizar: TDPSButton
        Left = 512
        Top = 8
        Anchors = [akRight, akBottom]
        Caption = '&Normalizar'
        Default = True
        TabOrder = 0
        OnClick = btn_NormalizarClick
      end
      object BtnSalir: TDPSButton
        Left = 596
        Top = 6
        Width = 79
        Height = 26
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 1
        OnClick = BtnSalirClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 89
    Width = 688
    Height = 323
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object pnl_grilla: TGroupBox
      Left = 0
      Top = 0
      Width = 688
      Height = 323
      Align = alClient
      Caption = 'Domicilios Desnormalizados'
      TabOrder = 0
      DesignSize = (
        688
        323)
      object dbl_Domicilios: TDBListEx
        Left = 8
        Top = 47
        Width = 673
        Height = 266
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 400
            Header.Caption = 'Domicilio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescripcionDomicilioCompleto'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 220
            Header.Caption = 'Esquina 1'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Esquina1'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 220
            Header.Caption = 'Esquina 2'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Esquina2'
          end>
        DataSource = DSObtenerDomiciliosDesnormalizado
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDblClick = dbl_DomiciliosDblClick
      end
      object chk_VerNoSePuedeNorm: TCheckBox
        Left = 16
        Top = 24
        Width = 265
        Height = 17
        Caption = '&Mostrar domicilios que no se pueden normalizar.'
        TabOrder = 1
        OnClick = chk_VerNoSePuedeNormClick
      end
      object btn_NoSePuede: TDPSButton
        Left = 547
        Top = 15
        Width = 123
        Anchors = [akTop, akRight]
        Caption = 'No se &puede Normalizar'
        TabOrder = 2
        OnClick = btn_NoSePuedeClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 688
      Height = 89
      Align = alClient
      Caption = 'Datos de ubicaci'#243'n pol'#237'tica'
      TabOrder = 0
      DesignSize = (
        688
        89)
      object Label1: TLabel
        Left = 14
        Top = 29
        Width = 37
        Height = 13
        Caption = 'Region:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object lbl_Comuna: TLabel
        Left = 350
        Top = 29
        Width = 42
        Height = 13
        Caption = 'Comuna:'
      end
      object Label3: TLabel
        Left = 15
        Top = 53
        Width = 36
        Height = 13
        Caption = 'Ciudad:'
      end
      object cbRegiones: TComboBox
        Left = 60
        Top = 22
        Width = 282
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbRegionesChange
        OnEnter = cbRegionesEnter
      end
      object cbCiudades: TComboBox
        Left = 60
        Top = 48
        Width = 282
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnChange = cbCiudadesChange
        OnEnter = cbCiudadesEnter
      end
      object cbComunas: TComboBox
        Left = 400
        Top = 22
        Width = 282
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbComunasChange
        OnEnter = cbComunasEnter
      end
    end
  end
  object ObtenerDomiciliosDesnormalizado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    AfterOpen = ObtenerDomiciliosDesnormalizadoAfterOpen
    ProcedureName = 'ObtenerDomiciliosDesnormalizado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@NoSePuedeNormalizar'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 312
    Top = 168
  end
  object DSObtenerDomiciliosDesnormalizado: TDataSource
    DataSet = ObtenerDomiciliosDesnormalizado
    Left = 344
    Top = 168
  end
  object ObtenerCallesYTramos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCallesYTramos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 312
    Top = 208
  end
  object NormalizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'NormalizarDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 424
  end
end
