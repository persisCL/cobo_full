{-----------------------------------------------------------------------------
 Unit Name: FrmImprimir
 Author:    ggomez
 Purpose: Form para mostrar un mensaje previo a realizar una impresi�n.
    Contiene dos botones uno Aceptar y otro Cancelar.
    El form retorna el resultado de la elecci�n del operador.
    Contine un CheckBox (Configurar Impresora). El valor de este CheckBox es
    utilizado para determinar el valor a retornar cuando se elige Aceptar.
    Si se elige Cancelar, retorna riCancelar.
    Si se elige Aceptar y est� chequeado el CheckBox, retorna
    riAceptarConfigurarImpresora.
    Si se elige Aceptar y NO est� chequeado el CheckBox, retorna riAceptar.

    Este form es �til cuando se quiere realizar una impresi�n directamente a la
    impresora o cuando se quiere mostrar la ventana de Configurar Impresora.

 History:
-----------------------------------------------------------------------------}

unit FrmImprimir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DPSControls;

type
  (* Tipo para las respuestas que puede retornar el form, seg�n el bot�n
  presionado.
  riAceptar: Aceptar y CheckBox NO chequeado.
  riAceptarConfigurarImpresora: Aceptar y CheckBox SI chequeado.
  riCancelar: Cancelar. *)
  TRespuestaImprimir = (riAceptar, riAceptarConfigurarImpresora, riCancelar);

  TFormImprimir = class(TForm)
    chb_ConfigurarImpresora: TCheckBox;
    Bevel1: TBevel;
    lbl_Mensaje: TLabel;
    img: TImage;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FValorRetorno: TRespuestaImprimir;
  public
    { Public declarations }
  end;

var
  FormImprimir: TFormImprimir;
  function Ejecutar(txtCaption, txtMensaje: AnsiString): TRespuestaImprimir;

implementation

{$R *.dfm}

{ TFormImprimir }

procedure TFormImprimir.btn_AceptarClick(Sender: TObject);
begin
    if chb_ConfigurarImpresora.Checked then
        FValorRetorno := riAceptarConfigurarImpresora
    else
        FValorRetorno := riAceptar;
    Close;
end;

function Ejecutar(txtCaption,
  txtMensaje: AnsiString): TRespuestaImprimir;
begin
    with TFormImprimir.Create(Application) do begin
        try
            Caption := txtCaption;
            lbl_Mensaje.Caption := txtMensaje;
            FValorRetorno := riCancelar;
            ShowModal;
            Result := FValorRetorno;
        finally
            Release;
        end;
    end;
end;


procedure TFormImprimir.btn_CancelarClick(Sender: TObject);
begin
    Close;
end;
end.
