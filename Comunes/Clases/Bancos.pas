//TASK_125_JMA_20170404
unit Bancos;

interface
uses ClaseBase, DB, ADODB, DBClient, SysUtils, DMConnection, Variants, Util;

type TBanco= class(TClaseBase)

    private
        function GetCodigoBanco(): Integer;
        procedure SetCodigoBanco(Valor:Integer);

        function GetDescripcion(): string;
        procedure SetDescripcion(Valor:string);

        function GetActivo(): Boolean;
        procedure SetActivo(Valor:Boolean);

        function GetCodigoBancoSBEI(): SmallInt;
        procedure SetCodigoBancoSBEI(Valor:SmallInt);

        function GetHabilitadoPagoVentanilla(): Boolean;
        procedure SetHabilitadoPagoVentanilla(Valor:Boolean);

        function GetHabilitadoEmisorTC(): Boolean;
        procedure SetHabilitadoEmisorTC(Valor:Boolean);

        function GetHabilitadoEmisorPAC(): Boolean;
        procedure SetHabilitadoEmisorPAC(Valor:Boolean);

        function GetPrefijoIDTRX(): string;
        procedure SetPrefijoIDTRX(Valor:string);

        function GetCodigoConciliacionBancaria(): string;
        procedure SetCodigoConciliacionBancaria(Valor:string);

        function GetEsMaestro(): Boolean;
        procedure SetEsMaestro(Valor:Boolean);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property CodigoBanco: Integer read GetCodigoBanco write SetCodigoBanco;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property Activo: Boolean read GetActivo write SetActivo;
        property CodigoBancoSBEI: SmallInt read GetCodigoBancoSBEI write SetCodigoBancoSBEI;
        property HabilitadoPagoVentanilla: Boolean read GetHabilitadoPagoVentanilla write SetHabilitadoPagoVentanilla;
        property HabilitadoEmisorTC: Boolean read GetHabilitadoEmisorTC write SetHabilitadoEmisorTC;
        property HabilitadoEmisorPAC: Boolean read GetHabilitadoEmisorPAC write SetHabilitadoEmisorPAC;
        property PrefijoIDTRX: string read GetPrefijoIDTRX write SetPrefijoIDTRX;
        property CodigoConciliacionBancaria: string read GetCodigoConciliacionBancaria write SetCodigoConciliacionBancaria;
        property EsMaestro: Boolean read GetEsMaestro write SetEsMaestro;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(Activo: Variant; CodigoBanco: Integer = 0; Descripcion: string = ''): TClientDataSet;
        //function Agregar(Usuario: string): Integer;
        //function Modificar(Usuario: string; ID: Integer): Boolean;
        //function Eliminar(ID: Integer; Usuario: string): Boolean;

end;
 
implementation
{$REGION 'GETTERS y SETTERS}
function TBanco.GetCodigoBanco(): Integer;
begin
    Result := GetField('CodigoBanco');
end;

procedure TBanco.SetCodigoBanco(Valor:Integer);
begin
    SetField('CodigoBanco', Valor);
end;

function TBanco.GetDescripcion(): string;
begin
    Result := GetField('Descripcion');
end;

procedure TBanco.SetDescripcion(Valor:string);
begin
    SetField('Descripcion', Valor);
end;

function TBanco.GetActivo(): Boolean;
begin
    Result := GetField('Activo');
end;

procedure TBanco.SetActivo(Valor:Boolean);
begin
    SetField('Activo', Valor);
end;

function TBanco.GetCodigoBancoSBEI(): SmallInt;
begin
    Result := GetField('CodigoBancoSBEI');
end;

procedure TBanco.SetCodigoBancoSBEI(Valor:SmallInt);
begin
    SetField('CodigoBancoSBEI', Valor);
end;

function TBanco.GetHabilitadoPagoVentanilla(): Boolean;
begin
    Result := GetField('HabilitadoPagoVentanilla');
end;

procedure TBanco.SetHabilitadoPagoVentanilla(Valor:Boolean);
begin
    SetField('HabilitadoPagoVentanilla', Valor);
end;

function TBanco.GetHabilitadoEmisorTC(): Boolean;
begin
    Result := GetField('HabilitadoEmisorTC');
end;

procedure TBanco.SetHabilitadoEmisorTC(Valor:Boolean);
begin
    SetField('HabilitadoEmisorTC', Valor);
end;

function TBanco.GetHabilitadoEmisorPAC(): Boolean;
begin
    Result := GetField('HabilitadoEmisorPAC');
end;

procedure TBanco.SetHabilitadoEmisorPAC(Valor:Boolean);
begin
    SetField('HabilitadoEmisorPAC', Valor);
end;

function TBanco.GetPrefijoIDTRX(): string;
begin
    Result := GetField('PrefijoIDTRX');
end;

procedure TBanco.SetPrefijoIDTRX(Valor:string);
begin
    SetField('PrefijoIDTRX', Valor);
end;

function TBanco.GetCodigoConciliacionBancaria(): string;
begin
    Result := GetField('CodigoConciliacionBancaria');
end;

procedure TBanco.SetCodigoConciliacionBancaria(Valor:string);
begin
    SetField('CodigoConciliacionBancaria', Valor);
end;

function TBanco.GetEsMaestro(): Boolean;
begin
    Result := GetField('EsMaestro');
end;

procedure TBanco.SetEsMaestro(Valor:Boolean);
begin
    SetField('EsMaestro', Valor);
end;

function TBanco.GetUsuarioCreacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioCreacion');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TBanco.SetUsuarioCreacion(value: string);
begin
    SetField('UsuarioCreacion', value);
end;

function TBanco.GetFechaHoraCreacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraCreacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TBanco.SetFechaHoraCreacion(value: TDateTime);
begin
    SetField('FechaHoraCreacion', value);
end;

function TBanco.GetUsuarioModificacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioModificacion');
    if t = null then
       t := '';
    Result := GetField('UsuarioModificacion');
end;

procedure TBanco.SetUsuarioModificacion(value: string);
begin
    SetField('UsuarioModificacion', value);
end;

function TBanco.GetFechaHoraModificacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraModificacion');
    if t = null then
       t := NullDate;
    Result := GetField('FechaHoraModificacion');
end;

procedure TBanco.SetFechaHoraModificacion(value: TDateTime);
begin
    SetField('FechaHoraModificacion', value);
end;

{$ENDREGION}

function TBanco.Obtener(Activo: Variant; CodigoBanco: Integer = 0; Descripcion: string = ''): TClientDataSet;
var
    sp: TADOStoredProc;
begin
    Result:= nil;
    try
        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'Bancos_SELECT';
        sp.Parameters.Refresh;
        if CodigoBanco <> 0 then
            sp.Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
        if Descripcion <> '' then
            sp.Parameters.ParamByName('@Descripcion').Value := Descripcion;
        if Activo <> null then
            sp.Parameters.ParamByName('@Activo').Value := Activo;

        sp.Open;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result := CrearClientDataSet(sp);

    finally
        FreeAndNil(sp);
    end;

end;

end.
