{INICIO: TASK_094_JMA_20160111}
unit Categoria;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TCategoria = Class(TClaseBase)     

    private
        function GetCodigoCategoria(): Byte;
        procedure SetCodigoCategoria(Valor:Byte);
        function GetDescripcion(): string;
        procedure SetDescripcion(Valor:string);
        function GetFactorTarifa(): Double;
        procedure SetFactorTarifa(Valor:Double);
        function GetCategoriaCambio(): Variant;
        procedure SetCategoriaCambio(Valor:Variant);
        function GetIDCategoriaClase(): Integer;
        procedure SetIDCategoriaClase(Valor:Integer);
        function GetClase(): string;
        procedure SetClase(Valor:string);


    public
        property CodigoCategoria: Byte read GetCodigoCategoria write SetCodigoCategoria;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property FactorTarifa: Double read GetFactorTarifa write SetFactorTarifa;
        property CategoriaCambio: Variant read GetCategoriaCambio write SetCategoriaCambio;
        property IDCategoriaClase: Integer read GetIDCategoriaClase write SetIDCategoriaClase;
        property Clase: string read GetClase write SetClase;

        //function ObtenerCategorias(CodigoCategoria:Byte; Descripcion:string): TClientDataSet;                                   //TASK_109_JMA_20170215
        function ObtenerCategorias(CodigoCategoria:Byte; Descripcion:string; Id_CategoriasClases: Integer = 0): TClientDataSet;   //TASK_109_JMA_20170215
        function ObtenerCategoriasCambioDisponibles(CodigoCategoriaInterurbana: Byte): TClientDataSet;
        function ObtenerIDCategoriaClase(Descripcion:string): Integer;
        function ActualizarCategorias(CodigoCategoria, IDCategoriaClase:Byte; FactorTarifa:Double; CategoriaCambio: Byte): Boolean;
        function ObtenerCategoriasInterurbanasAsociadas(CodigoCategoriaUrbana: Byte): TClientDataSet;  //TASK_106_JMA_20170206
    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TCategoria.GetCodigoCategoria(): Byte;
    begin
        Result := GetField('CodigoCategoria');
    end;

    procedure TCategoria.SetCodigoCategoria(Valor:Byte);
    begin
        SetField('CodigoCategoria', Valor);
    end;

    function TCategoria.GetDescripcion(): string;
    begin
        Result := GetField('Descripcion');
    end;

    procedure TCategoria.SetDescripcion(Valor:string);
    begin
        SetField('Descripcion', Valor);
    end;

    function TCategoria.GetFactorTarifa(): Double;
    begin
        Result := GetField('FactorTarifa');
    end;

    procedure TCategoria.SetFactorTarifa(Valor:Double);
    begin
        SetField('FactorTarifa', Valor);
    end;

    function TCategoria.GetCategoriaCambio(): Variant;
    begin
        Result := GetField('CategoriaCambio');
    end;

    procedure TCategoria.SetCategoriaCambio(Valor:Variant);
    begin
        SetField('CategoriaCambio', Valor);
    end;

    function TCategoria.GetIDCategoriaClase(): Integer;
    begin
        Result := GetField('IDCategoriaClase');
    end;

    procedure TCategoria.SetIDCategoriaClase(Valor:Integer);
    begin
        SetField('IDCategoriaClase', Valor);
    end;

     function TCategoria.GetClase(): string;
    begin
        Result := GetField('Clase');
    end;

    procedure TCategoria.SetClase(Valor:string);
    begin
        SetField('Clase', Valor);
    end;
{$ENDREGION}

    function TCategoria.ObtenerCategorias(CodigoCategoria:Byte; Descripcion:string; Id_CategoriasClases: Integer = 0): TClientDataSet;			//TASK_109_JMA_20170215
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'Categorias_Obtener';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@CodigoCategoria').Value := iif(CodigoCategoria <= 0, null, CodigoCategoria);
           sp.Parameters.ParamByName('@Descripcion').Value := IIf(Trim(Descripcion) = '', null, Descripcion);
           sp.Parameters.ParamByName('@Id_CategoriasClases').Value := IIf(Id_CategoriasClases = 0, null, Id_CategoriasClases);  //TASK_109_JMA_20170215

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
              
    function TCategoria.ObtenerCategoriasCambioDisponibles(CodigoCategoriaInterurbana: Byte): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try
            if CodigoCategoriaInterurbana <= 0 then
            begin
                ex:= Exception.Create('CodigoCategoriaInterurbana debe ser mayor que 0');
                raise ex;
            end;
            
           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'Categorias_ObtenerCambioDisponibles';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@CodigoCategoriaInterurbana').Value := CodigoCategoriaInterurbana;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
           FreeAndNil(sp);
        end;
    end;

    function TCategoria.ObtenerIDCategoriaClase(Descripcion:string): Integer;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try
            if Trim(Descripcion) = ''  then
            begin
                ex:= Exception.Create('Indique la Descripcion');
                raise ex;
            end;
            
           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'Categorias_ObtenerIDCategoriaClase';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@Descripcion').Value := Descripcion;

           sp.ExecProc;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;
                         
           Result := sp.Parameters.ParamByName('@IDCategoriaClase').Value
                    
        finally            
            FreeAndNil(sp);
        end;
    end;

    function TCategoria.ActualizarCategorias(CodigoCategoria, IDCategoriaClase:Byte; FactorTarifa:Double; CategoriaCambio: Byte): Boolean;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try
            Result := false;

            if CodigoCategoria <= 0 then
            begin
                ex := Exception.Create('CodigoCategoria debe ser mayor que 0');
                raise ex;
            end;

            if FactorTarifa < 0 then
            begin
                ex := Exception.Create('FactorTarifa debe ser mayor o igual a 0');
                raise ex;
            end;

            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'Categorias_Actualizar';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@FactorTarifa').Value := FactorTarifa;
            sp.Parameters.ParamByName('@CategoriaCambio').Value := IIf(CategoriaCambio > 0, CategoriaCambio, null);
            sp.Parameters.ParamByName('@CodigoCategoria').Value := CodigoCategoria;
            sp.Parameters.ParamByName('@IDCategoriaClase').Value := IDCategoriaClase;
            sp.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;

            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
            begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
            end
            else
                Result := True;
        finally
            FreeAndNil(sp);
        end;

    end;

{INICIO: TASK_106_JMA_20170206}
    function TCategoria.ObtenerCategoriasInterurbanasAsociadas(CodigoCategoriaUrbana: Byte): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'Categorias_InterurbanasAsociadas';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@CodigoCategoriaUrbana').Value := CodigoCategoriaUrbana;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
{TERMINO: TASK_106_JMA_20170206}
end.
