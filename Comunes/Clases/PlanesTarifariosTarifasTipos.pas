unit PlanesTarifariosTarifasTipos;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TTarifasTipos = Class(TClaseBase)

    private
        function GetCodigoTarifaTipo(): string;
        procedure SetCodigoTarifaTipo(value: string);

        function GetDescripcion(): string;
        procedure SetDescripcion(value: string);

        function GetColor(): string;
        procedure SetColor(value: string);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property CodigoTarifaTipo: string read GetCodigoTarifaTipo write SetCodigoTarifaTipo;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property Color: string read GetColor write SetColor;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(): TClientDataSet;
    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TTarifasTipos.GetCodigoTarifaTipo(): string;
    begin
        Result := GetField('CodigoTarifaTipo');
    end;

    procedure TTarifasTipos.SetCodigoTarifaTipo(value: string);
    begin
        SetField('CodigoTarifaTipo', value);
    end;

    function TTarifasTipos.GetDescripcion(): string;
    begin
        Result := GetField('Descripcion');
    end;

    procedure TTarifasTipos.SetDescripcion(value: string);
    begin
        SetField('Descripcion', value);
    end;

    function TTarifasTipos.GetColor(): string;
    begin
        Result := GetField('Color');
    end;

    procedure TTarifasTipos.SetColor(value: string);
    begin
        SetField('Color', value);
    end;

    function TTarifasTipos.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TTarifasTipos.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TTarifasTipos.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TTarifasTipos.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TTarifasTipos.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TTarifasTipos.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TTarifasTipos.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TTarifasTipos.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;
{$ENDREGION}

    function TTarifasTipos.Obtener(): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'PlanTarifarioTarifasTipos_Obtener';
           sp.Parameters.Refresh;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
end.
