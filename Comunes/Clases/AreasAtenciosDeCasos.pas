//TASK_121_JMA_20170328
unit AreasAtenciosDeCasos;

interface

uses ClaseBase, DB, ADODB, DBClient, SysUtils, DMConnection, UtilDB, PeaProcs, Variants, Util;


type TAreasAtencionDeCasosUsuarios= class(TClaseBase)
private
    function GetCodigoAreaAtencionDeCaso(): Integer;
    procedure SetCodigoAreaAtencionDeCaso(Valor:Integer);

    function GetCodigoUsuario(): string;
    procedure SetCodigoUsuario(Valor:string);

    function GetNombre(): string;
    procedure SetNombre(Valor:string);

    function GetApellido(): Variant;
    procedure SetApellido(Valor:Variant);

    function GetNombreUsuario(): string;
    procedure SetNombreUsuario(Valor:string);

    function GetPrincipal(): Boolean;
    procedure SetPrincipal(Valor:Boolean);

    public
    property CodigoAreaAtencionDeCaso: Integer read GetCodigoAreaAtencionDeCaso write SetCodigoAreaAtencionDeCaso;
    property CodigoUsuario: string read GetCodigoUsuario write SetCodigoUsuario;
    property Nombre: string read GetNombre write SetNombre;
    property Apellido: Variant read GetApellido write SetApellido;
    property NombreUsuario: string read GetNombreUsuario write SetNombreUsuario;
    property Principal: Boolean read GetPrincipal write SetPrincipal;

    function Obtener(CodigoAreaAtencionDeCasos: Integer): TClientDataSet;

end;
            
type TAreasAtencionDeCasosTiposCasos= class(TClaseBase)
private
    function GetCodigoAreaAtencionDeCaso(): Integer;
    procedure SetCodigoAreaAtencionDeCaso(Valor:Integer);

    function GetCodigoTipoDeCaso(): Integer;
    procedure SetCodigoTipoDeCaso(Valor:Integer);

    function GetDescripcion(): string;
    procedure SetDescripcion(Valor:string);

    function GetUsuarioCreacion(): string;
    procedure SetUsuarioCreacion(value: string);

    function GetFechaHoraCreacion(): TDateTime;
    procedure SetFechaHoraCreacion(value: TDateTime);

    function GetUsuarioModificacion(): string;
    procedure SetUsuarioModificacion(value: string);

    function GetFechaHoraModificacion(): TDateTime;
    procedure SetFechaHoraModificacion(value: TDateTime);


    public
    property CodigoAreaAtencionDeCaso: Integer read GetCodigoAreaAtencionDeCaso write SetCodigoAreaAtencionDeCaso;
    property CodigoTipoDeCaso: Integer read GetCodigoTipoDeCaso write SetCodigoTipoDeCaso;
    property Descripcion: string read GetDescripcion write SetDescripcion;
    property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
    property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
    property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
    property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

    function Obtener(CodigoAreaAtencionDeCasos: Integer): TClientDataSet;

end;

type TAreasAtencionDeCasos= class(TClaseBase)
private
    function GetCodigoAreaAtencionDeCaso(): Integer;
    procedure SetCodigoAreaAtencionDeCaso(Valor:Integer);

    function GetNombreAreaAtencionDeCaso(): string;
    procedure SetNombreAreaAtencionDeCaso(Valor:string);

    function GetResponsable(): Variant;
    procedure SetResponsable(Valor:Variant);

    function GetTelefono(): string;
    procedure SetTelefono(Valor:string);

    function GetEmail(): string;
    procedure SetEmail(Valor:string);

    function GetDescripcionArea(): Variant;
    procedure SetDescripcionArea(Valor:Variant);

    function GetUsuarioCreacion(): string;
    procedure SetUsuarioCreacion(value: string);

    function GetFechaHoraCreacion(): TDateTime;
    procedure SetFechaHoraCreacion(value: TDateTime);

    function GetUsuarioModificacion(): string;
    procedure SetUsuarioModificacion(value: string);

    function GetFechaHoraModificacion(): TDateTime;
    procedure SetFechaHoraModificacion(value: TDateTime);


    public

    Usuarios : TAreasAtencionDeCasosUsuarios;
    TiposCasos : TAreasAtencionDeCasosTiposCasos;

    property CodigoAreaAtencionDeCaso: Integer read GetCodigoAreaAtencionDeCaso write SetCodigoAreaAtencionDeCaso;
    property NombreAreaAtencionDeCaso: string read GetNombreAreaAtencionDeCaso write SetNombreAreaAtencionDeCaso;
    property Responsable: Variant read GetResponsable write SetResponsable;
    property Telefono: string read GetTelefono write SetTelefono;
    property Email: string read GetEmail write SetEmail;
    property DescripcionArea: Variant read GetDescripcionArea write SetDescripcionArea;
    property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
    property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
    property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
    property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

    function Obtener(): TClientDataSet;
    function Insertar(var CodigoArea: Integer;NombreArea, responsableArea, telf, mail, descripcion: string; dsUsuariosAsignados, dsTiposCasosAsignados:TDataSet; Usuario: string): Boolean;
    function Modificar(CodigoArea: Integer; NombreArea, responsableArea, telf, mail, descripcion: string; dsUsuariosAsignados, dsTiposCasosAsignados:TDataSet; Usuario: string): Boolean;
    function Eliminar(CodigoArea: Integer): Boolean;

end;


implementation


{$REGION 'TAreasAtencionDeCasosUsuarios'}
{$REGION 'GETTERS y SETTERS'}
function TAreasAtencionDeCasosUsuarios.GetCodigoAreaAtencionDeCaso(): Integer;
begin
    Result := GetField('CodigoAreaAtencionDeCaso');
end;

procedure TAreasAtencionDeCasosUsuarios.SetCodigoAreaAtencionDeCaso(Valor:Integer);
begin
    SetField('CodigoAreaAtencionDeCaso', Valor);
end;

function TAreasAtencionDeCasosUsuarios.GetCodigoUsuario(): string;
begin
    Result := GetField('CodigoUsuario');
end;

procedure TAreasAtencionDeCasosUsuarios.SetCodigoUsuario(Valor:string);
begin
    SetField('CodigoUsuario', Valor);
end;

function TAreasAtencionDeCasosUsuarios.GetNombre(): string;
begin
    Result :=  GetField('Nombre');
end;

procedure TAreasAtencionDeCasosUsuarios.SetNombre(Valor:string);
begin
    SetField('Nombre', Valor);
end;

function TAreasAtencionDeCasosUsuarios.GetApellido(): Variant;
var
    t: Variant;
begin
    t:= GetField('Apellido');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TAreasAtencionDeCasosUsuarios.SetApellido(Valor:Variant);
begin
    SetField('Apellido', Valor);
end;

function TAreasAtencionDeCasosUsuarios.GetNombreUsuario(): string;
begin
    Result := GetField('NombreUsuario');
end;

procedure TAreasAtencionDeCasosUsuarios.SetNombreUsuario(Valor:string);
begin
    SetField('NombreUsuario', Valor);
end;

function TAreasAtencionDeCasosUsuarios.GetPrincipal(): Boolean;
begin
    Result :=  GetField('Principal');
end;

procedure TAreasAtencionDeCasosUsuarios.SetPrincipal(Valor:Boolean);
begin
    SetField('Principal', Valor);
end;

{$ENDREGION}

function TAreasAtencionDeCasosUsuarios.Obtener(CodigoAreaAtencionDeCasos: Integer): TClientDataSet;
var
    sp: TADOStoredProc;
begin
    try

       sp := TADOStoredProc.Create(nil);
       sp.Connection := DMConnections.BaseCAC;
       sp.ProcedureName := 'ObtenerUsuariosAreas';
       sp.Parameters.Refresh;
       sp.Parameters.ParamByName('@Area').Value := CodigoAreaAtencionDeCasos;

       sp.Open;

       if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

       Result:= CrearClientDataSet(sp);

    finally
       FreeAndNil(sp);
    end;
end;

{$ENDREGION}

{$REGION 'TAreasAtencionDeCasosTiposCasos'}
{$REGION 'GETTERS y SETTERS'}
function TAreasAtencionDeCasosTiposCasos.GetCodigoAreaAtencionDeCaso(): Integer;
begin
    Result := GetField('CodigoAreaAtencionDeCaso');
end;

procedure TAreasAtencionDeCasosTiposCasos.SetCodigoAreaAtencionDeCaso(Valor:Integer);
begin
    SetField('CodigoAreaAtencionDeCaso', Valor);
end;

function TAreasAtencionDeCasosTiposCasos.GetCodigoTipoDeCaso(): Integer;
begin
    Result := GetField('CodigoTipoDeCaso');
end;

procedure TAreasAtencionDeCasosTiposCasos.SetCodigoTipoDeCaso(Valor:Integer);
begin
    SetField('CodigoTipoDeCaso', Valor);
end;

function TAreasAtencionDeCasosTiposCasos.GetDescripcion(): string;
begin
    Result :=  GetField('Descripcion');
end;

procedure TAreasAtencionDeCasosTiposCasos.SetDescripcion(Valor:string);
begin
    SetField('Descripcion', Valor);
end;

function TAreasAtencionDeCasosTiposCasos.GetUsuarioCreacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioCreacion');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TAreasAtencionDeCasosTiposCasos.SetUsuarioCreacion(value: string);
begin
    SetField('UsuarioCreacion', value);
end;

function TAreasAtencionDeCasosTiposCasos.GetFechaHoraCreacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraCreacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TAreasAtencionDeCasosTiposCasos.SetFechaHoraCreacion(value: TDateTime);
begin
    SetField('FechaHoraCreacion', value);
end;

function TAreasAtencionDeCasosTiposCasos.GetUsuarioModificacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioModificacion');
    if t = null then
       t := '';
    Result := t;
end;

procedure TAreasAtencionDeCasosTiposCasos.SetUsuarioModificacion(value: string);
begin
    SetField('UsuarioModificacion', value);
end;

function TAreasAtencionDeCasosTiposCasos.GetFechaHoraModificacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraModificacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TAreasAtencionDeCasosTiposCasos.SetFechaHoraModificacion(value: TDateTime);
begin
    SetField('FechaHoraModificacion', value);
end;

{$ENDREGION}

function TAreasAtencionDeCasosTiposCasos.Obtener(CodigoAreaAtencionDeCasos: Integer): TClientDataSet;
var
    sp: TADOStoredProc;
begin
    try

       sp := TADOStoredProc.Create(nil);
       sp.Connection := DMConnections.BaseCAC;
       sp.ProcedureName := 'AreasAtencionDeCasosTiposCasos_SELECT';
       sp.Parameters.Refresh;
       sp.Parameters.ParamByName('@CodigoAreaAtencionDeCaso').Value := CodigoAreaAtencionDeCasos;

       sp.Open;

       if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

       Result:= CrearClientDataSet(sp);

    finally
       FreeAndNil(sp);
    end;
end;

{$ENDREGION}

{$REGION 'TAreasAtencionDeCasos'}
{$REGION 'GETTERS y SETTERS'}
function TAreasAtencionDeCasos.GetCodigoAreaAtencionDeCaso(): Integer;
begin
    Result := GetField('CodigoAreaAtencionDeCaso');
end;

procedure TAreasAtencionDeCasos.SetCodigoAreaAtencionDeCaso(Valor:Integer);
begin
    SetField('CodigoAreaAtencionDeCaso', Valor);
end;

function TAreasAtencionDeCasos.GetNombreAreaAtencionDeCaso(): string;
begin
    Result := GetField('NombreAreaAtencionDeCaso');
end;

procedure TAreasAtencionDeCasos.SetNombreAreaAtencionDeCaso(Valor:string);
begin
    SetField('NombreAreaAtencionDeCaso', Valor);
end;

function TAreasAtencionDeCasos.GetResponsable(): Variant;
var
    t: Variant;
begin
    t:= GetField('Responsable');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TAreasAtencionDeCasos.SetResponsable(Valor:Variant);
begin
    SetField('Responsable', Valor);
end;

function TAreasAtencionDeCasos.GetTelefono(): string;
begin
    Result := GetField('Telefono');
end;

procedure TAreasAtencionDeCasos.SetTelefono(Valor:string);
begin
    SetField('Telefono', Valor);
end;

function TAreasAtencionDeCasos.GetEmail(): string;
begin
    Result := GetField('Email');
end;

procedure TAreasAtencionDeCasos.SetEmail(Valor:string);
begin
    SetField('Email', Valor);
end;

function TAreasAtencionDeCasos.GetDescripcionArea(): Variant;
var
    t: Variant;
begin
    t:= GetField('DescripcionArea');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TAreasAtencionDeCasos.SetDescripcionArea(Valor:Variant);
begin
    SetField('DescripcionArea', Valor);
end;

function TAreasAtencionDeCasos.GetUsuarioCreacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioCreacion');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TAreasAtencionDeCasos.SetUsuarioCreacion(value: string);
begin
    SetField('UsuarioCreacion', value);
end;

function TAreasAtencionDeCasos.GetFechaHoraCreacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraCreacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TAreasAtencionDeCasos.SetFechaHoraCreacion(value: TDateTime);
begin
    SetField('FechaHoraCreacion', value);
end;

function TAreasAtencionDeCasos.GetUsuarioModificacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioModificacion');
    if t = null then
       t := '';
    Result := t;
end;

procedure TAreasAtencionDeCasos.SetUsuarioModificacion(value: string);
begin
    SetField('UsuarioModificacion', value);
end;

function TAreasAtencionDeCasos.GetFechaHoraModificacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraModificacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TAreasAtencionDeCasos.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;

{$ENDREGION}

function TAreasAtencionDeCasos.Obtener(): TClientDataSet;
var
    sp: TADOStoredProc;
begin
    try

       sp := TADOStoredProc.Create(nil);
       sp.Connection := DMConnections.BaseCAC;
       sp.ProcedureName := 'ObtenerAreasAtencionDeCasos';
       sp.Parameters.Refresh;

       sp.Open;

       if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

       Result:= CrearClientDataSet(sp);

    finally
       FreeAndNil(sp);
    end;
end;

function TAreasAtencionDeCasos.Insertar(var CodigoArea: Integer; NombreArea, responsableArea, telf, mail, descripcion: string; dsUsuariosAsignados, dsTiposCasosAsignados:TDataSet; Usuario: string): Boolean;
var
    sp: TADOStoredProc;
    usuariosAsignados, tiposCasosAsignados: string;
    i: Integer;
begin
    try

        usuariosAsignados := '';
        dsUsuariosAsignados.DisableControls;
        dsUsuariosAsignados.First;
        while not dsUsuariosAsignados.eof do
        begin
            if usuariosAsignados <> '' then
                usuariosAsignados := usuariosAsignados + ';';
            usuariosAsignados := usuariosAsignados + dsUsuariosAsignados.FieldByName('CodigoUsuario').Value;
            dsUsuariosAsignados.Next;
        end;
        dsUsuariosAsignados.EnableControls;

        tiposCasosAsignados := '';
        dsTiposCasosAsignados.DisableControls;
        dsTiposCasosAsignados.First;
        while not dsTiposCasosAsignados.eof do
        begin
            if tiposCasosAsignados <> '' then
                tiposCasosAsignados := tiposCasosAsignados + ';';
            tiposCasosAsignados := tiposCasosAsignados + IntToStr(dsTiposCasosAsignados.FieldByName('CodigoTipoDeCaso').Value);
            dsTiposCasosAsignados.Next
        end;
        dsTiposCasosAsignados.EnableControls;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'AreasAtencionDeCasos_Agregar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@NombreAreaAtencionDeCaso').Value := NombreArea;
        sp.Parameters.ParamByName('@Responsable').Value := responsableArea;
        sp.Parameters.ParamByName('@Telefono').Value := telf;
        sp.Parameters.ParamByName('@Email').Value := mail;
        sp.Parameters.ParamByName('@Descripcion').Value := descripcion;
        sp.Parameters.ParamByName('@UsuariosAsignados').Value := usuariosAsignados;
        sp.Parameters.ParamByName('@TiposCasosAsignados').Value := tiposCasosAsignados;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        CodigoArea := sp.Parameters.ParamByName('@CodigoAreaAtencionDeCaso').Value;

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;

function TAreasAtencionDeCasos.Modificar(CodigoArea: Integer; NombreArea, responsableArea, telf, mail, descripcion: string; dsUsuariosAsignados, dsTiposCasosAsignados:TDataSet; Usuario: string): Boolean;
var
    sp: TADOStoredProc;
    usuariosAsignados, tiposCasosAsignados: string;
    i: Integer;
begin
    try

        usuariosAsignados := '';
        dsUsuariosAsignados.DisableControls;
        dsUsuariosAsignados.First;
        while not dsUsuariosAsignados.eof do
        begin
            if usuariosAsignados <> '' then
                usuariosAsignados := usuariosAsignados + ';';
            usuariosAsignados := usuariosAsignados + dsUsuariosAsignados.FieldByName('CodigoUsuario').Value;
            dsUsuariosAsignados.Next;
        end;
        dsUsuariosAsignados.EnableControls;

        tiposCasosAsignados := '';
        dsTiposCasosAsignados.DisableControls;
        dsTiposCasosAsignados.First;
        while not dsTiposCasosAsignados.eof do
        begin
            if tiposCasosAsignados <> '' then
                tiposCasosAsignados := tiposCasosAsignados + ';';
            tiposCasosAsignados := tiposCasosAsignados + IntToStr(dsTiposCasosAsignados.FieldByName('CodigoTipoDeCaso').Value);
            dsTiposCasosAsignados.Next
        end;
        dsTiposCasosAsignados.EnableControls;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'AreasAtencionDeCasos_Modificar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@NombreAreaAtencionDeCaso').Value := NombreArea;
        sp.Parameters.ParamByName('@Responsable').Value := responsableArea;
        sp.Parameters.ParamByName('@Telefono').Value := telf;
        sp.Parameters.ParamByName('@Email').Value := mail;
        sp.Parameters.ParamByName('@Descripcion').Value := descripcion;
        sp.Parameters.ParamByName('@UsuariosAsignados').Value := usuariosAsignados;
        sp.Parameters.ParamByName('@TiposCasosAsignados').Value := tiposCasosAsignados;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.Parameters.ParamByName('@CodigoAreaAtencionDeCaso').Value := CodigoArea;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;

function TAreasAtencionDeCasos.Eliminar(CodigoArea: Integer): Boolean;
var
    sp: TADOStoredProc;
    usuariosAsignados, tiposCasosAsignados: string;
    i: Integer;
begin
    try

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'AreasAtencionDeCasos_Eliminar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoAreaAtencionDeCaso').Value := CodigoArea;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;
       
{$ENDREGION}

end.
