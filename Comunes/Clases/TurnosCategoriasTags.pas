//TASK_104_JMA_20170201
unit TurnosCategoriasTags;

interface
uses ClaseBase, DB, ADODB, DBClient, SysUtils, DMConnection;

type TTurnoCategoriaTag= class(TClaseBase)

    private
        function GetCodigoCategoriaInterurbana(): Byte;
        procedure SetCodigoCategoriaInterurbana(Valor:Byte);
        function GetCodigoCategoriaUrbana(): Byte;
        procedure SetCodigoCategoriaUrbana(Valor:Byte);
        function GetCategoriaInterurbana(): string;
        procedure SetCategoriaInterurbana(Valor:string);
        function GetCategoriaUrbana(): string;
        procedure SetCategoriaUrbana(Valor:string);
        function GetCantidad(): Integer;
        procedure SetCantidad(Valor:Integer);

    public
        property CodigoCategoriaInterurbana: Byte read GetCodigoCategoriaInterurbana write SetCodigoCategoriaInterurbana;
        property CodigoCategoriaUrbana: Byte read GetCodigoCategoriaUrbana write SetCodigoCategoriaUrbana;
        property CategoriaInterurbana: string read GetCategoriaInterurbana write SetCategoriaInterurbana;
        property CategoriaUrbana: string read GetCategoriaUrbana write SetCategoriaUrbana;
        property Cantidad: Integer read GetCantidad write SetCantidad;

        function Obtener(): TClientDataSet;

        function ActualizarTurnoMovimientoValeTelevias(NumeroTurno, TipoMovimiento: Integer; NumeroVale: string): Boolean;
        function ActualizarTurnoMovimientoValeEfectivo(NumeroTurno, TipoMovimiento, Monto: Integer; NumeroVale: string): Boolean;

end;
 
implementation
{$REGION 'GETTERS y SETTERS}
function TTurnoCategoriaTag.GetCodigoCategoriaInterurbana(): Byte;
begin
    Result := GetField('CodigoCategoriaInterurbana');
end;

procedure TTurnoCategoriaTag.SetCodigoCategoriaInterurbana(Valor:Byte);
begin
    SetField('CodigoCategoriaInterurbana', Valor);
end;

function TTurnoCategoriaTag.GetCodigoCategoriaUrbana(): Byte;
begin
    Result := GetField('CodigoCategoriaUrbana');
end;

procedure TTurnoCategoriaTag.SetCodigoCategoriaUrbana(Valor:Byte);
begin
    SetField('CodigoCategoriaUrbana', Valor);
end;

function TTurnoCategoriaTag.GetCategoriaInterurbana(): string;
begin
    Result := GetField('CategoriaInterurbana');
end;

procedure TTurnoCategoriaTag.SetCategoriaInterurbana(Valor:string);
begin
    SetField('CategoriaInterurbana', Valor);
end;

function TTurnoCategoriaTag.GetCategoriaUrbana(): string;
begin
    Result := GetField('CategoriaUrbana');
end;

procedure TTurnoCategoriaTag.SetCategoriaUrbana(Valor:string);
begin
    SetField('CategoriaUrbana', Valor);
end;

function TTurnoCategoriaTag.GetCantidad(): Integer;
begin
    Result := GetField('Cantidad');
end;

procedure TTurnoCategoriaTag.SetCantidad(Valor:Integer);
begin
    SetField('Cantidad', Valor);
end;
{$ENDREGION}

function TTurnoCategoriaTag.Obtener(): TClientDataSet;
var
    sp: TADOStoredProc;
    ex: Exception;
begin
    Result:= nil;
    try
        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'ObtenerCategoriasTags';
        sp.Parameters.Refresh;

        sp.Open;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
        begin
           ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
           raise ex;
        end;

        Result := CrearClientDataSet(sp);

    finally
        FreeAndNil(sp);
    end;

end;

function TTurnoCategoriaTag.ActualizarTurnoMovimientoValeTelevias(NumeroTurno, TipoMovimiento: Integer; NumeroVale: string): Boolean;
var
    sp: TADOStoredProc;
    ex: Exception;
begin

    try
        Result := false;
        ClientDataSet.DisableControls;
        ClientDataSet.First;

        while not ClientDataSet.Eof do begin
            if ( Self.Cantidad > 0 ) then begin
                sp := TADOStoredProc.Create(nil);
                sp.Connection := DMConnections.BaseCAC;
                sp.ProcedureName := 'ActualizarTurnoMovimiento';
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@NumeroTurno').Value := NumeroTurno;
                sp.Parameters.ParamByName('@CodigoTipoMovimiento').Value := TipoMovimiento;
                sp.Parameters.ParamByName('@CodigoCategoria').Value := Self.CodigoCategoriaInterurbana;
                sp.Parameters.ParamByName('@CodigoCategoriaUrbana').Value := Self.CodigoCategoriaUrbana;
                sp.Parameters.ParamByName('@Cantidad').Value := Self.Cantidad;
                sp.Parameters.ParamByName('@NumeroVale').Value := Trim(NumeroVale);
                sp.ExecProc;

                if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                begin
                   ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
                   raise ex;
                end;
            end;

            ClientDataSet.Next;
        end;
        ClientDataSet.First;

        Result := True;
    finally
        FreeAndNil(sp);
    end;

end;

function TTurnoCategoriaTag.ActualizarTurnoMovimientoValeEfectivo(NumeroTurno, TipoMovimiento, Monto: Integer; NumeroVale: string): Boolean;
var
    sp: TADOStoredProc;
    ex: Exception;
begin

    try
        Result := false;
        ClientDataSet.DisableControls;
        ClientDataSet.First;

        while not ClientDataSet.Eof do begin
            if ( Self.Cantidad > 0 ) then begin
                sp := TADOStoredProc.Create(nil);
                sp.Connection := DMConnections.BaseCAC;
                sp.ProcedureName := 'ActualizarTurnoMovimiento';
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@NumeroTurno').Value := NumeroTurno;
                sp.Parameters.ParamByName('@CodigoTipoMovimiento').Value := TipoMovimiento;
                sp.Parameters.ParamByName('@CodigoCategoria').Value := Self.CodigoCategoriaInterurbana;
                sp.Parameters.ParamByName('@CodigoCategoriaUrbana').Value := Self.CodigoCategoriaUrbana;
                sp.Parameters.ParamByName('@Cantidad').Value := Monto * 100;
                sp.Parameters.ParamByName('@NumeroVale').Value := Trim(NumeroVale);
                sp.ExecProc;

                if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                begin
                   ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
                   raise ex;
                end;
            end;

            ClientDataSet.Next;
        end;
        ClientDataSet.First;

        Result := True;
    finally
        FreeAndNil(sp);
    end;

end;


end.
