//TASK_109_JMA_20170215
unit PlanesTarifarios;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util, PuntosCobro, Categoria;

type
    TTarifasTipos = Class(TClaseBase)

    private
        function GetCodigoTarifaTipo(): string;
        procedure SetCodigoTarifaTipo(value: string);

        function GetDescripcion(): string;
        procedure SetDescripcion(value: string);

        function GetColor(): string;
        procedure SetColor(value: string);

        function GetId_CategoriasClases(): Integer;
        procedure SetId_CategoriasClases(value: Integer);

        function GetAfectaPlanTarifario(): Boolean;
        procedure SetAfectaPlanTarifario(value: Boolean);

        function GetMostrarEnBandasHorarias(): Boolean;
        procedure SetMostrarEnBandasHorarias(value: Boolean);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property CodigoTarifaTipo: string read GetCodigoTarifaTipo write SetCodigoTarifaTipo;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property Color: string read GetColor write SetColor;
        property Id_CategoriasClases: Integer read GetId_CategoriasClases write SetId_CategoriasClases;
        property AfectaPlanTarifario: Boolean read GetAfectaPlanTarifario write SetAfectaPlanTarifario;
        property MostrarEnBandasHorarias: Boolean read GetMostrarEnBandasHorarias write SetMostrarEnBandasHorarias;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;    

        function Obtener(AfectaPlanTarifario: Variant; MostrarEnBandasHorarias: Variant): TClientDataSet;
    end;

type
    TTarifasDiasTipos = Class(TClaseBase)

    private
        function GetCodigoDiaTipo(): string;
        procedure SetCodigoDiaTipo(value: string);

        function GetDescripcion(): string;
        procedure SetDescripcion(value: string);

        function GetCodigoDiaTipoConcesionaria(): Variant;
        procedure SetCodigoDiaTipoConcesionaria(value: Variant);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property CodigoDiaTipo: string read GetCodigoDiaTipo write SetCodigoDiaTipo;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property CodigoDiaTipoConcesionaria: Variant read GetCodigoDiaTipoConcesionaria write SetCodigoDiaTipoConcesionaria;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(): TClientDataSet;
    end;
 
type
    TPlanTarifarioTarifasPuntosCobro = Class(TClaseBase)

    private
        function GetID_Tarifa(): Integer;
        procedure SetID_Tarifa(value: Integer);

        function GetNumeroPuntoCobro(): Byte;
        procedure SetNumeroPuntoCobro(value: Byte);

        function GetDescripcionPuntoCobro(): string;
        procedure SetDescripcionPuntoCobro(value: string);

        function GetCategoria(): Byte;
        procedure SetCategoria(value: Byte);

        function GetDescripcionCategoria(): string;
        procedure SetDescripcionCategoria(value: string);

        function GetCodigoCategoriasClases(): Integer;
        procedure SetCodigoCategoriasClases(value: Integer);

        function GetDescripcionCategoriasClases(): string;
        procedure SetDescripcionCategoriasClases(value: string);

        function GetCodigoTarifaTipo(): string;
        procedure SetCodigoTarifaTipo(value: string);

        function GetDescripcionTarifaTipo(): string;
        procedure SetDescripcionTarifaTipo(value: string);

        function GetCodigoDiaTipo(): string;
        procedure SetCodigoDiaTipo(value: string);

        function GetDescripcionDiaTipo(): string;
        procedure SetDescripcionDiaTipo(value: string);

        function GetID_PlanTarifarioVersion(): LongInt;
        procedure SetID_PlanTarifarioVersion(value: LongInt);

        function GetHoraDesde(): string;
        procedure SetHoraDesde(value: string);

        function GetHoraHasta(): string;
        procedure SetHoraHasta(value: string);

        function GetImporte(): Double;
        procedure SetImporte(value: Double);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

        function GetEstado(): Byte;
        procedure SetEstado(value: Byte);

    public
        property ID_Tarifa: Integer read GetID_Tarifa write SetID_Tarifa;
        property NumeroPuntoCobro: Byte read GetNumeroPuntoCobro write SetNumeroPuntoCobro;
        property DescripcionPuntoCobro: string read GetDescripcionPuntoCobro write SetDescripcionPuntoCobro;
        property Categoria: Byte read GetCategoria write SetCategoria;
        property DescripcionCategoria: string read GetDescripcionCategoria write SetDescripcionCategoria;
        property CodigoCategoriasClases: Integer read GetCodigoCategoriasClases write SetCodigoCategoriasClases;
        property DescripcionCategoriasClases: string read GetDescripcionCategoriasClases write SetDescripcionCategoriasClases;
        property CodigoTarifaTipo: string read GetCodigoTarifaTipo write SetCodigoTarifaTipo;
        property DescripcionTarifaTipo: string read GetDescripcionTarifaTipo write SetDescripcionTarifaTipo;
        property CodigoDiaTipo: string read GetCodigoDiaTipo write SetCodigoDiaTipo;
        property DescripcionDiaTipo: string read GetDescripcionDiaTipo write SetDescripcionDiaTipo;
        property ID_PlanTarifarioVersion: LongInt read GetID_PlanTarifarioVersion write SetID_PlanTarifarioVersion;
        property HoraDesde: string read GetHoraDesde write SetHoraDesde;
        property HoraHasta: string read GetHoraHasta write SetHoraHasta;
        property Importe: Double read GetImporte write SetImporte;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;
        property Estado: Byte read GetEstado write SetEstado;
        
        function Obtener(_IDPlanTarifarioVersion: LongInt; _NumeroPuntoCobro: Byte = 0; _CodigoCategoria: Byte = 0; _CodigoCategoriasClases: Integer = 0; _CodigoDiaTipo: string = ''): TClientDataSet;
        function Agregar(_IDPlanTarifarioVersion: LongInt; _NumeroPuntoCobro, _CodigoCategoria: Byte; _IdCategoriasClases: Integer; _CodigoTarifaTipo, _CodigoDiaTipo, _HoraDesde, _HoraHasta: string; _Importe: Double; _Usuario: string): Boolean;
        function Actualizar(_IDPlanTarifarioVersion: LongInt; _IDTarifa:Integer; _NumeroPuntoCobro, _CodigoCategoria: Byte; _IdCategoriasClases: Integer; _CodigoTarifaTipo, _CodigoDiaTipo, _HoraDesde, _HoraHasta: string; _Importe: Double; _Usuario: string): Boolean;
        function Eliminar(_IDTarifa:Integer; _Usuario: string): Boolean;
        function ValidarTarifas(_TarifasPuntosCobros: TPlanTarifarioTarifasPuntosCobro; _IDCategoriasClases: Integer): TClientDataSet;
    end;

type
    TPlanTarifarioVersion = Class(TClaseBase)

    private
        function GetID_PlanTarifarioVersion(): LongInt;
        procedure SetID_PlanTarifarioVersion(value: LongInt);

        function GetCodigoConcesionaria(): Byte;
        procedure SetCodigoConcesionaria(value: Byte);

        function GetConcesionaria(): string;
        procedure SetConcesionaria(value: string);

        function GetCodigoVersion(): Integer;
        procedure SetCodigoVersion(value: Integer);

        function GetID_CategoriasClases(): Integer;
        procedure SetID_CategoriasClases(value: Integer);

        function GetCategoriaClase(): string;
        procedure SetCategoriaClase(value: string);

        function GetFechaActivacion(): TDateTime;
        procedure SetFechaActivacion(value: TDateTime);

        function GetHabilitado(): Boolean;
        procedure SetHabilitado(value: Boolean);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

        function GetFechaAutorizacionMOP(): TDateTime;
        procedure SetFechaAutorizacionMOP(value: TDateTime);

        function GetFechaHabilitacion(): TDateTime;
        procedure SetFechaHabilitacion(value: TDateTime);

        function GetUsuarioHabilitacion1(): string;
        procedure SetUsuarioHabilitacion1(value: string);

        function GetUsuarioHabilitacion2(): string;
        procedure SetUsuarioHabilitacion2(value: string);

        function GetUsuarioHabilitacion3(): string;
        procedure SetUsuarioHabilitacion3(value: string);

        function GetTBFP(): Double;
        procedure SetTBFP(value: Double);

        function GetTBP(): Double;
        procedure SetTBP(value: Double);

        function GetTBI(): Double;
        procedure SetTBI(value: Double);

        function GetTS(): Double;
        procedure SetTS(value: Double);

        function GetIPC(): Double;
        procedure SetIPC(value: Double);

    public
        oTarifas: TPlanTarifarioTarifasPuntosCobro;
        property ID_PlanTarifarioVersion: LongInt read GetID_PlanTarifarioVersion write SetID_PlanTarifarioVersion;
        property CodigoConcesionaria: Byte read GetCodigoConcesionaria write SetCodigoConcesionaria;
        property Concesionaria: string read GetConcesionaria write SetConcesionaria;
        property CodigoVersion: Integer read GetCodigoVersion write SetCodigoVersion;
        property ID_CategoriasClases: Integer read GetID_CategoriasClases write SetID_CategoriasClases;
        property CategoriaClase: string read GetCategoriaClase write SetCategoriaClase;
        property FechaActivacion: TDateTime read GetFechaActivacion write SetFechaActivacion;
        property Habilitado: Boolean read GetHabilitado write SetHabilitado;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;
        property FechaFechaAutorizacionMOP: TDateTime read GetFechaAutorizacionMOP write SetFechaAutorizacionMOP;
        property FechaHabilitacion: TDateTime read GetFechaHabilitacion write SetFechaHabilitacion;
        property UsuarioHabilitacion1: string read GetUsuarioHabilitacion1 write SetUsuarioHabilitacion1;
        property UsuarioHabilitacion2: string read GetUsuarioHabilitacion2 write SetUsuarioHabilitacion2;
        property UsuarioHabilitacion3: string read GetUsuarioHabilitacion3 write SetUsuarioHabilitacion3;
        property TBFP: Double read GetTBFP write SetTBFP;
        property TBP: Double read GetTBP write SetTBP;
        property TBI: Double read GetTBI write SetTBI;
        property TS: Double read GetTS write SetTS;
        property IPC: Double read GetIPC write SetIPC;

        function Obtener(_CodigoConcesionaria: Byte = 0; _IDCategoriasClases: Integer = 0): TClientDataSet;
        function Agregar(_CodigoConcesionaria: Byte; _IDCategoriasClases: Integer; _FechaActivacion: TDateTime; _TarifasPuntosCobros: TPlanTarifarioTarifasPuntosCobro; _Usuario: string): LongInt;
        function Actualizar(_IdPlanTarifarioVersion: LongInt; _FechaActivacion: TDateTime; _TarifasPuntosCobros: TPlanTarifarioTarifasPuntosCobro; _Usuario: string): Boolean;
        function Eliminar(_IdPlantarifarioVersion: LongInt; _Usuario: string): Boolean;
        procedure ActualizarTarifasPuntosCobro(_IDPlanTarifarioVersion: LongInt; _TarifasPuntosCobro: TPlanTarifarioTarifasPuntosCobro);
        function HabilitarPlan(_IDPlanTarifarioVersion: LongInt; _Usuario: string; _Habilitar: Boolean): Boolean;
        function ActualizarFechaMOP(_IDPlanTarifarioVersion:LongInt; _FechaMOP:TDateTime; _Usuario: string):Boolean;
        destructor Destroy(); override;
    end;




implementation

{$REGION 'TPlanTarifarioTarifasTipos'}
{$REGION 'GETTERS y SETTERS'}
    function TTarifasTipos.GetCodigoTarifaTipo(): string;
    begin
        Result := GetField('CodigoTarifaTipo');
    end;

    procedure TTarifasTipos.SetCodigoTarifaTipo(value: string);
    begin
        SetField('CodigoTarifaTipo', value);
    end;

    function TTarifasTipos.GetDescripcion(): string;
    begin
        Result := GetField('Descripcion');
    end;

    procedure TTarifasTipos.SetDescripcion(value: string);
    begin
        SetField('Descripcion', value);
    end;

    function TTarifasTipos.GetColor(): string;
    begin
        Result := GetField('Color');
    end;

    procedure TTarifasTipos.SetColor(value: string);
    begin
        SetField('Color', value);
    end;

    function TTarifasTipos.GetId_CategoriasClases(): Integer;
    begin
        Result := GetField('Id_CategoriasClases');
    end;

    procedure TTarifasTipos.SetId_CategoriasClases(value: Integer);
    begin
        SetField('Id_CategoriasClases', value);
    end;

    function TTarifasTipos.GetAfectaPlanTarifario(): Boolean;
    begin
        Result := GetField('AfectaPlanTarifario');
    end;

    procedure TTarifasTipos.SetAfectaPlanTarifario(value: Boolean);
    begin
        SetField('AfectaPlanTarifario', value);
    end;

    function TTarifasTipos.GetMostrarEnBandasHorarias(): Boolean;
    begin
        Result := GetField('MostrarEnBandasHorarias');
    end;

    procedure TTarifasTipos.SetMostrarEnBandasHorarias(value: Boolean);
    begin
        SetField('MostrarEnBandasHorarias', value);
    end;

    function TTarifasTipos.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TTarifasTipos.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TTarifasTipos.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TTarifasTipos.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TTarifasTipos.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := t;
    end;

    procedure TTarifasTipos.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TTarifasTipos.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TTarifasTipos.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;

{$ENDREGION}

    function TTarifasTipos.Obtener(AfectaPlanTarifario: Variant; MostrarEnBandasHorarias: Variant): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try

            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifarioTarifasTipos_Obtener';
            sp.Parameters.Refresh;
            if AfectaPlanTarifario = null then
                sp.Parameters.ParamByName('@AfectaPlanTarifario').Value := null
            else
                sp.Parameters.ParamByName('@AfectaPlanTarifario').Value := Boolean(AfectaPlanTarifario);

            if MostrarEnBandasHorarias = null then
                sp.Parameters.ParamByName('@MostrarEnBandasHorarias').Value := null
            else
                sp.Parameters.ParamByName('@MostrarEnBandasHorarias').Value := Boolean(MostrarEnBandasHorarias);

            sp.Open;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
{$ENDREGION}

{$REGION 'TPlanTarifarioDiasTipos'}
{$REGION 'GETTERS y SETTERS'}
    function TTarifasDiasTipos.GetCodigoDiaTipo(): string;
    begin
        Result := GetField('CodigoDiaTipo');
    end;

    procedure TTarifasDiasTipos.SetCodigoDiaTipo(value: string);
    begin
        SetField('CodigoDiaTipo', value);
    end;

    function TTarifasDiasTipos.GetDescripcion(): string;
    begin
        Result := GetField('Descripcion');
    end;

    procedure TTarifasDiasTipos.SetDescripcion(value: string);
    begin
        SetField('Descripcion', value);
    end;

    function TTarifasDiasTipos.GetCodigoDiaTipoConcesionaria(): Variant;
    begin
        Result := GetField('CodigoDiaTipoConcesionaria');
    end;

    procedure TTarifasDiasTipos.SetCodigoDiaTipoConcesionaria(value: Variant);
    begin
        SetField('CodigoDiaTipoConcesionaria', value);
    end;

    function TTarifasDiasTipos.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TTarifasDiasTipos.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TTarifasDiasTipos.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TTarifasDiasTipos.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TTarifasDiasTipos.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TTarifasDiasTipos.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TTarifasDiasTipos.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TTarifasDiasTipos.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;
{$ENDREGION}

    function TTarifasDiasTipos.Obtener(): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseBO_Rating;
           sp.ProcedureName := 'PlanTarifarioDiasTipos_Obtener';
           sp.Parameters.Refresh;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
{$ENDREGION}

{$REGION 'TPlanTarifarioTarifasPuntosCobro'}
{$REGION 'GETTERS y SETTERS'}
    function TPlanTarifarioTarifasPuntosCobro.GetID_Tarifa(): Integer;
    begin
        Result := GetField('ID_Tarifa');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetID_Tarifa(value: Integer);
    begin
        SetField('ID_Tarifa', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetNumeroPuntoCobro(): Byte;
    begin
        Result := GetField('NumeroPuntoCobro');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetNumeroPuntoCobro(value: Byte);
    begin
        SetField('NumeroPuntoCobro', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetDescripcionPuntoCobro(): string;
    begin
        Result := GetField('DescripcionPuntoCobro');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetDescripcionPuntoCobro(value: string);
    begin
        SetField('DescripcionPuntoCobro', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetCategoria(): Byte;
    begin
        Result := GetField('Categoria');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetCategoria(value: Byte);
    begin
        SetField('Categoria', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetDescripcionCategoria(): string;
    begin
        Result := GetField('DescripcionCategoria');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetDescripcionCategoria(value: string);
    begin
        SetField('DescripcionCategoria', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetCodigoCategoriasClases(): Integer;
    var
        t :Variant;
    begin
        t := GetField('CodigoCategoriasClases');
        if t = null then
           t := 0;
        Result := t;
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetCodigoCategoriasClases(value: Integer);
    begin
        SetField('CodigoCategoriasClases', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetDescripcionCategoriasClases(): string;
    begin
        Result := GetField('DescripcionCategoriasClases');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetDescripcionCategoriasClases(value: string);
    begin
        SetField('DescripcionCategoriasClases', value);
    end;
     
    function TPlanTarifarioTarifasPuntosCobro.GetCodigoTarifaTipo(): string;
    begin
        Result := GetField('CodigoTarifaTipo');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetCodigoTarifaTipo(value: string);
    begin
        SetField('CodigoTarifaTipo', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetDescripcionTarifaTipo(): string;
    begin
        Result := GetField('DescripcionTarifaTipo');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetDescripcionTarifaTipo(value: string);
    begin
        SetField('DescripcionTarifaTipo', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetCodigoDiaTipo(): string;
    begin
        Result := GetField('CodigoDiaTipo');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetCodigoDiaTipo(value: string);
    begin
        SetField('CodigoDiaTipo', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetDescripcionDiaTipo(): string;
    begin
        Result := GetField('DescripcionDiaTipo');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetDescripcionDiaTipo(value: string);
    begin
        SetField('DescripcionDiaTipo', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetID_PlanTarifarioVersion(): LongInt;
    begin
        Result := GetField('ID_PlanTarifarioVersion');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetID_PlanTarifarioVersion(value: LongInt);
    begin
        SetField('ID_PlanTarifarioVersion', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetHoraDesde(): string;
    begin
        Result := GetField('HoraDesde');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetHoraDesde(value: string);
    begin
        SetField('HoraDesde', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetHoraHasta(): string;
    begin
        Result := GetField('HoraHasta');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetHoraHasta(value: string);
    begin
        SetField('HoraHasta', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetImporte(): Double;
    begin
        Result := GetField('Importe');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetImporte(value: Double);
    begin
        SetField('Importe', value);
    end;
    
    function TPlanTarifarioTarifasPuntosCobro.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;

    function TPlanTarifarioTarifasPuntosCobro.GetEstado(): Byte;
    begin
        Result := GetField('Estado');
    end;

    procedure TPlanTarifarioTarifasPuntosCobro.SetEstado(value: Byte);
    begin
        SetField('Estado', value);
    end;

{$ENDREGION}

    function TPlanTarifarioTarifasPuntosCobro.Obtener(_IDPlanTarifarioVersion: LongInt; _NumeroPuntoCobro: Byte = 0; _CodigoCategoria: Byte = 0; _CodigoCategoriasClases: Integer = 0; _CodigoDiaTipo: string = ''): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try

            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifarioTarifasPuntosCobro_Obtener';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _IDPlanTarifarioVersion;
            sp.Parameters.ParamByName('@NumeroPuntoCobro').Value := _NumeroPuntoCobro;
            sp.Parameters.ParamByName('@Categoria').Value := _CodigoCategoria;
            sp.Parameters.ParamByName('@CodigoCategoriasClases').Value := _CodigoCategoriasClases;
            sp.Parameters.ParamByName('@CodigoDiaTipo').Value := IIf(_CodigoDiaTipo = '', null, _CodigoDiaTipo);

            sp.Open;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= CrearClientDataSet(sp, True);
            
        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioTarifasPuntosCobro.Agregar(_IDPlanTarifarioVersion: LongInt; _NumeroPuntoCobro, _CodigoCategoria: Byte; _IdCategoriasClases: Integer; _CodigoTarifaTipo, _CodigoDiaTipo, _HoraDesde, _HoraHasta: string; _Importe: Double; _Usuario: string): Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseBO_Rating;
           sp.ProcedureName := 'PlanTarifarioTarifasPuntosCobro_Agregar';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _IDPlanTarifarioVersion;
           sp.Parameters.ParamByName('@NumeroPuntoCobro').Value := _NumeroPuntoCobro;
           sp.Parameters.ParamByName('@Categoria').Value := _CodigoCategoria;
           sp.Parameters.ParamByName('@CodigoCategoriasClases').Value := _IdCategoriasClases;
           sp.Parameters.ParamByName('@CodigoTarifaTipo').Value := _CodigoTarifaTipo;
           sp.Parameters.ParamByName('@CodigoDiaTipo').Value := _CodigoDiaTipo;
           sp.Parameters.ParamByName('@HoraDesde').Value := _HoraDesde;
           sp.Parameters.ParamByName('@HoraHasta').Value := _HoraHasta;
           sp.Parameters.ParamByName('@Importe').Value := _Importe*1000;
           sp.Parameters.ParamByName('@Usuario').Value := _Usuario;

           sp.ExecProc;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= True;

        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioTarifasPuntosCobro.Actualizar(_IDPlanTarifarioVersion: LongInt; _IDTarifa:Integer; _NumeroPuntoCobro, _CodigoCategoria: Byte; _IdCategoriasClases: Integer; _CodigoTarifaTipo, _CodigoDiaTipo, _HoraDesde, _HoraHasta: string; _Importe: Double; _Usuario: string): Boolean;
    var
        sp: TADOStoredProc;
     begin
        Result := False;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseBO_Rating;
           sp.ProcedureName := 'PlanTarifarioTarifasPuntosCobro_Actualizar';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _IDPlanTarifarioVersion;
           sp.Parameters.ParamByName('@ID_Tarifa').Value := _IDTarifa;                                       
           sp.Parameters.ParamByName('@NumeroPuntoCobro').Value := _NumeroPuntoCobro;
           sp.Parameters.ParamByName('@Categoria').Value := _CodigoCategoria;
           sp.Parameters.ParamByName('@CodigoCategoriasClases').Value := _IdCategoriasClases;
           sp.Parameters.ParamByName('@CodigoTarifaTipo').Value := _CodigoTarifaTipo;
           sp.Parameters.ParamByName('@CodigoDiaTipo').Value := _CodigoDiaTipo;
           sp.Parameters.ParamByName('@HoraDesde').Value := _HoraDesde;
           sp.Parameters.ParamByName('@HoraHasta').Value := _HoraHasta;
           sp.Parameters.ParamByName('@Importe').Value := _Importe*1000;
           sp.Parameters.ParamByName('@Usuario').Value := _Usuario;

           sp.ExecProc;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= True;

        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioTarifasPuntosCobro.Eliminar(_IDTarifa:Integer; _Usuario: string): Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseBO_Rating;
           sp.ProcedureName := 'PlanTarifarioTarifasPuntosCobro_Eliminar';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@ID_Tarifa').Value := _IDTarifa;
           sp.Parameters.ParamByName('@Usuario').Value := _Usuario;

           sp.ExecProc;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= True;

        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioTarifasPuntosCobro.ValidarTarifas(_TarifasPuntosCobros: TPlanTarifarioTarifasPuntosCobro; _IDCategoriasClases: Integer): TClientDataSet;
    var
        sp: TADOStoredProc;
        m: string;
        encontrado : boolean;
    begin
        try
            Result:= TClientDataSet.Create(nil);
            Result.FieldDefs.Add('DescripcionPuntoCobro', ftString, 100);
            Result.FieldDefs.Add('DescripcionCategoria', ftString, 60);
            Result.FieldDefs.Add('DescripcionDiaTipo', ftString, 60);
            Result.FieldDefs.Add('DescripcionTarifaTipo', ftString, 60);
            Result.CreateDataSet;

            _TarifasPuntosCobros.ClientDataSet.DisableControls;
            _TarifasPuntosCobros.ActivarAfterScroll(False);
            m := _TarifasPuntosCobros.ClientDataSet.Bookmark;

            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifario_PosiblesCombinaciones';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@ID_CategoriasClases').Value := _IDCategoriasClases;

            sp.Open;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            sp.First;
            while not sp.Eof do
            begin
                encontrado := False;

                _TarifasPuntosCobros.ClientDataSet.First;
                while not _TarifasPuntosCobros.ClientDataSet.Eof do
                begin

                    if (sp.FieldByName('NumeroPuntoCobro').Value = _TarifasPuntosCobros.NumeroPuntoCobro)
                        and (sp.FieldByName('CodigoCategoria').Value = _TarifasPuntosCobros.Categoria)
                        and (sp.FieldByName('CodigoDiaTipo').Value = _TarifasPuntosCobros.CodigoDiaTipo)
                        and (_TarifasPuntosCobros.HoraDesde = '00:00')
                        and (_TarifasPuntosCobros.HoraHasta = '23:59') then
                    begin
                        encontrado := True;
                        Break;
                    end;

                    _TarifasPuntosCobros.ClientDataSet.Next;
                end;

                if not encontrado then
                begin
                    Result.AppendRecord([_TarifasPuntosCobros.DescripcionPuntoCobro,
                                         _TarifasPuntosCobros.DescripcionCategoria,
                                         _TarifasPuntosCobros.DescripcionDiaTipo,
                                         _TarifasPuntosCobros.DescripcionTarifaTipo]);
                end;

                sp.Next;
            end;

            Result.First;

        finally
            _TarifasPuntosCobros.ClientDataSet.Bookmark := m;
            _TarifasPuntosCobros.ActivarAfterScroll(True);
            _TarifasPuntosCobros.ClientDataSet.EnableControls;
            FreeAndNil(sp);
        end;

    end;
{$ENDREGION}

{$REGION 'TPlanTarifarioVersion'}
{$REGION 'GETTERS y SETTERS'}
    function TPlanTarifarioVersion.GetID_PlanTarifarioVersion(): LongInt;
    begin
        Result := GetField('ID_PlanTarifarioVersion');
    end;

    procedure TPlanTarifarioVersion.SetID_PlanTarifarioVersion(value: LongInt);
    begin
        SetField('ID_PlanTarifarioVersion', value);
    end;

    function TPlanTarifarioVersion.GetCodigoConcesionaria(): Byte;
    begin
        Result := GetField('CodigoConcesionaria');
    end;

    procedure TPlanTarifarioVersion.SetCodigoConcesionaria(value: Byte);
    begin
        SetField('CodigoConcesionaria', value);
    end;

    function TPlanTarifarioVersion.GetConcesionaria(): string;
    begin
        Result := GetField('Concesionaria');
    end;

    procedure TPlanTarifarioVersion.SetConcesionaria(value: string);
    begin
        SetField('Concesionaria', value);
    end;

    function TPlanTarifarioVersion.GetCodigoVersion(): Integer;
    begin
        Result := GetField('CodigoVersion');
    end;

    procedure TPlanTarifarioVersion.SetCodigoVersion(value: Integer);
    begin
        SetField('CodigoVersion', value);
    end;

    function TPlanTarifarioVersion.GetID_CategoriasClases(): Integer;
    begin
        Result := GetField('ID_CategoriasClases');
    end;

    procedure TPlanTarifarioVersion.SetID_CategoriasClases(value: Integer);
    begin
        SetField('ID_CategoriasClases', value);
    end;

    function TPlanTarifarioVersion.GetCategoriaClase(): string;
    begin
        Result := GetField('CategoriaClase');
    end;

    procedure TPlanTarifarioVersion.SetCategoriaClase(value: string);
    begin
        SetField('CategoriaClase', value);
    end;

    function TPlanTarifarioVersion.GetFechaActivacion(): TDateTime;
    begin
        Result := GetField('FechaActivacion');
    end;

    procedure TPlanTarifarioVersion.SetFechaActivacion(value: TDateTime);
    begin
        SetField('FechaActivacion', value);
    end;

    function TPlanTarifarioVersion.GetHabilitado(): Boolean;
    begin
        Result := GetField('Habilitado');
    end;

    procedure TPlanTarifarioVersion.SetHabilitado(value: Boolean);
    begin
        SetField('Habilitado', value);
    end;

    function TPlanTarifarioVersion.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TPlanTarifarioVersion.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TPlanTarifarioVersion.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TPlanTarifarioVersion.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TPlanTarifarioVersion.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TPlanTarifarioVersion.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TPlanTarifarioVersion.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;

    function TPlanTarifarioVersion.GetFechaAutorizacionMOP(): TDateTime;
    var
        t: Variant;
    begin
        t := GetField('AutorizacionMOP');
        if t = null then
            t := NullDate;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetFechaAutorizacionMOP(value: TDateTime);
    begin
        SetField('AutorizacionMOP', value);
    end;

    function TPlanTarifarioVersion.GetFechaHabilitacion(): TDateTime;
    var
        t:Variant;
    begin
        t := GetField('FechaHabilitacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetFechaHabilitacion(value: TDateTime);
    begin
        SetField('FechaHabilitacion', value);
    end;

    function TPlanTarifarioVersion.GetUsuarioHabilitacion1(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioHabilitacion1');
        if t = null then
           t := '';
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetUsuarioHabilitacion1(value: string);
    begin
        SetField('UsuarioHabilitacion1', value);
    end;

    function TPlanTarifarioVersion.GetUsuarioHabilitacion2(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioHabilitacion2');
        if t = null then
           t := '';
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetUsuarioHabilitacion2(value: string);
    begin
        SetField('UsuarioHabilitacion2', value);
    end;

    function TPlanTarifarioVersion.GetUsuarioHabilitacion3(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioHabilitacion3');
        if t = null then
           t := '';
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetUsuarioHabilitacion3(value: string);
    begin
        SetField('UsuarioHabilitacion3', value);
    end;

    function TPlanTarifarioVersion.GetTBFP(): Double;
    var
        t: Variant;
    begin
        t:= IIf(ClientDataSet.FieldByName('TBFP') <> nil, GetField('TBFP'), null);
        if t = null then
           t := 0.00;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetTBFP(value: Double);
    begin     
        SetField('TBFP', value);
    end;

    function TPlanTarifarioVersion.GetTBP(): Double;
    var
        t: Variant;
    begin
        t:= IIf(ClientDataSet.FieldByName('TBP') <> nil, GetField('TBP'), null);
        if t = null then
           t := 0.00;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetTBP(value: Double);
    begin
        SetField('TBP', value);
    end;

    function TPlanTarifarioVersion.GetTBI(): Double;
    var
        t: Variant;
    begin
        t:= IIf(ClientDataSet.FieldByName('TBI') <> nil, GetField('TBI'), null);
        if t = null then
           t := 0.00;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetTBI(value: Double);
    begin
        SetField('TBI', value);
    end;

    function TPlanTarifarioVersion.GetTS(): Double;
    var
        t: Variant;
    begin
        t:= IIf(ClientDataSet.FieldByName('TS') <> nil, GetField('TS'), null);
        if t = null then
           t := 0.00;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetTS(value: Double);
    begin
        SetField('TS', value);
    end;

    function TPlanTarifarioVersion.GetIPC(): Double;
    var
        t: Variant;
    begin
        t:= IIf(ClientDataSet.FieldByName('IPC') <> nil, GetField('IPC'), null);
        if t = null then
           t := 0.00;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetIPC(value: Double);
    begin
        SetField('IPC', value);
    end;

{$ENDREGION}

    function TPlanTarifarioVersion.Obtener(_CodigoConcesionaria: Byte = 0; _IDCategoriasClases: Integer = 0): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try
            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifarioVersiones_Obtener';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoConcesionaria').Value := _CodigoConcesionaria;
            sp.Parameters.ParamByName('@ID_CategoriasClases').Value := _IDCategoriasClases;

            sp.Open;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= CrearClientDataSet(sp); 
        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioVersion.Agregar(_CodigoConcesionaria: Byte; _IDCategoriasClases: Integer; _FechaActivacion: TDateTime; _TarifasPuntosCobros: TPlanTarifarioTarifasPuntosCobro; _Usuario: string): LongInt;
    var
        sp: TADOStoredProc;
    begin
        Result := 0;
        try
            try
                DMConnections.BaseBO_Rating.BeginTrans;

                sp := TADOStoredProc.Create(nil);
                sp.Connection := DMConnections.BaseBO_Rating;
                sp.ProcedureName := 'PlanTarifarioVersiones_Agregar';
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@CodigoConcesionaria').Value := _CodigoConcesionaria;
                sp.Parameters.ParamByName('@ID_CategoriasClases').Value := _IDCategoriasClases;
                sp.Parameters.ParamByName('@FechaActivacion').Value := _FechaActivacion;
                sp.Parameters.ParamByName('@UsuarioCreacion').Value := _Usuario;

                sp.ExecProc;

                if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                   raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

                Result:= sp.Parameters.ParamByName('@ID_INT').Value;

                ActualizarTarifasPuntosCobro(Result, _TarifasPuntosCobros);

                DMConnections.BaseBO_Rating.CommitTrans;
                
            except
                on e: Exception do
                begin
                    DMConnections.BaseBO_Rating.RollbackTrans;
                    raise Exception.Create(e.Message);
                end;
            end;
        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioVersion.Actualizar(_IDPlanTarifarioVersion: LongInt; _FechaActivacion: TDateTime; _TarifasPuntosCobros: TPlanTarifarioTarifasPuntosCobro; _Usuario: string): Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try
            try
                DMConnections.BaseBO_Rating.BeginTrans;

                sp := TADOStoredProc.Create(nil);
                sp.Connection := DMConnections.BaseBO_Rating;
                sp.ProcedureName := 'PlanTarifarioVersiones_Actualizar';
                sp.Parameters.Refresh;
                sp.Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _IDPlanTarifarioVersion;
                sp.Parameters.ParamByName('@FechaActivacion').Value := _FechaActivacion;
                sp.Parameters.ParamByName('@Usuario').Value := _Usuario;

                sp.ExecProc;

                if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                    raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

                ActualizarTarifasPuntosCobro(_IDPlanTarifarioVersion, _TarifasPuntosCobros);

                DMConnections.BaseBO_Rating.CommitTrans;

                Result:= True;

            except
                on e: Exception do
                begin
                    DMConnections.BaseBO_Rating.RollbackTrans;
                    raise Exception.Create(e.Message);
                end;
            end;
        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioVersion.Eliminar(_IDPlanTarifarioVersion: LongInt; _Usuario: string): Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try
            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifarioVersiones_Eliminar';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _IDPlanTarifarioVersion;
            sp.Parameters.ParamByName('@Usuario').Value := _Usuario;

            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= True;
        finally
            FreeAndNil(sp);
        end;

    end;

    procedure TPlanTarifarioVersion.ActualizarTarifasPuntosCobro(_IDPlanTarifarioVersion: LongInt; _TarifasPuntosCobro: TPlanTarifarioTarifasPuntosCobro);
    begin
        if _TarifasPuntosCobro <> nil then
        begin
            _TarifasPuntosCobro.ClientDataSet.Filtered := False;
            _TarifasPuntosCobro.ClientDataSet.First;
            while not _TarifasPuntosCobro.ClientDataSet.Eof do
            begin

                if _TarifasPuntosCobro.Estado = 1 then
                begin
                   oTarifas.Agregar(_IDPlanTarifarioVersion,
                                                _TarifasPuntosCobro.NumeroPuntoCobro,
                                                _TarifasPuntosCobro.Categoria,
                                                _TarifasPuntosCobro.CodigoCategoriasClases,
                                                _TarifasPuntosCobro.CodigoTarifaTipo,
                                                _TarifasPuntosCobro.CodigoDiaTipo,
                                                _TarifasPuntosCobro.HoraDesde,
                                                _TarifasPuntosCobro.HoraHasta,
                                                _TarifasPuntosCobro.Importe,
                                                UsuarioSistema);
                end
                else if _TarifasPuntosCobro.Estado = 2 then
                begin
                    oTarifas.Actualizar(
                                                _IDPlanTarifarioVersion,
                                                _TarifasPuntosCobro.ID_Tarifa,
                                                _TarifasPuntosCobro.NumeroPuntoCobro,
                                                _TarifasPuntosCobro.Categoria,
                                                _TarifasPuntosCobro.CodigoCategoriasClases,
                                                _TarifasPuntosCobro.CodigoTarifaTipo,
                                                _TarifasPuntosCobro.CodigoDiaTipo,
                                                _TarifasPuntosCobro.HoraDesde,
                                                _TarifasPuntosCobro.HoraHasta,
                                                _TarifasPuntosCobro.Importe,
                                                UsuarioSistema);
                end
                else if _TarifasPuntosCobro.Estado = 3 then
                begin
                     oTarifas.Eliminar(
                                                _TarifasPuntosCobro.ID_Tarifa,
                                                UsuarioSistema);
                end;

                _TarifasPuntosCobro.ClientDataSet.Next;
            end;
            _TarifasPuntosCobro.ClientDataSet.Filtered := True;
        end;
    end;

    function TPlanTarifarioVersion.HabilitarPlan(_IDPlanTarifarioVersion: LongInt; _Usuario: string; _Habilitar: Boolean): Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try
            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifarioVersiones_Habilitar';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _IDPlanTarifarioVersion;
            sp.Parameters.ParamByName('@Usuario').Value := _Usuario;
            sp.Parameters.ParamByName('@Habilitar').Value := _Habilitar;

            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= True;
        finally
            FreeAndNil(sp);
        end;

    end;

    function TPlanTarifarioVersion.ActualizarFechaMOP(_IDPlanTarifarioVersion:LongInt; _FechaMOP:TDateTime; _Usuario: string):Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try
            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'PlanTarifarioVersiones_ActualizarFechaMOP';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _IDPlanTarifarioVersion;
            sp.Parameters.ParamByName('@Usuario').Value := _Usuario;
            sp.Parameters.ParamByName('@FechaMOP').Value := _FechaMOP;

            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= True;
        finally
            FreeAndNil(sp);
        end;

    end;

    destructor TPlanTarifarioVersion.Destroy();
    begin
        FreeAndNil(oTarifas);
        if Assigned(ClientDataSet) then
            FreeAndNil(ClientDataSet);
    end;

{$ENDREGION}

end.
