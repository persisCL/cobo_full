unit Dynac;

interface

uses ClaseBase, DB, ADODB, SysUtils, Variants, Classes, DBClient, Provider, Util;

type
    TTollRates = Class(TClaseBase)
    private
        function GetID(): Integer;
        function GetID_PlanTarifarioVersion(): Integer;
        function GetAnio(): Integer;
        function GetNumeroPuntoCobro(): Byte;
        function GetFechaDesde(): TDateTime;
        function GetFechaHasta(): TDateTime;
        function GetDescripcionDiaTipo(): string;
        function GetHoraDesde(): TDateTime;
        function GetHoraHasta(): TDateTime;
        function GetDescripcionCategoriaClase(): string;
        function GetDescripcionTarifaTipo(): string;
        function GetDescripcionCategoria(): string;
        function GetImporte(): Double;
        function GetEnviar(): Boolean;

    public
        property ID: Integer read GetID;
        property ID_PlanTarifarioVersion: Integer read GetID_PlanTarifarioVersion;
        property Anio: Integer read GetAnio;
        property NumeroPuntoCobro: Byte read GetNumeroPuntoCobro;
        property FechaDesde: TDateTime read GetFechaDesde;
        property FechaHasta: TDateTime read GetFechaHasta;
        property DescripcionDiaTipo: string read GetDescripcionDiaTipo;
        property HoraDesde: TDateTime read GetHoraDesde;
        property HoraHasta: TDateTime read GetHoraHasta;
        property DescripcionCategoriaClase: string read GetDescripcionCategoriaClase;
        property DescripcionTarifaTipo: string read GetDescripcionTarifaTipo;
        property DescripcionCategoria: string read GetDescripcionCategoria;
        property Importe: Double read GetImporte;
        property Enviar: Boolean read GetEnviar;


        function CrearTarifasDynac(FechaDesde, FechaHasta: TDateTime; Usuario: string; var MensajeError: string; Conexion: TADOConnection): TClientDataSet;
        function MarcarParaEnviar(Conexion: TADOConnection): Boolean;
        function ObtenerDisponiblesEnvio(NumeroPuntoCobro: Byte; Usuario: String; Conexion: TADOConnection): TClientDataSet;
    end;

type
    TTollRatesControl = Class(TClaseBase)
    private
        function GetID(): Integer;
        procedure SetID(value: Integer);

        function GetAnio(): Integer;
        procedure SetAnio(value: Integer);

        function GetNumeroPuntoCobro(): Byte;
        procedure SetNumeroPuntoCobro(value: Byte);

        function GetActualizado(): Boolean;
        procedure SetActualizado(value: Boolean);

        function GetEnviado(): Boolean;
        procedure SetEnviado(value: Boolean);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);


    public
        property ID: Integer read GetID write SetID;
        property Anio: Integer read GetAnio write SetAnio;
        property NumeroPuntoCobro: Byte read GetNumeroPuntoCobro write SetNumeroPuntoCobro;
        property Actualizado: Boolean read GetActualizado write SetActualizado;
        property Enviado: Boolean read GetEnviado write SetEnviado;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function ObtenerPuntosAEnviar(Usuario: string; Conexion: TADOConnection): TClientDataSet;
        function ActualizarEnviados(Usuario: string; Conexion: TADOConnection): Boolean;
    end;

type
    TTransits = Class(TClaseBase)
    private
        function GetBunchID(): Int64;
        function GetNumCorrCO(): Int64;
        function GetPatenteDetectada(): WideString;
        function GetTAGNumeroSerie(): Int64;
        function GetFechaHora(): TDateTime;
        function GetNumeroPuntoCobro(): Integer;
        function GetEnviado(): Boolean;
        function GetID_LOG(): Int64;

    public
        property BunchID: Int64 read GetBunchID;
        property NumCorrCO: Int64 read GetNumCorrCO;
        property PatenteDetectada: WideString read GetPatenteDetectada;
        property TAGNumeroSerie: Int64 read GetTAGNumeroSerie;
        property FechaHora: TDateTime read GetFechaHora;
        property NumeroPuntoCobro: Integer read GetNumeroPuntoCobro;
        property Enviado: Boolean read GetEnviado;
        property ID_LOG: Int64 read GetID_LOG;

        function Transitos_AEnviar_SELECT(CantidadTransitosAEnviar: Integer; Conexion: TADOConnection): TClientDataSet;
        function Transitos_MarcarEnviados(BunchID: Int64; _Transitos: TDataSet; Conexion: TADOConnection): Boolean;
    end;

implementation


{$REGION 'TTollRates'}
{$REGION 'GETTERS y SETTERS'}

function TTollRates.GetID(): Integer;
begin
    Result:= GetField('ID');
end;

function TTollRates.GetID_PlanTarifarioVersion(): Integer;
begin
    Result:= GetField('ID_PlanTarifarioVersion');
end;

function TTollRates.GetAnio(): Integer;
begin
    Result:= GetField('Anio');
end;

function TTollRates.GetNumeroPuntoCobro(): Byte;
begin
    Result:= GetField('NumeroPuntoCobro');
end;

function TTollRates.GetFechaDesde(): TDateTime;
begin
    Result:= GetField('FechaDesde');
end;

function TTollRates.GetFechaHasta(): TDateTime;
begin
    Result:= GetField('FechaHasta');
end;

function TTollRates.GetDescripcionDiaTipo(): string;
begin
    Result:= GetField('DescripcionDiaTipo');
end;

function TTollRates.GetHoraDesde(): TDateTime;
begin
    Result:= StrToDateTime(GetField('HoraDesde'));
end;

function TTollRates.GetHoraHasta(): TDateTime;
begin
    Result:= StrToDateTime(GetField('HoraHasta'));
end;

function TTollRates.GetDescripcionCategoriaClase(): string;
begin
    Result:= GetField('DescripcionCategoriaClase');
end;

function TTollRates.GetDescripcionTarifaTipo(): string;
begin
    Result:= GetField('DescripcionTarifaTipo');
end;

function TTollRates.GetDescripcionCategoria(): string;
begin
    Result:= GetField('DescripcionCategoria');
end;

function TTollRates.GetImporte(): Double;
begin
    Result:= GetField('Importe');
end;

function TTollRates.GetEnviar(): Boolean;
begin
    Result:= GetField('Enviar');
end;

{$ENDREGION}

    function TTollRates.CrearTarifasDynac(FechaDesde, FechaHasta: TDateTime; Usuario: string; var MensajeError: string; Conexion: TADOConnection): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try

            if FechaDesde > FechaHasta then
                raise Exception.Create('FechaHasta no puede ser menor a FechaDesde');

           sp := TADOStoredProc.Create(nil);
           sp.Connection := Conexion;
           sp.ProcedureName := 'PlanTarifarioDynacTarifasAEnviar_CrearTarifasDynac';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@FechaDesde').Value := FechaDesde;
           sp.Parameters.ParamByName('@FechaHasta').Value := FechaHasta;
           sp.Parameters.ParamByName('@Usuario').Value := Usuario;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= CrearClientDataSet(sp);

           MensajeError := sp.Parameters.ParamByName('@MensajeError').Value;

        finally
            FreeAndNil(sp);
        end;

    end;

    function TTollRates.MarcarParaEnviar(Conexion: TADOConnection): Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try

            sp := TADOStoredProc.Create(nil);
            sp.Connection := Conexion;
            sp.ProcedureName := 'PlanTarifarioDynacTarifasAEnviar_Enviar';
            sp.Parameters.Refresh;

            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result := True;

        finally
            FreeAndNil(sp);
        end;

    end;

    function TTollRates.ObtenerDisponiblesEnvio(NumeroPuntoCobro: Byte; Usuario: String; Conexion: TADOConnection): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := Conexion;
           sp.ProcedureName := 'Dynac_TollRates_SELECT';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@PorEnviar').Value := 1;
           sp.Parameters.ParamByName('@NumeroPuntoCobro').Value := NumeroPuntoCobro;
           sp.Parameters.ParamByName('@Usuario').Value := Usuario;
                                    
           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;

{$ENDREGION}

{$REGION 'TTollRatesControl'}
{$REGION 'GETTERS y SETTERS'}

function TTollRatesControl.GetID(): Integer;
var
    t: Variant;
begin
    t := GetField('ID');
    if t = null then
       t := 0;
    Result := t;
end;

procedure TTollRatesControl.SetID(value: Integer);
begin
    SetField('ID', value);
end;

function TTollRatesControl.GetAnio(): Integer;
begin
    Result := GetField('Anio');
end;

procedure TTollRatesControl.SetAnio(value: Integer);
begin
    SetField('Anio', value);
end;

function TTollRatesControl.GetNumeroPuntoCobro(): Byte;
begin
    Result := GetField('NumeroPuntoCobro');
end;

procedure TTollRatesControl.SetNumeroPuntoCobro(value: Byte);
begin
    SetField('NumeroPuntoCobro', value);
end;

function TTollRatesControl.GetActualizado(): Boolean;
var
    t: Variant;
begin
    t := GetField('Actualizado');
    if t = null then
       t := False;
    Result := t;
end;

procedure TTollRatesControl.SetActualizado(value: Boolean);
begin
    SetField('Actualizado', value);
end;

function TTollRatesControl.GetEnviado(): Boolean;
var
    t: Variant;
begin
    t := GetField('Enviado');
    if t = null then
       t := False;
    Result := t;
end;

procedure TTollRatesControl.SetEnviado(value: Boolean);
begin
    SetField('Enviado', value);
end;

function TTollRatesControl.GetUsuarioCreacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioCreacion');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TTollRatesControl.SetUsuarioCreacion(value: string);
begin
    SetField('UsuarioCreacion', value);
end;

function TTollRatesControl.GetFechaHoraCreacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraCreacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TTollRatesControl.SetFechaHoraCreacion(value: TDateTime);
begin
    SetField('FechaHoraCreacion', value);
end;

function TTollRatesControl.GetUsuarioModificacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioModificacion');
    if t = null then
       t := '';
    Result := t;
end;

procedure TTollRatesControl.SetUsuarioModificacion(value: string);
begin
    SetField('UsuarioModificacion', value);
end;

function TTollRatesControl.GetFechaHoraModificacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraModificacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TTollRatesControl.SetFechaHoraModificacion(value: TDateTime);
begin
    SetField('FechaHoraModificacion', value);
end;

{$ENDREGION}

    function TTollRatesControl.ObtenerPuntosAEnviar(Usuario: string; Conexion: TADOConnection): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := Conexion;
           sp.ProcedureName := 'Dynac_PuntosAEnviar';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@Usuario'). Value := Usuario;
         
           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;

    function TTollRatesControl.ActualizarEnviados(Usuario:string; Conexion: TADOConnection): Boolean;
    var
        sp: TADOStoredProc;
    begin
        Result := False;
        try

            sp := TADOStoredProc.Create(nil);
            sp.Connection := Conexion;
            sp.ProcedureName := 'Dynac_TollRates_ActualizarEnviados';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@Usuario').Value := Usuario;
                                       
            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result := True;

        finally
            FreeAndNil(sp);
        end;

    end;

{$ENDREGION}

{$REGION 'TTransits'}
{$REGION 'GETTERS y SETTERS'}

function TTransits.GetBunchID(): Int64;
begin
    Result:= GetField('BunchID');
end;

function TTransits.GetNumCorrCO(): Int64;
begin
    Result:= GetField('NumCorrCO');
end;

function TTransits.GetPatenteDetectada(): WideString;
begin
    Result:= GetField('PatenteDetectada');
end;

function TTransits.GetTAGNumeroSerie(): Int64;
begin
    Result:= GetField('TAGNumeroSerie');
end;

function TTransits.GetNumeroPuntoCobro(): Integer;
begin
    Result:= GetField('NumeroPuntoCobro');
end;

function TTransits.GetFechaHora(): TDateTime;
begin
    Result:= GetField('FechaHora');
end;

function TTransits.GetEnviado(): Boolean;
begin
    Result:= GetField('Enviado');
end;

function TTransits.GetID_LOG(): Int64;
begin
    Result:= GetField('ID_LOG');
end;

{$ENDREGION}

    function TTransits.Transitos_AEnviar_SELECT(CantidadTransitosAEnviar: Integer; Conexion: TADOConnection): TClientDataSet;
    var
        sp: TADOStoredProc;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := Conexion;
           sp.ProcedureName := 'Transitos_AEnviar_SELECT';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@Cantidad').Value := CantidadTransitosAEnviar;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;

    function TTransits.Transitos_MarcarEnviados(BunchID: Int64; _Transitos: TDataSet; Conexion: TADOConnection): Boolean;
    var
        sp: TADOStoredProc;
        NumCorrelativos : string;
    begin
        Result := False;
        try

            if NumCorrelativos <> '' then
                NumCorrelativos := NumCorrelativos + ';';
            NumCorrelativos := NumCorrelativos + IntToStr(_Transitos.FieldByName('NumCorrCO').Value);

            sp := TADOStoredProc.Create(nil);
            sp.Connection := Conexion;
            sp.ProcedureName := 'Transitos_MarcarEnviados';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@BunchID').Value := BunchID;
            sp.Parameters.ParamByName('@NumCorrelativos').Value := NumCorrelativos;

            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result := True;

        finally
            FreeAndNil(sp);
        end;

    end;

{$ENDREGION}

end.
