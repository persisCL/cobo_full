//TASK_125_JMA_20170404
unit RegistroDineroEntregadoEmpresaTransporte;

interface
uses ClaseBase, DB, ADODB, DBClient, SysUtils, DMConnection, Variants, Util;

type TRegistroDineroTransporte= class(TClaseBase)

    private
        function GetID(): Integer;
        procedure SetID(Valor:Integer);

        function GetFecha(): TDateTime;
        procedure SetFecha(Valor:TDateTime);

        function GetCodigoBanco(): Integer;
        procedure SetCodigoBanco(Valor:Integer);

        function GetDescripcionBanco(): string;

        function GetFolio(): string;
        procedure SetFolio(Valor:string);

        function GetNumCuentaCorriente(): string;
        procedure SetNumCuentaCorriente(Valor:string);

        function GetMonto(): Currency;
        procedure SetMonto(Valor:Currency);

        function GetEmpresaTransporteValores(): string;
        procedure SetEmpresaTransporteValores(Valor:string);

        function GetRecibidoPorBanco(): Boolean;
        procedure SetRecibidoPorBanco(Valor:Boolean);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property ID: Integer read GetID write SetID;
        property Fecha: TDateTime read GetFecha write SetFecha;
        property CodigoBanco: Integer read GetCodigoBanco write SetCodigoBanco;
        property DescripcionBanco: string read GetDescripcionBanco;
        property Folio: string read GetFolio write SetFolio;
        property NumCuentaCorriente: string read GetNumCuentaCorriente write SetNumCuentaCorriente;
        property Monto: Currency read GetMonto write SetMonto;
        property EmpresaTransporteValores: string read GetEmpresaTransporteValores write SetEmpresaTransporteValores;
        property RecibidoPorBanco: Boolean read GetRecibidoPorBanco write SetRecibidoPorBanco;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(RecibidoPorBanco: Boolean; ID: Integer = 0): TClientDataSet;
        function Agregar(Fecha:TDateTime; CodigoBanco: Integer; Folio, NumCuentaCorriente: String; Monto: Currency; EmpresaTransporteValores: string; CodigoPuntoVenta: Integer; Usuario: string): Integer;
        function Modificar(Fecha:TDateTime; CodigoBanco: Integer; Folio, NumCuentaCorriente: String; Monto: Currency; EmpresaTransporteValores: string; CodigoPuntoVenta: Integer; RecibidoPorBanco: Boolean; Usuario: string; ID: Integer): Boolean;
        function Eliminar(ID: Integer; Usuario: string): Boolean;

end;
 
implementation
{$REGION 'GETTERS y SETTERS}
function TRegistroDineroTransporte.GetID(): Integer;
begin
    Result := GetField('ID');
end;

procedure TRegistroDineroTransporte.SetID(Valor:Integer);
begin
    SetField('ID', Valor);
end;

function TRegistroDineroTransporte.GetFecha(): TDateTime;
begin
    Result := GetField('Fecha');
end;

procedure TRegistroDineroTransporte.SetFecha(Valor:TDateTime);
begin
    SetField('Fecha', Valor);
end;

function TRegistroDineroTransporte.GetCodigoBanco(): Integer;
begin
    Result := GetField('CodigoBanco');
end;

procedure TRegistroDineroTransporte.SetCodigoBanco(Valor:Integer);
begin
    SetField('CodigoBanco', Valor);
end;

function TRegistroDineroTransporte.GetDescripcionBanco(): string;
begin
    Result := GetField('DescripcionBanco');
end;

function TRegistroDineroTransporte.GetFolio(): string;
begin
    Result := GetField('Folio');
end;

procedure TRegistroDineroTransporte.SetFolio(Valor:string);
begin
    SetField('Folio', Valor);
end;

function TRegistroDineroTransporte.GetNumCuentaCorriente(): string;
begin
    Result := GetField('NumCuentaCorriente');
end;

procedure TRegistroDineroTransporte.SetNumCuentaCorriente(Valor:string);
begin
    SetField('NumCuentaCorriente', Valor);
end;

function TRegistroDineroTransporte.GetMonto(): Currency;
begin
    Result := GetField('Monto');
end;

procedure TRegistroDineroTransporte.SetMonto(Valor:Currency);
begin
    SetField('Monto', Valor);
end;

function TRegistroDineroTransporte.GetEmpresaTransporteValores(): string;
begin
    Result := GetField('EmpresaTransporteValores');
end;

procedure TRegistroDineroTransporte.SetEmpresaTransporteValores(Valor:string);
begin
    SetField('EmpresaTransporteValores', Valor);
end;

function TRegistroDineroTransporte.GetRecibidoPorBanco(): Boolean;
begin
    Result := GetField('RecibidoPorBanco');
end;

procedure TRegistroDineroTransporte.SetRecibidoPorBanco(Valor:Boolean);
begin
    SetField('RecibidoPorBanco', Valor);
end;

function TRegistroDineroTransporte.GetUsuarioCreacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioCreacion');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TRegistroDineroTransporte.SetUsuarioCreacion(value: string);
begin
    SetField('UsuarioCreacion', value);
end;

function TRegistroDineroTransporte.GetFechaHoraCreacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraCreacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TRegistroDineroTransporte.SetFechaHoraCreacion(value: TDateTime);
begin
    SetField('FechaHoraCreacion', value);
end;

function TRegistroDineroTransporte.GetUsuarioModificacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioModificacion');
    if t = null then
       t := '';
    Result := GetField('UsuarioModificacion');
end;

procedure TRegistroDineroTransporte.SetUsuarioModificacion(value: string);
begin
    SetField('UsuarioModificacion', value);
end;

function TRegistroDineroTransporte.GetFechaHoraModificacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraModificacion');
    if t = null then
       t := NullDate;
    Result := GetField('FechaHoraModificacion');
end;

procedure TRegistroDineroTransporte.SetFechaHoraModificacion(value: TDateTime);
begin
    SetField('FechaHoraModificacion', value);
end;

{$ENDREGION}

function TRegistroDineroTransporte.Obtener(RecibidoPorBanco: Boolean; ID: Integer = 0): TClientDataSet;
var
    sp: TADOStoredProc;
begin
    Result:= nil;
    try
        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'RegistroDineroEntregadoEmpTransporte_SELECT';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@RecibidoPorBanco').Value := RecibidoPorBanco;
        if ID <> 0 then
            sp.Parameters.ParamByName('@ID').Value := ID;

        sp.Open;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result := CrearClientDataSet(sp);

    finally
        FreeAndNil(sp);
    end;

end;

function TRegistroDineroTransporte.Agregar(Fecha:TDateTime; CodigoBanco: Integer; Folio, NumCuentaCorriente: String; Monto: Currency; EmpresaTransporteValores: string; CodigoPuntoVenta: Integer; Usuario: string): Integer;
var
    sp: TADOStoredProc;
begin

    try
        Result := 0;      

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'RegistroDineroEntregadoEmpTransporte_Agregar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Fecha').Value := Fecha;
        sp.Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
        sp.Parameters.ParamByName('@Folio').Value := Folio;
        sp.Parameters.ParamByName('@NumCuentaCorriente').Value := NumCuentaCorriente;
        sp.Parameters.ParamByName('@Monto').Value := Monto * 100;
        sp.Parameters.ParamByName('@EmpresaTransporteValores').Value := EmpresaTransporteValores;
        sp.Parameters.ParamByName('@CodigoPuntoVenta').Value := CodigoPuntoVenta;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result := sp.Parameters.ParamByName('@ID').Value;
    finally
        FreeAndNil(sp);
    end;

end;

function TRegistroDineroTransporte.Modificar(Fecha:TDateTime; CodigoBanco: Integer; Folio, NumCuentaCorriente: String; Monto: Currency; EmpresaTransporteValores: string; CodigoPuntoVenta: Integer; RecibidoPorBanco: Boolean; Usuario: string; ID: Integer): Boolean;
var
    sp: TADOStoredProc;
    ex: Exception;
begin

    try
        Result := false;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'RegistroDineroEntregadoEmpTransporte_Modificar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Fecha').Value := Fecha;
        sp.Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
        sp.Parameters.ParamByName('@Folio').Value := Folio;
        sp.Parameters.ParamByName('@NumCuentaCorriente').Value := NumCuentaCorriente;
        sp.Parameters.ParamByName('@Monto').Value := Monto * 100;
        sp.Parameters.ParamByName('@EmpresaTransporteValores').Value := EmpresaTransporteValores;
        sp.Parameters.ParamByName('@CodigoPuntoVenta').Value := CodigoPuntoVenta;
        sp.Parameters.ParamByName('@RecibidoPorBanco').Value := RecibidoPorBanco;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.Parameters.ParamByName('@ID').Value := ID;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result := True;
    finally
        FreeAndNil(sp);
    end;

end;

function TRegistroDineroTransporte.Eliminar(ID: Integer; Usuario: string): Boolean;
var
    sp: TADOStoredProc;
begin

    try
        Result := false;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'RegistroDineroEntregadoEmpTransporte_Eliminar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@ID').Value := ID;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result := True;
    finally
        FreeAndNil(sp);
    end;

end;

end.
