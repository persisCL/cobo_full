//TASK_109_JMA_20170215
unit BandasHorarias;

interface
uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;


type
    TEsquema = Class(TClaseBase)

    private
        function GetCodigoEsquema(): Integer;
        procedure SetCodigoEsquema(value: Integer);

        function GetDescripcion(): string;
        procedure SetDescripcion(value: string);

        function GetCompleto(): Boolean;
        procedure SetCompleto(value: Boolean);

        function GetID_CategoriasClases(): Integer;
        procedure SetID_CategoriasClases(value: Integer);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property CodigoEsquema: Integer read GetCodigoEsquema write SetCodigoEsquema;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property Completo: Boolean read GetCompleto write SetCompleto;
        property ID_CategoriasClases: Integer read GetID_CategoriasClases write SetID_CategoriasClases;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(ID_CategoriasClases: Integer = 0; _Completo: Boolean = True; _Descripcion: string = ''): TClientDataSet;

    end;

type
    TBandaHoraria = Class(TClaseBase)

    private
        function GetCodigoBandaHoraria(): Integer;
        procedure SetCodigoBandaHoraria(value: Integer);

        function GetCodigoEsquema(): Integer;
        procedure SetCodigoEsquema(value: Integer);

        function GetCodigoDiaTipo(): string;
        procedure SetCodigoDiaTipo(value: string);

        function GetTipoDiaDescripcion(): string;
        procedure SetTipoDiaDescripcion(value: string);

        function GetHoraDesde(): string;
        procedure SetHoraDesde(value: string);

        function GetHoraHasta(): string;
        procedure SetHoraHasta(value: string);

        function GetCodigoTarifaTipo(): string;
        procedure SetCodigoTarifaTipo(value: string);

        function GetTipoTarifaDescripcion(): string;
        procedure SetTipoTarifaDescripcion(value: string);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);


    public
        property CodigoBandaHoraria: Integer read GetCodigoBandaHoraria write SetCodigoBandaHoraria;
        property CodigoEsquema: Integer read GetCodigoEsquema write SetCodigoEsquema;
        property CodigoDiaTipo: string read GetCodigoDiaTipo write SetCodigoDiaTipo;
        property TipoDiaDescripcion: string read GetTipoDiaDescripcion write SetTipoDiaDescripcion;
        property HoraDesde: string read GetHoraDesde write SetHoraDesde;
        property HoraHasta: string read GetHoraHasta write SetHoraHasta;
        property CodigoTarifaTipo: string read GetCodigoTarifaTipo write SetCodigoTarifaTipo;
        property TipoTarifaDescripcion: string read GetTipoTarifaDescripcion write SetTipoTarifaDescripcion;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(_CodigoEsquema: Integer): TClientDataSet;

    end;

implementation


{$REGION 'TEsquema'}
{$REGION 'GETTERS y SETTERS'}
    function TEsquema.GetCodigoEsquema(): Integer;
    begin
        Result := GetField('CodigoEsquema');
    end;

    procedure TEsquema.SetCodigoEsquema(value: Integer);
    begin
        SetField('CodigoEsquema', value);
    end;

    function TEsquema.GetDescripcion(): string;
    begin
        Result := GetField('Descripcion');
    end;

    procedure TEsquema.SetDescripcion(value: string);
    begin
        SetField('Descripcion', value);
    end;

    function TEsquema.GetCompleto(): Boolean;
    begin
        Result := GetField('Completo');
    end;

    procedure TEsquema.SetCompleto(value: Boolean);
    begin
        SetField('Completo', value);
    end;

    function TEsquema.GetID_CategoriasClases(): Integer;
    begin
        Result := GetField('ID_CategoriasClases');
    end;

    procedure TEsquema.SetID_CategoriasClases(value: Integer);
    begin
        SetField('ID_CategoriasClases', value);
    end;

    function TEsquema.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TEsquema.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TEsquema.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TEsquema.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TEsquema.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TEsquema.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TEsquema.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TEsquema.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;

{$ENDREGION}

    function TEsquema.Obtener(ID_CategoriasClases: Integer = 0; _Completo: Boolean = True; _Descripcion: string = ''): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'ADM_Esquemas_SELECT';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@Descripcion').Value := IIf(ID_CategoriasClases = 0, null, ID_CategoriasClases);
            sp.Parameters.ParamByName('@Descripcion').Value := IIf(_Descripcion = '', null, _Descripcion);
            sp.Parameters.ParamByName('@Completo').Value := _Completo;

            sp.Open;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= CrearClientDataSet(sp, True);
            
        finally
            FreeAndNil(sp);
        end;

    end;

{$ENDREGION}

{$REGION 'TBandaHoraria'}
{$REGION 'GETTERS y SETTERS'}
    function TBandaHoraria.GetCodigoBandaHoraria(): Integer;
    begin
        Result := GetField('CodigoBandaHoraria');
    end;

    procedure TBandaHoraria.SetCodigoBandaHoraria(value: Integer);
    begin
        SetField('CodigoBandaHoraria', value);
    end;

    function TBandaHoraria.GetCodigoEsquema(): Integer;
    begin
        Result := GetField('CodigoEsquema');
    end;

    procedure TBandaHoraria.SetCodigoEsquema(value: Integer);
    begin
        SetField('CodigoEsquema', value);
    end;

    function TBandaHoraria.GetCodigoDiaTipo(): string;
    begin
        Result := GetField('CodigoDiaTipo');
    end;

    procedure TBandaHoraria.SetCodigoDiaTipo(value: string);
    begin
        SetField('CodigoDiaTipo', value);
    end;

    function TBandaHoraria.GetTipoDiaDescripcion(): string;
    begin
        Result := GetField('TipoDiaDescripcion');
    end;

    procedure TBandaHoraria.SetTipoDiaDescripcion(value: string);
    begin
        SetField('TipoDiaDescripcion', value);
    end;

    function TBandaHoraria.GetHoraDesde(): string;
    begin
        Result := GetField('HoraDesde');
    end;

    procedure TBandaHoraria.SetHoraDesde(value: string);
    begin
        SetField('HoraDesde', value);
    end;

    function TBandaHoraria.GetHoraHasta(): string;
    begin
        Result := GetField('HoraHasta');
    end;

    procedure TBandaHoraria.SetHoraHasta(value: string);
    begin
        SetField('HoraHasta', value);
    end;

    function TBandaHoraria.GetCodigoTarifaTipo(): string;
    begin
        Result := GetField('CodigoTarifaTipo');
    end;

    procedure TBandaHoraria.SetCodigoTarifaTipo(value: string);
    begin
        SetField('CodigoTarifaTipo', value);
    end;

    function TBandaHoraria.GetTipoTarifaDescripcion(): string;
    begin
        Result := GetField('TipoTarifaDescripcion');
    end;

    procedure TBandaHoraria.SetTipoTarifaDescripcion(value: string);
    begin
        SetField('TipoTarifaDescripcion', value);
    end;

    function TBandaHoraria.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TBandaHoraria.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TBandaHoraria.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TBandaHoraria.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TBandaHoraria.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TBandaHoraria.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TBandaHoraria.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TBandaHoraria.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;

{$ENDREGION}

    function TBandaHoraria.Obtener(_CodigoEsquema: Integer): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

            sp := TADOStoredProc.Create(nil);
            sp.Connection := DMConnections.BaseBO_Rating;
            sp.ProcedureName := 'ADM_BandasHorarias_SELECT';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoEsquema').Value := _CodigoEsquema;

            sp.Open;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
               raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

            Result:= CrearClientDataSet(sp, True);
            
        finally
            FreeAndNil(sp);
        end;

    end;

{$ENDREGION}

end.
