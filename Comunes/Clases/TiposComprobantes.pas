//TASK_117_JMA_20170320
unit TiposComprobantes;

interface

uses ClaseBase, DB, ADODB, DBClient, SysUtils, DMConnection, UtilDB, PeaProcs, Variants, Util;

type TTipoComprobante= class(TClaseBase)
private
    function GetTipoComprobante(): string;
    procedure SetTipoComprobante(Valor:string);

    function GetDescripcion(): string;
    procedure SetDescripcion(Valor:string);

    function GetSigno(): SmallInt;
    procedure SetSigno(Valor:smallint);

    function GetSeUsaEnSaldo(): Boolean;
    procedure SetSeUsaEnSaldo(Valor:Boolean);

    function GetEsFiscal(): Variant;
    procedure SetEsFiscal(Valor:Variant);

    function GetPermiteSaltosFolio(): Variant;
    procedure SetPermiteSaltosFolio(Valor:Variant);

    function GetUsuarioCreacion(): string;
    procedure SetUsuarioCreacion(value: string);

    function GetFechaHoraCreacion(): TDateTime;
    procedure SetFechaHoraCreacion(value: TDateTime);

    function GetUsuarioActualizacion(): string;
    procedure SetUsuarioActualizacion(value: string);

    function GetFechaHoraActualizacion(): TDateTime;
    procedure SetFechaHoraActualizacion(value: TDateTime);


    public
    property TipoComprobante: string read GetTipoComprobante write SetTipoComprobante;
    property Descripcion: string read GetDescripcion write SetDescripcion;
    property Signo: SmallInt read GetSigno write SetSigno;
    property SeUsaEnSaldo: Boolean read GetSeUsaEnSaldo write SetSeUsaEnSaldo;
    property EsFiscal: Variant read GetEsFiscal write SetEsFiscal;
    property PermiteSaltosFolio: Variant read GetPermiteSaltosFolio write SetPermiteSaltosFolio;
    property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
    property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
    property UsuarioActualizacion: string read GetUsuarioActualizacion write SetUsuarioActualizacion;
    property FechaHoraActualizacion: TDateTime read GetFechaHoraActualizacion write SetFechaHoraActualizacion;


    function Obtener(): TClientDataSet;
    function Agregar(TipoComprobante, Descripcion: string; Signo: SmallInt; SeUsaEnSaldo: Boolean; Usuario: string; EsFiscal, PermiteSaltosFolio: Boolean): Boolean;
    function Modificar(TipoComprobante, Descripcion: string; Signo: SmallInt; SeUsaEnSaldo: Boolean; Usuario: string; EsFiscal, PermiteSaltosFolio: Boolean): Boolean;
    function Eliminar(TipoComprobante, Usuario: string): Boolean;

end;

implementation

{$REGION 'GETTERS y SETTERS'}
function TTipoComprobante.GetTipoComprobante(): string;
begin
    Result := GetField('TipoComprobante');
end;

procedure TTipoComprobante.SetTipoComprobante(Valor:string);
begin
    SetField('IdIPC', Valor);
end;

function TTipoComprobante.GetDescripcion(): string;
begin
    Result := GetField('Descripcion');
end;

procedure TTipoComprobante.SetDescripcion(Valor:string);
begin
    SetField('Descripcion', Valor);
end;

function TTipoComprobante.GetSigno(): SmallInt;
begin
    Result := GetField('Signo');
end;

procedure TTipoComprobante.SetSigno(Valor:SmallInt);
begin
    SetField('Signo', Valor);
end;

function TTipoComprobante.GetSeUsaEnSaldo(): Boolean;
begin
    Result := GetField('SeUsaEnSaldo');
end;

procedure TTipoComprobante.SetSeUsaEnSaldo(Valor:Boolean);
begin
    SetField('SeUsaEnSaldo', Valor);
end;

function TTipoComprobante.GetEsFiscal(): Variant;
var
    t: Variant;
begin
    t := GetField('EsFiscal');
    if t = null then
        t := False;
    Result := t;
end;

procedure TTipoComprobante.SetEsFiscal(Valor:Variant);
begin
    SetField('EsFiscal', Valor);
end;

function TTipoComprobante.GetPermiteSaltosFolio(): Variant;
var
    t: Variant;
begin
    t := GetField('PermiteSaltosFolio');
    if t = null then
        t := False;
    Result := t;
end;

procedure TTipoComprobante.SetPermiteSaltosFolio(Valor:Variant);
begin
    SetField('PermiteSaltosFolio', Valor);
end;

function TTipoComprobante.GetUsuarioCreacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioCreacion');
    if t = null then
       t := '';
    Result :=  t;
end;

procedure TTipoComprobante.SetUsuarioCreacion(value: string);
begin
    SetField('UsuarioCreacion', value);
end;

function TTipoComprobante.GetFechaHoraCreacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraCreacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TTipoComprobante.SetFechaHoraCreacion(value: TDateTime);
begin
    SetField('FechaHoraCreacion', value);
end;

function TTipoComprobante.GetUsuarioActualizacion(): string;
var
    t: Variant;
begin
    t:= GetField('UsuarioActualizacion');
    if t = null then
       t := '';
    Result := t;
end;

procedure TTipoComprobante.SetUsuarioActualizacion(value: string);
begin
    SetField('UsuarioActualizacion', value);
end;

function TTipoComprobante.GetFechaHoraActualizacion(): TDateTime;
var
    t: Variant;
begin
    t:= GetField('FechaHoraActualizacion');
    if t = null then
       t := NullDate;
    Result := t;
end;

procedure TTipoComprobante.SetFechaHoraActualizacion(value: TDateTime);
begin
    SetField('FechaHoraActualizacion', value);
end;
{$ENDREGION}


function TTipoComprobante.Obtener(): TClientDataSet;
var
    sp: TADOStoredProc;
begin
    try
       Result := nil;

       sp := TADOStoredProc.Create(nil);
       sp.Connection := DMConnections.BaseCAC;
       sp.ProcedureName := 'ObtenerTiposComprobantes';
       sp.Parameters.Refresh;

       sp.Open;

       if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

       Result:= CrearClientDataSet(sp);

    finally
       FreeAndNil(sp);
    end;
end;

function TTipoComprobante.Agregar(TipoComprobante, Descripcion: string; Signo: SmallInt; SeUsaEnSaldo: Boolean; Usuario: string; EsFiscal, PermiteSaltosFolio: Boolean): Boolean;
var
    sp: TADOStoredProc;
begin
    try
        Result := False;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'TiposComprobantes_Agregar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        sp.Parameters.ParamByName('@Descripcion').Value := Descripcion;
        sp.Parameters.ParamByName('@Signo').Value := Signo;
        sp.Parameters.ParamByName('@SeUsaEnSaldo').Value := SeUsaEnSaldo;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.Parameters.ParamByName('@EsFiscal').Value := EsFiscal;
        sp.Parameters.ParamByName('@PermiteSaltosFolio').Value := PermiteSaltosFolio;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;

function TTipoComprobante.Modificar(TipoComprobante, Descripcion: string; Signo: SmallInt; SeUsaEnSaldo: Boolean; Usuario: string; EsFiscal, PermiteSaltosFolio: Boolean): Boolean;
var
    sp: TADOStoredProc;
begin
    try
        Result := False;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'TiposComprobantes_Modificar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Descripcion').Value := Descripcion;
        sp.Parameters.ParamByName('@Signo').Value := Signo;
        sp.Parameters.ParamByName('@SeUsaEnSaldo').Value := SeUsaEnSaldo;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.Parameters.ParamByName('@EsFiscal').Value := EsFiscal;
        sp.Parameters.ParamByName('@PermiteSaltosFolio').Value := PermiteSaltosFolio;
        sp.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;

function TTipoComprobante.Eliminar(TipoComprobante, Usuario: string): Boolean;
var
    sp: TADOStoredProc;
begin
    try
        Result := False;
        
        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'TiposComprobantes_Eliminar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;

end.
