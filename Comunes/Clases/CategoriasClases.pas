unit CategoriasClases;

interface
 
uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TCategoriaClase = Class(TClaseBase)

    private
        function GetId_CategoriasClases(): Integer;
        function GetDescripcion(): string;



    public
        property Id_CategoriasClases: Integer read GetId_CategoriasClases;
        property Descripcion: string read GetDescripcion;

        function Obtener(): TClientDataSet;
    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TCategoriaClase.GetId_CategoriasClases(): Integer;
    begin
        Result := GetField('ID_CategoriasClases');
    end;

    function TCategoriaClase.GetDescripcion(): string;
    begin
        Result := GetField('Descripcion');
    end;
{$ENDREGION}

    function TCategoriaClase.Obtener(): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'Categorias_CategoriasClases_SELECT';
           sp.Parameters.Refresh;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;

end.
