//TASK_109_JMA_20170215
unit PlanesTarifarios;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TPlanTarifarioVersion = Class(TClaseBase)

    private
        function GetID_PlanTarifarioVersion(): LongInt;
        procedure SetID_PlanTarifarioVersion(value: LongInt);

        function GetCodigoConcesionaria(): Byte;
        procedure SetCodigoConcesionaria(value: Byte);

        function GetConcesionaria(): string;
        procedure SetConcesionaria(value: string);

        function GetCodigoVersion(): Integer;
        procedure SetCodigoVersion(value: Integer);

        function GetID_CategoriasClases(): Integer;
        procedure SetID_CategoriasClases(value: Integer);

        function GetCategoriaClase(): string;
        procedure SetCategoriaClase(value: string);

        function GetFechaActivacion(): TDateTime;
        procedure SetFechaActivacion(value: TDateTime);

        function GetHabilitado(): Boolean;
        procedure SetHabilitado(value: Boolean);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property ID_PlanTarifarioVersion: LongInt read GetID_PlanTarifarioVersion write SetID_PlanTarifarioVersion;
        property CodigoConcesionaria: Byte read GetCodigoConcesionaria write SetCodigoConcesionaria;
        property Concesionaria: string read GetConcesionaria write SetConcesionaria;
        property CodigoVersion: Integer read GetCodigoVersion write SetCodigoVersion;
        property ID_CategoriasClases: Integer read GetID_CategoriasClases write SetID_CategoriasClases;
        property CategoriaClase: string read GetCategoriaClase write SetCategoriaClase;
        property FechaActivacion: TDateTime read GetFechaActivacion write SetFechaActivacion;
        property Habilitado: Boolean read GetHabilitado write SetHabilitado;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(CodigoConcesionaria: Byte = 0; ID_CategoriasClases: Integer = 0): TClientDataSet;
    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TPlanTarifarioVersion.GetID_PlanTarifarioVersion(): LongInt;
    begin
        Result := GetField('ID_PlanTarifarioVersion');
    end;

    procedure TPlanTarifarioVersion.SetID_PlanTarifarioVersion(value: LongInt);
    begin
        SetField('ID_PlanTarifarioVersion', value);
    end;

    function TPlanTarifarioVersion.GetCodigoConcesionaria(): Byte;
    begin
        Result := GetField('CodigoConcesionaria');
    end;

    procedure TPlanTarifarioVersion.SetCodigoConcesionaria(value: Byte);
    begin
        SetField('CodigoConcesionaria', value);
    end;

    function TPlanTarifarioVersion.GetConcesionaria(): string;
    begin
        Result := GetField('Concesionaria');
    end;

    procedure TPlanTarifarioVersion.SetConcesionaria(value: string);
    begin
        SetField('Concesionaria', value);
    end;

    function TPlanTarifarioVersion.GetCodigoVersion(): Integer;
    begin
        Result := GetField('CodigoVersion');
    end;

    procedure TPlanTarifarioVersion.SetCodigoVersion(value: Integer);
    begin
        SetField('CodigoVersion', value);
    end;

    function TPlanTarifarioVersion.GetID_CategoriasClases(): Integer;
    begin
        Result := GetField('ID_CategoriasClases');
    end;

    procedure TPlanTarifarioVersion.SetID_CategoriasClases(value: Integer);
    begin
        SetField('ID_CategoriasClases', value);
    end;

    function TPlanTarifarioVersion.GetCategoriaClase(): string;
    begin
        Result := GetField('CategoriaClase');
    end;

    procedure TPlanTarifarioVersion.SetCategoriaClase(value: string);
    begin
        SetField('CategoriaClase', value);
    end;

    function TPlanTarifarioVersion.GetFechaActivacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaActivacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetFechaActivacion(value: TDateTime);
    begin
        SetField('FechaActivacion', value);
    end;

    function TPlanTarifarioVersion.GetHabilitado(): Boolean;
    begin
        Result := GetField('Habilitado');
    end;

    procedure TPlanTarifarioVersion.SetHabilitado(value: Boolean);
    begin
        SetField('Habilitado', value);
    end;

    function TPlanTarifarioVersion.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TPlanTarifarioVersion.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TPlanTarifarioVersion.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TPlanTarifarioVersion.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TPlanTarifarioVersion.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TPlanTarifarioVersion.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TPlanTarifarioVersion.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TPlanTarifarioVersion.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;
{$ENDREGION}

    function TPlanTarifarioVersion.Obtener(CodigoConcesionaria: Byte = 0; ID_CategoriasClases: Integer = 0): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseBO_Rating;
           sp.ProcedureName := 'PlanTarifarioVersiones_Obtener';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
           sp.Parameters.ParamByName('@ID_CategoriasClases').Value := ID_CategoriasClases;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
end.
