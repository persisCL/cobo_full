//TASK_105_JMA_20170302
unit IPC;

interface

uses ClaseBase, DB, ADODB, DBClient, SysUtils, DMConnection, UtilDB, PeaProcs, Variants, Util;								//TASK_109_JMA_20170215

type TIPC= class(TClaseBase)
private
    function GetIdIPC(): Integer;
    procedure SetIdIPC(Valor:Integer);
    function GetAnio(): Integer;
    procedure SetAnio(Valor:Integer);
    function GetTBP(): Variant;
    procedure SetTBP(Valor:Variant);
    function GetTBFP(): Variant;
    procedure SetTBFP(Valor:Variant);
    function GetTBI(): Variant;
    procedure SetTBI(Valor:Variant);
    function GetTS(): Variant;
    procedure SetTS(Valor:Variant);
    function GetIPC(): Variant;				//TASK_112_JMA_20170309
    procedure SetIPC(Valor:Variant);		//TASK_112_JMA_20170309

    public
    property IdIPC: Integer read GetIdIPC write SetIdIPC;
    property Anio: Integer read GetAnio write SetAnio;
    property TBP: Variant read GetTBP write SetTBP;
    property TBFP: Variant read GetTBFP write SetTBFP;
    property TBI: Variant read GetTBI write SetTBI;
    property TS: Variant read GetTS write SetTS;
    property IPC: Variant read GetIPC write SetIPC;		//TASK_112_JMA_20170309

    function Obtener(Anio: Integer = 0): TClientDataSet;																	//TASK_109_JMA_20170215
    //function Agregar(IdIPC: Integer; Anio, TBP, TBFP, TBI, Valor, Usuario: string): Boolean;                              //TASK_112_JMA_20170309
    function Modificar(Anio: Integer; Datos: TDataSet; Usuario: string): Boolean;											//TASK_112_JMA_20170309
    function ObtenerUltimoAnio():Integer;
    function ObtenerPrimerAnio():Integer;
    function TieneTraficos():Integer;
    function AnioInicial: Integer;                                                                                          //TASK_112_JMA_20170309
end;

implementation

{$REGION 'GETTERS y SETTERS'}
function TIPC.GetIdIPC(): Integer;
begin
    Result := GetField('IdIPC');
end;

procedure TIPC.SetIdIPC(Valor:Integer);
begin
    SetField('IdIPC', Valor);
end;

function TIPC.GetAnio(): Integer;
begin
    Result := GetField('Anio');
end;

procedure TIPC.SetAnio(Valor:Integer);
begin
    SetField('Anio', Valor);
end;

function TIPC.GetTBP(): Variant;														//TASK_112_JMA_20170309
begin
    Result := IIf(ClientDataSet.FieldByName('TBP') <> nil, GetField('TBP'), null);		//TASK_112_JMA_20170309
end;

procedure TIPC.SetTBP(Valor:Variant);													//TASK_112_JMA_20170309
begin
    SetField('TBP', Valor);
end;

function TIPC.GetTBFP(): Variant;														//TASK_112_JMA_20170309
begin
    Result := IIf(ClientDataSet.FieldByName('TBFP') <> nil, GetField('TBFP'), null);	//TASK_112_JMA_20170309
end;

procedure TIPC.SetTBFP(Valor:Variant);													//TASK_112_JMA_20170309
begin
    SetField('TBFP', Valor);
end;

function TIPC.GetTBI(): Variant;														//TASK_112_JMA_20170309
begin
    Result := IIf(ClientDataSet.FieldByName('TBI') <> nil, GetField('TBI'), null);		//TASK_112_JMA_20170309
end;

procedure TIPC.SetTBI(Valor:Variant);													//TASK_112_JMA_20170309
begin
    SetField('TBI', Valor);
end;

function TIPC.GetTS(): Variant;															//TASK_112_JMA_20170309
begin
    Result := IIf(ClientDataSet.FieldByName('TS') <> nil, GetField('TS'), null);		//TASK_112_JMA_20170309
end;

procedure TIPC.SetTS(Valor:Variant);													//TASK_112_JMA_20170309
begin
    SetField('TS', Valor);																//TASK_112_JMA_20170309
end;
{INICIO: TASK_112_JMA_20170309}
function TIPC.GetIPC(): Variant;
begin
    Result := IIf(ClientDataSet.FieldByName('IPC') <> nil, GetField('IPC'), null);
end;

procedure TIPC.SetIPC(Valor:Variant);
begin
    SetField('IPC', Valor);
end;
{TERMINO: TASK_112_JMA_20170309}
{$ENDREGION}


function TIPC.Obtener(Anio: Integer = 0): TClientDataSet;           					//TASK_109_JMA_20170215
var
    sp: TADOStoredProc;
    ex: Exception;
begin
    try

       sp := TADOStoredProc.Create(nil);
       sp.Connection := DMConnections.BaseCAC;
       sp.ProcedureName := 'IPC_SELECT';
       sp.Parameters.Refresh;
       sp.Parameters.ParamByName('@Anio').Value := IIf(Anio = 0, null, Anio);			//TASK_109_JMA_20170215

       sp.Open;

       if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
       begin
           ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
           raise ex;
       end;

       Result:= CrearClientDataSet(sp);

    finally
       FreeAndNil(sp);
    end;
end;

{INICIO: TASK_112_JMA_20170309
function TIPC.Agregar(IdIPC: Integer; Anio, TBP, TBFP, TBI, Valor, Usuario: string): Boolean;
var
    sp: TADOStoredProc;
    ex: Exception;
    mensaje: string;
begin
    try

        if (Trim(Valor) <> '') and not IsFloat(Trim(TBI)) then
            mensaje := 'Ingrese un Valor de IPC v�lido';

        if self.ClientDataSet.RecordCount = 0 then
        begin

            if Trim(Anio) = '' then
                mensaje := 'Ingrese el A�o'
            else if not IsInteger(Trim(Anio)) then
                mensaje := 'Ingrese un A�o v�lido'
            else if Trim(TBP) = '' then
                mensaje := 'Ingrese el TBP'
            else if not IsFloat(Trim(TBP)) then
                mensaje := 'Ingrese un TBP v�lido'
            else if Trim(TBFP) = '' then
                mensaje := 'Ingrese el TBFP'
            else if not IsFloat(Trim(TBFP)) then
                mensaje := 'Ingrese un TBFP v�lido'
            else if Trim(TBI) = '' then
                mensaje := 'Ingrese el TBI'
            else if not IsFloat(Trim(TBI)) then
                mensaje := 'Ingrese un TBI v�lido';

        end;

        if mensaje <> '' then
        begin
            ex := Exception.Create(mensaje);
            raise ex;
        end;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'IPC_Agregar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@IdIPC').Value := IdIPC;
        sp.Parameters.ParamByName('@Anio').Value := StrToInt(Trim(Anio));
        sp.Parameters.ParamByName('@TBP').Value := StrToFloat(Trim(TBP));
        sp.Parameters.ParamByName('@TBFP').Value := StrToFloat(Trim(TBFP));
        sp.Parameters.ParamByName('@TBI').Value := StrToFloat(Trim(TBI));
        sp.Parameters.ParamByName('@Valor').Value := StrToFloat(StringReplace(Trim(Valor),',','.',[rfReplaceAll]));
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
        begin
           ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
           raise ex;
        end;

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;
}
function TIPC.Modificar(Anio: Integer; Datos: TDataSet; Usuario: string): Boolean;
var
    sp: TADOStoredProc;
    cadenaDatos: string;
    i: Integer;
begin
    try

        cadenaDatos := '';
        for I := 0 to Datos.Fields.Count - 1 do
        begin
            if (Datos.Fields[i].FieldName <> 'Anio') then
            begin
                if cadenaDatos <> '' then
                    cadenaDatos := cadenaDatos + ';';
                cadenaDatos := cadenaDatos + datos.Fields[i].FieldName + ',' + IntToStr(datos.Fields[i].Value*100);
            end;
        end;

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'IPC_Modificar';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Anio').Value := Anio;
        sp.Parameters.ParamByName('@Datos').Value := cadenaDatos;
        sp.Parameters.ParamByName('@Usuario').Value := Usuario;
        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);

        Result:= True;

    finally
       FreeAndNil(sp);
    end;
end;
{TERMINO: TASK_112_JMA_20170309}

function TIPC.ObtenerUltimoAnio():Integer;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.IPC_ObtenerUltimoAnio()')
end;

function TIPC.ObtenerPrimerAnio():Integer;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.IPC_ObtenerPrimerAnio()')
end;

function TIPC.TieneTraficos: Integer;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.IPC_EnUso(' + IntToStr(Self.Anio) + ')');
end;
{INICIO: TASK_112_JMA_20170309}
function TIPC.AnioInicial: Integer;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerParametroGeneralAnioInicioIPC()');
end;
{TERMINO: TASK_112_JMA_20170309}

end.
