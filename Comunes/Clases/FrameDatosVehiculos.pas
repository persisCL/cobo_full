unit FrameDatosVehiculos;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TFrameDatosVehiculo = Class(TClaseBase)

    private
        function GetContextMark(): Byte;
        procedure SetContextMark(Valor:Byte);
        function GetContractSerialNumber(): Cardinal;
        procedure SetContractSerialNumber(Valor:Cardinal);
        function GetEtiqueta(): string;
        procedure SetEtiqueta(Valor:string);
        function GetCodigoEstadoSituacionTAG(): Variant;
        procedure SetCodigoEstadoSituacionTAG(Valor:Variant);
        function GetEstadoSituacion(): Variant;
        procedure SetEstadoSituacion(Valor:Variant);
        function GetIndicadorListaVerde(): Boolean;
        procedure SetIndicadorListaVerde(Valor:Boolean);
        function GetIndicadorListaAmarilla(): Boolean;
        procedure SetIndicadorListaAmarilla(Valor:Boolean);
        function GetIndicadorListaGris(): Boolean;
        procedure SetIndicadorListaGris(Valor:Boolean);
        function GetIndicadorListaNegra(): Boolean;
        procedure SetIndicadorListaNegra(Valor:Boolean);
        function GetExceptionHandling(): Variant;
        procedure SetExceptionHandling(Valor:Variant);
        function GetMMIContactOperator(): Boolean;
        procedure SetMMIContactOperator(Valor:Boolean);
        function GetMMINotOK(): Boolean;
        procedure SetMMINotOK(Valor:Boolean);
        function GetEnviar(): Variant;
        procedure SetEnviar(Valor:Variant);
        function GetCodigoCategoriaInterurbana(): Variant;
        function GetCodigoCategoriaUrbana(): Variant;
        function GetCategoriaInterurbana(): Variant;
        function GetCategoriaUrbana(): Variant;
        function GetCodigoUbicacionTAG(): Variant;
        procedure SetCodigoUbicacionTAG(Valor:Variant);
        function GetDescripcionUbicacion(): Variant;
        procedure SetDescripcionUbicacion(Valor:Variant);
        function GetOtraConcesionaria(): Integer;
        procedure SetOtraConcesionaria(Valor:Integer);
        function GetCodigoEstadoConservacion(): Variant;
        procedure SetCodigoEstadoConservacion(Valor:Variant);
        function GetPatente(): Variant;
        function GetFechaModificacionDeseado(): TDateTime;
        procedure SetFechaModificacionDeseado(Valor:TDateTime);
        procedure SetPatente(Valor:Variant);


    public
        property ContextMark: Byte read GetContextMark write SetContextMark;
        property ContractSerialNumber: Cardinal read GetContractSerialNumber write SetContractSerialNumber;
        property Etiqueta: string read GetEtiqueta write SetEtiqueta;
        property CodigoEstadoSituacionTAG: Variant read GetCodigoEstadoSituacionTAG write SetCodigoEstadoSituacionTAG;
        property EstadoSituacion: Variant read GetEstadoSituacion write SetEstadoSituacion;
        property IndicadorListaVerde: Boolean read GetIndicadorListaVerde write SetIndicadorListaVerde;
        property IndicadorListaAmarilla: Boolean read GetIndicadorListaAmarilla write SetIndicadorListaAmarilla;
        property IndicadorListaGris: Boolean read GetIndicadorListaGris write SetIndicadorListaGris;
        property IndicadorListaNegra: Boolean read GetIndicadorListaNegra write SetIndicadorListaNegra;
        property ExceptionHandling: Variant read GetExceptionHandling write SetExceptionHandling;
        property MMIContactOperator: Boolean read GetMMIContactOperator write SetMMIContactOperator;
        property MMINotOK: Boolean read GetMMINotOK write SetMMINotOK;
        property Enviar: Variant read GetEnviar write SetEnviar;
        property CodigoCategoriaInterurbana: Variant read GetCodigoCategoriaInterurbana;
        property CodigoCategoriaUrbana: Variant read GetCodigoCategoriaUrbana;
        property CategoriaInterurbana: Variant read GetCategoriaInterurbana;
        property CategoriaUrbana: Variant read GetCategoriaUrbana;
        property CodigoUbicacionTAG: Variant read GetCodigoUbicacionTAG write SetCodigoUbicacionTAG;
        property DescripcionUbicacion: Variant read GetDescripcionUbicacion write SetDescripcionUbicacion;
        property OtraConcesionaria: integer read GetOtraConcesionaria write SetOtraConcesionaria;
        property CodigoEstadoConservacion: Variant read GetCodigoEstadoConservacion write SetCodigoEstadoConservacion;
        property FechaModificacionDeseado: TDateTime read GetFechaModificacionDeseado write SetFechaModificacionDeseado;
        property Patente: Variant read GetPatente write SetPatente;


        function LeerDatosTAG(ContractSerialNumber:Cardinal): TClientDataSet;

    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TFrameDatosVehiculo.GetContextMark(): Byte;
    begin
        Result := GetField('ContextMark');
    end;

    procedure TFrameDatosVehiculo.SetContextMark(Valor:Byte);
    begin
        SetField('ContextMark', Valor);
    end;

    function TFrameDatosVehiculo.GetContractSerialNumber(): Cardinal;
    begin
        Result := GetField('ContractSerialNumber');
    end;

    procedure TFrameDatosVehiculo.SetContractSerialNumber(Valor:Cardinal);
    begin
        SetField('ContractSerialNumber', Valor);
    end;

    function TFrameDatosVehiculo.GetEtiqueta(): string;
    begin
        Result := GetField('Etiqueta');
    end;

    procedure TFrameDatosVehiculo.SetEtiqueta(Valor:string);
    begin
        SetField('Etiqueta', Valor);
    end;

    function TFrameDatosVehiculo.GetCodigoEstadoSituacionTAG(): Variant;
    begin
        Result := GetField('CodigoEstadoSituacionTAG');
    end;

    procedure TFrameDatosVehiculo.SetCodigoEstadoSituacionTAG(Valor:Variant);
    begin
        SetField('CodigoEstadoSituacionTAG', Valor);
    end;

    function TFrameDatosVehiculo.GetEstadoSituacion(): Variant;
    begin
        Result := GetField('EstadoSituacion');
    end;

    procedure TFrameDatosVehiculo.SetEstadoSituacion(Valor:Variant);
    begin
        SetField('EstadoSituacion', Valor);
    end;

    function TFrameDatosVehiculo.GetIndicadorListaVerde(): Boolean;
    begin
        Result := GetField('IndicadorListaVerde');
    end;

    procedure TFrameDatosVehiculo.SetIndicadorListaVerde(Valor:Boolean);
    begin
        SetField('IndicadorListaVerde', Valor);
    end;

    function TFrameDatosVehiculo.GetIndicadorListaAmarilla(): Boolean;
    begin
        Result := GetField('IndicadorListaAmarilla');
    end;

    procedure TFrameDatosVehiculo.SetIndicadorListaAmarilla(Valor:Boolean);
    begin
        SetField('IndicadorListaAmarilla', Valor);
    end;

    function TFrameDatosVehiculo.GetIndicadorListaGris(): Boolean;
    begin
        Result := GetField('IndicadorListaGris');
    end;

    procedure TFrameDatosVehiculo.SetIndicadorListaGris(Valor:Boolean);
    begin
        SetField('IndicadorListaGris', Valor);
    end;

    function TFrameDatosVehiculo.GetIndicadorListaNegra(): Boolean;
    begin
        Result := GetField('IndicadorListaNegra');
    end;

    procedure TFrameDatosVehiculo.SetIndicadorListaNegra(Valor:Boolean);
    begin
        SetField('IndicadorListaNegra', Valor);
    end;

    function TFrameDatosVehiculo.GetExceptionHandling(): Variant;
    begin
        Result := GetField('ExceptionHandling');
    end;

    procedure TFrameDatosVehiculo.SetExceptionHandling(Valor:Variant);
    begin
        SetField('ExceptionHandling', Valor);
    end;

    function TFrameDatosVehiculo.GetMMIContactOperator(): Boolean;
    begin
        Result := GetField('MMIContactOperator');
    end;

    procedure TFrameDatosVehiculo.SetMMIContactOperator(Valor:Boolean);
    begin
        SetField('MMIContactOperator', Valor);
    end;

    function TFrameDatosVehiculo.GetMMINotOK(): Boolean;
    begin
        Result := GetField('MMINotOK');
    end;

    procedure TFrameDatosVehiculo.SetMMINotOK(Valor:Boolean);
    begin
        SetField('MMINotOK', Valor);
    end;

    function TFrameDatosVehiculo.GetEnviar(): Variant;
    begin
        Result := GetField('Enviar');
    end;

    procedure TFrameDatosVehiculo.SetEnviar(Valor:Variant);
    begin
        SetField('Enviar', Valor);
    end;

    function TFrameDatosVehiculo.GetCodigoCategoriaInterurbana(): Variant;
    begin
        Result := GetField('CodigoCategoriaInterurbana');
    end;

    function TFrameDatosVehiculo.GetCodigoCategoriaUrbana(): Variant;
    begin
        Result := GetField('CodigoCategoriaUrbana');
    end;

    function TFrameDatosVehiculo.GetCategoriaInterurbana(): Variant;
    begin
        Result := GetField('CategoriaInterurbana');
    end;

    function TFrameDatosVehiculo.GetCategoriaUrbana(): Variant;
    begin
        Result := GetField('CategoriaUrbana');
    end;

    function TFrameDatosVehiculo.GetCodigoUbicacionTAG(): Variant;
    begin
        Result := GetField('CodigoUbicacionTAG');
    end;

    procedure TFrameDatosVehiculo.SetCodigoUbicacionTAG(Valor:Variant);
    begin
        SetField('CodigoUbicacionTAG', Valor);
    end;

    function TFrameDatosVehiculo.GetDescripcionUbicacion(): Variant;
    begin
        Result := GetField('DescripcionUbicacion');
    end;

    procedure TFrameDatosVehiculo.SetDescripcionUbicacion(Valor:Variant);
    begin
        SetField('DescripcionUbicacion', Valor);
    end;

    function TFrameDatosVehiculo.GetOtraConcesionaria(): Integer;
    begin
        Result := GetField('OtraConcesionaria');
    end;

    procedure TFrameDatosVehiculo.SetOtraConcesionaria(Valor:Integer);
    begin
        SetField('OtraConcesionaria', Valor);
    end;

    function TFrameDatosVehiculo.GetCodigoEstadoConservacion(): Variant;
    begin
        Result := GetField('CodigoEstadoConservacion');
    end;

    procedure TFrameDatosVehiculo.SetCodigoEstadoConservacion(Valor:Variant);
    begin
        SetField('CodigoEstadoConservacion', Valor);
    end;

    function TFrameDatosVehiculo.GetFechaModificacionDeseado(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaModificacionDeseado');
        Result := IIf(t = null, NullDate, TDateTime(t));
    end;

    procedure TFrameDatosVehiculo.SetFechaModificacionDeseado(Valor:TDateTime);
    begin
        SetField('FechaModificacionDeseado', Valor);
    end;

    function TFrameDatosVehiculo.GetPatente(): Variant;
    begin
        Result := GetField('Patente');
    end;

    procedure TFrameDatosVehiculo.SetPatente(Valor:Variant);
    begin
        SetField('Patente', VarToStr(Valor));
    end;
{$ENDREGION}

    function TFrameDatosVehiculo.LeerDatosTAG(ContractSerialNumber:Cardinal): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'LeerDatosTAG';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@ContractSerialNumber').Value := ContractSerialNumber;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;

end.
