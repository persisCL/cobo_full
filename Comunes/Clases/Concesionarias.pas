//TASK_109_JMA_20170215
unit Concesionarias;

interface
uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util, SysUtilsCN, UtilDB;

type
    TConcesionaria = Class(TClaseBase)

    private
        function GetCodigoConcesionaria(): Byte;
        function GetDescripcion(): string;


    public
        property CodigoConcesionaria: Byte read GetCodigoConcesionaria;
        property Descripcion: string read GetDescripcion;

        function Obtener(CodigoTipoConcesionaria: Integer = 0; FiltraPorConcepto: Boolean = False): TClientDataSet;
    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TConcesionaria.GetCodigoConcesionaria(): Byte;
    begin
        Result := GetField('CodigoConcesionaria');
    end;

    function TConcesionaria.GetDescripcion(): string;
    begin
        Result := Trim(string(GetField('Descripcion')));
    end;
{$ENDREGION}

    function TConcesionaria.Obtener(CodigoTipoConcesionaria: Integer = 0; FiltraPorConcepto: Boolean = False): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'ObtenerConcesionarias';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@CodigoTipoConcesionaria').Value :=  IIf(CodigoTipoConcesionaria = 0, null, CodigoTipoConcesionaria);
           sp.Parameters.ParamByName('@FiltraPorConcepto').Value :=  FiltraPorConcepto;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
    
end.
