//TASK_094_JMA_20160111
unit CategoriasCambio;

interface

uses ClaseBase, DB, ADODB, DBClient, SysUtils, DMConnection;

type TCategoriasCambio= class(TClaseBase)
private
    function GetCodigoCategoria(): Byte;
    procedure SetCodigoCategoria(Valor:Byte);

    public
    property CodigoCategoria: Byte read GetCodigoCategoria write SetCodigoCategoria;

    function ObtenerCategoriasCambioDisponibles(CodigoCategoriaInterurbana: Byte): TClientDataSet;
end;

implementation

 function TCategoriasCambio.GetCodigoCategoria(): Byte;
begin
    Result := GetField('CodigoCategoria');
end;

procedure TCategoriasCambio.SetCodigoCategoria(Valor:Byte);
begin
    SetField('CodigoCategoria', Valor);
end;


function TCategoriasCambio.ObtenerCategoriasCambioDisponibles(CodigoCategoriaInterurbana: Byte): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try
            if CodigoCategoriaInterurbana <= 0 then
            begin
                ex:= Exception.Create('CodigoCategoriaInterurbana debe ser mayor que 0');
                raise ex;
            end;
            
           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'Categorias_ObtenerCambioDisponibles';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@CodigoCategoriaInterurbana').Value := CodigoCategoriaInterurbana;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
           FreeAndNil(sp);
        end;
    end;
end.
