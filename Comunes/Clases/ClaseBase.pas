//TASK_094_JMA_20160111
unit ClaseBase;

interface
uses DB, ADODB, DBClient, Util, Variants, SysUtils, Classes, Provider;       //TASK_106_JMA_20170206

type TClaseBase= class
    private																			//TASK_109_JMA_20170215
        FEventoAfterScroll : TDataSetNotifyEvent;									//TASK_109_JMA_20170215
    protected
        function GetField(nombre: string): Variant;
        procedure SetField(nombre: string; valor: Variant);         				//TASK_106_JMA_20170206

        function CrearClientDataSet(sp: TADOStoredProc; agregarEstado: Boolean = False):TClientDataSet;     //TASK_109_JMA_20170215
    public
        ClientDataSet: TDataSet;
        //procedure AceptarCambios();
        function CopiarClientDataSet(): TClientDataSet;                        //TASK_109_JMA_20170215
        procedure ActivarAfterScroll(activar: Boolean);                         //TASK_109_JMA_20170215
        function BuscarEnClientDataSet(Campo, Valor: string): TBookmark;	   //TASK_110_JMA_20170224
    published
        destructor Destroy(); override;
        
end;

implementation

function TClaseBase.CrearClientDataSet(sp: TADOStoredProc; agregarEstado: Boolean = False):TClientDataSet;    //TASK_109_JMA_20170215
var
    i: Integer;
begin
    {INICIO:  TASK_105_JMA_20170302
    if sp.RecordCount > 0 then
    begin
        ClientDataSet := TClientDataSet.Create(nil);
        for i := 0 to sp.FieldCount - 1 do
        begin
            ClientDataSet.FieldDefs.Add(sp.Fields[i].FieldName, sp.Fields[i].DataType, sp.Fields[i].Size);
        end;
        TClientDataSet(ClientDataSet).CreateDataSet;

        while not sp.Eof do
        begin
            ClientDataSet.Append;
            for I := 0 to sp.FieldCount - 1 do
            begin
                ClientDataSet.Fields[i].Value := sp.Fields[i].Value;
            end;
            ClientDataSet.Post;
            sp.Next
        end;
        Result := TClientDataSet(ClientDataSet);
    end;
    }
    if Assigned(ClientDataSet) then
        ClientDataSet.Free;
    ClientDataSet := TClientDataSet.Create(nil);
    for i := 0 to sp.FieldCount - 1 do
    begin
        ClientDataSet.FieldDefs.Add(sp.Fields[i].FieldName, IIf(sp.Fields[i].DataType = ftAutoInc, ftInteger,sp.Fields[i].DataType) , sp.Fields[i].Size);				//TASK_109_JMA_20170215
    end;
    if agregarEstado then																		//TASK_109_JMA_20170215
        ClientDataSet.FieldDefs.Add('Estado', ftSmallint);										//TASK_109_JMA_20170215
    TClientDataSet(ClientDataSet).CreateDataSet;
    while not sp.Eof do
    begin
        ClientDataSet.Append;
        for I := 0 to sp.FieldCount - 1 do
        begin
            ClientDataSet.Fields[i].Value := sp.Fields[i].Value;
        end;
        if agregarEstado then																		//TASK_109_JMA_20170215
            ClientDataSet.FieldByName('Estado').Value := 0;											//TASK_109_JMA_20170215
        ClientDataSet.Post;
        sp.Next
    end;
    Result := TClientDataSet(ClientDataSet);
    Result.First;                               //TASK_110_JMA_20170224
    Result.Filtered := True;                    //TASK_109_JMA_20170215
    {TERMINO: TASK_105_JMA_20170302}
end;

function TClaseBase.GetField(nombre: string):Variant;
begin
    Result := ClientDataSet.FieldByName(nombre).Value;
end;

procedure TClaseBase.SetField(nombre: string; valor: Variant);
begin
    if not (ClientDataSet.State = dsEdit) then
        ClientDataSet.Edit;
    ClientDataSet.FieldByName(nombre).Value := valor;
end;
{INICIO: TASK_109_JMA_20170215
procedure TClaseBase.AceptarCambios();
begin
     ClientDataSet.Post;
end;
TERMINO: TASK_109_JMA_20170215}

destructor TClaseBase.Destroy();
begin
    if Assigned(ClientDataSet) then
        FreeAndNil(ClientDataSet);     //TASK_109_JMA_20170215
end;

{INICIO: TASK_109_JMA_20170215}
function TClaseBase.CopiarClientDataSet(): TClientDataSet;
var
    i: Integer;
    m : string;
begin
    m := ClientDataSet.BookMark;

    Result := TClientDataSet.Create(nil);

    for I := 0 to ClientDataSet.FieldCount - 1 do
    begin
        Result.FieldDefs.Add(ClientDataSet.Fields[i].FieldName,
                            ClientDataSet.Fields[i].DataType,
                            ClientDataSet.Fields[i].Size);
    end;
    Result.CreateDataSet;
    
    ClientDataSet.First;
    while not ClientDataSet.eof do
    begin
        Result.Append;
        Result.CopyFields(ClientDataSet);
        Result.Post;
        ClientDataSet.Next;
    end;

    Result.Filtered := True;                    
    ClientDataSet.BookMark := m;

end;

procedure TClaseBase.ActivarAfterScroll(activar: Boolean);
begin
    if activar then
    begin
        if Assigned(FEventoAfterScroll) then
        begin
            ClientDataSet.AfterScroll := FEventoAfterScroll;
            FEventoAfterScroll := nil;
        end;
    end
    else
    begin
        FEventoAfterScroll:= ClientDataSet.AfterScroll;
        ClientDataSet.AfterScroll := nil;
    end;
end;

{TERMINO: TASK_109_JMA_20170215}

{INICIO: TASK_110_JMA_20170224}
function TClaseBase.BuscarEnClientDataSet(Campo, Valor: string): TBookmark;
var
    MarcaActual : TBookmark;
    t: Variant;
begin
    Result := nil;    
    MarcaActual := ClientDataSet.GetBookmark;

    try
        ClientDataSet.DisableControls;
        ClientDataSet.First;

        while not ClientDataSet.Eof do
        begin

            t := GetField(Campo);
            if t <> null then
            begin
                if VarToStr(t) = Valor then
                begin
                    Result := ClientDataSet.GetBookmark;
                    Break;
                end;
            end;

            ClientDataSet.Next;
        end;

    finally
        ClientDataSet.GotoBookmark(MarcaActual);
        ClientDataSet.EnableControls;
        ClientDataSet.FreeBookmark(MarcaActual);
    end;

end;
{TERMINO: TASK_110_JMA_20170224}

end.
