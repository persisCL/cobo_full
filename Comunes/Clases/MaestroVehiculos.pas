unit MaestroVehiculos;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TVehiculo = Class(TClaseBase)

    private
        function GetCodigoVehiculo(): Integer;
        procedure SetCodigoVehiculo(Valor:Integer);
        function GetCodigoTipoPatente(): string;
        procedure SetCodigoTipoPatente(Valor:string);
        function GetPatente(): string;
        procedure SetPatente(Valor:string);
        function GetDigitoVerificadorPatente(): string;
        procedure SetDigitoVerificadorPatente(Valor:string);
        function GetCodigoMarca(): Byte;
        procedure SetCodigoMarca(Valor:Byte);
        function GetModelo(): string;
        procedure SetModelo(Valor:string);
        function GetAnioVehiculo(): Word;       //0 a 65,535
        procedure SetAnioVehiculo(Valor:Word);
        function GetCodigoColor(): Integer;
        procedure SetCodigoColor(Valor:Integer);
        function GetTieneAcoplado(): Boolean;
        procedure SetTieneAcoplado(Valor:Boolean);
        function GetDigitoVerificadorValido(): Boolean;
        procedure SetDigitoVerificadorValido(Valor:Boolean);
        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(Valor:TDateTime);
        function GetRobado(): Boolean;
        procedure SetRobado(Valor:Boolean);


    public
        property CodigoVehiculo: Integer read GetCodigoVehiculo write SetCodigoVehiculo;
        property CodigoTipoPatente: string read GetCodigoTipoPatente write SetCodigoTipoPatente;
        property Patente: string read GetPatente write SetPatente;
        property DigitoVerificadorPatente: string read GetDigitoVerificadorPatente write SetDigitoVerificadorPatente;
        property CodigoMarca: Byte read GetCodigoMarca write SetCodigoMarca;
        property Modelo: string read GetModelo write SetModelo;
        property AnioVehiculo: Word read GetAnioVehiculo write SetAnioVehiculo;
        property CodigoColor: Integer read GetCodigoColor write SetCodigoColor;
        property TieneAcoplado: Boolean read GetTieneAcoplado write SetTieneAcoplado;
        property DigitoVerificadorValido: Boolean read GetDigitoVerificadorValido write SetDigitoVerificadorValido;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;
        property Robado: Boolean read GetRobado write SetRobado;


        function Obtener(Patente:string; CodigoTipoPatente:string): TClientDataSet;

    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TVehiculo.GetCodigoVehiculo(): Integer;
    begin
        Result := GetField('CodigoVehiculo');
    end;

    procedure TVehiculo.SetCodigoVehiculo(Valor:Integer);
    begin
        SetField('CodigoVehiculo', Valor);
    end;

    function TVehiculo.GetCodigoTipoPatente(): string;
    begin
        Result := GetField('CodigoTipoPatente');
    end;

    procedure TVehiculo.SetCodigoTipoPatente(Valor:string);
    begin
        SetField('CodigoTipoPatente', Valor);
    end;

    function TVehiculo.GetPatente(): string;
    begin
        Result := GetField('Patente');
    end;

    procedure TVehiculo.SetPatente(Valor:string);
    begin
        SetField('Patente', Valor);
    end;

    function TVehiculo.GetDigitoVerificadorPatente(): string;
    var
        t:Variant;
    begin
        t:= GetField('DigitoVerificadorPatente');
        Result := iif( t = null, '', string(t));
    end;

    procedure TVehiculo.SetDigitoVerificadorPatente(Valor:string);
    begin
        SetField('DigitoVerificadorPatente', Valor);
    end;

    function TVehiculo.GetCodigoMarca(): Byte;
    begin
        Result := GetField('CodigoMarca');
    end;

    procedure TVehiculo.SetCodigoMarca(Valor:Byte);
    begin
        SetField('CodigoMarca', Valor);
    end;

    function TVehiculo.GetModelo(): string;
    var
        t:Variant;
    begin
        t:= GetField('Modelo');
        Result := iif(t= null, '', string(t));
    end;

    procedure TVehiculo.SetModelo(Valor:string);
    begin
        SetField('Modelo', Valor);
    end;

    function TVehiculo.GetAnioVehiculo(): Word;
    begin
        Result := GetField('AnioVehiculo');
    end;

    procedure TVehiculo.SetAnioVehiculo(Valor:Word);
    begin
        SetField('AnioVehiculo', Valor);
    end;

    function TVehiculo.GetCodigoColor(): Integer;
    var
        t:Variant;
    begin
        t := GetField('CodigoColor');
        Result := iif(t= null, 0, Integer(t));
    end;

    procedure TVehiculo.SetCodigoColor(Valor:Integer);
    begin
        SetField('CodigoColor', Valor);
    end;

    function TVehiculo.GetTieneAcoplado(): Boolean;
    begin
        Result := GetField('TieneAcoplado');
    end;

    procedure TVehiculo.SetTieneAcoplado(Valor:Boolean);
    begin
        SetField('TieneAcoplado', Valor);
    end;

    function TVehiculo.GetDigitoVerificadorValido(): Boolean;
    begin
        Result := GetField('DigitoVerificadorValido');
    end;

    procedure TVehiculo.SetDigitoVerificadorValido(Valor:Boolean);
    begin
        SetField('DigitoVerificadorValido', Valor);
    end;

    function TVehiculo.GetFechaHoraModificacion(): TDateTime;
    var
        t:Variant;
    begin
        t := GetField('FechaHoraModificacion');
        Result := iif(t= null, NullDate, TDateTime(t));
    end;

    procedure TVehiculo.SetFechaHoraModificacion(Valor:TDateTime);
    begin
        SetField('FechaHoraModificacion', Valor);
    end;

    function TVehiculo.GetRobado(): Boolean;
    begin
        Result := GetField('Robado');
    end;

    procedure TVehiculo.SetRobado(Valor:Boolean);
    begin
        SetField('Robado', Valor);
    end;
{$ENDREGION}

    function TVehiculo.Obtener(Patente:string; CodigoTipoPatente:string): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := '';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@Patente').Value := Trim(Patente);
           sp.Parameters.ParamByName('@CodigoTipoPatente').Value := Trim(CodigoTipoPatente);

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;


end.
