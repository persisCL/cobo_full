unit PlanesTarifariosDiasTipos;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TTarifasDiasTipos = Class(TClaseBase)

    private
        function GetCodigoDiaTipo(): string;
        procedure SetCodigoDiaTipo(value: string);

        function GetDescripcion(): string;
        procedure SetDescripcion(value: string);

        function GetCodigoDiaTipoConcesionaria(): Variant;
        procedure SetCodigoDiaTipoConcesionaria(value: Variant);

        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

    public
        property CodigoDiaTipo: string read GetCodigoDiaTipo write SetCodigoDiaTipo;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property CodigoDiaTipoConcesionaria: Variant read GetCodigoDiaTipoConcesionaria write SetCodigoDiaTipoConcesionaria;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;

        function Obtener(): TClientDataSet;
    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TTarifasDiasTipos.GetCodigoDiaTipo(): string;
    begin
        Result := GetField('CodigoDiaTipo');
    end;

    procedure TTarifasDiasTipos.SetCodigoDiaTipo(value: string);
    begin
        SetField('CodigoDiaTipo', value);
    end;

    function TTarifasDiasTipos.GetDescripcion(): string;
    begin
        Result := GetField('Descripcion');
    end;

    procedure TTarifasDiasTipos.SetDescripcion(value: string);
    begin
        SetField('Descripcion', value);
    end;

    function TTarifasDiasTipos.GetCodigoDiaTipoConcesionaria(): Variant;
    begin
        Result := GetField('CodigoDiaTipoConcesionaria');
    end;

    procedure TTarifasDiasTipos.SetCodigoDiaTipoConcesionaria(value: Variant);
    begin
        SetField('CodigoDiaTipoConcesionaria', value);
    end;

    function TTarifasDiasTipos.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TTarifasDiasTipos.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TTarifasDiasTipos.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TTarifasDiasTipos.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TTarifasDiasTipos.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := GetField('UsuarioModificacion');
    end;

    procedure TTarifasDiasTipos.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TTarifasDiasTipos.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := GetField('FechaHoraModificacion');
    end;

    procedure TTarifasDiasTipos.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;
{$ENDREGION}

    function TTarifasDiasTipos.Obtener(): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseCAC;
           sp.ProcedureName := 'PlanTarifarioDiasTipos_Obtener';
           sp.Parameters.Refresh;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
end.
