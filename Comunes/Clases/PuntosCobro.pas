//TASK_109_JMA_20170215
unit PuntosCobro;

interface

uses ClaseBase, DB, ADODB, SysUtils, DMConnection, Variants, Classes, DBClient, Provider, Util;

type
    TPuntoCobro = Class(TClaseBase)

    private
        function GetNumeroPuntoCobro(): Byte;
        procedure SetNumeroPuntoCobro(value: Byte);

        function GetDomainID(): SmallInt;
        procedure SetDomainID(value: SmallInt);

        function GetTSMCID(): Byte;
        procedure SetTSMCID(value: Byte);

        function GetDescripcion(): string;
        procedure SetDescripcion(value: string);

        function GetCodigoEjeVial(): Byte;
        procedure SetCodigoEjeVial(value: Byte);

        function GetSentido(): string;
        procedure SetSentido(value: string);

        function GetProgresivaMetros(): Double;
        procedure SetProgresivaMetros(value: Double);

        function GetTSID(): Byte;
        procedure SetTSID(value: Byte);

        function GetPrefijoImagen(): string;
        procedure SetPrefijoImagen(value: string);

        function GetPuntoCobroAnterior(): Byte;
        procedure SetPuntoCobroAnterior(value: Byte);

        function GetCantidadCarriles(): Byte;
        procedure SetCantidadCarriles(value: Byte);

        function GetUltimoPorticoSentido(): Boolean;
        procedure SetUltimoPorticoSentido(value: Boolean);

        function GetPathFotosViajes(): string;
        procedure SetPathFotosViajes(value: string);

        function GetUltimaEjecucionLoader(): TDateTime;
        procedure SetUltimaEjecucionLoader(value: TDateTime);

        function GetCodigoPCConcesionaria(): Byte;
        procedure SetCodigoPCConcesionaria(value: Byte);

        function GetCodigoJuzgado(): Integer;
        procedure SetCodigoJuzgado(value: Integer);

        function GetKms(): Double;
        procedure SetKms(value: Double);
			
        function GetUsuarioCreacion(): string;
        procedure SetUsuarioCreacion(value: string);

        function GetFechaHoraCreacion(): TDateTime;
        procedure SetFechaHoraCreacion(value: TDateTime);

        function GetUsuarioModificacion(): string;
        procedure SetUsuarioModificacion(value: string);

        function GetFechaHoraModificacion(): TDateTime;
        procedure SetFechaHoraModificacion(value: TDateTime);

        function GetIDCategoriaClase(): Integer;
        procedure SetIDCategoriaClase(value: Integer);


    public
        property NumeroPuntoCobro: Byte read GetNumeroPuntoCobro write SetNumeroPuntoCobro;
        property DomainID: SmallInt read GetDomainID write SetDomainID;
        property TSMCID: Byte read GetTSMCID write SetTSMCID;
        property Descripcion: string read GetDescripcion write SetDescripcion;
        property CodigoEjeVial: Byte read GetCodigoEjeVial write SetCodigoEjeVial;
        property Sentido: string read GetSentido write SetSentido;
        property ProgresivaMetros: Double read GetProgresivaMetros write SetProgresivaMetros;
        property TSID: Byte read GetTSID write SetTSID;
        property PrefijoImagen: string read GetPrefijoImagen write SetPrefijoImagen;
        property PuntoCobroAnterior: Byte read GetPuntoCobroAnterior write SetPuntoCobroAnterior;
        property CantidadCarriles: Byte read GetCantidadCarriles write SetCantidadCarriles;
        property UltimoPorticoSentido: Boolean read GetUltimoPorticoSentido write SetUltimoPorticoSentido;
        property PathFotosViajes: string read GetPathFotosViajes write SetPathFotosViajes;
        property UltimaEjecucionLoader: TDateTime read GetUltimaEjecucionLoader write SetUltimaEjecucionLoader;
        property CodigoPCConcesionaria: Byte read GetCodigoPCConcesionaria write SetCodigoPCConcesionaria;
        property CodigoJuzgado: Integer read GetCodigoJuzgado write SetCodigoJuzgado;
        property Kms: Double read GetKms write SetKms;
        property UsuarioCreacion: string read GetUsuarioCreacion write SetUsuarioCreacion;
        property FechaHoraCreacion: TDateTime read GetFechaHoraCreacion write SetFechaHoraCreacion;
        property UsuarioModificacion: string read GetUsuarioModificacion write SetUsuarioModificacion;
        property FechaHoraModificacion: TDateTime read GetFechaHoraModificacion write SetFechaHoraModificacion;
        property IDCategoriaClase: Integer read GetIDCategoriaClase write SetIDCategoriaClase;

        function Obtener_from_BO_Raiting(CodigoConcesionaria: Byte): TClientDataSet;
        function ObtenerPlanTarifarioVersionesPuntosCobro(Id_PlanTarifarioVersiones: LongInt): TClientDataSet;
    end;

implementation

{$REGION 'GETTERS y SETTERS'}
    function TPuntoCobro.GetNumeroPuntoCobro(): Byte;
    begin
        Result := GetField('NumeroPuntoCobro');
    end;

    procedure TPuntoCobro.SetNumeroPuntoCobro(value: Byte);
    begin
        SetField('NumeroPuntoCobro', value);
    end;

    function TPuntoCobro.GetDomainID(): SmallInt;
    begin
        Result := GetField('DomainID');
    end;

    procedure TPuntoCobro.SetDomainID(value: SmallInt);
    begin
        SetField('DomainID', value);
    end;

    function TPuntoCobro.GetTSMCID(): Byte;
    begin
        Result := GetField('TSMCID');
    end;

    procedure TPuntoCobro.SetTSMCID(value: Byte);
    begin
        SetField('TSMCID', value);
    end;

    function TPuntoCobro.GetDescripcion(): string;
    var
        t: Variant;
    begin
        t := GetField('Descripcion');
        if t = null then
            t := '';
        Result := t;
    end;

    procedure TPuntoCobro.SetDescripcion(value: string);
    begin
        SetField('Descripcion', value);
    end;

    function TPuntoCobro.GetCodigoEjeVial(): Byte;
    begin
        Result := GetField('CodigoEjeVial');
    end;

    procedure TPuntoCobro.SetCodigoEjeVial(value: Byte);
    begin
        SetField('CodigoEjeVial', value);
    end;

    function TPuntoCobro.GetSentido(): string;
    begin
        Result := GetField('Sentido');
    end;

    procedure TPuntoCobro.SetSentido(value: string);
    begin
        SetField('Sentido', value);
    end;

    function TPuntoCobro.GetProgresivaMetros(): Double;
    var
        t: Variant;
    begin
        t := GetField('ProgresivaMetros');
        if t = null then
            t := 0;
        Result := t;
    end;

    procedure TPuntoCobro.SetProgresivaMetros(value: Double);
    begin
        SetField('ProgresivaMetros', value);
    end;

    function TPuntoCobro.GetTSID(): Byte;
    var
        t: Variant;
    begin
        t := GetField('TSID');
        if t = null then
            t := 0;
        Result := t;
    end;

    procedure TPuntoCobro.SetTSID(value: Byte);
    begin
        SetField('TSID', value);
    end;

    function TPuntoCobro.GetPrefijoImagen(): string;
    var
        t: Variant;
    begin
        t := GetField('PrefijoImagen');
        if t = null then
            t := '';
        Result := Trim(t);
    end;

    procedure TPuntoCobro.SetPrefijoImagen(value: string);
    begin
        SetField('PrefijoImagen', value);
    end;

    function TPuntoCobro.GetPuntoCobroAnterior(): Byte;
    var
        t: Variant;
    begin
        t := GetField('PuntoCobroAnterior');
        if t = null then
            t := 0;
        Result := t;
    end;

    procedure TPuntoCobro.SetPuntoCobroAnterior(value: Byte);
    begin
        SetField('PuntoCobroAnterior', value);
    end;

    function TPuntoCobro.GetCantidadCarriles(): Byte;
    var
        t: Variant;
    begin
        t := GetField('CantidadCarriles');
        if t = null then
            t := 0;
        Result := t;
    end;

    procedure TPuntoCobro.SetCantidadCarriles(value: Byte);
    begin
        SetField('CantidadCarriles', value);
    end;

    function TPuntoCobro.GetUltimoPorticoSentido(): Boolean;
    begin
        Result := GetField('UltimoPorticoSentido');
    end;

    procedure TPuntoCobro.SetUltimoPorticoSentido(value: Boolean);
    begin
        SetField('UltimoPorticoSentido', value);
    end;

    function TPuntoCobro.GetPathFotosViajes(): string;
    var
        t: Variant;
    begin
        t := GetField('PathFotosViajes');
        if t = null then
            t := '';
        Result := Trim(t);
    end;

    procedure TPuntoCobro.SetPathFotosViajes(value: string);
    begin
        SetField('PathFotosViajes', value);
    end;

    function TPuntoCobro.GetUltimaEjecucionLoader(): TDateTime;
    var
        t: Variant;
    begin
        t := GetField('UltimaEjecucionLoader');
        if t = null then
            t := NullDate;
        Result := t;
    end;

    procedure TPuntoCobro.SetUltimaEjecucionLoader(value: TDateTime);
    begin
        SetField('UltimaEjecucionLoader', value);
    end;

    function TPuntoCobro.GetCodigoPCConcesionaria(): Byte;
    var
        t: Variant;
    begin
        t := GetField('CodigoPCConcesionaria');
        if t = null then
            t := 0;
        Result := t;
    end;

    procedure TPuntoCobro.SetCodigoPCConcesionaria(value: Byte);
    begin
        SetField('CodigoPCConcesionaria', value);
    end;

    function TPuntoCobro.GetCodigoJuzgado(): Integer;
    var
        t: Variant;
    begin
        t := GetField('CodigoJuzgado');
        if t = null then
            t := 0;
        Result := t;
    end;

    procedure TPuntoCobro.SetCodigoJuzgado(value: Integer);
    begin
        SetField('CodigoJuzgado', value);
    end;

    function TPuntoCobro.GetKms(): Double;
    var
        t: Variant;
    begin
        t := GetField('Kms');
        if t = null then
            t := 0.0;
        Result := t;
    end;

    procedure TPuntoCobro.SetKms(value: Double);
    begin
        SetField('Kms', value);
    end;


    function TPuntoCobro.GetUsuarioCreacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioCreacion');
        if t = null then
           t := '';
        Result :=  t;
    end;

    procedure TPuntoCobro.SetUsuarioCreacion(value: string);
    begin
        SetField('UsuarioCreacion', value);
    end;

    function TPuntoCobro.GetFechaHoraCreacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraCreacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TPuntoCobro.SetFechaHoraCreacion(value: TDateTime);
    begin
        SetField('FechaHoraCreacion', value);
    end;

    function TPuntoCobro.GetUsuarioModificacion(): string;
    var
        t: Variant;
    begin
        t:= GetField('UsuarioModificacion');
        if t = null then
           t := '';
        Result := t;
    end;

    procedure TPuntoCobro.SetUsuarioModificacion(value: string);
    begin
        SetField('UsuarioModificacion', value);
    end;

    function TPuntoCobro.GetFechaHoraModificacion(): TDateTime;
    var
        t: Variant;
    begin
        t:= GetField('FechaHoraModificacion');
        if t = null then
           t := NullDate;
        Result := t;
    end;

    procedure TPuntoCobro.SetFechaHoraModificacion(value: TDateTime);
    begin
        SetField('FechaHoraModificacion', value);
    end;

    function TPuntoCobro.GetIDCategoriaClase(): Integer;
    begin
        Result := GetField('IDCategoriaClase');
    end;

    procedure TPuntoCobro.SetIDCategoriaClase(value: Integer);
    begin
        SetField('IDCategoriaClase', value);
    end;
{$ENDREGION}

    function TPuntoCobro.Obtener_from_BO_Raiting(CodigoConcesionaria: Byte): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseBO_Rating;
           sp.ProcedureName := 'PlanTarifarioPuntosCobroConcesionaria_Obtener';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;

    function TPuntoCobro.ObtenerPlanTarifarioVersionesPuntosCobro(Id_PlanTarifarioVersiones: LongInt): TClientDataSet;
    var
        sp: TADOStoredProc;
        ex: Exception;
    begin
        Result := nil;
        try

           sp := TADOStoredProc.Create(nil);
           sp.Connection := DMConnections.BaseBO_Rating;
           sp.ProcedureName := 'PlanTarifarioVersionesPuntosCobro_Obtener';
           sp.Parameters.Refresh;
           sp.Parameters.ParamByName('@Id_PlanTarifarioVersiones').Value := Id_PlanTarifarioVersiones;

           sp.Open;

           if sp.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           begin
               ex := Exception.Create(sp.Parameters.ParamByName('@ErrorDescription').Value);
               raise ex;
           end;

           Result:= CrearClientDataSet(sp);

        finally
            FreeAndNil(sp);
        end;

    end;
end.
