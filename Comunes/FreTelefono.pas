{*******************************************************************************
Revisi�n 1:
    Author      : FSandi
    Date        : 14-06-2007
    Description : Se verifica que no nos este llegan un valor null de horahasta en la funcion GetHorarioHasta, en caso de ser
                asi, lo convertimos a nulldate (hora 00:00), los valores almacenados por defecto a algunos medios de contacto,
                dependen de los par�metros HORA_DESDE y HORA_HASTA de PeaTypes.pas
*******************************************************************************}
unit FreTelefono;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Validate, VariantComboBox, StdCtrls, Peatypes, Peaprocs, RStrings, Util,
  UtilDB, UtilProc,ADODB;

type
  TFrameTelefono = class(TFrame)
    lblTelefono: TLabel;
    txtTelefono: TEdit;
    cbTipoTelefono: TVariantComboBox;
    lblDesde: TLabel;
    lblHasta: TLabel;
    cbDesde: TComboBox;
    cbHasta: TComboBox;
    cbCodigosArea: TComboBox;
    procedure txtTelefonoKeyPress(Sender: TObject; var Key: Char);
    procedure cbDesdeChange(Sender: TObject);
    procedure cbHastaChange(Sender: TObject);
    procedure cbCodigosAreaChange(Sender: TObject);
    procedure txtTelefonoExit(Sender: TObject);
    procedure cbTipoTelefonoExit(Sender: TObject);
  private
    FEsObligatorio: boolean;
    FPedirHora: boolean;
    FPedirTipoTelefono: boolean;
    FCodigoMedioComunicacion: Integer;
    { Private declarations }
    function GetCodigoArea: integer;
    function GetNumeroTelefono: AnsiString;
    function GetCodigoTipoTelefono: integer;
    function GetDescriTipoTelefono: AnsiString;
    function GetHorarioDesde: TDateTime;
    function GetHorarioHasta: TDateTime;
    procedure SetEsObligatorio(const Value: boolean);
    procedure FormatearObligatorio(Obligatorio: boolean);
    procedure CargarComboHora(Combo:TComboBox;Desde:Integer;Posicion:Integer=0; EsDesde: boolean = false);
    function GetCargoTelelefono:Boolean;
    function EsNumeroTelefonoValido(CodigoArea:Integer; Telefono:AnsiString;Var DescripError:String):Boolean;
    function GetAnexo:Integer;
    procedure SetCodigoMedioComunicacion(const Value: Integer);
    function GetMedioComunicacion: TTipoMedioComunicacion;
  public
    { Public declarations }
   property Anexo:Integer read GetAnexo;
   property CargoTelefono:Boolean read GetCargoTelelefono;
   property CodigoArea: integer read GetCodigoArea;
   property NumeroTelefono: AnsiString read GetNumeroTelefono;
   property CodigoTipoTelefono: integer read GetCodigoTipoTelefono;
   property DescriTipoTelefono: AnsiString read GetDescriTipoTelefono;
   property HorarioDesde: TDateTime read GetHorarioDesde;
   property HorarioHasta: TDateTime read GetHorarioHasta;
   property EsObligatorio: boolean read FEsObligatorio write SetEsObligatorio;
   property CodigoMedioComunicacion: Integer read FCodigoMedioComunicacion write SetCodigoMedioComunicacion;
   property MedioComunicacion: TTipoMedioComunicacion read GetMedioComunicacion;

   procedure Inicializa(CaptionLblTelefono: AnsiString='Tel�fono principal:';
                            Obligatorio: boolean = false; PedirHora: boolean = true; PedirTipoTelefono: boolean = true);
   procedure CargarTelefono(codArea: integer; nroTel: AnsiString; codTipoTelefono: integer;
                desde:TDateTime=NullDate; hasta: TDateTime=NullDate; CodigoMedioComunicacion: Integer = -1);
   function ValidarTelefono: boolean;
   function TelefonoValido: Boolean;
   procedure Limpiar;
  end;

implementation

uses DMConnection, DB;

var
    CodigosAreaCargados: boolean;
{$R *.dfm}

{ TFrameTelefono }

procedure TFrameTelefono.CargarTelefono(codArea: integer;
  nroTel: AnsiString; codTipoTelefono: integer; desde, hasta: TDateTime; CodigoMedioComunicacion: Integer);
var
    hdesde, hhasta:integer;
begin
    hdesde:=iif(desde=nulldate,strtoint(strleft(HORA_DESDE,2)),strtoint(FormatDateTime('hh',desde)));
    if hasta=NullDate then
        hhasta:=strtoint(strleft(HORA_HASTA,2))
    else begin
        if (FormatDateTime('hh',hasta)='23') and (FormatDateTime('nn',hasta)='59') then
            hhasta:=24
        else
            hhasta:=strtoint(FormatDateTime('hh',hasta));
    end;
    //Firma: TASK_001_AUN_20170308-CU.COBO.ADM.COB.102: Comienzo
    if not CodigosAreaCargados then
        CargarCodigosAreaTelefono(DMConnections.BaseCAC, cbCodigosArea, codArea);
    //Firma: TASK_001_AUN_20170308-CU.COBO.ADM.COB.102: Fin
    txtTelefono.Text := Trim(nroTel);
    MarcarItemComboVariant(cbTipoTelefono, codTipoTelefono);
    if FEsObligatorio then begin
        CargarComboHora(cbDesde,0,hdesde, true);
        CargarComboHora(cbHasta,0,hhasta, false);   // PAR00114_20120308_0838_Nombre_Cliente_No_Corresponde_Comprobante
    end;
    FCodigoMedioComunicacion := CodigoMedioComunicacion;
end;

procedure TFrameTelefono.FormatearObligatorio(Obligatorio: boolean);
begin
    if Obligatorio then begin
        lblTelefono.Font.Color := COLOR_LABEL_OBLIGATORIO;
        lblTelefono.Font.Style := STYLE_LABEL_OBLIGATORIO;
        cbCodigosArea.Color := COLOR_EDITS_OBLIGATORIO;
        txtTelefono.Color := COLOR_EDITS_OBLIGATORIO;
        if FPedirTipoTelefono then
            cbTipoTelefono.Color := COLOR_EDITS_OBLIGATORIO;
    end else begin
        lblTelefono.Font.Color := COLOR_LABEL_NO_OBLIGATORIO;
        lblTelefono.Font.Style := STYLE_LABEL_NO_OBLIGATORIO;
        cbCodigosArea.Color := COLOR_EDITS_NO_OBLIGATORIO;
        txtTelefono.Color := COLOR_EDITS_NO_OBLIGATORIO;
        cbTipoTelefono.Color := COLOR_EDITS_NO_OBLIGATORIO;
    end;
end;

function TFrameTelefono.GetCodigoArea: integer;
begin
    result := StrToInt(Trim(StrLeft(cbCodigosArea.Text,5)));
end;

function TFrameTelefono.GetCodigoTipoTelefono: integer;
begin
    result := iif(cbTipoTelefono.Visible, cbTipoTelefono.Value, TIPO_MEDIO_CONTACTO_TE_PARTICULAR);
end;

function TFrameTelefono.GetDescriTipoTelefono: AnsiString;
begin
    result := cbTipoTelefono.Items.Items[cbTipoTelefono.itemindex].Caption;
end;

function TFrameTelefono.GetHorarioDesde: TDateTime;
begin
    result:= StrToTime(cbDesde.Text);
end;

function TFrameTelefono.GetHorarioHasta: TDateTime;
begin
//Revisi�n 1
    if cbHasta.Text = EmptyStr then begin
        result:= StrToTime('00:00');
    end else begin
        result:= StrToTime(iif(StrLeft(cbHasta.Text,2)='24','23:59',cbHasta.Text));
    end;
//Fin de Revisi�n 1
end;

function TFrameTelefono.GetNumeroTelefono: AnsiString;
begin
    result := Trim(txtTelefono.Text);
end;

procedure TFrameTelefono.Inicializa(CaptionLblTelefono: AnsiString ='Tel�fono principal:';
  Obligatorio: boolean = false; PedirHora: boolean = true; PedirTipoTelefono: boolean = true);
begin
    {PARAMETROS:
        - CaptionLblTelefono: puede que quieran poner "Telefono principal" o "Telefono alternativo"
        - Obligatorio: es si todo el tel�fono es obligatorio. Entonces va color celeste
        - PedirHora:
    }
    Caption := LimpiarCaracteresLabel(CaptionLblTelefono);

    if (trim(CaptionLblTelefono) <> '') then
        lblTelefono.Caption := CaptionLblTelefono;
    lblDesde.Visible := PedirHora;
    lblHasta.Visible := PedirHora;
    cbDesde.Visible  := PedirHora;
    cbHasta.Visible  := PedirHora;
    //Firma: TASK_001_AUN_20170308-CU.COBO.ADM.COB.102: Comienzo
    FPedirTipoTelefono := PedirTipoTelefono;
    cbTipoTelefono.Visible := FPedirTipoTelefono;
    if not FPedirTipoTelefono then txtTelefono.Width := txtTelefono.Width + cbTipoTelefono.Width;
    //Firma: TASK_001_AUN_20170308-CU.COBO.ADM.COB.102: Fin
    FPedirHora := PedirHora;
    FEsObligatorio := Obligatorio;
    FormatearObligatorio(FEsObligatorio);

    if Obligatorio then begin
        if PedirHora then GrabarComponentesObligatoriosyHints(self,DMConnections.BaseCAC,[cbTipoTelefono,cbCodigosArea,txtTelefono])
        else GrabarComponentesObligatoriosyHints(self,DMConnections.BaseCAC,[cbTipoTelefono,cbCodigosArea,txtTelefono,cbDesde,cbHasta]);
    end else begin
        if PedirHora then GrabarComponentesObligatoriosyHints(self,DMConnections.BaseCAC,[])
        else GrabarComponentesObligatoriosyHints(self,DMConnections.BaseCAC,[cbDesde,cbHasta]);
    end;
    ObtenerComponentesObligatoriosyHints(self,DMConnections.BaseCAC);
    CargarCodigosAreaTelefono(DMConnections.BaseCAC, cbCodigosArea);
    CodigosAreaCargados := true;
    Limpiar;
end;

procedure TFrameTelefono.SetEsObligatorio(const Value: boolean);
begin
    FEsObligatorio := value;
end;

function TFrameTelefono.ValidarTelefono: boolean;
var
    HoraOk,NumeroOk, TipoTelefonoOk: boolean;
    DescripErrorTel:String;

begin
    if FPedirHora then begin
        HoraOk := (trim(cbHasta.Text)<>'') and (strtoint(StrLeft(cbDesde.Text,2)) <= strtoint(StrLeft(cbHasta.Text,2)));
    end
    else begin
        HoraOk := true;
    end;
    if FPedirTipoTelefono then begin
        if trim(txtTelefono.text) <> '' then
            TipoTelefonoOk := (cbTipoTelefono.ItemIndex > 0)
        else
            TipoTelefonoOk := (cbTipoTelefono.ItemIndex > 0) or (not EsComponenteObligatorio(cbTipoTelefono));
    end
    else begin
        TipoTelefonoOk := true;
    end;

    NumeroOk := EsNumeroTelefonoValido(self.CodigoArea,self.NumeroTelefono,DescripErrorTel);

    result := ValidateControls([txtTelefono, txtTelefono, cbTipoTelefono, cbCodigosArea, cbDesde],
                      [ (EsComponenteObligatorio(txtTelefono) and (trim(txtTelefono.Text)<>'')) or not(EsComponenteObligatorio(txtTelefono)),
                        (EsComponenteObligatorio(txtTelefono) and (NumeroOk)) or (trim(txtTelefono.Text)='') or (NumeroOk),
                        TipoTelefonoOk,
                        (EsComponenteObligatorio(cbCodigosArea) and (cbCodigosArea.ItemIndex >= 0)) or not(EsComponenteObligatorio(cbCodigosArea)),
                        (EsComponenteObligatorio(cbDesde) and (HoraOk)) or not(EsComponenteObligatorio(cbDesde))],
                        lblTelefono.Caption,
                        [Format(MSG_VALIDAR_DEBE_EL,[lblTelefono.Caption]),
                        DescripErrorTel,
                        MSG_VALIDAR_TIPO_TELEFONO,
                        MSG_VALIDAR_CODIGO_AREA,
                        MSG_RANGO_HORARIO]);
end;

procedure TFrameTelefono.txtTelefonoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9',#8]) then
        Key := #0;
end;

function TFrameTelefono.GetCargoTelelefono: Boolean;
begin
    result:=trim(self.NumeroTelefono)<>'';
end;

procedure TFrameTelefono.Limpiar;
begin
//    CargarCodigosAreaTelefono(DMConnections.BaseCAC, cbCodigosArea);

    if cbCodigosArea.ItemIndex = -1 then cbCodigosArea.ItemIndex := 0;
    CargarTiposMedioContacto(DMConnections.BaseCAC, cbTipoTelefono, 0, TMC_TELEFONO, False, True);

    txtTelefono.Clear;
    cbCodigosArea.ItemIndex := 0;
    CargarComboHora(cbDesde,0,strtoint(strleft(HORA_DESDE,2)), true);//HORA_DESDE);
    CargarComboHora(cbHasta,0,strtoint(strleft(HORA_Hasta,2)), false);  // PAR00114_20120308_0838_Nombre_Cliente_No_Corresponde_Comprobante
end;

procedure TFrameTelefono.cbDesdeChange(Sender: TObject);
begin
    //if cbDesde.ItemIndex+1 < cbDesde.Items.Count then begin
        if (cbDesde.ItemIndex >= cbHasta.itemindex) then
            cbHasta.ItemIndex := cbDesde.ItemIndex;
            //CargarComboHora(cbHasta,cbDesde.ItemIndex+1,cbDesde.ItemIndex+1);
    //end; //es igual a la cantidad, o sea que es la �ltima hora, marco tambien hasta la ultima hora.
    //else CargarComboHora(cbHasta,1,cbDesde.ItemIndex, false);
end;


procedure TFrameTelefono.CargarComboHora(Combo:TComboBox;Desde:Integer;Posicion:Integer=0; EsDesde: boolean = false);
var
    x,i:integer;
    maxHora: integer;
begin
    if EsDesde then
        maxHora := 23
    else
        maxHora := 24;
    with Combo do begin
        x:=Posicion;
        clear;
        for i:=desde to maxHora do begin
                Items.Add(StrRight('0'+inttostr(i),2)+':00');
                if i=Posicion then x:=i;
        end;
        Combo.ItemIndex:=x-desde;
    end;
end;

function TFrameTelefono.TelefonoValido: Boolean;
var
    ok: boolean;
    DescripError:String;
begin
    ok := EsNumeroTelefonoValido(self.CodigoArea, self.NumeroTelefono, DescripError);

    if not ok then
        MsgBoxBalloon(DescripError, lblTelefono.Caption, MB_ICONWARNING, txtTelefono);

    result := ok;
end;

procedure TFrameTelefono.cbHastaChange(Sender: TObject);
begin
    if (cbDesde.ItemIndex >= cbHasta.itemindex) then
        cbDesde.ItemIndex := cbHasta.ItemIndex;
end;

procedure TFrameTelefono.cbCodigosAreaChange(Sender: TObject);
begin
    if trim(StrRight(cbCodigosArea.Text,4))=TIPO_LINEA_CELULAR then begin
        cbTipoTelefono.ItemIndex := 3;
    end;
end;

function TFrameTelefono.EsNumeroTelefonoValido(CodigoArea:Integer; Telefono:AnsiString;
        Var DescripError:String):Boolean;
var
    SP: TADOStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    try
        try
            SP.Connection := DMConnections.BaseCAC;
            with SP, Parameters do begin
                ProcedureName := 'ValidarTelefonoCorrecto';
                CreateParameter('@CodigoArea', ftInteger, pdInput, 0, CodigoArea);
                CreateParameter('@Telefono',  ftString, pdInput, 255, Telefono);
            end;

            sp.Open;

            result:=sp.FieldByName('Valido').AsInteger=1;
            DescripError:=sp.FieldByName('DescripcionError').AsString;
        except
            result:= false;
            DescripError:=SIN_ESPECIFICAR;
        end;

    finally
        //listo
	    SP.Free;
    end;
end;

procedure TFrameTelefono.txtTelefonoExit(Sender: TObject);
begin
    if self.NumeroTelefono <> '' then
        self.TelefonoValido;

end;

procedure TFrameTelefono.cbTipoTelefonoExit(Sender: TObject);
begin
    if (self.NumeroTelefono<>'') and (self.CodigoTipoTelefono<1) then MsgBoxBalloon(format(MSG_VALIDAR_DEBE_EL,[FLD_TIPO_TELEFONO]), lblTelefono.Caption, MB_ICONWARNING, cbTipoTelefono);
end;

function TFrameTelefono.GetAnexo: Integer;
begin
    result:=-1;
end;

procedure TFrameTelefono.SetCodigoMedioComunicacion(const Value: Integer);
begin
  FCodigoMedioComunicacion := Value;
end;

function TFrameTelefono.GetMedioComunicacion: TTipoMedioComunicacion;
begin
        Result.CodigoTipoMedioContacto := CodigoTipoTelefono; //Self.CodigoMedioComunicacion;
        Result.CodigoArea := self.CodigoArea;
        Result.Valor := self.NumeroTelefono;
        Result.Anexo := self.Anexo;
        Result.HoraDesde := Self.HorarioDesde;
        Result.HoraHasta := Self.HorarioHasta;
        Result.Indice := 0;
        Result.Principal := Self.EsObligatorio;
end;

end.
