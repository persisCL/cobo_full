unit CobranzasClasses;

interface

USes PeaTypes, Controls;

type
  TEntradaDatosCuota = record
    Saldo,
    CodigoCuotaAntecesora: Integer;
    CuotaNueva,
    CuotaModificada,
    EstadoImpago,
    EnTramite      : Boolean
  end;

  TEntradaDatos = record
    CodigoTipoEntrada,
    CodigoCanalPago,
    CodigoFormaPago        : Byte;
    CodigoEntradaUsuario   : string[02];
    TitularNombre          : string[100];
    TitularTipoDocumento   : string[04];
    TitularNumeroDocumento : string[11];
    CodigoBanco            : Integer;
    DocumentoNumero        : string[20];
    Fecha,
    FechaPago: TDate;
    CodigoTarjeta,
    TarjetaCuotas,
    Importe,
    ImportePagado          : Integer;
    Persona                : TDatosPersonales;
    PersonaNombreCompleto  : String;
    DatosCuota             : TEntradaDatosCuota;
    DescripcionFormaPago   : string[30];
    DescripcionBanco,
    DescripcionTarjeta     : string[50];
  end;

    TDocumentosImpresosRefinanciacion = record
        DocumentoPrincipal,
        AnexoCuotas,
        AnexoComprobantes,
        AnexoComprobantes2: string;
    end;

implementation

end.
