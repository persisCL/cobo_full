{-------------------------------------------------------------------------------
 File Name: FrmReclamoContactarCliente.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos
 
 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 29-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y SubTipoReclamo

 Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos
-------------------------------------------------------------------------------}
unit FrmReclamoContactarCliente;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  PeaProcs,                       //serial number to etiqueta
  FreContactoReclamo,             //Frame contacto
  FreCompromisoOrdenServicio,     //Frame compromiso
  FreSolucionOrdenServicio,       //Frame solucion
  FreHistoriaOrdenServicio,       //Frame Historia
  FreFuenteReclamoOrdenServicio,  //Frame Fuente Reclamo
  FreNumeroOrdenServicio,         //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,      //Frame Respuesta Orden Servicio 
  OrdenesServicio,                //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, {FreSubTipoReclamoOrdenServicio,}                            // _PAN_
  FreConcesionariaReclamoOrdenServicio;

type
  TFormReclamoContactarCliente = class(TForm)
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
    Label1: TLabel;
    txtDetalle: TMemo;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio;
    Lreferencia: TLabel;
    EReferencia: TEdit;
    PContactarCliente: TPanel;
    cknotificado: TCheckBox;
    ckconforme: TCheckBox;
    LVer: TLabel;
    spObtenerOrdenServicio: TADOStoredProc;
    spObtenerOrdenServicioContactarCliente: TADOStoredProc;
    spObtenerOrdenesServicioTransito: TADOStoredProc;
    PAbajo: TPanel;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    Pdivisor2: TPanel;
    Pdivisor: TPanel;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
{    FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;}     // _PAN_
    spObtenerOrdenesServicioEstacionamiento: TADOStoredProc;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LVerClick(Sender: TObject);
  private
    { Private declarations }
    FTipoOrdenServicio : Integer;
    FCodigoOrdenServicio : Integer;
    FEditando : Boolean; //Indica si estoy editando o no
    Function GetCodigoOrdenServicio : Integer;
  public
    { Public declarations }
    Function Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0) : Boolean;
    Function InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
    Property CodigoOrdenServicio: Integer Read GetCodigoOrdenServicio;
  end;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoContactarCliente.Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0): Boolean;
Resourcestring
    MSG_ERROR_INIT = 'Error al inicializar Contactar Cliente';
    MSG_ERROR = 'Error';
Var
    Sz: TSize;
begin
    FTipoOrdenServicio := TipoOrdenServicio;
    try
        //Centro el form
        CenterForm(Self);
        //ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);
        //titulo
        Caption := QueryGetValue(DMConnections.BaseCAC, Format(
                   'SELECT dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
                   [TipoOrdenServicio]));
        PageControl.ActivePageIndex := 0;
        ActiveControl := txtDetalle;
        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(10) and
                                FrameSolucionOrdenServicio1.Inicializar and
                                    FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar{ and        // _PAN_
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar};             // _PAN_
                                        //---------------------------------------------------------------

        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    FrameNumeroOrdenServicio1.Visible := False;
    FEditando := False; //No estoy Editando
    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso := ObtenerFechaComprometidaOS(now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, FrameCompromisoOrdenServicio1.Prioridad);
    AddOSForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:  DDeMarco
  Date Created:
  Description: Modifico Reclamo que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoContactarCliente.InicializarEditando(CodigoOrdenServicio: Integer): Boolean;

    //form read Only
    Procedure ReadOnly;
    begin
      Pcontactarcliente.Enabled := False;
      FrameRespuestaOrdenServicio1.Deshabilitar;
      CKRetenerOrden.Visible := False;
      Btncancelar.Caption := 'Salir';
      AceptarBtn.Visible := False;
    end;

    Function ObtenerInforme(CodigoOrdenServicioOriginal : Integer) : String;
    Resourcestring
        STR_RECLAMO             = 'Reclamo: ';
        STR_INFO_RECLAMO        = 'Informaci�n sobre el Reclamo: ';
        STR_SOLUCION            = 'Soluci�n: ';
        STR_BLANCO              =  '';
        STR_SEPARADOR           = '    ';
        STR_VACIO               = 'No hay.';
        //Transito
        STR_NRO_TRANSITO        = 'Numero de Tr�nsito: ';
        STR_NRO_ESTACIONAMIENTO = 'Numero de Estacionamiento: ';
        STR_RECHAZO             = ' Rechazo: ';
    var
        List: TStringList;
        //tipo
        DescTipoOrdenServicio : String;
        //General
        ObservacionesSolicitante : String;
        ObservacionesEjecutante : String;
        TipoOrdenServicio : Integer;
        //Facturacion
        Comprobante : String;
        FechaPago : String;
        LugarPago : String;
        //Cuenta
        PatenteDesconocida : String;
        PatenteFaltante : String;
        Televia : String;
    begin
        Result := '';
        try
            try
                with spObtenerOrdenServicioContactarCliente do begin
                    Close;
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicioOriginal;
                    Open;
                    if not Eof then begin
                        List:= TStringList.Create;
                        //tipo de Reclamo
                        List.Add('Tipo de Reclamo:');
                        List.Add(STR_BLANCO);
                        DescTipoOrdenServicio:= FieldByName('desctipoOrdenServicio').AsString;
                        List.Add(STR_SEPARADOR + DescTipoOrdenServicio);
                        List.Add(STR_BLANCO);
                        //Reclamo
                        List.Add(STR_RECLAMO);
                        List.Add(STR_BLANCO);
                        ObservacionesSolicitante:= FieldByName('ObservacionesSolicitante').AsString;
                        List.Add(STR_SEPARADOR + ObservacionesSolicitante);
                        List.Add(STR_BLANCO);
                        //Informaci�n del reclamo
                        List.Add(STR_INFO_RECLAMO);
                        List.Add(STR_BLANCO);
                        TipoOrdenServicio:= FieldByName('TipoOrdenServicio').AsInteger;
                        case TipoOrdenServicio of
                            //Facturacion
                            204, 205, 206, 208, 214:
                            Begin
                                Comprobante := FieldByName('TipoComprobante').AsString + ' - ' + FieldByName('NumeroComprobante').AsString;
                                FechaPago := FieldByName('FechaPago').AsString;
                                LugarPago := FieldByName('LugarPago').AsString;
                                if Comprobante <> ' - ' then List.Add(STR_SEPARADOR + 'Comprobante: ' + Comprobante );
                                if FechaPago <> '' then List.Add(STR_SEPARADOR + 'Fecha Pago: ' + FechaPago );
                                if LugarPago <> '' then List.Add(STR_SEPARADOR + 'Lugar Pago: ' + LugarPago );
                            end;
                            //Cuenta
                            211, 301, 302:
                            Begin
                                PatenteDesconocida := FieldByName('PatenteDesconocida').AsString;
                                PatenteFaltante := FieldByName('PatenteFaltante').AsString;
                                Televia := SerialNumberToEtiqueta(FieldByName('ContractSerialNumber').AsString);
                                Comprobante := FieldByName('Cuentatipocomprobante').AsString + ' - ' + FieldByName('cuentanumerocomprobante').AsString;
                                if PatenteDesconocida <> '' then List.Add(STR_SEPARADOR + 'Patente Desconocida: ' + PatenteDesconocida );
                                if PatenteFaltante <> '' then List.Add(STR_SEPARADOR + 'Patente Faltante: ' + PatenteFaltante );
                                if Televia <> '' then List.Add(STR_SEPARADOR + 'Telev�a: ' + Televia );
                                if Comprobante <> ' - ' then List.Add(STR_SEPARADOR + 'Comprobante: ' + Comprobante );
                            end;
                            //Transito
                            207:
                            Begin
                                with spObtenerOrdenesServiciotransito do begin
                                    Close;
                                    Parameters.Refresh;
                                    Parameters.ParamByName('@CodigoOrdenservicio').Value := CodigoOrdenServicioOriginal;
                                    Open;
                                    while not Eof do begin
                                        List.Add(STR_SEPARADOR + STR_NRO_TRANSITO + FieldByName('NumCorrCa').AsString + STR_SEPARADOR + STR_RECHAZO + Iif( FieldByName('Rechaza').AsString = 'True','SI','NO'));
                                        next;
                                    end;
                                    close;
                                end;
                            end;
                            //Estacionamiento
                            217:
                            Begin
                                with spObtenerOrdenesServicioEstacionamiento do begin
                                    Close;
                                    Parameters.Refresh;
                                    Parameters.ParamByName('@CodigoOrdenservicio').Value := CodigoOrdenServicioOriginal;
                                    Open;
                                    while not Eof do begin
                                        List.Add(STR_SEPARADOR + STR_NRO_ESTACIONAMIENTO + FieldByName('NumCorrEstacionamiento').AsString + STR_SEPARADOR + STR_RECHAZO + Iif( FieldByName('Rechaza').AsString = 'True','SI','NO'));
                                        next;
                                    end;
                                    close;
                                end;
                            end;
                            //General
                            1000..2000:
                            begin
                                List.Add(STR_SEPARADOR + STR_VACIO);
                            end;
                        end;
                        List.Add(STR_BLANCO);
                        //Solucion
                        List.Add(STR_SOLUCION);
                        List.Add(STR_BLANCO);
                        ObservacionesEjecutante := FieldByName('ObservacionesEjecutante').AsString;
                        List.Add(STR_SEPARADOR + Iif(ObservacionesEjecutante = '', STR_VACIO, ObservacionesEjecutante));
                        List.Add(STR_BLANCO);

                        Result := List.Text;
                    end;
                    close;
                end;


            except

            end;
        finally
            FreeAndNil(List);
        end;
    end;

var
    Estado : String;
    CodigoOrdenServicioOriginal : Integer;
begin
    FCodigoOrdenServicio := CodigoOrdenServicio;
    spObtenerOrdenServicio.Parameters.Refresh;
    spObtenerOrdenServicio.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    Result := OpenTables([spObtenerOrdenServicio]);
    if not Result then Exit;

    // Cargamos los datos
    Result := Inicializar(spObtenerOrdenServicio['TipoOrdenServicio']);

    //Obtengo la referencia
    CodigoOrdenServicioOriginal:= spObtenerOrdenServicio.FieldByName('CodigoOrdenServicioOriginal').Asinteger;

    //muestro la referencia
    Ereferencia.Text := IntToStr(CodigoOrdenServicioOriginal);

    //notificado y conforme
    Cknotificado.Checked := not (QueryGetValue(DMconnections.BaseCAC,'select dbo.ObtenerNotificadoOS('+inttostr(codigoordenservicio)+')') = 'False');
    Ckconforme.Checked := not (QueryGetValue(DMconnections.BaseCAC,'select dbo.ObtenerConformeOS('+inttostr(codigoordenservicio)+')') = 'False');

    //obtengo las observaciones del solicitante
    TxtDetalle.Text := ObtenerInforme(CodigoOrdenServicioOriginal);
    TxtDetalleSolucion.Text := spObtenerOrdenServicio.FieldByName('ObservacionesEjecutante').AsString;

    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //obtengo la historia para esa orden de servicio
    FrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio);

    //obtengo la fuente del reclamo
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //Obtengo el Estado de la OS para saber si permito editar o read only
    Estado := spObtenerOrdenServicio.FieldByName('Estado').AsString;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;

    // Listo
    spObtenerOrdenServicio.Close;

    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(IntToStr(CodigoOrdenServicio));
    CKRetenerOrden.Checked := False; //por Default no retengo la orden de servicio
    FEditando := True; //Estoy Editando

end;

{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:  DDeMarco
  Date Created:
  Description: Soluciono un Reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoContactarCliente.InicializarSolucionando(CodigoOrdenServicio : Integer): Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.ActivePageIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormReclamoContactarCliente.GetCodigoOrdenServicio: Integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LVerClick
  Author:    lgisuk
  Date Created: 15/04/2005
  Description: abre el reclamo al cual se hace referencia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoContactarCliente.LVerClick(Sender: TObject);
begin
    EditarOrdenServicio(StrToInt(EReferencia.Text));
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoContactarCliente.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: registra el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoContactarCliente.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar : Boolean;
    resourcestring
        MSG_ERROR_DET   = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FECHA = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                          'debe ser menor a la Fecha Comprometida con el Cliente';
    begin
        Result := False;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            Result := true;
            Exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = False then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;

        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802
        

        Result := True;
    end;

Var
    OS: Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    // Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra ----------------------------------
    DMConnections.BaseCAC.BeginTrans;
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.Text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaBefore,       // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaAfter,        // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteBefore,  // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteAfter    // _PAN_
                                   {FrameSubTipoReclamoOrdenServicio1}                              // _PAN_
                                   );
    //FinRev.1-------------------------------------------------------------------------

    if OS <= 0 then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Especifico de contactar cliente
    if not GuardarOrdenServicioContactarCliente(OS, cknotificado.Checked, ckconforme.Checked ) then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Listo
    DMConnections.BaseCAC.CommitTrans;

    //si es un reclamo nuevo
    if FEditando = False then begin
        //informo al operador el numero de Reclamo que se genero
        InformarNumeroReclamo(IntToStr(OS));
        DesbloquearOrdenServicio(OS);
    end;

    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoContactarCliente.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoContactarCliente.FormDestroy(Sender: TObject);
begin
    RemoveOSform(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: desbloqueo la orden de servicio antes de salir
               y libero el formulario de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoContactarCliente.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //si se esta modificando un reclamo
    if FEditando = True then begin
        //verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = False then begin
            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //lo libero de memoria
    Action := caFree;
end;

end.
