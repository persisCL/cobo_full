object FormReclamoEstacionamiento: TFormReclamoEstacionamiento
  Left = 82
  Top = 150
  BorderStyle = bsDialog
  Caption = 'Reclamo de Estacionamiento'
  ClientHeight = 511
  ClientWidth = 862
  Color = clBtnFace
  Constraints.MinHeight = 458
  Constraints.MinWidth = 868
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  DesignSize = (
    862
    511)
  PixelsPerInch = 96
  TextHeight = 13
  inline FrameContactoReclamo1: TFrameContactoReclamo
    Left = 1
    Top = 0
    Width = 627
    Height = 172
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 1
    ExplicitHeight = 172
    inherited gbUsuario: TGroupBox
      inherited LNumeroConvenio: TLabel
        Width = 63
        ExplicitWidth = 63
      end
    end
    inherited gbContacto: TGroupBox
      ExplicitHeight = 162
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 472
    Width = 862
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PDerecha: TPanel
      Left = 616
      Top = 0
      Width = 246
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object CKRetenerOrden: TCheckBox
        Left = 3
        Top = 11
        Width = 89
        Height = 17
        Caption = 'Retener Orden'
        TabOrder = 0
      end
      object AceptarBTN: TButton
        Left = 96
        Top = 6
        Width = 70
        Height = 25
        Caption = '&Aceptar'
        Default = True
        TabOrder = 1
        OnClick = AceptarBTNClick
      end
      object btnCancelar: TButton
        Left = 171
        Top = 6
        Width = 70
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        ModalResult = 2
        TabOrder = 2
        OnClick = btnCancelarClick
      end
    end
  end
  object PageControl: TPageControl
    Left = 5
    Top = 173
    Width = 610
    Height = 332
    ActivePage = TabSheetDatos
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheetDatos: TTabSheet
      Caption = 'Datos del Reclamo'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label3: TLabel
        Left = 0
        Top = 242
        Width = 97
        Height = 13
        Align = alBottom
        Caption = 'Otras observaciones'
      end
      object txtDetalle: TMemo
        Left = 0
        Top = 255
        Width = 602
        Height = 49
        Align = alBottom
        TabOrder = 0
      end
      object DBLEstacionamientos: TDBListEx
        Left = 0
        Top = 0
        Width = 602
        Height = 242
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'Estacionamiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'NumCorrEstacionamiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 60
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NombreCorto'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Patente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'Entrada'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescripcionEntrada'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha / Hora Entrada'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaHoraEntrada'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 60
            Header.Caption = 'Salida'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescripcionSalida'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha / Hora Salida'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaHoraSalida'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Usuario Rechaza'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = True
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 85
            Header.Caption = 'Seleccionado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = True
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 70
            Header.Caption = 'Resoluci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
          end>
        DataSource = DataSource
        DragReorder = True
        ParentColor = False
        PopupMenu = PopupResolver
        TabOrder = 1
        TabStop = True
        OnContextPopup = DBLEstacionamientosContextPopup
        OnCheckLink = DBLEstacionamientosCheckLink
        OnDrawText = DBLEstacionamientosDrawText
        OnLinkClick = DBLEstacionamientosLinkClick
      end
    end
    object TabSheetProgreso: TTabSheet
      Caption = 'Progreso / soluci'#243'n'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        602
        304)
      object Ldetalledelprogreso: TLabel
        Left = 3
        Top = 7
        Width = 149
        Height = 13
        Caption = 'Detalles del progreso / soluci'#243'n'
      end
      object txtDetalleSolucion: TMemo
        Left = 1
        Top = 24
        Width = 592
        Height = 275
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
      end
    end
    object TabSheetRespuesta: TTabSheet
      Caption = 'Respuesta'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      inline FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 304
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 304
        inherited LRespuestadelaConcesionaria: TLabel
          Width = 153
        end
        inherited LComentariosDelCliente: TLabel
          Width = 116
          ExplicitWidth = 116
        end
        inherited PContactarCliente: TPanel
          Width = 602
          ExplicitWidth = 602
        end
        inherited txtDetalleRespuesta: TMemo
          Width = 602
          ExplicitWidth = 602
        end
        inherited TxtComentariosdelCliente: TMemo
          Width = 602
          Height = 130
          ExplicitWidth = 602
          ExplicitHeight = 130
        end
      end
    end
    object TabSheetHistoria: TTabSheet
      Caption = 'Historia'
      ImageIndex = 4
      inline FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 304
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 304
        inherited DBLHistoria: TDBListEx
          Width = 602
          Height = 228
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 120
              Header.Caption = 'F. Modificaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaHoraModificacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'F. Compromiso'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaCompromiso'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Usuario'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Prioridad'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Prioridad'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescEstado'
            end>
          ExplicitWidth = 602
          ExplicitHeight = 228
        end
        inherited Pencabezado: TPanel
          Width = 602
          ExplicitWidth = 602
          inherited Pder: TPanel
            Left = 594
            ExplicitLeft = 594
          end
          inherited Parriva: TPanel
            Width = 602
            ExplicitWidth = 602
          end
        end
        inherited Pabajo: TPanel
          Top = 263
          Width = 602
          ExplicitTop = 263
          ExplicitWidth = 602
        end
      end
    end
  end
  inline FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio
    Left = 623
    Top = 69
    Width = 239
    Height = 102
    Anchors = [akTop, akRight]
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 623
    ExplicitTop = 69
    inherited GBCompromisoCliente: TGroupBox
      Anchors = [akTop, akRight]
      inherited Label1: TLabel
        Width = 44
        ExplicitWidth = 44
      end
      inherited Label2: TLabel
        Width = 103
        ExplicitWidth = 103
      end
    end
  end
  inline FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio
    Left = 622
    Top = 171
    Width = 240
    Height = 116
    Anchors = [akTop, akRight]
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 171
    ExplicitHeight = 116
    inherited GBEstadoDelReclamo: TGroupBox
      inherited Label1: TLabel
        Width = 36
        ExplicitWidth = 36
      end
      inherited PAdicional: TPanel
        inherited Label2: TLabel
          Width = 72
          ExplicitWidth = 72
        end
        inherited Label3: TLabel
          Width = 70
          ExplicitWidth = 70
        end
      end
    end
  end
  inline FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio
    Left = 622
    Top = 6
    Width = 239
    Height = 65
    Anchors = [akTop, akRight]
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 6
    inherited PFuenteReclamo: TGroupBox
      Anchors = [akTop, akRight]
    end
  end
  inline FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio
    Left = 622
    Top = 285
    Width = 239
    Height = 68
    Anchors = [akTop, akRight]
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 285
    inherited PReclamo: TGroupBox
      inherited LOrden: TLabel
        Width = 72
        ExplicitWidth = 72
      end
    end
  end
  inline FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio
    Left = 623
    Top = 353
    Width = 239
    Height = 53
    Anchors = [akTop, akRight]
    TabOrder = 7
    Visible = False
    ExplicitLeft = 623
    ExplicitTop = 353
    ExplicitWidth = 239
    ExplicitHeight = 53
    inherited GBConcesionaria: TGroupBox
      inherited lblConcesionaria: TLabel
        Width = 70
        ExplicitWidth = 70
      end
    end
  end
  object DataSource: TDataSource
    DataSet = spObtenerEstacionamientosReclamo
    Left = 709
    Top = 454
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 741
    Top = 454
    Bitmap = {
      494C010102002C0038000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC000000008003000400000000BFFB7FF400000000BFFB77F400000000
      BFFB63F400000000BFFB41F400000000BFFB48F400000000BFFB5C7400000000
      BFFB7E3400000000BFFB7F1400000000BFFB7F9400000000BFFB7FD400000000
      BFFB7FF4000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
  object spObtenerOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenServicio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 619
    Top = 454
  end
  object spObtenerEstacionamientosReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEstacionamientosReclamo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = '1'
      end
      item
        Name = '@Enumeracion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8000
        Value = Null
      end>
    Left = 680
    Top = 454
  end
  object PopupResolver: TPopupMenu
    Left = 649
    Top = 454
    object mnu_Resolver: TMenuItem
      Caption = 'Resolver'
      object Mnu_Pendiente: TMenuItem
        Tag = 1
        Caption = 'Pendiente'
        OnClick = Mnu_ComunClick
      end
      object Mnu_Aceptado: TMenuItem
        Tag = 2
        Caption = 'Aceptado'
        OnClick = Mnu_ComunClick
      end
      object Mnu_Denegado: TMenuItem
        Tag = 3
        Caption = 'Denegado'
        OnClick = Mnu_ComunClick
      end
      object Mnu_Comun: TMenuItem
        Visible = False
        OnClick = Mnu_ComunClick
      end
    end
  end
  object ilCamara: TImageList
    Height = 15
    Width = 15
    Left = 773
    Top = 454
    Bitmap = {
      494C010102002C0038000F000F00FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4D4A004A4D4A004A4D
      4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D
      4A004A4D4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EB
      FF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4D4A0000EBFF00B5FBFF00B5FB
      FF00B5FBFF00848284004A4D4A004A4D4A004A4D4A00B5FBFF00B5FBFF00B5FB
      FF0000EBFF004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF00B5FBFF00FFFFFF004A4D4A0084828400B5B2B500B5B2
      B500848284004A4D4A00FFFFFF00B5FBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4D4A0000EBFF00B5FBFF00FFFF
      FF004A4D4A008482840084828400848284004A4D4A004A4D4A00FFFFFF00B5FB
      FF0000EBFF004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF00B5FBFF00FFFFFF004A4D4A0084828400B5B2B500B5B2
      B500848284004A4D4A00FFFFFF00B5FBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4D4A0000EBFF00B5FBFF00B5FB
      FF00B5FBFF004A4D4A004A4D4A004A4D4A004A4D4A00B5FBFF00B5FBFF00B5FB
      FF0000EBFF004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EB
      FF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4D4A004A4D4A004A4D
      4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D
      4A004A4D4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000004A4D4A0000EBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4D4A004A4D
      4A004A4D4A004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      8001FFFC000000008001FFFC0000000080018004000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080018004000000008001C3FC00000000
      8001C3FC000000008001FFFC00000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
end
