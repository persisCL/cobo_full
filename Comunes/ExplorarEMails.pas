{-------------------------------------------------------------------------------
  Revision 1:
    Author: nefernandez
    Date: 16/05/2007
    Description: Se agrega el filtro "Env�o de Clave" en el grupo "Tipos de Email"

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
	
	Etiqueta	:	20160530 MGO
	Descripci�n	:	Se agrega tipo "C" para Correspondencia.
					Se env�a la letra del Tipo para obtener el contenido del mensaje.
-------------------------------------------------------------------------------}
unit ExplorarEMails;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Util, UtilProc, UtilDB, DMMensaje, MensajesEmail, ListBoxEx,
  DBListEx, StdCtrls, ExtCtrls, CollPnl, Validate, DateEdit, DmiCtrls,
  DMConnection, DB, ADODB, ParamGen, ConstParametrosGenerales, PeaProcs,
  Menus, ComponerEMail;

type
  TfrmExplorarMails = class(TForm)
    Panel1: TPanel;
    cpOpciones: TCollapsablePanel;
    pnlFooter: TPanel;
    btnCerrar: TButton;
    DBListEx2: TDBListEx;
    Label4: TLabel;
    lblImportar: TLabel;
    Label6: TLabel;
    lblParametros: TLabel;
    Label8: TLabel;
    lblUpdate: TLabel;
    dsEMails: TDataSource;
    spExplorarEMails: TADOStoredProc;
    rgOtros: TRadioGroup;
    dlgAbrir: TOpenDialog;
    mnuComponer: TPopupMenu;
    mnuComponerEsteMail: TMenuItem;
    gbBusqueda: TGroupBox;
    Label1: TLabel;
    neTop: TNumericEdit;
    Label2: TLabel;
    deDesde: TDateEdit;
    Label3: TLabel;
    deHasta: TDateEdit;
    RgTipos: TRadioGroup;
    btnBuscar: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCerrarClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure lblParametrosClick(Sender: TObject);
    procedure lblImportarClick(Sender: TObject);
    procedure lblUpdateClick(Sender: TObject);
    procedure mnuComponerPopup(Sender: TObject);
    procedure mnuComponerEsteMailClick(Sender: TObject);
  private
    FMaxReintentos: Integer;
    FFiltroProceso: Boolean;
    FNumeroProceso : Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function ValidarParametros : Boolean;
    procedure AplicarOtrosFiltros;
  public
    function Inicializar(NumeroProcesoFacturacion: Integer = 0): Boolean;
  end;

var
  frmExplorarMails: TfrmExplorarMails;

implementation

resourcestring
    MSG_ERROR = 'Error';
    MSG_TITLE = 'Error al obtener un par�metro general';
    MSG_PARAM = 'Par�metro de cantidad de reintentos';

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: TfrmExplorarMails.Inicializar
Author : gcasais
Date Created : 18/07/2005
Description :
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmExplorarMails.Inicializar(NumeroProcesoFacturacion: Integer = 0): Boolean;
resourcestring
    MSG_PROCESS = 'S�lo los e-mails relativos al proceso %d';
const
    QUERY_TOP = 100;
var
    S: TSize;
begin

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    if NumeroprocesoFacturacion > 0 then begin
        FNumeroProceso := NumeroProcesoFacturacion;
        Caption := Caption + Space(2) + '[' +
          Format(MSG_PROCESS, [FNumeroProceso]) + ']';
        FFiltroProceso := True;
    end else begin
        FFiltroProceso := False;
    end;
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    Result := False;
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_CANTIDAD_REINTENTOS', FMaxReintentos) then begin
        MsgBoxErr(MSG_ERROR, MSG_TITLE + CRLF + MSG_PARAM , MSG_TITLE, MB_ICONERROR);
        Exit;
    end;
    deDesde.Date := Now;
    deHasta.Date := Now;
    neTop.ValueInt := QUERY_TOP;
    cpOpciones.Open := False;
    Result := True;
end;

procedure TfrmExplorarMails.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmExplorarMails.btnCerrarClick(Sender: TObject);
begin
    Close;
end;
{-----------------------------------------------------------------------------
  Procedure: TfrmExplorarMails.Button1Click
  Author:    gcasais
  Date:      18-Jul-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}

procedure TfrmExplorarMails.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_ERROR = 'Error';
    MSG_TITLE = 'B�squeda de e-Mails';
begin
    if not ValidarParametros then Exit;
    spExplorarEmails.Close;
    spExplorarEmails.Parameters.Refresh;
    case RgTipos.ItemIndex of
        0:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'T';
            end;
        1:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'N';
            end;
        2:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'F';
            end;
        3: // Revision 1
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'E';
            end;
        // INICIO : 20160530 MGO
        4:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'C';
            end;
        // FIN : 20160530 MGO
    end;
    spExplorarEmails.Parameters.ParamByName('@TopFilter').Value := neTop.ValueInt;
    spExplorarEmails.Parameters.ParamByName('@FechaDesde').Value := FormatDateTime('yyyymmdd 00:00:00', deDesde.Date);
    spExplorarEmails.Parameters.ParamByName('@FechaHasta').Value := FormatDateTime('yyyymmdd 23:59:59', deHasta.Date);
    spExplorarEmails.Parameters.ParamByName('@CodigoConvenio').Value := 0;
    Cursor := crHourGlass;
    try
        try
            spExplorarEmails.Open;
            AplicarOtrosFiltros;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
            end;
        end;
    finally
        Cursor := crDefault;
        if spExplorarEmails.IsEmpty then begin
            spExplorarEmails.close;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: TfrmExplorarMails.ValidarParametros
  Author:    gcasais
  Date:      18-Jul-2005
  Arguments: None
  Result:    Boolean
-----------------------------------------------------------------------------}

function TfrmExplorarMails.ValidarParametros: Boolean;
resourcestring
    MSG_TITLE = 'Validaciones';
    MSG_END_DATE_ERROR = 'La fecha "Hasta" es inv�lida';
    MSG_START_DATE_ERROR = 'La fecha "Desde" es inv�lida';
    MSG_END_DATE_LOWER_THAN_START_DATE  = 'La fecha "Hasta" debe ser posterior o igual a la fecha "Desde"';
    MSG_FILTER_ERROR = 'La cantidad de registros debe ser mayor a cero';
begin
    Result := ValidateControls([neTop, deDesde, deHasta, deHasta],
        [(neTop.ValueInt > 0), (IsValidDate(DateToStr(deDesde.Date))),
            (IsValidDate(DateToStr(deHasta.Date))), (deHasta.Date >= deDesde.Date)],
        MSG_TITLE,
        [MSG_FILTER_ERROR, MSG_START_DATE_ERROR,
            MSG_END_DATE_ERROR, MSG_END_DATE_LOWER_THAN_START_DATE]);
end;
{-----------------------------------------------------------------------------
  Procedure: TfrmExplorarMails.AplicarOtrosFiltros
  Author:    gcasais
  Date:      18-Jul-2005
  Arguments: None
  Result:    None
-----------------------------------------------------------------------------}

procedure TfrmExplorarMails.AplicarOtrosFiltros;
begin
    spExplorarEmails.Filtered := False;
    case rgOtros.ItemIndex of
        1:
            begin
                spExplorarEmails.Filter := 'Enviado = ' + QuotedStr('S�');
                spExplorarEmails.Filtered := True;

            end;
        2:
            begin
                spExplorarEmails.Filter := 'Enviado = ' + QuotedStr('No');
                spExplorarEmails.Filtered := True;
            end;
        3:
            begin
                spExplorarEmails.Filter := 'Reintentos >= ' + IntToStr(FMaxReintentos);
                spExplorarEmails.Filtered := True;
            end;
    end;
    // Adem�s si nos inicializaron con un proceso mostramos s�lo los que correspondan
    if FFiltroProceso then begin
        spExplorarEmails.Filtered := False;
        if rgOtros.ItemIndex = 0 then begin
            spExplorarEmails.Filter :=  'NumeroProcesoFacturacion = ' +
              IntToStr(FNumeroProceso);
        end else begin
            spExplorarEmails.Filter := spExplorarEmails.Filter +
             ' AND NumeroProcesoFacturacion = ' +  IntToStr(FNumeroProceso);
        end;


        spExplorarEmails.Filtered := True;
    end;
end;

procedure TfrmExplorarMails.lblParametrosClick(Sender: TObject);
var
    f: TFormParam;
begin
    Application.CreateForm(TFormParam, f);
    if f.Inicializar('M', False) then f.ShowModal;
    f.Release;

    // Vuelvo a cargar los par�metros generales
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'EMAIL_CANTIDAD_REINTENTOS', FMaxReintentos) then begin
        MsgBoxErr(MSG_ERROR, MSG_TITLE + CRLF + MSG_PARAM , MSG_TITLE, MB_ICONERROR);
        Exit;
    end;
end;

procedure TfrmExplorarMails.lblImportarClick(Sender: TObject);
var
    dm: TDMMensajes;
    Error: string;
begin
    dm := TDMMensajes.Create(nil);
    try
        if dlgAbrir.Execute then begin
            if MensajesEmail.ImportarMensajesDesdeArchivo(dm, dlgAbrir.FileName, Error) then begin
                msgbox('El archivo se ha importado correctamente.'#13#10'Verifique el log de errores para ver que no hayan quedado registros sin procesar.' , 'Importar', MB_ICONINFORMATION);
            end else begin
                msgbox('Error al importar el archivo: ' + Error, 'Error', MB_ICONERROR);
            end;
        end;
    finally
        dm.Free;
    end;
end;

procedure TfrmExplorarMails.lblUpdateClick(Sender: TObject);
resourcestring
    MSG_CONFIRMACION = 'Est� seguro de setear a cero los reintentos mayores o iguales al m�ximo establecido?';
    MSG_TITLE = 'Confirmaci�n';
    MSG_UPDATE = 'Resetear reintentos';
    MSG_ERROR = 'Error';
begin
    if (MsgBox(MSG_CONFIRMACION, MSG_TITLE, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin
        try
            QueryExecute(DMConnections.BaseCAC,
              'UPDATE EMAIL_MENSAJES SET REINTENTOS = 0 WHERE REINTENTOS >= ' +
               IntToStr(FMaxReintentos));
        except
            on e:Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_UPDATE, MB_ICONERROR);
            end;
        end;
    end;
end;

procedure TfrmExplorarMails.mnuComponerPopup(Sender: TObject);
begin
    mnucomponerEsteMail.Enabled := not spExplorarEmails.IsEmpty;    
end;
{******************************** Function Header ******************************
Function Name: TfrmExplorarMails.mnuComponerEsteMailClick
Author : gcasais
Date Created : 19/07/2005
Description : Muestra un mensaje parecido a como se ver�a en un cliente
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TfrmExplorarMails.mnuComponerEsteMailClick(Sender: TObject);
var
    f: TfrmComponerMail;
    dm: TDMMensajes;
    MailCompuesto: TMensaje;
    ErrorMsg: String;
begin
    Application.CreateForm(TfrmComponerMail, f);
    dm := TDMMensajes.Create(nil);
    try
    if spExplorarEMails.FieldByName('Tipo').Value<>'Email Nota de Cobro' then begin
        { INICIO : 20160530 MGO
        if not f.Inicializar(dm, spExplorarEMails.FieldByName('CodigoMensaje').Value, Mailcompuesto, ErrorMsg) then begin
        }
        if not f.Inicializar(dm, spExplorarEMails.FieldByName('CodigoMensaje').Value, spExplorarEMails.FieldByName('TipoLetra').Value, Mailcompuesto, ErrorMsg) then begin
        // FIN : 20160530 MGO
            MsgBoxErr('Error', ErrorMsg, 'Componer e-Mail', MB_ICONERROR);
        end else begin
            f.ShowModal;
        end;
    end;
    finally
        FreeAndNil(DM);
        f.Release;
    end;
end;

end.
