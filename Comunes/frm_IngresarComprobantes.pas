{-----------------------------------------------------------------------------
 File Name: frm_IngresarComprobantes.pas
 Author:    Flamas
 Date Created: 24/01/2005
 Language: ES-AR
 Description:   Permite ingresar Facturas e items a facturar
    - Para la emisi�n de las boletas en la impresora fiscal se utiliza una dll que
    debe estar ubicada en el mismo lugar que el ejecutable.

 Revision: 1
 Author:    ggomez
 Date:      04/07/2005
 Description:
    - Cuando se selecciona el Tipo de Comprobante Boleta, se deshabilita
    el N�mero de Comprobante y la Fecha de Emisi�n.
    - Agregu� los campos Cantidad y CoeficienteIVA al ClientDataSet de los Items.

 Revision: 2
 Author:    ggomez
 Date:      12/07/2005
 Description: Implement� la emisi�n de boletas en la impresora fiscal. Si al
    emitir una boleta ya existe una boleta abierta se realiza una cancelaci�n
    (cierre forzozo) de la boleta abierta y se inicia la emisi�n de la nueva
    boleta.
    Si la impresi�n de la boleta no se realiza con exito, no se almacena en la
    base de datos el comprobante para la misma.

 Revision: 3
 Author:    ggomez
 Date:      13/09/2005
 Description: Emisi�n de boletas en la impresora fiscal. Si ocurre un error en
    la emisi�n, la recuperaci�n del error se realiza de dos modos:
        1- Cancelando la boleta emitida (Modo 0 (cero)), o
        2- Continuando con la emisi�n de la boleta (Modo 1 (uno)).
    Para esto se debe configurar el modo de recuperaci�n de errores, cuyo dato
    queda almacenado en el ini de la aplicaci�n. Por ahora hay que configurarlo
    manualmente, colocando en el ini los siguiente:
    [FiscalPrinter]
    ModoRecuperacionError = 0 (o 1)

 Revision: 4
 Author:    mvitali
 Date:      13/09/2005
 Description: Agregue un parametro boolean al metodo inicializar para que se
 puedan asentar el comprobante del tipo boleta sin necesidad de usar la
 impresora fiscal.
 True = Usar impresora fiscal    False = Ingresar Comprobante a mano.

 Revision: 5
 Author:    jconcheyro
 Date:      24/08/2006
 Description: en ImprimirItemsBoleta no se estaba filtrando el envio do los marcados

 Revision: 6
 Author: mlopez
 Date: 16/08/2005
 Description: Bueno, en breves palabras, sucedia que en produccion se estaba
              dando la situacion en la que Boletas han salido impresas pero
              no fueron asentadas en la DB. Para solucionar esto, se agrego
              un archivo que guarda en el disco la boleta para que en caso
              de falla de la impresora o falla por time out (por ejemplo),
              el sistema detectara este archivo y enviara nuevamente a imprimir
              y a guardar lo que falte.

 Revision: 5
 Author: jconcheyro
 date: 08/09/2006
 Description: Se agrega el nuevo tipo de comprobante Boleta Manual. Se cambia el form
 de ingreso de cargos por uno nuevo que tiene discriminado el IVA. Se agregaron funciones de
 validacion de numero de comprobante, de impresoras, de iva, etc

 Revision: 6
 Author: jconcheyro
 date: 09/11/2006
 Se incorporan boletas de Arriendo de telev�a, que siguen siendo BO, pero llevan un
 concepto especial y ademas, cuando se crea una de estas, se completa una estructura
 de pagos en cuotas. Ver tablas ComprobantesCuotas y ComprobantesCuotasDetalle

 Revision: 7
 Author: jconcheyro
 Date: 24/11/2006
 Description : Se oculta por ahora la funcionalidad de boletas manuales. Entonces desde Facturacion
 ya no se puede hacer ning�n tipo de boleta, y desde el CAC solo las de Impresora fiscal
 cuando se vuelva a necesitar la funcionalidad , hay que buscar SACAR PARA VOLVER A TENER BOLETAS MANUALES
 Se saca el editor de numero de convenio a pedido de Fernando, la unica opcion ser� el combo

 Revision: 8
 Author: jconcheyro
 Date: 29/11/2006
 Description : Saco el timer y agrego el bont�n buscar. Agregu� un mensaje si el Rut ingresado es inv�lido.

 Revision: 9
 Author: jconcheyro
 Date: 12/12/2006
 Description : ImprimirContinuacionBoletaFiscal se sacan los abort y se pasa a una estructura de
 Result False y Exit. Adem�s en caso de error no retornaba False. Si se daba un error en este metodo
 el programa suponia que habia quedado OK, pero el archivo seguia existiendo. La boleta se guardaba
 con numero fiscal -1

 Revision : 10
     Author : vpaszkowicz
     Date : 19/02/2007
     Description : Agrego el la verificaci�n de que se haya creado y grabado el
     archivo .BOL luego de cerrarlo.
     De esta forma nos aseguramos que la impresora
     no abra un ticket si no hay items.
     Si qued� un archivo en fase -1 y sin items(0) bytes, informo al usuario que
     qued� esa boleta sin generar y por la opci�n "continuar" le renombro el
     archivo a extensi�n "old"

 Revision : 11
     Author : vpaszkowicz
     Date : 21/02/2007
     Description : Agrego un registro de marca al final del archivo que me
     asegure que el archivo de respaldo no esta corrupto.
     Si la marca est� bien, puedo procesar el archivo.
     Estructura del registro: En la etiqueta Descripcion debe figurar la cons-
     tante MARCA;

Revision : 12
    Author : vpaszkowicz
    Date : 28/02/2007
    Description : Consulto por el estado de la impresora antes de hacer algo
    con ella como por ej consultarle si tiene un ticket abierto.. Si la impre-
    sora est� apagada le tiro un mensaje de error.

Revision: 13
    Author: nefernandez
    Date: 16/03/2007
    Description : Se pasa el parametro CodigoUsuario al SP CrearComprobante
    (para Auditoria).

Revision: 14
    Author: FSandi
    Date: 21/03/2007
    Description : Modificaci�n de Soporte para Motos (Se agrega el concepto de soporte
                para motos al momento de seleccionar un cargo de arriendo de telev�a
Revision : 15
    Author : jconcheyro
    Date : 11/04/2007
    Description : No se permite agregar en boletas el cargo de arriendo del diario
    La Tercera.

Revision : 16
    Author : jconcheyro
    Date : 04/05/2007
    Description : ahora se permite hacer varias boletas de arriendo al mismo TAG + Convenio
    para solucionar errores de cantidades de cuotas por ejemplo. Cambi� la funcion ValidarTelevia

Revision : 17
    Author : jconcheyro
    Date : 07/06/2007
    Description : Se le agrega una columna al store ObtenerConveniosCliente para que traiga
    el codigo de concesionaria, para no subir convenios que no son de CN al combo, porque
    no se le pueden hacer comprobantes con IVA a convenios no de CN.

Revision : 18
    Author : vpaszkowicz
    Date : 21/06/2007
    Description : Se valida que no se habilite el bot�n registrar si el total
    de la misma es mayor que cero.

Revision : 19
    Author : Fsandi
    Date : 21/06/2007
    Description : Por defecto coloco el boton de registrar como deshabilitado al momento de cambiar de tipo de comprobante,
                en caso de que deba activarse, se encarga la funcion ActualizarBotonRegistrar.

Revision : 20
  F  Author : FSandi
    Date : 06/08/2007
    Description : Se cambian todos los importes integer a Float, para evitar desbordamientos.
Revision : 20
    Author : vpaszkowicz
    Date : 16/08/2007
    Description : Agrego 2 cosas
        -Si estoy volviendo de un error y tengo una boleta abierta en fase ini-
        cial, si cuando el sistema me pregunta si deseo continuar Si o NO,
        cuando presiono No que cierre la ventana y no deje continuar. El tema
        es que pueden haber sido impresos los datos del cliente y si lo dejo
        seguir sin continuar la impresi�n abierta, puede hacerle una boleta a
        un cliente equivocado.
        -Agrego los datos del cliente cuando vuelvo de un error. Los obtengo del
        registro que tengo guardado en el archivo "BOL".
        -Valido que el importe que se ingresa este soportado por la impresora
Revision : 21
    Author : vpaszkowicz
    Date : 19/09/2007
    Description : Le quito la posibilidad de decir que no a una continuaci�nm
    de boleta fiscal abierta.
    Cambi� los mensajes a mostrar al usuario para indicarle que se va a conti-
    nuar con la emisi�n de la boleta.

Revision : 22
    Author : vpaszkowicz
    Date : 09/05/2007
    Description : Agrego el movimiento cuenta al registro y lo grabo en el ar-
    chivo BOL, ya que si es un movimiento que se recupera de la base (ej, pro-
    moci�n la Tercera) y ocurre un corte en la impresi�n, el numero de movimiento
    deber�a guardarse en el archivo para poder recuperarlo y grabarlo en la base
    porque ya est� creado. De lo contrario est�n quedando boletas sin movimientos
    cuenta.

Revision 23:
    Author: dAllegretti
    Date: 22/05/2008
    Description:    Se le agreg� el parametro @UsuarioCreacion del Stored spAgregarMovimientoCuentaIVA
                    y la correspondiente asignaci�n en el llamado al stored.
                    Se agregaron los par�metros @UsuarioModificacion y @FechaHoraModificacion
                    para el stored procedure spActualizarLoteFacturacion y la correspondiente asignaci�n
                    en el llamado al stored.

Revision 24:
    Author: rharris
    Date: 29/12/2008
    Description:    Se agrega un nuevo tipo de arriendo (arriendo en cuotas) para soslayar el tema de las
                    cuotas hasta el 1ro de mayo del 2009 (entrada en vigencia de factura electronica).
Revision : 25
    Author : vpaszkowicz
    Date : 18/02/2009
    Description : Cambio el orden en la grabaci�n del comprobante. El borrado del archivo BOL debe ir al
    final, de lo contrario un timeout en el reaplicar, me puede dejar sin boleta (rollbak) y sin respaldo
    para volver a generarla.
    Seteo que guarde datos del arriendo cuotas en el archivo BOL (datos del telev�a).

Firma : SS_1147_MCA_20140408
Descripcion: se elimina sigla CN en las fn o sp

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

-----------------------------------------------------------------------------}
unit frm_IngresarComprobantes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Dateedit, ExtCtrls, utilProc, DmiCtrls, util, DBCtrls,
  ImgList, ADODB, DMConnection, ComCtrls, BuscaClientes, Utildb, DB,
  DPSControls, VariantComboBox, constParametrosGenerales, Validate,
  peaTypes, PeaProcs, dateUtils, ListBoxEx, DBListEx, DBClient,
  Frm_CargoNotaCredito, TimeEdit, DeclHard, DeclPrn, Menus;

const
    CONST_MARCA = 'MARCA_EL_ARCHIVO_ES_CONSISTENTE';

type


    // Tipo para los items del archivo temporal de boleta.
    TItem = record
        Descripcion     : String[100];
        Cantidad        : Real;
        PrecioUnitario  : Int64;
        PorcentajeIVA  : Real;
        ImporteIVA     : Int64;
        Marcado         : Boolean;
        CodigoConvenio  : Integer;
        CodigoConcepto  : Integer;
        Observaciones   : String[100];
        Nuevo           : Boolean;

        CodigoCliente   : Integer;
        // Indica si el item ya fue impreso por la impresora fiscal.
        EstaImpreso     : Boolean;
        CantidadCuotas  : Int64;
        Etiqueta        : String[11];
        //Agrego el movimiento, ya que si ocurre un corte de impresi�n y el movimiento
        //ya exist�a antes, no lo voy a poder grabar. (x ej Promoci�n la Tercera).
        NumeroMovimiento: integer;
    end; //record

    ExceptionFiscal = class(Exception);

    TArrString = Array Of String;

  TfrmIngresarComprobantes = class(TForm)
    gbDatosConvenio: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    peRUTCliente: TPickEdit;
    cbConveniosCliente: TVariantComboBox;
    Label7: TLabel;
    spObtenerMovimientosAFacturar: TADOStoredProc;
    dsObtenerPendienteFacturacion: TDataSource;
    gbDatosCliente: TGroupBox;
    lNombreCompleto: TLabel;
    lDomicilio: TLabel;
    lComuna: TLabel;
    lRegion: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    gbItemsPendientes: TGroupBox;
    dbItemsPendientes: TDBListEx;
    cdsObtenerPendienteFacturacion: TClientDataSet;
    Imagenes: TImageList;
    Label2: TLabel;
    gbDatosComprobante: TGroupBox;
    lbl_NroComprobante: TLabel;
    lbl_FechaEmision: TLabel;
    edFechaEmision: TDateEdit;
    edNumeroComprobante: TNumericEdit;
    lblFechaVencimiento: TLabel;
    edFechaVencimiento: TDateEdit;
    ObtenerCLiente: TADOStoredProc;
    spAgregarMovimientoCuentaIVA: TADOStoredProc;
    spCrearLoteFacturacion: TADOStoredProc;
    spCrearComprobante: TADOStoredProc;
    spActualizarLoteFacturacion: TADOStoredProc;
    Label5: TLabel;
    cbTiposComprobantes: TVariantComboBox;
    Label6: TLabel;
    lbl_Importe: TLabel;
    spObtenerConveniosCliente: TADOStoredProc;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    ImpresoraFiscal: TFiscalPrinter;
    lbl_EstadoEmisionBoleta: TLabel;
    btnSalir: TButton;
    btnAgregarItem: TButton;
    btnModificarItem: TButton;
    btnEliminarItem: TButton;
    nb_Botones: TNotebook;
    btn_Continuar: TButton;
    btnRegistrar: TButton;
    chkBoletaManual: TCheckBox;
    lblNumeroTelevia: TLabel;
    edCantidadCuotas: TNumericEdit;
    lblCuotas: TLabel;
    spObtenerDetalleConceptoMovimiento: TADOStoredProc;
    lblRUT: TLabel;
    lblRUTRUT: TLabel;
    edNumeroTelevia: TEdit;
    spCrearComprobanteCuotas: TADOStoredProc;
    btnBuscarRUT: TButton;
    PopupGrilla: TPopupMenu;
    Marcarcomoimpreso1: TMenuItem;
    CbSoporte: TCheckBox;
    spReaplicarConvenio: TADOStoredProc;
    procedure chkBoletaManualClick(Sender: TObject);
    procedure btn_ContinuarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure dbItemsPendientesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dbItemsPendientesDblClick(Sender: TObject);
    procedure edNumeroComprobanteChange(Sender: TObject);
    procedure cdsObtenerPendienteFacturacionAfterScroll(DataSet: TDataSet);
    procedure btnAgregarItemClick(Sender: TObject);
    procedure btnModificarItemClick(Sender: TObject);
    procedure btnEliminarItemClick(Sender: TObject);
    procedure btnRegistrarClick(Sender: TObject);
    procedure cbTiposComprobantesChange(Sender: TObject);
    procedure cdsObtenerPendienteFacturacionCalcFields(DataSet: TDataSet);
    procedure edCantidadCuotasExit(Sender: TObject);
    procedure edNumeroTeleviaExit(Sender: TObject);
    procedure btnBuscarRUTClick(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure Marcarcomoimpreso1Click(Sender: TObject);
    procedure CbSoporteClick(Sender: TObject);
  private
    { Private declarations }
	FUltimaBusqueda : TBusquedaCliente;
    FLoteFacturacion : Integer;
	FCodigoCliente : Integer;
	FFechaCorteMaximaPosible: TDateTime;
    FTotalComprobante : Double;
    FTipoComprobante: AnsiString;
    // Almacena el n� de boleta que retorna la impresora fiscal.
    FNroBoletaFiscal: Int64;
    // Indica si se est� registrando una boleta fiscal.
    FRegistrandoBoletaFiscal: Boolean;
    // Almacena el C�digo de Convenio a quien se le emite el Comprobante.
    FCodigoConvenio: Integer;
    // Solo para las boletas, porque si se pierde uno no importa para el campo numeroComprobante de Comprobantes
    FNumeroComprobanteBoleta: Int64;
    // Modo de recuperaci�n de error con el cual se realiza la impresi�n de boletas
    FModoRecuperacionImpresion: Byte;
    FImprimirBoletaEnImpresoraFiscal: boolean;
    FCodigoImpresora: integer;
    FNumeroSerie: String;
    // Para mantener el path y nombre del archivo para almacenar los datos de la Boleta.
    FArchivoBoleta: AnsiString;
    FCodigoConceptoArriendo: integer;
    FCodigoConceptoArriendoCuotasMail: integer;
    FCodigoConceptoArriendoCuotasPostal: integer;
    FTipoEnvioDocumento: integer;
//Revision 14
    FCodigoConceptoSoporteTelevia : Integer;
    FIncluirSoporte : Boolean;
//Fin de la Revision 14
    FMinimaCantidadCuotasArriendo: integer;
    FMaximaCantidadCuotasArriendo: integer;
//Revision 10
    FCotizacion: double;
//Revision 10
    FCodigoConceptoLaTercera: integer; //Rev15
    FCodigoConcesionariaNativa: Integer;		//SS_1147_MCA_20140408
    procedure ClearCaptions;
	procedure CargarTiposComprobantes;
    procedure CargarComprobantesPendientes;
    procedure MostrarDomicilioFacturacion;
	procedure ActualizarBotonRegistrar;
	procedure AgregarNuevosMovimientosCuenta;
    procedure AgregarLoteFacturacion(NumeroComprobante: integer);
    function AgregarComprobante: integer;
	procedure ActualizarLotesFacturacion;
    function  ObtenerSignoMovimientoCuenta : integer;
    function ContinuarBoletaFiscalAbierta: Boolean;
    function ImprimirContinuacionBoletaFiscal: Boolean;
    function ImprimirBoleta: Boolean;
    function ObtenerCodigoImpresora(NumeroDeSerie: String): Integer;
    function ObtenerCodigoImpresoraManual: Integer;
    procedure GetDirBoletaTemporal(var DirectorioTemporal: AnsiString; var Exito: Boolean);
    procedure CargarItemsBoletaDesdeTemporal(Archivo: AnsiString);
    procedure EliminarBoletaTemporal(Archivo: AnsiString);
    procedure ActualizarNombreArchivoBoletaFiscal(NumeroBoletaFiscal: Int64; Fase: AnsiString);
    function ObtenerArchivoFase(NombreArchivo: String): Integer;
    function ExisteComprobanteImpreso: Boolean;
    function ObtenerPartesNombreArchivo(NombreArchivo: TFileName;
        var NumeroSerieImpresora: String; var NumeroComprobanteFiscal: Int64;
        var NumeroComprobante: Int64; var FaseActual: Integer): Boolean;
    procedure ActualizarArchivoItemImpreso(NumeroRegistro: Integer; EstaImpreso: Boolean);
    procedure AbrirBoleta;
    procedure ImprimirItemsBoleta;
    procedure AgregarPagoBoleta(Monto: Double; Tipo: TTipoPago);
    procedure CerrarBoleta;
    function CancelarBoleta: Boolean;
    procedure AsentarComprobante;
    procedure GuardarComprobante;
    procedure GuardarBoletaTemporal(Archivo: AnsiString);
    function LimpiarEncabezadoImpresora: Boolean ;
    function ExisteArchivoContinuarBoletaFiscalAbierta: Boolean;
    procedure ResetearControles;
    function ObtenerIVA: Double;
    function ProgramarEncabezadoImpresora: boolean;
    function ValidarBoletaManualContraTalonarios: boolean;
    function ValidarComprobanteFiscal(CodigoImpresoraFiscal: integer): boolean;
//Revision 14
    Function SetearDatosArriendo:Boolean;
    Procedure AgregarSoporteInterfaz (Mostrar:Boolean);
//Revision 14
    function InicializarDatosArriendo: boolean;
    function ValidarCuotasArriendo: boolean;
    function ValidarTelevia: boolean;
//Revision 10
    function ObtenerCotizacionMoneda(const Fecha: TDateTime; const Moneda:String): boolean;
//Revision 10
    procedure AgregarCuotasComprobanteArriendo;
//Revision 11
    function ArchivoBoletaFiscalCorrupto: Boolean;
    function VerificarArchivoTemporal: Boolean;
    function VerificarMarcaItemImpreso(NumeroRegistro: Integer): Boolean;
    procedure CambiarExtensionArchivoBoletaFiscal;
    //Rev. 11
    procedure InicializarItemMarca(var Item: TItem);
    //Rev. 15
    procedure CompletarDatosConClienteDelArchivo;
    function EsImporteValido(ImporteConIva: double): Boolean;
    procedure SetearEventosClientDataSet(Activar: Boolean);
    function SetearDatosArriendoCuotas:Boolean;
  public
    { Public declarations }
    function Inicializar(ImprimirBoletaEnImpresoraFiscal: boolean): Boolean;
  end;


implementation

resourcestring
    MSG_ERROR_GENERAL = 'Ocurri� un error cuando se intent� generar una Boleta.';
    MSG_BOLETA_ABIERTA = 'Se encuentra en proceso de emisi�n la boleta N�: %s.';
    //MSG_CONTINUAR_CON_BOLETA = '�Desea continuar con la emisi�n de la boleta N�: %s?';
    MSG_CONTINUAR_CON_BOLETA = 'Se continuar� con la emisi�n de la boleta N�: %s';
    //MSG_CONTINUAR_CON_BOLETA_SIN_ABRIR = 'El sistema detect� un problema al intentar generar la �ltima boleta.' + CRLF +
    //                                      '�Desea continuar con la generaci�n de la boleta?';
    MSG_CONTINUAR_CON_BOLETA_SIN_ABRIR = 'El sistema detect� un problema al intentar generar la �ltima boleta.' + CRLF +
                                          'Se continuar� con la generaci�n de la boleta';
    //MSG_CONTINUAR_CON_BOLETA_SIN_NUMERO = 'El sistema detect� un problema al asentar la �ltima boleta generada.' + CRLF +
   //                                       '�Desea continuar con la generaci�n de la boleta?';
   MSG_CONTINUAR_CON_BOLETA_SIN_NUMERO = 'El sistema detect� un problema al asentar la �ltima boleta generada.' + CRLF +
                                          'Se continuar� con la generaci�n de la boleta';
    MSG_FASE_FINAL = 'Para finalizar la emisi�n de la boleta debe encender la impresora.';
    MSG_FASE_DESCONOCIDA = 'No se ha podido obtener informaci�n de la fase del proceso de emisi�n de la boleta.'
                                        + CRLF + 'Contacte al Administrador del Sistema.';
    MSG_DRIVER_IMPRESORA = 'Driver Impresora: ';
    MSG_BASE_DATOS = 'Base de Datos: ';
    MSG_ERROR_AL_OBTENER_NRO_SERIE = 'Ocurri� un error al obtener el N�mero de Serie de la Impresora Fiscal';
    MSG_NO_EXISTE_ARCHIVO_BOLETA = 'No se encuentra el archivo para la boleta.' + CRLF + 'Archivo: %s';
    MSG_FORMATO_NOMBRE_ARCHIVO_BOLETA_INCORRECTO = 'El formato del nombre del archivo no es correcto.' + CRLF + 'Archivo: %s';
    MSG_ERROR_RENOMBRAR_ARCHIVO_BOLETA = 'Error al renombrar el archivo para la boleta.' + CRLF + 'Archivo: %s';
    MSG_ERROR_OBTENER_NRO_SERIE_NOMBRE_ARCHIVO  = 'Ocurri� un error al obtener el N�mero de Serie de la Impresora Fiscal desde el nombre Archivo';
    MSG_ERROR_OBTENER_PARTES_NOMBRE_ARCHIVO = 'Error al analizar el nombre del archivo %s';
    MSG_CONFIGURACION_NO_POSIBLE = 'No se puede realizar la configuraci�n de la impresora con los datos del cliente.'
                                    + CRLF + 'Contacte al administrador del sistema.';
    MSG_NUMERO_FISCAL_ERRONEO = 'El N�mero fiscal no puede ser negativo. Verifique la impresora se encuentre encendida y en l�nea'  + CRLF + 'Archivo: %s';

const
    // Modos de recuperaci�n de error al imprimir una boleta fiscal.
    // Modo que indica que NO se desea continuar con la impresi�n de la boleta.
    MODO_RECUPERACION_CANCELAR  = 0;
    // Modo que indica que SI se desea continuar con la impresi�n de la boleta.
    MODO_RECUPERACION_CONTINUAR = 1;

    BOTON_CONTINUAR = 0;
    BOTON_REGISTRAR = 1;

    NOMBRE_BOLETA_TEMPORAL      = 'BO';
    EXTENSION_BOLETA_TEMPORAL   = '.BOL';

    // �ndices para acceder a los campos de Informaci�n de la Boleta Fiscal en
    // proceso de emisi�n. Est�n decrementados en 1, el resultado del comando
    // para obtener la informaci�n se almancena en un Array Of String.
    INDICE_INFO_BOLETA_FISCAL_NRO_BOLETA                        = 0;
    INDICE_INFO_BOLETA_FISCAL_TOTAL_BRUTO                       = 1;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_ITEMS                    = 2;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_MAX_ITEMS                = 3;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_DESCUENTOS_RECARGOS      = 4;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_MAX_DESCUENTOS_RECARGOS  = 5;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_TASAS_IMPUESTOS          = 6;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_MAX_TASAS_IMPUESTOS      = 7;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_PAGOS                    = 8;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_MAX_PAGOS                = 9;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_DONACIONES               = 10;
    INDICE_INFO_BOLETA_FISCAL_CANTIDAD_MAX_DONACIONES           = 11;
    INDICE_INFO_BOLETA_FISCAL_FASE_ACTUAL                       = 12;

    // Fase de preparaci�n de la generaci�n de la boleta. Esta no es un fase de
    // de la impresi�n.
    FASE_PREPARACION            = '-1';
    // Fases de la boleta fiscal.
    FASE_INICIAL                = '0';
    FASE_VENTA                  = '1';
    FASE_DESCUENTOS_RECARGOS    = '2';
    FASE_PAGOS                  = '3';
    FASE_FINAL                  = '4';

    TC_ARRIENDO = 'Arriendo Telev�a';
    TC_ARRIENDO_CUOTAS = 'Arriendo Telev�a Cuotas';
    EXTENSION_BOLETA_INVALIDA = 'OLD';
    TAMANO_EXTENSION = 3;

    CONST_MAXIMO_IMPORTE_IMPRESORA = 9999999;
    CONST_MAXIMO_IMPORTE_TOTAL = 999999999999999;

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: inicializar
Author       : flamas
Date Created : 20-12-2004
Description  : Iniciliza el form.
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TfrmIngresarComprobantes.Inicializar(ImprimirBoletaEnImpresoraFiscal: boolean): Boolean;
resourcestring
	MSG_CANNOT_LOAD_INVOICE_GENERATION  = 'No se pudo cargar el Ingreso de Comprobantes';
//var
//    LastError: AnsiString;
//    IdTurno: integer;
begin
    try
        AgregarSoporteInterfaz(False);
        lbl_EstadoEmisionBoleta.Caption := EmptyStr;
        FImprimirBoletaEnImpresoraFiscal := ImprimirBoletaEnImpresoraFiscal;

        // Si el dato que indica el Modo de Recuperaci�n no existe en el archivo,
        // crearlo con el valor por defecto de Continuar.
        //{ INICIO : 20160315 MGO
        if not ApplicationIni.ValueExists('FiscalPrinter', 'ModoRecuperacionError') then begin
            ApplicationIni.WriteInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        end;
        FModoRecuperacionImpresion  := ApplicationIni.ReadInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        {
        if not InstallIni.ValueExists('FiscalPrinter', 'ModoRecuperacionError') then begin
            InstallIni.WriteInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        end;
        FModoRecuperacionImpresion  := InstallIni.ReadInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        }// FIN : 20160315 MGO
        FCodigoCliente              := -1;
        FCodigoConvenio             := -1;
        FNroBoletaFiscal            := -1;
        FRegistrandoBoletaFiscal    := False;
{INICIO: TASK_005_JMA_20160418
        FCodigoConceptoLaTercera    := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_CODIGO_CONCEPTO_ARRIENDO_LA_TERCERA ()');
}
		FCodigoConceptoLaTercera    := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_CODIGO_CONCEPTO_ARRIENDO_LA_TERCERA (dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}
        FCodigoConcesionariaNativa	:= ObtenerCodigoConcesionariaNativa;		//SS_1147_MCA_20140408
        //busca el codigo de concepto y oculta los controles de arriendo televia y cuotas
        //solo para el CAC
        if FImprimirBoletaEnImpresoraFiscal then
            if not InicializarDatosArriendo then begin
                Result := False;
                Exit;
            end;

        ClearCaptions;

		FFechaCorteMaximaPosible := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));

        //result := ObtenerParametroGeneral(DMConnections.BaseCAC, DIAS_VENCIMIENTO_COMPROBANTE, FDiasVencimiento);

        edFechaEmision.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));

        //edFechaVencimiento.Date := edFechaEmision.Date + FDiasVencimiento;
		CargarTiposComprobantes;
        if FImprimirBoletaEnImpresoraFiscal then begin
            if FModoRecuperacionImpresion = MODO_RECUPERACION_CANCELAR then begin
                Result := True;
            end else begin
                //El ProccessMessage es para que termine de dibujar cuando no encuentre la impresora
                Application.ProcessMessages;
                if ExisteArchivoContinuarBoletaFiscalAbierta then begin
                    Result := ContinuarBoletaFiscalAbierta;
                end else begin
                    Result := True;
                end;
            end;
        end else begin
            Result := True;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_CANNOT_LOAD_INVOICE_GENERATION, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;


{******************************** Function Header ******************************
Function Name: InicializarDatosArriendo
Author : jconcheyro
Date Created : 08/11/2006
Description : carga el parametro para saber cual es el cargo para boletas de arriendo
y lo verifica. Oculta los controles que son unicamente para casos de arriendo de televia
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmIngresarComprobantes.InicializarDatosArriendo: boolean;
resourcestring
    MSG_CONCEPTO_ARRIENDO_INVALIDO       = 'El valor del concepto para generar arriendos de telev�as es inv�lido';
    MSG_CONCEPTO_SOPORTE_INVALIDO        = 'El valor del concepto para soporte de telev�as para motos es inv�lido';
    MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO = 'Error obteniendo el concepto para generar arriendos de telev�as';
    MSG_ERROR_CARGANDO_CONCEPTO_SOPORTE = 'Error obteniendo el concepto para soportes de telev�as de motos';
    MSG_ERROR_CARGANDO_CUOTAS_ARRIENDO   = 'Error obteniendo cantidades m�xima y m�nima de cuotas para arriendo de telev�as ';

begin
    Result := True;
    lblNumeroTelevia.Visible := False ;
    edNumeroTelevia.Visible  := False ;
    lblCuotas.Visible        := False ;
    edCantidadCuotas.Visible := False ;

    try
        if not (ObtenerParametroGeneral(DMConnections.BaseCAC, 'MINIMA_CANTIDAD_CUOTAS_ARRIENDO', FMinimaCantidadCuotasArriendo) and
                ObtenerParametroGeneral(DMConnections.BaseCAC, 'MAXIMA_CANTIDAD_CUOTAS_ARRIENDO', FMaximaCantidadCuotasArriendo)) then begin
                MsgBoxErr( MSG_ERROR_CARGANDO_CUOTAS_ARRIENDO, Caption, Caption, MB_ICONSTOP);
                Result := False;
        end;
    except
        on e : Exception do begin
                MsgBoxErr( MSG_ERROR_CARGANDO_CUOTAS_ARRIENDO, Caption, Caption, MB_ICONSTOP);
                Result := False;
        end;
    end;

    try
{INICIO: TASK_005_JMA_20160418	
        FCodigoConceptoArriendo := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA()');
}
		FCodigoConceptoArriendo := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}		
        if FCodigoConceptoArriendo <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_ARRIENDO_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;

//Revision 14    
    try
{INICIO: TASK_005_JMA_20160418	
        FCodigoConceptoSoporteTelevia := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_SOPORTE_TELEVIA()');
}		
		FCodigoConceptoSoporteTelevia := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_SOPORTE_TELEVIA(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}		
        if FCodigoConceptoSoporteTelevia <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_SOPORTE_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_SOPORTE, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //arriendo en cuotas por mail
    try
{INICIO: TASK_005_JMA_20160418		
        FCodigoConceptoArriendoCuotasMail := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL()');
}		
		FCodigoConceptoArriendoCuotasMail := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}				
        if FCodigoConceptoArriendoCuotasMail <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_ARRIENDO_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //arriendo en cuotas por correo postal
    try
{INICIO: TASK_005_JMA_20160418	
        FCodigoConceptoArriendoCuotasPostal := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL()');
}
		FCodigoConceptoArriendoCuotasPostal := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}	
        if FCodigoConceptoArriendoCuotasPostal <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_ARRIENDO_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;

end;
//Fin de la Revision 14 


procedure TfrmIngresarComprobantes.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposComprobantes;
  Author:    flamas
  Date Created: 22/01/2005
  Description: Carga los tipos de Comprobantes
  Parameters: TipoComprobante: AnsiString;
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.CargarTiposComprobantes;
begin
    cbTiposComprobantes.Items.Clear;
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_FACTURA), TC_FACTURA);

//    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_CREDITO), TC_NOTA_CREDITO);
//   los cr�ditos NC por ahora se ingresan solo como anulaciones de boletas jconcheyro 06/11/2006
//   si se vuelven a ingresar desde ac�, hay que usar el nuevo m�todo de numeraci�n de comprobantes

    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_DEBITO), TC_NOTA_DEBITO);
// las unicas boletas que se pueden hacer desde facturacion son las boletas manuales
// pero por ahora deben estar desactivadas SACAR PARA VOLVER A TENER BOLETAS MANUALES
// sacar este if cuando se necesite nuevamente
    if FImprimirBoletaEnImpresoraFiscal then
        cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_BOLETA), TC_BOLETA);

//  Agregamos Boleta de Arriendo, pero en realidad es una boleta com�n.
//    if FImprimirBoletaEnImpresoraFiscal then
//        if ObtenerCotizacionUF(NowBase(DMConnections.BaseCAC)) then
//            cbTiposComprobantes.Items.Add(TC_ARRIENDO, TC_BOLETA);
// lo pase al change de convenios


    cbTiposComprobantes.Value := Null;
    chkBoletaManual.Checked := False;
    chkBoletaManual.Visible := False;
    FTipoComprobante := EmptyStr;
end;

{******************************** Function Header ******************************
Function Name: ObtenerSignoMovimientoCuenta
Author       : FLamas
Date Created : 24-01-2005
Description  : Devuelve el signo del Movimiento de cuenta
Parameters   : Sender:
Return Value : None
*******************************************************************************}
function TfrmIngresarComprobantes.ObtenerSignoMovimientoCuenta : integer;
begin
	if FTipoComprobante = TC_NOTA_CREDITO then Result := -1
    else Result := 1;
end;


{******************************** Function Header ******************************
Function Name: CargarComprobantesPendientes
Author       : FLamas
Date Created : 24-01-2005
Description  : Carga los comprobantes pendientes de Facturacion
Parameters   : Sender:
Return Value : None

Revision 1
Author: nefernandez
Date: 09/04/2008
Description: Se redondean los valores de importes.
*******************************************************************************}
procedure TfrmIngresarComprobantes.CargarComprobantesPendientes;
begin
    FTotalComprobante   := 0;

    lbl_Importe.Caption := FormatearImporteConDecimales(DMConnections.BaseCAC,FTotalComprobante, True, True, True);
	cdsObtenerPendienteFacturacion.DisableControls;
    try
        cdsObtenerPendienteFacturacion.EmptyDataSet;
        with spObtenerMovimientosAFacturar do begin
            Close;
            Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            Parameters.ParamByName('@TipoComprobante').Value := cbTiposComprobantes.Value;
            Open;
            while not EOF do begin
                cdsObtenerPendienteFacturacion.Insert;
                cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False;
                cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FieldByName('CodigoConvenio').AsInteger;
                cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').AsInteger := FieldByName('NumeroMovimiento').AsInteger;
                cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := FieldByName('FechaHora').AsDateTime;
                cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := FieldByName('CodigoConcepto').AsInteger;
                cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := FieldByName('Concepto').AsString;
                cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := FieldByName('Observaciones').AsString;
                cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, Round(FieldByName('ImporteCtvs').AsFloat / 100), True, True, True);
                cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round(Abs( FieldByName('ImporteCtvs').AsFloat / 100 ));
                cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
                cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := FieldByName('PorcentajeIVA').AsInteger / 100; //19
                cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round(Abs(FieldByName('ImporteIVA').AsFloat / 100)) ;
                cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
                cdsObtenerPendienteFacturacion.Post;
                Next;
            end;
            Close;
        end;
        cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion);
        //de esta manera definimos que solo se pueden modificar solamente los movimientos que se ingresen como nuevos en este form
//        btnModificarItem.Enabled := not cdsObtenerPendienteFacturacion.IsEmpty;
//        btnEliminarItem.Enabled := not cdsObtenerPendienteFacturacion.IsEmpty;
    finally
        cdsObtenerPendienteFacturacion.EnableControls;
    end; // finally
end;

procedure TfrmIngresarComprobantes.Marcarcomoimpreso1Click(Sender: TObject);
begin
    cdsObtenerPendienteFacturacion.DisableControls;
    cdsObtenerPendienteFacturacion.Edit;
    cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').asBoolean := (not cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').asBoolean);
    cdsObtenerPendienteFacturacion.Post;
    cdsObtenerPendienteFacturacion.EnableControls;
    Application.ProcessMessages;
    cdsObtenerPendienteFacturacionAfterScroll(cdsObtenerPendienteFacturacion);
end;


{-----------------------------------------------------------------------------
  Function Name: MostrarDomicilioFacturacion
  Author:    flamas
  Date Created: 23/03/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.MostrarDomicilioFacturacion;
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        try
        	ExecProc;
    		lDomicilio.Caption	:= ParamByName('@Domicilio').Value;
 			lComuna.Caption		:= ParamByName('@Comuna').Value;
            lRegion.Caption 	:= ParamByName('@Region').Value;
        except
        	on E: Exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: cbConveniosClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la lista de convenios se refrescan
               la lista de items de la factura a realizar.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.cbConveniosClienteChange(Sender: TObject);
var
  Index: integer;
begin
    if cbConveniosCliente.ItemIndex = -1 then Exit;
    FCodigoConvenio := cbConveniosCliente.Value;
	MostrarDomicilioFacturacion;
	CargarComprobantesPendientes;
    for Index := cbTiposComprobantes.Items.Count - 1 downto  0 do begin
        if (cbTiposComprobantes.Items[Index].Caption = TC_ARRIENDO) OR (cbTiposComprobantes.Items[Index].Caption = TC_ARRIENDO_CUOTAS) then begin
            cbTiposComprobantes.Items.Delete(Index);
        end;
    end;

    //if QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.EsConvenioCN(%d)', [FCodigoConvenio])) = 1 then										//SS_1147_MCA_20140408
    if QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.EsConvenioNativo(%d)', [FCodigoConvenio])) = 1 then			//SS_1147_MCA_20140408

        if FImprimirBoletaEnImpresoraFiscal then
        begin
                cbTiposComprobantes.Items.Add(TC_ARRIENDO, TC_BOLETA);
                cbTiposComprobantes.Items.Add(TC_ARRIENDO_CUOTAS, TC_BOLETA);
        end;

    cbTiposComprobantes.ItemIndex := 0 ;
    cbTiposComprobantesChange(Self);


end;

procedure TfrmIngresarComprobantes.CbSoporteClick(Sender: TObject);
begin
    FIncluirSoporte := CbSoporte.Checked;
    SetearDatosArriendo;
end;

{******************************** Function Header ******************************
Function Name: peRUTClienteButtonClick
Author       : fmalisia
Date Created : 24-11-2004
Description  : Permite buscar un cliente por medio del form FormBuscaClientes.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);
    Application.CreateForm(TFormBuscaClientes, F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peRUTCliente.Text :=  F.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
        end;
    end;
    btnBuscarRUTClick(Self);
    F.Free;
end;


procedure TfrmIngresarComprobantes.peRUTClienteChange(Sender: TObject);
begin
    cbConveniosCliente.Clear;
    cbConveniosClienteChange(Self);
end;

{******************************** Function Header ******************************
Function Name: LimpiarEncabezadoImpresora
Author       : aganc
Date Created : 20/06/2006
Description  : setea las lineas 6,7,8,9 y 10 de la impresora en blanco
Parameters   : None
Return Value : boolean
*******************************************************************************}
Function TfrmIngresarComprobantes.LimpiarEncabezadoImpresora : boolean;
resourcestring
    MSG_DESCONFIGURACION_NO_POSIBLE = 'No se puede realizar la configuraci�n de la impresora para limpiar los datos del cliente.'
                                     + CRLF + 'Contacte al administrador del sistema.';
begin
    try
        if ImpresoraFiscal.CanSetup then begin
            ImpresoraFiscal.ProgramarEncabezado(6, '  ');
            ImpresoraFiscal.ProgramarEncabezado(7, '  ');
            ImpresoraFiscal.ProgramarEncabezado(8, '  ');
            ImpresoraFiscal.ProgramarEncabezado(9, '  ');
            ImpresoraFiscal.ProgramarEncabezado(10, '  ');
            Result := True;
        end else begin
            MsgBoxErr(MSG_DESCONFIGURACION_NO_POSIBLE,
                MSG_DRIVER_IMPRESORA + MSG_DESCONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
            Result := False;
        end;
    except
        MsgBoxErr(MSG_DESCONFIGURACION_NO_POSIBLE,
            MSG_DRIVER_IMPRESORA + MSG_DESCONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
        Result := False;
    end;
end;

procedure TfrmIngresarComprobantes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    // Desactivar la Impresora Fiscal
    if FImprimirBoletaEnImpresoraFiscal and ImpresoraFiscal.Active then begin
        ImpresoraFiscal.Active := False;
    end;

	Action := caFree;
end;

procedure TfrmIngresarComprobantes.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;

procedure TfrmIngresarComprobantes.ClearCaptions;
begin
    lNombreCompleto.Caption	:= '';
    lDomicilio.Caption      := '';
    lComuna.Caption         := '';
    lRegion.Caption 		:= '';
    lblRUTRUT.CAption       := '';
end;

{******************************** Function Header ******************************
Function Name: CompletarDatosClienteArchivo
Author : vpaszkowicz
Date Created : 17/08/2007
Description : Recupera los datos del cliente almacenado en el archivo.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.CompletarDatosConClienteDelArchivo;
begin
    obtenerCliente.Close;
    obtenerCliente.Parameters.ParamByName('@CodigoCliente').Value := FCodigoCliente;
    obtenerCliente.Parameters.ParamByName('@CodigoDocumento').Value := NULL;
    obtenerCliente.Parameters.ParamByName('@NumeroDocumento').Value := NULL;
    obtenerCliente.Open;
    if not ObtenerCliente.Eof then begin
        lblRUTRUT.Caption := obtenerCliente.FieldByName('NumeroDocumento').AsString;
            // Mostramos el nombre completo del cliente
        if (obtenerCliente.FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
            lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(obtenerCliente.FieldByName('Apellido').asString),
            Trim(obtenerCliente.FieldByName('ApellidoMaterno').asString), Trim(obtenerCliente.FieldByName('Nombre').asString))
        else
            lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(obtenerCliente.FieldByName('Apellido').asString),
            Trim(obtenerCliente.FieldByName('ApellidoMaterno').asString));
    end;
end;

{******************************** Function Header ******************************
Function Name: dbItemsPendientesDrawText
Author       : flamas
Date Created : 24-01-2005
Description  : Dibuja el s�mbolo de Checked
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.dbItemsPendientesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcado') then begin
            DefaultDraw := False;
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            if (cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean ) then
          	    Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);
            canvas.Draw(rect.Left, rect.Top,bmp);
            bmp.Free;
        end
        else begin
            if (Column.FieldName = 'EstaImpreso') then begin
                DefaultDraw := False;
                Text := '';
                canvas.FillRect(rect);
                bmp := TBitMap.Create;
                if (cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean ) then
          	        Imagenes.GetBitmap(2, Bmp)
                else
                    Imagenes.GetBitmap(1, Bmp);
                canvas.Draw(rect.Left, rect.Top,bmp);
                bmp.Free;
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: dbItemsPendientesDblClick
Author       : flamas
Date Created : 24-01-2005
Description  : Marca y Desmarca items
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.dbItemsPendientesDblClick(Sender: TObject);
begin
	if cdsObtenerPendienteFacturacion.IsEmpty then Exit;

	with cdsObtenerPendienteFacturacion do begin
    	Edit;
        FieldByName('Marcado').AsBoolean := not FieldByName('Marcado').AsBoolean;
        Post;
        ActualizarBotonRegistrar;
    end;
end;

{******************************** Function Header ******************************
Function Name: ActualizarBotonRegistrar
Author       : flamas
Date Created : 24-01-2005
Description  : Habilita y deshabilita el bot�n de registrar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.ActualizarBotonRegistrar;
var
	bHayMarcados : boolean;
    FBookmark  	 : TBookmark;
begin
    FTotalComprobante := 0;
    lbl_Importe.Caption := FormatearImporteconDecimales(DMConnections.BaseCAC, FTotalComprobante, true, true, true);
    with cdsObtenerPendienteFacturacion do begin
    	bHayMarcados := False;
        DisableControls;
		FBookmark := GetBookmark;
        First;
        // Verifica si hay items marcados
        while not EOF do begin
        	if ( FieldByName('Marcado').AsBoolean ) then begin
            	bHayMarcados := True;
                FTotalComprobante := FTotalComprobante + FieldByName('ImporteCtvs').AsFloat ;
            end;
            Next;
        end;
        GotoBookmark( FBookmark );
        FreeBookmark( FBookmark );
        lbl_Importe.Caption := FormatearImporteconDecimales(DMConnections.BaseCAC, FTotalComprobante, true, true, true);
        EnableControls;
    end;

    btnRegistrar.Enabled := (not cdsObtenerPendienteFacturacion.IsEmpty) and
    						(bHayMarcados) and
                            (cbTiposComprobantes.Value <> Null) and
    						((((cbTiposComprobantes.Value = TC_BOLETA) or (cbTiposComprobantes.Value = TC_ARRIENDO)) and (FTotalComprobante > 0)) or ((cbTiposComprobantes.Value <> TC_BOLETA) and (cbTiposComprobantes.Value <> TC_ARRIENDO))) and
                            //(cbTiposComprobantes.Value <> Null) and
    						((cbTiposComprobantes.Value = TC_BOLETA) or (edNumeroComprobante.Text <> EmptyStr)) and
    						(edFechaEmision.Date <> Nulldate);
                            // and (edFechaVencimiento.Date <> nulldate);



end;


procedure TfrmIngresarComprobantes.edCantidadCuotasExit(Sender: TObject);
resourcestring
    MSG_ERROR_CANTIDAD_CUOTAS = 'La cantidad de cuotas debe estar entre %d y %d';
begin
    if not ValidarCuotasArriendo then
      MsgBoxBalloon(format(MSG_ERROR_CANTIDAD_CUOTAS, [FMinimaCantidadCuotasArriendo,FMaximaCantidadCuotasArriendo]),Caption, MB_ICONSTOP, edCantidadCuotas);
end;

function TfrmIngresarComprobantes.ValidarCuotasArriendo:boolean;
begin
    Result := True;
    if not ((edCantidadCuotas.ValueInt >= FMinimaCantidadCuotasArriendo) and
        (edCantidadCuotas.ValueInt <= FMaximaCantidadCuotasArriendo)) then
        Result := False;
end;


procedure TfrmIngresarComprobantes.edNumeroComprobanteChange(Sender: TObject);
begin
    ActualizarBotonRegistrar;
end;

procedure TfrmIngresarComprobantes.edNumeroTeleviaExit(Sender: TObject);
var
    EsMoto : Boolean;
begin
    if not ValidarTelevia then Exit;
//REVISION 14
    if cbTiposComprobantes.Text = TC_ARRIENDO then begin
        if QueryGetValue(DMConnections.BaseCAC,
            format('select dbo.EsTagMoto(''%s'')',
            [edNumeroTelevia.Text]))= 'True' then begin
                EsMoto := True
            end else begin
                EsMoto := False;
            end;
        if EsMoto then begin
            AgregarSoporteInterfaz (True);
            CbSoporte.Checked := True;
            SetearDatosArriendo;
        end else begin
            AgregarSoporteInterfaz (False);
            CbSoporte.Checked := False;
        end;
    end else begin
        AgregarSoporteInterfaz (False);
        CbSoporte.Checked := False;
    end;
//FIN DE REVISION 14
end;

{******************************** Function Header ******************************
Function Name: ValidarTelevia
Author : jconcheyro
Date Created : 08/11/2006
Description : Busca unicamente el valor sin importar el estado de la cuenta
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmIngresarComprobantes.ValidarTelevia: boolean;
resourcestring
    MSG_ERROR_TELEVIA0 = 'El telev�a ingresado no existe';
    MSG_ERROR_TELEVIA1 = 'El telev�a pertence al convenio pero est� mal ingresado';
    MSG_ERROR_TELEVIA2 = 'El telev�a est� bien ingresado pero no pertenece al convenio';
    MSG_ERROR_TELEVIA3 = 'El telev�a ingresado no fue entregado en arriendo en cuotas para este convenio';

    MSG_ERROR_TELEVIA_1 = 'Atenci�n: El telev�a que se indic�, est� actualmente en una boleta de arriendo no anulada para este convenio';
    MSG_ERROR_TELEVIA_2 = 'El telev�a no est� en arriendo o entregado al cliente. No se puede facturar el arriendo';
var
    SerialNumber: DWORD;
    ValorDevuelto: integer;
    ResultadoFase1: boolean ;
    ResultadoFase2: boolean ;
    StringMensajeError: string;
    ResultadoFase3: boolean ;
begin
    // no puedo lanzar dos MsgBoxBalloon juntos...

    ValorDevuelto := QueryGetValueInt(DMConnections.BaseCAC,
               //format('select dbo.ExisteTeleviaEnCuentaCN(%d,''%s'')',		//SS_1147_MCA_20140408
               Format('select dbo.ExisteTeleviaEnCuentaNativa(%d, ''%s'')',     //SS_1147_MCA_20140408
                [FCodigoConvenio , edNumeroTelevia.Text ])) ;


    ResultadoFase1 := False;
    ResultadoFase2 := False;
    StringMensajeError := '';
    ResultadoFase3 := True;

    // entonces: si la respuesta es 1, la cuenta est� bien pero el TAG Mal
    // si la respuesta es cero ninguno de los dos est� bien
    // si la respuesta es 3, los dos est�n bien


    if ValorDevuelto = 0  then StringMensajeError := MSG_ERROR_TELEVIA0;
    if ValorDevuelto = 1  then StringMensajeError := MSG_ERROR_TELEVIA1;
    if ValorDevuelto = 2  then StringMensajeError := MSG_ERROR_TELEVIA2;
    if ValorDevuelto = 3  then ResultadoFase1 := True;

    SerialNumber := EtiquetaToSerialNumber(edNumeroTelevia.Text);

    // si el resultado es 0, no existe en arriendo y est� en situacion de ser arrendado
    // si el resultado es 1, entonces est� en arriendo y ya sali� por arriba
    // si el resultado es 2, no est� en arriendo y no puede ser arrendado.
    ValorDevuelto := QueryGetValueInt(DMConnections.BaseCAC,
                    format('select dbo.ExisteTeleviaEnArriendoConvenio(%d,%d)',
                    [FCodigoConvenio,SerialNumber])) ;

    if ValorDevuelto = 0  then ResultadoFase2 := True;
    if ValorDevuelto = 1  then begin  //esto ahora solo genera un warning rev 16
            ResultadoFase2 := True;
            StringMensajeError := StringMensajeError + CRLF + MSG_ERROR_TELEVIA_1;
    end;
    if ValorDevuelto = 2  then StringMensajeError := StringMensajeError + CRLF + MSG_ERROR_TELEVIA_2;

    if (StringMensajeError <> '')  then
        if (ValorDevuelto <> 1) then MsgBoxBalloon(StringMensajeError ,Caption, MB_ICONSTOP, edNumeroTelevia)
            else MsgBoxBalloon(StringMensajeError ,Caption, MB_ICONWARNING, edNumeroTelevia);

    // valido lo correspondiente a cuotas
    if cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS then begin
        ValorDevuelto := QueryGetValueInt(DMConnections.BaseCAC,
                            format('SELECT dbo.TagEntregadoEnArriendoEnCuotas(%d,''%s'')',
                            [FCodigoConvenio , edNumeroTelevia.Text ]));

        if ValorDevuelto = 1 then
            ResultadoFase3 := True
        else begin
            StringMensajeError :=  StringMensajeError + CRLF + MSG_ERROR_TELEVIA3;
            MsgBoxBalloon(StringMensajeError ,Caption, MB_ICONWARNING, edNumeroTelevia);
            ResultadoFase3 := False;
        end;
    end;

    Result := ResultadoFase1 and ResultadoFase2 and ResultadoFase3;


end;




{******************************** Function Header ******************************
Function Name: cdsObtenerPendienteFacturacionAfterScroll
Author       : flamas
Date Created : 24-01-2005
Description  : Habilita y deshabilita los botones de modificar y eliminar
Parameters   : DataSet: TDataSet
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.cdsObtenerPendienteFacturacionAfterScroll(DataSet: TDataSet);
begin
    // Habilito el bot�n de Modificar si es un item Nuevo y no estoy
    // registrando un comprobante.
	btnModificarItem.Enabled := (cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean)
                                    and (nb_Botones.PageIndex = BOTON_REGISTRAR)
                                    and (not FRegistrandoBoletaFiscal);
    btnEliminarItem.Enabled	:= btnModificarItem.Enabled;
    if (cbTiposComprobantes.Text = TC_ARRIENDO) OR (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then
        btnEliminarItem.Enabled	:= False ;
    if dbItemsPendientes.PopupMenu <> nil then
        MarcarComoImpreso1.Caption := iif(cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').asBoolean, 'Marcar como no impreso', 'Marcar como impreso');
end;

{******************************** Function Header ******************************
Function Name: EsImporteValido
Author : vpaszkowicz
Date Created : 20/08/2007
Description : Retorna true si el importe es igual o menor al m�ximo largo permi-
tido por la impresora fiscal.
Parameters : ImporteConIva: double
Return Value : Boolean

Revision 1
Author: FSandi
Date: 21/08/2007
Description: Se cambia la devoluci�n asinteger por AsFloat para evitar desbordamientos (SS 548)
Revision :2
    Author : vpaszkowicz
    Date : 27/12/2007
    Description : Le agrego un bookmark para que no me mueva al modificar.
*******************************************************************************}
function TfrmIngresarComprobantes.EsImporteValido(ImporteConIva: double): Boolean;
var
    TotalComprobante: double;
    FBookmark: TBookmark;
begin
    TotalComprobante := 0;
    Result := False;
    if (CbTiposComprobantes.Value = TC_BOLETA) or (CbTiposComprobantes.Value = TC_ARRIENDO) OR (CbTiposComprobantes.Value = TC_ARRIENDO_CUOTAS) then begin
        if ImporteConIva <= CONST_MAXIMO_IMPORTE_IMPRESORA then begin
            cdsObtenerPendienteFacturacion.DisableControls;
            FBookmark := cdsObtenerPendienteFacturacion.GetBookMark;
            cdsObtenerPendienteFacturacion.First;
            while not cdsObtenerPendienteFacturacion.Eof do begin
                TotalComprobante := TotalComprobante + cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').asfloat;  //Revision 1
                cdsObtenerPendienteFacturacion.Next;
            end;
            cdsObtenerPendienteFacturacion.GotoBookmark(FBookmark);
            cdsObtenerPendienteFacturacion.FreeBookmark(FBookmark);
            cdsObtenerPendienteFacturacion.EnableControls;
            Result := (TotalComprobante + ImporteConIva <=  CONST_MAXIMO_IMPORTE_TOTAL);
        end
        else Result := False;
    end
    else Result := True;
end;

{******************************** Function Header ******************************
Function Name: btnAgregarItemClick
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega un item a facturar
Parameters   : Sender: TObject
Return Value : None

Revision: 1
Author: jconcheyro
Date: 08/09/2006
Description: Se utiliza ahora el form frmCargoNotaCredito que tiene el IVA discriminado
Revision : 2
    Author : vpaszkowicz
    Date : 17/08/2007
    Description : Valido que el importe no seme salga del m�ximo permitido por
    la impresora.
*******************************************************************************}
procedure TfrmIngresarComprobantes.btnAgregarItemClick(Sender: TObject);
resourceString
    MSG_IMPORTE_INVALIDO = 'El importe ingresado no est� permitido por la impresora.';
var
	f : TfrmCargoNotaCredito;
    EditorFechasHabilitado : boolean;
    MensajeDeLaTercera: string;
begin
    Application.CreateForm(TfrmCargoNotaCredito, f);
    f.Visible := False;
    if cbTiposComprobantes.Value = TC_BOLETA  then EditorFechasHabilitado := False else
        EditorFechasHabilitado := True;

    with cdsObtenerPendienteFacturacion do begin
    	if f.Inicializar(EditorFechasHabilitado, false, false, cbTiposComprobantes.Value, ObtenerIVA, edFechaEmision.Date, False) then begin
    		if f.ShowModal = mrOK then begin
                //Rev13 este if
                if f.cbConcepto.Value = FCodigoConceptoLaTercera then begin
                    ObtenerParametroGeneral(DMConnections.BaseCAC, 'MENSAJE_BOLETA_LA_TERCERA' , MensajeDeLaTercera);
                    MsgBoxBalloon(MensajeDeLaTercera,Caption, MB_ICONSTOP,btnAgregarItem);
                end else begin
                    if not EsImporteValido(f.edImporte.Value) then begin
                        MsgBoxBalloon(MSG_IMPORTE_INVALIDO, Caption, MB_ICONSTOP, btnAgregarItem);
                        Exit;
                    end
                    else begin
                        Append;
                        FieldByName('Marcado').AsBoolean        := True;
                        FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
                        FieldByName('FechaHora').AsDateTime     := f.edFecha.Date;
                        FieldByName('CodigoConcepto').AsInteger := f.cbConcepto.Value;
                        FieldByName('Concepto').AsString        := f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
                        FieldByName('Observaciones').AsString   := f.edDetalle.Text;
                        FieldByName('DescImporte').AsString     := FormatearImporteConDecimales(DMConnections.BaseCAC,f.edImporte.Value + f.edImporteIVA.Value, True, True, True);
                        FieldByName('ImporteCtvs').AsFloat      := f.edImporte.Value + f.edImporteIVA.Value;
                        FieldByName('Nuevo').AsBoolean          := True;
                        FieldByName('Cantidad').AsFloat         := 1;
                        FieldByName('PorcentajeIVA').AsFloat   := f.edPorcentajeIVA.Value; //19
                        FieldByName('ValorNeto').Value          := f.edImporte.Value ;
                        FieldByName('ImporteIVA').Value         := f.edImporteIVA.Value;
                        FieldByName('EstaImpreso').AsBoolean    := False;
                        Post;
                        ActualizarBotonRegistrar;
                        btnModificarItem.Enabled    := not cdsObtenerPendienteFacturacion.IsEmpty;
                        btnEliminarItem.Enabled     := not cdsObtenerPendienteFacturacion.IsEmpty;
                    end;
                    cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion );
                end;
            end;
        end;
    end;
    f.Release;
end;

procedure TfrmIngresarComprobantes.SetearEventosClientDataSet(Activar: Boolean);
begin
    if Activar then begin
        cdsObtenerPendienteFacturacion.AfterScroll := cdsObtenerPendienteFacturacion.AfterScroll;
        cdsObtenerPendienteFacturacion.OnCalcFields := cdsObtenerPendienteFacturacionCalcFields;
        cdsObtenerPendienteFacturacion.EnableControls;
    end
    else begin
        cdsObtenerPendienteFacturacion.DisableControls;
        cdsObtenerPendienteFacturacion.AfterScroll := nil;
        cdsObtenerPendienteFacturacion.OnCalcFields := nil;
    end;
end;


{******************************** Function Header ******************************
Function Name: btnModificarItemClick
Author       : flamas
Date Created : 24-01-2005
Description  : Modifica un item a facturar
Parameters   : Sender: TObject
Return Value : None

Revision: 1
Author: jconcheyro
Date: 08/09/2006
Description: Se utiliza ahora el form frmCargoNotaCredito que tiene el IVA discriminado

Revision : 2
    Author : vpaszkowicz
    Date : 20/08/2007
    Description : Controlo que si es boleta, que no se pase del m�ximo de la bo-
    leta fiscal.
*******************************************************************************}
procedure TfrmIngresarComprobantes.btnModificarItemClick(Sender: TObject);
resourceString
    MSG_IMPORTE_INVALIDO = 'El importe ingresado no est� permitido por la impresora.';
var
	f : TfrmCargoNotaCredito;
    EditorFechasHabilitado : boolean;
    MensajeDeLaTercera: string;
begin
    // Si no hay items no permitir modificar.
    if cdsObtenerPendienteFacturacion.IsEmpty then Exit;

    //Desasociamos los eventos.
    SetearEventosClientDataSet(False);

    // no se puede modificar la fecha del movimiento para boletas, de arriendo o no
    if cbTiposComprobantes.Value = TC_BOLETA  then EditorFechasHabilitado := False else
    EditorFechasHabilitado := True;

    Application.CreateForm(TfrmCargoNotaCredito, f);
    f.Visible := False;
    if f.Inicializar(EditorFechasHabilitado, false, false, cbTiposComprobantes.Text <> TC_ARRIENDO, cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').Value,
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat,
        99999999,
        cdsObtenerPendienteFacturacion.FieldByName('Observaciones').Value,
        '',
        cbTiposComprobantes.Value,
        ObtenerIVA,
        cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime,
        False) then begin
        if f.ShowModal = mrOK then begin
            //Rev13 este if
            if f.cbConcepto.Value = FCodigoConceptoLaTercera then begin
                ObtenerParametroGeneral(DMConnections.BaseCAC, 'MENSAJE_BOLETA_LA_TERCERA' , MensajeDeLaTercera);
                MsgBoxBalloon(MensajeDeLaTercera,Caption, MB_ICONSTOP,btnAgregarItem);
            end else begin
                if not EsImporteValido(f.edImporte.Value) then begin
                        MsgBoxBalloon(MSG_IMPORTE_INVALIDO, Caption, MB_ICONSTOP, btnModificarItem);
                        Exit;
                end
                else begin
                    cdsObtenerPendienteFacturacion.Edit;
                    cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime       := f.edFecha.Date;
                    cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger   := f.cbConcepto.Value;
                    cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString          := f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
                    cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString     := f.edDetalle.Text;
                    cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString       := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value + f.edImporteIVA.Value, True, True, True);
                    cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat        := f.edImporte.Value + f.edImporteIVA.Value;
                    cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat           := 1;
                    cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat     := f.edPorcentajeIVA.Value; //19
                    cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').Value            := f.edImporte.Value ;
                    cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').Value           := f.edImporteIVA.Value;
                    cdsObtenerPendienteFacturacion.Post;
                end;
            end;
            ActualizarBotonRegistrar;
        end;
    end;
    //Asociamos los eventos.
    SetearEventosClientDataSet(True);
    f.Release;
end;

{******************************** Function Header ******************************
Function Name: btnEliminarItemClick
Author       : flamas
Date Created : 24-01-2005
Description  : Elimina un item a facturar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.btnEliminarItemClick(Sender: TObject);
resourcestring
	MSG_ITEM_DELETE_CONFIRMATION = 'Desea eliminar el item ?';
begin
    // Si no hay items no permitir eliminar.
    if cdsObtenerPendienteFacturacion.IsEmpty then Exit;

	if MsgBox(	MSG_ITEM_DELETE_CONFIRMATION, Caption,
    			MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES then begin
		cdsObtenerPendienteFacturacion.Delete;
		ActualizarBotonRegistrar;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnRegistrarClick
Author       : flamas
Date Created : 24-01-2005
Description  : Registra los movimientos de cuenta, el lote de facturacion y
				genera el comprobante
Parameters   : Sender: TObject
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 20/02/2007
    Description : Agrego que si estoy trabajando con una boleta normal (no de
    arriendo) y me ingresa por alguna causa externa que puede ser un alta de
    cuenta, un mov. de arriendo, si este no est� marcado para que se imprima,
    lo dejo que continue con la impresi�n.
Revision :
    Author : vpaszkowicz
    Date : 28/02/2007
    Description : Le agrego una verificaci�n del estado de la impresora antes de
    entrar a continuar con una boleta fiscal, ya que necesito que cuando entre a
    este m�todo la impresora este encendida.    
*******************************************************************************}
procedure TfrmIngresarComprobantes.btnRegistrarClick(Sender: TObject);
resourcestring
	MSG_INVOICE_NUMBER_ALREADY_EXISTS 	   	= 'El comprobante ingresado ya existe.';
    MSG_DUE_DATE_MUST_BE_LATER_THAN_ISSUE 	= 'La fecha de vencimiento debe ser posterior a la de emisi�n';
    MSG_DUE_DATE_MUST_BE_WORKING_DATE 		= 'La fecha de vencimiento debe ser un d�a laborable';
    MSG_INVOICE_GENERATED_OK             	= 'La %s se gener� exitosamente.';
    MSG_INVOICE_NUMBER_MUST_BE_POSITIVE     = 'El N� de la boleta debe ser mayor a cero.';
	MSG_ERROR_IMPRESORA_NO_ENCONTRADA 	   	= 'No se encontro la impresora en la nomina de impresoras registadas';
	MSG_ERROR_IMPRESORA_MANUAL_NO_ENCONTRADA = 'No se encontro la impresora manual en la nomina de impresoras registadas';
    MSG_ERROR_GENERAR_COMPROBANTE           = 'Ocurri� un error al generar el comprobante.';
    MSG_ERROR_OBTENER_NUMERO_COMPROBANTE    = 'Ocurri� un error al obtener el identificador para la Boleta.';
	MSG_NUMERO_COMPROBANTE_FUERA_TALONARIO  = 'No se encuentra Talonario asignado para el comprobante ingresado';
    MSG_ERROR_CANTIDAD_CUOTAS = 'La cantidad de cuotas debe estar entre %d y %d';
    MSG_ERROR_CONCEPTO_ARRIENDO_NO_PERMITIDO = 'Solo se puede ingresar un concepto de arriendo en una boleta de arriendo';
    MSG_ERROR_IMPRESORA = 'Error tratando de acceder a la impresora';
    MSG_VERIFIQUE_IMPRESORA = 'Verifique que la impresora este encendida y en linea';
var
    NumeroSerie: PChar;
    unEstado: TEstadoImpresora;
begin
    // Si no hay items no permitir registrar.
    if cdsObtenerPendienteFacturacion.IsEmpty then Exit;

    // Si el comprobante a emitir es Facturao Boleta Manual, controlar el N�mero del Comprobante ingresado.
    // contra la columna Comprobantes.NumeroComprobanteFiscal
    if (cbTiposComprobantes.Value = TC_FACTURA) then begin
    	if not ValidateControls(
    		[edNumeroComprobante],
    		[ValidarComprobanteFiscal(0)], //el cero es el codigo de impresora fiscal que para factura no se usa
    		Caption,
            [MSG_INVOICE_NUMBER_ALREADY_EXISTS]) then Exit;
    end;

    if (cbTiposComprobantes.Text = TC_ARRIENDO) OR (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then begin
        // si es arriendo normal valido la cantidad de cuotas
        if (cbTiposComprobantes.Text = TC_ARRIENDO) then
                if not ValidarCuotasArriendo then begin
                    MsgBoxBalloon(format(MSG_ERROR_CANTIDAD_CUOTAS, [FMinimaCantidadCuotasArriendo,FMaximaCantidadCuotasArriendo]),Caption, MB_ICONSTOP, edCantidadCuotas);
                    Exit;
                end;
        if not ValidarTelevia then Exit;
    end else begin //si no es de arriendo no puede estar el cargo de arriendo
        cdsObtenerPendienteFacturacion.First;
        while not cdsObtenerPendienteFacturacion.eof do begin
        //Pregunto adem�s si est� marcado. Si no lo est� dejo que imprima el resto.
            if (cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger = FCodigoConceptoArriendo) and
                (cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean = True) then begin
                MsgBoxBalloon(MSG_ERROR_CONCEPTO_ARRIENDO_NO_PERMITIDO,Caption, MB_ICONSTOP, dbItemsPendientes);
                cdsObtenerPendienteFacturacion.First;
                Exit;
            end;
            cdsObtenerPendienteFacturacion.Next;
        end;
        cdsObtenerPendienteFacturacion.First;
    end;
    // Si el comprobante a emitir es NC o ND, controlar el N�mero del Comprobante ingresado.
    // contra la columna Comprobantes.NumeroComprobante
    if (cbTiposComprobantes.Value = TC_NOTA_CREDITO) or (cbTiposComprobantes.Value = TC_NOTA_DEBITO) then begin
    	if not ValidateControls(
    		[edNumeroComprobante],
    		[QueryGetValue( DMConnections.BaseCAC,
    		        Format('SELECT dbo.VerificarExistenciaComprobante (''%s'', %d )',
                	[cbTiposComprobantes.Value, edNumeroComprobante.ValueInt] )) = 'False'],
    		Caption,
            [MSG_INVOICE_NUMBER_ALREADY_EXISTS]) then Exit;
    end;

    // Si NO hay que imprimir Boleta y el comprobante a emitir ES Boleta, entonces en una boleta para
    // impresora fiscal pero que se ingresa a mano. Pero no es una boleta de talonario
    // controlar el N�mero del Comprobante ingresado.
    //esta opcion no se deber�a usar maa, ya que el ingreso de una boleta fiscal que no est� en la base
    //se deber�a hacer leyendo el archivo. Si ese archivo ya no existe por algun motivo, se usar� esta opcion
   if (not FImprimirBoletaEnImpresoraFiscal) and (cbTiposComprobantes.Value = TC_BOLETA)
            and (not chkBoletaManual.Checked) then begin
        if not ValidateControls(
            [edNumeroComprobante],
            [edNumeroComprobante.ValueInt > 0],
            Caption,
            [MSG_INVOICE_NUMBER_MUST_BE_POSITIVE]) then Exit;
    end; // if

    //si se est� ingresando una boleta de Talonario
    if (cbTiposComprobantes.Value = TC_BOLETA) and (chkBoletaManual.Checked) then begin
        // #1 El numero de comprobanteFiscal para la impresora fiscal manual no puede ser repetido
        // #2 El numero de Boleta Manual tiene que estar en un talonario asigando
        // #3 Debe esxistir una impresora fiscal con el flag de ImpresoraManual activo
    	if not ValidateControls(
                                  [edNumeroComprobante, // #1
                                   edNumeroComprobante, // #2
                                   chkBoletaManual],    // #3
                                  [ValidarComprobanteFiscal(ObtenerCodigoImpresoraManual), // #1
                                   ValidarBoletaManualContraTalonarios,  // #2
                                   Abs(ObtenerCodigoImpresoraManual) <> 0 ],  // #3
                                  Caption,
                                  [MSG_INVOICE_NUMBER_ALREADY_EXISTS,       // #1
                                   MSG_NUMERO_COMPROBANTE_FUERA_TALONARIO,  // #2
                                   MSG_ERROR_IMPRESORA_MANUAL_NO_ENCONTRADA // #3
                                   ]) then Exit;
    end;


	Screen.Cursor := crHourglass;
    try
        // Si el comprobante es Boleta, Obtener un NumeroComprobante para la Boleta a generar.
        // si el comprobante es boleta manual, o es boleta de impresora fiscal pero no hay un archivo de boleta pendiente....
        if ( cbTiposComprobantes.Value = TC_BOLETA ) and
           (  (chkBoletaManual.Checked = False and  not ExisteArchivoContinuarBoletaFiscalAbierta) or
              (chkBoletaManual.Checked = True )) then begin
            try
                FNumeroComprobanteBoleta := ObtenerNumeroSiguienteComprobante(spCrearComprobante.Connection , FTipoComprobante);
                //con esto nos aseguramos que tenemos base de datos
            except
                on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_OBTENER_NUMERO_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        // Si el Tipo de Comprobante seleccionado es Boleta y hay que imprimir Boleta,
        // entonces, imprimir la boleta.
        if ((cbTiposComprobantes.Value = TC_BOLETA) and (chkBoletaManual.Checked = False)) and FImprimirBoletaEnImpresoraFiscal then begin
            if not ImpresoraFiscal.Active then ImpresoraFiscal.Active := True;
            unEstado := ImpresoraFiscal.ObtenerEstado;
            if unEstado.Performance <> pNormal then begin
                MsgBoxErr(MSG_ERROR_IMPRESORA, MSG_VERIFIQUE_IMPRESORA, Caption, MB_ICONERROR);
                exit;
            end;
            //Obtiene el codigo de la impresora
            if not ExisteArchivoContinuarBoletaFiscalAbierta then begin
                if not ImpresoraFiscal.ObtenerNumeroSerie(NumeroSerie) then begin
                    MsgBoxErr(MSG_ERROR_AL_OBTENER_NRO_SERIE, MSG_DRIVER_IMPRESORA + MSG_ERROR_AL_OBTENER_NRO_SERIE, Caption, MB_ICONERROR);
                    Exit;
                end;

                FNumeroSerie        := NumeroSerie;
                FCodigoImpresora    := ObtenerCodigoImpresora(NumeroSerie);
                if (FCodigoImpresora = -1) then begin
                    MsgBoxErr(MSG_ERROR_IMPRESORA_NO_ENCONTRADA, MSG_BASE_DATOS + MSG_ERROR_IMPRESORA_NO_ENCONTRADA, Caption, MB_ICONERROR);
                    Exit;
                end;
            end;

            // Deshabilitar los componentes para que si ocurre un error no
            // permitir modificar la boleta que se intent� emitir.
            FRegistrandoBoletaFiscal := True;
            EnableControlsInContainer(gbDatosConvenio, not FRegistrandoBoletaFiscal);
            EnableControlsInContainer(gbDatosComprobante, not FRegistrandoBoletaFiscal);
            EnableControlsInContainer(gbItemsPendientes, not FRegistrandoBoletaFiscal);

            // Si hay un comprobante abierto en la impresora fiscal,
            // realizar la continuaci�n de la emisi�n.
            if ExisteArchivoContinuarBoletaFiscalAbierta then begin
                if FModoRecuperacionImpresion = MODO_RECUPERACION_CANCELAR then begin
                    if not CancelarBoleta then Exit;
                end else begin
                    if not ImprimirContinuacionBoletaFiscal then Exit;
                end;
            end else begin
                // Imprimir la boleta que se envi� a imprimir.
                if not ImprimirBoleta then Exit;
            end;
            // Habilitar los componentes.
            lbl_EstadoEmisionBoleta.Caption := EmptyStr;
            FRegistrandoBoletaFiscal := False;
            EnableControlsInContainer(gbDatosConvenio, not FRegistrandoBoletaFiscal);
            EnableControlsInContainer(gbDatosComprobante, not FRegistrandoBoletaFiscal);
            EnableControlsInContainer(gbItemsPendientes, not FRegistrandoBoletaFiscal);
            btnAgregarItem.Enabled      := False;
            btnModificarItem.Enabled    := False;
            btnEliminarItem.Enabled     := False;
            ResetearControles;
        end else begin
            // El Tipo de Comprobante NO es Boleta o NO hay que imprimir Boleta.
            try
                AsentarComprobante;
                ResetearControles;
            except
                on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_GENERAR_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                end;
            end; // except
        end; // else if (cbTiposComprobantes.Value = TC_BOLETA)

    finally
    	Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: AgregarNuevosMovimientosCuenta
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega los Movimientos de cuenta correspondientes a los
				nuevos items cargados
Parameters   :
Return Value : boolean

Revision:   1
Author:     ggomez
Date:       06/07/2005
Description: Cambi� de function a procedure.
*******************************************************************************}
procedure TfrmIngresarComprobantes.AgregarNuevosMovimientosCuenta;
resourcestring
	MSG_ERROR_UPDATING_ITEM = 'Error actualizando Movimientos de Cuenta.';
begin
	with cdsObtenerPendienteFacturacion do begin
    	DisableControls;
        cdsObtenerPendienteFacturacion.Filter := 'Marcado = 1';
        cdsObtenerPendienteFacturacion.Filtered := True;
        try
            First;
            while not EOF do begin
                if ( FieldByName('Nuevo').AsBoolean ) then
                    with spAgregarMovimientoCuentaIVA, Parameters do begin
                        try
                            ParamByName('@CodigoConvenio').Value        := cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger;
                            ParamByName('@IndiceVehiculo').Value        := NULL;
                            ParamByName('@FechaHora').Value	            := cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime;
                            ParamByName('@CodigoConcepto').Value        := cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger;
                            ParamByName('@Importe').Value               := Abs(cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat ) * ObtenerSignoMovimientoCuenta * 100;
                            ParamByName('@LoteFacturacion').Value       := NULL;
                            ParamByName('@EsPago').Value                := False;
                            ParamByName('@Observaciones').Value	        := cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString;
                            ParamByName('@NumeroPromocion').Value		:= NULL;
                            ParamByName('@NumeroFinanciamiento').Value	:= NULL;
                            ParamByName('@ImporteIVA').Value            := Abs(cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat ) * ObtenerSignoMovimientoCuenta * 100;
                            ParamByName('@PorcentajeIVA').Value         := Abs(cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat * 100 ) * ObtenerSignoMovimientoCuenta;
                            ParamByName('@NumeroMovimiento').Value      := NULL;
                            ParamByName('@UsuarioCreacion').Value      := UsuarioSistema;
                            ExecProc;
                            cdsObtenerPendienteFacturacion.Edit;
                            cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').Value := ParamByName('@NumeroMovimiento').Value;
                            cdsObtenerPendienteFacturacion.Post;
                        except
                            on E : Exception do begin
                                raise Exception.Create(MSG_ERROR_UPDATING_ITEM + CRLF + E.Message);
                            end;
                        end; // except
                end; // with
                Next;
            end; // while
        finally
            cdsObtenerPendienteFacturacion.Filtered := False;
            cdsObtenerPendienteFacturacion.Filter := '';
            EnableControls;
        end; // finally
    end; // with
end;

{******************************** Function Header ******************************
Function Name: AgregarLoteFacturacion
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega el lote de facturaci�n
Parameters   :
Return Value : boolean

Revision:   1
Author:     ggomez
Date:       06/07/2005
Description: Cambi� de function a procedure.

Revision:   1
Author:     mvitali
Date:       22/09/2005
Description: Agrege como parametro el n�mero de comprobante
*******************************************************************************}
procedure TfrmIngresarComprobantes.AgregarLoteFacturacion(NumeroComprobante: integer);
resourcestring
	MSG_ERROR_GENERATING_INVOICE_BATCH = 'Error creando Lote de Facturaci�n.';
begin
	with spCrearLoteFacturacion, Parameters do begin
    	try
            ParamByName('@CodigoConvenio').Value 			:= FCodigoConvenio;
            ParamByName('@Total').Value 				   	:= Abs(FTotalComprobante * 100) * ObtenerSignoMovimientoCuenta;
            ParamByName('@FechaHora').Value				 	:= StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
            ParamByName('@NumeroProcesoFacturacion').Value 	:= NULL;
            ParamByName('@TipoComprobante').Value		   	:= FTipoComprobante;
            ParamByName('@NumeroComprobante').Value		 	:= NumeroComprobante;
            ExecProc;
            FLoteFacturacion := ParamByName('@LoteFacturacion').Value;
        except
        	on E : Exception do begin
                raise Exception.Create(MSG_ERROR_GENERATING_INVOICE_BATCH + CRLF + E.Message);
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: AgregarComprobante
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega la factura
Parameters   :
Return Value : boolean

Revision:   1
Author:     ggomez
Date:       06/07/2005
Description: Cambi� de function a procedure.

Revision:   2
Author:     mvitali
Date:       22/09/2005
Description: Cambi� de procedure a fucntion para devolver el numero de comprobante

Revision:   3
Author:     jconcheyro, ggomez
Date:       12/08/2006
Description: En el caso de comprobante Boleta, se setea el par�metro del
    NumeroComprobante.

*******************************************************************************}
function TfrmIngresarComprobantes.AgregarComprobante: integer;
resourcestring
	MSG_ERROR_GENERATING_INVOICE = 'Error generando el comprobante.';
begin
    if ((FTipoComprobante = TC_BOLETA) and (chkBoletaManual.Checked = False)) and FImprimirBoletaEnImpresoraFiscal then
        edNumeroComprobante.ValueInt := FNroBoletaFiscal;

    try
        spCrearComprobante.Parameters.ParamByName('@TipoComprobante').Value 		:= FTipoComprobante;

        if (FTipoComprobante = TC_BOLETA) then  begin
            spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobanteBoleta
            //solo las boletas por ahora usan numeracion no basada en MAX
        end else begin
            spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value		:= NULL;
        end;
        spCrearComprobante.Parameters.ParamByName('@NumeroComprobanteFiscal').Value	:= edNumeroComprobante.Text;
        spCrearComprobante.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := NULL;
        if (FTipoComprobante = TC_BOLETA) and FImprimirBoletaEnImpresoraFiscal then begin
            spCrearComprobante.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := FCodigoImpresora;
        end;

        if ((FTipoComprobante = TC_BOLETA) and (chkBoletaManual.Checked = True)) then begin
            spCrearComprobante.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := ObtenerCodigoImpresoraManual;
        end;

        spCrearComprobante.Parameters.ParamByName('@FechaEmision').Value := edFechaEmision.Date;
        spCrearComprobante.Parameters.ParamByName('@FechaVencimiento').Value := NULL; //iif( FTipoComprobante <> TC_NOTA_CREDITO, edFechaVencimiento.Date, NULL );
        spCrearComprobante.Parameters.ParamByName('@CodigoPersona').Value := FCodigoCliente;
        spCrearComprobante.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        spCrearComprobante.Parameters.ParamByName('@CodigoTipoMedioPago').Value := TPA_NINGUNO;
        spCrearComprobante.Parameters.ParamByName('@TotalComprobante').Value := Abs( FTotalComprobante * 100 );
        spCrearComprobante.Parameters.ParamByName('@TotalAPagar').Value := Abs( FTotalComprobante * 100 );
        spCrearComprobante.Parameters.ParamByName('@AjusteSencilloAnterior').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@AjusteSencilloActual').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto1').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto2').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto3').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto4').Value := 0;
        // Revision 13
        spCrearComprobante.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        spCrearComprobante.ExecProc;
        Result := spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value;

    except
        on E: Exception do begin
            raise Exception.Create(MSG_ERROR_GENERATING_INVOICE + CRLF + E.Message);
        end;
    end;
end;


procedure TfrmIngresarComprobantes.AgregarCuotasComprobanteArriendo;
begin
        if (cbTiposComprobantes.Text = TC_ARRIENDO) OR  (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then begin
            //creamos el comprobante de deuda y la primera cuota
            spCrearComprobanteCuotas.Parameters.ParamByName('@TipoComprobante'      ).Value := TC_BOLETA;
            spCrearComprobanteCuotas.Parameters.ParamByName('@NumeroComprobante'    ).Value := spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value;
            spCrearComprobanteCuotas.Parameters.ParamByName('@CantidadCuotasTotales').Value := edCantidadCuotas.ValueInt;
            spCrearComprobanteCuotas.Parameters.ParamByName('@ContractSerialNumber' ).Value := EtiquetaToSerialNumber(edNumeroTelevia.Text);
            spCrearComprobanteCuotas.Parameters.ParamByName('@UsuarioCreacion'      ).Value := UsuarioSistema;
            spCrearComprobanteCuotas.Parameters.ParamByName('@FechaEmision'         ).Value := edFechaEmision.Date;
            spCrearComprobanteCuotas.ExecProc;
        end;
end;


{******************************** Function Header ******************************
Function Name: ActualizarLotesFacturacion
Author       : flamas
Date Created : 24-01-2005
Description  : Actualiza los lotes de Facturaci�n en los movimientos de cuentas
Parameters   :
Return Value : boolean

Revision:   1
Author:     ggomez
Date:       06/07/2005
Description: Cambi� de function a procedure.
*******************************************************************************}
procedure TfrmIngresarComprobantes.ActualizarLotesFacturacion;
resourcestring
	MSG_ERROR_UPDATING_INVOICE_BATCH = 'Error actualizando Lote de Facturaci�n';
begin
	with cdsObtenerPendienteFacturacion do begin
    	DisableControls;
        try
            First;
            while not EOF do begin
                if ( FieldByName('Marcado').AsBoolean ) then
                    with spActualizarLoteFacturacion, Parameters do begin
                        try
                        ParamByName('@CodigoConvenio').Value := cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger;
                        ParamByName('@NumeroMovimiento').Value := cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').AsInteger;
                        ParamByName('@LoteFacturacion').Value := FLoteFacturacion;
                        ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
                        ExecProc;
                        except
                            on E : Exception do begin
                                raise Exception.Create(MSG_ERROR_UPDATING_INVOICE_BATCH + CRLF + E.Message);
                            end;
                        end;
                    end;
                Next;
            end;
        finally
            EnableControls;
        end;
    end;
end;

procedure TfrmIngresarComprobantes.cbTiposComprobantesChange(Sender: TObject);
begin
//Inicializamos el boton como false, en caso de que deba habilitarse, lo hara la funci�n ActualizarBotonRegistrar
    btnRegistrar.Enabled := False; //Revision 19


	btnAgregarItem.Enabled := (cbConveniosCliente.Items.Count > 0) and
    							(cbTiposComprobantes.ItemIndex >= 0);

    if btnAgregarItem.Enabled then
    	btnAgregarItem.Enabled := cbTiposComprobantes.Text <> TC_ARRIENDO;

    edNumeroComprobante.Enabled := True;

    FTipoComprobante := cbTiposComprobantes.Value;
    if FImprimirBoletaEnImpresoraFiscal and (FTipoComprobante = TC_BOLETA ) then begin
        edNumeroComprobante.Clear;
        lbl_NroComprobante.Enabled  := False;
        edNumeroComprobante.Enabled := False;
        lbl_FechaEmision.Enabled    := False;
        edFechaEmision.Enabled      := False;
        edFechaEmision.Date         := NowBase(DMConnections.BaseCAC);
    end
    else begin
        lbl_NroComprobante.Enabled  := True;
        edNumeroComprobante.Enabled := True;
        lbl_FechaEmision.Enabled    := True;
        edFechaEmision.Enabled      := True;
    end;

//    chkBoletaManual.Visible := (FTipoComprobante = TC_BOLETA) ; //ESTO ESTA COMENTADO PARA TEST SACAR PARA VOLVER A TENER BOLETAS MANUALES
    if (FTipoComprobante = TC_BOLETA) and not FImprimirBoletaEnImpresoraFiscal then begin// pantalla desde facturacion, obligatoriamente boletas manuales
        chkBoletaManual.Checked := True;
        chkBoletaManual.Enabled := False;
    end;

	// No se carga m�s la fecha de vencimiento.
    // Se utiliza la fecha de vencimiento de la NK en la que se incluyen los comprobantes
    //lblFechaVencimiento.Visible := (FTipoComprobante <> TC_NOTA_CREDITO);
    //edFechaVencimiento.Visible := lblFechaVencimiento.Visible;

    if cbTiposComprobantes.Text = TC_ARRIENDO then begin
//Revision 10
        if not SetearDatosArriendo then begin
            btnRegistrar.Enabled := False;  //Revision 19
            Exit;
        end;
        edCantidadCuotas.Enabled := true;
//Revision 10
    end else if cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS then
             begin
                    if not SetearDatosArriendoCuotas then begin
                            btnRegistrar.Enabled := False;  //Revision 19
                            Exit;
                    end;
             end else begin
                            AgregarSoporteInterfaz (False);
                            CbSoporte.Checked := False;
                            lblNumeroTelevia.Visible := False ;
                            edNumeroTelevia.Visible  := False ;
                            lblCuotas.Visible        := False ;
                            edCantidadCuotas.Visible := False ;
                            CargarComprobantesPendientes;
                      end;
    //Revision 11
    ActualizarBotonRegistrar;
end;

{******************************** Function Header ******************************
Revision 1
Author: nefernandez
Date: 08/04/2008
Description: Se redondean los valores de importes, pues en ConceptosMovimiento
el Precio puede contener centavos
*******************************************************************************}
Function TfrmIngresarComprobantes.SetearDatosArriendo;
begin
    //Revision 10
    Result := False;
    //Revision 10
    cdsObtenerPendienteFacturacion.DisableControls;
    cdsObtenerPendienteFacturacion.EmptyDataSet;
    lblNumeroTelevia.Visible := True ;
    edNumeroTelevia.Visible  := True ;
    lblCuotas.Visible        := True ;
    edCantidadCuotas.Visible := True ;
    if not (CbSoporte.Visible) then edNumeroTelevia.Clear;
    if not (CbSoporte.Visible) then edCantidadCuotas.Clear;

    spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoArriendo;
    spObtenerDetalleConceptoMovimiento.Open ;
//Revision 10
    if Not ObtenerCotizacionMoneda(NowBase(DMConnections.BaseCAC),
            spObtenerDetalleConceptoMovimiento.FieldByName('Moneda').AsString ) then Exit;
//Revision 10
    cdsObtenerPendienteFacturacion.Append;
    cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
    cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
    cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := edFechaEmision.Date;
    cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := spObtenerDetalleConceptoMovimiento.FieldByName('CodigoConcepto').AsInteger;
    cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := spObtenerDetalleConceptoMovimiento.FieldByName('Descripcion').AsString;
    cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := '';


    if (spObtenerDetalleConceptoMovimiento.FieldByName('AfectoIVA').AsBoolean = False) then
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion  ) )
    else begin
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion * ( 1 + ObtenerIVA / 100) ) );
        cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat :=   Round ( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion );
        cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := ObtenerIVA ;
        cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round ( cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat - cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat  );
    end;
    cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat, True, True, True);
    cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
    cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
    cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
    cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False ; //True;
    spObtenerDetalleConceptoMovimiento.Close ;

//Revision 14 
    If FIncluirSoporte then begin
        spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoSoporteTelevia;
        spObtenerDetalleConceptoMovimiento.Open ;
        if Not ObtenerCotizacionMoneda(NowBase(DMConnections.BaseCAC),
                spObtenerDetalleConceptoMovimiento.FieldByName('Moneda').AsString ) then Exit;

        cdsObtenerPendienteFacturacion.Append;
        cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
        cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
        cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := edFechaEmision.Date;
        cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := spObtenerDetalleConceptoMovimiento.FieldByName('CodigoConcepto').AsInteger;
        cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := spObtenerDetalleConceptoMovimiento.FieldByName('Descripcion').AsString;
        cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := '';


        if (spObtenerDetalleConceptoMovimiento.FieldByName('AfectoIVA').AsBoolean = False) then
            cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion  ) )
        else begin
            cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion * ( 1 + ObtenerIVA / 100) ) );
            cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat :=   Round ( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion );
            cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := ObtenerIVA ;
            cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round ( cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat - cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat );
        end;
        cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat, True, True, True);
        cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
        cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
        cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
        cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False ;
        spObtenerDetalleConceptoMovimiento.Close ;
    //Fin de la Revision 14
    end;
    cdsObtenerPendienteFacturacion.Post;
    cdsObtenerPendienteFacturacion.EnableControls;
    btnEliminarItem.Enabled := False ;
    btnAgregarItem.Enabled  := False ;
    cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion);
    //Revision 18
    ActualizarBotonRegistrar;
    //Revision 10
    Result := True;
    //Revision 10
end;



procedure TfrmIngresarComprobantes.cdsObtenerPendienteFacturacionCalcFields(DataSet: TDataSet);
begin
    cdsObtenerPendienteFacturacion.FieldByName('FechaDescrip').AsString := DateToStr(cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime)
end;

procedure TfrmIngresarComprobantes.btnBuscarRUTClick(Sender: TObject);
resourcestring
    MSG_RUT_INVALIDO = 'No se han encontrado datos para ese RUT';
    MSG_RUT_SIN_CONVENIOS_CN = 'El RUT ingresado no tiene convenios de CN';
    MSG_DEBE_INGRESAR_RUT = 'Debe ingresar un n�mero de RUT';
var
    CodigoConvenio : Variant;
    Index: integer;
begin
	Screen.Cursor := crHourglass;
    try
        ClearCaptions;
        cbConveniosCliente.Items.Clear;
        for Index := cbTiposComprobantes.Items.Count - 1 downto 0 do begin
            if cbTiposComprobantes.Items[Index].Caption = TC_ARRIENDO then
                cbTiposComprobantes.Items.Delete(Index);
        end;
        cbTiposComprobantesChange(Self);
        if edNumeroTelevia.Visible  then edNumeroTelevia.Text:= '';
        if edCantidadCuotas.Visible then edCantidadCuotas.Clear;
        cbTiposComprobantes.ItemIndex := 0;

        if not ValidateControls([peRUTCliente], [(Trim(peRUTCliente.Text) <> EmptyStr)], caption, [MSG_DEBE_INGRESAR_RUT]) then exit;

        // Obtenemos el cliente seleccionado
        with obtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value := NULL;
            Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').Value := iif(peRUTCLiente.Text <> EmptyStr,
                                                                PadL(Trim(peRUTCLiente.Text), 9, '0'), EmptyStr);
            Open;
        end;
        // Obtenemos la lista de convenios para el cliente seleccionado
        CodigoConvenio := Null;

        if ObtenerCliente.Eof then begin
            MsgBoxErr(MSG_RUT_INVALIDO,Caption, Caption, MB_ICONSTOP);
            Exit;
        end;



        With spObtenerConveniosCliente do begin
            Parameters.ParamByName('@CodigoCliente').Value := obtenerCliente.fieldbyname('CodigoCliente').AsInteger;
			Parameters.ParamByName('@IncluirConveniosDeBaja').Value := True;
            Open;
            First;
            while not spObtenerConveniosCliente.Eof do begin
                //if spObtenerConveniosCliente.fieldbyname('CodigoConcesionaria').AsInteger = CODIGO_CN then						//SS_1147_MCA_20140408
                if spObtenerConveniosCliente.fieldbyname('CodigoConcesionaria').AsInteger = FCodigoConcesionariaNativa then	//SS_1147_MCA_20140408
                    cbConveniosCliente.Items.Add(spObtenerConveniosCliente.fieldbyname('NumeroConvenioFormateado').AsString,
                      trim(spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString));
                spObtenerConveniosCliente.Next;
            end;
            Close;
        end;

        if (cbConveniosCliente.Items.Count = 0) then
            MsgBoxBalloon(MSG_RUT_SIN_CONVENIOS_CN,'Error',MB_ICONSTOP, cbConveniosCliente);

        // Si no se recuperan convenios es porque ocurri� un inconveniente al
        // retornarlos, ya que un cliente siempre tiene convenios.
        if (CodigoConvenio <> null ) then cbConveniosCliente.Value := CodigoConvenio
        else cbConveniosCliente.ItemIndex := 0;

        cbConveniosClienteChange(cbConveniosCliente);

        with ObtenerCliente do begin
            FCodigoCliente := FieldByName('CodigoCliente').AsInteger;
            lblRUTRUT.Caption := FieldByName('NumeroDocumento').AsString;
            // Mostramos el nombre completo del cliente
            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString), Trim(FieldByName('Nombre').asString))
            else
                lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString));

            // Mostramos el domicilio de Facturaci�n
            {
            DescripcionCalle := ObtenerDescripCalle(DMConnections.BaseCAC,
              PAIS_CHILE,
                  trim(ObtenerCliente.FieldByName('CodigoRegion').asString),
                  trim(ObtenerCliente.FieldByName('CodigoComuna').asString),
                  ObtenerCliente.FieldByName('CodigoCalle').asInteger,
                  trim(ObtenerCliente.FieldByName('CalleDesnormalizada').asString));
            lDomicilio.Caption := ArmarDomicilioSimple(DMConnections.BaseCAC, DescripcionCalle,
              Trim(ObtenerCliente.FieldByName('Numero').asString), 0, '',
              Trim(ObtenerCliente.FieldByName('detalle').asString));
            lComuna.Caption := BuscarDescripcionComuna(DMConnections.BaseCAC,
              PAIS_CHILE,
              trim(ObtenerCliente.FieldByName('Codigoregion').asString),
              trim(ObtenerCliente.FieldByName('CodigoComuna').asString));
            lRegion.Caption := BuscarDescripcionRegion(DMConnections.BaseCAC,
              PAIS_CHILE,
              trim(ObtenerCliente.FieldByName('CodigoRegion').asString));     }
            btnAgregarItem.Enabled := ( cbConveniosCliente.Items.Count > 0 ) and
                                        (cbTiposComprobantes.ItemIndex >= 0 )
                                        and (not FRegistrandoBoletaFiscal);
            if btnAgregarItem.Enabled then
            	btnAgregarItem.Enabled := cbTiposComprobantes.Text <> TC_ARRIENDO;
        end;
    finally
		Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarComprobantes.ImprimirBoleta
  Author:    ggomez
  Date:      04-Jul-2005
  Arguments: None
  Result:    Boolean
  Description: Realiza la impresi�n de una boleta en la impresora fiscal.
    Retorna True, si la impresi�n fue exitosa.
    Almacena en disco un archivo temporal con los items de la boleta que se
    imprime. Este archivo es utilizado en el caso de que ocurra un inconveniente
    y no se finalice exitosamente la impresi�n.

  Revision 1:
  Author:    aganc
  Date:      07/06/2006
  Arguments: None
  Result:    Boolean
  Description: se agrego lineas de configuraci�n a la impresora para que en
    la cabecera del ticket se impriman los datos del cliente. Al finalizar la
    impresi�n de la boleta se configura la cabecera de la impresora con en
    blanco para mantener su estado original.
Revision :
    Author : vpaszkowicz
    Date : 23/02/2007
    Description : Controlo que haya guardado el archivo temporal volvi�ndolo
    a leer.

Revision 3
    Author: nefernandez
    Date: 17/06/2008
    Description : Se agregan logs durante el proceso de impresion y se guarda
    en un archivo al producirse una excepci�n.
-----------------------------------------------------------------------------}
function TfrmIngresarComprobantes.ImprimirBoleta: Boolean;
resourcestring
    MSG_CONFIGURACION_EXITOSA = 'La impresora se ha configurado con exito.';
    MSG_ABRIENDO_COMPROBANTE    = 'Abriendo boleta...';
    MSG_IMPRIMIENDO_ITEMS       = 'Imprimiendo items de la boleta...';
    MSG_AGREGANDO_PAGOS         = 'Agregando pagos...';
    MSG_CERRANDO_COMPROBANTE    = 'Cerrando boleta...';
    MSG_ERROR_CREANDO_ARCHIVO_TEMPORAL  = 'Ocurri� un error cuando se intent� generar el archivo temporal para la Boleta.';
    MSG_ERROR_VERIFICANDO_ARCHIVO = 'Ocurri� un error cuando se leia el archivo temporal';
    MSG_ERROR_ARCHIVO_CORRUPTO = 'El archivo temporal esta corrupto';
    MSG_ACLARACION = 'Se debe registrar la impresi�n para que pueda generar el comprobante fiscal.';
var
    LogImprimirBoleta: String;
    LogNombreArchivo: String;
    dd, mm, aaaa, hh24, mi, ss, ms: word;
    ExitoLog: Boolean;
    RutaLog: AnsiString;
begin
    Result := False;
    LogImprimirBoleta := '';
    DecodeDateTime(Now, aaaa, mm, dd, hh24, mi, ss, ms);
    GetDirBoletaTemporal(RutaLog, ExitoLog);
    if ExitoLog then
        LogNombreArchivo := GoodDir(RutaLog) + 'ImprBoleta_' + IntToStr(aaaa) + IntToStr(mm) + IntToStr(dd) + IntToStr(hh24) + IntToStr(mi) + IntToStr(ss) + IntToStr(ms) + '.log'
    else
        LogNombreArchivo := 'C:\ImprBoleta_' + IntToStr(aaaa) + IntToStr(mm) + IntToStr(dd) + IntToStr(hh24) + IntToStr(mi) + IntToStr(ss) + IntToStr(ms) + '.log';

    if not cdsObtenerPendienteFacturacion.IsEmpty then begin
        // Guardar el archivo con los datos de la boleta que se va a generar.
        try
            LogImprimirBoleta := LogImprimirBoleta + 'IB: GuardarBoletaTemporal' + CRLF;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: Convenio al que se le generar� la boleta = ' + cbConveniosCliente.Text + CRLF;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: Tipo de comprobante que se le generar� la boleta = ' + cbTiposComprobantes.Text + CRLF;
            GuardarBoletaTemporal(NOMBRE_BOLETA_TEMPORAL
                // N� de Serie de la Impresora
                + '_S' + Trim(FNumeroSerie)
                // N� de la Boleta Fiscal. Colocar cero pues a�n no se tiene el n� que la impresora le dar� a la boleta.
                + '_B' + IntToStr(0)
                // N� de Comprobante (primary Key de la Boleta)
                + '_C' + IntToStr(FNumeroComprobanteBoleta)
                // Fase en la que se encuentra la boleta
                + '_F' + FASE_PREPARACION
                + EXTENSION_BOLETA_TEMPORAL);
            if not VerificarArchivoTemporal then begin
                LogImprimirBoleta := LogImprimirBoleta + 'IB: VerificarArchivoTemporal' + CRLF;
                LogImprimirBoleta := LogImprimirBoleta + 'IB: ' + MSG_ERROR_VERIFICANDO_ARCHIVO + CRLF;
                raise Exception.Create(MSG_ERROR_VERIFICANDO_ARCHIVO + CRLF + MSG_ERROR_ARCHIVO_CORRUPTO);
            end;
        except
            on E: Exception do begin
                Result := False;
                LogImprimirBoleta := LogImprimirBoleta + 'IB: ' + MSG_ERROR_CREANDO_ARCHIVO_TEMPORAL + ':' + E.Message + CRLF;
                StringToFile(LogImprimirBoleta, LogNombreArchivo);
                MsgBoxErr(MSG_ERROR_CREANDO_ARCHIVO_TEMPORAL, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end; // except

        //  revision 1:
        //  configurar el encabezado con los datos del cliente
        try
            LogImprimirBoleta := LogImprimirBoleta + 'IB: ImpresoraFiscal.CanSetup' + CRLF;
            if ImpresoraFiscal.CanSetup then begin
                //Calculado antes en mas facil de pisar en modo debugger...
                LogImprimirBoleta := LogImprimirBoleta + 'IB: True ImpresoraFiscal.CanSetup' + CRLF;
                if not (ProgramarEncabezadoImpresora) then begin
                        LogImprimirBoleta := LogImprimirBoleta + 'IB: not ProgramarEncabezadoImpresora 1' + CRLF;
                        if not (ProgramarEncabezadoImpresora) then begin
                                LogImprimirBoleta := LogImprimirBoleta + 'IB: not ProgramarEncabezadoImpresora 2' + CRLF;
                                MsgBoxErr(MSG_CONFIGURACION_NO_POSIBLE,
                                    MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
                                Result := False;
                                Exit;
                        end; // if
                end; // if
            end else begin // if ImpresoraFiscal.CanSetup
                LogImprimirBoleta := LogImprimirBoleta + 'IB: False ImpresoraFiscal.CanSetup' + CRLF;
                //si la impresora no admite la configuracion
                //salgo de la funcion
                MsgBoxErr(MSG_CONFIGURACION_NO_POSIBLE, MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
                Result := False;
                Exit;
            end; // else if ImpresoraFiscal.CanSetup
        except
            //si salio una ecepcion
            //sino salgo de la funcion
            LogImprimirBoleta := LogImprimirBoleta + 'IB: ' + MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE + CRLF;
            StringToFile(LogImprimirBoleta, LogNombreArchivo);
            MsgBoxErr(MSG_CONFIGURACION_NO_POSIBLE, MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
            Result := False;
            LimpiarEncabezadoImpresora;
            Exit;
        end;
        //fin revision 1:

        try
            lbl_EstadoEmisionBoleta.Caption := MSG_ABRIENDO_COMPROBANTE;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: AbrirBoleta' + CRLF;
            AbrirBoleta;
            FNroBoletaFiscal := ImpresoraFiscal.ObtenerTicket;
            edNumeroComprobante.ValueInt := FNroBoletaFiscal;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_INICIAL' + CRLF;
            ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_INICIAL);

            // Imprimir Items.
            lbl_EstadoEmisionBoleta.Caption := MSG_IMPRIMIENDO_ITEMS;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: ImprimirItemsBoleta' + CRLF;
            ImprimirItemsBoleta;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_VENTA' + CRLF;
            ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_VENTA);

            // Enviar el comando para el pago.
            lbl_EstadoEmisionBoleta.Caption := MSG_AGREGANDO_PAGOS;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: AgregarPagoBoleta(' + FloatToStr(FTotalComprobante) + ', tpEfectivo)' + CRLF;
            AgregarPagoBoleta(FTotalComprobante, tpEfectivo);
            LogImprimirBoleta := LogImprimirBoleta + 'IB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_PAGOS' + CRLF;
            ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_PAGOS);

            // Cerrar la boleta.
            lbl_EstadoEmisionBoleta.Caption := MSG_CERRANDO_COMPROBANTE;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: CerrarBoleta' + CRLF;
            CerrarBoleta;
            LogImprimirBoleta := LogImprimirBoleta + 'IB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_FINAL' + CRLF;
            ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_FINAL);

            LogImprimirBoleta := LogImprimirBoleta + 'IB: AsentarComprobante' + CRLF;
            // Asentar la Boleta en la BD.
            AsentarComprobante;

            // Si se pudo Imprimir y Guardar
            Result := True;

            // revision 1:
            //limpiar configuracion del encabezado
            //fin revision 1:
            lbl_EstadoEmisionBoleta.Caption := EmptyStr;
            LimpiarEncabezadoImpresora;
        except
            on E: Exception do begin
                Result := False;
                LogImprimirBoleta := LogImprimirBoleta + 'IB: ' + MSG_ERROR_GENERAL + ': ' + E.Message + CRLF;
                StringToFile(LogImprimirBoleta, LogNombreArchivo);
                LimpiarEncabezadoImpresora;
                MsgBoxErr(MSG_ERROR_GENERAL, E.Message + CRLF + MSG_ACLARACION, Caption, MB_ICONERROR);
            end;
        end; // except
    end; // if not cdsObtenerPendienteFacturacion.IsEmpty
end; // function TfrmIngresarComprobantes.ImprimirBoleta

{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarComprobantes.ImpresoraFiscalLista
  Author:    ggomez
  Date:      04-Jul-2005
  Arguments: None
  Result:    Boolean
  Description: Retorna True si la impresora fiscal est� conectada, encendida y
    con papel.
-----------------------------------------------------------------------------}
//function TfrmIngresarComprobantes.ImpresoraFiscalLista: Boolean;
//begin
//    Result := False;
//    if not ImpresoraFiscal.Active then ImpresoraFiscal.Active := True;
//    if (ImpresoraFiscal.ObtenerEstado.Performance = pNormal) then begin
//        Result := True;
//    end;
//    if not Result then begin
//        MsgBox(ImpresoraFiscal.GetLastError, Caption, MB_ICONSTOP);
//    end;
//end;

{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarComprobantes.AbrirBoleta
  Author:    ggomez
  Date:      05-Jul-2005
  Arguments: None
  Result:    None
  Description: Realiza la apertura de una boleta en la impresora fiscal.
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.AbrirBoleta;
resourcestring
    MSG_ERROR_APERTURA_COMPROBANTE = 'Error al abrir el comprobante fiscal.';
begin
    if not ImpresoraFiscal.AbrirTicket then begin
        raise ExceptionFiscal.Create(MSG_ERROR_APERTURA_COMPROBANTE + CRLF + ImpresoraFiscal.GetLastError);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarComprobantes.ImprimirItemsBoleta
  Author:    ggomez
  Date:      05-Jul-2005
  Arguments: None
  Result:    None
  Description: Envia a la impresora fiscal los items de la boleta.

  Revision:   1
  Author: mlopez
  Date: 16/08/2006
  Description: Modifique el modo en el que se detecta si un item ya fue
               enviado a la impresora
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.ImprimirItemsBoleta;
resourcestring
    MSG_ERROR_ENVIAR_ITEM = 'Error al enviar un item a la impresora fiscal.';
    MSG_ERROR_VERIFICANDO_ARCHIVO = 'Error de verificaci�n de archivo temporal';
    MSG_ARCHIVO_CORRUPTO = 'El archivo temporal est� inconsistente con los datos de la base.';
var
    InfoTicket: TInfoTicket;
    Descrip: AnsiString;
begin
    try
        InfoTicket := ImpresoraFiscal.ObtenerInformacionTicket;
    finally
        Screen.Cursor := crDefault;
    end;

    // Verificar que haya un Ticket Abierto.
    if InfoTicket.NumeroTicket = -1 then begin
        raise ExceptionFiscal.Create(MSG_ERROR_ENVIAR_ITEM);
    end else begin
        // Hay un Ticket Abierto.
        cdsObtenerPendienteFacturacion.DisableControls;
        // Se filtra los items Marcados para que coincidan con el numero de items del Archivo (Para usar RecNo)
        cdsObtenerPendienteFacturacion.Filter := 'Marcado = 1';
        cdsObtenerPendienteFacturacion.Filtered := True;
        try
            // Se busca recorren los items y se imprimen solo los que no han sido impresos.
            // Luego de imprimir se actualiza el campo del archivo "EstaImpreso"
            // IMPORTANTE:
            // Esto tiene el siguiente problema: si se env�a a imprimir el mismo
            // �tem dos veces, la impresora retorna que la cantidad de �tems es una
            // por cada par de �tems iguales. Luego, si esto ocurre, habr� un error
            // al buscar el pr�ximo �tem a imprimir.
            cdsObtenerPendienteFacturacion.First;
            while not cdsObtenerPendienteFacturacion.Eof do begin
                if not cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').Value then begin
                    Descrip := Trim(cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString);
                    if (Trim(cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString) <> EmptyStr) then begin
                        Descrip := Descrip + Space(1) + '(' + Trim(cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString) + ')'
                    end;

                    if not ImpresoraFiscal.ImprimirLineaDetalle(Descrip,
                            cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat,
                            (cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat),
                            cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat / 100  ) then begin
                        // En caso de que no se logre imprimir la linea, lanza una exception
                        raise ExceptionFiscal.Create(MSG_ERROR_ENVIAR_ITEM + CRLF + ImpresoraFiscal.GetLastError);
                    end else begin
                        // Actualizar ClientDataSet para indicar item impreso.
                        cdsObtenerPendienteFacturacion.Edit;
                        cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := True;
                        cdsObtenerPendienteFacturacion.Post;
                        // Se actualiza el item en el archivo
                        ActualizarArchivoItemImpreso(cdsObtenerPendienteFacturacion.RecNo, True);
                        if not VerificarMarcaItemImpreso(cdsObtenerPendienteFacturacion.RecNo) then
                            raise ExceptionFiscal.Create(MSG_ERROR_VERIFICANDO_ARCHIVO + CRLF + MSG_ARCHIVO_CORRUPTO);
                        end;
                end;
                cdsObtenerPendienteFacturacion.Next;
            end;
        finally
            cdsObtenerPendienteFacturacion.Filtered := False;
            cdsObtenerPendienteFacturacion.Filter := '';
            cdsObtenerPendienteFacturacion.EnableControls;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarComprobantes.AgregarPagoBoleta
  Author:    ggomez
  Date:      05-Jul-2005
  Arguments: Monto: Float; Tipo: TTipoPago
  Result:    None
  Description: Env�a un pago a la impresora fiscal.
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.AgregarPagoBoleta(Monto: Double; Tipo: TTipoPago);
resourcestring
    MSG_TEXTO_PAGO          = 'Pago: ';
    MSG_ERROR_ENVIAR_PAGO   = 'Error al enviar un pago a la impresora fiscal.';
begin
    if not ImpresoraFiscal.AgregarPago(Monto, Tipo, MSG_TEXTO_PAGO) then begin
        raise ExceptionFiscal.Create(MSG_ERROR_ENVIAR_PAGO + CRLF + ImpresoraFiscal.GetLastError);
    end;
end;


procedure TfrmIngresarComprobantes.AgregarSoporteInterfaz(Mostrar: Boolean);
begin
    CbSoporte.Visible := Mostrar;
    if Mostrar then begin
        gbDatosComprobante.Height:= 107;
    end else begin
        gbDatosComprobante.Height:= 90;
    end;
    gbDatosCliente.Top := gbDatosComprobante.Height + gbDatosComprobante.Top + 4;
    gbItemsPendientes.Top := gbDatosCliente.Height + gbDatosCliente.Top + 4;
    Label2.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 2;
    Label2.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 2;
    lbl_EstadoEmisionBoleta.Top := Label2.Height + Label2.Top + 2;
    NB_Botones.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 2;
    BtnSalir.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 3;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarComprobantes.CerrarBoleta
  Author:    ggomez
  Date:      05-Jul-2005
  Arguments: None
  Result:    None
  Description: Realiza el cierre de la boleta en la impresora fiscal.
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.CerrarBoleta;
resourcestring
    MSG_ERROR_CERRAR_COMPROBANTE = 'Error al cerrar el comprobante fiscal.'+ CRLF +
    'Se debe registrar la impresi�n para que pueda generar el comprobante fiscal';
begin
    if not ImpresoraFiscal.CerrarTicket then begin
        raise ExceptionFiscal.Create(MSG_ERROR_CERRAR_COMPROBANTE + CRLF + ImpresoraFiscal.GetLastError);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmIngresarComprobantes.AsentarComprobante
  Author:    ggomez
  Date:      06-Jul-2005
  Arguments: None
  Result:    None
  Description: Realiza el asiento del comprobante en la BD.

  Revision 1:
    Author: ggomez
    Date: 12/08/2006
    Description: Cambi� para que ahora sea un procedure.
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.AsentarComprobante;
resourcestring
    MSG_INVOICE_GENERATED_OK    = 'La %s se gener� exitosamente.';
    MSG_INVOICE_NOT_GENERATED   = 'Ha ocurrido un error al generar la %s ';
var
	sDescCompr : AnsiString;
begin
    sDescCompr := DescriTipoComprobante(FTipoComprobante) + Space(1) + edNumeroComprobante.Text;
    try
        GuardarComprobante;
        Screen.Cursor := crDefault;
        MsgBox(Format(MSG_INVOICE_GENERATED_OK, [sDescCompr]), Caption, MB_ICONINFORMATION);
    except
        on E: Exception do begin
            // Crear una excepci�n para que la resuelva quien llama a este m�todo.
            raise Exception.Create(Format(MSG_INVOICE_NOT_GENERATED, [sDescCompr]) + CRLF + E.Message);
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CancelarBoleta
Author : ggomez
Date Created : 07/09/2005
Description : Realiza el cierre forzazo de una boleta en la impresora fiscal.
    Retorna True, si la cancelaci�n fue exitosa.
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TfrmIngresarComprobantes.CancelarBoleta: Boolean;
resourcestring
    MSG_ERROR_CANCELAR_COMPROBANTE = 'Error al cancelar el comprobante fiscal.';
begin
    Result := ImpresoraFiscal.CancelarTicket;
    if not Result then begin
        MsgBoxErr(MSG_ERROR_CANCELAR_COMPROBANTE,
            ImpresoraFiscal.GetLastError, Caption, MB_ICONERROR);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: CambiarExtensionArchivoBoletaFiscal
  Author:    vpaszkowicz
  Date:      19-Feb-2007
  Arguments: None
  Result:    None
  Description: Renombra el archivo como extensi�n OLD para que el proceso no lo
  encuentre nuevamente, ya que esaba vacio.
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.CambiarExtensionArchivoBoletaFiscal;
var
    NombreArchivo: String;
begin
    if not FileExists(FArchivoBoleta) then begin
        raise Exception.Create(Format(MSG_NO_EXISTE_ARCHIVO_BOLETA, [FArchivoBoleta]));
    end;
    NombreArchivo := ExtractFileName(FArchivoBoleta);
    NombreArchivo := Copy(NombreArchivo, 1, Length(NombreArchivo) - 3) + EXTENSION_BOLETA_INVALIDA;

    if not RenameFile(FArchivoBoleta, GoodDir(ExtractFilePath(FArchivoBoleta)) + NombreArchivo) then begin
        raise Exception.Create(Format(MSG_ERROR_RENOMBRAR_ARCHIVO_BOLETA, [FArchivoBoleta]));
    end else begin
        //El archivo boleta deber� ser regenerado porque este es inv�lido
        FArchivoBoleta := '';
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TamanoArchivoBoletaFiscal
  Author:    vpaszkowicz
  Date:      19-Feb-2007
  Arguments: None
  Result:    integer
  Description: Retorna si el archivo boleta es consistente, para esto tengo
  una marca de fin de archivo, de manera tal que si cuando lo abro no est�
  se que la grabaci�n est� incompleta y no
  deberia continuerse con el proceso de impresi�n.
-----------------------------------------------------------------------------}
function TfrmIngresarComprobantes.ArchivoBoletaFiscalCorrupto: Boolean;
resourceString
    MSG_ERROR_CALCULANDO_TAMANO = 'Error al intentar verificar que el archivo sea consistente.';
var
    F: File of TItem;
    unItem: TItem;
begin
    Result := True;
    try
        try
            AssignFile(F, FArchivoBoleta);
            Reset(F);
            //Si esta en EOF ya va a salir con True
            while not Eof(F) do
            begin
                Read(F, unItem);
                if unItem.Descripcion = CONST_MARCA then
                    Result := False;
            end;
        except
            on E: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_CALCULANDO_TAMANO, [FArchivoBoleta]), e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        CloseFile(F);
    end;
end;


{******************************** Function Header ******************************
Function Name: ImprimirContinuacionBoletaFiscal
Author : ggomez
Date Created : 13/09/2005
Description : Continua la impresi�n de una boleta en la impresora fiscal.
Parameters : None
Return Value : Boolean
Revision :
    Author : vpaszkowicz
    Date : 23/02/2007
    Description : Agregu� el chequeo de que el archivo temporal que levanto sea
    consistente verificando sobre una marca.

Revision 3
    Author: nefernandez
    Date: 17/06/2008
    Description : Se agregan logs durante el proceso de impresion y se guarda
    en un archivo al producirse una excepci�n.
*******************************************************************************}
function TfrmIngresarComprobantes.ImprimirContinuacionBoletaFiscal: Boolean;
resourcestring
    MSG_BOLETA_CERRADA = 'No se ha detectado una boleta abierta.';
    MSG_ERROR_GENERAR_COMPROBANTE = 'Ocurri� un error cuando se intent� generar la Boleta.';
    MSG_ERROR_GUARDAR_COMPROBANTE = 'Ocurri� un error cuando se intent� guardar la Boleta.';
    MSG_ARCHIVO_CORRUPTO = 'El archivo de resguardo de la boleta est� vac�o o corrupto. Se cambiar� la extensi�n del mismo. Desea continuar el proceso?';
    MSG_ERROR_IMPROBABLE = 'Error de sincronizaci�n entre el proceso y la impresora.' + CRLF + 'Vuelva a iniciar el proceso y verifique que la impresora est� encendida y en l�nea';
var
    InfoTicket: TInfoTicket;
    Respuesta: Word;
    Mensaje: AnsiString;

    function ExisteTicketAbierto: Boolean;
    begin
        Screen.Cursor := crHourGlass;
        try
            InfoTicket := ImpresoraFiscal.ObtenerInformacionTicket;
            FCodigoImpresora := ObtenerCodigoImpresora(FNumeroSerie);
            Result := InfoTicket.NumeroTicket <> -1
        finally
            Screen.Cursor := crDefault;
        end;
    end;
var
    LogImprimirBoleta: String;
    LogNombreArchivo: String;
    dd, mm, aaaa, hh24, mi, ss, ms: word;
    ExitoLog: Boolean;
    RutaLog: AnsiString;
begin
//    Obtener en que parte del comprobante est� la impresora.
//    Seg�n la fase, continuar con la impresi�n.
//    Casos:
//        - NO se han enviado items
//        - Se han enviado items, pero no se ha hecho ning�n pago.
//        - Se ha hecho al menos un pago.
//        - Se ha enviado el comando de cerrar el comprobante.
        Result := False;
        LogImprimirBoleta := '';
        DecodeDateTime(Now, aaaa, mm, dd, hh24, mi, ss, ms);
        GetDirBoletaTemporal(RutaLog, ExitoLog);
        if ExitoLog then
            LogNombreArchivo := GoodDir(RutaLog) + 'ImprBoleta_' + IntToStr(aaaa) + IntToStr(mm) + IntToStr(dd) + IntToStr(hh24) + IntToStr(mi) + IntToStr(ss) + IntToStr(ms) + '.log'
        else
            LogNombreArchivo := 'C:\ImprBoleta_' + IntToStr(aaaa) + IntToStr(mm) + IntToStr(dd) + IntToStr(hh24) + IntToStr(mi) + IntToStr(ss) + IntToStr(ms) + '.log';

        if ExisteTicketAbierto then begin
        LogImprimirBoleta := LogImprimirBoleta + 'RB: True ExisteTicketAbierto' + CRLF;
        LogImprimirBoleta := LogImprimirBoleta + 'RB: Convenio al que se le generar� la boleta = ' + intToStr(FCodigoConvenio) + CRLF;
        case InfoTicket.Fase  of
            ftInicial, ftVenta:
                begin
                    try
                        //si retoma una impresi�n y la impresora estaba apagada,
                        //deber� obtener nuevamente el n�mero de ticket.
                        //Y si estoy en la continuaci�n quiere decir que no cargu� el comprobante en el edit
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: InfoTicket.Fase : ftInicial, ftVenta' + CRLF;
                        if FNroBoletaFiscal = - 1  then
                        begin
                            FNroBoletaFiscal := ImpresoraFiscal.ObtenerTicket;
                            edNumeroComprobante.ValueInt := FNroBoletaFiscal;
                        end;
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ImprimirItemsBoleta' + CRLF;
                        // Imprimir Items y actualizar el nombre del archivo
                        ImprimirItemsBoleta;
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_VENTA' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_VENTA);
                        // Enviar el comando para el pago y actualizar el nombre del archivo
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: AgregarPagoBoleta(' + FloatToStr(FTotalComprobante) + ', tpEfectivo)' + CRLF;
                        AgregarPagoBoleta(FTotalComprobante, tpEfectivo);
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_PAGOS' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_PAGOS);
                        // Cerrar la boleta y actualizar el nombre del archivo
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: CerrarBoleta' + CRLF;
                        CerrarBoleta;
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_FINAL' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_FINAL);
                        // Asentar la Boleta en la BD
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: AsentarComprobante' + CRLF;
                        AsentarComprobante;
                    except
                        on E: Exception do begin
                            LogImprimirBoleta := LogImprimirBoleta + 'RB: ' + MSG_ERROR_GENERAR_COMPROBANTE + ': ' + E.Message + CRLF;
                            StringToFile(LogImprimirBoleta, LogNombreArchivo);
                            MsgBoxErr(MSG_ERROR_GENERAR_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                            Result := False;
                            Exit;
                        end;
                    end;
                end;

            ftPagos:
                begin
                    try
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: InfoTicket.Fase : ftPagos' + CRLF;
                        // Cerrar la boleta y actualizar el nombre del archivo
                        CerrarBoleta;
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_FINAL' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_FINAL);
                        //Si llego hasta ac� va a usar el valor del edit para sacar el Nro de comprobante
                        //as� que me aseguro de que est�
                        if edNumeroComprobante.ValueInt = 0 then
                            edNumeroComprobante.ValueInt := FNroBoletaFiscal;
                        // Asentar la Boleta en la BD
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: AsentarComprobante' + CRLF;
                        AsentarComprobante;
                    except
                        on E: Exception do begin
                            LogImprimirBoleta := LogImprimirBoleta + 'RB: ' + MSG_ERROR_GENERAR_COMPROBANTE + ': ' + E.Message + CRLF;
                            StringToFile(LogImprimirBoleta, LogNombreArchivo);
                            MsgBoxErr(MSG_ERROR_GENERAR_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                            Result := False;
                            Exit;
                        end;
                    end;
                end;
            ftFinal:
                begin
                    try
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: InfoTicket.Fase : ftFinal' + CRLF;
                        // Asentar la Boleta en la BD.
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: AsentarComprobante' + CRLF;
                        AsentarComprobante;
                        // Informar que apagando y prendiendo la impresora se terminar� la emisi�n del comprobante.
                        MsgBox(MSG_FASE_FINAL, Caption, MB_ICONERROR);
                    except
                        on E: Exception do begin
                            LogImprimirBoleta := LogImprimirBoleta + 'RB: ' + MSG_ERROR_GENERAR_COMPROBANTE + ': ' + E.Message + CRLF;
                            StringToFile(LogImprimirBoleta, LogNombreArchivo);
                            MsgBoxErr(MSG_ERROR_GENERAR_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                            Result := False;
                            Exit;
                        end;
                    end;
                end;
            ftDesconocida:
                begin
                    LogImprimirBoleta := LogImprimirBoleta + 'RB: InfoTicket.Fase : ftDesconocida' + CRLF;
                    MsgBox(MSG_FASE_DESCONOCIDA, Caption, MB_ICONERROR);
                    Result := False;
                    Exit;
                end;
        end; // case
        Result := True;

    end else begin    // Cuando no existe ticket Abierto en la impresora fiscal
        LogImprimirBoleta := LogImprimirBoleta + 'RB: True ExisteTicketAbierto' + CRLF;
        if not ExisteComprobanteImpreso then begin
            LogImprimirBoleta := LogImprimirBoleta + 'RB: Not ExisteComprobanteImpreso' + CRLF;
            if (ArchivoBoletaFiscalCorrupto) then begin
                LogImprimirBoleta := LogImprimirBoleta + 'RB: True ArchivoBoletaFiscalCorrupto' + CRLF;
                try
                    Mensaje := MSG_ARCHIVO_CORRUPTO;
                    Respuesta := MsgBox(Mensaje, Caption, MB_YESNO + MB_ICONQUESTION);
                    if Respuesta = mrNo then Respuesta := mrCancel
                    else begin
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: CambiarExtensionArchivoBoletaFiscal' + CRLF;
                        CambiarExtensionArchivoBoletaFiscal;
                        lbl_EstadoEmisionBoleta.Caption := EmptyStr;
                        FRegistrandoBoletaFiscal := False;
                        EnableControlsInContainer(gbDatosConvenio, not FRegistrandoBoletaFiscal);
                        EnableControlsInContainer(gbDatosComprobante, not FRegistrandoBoletaFiscal);
                        EnableControlsInContainer(gbItemsPendientes, not FRegistrandoBoletaFiscal);
                        btnAgregarItem.Enabled := False;
                        btnModificarItem.Enabled := False;
                        btnEliminarItem.Enabled := False;
                        nb_Botones.PageIndex := BOTON_REGISTRAR;
                        btnRegistrar.Enabled := False;
                        ResetearControles;
                    end;
                except
                    on E: Exception do begin
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ' + MSG_ERROR_GENERAR_COMPROBANTE + ': ' + E.Message + CRLF;
                        StringToFile(LogImprimirBoleta, LogNombreArchivo);
                        MsgBoxErr(MSG_ERROR_GENERAR_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                        Result := False;
                        Exit;
                    end;
                end;
            end
            else begin
                //-------------------------------------------------------------------------------------------------
                // IMPORTANTE: Si pasa por aqui quiere decir que la boleta no alcanzo ni a abrirse en la
                //             y por lo tanto hay que enviar toda la generacion de la boleta a la Impresora Fiscal
                //-------------------------------------------------------------------------------------------------
                //  configurar el encabezado con los datos del cliente
                LogImprimirBoleta := LogImprimirBoleta + 'RB: False ArchivoBoletaFiscalCorrupto' + CRLF;
                try
                    if ImpresoraFiscal.CanSetup then begin
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: True ImpresoraFiscal.CanSetup' + CRLF;
                        if not (ProgramarEncabezadoImpresora) then begin
                                LogImprimirBoleta := LogImprimirBoleta + 'RB: False ProgramarEncabezadoImpresora 1' + CRLF;
                                if not (ProgramarEncabezadoImpresora) then begin
                                        LogImprimirBoleta := LogImprimirBoleta + 'RB: False ProgramarEncabezadoImpresora 2' + CRLF;
                                        MsgBoxErr(MSG_CONFIGURACION_NO_POSIBLE,
                                            MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
                                        Result := False;
                                        Exit;
                                end; // if
                        end; // if
                    end else begin // if ImpresoraFiscal.CanSetup
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: False ImpresoraFiscal.CanSetup' + CRLF;
                        //si la impresora no admite la configuracion
                        //salgo de la funcion
                        MsgBoxErr(MSG_CONFIGURACION_NO_POSIBLE, MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
                        Result := False;
                        Exit;
                    end; // else if ImpresoraFiscal.CanSetup
                except
                    LogImprimirBoleta := LogImprimirBoleta + 'RB: ' + MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE + CRLF;
                    StringToFile(LogImprimirBoleta, LogNombreArchivo);
                    //si salio una ecepcion
                    //sino salgo de la funcion
                    MsgBoxErr(MSG_CONFIGURACION_NO_POSIBLE, MSG_DRIVER_IMPRESORA + MSG_CONFIGURACION_NO_POSIBLE, Caption, MB_ICONINFORMATION);
                    Result := False;
                    LimpiarEncabezadoImpresora;
                    Exit;
                end;
                //Existe una remotisima posibilidad que entre por aca. Si tira
                //este mensaje es porque logro algo muy improbable
                if ExisteTicketAbierto then
                begin                                               
                    MsgBoxErr(MSG_ERROR_GENERAR_COMPROBANTE, MSG_ERROR_IMPROBABLE, Caption, MB_ICONERROR);
                    Exit;
                end
                else begin
                    // Se genera la boleta
                    LogImprimirBoleta := LogImprimirBoleta + 'RB: False ExisteTicketAbierto' + CRLF;
                    try
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: AbrirBoleta' + CRLF;
                        AbrirBoleta;
                        FNroBoletaFiscal := ImpresoraFiscal.ObtenerTicket;
                        edNumeroComprobante.ValueInt := FNroBoletaFiscal;
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_INICIAL' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_INICIAL);
                        // Imprimir Items.
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ImprimirItemsBoleta' + CRLF;
                        ImprimirItemsBoleta;
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_VENTA' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_VENTA);
                        // Enviar el comando para el pago.
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: AgregarPagoBoleta(' + FloatToStr(FTotalComprobante) + ', tpEfectivo)' + CRLF;
                        AgregarPagoBoleta(FTotalComprobante, tpEfectivo);
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_PAGOS' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_PAGOS);
                        // Cerrar la boleta.
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: CerrarBoleta' + CRLF;
                        CerrarBoleta;
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: ActualizarNombreArchivoBoletaFiscal(' + IntToStr(FNroBoletaFiscal) + ', FASE_FINAL' + CRLF;
                        ActualizarNombreArchivoBoletaFiscal(FNroBoletaFiscal, FASE_FINAL);
                        // Asentar la Boleta en la BD.
                        LogImprimirBoleta := LogImprimirBoleta + 'RB: AsentarComprobante' + CRLF;
                        AsentarComprobante;
                        // Si se pudo Imprimir y Guardar
                        Result := True;
                    except
                        on E: Exception do begin
                            LogImprimirBoleta := LogImprimirBoleta + 'RB: ' + MSG_ERROR_GENERAL + ': ' + E.Message + CRLF;
                            StringToFile(LogImprimirBoleta, LogNombreArchivo);
                            MsgBoxErr(MSG_ERROR_GENERAL, E.Message, Caption, MB_ICONERROR);
                            Result := False;
                            Exit;
                        end;
                    end; // except
                end
            end;//else: el archivo no esta corrupto.
        end
        else begin //else de que no existe comprobante impreso.
            LogImprimirBoleta := LogImprimirBoleta + 'RB: True ExisteComprobanteImpreso' + CRLF;
            // Si pasa por aqui quiere decir que la boleta se imprimio correctamente pero falto guardarla en la DB
            try
                //Importante: se puede dar una secuencia de apagados y prendidos
                //de la impresora que hacen que entre ac� y tenga un ticket abierto.
                //Esto ocurrir�a si est� la impresora apagada despu�s de pasar el cansetup.
                //si se da en alg�n momento simplemente en esta parte hay que preguntar
                //si la impresora est� apagada y tirar un mensaje de error.
                LogImprimirBoleta := LogImprimirBoleta + 'RB: AsentarComprobante' + CRLF;
                AsentarComprobante;
                Result := True;
            except
                on E: Exception do begin
                    LogImprimirBoleta := LogImprimirBoleta + 'RB: ' + MSG_ERROR_GUARDAR_COMPROBANTE + ': ' + E.Message + CRLF;
                    StringToFile(LogImprimirBoleta, LogNombreArchivo);
                    MsgBoxErr(MSG_ERROR_GUARDAR_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                    Result := False;
                    Exit;
                end;
            end;
        end;
    end;
end; // TfrmIngresarComprobantes.ImprimirContinuacionBoletaFiscal

{******************************** Function Header ******************************
Function Name: ContinuarBoletaFiscalAbierta
Author : ggomez
Date Created : 13/09/2005
Description : Realiza el proceso que trata el caso de boleta abierta en la
    impresora fiscal.
    Si no hay comprobante abierto no hace nada.
Parameters : None
Return Value : Boolean

Revision 1:
Author: mlopez
Date: 17/08/2006
Description: Modifique el procedimiento para que continue en el caso de que la
             impresion haya salido correctamente pero no se haya guardado en la
             base de datos, se intente nuevamente hacer el asiento de los datos.
Revision :
    Author : vpaszkowicz
    Date : 28/02/2007
    Description : Me aseguro antes de entrar a continuar con la impresi�n, que
    la impresora est� en estado normal, ya que necesito que est� prendida al menos
    para poder saber si se hab�a dejado un ticket por la mitad, de lo contrario
    el programa creer� que no hay ticket ni documento y podr�a entrar en una fase
    equivocada.
Revision : 3
    Author : vpaszkowicz
    Date : 16/08/2007
    Description : Agrego que si est� en estado inicial y presion� que no desea
    continuar con la impresi�n, que salga de la ventana.
Revision : 4
    Author : vpaszkowicz
    Date : 19/09/2007
    Description : Si est� en la fase inicial, ventas y pagos lo obligo a conti-
    nuar la impresi�n, dado que puede haber empezado el ticket.
*******************************************************************************}
function TfrmIngresarComprobantes.ContinuarBoletaFiscalAbierta: Boolean;
resourcestring
    MSG_IMPRESORA_NO_RESPONDE = 'La impresora no responde. Verifique que est� encendida.';
    MSG_ERROR_CONTINUAR_EMISION_BOLETA = 'Ha ocurrido un error al intentar continuar con la emisi�n de la boleta.';
    MSG_DEBE_CERRAR_COMPROBANTE = 'Debe cerrar el comprobante en la impresora fiscal.' + CRLF +
                                  'Si lo cierra no se registrar� en la base de datos.' + CRLF +
                                  '�Lo desea cerrar en este momento?';
    MSG_ERROR_RECUPERAR_ITEMS_PERO_FASE_INICIAL = 'No se han podido recuperar los items de la boleta N�: %s.' + CRLF +
                                                  'Pero puede confeccionar nuevamente los items y registrar la boleta.';
    MSG_ERROR_IMPRESORA = 'Error tratando de acceder a la impresora' + CRLF +
    'Se debe registrar la impresi�n para que pueda generar el comprobante fiscal';
    MSG_VERIFIQUE_IMPRESORA = 'Verifique que la impresora este encendida y en linea';

var
    Mensaje: AnsiString;
    InfoTicket: TInfoTicket;
    InfoPagos: TInfoPagosTicket;
    Respuesta: Word;
    Fase: Integer;
    unEstado: TEstadoImpresora;
begin
//    Obtener en que parte del comprobante est� la impresora.
//    Seg�n la fase, mostrar diferentes mensajes al operador.
//    Casos:
//        - NO se han enviado items
//        - Se han enviado items, pero no se ha hecho ning�n pago.
//        - Se ha hecho al menos un pago.
//        - Se ha enviado el comando de cerrar el comprobante.

    Result      := True;
    Respuesta   := mrNo;

    // Obtener el N� de Boleta desde la impresora Fiscal.
    if not ImpresoraFiscal.Active then ImpresoraFiscal.Active := True;
    unEstado := ImpresoraFiscal.ObtenerEstado;
    if unEstado.Performance <> pNormal then begin
        MsgBoxErr(MSG_ERROR_IMPRESORA, MSG_VERIFIQUE_IMPRESORA, Caption, MB_ICONERROR);
        Result := False;
        exit;
    end;

    Screen.Cursor := crHourGlass;
    try
        InfoTicket := ImpresoraFiscal.ObtenerInformacionTicket;
    finally
        Screen.Cursor := crDefault;
    end;

    if (InfoTicket.NumeroTicket <> -1) then begin
        // Hay Ticket Abierto.
        Mensaje := Format(MSG_BOLETA_ABIERTA, [IntToStr(InfoTicket.NumeroTicket)]);
        case InfoTicket.Fase of

            ftInicial:
                begin
                    // Preguntar al operador si desea continuar con el comprobante o no.
                    Mensaje := Mensaje + CRLF + Format(MSG_CONTINUAR_CON_BOLETA, [InttoStr(InfoTicket.NumeroTicket)]);
                    //Respuesta := MsgBox(Mensaje, Caption, MB_YESNO + MB_ICONQUESTION);
                    Respuesta := MsgBox(Mensaje, Caption, MB_OK + MB_ICONQUESTION);
                    //Agregado en Rev. 3
                    //if Respuesta = mrNo then Respuesta := mrCancel;
                    if Respuesta <> mrOk then Respuesta := mrCancel;
                end;

            ftVenta, ftDescuentosRecargos, ftPagos:
                begin
                    // Obligar al operador a continuar con el comprobante. Si
                    // no desea continuar, debe cancelar.
                    Mensaje := Mensaje + CRLF + Format(MSG_CONTINUAR_CON_BOLETA, [InttoStr(InfoTicket.NumeroTicket)]);
                    //Respuesta := MsgBox(Mensaje, Caption, MB_YESNO + MB_ICONQUESTION);
                    Respuesta := MsgBox(Mensaje, Caption, MB_OK + MB_ICONQUESTION);
                    //if Respuesta = mrNo then Respuesta := mrCancel;
                    if Respuesta <> mrOk then Respuesta := mrCancel;
                end;

            ftFinal:
                begin
                    // Informar que apagando y prendiendo la impresora se
                    // terminar� la emisi�n del comprobante.
                    Mensaje := Mensaje + CRLF + MSG_FASE_FINAL;
                    MsgBox(Mensaje, Caption, MB_ICONINFORMATION);
                    Respuesta := mrCancel;
                end;

            ftDesconocida:
                begin
                    // Informar que no se tiene informaci�n de la fase que
                    // contacte al Administrador del Sistema.
                    Mensaje := Mensaje + CRLF + MSG_FASE_DESCONOCIDA;
                    MsgBox(Mensaje, Caption, MB_ICONWARNING);
                    Respuesta := mrCancel;
                end;
        end; // case
    end else begin
        if not ExisteComprobanteImpreso then begin
            // Si llega a este punto quiere decir que la Boleta ni se pudo abrir en
            // la Impresora Fiscal
            Mensaje := MSG_CONTINUAR_CON_BOLETA_SIN_ABRIR;
            //Respuesta := MsgBox(Mensaje, Caption, MB_YESNO + MB_ICONQUESTION);
            Respuesta := MsgBox(Mensaje, Caption, MB_OK + MB_ICONQUESTION);
            //if Respuesta = mrNo then Respuesta := mrCancel;
            if Respuesta <> mrOk then Respuesta := mrCancel;
        end else begin
            // Si llega a este punto quiere decir que la Boleta fue impresa pero falto asentarla, por lo que
            // es necesario obligar al operador a que termine la operacion
            Mensaje := MSG_CONTINUAR_CON_BOLETA_SIN_NUMERO;
            Respuesta := MsgBox(Mensaje, Caption, MB_YESNO + MB_ICONQUESTION);
            if Respuesta = mrNo then Respuesta := mrCancel;
        end;
    end;

    case Respuesta of
        mrYes, mrOk:
            begin
                // Deshabilitar los controles, s�lo dejar habilitados el bot�n Continuar y Salir.
                FTipoComprobante := TC_BOLETA;
                try
                    // Se obtiene el codigo de la impresora, el Numero de Comprobante Fiscal y la Fase en la que se encontraba la impresion
                    if not ObtenerPartesNombreArchivo(FArchivoBoleta, FNumeroSerie, FNroBoletaFiscal, FNumeroComprobanteBoleta, Fase) then begin
                        MsgBoxErr(MSG_ERROR_OBTENER_NRO_SERIE_NOMBRE_ARCHIVO, Format(MSG_ERROR_OBTENER_PARTES_NOMBRE_ARCHIVO, [FArchivoBoleta]), Caption, MB_ICONERROR);
                        //Result := False;
                        Exit;
                    end;
                    // Cargamos el Numero Boleta Fiscal en el edit xq luego se usa para mostrar el mensaje de "Generacion OK"
                    edNumeroComprobante.ValueInt := FNroBoletaFiscal;
                    //Si est� en fase-1 FNroBoletaFiscal ser� cero y no lo actualizar� mas.
                    //lo cambio a -1 para que actualice los valores.
                    if FNroBoletaFiscal = 0 then
                        FNroBoletaFiscal := -1;
                    // Se cargan los items en el ClientDataSet
                    CargarItemsBoletaDesdeTemporal(FArchivoBoleta);
                    // Se Activan controles y selecciona el boton Continuar
                    EnableControlsInContainer(gbDatosConvenio, False);
                    EnableControlsInContainer(gbDatosComprobante, False);
                    //Le dejo cambiar por si quiere elegir no imprimir uno.
                    dbItemsPendientes.PopupMenu := PopUpGrilla;
                    EnableControlsInContainer(gbItemsPendientes, True);
                    btnAgregarItem.Enabled := False;
                    dbItemsPendientes.OnDblClick := nil;
                    nb_Botones.PageIndex := BOTON_CONTINUAR;
                    btnModificarItem.Enabled := False;
                    btnAgregarItem.Enabled := False;
                    btnEliminarItem.Enabled := False;
                except
                    on E: Exception do begin
                        if (InfoTicket.Fase = ftInicial) then begin
                            // No pudo levantar los datos de la boleta pero como
                            // est� en la fase inicial del comprobante, dejar que
                            // ingrese nuevos items para la boleta.
                            nb_Botones.PageIndex := BOTON_REGISTRAR;
                            MsgBox(Format(MSG_ERROR_RECUPERAR_ITEMS_PERO_FASE_INICIAL, [Trim(IntToStr(FNroBoletaFiscal))]),
                                Caption, MB_ICONINFORMATION);
                        end else begin
                            Result := False;
                            Respuesta := MsgBox(E.Message
                                        + CRLF + MSG_DEBE_CERRAR_COMPROBANTE,
                                        Caption, MB_YESNO + MB_ICONSTOP);
                            if Respuesta = mrYes then begin
                                InfoPagos := ImpresoraFiscal.ObtenerInformacionPagosTicket;
                                AgregarPagoBoleta(InfoPagos.TotalAPagar, tpEfectivo);
                                CerrarBoleta;
                                Result := True;
                            end
                        end;

                    end;
                end;
            end;

        mrNo:
            begin
                // Habilitar todos los controles como para ingresar un nuevo
                // comprobante, es decir, lo dejo como est� en tiempo de dise�o.
                nb_Botones.PageIndex := BOTON_REGISTRAR;
            end;

        mrCancel:
            begin
                // Salir del form.
                Result := False;
            end;
    end; // case
end; // TfrmIngresarComprobantes.ContinuarBoletaFiscalAbierta

{******************************** Function Header ******************************
Function Name: CargarItemsBoletaDesdeTemporal
Author : ggomez
Date Created : 13/09/2005
Description: Carga en la grilla de items del comprobante, los items
    almacenados en el archivo temporal de boleta.
Parameters : Archivo: AnsiString
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.CargarItemsBoletaDesdeTemporal(
  Archivo: AnsiString);
resourcestring
    MSG_ERROR_RECUPERAR_ITEMS   = 'No se han podido recuperar los datos de la boleta.';
var
    F: File of TItem;
    Item: TItem;
    index : integer;
    IndexDeArriendoEnCombo, IndexDeArriendoCuotasEnCombo: integer;
begin
//-------------------------------------------------------------------------------------------------
// IMPORTANTE: Este procedure recibe el nombre del archivo completo con Path
//-------------------------------------------------------------------------------------------------
    if FileExists(Archivo) then begin
        FTotalComprobante := 0;
        cdsObtenerPendienteFacturacion.Open;
        cdsObtenerPendienteFacturacion.EmptyDataSet;

        AssignFile(F, Archivo);
        Reset(F);
        while not Eof(F) do begin
            Read(F, Item);
            //Salteo la constante marca que agregue
            if Item.Descripcion <> CONST_MARCA then
            begin
                //Cargo tambi�n los datos del cliente.
                if FCodigoCliente = -1 then begin
                    FCodigoCliente := Item.CodigoCliente;
                    CompletarDatosConClienteDelArchivo;
                end;
                if FCodigoConvenio = -1 then begin
                    FCodigoConvenio := Item.CodigoConvenio;
                    MostrarDomicilioFacturacion;
                end;

                cdsObtenerPendienteFacturacion.DisableControls;
                try
                    cdsObtenerPendienteFacturacion.Append;
                    cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString         := Item.Descripcion;
                    cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat          := Item.Cantidad;
                    cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat       := Item.PrecioUnitario / 100 ;
                    cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat     := Item.PorcentajeIVA  ;
                    cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat        := Item.ImporteIVA / 100 ;
                    cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean         := Item.Marcado;
                    cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger  := Item.CodigoConvenio;
                    cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger  := Item.CodigoConcepto;
                    cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString    := Item.Observaciones;
                    cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean           := Item.Nuevo;
                    cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime      := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
                    cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString      := FormatearImporte(DMConnections.BaseCAC, Item.PrecioUnitario div 100 );
                    cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean     := Item.EstaImpreso;
                    //Agrego el movimiento cuenta por si ya exist�a en la base.
                    cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').AsInteger:= Item.NumeroMovimiento;
                    cdsObtenerPendienteFacturacion.Post;

                    if (Item.CodigoConcepto = FCodigoConceptoArriendo) or (Item.CodigoConcepto = FCodigoConceptoArriendoCuotasMail)
                            or (Item.CodigoConcepto = FCodigoConceptoArriendoCuotasPostal) then begin
                        edCantidadCuotas.ValueInt := Item.CantidadCuotas ;
                        edNumeroTelevia.Text      := Item.Etiqueta ;
                        if (Item.CodigoConcepto = FCodigoConceptoArriendo) then begin
                            IndexDeArriendoEnCombo    := -1 ;
                            for index := 0 to cbTiposComprobantes.Items.Count - 1 do
                                if cbTiposComprobantes.Items[index].Caption = TC_ARRIENDO then begin
                                    cbTiposComprobantes.ItemIndex := index ;
                                    IndexDeArriendoEnCombo := index ;
                                end;

                            if IndexDeArriendoEnCombo = -1 then cbTiposComprobantes.Items.Add(TC_ARRIENDO, TC_BOLETA);

                            for index := 0 to cbTiposComprobantes.Items.Count - 1 do
                                if cbTiposComprobantes.Items[index].Caption = TC_ARRIENDO then cbTiposComprobantes.ItemIndex := index ;

                        end
                        else if (Item.CodigoConcepto = FCodigoConceptoArriendoCuotasMail)
                            or (Item.CodigoConcepto = FCodigoConceptoArriendoCuotasPostal) then begin
                            IndexDeArriendoCuotasEnCombo    := -1 ;
                            for index := 0 to cbTiposComprobantes.Items.Count - 1 do
                                if cbTiposComprobantes.Items[index].Caption = TC_ARRIENDO_CUOTAS then begin
                                    cbTiposComprobantes.ItemIndex := index ;
                                    IndexDeArriendoEnCombo := index ;
                                end;

                            if IndexDeArriendoCuotasEnCombo = -1 then cbTiposComprobantes.Items.Add(TC_ARRIENDO_CUOTAS, TC_BOLETA);

                            for index := 0 to cbTiposComprobantes.Items.Count - 1 do
                                if cbTiposComprobantes.Items[index].Caption = TC_ARRIENDO_CUOTAS then cbTiposComprobantes.ItemIndex := index;

                        end
                             
                    end;

                    FTotalComprobante := FTotalComprobante + Item.PrecioUnitario / 100;
                finally
                    cdsObtenerPendienteFacturacion.EnableControls;
                end; // finally
            end;
        end; // while
        CloseFile(F);
    end else begin
        raise Exception.Create(MSG_ERROR_RECUPERAR_ITEMS);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: VerificarArchivoTemporal
  Author:    vpaszkowicz
  Date:      23-Feb-2007
  Arguments: None
  Result:    Boolean
  Description: Verifica que el archivo que acaba de grabar sea igual al dataset.
-----------------------------------------------------------------------------}
function TfrmIngresarComprobantes.VerificarArchivoTemporal: Boolean;
resourcestring
    MSG_ERROR_LEYENDO_ARCHIVO = 'Error leyendo el archivo temporal %s';

    function CompararRegistros(unItem: TItem): Boolean;
    begin
        Result := (cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString = unItem.Descripcion) and
                (cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat = unItem.Cantidad) and
                (Trunc(cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat * 100) = unItem.PrecioUnitario) and
                (Trunc(cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat * 100) = unItem.ImporteIVA) and
                (cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat = unItem.PorcentajeIVA) and
                (cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean = unItem.Marcado) and
                (cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger = unItem.CodigoConvenio) and
                (cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger = unItem.CodigoConcepto) and
                (cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString = unItem.Observaciones) and
                (cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean = unItem.Nuevo) and
                (cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean = unItem.EstaImpreso);
                if (cbTiposComprobantes.Text = TC_ARRIENDO)or (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then
                    Result := Result and (unItem.CantidadCuotas = edCantidadCuotas.ValueInt) and (unItem.Etiqueta = edNumeroTelevia.Text)
                else Result := Result and (unItem.CantidadCuotas = 0) and (unItem.Etiqueta= '');

    end;

var
    F: File of TItem;
    Item: TItem;
    Distintos: Boolean;
    NumeroRegistro: integer;
begin
    Distintos := False;
    Result := False;
    if FileExists(FArchivoBoleta) then begin
        cdsObtenerPendienteFacturacion.Filter := 'Marcado = 1';
        cdsObtenerPendienteFacturacion.Filtered := True;
        try
            try
                AssignFile(F, FArchivoBoleta);
                Reset(F);
                //Verifico que todos los items de la tabla esten en el archivo.
                //Me va a sobrar la marca de fin
                cdsObtenerPendienteFacturacion.First;
                while (not cdsObtenerPendienteFacturacion.Eof) and not Distintos do begin
                    if cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean then begin
                        NumeroRegistro := cdsObtenerPendienteFacturacion.RecNo;
                        Seek(F, NumeroRegistro -1);
                        if not Eof(F) then begin
                            Read(F, Item);
                            if not CompararRegistros(Item) then
                                Distintos := True
                            else Seek(F, NumeroRegistro -1); // retrocede porque el puntero quedo en el comienzo del proximo registro
                        end;
                    end;
                    cdsObtenerPendienteFacturacion.Next;
                end;
                Result := not Distintos;
            except
                on E: Exception do begin
                    MsgBoxErr(Format(MSG_ERROR_LEYENDO_ARCHIVO, [FArchivoBoleta]), e.Message, Caption, MB_ICONERROR);
                end;
            end;
        finally
            CloseFile(F);
            cdsObtenerPendienteFacturacion.Filtered := False;
            cdsObtenerPendienteFacturacion.Filter := '';
        end;
    end
    else Result := False;
end;

{-----------------------------------------------------------------------------
  Procedure: InicializarItemMarca
  Author:    vpaszkowicz
  Date:      21-Feb-2007
  Arguments: var Item: TItem
  Result:    None
  Description: Creo un registro ficticio con la descripcion con una CONST_MARCA
  para saber despues que el archivo este acorde con la grilla.

  Revision: 1
  Author:    vpaszkowicz
  Date:      09-May-2008
  Description: Agrego el default para el movimientoCuenta
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.InicializarItemMarca(var Item: TItem);
begin
    Item.Descripcion := CONST_MARCA;
    Item.Cantidad := 0;
    Item.PrecioUnitario := 0;
    Item.PorcentajeIVA := 0;
    Item.ImporteIVA := 0;
    Item.Marcado := False;
    Item.CodigoConvenio := 0;
    Item.CodigoConcepto := 0;
    Item.Observaciones := '';
    Item.Nuevo := False;
    Item.CodigoCliente := 0;
    //Por las dudas, para que no moleste..
    Item.EstaImpreso := True;
    Item.CantidadCuotas := 0;
    Item.Etiqueta := '';
    Item.NumeroMovimiento := 0;
end;

{******************************** Function Header ******************************
Function Name: GuardarBoletaTemporal
Author : ggomez
Date Created : 13/09/2005
Description : Guarda en un archivo temporal los items de la boleta actual.
Parameters : Archivo: AnsiString
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 19/02/2007
    Description : Agrego el comando flush antes de cerrar el archivo para forzar
    la escritura en disco de los items antes de devolver el control al Delphi y
    que se vaya a imprimir.

    Revision :2
    Author : vpaszkowicz
    Date : 09/05/2008
    Description : Obtengo el movimiento cuenta del archivo, esto es por si ya
    existe. Est� asociado a la propiedad EsNuevo.
*******************************************************************************}
procedure TfrmIngresarComprobantes.GuardarBoletaTemporal(Archivo: AnsiString);
resourcestring
    MSG_ERROR_GUARDANDO_ARCHIVO = 'Error guardando el archivo temporal %s';
    MSG_ERROR_DIRECTORIO_TEMPORAL = 'Error al crear el directorio temporal';
    MSG_ERROR_NO_PUEDE_CREARSE = 'No se pudo crear el directorio temporal';
var
    F: File of TItem;
    Item: TItem;
    Exito: Boolean;
    Ruta: AnsiString;
begin
    GetDirBoletaTemporal(Ruta, Exito);
    if Exito then begin
        Archivo := GoodDir(Ruta) + Archivo;
        // Si el archivo no existe, crearlo.
        if not FileExists(Archivo) then begin
            try
                try
                    AssignFile(F, Archivo);
                    Rewrite(F);
                    cdsObtenerPendienteFacturacion.First;
                    while not cdsObtenerPendienteFacturacion.Eof do begin
                        if cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean then begin
                            Item.Descripcion    := cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString;
                            Item.Cantidad       := cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat;
                            Item.PrecioUnitario := Trunc(cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat * 100);
                            Item.ImporteIVA     := Trunc(cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat * 100);
                            Item.PorcentajeIVA  := cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat  ;
                            Item.Marcado        := cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean;
                            Item.CodigoConvenio := cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger;
                            Item.CodigoConcepto := cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger;
                            Item.Observaciones  := cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString;
                            Item.Nuevo          := cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean;
                            //Agrego el movimiento cuenta para guardar los datos
                            Item.NumeroMovimiento :=  cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').AsInteger;
                            Item.CodigoCliente  := FCodigoCliente;
                            Item.EstaImpreso    := False;
                            if (cbTiposComprobantes.Text = TC_ARRIENDO) or (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then begin
                                Item.CantidadCuotas := edCantidadCuotas.ValueInt;
                                Item.Etiqueta       := edNumeroTelevia.Text;
                            end else begin
                                Item.CantidadCuotas := 0;
                                Item.Etiqueta       := '';
                            end;
                            Write(F, Item);
                        end;
                        cdsObtenerPendienteFacturacion.Next;
                    end;
                    //Como termino de aplicar todos los datos, agrego mi marca de fin de datos
                    InicializarItemMarca(Item);
                    Write(F, Item);
                    // Almacenar el nombre del archivo de la boleta.
                    FArchivoBoleta := Archivo;
                except
                    on E: Exception do begin
                        MsgBoxErr(Format(MSG_ERROR_GUARDANDO_ARCHIVO, [FArchivoBoleta]), e.Message, Caption, MB_ICONERROR);
                    end;
                end;
            finally
                CloseFile(F);
            end;
        end;
    end
    else raise Exception.Create(MSG_ERROR_DIRECTORIO_TEMPORAL + CRLF + MSG_ERROR_NO_PUEDE_CREARSE);
end;

{******************************** Function Header ******************************
Function Name: GetDirBoletaTemporal
Author : ggomez
Date Created : 13/09/2005
Description : Retorna el directorio para almacenar las boletas temporales.
Parameters : None
Return Value : AnsiString
*******************************************************************************}
procedure TfrmIngresarComprobantes.GetDirBoletaTemporal(var DirectorioTemporal: AnsiString; var Exito: Boolean);
resourceString
    MSG_ERROR_AL_OBTENER_DIRECTORIO_TEMPORAL = 'Error al obtener el directorio temporal.';
var
    Buf: Array[0..1024] of char;
begin
    Exito := False;
    try
        if GetTempPath(SizeOf(Buf), Buf) > 0 then begin
            DirectorioTemporal := GoodDir(StrPas(Buf));
        end else begin
            DirectorioTemporal := 'C:\Temp';
        end;

        // Quitar la �ltima barra de separaci�n de directorios.
        if StrRight(DirectorioTemporal, 1) = '\' then DirectorioTemporal := StrLeft(DirectorioTemporal, Length(DirectorioTemporal) - 1);

        // Quitar el �ltimo directorio del path.
        DirectorioTemporal := ExtractFilePath(DirectorioTemporal);

        // Agregar como �ltimo directorio del path el directorio con el nombre del exe.
        DirectorioTemporal := DirectorioTemporal + GetAppName + '\';
        Exito := True;
        if not DirectoryExists(DirectorioTemporal) then
            //Me aseguro de que pudo crear el directorio
            if not CreateDir(DirectorioTemporal) then
                Exito := False;
    except
        on E: Exception do begin
           MsgBoxErr(MSG_ERROR_AL_OBTENER_DIRECTORIO_TEMPORAL, e.Message, Caption, MB_ICONERROR);
           Exito := False;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: EliminarBoletaTemporal
Author : ggomez
Date Created : 13/09/2005
Description : Elimina el archivo que contiene la boleta temporal, recientemente
    impresa.
Parameters : Archivo: AnsiString
Return Value : None
*******************************************************************************}
procedure TfrmIngresarComprobantes.EliminarBoletaTemporal(Archivo: AnsiString);
resourcestring
    MSG_ERROR_ELIMINAR_ARCHIVO_BOLETA = 'Ocurri� un error al eliminar el archivo de la boleta.';
begin
//-------------------------------------------------------------------------------------------------
// IMPORTANTE: Este procedure recibe el nombre del archivo completo con Path
//-------------------------------------------------------------------------------------------------
    // Si el archivo existe y no se pudo Eliminar tirar error.
    if Del(Archivo) = 0 then begin
        raise Exception.Create(MSG_ERROR_ELIMINAR_ARCHIVO_BOLETA);
    end;
    FNroBoletaFiscal := -1;
end;

{-----------------------------------------------------------------------------
  Procedure: btn_ContinuarClick
  Author:
  Date:
  Arguments: Sender: TObject
  Result:    None
  Revision :
      Author : vpaszkowicz
      Date : 28/02/2007
      Description : Me aseguro que la impresora este encendida antes de continuar
      con el proceso, de lo contrario cuando pregunte si hay un ticket empezado
      podr�a decirme que no cuando lo que estaba pasando era que la impresora
      estaba apagada.
-----------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.btn_ContinuarClick(Sender: TObject);
resourceString
    MSG_ERROR_IMPRESORA = 'Error tratando de acceder a la impresora'+ CRLF +
    'Se debe registrar la impresi�n para que pueda generar el comprobante fiscal';
    MSG_VERIFIQUE_IMPRESORA = 'Verifique que la impresora este encendida y en linea';
var
    unEstado: TEstadoImpresora;
begin
    // Continuar con la emisi�n del comprobante que se estaba imprimiendo.
    if FImprimirBoletaEnImpresoraFiscal then  begin
        if not ImpresoraFiscal.Active then ImpresoraFiscal.Active := True;
        //Me aseguro que la impresora est� en estado normal antes de empezar a imprimir
        unEstado := ImpresoraFiscal.ObtenerEstado;
        if unEstado.Performance <> pNormal then begin
            MsgBoxErr(MSG_ERROR_IMPRESORA, MSG_VERIFIQUE_IMPRESORA, Caption, MB_ICONERROR);
            exit;
        end
        else if not ImprimirContinuacionBoletaFiscal then Exit;
        // Habilitar los componentes.
        EnableControlsInContainer(gbDatosConvenio, True);
        EnableControlsInContainer(gbDatosComprobante, True);
        EnableControlsInContainer(gbItemsPendientes, True);
        btnAgregarItem.Enabled      := False;
        btnModificarItem.Enabled    := False;
        btnEliminarItem.Enabled     := False;
        nb_Botones.PageIndex        := BOTON_REGISTRAR;
        ResetearControles;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerCodigoImpresora
Author : mvitali
Date Created : 22/09/2005
Description : Dado un numero de serie de impresora la busca en la nomina de
    impresoras y devuelve el codigo de la misma, en caso de no encontrarla
    devuelve -1
Parameters : NumeroDeSerie: String
Return Value : integer

Revision:   1
Author: mlopez
Date: 16/08/2006
Description: Agregue el try except y un mensaje de error si falla
*******************************************************************************}
function TfrmIngresarComprobantes.ObtenerCodigoImpresora(
  NumeroDeSerie: String): Integer;
resourcestring
    MSG_ERROR_OBTENIENDO_CODIGO_IMPRESORA = 'Error al obtener el Codigo de la Impresora.';
var
    sp: TADOStoredProc;
    Codigo: Integer;
    Descripcion: AnsiString;
begin
    Codigo := -1;
    Descripcion := EmptyStr;
    sp := TADOStoredProc.Create(nil);
    try
        try
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'ObtenerCodigoImpresora';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@NumeroSerie').Value := NumeroDeSerie;
            sp.Parameters.ParamByName('@Codigo').Value := Codigo;
            sp.Parameters.ParamByName('@Descripcion').Value := Descripcion;
            sp.ExecProc;
            if (sp.Parameters.ParamByName('@Codigo').Value <> Null) then begin
                Result := sp.Parameters.ParamByName('@Codigo').Value
            end else begin
                Result := -1;
            end;
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_AL_OBTENER_NRO_SERIE, e.Message, Caption, MB_ICONERROR);
                Result := -1;
            end;
        end;
    finally
        sp.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: GuardarComprobante
Author : mlopez
Date Created : 11/08/2006
Description : Genera Movimientos Cuenta, Lote Facturacion, Guarda el comprobante
              y por ultimo actualiza el Lote de Facturacion recien creado

Revision: 1
Author: nefernandez
Date: 01/10/2007
Description: SS 611: Se agrega la llamada al store ReaplicarConvenio luego de
generar todos los comprobantes

Revision :2
    Author : vpaszkowicz
    Date : 18/02/2009
    Description :Bajo el borrado del archivo antes del Commit para que si
    falla el reaplicar no se borre el archivo y me quede sin registro
*******************************************************************************}
procedure TfrmIngresarComprobantes.GuardarComprobante;
begin
    DMConnections.BaseCAC.BeginTrans;
    try
        AgregarNuevosMovimientosCuenta;
        AgregarLoteFacturacion(AgregarComprobante);
        ActualizarLotesFacturacion;
        AgregarCuotasComprobanteArriendo;
        //Revision 1
        spReaplicarConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        spReaplicarConvenio.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        spReaplicarConvenio.ExecProc;
        //Bajo esta parte para que no se borre el archivo antes de reaplicar.
        //Antes fallaba el reaplicar y me quedaba sin archivo y sin dato en la base
        if ((FTipoComprobante = TC_BOLETA) and (chkBoletaManual.Checked = False) ) then begin
            EliminarBoletaTemporal(FArchivoBoleta);
        end;
        DMConnections.BaseCAC.CommitTrans;
    except
        on E: Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            // Subir la excepci�n para que la resuelva quien llama a este m�todo.
            raise;
        end;
    end;
end;

{--------------------------------------------------------------------------------
Procedure Name  : ActualizarNombreArchivoBoletaFiscal
    Author      : ggomez
    Date Created: 12/08/2006
    Description : Actualiza el nombre del archivo de boleta fiscal para colocarle
        el N� de Boleta Fiscal.

Revision 1: Agregue la parte que cambia la parte del nombre del archivo efectivamente
            para Numero Boleta Fiscal o Numero Fase
------------------------------------------------------------------------------}
procedure TfrmIngresarComprobantes.ActualizarNombreArchivoBoletaFiscal(NumeroBoletaFiscal: Int64;
    Fase: AnsiString);
var
    AuxStr,
    NombreArchivo: String;
begin
    if not FileExists(FArchivoBoleta) then begin
        raise Exception.Create(Format(MSG_NO_EXISTE_ARCHIVO_BOLETA, [FArchivoBoleta]));
    end;

    if NumeroBoletaFiscal = -1 then
        raise Exception.Create(Format(MSG_NUMERO_FISCAL_ERRONEO, [FArchivoBoleta]));

    // Se actualiza el Numero Boleta Fiscal
    NombreArchivo := ExtractFileName(FArchivoBoleta);
    AuxStr := ParseParamByNumber(ParseParamByNumber(NombreArchivo, 2, '_B'), 1, '_');
    if AuxStr = '' then begin
        raise Exception.Create(Format(MSG_FORMATO_NOMBRE_ARCHIVO_BOLETA_INCORRECTO, [FArchivoBoleta]));
    end else begin
        NombreArchivo := StringReplace(NombreArchivo, '_B' + AuxStr, '_B' + IntToStr(NumeroBoletaFiscal), [rfIgnoreCase]);
    end;

    // Se actualiza la Fase
    AuxStr := ParseParamByNumber(ParseParamByNumber(NombreArchivo, 2, '_F'), 1, '.');
    if AuxStr = '' then begin
        raise Exception.Create(Format(MSG_FORMATO_NOMBRE_ARCHIVO_BOLETA_INCORRECTO, [FArchivoBoleta]));
    end else begin
        NombreArchivo := StringReplace(NombreArchivo, '_F' + AuxStr, '_F' + Fase, [rfIgnoreCase]);
    end;

    if not RenameFile(FArchivoBoleta, GoodDir(ExtractFilePath(FArchivoBoleta)) + NombreArchivo) then begin
        raise Exception.Create(Format(MSG_ERROR_RENOMBRAR_ARCHIVO_BOLETA, [FArchivoBoleta]));
    end else begin
        FArchivoBoleta := GoodDir(ExtractFilePath(FArchivoBoleta)) + NombreArchivo;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerCodigoImpresora
Author : mlopez
Date Created : 11/08/2006
Description : Guarda el comprobante
*******************************************************************************}
function TfrmIngresarComprobantes.ObtenerPartesNombreArchivo(NombreArchivo: TFileName;
    var NumeroSerieImpresora: String; var NumeroComprobanteFiscal: Int64;
    var NumeroComprobante: Int64; var FaseActual: Integer): Boolean;
var
    AuxStr: String;
begin
    NombreArchivo := ExtractFileName(NombreArchivo);

    AuxStr := ParseParamByNumber(ParseParamByNumber(NombreArchivo, 2, '_S'), 1, '_');
    NumeroSerieImpresora := AuxStr;

    AuxStr := ParseParamByNumber(ParseParamByNumber(NombreArchivo, 2, '_B'), 1, '_');
    NumeroComprobanteFiscal := IVal(AuxStr);

    AuxStr := ParseParamByNumber(ParseParamByNumber(NombreArchivo, 2, '_C'), 1, '_');
    NumeroComprobante := IVal(AuxStr);

//    FaseActual := -2; // -1 es la fase previa a la inicial (lo coloco asi para asegurarme que lee algo al realizar las validaciones)
//    AuxStr := ParseParamByNumber(ParseParamByNumber(NombreArchivo, 2, '_F'), 1, '.');
//    FaseActual := IVal(AuxStr);
    FaseActual := ObtenerArchivoFase(NombreArchivo);

    Result := (NumeroSerieImpresora <> '') and (NumeroComprobanteFiscal <> -1) and
              (NumeroComprobante <> 0) and (FaseActual <> -2);
end;

{******************************** Function Header ******************************
Function Name: ActualizarArchivoItemImpreso
Author : mlopez
Date Created : 11/08/2006
Description : Actualiza item por item a medida que se van imprimiendo
*******************************************************************************}
procedure TfrmIngresarComprobantes.ActualizarArchivoItemImpreso(NumeroRegistro: Integer;
    EstaImpreso: Boolean);
resourcestring
    MSG_ERROR_MODIFICANDO_ARCHIVO = 'Error al intentar modificar el archivo %s';
var
    F: File of TItem;
    Item: TItem;
begin
    try
        try
            AssignFile(F, FArchivoBoleta);
            Reset(F);
            if NumeroRegistro >= 1 then begin
                Seek(F, NumeroRegistro -1);
                if not Eof(F) then begin
                    Read(F, Item);
                    Item.EstaImpreso := EstaImpreso;
                    Seek(F, NumeroRegistro -1); // retrocede porque el puntero quedo en el comienzo del proximo registro
                    Write(F, Item); // escribe en el archivo
                end;
            end;
        except
            on E: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_MODIFICANDO_ARCHIVO, [FArchivoBoleta]), e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        //Flush(F);
        CloseFile(F);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: VerificarMarcaItemImpreso
  Author:    vpaszkowicz
  Date:      21-Feb-2007
  Arguments: NumeroRegistro: Integer
  Result:    Boolean
  Description: Verifico que el item que acabo de imprimir haya quedado marcado
  como impreso en el archivo.
-----------------------------------------------------------------------------}
function TfrmIngresarComprobantes.VerificarMarcaItemImpreso(NumeroRegistro: Integer): Boolean;
resourcestring
    MSG_ERROR_VERIFICANDO_ARCHIVO = 'Error al intentar verificar el archivo %s';
var
    F: File of TItem;
    Item: TItem;
begin
    Result := False;
    try
        try
            AssignFile(F, FArchivoBoleta);
            Reset(F);
            if NumeroRegistro >= 1 then begin
                Seek(F, NumeroRegistro -1);
                if not Eof(F) then begin
                    Read(F, Item);
                    if Item.EstaImpreso then
                        Result := True;
                    Seek(F, NumeroRegistro -1); // retrocede porque el puntero quedo en el comienzo del proximo registro
                end;
            end;
        except
            on E: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_VERIFICANDO_ARCHIVO, [FArchivoBoleta]), e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        CloseFile(F);
    end;
end;



{******************************** Function Header ******************************
Function Name: ExisteArchivoContinuarBoletaFiscalAbierta
Author : mlopez
Date Created : 11/08/2006
Description : Devuelve True si Existe un comprobante
*******************************************************************************}
function TfrmIngresarComprobantes.ExisteArchivoContinuarBoletaFiscalAbierta: Boolean;
var
    SearchRec: TSearchRec;
    Exito: Boolean;
    Ruta: AnsiString;
begin
    Result := False;
    GetDirBoletaTemporal(Ruta, Exito);
    if Exito then begin
        if FindFirst(Ruta + '*.bol', faAnyFile and not faDirectory, SearchRec) = 0 then begin
            FArchivoBoleta := Ruta + SearchRec.Name;
            Result := True;
        end;
    end;
    FindClose(SearchRec);
end;

{******************************** Function Header ******************************
Function Name: ResetearControles
Author : mlopez
Date Created : 14/08/2006
Description : Reinicia los componentes y variables en la ventana
*******************************************************************************}
procedure TfrmIngresarComprobantes.ResetearControles;
begin
    // Se resetean variables
    FCodigoCliente := -1;
    FCodigoConvenio := -1;
    FNroBoletaFiscal := -1;
    FNumeroComprobanteBoleta := -1;
    FRegistrandoBoletaFiscal := False;
    ClearCaptions;
    // Se inicializan los controles
    peRUTCliente.Clear;
    cbConveniosCliente.Clear;
    cbTiposComprobantes.Clear;
	CargarTiposComprobantes;
    edFechaEmision.Date := StrToDate(DateToStr(NowBase( DMConnections.BaseCAC )));
    edNumeroComprobante.Clear;
    edFechaVencimiento.Clear;
    cdsObtenerPendienteFacturacion.EmptyDataSet;
    edNumeroTelevia.Text := '';
    edCantidadCuotas.Clear;
    if dbItemsPendientes.PopupMenu <> nil then
        dbItemsPendientes.PopupMenu := nil;
    dbItemsPendientes.OnDblClick := dbItemsPendientesDblClick;
    lbl_Importe.Caption := '$0';
    peRUTCliente.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: ObtenerArchivoFase
Author : mlopez
Date Created : 23/08/2006
Description : Obtiene la fase de un nombre de archivo temporal
              (Antes esto se realizaba directamente en ObtenerPartesNombreArchivo
              pero lo separe porque tambien nos sirve para sabes si una boleta
              fue Impresa y solo necesita guardarse en la Base de datos, o tambien
              sirve para saber si la boleta ni se pudo comenzar a imprimir por falla
              de la impresora)
Return Values:
    BOLETA NO ABIERTA           = -1
    NO SE PUDO EVALUAR          = -2
    // Fases de la boleta fiscal.
    FASE_INICIAL                = 0
    FASE_VENTA                  = 1
    FASE_DESCUENTOS_RECARGOS    = 2
    FASE_PAGOS                  = 3
    FASE_FINAL                  = 4
*******************************************************************************}
function TfrmIngresarComprobantes.ObtenerArchivoFase(NombreArchivo: String): Integer;
var
    AuxStr: String;
begin
    //Result := -2; // -1 es la fase previa a la inicial (lo coloco asi para asegurarme que lee algo al realizar las validaciones)
    AuxStr := ParseParamByNumber(ParseParamByNumber(NombreArchivo, 2, '_F'), 1, '.');
    Result := IVal(AuxStr);
end;

{******************************** Function Header ******************************
Function Name: ExisteComprobanteImpreso
Author : mlopez
Date Created : 23/08/2006
Description : Devuelve True si el comprobante esta en Fase 0..4
*******************************************************************************}
function TfrmIngresarComprobantes.ExisteComprobanteImpreso: Boolean;
var
    Fase: Integer;
begin
    Fase := ObtenerArchivoFase(FArchivoBoleta);
    // Se comprueba que no devuelva:
    //      -1: La boleta no se encuentra abierta
    //      -2: El archivo no pudo ser evaluado
    Result := (Fase <> -1) and (Fase <> -2);
end;


{******************************** Function Header ******************************
Function Name: ObtenerIVA
Author : jconcheyro
Date Created : 11/09/2006
Description :
Parameters : None
Return Value : devuelve el porcentaje actual de IVA
*******************************************************************************}
function TfrmIngresarComprobantes.ObtenerIVA: Double;
begin
    //se usa now porque la emision de los creditos en la base es getdate
    Result := QueryGetValueInt(DMConnections.BaseCAC,
                                'SELECT dbo.ObtenerIVA(''' + FormatDateTime('yyyymmdd',
                                NowBase(spCrearComprobante.Connection)) + ''')');
    Result := Result / 100;       // esto devuelve un 19 porque en la base dice 1900

end;

{******************************** Function Header ******************************
Function Name: ProgramarEncabezadoImpresora
Author : jconcheyro
Date Created : 11/09/2006
Description :
Parameters : None
Return Value : Setea los datos del encabezado de la impresora
*******************************************************************************}
function TfrmIngresarComprobantes.ProgramarEncabezadoImpresora:boolean;
begin
    Result := ImpresoraFiscal.ProgramarEncabezado(6, ' ')
            and ImpresoraFiscal.ProgramarEncabezado(7, peRUTCliente.Text)
            and ImpresoraFiscal.ProgramarEncabezado(8, lNombreCompleto.Caption)
            and ImpresoraFiscal.ProgramarEncabezado(9, lDomicilio.Caption + ', ' + lComuna.Caption + ', ' + lRegion.Caption)
            and ImpresoraFiscal.ProgramarEncabezado(10, ' ');
    Result := Result;
end;

{******************************** Function Header ******************************
Function Name: ValidarBoletaManualContraTalonarios
Author : jconcheyro
Date Created : 11/09/2006
Description :
Parameters : None
Return Value : busca que numero en la tabla TalonariosComprobantesLocales
*******************************************************************************}
function TfrmIngresarComprobantes.ValidarBoletaManualContraTalonarios:boolean;
begin
    Result := QueryGetValue(DMConnections.BaseCAC,
                Format('select dbo.TalonariosComprobantesExisteTalonario(''%s'',%d)',
                [cbTiposComprobantes.Value, edNumeroComprobante.ValueInt])) = 'False';
end;


{******************************** Function Header ******************************
Function Name: ObtenerCodigoImpresoraManual
Author : jconcheyro
Date Created : 11/09/2006
Description :
Parameters : None
Return Value : Hay una impresora con marca de Manual, que se usa cuando la boleta
que se ingresa es de talonario preimpreso
*******************************************************************************}
function TfrmIngresarComprobantes.ObtenerCodigoImpresoraManual: Integer;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC,'select dbo.ObtenerCodigoImpresoraManual()');
end;

{******************************** Function Header ******************************
Function Name: ValidarComprobanteFiscal
Author : jconcheyro
Date Created : 11/09/2006
Description : busca en comprobantes.NumeroComprobanteFiscal para una impresora en particular
o para cualquier impresora , dependiendo del tipo de comprobante. Ejemplo boletas de impresora fiscal
puede haber varias numero 1, pero de distintas impresoras. Facturas numero 1 solo puede haber 1 sin importar
lo que diga el campo de impresora.
Parameters : CodigoImpresoraFiscal:integer
Return Value : boolean. Si devuelve True es que es un numero V�lido

*******************************************************************************}
function TfrmIngresarComprobantes.ValidarComprobanteFiscal(CodigoImpresoraFiscal:integer):boolean;
begin
    //CodigoImpresoraFiscal puede ser la manual o una que exista o ninguna, en ese caso el parametro es cero
    Result := QueryGetValue( DMConnections.BaseCAC, Format('SELECT dbo.ExisteComprobanteFiscal (''%s'', %d , %d )',
                	[cbTiposComprobantes.Value, edNumeroComprobante.ValueInt, CodigoImpresoraFiscal] )) = 'False';
end;





procedure TfrmIngresarComprobantes.chkBoletaManualClick(Sender: TObject);
begin
    if not chkBoletaManual.Checked then begin
        edNumeroComprobante.Clear;
        lbl_NroComprobante.Enabled  := False;
        edNumeroComprobante.Enabled := False;
        lbl_FechaEmision.Enabled    := False;
        edFechaEmision.Enabled      := False;
        edFechaEmision.Date         := NowBase(DMConnections.BaseCAC);
    end
    else begin
        lbl_NroComprobante.Enabled  := True;
        edNumeroComprobante.Enabled := True;
        lbl_FechaEmision.Enabled    := True;
        edFechaEmision.Enabled      := True;
    end;

end;

//Revision 10
function TfrmIngresarComprobantes.ObtenerCotizacionMoneda( const Fecha: TDateTime; const Moneda:String): boolean;
resourcestring
    MSG_ERROR_COTIZACION_INVALIDA = 'La cotizaci�n de %s a la fecha %s est� en cero o no existe. Debe ingresarse desde el m�dulo de Facturaci�n';
var
    CotizacionInt: integer;
begin
    //de la base viene como INT a 4 decimales
    Result := True;
    CotizacionInt := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerCotizacionAFecha(''%s'',''%s'')',[Moneda , FormatDateTime('yyyymmdd',Fecha)]));
    FCotizacion := CotizacionInt / 100 ;
    if FCotizacion = 0  then begin
        MsgBoxErr(format(MSG_ERROR_COTIZACION_INVALIDA, [Moneda, DateToStr(Fecha)]),Caption, Caption, MB_ICONSTOP);
        Result := False ;
    end;
end;
//Revision 10


{******************************** Function Header ******************************
Author: rharris
Date: 26/12/2008
Description: Se agrega funcion para el arriendo de televias en cuotas hasta abril 2008
*******************************************************************************}
Function TfrmIngresarComprobantes.SetearDatosArriendoCuotas;
begin
    Result := False;
    cdsObtenerPendienteFacturacion.DisableControls;
    cdsObtenerPendienteFacturacion.EmptyDataSet;
    lblNumeroTelevia.Visible := True ;
    edNumeroTelevia.Visible  := True ;
    lblCuotas.Visible        := True ;
    edCantidadCuotas.Visible := True ;
    if not (CbSoporte.Visible) then edNumeroTelevia.Clear;
    if not (CbSoporte.Visible) then edCantidadCuotas.Clear;
    // determino la cantidad de cuotas a generar al 01-06-2009
    edCantidadCuotas.ValueInt := QueryGetValueInt( DMConnections.BaseCAC, 'SELECT DATEDIFF(Month,GETDATE(),' + QuotedStr('20090701') + ')');
    edCantidadCuotas.Enabled := False;
    FTipoEnvioDocumento := QueryGetValueInt(DMConnections.BaseCAC,format('SELECT dbo.ObtenerMedioEnvioDocumento(%d)',[FCodigoConvenio]));
    // Determino el codigoconcepto a utilizar dependiendo del tipo medio envio del convenio
    if FTipoEnvioDocumento = 1 then
        spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoArriendoCuotasPostal
    else
        spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoArriendoCuotasMail;

    spObtenerDetalleConceptoMovimiento.Open ;

    if Not ObtenerCotizacionMoneda(NowBase(DMConnections.BaseCAC),
            spObtenerDetalleConceptoMovimiento.FieldByName('Moneda').AsString ) then Exit;

    cdsObtenerPendienteFacturacion.Append;
    cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
    cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
    cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := edFechaEmision.Date;
    cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := spObtenerDetalleConceptoMovimiento.FieldByName('CodigoConcepto').AsInteger;
    cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := spObtenerDetalleConceptoMovimiento.FieldByName('Descripcion').AsString;
    cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := '';


    if (spObtenerDetalleConceptoMovimiento.FieldByName('AfectoIVA').AsBoolean = False) then
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := edCantidadCuotas.ValueInt * (Round( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion  ) ) )
    else begin
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := edCantidadCuotas.ValueInt * (Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion * ( 1 + ObtenerIVA / 100) ) ) );
        cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat :=   edCantidadCuotas.ValueInt * (Round ( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion ) );
        cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := ObtenerIVA ;
        cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round ( cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat - cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat  );
    end;
    cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat, True, True, True);
    cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
    cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
    cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
    cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False ; //True;
    spObtenerDetalleConceptoMovimiento.Close ;
    cdsObtenerPendienteFacturacion.Post;
    cdsObtenerPendienteFacturacion.EnableControls;
    btnEliminarItem.Enabled := False ;
    btnAgregarItem.Enabled  := False ;
    cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion);
    ActualizarBotonRegistrar;
    Result := True;
end;

end.

