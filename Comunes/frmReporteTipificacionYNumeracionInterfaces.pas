{---------------------------------------------------------------------
		frmReporteTipificacionYNumeracionInterfaces

 Author: mbecerra
 Date: 24-Marzo-2009
 Description: 	(Ref. Facturación Electronica)
            	Reporte que visualiza el detalle del proceso de
                Tipificación y numeración masiva


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

----------------------------------------------------------------------}
unit frmReporteTipificacionYNumeracionInterfaces;

interface

uses
	DMConnection,
    UtilProc,
    UtilDB,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppBands, ppCtrls, ppReport, ppStrtch, ppSubRpt, ppPrnabl, ppClass,
  ppCache, ppComm, ppRelatv, ppProd, ppDB, ppDBPipe, DB, ADODB, UtilRB,
  ppParameter,ConstParametrosGenerales;                                         //SS_1147_NDR_20140710

type
  TReporteTipificacionYNumeracionInterfacesForm = class(TForm)
    ppReport1: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    ppLine3: TppLine;
    ppLabel5: TppLabel;
    ppLine12: TppLine;
    pptLineas: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel6: TppLabel;
    ppTituloReporte: TppLabel;
    ppLabel21: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppHeaderBand3: TppHeaderBand;
    ppLabel11: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppDBText17: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppFooterBand1: TppFooterBand;
    RBITipificacion: TRBInterface;
    spIH_RendicionesObtenerDatosReporte: TADOStoredProc;
    dsLogOperaciones: TDataSource;
    dbppLogOperaciones: TppDBPipeline;
    spObtenerErroresInterfase: TADOStoredProc;
    dsObtenerErroresInterfase: TDataSource;
    dbppErroresInterface: TppDBPipeline;
    ppComprobanteInicial: TppLabel;
    ppComprobanteFinal: TppLabel;
    ppLabel1: TppLabel;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppLabel2: TppLabel;
    ppDBText5: TppDBText;
    procedure RBITipificacionExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FCodigoOperacion : integer;
  public
    { Public declarations }
    function Inicializar(CodigoOperacion : integer) : boolean;
  end;

var
  ReporteTipificacionYNumeracionInterfacesForm: TReporteTipificacionYNumeracionInterfacesForm;

implementation

{$R *.dfm}

function TReporteTipificacionYNumeracionInterfacesForm.Inicializar;
resourcestring
	MSG_ERROR	= 'Ocurrió un error al invocar el Reporte';
var                                                                                                     //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                        //SS_1147_NDR_20140710
begin
  ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
  try                                                                                                 //SS_1147_NDR_20140710
    ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
  except                                                                                              //SS_1147_NDR_20140710
    On E: Exception do begin                                                                          //SS_1147_NDR_20140710
      Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
      MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
      Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
    end;                                                                                              //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

	try
    	FCodigoOperacion	:= CodigoOperacion;
        RBITipificacion.Execute;
        Result := True;
    except on e:exception do begin
        	Result := False;
            MsgBox(MSG_ERROR, Caption, MB_ICONERROR);
    	end;
    end;
end;

procedure TReporteTipificacionYNumeracionInterfacesForm.RBITipificacionExecute(
  Sender: TObject; var Cancelled: Boolean);
resourcestring
	MSG_CAPTION		= 'Tipificación y Numeración';
    MSG_TITULO		= 'Tipificación y Numeración de Comprobantes';

    MSG_SQL_MIN		= 'SELECT MIN(NumeroComprobante) ';
    MSG_SQL_MAX		= 'SELECT MAX(NumeroComprobante) ';
    MSG_SQL_FROM	= 'FROM Comprobantes (nolock) WHERE TipoComprobante = ''NK'' AND NumeroProcesoFacturacion = %d';

var
	ProcesoFacturacion, ComprobanteMin, ComprobanteMax : int64;

begin
    RBITipificacion.Caption := MSG_CAPTION;
    ppTituloReporte.Caption := MSG_TITULO;

    spIH_RendicionesObtenerDatosReporte.Close;
    spObtenerErroresInterfase.Close;

    spIH_RendicionesObtenerDatosReporte.Parameters.ParamByName('@CodigoOperacionInterface').Value := FCodigoOperacion;
    spObtenerErroresInterfase.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;

    spIH_RendicionesObtenerDatosReporte.Open;
    spObtenerErroresInterfase.Open;

    ProcesoFacturacion := spIH_RendicionesObtenerDatosReporte.FieldByName('NumeroProcesoFacturacion').AsInteger;
    ComprobanteMin := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_MIN + MSG_SQL_FROM, [ProcesoFacturacion]));
    ComprobanteMax := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_MAX + MSG_SQL_FROM, [ProcesoFacturacion]));
    ppComprobanteInicial.Caption := Format('%.0n', [1.0 * ComprobanteMin]);
    ppComprobanteFinal.Caption := Format('%.0n', [1.0 * ComprobanteMax]);
end;

end.
