object formSeleccionConvenio: TformSeleccionConvenio
  Left = 45
  Top = 290
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Convenios'
  ClientHeight = 269
  ClientWidth = 852
  Color = clBtnFace
  Constraints.MaxHeight = 400
  Constraints.MaxWidth = 1000
  Constraints.MinHeight = 300
  Constraints.MinWidth = 830
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  DesignSize = (
    852
    269)
  PixelsPerInch = 96
  TextHeight = 13
  object GBConvenios: TGroupBox
    Left = 0
    Top = 95
    Width = 852
    Height = 135
    Align = alBottom
    Caption = 'Convenios'
    TabOrder = 0
    object dbl_Convenios: TDBListEx
      Left = 2
      Top = 15
      Width = 848
      Height = 118
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 110
          Header.Caption = 'N'#250'mero convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 90
          Header.Caption = 'Fecha Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHoraCreacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Cant. Veh'#237'culos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 260
          Header.Caption = 'Domicilio de facturaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TelefonoPrincipal'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Forma de pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TelefonoAlternativo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 90
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescripcionEstadoSolicitud'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 85
          Header.Caption = 'Inicio vigencia'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
        end>
      DataSource = DS_SolicitudContacto
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbl_ConveniosDblClick
    end
  end
  object PanelDeAbajo: TPanel
    Left = 0
    Top = 230
    Width = 852
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      852
      39)
    object BtnCancelar: TDPSButton
      Left = 770
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Cancelar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtnCancelarClick
    end
    object btnAceptar: TDPSButton
      Left = 688
      Top = 5
      Width = 79
      Height = 26
      Hint = 'Aceptar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
  end
  object GBPersona: TGroupBox
    Left = 2
    Top = 0
    Width = 848
    Height = 96
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Persona'
    TabOrder = 2
    inline FrameDatosPersonaConsulta1: TFrameDatosPersonaConsulta
      Left = 8
      Top = 16
      Width = 544
      Height = 68
      TabOrder = 0
    end
  end
  object ObtenerConveniosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerSolicitudContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHoraCreacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraFin'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadVehiculos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@OrderBy'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 100
        Value = Null
      end>
    Left = 120
    Top = 184
  end
  object DS_SolicitudContacto: TDataSource
    DataSet = ObtenerConveniosPersona
    Left = 152
    Top = 184
  end
end
