unit FreAltaOSMultiple;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, validate, Dateedit, DmiCtrls,
  BuscaTab, UtilDB, DB, ADODB, Util, PeaProcs, PeaTypes, CacProcs,
  UtilProc, BuscaClientes, ImgList, MaskCombo, Buttons, Mensajes, Grids,
  DPSControls, FrmEditarDomicilio, Provider, DBClient, ListBoxEx, DBListEx,
  FrmDatosVehiculo, frePersonas, FreTelefono, RStrings, freListVehiculos;


type
  TFrameAltaOSMultiple = class(TFrame)
    ObtenerOrdenesServicioAdhesionDomicilios: TADOStoredProc;
    ObtenerOrdenServicioAdhesionCuentas: TADOStoredProc;
    dsCuentas: TDataSource;
    ObtenerOrdenesServicioAdhesionPersonas: TADOStoredProc;
    Panel1: TPanel;
    pnlBotones: TPanel;
    ObtenerDatosComunicacion: TADOStoredProc;
    Panel3: TPanel;
    btnSiguiente: TDPSButton;
    btnAnterior: TDPSButton;
    PageControl: TPageControl;
    tab_cuentas: TTabSheet;
    Panel2: TPanel;
    pc_Adhesion: TPageControl;
    ts_DatosCliente: TTabSheet;
    ts_Vehiculos: TTabSheet;
    pnlVehiculos: TPanel;
    ts_FormaPago: TTabSheet;
    Label22: TLabel;
    lbl_entidad: TLabel;
    Label23: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    rb_pospagoautomatico: TRadioButton;
    cb_tipodebito: TComboBox;
    cb_entidaddebito: TComboBox;
    txt_cuentadebito: TEdit;
    rb_pospagomanual: TRadioButton;
    rb_prepago: TRadioButton;
    chk_enviofacturas: TCheckBox;
    btnCancelar: TDPSButton;
    ts_Contrato: TTabSheet;
    DPSButton1: TDPSButton;
    DPSButton2: TDPSButton;
    ObtenerOrdenesServicioAdhesionMediosComunicacion: TADOStoredProc;
    dsOrdenesServicioAdhesionRoles: TDataSource;
    cdsOrdenesServicioAdhesionPersonas: TClientDataSet;
    IntegerField10: TIntegerField;
    IntegerField12: TIntegerField;
    GBDomicilios: TGroupBox;
    lblnada: TLabel;
    Label2: TLabel;
    LblDomicilioComercial: TLabel;
    LblDomicilioParticular: TLabel;
    Label1: TLabel;
    btnBorrarDomicilioParticular: TSpeedButton;
    btnBorrarDomicilioComercial: TSpeedButton;
    BtnDomicilioComercialAccion: TDPSButton;
    BtnDomicilioParticularAccion: TDPSButton;
    RGDomicilioEntrega: TRadioGroup;
    GBMediosComunicacion: TGroupBox;
    Label35: TLabel;
    txtEmailParticular: TEdit;
    GBRolesContrato: TGroupBox;
    GBDomicilioEntrega: TGroupBox;
    RBDomicilioEntregaParticular: TRadioButton;
    RBDomicilioEntregaComercial: TRadioButton;
    RBDomicilioEntregaOtros: TRadioButton;
    Label6: TLabel;
    DPSButton4: TDPSButton;
    btnBorrarDomicilioDocumentacion: TSpeedButton;
    DPSButton3: TDPSButton;
    DPSButton5: TDPSButton;
    ts_Observaciones: TTabSheet;
    txtObservaciones: TMemo;
    GBDetalleVehiculo: TGroupBox;
    btnEditarRolVehiculo: TDPSButton;
    btnEliminarRolVehiculo: TDPSButton;
    GBVehiculos: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    lblPatente: TLabel;
    Label11: TLabel;
    lblMarca: TLabel;
    Label13: TLabel;
    lblModelo: TLabel;
    Label14: TLabel;
    lblAnio: TLabel;
    label100: TLabel;
    lblColor: TLabel;
    dsObtenerOrdenesServicioAdhesionPersonas: TDataSource;
    cdsOrdenesServicioAdhesionCuentas: TClientDataSet;
    IntegerField14: TIntegerField;
    IntegerField15: TIntegerField;
    cdsOrdenesServicioAdhesionDomicilios: TClientDataSet;
    IntegerField16: TIntegerField;
    IntegerField17: TIntegerField;
    dsOrdenesServicioAdhesionDomicilios: TDataSource;
    cdsOrdenesServicioAdhesionMediosComunicacion: TClientDataSet;
    IntegerField18: TIntegerField;
    IntegerField19: TIntegerField;
    dsOrdenesServicioAdhesionMediosComunicacion: TDataSource;
    cdsOrdenesServicioAdhesionRolesCuentas: TClientDataSet;
    IntegerField20: TIntegerField;
    IntegerField21: TIntegerField;
    dsOrdenesServicioAdhesionRolesCuentas: TDataSource;
    cdsOrdenesServicioAdhesionRoles: TClientDataSet;
    IntegerField22: TIntegerField;
    IntegerField23: TIntegerField;
    QryOrdenesServicioAdhesionRoles: TADOQuery;
    qryOrdenesServicioAdhesionRolesCuentas: TADOQuery;
    btnIngresarRolVehiculo: TDPSButton;
    dblRolesCuenta: TDBListEx;
    DBListEx1: TDBListEx;
    DPSButton6: TDPSButton;
    GBDetalleRolContrato: TGroupBox;
    cb_Roles: TComboBox;
    Label7: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    ComboBox1: TComboBox;
    BtnAceptar: TBitBtn;
    BitBtn2: TBitBtn;
    FMVehiculos: TFrameListVehiculos;
    FMPersona: TFramePersona;
    FMTelefonoPrincipal: TFrameTelefono;
    FMTelefonoAlternativo: TFrameTelefono;
	procedure chk_contagClick(Sender: TObject);
    procedure BtnDomicilioParticularAccionClick(Sender: TObject);
    procedure BtnDomicilioComercialAccionClick(Sender: TObject);
    procedure dblVehiculoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cb_tipodebitoChange(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure btnBorrarDomicilioParticularClick(Sender: TObject);
    procedure GBDomiciliosClick(Sender: TObject);
    procedure PageControlGetImageIndex(Sender: TObject; TabIndex: Integer;
      var ImageIndex: Integer);
  private
	{ Private declarations }
    FTipoOrdenServicio: Integer;
    FComunicacion: TDatosContactoComunicacion;
	FResult: TModalResult;
	FOrdenServicio: integer;
    CodigoDomicilioParticular, CodigoDomicilioComercial, CodigoDomicilioEntrega: integer;
    DomicilioParticular: TDatosDomicilio;
    DomicilioComercial: TDatosDomicilio;
    BajaDomicilioComercial,
    BajaDomicilioParticular,
    GuardarTelefonoParticular, GuardarTelefonoComercial,
    GuardarTelefonoMovil, GuardarEmail: boolean;
    procedure GuardarComunicacion;
	procedure GuardarAdhesion;
    procedure MarcarComoDomicilioEntrega(tipoOrigenDatos: integer);
    procedure PrepararMostrarDomicilio (descripcion: AnsiString; tipoOrigen: integer);
    procedure MostrarDomicilio (var L: TLabel; descripcion: AnsiString);
    procedure AltaDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    procedure ModificarDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    function  EstaElegidoComoDomicilioEntrega(tipoOrigenDatos: integer): boolean;
    procedure HabilitarBotonesVehiculos;
    procedure OrganizarControlesPageAdhesion;
    procedure CargarDatosPersona(Persona: TDatosPersonales);
    procedure CargarDatosComunicacion(var Comunicacion: TDatosContactoComunicacion);
    procedure InicializaClientDataSet(var CDS: TClientDataSet);
    function TabDatosClienteOk: boolean;
    function TabDatosVehiculosOk: boolean;
    function TabFormaPagoOk: boolean;
    function TabDatosContratoOk: boolean;
    function TabObservacionesOk: boolean;

    //** PROPIEDADES DE LA PERSONA CLIENTE **//
    function GetPersoneria: AnsiString;
    function GetRut: String;
    function GetRazonSocial: String;
    function GetApellido: String;
    function GetApellidoMaterno: String;
    function GetNombre: String;
    function GetCodigoSituacionIVA: String;
    function GetCodigoPaisOrigen: String;
    function GetSexo: String;
    function GetCodigoActividad: Integer;
    function GetEmail: String;
    function GetFechaNacimiento: TDateTime;
    function GetFechaCreacion: TDateTime;
    function GetApellidoContacto: String;
    function GetApellidoMaternoContacto: String;
    function GetNombreContacto: String;
    function GetSexoContacto: String;
  public
	{ Public declarations }
    Property OrdenServicio : integer Read FOrdenServicio;

    //** PROPIEDADES DE LA PERSONA CLIENTE **//
    property Personeria:AnsiString read GetPersoneria;
    property RUT:String read GetRut;
    property RazonSocial:String read GetRazonSocial;
    property Apellido:String read GetApellido;
    property ApellidoMaterno:String read GetApellidoMaterno;
    property Nombre:String read GetNombre;
    property Sexo:String read GetSexo;
    property ApellidoContacto:String read GetApellidoContacto;
    property ApellidoMaternoContacto:String read GetApellidoMaternoContacto;
    property NombreContacto:String read GetNombreContacto;
    property SexoContacto:String read GetSexoContacto;
    property CodigoSituacionIVA:String read GetCodigoSituacionIVA;
    property CodigoPais:String read GetCodigoPaisOrigen;
    property CodigoActividad:Integer read GetCodigoActividad;
    property Email:String read GetEmail;
    property FechaNacimiento:TDateTime read GetFechaNacimiento;
    property FechaCreacion:TDateTime read GetFechaCreacion;


 	function Inicializar(Comunicacion: integer): Boolean;
	function Guardar: TModalResult;
    function Cancelar: TModalResult;
    procedure Siguiente;
    procedure Anterior;
  end;

var
  FrameAltaOSMultiple: TFrameAltaOSMultiple;
  PersonaOS: TDatosPersonales;
  MedioContacto: integer;

implementation

uses DMConnection;

{ TFormAltaOrdenServicio }
{$R *.dfm}

resourcestring
    CAPTION_BOTON_DOMICILIO_ALTA = 'Alta';
    CAPTION_BOTON_DOMICILIO_MODIFICAR = 'Modificar';
    CAPTION_RECLAMO_FACTURACION   = 'Validar datos del Reclamo de Facturaci�n';
    MSG_OBSERVACIONES_FACTURACION = 'Debe indicar alguna observaci�n como explicaci�n del Reclamo por Facturaci�n.';
    MSG_CLIENTE                   = 'Debe indicar un cliente para este reclamo.';
    MSG_ERROR_CODIGO_INEXISTENTE  = 'El c�digo de orden de servicio no existe.';
    MSG_SELECCIONAR_FACTURA	  	  = 'Debe indicar el Comprobante que posee la facturaci�n incorrecta';
    MSG_ACTUALIZAR_CONTACTO       = 'Actualizar Contacto';
    MSG_ACTUALIZAR_COMUNICACION   = 'Actualizar Comunicaci�n';
    MSG_FINALIZAR_COMUNICACION    = 'Finalizar Comunicaci�n';


procedure TFrameAltaOSMultiple.Siguiente;
begin
    if btnSiguiente.Enabled then btnSiguiente.OnClick(btnSiguiente);
end;

procedure TFrameAltaOSMultiple.Anterior;
begin
    if btnAnterior.Enabled then btnAnterior.OnClick(btnAnterior);
end;


procedure TFrameAltaOSMultiple.chk_contagClick(Sender: TObject);
begin
	rb_pospagoautomatico.Enabled    := True;//REVISAR chk_contag.Checked;
	cb_tipodebito.Enabled           := rb_pospagoautomatico.Checked;
	cb_entidaddebito.Enabled        := cb_tipodebito.Enabled;
	txt_cuentadebito.Enabled        := cb_tipodebito.Enabled;
end;

procedure TFrameAltaOSMultiple.GuardarAdhesion;
resourcestring
    MSG_CAPTION_ORDEN_SERVICIO_ADHESION = 'Orden de servicio de Adhesi�n';
    CAPTION_VALIDAR_ADHESION = 'Validar Datos de la Adhesi�n';
    CAPTION_GRABADOS_CORRECTAMENTE = 'Datos grabados correctamente.';
    GRABADOS_CORRECTAMENTE = 'Datos grabados correctamente.';

    MSG_NOMBRE       = 'Debe indicar un nombre.';
    MSG_APELLIDO     = 'Debe indicar un apellido.';
    MSG_DOCUMENTO    = 'Debe indicar un documento.';
    MSG_NUMERODOCUMENTO = 'El documento especificado es incorrecto.';

    MSG_CALLE_PERS      = 'Debe indicar la calle (domicilio personal)';
    MSG_NUMERO_PERS     = 'Debe indicar un n�mero (domicilio personal)';
    MSG_CPOSTAL_PERS    = 'Debe indicar el c�digo postal (domicilio personal)';
    MSG_REGION_PERS     = 'Debe indicar la regi�n o estado de residencia (domicilio personal)';
    MSG_COMUNA_PERS     = 'Debe indicar la comuna (domicilio personal)';
    MSG_CALLE_DOCU      = 'Debe indicar la calle (domicilio de la documentaci�n)';
    MSG_NUMERO_DOCU     = 'Debe indicar un n�mero (domicilio de la documentaci�n)';
    MSG_CPOSTAL_DOCU    = 'Debe indicar el c�digo postal (domicilio de la documentaci�n)';
    MSG_REGION_DOCU     = 'Debe indicar la regi�n o estado de residencia (domicilio de la documentaci�n)';
    MSG_COMUNA_DOCU     = 'Debe indicar la comuna (domicilio de la documentaci�n)';

    MSG_PAIS         = 'Debe indicar el pa�s de residencia.';

    MSG_FECHANACIMIENTO = 'La fecha es incorrecta';
    MSG_MAIL            = 'La direcci�n de mail es inv�lida.';


    MSG_CUENTA_ACTIVA   = 'Ya existen una cuenta activa con la patente %s';
    MSG_TARJETA_BANCO   = 'Debe indicar el tipo de tarjeta o banco.';
    MSG_CUENTA_BANCARIA = 'Debe indicar n�mero de cuenta bancaria.';
    MSG_DOMICILIO_DOCUMENTOS_INCOMPLETO = 'Ingres� el domicilio de documentos en forma incompleta. '+ CRLF +
                                          'Por favor revise el n�mero, la comuna y la regi�n.';
    MSG_DOMICILIO_NINGUNO = 'No ingres� ninguno de los domicilios. '+ CRLF +
                            'Por favor complete los datos.';
    MSG_DOMICILIO_NINGUNO_ENTREGA = 'No seleccion� ninguno de los domicilios para que sea de entrega. ';
    MSG_DOMICILIO_ENTREGA_LIMPIO = 'Seleccion� un domicilio de entrega que tiene los datos incompletos. ';
    MSG_ERROR_DOMICILIO_ENTREGA_BORRAR = 'Eligi� un domicilio de entrega que est� marcado para ser borrado.';
Var
	TarjOk: Boolean;
    DescriError: AnsiString;
    DocCorrecto: Boolean;
    DatosContacto : TDatosAdhesion;
    TipoOrigenDatoDomicilioEntrega: smallint;
begin
    FResult := mrNone;

    // Validamos el documento del cliente
    //DocCorrecto := cbTipoNumeroDocumento.ValidateMask;

    (*Si ya viene ingresado de la base, se encuentra deshabilitado. Pero puede
    ocurrir que est� mal cargado en la base, por eso lo habilito en este momento
    para que pueda corregirlo (y no de error!!!) si esta mal*)


	// Validamos los datos del cliente

    //Si no carg� ninguno de los dos domicilios, reboto!
    {
    if (DomicilioParticular.IndiceDomicilio <> 1) and
       (DomicilioParticular.IndiceDomicilio <> 1) then begin
        MsgBox(MSG_DOMICILIO_NINGUNO, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
        Exit;
    end;
    }
    //Si marc� el domicilio particular para entrega, pero no lo carg�, le aviso
    {
    if (EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR)) then begin
        if (DomicilioParticular.IndiceDomicilio <> 1) then begin
            MsgBox(MSG_DOMICILIO_ENTREGA_LIMPIO, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
            exit;
        end
        else begin
            if (ChkEliminarDomicilioParticular.Checked) then begin
                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
                exit;
            end;
        end;
    end;
    }
    //Si marc� el domicilio comercial para entrega, pero no lo carg�, le aviso
    {
    if (EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL)) then begin
        if (DomicilioComercial.IndiceDomicilio <> 1) then begin
            MsgBox(MSG_DOMICILIO_ENTREGA_LIMPIO, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
            exit;
        end
        else begin
            if (ChkEliminarDomicilioComercial.Checked) then begin
                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
                exit;
            end;
        end;
    end;

    //Si no marc� ninguno, me fijo cual ingres� y lo marco.
    if (not EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR)) and
       (not EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL)) then begin
        if StrRight(Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
            //Si es persona f�sica y cargo el domicilio particular lo marco...
            if (DomicilioParticular.IndiceDomicilio = 1) then
                MarcarComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR)
            else
                MarcarComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL);
        end
        else begin //Es Persona Jur�dica
            //Si es persona f�sica y cargo el domicilio comercial lo marco...
            if (DomicilioComercial.IndiceDomicilio = 1) then
                MarcarComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL)
            else
                MarcarComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR);
        end;
    end;

	// Validamos las formas de pago
	if rb_pospagoautomatico.checked then begin
		if not ValidateControls([cb_entidaddebito], [cb_entidaddebito.ItemIndex >= 0],
		  CAPTION_VALIDAR_ADHESION, [MSG_TARJETA_BANCO]) then Exit;
		if cb_tipodebito.ItemIndex = 0 then begin
			// Con tarjeta
            TarjOk := ValidarTarjetaCredito(DMConnections.BaseCAC,
			  IVal(StrRight(cb_entidaddebito.Text, 20)), txt_cuentadebito.text, DescriError);
			if not ValidateControls([txt_cuentadebito], [TarjOk], CAPTION_VALIDAR_ADHESION,
              [DescriError]) then Exit;
		end else begin
			// Con cuenta bancaria
			if not ValidateControls([txt_cuentadebito], [Trim(txt_cuentadebito.Text) <> ''],
			  CAPTION_VALIDAR_ADHESION, [MSG_CUENTA_BANCARIA]) then Exit;
		end;
	end;

    //Cargo el registro //**--**CORREGIR
    With DatosContacto do begin
        Persona.Personeria          := Trim(StrRight(cbPersoneria.Text, 1))[1];
        Persona.CodigoPersona       := Persona.CodigoPersona;
        Persona.Apellido            := Trim(txt_apellido.text);
        if StrRight(Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
            Persona.ApellidoMaterno := Trim(txt_apellidoMaterno.text);
            ContactoComercial       := '';
            Persona.Sexo            := trim(cb_Sexo.Text)[1];
        end else begin //JURIDICAS
            Persona.ApellidoMaterno := '';
            ContactoComercial       := Trim(txt_apellidoMaterno.text);
            ///Sexo                 := '';
        end;
        Persona.Nombre              := Trim(txt_nombre.text);
        Persona.TipoDocumento       := Trim(StrRight(cbTipoNumeroDocumento.ComboText, 10));
        Persona.NumeroDocumento     := Trim(cbTipoNumeroDocumento.MaskText);
        Persona.LugarNacimiento     := Trim(StrRight(cbLugarNacimiento.Text, 20));
        Persona.CodigoActividad     := iif(cb_actividad.ItemIndex < 0, 0,  Ival(StrRight( Trim(cb_actividad.Text), 10)));

        Persona.FechaNacimiento     := dtfechanacimiento.Date;

        TipoOrigenDatoDomicilioEntrega := 0;
        //Domicilio Particular
        if (DomicilioParticular.IndiceDomicilio = 1) then begin
            //CARGO EL DOMICILIO
            {EVALUAR PONER ESTO EN UNA FUNCION QUE RECIBA EL IDENTIFICADOR DEL DOMICILIO Y POR VAR UN REGISTRO
            CodigoDomicilioParticular := DomicilioParticular.CodigoDomicilio;
            CodigoCalleParticular := DomicilioParticular.CodigoCalle;
            CodigoTipoCalleParticular := DomicilioParticular.CodigoTipoCalle;
            NumeroCalleParticular := DomicilioParticular.NumeroCalle;
            PisoParticular := DomicilioParticular.Piso;
            DptoParticular                      := DomicilioParticular.Dpto;
            DetalleParticular                   := DomicilioParticular.Detalle;
            CodigoPostalParticular              := DomicilioParticular.CodigoPostal;
            CalleDesnormalizadaParticular       := DomicilioParticular.CalleDesnormalizada;
            CodigoTipoEdificacionParticular     := DomicilioParticular.CodigoTipoEdificacion;
            CodigoCalleRelacionadaUnoParticular := DomicilioParticular.CodigoCalleRelacionadaUno;
            NumeroCalleRelacionadaUnoParticular := DomicilioParticular.NumeroCalleRelacionadaUno;
            CodigoCalleRelacionadaDosParticular := DomicilioParticular.CodigoCalleRelacionadaDos;
            NumeroCalleRelacionadaDosParticular := DomicilioParticular.NumeroCalleRelacionadaDos;
            NormalizadoParticular := DomicilioParticular.Normalizado;
            if EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR) then
                TipoOrigenDatoDomicilioEntrega := TIPO_DOMICILIO_PARTICULAR;

        end else begin

            CodigoDomicilioParticular := -1;
            CodigoCalleParticular := -1;
            CodigoTipoCalleParticular := -1;
            NumeroCalleParticular := -1;
            PisoParticular := -1;
            DptoParticular := '';
            DetalleParticular := '';
            CodigoPostalParticular := '';
            CalleDesnormalizadaParticular := '';
            CodigoTipoEdificacionParticular := -1;
            CodigoCalleRelacionadaUnoParticular := -1;
            NumeroCalleRelacionadaUnoParticular := -1;
            CodigoCalleRelacionadaDosParticular := -1;
            NumeroCalleRelacionadaDosParticular := -1;
            NormalizadoParticular := false;

        end;
        //Domicilio Comercial
        if (DomicilioComercial.IndiceDomicilio = 1) then begin
            //CARGO EL DOMICILIO
            CodigoDomicilioComercial := DomicilioComercial.CodigoDomicilio;
            CodigoCalleComercial := DomicilioComercial.CodigoCalle;
            CodigoTipoCalleComercial := DomicilioComercial.CodigoTipoCalle;
            NumeroCalleComercial := DomicilioComercial.NumeroCalle;
            PisoComercial := DomicilioComercial.Piso;
            DptoComercial := DomicilioComercial.Dpto;
            DetalleComercial := DomicilioComercial.Detalle;
            CodigoPostalComercial := DomicilioComercial.CodigoPostal;
            CalleDesnormalizadaComercial := DomicilioComercial.CalleDesnormalizada;
            CodigoTipoEdificacionComercial := DomicilioComercial.CodigoTipoEdificacion;
            CodigoCalleRelacionadaUnoComercial := DomicilioComercial.CodigoCalleRelacionadaUno;
            NumeroCalleRelacionadaUnoComercial := DomicilioComercial.NumeroCalleRelacionadaUno;
            CodigoCalleRelacionadaDosComercial := DomicilioComercial.CodigoCalleRelacionadaDos;
            NumeroCalleRelacionadaDosComercial := DomicilioComercial.NumeroCalleRelacionadaDos;
            NormalizadoComercial := DomicilioComercial.Normalizado;
            if EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL) then
                TipoOrigenDatoDomicilioEntrega := TIPO_DOMICILIO_COMERCIAL;
        end else begin
            //Limpiar domicilio
            CodigoDomicilioComercial := -1;
            CodigoCalleComercial := -1;
            CodigoTipoCalleComercial := -1;
            NumeroCalleComercial := -1;
            PisoComercial := -1;
            DptoComercial := '';
            DetalleComercial := '';
            CodigoPostalComercial := '';
            CalleDesnormalizadaComercial := '';
            CodigoTipoEdificacionComercial := -1;
            CodigoCalleRelacionadaUnoComercial := -1;
            NumeroCalleRelacionadaUnoComercial := -1;
            CodigoCalleRelacionadaDosComercial := -1;
            NumeroCalleRelacionadaDosComercial := -1;
            NormalizadoComercial := false;
        end;

        EnvioFacturas           := chk_enviofacturas.checked;

		// Datos del Pago
        TipoPago        := iif(rb_pospagomanual.Checked, TP_POSPAGO_MANUAL, TP_DEBITO_AUTOMATICO);
        TipoDebito      := iif( not rb_pospagoautomatico.Checked, '', iif(cb_tipodebito.ItemIndex = 0, DA_TARJETA_CREDITO, DA_CUENTA_BANCARIA));
        CodigoEntidad   := iif( not rb_pospagoautomatico.Checked, 0, IVal(StrRight(cb_entidaddebito.Text, 20)));
        CuentaDebito    := iif( not rb_pospagoautomatico.Checked, '', Trim(txt_cuentadebito.Text));

        //Medios de comunicacion
        TelefonoParticular.Valor := trim(txtTelefonoParticular.Text);
        TelefonoParticular.Guardar := GuardarTelefonoParticular;
        TelefonoComercial.Valor := trim(txtTelefonoComercial.Text);
        TelefonoComercial.Guardar := GuardarTelefonoComercial;
        //TelefonoMovilParticular.Valor := trim(txtTelefonoMovil.Text);
        TelefonoMovilParticular.Guardar := false;
        EmailParticular.Valor := trim(txtEmailParticular.Text);
        EmailParticular.Guardar := GuardarEmail;
	end;
    }
	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenAdhesion(
        DMConnections.BaseCAC,
        FComunicacion.CodigoComunicacion,
        PRI_NORMAL,
        Trim(txtobservaciones.Text),
        DatosContacto,
        TipoOrigenDatoDomicilioEntrega,
        DescriError);

	if FOrdenServicio <= 0 then begin
        MsgBox( DescriError, MSG_CAPTION_ORDEN_SERVICIO_ADHESION, MB_ICONSTOP);
        Exit;
    end;
	// Listo
	FResult := mrOk;
end;


procedure TFrameAltaOSMultiple.GuardarComunicacion;
resourcestring
    CAPTION_COMUNICACION    = 'Validar datos de la Comunicaci�n';

    MSG_MEDIO_CONTACTO      = 'Debe seleccionar un Medio de Contacto para Crear la Comunicaci�n';
    MSG_TIPO_CONTACTO       = 'Debe seleccionar un Tipo de Contacto para Crear la Comunicaci�n';
    MSG_CONTACTO            = 'Debe indicar un Contacto para Crear la Comunicaci�n';
var
    ComunicacionTemp: integer;
    FechaHoraInicioTmp: TDateTime;
    MsgError: TMsgError;
    ContactoComunicacion: TDatosContactoComunicacion;
begin
    FResult := mrNone;

	// Validamos los datos del cliente
    ComunicacionTemp := 0;

    if not ActualizarComunicacion(
        ContactoComunicacion,
        UsuarioSistema,
        FechaHoraInicioTmp,
        nulldate,
        null,
        0,
        '',//Estado
        nil, //Observaciones	: TStringList;
        ComunicacionTemp, MsgError) then begin
        MsgBox(StrPas(MsgError), MSG_ACTUALIZAR_COMUNICACION, MB_ICONSTOP);
        Exit;
    end;

    if not FinalizarComunicacion(ComunicacionTemp, MsgError) then begin
        MsgBox(StrPas(MsgError), MSG_FINALIZAR_COMUNICACION, MB_ICONSTOP);
        Exit;
    end;

    FComunicacion.CodigoComunicacion  := ComunicacionTemp;
end;


function TFrameAltaOSMultiple.Cancelar: TModalResult;
begin
    Result :=  mrCancel;
end;

function TFrameAltaOSMultiple.Guardar: TModalResult;
ResourceString
	MSG_CAPTION = 'Guardar Orden de Servicio';
    MSG_DETALLE = 'Debe Seleccionar un Tipo de Orden de Servicio';
var
    ComunicacionTemp: integer;
    FechaHoraInicioTmp: TDateTime;
    MsgError: TMsgError;
    ContactoComunicacion: TDatosContactoComunicacion;
begin
    // Primero me fijo si tengo una Comunicacion creada
    if (FComunicacion.CodigoComunicacion <= 0) then begin
        if (FTipoOrdenServicio in [OS_GENERAR_ORDEN_DE_TRABAJO, OS_REPARAR_POSTE_SOS, OS_REPARAR_PORTICO, OS_REPARAR_PAVIMENTO])
            and (not MedioContacto in [MC_EMAIL, MC_FAX, MC_VOICE_MAIL]) then
            GuardarComunicacion
        else begin
            ComunicacionTemp := 0;
            FechaHoraInicioTmp := nulldate;
            if not ActualizarComunicacion(
                ContactoComunicacion,
                UsuarioSistema,
                FechaHoraInicioTmp,
                nulldate,
                null,
                MedioContacto,
                '',
                nil,
                ComunicacionTemp, MsgError) then begin
                    MsgBox(StrPas(MsgError), MSG_ACTUALIZAR_COMUNICACION, MB_ICONSTOP);
                    Exit;
            end;

            if not FinalizarComunicacion(ComunicacionTemp, MsgError) then begin
                MsgBox(StrPas(MsgError), MSG_FINALIZAR_COMUNICACION, MB_ICONSTOP);
                Exit;
            end;

            FComunicacion.CodigoComunicacion  := ComunicacionTemp;
        end;
    end;

    GuardarAdhesion;
end;





procedure TFrameAltaOSMultiple.MarcarComoDomicilioEntrega(
  tipoOrigenDatos: integer);
begin
    if tipoOrigenDatos = TIPO_DOMICILIO_PARTICULAR then
        RGDomicilioEntrega.ItemIndex := 0
    else
        if tipoOrigenDatos = TIPO_DOMICILIO_COMERCIAL then
            RGDomicilioEntrega.ItemIndex := 1
        else
            RGDomicilioEntrega.ItemIndex := -1;
end;

procedure TFrameAltaOSMultiple.PrepararMostrarDomicilio(
  descripcion: AnsiString; tipoOrigen: integer);
begin
    if tipoOrigen = TIPO_DOMICILIO_PARTICULAR then begin
        if (descripcion = '') then
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA
        else
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        //ChkEliminarDomicilioParticular.Enabled := (descripcion <> '') and (codigodomicilioparticular > 0);
        MostrarDomicilio (lblDomicilioParticular, descripcion);
    end
    else begin
        if (descripcion = '') then
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA
        else
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        //ChkEliminarDomicilioComercial.Enabled := (descripcion <> '') and (codigodomiciliocomercial > 0);
        MostrarDomicilio (lblDomicilioComercial, descripcion);
    end;
end;

procedure TFrameAltaOSMultiple.MostrarDomicilio(var L: TLabel;
  descripcion: AnsiString);
begin
    if descripcion = '' then begin
        l.Font.Style := [fsItalic];
        l.Caption := FALTA_INGRESAR_DOMICILIO;
    end
    else begin
        l.Font.Style := [];
        l.Caption := descripcion;
    end;
end;

procedure TFrameAltaOSMultiple.AltaDomicilio(tipoOrigenDatos: integer;
  var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_AGREGAR_CAPTION = 'Agregar Domicilio';
    MSG_ERROR_AGREGAR   = 'No se ha podido agregar el domicilio';
var
    f: TFormEditarDomicilio;
begin
{    LimpiarRegistroDomicilio(datosDomicilio, tipoOrigenDatos);
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar(tipoOrigenDatos, true) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                //lo utilizo para indicar que hay datos. Si tiene -1 es porque esta vac�o
                IndiceDomicilio := 1;
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
                PrepararMostrarDomicilio(Descripcion,tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR, E.message, MSG_AGREGAR_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    f.Release;}
end;

procedure TFrameAltaOSMultiple.ModificarDomicilio(
  tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_EDITAR_CAPTION = 'Modificar Domicilio';
    MSG_ERROR_EDITAR   = 'No se ha podido modificar el domicilio';
var
	f: TFormEditarDomicilio;
begin
    Application.CreateForm(TFormEditarDomicilio, f);
    {
    if f.Inicializar(
        datosDomicilio.CodigoDomicilio,
        tipoOrigenDatos,
        datosDomicilio.CodigoPais,
        datosDomicilio.CodigoRegion,
        datosDomicilio.CodigoComuna,
        datosDomicilio.CodigoCiudad,
        datosDomicilio.CodigoTipoCalle,
        datosDomicilio.CodigoCalle,
        datosDomicilio.CalleDesnormalizada,
        datosDomicilio.NumeroCalle,
        datosDomicilio.Piso,
        datosDomicilio.Dpto,
        datosDomicilio.Detalle,
        datosDomicilio.CodigoPostal,
        datosDomicilio.CodigoTipoEdificacion,
        EstaElegidoComoDomicilioEntrega(tipoOrigenDatos),
        datosDomicilio.CodigoCalleRelacionadaUno,
        datosDomicilio.NumeroCalleRelacionadaUno,
        datosDomicilio.CodigoCalleRelacionadaDos,
        datosDomicilio.NumeroCalleRelacionadaDos
        ) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                IndiceDomicilio := 1; //significa que hay datos
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
                PrepararMostrarDomicilio(Descripcion, tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR, E.message, MSG_EDITAR_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    }
    f.Release;
end;

function TFrameAltaOSMultiple.EstaElegidoComoDomicilioEntrega(
  tipoOrigenDatos: integer): boolean;
begin
    if tipoOrigenDatos = TIPO_DOMICILIO_PARTICULAR then
        result := RGDomicilioEntrega.ItemIndex = 0
    else
        if tipoOrigenDatos = TIPO_DOMICILIO_COMERCIAL then
            result := RGDomicilioEntrega.ItemIndex = 1
        else
            result := false;
end;

procedure TFrameAltaOSMultiple.BtnDomicilioParticularAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioParticularAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_DOMICILIO_PARTICULAR, DomicilioParticular);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR);
        //AltaDomicilioParticular := true;
        BajaDomicilioParticular := false;
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_DOMICILIO_PARTICULAR, DomicilioParticular);
    end;
end;

procedure TFrameAltaOSMultiple.BtnDomicilioComercialAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioComercialAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_DOMICILIO_COMERCIAL, DomicilioComercial);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_DOMICILIO_COMERCIAL);
        BajaDomicilioComercial := false;
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_DOMICILIO_COMERCIAL, DomicilioComercial);
    end;
end;

procedure TFrameAltaOSMultiple.dblVehiculoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    {
    case Key of

        VK_INSERT: if (ssShift in Shift) then begin
                        if btnEditarVehiculo.enabled then btnEditarVehiculo.OnClick(nil)
                   end else begin
                        if btnAgregarVehiculo.Enabled then btnAgregarVehiculo.OnClick(nil);
                   end;
        VK_DELETE: if btnEliminarVehiculo.Enabled then btnEliminarVehiculo.OnClick(nil);

    end }
end;

procedure TFrameAltaOSMultiple.HabilitarBotonesVehiculos;
begin
    //btnEditarVehiculo.Enabled      := iif( cdsOrdenesServicioAdhesionCuentas.Active, cdsOrdenesServicioAdhesionCuentas.RecordCount > 0, False);
    //btnEliminarVehiculo.Enabled    := btnEditarVehiculo.Enabled;
end;

procedure TFrameAltaOSMultiple.OrganizarControlesPageAdhesion;
begin

end;

procedure TFrameAltaOSMultiple.CargarDatosComunicacion(var Comunicacion: TDatosContactoComunicacion);
var
    PersoneriaAux: char;
begin
    try
        ObtenerDatosComunicacion.Parameters.ParamByName('@CodigoComunicacion').Value := Comunicacion.CodigoComunicacion;
        if not OpenTables([ObtenerDatosComunicacion]) then Exit;
        // CARGO LOS DATOS EN EL REGISTRO COMUNICACION
        //Cargo los datos de la Persona
        with Comunicacion.Persona do begin
            CodigoPersona   := iif(ObtenerDatosComunicacion.FieldByName('CodigoPersona').IsNull, -1, ObtenerDatosComunicacion.FieldByName('CodigoPersona').AsInteger);
            if ObtenerDatosComunicacion.FieldByName('Personeria').IsNull then
                 PersoneriaAux := ' '
            else PersoneriaAux := ObtenerDatosComunicacion.FieldByName('Personeria').AsString[1];
            //PersoneriaAux   := iif(ObtenerDatosComunicacion.FieldByName('Personeria').IsNull, '', ObtenerDatosComunicacion.FieldByName('Personeria').AsString[1]);
            Personeria      := PersoneriaAux;
            TipoDocumento   := Trim(ObtenerDatosComunicacion.FieldByName('CodigoDocumento').AsString);
            NumeroDocumento := Trim(ObtenerDatosComunicacion.FieldByName('NumeroDocumento').AsString);
        	Apellido        := Trim(ObtenerDatosComunicacion.FieldByName('Apellido').AsString);
            ApellidoMaterno := Trim(ObtenerDatosComunicacion.FieldByName('ApellidoMaterno').AsString);
        	Nombre          := Trim(ObtenerDatosComunicacion.FieldByName('Apellido').AsString);
    		//Sexo            := Trim(ObtenerDatosComunicacion.FieldByName('Sexo').AsString)[1];
            LugarNacimiento := '';
    		FechaNacimiento := nulldate;
        	CodigoActividad := -1;
        end;

        //Cargo los datos de la Comunicacion
        with Comunicacion do begin
            Domicilio       := Trim(ObtenerDatosComunicacion.FieldByName('Domicilio').AsString);
            NumeroCalle     := Trim(ObtenerDatosComunicacion.FieldByName('Numero').AsString);
            Telefono        := Trim(ObtenerDatosComunicacion.FieldByName('Telefono').AsString);
            Email           := Trim(ObtenerDatosComunicacion.FieldByName('Email').AsString);
        end;
    finally
        ObtenerDatosComunicacion.Close;
    end;
end;

procedure TFrameAltaOSMultiple.CargarDatosPersona(Persona: TDatosPersonales);
begin
    (*
    Si la persona a la que se le asocia la Orden de servicio ya la tenemos registrada
    mostramos todos sus datos y no le permitimos editar nada.
    Tambi�n puede ser que la comunicaci�n sea an�nima, asi que no se va a
    mostrar nada relacionado con la persona y se cargar� en el momento.
    *)
    //FMPersona.Inicializar('',TBP_PERSONAS);
    if Persona.CodigoPersona <= 0 then begin
        //Si no tiene codigo persona tomo los datos del contacto
        FMPersona.Inicializar(Persona);
    end
    else begin
        FMPersona.CargarDatosPersona(Persona.CodigoPersona);
        {
        ObtenerDatosPersona.Parameters.ParamByName('@CodigoPersona').Value := Persona.CodigoPersona;
        if not OpenTables([ObtenerDatosPersona]) then Exit;

        try
            with ObtenerDatosPersona do begin
                //Cargar Datos Personales
                CargarPersoneria(cbPersoneria, Trim(FieldByName('Personeria').AsString));
                cbPersoneria.OnChange(cbPersoneria);
                txt_Nombre.Text			    := Trim(FieldByName('Nombre').AsString);
                txt_Apellido.Text  		    := Trim(FieldByName('Apellido').AsString);
                txt_ApellidoMaterno.Text  	:= Trim(FieldByName('ApellidoMaterno').AsString);

                CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento, FieldByName('LugarNacimiento').AsString);
                dtFechaNacimiento.Date	    := FieldByName('FechaNacimiento').AsDateTime;

                if Trim(FieldByName('Personeria').AsString) = PERSONERIA_FISICA then
                    CargarSexos(cb_Sexo, Trim(FieldByName('Sexo').AsString))
                else if Trim(FieldByName('Personeria').AsString) = PERSONERIA_JURIDICA then
                    CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Trim(FieldByName('CodigoSituacionIVA').AsString));

                CargarTiposDocumento(DMConnections.BaseCAC, cbTipoNumeroDocumento, Trim(FieldByName('CodigoDocumento').AsString));

                cbTipoNumeroDocumento.MaskText	:= Trim(FieldByName('NumeroDocumento').AsString);

                // Cargamos los domicilios
                //CargarDomiciliosPersona(FieldByName('CodigoPersona').AsInteger, 0);

                // Cargamos los Medios de Comunicacion
                //CargarMediosComunicacionPersona( FieldByName('CodigoPersona').AsInteger, 0);
            end;
            ObtenerDatosPersona.Close;
        except
            on E: Exception do begin
                ObtenerDatosPersona.Close;
            end;
        end;
        }
    end;
    //Ahora inhabilitamos lo que corresponda.
end;

procedure TFrameAltaOSMultiple.cb_tipodebitoChange(Sender: TObject);
resourcestring
    CAPTION_TARJETA = 'Tarjeta:';
    CAPTION_BANCO   = 'Banco:';
begin
	if cb_tipodebito.ItemIndex = 0 then begin
		CargarTarjetasCredito(DMConnections.BaseCAC, cb_entidaddebito);
		lbl_entidad.Caption := CAPTION_TARJETA;
	end else begin
		CargarBancos(DMConnections.BaseCAC, cb_entidaddebito);
		lbl_entidad.Caption := CAPTION_BANCO;
	end;
end;

function TFrameAltaOSMultiple.Inicializar(Comunicacion: integer): Boolean;
begin
    Result := False;
    //Inicializa(CaptionLblTelefono: AnsiString; Obligatorio: boolean = false; PedirHora: boolean = true);
    FMTelefonoPrincipal.Inicializa(STR_TELEFONO_PRINCIPAL,true, true);
    FMTelefonoAlternativo.Inicializa(STR_TELEFONO_ALTERNATIVO, false, false);
    FMVehiculos.inicializar();
    //Obtengo los datos de la Comunicacion.
    CargarDatosComunicacion(FComunicacion);
    MedioContacto := -1; //Terminar;
    CargarDatosPersona(FComunicacion.Persona);
    Result := True;
end;

procedure TFrameAltaOSMultiple.btnAnteriorClick(Sender: TObject);
begin
	if pc_Adhesion.ActivePage = ts_DatosCliente then begin
		pc_Adhesion.ActivePage := ts_DatosCliente
	end else if pc_Adhesion.ActivePage = ts_Vehiculos then begin
		pc_Adhesion.ActivePage := ts_DatosCliente
	end else if pc_Adhesion.ActivePage = ts_FormaPago then begin
		pc_Adhesion.ActivePage := ts_Vehiculos
	end else if pc_Adhesion.ActivePage = ts_Contrato then begin
		pc_Adhesion.ActivePage := ts_FormaPago
    end else if pc_Adhesion.ActivePage = ts_Observaciones then begin
        pc_Adhesion.ActivePage := ts_Contrato
	end;
end;

procedure TFrameAltaOSMultiple.btnSiguienteClick(Sender: TObject);
begin
	if (pc_Adhesion.ActivePage = ts_DatosCliente) and (TabDatosClienteOk) then begin
		pc_Adhesion.ActivePage := ts_Vehiculos
	end else if (PageControl.ActivePage = ts_Vehiculos) and (TabDatosVehiculosOk) then begin
		PageControl.ActivePage := ts_FormaPago
	end else if (PageControl.ActivePage = ts_FormaPago) and (TabFormaPagoOk) then begin
		PageControl.ActivePage := ts_Contrato
	end else if (PageControl.ActivePage = ts_Contrato) and (TabDatosContratoOk) then begin
		PageControl.ActivePage := ts_Observaciones
	end;
end;

procedure TFrameAltaOSMultiple.btnBorrarDomicilioParticularClick(Sender: TObject);
resourcestring
    ES_DOMICILIO_ENTREGA = 'Atenci�n: El domicilio que selecciona para eliminar es el Domicilio de entrega ' + #13#10 +
						   'Tendr� que indicar otro domicilio para entrega. �Esta seguro?';
    CAPTION_ELIMINAR_DOMICILIO = 'Eliminar domicilio';
    CAPTION_ALTA = 'Alta';
    CAPTION_MODIFICAR = 'Modificar';
begin
    //Si no es el domicilio de entrega
    if (CodigoDomicilioParticular < 0) then begin
        //Si no esta dada de alta la direccion, la limpio.
        LblDomicilioParticular.Caption := FALTA_INGRESAR_DOMICILIO;
        BtnDomicilioParticularAccion.Caption := CAPTION_ALTA;
        LimpiarRegistroDomicilio(DomicilioParticular, TIPO_DOMICILIO_PARTICULAR);
    end
    else begin
        // Es un domicilio existente en la base. Lo marco para borrar
        // y luego lo doy de baja (mientras no sea el domicilio de entrega)

        BajaDomicilioParticular := true;
    end;
end;

procedure TFrameAltaOSMultiple.GBDomiciliosClick(Sender: TObject);
begin
    LblDomicilioParticular.Caption := FALTA_INGRESAR_DOMICILIO;
end;

procedure TFrameAltaOSMultiple.InicializaClientDataSet(
  var CDS: TClientDataSet);
begin
    CDS.CreateDataSet;
    CDS.Open;
end;

function TFrameAltaOSMultiple.GetApellido: String;
begin
    Result := FMPersona.Apellido;
end;

function TFrameAltaOSMultiple.GetApellidoContacto: String;
begin
    Result := FMPersona.ApellidoContacto;
end;

function TFrameAltaOSMultiple.GetApellidoMaterno: String;
begin
    Result := FMPersona.ApellidoMaterno;
end;

function TFrameAltaOSMultiple.GetApellidoMaternoContacto: String;
begin
    result := FMPersona.ApellidoMaternoContacto;
end;

function TFrameAltaOSMultiple.GetCodigoActividad: Integer;
begin
    result := FMPersona.CodigoActividad;
end;

function TFrameAltaOSMultiple.GetCodigoPaisOrigen: String;
begin
    result := FMPersona.CodigoPais;
end;

function TFrameAltaOSMultiple.GetCodigoSituacionIVA: String;
begin
    Result := FMPersona.CodigoSituacionIVA;
end;

function TFrameAltaOSMultiple.GetEmail: String;
begin
    Result := FMPersona.Email;
end;

function TFrameAltaOSMultiple.GetFechaCreacion: TDateTime;
begin
    result := FMPersona.FechaCreacion;
end;

function TFrameAltaOSMultiple.GetFechaNacimiento: TDateTime;
begin
    Result := FMPersona.FechaNacimiento;
end;

function TFrameAltaOSMultiple.GetNombre: String;
begin
    Result := FMPersona.Nombre;
end;

function TFrameAltaOSMultiple.GetNombreContacto: String;
begin
    Result := FMPersona.NombreContacto;
end;

function TFrameAltaOSMultiple.GetPersoneria: AnsiString;
begin
    result := FMPersona.Personeria;
end;

function TFrameAltaOSMultiple.GetRazonSocial: String;
begin
    result := FMPersona.RazonSocial;
end;

function TFrameAltaOSMultiple.GetRut: String;
begin
    result := FMPersona.RUT;
end;

function TFrameAltaOSMultiple.GetSexo: String;
begin
    result := FMPersona.Sexo;
end;

function TFrameAltaOSMultiple.GetSexoContacto: String;
begin
    Result := FMPersona.SexoContacto;
end;

function TFrameAltaOSMultiple.TabDatosClienteOk: boolean;
begin
    //validar los datos de la persona,
    //si tiene codigo en realidad no validar nada porque no puede modificar
    result := FMPersona.Validar and
              FMTelefonoPrincipal.ValidarTelefono and
              FMTelefonoAlternativo.ValidarTelefono;
end;

function TFrameAltaOSMultiple.TabDatosContratoOk: boolean;
begin
    result := true;
end;

function TFrameAltaOSMultiple.TabDatosVehiculosOk: boolean;
begin
    result := true;
end;

function TFrameAltaOSMultiple.TabFormaPagoOk: boolean;
begin
    result := true;
end;

function TFrameAltaOSMultiple.TabObservacionesOk: boolean;
begin
    result := true;
end;

procedure TFrameAltaOSMultiple.PageControlGetImageIndex(Sender: TObject;
  TabIndex: Integer; var ImageIndex: Integer);
begin
    ImageIndex := -1;
end;

end.
