unit frmDMGuardarConvenios;

interface

uses
  SysUtils, Classes, ADODB, DMConnection, DB, DateUtils, Variants, Util, PeaProcs, PeaTypes, RStrings;  //TASK_076_JMA_20161121

type
//  TDMGuardarConvenios = class(TDataModule)                                       //TASK_076_JMA_20161121
    TDMGuardarConvenios = class //TASK_076_JMA_20161121
    spActualizarCuentaAPerdido: TADOStoredProc;
    spBajaCuentaxCambioVehiculo: TADOStoredProc;
    spActivarCuenta: TADOStoredProc;
    spVehiculos: TADOStoredProc;
    spCuentas: TADOStoredProc;
    spBajaCuenta: TADOStoredProc;
    spSuspenderCuenta: TADOStoredProc;
    spActualizarTurnoMovimientoEntrega: TADOStoredProc;
    spActualizarTurnoMovimientoDevolucion: TADOStoredProc;
    spGenerarMovimientoCuentaConTelevia: TADOStoredProc;
    spRegistrarTAGVendido: TADOStoredProc;
    spHabilitarListaBlanca: TADOStoredProc;



  private
    { Private declarations }
  public
    { Public declarations }
{INICIO: TASK_076_JMA_20161121}
    //constructor Create();

    procedure BajarCuentaxCambioVehiculo(CodigoConvenio, IndiceVehiculo: Integer; ContextMark: SmallInt; ContractSerialNumber: Int64;
                                                FechaBajaCuenta, FechaCreacion, FechaSuspendida: TDateTime; UsuarioSistema: string );

    procedure BajarCuenta(CodigoConvenio, IndiceVehiculo: Integer; ContextMark: SmallInt; ContractSerialNumber: Int64;
                                   CodigoPuntoEntrega: Integer; TagEnCliente, EsTagVendido, EsTagPerdido, EsTagRecuperado: Boolean; CodigoEstadoConservacion, CodigoMotivoBaja: Integer;
                                    FechaBajaCuenta, FechaCreacion, FechaSuspendida: TDateTime; UsuarioSistema: string );

    procedure RegistrarTAGVendido(CodigoConvenio: Integer; ContractSerialNumber: Int64);

    procedure ActualizarTurnoMovimientoDevolucion(NumeroTurno, CodigoConvenio, CodigoTipoVehiculo: Integer; CuentaTieneAcoplado: boolean);

    function ActualizarMaestroVehiculos(CodigoTipoPatente, Patente, DigitoPatente: string; CodigoMarca: Integer; Modelo, Anio: string; CodigoColor: Integer; //TASK_106_JMA_20170206
                            //CodigoTipoVehiculo: Integer;                                                                                                  //TASK_106_JMA_20170206
                            CuentaTieneAcoplado: Boolean;                                                                                                   //TASK_106_JMA_20170206
                            //CodigoCategoriaVehiculo: Integer;                                                                                             //TASK_106_JMA_20170206
                            DigitoVerificadorValido: Boolean;                                                                                               //TASK_106_JMA_20170206
                            //PatenteRVM, DigitoPatenteRVM: string;                                                                                         //TASK_106_JMA_20170206
                            Robado, Recuperado: Boolean; CodigoVehiculo: Integer): Integer;                                                                 //TASK_106_JMA_20170206

    procedure ActivarCuenta(CodigoConvenio, CodigoConvenioRNUT,IndiceVehiculo: Integer; FechaBajaCuenta, FechaSuspendida:TDateTime; UsuarioSistema: string);

    procedure SuspenderCuenta(CodigoConvenio, IndiceVehiculo: Integer; FechaSuspendida, DeadlineSuspension:TDateTime; SuspensionForzada: Boolean; UsuarioSistema: string);

    procedure ActualizarCuentaAPerdido(CodigoConvenio, IndiceVehiculo: Integer; EsTagRecuperado: Boolean; UsuarioSistema: string);

    procedure ActualizarTurnoMovimientoEntrega(NumeroTurno, CodigoConvenio: Integer; //TASK_106_JMA_20170206
                                                //CodigoTipoVehiculo: Integer;      //TASK_106_JMA_20170206
                                                CodigoVehiculo: Integer;            //TASK_106_JMA_20170206
                                                CuentaTieneAcoplado: Boolean);      //TASK_106_JMA_20170206

    procedure GenerarMovimientoCuentaConTelevia(CodigoConvenio, IndiceVehiculo, OtroConcepto, ConceptoMail, ConceptoPostal: Integer;
                                                ConvenioTieneMedioEnvioPorEMail: Boolean; DescripcionMotivo: string;
                                                ContactSerialNumber: Int64; ContextMark: SmallInt;
                                                AlmacenDestino: Integer; TipoAsignacionTag: string; NumeroVoucher: Integer; UsuarioSistema: string);

    function ActualizarCuenta(CodigoConvenio, NumeroTurno, PuntoEntrega: Integer;
                              EsActivacionCuenta, EsVehiculoModificado, EsVehiculoNuevo, EsCambioVehiculo, EsTagNuevo, EsTagVendido, EsTagPerdido, EsTagRecuperado: Boolean;
                              Cuenta: TCuentaVehiculo): Integer;
{TERMINO: TASK_076_JMA_20161121}
  end;


//var               //TASK_076_JMA_20161121

implementation

                    //TASK_076_JMA_20161121

{INICIO: TASK_076_JMA_20161121}

procedure TDMGuardarConvenios.BajarCuentaxCambioVehiculo(CodigoConvenio, IndiceVehiculo: Integer; ContextMark: SmallInt; ContractSerialNumber: Int64;
                                                                FechaBajaCuenta, FechaCreacion, FechaSuspendida: TDateTime; UsuarioSistema: string );
begin
    try

        //if NOT Assigned(spBajaCuentaxCambioVehiculo) then                                      //TASK_108_JMA_20170214
        //begin                                                                                  //TASK_108_JMA_20170214
        spBajaCuentaxCambioVehiculo := TADOStoredProc.Create(nil);
        spBajaCuentaxCambioVehiculo.Connection := DMConnections.BaseCAC;
        spBajaCuentaxCambioVehiculo.ProcedureName := 'BajaCuentaxCambioVehiculo';
        //end;                                                                                   //TASK_108_JMA_20170214

        // spBajaCuentaxCambioVehiculo.Refresh;                                                                     //TASK_023_GLE_20161212 Comenta
        spBajaCuentaxCambioVehiculo.Parameters.Refresh;                                                             //TASK_023_GLE_20161212 Corrige
        spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@ContextMark').Value := ContextMark;
        spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@ContractSerialNumber').Value := ContractSerialNumber;
        //Si la fecha de baja de la misma cuenta me va a quedar menor que la de alta, la tengo que corregir.
        if FechaBajaCuenta <= FechaCreacion then
            FechaBajaCuenta := incMinute(FechaCreacion, 1);
        //Si la fecha de baja queda menor a la de suspensión en la misma cuenta se traslapará en el
        //histórico de Tags, por lo tanto la baja tiene que ser mayor a su suspensión
        if FechaBajaCuenta <= FechaSuspendida then
            FechaBajaCuenta := incMinute(FechaSuspendida, 1);
        spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@FechaBajaCuenta').Value := FechaBajaCuenta;
        spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spBajaCuentaxCambioVehiculo.ExecProc;

        // if (spBajaCuenta.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then                                //TASK_023_GLE_20161212 Comenta
        // raise Exception.Create(spBajaCuenta.Parameters.ParamByName('@ErrorDescription').Value);                  //TASK_023_GLE_20161212 Comenta
        if (spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then                    //TASK_023_GLE_20161212 Corrige
            raise Exception.Create(spBajaCuentaxCambioVehiculo.Parameters.ParamByName('@ErrorDescription').Value);  //TASK_023_GLE_20161212 Corrige
    finally
        if Assigned(spBajaCuentaxCambioVehiculo) and spBajaCuentaxCambioVehiculo.Active then
            spBajaCuentaxCambioVehiculo.close;
        FreeAndNil(spBajaCuentaxCambioVehiculo);            //TASK_108_JMA_20170214
    end;

end;

procedure TDMGuardarConvenios.BajarCuenta(CodigoConvenio, IndiceVehiculo: Integer; ContextMark: SmallInt; ContractSerialNumber: Int64;
                                   CodigoPuntoEntrega: Integer; TagEnCliente, EsTagVendido, EsTagPerdido, EsTagRecuperado: Boolean; CodigoEstadoConservacion, CodigoMotivoBaja: Integer;
                                    FechaBajaCuenta, FechaCreacion, FechaSuspendida: TDateTime; UsuarioSistema: string );
begin

    try
        //if not Assigned(spBajaCuenta) then                                        //TASK_108_JMA_20170214
        //begin                                                                     //TASK_108_JMA_20170214
            spBajaCuenta:= TADOStoredProc.Create(nil);
            spBajaCuenta.Connection := DMConnections.BaseCAC;
            spBajaCuenta.ProcedureName := 'BajaCuenta';
        //end;                                                                      //TASK_108_JMA_20170214

        spBajaCuenta.Parameters.Refresh;
        spBajaCuenta.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spBajaCuenta.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        spBajaCuenta.Parameters.ParamByName('@ContextMark').Value := ContextMark;
        spBajaCuenta.Parameters.ParamByName('@ContractSerialNumber').Value := ContractSerialNumber;
        spBajaCuenta.Parameters.ParamByName('@CodigoPuntoEntrega').Value := CodigoPuntoEntrega;
        spBajaCuenta.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spBajaCuenta.Parameters.ParamByName('@TagEnCliente').Value := TagEnCliente;
        spBajaCuenta.Parameters.ParamByName('@TagVendido').Value := EsTagVendido;

        //esto marca la diferencia entre pasarlo a devuelto o extraviado.
        if EsTagPerdido then
            spBajaCuenta.Parameters.ParamByName('@TagEnCliente').Value := EsTagPerdido;

        if EsTagRecuperado then
            spBajaCuenta.Parameters.ParamByName('@CodigoEstadoConservacion').Value := CodigoEstadoConservacion
        else
            spBajaCuenta.Parameters.ParamByName('@CodigoEstadoConservacion').Value := iif((CodigoEstadoConservacion = 0) or (CodigoEstadoConservacion = null), null, CodigoEstadoConservacion);

        //Si la fecha de baja de la misma cuenta me va a quedar menor que la de alta, la tengo que corregir.
        if FechaBajaCuenta <= FechaCreacion then
            FechaBajaCuenta := incMinute(FechaCreacion, 1);

        //Si la fecha de baja queda menor a la de suspensión en la misma cuenta se traslapará en el
        //histórico de Tags, por lo tanto la baja tiene que ser mayor a su suspensión
        if FechaBajaCuenta <= FechaSuspendida then
            FechaBajaCuenta := incMinute(FechaSuspendida, 1);

        spBajaCuenta.Parameters.ParamByName('@FechaBajaCuenta').Value := FechaBajaCuenta;
        spBajaCuenta.Parameters.ParamByName('@DescError').Value := null;

        spBajaCuenta.Parameters.ParamByName('@CodigoMotivoBaja').Value := CodigoMotivoBaja;
        spBajaCuenta.ExecProc;

        if (spBajaCuenta.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spBajaCuenta.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spBajaCuenta) and spBajaCuenta.Active then
            spBajaCuenta.close;
        FreeAndNil(spBajaCuenta);       //TASK_108_JMA_20170214
    end;
end;

procedure TDMGuardarConvenios.RegistrarTAGVendido(CodigoConvenio: Integer; ContractSerialNumber: Int64);
begin
    try
        //if not Assigned(spRegistrarTAGVendido) then                                     //TASK_108_JMA_20170214
        //begin                                                                           //TASK_108_JMA_20170214
        spRegistrarTAGVendido:= TADOStoredProc.Create(nil);
        spRegistrarTAGVendido.Connection := DMConnections.BaseCAC;
        spRegistrarTAGVendido.ProcedureName := 'RegistrarTAGVendido';
        //end;                                                                           //TASK_108_JMA_20170214

        spRegistrarTAGVendido.Parameters.Refresh;
        spRegistrarTAGVendido.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spRegistrarTAGVendido.Parameters.ParamByName('@ContractSerialNumber').Value := ContractSerialNumber;
        spRegistrarTAGVendido.ExecProc;

        if (spRegistrarTAGVendido.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spRegistrarTAGVendido.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spRegistrarTAGVendido) and spRegistrarTAGVendido.Active then
            spRegistrarTAGVendido.close;
        FreeAndNil(spRegistrarTAGVendido);          //TASK_108_JMA_20170214
    end;
end;

procedure TDMGuardarConvenios.ActualizarTurnoMovimientoDevolucion(NumeroTurno, CodigoConvenio, CodigoTipoVehiculo: Integer; CuentaTieneAcoplado: boolean);
begin
    try
        //if not Assigned(spActualizarTurnoMovimientoDevolucion) then                              //TASK_108_JMA_20170214
        //begin                                                                                    //TASK_108_JMA_20170214
        spActualizarTurnoMovimientoDevolucion:= TADOStoredProc.Create(nil);
        spActualizarTurnoMovimientoDevolucion.Connection := DMConnections.BaseCAC;
        spActualizarTurnoMovimientoDevolucion.ProcedureName := 'ActualizarTurnoMovimientoDevolucion';
        //end;                                                                                     //TASK_108_JMA_20170214

        spActualizarTurnoMovimientoDevolucion.Parameters.Refresh;
        spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@NumeroTurno').Value:= NumeroTurno;
        spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@CodigoTipoVehiculo').Value:= CodigoTipoVehiculo;
        spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@TieneAcoplado').Value:= CuentaTieneAcoplado;
        spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@CodigoConvenio').Value:= CodigoConvenio;

        spActualizarTurnoMovimientoDevolucion.ExecProc;

        if (spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spActualizarTurnoMovimientoDevolucion.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spActualizarTurnoMovimientoDevolucion) and spActualizarTurnoMovimientoDevolucion.Active then
            spActualizarTurnoMovimientoDevolucion.close;
        FreeAndNil(spActualizarTurnoMovimientoDevolucion);  //TASK_108_JMA_20170214
    end;
end;

function TDMGuardarConvenios.ActualizarMaestroVehiculos(CodigoTipoPatente, Patente, DigitoPatente: string; CodigoMarca: Integer; Modelo, Anio: string; CodigoColor: Integer; 	//TASK_106_JMA_20170206
                                            //CodigoTipoVehiculo: Integer;                                                                  									//TASK_106_JMA_20170206
                                            CuentaTieneAcoplado: Boolean;                                                                   									//TASK_106_JMA_20170206
                                            //CodigoCategoriaVehiculo: Integer;                                                             									//TASK_106_JMA_20170206
                                            DigitoVerificadorValido: Boolean;																									//TASK_106_JMA_20170206
                                            //PatenteRVM, DigitoPatenteRVM: string;     																						//TASK_106_JMA_20170206
                                            Robado, Recuperado: Boolean; CodigoVehiculo: Integer): Integer;     																//TASK_106_JMA_20170206
begin

    try
        Modelo :=  trim(Modelo);
        //PatenteRVM := Trim(PatenteRVM);                                                                                                 //TASK_106_JMA_20170206
        //DigitoPatenteRVM := Trim(DigitoPatenteRVM);                                                                                     //TASK_106_JMA_20170206

        //if not Assigned(spVehiculos) then                                                      //TASK_108_JMA_20170214
        //begin                                                                                  //TASK_108_JMA_20170214
        spVehiculos:= TADOStoredProc.Create(nil);
        spVehiculos.Connection := DMConnections.BaseCAC;
        spVehiculos.CursorType := ctDynamic;
        spVehiculos.ProcedureName := 'ActualizarMaestroVehiculos';
        //end;                                                                                  //TASK_108_JMA_20170214

        spVehiculos.Parameters.Refresh;       
        spVehiculos.Parameters.ParamByName('@CodigoTipoPatente').Value := CodigoTipoPatente;
        spVehiculos.Parameters.ParamByName('@Patente').Value           := Patente;
        spVehiculos.Parameters.ParamByName('@DigitoVerificadorPatente').Value := DigitoPatente;
        spVehiculos.Parameters.ParamByName('@CodigoMarca').Value       := CodigoMarca;
        spVehiculos.Parameters.ParamByName('@Modelo').Value            := iif(Modelo='',null,Modelo);
        spVehiculos.Parameters.ParamByName('@AnioVehiculo').Value      := Anio;
        spVehiculos.Parameters.ParamByName('@CodigoColor').Value       := CodigoColor;
        //spVehiculos.Parameters.ParamByName('@CodigoTipoVehiculo').Value:= CodigoTipoVehiculo;                                             //TASK_106_JMA_20170206
        spVehiculos.Parameters.ParamByName('@TieneAcoplado').Value     := CuentaTieneAcoplado;
        //spVehiculos.Parameters.ParamByName('@CodigoCategoria').Value   := CodigoCategoriaVehiculo;                                        //TASK_106_JMA_20170206
        spVehiculos.Parameters.ParamByName('@DigitoVerificadorValido').Value := DigitoVerificadorValido;
        //spVehiculos.Parameters.ParamByName('@PatenteRVM').Value        := iif(PatenteRVM = '', Null, PatenteRVM);                         //TASK_106_JMA_20170206
        //spVehiculos.Parameters.ParamByName('@DigitoVerificadorPatenteRVM').Value := iif(DigitoPatenteRVM = '', Null, DigitoPatenteRVM);   //TASK_106_JMA_20170206
        spVehiculos.Parameters.ParamByName('@CodigoVehiculo').Value := CodigoVehiculo;

        if Robado and Recuperado then
            spVehiculos.Parameters.ParamByName('@Robado').Value := not Robado
        else
            spVehiculos.Parameters.ParamByName('@Robado').Value := Robado;

        spVehiculos.ExecProc;

        if (spVehiculos.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spVehiculos.Parameters.ParamByName('@ErrorDescription').Value)
        else
            Result := spVehiculos.Parameters.ParamByName('@CodigoVehiculo').Value;
    finally
          if Assigned(spVehiculos) and spVehiculos.Active then
            spVehiculos.close;
          FreeAndNil(spVehiculos);      //TASK_108_JMA_20170214
    end;

end;

procedure TDMGuardarConvenios.SuspenderCuenta(CodigoConvenio, IndiceVehiculo: Integer; FechaSuspendida, DeadlineSuspension:TDateTime; SuspensionForzada: Boolean; UsuarioSistema: string);
begin
    try
        //if not Assigned(spSuspenderCuenta) then                                                    //TASK_108_JMA_20170214
        //begin                                                                                      //TASK_108_JMA_20170214
        spSuspenderCuenta:= TADOStoredProc.Create(nil);
        spSuspenderCuenta.Connection := DMConnections.BaseCAC;
        spSuspenderCuenta.ProcedureName := 'SuspenderCuenta';
        //end;                                                                                       //TASK_108_JMA_20170214

        spSuspenderCuenta.Parameters.Refresh;
        spSuspenderCuenta.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spSuspenderCuenta.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        spSuspenderCuenta.Parameters.ParamByName('@Suspendida').Value := FechaSuspendida;
        spSuspenderCuenta.Parameters.ParamByName('@DeadlineSuspension').Value := iif( DeadlineSuspension = NullDate, null, DeadlineSuspension);
        spSuspenderCuenta.Parameters.ParamByName('@SuspensionForzada').Value := SuspensionForzada;
        spSuspenderCuenta.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spSuspenderCuenta.ExecProc;

        if (spSuspenderCuenta.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spSuspenderCuenta.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spSuspenderCuenta) and spSuspenderCuenta.Active then
            spSuspenderCuenta.close;
        FreeAndNil(spSuspenderCuenta);      //TASK_108_JMA_20170214
    end;
end;

procedure TDMGuardarConvenios.ActivarCuenta(CodigoConvenio, CodigoConvenioRNUT,IndiceVehiculo: Integer; FechaBajaCuenta, FechaSuspendida:TDateTime; UsuarioSistema: string);
begin
    try
        //if not Assigned(spActivarCuenta) then                                                      //TASK_108_JMA_20170214
        //begin                                                                                      //TASK_108_JMA_20170214
            spActivarCuenta:= TADOStoredProc.Create(nil);
            spActivarCuenta.Connection := DMConnections.BaseCAC;
            spActivarCuenta.CursorType := ctDynamic;
            spActivarCuenta.ProcedureName := 'ActivarCuenta';
        //end;                                                                                       //TASK_108_JMA_20170214

        spActivarCuenta.Parameters.Refresh;
        spActivarCuenta.Parameters.ParamByName('@CodigoConvenio').Value := iif(CodigoConvenioRNUT=0, CodigoConvenio, CodigoConvenioRNUT);
        spActivarCuenta.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        spActivarCuenta.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        FechaBajaCuenta := NowBaseSmall(DMConnections.BaseCAC);
        if FechaBajaCuenta <= FechaSuspendida then
            FechaBajaCuenta := incMinute(FechaSuspendida, 1);
        spActivarCuenta.Parameters.ParamByName('@FechaBajaCuenta').Value := FechaBajaCuenta;
        spActivarCuenta.ExecProc;

        if (spActivarCuenta.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spActivarCuenta.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spActivarCuenta) and spActivarCuenta.Active then
            spActivarCuenta.close;
        FreeAndNil(spActivarCuenta);        //TASK_108_JMA_20170214
    end;
end;

procedure TDMGuardarConvenios.ActualizarCuentaAPerdido(CodigoConvenio, IndiceVehiculo: Integer; EsTagRecuperado: Boolean; UsuarioSistema: string);
begin
    try
        //if not Assigned(spActualizarCuentaAPerdido) then                                        //TASK_108_JMA_20170214
        //begin                                                                                   //TASK_108_JMA_20170214
        spActualizarCuentaAPerdido := TADOStoredProc.Create(nil);
        spActualizarCuentaAPerdido.Connection := DMConnections.BaseCAC;
        spActualizarCuentaAPerdido.CursorType := ctDynamic;
        spActualizarCuentaAPerdido.ProcedureName := 'ActualizarCuentaAPerdido';
        //end;                                                                                   //TASK_108_JMA_20170214

        spActualizarCuentaAPerdido.Parameters.Refresh;
        spActualizarCuentaAPerdido.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spActualizarCuentaAPerdido.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        spActualizarCuentaAPerdido.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spActualizarCuentaAPerdido.Parameters.ParamByName('@Recupero').Value := EsTagRecuperado;
        spActualizarCuentaAPerdido.ExecProc;

        if (spActualizarCuentaAPerdido.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spActualizarCuentaAPerdido.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spActualizarCuentaAPerdido) and spActualizarCuentaAPerdido.Active then
            spActualizarCuentaAPerdido.close;
        FreeAndNil(spActualizarCuentaAPerdido);     //TASK_108_JMA_20170214
    end;
end;

procedure TDMGuardarConvenios.ActualizarTurnoMovimientoEntrega(NumeroTurno, CodigoConvenio:Integer; //TASK_106_JMA_20170206
        //CodigoTipoVehiculo: Integer;  //TASK_106_JMA_20170206
        CodigoVehiculo: Integer;        //TASK_106_JMA_20170206
        CuentaTieneAcoplado: Boolean);  //TASK_106_JMA_20170206
begin
    try
        //if not Assigned(spActualizarTurnoMovimientoEntrega) then               //TASK_108_JMA_20170214
        //begin                                                                  //TASK_108_JMA_20170214
        spActualizarTurnoMovimientoEntrega:= TADOStoredProc.Create(nil);
        spActualizarTurnoMovimientoEntrega.Connection := DMConnections.BaseCAC;
        spActualizarTurnoMovimientoEntrega.ProcedureName := 'ActualizarTurnoMovimientoEntrega';
        //end;                                                                   //TASK_108_JMA_20170214

        spActualizarTurnoMovimientoEntrega.Parameters.Refresh;
        spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@NumeroTurno').Value:= NumeroTurno;
        //spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@CodigoTipoVehiculo').Value:= CodigoTipoVehiculo;  //TASK_106_JMA_20170206
        spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@CodigoVehiculo').Value:= CodigoVehiculo;  //TASK_106_JMA_20170206
        spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@TieneAcoplado').Value:= CuentaTieneAcoplado;
        spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
        spActualizarTurnoMovimientoEntrega.ExecProc;

        if (spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spActualizarTurnoMovimientoEntrega.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spActualizarTurnoMovimientoEntrega) and spActualizarTurnoMovimientoEntrega.Active then
            spActualizarTurnoMovimientoEntrega.close;
        //spActualizarTurnoMovimientoEntrega.Free;                        //TASK_106_JMA_20170206            //TASK_108_JMA_20170214
        FreeAndNil(spActualizarTurnoMovimientoEntrega);                                                      //TASK_108_JMA_20170214
    end;
end;

procedure TDMGuardarConvenios.GenerarMovimientoCuentaConTelevia(CodigoConvenio, IndiceVehiculo, OtroConcepto, ConceptoMail, ConceptoPostal: Integer;
                                                                ConvenioTieneMedioEnvioPorEMail: Boolean; DescripcionMotivo: string;
                                                                ContactSerialNumber: Int64; ContextMark: SmallInt;
                                                                AlmacenDestino: Integer; TipoAsignacionTag: string; NumeroVoucher: Integer; UsuarioSistema: string);
begin
    try
        //if not Assigned(spGenerarMovimientoCuentaConTelevia) then                                //TASK_108_JMA_20170214
        //begin                                                                                    //TASK_108_JMA_20170214
        spGenerarMovimientoCuentaConTelevia:= TADOStoredProc.Create(nil);
        spGenerarMovimientoCuentaConTelevia.Connection := DMConnections.BaseCAC;
        spGenerarMovimientoCuentaConTelevia.ProcedureName := 'GenerarMovimientoCuentaConTelevia';
        //end;                                                                                     //TASK_108_JMA_20170214

        spGenerarMovimientoCuentaConTelevia.Parameters.Refresh;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@FechaHora').Value := NowBase(DMConnections.BaseCAC);
        if (OtroConcepto = ConceptoMail) or (OtroConcepto = ConceptoPostal) then begin
            if ConvenioTieneMedioEnvioPorEMail then
                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := ConceptoMail
            else
                spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := ConceptoPostal;
        end
        else
            spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoConcepto').Value := OtroConcepto;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@DescripcionMotivo').Value := DescripcionMotivo;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@ContractSerialNumber').Value := ContactSerialNumber;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@ContextMark').Value := ContextMark;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@NumeroMovimiento').Value := Null;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@CodigoAlmacen').Value := AlmacenDestino;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@TipoAsignacionTag').Value := TipoAsignacionTag;
        spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@Observaciones').Value := IntToStr(NumeroVoucher);
        spGenerarMovimientoCuentaConTelevia.ExecProc;

        if (spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@RETURN_VALUE').Value = -1) then
            raise Exception.Create(spGenerarMovimientoCuentaConTelevia.Parameters.ParamByName('@ErrorDescription').Value);
    finally
        if Assigned(spGenerarMovimientoCuentaConTelevia) and spGenerarMovimientoCuentaConTelevia.Active then
            spGenerarMovimientoCuentaConTelevia.close;
        FreeAndNil(spGenerarMovimientoCuentaConTelevia);            //TASK_108_JMA_20170214
    end;
end;

function TDMGuardarConvenios.ActualizarCuenta(CodigoConvenio, NumeroTurno, PuntoEntrega: Integer;
                                              EsActivacionCuenta, EsVehiculoModificado, EsVehiculoNuevo, EsCambioVehiculo, EsTagNuevo, EsTagVendido, EsTagPerdido, EsTagRecuperado: Boolean;
                                              Cuenta: TCuentaVehiculo): Integer;
var
    j: Integer;
begin
    try

        //if not Assigned(spCuentas) then                                                     //TASK_108_JMA_20170214
        //begin                                                                               //TASK_108_JMA_20170214
        spCuentas := TADOStoredProc.Create(nil);
        spCuentas.Connection := DMConnections.BaseCAC;
        spCuentas.ProcedureName := 'ActualizarCuentaVehiculo';                                //TASK_108_JMA_20170214
        //end;                                                                                //TASK_108_JMA_20170214

        spCuentas.Parameters.Refresh;
        spCuentas.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        //Solo pasamos el indice en caso de una modificacion. En cualquier otro caso null para que cree una nueva cuenta

        if EsVehiculoModificado and not (EsVehiculoNuevo or EsTagNuevo or EsTagVendido or EsCambioVehiculo or EsActivacionCuenta or EsTagPerdido or EsTagRecuperado) and not (Cuenta.EstaSuspendida) then
            spCuentas.Parameters.ParamByName('@IndiceVehiculo').Value := Cuenta.IndiceVehiculo
        else begin
            // Alta de vehiculo
            if (not EsCambioVehiculo) and (NumeroTurno < 1) then
                    raise Exception.Create(MSG_CAPTION_ESTADO_TURNO_CERRADO);
            spCuentas.Parameters.ParamByName('@IndiceVehiculo').Value := Null;
        end;
        spCuentas.Parameters.ParamByName('@CodigoVehiculo').Value := Cuenta.Vehiculo.CodigoVehiculo;
        spCuentas.Parameters.ParamByName('@DetallePasadas').Value := 0;
{INICIO: TASK_106_JMA_20170206
        spCuentas.Parameters.ParamByName('@CodigoCategoria').Value:= ObtenerCategoriaVehiculo(DMConnections.BaseCAC, Cuenta.Vehiculo.CodigoTipo);
}
        spCuentas.Parameters.ParamByName('@CodigoCategoria').Value:= Cuenta.CodigoCategoriaInterurbana;
        spCuentas.Parameters.ParamByName('@CodigoCategoriaUrbana').Value:= Cuenta.CodigoCategoriaUrbana;
{TERMINO: TASK_106_JMA_20170206}
        spCuentas.Parameters.ParamByName('@CentroCosto').Value := null;
        spCuentas.Parameters.ParamByName('@FechaAltaCuenta').Value := Cuenta.FechaCreacion;
        spCuentas.Parameters.ParamByName('@FechaBajaCuenta').Value := null;
        spCuentas.Parameters.ParamByName('@CodigoEstadoCuenta').Value := ESTADO_CUENTA_VEHICULO_ACTIVO;
        spCuentas.Parameters.ParamByName('@Suspendida').Value := NULL;
        spCuentas.Parameters.ParamByName('@DeadlineSuspension').Value := NULL;
        spCuentas.Parameters.ParamByName('@SuspensionForzada').Value := False;
        spCuentas.Parameters.ParamByName('@ContextMark').Value := Cuenta.ContextMark;
        if (Cuenta.ContactSerialNumber <> 0) then
            spCuentas.Parameters.ParamByName('@ContractSerialNumber').Value := Cuenta.ContactSerialNumber
        else //Esto es para que haga solo las bonificaciones en televías de terceros y no error haciendo un update en cuentas
            spCuentas.Parameters.ParamByName('@ContractSerialNumber').Value := Cuenta.ContactSerialNumberOtraConcesionaria;

        if not (EsActivacionCuenta) and not (EsCambioVehiculo) then
            Cuenta.FechaAltaTag := Cuenta.FechaCreacion;
        // spCuentas.Parameters.ParamByName('@FechaAltaTAG').Value := Cuenta.FechaAltaTag;                    //TASK_023_GLE_20161212 Comenta
        spCuentas.Parameters.ParamByName('@FechaAltaTAG').Value := Cuenta.FechaCreacion;                      //TASK_023_GLE_20161212 Corrige
        spCuentas.Parameters.ParamByName('@FechaBajaTAG').Value := iif((Cuenta.FechaBajaTag = NullDate) or (Cuenta.FechaBajaTag = 0), Null, Cuenta.FechaBajaTag);

        //Si a pesar de todo es Nulldate y es de terceros, lo dejo en Null porque no se actualiza.
        if (Cuenta.ContactSerialNumber = 0) and (Cuenta.ContactSerialNumberOtraConcesionaria <> 0) then
            spCuentas.Parameters.ParamByName('@FechaVencimientoTAG').Value := Null
        else begin
            //Si modifique la fecha de alta (alta retroactiva), la garantia del televia se mantiene.
            //Si es una transferencia de televia tambien se mantiene. En caso contrario la recalculo por el else.
            spCuentas.Parameters.ParamByName('@FechaVencimientoTAG').Value := Cuenta.FechaVencimientoTag;
        end;
        spCuentas.Parameters.ParamByName('@CodigoPuntoEntrega').Value:= PuntoEntrega;
        spCuentas.Parameters.ParamByName('@Usuario').Value:=UsuarioSistema;
        spCuentas.Parameters.ParamByName('@ResponsableInstalacion').Value := 1; //Atencion va el responsable tomarlo de algun lado
        spCuentas.Parameters.ParamByName('@CambiodeVehiculo').Value := EsCambioVehiculo;

(* Cargar los parámetros para la Eximición de Facturación. *)
        if (Cuenta.EximirFacturacion.FechaInicioOriginal <= 0) then begin
            spCuentas.Parameters.ParamByName('@FechaInicioOriginalEximicionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@FechaInicioOriginalEximicionFacturacion').Value := Cuenta.EximirFacturacion.FechaInicioOriginal;
        end;

        if (Cuenta.EximirFacturacion.FechaInicio <= 0) then begin
            spCuentas.Parameters.ParamByName('@FechaInicioEximicionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@FechaInicioEximicionFacturacion').Value := Cuenta.EximirFacturacion.FechaInicio;
        end;
        if (Cuenta.EximirFacturacion.FechaFinalizacion <= 0) then begin
            spCuentas.Parameters.ParamByName('@FechaFinalizacionEximicionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@FechaFinalizacionEximicionFacturacion').Value := Cuenta.EximirFacturacion.FechaFinalizacion;
        end;
        spCuentas.Parameters.ParamByName('@CodigoMotivoEximicionFacturacion').Value := Cuenta.EximirFacturacion.CodigoMotivoNoFacturable;
        spCuentas.Parameters.ParamByName('@Patente').Value := Cuenta.Vehiculo.Patente;

(* Cargar los parámetros para la Bonificacion de Facturación. *)
        if (Cuenta.BonificarFacturacion.FechaInicioOriginal <= 0) then begin
            spCuentas.Parameters.ParamByName('@FechaInicioOriginalBonificacionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@FechaInicioOriginalBonificacionFacturacion').Value := Cuenta.BonificarFacturacion.FechaInicioOriginal;
        end;
        if (Cuenta.BonificarFacturacion.FechaInicio <= 0) then begin
            spCuentas.Parameters.ParamByName('@FechaInicioBonificacionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@FechaInicioBonificacionFacturacion').Value := Cuenta.BonificarFacturacion.FechaInicio;
        end;
        if (Cuenta.BonificarFacturacion.FechaFinalizacion <= 0) then begin
            spCuentas.Parameters.ParamByName('@FechaFinalizacionBonificacionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@FechaFinalizacionBonificacionFacturacion').Value := Cuenta.BonificarFacturacion.FechaFinalizacion;
        end;

        if (Cuenta.BonificarFacturacion.Porcentaje <= 0) then begin
            spCuentas.Parameters.ParamByName('@PorcentajeBonificacionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@PorcentajeBonificacionFacturacion').Value := Cuenta.BonificarFacturacion.Porcentaje;
        end;

        if (Cuenta.BonificarFacturacion.Importe <= 0) then begin
            spCuentas.Parameters.ParamByName('@ImporteBonificacionFacturacion').Value := Null;
        end else begin
            spCuentas.Parameters.ParamByName('@ImporteBonificacionFacturacion').Value := Cuenta.BonificarFacturacion.Importe;
        end;

        spCuentas.Parameters.ParamByName('@CodigoAlmacenDestino').Value := Cuenta.CodigoAlmacenDestino;
        //spCuentas.Parameters.ParamByName('@AdheridoPA').Value := False;                                          //TASK_023_GLE_20161212 Comenta
        spCuentas.Parameters.ParamByName('@AdheridoPA').Value := 0;                                                //TASK_023_GLE_20161212 Corrige
        spCuentas.Parameters.ParamByName('@CodigoConvenioFacturacion').Value := Cuenta.CodigoConvenioFacturacion;
        spCuentas.Parameters.ParamByName('@Foraneo').Value := Cuenta.Vehiculo.Foraneo;
        spCuentas.ExecProc;

        if spCuentas.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
            raise Exception.Create(spCuentas.Parameters.ParamByName('@ErrorDescription').Value);

        Result := spCuentas.Parameters.ParamByName('@IndiceVehiculo').Value;

    finally
        if Assigned(spCuentas) and spCuentas.Active then
                spCuentas.close;
        FreeAndNil(spCuentas);    //TASK_106_JMA_20170206
    end;
end;
{TERMINO: TASK_076_JMA_20161121}

end.
