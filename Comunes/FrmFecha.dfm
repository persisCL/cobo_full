object formFecha: TformFecha
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Fecha'
  ClientHeight = 92
  ClientWidth = 254
  Color = clBtnFace
  Constraints.MaxHeight = 130
  Constraints.MaxWidth = 270
  Constraints.MinHeight = 130
  Constraints.MinWidth = 270
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblFecha: TLabel
    Left = 8
    Top = 16
    Width = 103
    Height = 13
    Alignment = taRightJustify
    Caption = 'lblFecha'
  end
  object edtFecha: TDateEdit
    Left = 117
    Top = 13
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    Date = -693594.000000000000000000
  end
  object btnAceptar: TButton
    Left = 36
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 144
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = btnCancelarClick
  end
end
