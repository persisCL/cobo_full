unit FrmRecibirTags;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ADODB, Provider, DB, DBClient,
  Grids, DBGrids, DMConnection, UtilProc, PeaTypes, UtilDB, Util, RStrings, PeaProcs,
  VariantComboBox, FrmObservacionesGeneral;

type
  TGuiasDespacho = record
    CodigoGuiaDespacho: integer;
    NumeroGuiaDespacho: string[20];
    CodigoAlmacenOrigen: integer;
    CodigoAlmacenDestino: integer;
    CodigoTransportista: integer;
    Estado: String[1];
    FechaHoraEnvio: TDateTime;
    FechaHoraRecepcion: TDateTime;
    FechaHoraAceptacionRechazo: TDateTime;
    Observacion: string;
  end;


  TFormRecibirTags = class(TForm)
    pnlTop: TPanel;
    dsRecibirTags: TDataSource;
    pnlBottom: TPanel;
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
    lblGuiaDespacho: TLabel;
    dbgRecibirTags: TDBGrid;
    ActualizarStockTagsRecibidos: TADOStoredProc;
    lblTransportista: TLabel;
    cbTransportistas: TVariantComboBox;
    lblAlmacenDestino: TLabel;
    btnObservacion: TDPSButton;
    cbGuiaDespacho: TComboBox;
    ObtenerDatosGuiaDespacho: TADOStoredProc;
    ActualizarEstadoGuiaDespacho: TADOStoredProc;
    txtAlmacenDestino: TEdit;
    ObtenerGuiaDespachoCategorias: TADOStoredProc;
    cdsRecibirTags: TClientDataSet;
    procedure btnAceptarClick(Sender: TObject);
    procedure dbgRecibirTagsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsRecibirTagsAfterInsert(DataSet: TDataSet);
    procedure cbTransportistasChange(Sender: TObject);
    procedure btnObservacionClick(Sender: TObject);
  private
    FGuiasDespacho: TGuiasDespacho;
    FUsuario: Ansistring;
    FCodigoPuntoEntrega: integer;
  public
    function Inicializar(Caption: TCaption; Usuario: AnsiString; CodigoPuntoEntrega: integer = -1): boolean;
  end;

var
  FormRecibirTags: TFormRecibirTags;

implementation

{$R *.dfm}

function TFormRecibirTags.Inicializar(Caption: TCaption; Usuario: AnsiString; CodigoPuntoEntrega: integer = -1): boolean;
begin
    result := False;
    self.Caption := Caption;
    FUsuario := Usuario;
    FCodigoPuntoEntrega := CodigoPuntoEntrega;
    FGuiasDespacho.Observacion := '';
    //Si no se pasa el CodigoPuntoEntrega significa que estoy recibiendo rechazados
//    FGuiasDespacho.Estado:= iif(CodigoPuntoEntrega = -1, ESTADO_RECHAZADO, ESTADO_ENVIADO);
    FGuiasDespacho.Estado:= ESTADO_ENVIADO;
    try
        if CodigoPuntoEntrega <> -1 then
            txtAlmacenDestino.Text := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM PuntosEntrega WHERE CodigoPuntoEntrega = ''' + inttostr(FCodigoPuntoEntrega) + ''''))
        else
            btnObservacion.Caption := '&' + STR_OBSERVACION;
        CargarTransportistas(DMConnections.BaseCAC, cbTransportistas);
        CargarGuiaDespacho(DMConnections.BaseCAC, cbGuiaDespacho, True);

	except
		on e: Exception do begin
			Screen.Cursor := crDefault;
			MsgBox(MSG_ERROR_APERTURA + #13#10 +
                    E.Message, self.Caption, MB_ICONSTOP);
			Exit;
		end;
	end;
    result := True;
end;

procedure TFormRecibirTags.btnAceptarClick(Sender: TObject);
var
    TodoOk: Boolean;
begin

    if (cbTransportistas.ItemIndex < 1) then begin
        MsgBoxBalloon(MSG_ERROR_TRANSPORTISTA_SELECCIONAR,self.Caption,MB_ICONSTOP,cbTransportistas);
        Exit;
    end;

    if (cbGuiaDespacho.ItemIndex < 1) then begin
        MsgBoxBalloon(MSG_VALIDAR_GUIA_DESPACHO,self.Caption,MB_ICONSTOP,cbGuiaDespacho);
        Exit;
    end;

	Screen.Cursor := crHourGlass;
    TodoOk:= False;

    DMConnections.BaseCAC.BeginTrans;
    try
        try

            cdsRecibirTags.First;

            while not cdsRecibirTags.Eof do begin

                ActualizarStockTagsRecibidos.Close;
                ActualizarStockTagsRecibidos.Parameters.Refresh;
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@Categoria').Value := cdsRecibirTags.FieldByName('Categoria').AsInteger;;
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@Cantidad').Value := cdsRecibirTags.FieldByName('CantidadTags').AsInteger;;
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@CodigoPuntoEntrega').Value := iif(FCodigoPuntoEntrega<>-1, FCodigoPuntoEntrega, NULL);
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@CodigoAlmacenOrigen').Value := iif(FCodigoPuntoEntrega<>-1, NULL, FGuiasDespacho.CodigoAlmacenOrigen);
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@CodigoAlmacenDestino').Value := iif(FCodigoPuntoEntrega<>-1, NULL, FGuiasDespacho.CodigoAlmacenDestino);
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@CodigoTransportista').Value := cbTransportistas.Items[cbTransportistas.ItemIndex].Value;
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@CodigoGuiaDespacho').Value := FGuiasDespacho.CodigoGuiaDespacho;
                ActualizarStockTagsRecibidos.Parameters.ParamByName('@Usuario').Value := FUsuario;
                ActualizarStockTagsRecibidos.ExecProc;

                cdsRecibirTags.Next;

            end;

            //Actualizo el Estado de la Guia de Despacho
            ActualizarEstadoGuiaDespacho.Close;
            ActualizarEstadoGuiaDespacho.Parameters.Refresh;
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@CodigoGuiaDespacho').Value := FGuiasDespacho.CodigoGuiaDespacho;
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@FechaHoraRecepcion').Value := iif(FCodigoPuntoEntrega<>-1, NowBase(DMConnections.BaseCAC), NULL);
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@FechaAceptacionRechazo').Value := iif(FCodigoPuntoEntrega<>-1, NULL, NowBase(DMConnections.BaseCAC));
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Observacion').Value := iif(Trim(FGuiasDespacho.Observacion) = '', NULL, FGuiasDespacho.Observacion);
            ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Estado').Value := ESTADO_ACEPTADO;
            ActualizarEstadoGuiaDespacho.ExecProc;

            TodoOk:= True;

        except
		    on e: Exception do begin
			    Screen.Cursor := crDefault;
			    MsgBox(MSG_ERROR_TURNO_GUARDAR + #13#10 +
                        E.Message, self.Caption, MB_ICONSTOP);
			    Exit;
    		end;
        end;
    finally
        Screen.Cursor := crDefault;
        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;
            MsgBox(MSG_CAPTION_RECEPCION, self.Caption, MB_ICONINFORMATION);
            ModalResult := mrOk;
        end
        else DMConnections.BaseCAC.RollbackTrans;
    end;
end;

procedure TFormRecibirTags.dbgRecibirTagsKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
end;

procedure TFormRecibirTags.cdsRecibirTagsAfterInsert(
  DataSet: TDataSet);
begin
    if visible then begin
        if Trim(DataSet.FieldByName('Descripcion').AsString) = '' then DataSet.Delete;
        btnAceptar.SetFocus;
    end;
end;

procedure TFormRecibirTags.cbTransportistasChange(Sender: TObject);
var
    CodigoAlmacenDestino: integer;
begin


    try
        cdsRecibirTags.Close;
        cdsRecibirTags.CreateDataSet;
        cdsRecibirTags.Open;

        if (cbTransportistas.ItemIndex < 1) or (cbGuiaDespacho.ItemIndex < 1) then Exit;

        ObtenerDatosGuiaDespacho.Close;
        ObtenerDatosGuiaDespacho.Parameters.Refresh;
        ObtenerDatosGuiaDespacho.Parameters.ParamByName('@CodigoTransportista').Value := cbTransportistas.Items[cbTransportistas.ItemIndex].Value;
        ObtenerDatosGuiaDespacho.Parameters.ParamByName('@NumeroGuiaDespacho').Value := Trim(cbGuiaDespacho.Text);
        ObtenerDatosGuiaDespacho.Parameters.ParamByName('@Estado').Value := FGuiasDespacho.Estado[1];
        ObtenerDatosGuiaDespacho.Open;

        if FCodigoPuntoEntrega<>-1 then begin
            CodigoAlmacenDestino := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoAlmacenRecepcion FROM PuntosEntrega WHERE CodigoPuntoEntrega = ''' + inttostr(FCodigoPuntoEntrega) + '''');

            if (ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenDestino').AsInteger <> CodigoAlmacenDestino) then begin
                MsgBox(MSG_ERROR_RECEPCION_TAGS, self.Caption, MB_ICONSTOP);
                Exit;
            end;

        end else begin
            txtAlmacenDestino.Text := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MaestroAlmacenes WHERE CodigoAlmacen = ''' + ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenDestino').AsString + ''''));
            FGuiasDespacho.Observacion := ObtenerDatosGuiaDespacho.FieldByName('Observacion').AsString;
            FGuiasDespacho.CodigoAlmacenOrigen := ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenOrigen').AsInteger;
            FGuiasDespacho.CodigoAlmacenDestino := ObtenerDatosGuiaDespacho.FieldByName('CodigoAlmacenDestino').AsInteger;
        end;

        FGuiasDespacho.CodigoGuiaDespacho := ObtenerDatosGuiaDespacho.FieldByName('CodigoGuiaDespacho').AsInteger;
        FGuiasDespacho.Observacion := ObtenerDatosGuiaDespacho.FieldByName('Observacion').AsString;

        ObtenerGuiaDespachoCategorias.Close;
        ObtenerGuiaDespachoCategorias.Parameters.Refresh;
        ObtenerGuiaDespachoCategorias.Parameters.ParamByName('@CodigoGuiaDespacho').Value := ObtenerDatosGuiaDespacho.FieldByName('CodigoGuiaDespacho').AsInteger;
        ObtenerGuiaDespachoCategorias.Open;

        ObtenerGuiaDespachoCategorias.First;

        while not ObtenerGuiaDespachoCategorias.Eof do begin
            cdsRecibirTags.AppendRecord([ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsInteger,
            ObtenerGuiaDespachoCategorias.FieldByName('Descripcion').AsString,
            ObtenerGuiaDespachoCategorias.FieldByName('Cantidad').AsInteger]);
(*
            cdsRecibirTags.FieldByName('Categoria').AsInteger := ObtenerGuiaDespachoCategorias.FieldByName('Categoria').AsInteger;
            cdsRecibirTags.FieldByName('Descripcion').AsString := ObtenerGuiaDespachoCategorias.FieldByName('Descripcion').AsString;
            cdsRecibirTags.FieldByName('CantidadTags').AsInteger := ObtenerGuiaDespachoCategorias.FieldByName('Cantidad').AsInteger;
            cdsRecibirTags.Post;
*)
            ObtenerGuiaDespachoCategorias.Next;
        end;

        cdsRecibirTags.First;
    except
        on e: Exception do begin
            Screen.Cursor := crDefault;
            MsgBox(MSG_ERROR_APERTURA + #13#10 +
                    E.Message, self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end;
end;

procedure TFormRecibirTags.btnObservacionClick(Sender: TObject);
var
    f: TFormObservacionesGeneral;
begin

    if (cbTransportistas.ItemIndex < 1) then begin
        MsgBoxBalloon(MSG_ERROR_TRANSPORTISTA_SELECCIONAR,self.Caption,MB_ICONSTOP,cbTransportistas);
        Exit;
    end;

    if (cbGuiaDespacho.ItemIndex < 1) then begin
        MsgBoxBalloon(MSG_VALIDAR_GUIA_DESPACHO,self.Caption,MB_ICONSTOP,cbGuiaDespacho);
        Exit;
    end;

    if (btnObservacion.Caption = '&' + STR_OBSERVACION) then begin
        Application.CreateForm(TFormObservacionesGeneral, f);
        f.Inicializa(FGuiasDespacho.Observacion, False);
        f.ShowModal;
        FGuiasDespacho.Observacion := f.Observaciones;
        f.Release;
        Exit;
    end;


    Application.CreateForm(TFormObservacionesGeneral, f);
    f.Inicializa(FGuiasDespacho.Observacion, False);
    f.ShowModal;
    FGuiasDespacho.Observacion := f.Observaciones;
    f.Release;
    Exit;

(*
    //Actualizo el Estado de la Guia de Despacho
    ActualizarEstadoGuiaDespacho.Close;
    ActualizarEstadoGuiaDespacho.Parameters.Refresh;
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@CodigoGuiaDespacho').Value := FGuiasDespacho.CodigoGuiaDespacho;
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@FechaHoraRecepcion').Value := iif(FCodigoPuntoEntrega<>-1, NowBase(DMConnections.BaseCAC), NULL);
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Estado').Value := ESTADO_RECHAZADO;
    ActualizarEstadoGuiaDespacho.Parameters.ParamByName('@Observacion').Value := Observacion;
    ActualizarEstadoGuiaDespacho.ExecProc;
*)
end;

end.
