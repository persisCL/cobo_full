object FrameDatosVehiculo: TFrameDatosVehiculo
  Left = 0
  Top = 0
  Width = 333
  Height = 139
  TabOrder = 0
  object Label13: TLabel
    Left = 9
    Top = 14
    Width = 49
    Height = 13
    Caption = 'Patente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 9
    Top = 39
    Width = 40
    Height = 13
    Caption = 'Marca:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label20: TLabel
    Left = 9
    Top = 64
    Width = 38
    Height = 13
    Caption = 'Modelo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblTipoVehiculo: TLabel
    Left = 9
    Top = 89
    Width = 30
    Height = 13
    Caption = 'Tipo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label47: TLabel
    Left = 9
    Top = 119
    Width = 27
    Height = 13
    Caption = '&A'#241'o:'
    FocusControl = txt_anio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 163
    Top = 119
    Width = 27
    Height = 13
    Caption = 'Color:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object txt_Patente: TEdit
    Left = 88
    Top = 9
    Width = 108
    Height = 21
    Hint = 'patente'
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 10
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnKeyPress = txt_PatenteKeyPress
  end
  object cb_marca: TComboBox
    Left = 88
    Top = 35
    Width = 236
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 1
    OnChange = cb_marcaChange
  end
  object txt_DescripcionModelo: TEdit
    Left = 88
    Top = 59
    Width = 236
    Height = 21
    TabOrder = 2
  end
  object cb_TipoVehiculo: TVariantComboBox
    Left = 88
    Top = 85
    Width = 236
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 3
    Items = <>
  end
  object txt_anio: TNumericEdit
    Left = 88
    Top = 112
    Width = 47
    Height = 21
    Color = 16444382
    MaxLength = 4
    TabOrder = 4
    Decimals = 0
  end
  object cb_color: TComboBox
    Left = 199
    Top = 111
    Width = 126
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    Visible = False
  end
  object VerificarFormatoPatenteChilena: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarFormatoPatenteChilena'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 220
    Top = 6
  end
end
