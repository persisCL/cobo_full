{-------------------------------------------------------------------------------
 File Name: FrmOrdenesServicioBloqueadas;.pas
 Author:    lgisuk
 Date Created: 28/03/2005
 Language: ES-AR
 Description: Permite ver las ordenes de servicio bloqueadas. y desbloquearlas
-------------------------------------------------------------------------------}
unit FrmOrdenesServicioBloqueadas;

interface

uses
  //Reclamos
  DMConnection,              //coneccion a base de datos OP_CAC
  UtilProc,                  //Mensajes
  Util,                      //IIF
  UtilDB,                    //Rutinas para base de datos
  BuscaClientes,             //Busca Clientes
  OrdenesServicio,           //Rutinas para Ordenes de Servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, Validate, DateEdit, ExtCtrls, DB,
  ADODB, VariantComboBox, DmiCtrls;

type
  TFOrdenesServicioBloquedas = class(TForm)
    GBCriterios: TGroupBox;
    DBLReclamos: TDBListEx;
    PBotones: TPanel;
    PCriterios: TPanel;
    BuscarBTN: TButton;
    PBotonesDerecha: TPanel;
    DesbloquearBTN: TButton;
    SalirBTN: TButton;
    SpObtenerOrdenesServicioBloqueadas: TADOStoredProc;
    DataSource: TDataSource;
    SpObtenerOrdenesServicioBloqueadasCodigoOrdenServicio: TAutoIncField;
    SpObtenerOrdenesServicioBloqueadasDescripcion: TStringField;
    SpObtenerOrdenesServicioBloqueadasFechaHoraCreacion: TDateTimeField;
    SpObtenerOrdenesServicioBloqueadasFechaCompromiso: TDateTimeField;
    SpObtenerOrdenesServicioBloqueadasPrioridad: TWordField;
    SpObtenerOrdenesServicioBloqueadasRut: TStringField;
    SpObtenerOrdenesServicioBloqueadasNumeroConvenio: TStringField;
    SpObtenerOrdenesServicioBloqueadasFechaHoraFin: TDateTimeField;
    SpObtenerOrdenesServicioBloqueadasDescEstado: TStringField;
    SpObtenerOrdenesServicioBloqueadasPatente: TStringField;
    SpObtenerOrdenesServicioBloqueadasUsuario: TStringField;
    SpObtenerOrdenesServicioBloqueadasEstado: TStringField;
    LCantidadTotal: TLabel;
    procedure SalirBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarBTNClick(Sender: TObject);
    procedure EtipoFechaKeyPress(Sender: TObject; var Key: Char);
    procedure DesbloquearBTNClick(Sender: TObject);
    procedure DBLReclamosColumns0HeaderClick(Sender: TObject);
    procedure DBLReclamosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLReclamosDblClick(Sender: TObject);
    procedure DBLReclamosMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure FormDestroy(Sender: TObject);
  private
    procedure OSChanged;
    Function  Buscar:boolean;
    Function  Desbloquear:boolean;
    { Private declarations }
  public
    function  Inicializar : Boolean;
    { Public declarations }
  end;

var
  FOrdenesServicioBloquedas: TFOrdenesServicioBloquedas;
  GBitLock: TBitmap;
  GBitLockOwner: TBitmap;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFOrdenesServicioBloquedas.Inicializar: Boolean;
resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
    SELECCIONAR = '(Seleccionar)';
const
    TOP = 100;
var
    S: Tsize;
begin
	Result := False;
    //lo ajusto al tama�o disponible
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    //me conecto a la base
	try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected; //and Buscar;

    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    // Todo Ok, terminamos de inicializar
    AddOSNotification(OSChanged); //acepto ser notificado por cambios en las ordenes de servicio
end;

{-----------------------------------------------------------------------------
  Function Name: Do_Init
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Creo las imagenes que iran en la grilla
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Procedure Do_Init;
begin
    //Imagen Candado Bloqueado
	GBitLock := TBitmap.Create;
	GBitLock.LoadFromResourceName(HInstance, 'TASK_LOCK');
    GBitLock.TransparentColor := clOlive;
    GBitLock.Transparent := True;
    //Imagen Candado Bloquedado
	GBitLockOwner := TBitmap.Create;
	GBitLockOwner.LoadFromResourceName(HInstance, 'TASK_LOCK_OWNER');
    GBitLockOwner.TransparentColor := clOlive;
    GBitLockOwner.Transparent := True;
end;

{-----------------------------------------------------------------------------
  Function Name: OSChanged
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: cuando cambio una orden de servicio refresca los cambio
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.OSChanged;
begin
    if spObtenerOrdenesServicioBloqueadas.Active then spObtenerOrdenesServicioBloqueadas.Requery;
end;


{-----------------------------------------------------------------------------
  Function Name: EtipoFechaKeyPress
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: evito que edite los tipos de fecha
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.EtipoFechaKeyPress(Sender: TObject; var Key: Char);
begin
    key:=#0;
end;

{-----------------------------------------------------------------------------
  Function Name: Buscar
  Author:    lgisuk
  Date Created: 06/04/2005
  Description: Busco los Reclamos Bloqueados
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFOrdenesServicioBloquedas.Buscar:boolean;
begin
   Result:=false;
   //ejecuto la consulta
    try
        SpObtenerOrdenesServicioBloqueadas.Close;
        SpObtenerOrdenesServicioBloqueadas.Open;
        Result:=true;
    except
        on e: Exception do begin
            //el stored procedure podria fallar
            MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarBTNClick
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: Busco los Reclamos Bloqueados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.BuscarBTNClick(Sender: TObject);
begin
    Buscar;
end;



{-----------------------------------------------------------------------------
  Function Name: DBLReclamosDrawText
  Author:    lgisuk
  Date Created: 30/03/2005
  Description: Quito los segundos a las Fechas
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.DBLReclamosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
Resourcestring
    NO_DETERMINADA =  'No Determinada';
Const
    DATE_FORMAT = 'dd/mm/yyyy hh:nn';
Var
    Estado, Usuario: AnsiString;
begin

    //Bloqueado / Desbloqueado
    Usuario := spObtenerOrdenesServicioBloqueadas.FieldByName('Usuario').AsString;
    Estado := spObtenerOrdenesServicioBloqueadas.FieldByName('Estado').AsString;
    if Column = dblReclamos.Columns[0] then begin
        if ((Estado = 'P') or (Estado = 'I') or (Estado = 'E')) and (Usuario <> '') then begin  //inclui pendientes internas //(Estado = 'P')//
            if Usuario = UsuarioSistema then
                Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GBitLock)
            else begin
                Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GBitLockOwner);
            end;
            ItemWidth := GBitLock.Width + 4;
        end;
    end;
    //FechaHoraCreacion
    if Column = dblreclamos.Columns[3] then
       if text <> '' then Text := FormatDateTime( DATE_FORMAT , spObtenerOrdenesServiciobloqueadas.FieldByName('FechaHoraCreacion').AsDateTime);
    //FechaCompromiso
    if Column = dblreclamos.Columns[4] then
        if text = '' then Text := NO_DETERMINADA;
    //FechaHoraFin
    if Column = dblreclamos.Columns[5] then
       if text <> '' then Text := FormatDateTime( DATE_FORMAT , spObtenerOrdenesServicioBloqueadas.FieldByName('FechaHoraFin').AsDateTime);
    //Prioridad
    if Column = dblreclamos.Columns[6] then
        Text := ObtenerDescripcionPrioridadOS(spObtenerOrdenesServicioBloqueadas.FieldByName('Prioridad').Asinteger);//Obtengo la descripci�n de la prioridad

end;


{-----------------------------------------------------------------------------
  Function Name: DBLReclamosColumns0HeaderClick
  Author:    lgisuk
  Date Created: 30/03/2005
  Description: Ordena por Columna
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.DBLReclamosColumns0HeaderClick(Sender: TObject);
begin
  inherited;
    if SpObtenerOrdenesServicioBloqueadas.Active = false then exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;


    if SpObtenerOrdenesServicioBloqueadas.RecordCount > 0 then
        SpObtenerOrdenesServicioBloqueadas.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;


{-----------------------------------------------------------------------------
  Function Name: Editar
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Edito un Reclamo
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFOrdenesServicioBloquedas.Desbloquear:boolean;
var
    CodigoOrdenServicio: Integer;
begin
    result:=false;
    try
        //Salgo si no hay reclamos para seleccionar
        if SpObtenerOrdenesServicioBloqueadas.active = false then exit;
        if SpObtenerOrdenesServicioBloqueadas.Fields.Count = 0 then exit;
        //Obtengo el codigo de orden de servicio
        CodigoOrdenServicio:=SpObtenerOrdenesServicioBloqueadas.FieldByName('CodigoOrdenServicio').value;
        //Desbloqueo la orden
        DesbloquearOrdenservicioAdministrador(codigoOrdenServicio);
        //Actualizo la grilla de reclamos
        Buscar;
        result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EditarBTNClick
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: Desbloqueo una OS
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.DesbloquearBTNClick(Sender: TObject);
begin
    Desbloquear;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLReclamosDblClick
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Hace que el doble click en la grilla sea equivalente
               al boton desbloquear.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.DBLReclamosDblClick(Sender: TObject);
begin
    Desbloquear;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.SalirBTNClick(Sender: TObject);
begin
	close;
end;

{-----------------------------------------------------------------------------
  Function Name: Do_Final
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Libero los recursos Imagen
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Procedure Do_Final;
begin
    FreeAndNil(GBitLock);       //Libero el recurso Imagen Lock
    FreeAndNil(GBitLockOwner);  //Libero el recurso Imagen LockOwner
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: ya no deseo ser mas notificado, si hubo cambios en las
               ordenes de servicio.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.FormDestroy(Sender: TObject);
begin
    RemoveOSNotification(OSChanged);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 28/03/2005
  Description:  Lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;


{-----------------------------------------------------------------------------
  Function Name: DBLReclamosMouseMove
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: Muestro un Hint con el usuario que Tiene el Reclamo Bloqueado
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFOrdenesServicioBloquedas.DBLReclamosMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
Var
    Estado, Usuario: AnsiString;
begin
    if SpObtenerOrdenesServicioBloqueadas.Active = false then exit;
    dblreclamos.ShowHint:=false;
    dblreclamos.Hint:='';
    if x < 20 then begin
        try
            //Bloqueado / Desbloqueado
            Usuario := SpObtenerOrdenesServicioBloqueadas.FieldByName('Usuario').AsString;
            Estado := SpObtenerOrdenesServicioBloqueadas.FieldByName('Estado').AsString;
            if ((Estado = 'P') or (Estado = 'I')) and (Usuario <> '') then begin  //inclui pendientes internas //(Estado = 'P')//
                if Usuario <> UsuarioSistema then begin
                     DBLReclamos.Hint:='Bloqueado por Usuario: '+Usuario;
                     dblreclamos.ShowHint:=true;
                end;
            end;
        except
        end;
    end;
end;


initialization
    Do_Init;
finalization
    Do_Final;
end.
