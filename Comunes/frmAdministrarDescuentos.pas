unit frmAdministrarDescuentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, Buttons, ExtCtrls, ImgList,
  UtilProc, Grids, Util;

type
  TFormAdministrarDescuentos = class(TForm)
    Panel1: TPanel;
    btnAplicarDescuentos: TBitBtn;
    btnCancelar: TBitBtn;
    dblDescuentos: TDBListEx;
    spObtenerDescuentos: TADOStoredProc;
    dsDescuentos: TDataSource;
    lnCheck: TImageList;
    spAsignarDescuentoConvenio: TADOStoredProc;
    spAsignarDescuentoCuenta: TADOStoredProc;
    spEliminarDescuentoCuenta: TADOStoredProc;
    SPEliminarDescuentoConvenio: TADOStoredProc;
    procedure dblDescuentosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnAplicarDescuentosClick(Sender: TObject);
    procedure dblDescuentosDblClick(Sender: TObject);
  private
    { Private declarations }
	CurConvenio: Integer;	
    procedure Refrescar;
  public
    { Public declarations }
	function Inicializa(CodigoConvenio: Integer): Boolean;
  end;

var
  FormAdministrarDescuentos: TFormAdministrarDescuentos;

implementation

uses
	frmPatentesConvenio;

{$R *.dfm}

function TFormAdministrarDescuentos.Inicializa(CodigoConvenio: Integer): Boolean;
begin
	visible 	:= False;
    CurConvenio	:= CodigoConvenio;
    Refrescar;
    result := True;
end;

procedure TFormAdministrarDescuentos.btnAplicarDescuentosClick(Sender: TObject);
begin

    ModalResult := mrOk;
end;

procedure TFormAdministrarDescuentos.dblDescuentosDblClick(Sender: TObject);
var
	i: Integer;
    f: TFormPatentesConvenio;
const
	CONST_TIPO_CUENTAS			= 'Cuentas';
    CONST_TIPO_CONVENIOS		= 'Convenio';
    CAPTION_POST_DESCUENTOS		= 'Asignar Descuentos';
    MSG_CANNOT_POST_DESCUENTOS	= 'No se pudo asignar descuentos';
begin
	if (dsDescuentos.DataSet.FieldByName('Asignado').AsBoolean = False) and 
    (dsDescuentos.DataSet.FieldByName('AplicaA').AsString = CONST_TIPO_CUENTAS) then begin
        Application.CreateForm(TFormPatentesConvenio, f);
        if f.Inicializa(CurConvenio) then begin
            if (f.ShowModal = mrOk) then begin
            	for i := 0 to f.chklstPatentesConvenio.Items.Count - 1 do begin
                    if f.chklstPatentesConvenio.Checked[i] then begin
                        with spAsignarDescuentoCuenta, Parameters do begin
                            if Active then
                                Close;
                            Parameters.Refresh;
                            ParamByName('@CodigoConvenio').Value	:= CurConvenio;
                            ParamByName('@CodigoDescuento').Value	:= dsDescuentos.DataSet.FieldByName('CodigoDescuento').Value;
                            ParamByName('@Patente').Value			:= f.chklstPatentesConvenio.Items[i];
                            ParamByName('@Usuario').Value			:= UsuarioSistema;
                            try
                                ExecProc;
                            except
                                on e:Exception do begin
                                    MsgBoxErr(MSG_CANNOT_POST_DESCUENTOS, e.message, CAPTION_POST_DESCUENTOS, MB_ICONSTOP);
                                end;
                            end;
                        end;
                    
                    end
                	else begin
                        with spEliminarDescuentoCuenta, Parameters do begin
                            if Active then
                            	Close;
                            Parameters.Refresh;
                            ParamByName('@CodigoConvenio').Value	:= CurConvenio;
                            ParamByName('@CodigoDescuento').Value	:= dsDescuentos.DataSet.FieldByName('CodigoDescuento').Value;
                            ParamByName('@Patente').Value			:= f.chklstPatentesConvenio.Items[i];
                            ParamByName('@Usuario').Value			:= UsuarioSistema;
                            try
                                ExecProc;
                            except
                                on e:Exception do begin
                                    MsgBoxErr(MSG_CANNOT_POST_DESCUENTOS, e.message, CAPTION_POST_DESCUENTOS, MB_ICONSTOP);
                                end;
                            end;
                        end;
                    end;

                end;
            end;
        end;
        f.Release;

    	
    end
    else if (dsDescuentos.DataSet.FieldByName('Asignado').AsBoolean = True) and
    ((dsDescuentos.DataSet.FieldByName('AplicaA').AsString <> CONST_TIPO_CUENTAS) and
     (dsDescuentos.DataSet.FieldByName('AplicaA').AsString <> CONST_TIPO_CONVENIOS)) then begin
        with spEliminarDescuentoCuenta, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value	:= CurConvenio;
            ParamByName('@CodigoDescuento').Value	:= dsDescuentos.DataSet.FieldByName('CodigoDescuento').Value;
            ParamByName('@Patente').Value			:= TrimRight(dsDescuentos.DataSet.FieldByName('AplicaA').AsString);
            ParamByName('@Usuario').Value			:= UsuarioSistema;
            try
                ExecProc;
            except
                on e:Exception do begin
                    MsgBoxErr(MSG_CANNOT_POST_DESCUENTOS, e.message, CAPTION_POST_DESCUENTOS, MB_ICONSTOP);
                end;
            end;
        end;
    end
    else if (dsDescuentos.DataSet.FieldByName('Asignado').AsBoolean = False) and
    (dsDescuentos.DataSet.FieldByName('AplicaA').AsString = CONST_TIPO_CONVENIOS) then begin
		with spAsignarDescuentoConvenio, Parameters do begin
            if Active then
            	Close;
            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value	:= CurConvenio;
            ParamByName('@CodigoDescuento').Value	:= dsDescuentos.DataSet.FieldByName('CodigoDescuento').Value;
            ParamByName('@Usuario').Value			:= UsuarioSistema;
            try
                ExecProc;
            except
                on e:Exception do begin
                    MsgBoxErr(MSG_CANNOT_POST_DESCUENTOS, e.message, CAPTION_POST_DESCUENTOS, MB_ICONSTOP);
                end;
            end;
        end;
    end
    else if (dsDescuentos.DataSet.FieldByName('Asignado').AsBoolean = True) and
    (dsDescuentos.DataSet.FieldByName('AplicaA').AsString = CONST_TIPO_CONVENIOS) then begin
		with spEliminarDescuentoConvenio, Parameters do begin
            if Active then
            	Close;
            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value	:= CurConvenio;
            ParamByName('@CodigoDescuento').Value	:= dsDescuentos.DataSet.FieldByName('CodigoDescuento').Value;
            ParamByName('@Usuario').Value			:= UsuarioSistema;
            try
                ExecProc;
            except
                on e:Exception do begin
                    MsgBoxErr(MSG_CANNOT_POST_DESCUENTOS, e.message, CAPTION_POST_DESCUENTOS, MB_ICONSTOP);
                end;
            end;
        end;
    end;
    Refrescar;
	
end;

procedure TFormAdministrarDescuentos.Refrescar;
resourcestring
    CAPTION_GET_DESCUENTOS		= 'Obtener Descuentos';
	MSG_CANNOT_GET_DESCUENTOS	= 'No se pudieron obtener los descuentos';
begin
    try
        with spObtenerDescuentos, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value	:= CurConvenio;
            Open;
        end;
    except
    	on E:Exception do begin
        	MsgBoxErr(MSG_CANNOT_GET_DESCUENTOS, e.message, CAPTION_GET_DESCUENTOS, MB_ICONSTOP);
			Exit;
        end;
    end;
end;


procedure TFormAdministrarDescuentos.dblDescuentosDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
var
    lBitmap: TBitmap;
begin

	if (Column = dblDescuentos.Columns[0]) then try

    	lBitmap := TBitmap.Create;

        if(dsDescuentos.DataSet.FieldByName('Asignado').AsBoolean = True) then begin

            lnCheck.GetBitmap(1, lBitmap);
            Text := '';
            Sender.Canvas.FillRect(Rect);
            DefaultDraw := False;
            Sender.Canvas.Draw((Rect.Left + Rect.Right - lBitmap.Width) div 2, Rect.Top, lBitmap);

        end 
        else begin

            lnCheck.GetBitmap(0, lBitmap);
            Text := '';
            Sender.Canvas.FillRect(Rect);
            DefaultDraw := False;
            Sender.Canvas.Draw((Rect.Left + Rect.Right - lBitmap.Width) div 2, Rect.Top, lBitmap);

        end;
        
    finally
        lBitmap.Free;
    end;

end;

end.


