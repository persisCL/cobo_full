{-------------------------------------------------------------------------------
 File Name: FreFuenteReclamoOrdenServicio.pas
 Author		: Nelson Droguett Sierra
 Date Created	: 24/06/2010
 Language: ES-AR
 Description: Seccion con la información de SubTipo del reclamo
-------------------------------------------------------------------------------}
unit FreSubTipoReclamoOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, VariantComboBox, DB, ADODB, UtilProc;

type
  TFrameSubTipoReclamoOrdenServicio = class(TFrame)
    GBSubTipo: TGroupBox;
    lblSubTipo: TLabel;
    vcbSubTiporeclamo: TVariantComboBox;
  private
    { Private declarations }
    Function  GetSubTipoReclamo: Integer;
    Procedure SetSubTipoReclamo(const Value: Integer);
  public
    { Public declarations }
    Function  Inicializar: Boolean; overload;
    Function  Inicializar(SubTipoReclamo: Integer): Boolean; overload;
    Function  Deshabilitar:boolean;
    Function  Validar: Boolean;
    Procedure LoadFromDataset(DS: TDataset);
    //
    Property  SubTipoReclamo: Integer read GetSubTipoReclamo write SetSubTipoReclamo;
  end;

implementation

uses RStrings, DMConnection, PeaProcs;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    Nelson Droguett Sierra
  Date Created: 24-Junio-2010
  Description: inicializacion del frame
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameSubTipoReclamoOrdenServicio.Inicializar: Boolean;
var SQLQuery:string;
    CodigoSubTipoReclamo:integer;
begin
    //Cargo las SubTipos de reclamo
    CargarSubTiposReclamo(vcbSubTiporeclamo);
    //Verifico si hay una SubTipo de reclamo predeterminada para ese punto de Entrega
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    Nelson Droguett Sierra
  Date Created: 24-Junio-2010
  Description: inicializo el frame eligiendo una SubTipo de reclamo
  Parameters: SubTipoReclamo: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameSubTipoReclamoOrdenServicio.Inicializar(SubTipoReclamo: Integer): Boolean;
begin
    vcbSubTiporeclamo.Value := SubTipoReclamo;
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: GetSubTipoReclamo
  Author:    Nelson Droguett Sierra
  Date Created: 24-Junio-2010
  Description:  Obtengo la SubTipo de reclamo
  Parameters: None
  Return Value: Integer
-----------------------------------------------------------------------------}
function TFrameSubTipoReclamoOrdenServicio.GetSubTipoReclamo: Integer;
begin
    Result := vcbSubTiporeclamo.Value;
end;

{-----------------------------------------------------------------------------
  Function Name: SetSubTipoReclamo
  Author:    Nelson Droguett Sierra
  Date Created: 24-Junio-2010
  Description: Asigno la SubTipo de Reclamo
  Parameters: const Value: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSubTipoReclamoOrdenServicio.SetSubTipoReclamo(const Value: Integer);
begin
    vcbSubTiporeclamo.Value := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: LoadFromDataset
  Author:    Nelson Droguett Sierra
  Date Created: 24-Junio-2010
  Description: cargo la SubTipo de reclamo de un dataset
  Parameters: DS: TDataset
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSubTipoReclamoOrdenServicio.LoadFromDataset(DS: TDataset);
begin
    vcbSubTiporeclamo.Value := DS['SubTipoOrdenServicio'];
end;

{-----------------------------------------------------------------------------
  Function Name: Deshabilitar
  Author:    Nelson Droguett Sierra
  Date Created: 24-Junio-2010
  Description:  deshabilito los campos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFrameSubTipoReclamoOrdenServicio.Deshabilitar:boolean;
begin
    vcbSubTiporeclamo.Enabled:= False;
    vcbSubTiporeclamo.Color:= clBtnFace;
    result:= true;
end;

{-----------------------------------------------------------------------------
  Function Name: Validar
  Author:    Nelson Droguett Sierra
  Date Created: 24-Junio-2010
  Description: valido la SubTipo de reclamo
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameSubTipoReclamoOrdenServicio.Validar: Boolean;
resourcestring
    MSG_ERROR_SubTipo = 'Debe indicar la SubTipo del reclamo';
begin
    Result := False;
    //obligo a que carguen una SubTipo de reclamo
    if vcbSubTiporeclamo.value = 0 then begin
        MsgBoxBalloon(MSG_ERROR_SubTipo, STR_ERROR, MB_ICONSTOP, vcbSubTiporeclamo);
        vcbSubTiporeclamo.SetFocus;
        Exit;
    end;
    Result := True;
end;

end.
