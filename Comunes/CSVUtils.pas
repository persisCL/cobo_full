unit CSVUtils;

{
    Firma       : SS_1397_MCA_20150930
    Descripcion : se reemplaza el . por la , en los separador de mil y decimal

}
interface

uses
    DB, sysutils, util;

function DatasetToExcelCSV(Dataset: TDataset; var CSV: string; var Error: string): Boolean;

implementation

const
    DELIMITADOR_EXCEL = '"';
    SEPARADOR_EXCEL = ';';


function EscaparCampoExcel(Campo: TField): string;
var                                                                             //SS_1397_MCA_20150930
    lds, lts: Char;                                                             //SS_1397_MCA_20150930
begin
    lts:=ThousandSeparator;                                                     //SS_1397_MCA_20150930
    ThousandSeparator:='.';                                                     //SS_1397_MCA_20150930
    lds:=DecimalSeparator;                                                      //SS_1397_MCA_20150930
    DecimalSeparator:=',';                                                      //SS_1397_MCA_20150930
    result := '';

    if Campo.IsNull then exit;
    case Campo.DataType of
        ftUnknown, ftString, ftFixedChar, ftWideString, ftBytes, ftVarBytes, ftLargeint, ftVariant, ftGuid:
            result := result + DELIMITADOR_EXCEL + stringreplace(Campo.AsString, DELIMITADOR_EXCEL, DELIMITADOR_EXCEL + DELIMITADOR_EXCEL, [rfReplaceAll]) + DELIMITADOR_EXCEL;

        ftSmallint, ftInteger, ftWord, ftAutoInc:
            result := result + Campo.AsString;

        ftBoolean:
            result := result + Campo.AsString;

        ftFloat, ftCurrency, ftBCD, ftFMTBcd:
            result := result + Campo.AsString;

        ftDate:
            result := result + FormatDateTime('dd/mm/yyyy', Campo.AsDateTime);

        ftTime:
            result := result + FormatDateTime('hh:nn:ss', Campo.AsDateTime);

        ftDateTime, ftTimeStamp:
            begin
                if trunc(Campo.AsDateTime) = 0 then begin
                    // es una hora
                    result := result + FormatDateTime('hh:nn:ss', Campo.AsDateTime);
                end else if frac(Campo.AsDateTime) = 0 then begin
                    // es una fecha
                    result := result + FormatDateTime('dd/mm/yyyy', Campo.AsDateTime);
                end else begin
                    // es una fecha completa
                    result := result + FormatDateTime('dd/mm/yyyy hh:nn:ss', Campo.AsDateTime);
                end;
            end;

        ftBlob, ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob, ftInterface, ftIDispatch:
            result := result + 'Tipo de campo no soportado';
    end;
    ThousandSeparator := lts;                                                   //SS_1397_MCA_20150930
    DecimalSeparator := lds;                                                    //SS_1397_MCA_20150930
end;

function DatasetToExcelCSV(Dataset: TDataset; var CSV: string; var Error: string): boolean;
var
    i: integer;
begin
    result := false;
    try
        CSV := '';
        with Dataset do begin
            // Pongo las cabeceras
            for i := 0 to FieldCount - 2 do begin
                CSV := CSV + AnsiQuotedStr(Fields[i].FieldName, DELIMITADOR_EXCEL) + SEPARADOR_EXCEL;
            end;
            CSV := CSV + AnsiQuotedStr(Fields[FieldCount - 1].FieldName, DELIMITADOR_EXCEL) + CRLF;

            // Insertamos los datos
            First;
            while not eof do begin
                for i := 0 to FieldCount - 2 do begin
                    CSV := CSV + EscaparCampoExcel(Fields[i]) + SEPARADOR_EXCEL;
                end;
                CSV := CSV + EscaparCampoExcel(Fields[FieldCount - 1]) + CRLF;
                next;
            end;
        end;
        result := true;
    except
        on e: exception do begin
            Error := e.message;
        end;
    end;
end;

end.
