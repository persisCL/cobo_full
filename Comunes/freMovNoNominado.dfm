object freMovTeleviasNoNominado: TfreMovTeleviasNoNominado
  Left = 0
  Top = 0
  Width = 434
  Height = 146
  TabOrder = 0
  object dbgEntrega: TDBGrid
    Left = 0
    Top = 0
    Width = 434
    Height = 146
    Align = alClient
    DataSource = dsEntrega
    Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Categoria'
        ReadOnly = True
        Title.Caption = 'Categor'#237'a'
        Width = 203
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CantidadTAGsDisponibles'
        ReadOnly = True
        Title.Caption = 'Telev'#237'as Disponibles'
        Width = 109
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CantidadTags'
        Title.Alignment = taCenter
        Title.Caption = 'Cantidad Telev'#237'as '
        Width = 100
        Visible = True
      end>
  end
  object ObtenerSaldosTAGsAlmacen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerSaldosTAGsAlmacen'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 328
    Top = 72
  end
  object cdsEntrega: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Categoria'
        DataType = ftString
        Size = 120
      end
      item
        Name = 'CantidadTAGsDisponibles'
        DataType = ftLargeint
      end
      item
        Name = 'CantidadTAGs'
        DataType = ftLargeint
      end
      item
        Name = 'CodigoCategoria'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    OnNewRecord = cdsEntregaNewRecord
    Left = 296
    Top = 72
  end
  object dsEntrega: TDataSource
    DataSet = cdsEntrega
    Left = 264
    Top = 72
  end
  object RegistrarMovimientoStockNoNominado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarMovimientoStockNoNominado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 232
    Top = 73
  end
  object qryCategorias: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT Categoria, Descripcion FROM Categorias')
    Left = 360
    Top = 72
  end
end
