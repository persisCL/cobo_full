object FormChecklist: TFormChecklist
  Left = 321
  Top = 190
  BorderStyle = bsDialog
  Caption = 'Item pendiente'
  ClientHeight = 219
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 111
    Height = 13
    Caption = 'Tipo de item pendiente:'
  end
  object Label2: TLabel
    Left = 10
    Top = 42
    Width = 61
    Height = 13
    Caption = 'Vencimiento:'
  end
  object Label3: TLabel
    Left = 9
    Top = 72
    Width = 58
    Height = 13
    Caption = 'Comentarios'
  end
  object cbChecklist: TVariantComboBox
    Left = 128
    Top = 12
    Width = 313
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  object txtVencimiento: TDateEdit
    Left = 128
    Top = 40
    Width = 113
    Height = 21
    AutoSelect = False
    TabOrder = 1
    Date = -693594
  end
  object txtObservaciones: TMemo
    Left = 8
    Top = 88
    Width = 441
    Height = 89
    TabOrder = 2
  end
  object btnAceptar: TButton
    Left = 286
    Top = 187
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 3
  end
  object Button2: TButton
    Left = 374
    Top = 187
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
  end
  object Query: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT * FROM MaestroChecklist WITH (NOLOCK)  ORDER BY Descripci' +
        'on')
    Left = 264
    Top = 48
  end
end
