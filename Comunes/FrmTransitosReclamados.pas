{-------------------------------------------------------------------------------
 File Name: FrmTransitosReclamados.pas
 Author: lgisuk
 Date Created: 24/05/05
 Language: ES-AR
 Description: Informo los transitos que ya fueron reclamados
              y permito abrir un reclamo por los tr�nsitos
              no reclamados.
Revision : 1
    Date: 04/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.
-------------------------------------------------------------------------------}
unit FrmTransitosReclamados;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  ImagenesTransitos,              //Muestra la Ventana con la imagen
  OrdenesServicio,                //ObtenerDescripcionOS
  FrmReclamoTransito,             //Reclamo Transito
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Menus;

type
  TFormTransitosReclamados = class(TForm)
    DataSource: TDataSource;
    lnCheck: TImageList;
    spObtenerTransitosReclamados: TADOStoredProc;
    DBLTransitos: TDBListEx;
    Ltitulo: TLabel;
    Lmensaje: TLabel;
    PBotonera: TPanel;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    OKBTN: TButton;
    ilCamara: TImageList;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure DBLTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
  private
    { Private declarations }
    fcodigoconvenio:integer;
    fListaTransitos:tstringlist;
    fListaNoincluidos:tstringlist;
  public
    { Public declarations }
    function Inicializar(CodigoConvenio: Integer; ListaTransitos:tstringlist ): Boolean;
  end;

var
  FormTransitosReclamados: TFormTransitosReclamados;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicializaci�n de Este Formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormTransitosReclamados.Inicializar(CodigoConvenio: Integer; ListaTransitos:tstringlist ): Boolean;

    //Cargo la grilla de reclamos
    Function ObtenerDatosTransitos(CodigoConvenio:integer; Transitos: TStringList):boolean;

        //Obtengo la Enumeraci�n
        function ObtenerEnumeracion(Lista: TStringList): String;
        var
            Enum: AnsiString;
            i: Integer;
        begin
            Enum := '';
            for i:= 0 to Lista.Count - 1 do begin
                //Enum := Enum + Lista.Strings[i] + ',';
                Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
            end;
            Delete(Enum, Length(Enum), 1);
            Result := Enum;
        end;

    begin
        with SPObtenerTransitosReclamados do begin
            close;
            parameters.Refresh;
            parameters.ParamByName('@CodigoConvenio').value := IIF(CodigoConvenio = 0, NULL, CodigoConvenio);
            parameters.ParamByName('@Enumeracion').value := ObtenerEnumeracion(Transitos);
            open;
        end;

        result:=true;
    end;

    {-----------------------------------------------------------------------------
        Function Name:
        Author:
        Date Created:  /  /
        Description:
        Parameters: var ListaNoIncluidos:TstringList
        Return Value: boolean

        Revision : 1
        Date: 04/03/2009
        Author: pdominguez
        Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
            los bloqueos de tablas en la lectura

    -----------------------------------------------------------------------------}
    Function ObtenerTransitosNoIncluidos(var ListaNoIncluidos:TstringList):boolean;

        //me fijo si esta incluido el transito en algun otro reclamo del cliente
        Function EstaIncluidoTransito(CodigoConvenio:integer;NumCorrCA:string):boolean;
        begin
            result:=false;
            if codigoconvenio =  0 then
                result := QueryGetValue(dmconnections.BaseCAC,'select top 1 NumCorrCA from ordenesservicio o WITH (NOLOCK), ordenesserviciotransito t WITH (NOLOCK) where o.codigoordenservicio = t.codigoordenservicio AND ((o.estado = '''+'T'+''' and t.codigoresolucionreclamotransito = 2) or (o.estado in ('''+'P'+''','''+'I'+''','''+'E'+'''))) and NumCorrCA = '+ NumCorrCA) <> '';
            if codigoconvenio <> 0 then
                result := QueryGetValue(dmconnections.BaseCAC,'select top 1 NumCorrCA from ordenesservicio o WITH (NOLOCK), ordenesserviciotransito t WITH (NOLOCK) where o.codigoordenservicio = t.codigoordenservicio AND ((o.estado = '''+'T'+''' and t.codigoresolucionreclamotransito = 2) or (o.estado in ('''+'P'+''','''+'I'+''','''+'E'+'''))) and o.codigoconvenio = ' +inttostr(codigoconvenio)+' and NumCorrCA = '+ NumCorrCA) <> '';
        end;

    var
      i:integer;
    begin
        i:=0;
        while i <= FListaTransitos.Count-1 do
        begin
            if not EstaIncluidoTransito(FCodigoConvenio, FListaTransitos.Strings[i]) then begin
                ListaNoIncluidos.Add(FListaTransitos.Strings[i]);
            end;
            inc(i);
        end;
        result:=true;
    end;

Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Transitos Reclamados';
    MSG_ERROR = 'Error';
Const
    STR_MENSAJE = 'Todos los tr�nsitos seleccionados estan incluidos en reclamos anteriores!';
var
    ListaNoIncluidos: TstringList;
begin
    try
        //si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaTransitos <> nil then begin
            //cargo la grilla con los transitos
            ObtenerDatosTransitos(CodigoConvenio, ListaTransitos);
        end;
        FCodigoConvenio := CodigoConvenio;
        FListaTransitos := ListaTransitos;
        //Centro el form
        CenterForm(Self);
        //creo la lista
        ListaNoIncluidos:= TStringList.Create;
        //cargo la lista de transitos no includos
        ObtenerTransitosNoIncluidos(ListaNoIncluidos);
        //si no hay ninguno sin incluir
        if ListanoIncluidos.Text = '' then begin
            Lmensaje.caption := STR_MENSAJE;
            OKBTN.Visible :=true;
            AceptarBTN.Visible := False;
            BTNCancelar.Visible := False;
        end;
        FlistaNoIncluidos := ListaNoIncluidos;
        //titulo
        ActiveControl := dbltransitos;
        Result := true;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamados.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosDrawText
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamados.DBLTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 29/06/2005
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer):boolean;
    var
        bmp: TBitMap;
    begin
        bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            bmp.Free;
        end;
    end;

var
    I : Integer;
    Resolucion : String;
begin
    //FechaHora
    if Column = dbltransitos.Columns[2] then begin
       if Text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', SPObtenerTransitosReclamados.FieldByName('FechaHora').AsDateTime);
    end;

    //muestra si el cliente rechazo un transito o no
  	if Column = dbltransitos.Columns[3] then begin
        //verifica si esta seleccionado o no
        I := Iif(SPObtenerTransitosReclamados.FieldByName('Rechaza').Asstring = 'True', 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos, lncheck, I);
  	end;

    //Muestra la resolucion de la empresa con respecto al transito
  	if Column = dbltransitos.Columns[4] then begin
        //Obtengo la resolcion
        Resolucion := SPObtenerTransitosReclamados.FieldByName('CodigoResolucionReclamoTransito').AsString;
        if Resolucion = '' then begin
            Text := 'Pendiente';
        end else begin
            if Resolucion = '1' then Text := 'Pendiente';
            if Resolucion = '2' then Text := 'Aceptado';
            if Resolucion = '3' then Text := 'Denegado';
        end;
  	end;

    //si el transito tiene imagen le crea un link para que se pueda acceder a verla
    if Column = dblTransitos.Columns[5] then begin
        I := Iif(SPObtenerTransitosReclamados.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos,ilCamara,i);
    end;

    //muestros la descripcion del estado
    if Column = dblTransitos.Columns[7] then begin
        Text := ObtenerDescripcionEstadoOS(SPObtenerTransitosReclamados.FieldByName('Estado').Asstring);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosLinkClick
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: permito rechazar o aceptar un transito
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamados.DBLTransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    //Mostrar Imagen del Transito
    if Column = dblTransitos.Columns[5] then begin
        //Si hay imagen
        if SPObtenerTransitosReclamados.FieldByName('RegistrationAccessibility').AsInteger > 0 then begin
            //La muestra
            //MostrarVentanaImagen(Self,SPObtenerTransitosReclamados['NumCorrCA'], SPObtenerTransitosReclamados['FechaHora']);  // SS_1091_CQU_20130516
            MostrarVentanaImagen(Self,                                                                                          // SS_1091_CQU_20130516
                SPObtenerTransitosReclamados['NumCorrCA'],                                                                      // SS_1091_CQU_20130516
                SPObtenerTransitosReclamados['FechaHora'],                                                                      // SS_1091_CQU_20130516
                SPObtenerTransitosReclamados['EsItaliano']);                                                                    // SS_1091_CQU_20130516
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: permito abrir un reclamo por los tr�nsitos
               no reclamados.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamados.AceptarBTNClick(Sender: TObject);

    Procedure CrearReclamoTransito(CodigoConvenio : Integer ; ListaNoIncluidos : TStringList);
    var
        F:TFormReclamoTransito;
    begin
        //creo el form de reclamo por transitos no aceptados
        Application.CreateForm(TFormReclamoTransito, F);
        if F.Inicializar(207, CodigoConvenio, ListaNoIncluidos) then F.show;
    end;

begin
    //creo el reclamo con los tr�nsitos que no estan incluidos
    CrearReclamoTransito(FCodigoConvenio, FListaNoIncluidos);
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamados.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description:
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamados.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //lo libero de memoria
    Action := caFree;
end;


end.
