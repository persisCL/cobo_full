unit frmReporteContratoAdhesionPA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppDB, ppDBPipe, UtilRB, ppParameter, ppCtrls, ppPrnabl, ppClass, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  GIFImg, ppStrtch, ppSubRpt, LMDPNGImage, ppPDFDevice, ppBarCod, PeaProcsCN, ppRichTx, UtilHttp, ConstParametrosGenerales,
  DMConnection;

type
  TReporteContratoAdhesionPAForm = class(TForm)
    prprtContrato: TppReport;
    pdtlbnd1: TppDetailBand;
    prmtrlst1: TppParameterList;
    RBInterface: TRBInterface;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand1: TppDetailBand;
    ppImage1: TppImage;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppImage2: TppImage;
    pplblNombreCliente: TppLabel;
    pplblRUT: TppLabel;
    pplblEMail: TppLabel;
    pplblFirma: TppLabel;
    pplblFecha: TppLabel;
    ppImgNo: TppImage;
    ppImgSi: TppImage;
    ppShpNo: TppShape;
    ppshpSi: TppShape;
    ppBCRUT: TppBarCode;
    ppSubReport3: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppRichText1: TppRichText;
    pplblContratoConAnexo: TppLabel;
    ppImage3: TppImage;
    ppImage4: TppImage;
    ppLabel1: TppLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(pNombreCliente, pFirma, pRUT, pEMail: string; pFecha: TDate; pClienteAdheridoEnvioEMail: Boolean; pSoloDatosPagina2: Boolean = False; pSinFechaContrato: Boolean = False; pSituacionesARegularizar: string = ''): Boolean;
    procedure ImprimirPDF(pNombreArchivo: string);
    procedure ConfigurarImpresionContrato(pImprimirContrato, pImprimirAnexo: Boolean);
  end;

var
  ReporteContratoAdhesionPAForm: TReporteContratoAdhesionPAForm;

implementation

{$R *.dfm}

function TReporteContratoAdhesionPAForm.Inicializar(pNombreCliente, pFirma, pRUT, pEMail: string; pFecha: TDate; pClienteAdheridoEnvioEMail: Boolean; pSoloDatosPagina2: Boolean = False; pSinFechaContrato: Boolean = False; pSituacionesARegularizar: string = ''): Boolean;
	const
    	cFormatoFecha = '"a "dd" de %s de "yyyy';
        cFormatoCode3de9 = '*%s*';
        PLANTILLA_ANEXO_CONTRATO_PAK = 'PLANTILLA_ANEXO_CONTRATO_PAK';
	resourcestring
        MSG_NO_EXISTE_PLANTILLA = 'La plantilla del Anexo del Contrado de Adhesi�n PAK NO Existe. Archivo: %s';
        MSG_NO_EXISTE_PARAMETRO = 'No se puede leer el par�metro general "%s". El par�metro no existe.';
    var
        lPlantillaAnexoPAK: string;

begin
	try
	if pSoloDatosPagina2 then begin
    	ppSubReport1.Visible := False;
        ppImage2.Visible     := False;
        ppShpNo.Visible		 := False;
        ppshpSi.Visible      := False;
    end;

    pplblNombreCliente.Caption  := pNombreCliente;
    pplblFirma.Caption := pFirma;
//    pplblBarcodeRUT.Caption		:= Format(cFormatoCode3de9, [pRUT]);
	ppBCRUT.Data				:= pRUT;
    pplblRUT.Caption            := FormatearRUTCAC(pRUT);
    pplblEMail.Caption          := pEMail;
    pplblFecha.Caption          := FormatDateTime(Format(cFormatoFecha, [MesNombre(pFecha)]), pFecha);

    if (pClienteAdheridoEnvioEMail = null) then begin
        ppImgSi.Visible := False;
        ppImgNo.Visible := False;
    end
    else begin
        ppImgNo.Visible := not pClienteAdheridoEnvioEMail;
        ppImgSi.Visible := pClienteAdheridoEnvioEMail;
    end;

    pplblFecha.Visible := not pSinFechaContrato;

        if not ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_ANEXO_CONTRATO_PAK, lPlantillaAnexoPAK) then begin
        	raise Exception.Create(Format(MSG_NO_EXISTE_PARAMETRO, [PLANTILLA_ANEXO_CONTRATO_PAK]));
        end;
 //INICIO: TASK_013_ECA_20160520
//        if pSituacionesARegularizar <> EmptyStr then begin
//            ppRichText1.LoadFromFile(lPlantillaAnexoPAK);
//            ppRichText1.RichText := ReplaceTag(ppRichText1.RichText,'NUMERO_RUT', pplblRUT.Caption);
//            ppRichText1.RichText := ReplaceTag(ppRichText1.RichText,'A_REGULARIZAR', pSituacionesARegularizar);
//
//            ppSubReport3.Visible := True;
//            pplblContratoConAnexo.Visible := True;
//        end
//        else begin
//            ppSubReport3.Visible := False;
//            pplblContratoConAnexo.Visible := False;
//        end;
//FIN: TASK_013_ECA_20160520
    Result := True;
    except
    	on e: Exception do begin
        	raise Exception.Create(e.Message);
        end;
    end;
end;

procedure TReporteContratoAdhesionPAForm.ImprimirPDF(pNombreArchivo: string);

begin
	try
    	PeaProcsCN.ExportarReporteAPDF(prprtContrato, pNombreArchivo);
    except
    	on e: Exception do begin
            raise Exception.Create(e.Message);
        end;
    end;
end;

procedure TReporteContratoAdhesionPAForm.ConfigurarImpresionContrato(pImprimirContrato, pImprimirAnexo: Boolean);
	resourcestring
        MSG_CAPTION_CONTRATO = 'Impresi�n Contrato Adhesi�n Lista Blanca';
        MSG_CAPTION_ANEXO    = 'Impresi�n Anexo Contrato Adhesi�n Lista Blanca';
begin
    ppSubReport1.Visible := ppSubReport1.Visible and pImprimirContrato;
    ppSubReport2.Visible := pImprimirContrato;
    ppSubReport3.Visible := pImprimirAnexo;

    if pImprimirContrato then begin
        RBInterface.Caption := MSG_CAPTION_CONTRATO;
    end
    else begin
    	if pImprimirAnexo then begin
	        RBInterface.Caption := MSG_CAPTION_ANEXO;
        end;
    end;
end;


end.
