{-----------------------------------------------------------------------------
 File Name: Frm_AgregarItemFacturacion.pas
 Author:    Flamas
 Date Created: 24/01/2005
 Language: ES-AR
 Description: Agrega un item a ser facturado

 Revision: 1
 Author:    ggomez
 Date:      05/07/2005
 Description:
    - Cuando el tipo de Comprobante es Boleta se checkea por defecto que el
    Importe incluye el IVA.

 Revision: 2
 Author:    ggomez
 Date:      06/07/2005
 Description:
    - Cuando el tipo de Comprobante es Boleta se deshabilita la fecha.

-----------------------------------------------------------------------------}
unit Frm_AgregarItemFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, VariantComboBox, DmiCtrls, DB, ADODB,
  PeaProcs, Util, ExtCtrls, UtilDB,PeaTypes, TimeEdit;

type
  TfrmAgregarItemFacturacion = class(TForm)
    Label1: TLabel;
    edFecha: TDateEdit;
    Label2: TLabel;
    cbConcepto: TVariantComboBox;
    btnCancelar: TButton;
    btnAceptar: TButton;
    Label3: TLabel;
    spObtenerConceptosComprobante: TADOStoredProc;
    Label4: TLabel;
    edImporte: TNumericEdit;
    edDetalle: TEdit;
    Bevel1: TBevel;
    chk_ImporteIncluyeIVA: TCheckBox;
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    procedure CargarCombo( sTipoComprobante : string );
  public
    { Public declarations }
    function Inicializar( sTipoComprobante : string; dFecha : TDateTime ) : Boolean; overload;
    function Inicializar( sTipoComprobante : string; dFecha : TDateTime; nCodigoConcepto, nImporte: integer; sDecripcion : string ) : Boolean; overload;
  end;

var
  frmAgregarItemFacturacion: TfrmAgregarItemFacturacion;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 24/01/2005
  Description: Inicializa el Form - Carga los Conceptos de Facturación
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmAgregarItemFacturacion.Inicializar( sTipoComprobante : string; dFecha : TDateTime) : Boolean;
begin
	result := True;
    edFecha.Date := dFecha;
	CargarCombo( sTipoComprobante );
    chk_ImporteIncluyeIVA.Checked := False;
    if sTipoComprobante = TC_BOLETA then begin
        edFecha.Enabled                 := False;
        chk_ImporteIncluyeIVA.Visible   := True;
        chk_ImporteIncluyeIVA.Checked   := True;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 24/01/2005
  Description: Inicializa el Form - Carga los Conceptos de Facturación
  				y los datos del item para modificarlo
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmAgregarItemFacturacion.Inicializar( sTipoComprobante : string; dFecha : TDateTime; nCodigoConcepto, nImporte: integer; sDecripcion : string) : Boolean;
begin
	result := Inicializar( sTipoComprobante, dFecha );
    cbConcepto.Value := nCodigoConcepto;
    edImporte.ValueInt := nImporte;
    edDetalle.Text := sDecripcion;
    chk_ImporteIncluyeIVA.Checked := False;
    if sTipoComprobante = TC_BOLETA then begin
        chk_ImporteIncluyeIVA.Visible := True;
        chk_ImporteIncluyeIVA.Checked := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCombo
  Author:    flamas
  Date Created: 24/01/2005
  Description: Carga los conceptos de facturación
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TfrmAgregarItemFacturacion.CargarCombo( sTipoComprobante : string );
begin
	with spObtenerConceptosComprobante do begin
    	Parameters.ParamByName( '@TipoComprobante' ).Value := sTipoComprobante;
    	Open;
        while not EOF do begin
        	cbConcepto.Items.Add( Trim( FieldByName( 'Descripcion' ).AsString ),
            						FieldByName( 'CodigoConcepto' ).AsString );
        	Next;
        end;
        Close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    flamas
  Date Created: 24/01/2005
  Description: Valida los valores de los campos para poder aceptar
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TfrmAgregarItemFacturacion.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_DATE_MUST_HAVE_A_VALUE 		= 'Debe especificar una fecha.';
    MSG_CONCEPT_MUST_HAVE_A_VALUE 	= 'Debe especificar un concepto';
    MSG_AMMOUNT_MUST_HAVE_A_VALUE 	= 'Debe especificar un monto';
begin
    if ValidateControls(
    	[edFecha, cbConcepto, edImporte],
    	[edFecha.Date <> nulldate, cbConcepto.ItemIndex >= 0, edImporte.ValueInt > 0],
    	caption,
        [MSG_DATE_MUST_HAVE_A_VALUE, MSG_CONCEPT_MUST_HAVE_A_VALUE, MSG_AMMOUNT_MUST_HAVE_A_VALUE]) then
    	ModalResult := mrOK;
end;

end.
