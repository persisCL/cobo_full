{********************************** Unit Header ********************************
File Name : ConstParametrosGenerales.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
    Author : ggomez
    Date : 04/07/2006
    Description : Agregu� la constante INTERVALO_REFRESCO_LISTA_TAREAS.

Revision 1:
    Author : nefernandez
    Date : 19/11/2007
    Description : Agregu� la constante DIAS_DIF_MIN_FECHAS_CUENTAS_REIMPRIMIR.
Revision 3:
    Author : lcanteros
    Date : 15/07/2008
    Description : Agregu� las constantes
        TIME_OUT_CONSULTAS_GESTION_INFORMES_MINIMO
        TIME_OUT_CONSULTAS_INFORMES

Revisi�n 4:
   Author       : Nelson Droguett Sierra (pdominguez)
   Date         : 07-Junio-2011
   Description  :
        -Agregu� las siguientes constantes debido al cambio en el formato de
    almacenamiento de las imagenes:
            DIR_IMAGENES_PATENTES_NUEVO_FORMATO_IMAGEN
            DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN
            DIR_IMAGENES_EXISTENTES_NUEVO_FORMATO_IMAGEN
    Firma       : SS-377-NDR-20110607 

Revision    : 5
Author      : Nelson Droguett Sierra
Date        : 05-Mayo-2011
Description : (SS-231)
                Se agrega los siguientes parametros :
                DIR_IMAGENES_TRANSITOS_INFRACTORES
                ALMACENA_IMAGENES_INFRAC_FRONTAL
                ALMACENA_IMAGENES_INFRAC_POSTERIOR
                ALMACENA_IMAGENES_INFRAC_GENERAL1
                ALMACENA_IMAGENES_INFRAC_GENERAL2
                ALMACENA_IMAGENES_INFRAC_GENERAL3
Firma       : SS-231-NDR-20110505

Autor		: Claudio Quezada Ib��ez
Fecha		: 06-Noviembre-2013
Firma		: SS_1135_CQU_20131030
Descripcion	: Se agrega los siguientes parametros :
                ALMACENA_IMAGENES_INFRAC_FRONTAL2
                ALMACENA_IMAGENES_INFRAC_POSTERIOR2

Autor		: Claudio Quezada Ib��ez
Fecha		: 03-Enero-2014
Firma		: SS_1151_CQU_20140103
Descripcion	: Se agrega el par�metro CANT_MAX_PATENTES_PROCESAR, indicar�
              la cantidad m�xima de patentes a procesar por el webservice de
              AutoInscripci�n de PA

Firma       : SS_1397_MGO_20151015
Descripcion : Se agrega par�metro LOGO_REPORTES_WEB

Firma       : SS_1397_MCA_20160105
Descripcion : Se agrega par�metro LOGO_REPORTES_WS

*******************************************************************************}
unit ConstParametrosGenerales;

interface

uses ADODB, DB, variants, util, eventlog, sysutils;


// Clases de par�metros Generales
const
    CP_SOLICITUD = 'S';
    CP_VALIDACION = 'V';
    CP_INFORMES = 'I';
    CP_MAIL = 'M';


// Tipos de par�metros Generales
const
    TP_FECHA    = 'F';
    TP_NUMERICO = 'N';
    TP_ENTERO   = 'E';
    TP_TEXTO    = 'T';


// Par�metros Generales de Solicitud
const
    PS_Cantidad_Vehiculos_Defecto_WEB = 'Cantidad_Vehiculos_Defecto_WEB';
    Dir_Documentos_FAQ                = 'Dir_Documentos_FAQ';
    DIR_IMAGENES_PREGUNTAS            = 'Dir_Imagenes_Preguntas';
    Dir_Imagenes_Titulos              = 'Dir_Imagenes_Titulos';
    Dir_Imagenes_Sub_Titulos          = 'Dir_Imagenes_Sub_Titulos';
    Dir_Plantillas_WEB                = 'Dir_Plantillas_WEB';
    LOGO_FACTURACION                  = 'LOGO_FACTURACION';                      //TASK_052_JMA_20160926
    LOGO_REPORTES                     = 'Logo_Reportes';
    LOGO_REPORTES_WS                  = 'Logo_Reportes_WS';                     //SS_1397_MCA_20160105
    LOGO_REPORTES_WEB                 = 'Logo_Reportes_WEB';                    //SS_1397_MGO_20151015
    TEXTO_LOGO_REPORTES               = 'Texto_Logo_Reportes';
    PS_EDAD_MINIMA                    = 'Edad_Minima_Solicitud';
    Dir_Importacion_Vehiculos         = 'Directorio_Importacion_Vehiculos';
    Dir_Importacion_Santander         = 'Directorio_Importacion_Santander';
    Dir_Importacion_Citas             = 'Directorio_Importacion_Citas';
    DIR_IMAGENES_TRANSITOS_PENDIENTES = 'Dir_Imagenes_Transitos_Pendientes';
    DIR_IMAGENES_PATENTES             = 'DIR_IMAGENES_PATENTES';
    DIR_IMAGENES_TRANSITOS            = 'DIR_IMAGENES_TRANSITOS';
    //Rev.5 / 05-Mayo-2011 / Nelson Droguett Sierra-----------------------------------------------------
    DIR_IMAGENES_TRANSITOS_INFRACTORES = 'DIR_IMAGENES_TRANSITOS_INFRACTORES';    //SS-231-NDR-20110505
    ALMACENA_IMAGENES_INFRAC_FRONTAL   ='ALMACENA_IMAGENES_INFRAC_FRONTAL';       //SS-231-NDR-20110505
    ALMACENA_IMAGENES_INFRAC_POSTERIOR ='ALMACENA_IMAGENES_INFRAC_POSTERIOR';     //SS-231-NDR-20110505
    ALMACENA_IMAGENES_INFRAC_GENERAL1  ='ALMACENA_IMAGENES_INFRAC_GENERAL1';      //SS-231-NDR-20110505
    ALMACENA_IMAGENES_INFRAC_GENERAL2  ='ALMACENA_IMAGENES_INFRAC_GENERAL2';      //SS-231-NDR-20110505
    ALMACENA_IMAGENES_INFRAC_GENERAL3  ='ALMACENA_IMAGENES_INFRAC_GENERAL3';      //SS-231-NDR-20110505
    ALMACENA_IMAGENES_INFRAC_FRONTAL2  ='ALMACENA_IMAGENES_INFRAC_FRONTAL2';      // SS_1135_CQU_20131030
    ALMACENA_IMAGENES_INFRAC_POSTERIOR2='ALMACENA_IMAGENES_INFRAC_POSTERIOR2';    // SS_1135_CQU_20131030

    DIR_DESTINO_XML_INFRACCIONES      = 'DIR_Destino_XML_Infracciones';
    INFRACCIONES_XML_TAMANIO_CD       = 'Infracciones_XML_Tama�o_CD';
    LIMITE_CANTIDAD_IGNORADO          = 'CantidadVecesIgnoraTransito';
    UMBRAL_CONF_CARACTER_OCR          = 'UmbralConfiabilidadCaracterOCR';
    PARAM_EMAIL_ORIGEN_SOLICITUD      = 'Email_Origen_Solicitud';
    PARAM_PLANTILLA_EMAIL_ALTA_SOLICITUD = 'PLANTILLA_EMAIL_ALTA_SOLICITUD';
    SCRIPT_SOLICITUD_CONTACTO         = 'SCRIPT_SOLICITUD_CONTACTO';
    DIR_IMAGENES_VARIAS               = 'Dir_Imagenes_Varias';
    DEADLINE_SUSPENSION               = 'DEADLINE_SUSPENSION';

    //impresion convenio
    PLANTILLA_CONVENIO_PERSONA_NATURAL = 'Plantilla_Convenio_Persona_Natural';
    PLANTILLA_CONVENIO_PERSONA_JURIDICA = 'Plantilla_Convenio_Persona_Juridica';
    PLANTILLA_BAJA_CONVENIO_NATURAL    = 'PLANTILLA_BAJA_CONVENIO_NATURAL';
    PLANTILLA_BAJA_CONVENIO_JURIDICA   = 'PLANTILLA_BAJA_CONVENIO_JURIDICA';
    PLANTILLA_MANDATO_PAC               = 'Plantilla_Mandato_PAC';
    PLANTILLA_MANDATO_PAT               = 'Plantilla_Mandato_PAT';
    PLANTILLA_CONTRATO_MOTO             = 'Plantilla_Contrato_Moto';
    PLANTILLA_ANEXO_VEHICULOS_NATURAL = 'Plantilla_Anexo_Vehiculos_Natural';
    PLANTILLA_ANEXO_VEHICULOS_FISICA = 'Plantilla_Anexo_Vehiculos_Fisica';

    PLANTILLA_ANEXO_MOTOS = 'Plantilla_Anexo_Motos';

//    PLANTILLA_ABM_CUENTA_JURIDICA = 'PLANTILLA_ABM_CUENTA_JURIDICA';
//    PLANTILLA_ABM_CUENTA_NATURAL  = 'PLANTILLA_ABM_CUENTA_NATURAL';
//    PLANTILLA_INDI_VEHICULOS = 'PLANTILLA_INDI_VEHICULOS';
//    PLANTILLA_ANEXO_VEHICULOS_JURIDICA_MODIFICACION_CONVENIO = 'Plantilla_AnexoVehiculoJuridicaMod';
//    PLANTILLA_ANEXO_VEHICULOS_NATURAL_MODIFICACION_CONVENIO = 'Plantilla_AnexoVehiculoFisicaMod';
//    PLANTILLA_ANEXO_VEHICULOS_JURIDICA_BAJA_CONVENIO = 'Plantilla_AnexoVehiculoJuridicaBaja';
//    PLANTILLA_ANEXO_VEHICULOS_NATURAL_BAJA_CONVENIO = 'Plantilla_AnexoVehiculoFisicaBaja';



    PLANTILLA_CARATULA_JURIDICA       = 'PLANTILLA_CARATULA_JURIDICA';
    PLANTILLA_CARATULA_NATURAL        = 'PLANTILLA_CARATULA_NATURAL';

    CAN_COPIAS_CONVENIO_JURIDICA=       'CAN_COPIAS_CONVENIO_JURIDICA';
    CAN_COPIAS_CONVENIO_FISICA=         'CAN_COPIAS_CONVENIO_FISICA';
    CAN_COPIAS_MANDATO_PAC=             'CAN_COPIAS_MANDATO_PAC';
    CAN_COPIAS_MANDATO_PAT=             'CAN_COPIAS_MANDATO_PAT';
    CAN_COPIAS_MANDATO_PAT_PRESTO =     'CAN_COPIAS_MANDATO_PAT_PRESTO';
    CAN_COPIAS_CARATULA=                'CAN_COPIAS_CARATULA';
    CAN_COPIAS_ESTADO_CONVENIO=         'CAN_COPIAS_ESTADO_CONVENIO';

    CAN_COPIAS_BAJA_CUENTA =            'CAN_COPIAS_BAJA_CUENTA';
    CAN_COPIAS_ALTA_CUENTA =            'CAN_COPIAS_ALTA_CUENTA';
    CAN_COPIAS_BAJA_CONVENIO =          'CAN_COPIAS_BAJA_CONVENIO';


    ROOT_TEMPLATE_REPORTES   =          'ROOT_TEMPLATE_REPORTES';

    XML_PARAMETRO_ARCHIVO_XML               = 'PATH_ARCHIVO_SALIDA_XML';
    XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS    = 'PATH_ARCHIVO_SALIDA_XML_ENVIADOS';
    XML_PATH_ARCHIVO_ENTRADA_XML            = 'PATH_ARCHIVO_ENTRADA_XML';
    XML_PATH_ARCHIVO_ENTRADA_XML_RECIBIDOS  = 'PATH_ARCHIVO_ENTRADA_XML_RECIBIDOS';

    DIAS_EMISION_COMPROBANTE              = 'DIAS_EMISION_COMPROBANTE';
    DIAS_VENCIMIENTO_COMPROBANTE          = 'DIAS_VENCIMIENTO_COMPROBANTE';         
    DIAS_DIF_MIN_FECHAS_CORTE_SEMANAL     = 'DIAS_DIF_MIN_FECHAS_CORTE_SEMANAL';    // TASK_133_MGO_20170207
    DIAS_DIF_MIN_FECHAS_CORTE_QUINCENAL   = 'DIAS_DIF_MIN_FECHAS_CORTE_QUINCENAL';
    DIAS_DIF_MIN_FECHAS_CORTE_MENSUAL     = 'DIAS_DIF_MIN_FECHAS_CORTE_MENSUAL';
    DIAS_DIF_MIN_FECHAS_CORTE_BIMESTRAL   = 'DIAS_DIF_MIN_FECHAS_CORTE_BIMESTRAL';
    PORCENTAJE_GASTOS_COBRANZAS_BI        = 'PORCENTAJE_GASTOS_COBRANZAS_BI';
    FAC_FECHAS_SOLO_DIA_HABIL             = 'FAC_FECHAS_SOLO_DIA_HABIL';        // TASK_133_MGO_20170207

    SYNCMOPT_ULTIDCONTRATO                  = 'ULTIDCONTRATO';

    PARAM_M_FECHA_TOPE                      = 'PARAM_M_FECHA_TOPE';
    PARAM_EPS                               = 'PARAM_EPS';
    PARAM_SERVICIO                          = 'PARAM_SERVICIO';

    INTERVALO_REFRESCO_LISTA_TAREAS         = 'INTERVALO_REFRESCO_LISTA_TAREAS';

    DIAS_DIF_MIN_FECHAS_CUENTAS_REIMPRIMIR  = 'DIAS_DIF_CUENTAS_REIMPRIMIR';

    TIME_OUT_CONSULTAS_GESTION_INFORMES_MINIMO = 30;
    TIME_OUT_CONSULTAS_INFORMES = 'TIME_OUT_CONSULTAS_INFORMES';

    // Para migraci�n de estructura de Imagenes en Archivo �nico a Archivos Individuales              //SS-377-NDR-20110607
    DIR_IMAGENES_PATENTES_NUEVO_FORMATO_IMAGEN   = 'DIR_IMAGENES_PATENTES_NFI';                       //SS-377-NDR-20110607
    DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN  = 'DIR_IMAGENES_TRANSITOS_NFI';                      //SS-377-NDR-20110607
    DIR_IMAGENES_EXISTENTES_NUEVO_FORMATO_IMAGEN = 'DIR_IMAGENES_EXISTENTES_NFI';                     //SS-377-NDR-20110607
    //                                                                                                //SS-377-NDR-20110607

    CANT_MAX_PATENTES_PROCESAR              =   'CANT_MAX_PATENTES_PROCESAR';       // SS_1151_CQU_20140103
    WEBSERVICE_PAK_INFORMAR_DETALLE         =   'WEBSERVICE_PAK_INFORMAR_DETALLE';  // SS_1151_CQU_20140103

    //INICIO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal
    VALOR_MAX_RUT_REP_LEGAL					= 49999999;
    //TERMINO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal


function ObtenerParametroGeneral(Conn: TADOConnection;Nombre: AnsiString; Var Valor: AnsiString): Boolean; overload;
function ObtenerParametroGeneral(Conn: TADOConnection;Nombre: AnsiString; Var Valor: Integer): Boolean; overload;
function ObtenerParametroGeneral(Conn: TADOConnection;Nombre: AnsiString; Var Valor: TDateTime): Boolean; overload;

// Funciones para servicios
function ObtenerParametroServicio(Conn: TADOConnection; Servicio, Nombre: String; Var Valor : string) : Boolean; overload;
function ObtenerParametroServicio(Conn: TADOConnection; Servicio, Nombre: String; Var Valor : Integer) : Boolean; overload;
function ObtenerParametroServicio(Conn: TADOConnection; Servicio, Nombre: String; Var Valor : TDatetime) : Boolean; overload;
function ObtenerParametroServicio(Conn: TADOConnection; Servicio, Nombre: String; Var Valor : Real) : Boolean; overload;

implementation

procedure ProcesarError(Conn: TADOConnection; Modulo: string; E: Exception);
begin
    try
        EventLogReportEvent(elError, Modulo + ': ' + e.message, '');
        Conn.Close;
    except
    end;
end;

function _ObtenerParametroGeneral(Conn: TADOConnection; Nombre: AnsiString; Var Valor, TipoParametro: AnsiString): Boolean;
resourcestring
	MSG_CANNOT_READ_PARAM = 'No se puede leer el par�metro general "%s". El par�metro no existe.';
var
	SP: TAdoStoredProc;
begin
	Result := False;
	SP := TAdoStoredProc.Create(nil);
	try
		SP.Connection := Conn;
		SP.ProcedureName := 'ObtenerParametroGeneral';
		SP.Parameters.Refresh;
		SP.Parameters.ParamByName('@Nombre').Value := Nombre;
		SP.Parameters.ParamByName('@Valor').Value := NULL;
		SP.Parameters.ParamByName('@TipoParametro').Value := NULL;
		SP.ExecProc;
		TipoParametro := Trim(VarToStr(SP.Parameters.ParamByName('@TipoParametro').Value));
		if TipoParametro = '' then raise exception.CreateFmt(MSG_CANNOT_READ_PARAM, [Nombre]);
		Valor := SP.Parameters.ParamByName('@Valor').Value;
		Result := True;
	except
		on e: exception do ProcesarError(Conn, '_ObtenerParametroGeneral', e);
	end;
	SP.Free;
end;

function ObtenerParametroGeneral(Conn: TADOConnection; Nombre: AnsiString; Var Valor: AnsiString):Boolean;
var
	TipoParametro, ValorAux: String;
begin
	try
		Result := _ObtenerParametroGeneral(Conn, Nombre, ValorAux, TipoParametro);
		Result := Result and (TipoParametro = TP_TEXTO);
		if Result then Valor := Trim(ValorAux)
	except
		// Fall� _ObtenerParametroGeneral(). No hace falta loggear: ella misma lo hace.
		Result := False;
	end;
end;

function ObtenerParametroGeneral(Conn: TADOConnection; Nombre: AnsiString; Var Valor: Integer):Boolean;
var
	TipoParametro, ValorAux: AnsiString;
begin
	try
		Result := _ObtenerParametroGeneral(Conn, Nombre, ValorAux, TipoParametro);
		Result := Result and (TipoParametro = TP_ENTERO);
		Valor :=  Ival(ValorAux);
	except
		on e: exception do begin
			// Fall� el StrToInt. Lo loggeamos.
			ProcesarError(Conn, 'ObtenerParametroGeneral (Integer)', e);
			Result := False;
		end;
	end;
end;

function ObtenerParametroGeneral(Conn: TADOConnection; Nombre: AnsiString; Var Valor: TDateTime):Boolean;
var
	TipoParametro, ValorAux                 : AnsiString;
    Old_TimeAMString, Old_TimePMString      : AnsiString;
    Old_DateSeparator, Old_TimeSeparator    : Char;
    Old_ShortDateFormat, Old_LongTimeFormat : AnsiString;
    TimeAMString, TimePMString              : AnsiString;

begin

    Old_DateSeparator := DateSeparator;
    Old_ShortDateFormat := ShortDateFormat;
    Old_TimeSeparator := TimeSeparator;
    Old_LongTimeFormat := LongTimeFormat;
    Old_TimeAMString := TimeAMString;
    Old_TimePMString := TimePMString;

    DateSeparator       := '/';             // : Char;
    TimeSeparator       := ':';             // : Char;
    ShortDateFormat     := 'dd/mm/yyyy';     // : string;
    LongTimeFormat      := 'hh:nn:ss ampm';       // : string;
    TimeAMString        := 'AM';
    TimePMString        := 'PM';
    try
        try
            Result := _ObtenerParametroGeneral(Conn, Nombre, ValorAux, TipoParametro);
            Result := Result and (TipoParametro = TP_FECHA);
            Valor :=  StrToDateTime(ValorAux);
        except
            on e: exception do begin
                // Fall� el StrToDateTime. Lo loggeamos.
                ProcesarError(Conn, 'ObtenerParametroGeneral (DateTime)', e);
                Result := False;
            end;
        end;
    finally
        DateSeparator   := Old_DateSeparator;
        ShortDateFormat := Old_ShortDateFormat;
        TimeSeparator   := Old_TimeSeparator;
        LongTimeFormat  := Old_LongTimeFormat;
        TimeAMString    := Old_TimeAMString;
        TimePMString    := Old_TimePMString;
    end;
end;

function ObtenerParametroServicio(Conn: TADOConnection; Servicio, Nombre:String; Var Valor:Variant; Var TipoParametro:String):Boolean; overload;
resourcestring
    MSG_PARAMS_ERROR = 'Error al obtener par�metros: ';
var
  ValorDefault : Variant;
begin
    Result := False;
    with TAdoStoredProc.Create(nil) do
    try
        try
            ProcedureName := 'ObtenerParametroServicio';
            Connection := Conn;
            Parameters.CreateParameter('@Return_Value', ftInteger, pdReturnValue, 0, NULL);
            Parameters.CreateParameter('@Servicio', ftString, pdInput, 255, Servicio);
            Parameters.CreateParameter('@Nombre', ftString, pdInput, 35, Nombre);
            Parameters.CreateParameter('@Valor', ftString, pdOutput, 255, NULL);
            Parameters.CreateParameter('@ValorDefault', ftString, pdOutput, 255, NULL);
            Parameters.CreateParameter('@TipoParametro', ftString, pdOutput, 1, NULL);

            ExecProc;
            TipoParametro := VarToStr(Parameters.ParamByName('@TipoParametro').Value);
            Valor         := Parameters.ParamByName('@Valor').Value;
            ValorDefault  := Parameters.ParamByName('@ValorDefault').Value;

            if (Valor = Null) and (ValorDefault <> null) then
                  Valor := ValorDefault;

            if TipoParametro=Null then Result:=False
            else result:=True;
        except
	    	On e: exception do begin
    		    raise Exception.Create(MSG_PARAMS_ERROR +  E.message);
                result := false;
            end;
        end;
    finally
        Close;
        Free;
    end;
end;


function ObtenerParametroServicio(Conn: TADOConnection;Servicio,Nombre:String; Var Valor:Integer):Boolean; overload;
var
    ValorAux : Variant;
    TipoParametro:String;
begin
    Try
        Result:=ObtenerParametroServicio(Conn, Servicio, Nombre, ValorAux, TipoParametro);
        if (TipoParametro = TP_ENTERO) and (Result) then Valor := Integer(ValorAux)
        else Valor:=0;
    except
        on e: exception do begin
            ProcesarError(Conn, 'ObtenerParametroGeneral (Integer)', e);
            Valor:=0;
            Result:=False;
        end;
    end;
end;


function ObtenerParametroServicio(Conn: TADOConnection;Servicio,Nombre:String; Var Valor: String):Boolean; overload;
var
    ValorAux : Variant;
    TipoParametro:String;
begin
    Try
        Result:=ObtenerParametroServicio(Conn, Servicio, Nombre, ValorAux, TipoParametro);
        if (TipoParametro = TP_TEXTO) and (Result) then Valor := VarToStr(ValorAux)
        else Valor:='';
    except
        on e: exception do begin
            ProcesarError(Conn, 'ObtenerParametroGeneral (String)', e);
            Valor:='';
            Result:=False;
        end;
    end;
end;

function ObtenerParametroServicio(Conn: TADOConnection;Servicio,Nombre:String; Var Valor: TDateTime):Boolean; overload;
var
    ValorAux : Variant;
    TipoParametro:String;
begin
    Try
        Result:=ObtenerParametroServicio(Conn, Servicio, Nombre, ValorAux, TipoParametro);
        if (TipoParametro = TP_FECHA) and (Result) then Valor := VarToDateTime(ValorAux)
        else Valor:=NullDate;
    except
        on e: exception do begin
            ProcesarError(Conn, 'ObtenerParametroServicio (DateTime)', e);
            Valor:=NullDate;
            Result:=False;
        end;
    end;
end;


function ObtenerParametroServicio(Conn: TADOConnection;Servicio,Nombre:String; Var Valor: Real):Boolean; overload;
var
    ValorAux : Variant;
    TipoParametro:String;
begin
    Try
        Result:=ObtenerParametroServicio(Conn, Servicio, Nombre, ValorAux, TipoParametro);
        if (TipoParametro = TP_NUMERICO) and (Result) then Valor := real(ValorAux)
        else Valor:=0;
    except
        on e: exception do begin
            ProcesarError(Conn, 'ObtenerParametroGeneral (String)', e);
            Valor:=0;
            Result:=False;
        end;
    end;
end;




end.
