{********************************** File Header ********************************
File Name   : DMEvaluadorWorkflow.pas
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Author      :
Date        :
Description :
*******************************************************************************}
unit DMEvaluadorWorkflow;

interface

uses
  Windows, SysUtils, Classes, DB, ADODB, Evaluate, Variants, UtilDB,
  Util, Forms, OPScripts ;

type
  TDMEvalWorkflow = class(TDataModule)
	qry_Tareas: TADOQuery;
	qry_Secuencias: TADOQuery;
    qry_GrabarTarea: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
  private
	{ Private declarations }
	FPrioridad: Integer;
	TareasOmitidas: set of BYTE;
	Evaluador: TExpressionEvaluator;
	procedure VerificarTareas(CodigoOrdenServicio: Integer);
	procedure VerificarTarea(CodigoOrdenServicio, CodigoTarea: Integer);
	procedure GrabarTarea(CodigoOrdenServicio: Integer);
  public
	{ Public declarations }
	function Ejecutar(CodigoOrdenServicio, CodigoWorkflow: Integer; var DescriError: AnsiString): Boolean;
  end;

var
  DMEvalWorkflow: TDMEvalWorkflow;

implementation

Uses
	DMConnection;

{$R *.dfm}

{ TDataModuleWorkflow }

function TDMEvalWorkflow.Ejecutar(CodigoOrdenServicio, CodigoWorkflow: Integer;
  var DescriError: AnsiString): Boolean;
resourcestring
    MSG_ERROR_TAREAS    = 'Error al abrir "qry_Tareas": %s';
    MSG_PRIORIDAD       = 'Error al obtener la prioridad: %s';
    MSG_SECUENCIAS      = 'Error al abrir "qry_Secuencias": %s';
    MSG_WORKFLOW        = 'Error al grabar el Workflow: %s';
    MSG_INICIO_TAREAS   = 'Error al ejecutar "ActualizarInicioTareas": %s';

//Var
 //	Version: Integer;
begin
	Result := False;
	Evaluador := nil;

	// Obtenemos todas las tareas
	qry_Tareas.Parameters.ParamByName('CodigoWorkflow').Value := CodigoWorkflow;
	try
		qry_Tareas.Open;
		//Version := qry_Tareas.FieldByName('Version').AsInteger;
	except
		on e: exception do begin
			DescriError := Format(MSG_ERROR_TAREAS, [trim(e.message)]);
			Exit;
		end;
	end;

	try
		FPrioridad := QueryGetValueInt(DMConnections.BaseCAC, Format(
		  'SELECT Prioridad FROM OrdenesServicio WHERE CodigoOrdenServicio = %d',
		  [CodigoOrdenServicio]));
	except
		on e: exception do begin
			DescriError := Format(MSG_PRIORIDAD, [trim(e.message)]);
			Exit;
		end;
	end;
	// Obtenemos todas las secuencias condicionales
	qry_Secuencias.Parameters.ParamByName('CodigoWorkflow').Value := CodigoWorkflow;
	try
		qry_Secuencias.Open;
	except
		on e: exception do begin
			DescriError := Format(MSG_SECUENCIAS, [trim(e.message)]);
			Exit;
		end;
	end;
	// Verificamos las tareas
	try
		VerificarTareas(CodigoOrdenServicio);
	except
		on e: exception do begin
			DescriError := e.Message;
			Exit;
		end;
	end;
	(*// Grabamos el Workflow
	qry_GrabarWorkflow.Parameters.ParamByName('CodigoOrdenServicio').Value := CodigoOrdenServicio;
	qry_GrabarWorkflow.Parameters.ParamByName('CodigoWorkflow').Value := CodigoWorkflow;
	try
		qry_GrabarWorkflow.ExecSQL;
	except
		on e: exception do begin
			DescriError := Format(MSG_WORKFLOW, [Trim(e.message)]);
			Exit;
		end;
	end;*)
	// Vamos grabando las tareas, exceptuando las omitidas
	qry_Tareas.First;
	while not qry_Tareas.Eof do begin
		try
			if not (qry_Tareas.FieldByName('CodigoTarea').AsInteger in TareasOmitidas)
			  then GrabarTarea(CodigoOrdenServicio);
		except
			on e: exception do begin
				DescriError := e.Message;
				Exit;
			end;
		end;
		qry_Tareas.Next;
	end;
	// Actualizamos el inicio de las tareas
	try
		QueryExecute(DMConnections.BaseCAC, Format('EXECUTE ActualizarInicioTareas %d', [CodigoOrdenServicio]));
	except
		on e: exception do begin
			DescriError := Format(MSG_INICIO_TAREAS, [Trim(e.message)]);
			Exit;
		end;
	end;
	// Listo
	Result := True;
end;

procedure TDMEvalWorkflow.GrabarTarea(CodigoOrdenServicio: Integer);
resourcestring
    MSG_GRABAR_TAREA = 'Error al grabar tarea: %s';

begin
	try
		qry_GrabarTarea.Parameters.ParamByName('CodigoOrdenServicio').Value := CodigoOrdenServicio;
//		qry_GrabarTarea.Parameters.ParamByName('CodigoWorkflow').Value := qry_Tareas.FieldByName('CodigoWorkflow').AsInteger;
//		qry_GrabarTarea.Parameters.ParamByName('Version').Value := qry_Tareas.FieldByName('Version').AsInteger;
		qry_GrabarTarea.Parameters.ParamByName('CodigoTarea').Value := qry_Tareas.FieldByName('CodigoTarea').AsInteger;
		qry_GrabarTarea.Parameters.ParamByName('Prioridad').Value := FPrioridad;
		qry_GrabarTarea.ExecSQL;
	except
		on e: exception do
            raise exception.Create(Format(MSG_GRABAR_TAREA, [Trim(e.Message)]));
	end;
end;

procedure TDMEvalWorkflow.VerificarTarea(CodigoOrdenServicio, CodigoTarea: Integer);
resourcestring
    MSG_ERROR_EVALUADOR = 'Error al crear el evaluador de expresiones.';
    MSG_EXPRESION_BOOLEANA = 'La expresi�n no es booleana.';
    MSG_EVALUAR = 'Error al evaluar la expresi�n "%s": %s';
Var
	V: Variant;
	Exp: AnsiString;
	CondicionOk: Boolean;
begin
	// Si ya est� omitida, no hace falta seguir
	if CodigoTarea in TareasOmitidas then Exit;
	// Si no hay secuencias condicionales para esta tarea, est� ok.
	qry_Secuencias.Filter := 'CodigoTarea = ' + IntToStr(CodigoTarea);
	qry_Secuencias.Filtered := True;
	qry_Secuencias.First;
	if qry_Secuencias.Eof then Exit;
	// Si no tenemos creado el evaluador, lo creamos ahora
	if Evaluador = nil then begin
		Evaluador := CrearEvaluadorOrdenServicio(DMConnections.BaseCAC, CodigoOrdenServicio);
		if Evaluador = nil then raise exception.Create(MSG_ERROR_EVALUADOR);
	end;
	// Verificamos todas las condiciones
	CondicionOk := False;
	While not CondicionOk and not qry_Secuencias.Eof do begin
		try
			if qry_Secuencias.FieldByName('CodigoTareaAnterior').AsInteger in
			  TareasOmitidas then begin
				// La tarea predecesora est� omitida
			end else begin
				Exp := Trim(qry_Secuencias.FieldByName('Condicion').AsString);
				if Trim(Exp) = '' then
					CondicionOk := True
				else begin
					V := Evaluador.Evaluate(Exp);
					if VarType(V) <> varBoolean then
                        raise exception.create(MSG_EXPRESION_BOOLEANA);
					CondicionOk := V;
				end;
			end;
		except
			on e: exception do begin
				raise exception.Create(Format(MSG_EVALUAR, [trim(Exp), Trim(e.message)]));
			end;
		end;
		qry_Secuencias.Next;
	end;
	if not CondicionOk then Include(TareasOmitidas, CodigoTarea);
end;

procedure TDMEvalWorkflow.DataModuleDestroy(Sender: TObject);
begin
	if Evaluador <> nil then Evaluador.Free;
end;

procedure TDMEvalWorkflow.VerificarTareas(CodigoOrdenServicio: Integer);
Var
	i: Integer;
begin
	TareasOmitidas := [];
	for i := 1 to qry_Tareas.RecordCount do begin
		qry_Tareas.First;
		while not qry_Tareas.Eof do begin
			VerificarTarea(CodigoOrdenServicio,
			  qry_Tareas.FieldByName('CodigoTarea').AsInteger);
			qry_Tareas.Next;
		end;
	end;
end;


end.
