object frmLoginSetup: TfrmLoginSetup
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n de Instalaci'#243'n de la Aplicaci'#243'n'
  ClientHeight = 227
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    428
    227)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 3
    Width = 401
    Height = 182
  end
  object btnAceptar: TButton
    Left = 255
    Top = 195
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancelar: TButton
    Left = 335
    Top = 195
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    Default = True
    ModalResult = 2
    TabOrder = 1
  end
  object Secciones: TCategoryButtons
    Left = 24
    Top = 16
    Width = 377
    Height = 153
    ButtonFlow = cbfVertical
    ButtonOptions = [boAllowReorder, boFullSize, boGradientFill, boShowCaptions, boBoldCaptions, boCaptionOnlyBorder]
    Categories = <>
    RegularButtonColor = clBtnFace
    SelectedButtonColor = clActiveCaption
    TabOrder = 2
  end
end
