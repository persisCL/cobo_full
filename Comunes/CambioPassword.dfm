object FormCambioPassword: TFormCambioPassword
  Left = 230
  Top = 222
  BorderStyle = bsDialog
  Caption = 'Cambiar Contrase'#241'a'
  ClientHeight = 144
  ClientWidth = 326
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 7
    Top = 24
    Width = 108
    Height = 13
    Caption = 'Contrase'#241'a actual:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 7
    Top = 52
    Width = 109
    Height = 13
    Caption = 'Nueva contrase'#241'a:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 7
    Top = 80
    Width = 178
    Height = 13
    Caption = 'Confirmar la nueva contrase'#241'a:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 0
    Top = 8
    Width = 321
    Height = 99
  end
  object Edit1: TEdit
    Left = 191
    Top = 20
    Width = 116
    Height = 21
    Color = 16444382
    PasswordChar = '*'
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 191
    Top = 49
    Width = 116
    Height = 21
    Color = 16444382
    PasswordChar = '*'
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 191
    Top = 77
    Width = 116
    Height = 21
    Color = 16444382
    PasswordChar = '*'
    TabOrder = 2
  end
  object Button1: TButton
    Left = 166
    Top = 112
    Width = 75
    Height = 28
    Caption = '&Aceptar'
    Default = True
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 246
    Top = 112
    Width = 75
    Height = 28
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
  end
  object ValidarLogin: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ValidarLogin'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NombreCompleto'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 80
        Value = Null
      end
      item
        Name = '@DescriError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end>
    Left = 8
    Top = 112
  end
end
