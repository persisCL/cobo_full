{********************************** File Header ********************************
File Name   : Navigator.pas
Author      :
Date Created:
Language    : ES-AR
Description :

Revision 1:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

*******************************************************************************}
unit Navigator;

interface

uses
  Windows, Messages, Classes, Dialogs, Forms, ExtCtrls, Controls, IconBars,
  Graphics, sysutils, StdCtrls, Dmictrls, ComCtrls, ToolWin, Math,
  ActnList, Util, UtilProc, Menus, DB, AdoDB, Variants, UtilDB,
  FrmCancelarTarea, FrmTerminarTarea, PeaTypes, DBListEx, OrdenesServicio,
  listboxex, ConstParametrosGenerales;

type
  TNavigator = class;
  TNavCapability = (ncFind, ncFilter, ncRefresh);
  TNavCapabilities = set of TNavCapability;

  TEventoHotLink = procedure(Params: AnsiString) of object;

  THotLinkData = record
    Params: AnsiString;
    Evento: TEventoHotLink;
  end;

  TNavWindowFrm = class(TForm)
  private
	FNavigator: TNavigator;
	{ Private declarations }
  protected
	{ Protected declarations }
	function  GetCapabilities: TNavCapabilities; virtual;
	procedure Loaded; override;
	procedure DoClose(var Action: TCloseAction); override;
	procedure AddFilterText(Filter:AnsiString);
	procedure ClearFilters;
	procedure ControlsAligned; override;
  public
	{ Public declarations }
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; virtual;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; virtual;
	Function FindFirst(SearchText: AnsiString): Boolean; virtual;
	Function FindNext(SearchText: AnsiString): Boolean; virtual;
	Function Filter(var FilterBookmak, FilterName: AnsiString): Boolean; virtual;
	Function CanTerminate: Boolean; virtual;
	Procedure RefreshData; virtual;
	Procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    function Inicializa: Boolean; overload; virtual;
	function Inicializa(MDIChild: Boolean): Boolean; overload; virtual;
	function Inicializa(ArchiveFile, ReportName, ReportTitle: AnsiString; CodigoCliente: integer): Boolean; overload; virtual;
  published
	{ Published declarations }
	property Navigator: TNavigator Read FNavigator;
	property Capabilities: TNavCapabilities read GetCapabilities;
  end;

  TNavWindowClass = class of TNavWindowFrm;

  THistory = class(TComponent)
  private
	{ Private declarations }
  protected
	{ Protected declarations }
	FItemIndex: integer;
	FHistoryList: TStringList;
	Function  GetItemIndex: integer;
	Function  GetItemsCount: integer;
	Procedure SetItemIndex(const Value: integer);
	Function  GetDescription(Index: integer): AnsiString;
	Function  GetBookmark(Index: integer): AnsiString;
	Function  GetObject(Index: integer): TNavWindowClass;
  public
	{ Public declarations }
	Constructor Create(AOwner: TComponent); override;
	Procedure   AddToHistory(Pagina: TNavWindowClass; BookMark, Descripcion: Ansistring);
	Procedure   RemoveFwdHistory;
	property    Description[Index: Integer]: AnsiString read GetDescription;
	property    Bookmark[Index: Integer]: AnsiString read GetBookmark;
	property    WindowClass[Index: Integer]: TNavWindowClass read GetObject;
  published
	{ Published declarations }
	property ItemIndex: integer Read GetItemIndex Write SetItemIndex;
	property ItemsCount: integer Read GetItemsCount;
  end;

  TSeparator = class(TPanel)
  public
  	constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;


  TNavigator = class(TComponent)
  private
	{ Private declarations }
	HL: THistory;
	FTimer: TTimer;
	FNextBlink: Int64;
	lbl_Filter: TLabel;
	FCoolBar: TCoolbar;
	FTaskList: TDBListEx;
	FAntMDIChild: TForm;
	cb_Filter: TComboBox;
	FTLFadeBox: TFadeBox;
	FHotLinkImage: TImage;
	FFwdPopUp: TPopUpMenu;
	ScrollBox: TScrollBox;
	FTaskListImage: TImage;
	FImageList: TImageList;
	FBackPopUp: TPopUpMenu;
	FNextTaskReload: Int64;
	FSearchText: AnsiString;
    FHotlinksCount: integer;
	BarraIzquierda: TIconBar;
	FAntFilterIndex: Integer;
	FToolImageList: TImageList;
	FHLWindowList: TStringList;
	FTLWindowList: TStringList;
	FIBWindowList: TStringList;
	FSP_OrdenesServicio: TAdoStoredProc;
	FToolHotImageList: TImageList;
	FEventoHotLink: TEventoHotLink;     // Pr�ximo evento a ejecutar
	FEventoHotLinkParams: AnsiString;   // Par�metros del pr�ximo evento a ejecutar
	FCurrentNavWindow: TNavWindowFrm;
	FHotLinkData: Array[0..100] of THotLinkData;
	PanelIzquierda, PanelDerecho, PanelInferior, PanelTrucho: TPanel;
	btn_Back, btn_Fwd, btn_Refresh, btn_find: TToolButton;
    FConnCOP: TAdoCOnnection;
	FConnCAC: TAdoCOnnection;
	FHotlinkTitle: AnsiString;
	FLabelHotLinkTitle: TLabel;
	FImageHotLinkTitle: TImage;
	FSeparators: integer;
    FPuntoVenta: Integer;
    FPuntoEntrega: Integer;
    // Cantidad de milisegundos de intervalo para el refresco de la lista de tareas.
    FIntervaloRefrescoListaTareas: Integer;
	procedure AnimateHotLinks;
	procedure AnimateTaskList;
	procedure AnimateIconBar;
	function  GetHLVisible: Boolean;
	procedure SetHLVisible(const Value: Boolean);
	function  GetTLVisible: Boolean;
	procedure SetTLVisible(const Value: Boolean);
	procedure SetActivePage(const Value: Integer);
	function  GetActivePage: Integer;
	procedure SetConnCAC(const Value: TAdoCOnnection);
	procedure SetConnCOP(const Value: TAdoCOnnection);
	procedure setHotLinksTitle(const Value: AnsiString);
	function  GetIBVisible: Boolean;
	procedure SetIBVisible(const Value: Boolean);
    procedure EjecutarDevolverTarea(OS, Tarea: integer);
    procedure SetPuntoEntrega(const Value: Integer);
    procedure SetPuntoVenta(const Value: Integer);
    procedure FTaskListColumns0HeaderClick(Sender: TObject); //permite ordenar haciendo click en las columnas
    procedure FTaskListLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);   //permite editar la orden haciendo click en el link
    procedure FTaskListMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer); //muestra el usuario que bloqueo la orden
    function  GetIconBarItem( const PageName, ItemTitle : AnsiString ) : TIconBarItem;
    function GetShowBrowserBar: Boolean;
    procedure SetShowBrowserBar(const Value: Boolean);
    function GetShowHotLinkBar: Boolean;
    procedure SetShowHotLinkBar(const Value: Boolean);
    function GetIntervaloRefrescoListaTareas: Integer;
    procedure SetIntervaloRefrescoListaTareas(const Value: Integer);
  protected
	FAddHistory: boolean;
	procedure ReloadTasks; virtual;
	procedure DoHotLinkClick(Sender: TObject);
	procedure DoRefresh(Sender: TObject);
	procedure DoFind(Sender: TObject);
	procedure DoTimer(Sender: TObject); virtual;
	procedure DoFilterClick(Sender: TObject);
	procedure BringObjects(Form: Tform);
	procedure DoAnimateHotLinks(Sender: TObject); virtual;
	procedure DoAnimateTaskList(Sender: TObject); virtual;
	procedure DoMouseEnter(Sender: TObject);
	procedure DoMouseLeave(Sender: TObject);
	procedure DoPanelIzquierdaResize(Sender: TObject);
	procedure DoGetHistoryFwdList(Sender: TObject);
	procedure DoGetHistoryBackList(Sender: TObject);
	procedure DoHistoryClick(Sender: TObject);
	procedure Notification(AComponent: TComponent; Operation: TOperation); override;
	procedure ArrangeHLWindows(Open: Boolean);
	procedure ArrangeIBWindows(Open: Boolean);
	procedure ArrangeTLWindows(Open: Boolean);
	Procedure DoGoBack(Sender: TObject);
	Procedure DoGoForward(Sender: TObject);
	Procedure ActualizarBotones;
	procedure AutomatizarTarea(CodigoOrdenServicio: integer; CodigoProceso: integer = 0);
	Function  DoJumpTo(WindowClass: TNavWindowClass; Agregar: Boolean; Bookmark: AnsiString): Boolean;
    procedure DoTaskDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure DoOSChanged;
	procedure DoComenzarTarea(Sender: TObject);
	procedure DoDevolverTarea(Sender: TObject);
	procedure DoTerminarTarea(Sender: TObject);
	procedure DoCancelarTarea(Sender: TObject);
	procedure DoTareasContextMenu(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure DoAutomatizarTarea(Sender: TObject);
  public
	constructor Create(Aowner: TComponent); override;
	destructor Destroy; override;
	procedure AddPage(const PageName, PageTitle, Hint: AnsiString; PageColor: TColor = $00FAF8F2);
	procedure AddIcon(const PageName, ItemTitle, Hint: Ansistring; Bitmap: TBitmap; Event: TNotifyEvent);
	procedure AddSeparator(Alto: integer; Color: TColor = clBtnFace);
	procedure AddHotLink(const ItemTitle, Hint: Ansistring; Fuente: TFont; Bitmap: TBitmap; Event: TEventoHotLink; Params: AnsiString = '');
	procedure ClearHotLinks;
	Procedure CloseNavWindow(Window: TNavWindowFrm);
	Procedure GoBack;
	Procedure GoForward;
	Procedure RefreshData;
    procedure SetIconEnabled( const PageName, ItemTitle : AnsiString; const Value : boolean);
	Function  FindFirst(SearchText: AnsiString): Boolean;
	function  JumpTo(WindowClass: TNavWindowClass; Bookmark: AnsiString = ''): Boolean;
  published
	property CurrentNavWindow: TNavWindowFrm Read FCurrentNavWindow;
	property HotLinksVisible: Boolean Read GetHLVisible Write SetHLVisible;
	property TaskListVisible: Boolean Read GetTLVisible Write SetTLVisible;
	property IconBarActivePage: Integer Read GetActivePage Write SetActivePage;
	property ConnectionCOP: TAdoCOnnection Read FConnCOP Write SetConnCOP;
	property ConnectionCAC: TAdoCOnnection Read FConnCAC Write SetConnCAC;
	property Coolbar: TCoolbar Read FCoolBar;
	property HotLinksTitle: AnsiString write SetHotLinksTitle;
	property IconBarVisible: Boolean Read GetIBVisible write SetIBVisible;
    property PuntoEntrega: Integer read FPuntoEntrega write SetPuntoEntrega;
    property PuntoVenta: Integer read FPuntoVenta write SetPuntoVenta;
    property ShowBrowserBar: Boolean read GetShowBrowserBar write SetShowBrowserBar;
    property ShowHotLinkBar: Boolean read GetShowHotLinkBar write SetShowHotLinkBar;
    property IntervaloRefrescoListaTareas: Integer  read GetIntervaloRefrescoListaTareas
                                                    write SetIntervaloRefrescoListaTareas;
  end;

Var
    GNavigator: TNavigator = nil;

function GetLockBitmap: TBitmap;
function GetLockOwnerBitmap: TBitmap;

implementation


Uses
    MenuesContextuales;

{ Esto es para incluir los bitmaps de los HotLinks, etc. }
{$R Navigator.res}
{$R *.dfm}

Const
	IB_MIN_SIZE 		= 0;
	IB_MAX_SIZE 		= 120;
	HL_MIN_SIZE 		= 24;
	HL_MAX_SIZE 		= 180;
	TL_MIN_SIZE 		= 24;
	TL_MAX_SIZE 		= 150;
	DEGRADEE_START 		= $00A27251;
	DEGRADEE_END   		= $00D6B59F;
	HL_TEXT_XPOSITION 	= 30;
	HL_TEXT_YPOSITION 	= 40;
	HL_TEXT_INTERLEAVE 	= 9;
	HL_SEPARATOR_INTERLEAVE = 15;
	HL_ICON_XPOSITION 	= 5;
	MAX_HISTORY_LINES 	= 10;
	BLINK_RATE 			= 800;

resourcestring
	MSG_ORDEN			= 'Orden';
	MSG_TIEMPO_RESTANTE	= 'Tiempo Restante';
	MSG_WORKFLOW		= 'Workflow';
    CAPTION_CANCELAR_TAREA  = 'Cancelar Tarea';
	CAPTION_FINALIZAR_TAREA = 'Finalizar Tarea';
  	CAPTION_DEVOLVER_TAREA  = 'Devolver Tarea';
	CAPTION_COMENZAR_TAREA  = 'Comenzar Tarea';

Type
	TNoDrawPanel = class(TPanel)
	protected
		procedure Paint; override;
		procedure EraseBkGnd(var message: TMessage); message WM_ERASEBKGND;
	end;

Var
	GBitLock: TBitmap;
    GBitLockOwner: TBitmap;


{ TNoDrawPanel }
procedure TNoDrawPanel.EraseBkGnd(var message: TMessage);
begin
	inherited
end;

procedure TNoDrawPanel.Paint;
begin
end;

{ TNavigator }
constructor TNavigator.Create(AOwner:TComponent);

	procedure AddImage(ImageList: TImageList; ResourceName: AnsiString);
	Var
		B: TBitmap;
	begin
		B := TBitmap.Create;
		B.LoadFromResourceName(HInstance, ResourceName);
		ImageList.AddMasked(B, B.Canvas.Pixels[0, 0]);
		B.Free;
	end;

resourcestring
	MSG_NO_EXISTE_FORM_RINCIPAL = 'No hay Formulario Principal!';
begin
	inherited Create(AOwner);
    GNavigator := Self;
	FEventoHotLink := nil;
	FAddHistory := False;
	FAntFilterIndex := -1;
	FHLWindowList := TStringList.Create;
	FTLWindowList := TStringList.Create;
	FIBWindowList := TStringList.Create;

	FCurrentNavWindow := nil;
	FNextTaskReload := 0;
	FSeparators		:= 0;
    FPuntoVenta     := 0;
    FPuntoEntrega   := 0;
	// Timer
	FTimer := TTimer.Create(Self);
	FTimer.Interval := 1;
	FTimer.OnTimer := DoTimer;
	// Toolbar: Images
	FToolImageList := TImageList.Create(Self);
	FToolImageList.Width := 30;
	FToolImageList.Height := 20;
	AddImage(FToolImageList, 'BROWSER_BACK');
	AddImage(FToolImageList, 'BROWSER_NEXT');
	AddImage(FToolImageList, 'BROWSER_REFRESH');
	AddImage(FToolImageList, 'BROWSER_FIND');
	// Toolbar: HotImages
	FToolHotImageList := TImageList.Create(Self);
	FToolHotImageList.Width := 30;
	FToolHotImageList.Height := 20;
	AddImage(FToolHotImageList, 'BROWSER_BACK_HOT');
	AddImage(FToolHotImageList, 'BROWSER_NEXT_HOT');
	AddImage(FToolHotImageList, 'BROWSER_REFRESH_HOT');
	AddImage(FToolHotImageList, 'BROWSER_FIND_HOT');
	// Stored procedure para las tareas
	FSP_OrdenesServicio := TAdoStoredProc.Create(Self);
	FSP_OrdenesServicio.ProcedureName := 'ObtenerOrdenesServicioPendientesUsuario';
	// Otros temas
	FImageList := TImageList.Create(Self);
	FImageList.Width := 32;
	FImageList.Height := 32;
	if Assigned(Application.MainForm) then BringObjects(Application.MainForm)
	  else raise exception.Create(MSG_NO_EXISTE_FORM_RINCIPAL);

end;

destructor Tnavigator.Destroy;
begin
	FHLWindowList.Free;
	FTLWindowList.Free;
	FIBWindowList.Free;
	inherited Destroy;
end;


procedure TNavigator.BringObjects(Form: Tform);
resourcestring
	MSG_REFRESH 	    = 'Refrescar';
	MSG_ADELANTE 	    = 'Adelante';
	MSG_ATRAS		    = 'Atr�s';
	MSG_BUSCAR		    = 'Buscar';
	MSG_FILTER		    = 'Filtro:';
	MSG_LISTA_TAREAS	= 'Lista de Tareas';
	MSG_CONTACTO        = 'Contacto';
	MSG_TAREA           = 'Tarea';
	MSG_PRIORIDAD       = 'Prioridad';
    MSG_PRIORIDAD_VALOR = 'Puntaje';
	MSG_HOTLINKS        = 'Hot Links';
    HINT_REFRESCAR      = 'Refrescar Datos';
    MSG_ADELANTAR_PAGINA = 'Adelante una p�gina %s';
    MSG_RETROCEDER_PAGINA = '%s Atr�s una p�gina';
Var
    TB: TToolbar;
	Pnl, Pnl2: TPanel;
	NewItem: TMenuItem;
begin
	(* Creamos el panel de la parte Superior *)
	(*PanelSuperior := TPanel.Create(Self);
	PanelSuperior.Align := alTop;
	PanelSuperior.Parent := form;
	PanelSuperior.BevelOuter := bvNone;*)
	FHotlinksCount   	:= 0;
	FHotLinkTitle		:= MSG_HOTLINKS;
	(* creamos los PopUpMenu para los botones de atras y adelante*)

	//menu de prueba con un item.
	FBackPopUp  := TPopUpMenu.Create(Self);
	With FBackPopUp do begin
		OnPopUp := DoGetHistoryBackList;
	end;
	FFwdPopUp  := TPopUpMenu.Create(Self);
	With FFwdPopUp do begin
		OnPopUp := DoGetHistoryFwdList;
	end;

	(*Creamos la parte superior *)
	FCoolBar := TCoolBar.Create(Self);
	FCoolBar.EdgeBorders := [ebTop];
	FCoolBar.Visible := False;
	with FCoolbar do begin
		Parent := Application.MainForm;
		Align := alTop;
		Left := 0;
		Top := 0;
		Width := 616;
		Height := 95;
		AutoSize := True;
		Color := clBtnFace;
	end;
	TB := TToolbar.Create(Self);
	With TB do begin
		Parent := FCoolbar;
		EdgeBorders := [];
		Flat := True;
		Height := 42;
		ButtonHeight := 40;
		ButtonWidth  := 40;
		ShowCaptions := True;
		Images := FToolImageList;
		HotImages := FToolHotImageList;
	end;
	btn_Refresh := TToolButton.Create(Self);
	With btn_Refresh do begin
		Parent := TB;
		Caption := MSG_REFRESH;
		Enabled := False;
		ImageIndex := 2;
		Left := 1000;
		ShowHint := True;
		Hint := HINT_REFRESCAR;
		OnClick := DoRefresh;
	end;

	btn_Fwd := TToolButton.Create(Self);
	With btn_Fwd  do begin
		Parent := TB;
		Style := tbsDropDown;
		Caption := MSG_ADELANTE;
		Enabled := False;
		ImageIndex := 1;
		DropDownMenu := FFwdPopUp;
		ShowHint := True;
		Hint := Format(MSG_ADELANTAR_PAGINA, [chr(187)]);
		OnClick := DOGoForward;
	end;

	btn_Back := TToolButton.Create(Self);
	With btn_Back do begin
		Parent := TB;
		Enabled := False;
		Style := tbsDropDown;
		Caption := MSG_ATRAS;
		ImageIndex := 0;
		DropDownMenu := FBackPopUp;
		ShowHint := True;
		Hint := Format(MSG_RETROCEDER_PAGINA, [chr(171)]);
		OnClick := DOGoBack;
	end;
	TB := TToolbar.Create(Self);
	With TB do begin
		Parent := FCoolbar;
		EdgeBorders := [];
		Flat := True;
		Height := 42;
		ButtonHeight := 40;
		ButtonWidth  := 40;
		ShowCaptions := True;
		Images := FToolImageList;
		HotImages := FToolHotImageList;
	end;
	btn_find := TToolButton.Create(Self);
	With btn_find do begin
		Parent := TB;
		Caption := MSG_BUSCAR;
		ImageIndex := 3;
		Left := 1000;
		Enabled := False;
		OnClick := DoFind;
	end;
	TB := TToolbar.Create(Self);
	With TB do begin
		Parent := FCoolbar;
		Flat := True;
		EdgeBorders := [];
		Height := 42;
		ButtonHeight := 40;
	end;
	Pnl := TPanel.Create(Self);
	With Pnl do begin
		Parent := TB;
		BevelInner := bvNone;
		BevelOuter := bvNone;
		Height := 40;
		Caption := '';
		Width := 300;
		Left := 1000;
	end;
	lbl_Filter := TLabel.Create(Self);
	With lbl_Filter do begin
		Parent := Pnl;
		Left   := 7;
		Top    := 14;
		Caption := MSG_FILTER;
		Enabled := False;
	end;
	cb_Filter := TComboBox.Create(Self);
	With cb_Filter do begin
		Parent := Pnl;
		SetBounds(40, 11, 250, Height);
		Style := csDropDownList;
		Enabled := False;
		OnClick := DoFilterClick;
	end;
	FCoolbar.Bands[0].Width := 220;
	FCoolbar.Bands[1].Width := 53;
	FCoolbar.Bands[0].Break := False;
	FCoolbar.Bands[1].Break := False;
	FCoolbar.Bands[2].Break := False;

	// Creamos el panel de la Izquierda
	PanelIzquierda := TPanel.Create(Self);
	PanelIzquierda.Visible := False;
	PanelIzquierda.Parent := Form;
	PanelIzquierda.Align := alLeft;
	PanelIzquierda.BevelOuter := bvNone;
	PanelIzquierda.Width := IB_MAX_SIZE;
	PanelIzquierda.OnResize 	:= DoPanelIzquierdaResize;

	// Creamos el panel Inferior
	PanelInferior				:= TPanel.Create(Self);
	PanelInferior.Visible		:= False;
	PanelInferior.Align			:= alBottom;
	PanelInferior.Parent		:= form;
	PanelInferior.BevelOuter	:= bvNone;
	PanelInferior.Height		:= TL_MIN_SIZE;

	PanelTrucho					:= TPanel.Create(Self);
	With PanelTrucho do begin
		Parent	:= PanelInferior;
		Align	:= alLeft;
		Width	:= PanelIzquierda.Width;
	end;

	Pnl2	:= TPanel.Create(Self);
	With Pnl2 do begin
		Parent 		:= PanelInferior;
		Align 		:= alClient;
		BorderStyle	:= bsSingle;
		BevelInner	:= bvNone;
		BevelOuter	:= bvNone;
		Color		:= $00FAF8F2;
	end;

	Pnl := TNoDrawPanel.Create(Self);
	With Pnl do begin				// Panel con el t�tulo y el bot�n retr�ctil
		Parent		:= Pnl2;
		Align		:= alTop;
		Height		:= 22;
		BevelInner	:= bvNone;
		BevelOuter	:= bvNone;
	end;

	FTLFadeBox := TFadeBox.Create(Self);
	With FTLFadeBox do begin
		Parent := Pnl;
		Align := alClient;
		FadeStyle := fsLeftRight;
		StartColor := DEGRADEE_START;
		EndColor := DEGRADEE_END;
	end;
	With TShape.Create(Self) do begin
		Parent := Pnl;
		Height := 1;
		Pen.Color := $00FAF8F2;
		Align := alTop;
	end;
	With TShape.Create(Self) do begin
		Parent := Pnl;
		Width := 1;
		Pen.Color := $00FAF8F2;
		Align := alLeft;
	end;
	FTaskListImage := TImage.Create(Self);
	With FTaskListImage do begin
		Parent := Pnl;
		SetBounds(3, 3, 15, 15);
		Transparent := True;
		Picture.Bitmap.LoadFromResourceName(HInstance, 'TASKLIST_DISABLED');
		Cursor  := crHandPoint;
		OnClick := DoAnimateTaskList;
        Enabled := False;
	end;
	With TLabel.Create(Self) do begin
		Parent := Pnl;
		Anchors := [];
		Font.Name := 'Verdana';
		Font.Size := 12;
		Font.Color := clWhite;
		Font.Style := [fsBold];
		Transparent := True;
		Caption := MSG_LISTA_TAREAS;
		Left := (Pnl.Width - Width + 6) div 2;
		Top := 2;
	end;
	With TPanel.Create(Self) do begin
		Align := alTop;
		Parent := Pnl2;
		Color := clBlack;
		BevelInner := bvNone;
		BevelOuter := bvNone;
		Height := 2;
	end;

    //Crea la Lista de tareas
	FTaskList := TDBListEx.Create(Self);
    AddOSNotification(DoOSChanged);
	With FTaskList do begin
		Parent := Pnl2;
		Align  := alTop;
		Top := Pnl.Height;
		Height := TL_MAX_SIZE - TL_MIN_SIZE - 4;
		Color  := $00FAF8F2;
		BorderStyle := bsNone;
		DataSource := TDataSource.Create(Self);
		DataSource.DataSet := FSP_OrdenesServicio;
        OnContextPopup := DoTareasContextMenu;
        OnDrawText := DoTaskDrawText;
        OnMouseMove:= FTaskListMouseMove;
        //OnDrawColumnCell := DoTaskDrawCell;
        //OnLinkClick := DoTaskLinkClick;
        //OnDblClick := DoTaskDblClick;
        //OnMouseUp := DoTaskMouseUp;
		With Columns.Add do begin
			FieldName := '';
			Width := 20;
			Header.Caption := '';
		end;
		With Columns.Add do begin
			FieldName := 'CodigoOrdenServicio';
			Width := 60;
            Alignment := taCenter;
            Font.Name := 'Tahoma';
            Font.Size := 9;
			Font.Color := clBlue;
			Font.Style := [fsUnderline];
            IsLink := True;
			Header.Caption := MSG_ORDEN;
			Header.Alignment := taCenter;
            //permiso hacer click enlink
            //al hacer click se puede editar la tarea
            OnLinkClick := FTaskListLinkClick;
            //permito ordenar
            OnHeaderClick:= FTaskListColumns0HeaderClick;
		end;
		With Columns.Add do begin
			FieldName := 'FechaHoraCreacion';
            Alignment := taCenter;
            Font.Name := 'Tahoma';
            Font.Size := 9;
			Header.Caption := 'F. Creaci�n';
			Width := 140;
            Header.Alignment := taCenter;
            //permito ordenar
            OnHeaderClick:= FTaskListColumns0HeaderClick;
		end;
		With Columns.Add do begin
			FieldName := 'DescripcionTipoOrdenServicio';
			Width := 400; //200
            Font.Name := 'Tahoma';
            Font.Size := 9;
            Font.Style := [fsBold];
			Header.Caption := MSG_TAREA;
            //permito ordenar
            OnHeaderClick:= FTaskListColumns0HeaderClick;
		end;
		With Columns.Add do begin
			FieldName := 'Prioridad';
			Alignment := taCenter;
			Width := 60;
            Font.Name := 'Tahoma';
            Font.Size := 9;
			Header.Caption := MSG_PRIORIDAD;
			Header.Alignment := taCenter;
            //permito ordenar
            OnHeaderClick:= FTaskListColumns0HeaderClick;
		end;
        With Columns.Add do begin
			FieldName := 'FechaCompromiso';
			Alignment := taCenter;
			Width := 120;
            Font.Name := 'Tahoma';
            Font.Size := 9;
			Header.Caption := 'F. Comprometida';
			Header.Alignment := taCenter;
            //permito ordenar
            OnHeaderClick:= FTaskListColumns0HeaderClick;
		end;
        With Columns.Add do begin
			FieldName := 'DescEstado';
			Alignment := taCenter;
			Width := 80;
            Font.Name := 'Tahoma';
            Font.Size := 9;
			Header.Caption := 'Estado';
			Header.Alignment := taCenter;
            //permito ordenar
            OnHeaderClick:= FTaskListColumns0HeaderClick;
		end;
	end;

	(* Creamos la IconBar *)
	BarraIzquierda := TIconBar.Create(Self);
	BarraIzquierda.Visible := False;
	BarraIzquierda.Width := PanelIzquierda.Width;
	BarraIzquierda.Name := 'BarraIzquierda';
	BarraIzquierda.Align := alNone;
	BarraIzquierda.Anchors := [akTop, akLeft, akBottom];
	BarraIzquierda.Top  := PanelIzquierda.Top;
	BarraIzquierda.Height := PanelIzquierda.Height + PanelInferior.Height;
	BarraIzquierda.Parent := Form;
	BarraIzquierda.FlatButtons := True;
	// BarraIzquierda.Animated 		:= False;
	// BarraIzquierda.FlatButtons 	:= True;
	PanelIzquierda.Width := BarraIzquierda.Width;

    (* Creamos la lista de HotLinks *)
	PanelDerecho := TPanel.Create(Self);
	PanelDerecho.Visible := False;
	PanelDerecho.BorderStyle := bsSIngle;
	PanelDerecho.Width := HL_MIN_SIZE;
	PanelDerecho.Align := alRight;
	PanelDerecho.Parent := form;
	PanelDerecho.BorderWidth := PanelDerecho.BorderWidth + 1;
	PanelDerecho.BevelOuter := bvNone;
	PanelDerecho.BevelInner := bvNone;
	PanelDerecho.Color := $00FAF8F2;
	Pnl := TNoDrawPanel.Create(Self);
	With Pnl do begin				// Panel con el t�tulo y el bot�n retr�ctil
		Parent := PanelDerecho;
		Align := alLeft;
        //Align := alNone;
        //anchors := [akleft, aktop, akbottom];
		Width := 20;
		BevelInner := bvNone;
		BevelOuter := bvNone;
	end;
	With TFadeBox.Create(Self) do begin
		Parent		:= Pnl;
		Align		:= alClient;
		FadeStyle	:= fsTopBottom;
		StartColor	:= DEGRADEE_START;
		EndColor 	:= DEGRADEE_END;
	end;
	FHotLinkImage := TImage.Create(Self);
	With FHotLinkImage do begin
		Parent 		:= Pnl;
		SetBounds(2, 2, 15, 15);
		Transparent := True;
		Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_DISABLED');
		Cursor 		:= crHandPoint;
		OnClick 	:= DoAnimateHotlinks;
        Enabled 	:= False;
	end;
	with TRotatedLabel.Create(Self) do begin
		Parent 		:= Pnl;
		Angle 		:= 90;
		Anchors     := [];
		Caption 	:= MSG_HOTLINKS;
		Font.Name	:= 'Verdana';
		Font.Size	:= 12;
		Font.Color	:= clWhite;
		Font.style	:= [fsbold];
		SetBounds(-1, (pnl.Height - height + 6) div 2, 20, Height);
		Transparent := True;
	end;
    ScrollBox:= TScrollBox.Create(Self);
    With ScrollBox do begin
    	Parent 					:= PanelDerecho;
        Align					:= alClient;
        //Align					:= alNone;
        //Anchors					:= [akTop, akLeft, akbottom, akleft];
        HorzScrollBar.Visible	:= False;
        BorderStyle 			:= bsNone;
    end;
    FLabelHotLinkTitle := TLabel.Create(Self);
    With FLabelHotLinkTitle do begin
        Autosize		:= False;
        Height			:= 400;
        Width			:= HL_MAX_SIZE - HL_TEXT_XPOSITION - 40;
        Parent 			:= ScrollBox;
        Left 			:= HL_TEXT_XPOSITION;
        Top  			:= 10;
		Font.Name 		:= 'Arial';
		Font.Style 		:=  [fsBold];
        Font.Size    	:= 12;
        Font.Color 		:= clBlack;
        Transparent		:= True;
        Wordwrap 		:= True;
        Caption 		:= FHotLinkTitle;
        Autosize		:= True;
        ShowAccelChar   := True;
        Tag 			:= -1;
	end;

    FImageHotLinkTitle := TImage.Create(Self);
    With FImageHotLinkTitle do begin
    	Width			:= 20;
    	Height			:= 20;
    	Parent 			:= ScrollBox;
		Stretch 	   	:= True;
        Transparent		:= True;
        SetBounds(HL_ICON_XPOSITION, 10, Width, Height);
        Picture.Bitmap.LoadFromResourceName(HInstance, 'HL_FACE');
        Tag 			:= -1;
    end;


    HL := THistory.Create(Self);

	// Mostramos todo
	FCoolBar.Visible 		:= True;
	PanelIzquierda.Visible 	:= True;
	BarraIzquierda.Visible 	:= True;
	PanelDerecho.Visible 	:= True;
	PanelInferior.Visible 	:= True;
end;

procedure TNavigator.AddPage(const PageName, PageTitle, Hint: AnsiString; PageColor: TColor = $00FAF8F2);
var
	NewPage: TIconBarPage;
begin
	NewPage := TIconBarPage.Create(BarraIzquierda);
    NewPage.Caption  := PageTitle;
	NewPage.Name     := PageName;
	NewPage.ShowHint := True;
	NewPage.SelectionColor := RGB(183, 197, 220);
	if Trim(Hint) = '' then NewPage.Hint := PageTitle else NewPage.Hint := Hint;
    if Assigned(FImageList) then NewPage.Images := FImageList;
    BarraIzquierda.InsertPage(NewPage);
    NewPage.Color   := PageColor;
	NewPage.Parent  := BarraIzquierda;
end;

procedure TNavigator.AddIcon(const PageName, ItemTitle, Hint: AnsiString; Bitmap: TBitmap; Event: TNotifyEvent);
var
	i: integer;
	Bmp: TBitmap;
	NewItem: TIconBarItem;
begin
	for i:= 0 to BarraIzquierda.PageCount - 1 do begin
		if AnsiCompareText(BarraIzquierda.Pages[i].Name, PageName) = 0 then begin
			Bmp := TBitmap.Create;
			Bmp.Width := FImageList.Width;
			Bmp.Height := FImageList.Height;
			Bmp.Canvas.StretchDraw(Rect(0, 0, Bmp.Width, Bmp.Height), Bitmap);
			NewItem             := TIconBarItem.Create(BarraIzquierda.Pages[i].Items);
			NewItem.Caption     := ItemTitle;
			NewItem.ShowHint    := True;
			NewItem.Hint        := Hint;
			NewItem.OnClick     := Event;
			NewItem.Font.Color  := clBlack;
			if Assigned(Bitmap) then NewItem.ImageIndex := FImageList.AddMasked(Bmp, Bmp.Canvas.Pixels[0, 0])
			  else NewItem.ImageIndex := -1;
			Bmp.Free;
			Break;
		end;
	end;
end;

procedure TNavigator.AddSeparator(Alto: integer; Color: TColor = clBtnFace);
Var
	i: integer;
    TextYPosition: integer;
    TmpColor : TColor;
begin
    inc(FSeparators);

    TextYPosition := HL_TEXT_INTERLEAVE;
    For i := 0 to ScrollBox.ControlCount - 1 do begin
        if (ScrollBox.Controls[i] is TLabel)
        	or (ScrollBox.Controls[i] is TSeparator) then begin
			TextYPosition := TextYPosition +
            	ScrollBox.Controls[i].Height +
                iif( ScrollBox.Controls[i] is TLabel, HL_TEXT_INTERLEAVE, HL_TEXT_INTERLEAVE + HL_SEPARATOR_INTERLEAVE);
        end;
    end;


    (*TextYPosition := HL_TEXT_YPOSITION;
    For i := 0 to ScrollBox.ControlCount - 1 do begin
    	if (ScrollBox.Controls[i] is TLabel) or
           (ScrollBox.Controls[i] is TSeparator) then begin
            TextYPosition := ScrollBox.Controls[i].Top + ScrollBox.Controls[i].Height + HL_TEXT_INTERLEAVE;
        end;
    end;*)

    TextYPosition := TextYPosition + HL_TEXT_INTERLEAVE;
    
    TmpColor := Color;
    With TSeparator.Create(Self) do begin
    	Name			:= 'pnlSeparator' + IntToStr(FSeparators);
        Parent 			:= ScrollBox;
        Setbounds( 0,TextYPosition,HL_MAX_SIZE(*- HL_TEXT_XPOSITION*), Alto);
        Color			:= TmpColor;
        Caption 		:= '';
	end;
end;

procedure TNavigator.SetActivePage(const Value: Integer);
begin
	BarraIzquierda.ActivePageIndex := Value;
end;

procedure TNavigator.AnimateHotLinks;
const
	STEPS = 20;
	Delays: Array[1..STEPS] of Integer =
	  (60, 40, 20, 10, 5, 3, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0);
	dx = (HL_MAX_SIZE - HL_MIN_SIZE) div STEPS;
Var
	i: Integer;
begin
	// Este efecto lo copi� de la IconBar para que parezca que la barra se va
	// desplazando cada vez m�s r�pido. Queda re-fashion!
	if HotLinksVisible then begin
		// Achicamos
		FHotLinkImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_OPEN');
		for i := 1 to Steps do begin
			PanelDerecho.Width := PanelDerecho.Width - dx;
			PanelDerecho.Update;
			if Assigned(Application.MainForm.OnResize) then Application.MainForm.OnResize(Application.MainForm);
			Sleep(Delays[i]);
		end;
		PanelDerecho.Width := HL_MIN_SIZE;
	end else begin
		// Agrandanos
        FHotLinkImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_CLOSE');
		for i := 1 to Steps do begin
			PanelDerecho.Width := PanelDerecho.Width + dx;
			PanelDerecho.Update;
			if Assigned(Application.MainForm.OnResize) then Application.MainForm.OnResize(Application.MainForm);
			Sleep(Delays[i]);
		end;
		PanelDerecho.Width := HL_MAX_SIZE;
	end;
end;

procedure TNavigator.DoAnimateHotLinks(Sender: TObject);
begin
	HotLinksVisible := not HotLinksVisible;
end;


function TNavigator.GetHLVisible: Boolean;
begin
	Result := (PanelDerecho.Width = HL_MAX_SIZE);
end;

procedure TNavigator.SetHLVisible(const Value: Boolean);
begin
	if (Value = HotLinksVisible)then Exit;
	if Value then ArrangeHLWindows(Value);
	AnimateHotLinks;
	if not Value then ArrangeHLWindows(Value);
end;

procedure TNavigator.DoMouseEnter(Sender: TObject);
begin
	if Sender is TLabel then begin
		TLabel(Sender).Font.Color := clBlue;
	end;
end;

procedure TNavigator.DoMouseLeave(Sender: TObject);
begin
	if Sender is TLabel then begin
		TLabel(Sender).Font.Color := clBlack;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DoAnimateTaskList
  Author:
  Date Created:
  Description: Muestra o Oculta la Barra de Tareas
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TNavigator.DoAnimateTaskList(Sender: TObject);

    {---------------------------------------------------------
      Function Name: RecorrerForms
      Author:    lgisuk
      Date Created: 16/05/2005
      Description: Recorre los forms que hay en la pantalla
      Parameters: None
      Return Value: Boolean
    ----------------------------------------------------------}
    function RecorrerForms: Boolean;

        {----------------------------------------------------
          Function Name: AjustarAEspacioDisponible
          Author:    lgisuk
          Date Created: 16/05/2005
          Description: Ajusta el form al tama�o disponible
          Parameters: F:Tform
          Return Value: boolean
        ----------------------------------------------------}
        Function AjustarAEspacioDisponible(F:Tform):boolean;
        var
            S: Tsize;
        begin
    	    Result := False;
           try
                //lo ajusto al tama�o disponible
                S := GetFormClientSize(Application.MainForm);
                F.SetBounds(0, 0, S.cx, S.cy);
                result:=true;
            except
            end;
        end;

    var i: integer;
    begin
        for i := 0 to Screen.FormCount - 1 do begin
            if (
                 (Screen.Forms[i].Name = 'TFReclamos') or
                 (Screen.Forms[i].Name = 'NavWindowInicioConsultaConvenio') or
                 (Screen.Forms[i].Name = 'TFOrdenesServicioBloquedas') or
                 (Screen.Forms[i].Name = 'TFormReclamoGeneral')
               )
               then begin
                    AjustarAEspacioDisponible(Screen.Forms[i]);
               end;
        end;
        result:=true;
    end;

begin
	TaskListVisible := not TaskListVisible;
    RecorrerForms;
end;

function TNavigator.GetTLVisible: Boolean;
begin
	Result := (PanelInferior.Height = TL_MAX_SIZE);
end;

procedure TNavigator.SetTLVisible(const Value: Boolean);
begin
	if Value = TaskListVisible then Exit;
	if Value then ArrangeTLWindows(Value);
	AnimateTaskList;
	if not Value then ArrangeTLWindows(Value);
end;

procedure TNavigator.AnimateTaskList;
const
	STEPS = 10;
	Delays: Array[1..STEPS] of Integer =
	  (10, 8, 6, 5, 4, 3, 2, 1, 1, 1);
	dy = (TL_MAX_SIZE - TL_MIN_SIZE) div STEPS;
Var
	i: Integer;
begin
	// Este efecto lo copi� de la IconBar para que parezca que la barra se va
	// desplazando cada vez m�s r�pido. Queda re-fashion!
	if TaskListVisible then begin
		// Achicamos
		FTaskListImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'TASKLIST_OPEN');
		for i := 1 to Steps do begin
			PanelInferior.Height := PanelInferior.Height - dy;
			PanelInferior.Update;
			PanelDerecho.Update;
			if Assigned(Application.MainForm.OnResize) then Application.MainForm.OnResize(Application.MainForm);
            if Assigned(Application.MainForm.OnPaint) then Application.MainForm.OnPaint(Application.MainForm);
			Sleep(Delays[i]);
		end;
		PanelInferior.Height := TL_MIN_SIZE;
	end else begin
		// Agrandanos
		FTaskListImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'TASKLIST_CLOSE');
		for i := 1 to Steps do begin
			PanelInferior.Height := PanelInferior.Height + dy;
			PanelInferior.Update;
			PanelDerecho.Update;
			if Assigned(Application.MainForm.OnResize) then Application.MainForm.OnResize(Application.MainForm);
            if Assigned(Application.MainForm.OnPaint) then Application.MainForm.OnPaint(Application.MainForm);
			Sleep(Delays[i]);
		end;
		PanelInferior.Height := TL_MAX_SIZE;
	end;
end;

procedure TNavigator.DoPanelIzquierdaResize(Sender: TObject);
begin
    if Assigned(BarraIzquierda) then begin
        with BarraIzquierda do begin
            Height := PanelIzquierda.Height + PanelInferior.Height;
			Width := PanelIzquierda.Width;
			Top := PanelIzquierda.Top;
		end;
    end;
end;
function TNavigator.GetActivePage: Integer;
begin
	Result := BarraIzquierda.ActivePageIndex;
end;

Procedure TNavigator.DoGoBack(Sender: TObject);
begin
	GoBack;
end;

Procedure TNavigator.GoBack;
var
	Indice : integer;
begin
	if Hl.ItemIndex < 0 then Exit;
    Indice := Hl.ItemIndex - 1;
	DoJumpTo(Hl.GetObject(Hl.ItemIndex), False, Hl.GetBookMark(Hl.ItemIndex));
    Hl.ItemIndex := Indice;
    ActualizarBotones;
end;

Procedure TNavigator.DOGoForward(Sender: TObject);
begin
	GoForward;
end;

Procedure TNavigator.GoForward;
begin
	if Assigned(FCurrentNavWindow) then begin
		if Hl.ItemIndex >= Hl.ItemsCount - 1 then Exit;
		Hl.ItemIndex := Hl.ItemIndex + 1;
		DoJumpTo(Hl.GetObject(Hl.ItemIndex + 1), False, Hl.GetBookMark(Hl.ItemIndex + 1));
	end else begin
		if Hl.ItemIndex >= Hl.ItemsCount then Exit;
		DoJumpTo(Hl.GetObject(Hl.ItemIndex + 1), False, Hl.GetBookMark(Hl.ItemIndex + 1));
	end;
	ActualizarBotones;
end;


function TNavigator.DOJumpTo(WindowClass: TNavWindowClass; Agregar: Boolean; Bookmark: AnsiString): Boolean;
resourcestring
    MSG_PAGINA_CADUCADA = 'La p�gina ha caducado o no se encontraron datos.';
    CAPTION_IR_A = 'Ir a';
Var
	F: TNavWindowFrm;
	Bm, Desc: AnsiString;
    CanGoPrevBookmark: Boolean;
begin
	// Verificaciones sobre el proceso actual
	if Assigned(FCurrentNavWindow) then begin
		// Si estamos en el mismo bookmark de la misma ventana o proceso, no hacemos nada
		if (WindowClass.ClassName = FCurrentNavWindow.ClassName) and
		  FCurrentNavWindow.GetBookmark(Bm, Desc) and (Bm = Bookmark) then begin
		  	FCurrentNavWindow.Show;
			Result := True;
			Exit;
		end;
		// Si no podemos salir del bookmark actual, nos vamos
		if not FCurrentNavWindow.CanTerminate then begin
			Result := False;
			Exit;
		end;
        CanGoPrevBookmark := FCurrentNavWindow.GetBookmark(Bm, Desc) and Agregar; //FAddHistory and FCurrentNavWindow.GetBookmark(Bm, Desc);
	end else begin
		CanGoPrevBookmark := False;
    end;
	// Abrimos la nueva ventana  o proceso, o utilizamos la actual seg�n corresponda
	if Assigned(FCurrentNavWindow) and (FCurrentNavWindow.ClassName = WindowClass.ClassName) then begin
		F := FCurrentNavWindow;
	end else begin
		Application.CreateForm(WindowClass, F);
		F.FNavigator := Self;
		if not F.Inicializa then begin
			F.Release;
			Result := False;
			Exit;
		end;
	end;
	// Si nos pidieron, vamos a un bookmark
	if Trim(Bookmark) <> '' then begin
		if not F.GotoBookmark(Bookmark) then begin
			MsgBox(MSG_PAGINA_CADUCADA, CAPTION_IR_A, MB_ICONSTOP);
			if F <> FCurrentNavWindow then F.Release;
			Result := False;
			Exit;
		end;
	end;

  {	// Armamos la lista de Filtros de la ventana actual
	cb_Filter.Clear;
	cb_Filter.Items.Add('<Nuevo>');
	cb_Filter.ItemIndex := -1;}

	// Hay que agregar la ventana actual al history?
    if CanGoPrevBookmark then HL.AddToHistory(TNavWindowClass(FCurrentNavWindow.ClassType), Bm, Desc);
    if Agregar then HL.RemoveFwdHistory;

    // Listo
	if (F <> FCurrentNavWindow) then begin
		if Assigned(FCurrentNavWindow) then FCurrentNavWindow.Release;
		FCurrentNavWindow := F;
	end;
	FCurrentNavWindow.Show;
    FAddHistory := Agregar;
	ActualizarBotones;
	Result := True;
end;


function TNavigator.JumpTo(WindowClass: TNavWindowClass; Bookmark: AnsiString): Boolean;
begin
    Result := DoJumpTo(WindowClass, True, Bookmark);
end;

procedure TNavigator.Notification(AComponent: TComponent; Operation: TOperation);
Var
	i: Integer;
begin
	inherited Notification(AComponent, Operation);
	if (Operation = opRemove) then begin
		if (AComponent = FCurrentNavWindow) then FCurrentNavWindow := nil;
		if AComponent is TForm then begin
			for i := 0 to FHLWindowList.Count - 1 do begin
				if FHLWindowList.Objects[i] = AComponent then begin
					FHLWindowList.Delete(i);
					Break;
				end;
			end;
			for i := 0 to FTLWindowList.Count - 1 do begin
				if FTLWindowList.Objects[i] = AComponent then begin
					FTLWindowList.Delete(i);
					Break;
				end;
			end;
			for i := 0 to FIBWindowList.Count - 1 do begin
				if FIBWindowList.Objects[i] = AComponent then begin
					FIBWindowList.Delete(i);
					Break;
				end;
			end;
		end;
	end;
end;

procedure TNavigator.ArrangeHLWindows(Open: Boolean);
Var
	f: TForm;
	i, MaxX, x, w: Integer;
begin
	MaxX := GetFormClientSize(Application.MainForm).cx - HL_MAX_SIZE + HL_MIN_SIZE;
	if Open then begin
		FHLWindowList.Clear;
		for i := 0 to Screen.FormCount - 1 do begin
			f := Screen.Forms[i];
			if f.FormStyle = fsMDIChild then begin
				if f.Left + f.Width > MaxX then begin
					FHLWindowList.AddObject(IntToStr(f.Left) + ',' +
					  IntToStr(f.Width), f);
					f.Left := 0;
					f.Width := MaxX;
				end;
			end;
		end;
	end else begin
		for i := 0 to FHLWindowList.Count - 1 do begin
			f := TForm(FHLWindowList.Objects[i]);
			x := IVal(ParseParamByNumber(FHLWindowList[i], 1));
			w := IVal(ParseParamByNumber(FHLWindowList[i], 2));
			f.SetBounds(x, f.Top, w, f.Height);
		end;
		FHLWindowList.Clear;
	end;
end;

procedure TNavigator.ArrangeTLWindows(Open: Boolean);
Var
	f: TForm;
	i, MaxY, y, h: Integer;
begin
	MaxY := GetFormClientSize(Application.MainForm).cy - TL_MAX_SIZE + TL_MIN_SIZE;
	if Open then begin
		FTLWindowList.Clear;
		for i := 0 to Screen.FormCount - 1 do begin
			f := Screen.Forms[i];
			if f.FormStyle = fsMDIChild then begin
				if f.Top + f.Height > MaxY then begin
					FTLWindowList.AddObject(IntToStr(f.Top) + ',' +
					  IntToStr(f.Height), f);
					f.Top := 0;
					f.Height := MaxY;
				end;
			end;
		end;
	end else begin
		for i := 0 to FTLWindowList.Count - 1 do begin
			f := TForm(FTLWindowList.Objects[i]);
			y := IVal(ParseParamByNumber(FTLWindowList[i], 1));
			h := IVal(ParseParamByNumber(FTLWindowList[i], 2));
			f.SetBounds(f.Left, y, f.Width, h);
		end;
		FTLWindowList.Clear;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DoTimer
  Author:    lgisuk
  Date Created: 28/04/2005
  Description: Actualiza las ordenes de servicio, con un timer
  Parameters: Sender: TObject
  Return Value: None

  Revision 1:
      Author : ggomez
      Date : 04/07/2006
      Description : Modifiqu� el tiempo de intervalo para refrescar la lista de
        tareas. Ahora utiliza una property del TNavigator.
-----------------------------------------------------------------------------}
procedure TNavigator.DoTimer(Sender: TObject);
Var
    Eve: TEventoHotLink;
begin
    FTimer.Enabled := False;
    try
        // Revisamos la TaskList (Para ver si tiene que parpadear)
        if TaskListVisible or not FSP_OrdenesServicio.Active or (FSP_OrdenesServicio.RecordCount = 0) then
            FTLFadeBox.FadeStyle := fsLeftRight
        else begin
            if GetMSCounter > FNextBlink then begin
                if FTLFadeBox.FadeStyle = fsLeftRight then FTLFadeBox.FadeStyle := fsRightLeft
                  else FTLFadeBox.FadeStyle := fsLeftRight;
                FNextBlink := GetMSCounter + BLINK_RATE;
            end;
        end;
        // La aplicaci�n cambi� de ventana
        if Application.MainForm.ActiveMDIChild <> FAntMDIChild then begin
            ActualizarBotones;
            FAntMDIChild := Application.MainForm.ActiveMDIChild;
        end;
        // Revisamos si hay que ejecutar alg�n evento de la lista de HotLinks
        if Assigned(FEventoHotLink) then begin
            Eve := FEventoHotLink;
            FEventoHotLink := nil;
            Eve(FEventoHotLinkParams);
		end;
		// Actualizamos la lista de Tareas, si corresponde
		if GetMSCounter > FNextTaskReload then begin
//			FNextTaskReload := GetMSCounter + 30000;
			FNextTaskReload := GetMSCounter + IntervaloRefrescoListaTareas;
			ReloadTasks;
		end;
	finally
        FTimer.Enabled := True;
    end;
end;

procedure TNavigator.DoHistoryClick(Sender: TObject);
var
	Indice: integer;
begin
	Indice := TMenuItem(Sender).Tag;
    Hl.ItemIndex := Indice;
   	DoJumpTo(Hl.GetObject(indice), False, Hl.GetBookMark(Indice));
    Hl.ItemIndex := Indice - 1;
    ActualizarBotones;
end;

procedure TNavigator.DoGetHistoryBackList(Sender: TObject);
var
	i: integer;
    NewMenu : TMenuItem;
begin
	// Si no hay ningun item me voy
	if not btn_back.enabled then exit;
	While FBackPopUp.Items.Count > 0 do FBackPopUp.Items[0].Free;
	for i := 0 to HL.ItemIndex do begin
    	NewMenu 			:= TMenuItem.Create(self);
        NewMenu.Caption 	:= HL.Description[i];
        NewMenu.Tag			:= i;
		NewMenu.OnClick 	:= DoHistoryClick;
        FBackPopUp.Items.Add(NewMenu);
    end;
end;

procedure TNavigator.DoGetHistoryFwdList(Sender: TObject);
var
	NewMenu : TMenuItem;
	i, StartIndex: integer;
begin
	// Si no hay ningun item me voy
	if not btn_fwd.enabled then exit;
	While FFwdPopUp.Items.Count > 0 do FFwdPopUp.Items[0].Free;
	StartIndex := HL.ItemIndex + 1;
	if Assigned(FCurrentNavWindow) then Inc(StartIndex);
	for i := StartIndex to HL.ItemsCount - 1 do begin
    	NewMenu 			:= TMenuItem.Create(self);
        NewMenu.Caption 	:= HL.Description[i];
        NewMenu.Tag			:= i;
        NewMenu.OnClick 	:= DoHistoryClick;
		FFwdPopUp.Items.Add(NewMenu);
    end;
end;

Procedure TNavigator.ActualizarBotones;
Var
	ViendoNav: Boolean;
begin
	// Debemos actualizar los botones de atras y adelante
	ViendoNav := Assigned(FCurrentNavWindow)
	  and (Application.MainForm.ActiveMDIChild = FCurrentNavWindow);
	btn_Back.Enabled := Hl.ItemIndex >= 0;
	if Assigned(FCurrentNavWindow) then begin
		btn_Fwd.Enabled  := hl.ItemIndex < (hl.ItemsCount - 2);
	end else begin
		btn_Fwd.Enabled  := hl.ItemIndex < (hl.ItemsCount - 1);
	end;
	btn_Refresh.Enabled := ViendoNav and (ncRefresh in FCurrentNavWindow.GetCapabilities);
	btn_Find.Enabled := ViendoNav and (ncFind in FCurrentNavWindow.GetCapabilities);
	lbl_Filter.Enabled := ViendoNav and (ncFilter in FCurrentNavWindow.GetCapabilities);
	cb_Filter.Enabled := lbl_Filter.Enabled;
end;

Procedure TNavigator.CloseNavWindow(Window: TNavWindowFrm);
Var
	Bm, Desc: AnsiString;
begin
	if FAddHistory and FCurrentNavWindow.GetBookmark(Bm, Desc) then begin
    	Hl.AddToHistory(TNavWindowClass(FCurrentNavWindow.ClassType), Bm, Desc);
	end else begin
		if HL.ItemIndex < HL.ItemsCount - 1 then HL.ItemIndex := HL.ItemIndex + 1;
	end;
	FCurrentNavWindow := nil;
	ActualizarBotones;
end;

procedure TNavigator.RefreshData;
begin
	if Assigned(FCurrentNavWindow) and (ncRefresh in FCurrentNavWindow.Capabilities) then
	  FCurrentNavWindow.RefreshData;
end;

procedure TNavigator.DoRefresh(Sender: TObject);
begin
	RefreshData;
end;

function TNavigator.FindFirst(SearchText: AnsiString): Boolean;
begin
	Result := Assigned(FCurrentNavWindow) and (ncFind in FCurrentNavWindow.Capabilities)
	  and FCurrentNavWindow.FindFirst(SearchText);
end;

procedure TNavigator.DoFind(Sender: TObject);
resourcestring
    CAPTION_BUSCAR_TEXTO = 'Buscar';
    MSG_BUSCAR_TEXTO = 'Texto "%s" no encontrado';
    MSG_TEXTO_A_BUSCAR = 'Texto a buscar:';
begin
	if Assigned(FCurrentNavWindow) and (ncFind in FCurrentNavWindow.Capabilities)
	  and InputQuery(CAPTION_BUSCAR_TEXTO, MSG_TEXTO_A_BUSCAR, FSearchText) then begin
		if not FCurrentNavWindow.FindFirst(FSearchText) then begin
			MsgBox(Format(MSG_BUSCAR_TEXTO, [FSearchText]), CAPTION_BUSCAR_TEXTO, MB_ICONINFORMATION);
		end;
	end;
end;

procedure TNavigator.DoFilterClick(Sender: TObject);
Var
	FilterBookmark, FilterName: AnsiString;
begin
	if cb_Filter.ItemIndex = 0 then begin
		// Nuevo Filtro
		FilterBookmark := '';
		if FCurrentNavWindow.Filter(FilterBookmark, FilterName) then begin
			cb_Filter.Items.Add(PadR(FilterName, 200) + FilterBookmark);
			cb_Filter.ItemIndex := cb_Filter.Items.Count - 1;
			FAntFilterIndex := cb_Filter.ItemIndex;
		end else begin
			cb_Filter.ItemIndex := FAntFilterIndex;
		end;
	end else begin
		FilterBookmark := Copy(cb_Filter.Text, 201, MaxInt);
		FCurrentNavWindow.Filter(FilterBookmark, FilterName)
	end;
end;

procedure TNavigator.DoHotLinkClick(Sender: TObject);
Var
    Evento: TEventoHotLink;
    NumeroHotLink: Integer;
begin
    NumeroHotLink := (Sender as TComponent).Tag;
    Evento := FHotLinkData[NumeroHotLink].Evento;
    if Assigned(Evento) then begin
        FEventoHotLink := Evento;
        FEventoHotLinkParams := FHotLinkData[NumeroHotLink].Params;
    end;
end;

{******************************** Function Header ******************************
Function Name: SetConnCAC
Author :
Date Created :
Description :
Parameters : const Value: TAdoCOnnection
Return Value : None

Revision 1:
    Author : ggomez
    Date : 04/07/2006
    Description : Agregu� el seteo de la propiedad que almacena la cantidad de
        milisegundos del intervalo de refresco de la lista de tareas. Lo hice
        aqu� pues en el create del TNavigator no se sabe cual es la conexi�n al
        CAC.
*******************************************************************************}
procedure TNavigator.SetConnCAC(const Value: TAdoCOnnection);
resourcestring
	MSG_NO_ERROR_OBTENER_INTERVALO_REFRESCO_LISTA_TAREAS = 'Ha ocurrido un error al obtener el par�metro general: ';
var
    Intervalo: Integer;
begin
	if FConnCAC = Value then Exit;
	FConnCAC := Value;
	FSP_OrdenesServicio.Connection := FConnCAC;
	FNextTaskReload := 0;


    // Setear el Intervalo de refresco de la lista de tareas.
    if not ObtenerParametroGeneral(ConnectionCAC, INTERVALO_REFRESCO_LISTA_TAREAS, Intervalo) then begin
        // Si no se puede obtener el par�metro general, lanzar una excepci�n
        raise Exception.Create(MSG_NO_ERROR_OBTENER_INTERVALO_REFRESCO_LISTA_TAREAS
            + INTERVALO_REFRESCO_LISTA_TAREAS);
    end else begin
        IntervaloRefrescoListaTareas := Intervalo;
    end;
end;

procedure TNavigator.SetConnCOP(const Value: TAdoCOnnection);
begin
	if FConnCOP = Value then Exit;
	FConnCOP := Value;
	FNextTaskReload := 0;
end;

procedure TNavigator.SetHotLinksTitle(const Value: AnsiString);
begin
//	if FHotLinkTitle = Value then exit;
    FHotLinkTitle := Value;
	With Self.FLabelHotLinkTitle do begin
        Wordwrap 		:= False;
        Autosize		:= False;
        Height			:= 400;
        Width			:= HL_MAX_SIZE - HL_TEXT_XPOSITION - 40;
        Left 			:= HL_TEXT_XPOSITION;
        Wordwrap 		:= True;
        Caption 		:= FHotLinkTitle;
        Autosize		:= True;
        ShowAccelChar   := True;
	end;
    FLabelHotLinkTitle.Invalidate;
end;
{-----------------------------------------------------------------------------
  Procedure: ReloadTasks
  Author:
  Date:
  Arguments: None
  Result:    None
  Description:
  History:
  Revision 1 :
     Author:    lcanteros
     Date:      07/Ago/2008
     Description: se quita la actualizacion de la barra de tareas para que quede
     siempre habilitada (SS 330).
-----------------------------------------------------------------------------}
procedure TNavigator.ReloadTasks;
Var
	Orden: Integer;
begin
    if not Assigned(ConnectionCAC) then Exit;
     
	// Nos acordamos d�nde estamos parados ahora
	if not (FSP_OrdenesServicio.Active) or (FSP_OrdenesServicio.RecordCount = 0) then Orden := 0
      else Orden := FSP_OrdenesServicio.FieldByName('CodigoOrdenServicio').AsInteger;

	// Recargamos

//SACAR!!!!
{    FSP_OrdenesServicio.Close;
    if FSP_OrdenesServicio.Parameters.Count = 0 then FSP_OrdenesServicio.Parameters.Refresh;
    FSP_OrdenesServicio.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
    try
        FSP_OrdenesServicio.Open;
        // Volvemos al �tem en el que est�bamos
        if not FSP_OrdenesServicio.Locate('CodigoOrdenServicio', Orden, []) then
          FSP_OrdenesServicio.First;
    except
        on e: exception do begin
            FSP_OrdenesServicio.Close;
            msgbox(e.Message);
        end;
    end;}
    //FIN SACAR!!!!
	// Actualizamos la Barra de Tareas
	{if not FSP_OrdenesServicio.Active or (FSP_OrdenesServicio.RecordCount = 0) then begin
		if FTaskListImage.Enabled then begin
			TaskListVisible := False;
			FTaskListImage.Enabled := False;
			FTaskListImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'TASKLIST_DISABLED');
		end;
	end else begin
		if not FTaskListImage.Enabled then begin
			FTaskListImage.Enabled := True;
			FTaskListImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'TASKLIST_OPEN');
		end;
	end;}
end;
{-----------------------------------------------------------------------------
  Function Name: DoComenzarTarea
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TNavigator.DoComenzarTarea(Sender: TObject);
resourcestring
    MSG_COMENZAR_TAREA = 'La tarea ya ha sido comenzada por otro usuario.';
Var
	Ok: Boolean;
	OS, (* Wrk,*) Tarea: Integer;
begin
	if not (FSP_OrdenesServicio.Active) or (FSP_OrdenesServicio.RecordCount = 0) then begin
		Ok := False;
	end else begin
		OS := FSP_OrdenesServicio.FieldByName('CodigoOrdenServicio').AsInteger;
		//Wrk := FSP_Tareas.FieldByName('CodigoWorkflow').AsInteger;
		Tarea := FSP_OrdenesServicio.FieldByName('CodigoTarea').AsInteger;
		QueryExecute(FSP_OrdenesServicio.Connection, Format(
		  'UPDATE OrdenesServicioTareas SET CodigoUsuario = ''%s'' '+
          'WHERE CodigoOrdenServicio = %d ' +
//          ' and CodigoWorkflow = %d ' +
		  ' AND CodigoTarea = %d AND CodigoUsuario IS NULL',
		  [UsuarioSistema, OS, (*Wrk,*) Tarea]));

		Ok := Trim(QueryGetValue(FSP_OrdenesServicio.Connection, Format(
		  'SELECT CodigoUsuario FROM OrdenesServicioTareas  WITH (NOLOCK) ' +
          ' WHERE ' +
          ' OrdenesServicioTareas.CodigoOrdenServicio = %d ' +
//		  ' AND OrdenesServicio.CodigoOrdenServicio = OrdenesServicioTareas.CodigoOrdenServicio ' +
          //' AND CodigoWorkflow = %d ' +
		  ' AND OrdenesServicioTareas.CodigoTarea = %d', [OS, (*Wrk, *)Tarea]))) = UsuarioSistema;
	end;
	if not Ok then begin
		MsgBox(MSG_COMENZAR_TAREA, CAPTION_COMENZAR_TAREA, MB_ICONSTOP);
	end;
	ReloadTasks;
end;

procedure TNavigator.DoDevolverTarea(Sender: TObject);
Var
	OS, (*Wrk,*) Tarea: Integer;
begin
	if not FSP_OrdenesServicio.Active or (FSP_OrdenesServicio.RecordCount = 0) then Exit;
	OS := FSP_OrdenesServicio.FieldByName('CodigoOrdenServicio').AsInteger;
	Tarea := FSP_OrdenesServicio.FieldByName('CodigoTarea').AsInteger;
   // if Sender = nil then begin
        self.EjecutarDevolverTarea (OS, Tarea);
   //	end;
	ReloadTasks;
end;
{-----------------------------------------------------------------------------
  Function Name: DoCancelarTarea
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TNavigator.DoCancelarTarea(Sender: TObject);
resourcestring
    MSG_CANCELAR_TAREA = 'La tarea que intenta cancelar es opcional, de modo que las ' +
      'dem�s tareas de la orden de Servicio seguir�n ejecut�ndose normalmente.' + CRLF +
	  '�Est� seguro de que desea cancelar esta tarea?';
    MSG_TAREA_OBLIGATORIA = 'La tarea que intenta cancelar es obligatoria, de modo que la orden de servicio' + CRLF +
      'completa ser� cancelada junto con esta tarea.' + CRLF + '�Desea continuar?';
Var
	f: TFormCancelarTarea;
	TipoTarea: AnsiString;
	OS, Wrk, Ver, Tarea: Integer;
begin
	if not (FSP_OrdenesServicio.Active) or (FSP_OrdenesServicio.RecordCount = 0) then Exit;
	OS := FSP_OrdenesServicio.FieldByName('CodigoOrdenServicio').AsInteger;
	Wrk := FSP_OrdenesServicio.FieldByName('CodigoWorkflow').AsInteger;
	Ver := FSP_OrdenesServicio.FieldByName('Version').AsInteger;
	Tarea := FSP_OrdenesServicio.FieldByName('CodigoTarea').AsInteger;
	TipoTarea := QueryGetValue(FSP_OrdenesServicio.Connection, Format(
	  'SELECT TipoTarea FROM TareasWorkflow  WITH (NOLOCK) WHERE CodigoWorkflow = %d ' +
	  'AND Version = %d', [Wrk, Ver]));
	if UpperCase(TipoTarea) = 'O' then begin
		if MsgBox(MSG_CANCELAR_TAREA, CAPTION_CANCELAR_TAREA, MB_ICONWARNING or MB_YESNO) <> IDYES then Exit;
	end else begin
		if MsgBox(MSG_TAREA_OBLIGATORIA, CAPTION_CANCELAR_TAREA, MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2) <> IDYES then Exit;
	end;
	Application.CreateForm(TFormCancelarTarea, f);
	if f.Inicializa(OS, Wrk, Ver, Tarea) and (f.ShowModal = mrOk) then ReloadTasks;
	f.Release;
end;

procedure TNavigator.DoTerminarTarea(Sender: TObject);
Var
	f: TFormTerminarTarea;
	OS, Tarea: Integer;
begin
	if not (FSP_OrdenesServicio.Active) or (FSP_OrdenesServicio.RecordCount = 0) then Exit;
	OS := FSP_OrdenesServicio.FieldByName('CodigoOrdenServicio').AsInteger;
	Tarea := FSP_OrdenesServicio.FieldByName('CodigoTarea').AsInteger;
	Application.CreateForm(TFormTerminarTarea, f);
	if f.Inicializa(OS, Tarea) and (f.ShowModal = mrOk) then ReloadTasks;
	f.Release;
end;

procedure TNavigator.DoTareasContextMenu(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
	if FSP_OrdenesServicio.Active and not FSP_OrdenesServicio.IsEmpty then
      Handled := MostrarMenuContextualOrdenServicio(FTaskList.ClientToScreen(MousePos),
      FSP_OrdenesServicio['CodigoOrdenServicio'], FPuntoEntrega, FPuntoVenta);
end;

(*
procedure TNavigator.DoTaskDblClick(Sender: TObject);
begin
	DoTaskLinkClick(FTaskList, FTaskList.Columns[FTaskCol]);
end;
*)

procedure TNavigator.AutomatizarTarea(CodigoOrdenServicio: integer;
 CodigoProceso: integer = 0);


Var
(*	f: TFormMantenimientoCuenta;
    g: TfrmHabilitarDeshabilitarTag;
    h: TFrmAjusteFacturacion;
    i: TfrmGrabarTags;
    j: TfrmCambiarUbicaciones;
    K: TFormConectores;
    l: TFormCerrarCuenta;
    m: TFormOrdenTrabajo;
    n: TFormTerminarTareaMantenimiento;
 *)
    TodoOk: boolean;
begin
 	TodoOk:= False;
 (*	DoComenzarTarea(nil);
	case CodigoProceso of
    	VA_MANUAL:
        	begin
        		exit
            end;
        VA_ALTA_CUENTA: begin
			Application.CreateForm(TFormMantenimientoCuenta, f);
			if f.InicializaOS(CodigoOrdenServicio) and
            	(f.showModal = mrOk) then begin
				TodoOk := True;
			end;
			f.Release;
          end;
        VA_INHABILITAR_TAG, VA_REHABILITAR_TAG: begin
				Application.createForm(TfrmHabilitarDeshabilitarTag, g);
            	if g.inicializa(CodigoOrdenServicio, False) and (g.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                g.Release;
        	end;
		VA_AJUSTE_FACTURACION: begin
				Application.createForm(TfrmAjusteFacturacion, h);
            	if h.inicializa(False, CodigoOrdenServicio) and (h.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                h.Release;
	       end;
		VA_VINCULAR_TAG: begin
				Application.createForm(TfrmGrabarTags, i);
            	if i.inicializa(False, CodigoOrdenServicio) and (i.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                i.Release;
	       end;
		VA_ENTRAR_TAG: begin
				Application.createForm(TfrmCambiarUbicaciones, j);
                j.inicializa(False, CodigoOrdenServicio);
            	//if j.inicializa(False, CodigoOrdenServicio) and (j.ShowModal = mrOK) then begin
                if (j.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                j.Release;
           end;
		VA_CONECTORES: begin
				Application.createForm(TFormConectores, k);
            	if k.inicializa(CodigoOrdenServicio) and (k.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                k.Release;
           end;
		VA_CERRAR_CUENTA: begin
			 	Application.createForm(TFormCerrarCuenta, l);
            	if l.inicializar(CodigoOrdenServicio) and (l.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                l.Release;
           end;
		VA_ORDEN_TRABAJO: begin
			 	Application.createForm(TFormOrdenTrabajo, M);
            	if M.inicializar(CodigoOrdenServicio) and (M.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                M.Release;
           end;
		VA_TAREA_MANTENIMIENTO: begin
			 	Application.createForm(TFormTerminarTareaMantenimiento, n);
            	if n.inicializar(CodigoOrdenServicio, FSP_Tareas.FieldByName('CodigoTarea').AsInteger) and (n.ShowModal = mrOK) then begin
					TodoOk := True;
                end;
                n.Release;
           end;
    end;

    *)

    if TodoOk then DoTerminarTarea(nil)
		else DoDevolverTarea(nil);
end;

(*
procedure TNavigator.DoTaskMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
Var
	Coord: TGridCoord;
begin
	coord := (Sender As TDPSGrid).MouseCoord(X, Y);
	// Calculamos la columna
	if dgIndicator in (Sender As TDPSGrid).Options then
		FTaskCol := Coord.X - 1
	else
		FTaskCol := Coord.X;
end;
*)

function TNavigator.GetIBVisible: Boolean;
begin
	Result := (PanelIzquierda.Width = IB_MAX_SIZE);
end;

procedure TNavigator.SetIBVisible(const Value: Boolean);
begin
	if (Value = IconBarVisible)then Exit;
	if Value then ArrangeIBWindows(Value);
   	AnimateIconBar;
	if not Value then ArrangeIBWindows(Value);
end;

procedure TNavigator.ArrangeIBWindows(Open: Boolean);
Var
	f: TForm;
	i, MaxX, x, w: Integer;
begin
	MaxX := GetFormClientSize(Application.MainForm).cx - HL_MAX_SIZE + HL_MIN_SIZE;
	if Open then begin
		FIBWindowList.Clear;
		for i := 0 to Screen.FormCount - 1 do begin
			f := Screen.Forms[i];
			if f.FormStyle = fsMDIChild then begin
				if f.Left + f.Width > MaxX then begin
					FIBWindowList.AddObject(IntToStr(f.Left) + ',' +
					  IntToStr(f.Width), f);
					f.Left := 0;
					f.Width := MaxX;
				end;
			end;
		end;
	end else begin
		for i := 0 to FIBWindowList.Count - 1 do begin
			f := TForm(FIBWindowList.Objects[i]);
			x := IVal(ParseParamByNumber(FIBWindowList[i], 1));
			w := IVal(ParseParamByNumber(FIBWindowList[i], 2));
			f.SetBounds(x, f.Top, w, f.Height);
		end;
		FIBWindowList.Clear;
	end;
end;

procedure TNavigator.AnimateIconBar;
const
	STEPS = 4;
	Delays: Array[1..STEPS] of Integer = (20, 10, 5, 0);
	dx = (IB_MAX_SIZE - IB_MIN_SIZE) div STEPS;
//Var
//	i: Integer;
begin
	if IconBarVisible then begin
		// Achicamos
//		FHotLinkImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_OPEN');
		(*for i := 1 to Steps do begin
			PanelIzquierda.Width := PanelIzquierda.Width - dx;
			PanelIzquierda.Update;
            PanelTrucho.Width := PanelIzquierda.Width;
			PanelTrucho.Update;
            BarraIzquierda.Update;
			if Assigned(Application.MainForm.OnResize) then Application.MainForm.OnResize(Application.MainForm);
			Sleep(Delays[i]);
		end;*)
		PanelIzquierda.Width := IB_MIN_SIZE;
		PanelTrucho.Width := PanelIzquierda.Width;
        BarraIzquierda.Update;
	end else begin
		// Agrandanos
//        FHotLinkImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_CLOSE');
		(*for i := 1 to Steps do begin
			PanelIzquierda.Width := PanelIzquierda.Width + dx;
			PanelIzquierda.Update;
			PanelTrucho.Width := PanelIzquierda.Width;
			PanelTrucho.Update;
            BarraIzquierda.Update;
			if Assigned(Application.MainForm.OnResize) then Application.MainForm.OnResize(Application.MainForm);
			Sleep(Delays[i]);
		end;*)
		PanelIzquierda.Width := IB_MAX_SIZE;
		PanelTrucho.Width := PanelIzquierda.Width;
        BarraIzquierda.Update;
	end;
end;

procedure TNavigator.DoAutomatizarTarea(Sender: TObject);
begin
	if (FSP_OrdenesServicio.Active) or (FSP_OrdenesServicio.RecordCount >= 0) then
        AutomatizarTarea(
        	    FSP_OrdenesServicio.FieldByName('CodigoOrdenServicio').AsInteger,
                FSP_OrdenesServicio.FieldByName('CodigoProceso').AsInteger);
end;

procedure TNavigator.EjecutarDevolverTarea(OS, Tarea: integer);
begin
    if MsgBox(MSG_DEVOLVER_TAREA, CAPTION_DEVOLVER_TAREA, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
        QueryExecute(FSP_OrdenesServicio.Connection, Format(
          'UPDATE OrdenesServicioTareas SET CodigoUsuario = NULL WHERE ' +
          'CodigoOrdenServicio = %d AND ' +
          ' CodigoTarea = %d', [OS, (*Wrk,*) Tarea]));
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DoTaskDrawText
  Author:    lgisuk
  Date Created: 04/04/2005
  Description:
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TNavigator.DoTaskDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);

    //descripcion de la prioridad
    Function Prioridad(codigo:integer):string;
    begin
        case codigo of
            1: result:='Muy Alta';
            2: result:='Alta';
            3: result:='Media';
            4: result:='Baja';
        end;
    end;

Var
    Usuario: AnsiString;
begin
    //Candado
    if (Column = FTaskList.Columns[0]) and ((FSP_OrdenesServicio['Estado'] = 'P') or (FSP_OrdenesServicio['Estado'] = 'I') or (FSP_OrdenesServicio['Estado'] = 'E')) then begin
        Usuario := Trim(FSP_OrdenesServicio.FieldByName('Usuario').AsString);
        if AnsiCompareText(Usuario, UsuarioSistema) = 0 then begin
            Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GBitLockOwner);
            ItemWidth := GBitLockOwner.Width + 4;
            DefaultDraw := False;
        end else if Usuario <> '' then begin
            Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GBitLock);
            ItemWidth := GBitLock.Width + 4;
            DefaultDraw := False;
        end;
    end;
    //FechaHoraCreacion
    if Column = FTaskList.Columns[2] then
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', FSP_OrdenesServicio.FieldByName('FechaHoraCreacion').AsDateTime);
   //Prioridad
    if Column = FTaskList.Columns[4] then
        Text := Prioridad(FSP_OrdenesServicio.FieldByName('Prioridad').Asinteger);
   //FechaCompromiso
    if Column = FTaskList.Columns[5] then
        if text = '' then Text := 'No Determinada';
end;

{-----------------------------------------------------------------------------
  Function Name: FTaskListColumns0HeaderClick
  Author:    lgisuk
  Date Created: 30/03/2005
  Description: Ordena por Columna
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TNavigator.FTaskListColumns0HeaderClick(Sender: TObject);
begin
  inherited;
    if FSP_OrdenesServicio.Active = false then exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;

    if FSP_OrdenesServicio.RecordCount > 0 then
        FSP_OrdenesServicio.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;


{-----------------------------------------------------------------------------
  Function Name: FTaskListLinkClick
  Author:    lgisuk
  Date Created: 01/04/2005
  Description: Permito editar al hacer click en la tarea
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------}
procedure TNavigator.FTaskListLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
begin
  inherited;
    if FSP_OrdenesServicio.Active = false then exit;
    EditarOrdenServicio(FSP_OrdenesServicio.fieldbyname('codigoordenservicio').asinteger);
end;

{-----------------------------------------------------------------------------
  Function Name: FTaskListMouseMove
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: Muestro un Hint con el usuario que Tiene el Reclamo Bloqueado
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TNavigator.FTaskListMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
Var
    Estado, Usuario: AnsiString;
begin
    if FSP_OrdenesServicio.Active = false then exit;
    FTaskList.ShowHint:=false;
    FTaskList.hint:='';
    if x < 20 then begin
          try
              //Bloqueado / Desbloqueado
              Usuario := FSP_OrdenesServicio.FieldByName('Usuario').AsString;
              Estado := FSP_OrdenesServicio.FieldByName('Estado').AsString;
              if ((Estado = 'P') or (Estado = 'I')) and (Usuario <> '') then begin  //inclui pendientes internas //(Estado = 'P')//
                  if Usuario <> UsuarioSistema then begin
                       FTaskList.Hint:='Bloqueado por Usuario: '+Usuario;
                       FTaskList.ShowHint:=true;
                  end;
              end;
          except
          end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetIconBarItem
  Author:    flamas
  Date Created: 03/06/2005
  Description: DEvuelve un Icono del Toolbar
  Parameters: const PageName, ItemTitle : AnsiString
  Return Value: TIconBarItem
-----------------------------------------------------------------------------}
function  TNavigator.GetIconBarItem( const PageName, ItemTitle : AnsiString ) : TIconBarItem;
var
	i, j: integer;
begin
	result := nil;
	for i:= 0 to BarraIzquierda.PageCount - 1 do begin
		if AnsiCompareText(BarraIzquierda.Pages[i].Name, PageName) = 0 then begin
			for j:= 0 to BarraIzquierda.Pages[i].Items.Count - 1 do begin
				if (TIconBarItem(BarraIzquierda.Pages[i].Items[j]).Caption = ItemTitle) then begin
                	result := TIconBarItem(BarraIzquierda.Pages[i].Items[j]);
                    Break;
                end;
            end;
            Break;
        end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: SetIconEnabled
  Author:    flamas
  Date Created: 03/06/2005
  Description: Habilita o Deshabilita un �cono del Toolbar
  Parameters: const PageName, ItemTitle : AnsiString; const Value : boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TNavigator.SetIconEnabled( const PageName, ItemTitle : AnsiString; const Value : boolean);
var
	IconBarItem : TIconBarItem;
begin
    IconBarItem := GetIconBarItem(PageName, ItemTitle);
    if ( IconBarItem <> nil ) then IconBarItem.Enabled := Value;
end;


procedure TNavigator.DoOSChanged;
begin
    ReloadTasks;
end;

procedure TNavigator.SetPuntoEntrega(const Value: Integer);
begin
  FPuntoEntrega := Value;
end;

procedure TNavigator.SetPuntoVenta(const Value: Integer);
begin
  FPuntoVenta := Value;
end;

function TNavigator.GetShowBrowserBar: Boolean;
begin
    Result := FCoolBar.Visible;
end;

procedure TNavigator.SetShowBrowserBar(const Value: Boolean);
begin
    if Value = FCoolBar.Visible then Exit;
    FCoolBar.Visible := Value;
end;

function TNavigator.GetShowHotLinkBar: Boolean;
begin
    Result := PanelDerecho.Visible;
end;

procedure TNavigator.SetShowHotLinkBar(const Value: Boolean);
begin
    if Value = PanelDerecho.Visible then Exit;
    PanelDerecho.Visible := Value;
end;

procedure TNavigator.SetIntervaloRefrescoListaTareas(const Value: Integer);
begin
    FIntervaloRefrescoListaTareas := Value;
end;

function TNavigator.GetIntervaloRefrescoListaTareas: Integer;
begin
    Result := FIntervaloRefrescoListaTareas;
end;

{ TNavWindowFrm }

function TNavWindowFrm.CanTerminate: Boolean;
begin
	Result := True;
end;

function TNavWindowFrm.Filter(var FilterBookmak, FilterName: AnsiString): Boolean;
begin
	Result := False;
end;

function TNavWindowFrm.FindFirst(SearchText: AnsiString): Boolean;
begin
	Result := False;
end;

function TNavWindowFrm.FindNext(SearchText: AnsiString): Boolean;
begin
	Result := False;
end;

procedure TNavWindowFrm.DoClose(var Action: TCloseAction);
begin
	//verificar si se puede cerrar
    if FormStyle = fsMDIChild then begin
        if CanTerminate then begin
            Action := caFree;
            if Assigned(FNavigator) then FNavigator.CloseNavWindow(Self);
        end else begin
            Action := caNone;
        end;
    end;
	inherited;
end;

Function TNavWindowFrm.GetBookmark(Var Bookmark, Description: AnsiString): Boolean;
begin
	Result := False;
end;

function TNavWindowFrm.GetCapabilities: TNavCapabilities;
begin
	Result := [ncFind, ncRefresh, ncFilter];
end;

function TNavWindowFrm.GotoBookmark(Bookmark: AnsiString): Boolean;
begin
	Result := True;
end;

function TNavWindowFrm.Inicializa: Boolean;
begin
	SetBounds(0, 0, GetFormClientSize(Application.MainForm).cx, GetFormClientSize(Application.MainForm).cy);
	Result := True;
end;

function TNavWindowFrm.Inicializa(MDIChild: Boolean): Boolean;
begin
	if MDIChild then begin
		SetBounds(0, 0, GetFormClientSize(Application.MainForm).cx,
		  GetFormClientSize(Application.MainForm).cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
		CenterForm(Self);
    end;
	Result := True;
end;

procedure TNavWindowFrm.Loaded;
begin
	inherited;
	Color := $00E0CCB3;
    KeyPreview := True;
end;

procedure TNavWindowFrm.RefreshData;
begin

end;
procedure TNavigator.AddHotLink(const ItemTitle, Hint: Ansistring;
  Fuente: TFont; Bitmap: TBitmap; Event: TEventoHotLink; Params: AnsiString = '');
Var
	i: integer;
    TextYPosition: integer;
begin
	// Agregamos un Hot Link
    if FHotlinksCount = 0 then begin
        FHotLinkImage.Enabled := True;
	    if HotLinksVisible then
    		FHotLinkImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_CLOSE')
	    else begin
    		FHotLinkImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_OPEN');
        end;
    end;

//    TextYPosition := HL_TEXT_YPOSITION;
    TextYPosition := HL_TEXT_INTERLEAVE;
    For i := 0 to ScrollBox.ControlCount - 1 do begin
        if (ScrollBox.Controls[i] is TLabel)
        	or (ScrollBox.Controls[i] is TSeparator) then begin
			TextYPosition := TextYPosition +
            	ScrollBox.Controls[i].Height +
                iif( ScrollBox.Controls[i] is TLabel, HL_TEXT_INTERLEAVE, HL_TEXT_INTERLEAVE + HL_SEPARATOR_INTERLEAVE);
        end;
    end;


    With TLabel.Create(Self) do begin
    	SetBounds( HL_TEXT_XPOSITION, TextYPosition, HL_MAX_SIZE - HL_TEXT_XPOSITION - 40, 400);
        Autosize		:= False;
        Parent 			:= ScrollBox;
        Hint			:= Hint;
        Font 			:= Fuente;
        Cursor 		 	:= crHandPoint;
        Transparent		:= True;
        Wordwrap 		:= True;
        Caption 		:= ItemTitle;
        Autosize		:= True;
        ShowAccelChar   := True;
        OnMouseEnter 	:= DoMouseEnter;
        OnMouseLeave 	:= DoMouseLeave;
        Tag             := FHotLinksCount;
        OnClick		 	:= DoHotLinkClick;
	end;
    With TImage.Create(Self) do begin
    	Parent 			:= ScrollBox;
		Stretch 	   	:= True;
        Transparent		:= True;
        Cursor 		 	:= crHandPoint;
        SetBounds(HL_ICON_XPOSITION, TextYPosition, 20, 20);
        if Assigned(Bitmap) then Picture.Bitmap.Assign(Bitmap);
        Tag             := FHotLinksCount;
        OnClick		 	:= DoHotLinkClick;
    end;
    FHotLinkData[FHotlinksCount].Params := Params;
    FHotLinkData[FHotlinksCount].Evento := Event;
	Inc(FHotlinksCount);
end;

procedure TNavigator.ClearHotLinks;
begin
	// Borramos los HotLinks. Empiezo desde la posicion 2
    // 	porque la posicion 0 y 1 son la imagen y label del titulo respectivamente.
    While ScrollBox.ControlCount > 2 do ScrollBox.Controls[2].Free;
    FHotlinksCount := 0;
    // Cerramos la lista de HotLinks.
    HotLinksVisible 	  := False;
  	FHotLinkImage.Enabled := False;
	FHotLinkImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'HOTLINK_DISABLED');
end;

Constructor THistory.Create(AOwner: TComponent);
begin
	inherited;
    try
	    fHistoryLIst	:= TStringList.Create();
    	FItemIndex 		:= -1;
    except;
    end;
end;


//History

Function THistory.GetItemIndex: integer;
begin
	Result:= FItemIndex;
end;

procedure THistory.SetItemIndex(Const Value: integer);
begin
	FItemIndex := Value;
end;

Function  THistory.GetItemsCount: integer;
begin
	if Assigned(self) then result := FHistoryList.Count
    else result := 0;
end;

procedure THistory.AddToHistory(Pagina: TNavWindowClass; BookMark, Descripcion: Ansistring);
begin
    While FHistoryList.Count > FItemIndex + 1 do begin
    	FHistoryList.Delete(FItemIndex + 1);
    end;
	FHistoryList.AddObject('d=' + Descripcion + ',b=' + BookMark, TObject(Pagina));
	FItemIndex:= FHistoryList.Count - 1;
end;

Function THistory.GetDescription(Index: integer): AnsiString;
begin
	Result := copy(FHistoryList.Strings[Index], 3, Pos(',b=', FHistoryList.Strings[Index]) -3);
end;

Function THistory.GetBookmark(Index: integer): AnsiString;
begin
	//retocar
	Result := Trim(copy(FHistoryList.Strings[Index],
      Pos(',b=', FHistoryList.Strings[Index]) + 3,
	  Length(FHistoryList.Strings[Index])) );
end;

Function THistory.GetObject(Index: integer): TNavWindowClass;
begin
	Result := TNavWindowClass(FHistoryList.Objects[Index]);
end;


Procedure THistory.RemoveFwdHistory;
begin
    While FHistoryList.Count > FItemIndex + 2 do begin
    	FHistoryList.Delete(FItemIndex + 2);
    end;
    FItemIndex := ItemsCount - 1;
end;

procedure TNavWindowFrm.KeyDown(var Key: Word; Shift: TShiftState);
begin
    inherited;
    if (Shift = [ssAlt]) and (Key = VK_LEFT) then Navigator.GoBack;
    if (Shift = [ssAlt]) and (Key = VK_RIGHT) then Navigator.GoForward;
    if (Shift = []) and (Key = VK_ESCAPE) then close;
end;

procedure TNavWindowFrm.AddFilterText(Filter: AnsiString);
begin
    with Fnavigator.cb_filter.items do begin
        Add(Filter);
    end;
end;

procedure TNavWindowFrm.ClearFilters;
begin
    if Fnavigator.cb_filter.Items.Count < 0 then exit;
    fnavigator.cb_filter.Items.Clear;
end;

procedure TNavWindowFrm.ControlsAligned;
begin
	inherited;
end;


function TNavWindowFrm.Inicializa(ArchiveFile, ReportName, ReportTitle: AnsiString; CodigoCliente: integer): Boolean;
begin
	SetBounds(0, 0, GetFormClientSize(Application.MainForm).cx, GetFormClientSize(Application.MainForm).cy);
	Result := True;
end;

function GetLockBitmap: TBitmap;
begin
    Result := GBitLock;
end;

function GetLockOwnerBitmap: TBitmap;
begin
    Result := GBitLockOwner;
end;

{ TSeparator }

constructor TSeparator.Create(AOwner: TComponent);
begin
  inherited;
  BevelOuter := bvNone;
end;

destructor TSeparator.destroy;
begin
  inherited;
end;


initialization
	GBitLock := TBitmap.Create;
	GBitLock.LoadFromResourceName(HInstance, 'TASK_LOCK');
    GBitLock.TransparentColor := clOlive;
    GBitLock.Transparent := True;
    //
	GBitLockOwner := TBitmap.Create;
	GBitLockOwner.LoadFromResourceName(HInstance, 'TASK_LOCK_OWNER');
    GBitLockOwner.TransparentColor := clOlive;
    GBitLockOwner.Transparent := True;
finalization
    FreeAndNil(GBitLock);
    FreeAndNil(GBitLockOwner);
end.
