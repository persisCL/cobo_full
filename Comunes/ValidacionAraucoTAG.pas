{-----------------------------------------------------------------------------
 File Name      : ValidacionAraucoTAG
 Author         : Claudio Quezada Ib��ez
 Firma          : SS_1151_CQU_20140103
 Date Created   : 03-01-2014
 Language       : ES-CL
 Description    : Clase Gen�rica que contendr� los m�todos, funciones
                  y todo lo necesario para AraucoTAG (Consultas, Altas, Bajas, etc)
-----------------------------------------------------------------------------}
unit ValidacionAraucoTAG;

interface

uses
    Classes,
    Conexion, Util, SysUtils, SysUtilsCN,
    ConstParametrosGenerales, RespuestaAraucoTAG,
    PeaTypes, Types, RStrings,
    ADODB, DB, PeaProcs, Variants;

type

    TValidacionAraucoTAG = class
    private
        FDatosPersonales        : TDatosPersonales;
        FIDUsuarioWebService    : Integer;
        FEMailPersona,
        FUsuarioSistema         : string;
        FAdheridoEnvioEMail     : Boolean;
        oCnx                    : TDMConexion;
        FConsultaPrevia         : TRespuesta;
        /// <summary>Obtiene el m�ximo de cuentas permitidas para Adherir a Parque Arauco</summary>
        /// <returns>Cantidad m�xima de cuentas permitidas a adherir</returns>
        function getMaximoCuentasPermitidas : Integer;
        /// <summary>Obtiene si debe enviar o no el detalle en la respuesta</summary>
        /// <returns>True si debe enviar; False si no</returns>
        function getInformarDetalle : Boolean;
        /// <summary>Funcion que devuelve un objeto de tipo TMensajes</summary>
        /// <param name="Codigo">Codigo del mensaje</param>
        /// <param name="Mensaje">Texto del mensaje</param>
        /// <returns>TMensaje</returns>
        function AgregarError(Codigo : Integer; Mensaje : string) : TMensajes;
        /// <summary>Realiza la actualizaci�n del convenio o los convenios de la persona a adherir</summary>
        /// <param name="pCodigoPersona">C�digo de la persna a adherir</param>
        procedure ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
        /// <summary>Actualiza el correo elctr�nico de la persona a Adherir</summary>
        /// <param name="pEMail">eMail a actualizar</param>
        /// <param name="pCodigoPersona">Codigo de la persona a adherir</param>
        procedure ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
        /// <summary>Inicializa los datos de la persona a consultar</summary>
        function getPersona : TDatosPersonales;
        /// <summary>Carga el Record FPersona</summary>
        /// <param name="NumeroDocumento">Rut del cliente</param>
        procedure CargarDatosPersona(NumeroDocumento : string);
        /// <summary>Indica si se env�a el detalle en la respuesta</summary>
        property InformarDetalle : Boolean read getInformarDetalle;
    public
        /// <summary>Constructor de la clase</summary>
        /// <param name="pCnx">Conexi�n a Base de Datos del tipo TDMConexion</param>
        /// <param name="UsuarioSistema">Nombre de usuario del Sistema</param>
        constructor Create(pCnx : TDMConexion; UsuarioSistema : String); overload;
        /// <summary>Constructor de la clase</summary>
        /// <param name="pCnx">Conexi�n a Base de Datos del tipo TDMConexion</param>
        /// <param name="UsuarioSistema">Nombre de usuario del Sistema</param>
        /// <param name="pIDConsultaPAK">Identificador Unico de la consulta</param>
        constructor Create(pCnx : TDMConexion; UsuarioSistema : String; pIDConsultaPAK : Integer); overload;
        /// <summary>Datos de la persona a consultar</summary>
        property DatosPersona : TDatosPersonales read getPersona;
        /// <summary>M�ximo de cuentas a procesar por el WebService.</summary>
        property MaximoCuentasPermitidas : Integer read getMaximoCuentasPermitidas;
        /// <summary>Le agrega los 0 necesarios al Rut para dejarlo como en el sistema</summary>
        /// <param name="pNumeroDocumento">Rut del cliente</param>
        /// <returns>Rut del cliente con 0 al inicio</returns>
        function RellenarRut(pNumeroDocumento : string) : string;
        /// <summary>Audita los errores del sistema</summary>
        /// <param name="pMensajeDeError">Mensaje de error</param>
        procedure AgregarErrorWebServicePAK(pMensajeDeError : string);
        /// <summary>Registra la consulta WEB y devuelve el ID</summary>
        /// <param name="pIDConsultaPAK">Identificador Unico de la consulta</param>
        /// <param name="pCodigoPersona">Codigo del Cliente</param>
        /// <param name="pNumeroDocumento">Rut del cliente</param>
        /// <param name="pMotivo">Mensaje de la consulta</param>
        /// <param name="pPuedeAdherir">Indica si puede o no Adherir</param>
        /// <returns>Identificador Unico de la consulta</returns>
        function AuditarConsultaWebServicePAK(pIDConsultaPAK, pCodigoPersona : Integer; pNumeroDocumento, pMotivo : string; pPuedeAdherir : Boolean) : Integer;
        /// <summary>Indica si el cliente est� adherido a la recepci�n de eMail</summary>
        /// <param name="pNumeroDocumento">Numero del cliente (RUT)</param>
        /// <returns>True si recibe eMail; False si no</returns>
        function ClienteAdheridoMail(pNumeroDocumento : String) : Boolean;
        /// <summary>Valida que el rut ingresado se encuentre en el sistema</summary>
        /// <param name="Numero">Numero del cliente (RUT)</param>
        /// <returns>True si lo encuentra; False si no</returns>
        function ValidarRut(Numero : string) : Boolean;
        /// <summary>Verifica si el cliente puede ser adherido a Parque Arauco.</summary>
        /// <param name="IDConsultaPAK">Identificador Unico de la consulta</param>
        /// <param name="NumeroDocumento">Rut del cliente</param>
        /// <param name="pRespuesta">TRespuesta con los datos del cliente y las cuentas asociadas</param>
        procedure ValidarClienteAraucoTAG(IDConsultaPAK : Integer; NumeroDocumento: AnsiString; var pRespuesta : TRespuesta);
        /// <summary>Actualiza el correo elctr�nico de la persona a adherir</summary>
        /// <param name="eMail">Nuevo eMail de la persona</param>
        /// <param name="NumeroDocumento">Rut del cliente</param>
        /// <returns>True si actualiz�; False si no</returns>
        function ActualizarMail(eMail, NumeroDocumento : AnsiString): Boolean;
        /// <summary>Busca en la Base de datos la consulta seg�n la llave</summary>
        /// <param name="IDConsultaPAK">Identificador Unico de la consulta</param>
        /// <param name="pRespuesta">TRespuesta con los datos del cliente y las cuentas adheridas o no (con mensaje de respuesta)</param>
        /// <returns>true si obtuvo datos; false si no</returns>
        function ObtenerConsultaPrevia(IDConsultaPAK : Integer; var pRespuesta : TRespuesta) : Boolean;
        /// <summary>Guarda una patente indicada por el cliente para ser dada de alta</summary>
        /// <param name="IDConsultaPAK">Identificador Unico de la consulta</param>
        /// <param name="Patente">Patente a Adherir</param>
        /// <returns>Identificador Unico de la Adhesi�n</returns>
        function AgregarAltaAraucoTAG(IDConsultaPAK : Integer; Patente : string) : Integer;
        /// <summary>Obtiene el �ndice de la patente buscada</summary>
        /// <param name="Value">Patente a buscar</param>
        /// <param name="pPatentes">Arreglo de TPatenteConsultar</param>
        /// <returns>Indice de la patente encontrada o -1 si no la encuentra</returns>
        function ObtenerIndicePatente(const Value: String; pPatentes: TPatentes) : Integer;
        /// <summary>Realiza al Adhesi�n al servicio AraucoTAG</summary>
        /// <param name="IDConsultaPAK">Identificador Unico de la consulta</param>
        /// <param name="NumeroDocumento">Rut del cliente</param>
        /// <param name="eMail">Nuevo eMail de la persona</param>
        /// <param name="Patentes">Patentes a dar de Alta</param>
        /// <param name="oRespuesta">TRespuesta con los datos del cliente y las cuentas adheridas o no (con mensaje de respuesta)</param>
        procedure AdherirClienteAraucoTAG(IDConsultaPAK : Integer; NumeroDocumento, eMail : AnsiString; Patentes : TStringDynArray; out oRespuesta: TRespuesta);
    end;

implementation

const
    cSQLObtenerEmailPersona                     = 'SELECT dbo.ObtenerEmailPersona(%d)';
    cSQLValidarConveniosPersonaEnvioPorEMail    = 'SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)';
    cSQLValidarPersonaAdheridaPA                = 'SELECT dbo.ValidarPersonaAdheridaPAK(%d)';
    cSQLCtasActivasPorPersona                   = 'SELECT dbo.ObtenerCantidadCuentasActivasPorPersona(%d)';
    cSQLObtenerUsuarioWebService                = 'SELECT dbo.ObtenerIDUsuarioWebService(''%s'', NULL, NULL)';

    constructor TValidacionAraucoTAG.Create(pCnx : TDMConexion; UsuarioSistema : String);
    begin
        oCnx := pCnx;
        FUsuarioSistema := UsuarioSistema;
        FIDUsuarioWebService := QueryGetIntegerValue(oCnx.BaseCAC, Format(cSQLObtenerUsuarioWebService, [FUsuarioSistema]));
        if FIDUsuarioWebService = -1 then begin
            AgregarErrorWebServicePAK('Usuario Invalido');
            raise Exception.Create('Usuario Invalido');
        end;
    end;

    constructor TValidacionAraucoTAG.Create(pCnx : TDMConexion; UsuarioSistema : String; pIDConsultaPAK : Integer);
    begin
        oCnx := pCnx;
        FUsuarioSistema := UsuarioSistema;
        FIDUsuarioWebService := QueryGetIntegerValue(oCnx.BaseCAC, Format(cSQLObtenerUsuarioWebService, [FUsuarioSistema]));
        if FIDUsuarioWebService = -1 then begin
            AgregarErrorWebServicePAK('Usuario Invalido');
            raise Exception.Create('Usuario Invalido');
        end;
        ObtenerConsultaPrevia(pIDConsultaPAK, FConsultaPrevia)
    end;

    procedure TValidacionAraucoTAG.CargarDatosPersona(NumeroDocumento : string);
    begin
        try
            // Obtengo los datos de la persona
            FDatosPersonales := ObtenerDatosPersonales(oCnx.BaseCAC, 'RUT', NumeroDocumento);
            // Obtengo si recibe o no correos electr�nicos
            FAdheridoEnvioEMail := ClienteAdheridoMail(NumeroDocumento);
            // Obtengo el email de la persona
            FEMailPersona := QueryGetStringValue(oCnx.BaseCAC, Format(cSQLObtenerEmailPersona, [DatosPersona.CodigoPersona]));
        except
            on e: Exception do begin
                AgregarErrorWebServicePAK('procedure ValidacionAraucoTAG.CargarDatosPersona, ' + e.Message);
                raise Exception.Create('procedure ValidacionAraucoTAG.CargarDatosPersona, ' + e.Message);
            end;
        end;
    end;

    function TValidacionAraucoTAG.getPersona;
    begin
        Result := FDatosPersonales;
    end;

    function TValidacionAraucoTAG.ObtenerIndicePatente(const Value: String; pPatentes: TPatentes): Integer;
    var
        i: Integer;
    begin
        Result := -1;
        for i := 0 to Length(pPatentes) do
            begin
            if Trim(Value) = Trim(pPatentes[i].Patente) then
            begin
                Result := i;
                Break;
            end;
        end;
    end;

    function TValidacionAraucoTAG.ClienteAdheridoMail(pNumeroDocumento : String) : Boolean;
    var
        Retorno : Boolean;
    begin
        Retorno := False;
        try
            try
                // Verifico que reciba eMail
                Retorno := QueryGetBooleanValue(oCnx.BaseCAC, Format(cSQLValidarConveniosPersonaEnvioPorEMail, [DatosPersona.CodigoPersona]));
            except
                on e: Exception do begin
                    AgregarErrorWebServicePAK('procedure ValidacionAraucoTAG.ClienteAdheridoMail, ' + e.Message);
                    raise Exception.Create('procedure ValidacionAraucoTAG.ClienteAdheridoMail, ' + e.Message);
                end;
            end;
        finally
            Result := Retorno;
        end;
    end;

    procedure TValidacionAraucoTAG.AgregarErrorWebServicePAK(pMensajeDeError : string);
    var
        spAgregarErrorWebServicePAK: TADOStoredProc;
    begin
        try
            spAgregarErrorWebServicePAK := TADOStoredProc.Create(nil);
            with spAgregarErrorWebServicePAK do begin
                Connection := oCnx.BaseCAC;
                ProcedureName := 'AgregarErrorWebServicePAK';
                Parameters.Refresh;
                Parameters.ParamByName('@MensajeError').Value := pMensajeDeError;
            end;
            spAgregarErrorWebServicePAK.ExecProc;
        finally
            if Assigned(spAgregarErrorWebServicePAK) then FreeAndNil(spAgregarErrorWebServicePAK);
        end;
    end;

    function TValidacionAraucoTAG.AuditarConsultaWebServicePAK(pIDConsultaPAK, pCodigoPersona : Integer; pNumeroDocumento, pMotivo : string; pPuedeAdherir : Boolean) : Integer;
        var
            spAgregarConsultaWebServicePAK: TADOStoredProc;
            Retorno : Integer;
    begin
        Retorno := 0;
        try
            try
                spAgregarConsultaWebServicePAK := TADOStoredProc.Create(nil);

                with spAgregarConsultaWebServicePAK do begin
                    Connection    := oCnx.BaseCAC;
                    ProcedureName := 'AgregarConsultaWebServicePAK';
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoPersona').Value         := pCodigoPersona;          // Codigo del Cliente a Consultar / Adherir
                        ParamByName('@NumeroDocumento').Value       := pNumeroDocumento;        // Rut del cliente a Consultar / Adherir
                        ParamByName('@IDUsuarioWebService').Value   := FIDUsuarioWebService;    // ID de quien realiza la Consulta / Adhesion
                        ParamByName('@PuedeAdherir').Value          := pPuedeAdherir;           // Inidica si puede adherir o no
                        ParamByName('@Motivo').Value                := pMotivo;                 // Motivo de la consulta, rechazo o aceptaci�n
                        if pIDConsultaPAK > 0 then
                            ParamByName('@IDConsultaPAK').Value     := pIDConsultaPAK;          // Coloco el ID de la Consulta
                    end;
                end;
// Descomentar cuando est� lo de las transacciones
//                oCnx.BaseCAC.BeginTrans;
                spAgregarConsultaWebServicePAK.ExecProc;
                Retorno := spAgregarConsultaWebServicePAK.Parameters.ParamByName('@IDConsultaPAK').Value;    // Identificador Unico
//                if oCnx.BaseCAC.InTransaction then oCnx.BaseCAC.CommitTrans;
            except
                on e: Exception do begin
//                    if oCnx.BaseCAC.InTransaction then oCnx.BaseCAC.RollbackTrans;
                    AgregarErrorWebServicePAK('procedure ValidacionAraucoTAG.AgregarConsultaWebServicePAK, ' + e.Message);
                    raise Exception.Create('procedure ValidacionAraucoTAG.AgregarConsultaWebServicePAK, ' + e.Message);
                end;
            end;
        finally
            if Assigned(spAgregarConsultaWebServicePAK) then FreeAndNil(spAgregarConsultaWebServicePAK);
            Result := Retorno;
        end;
    end;

    function TValidacionAraucoTAG.getMaximoCuentasPermitidas : Integer;
    var
        Valor : Integer;
    begin
        if not ObtenerParametroGeneral(oCnx.BaseCAC, CANT_MAX_PATENTES_PROCESAR, Valor) then Valor := 0;
        Result := Valor;
    end;

    function TValidacionAraucoTAG.getInformarDetalle : Boolean;
    var
        Valor : Integer;
    begin
        if not ObtenerParametroGeneral(oCnx.BaseCAC, WEBSERVICE_PAK_INFORMAR_DETALLE, Valor) then Valor := 0;
        Result := IIf(Valor=1, True, False);
    end;

    function TValidacionAraucoTAG.RellenarRut(pNumeroDocumento : string) : string;
    begin
        Result := strright('000000000000000' + trim(pNumeroDocumento), 9);
    end;

    function TValidacionAraucoTAG.ValidarRut(Numero : string) : Boolean;
    begin
        try
            Result := PeaProcs.ValidarRUT(oCnx.BaseCAC, Numero);
        except
            Result := False;
        end;
    end;

    function TValidacionAraucoTAG.ObtenerConsultaPrevia(IDConsultaPAK : Integer; var pRespuesta : TRespuesta) : Boolean;
    var
        spObtenerConsultaPrevia : TADOStoredProc;
        tmpRespuesta : TRespuesta;
        Vehiculos   : TPatentes;
        Retorno,
        PuedeAdherirPatente : Boolean;
        I : Integer;
    begin
        tmpRespuesta := TRespuesta.Create;
        Retorno := False;
        if Assigned(FConsultaPrevia) then begin
            pRespuesta := FConsultaPrevia;
            Retorno := True;
        end else begin
            try
                try
                    spObtenerConsultaPrevia := TADOStoredProc.Create(nil);
                    with spObtenerConsultaPrevia do begin
                        Connection    := oCNX.BaseCAC;
                        ProcedureName := 'ObtenerConsultaWebServicePAK';
                        Parameters.Refresh;
                        Parameters.ParamByName('@IDConsultaPAK').Value := IDConsultaPAK;
                    end;
                    spObtenerConsultaPrevia.Open;
                    if spObtenerConsultaPrevia.IsEmpty then begin
                        tmpRespuesta.PuedeAdherir := False;
                        tmpRespuesta.Mensajes := AgregarError(-1, 'No se encontr� consulta previa para el cliente especificado - ID =' +  IntToStr(IDConsultaPAK));
                    end else begin
                        Retorno := True;
                        // Cargo la Respuesta
                        tmpRespuesta.Persona.Rut := DatosPersona.NumeroDocumento;
                        tmpRespuesta.Persona.Nombre := DatosPersona.Nombre + ' ' + DatosPersona.Apellido + ' ' + DatosPersona.ApellidoMaterno;
                        tmpRespuesta.Persona.Mail := FEMailPersona;

                        SetLength(Vehiculos, spObtenerConsultaPrevia.RecordCount);

                        spObtenerConsultaPrevia.First;
                        I := 0;
                        while not spObtenerConsultaPrevia.Eof do begin
                            if tmpRespuesta.ConsultaPAK = 0 then tmpRespuesta.ConsultaPAK := spObtenerConsultaPrevia.FieldByName('IDConsultaPAK').Value;
                            if tmpRespuesta.Persona.Rut = '' then tmpRespuesta.Persona.Rut := spObtenerConsultaPrevia.FieldByName('NumeroDocumento').Value;
                            // Cargo la Patente
                            Vehiculos[I] := TPatenteConsultar.Create;
                            Vehiculos[I].Patente := Trim(spObtenerConsultaPrevia.FieldByName('Patente').AsString);
                            // Evaluo el Bit de Puede Adherir
                            if spObtenerConsultaPrevia.FieldByName('PuedeAdherirPatente').AsBoolean then
                                PuedeAdherirPatente := True else PuedeAdherirPatente := False;
                            // Cargo los datos de la Patente
                            Vehiculos[I].EstadoPatente := TMensaje.Create;
                            Vehiculos[I].EstadoPatente.CodigoMensaje := IIf(PuedeAdherirPatente, 0, -1);
                            Vehiculos[I].EstadoPatente.Mensaje := IIf(PuedeAdherirPatente, rsPatentePuedeAdeherir, rsPatenteNoPuedeAdeherir);
                            Inc(I);
                            spObtenerConsultaPrevia.Next;
                        end;
                        tmpRespuesta.Patentes := Vehiculos;
                    end;
                    spObtenerConsultaPrevia.Close;
                except
                    on e: Exception do begin
                        Retorno := False;
                        tmpRespuesta.PuedeAdherir := False;
                        tmpRespuesta.Mensajes := AgregarError(-1, Format(rsErrorConsulta, [e.Message]));
                    end;
                end;  
            finally
                pRespuesta := tmpRespuesta;
            end;
        end;
        Result := Retorno;
    end;

    procedure TValidacionAraucoTAG.ActualizaEMailPersona(pEMail: string; pCodigoPersona: Integer);
        var
            CodigoMedioComunicacionEmailPersona: Integer;
            spActualizarMedioComunicacionPersona: TADOStoredProc;
        const
            cSQLObtenerCodigoMedioComunicacionEmailPersona = 'SELECT dbo.ObtenerCodigoMedioComunicacionEmailPersona(%d)';
    begin
        try
            try
                CodigoMedioComunicacionEmailPersona := QueryGetIntegerValue(oCNX.BaseCAC, Format(cSQLObtenerCodigoMedioComunicacionEmailPersona, [pCodigoPersona]));

                spActualizarMedioComunicacionPersona := TADOStoredProc.Create(nil);

                with spActualizarMedioComunicacionPersona do begin
                    Connection    := oCNX.BaseCAC;
                    ProcedureName := 'ActualizarMedioComunicacionPersona';
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoTipoMedioContacto').Value := 1;
                        ParamByName('@CodigoArea').Value              := null;
                        ParamByName('@Valor').Value                   := pEMail;
                        ParamByName('@Anexo').Value                   := null;
                        ParamByName('@CodigoDomicilio').Value         := null;
                        ParamByName('@HorarioDesde').Value            := null;
                        ParamByName('@HorarioHasta').Value            := null;
                        ParamByName('@Observaciones').Value           := null;
                        ParamByName('@CodigoPersona').Value           := pCodigoPersona;
                        ParamByName('@Principal').Value               := 0;
                        ParamByName('@EstadoVerificacion').Value      := null;
                        ParamByName('@CodigoMedioComunicacion').Value := IIf(CodigoMedioComunicacionEmailPersona = 0, null, CodigoMedioComunicacionEmailPersona);
                        ParamByName('@Usuario').Value                 := UsuarioSistema;
                    end;
                end;
// Descomentar cuando est� lo de las transacciones
                //oCnx.BaseCAC.BeginTrans;
                spActualizarMedioComunicacionPersona.ExecProc;
                //if oCNX.BaseCAC.InTransaction then oCNX.BaseCAC.CommitTrans;
            except
                on e: Exception do begin
                    //if oCNX.BaseCAC.InTransaction then oCNX.BaseCAC.RollbackTrans;
                    AgregarErrorWebServicePAK('procedure TAdhesionPreInscripcionPAKForm.ActualizaEMailPersona, ' + e.Message);
                    raise Exception.Create('procedure TAdhesionPreInscripcionPAKForm.ActualizaEMailPersona, ' + e.Message);
                end;
            end;
        finally
            if Assigned(spActualizarMedioComunicacionPersona) then FreeAndNil(spActualizarMedioComunicacionPersona);
        end;
    end;

    procedure TValidacionAraucoTAG.ActualizarConveniosPersonaEnvioPorEMail(pCodigoPersona: Integer);
        var
            spActualizarConveniosPersonaEnvioPorEMail: TADOStoredProc;
    begin
        try
            try
                spActualizarConveniosPersonaEnvioPorEMail := TADOStoredProc.Create(nil);

                with spActualizarConveniosPersonaEnvioPorEMail do begin
                    Connection    := oCnx.BaseCAC;
                    ProcedureName := 'ActualizarConveniosPersonaEnvioPorEMail';
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoPersona').Value := pCodigoPersona;
                        ParamByName('@Usuario').Value       := UsuarioSistema;
                    end;
                end;
// Descomentar cuando est� lo de las transacciones
//                oCnx.BaseCAC.BeginTrans;
                spActualizarConveniosPersonaEnvioPorEMail.ExecProc;
//                if oCnx.BaseCAC.InTransaction then oCnx.BaseCAC.CommitTrans;
            except
                on e: Exception do begin
//                    if oCnx.BaseCAC.InTransaction then oCnx.BaseCAC.RollbackTrans;
                    AgregarErrorWebServicePAK('procedure ValidacionAraucoTAG.ActualizarConveniosPersonaEnvioPorEMail, ' + e.Message);
                    raise Exception.Create('procedure ValidacionAraucoTAG.ActualizarConveniosPersonaEnvioPorEMail, ' + e.Message);
                end;
            end;
        finally
            if Assigned(spActualizarConveniosPersonaEnvioPorEMail) then FreeAndNil(spActualizarConveniosPersonaEnvioPorEMail);
        end;
    end;

    function TValidacionAraucoTAG.ActualizarMail(eMail, NumeroDocumento : AnsiString): Boolean;
    var
        EMailPersona : string;
        ClienteAdheridoEnvioEMail : Boolean;
    begin
        try
            // Obtengo el email de la persona
            EMailPersona := QueryGetStringValue(oCnx.BaseCAC, Format(cSQLObtenerEmailPersona, [DatosPersona.CodigoPersona]));
            // Obtengo si recibe o no correos electr�nicos
            ClienteAdheridoEnvioEMail :=  ClienteAdheridoMail(NumeroDocumento);
            // Si los eMails son iguales, s�lo actualizo el env�o
            if (Trim(EMailPersona) = Trim(eMail)) and not ClienteAdheridoEnvioEMail then
                ActualizarConveniosPersonaEnvioPorEMail(DatosPersona.CodigoPersona)
            else if (EMailPersona <> eMail) then begin
                // Actalizo el eMail
                ActualizaEMailPersona(Trim(eMail), DatosPersona.CodigoPersona);
                // Actualizo el Convenio s�lo si no tiene el bit
                if not ClienteAdheridoEnvioEMail then
                    ActualizarConveniosPersonaEnvioPorEMail(DatosPersona.CodigoPersona);
            end;
            Result := True;
        except
            on e: Exception do begin
                AgregarErrorWebServicePAK('procedure TAdhesionPreInscripcionPAKForm.ActualizarMail, ' + e.Message);
                raise Exception.Create('procedure TAdhesionPreInscripcionPAKForm.ActualizarMail, ' + e.Message);
                Result := False;
            end;
        end;
//        Finalize(FDatosPersonales);
    end;

    function TValidacionAraucoTAG.AgregarError(Codigo : Integer; Mensaje : string) : TMensajes;
    var
        oMensaje : TMensajes;
    begin
        SetLength(oMensaje, 1);
        oMensaje[0] := TMensaje.Create;
        oMensaje[0].CodigoMensaje := Codigo;
        oMensaje[0].Mensaje := Mensaje;
        Result := oMensaje;
    end;

    function TValidacionAraucoTAG.AgregarAltaAraucoTAG(IDConsultaPAK : Integer; Patente : string) : Integer;
        var
            spAgregarAltasWebServicePAK: TADOStoredProc;
            Resultado : Integer;
    begin
        Resultado := -1;
        try
            try
                spAgregarAltasWebServicePAK := TADOStoredProc.Create(nil);

                with spAgregarAltasWebServicePAK do begin
                    Connection    := oCnx.BaseCAC;
                    ProcedureName := 'AgregarAltasWebServicePAK';
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@IDConsutaPAK').Value          := IDConsultaPAK;
                        ParamByName('@Patente').Value               := Patente;
                        ParamByName('@IDUsuarioWebService').Value   := FIDUsuarioWebService;
                    end;
                end;
// Descomentar cuando est� lo de las transacciones
//                oCnx.BaseCAC.BeginTrans;
                spAgregarAltasWebServicePAK.ExecProc;
                Resultado := spAgregarAltasWebServicePAK.Parameters.ParamByName('@IDAltasWebServicePAK').Value;
//                if oCnx.BaseCAC.InTransaction then oCnx.BaseCAC.CommitTrans;
            except
                on e: Exception do begin
//                    if oCnx.BaseCAC.InTransaction then oCnx.BaseCAC.RollbackTrans;
                    AgregarErrorWebServicePAK('procedure ValidacionAraucoTAG.AgregarAltaAraucoTAG, ' + e.Message);
                    raise Exception.Create('procedure ValidacionAraucoTAG.AgregarAltaAraucoTAG, ' + e.Message);
                end;
            end;
        finally
            if Assigned(spAgregarAltasWebServicePAK) then FreeAndNil(spAgregarAltasWebServicePAK);
        end;
        Result := Resultado;
    end;

    procedure TValidacionAraucoTAG.ValidarClienteAraucoTAG(IDConsultaPAK : Integer; NumeroDocumento: AnsiString; var pRespuesta : TRespuesta);
    var
        spRestricciones : TADOStoredProc;
        spCuentasRut : TADOStoredProc;
        tmpRespuesta : TRespuesta;
        Vehiculos : TPatentes;
        MensajesError : TMensajes;
        I, CtasPorCliente : Integer;
        PuedeAdherirPAK : Boolean;
    begin
        tmpRespuesta := TRespuesta.Create;
        // Obtengo los datos de la persona
        CargarDatosPersona(NumeroDocumento);
        // Asocio el ID a la respuesta
        tmpRespuesta.ConsultaPAK := IDConsultaPAK;
        // Si no encuentro al cliente aviso
        if DatosPersona.CodigoPersona <= 0 then begin
            tmpRespuesta.PuedeAdherir := False;
            tmpRespuesta.Persona.Rut := NumeroDocumento;
            tmpRespuesta.Persona.Nombre := '';
            tmpRespuesta.Mensajes := AgregarError(-1, rsClienteNoExiste);
        end else
        // Valido que sea persona natual, no puede adherir si es jur�dica.
        if DatosPersona.Personeria = PERSONERIA_JURIDICA then begin
            tmpRespuesta.PuedeAdherir := False;
            tmpRespuesta.Persona.Rut := DatosPersona.NumeroDocumento;
            tmpRespuesta.Persona.Nombre := DatosPersona.RazonSocial;
            tmpRespuesta.Mensajes := AgregarError(-1, rsClientePersonaJuridica);
        end else begin
            // Verifico que las patentes no superen el l�mite
            CtasPorCliente := QueryGetIntegerValue(oCnx.BaseCAC, Format(cSQLCtasActivasPorPersona,[DatosPersona.CodigoPersona]));
            if CtasPorCliente > MaximoCuentasPermitidas then begin
                tmpRespuesta.PuedeAdherir := False;
                tmpRespuesta.Mensajes := AgregarError(-1, Format(rsClienteDemasiadasPatentes, [MaximoCuentasPermitidas]));
                AuditarConsultaWebServicePAK(IDConsultaPAK, DatosPersona.CodigoPersona, NumeroDocumento, tmpRespuesta.Mensajes[0].Mensaje, False);
                pRespuesta := tmpRespuesta;
                Exit;
            end;

            tmpRespuesta.Persona.Rut := DatosPersona.NumeroDocumento;
            tmpRespuesta.Persona.Nombre := DatosPersona.Nombre + ' ' + DatosPersona.Apellido + ' ' + DatosPersona.ApellidoMaterno;
            if FAdheridoEnvioEMail then tmpRespuesta.Persona.Mail := FEMailPersona
            else tmpRespuesta.Persona.Mail := rsClienteNoHabilitadoMail;

            try
                // Consultar Restricciones
                spRestricciones := TADOStoredProc.Create(nil);
                with spRestricciones do begin

                    Connection := oCnx.BaseCAC;
                    ProcedureName := 'ObtenerRestriccionesAdhesionPAK';
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoPersona').Value          := DatosPersona.CodigoPersona;
                    Parameters.ParamByName('@IDUsuarioWebService').Value    := FIDUsuarioWebService;
                    Parameters.ParamByName('@IDConsultaPAK').Value          := IDConsultaPAK;
                    Open;

                    if IsEmpty then begin
                        tmpRespuesta.PuedeAdherir := False;
                        tmpRespuesta.Mensajes := AgregarError(-1, Format(rsErrorConsulta, ['No se encontraron registros']));
                    end else begin
                        if RecordCount > 0 then begin
                            PuedeAdherirPAK := Parameters.ParamByName('@PuedeAdherirsePA').Value;

                            tmpRespuesta.ConsultaPAK := Parameters.ParamByName('@IDConsultaPAK').Value;
                            if InformarDetalle then begin
                                // Se agrega el mensaje de error o restricciones de adhesi�n
                                I := 0;
                                SetLength(MensajesError, RecordCount);
                                while not Eof do begin
                                    MensajesError[I] := TMensaje.Create;
                                    MensajesError[I].CodigoMensaje := FieldByName('Codigo').AsInteger;
                                    MensajesError[I].Mensaje := FieldByName('Descripcion').AsString;
                                    Inc(I);
                                    Next;
                                end;
                                tmpRespuesta.Mensajes := MensajesError;
                            end else begin
                                // Se agrega el mensaje est�ndar de no Adhesi�n
                                if PuedeAdherirPAK then
                                    tmpRespuesta.Mensajes := AgregarError(0, rsClientePuedeAdeherir)
                                else
                                    tmpRespuesta.Mensajes := AgregarError(-1, rsRespuestaGeneral);
                            end;
                            tmpRespuesta.PuedeAdherir := PuedeAdherirPAK;
                        end;
                    end;
                    Close;
                end;
                FreeAndNil(spRestricciones);
            except
                on e: Exception do begin
                    tmpRespuesta.PuedeAdherir := False;
                    tmpRespuesta.Mensajes := AgregarError(-1, Format(rsErrorConsulta, [e.Message]));
                end;
            end;

            if tmpRespuesta.PuedeAdherir then begin
                try
                    spCuentasRut := TADOStoredProc.Create(nil);
                    with spCuentasRut do begin
                        Connection := oCnx.BaseCAC;
                        ProcedureName := 'ObtenerRestriccionesAdhesionPAKPorPatente';
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoPersona').Value  := DatosPersona.CodigoPersona;
                        Parameters.ParamByName('@Patente').Value        := null;
                        Parameters.ParamByName('@TraerBajas').Value     := False;
                        Parameters.ParamByName('@IDConsultaPAK').Value  := tmpRespuesta.ConsultaPAK;
                        Open;
                        if IsEmpty then begin
                            tmpRespuesta.PuedeAdherir := False;
                            tmpRespuesta.Mensajes  := AgregarError(-1, rsClienteSinCuentas);
                        end else begin
                            SetLength(Vehiculos, RecordCount);
                            I := 0;
                            PuedeAdherirPAK := False;
                            while not Eof do begin
                                Vehiculos[I] := TPatenteConsultar.Create;
                                Vehiculos[I].EstadoPatente := TMensaje.Create;
                                Vehiculos[I].Patente := FieldByName('Patente').AsString;

                                if FieldByName('Adherido').AsBoolean then begin
                                    Vehiculos[I].EstadoPatente.CodigoMensaje    := -1;
                                    Vehiculos[I].EstadoPatente.Mensaje          := rsPatenteYaAdherido;

                                end else if FieldByName('PuedeAdherir').AsBoolean then begin
                                    Vehiculos[I].EstadoPatente.CodigoMensaje        := CONST_CODIGO_MOTIVO_RESTRIC_PAK_PUEDE_ADHERIR;
                                    Vehiculos[I].EstadoPatente.Mensaje              := rsPatentePuedeAdeherir;
                                    PuedeAdherirPAK := True;

                                end else begin
                                    if InformarDetalle then begin
                                        // Informo el detalle por Patente

                                        if FieldByName('TagVencido').AsBoolean then begin
                                            Vehiculos[I].EstadoPatente.CodigoMensaje    := CONST_CODIGO_MOTIVO_RESTRIC_PAK_TAGS_VENCIDOS;
                                            Vehiculos[I].EstadoPatente.Mensaje          := rsTagVencido;

                                        end else if FieldByName('CuentaSuspendida').AsBoolean then begin
                                            Vehiculos[I].EstadoPatente.CodigoMensaje    := CONST_CODIGO_MOTIVO_RESTRIC_PAK_CUENTAS_SUSPENDIDAS;
                                            Vehiculos[I].EstadoPatente.Mensaje          := rsCuentaSuspendida;

                                        end else if FieldByName('TieneInfracciones').AsBoolean then begin
                                            Vehiculos[I].EstadoPatente.CodigoMensaje    := CONST_CODIGO_MOTIVO_RESTRIC_PAK_INFRACCIONES_PENDIENTES;
                                            Vehiculos[I].EstadoPatente.Mensaje          := rsInfraccionesPendiente;

                                        end else if FieldByName('LecturasSinTAG').AsBoolean then begin
                                            Vehiculos[I].EstadoPatente.CodigoMensaje    := CONST_CODIGO_MOTIVO_RESTRIC_PAK_TTOO_SIN_TAG;
                                            Vehiculos[I].EstadoPatente.Mensaje          := rsSinTag;

                                        end else if FieldByName('ListaAmarilla').AsBoolean then begin
                                            Vehiculos[I].EstadoPatente.CodigoMensaje    := CONST_CODIGO_MOTIVO_RESTRIC_PAK_LISTA_AMARILLA;
                                            Vehiculos[I].EstadoPatente.Mensaje          := rsListaAmarilla;

                                        end else if FieldByName('CuentaDeBaja').AsBoolean then begin
                                            Vehiculos[I].EstadoPatente.CodigoMensaje    := CONST_CODIGO_MOTIVO_RESTRIC_PAK_SIN_CUENTAS_ACTIVAS;
                                            Vehiculos[I].EstadoPatente.Mensaje          := rsCuentaBaja;

                                        end else if FieldByName('BajaForzada').AsBoolean then begin
                                            Vehiculos[I].EstadoPatente.CodigoMensaje    := CONST_CODIGO_MOTIVO_RESTRIC_PAK_BAJA_FORZADA;
                                            Vehiculos[I].EstadoPatente.Mensaje          := rsBajaForzada;
                                            // Un cliente que tenga una cuenta con una baja forzada no se le debe permitir la inscripci�n por t�tem
                                            PuedeAdherirPAK                             := False;
                                        end;
                                    end else begin
                                        // S�lo informo que no puede adherir
                                        Vehiculos[I].EstadoPatente.CodigoMensaje    := -1;
                                        Vehiculos[I].EstadoPatente.Mensaje          := rsPatenteNoPuedeAdeherir;
                                        // Un cliente que tenga una cuenta con una baja forzada no se le debe permitir la inscripci�n por t�tem
                                        if FieldByName('BajaForzada').AsBoolean then PuedeAdherirPAK := False;
                                    end;
                                end;
                                Inc(I);
                                Next;
                            end;
                            tmpRespuesta.Patentes := Vehiculos;
                            if not PuedeAdherirPAK then begin
                                tmpRespuesta.PuedeAdherir := PuedeAdherirPAK;
                                if InformarDetalle then
                                    tmpRespuesta.Mensajes := AgregarError(-1, rsClienteNoPuedeAdeherir)
                                else
                                    tmpRespuesta.Mensajes := AgregarError(-1, rsRespuestaGeneral);
                            end;

                        end;
                        Close;
                    end;
                    FreeAndNil(spCuentasRut);
                except
                    on e: Exception do begin
                        tmpRespuesta.PuedeAdherir := False;
                        tmpRespuesta.Mensajes  := AgregarError(-1, Format(rsErrorConsulta, [e.Message]));
                    end;
                end;
            end else begin
                SetLength(Vehiculos, 1);
                Vehiculos[I] := TPatenteConsultar.Create;
                Vehiculos[I].EstadoPatente := TMensaje.Create;
                tmpRespuesta.Patentes := Vehiculos;
            end;
        end;

        AuditarConsultaWebServicePAK(tmpRespuesta.ConsultaPAK, DatosPersona.CodigoPersona, NumeroDocumento, tmpRespuesta.Mensajes[0].Mensaje, tmpRespuesta.PuedeAdherir);

        pRespuesta := tmpRespuesta;
        
//        Finalize(FDatosPersonales);
    end;

    procedure TValidacionAraucoTAG.AdherirClienteAraucoTAG(IDConsultaPAK : Integer; NumeroDocumento, eMail : AnsiString; Patentes : TStringDynArray; out oRespuesta: TRespuesta);
    var
        tmpRespuesta,
        tmpRespuestaPrevia      : TRespuesta;
        MensajeError            : TMensajes;
        Vehiculos               : TPatentes;
        I, IndicePatente,
        CtasPorCliente,
        IDAltasWebServicePAK    : Integer;
        Patente                 : string;
        Pasar, HuboError        : Boolean;
    begin
        try
            // Inicializo la respuesta
            tmpRespuesta            := TRespuesta.Create;
            tmpRespuestaPrevia      := TRespuesta.Create;
            HuboError := False;
            // Asocio el ID a la respuesta
            tmpRespuesta.ConsultaPAK := IDConsultaPAK;
            try
                // Cargo los datos de la persona
                CargarDatosPersona(NumeroDocumento);
                // Si no encuentro al cliente aviso
                if DatosPersona.CodigoPersona <= 0 then begin
                    tmpRespuesta.PuedeAdherir := False;
                    tmpRespuesta.Persona.Rut := NumeroDocumento;
                    tmpRespuesta.Persona.Nombre := '';
                    tmpRespuesta.Mensajes := AgregarError(-1, rsClienteNoExiste);
                    HuboError := True;
                end else

                if DatosPersona.Personeria = PERSONERIA_JURIDICA then begin
                    HuboError := True;
                    tmpRespuesta.Persona.Rut := DatosPersona.NumeroDocumento;
                    tmpRespuesta.Persona.Nombre := DatosPersona.RazonSocial;
                    tmpRespuesta.Mensajes := AgregarError(-1, rsClientePersonaJuridica);
                end else

                if Length(Patentes) = 0 then begin
                    HuboError := True;
                    tmpRespuesta.Mensajes := AgregarError(-1, rsSinPatente);
                end else

                // Verifico que no haya enviado m�s patentes.
                if Length(Patentes) > MaximoCuentasPermitidas then begin
                    HuboError := True;
                    tmpRespuesta.Mensajes := AgregarError(-1, Format(rsDemasiadasPatentes, [MaximoCuentasPermitidas]));
                end else

                // Verifico que la Consulta haya sido enviada realizada
                if not ObtenerConsultaPrevia(IDConsultaPAK, tmpRespuestaPrevia) then begin
                    tmpRespuesta.Mensajes := tmpRespuestaPrevia.Mensajes;
                    HuboError := True;
                end else

                // Nos mandaron un rut distinto al consultado
                if Trim(NumeroDocumento) <> Trim(tmpRespuestaPrevia.Persona.Rut) then begin
                    HuboError := True;
                    tmpRespuesta.Mensajes := AgregarError(-1, rsRutNoPertenece);
                end else

                // Tiene que actualizar el eMail y no lo evi�
                if not FAdheridoEnvioEMail and (eMail = '') then begin
                    HuboError := True;
                    tmpRespuesta.Mensajes := AgregarError(-1, rsNoenvioMail);
                end else

                // Si se recibi� un mail significa que hay que actualizar el mail al cliente.
                if eMail <> '' then begin
                    if not IsValidEMailCN(eMail)then begin
                        HuboError := True;
                        tmpRespuesta.Mensajes := AgregarError(-1, rsMailInvalido);
                    // Si es v�lido, le actualizo el eMail al convenio.
                    end else if not (ActualizarMail(eMail, NumeroDocumento)) then begin
                        HuboError := True;
                        tmpRespuesta.Mensajes := AgregarError(-1, rsNoSePudoActualizarMail);
                    end;
                end;

                //  Armo el nombre del cliente
                tmpRespuesta.Persona.Rut := DatosPersona.NumeroDocumento;
                tmpRespuesta.Persona.Nombre := DatosPersona.Nombre + ' ' + DatosPersona.Apellido + ' ' + DatosPersona.ApellidoMaterno;               

                if not HuboError then begin
                    // No hubo errores, se graban las patentes a Adherir
                    HuboError := False;
                    I := 0;
                    for Patente in Patentes do begin
                        Pasar := False;
                        // Agergo una Patente
                        SetLength(Vehiculos, Length(Vehiculos) + 1);
                        Vehiculos[I] := TPatenteConsultar.Create;
                        Vehiculos[I].EstadoPatente := TMensaje.Create;
                        Vehiculos[I].Patente := Patente;
                        // Verifico que exista la patente en la consulta previa
                        IndicePatente := ObtenerIndicePatente(Patente, tmpRespuestaPrevia.Patentes);
                        if IndicePatente >= 0 then begin
                            if tmpRespuestaPrevia.Patentes[IndicePatente].EstadoPatente.CodigoMensaje = 0 then begin
                                IDAltasWebServicePAK := AgregarAltaAraucoTAG(IDConsultaPAK, Trim(Patente));
                                if (IDAltasWebServicePAK = -999) then begin
                                    Vehiculos[I].EstadoPatente.CodigoMensaje := -1;
                                    Vehiculos[I].EstadoPatente.Mensaje := rsPatenteYaAdherido;
                                    HuboError := True;
                                end else if (IDAltasWebServicePAK < 0) then begin
                                    Vehiculos[I].EstadoPatente.CodigoMensaje := -1;
                                    Vehiculos[I].EstadoPatente.Mensaje := rsErrorEnAlta;
                                    HuboError := True;
                                end else begin
                                    Vehiculos[I].EstadoPatente.CodigoMensaje := 0;
                                    Vehiculos[I].EstadoPatente.Mensaje := rsAltaOK;
                                end;
                            end else begin
                                Vehiculos[I].EstadoPatente.CodigoMensaje := tmpRespuestaPrevia.Patentes[IndicePatente].EstadoPatente.CodigoMensaje;
                                Vehiculos[I].EstadoPatente.Mensaje := tmpRespuestaPrevia.Patentes[IndicePatente].EstadoPatente.Mensaje;
                                HuboError := True;
                            end;
                            Pasar := True;
                        // No existe en la consulta (Es un Error), lo agergo
                        end else begin
                            IDAltasWebServicePAK := AgregarAltaAraucoTAG(IDConsultaPAK, Trim(Patente));
                            if (IDAltasWebServicePAK = -999) then begin
                                Vehiculos[I].EstadoPatente.CodigoMensaje := -1;
                                Vehiculos[I].EstadoPatente.Mensaje := rsPatenteYaAdherido;
                                HuboError := True;
                            end else if (IDAltasWebServicePAK < 0) then begin
                                Vehiculos[I].EstadoPatente.CodigoMensaje := -1;
                                Vehiculos[I].EstadoPatente.Mensaje := rsErrorEnAlta;
                                HuboError := True;
                            end else begin
                                Vehiculos[I].EstadoPatente.CodigoMensaje := 0;
                                Vehiculos[I].EstadoPatente.Mensaje := rsAltaOK;
                            end;
                            Pasar := True;
                        end;
                        if Pasar then begin
                            Inc(I);
                            Continue;
                        end;
                    end;
                    tmpRespuesta.Patentes := Vehiculos;

                    if HuboError then begin
                        tmpRespuesta.PuedeAdherir := False;
                        tmpRespuesta.Mensajes := AgregarError(-1, rsErrorEnAlta);
                    end else begin
                        tmpRespuesta.PuedeAdherir := True;
                        tmpRespuesta.Mensajes := AgregarError(0, rsClienteOK);
                    end;
                end;
            except
                on E : Exception do begin
                    tmpRespuesta.PuedeAdherir := False;
                    tmpRespuesta.Mensajes := AgregarError(-1, Format(rsErrorInesperado, [E.Message]));;
                end;
            end;
        finally
            AuditarConsultaWebServicePAK(tmpRespuesta.ConsultaPAK, DatosPersona.CodigoPersona, NumeroDocumento, tmpRespuesta.Mensajes[0].Mensaje, tmpRespuesta.PuedeAdherir);
            oRespuesta := tmpRespuesta;
            MensajeError := nil;
        end;
    end;
end.
