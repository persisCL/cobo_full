{-------------------------------------------------------------------------------
 File Name: FrmWebAuditoria.pas
 Author:    lgisuk
 Date Created: 25/10/2006
 Language: ES-AR
 Description:  M�dulo de la Web - Ver registros de auditoria

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------}
unit FrmWebAuditoria;

interface

uses
  //Log Auditoria
  DMConnection,                      //Coneccion a base de datos OP_CAC
  Util,                              //IIF
  UtilProc,                          //Mensajes
  Rstrings,                          //STR_ERROR
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, ExtCtrls, DPSControls, ListBoxEx,
  DBListEx, Validate, DateEdit, StrUtils, VariantComboBox;

type
  TFWebAuditoria = class(TForm)
    DataSource: TDataSource;
    PAbajo: TPanel;
    PDerecha: TPanel;
    Parriba: TPanel;
    PAderecha: TPanel;
    DBListEx: TDBListEx;
    EDesde: TDateEdit;
    EHasta: TDateEdit;
    LDesde: TLabel;
    LHasta: TLabel;
    SalirBTN: TButton;
    AbrirBTN: TButton;
    spObtenerLogWebAuditoria: TADOStoredProc;
    EBanco: TVariantComboBox;
    SPObtenerBancosWEBAuditoria: TADOStoredProc;
    lblBanco: TLabel;
    ETransaccion: TEdit;
    lbltransaccion: TLabel;
    procedure AbrirBTNClick(Sender: TObject);
    procedure SalirBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBListExColumns0HeaderClick(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    function Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
    { Public declarations }
  end;

var
  FWebAuditoria: TFWebAuditoria;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created:  25/10/2006
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFWebAuditoria.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

    Procedure CargarBancos;
    resourcestring
        SELECCIONAR = '(Seleccionar)';
    begin
        result:=false;
        EBanco.Clear;
        EBanco.value:= SELECCIONAR;
        EBanco.items.add(SELECCIONAR,0);
        //ejecuto la consulta
        try
            with SpObtenerBancosWebAuditoria do begin
                Close;
                Open;
                //cargo el combo
                while not eof do begin
                    EBanco.Items.Add(trim(fieldbyname('Descripcion').asstring),trim(fieldbyname('CodigoBanco').asstring));
                    next;
                end;
                close;
            end;
            //me paro en el primero
            EBanco.ItemIndex:= 0;
        except
            on e: Exception do begin
                //el stored procedure podria fallar
                MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
Var
  	S: TSize;
begin
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    //Manejo el MdiChild
    if MDIChild then begin
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;
  	CenterForm(Self);
  	Caption := AnsiReplaceStr(txtCaption, '&', '');
    //Fechas
    edesde.Date:=now;
    ehasta.Date:=now;
    //Cargo los bancos
    CargarBancos;
    //Numero
    ETransaccion.Text := '';
    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AbrirBTNClick
  Author:    lgisuk
  Date Created:  25/10/2006
  Description: Abro la consulta
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFWebAuditoria.AbrirBTNClick(Sender: TObject);
Resourcestring
    STR_FECHA_DESDE = 'Debe Indicar Fecha Desde';
    STR_FECHA_HASTA = 'Debe Indicar Fecha Hasta';
    STR_FECHA_MAYOR = 'La Fecha Hasta debe ser mayor a la Fecha Desde';
begin
    //Obligo a cargar fecha desde
    if edesde.Date = NULLDATE then begin
        MsgBoxBalloon(STR_FECHA_DESDE, STR_ERROR, MB_ICONSTOP, edesde);
        edesde.SetFocus;
        Exit;
    end;

    //Obligo a cargar fecha hasta
    if ehasta.Date = NULLDATE then begin
        MsgBoxBalloon(STR_FECHA_HASTA, STR_ERROR, MB_ICONSTOP, ehasta);
        ehasta.SetFocus;
        exit;
    end;

    //valido que la fecha hasta sea mayor a la fecha desde
    if edesde.date > ehasta.Date then begin
        MsgBoxBalloon(STR_FECHA_MAYOR, STR_ERROR, MB_ICONSTOP, ehasta);
        ehasta.SetFocus;
        exit;
    end;

    //Abro la consulta
    with SPObtenerLogWebAuditoria.Parameters do begin
        Refresh;
        ParamByName('@DesdeFecha').Value:= Edesde.date;
        ParamByName('@HastaFecha').Value:= Ehasta.date;
        ParamByName('@CodigoBanco').Value:= EBanco.Value;
        ParamByName('@IDTRX').Value:= IIF(TRIM(ETransaccion.Text) <> '', ETransaccion.Text, NULL);
    end;
    try
        SPObtenerLogWebAuditoria.Close;
        SPObtenerLogWebAuditoria.Open;
    except
        on e: Exception do begin
            MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR); //el stored procedure podria fallar
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListExColumns0HeaderClick
  Author:    lgisuk
  Date Created:  25/10/2006
  Description: permito ordenar haciendo click en la columna
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFWebAuditoria.DBListExColumns0HeaderClick(Sender: TObject);
begin
	if SPObtenerLogWebAuditoria.IsEmpty then Exit;
	if TDBListExColumn(sender).Sorting = csAscending then begin
      TDBListExColumn(sender).Sorting := csDescending;
  end else begin
      TDBListExColumn(sender).Sorting := csAscending;
  end;
	SPObtenerLogWebAuditoria.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created:  25/10/2006
  Description: Cierro el Formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFWebAuditoria.SalirBTNClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 25/10/2006
  Description: Lo libero de Memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFWebAuditoria.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
