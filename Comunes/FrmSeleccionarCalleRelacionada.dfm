object FormSeleccionarCalleRelacionada: TFormSeleccionarCalleRelacionada
  Left = 240
  Top = 234
  BorderStyle = bsDialog
  Caption = 'Seleccionar Calle Relacionada'
  ClientHeight = 120
  ClientWidth = 498
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 10
    Top = 68
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 10
    Top = 16
    Width = 37
    Height = 13
    Caption = 'Regi'#243'n:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label15: TLabel
    Left = 218
    Top = 16
    Width = 42
    Height = 13
    Caption = 'Comuna:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 10
    Top = 41
    Width = 26
    Height = 13
    Caption = 'Calle:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btn_buscaCalle: TSpeedButton
    Left = 466
    Top = 37
    Width = 22
    Height = 20
    Caption = '...'
    OnClick = btn_buscaCalleClick
  end
  object btnAceptar: TDPSButton
    Left = 334
    Top = 91
    Caption = '&Aceptar'
    Default = True
    TabOrder = 4
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 414
    Top = 91
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 5
    OnClick = btnCancelarClick
  end
  object cb_Regiones: TComboBox
    Left = 75
    Top = 12
    Width = 134
    Height = 21
    Hint = 'Regi'#243'n'
    Style = csDropDownList
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnChange = cb_RegionesChange
  end
  object cb_Comunas: TComboBox
    Left = 266
    Top = 12
    Width = 222
    Height = 21
    Hint = 'Comuna'
    Style = csDropDownList
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnChange = cb_ComunasChange
  end
  object cb_Calle: TComboBox
    Left = 75
    Top = 37
    Width = 388
    Height = 21
    Hint = 'Calle'
    CharCase = ecUpperCase
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
  end
  object txt_numeroCalle: TEdit
    Left = 75
    Top = 62
    Width = 110
    Height = 21
    Hint = 'N'#250'mero'
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 10
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
  end
end
