object FormSeleccionarTipoCliente: TFormSeleccionarTipoCliente
  Left = 324
  Top = 252
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Seleccionar Tipo de Cliente'
  ClientHeight = 425
  ClientWidth = 393
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Convenio: TGroupBox
    Left = 0
    Top = 0
    Width = 393
    Height = 386
    Align = alClient
    Caption = 'Seleccione Tipo de Cliente a definir'
    TabOrder = 0
    DesignSize = (
      393
      386)
    object sb_Convenio: TScrollBox
      Left = 11
      Top = 22
      Width = 370
      Height = 354
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      object rgTiposCliente: TRadioGroup
        Left = 0
        Top = 0
        Width = 366
        Height = 350
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 386
    Width = 393
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      393
      39)
    object btn_Cancelar: TButton
      Left = 260
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Cancelar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object btn_ok: TButton
      Left = 179
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Documentaci'#243'n presentada en forma correcta'
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btn_okClick
    end
  end
  object spTiposDeClientesDeCobranza_Listar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposDeCliente'
    Parameters = <>
    Left = 88
    Top = 128
  end
end
