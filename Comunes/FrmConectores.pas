unit FrmConectores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  ExtCtrls, DmConnection, utildb, utilproc,
  util, DmiCtrls, BuscaClientes, DB, ADODB, MPlayer, ComCtrls, jpeg, ImgList, MMSystem, PeaProcs,
  MaskCombo, peatypes, DPSControls, CacProcs, FreAltaOrdenServicio;//, freAltaOrdenServicio;

const
    EXTENDED_HEIGHT_NB_CONECTORES = 260 + 372;
	HORIZONTAL_TRACK	= 82;
    MIN_TRACKLEFT		= 479;
    MIN_VOLUMEN         = 145;
    RANGO_VOLUMEN       = -55;
    TRACK_LENGHT		= 200;
	MAX_TRACKRIGHT		= TRACK_LENGHT + MIN_TRACKLEFT;


type
  TFormConectores = class(TForm)
    nbConectores: TNotebook;
    mmBody: TMemo;
    pnlCliente: TPanel;
    Label1: TLabel;
    lbl_Cliente: TLabel;
    qryOrdenesServicioMail: TADOQuery;
    mpAudio: TMediaPlayer;
    tmrAudio: TTimer;
    lblDuracion: TLabel;
    sbFax: TScrollBox;
    imgFax: TImage;
    btnPause: TShapeButton;
    btnStep: TShapeButton;
    btnStop: TShapeButton;
    btnBack: TShapeButton;
    btnPlay: TShapeButton;
    NormalImages: TImageList;
    HotImages: TImageList;
    lblTotal: TLabel;
    Image1: TImage;
    ImagenesChicas: TImageList;
    HotImagenesChicas: TImageList;
    btnTrack: TImage;
    imgPlay: TImage;
    Image4: TImage;
    Image5: TImage;
    ImageDeshabilitadasChicas: TImageList;
    imgStop: TImage;
    btnVolumen: TButton;
    Label5: TLabel;
    Label6: TLabel;
    cbVerArbolOrdenesServicio: TCheckBox;
    mcTipoNumeroDocumento: TMaskCombo;
    btnBuscar: TDPSButton;
    btnGuardar: TDPSButton;
    btnCancelar: TDPSButton;
    FrameAltaOrdenServicio: TFrameAltaOrdenServicio;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lblSubject: TLabel;
    lblSender: TLabel;
    lblFechaHora: TLabel;
    btnConsultar: TDPSButton;
    procedure btnGuardarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnPlayClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure tmrAudioTimer(Sender: TObject);
    procedure ShapeButton1Click(Sender: TObject);
    procedure btnTrackMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnTrackMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ActualizarBotones(Sender: TObject);
    procedure btnVolumenMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnVolumenMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnBuscarClick(Sender: TObject);
    procedure cbVerArbolOrdenesServicioClick(Sender: TObject);
    procedure imgFaxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure imgFaxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgFaxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FrameAltaOrdenServiciotvOrdenesServicioClick(
      Sender: TObject);
    procedure FrameAltaOrdenServiciotvOrdenesServicioDeletion(
      Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
  private
    { Private declarations }
    FCargarClienteBuscar: Boolean;
    FCodigoComunicacion, FOrdenServicio: integer;
    FTipoOrdenServicio: integer;
    FCodigoCliente: integer;
    FPixelsPorSegundo : integer;
    FMouseY, FMouseX: integer;
    Fseconds: integer;
    FTipoContacto: AnsiString;
    FNombre, FApellido, FApellidoMaterno: AnsiString;
    FStandardHeight: Integer;

  	function CargarCliente(CodigoCliente: Integer; TipoDocumento, NumeroDocumento: AnsiString; TipoContacto: AnsiString): Boolean;
  public
    { Public declarations }
    function inicializa(OrdenServicio: integer): boolean;
  end;

var
  FormConectores: TFormConectores;

implementation

{$R *.dfm}

procedure TFormConectores.btnGuardarClick(Sender: TObject);
begin
	FrameAltaOrdenServicio.Guardar;
    if FrameAltaOrdenServicio.Resultado = mrOk then begin
        Case FrameAltaOrdenServicio.MedioContacto of
            MC_EMAIL:
            		QueryExecute(DMConnections.BaseCAC,
            	      'UPDATE OrdenesServicioMail SET CodigoOrdenServicioAsociada = ' +
                      IntToStr(FrameAltaOrdenServicio.OrdenServicio) +
            		  ' WHERE CodigoOrdenServicio = ' + IntToStr(FOrdenServicio));
            MC_FAX:
            		QueryExecute(DMConnections.BaseCAC,
            	      'UPDATE OrdenesServicioFax SET CodigoOrdenServicioAsociada = ' +
                      IntToStr(FrameAltaOrdenServicio.OrdenServicio) +
            		  ' WHERE CodigoOrdenServicio = ' + IntToStr(FOrdenServicio));
            MC_VOICE_MAIL:
            		QueryExecute(DMConnections.BaseCAC,
            	      'UPDATE OrdenesServicioAudio SET CodigoOrdenServicioAsociada = ' +
                      IntToStr(FrameAltaOrdenServicio.OrdenServicio) +
            		  ' WHERE CodigoOrdenServicio = ' + IntToStr(FOrdenServicio));
        end;
    end;
    ModalResult :=  FrameAltaOrdenServicio.Resultado;
end;

procedure TFormConectores.btnCancelarClick(Sender: TObject);
begin
    ModalResult :=  FrameAltaOrdenServicio.Resultado;
    Close;
end;

function TFormConectores.inicializa(OrdenServicio: integer): boolean;
resourcestring
    // Mensajes de verificaci�n
    CAPTION_VALIDAR_OS      = 'Validar datos de la Orden Servicio';
    MSG_TIPO_ORDEN_SERVICIO = 'El tipo de Orden de Servicio no puede ser resuelto mediante esta pantalla.';
    // Captions del formulario
    CAPTION_RECLAMO_POR_MAIL = 'Atender comunicaci�n por Mail recibido';
    CAPTION_RECLAMO_POR_AUDIO = 'Atender comunicaci�n por mensaje de Audio';
    CAPTION_RECLAMO_POR_FAX = 'Atender comunicaci�n por Fax recibido';
    // Mensajes de error
    MSG_ERROR_CARGAR_AUDIO   = 'No se encontr� el mensaje de Audio.';
    CAPTION_CARGAR_AUDIO     = 'Cargar Audio';
    MSG_ERROR_CARGAR_FAX     = 'No se encontr� el Fax.';
    CAPTION_CARGAR_FAX       = 'Cargar Fax.';
var
    NumDevs: integer;
	volumen : DWord;
	waveCaps: TWaveOutCaps;
    AudioFileName, FaxFileName: AnsiString;

begin
	Result			:= False;
    FCargarClienteBuscar := True;
    lbl_Cliente.Caption := '';
   	CargarTiposDocumento(DMConnections.BaseCAC, FrameAltaOrdenServicio.cbTipoNumeroDocumento);
   	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento);

	FOrdenServicio	:= OrdenServicio;
    cbVerArbolOrdenesServicio.Checked := True;
    FrameAltaOrdenServicio.Enabled := False;
    FTipoContacto := TIPO_CLIENTE;

	//Obtengo los Datos que necesito
	FCodigoComunicacion := QueryGetValueInt(DMConnections.BaseCAC,
    	'SELECT CodigoComunicacion FROM OrdenesServicio WHERE CodigoOrdenServicio = ' + IntToStr(OrdenServicio));

	FTipoOrdenServicio := QueryGetValueInt(DMConnections.BaseCAC,
    	'SELECT CodigoWorkflow FROM OrdenesServicio WHERE CodigoOrdenServicio = ' + IntToStr(OrdenServicio));

	if (FTipoOrdenServicio <> OS_ATENDER_MAIL) and
    	(FTipoOrdenServicio <> OS_ATENDER_AUDIO) and
        (FTipoOrdenServicio <> OS_ATENDER_FAX) then begin
		MsgBox(MSG_TIPO_ORDEN_SERVICIO, CAPTION_VALIDAR_OS, MB_ICONSTOP);
    	Exit;
    end;

	case FTipoOrdenServicio of
	    OS_ATENDER_MAIL : begin
                // Actualizo Captions y notebook
                FrameAltaOrdenServicio.MedioContacto := MC_EMAIL;
                Caption						:= CAPTION_RECLAMO_POR_MAIL;
        		nbConectores.PageIndex 		:= 0;
                nbConectores.ActivePage 	:= 'mail';

                // Cargo los datos del mail y busco el cliente
                qryOrdenesServicioMail.Parameters.ParamByName('CodigoOrdenServicio').Value := OrdenServicio;
        		if not OpenTables([qryOrdenesServicioMail]) then exit;
                lblFechaHora.Caption	:= FormatDateTime('dd/mm/yyyy, hh:mm', qryOrdenesServicioMail.FieldByName('FechaHora').AsDateTime);
                lblSender.Caption		:= Trim(qryOrdenesServicioMail.FieldByName('Sender').AsString);
                lblSubject.Caption		:= Trim(qryOrdenesServicioMail.FieldByName('Subject').AsString);
                mmBody.Lines.Assign(qryOrdenesServicioMail.FieldByName('Body'));
				qryOrdenesServicioMail.Close;

                cbVerArbolOrdenesServicio.Checked := False;
                cbVerArbolOrdenesServicioClick(nil);
                cbVerArbolOrdenesServicio.Enabled := False;
                // nbConectores.Height := 500;

                // Si es Cliente cargamos al cliente, si no es cliente intentamos cargar un contacto
(*
                FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, format('select top 1 ISNULL(CodigoCliente, 0) From ' +
                  ' Clientes where email = ''%s''',[lblSender.Caption]));
                if FCodigoCliente = 0 then begin
                    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, format('select top 1 ISNULL(CodigoContacto, 0) From ' +
                      ' Contactos where email = ''%s''',[lblSender.Caption]));
                    FTipoContacto := TIPO_CONTACTO;
                end
                else begin
                    FTipoContacto := TIPO_CLIENTE;
                end;
                if FCodigoCLiente <> 0 then
                    cbVerArbolOrdenesServicio.Checked := CargarCliente(FCodigoCliente, '', '', FTipoContacto)
                else begin
                    cbVerArbolOrdenesServicio.Checked := False;
                    cbVerArbolOrdenesServicioClick(nil);
                    cbVerArbolOrdenesServicio.Enabled := False;
                end;
*)
        	end;
	    OS_ATENDER_AUDIO : begin
        		//REVISO EL TAMA�O DE LA VENTANA Y LOS CAPTIONS
                FrameAltaOrdenServicio.MedioContacto := MC_VOICE_MAIL;
           //     cbVerArbolOrdenesServicio.Checked := False;
           //     cbVerArbolOrdenesServicioClick(nil);
                cbVerArbolOrdenesServicio.Enabled := False;

                Caption						:= CAPTION_RECLAMO_POR_AUDIO;
				BorderStyle					:= bsDialog;
                nbConectores.Color			:= $00F8F4EF;
        		nbConectores.PageIndex		:= 1;
                nbConectores.ActivePage 	:= 'audio';

                AudioFileName := Trim(QueryGetValue(DmConnections.BaseCAC,
                  Format('SELECT NombreCompleto FROM OrdenesServicioAudio WHERE CodigoOrdenServicio = %d', [OrdenServicio])));

                if FileExists(AudioFileName) then
                	mpAudio.FileName := AudioFileName
                else begin
                	MsgBox(MSG_ERROR_CARGAR_AUDIO, CAPTION_CARGAR_AUDIO, MB_ICONSTOP);
                	mpAudio.FileName := '';
                    Exit;
				end;
				mpAudio.TimeFormat	:= tfMilliseconds;
                mpAudio.Open;

                FMouseX	:= -1;
                FMouseY := -1;
                NumDevs := waveOutGetNumDevs;
                waveOutGetDevCaps(NumDevs, @waveCaps, SizeOf(waveCaps));

                // Verificamos si el device soporta control de volumen
                if waveCaps.dwSupport AND WAVECAPS_VOLUME <> 0 then begin
                    waveOutGetVolume(0, @Volumen);
               		btnVolumen.Top := trunc((volumen / $FFFFFFFF) * RANGO_VOLUMEN) + MIN_VOLUMEN;
                end;
               	tmrAudio.Enabled	:= True;
                FSeconds := mpAudio.Length div 1000;

                FPixelsPorSegundo	:= (MAX_TRACKRIGHT - MIN_TRACKLEFT) div Fseconds;
                lblTotal.Caption	:= Copy(FormatDateTime('hh:mm:ss', EncodeTime(0, FSeconds div 60, Fseconds mod 60, 0)), 4, 5);
                btnPlay.OnClick(btnPlay);
        	end;
		OS_ATENDER_FAX : begin
        		//REVISO EL TAMA�O DE LA VENTANA Y LOS CAPTIONS
                FrameAltaOrdenServicio.MedioContacto := MC_FAX;
                cbVerArbolOrdenesServicio.Checked := False;
                cbVerArbolOrdenesServicioClick(nil);
                cbVerArbolOrdenesServicio.Enabled := False;
                Caption						:= CAPTION_RECLAMO_POR_FAX;
        		nbConectores.PageIndex		:= 2;
                nbConectores.ActivePage 	:= 'Fax';
                FaxFileName := Trim(QueryGetValue(DmConnections.BaseCAC,
                  Format('SELECT NombreCompleto FROM OrdenesServicioFax WHERE CodigoOrdenServicio = %d', [OrdenServicio])));
                if FileExists(FaxFileName) then begin
                	imgFax.Picture.LoadFromFile(FaxFileName);
                    try
                        imgFax.Height := Round(imgFax.Width * imgFax.Picture.Height / imgFax.Picture.Width);
                    except
                        imgFax.Height := 0;
                    end;
                end else begin
                	MsgBox(MSG_ERROR_CARGAR_FAX, CAPTION_CARGAR_FAX, MB_ICONSTOP);
                	imgFax.Picture.Assign(nil);
                    Exit;
				end;

        	end;
	end;
	Result := True;
end;


function TFormConectores.CargarCliente(CodigoCliente: Integer; TipoDocumento, NumeroDocumento: AnsiString;
  TipoContacto: AnsiString): Boolean;
resourcestring
    CAPTION_OBTENER_CONTACTO = 'Obtener Contacto';
    MSG_OBTENER_CONTACTO     = 'No se pudo obtener el Contacto.';
begin
    result := False;
    try
        FCodigoCLiente      := 0;
        FApellido			:= '';
        FApellidoMaterno    := '';
        FNombre				:= '';
        FrameAltaOrdenServicio.Inicializa(FCodigoComunicacion, CodigoCliente, TipoDocumento,
          NumeroDocumento);
        FCodigoCLiente      := FrameAltaOrdenServicio.CodigoCliente;
        FApellido           := FrameAltaOrdenServicio.txt_apellido.text;
        FApellidoMaterno    := FrameAltaOrdenServicio.txt_apellidoMaterno.text;
        FNombre             := FrameAltaOrdenServicio.txt_Nombre.text;
        if FApellido <> '' then begin
            lbl_Cliente.Caption	 := ArmaNombreCompleto(FApellido, FApellidoMaterno, FNombre);
            if FCargarClienteBuscar then begin
                mcTipoNumeroDocumento.MaskText := FrameAltaOrdenServicio.cbTipoNumeroDocumento.MaskText;
                mcTipoNumeroDocumento.ItemIndex := FrameAltaOrdenServicio.cbTipoNumeroDocumento.ItemIndex;
            end;

            if (FTipoOrdenServicio <> OS_ATENDER_AUDIO) then begin
                cbVerArbolOrdenesServicio.Checked := true;
                cbVerArbolOrdenesServicioClick(nil);
                cbVerArbolOrdenesServicio.Enabled := true;
            end else begin
                cbVerArbolOrdenesServicio.Checked := true;
                cbVerArbolOrdenesServicioClick(nil);
                cbVerArbolOrdenesServicio.Enabled := False;
            end;
            FrameAltaOrdenServicio.Enabled := true;
            result := true;
        end else begin
            lbl_Cliente.Caption	 := '';
(*            if (ActiveControl <> cb_TiposDocumento) and (ActiveControl <> txt_NumeroDocumento) then begin
                cb_TiposDocumento.ItemIndex := -1;
                txt_numeroDocumento.value := 0;
            end;*)

            if FCargarClienteBuscar then begin
                mcTipoNumeroDocumento.ItemIndex	:= -1;
                mcTipoNumeroDocumento.MaskText	:= '';
            end;

            if (FTipoOrdenServicio <> OS_ATENDER_AUDIO) then begin
                cbVerArbolOrdenesServicio.Checked := false;
                cbVerArbolOrdenesServicioClick(nil);
                cbVerArbolOrdenesServicio.Enabled := false;
            end else begin
                cbVerArbolOrdenesServicio.Checked := true;
                cbVerArbolOrdenesServicioClick(nil);
                cbVerArbolOrdenesServicio.Enabled := False;
            end;
            FrameAltaOrdenServicio.Enabled := False;
            result := False;
        end;
    except
        On E: exception do begin
            MsgBoxErr(MSG_OBTENER_CONTACTO, e.Message, CAPTION_OBTENER_CONTACTO, MB_ICONSTOP);
            Exit;
        end;
    end;
end;


procedure TFormConectores.btnPlayClick(Sender: TObject);
begin
	if mpAudio.Mode = mpPaused  then mpAudio.Resume
	else if mpAudio.Mode = mpStopped then mpAudio.Play
	else Exit;
	ActualizarBotones(Sender);
end;

procedure TFormConectores.btnStopClick(Sender: TObject);
begin
	mpAudio.Stop;
	mpaudio.Position	:= 0;
   	btnTrack.Left		:= MIN_TRACKLEFT;
	lblDuracion.Caption	:= '00:00';
    ActualizarBotones(Sender);
end;

procedure TFormConectores.tmrAudioTimer(Sender: TObject);
var
	X: integer;
begin
    tmrAudio.enabled := False;

    try
        if FMouseX = -1 then begin
            if (mpAudio.Mode = mpPlaying) then
                btnTrack.Left := MIN_TRACKLEFT + Round(TRACK_LENGHT / mpAudio.Length * mpAudio.Position)
        end  else begin
            x := btnTrack.Parent.ScreenToClient(Mouse.CursorPos).X - FMouseX;
            if x < MIN_TRACKLEFT then	x := MIN_TRACKLEFT
            else if x > MAX_TRACKRIGHT then x := MAX_TRACKRIGHT;
            btnTrack.Left := X;
        end;

        if FMouseY <> -1 then begin
            x := btnVolumen.parent.ScreenToClient(Mouse.CursorPos).Y - FMouseY;
            if (x >= MIN_VOLUMEN) then x := MIN_VOLUMEN
            else if (x <= MIN_VOLUMEN + RANGO_VOLUMEN) then x := MIN_VOLUMEN + RANGO_VOLUMEN;
            btnVolumen.Top := x;
        end;

	    if mpAudio.Mode = mpPlaying then
            try
                lblDuracion.Caption	:= Copy(FormatDateTime('hh:mm:ss', EncodeTime(0, (Round(mpAudio.Position / 1000) div 60),
                  (Round(mpAudio.Position / 1000) mod 3600) mod 60, 0)), 4, 5);
            except
            end;
    finally
        tmrAudio.enabled := true;
    end;
end;


procedure TFormConectores.ShapeButton1Click(Sender: TObject);
begin
	mpAudio.Pause;
	ActualizarBotones(Sender);
end;

procedure TFormConectores.btnTrackMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	FMouseX		:= X;
end;

procedure TFormConectores.btnTrackMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
	AntMode : TMPModes;
begin
	AntMode := mpAudio.Mode;
	mpAudio.Position := Round(((btnTrack.Left - MIN_TRACKLEFT) / TRACK_LENGHT) * mpAudio.Length);
    if AntMode = mpPlaying then mpAudio.Play;
    FMouseX := -1;
end;

procedure TFormConectores.ActualizarBotones(Sender: TObject);
begin
	if Sender = btnPlay then begin
		imgPlay.Top 		:= 71;
        btnPlay.ImageIndex	:= 1;

        btnPause.images		:= ImagenesChicas;

        btnStop.Images		:= ImagenesChicas;
        imgStop.Left		:= 48;
    end else if Sender = btnPause then begin

        btnPlay.ImageIndex	:= 0;
        imgPlay.Top 		:= 7;

        btnPause.images		:= ImageDeshabilitadasChicas;
        btnStop.Images		:= ImagenesChicas;
        imgStop.Left		:= 48;

    end else if Sender = btnStop then begin
        imgStop.Left		:= 108;
        btnStop.Images		:= ImageDeshabilitadasChicas;

        btnPlay.ImageIndex	:= 0;

        imgPlay.Top 		:= 7;
		btnPause.images		:= ImageDeshabilitadasChicas;
    end;
end;

procedure TFormConectores.btnVolumenMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    FMouseY := Y;
end;

procedure TFormConectores.btnVolumenMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    waveOutSetVolume(0, trunc(((btnVolumen.top - MIN_VOLUMEN + btnVolumen.parent.top) / RANGO_VOLUMEN) * $FFFFFFFF));
    FMouseY := -1;
end;

procedure TFormConectores.btnBuscarClick(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes , f);
(*
	if f.Inicializa(mcTipoNumeroDocumento.MaskText, Trim(StrRight(mcTipoNumeroDocumento.ComboText, 20)),
	  FApellido, FApellidoMaterno, FNombre, TBP_PERSONAS) then begin
		if (f.ShowModal = mrok) then begin
			mcTipoNumeroDocumento.MaskText := f.Persona.NumeroDocumento;
			CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, f.Persona.TipoDocumento);
//            FTipoContacto           := iif(f.EsCliente, TIPO_CLIENTE, TIPO_CONTACTO);
            FCargarClienteBuscar    := True;
			CargarCliente( f.Persona.CodigoPersona, trim(StrRight( mcTipoNumeroDocumento.ComboText, 20)), mcTipoNumeroDocumento.MaskText,
               FTipoContacto);
            FCargarClienteBuscar := False;
		end;
	end;
*)
	f.Release;
end;

procedure TFormConectores.cbVerArbolOrdenesServicioClick(Sender: TObject);
begin
    FrameAltaOrdenServicio.Visible := cbVerArbolOrdenesServicio.Checked;
    if cbVerArbolOrdenesServicio.Checked then
        nbConectores.Height := FStandardHeight
    else
        nbConectores.Height := EXTENDED_HEIGHT_NB_CONECTORES;
    btnGuardar.Enabled := cbVerArbolOrdenesServicio.Checked;
end;

procedure TFormConectores.imgFaxMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var dif: Integer;
begin
    if (ssLeft in Shift) then begin
        dif := FMouseY - imgFax.ClientToScreen(Point(X, Y)).Y;
        FMouseY := imgFax.ClientToScreen(Point(X, Y)).Y;
        sbFax.VertScrollBar.Position := sbFax.VertScrollBar.Position + dif;
    end;
end;

procedure TFormConectores.imgFaxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    FMouseY := imgFax.ClientToScreen(Point(X, Y)).Y;
    screen.cursor := crHandPoint;
end;

procedure TFormConectores.imgFaxMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    screen.cursor := crDefault;
end;

procedure TFormConectores.FrameAltaOrdenServiciotvOrdenesServicioClick(
  Sender: TObject);
begin
    FrameAltaOrdenServicio.tvOrdenesServicioClick(Sender);
end;

procedure TFormConectores.FrameAltaOrdenServiciotvOrdenesServicioDeletion(
  Sender: TObject; Node: TTreeNode);
begin
    FrameAltaOrdenServicio.tvOrdenesServicioDeletion(Sender, Node);
end;


procedure TFormConectores.FormCreate(Sender: TObject);
begin
    FStandardHeight := nbConectores.height;
end;

procedure TFormConectores.btnConsultarClick(Sender: TObject);
begin
    if not CargarCliente(0, trim(StrRight( mcTipoNumeroDocumento.ComboText, 20)), mcTipoNumeroDocumento.MaskText, TIPO_CLIENTE) then
        CargarCliente(0, trim(StrRight( mcTipoNumeroDocumento.ComboText, 20)), mcTipoNumeroDocumento.MaskText, TIPO_CONTACTO);

end;

end.
