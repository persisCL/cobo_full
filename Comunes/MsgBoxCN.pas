{
    File Name   : MsgBoxCN
    Author      : pdominguez
    Date Created: 14/06/2010
    Language    : ES-AR
    Description : Formulario de Mensajes para Proyecto CN.
}
unit MsgBoxCN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DmiCtrls, ComCtrls;

type
  TfrmMsgBoxCN = class(TForm)
    imgMsgBoxCN: TImage;
    lblTitulo: TLabel;
    Panel1: TPanel;
    meMensajeAMostrar: TMemo;
    chBxInfoExtendida: TCheckBox;
    btnAceptar: TButton;
    btnCancelar: TButton;
    procedure FormCreate(Sender: TObject);
    procedure chBxInfoExtendidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FAplicacionEjecutable,
    FAplicacionNombre,
    FVersion,
    FUsuarioSistema,
    FPropietario,
    FEquipoNombre,
    FEquipoIP,
    FMensaje: String;
    FEnviarEMail: Boolean;
    FAlturaForm,
    FAlturaMemo,
    FEstiloForm: Integer;
  public
    { Public declarations }
    Procedure AjustaAlturaMsgBoxCN;
    property AplicacionEjecutable: string read FAplicacionEjecutable write FAplicacionEjecutable;
    property AplicacionNombre: string read FAplicacionNombre write FAplicacionNombre;
    property Version: string read FVersion write FVersion;
    property Usuario: string read FUsuarioSistema write FUsuarioSistema;
    property PropietarioMsgBox: string read FPropietario write FPropietario;
    property EquipoNombre: string read FEquipoNombre write FEquipoNombre;
    property EquipoIP: string read FEquipoIP write FEquipoIP;
    property EnviarEMail: boolean read FEnviarEMail write FEnviarEMail;
    Property MensajeAMostrar: string read FMensaje write FMensaje;
    property EstiloForm: Integer read FEstiloForm write FEstiloForm;
  end;

var
  frmMsgBoxCN: TfrmMsgBoxCN;

implementation

{$R *.dfm}

Uses
    PeaProcsCN, Util;

procedure TfrmMsgBoxCN.chBxInfoExtendidaClick(Sender: TObject);
begin
    case chBxInfoExtendida.Checked of
        True: begin
            with meMensajeAMostrar.Lines do begin
                Add(' ');
                Add('M�dulo:' + AplicacionNombre);
                Add('Ejecutable: ' + AplicacionEjecutable);
                Add('Versi�n: ' + Version);
                Add(' ');
                Add('Owner: ' + PropietarioMsgBox);
                Add(' ');
                Add('Usuario: ' + Usuario);
                Add('Host Name: ' + EquipoNombre);
                Add('Host IP: ' + EquipoIP);
            end;
            AjustaAlturaMsgBoxCN;
        end;
        False: begin
            meMensajeAMostrar.Lines.Text := MensajeAMostrar;
            AjustaAlturaMsgBoxCN;
        end;
    end;
end;

procedure TfrmMsgBoxCN.FormCreate(Sender: TObject);
begin
    AplicacionNombre     := GetAppName;
    AplicacionEjecutable := Application.ExeName;
    Version              := GetFileVersionString(AplicacionEjecutable, True);
    Usuario              := UsuarioSistema;
    PropietarioMsgBox    := Owner.Name;
    EquipoNombre         := GetMachineName;
    EquipoIP             := ResolveHostNameStr(EquipoNombre);
    EnviarEMail          := False;
    FAlturaForm          := Height;
    FAlturaMemo          := meMensajeAMostrar.Height;
    FMensaje             := '';
end;

procedure TfrmMsgBoxCN.FormShow(Sender: TObject);
begin
    MessageBeep(EstiloForm);
end;

procedure TfrmMsgBoxCN.AjustaAlturaMsgBoxCN;
begin
    Height := FAlturaForm + ((meMensajeAMostrar.Lines.Count - 1) * FAlturaMemo);
end;

end.
