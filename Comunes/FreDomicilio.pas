unit FreDomicilio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, DmiCtrls, StdCtrls, Peatypes, Peaprocs, ExtCtrls, Util,
  VariantComboBox,DMConnection,RStrings,utildb,UtilProc,ADODB,
  DB, Variants, math, DPSControls, ComboBuscarCalle;

type
  TFrameDomicilio = class(TFrame)
    Pnl_Arriba: TPanel;
    Label14: TLabel;
    cb_Regiones: TComboBox;
    Label2: TLabel;
    txt_DescripcionCiudad: TEdit;
    Label6: TLabel;
    Label24: TLabel;
    Pnl_Abajo: TPanel;
    Pnl_Medio: TPanel;
    Label1: TLabel;
    txt_Piso: TNumericEdit;
    txt_numeroCalle: TEdit;
    lbl_Comuna: TLabel;
    cb_Comunas: TComboBox;
    cb_BuscarCalle: TComboBuscarCalle;
    Label7: TLabel;
    txt_CodigoPostal: TEdit;
    Label25: TLabel;
    txt_Detalle: TEdit;
    txt_Depto: TEdit;
    Label4: TLabel;
    procedure cb_RegionesChange(Sender: TObject);
    procedure txt_numeroCalleExit(Sender: TObject);
    procedure cb_BuscarCalleExit(Sender: TObject);
    procedure cb_ComunasChange(Sender: TObject);
    procedure cb_BuscarCalleKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FPais: AnsiString;
    FCodigoRegionAnterior: AnsiString;
    FCodigoComunaAnterior:AnsiString;
    FNumeroNormalizado:Boolean;
    FCodigoDomicilio: integer;
    function GetRegion: AnsiString;
    function GetComuna: AnsiString;
    function GetCiudad: AnsiString;
    function GetCodigoCalle: integer;
    function GetDescripcionCalle: AnsiString;
    function GetNumero: AnsiString;
    function GetPiso: integer;
    function GetDepto: AnsiString;
    function GetDetalle: AnsiString;
    function GetCodigoPostal: AnsiString;
    function GetDomicilioSomple: AnsiString;
    function GetDescripcionPais: AnsiString;
    function GetDescriRegion: AnsiString;
    function GetDescriComuna: AnsiString;
    function GetPais:AnsiString;
    function GetEsNormalizado: boolean;
    function GetEsCalleNormalizada:Boolean;
    function GetEsNumeroNormalizado:Boolean;
    function GetCodigoSegmento:Integer;
    function GetDomiciliocompleto:AnsiString;
    function GetRegDatosDomicilio:TDatosDomicilio;
  public
    { Public declarations }
    property CodigoDomicilio: integer read FCodigoDomicilio;
    property Pais: AnsiString read GetPais;
    property Region: AnsiString read GetRegion;
    property Comuna: AnsiString read GetComuna;
    property Ciudad: AnsiString read GetCiudad;
    property CodigoCalle: integer read GetCodigoCalle;
    property DescripcionCalle: AnsiString read GetDescripcionCalle;
    property Numero: AnsiString read GetNumero;
    property Piso: integer read GetPiso;
    property Depto: AnsiString read GetDepto;
    property Detalle: AnsiString read GetDetalle;
    property CodigoPostal: AnsiString read GetCodigoPostal;
    property DescriPais: AnsiString read GetDescripcionPais;
    property DescriRegion: AnsiString read GetDescriRegion;
    property DescriComuna: AnsiString read GetDescriComuna;
    property EsNormalizado: boolean read GetEsNormalizado;
    property EsCalleNormalizada:Boolean read GetEsCalleNormalizada;
    property EsNumeroNormalizada:Boolean read GetEsNumeroNormalizado;
    property CodigoSegmento:Integer read GetCodigoSegmento;
    property DomicilioCompleto:AnsiString read GetDomiciliocompleto;
    property DomicilioSimple: AnsiString read GetDomicilioSomple;
    property RegDatosDomicilio:TDatosDomicilio read GetRegDatosDomicilio;
    function ValidarDomicilio: boolean;
    function Inicializa(CodigoPais: AnsiString = PAIS_CHILE; CodigoRegion: AnsiString = REGION_SANTIAGO; CodigoComuna: AnsiString = ''):Boolean;
    function SetCodigoPostal : Boolean;

    procedure CargarDatosDomicilio(DatoDomicilio:TDatosDomicilio);
    procedure Limpiar;
    procedure ForzarBusqueda;
  end;

implementation



{$R *.dfm}

{ TFrameDomicilio }

function TFrameDomicilio.GetCiudad: AnsiString;
begin
    result := trim(txt_DescripcionCiudad.text);
end;

function TFrameDomicilio.GetCodigoCalle: integer;
begin
    result:=cb_BuscarCalle.CodigoCalle;
end;

function TFrameDomicilio.GetCodigoPostal: AnsiString;
begin
    Result := iif(Trim(txt_CodigoPostal.Text)<>SIN_ESPECIFICAR,Trim(txt_CodigoPostal.Text),'');
end;

function TFrameDomicilio.GetComuna: AnsiString;
begin
    Result := Trim(StrRight(cb_Comunas.Text, 10));
end;

function TFrameDomicilio.GetDepto: AnsiString;
begin
    result := trim(txt_Depto.text);
end;

function TFrameDomicilio.GetDescriComuna: AnsiString;
begin
    result:=trim(StrLeft(cb_Comunas.Text,200));
end;

function TFrameDomicilio.GetDescripcionPais: AnsiString;
begin
    result := PeaProcs.DescriPais(DMConnections.BaseCAC, self.Pais);
end;

function TFrameDomicilio.GetDescripcionCalle: AnsiString;
begin
    Result := cb_BuscarCalle.NombreCalle;
end;

function TFrameDomicilio.GetDescriRegion: AnsiString;
begin
    result:=Trim(StrLeft(cb_Regiones.Text,200));
end;

function TFrameDomicilio.GetDetalle: AnsiString;
begin
    result := trim(txt_Detalle.Text);
end;

function TFrameDomicilio.GetDomicilioSomple: AnsiString;
begin
    Result := ArmarDomicilioSimple(DMConnections.BaseCAC,
                Self.DescripcionCalle, Self.Numero,
                Self.Piso, Self.Depto, Self.Detalle);
end;

function TFrameDomicilio.GetNumero: AnsiString;
begin
    result := iif(trim(txt_numeroCalle.Text)='','0',trim(txt_numeroCalle.Text));
end;

function TFrameDomicilio.GetPiso: integer;
begin
    Result :=0;
end;

function TFrameDomicilio.GetRegion: AnsiString;
begin
    Result := Trim(StrRight(cb_Regiones.Text, 10));
end;

function TFrameDomicilio.Inicializa(CodigoPais: AnsiString = PAIS_CHILE; CodigoRegion: AnsiString = REGION_SANTIAGO; CodigoComuna: AnsiString = ''):Boolean;
resourcestring
	STR_DIRECCION = 'Direcci�n';
begin

	// Manejo los hints y campos obligatorios
	Self.Caption:=STR_DIRECCION;
    FCodigoRegionAnterior:='-1';
    FCodigoComunaAnterior:='-1';

    if txt_DescripcionCiudad.Visible then GrabarComponentesObligatoriosyHints(self,DMConnections.BaseCAC,[cb_Regiones,cb_Comunas,cb_BuscarCalle ,txt_numeroCalle,txt_Piso,txt_Depto])
    else GrabarComponentesObligatoriosyHints(self,DMConnections.BaseCAC,[txt_DescripcionCiudad,cb_Regiones,cb_Comunas,cb_BuscarCalle,txt_numeroCalle,txt_Piso,txt_Depto]);
    ObtenerComponentesObligatoriosyHints(self,DMConnections.BaseCAC);

    Limpiar;
    FNumeroNormalizado:=False;
    FPais := codigoPais;
    CargarRegiones(DMConnections.BaseCAC, cb_Regiones, FPais, trim(CodigoRegion));
	cb_Regiones.OnChange(cb_Regiones);

    result:=True;
end;

function TFrameDomicilio.ValidarDomicilio: boolean;
begin
    result:=False;
    if ValidateControls([cb_Regiones,
                        txt_DescripcionCiudad,
                        cb_BuscarCalle,
                        txt_numeroCalle,
                        txt_Detalle,
                        cb_Comunas,
                        txt_CodigoPostal,
                        txt_CodigoPostal],
                            [cb_Regiones.ItemIndex>=0,
                            (EsComponenteObligatorio(txt_DescripcionCiudad) and (trim(txt_DescripcionCiudad.Text)<>'')) or not(EsComponenteObligatorio(txt_DescripcionCiudad)),
                            cb_BuscarCalle.nombrecalle<>'',
                            trim(txt_numeroCalle.Text)<>'',
                            (EsComponenteObligatorio(txt_Detalle) and (trim(txt_Detalle.Text)<>'')) or not(EsComponenteObligatorio(txt_Detalle)),
                            cb_Comunas.ItemIndex>0,
                            (EsComponenteObligatorio(txt_CodigoPostal) and (trim(txt_CodigoPostal.Text)<>'' )) or not(EsComponenteObligatorio(txt_CodigoPostal)),
                            PermitirSoloDigitos(txt_CodigoPostal.Text)],
                            MSG_CAPTION_DOMICILIO,
                            [format(MSG_VALIDAR_DEBE_LA,[FLD_REGION]),
                            format(MSG_VALIDAR_DEBE_LA,[FLD_CIUDAD]),
                            format(MSG_VALIDAR_DEBE_LA,[FLD_CALLE]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_NUMERO]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_DETALLE]),
                            format(MSG_VALIDAR_DEBE_LA,[FLD_COMUNA]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_CODIGO_POSTAL]),
                            format(MSG_VALIDAR_CODIGO_POSTAL_DIGITOS,[FLD_CODIGO_POSTAL])
                            ]) then  result:=true;
end;

procedure TFrameDomicilio.CargarDatosDomicilio(
  DatoDomicilio: TDatosDomicilio);
begin
    self.inicializa(DatoDomicilio.CodigoPais,DatoDomicilio.CodigoRegion,DatoDomicilio.CodigoComuna);
    FCodigoDomicilio := DatoDomicilio.CodigoDomicilio;
    cb_BuscarCalle.CodigoRegion := DatoDomicilio.CodigoRegion;
    cb_BuscarCalle.CodigoComuna := DatoDomicilio.CodigoComuna;
    cb_BuscarCalle.CodigoPais := DatoDomicilio.CodigoPais;
    if DatoDomicilio.CodigoCalle > 0 then begin
        cb_BuscarCalle.CodigoCalle := DatoDomicilio.CodigoCalle;
        CargarComunas(DMConnections.BaseCAC,cb_Comunas,DatoDomicilio.CodigoPais,DatoDomicilio.CodigoRegion,DatoDomicilio.CodigoComuna,false,true);
        cb_Comunas.OnChange(cb_Comunas);
        cb_Comunas.Enabled := False;
    end else begin
        CargarComunas(DMConnections.BaseCAC,cb_Comunas,DatoDomicilio.CodigoPais,DatoDomicilio.CodigoRegion,DatoDomicilio.CodigoComuna,false,true);
        cb_Comunas.OnChange(cb_Comunas);
        cb_Comunas.Enabled := True;
        cb_BuscarCalle.NombreCalle := Trim(DatoDomicilio.CalleDesnormalizada);
    end;

    txt_DescripcionCiudad.Text := DatoDomicilio.DescripcionCiudad;
    txt_numeroCalle.Text := Trim(DatoDomicilio.NumeroCalleSTR);
    if Self.EsCalleNormalizada then begin
        if not(NumeroCalleNormalizado(DMConnections.BaseCAC,DatoDomicilio.CodigoCalle,DatoDomicilio.CodigoRegion,DatoDomicilio.CodigoComuna,ABS(DatoDomicilio.NumeroCalle))) then begin
            txt_CodigoPostal.Enabled := true;
            FNumeroNormalizado:=False;
        end else begin
            txt_CodigoPostal.Enabled := False;
            FNumeroNormalizado := True;
        end;
    end else begin
        FNumeroNormalizado := False;
        txt_CodigoPostal.Enabled := True;
    end;
    txt_CodigoPostal.Text := DatoDomicilio.CodigoPostal;
    txt_Piso.Value := DatoDomicilio.Piso;
    txt_Depto.Text := DatoDomicilio.Dpto;
    txt_Detalle.Text := DatoDomicilio.Detalle;
end;

procedure TFrameDomicilio.cb_RegionesChange(Sender: TObject);
begin
    if FCodigoRegionAnterior<>Self.Region then begin
        CargarComunas(DMConnections.BaseCAC,cb_Comunas,FPais,Self.Region,'',false,true);
        cb_Comunas.OnChange(cb_Comunas);
        FCodigoRegionAnterior:=self.Region;
        cb_BuscarCalle.CodigoRegion:=self.Region;
        Self.Tag := 1;
    end;
end;


function TFrameDomicilio.GetPais: AnsiString;
begin
    Result := FPais;
end;

procedure TFrameDomicilio.Limpiar;
begin
    cb_BuscarCalle.NombreCalle:='';
    cb_BuscarCalle.CodigoCalle:=-1;
    FCodigoDomicilio := -1;
    txt_CodigoPostal.Clear;
    txt_DescripcionCiudad.clear;
    txt_Piso.Clear;
    txt_Depto.Clear;
    txt_Detalle.Clear;
    txt_CodigoPostal.Clear;
    txt_numeroCalle.Clear;
    txt_CodigoPostal.Enabled:=false;
end;

procedure TFrameDomicilio.txt_numeroCalleExit(Sender: TObject);
begin
    if (Self.EsCalleNormalizada) and (self.Numero<>'0') then begin
        SetCodigoPostal;
        if trim(txt_CodigoPostal.Text) = '' then begin // N�mero no normalizado o la calle no tiene CP
            txt_CodigoPostal.Enabled:=True;
            FNumeroNormalizado:=False;
            if txt_Detalle.Focused then txt_CodigoPostal.SetFocus;
            MsgBoxBalloon(MSG_NUMERO_DESNORMALIZADO,FLD_DOMICILIO,MB_ICONWARNING,txt_CodigoPostal);
        end else begin
            txt_CodigoPostal.Enabled:=False;
            FNumeroNormalizado:=True;
        end;
    end else txt_CodigoPostal.Enabled:=True;
    if not txt_CodigoPostal.Enabled and txt_CodigoPostal.Focused then
        txt_Detalle.SetFocus;
end;

function TFrameDomicilio.GetEsNormalizado: boolean;
begin
    Result := self.EsCalleNormalizada and EsNumeroNormalizada ;
end;

function TFrameDomicilio.GetEsCalleNormalizada: Boolean;
begin
    result:=cb_BuscarCalle.CalleNormalizada;
end;

function TFrameDomicilio.GetEsNumeroNormalizado: Boolean;
begin
    result:= (self.EsCalleNormalizada) and (FNumeroNormalizado);
end;

function TFrameDomicilio.GetCodigoSegmento: Integer;
begin
    if self.EsNormalizado then result:=QueryGetValueInt(DMConnections.BaseCAC,format('EXEC ObtenerCodigoSegmento ''%s'' ,''%s'',''%s'',%d,%d, NULL',[self.Pais, self.Region, self.Comuna, self.CodigoCalle, FormatearNumeroCalle(self.Numero)]))
    else result:=-1;
end;

function TFrameDomicilio.GetDomiciliocompleto: AnsiString;
begin
    result:=ArmarDomicilioCompleto(DMConnections.BaseCAC,self.DescripcionCalle,self.Numero,-1,'',self.Detalle,'',self.Comuna,Self.Region);
end;

procedure TFrameDomicilio.cb_BuscarCalleExit(Sender: TObject);
begin
    if self.EsCalleNormalizada then begin
        CargarComunas(DMConnections.BaseCAC,cb_Comunas,Self.Pais,Self.Region, cb_BuscarCalle.CodigoComuna,false,true); // hago toda esta convercion porque se carga el (codigo de calle *1000) + codigo comuna
        cb_Comunas.OnChange(cb_Comunas);
        cb_Comunas.Enabled:=False;
        txt_CodigoPostal.Enabled:=False;
        txt_CodigoPostal.Clear;
    end else begin
        if cb_Comunas.ItemIndex=0 then begin
            CargarComunas(DMConnections.BaseCAC, cb_Comunas, Self.Pais, Self.Region, '', false, true); // solo si no hay ninguna comuna seleccioada
            cb_Comunas.OnChange(cb_Comunas);            
            MsgBoxBalloon(MSG_ERROR_CALLE_DESNORMALIZADA,FLD_DOMICILIO,MB_ICONWARNING,cb_BuscarCalle);
        end;
        cb_Comunas.Enabled:=True;
        txt_CodigoPostal.Enabled:=True;
    end;
    //txt_CodigoPostal.Clear;
    if trim(txt_numeroCalle.Text)<>'' then txt_numeroCalle.OnExit(txt_numeroCalle);

end;

procedure TFrameDomicilio.cb_ComunasChange(Sender: TObject);
begin
    if FCodigoComunaAnterior<>Self.Comuna then begin
        FCodigoComunaAnterior:=self.Comuna;
        cb_BuscarCalle.CodigoComuna:=self.Comuna;
        Self.Tag := 1;
    end;
end;

function TFrameDomicilio.GetRegDatosDomicilio: TDatosDomicilio;
var
    aux:TDatosDomicilio;
begin
    with aux do begin
        IndiceDomicilio:=-1;
        Descripcion:=self.DescripcionCalle;
        CodigoDomicilio:=self.CodigoDomicilio;
        CodigoTipoDomicilio:=1; // Domiciolio particular
        CodigoCalle:=self.CodigoCalle;
        CodigoSegmento:=self.CodigoSegmento;
        CodigoTipoCalle:=-1;
        NumeroCalle:= FormatearNumeroCalle(self.Numero);
        NumeroCalleSTR:=self.Numero;
        Piso:=self.Piso;
        Dpto:=self.Depto;
        Detalle:=self.Detalle;
        CodigoPostal:=self.CodigoPostal;
        CodigoPais:=self.Pais;
        CodigoRegion:=self.Region;
        CodigoComuna:=self.Comuna;
        CodigoCiudad:='';
        DescripcionCiudad:='';
        CalleDesnormalizada:=self.DescripcionCalle;
        DomicilioEntrega:=false;
        Normalizado:=self.EsNormalizado;
        Activo:=True;
    end;
    Result:=aux;
end;


{******************************** Function Header ******************************
Function Name: TFrameDomicilio.SetCodigoPostal
Author       : dcalani
Date Created : 27/07/2004
Description  : Obtiene el CP, lo setea en el edit y devuelve true si pudo encontrar un CP
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TFrameDomicilio.SetCodigoPostal: Boolean;
begin
    txt_CodigoPostal.Text := QueryGetValue(DMConnections.BaseCAC,format('EXEC ObtenerCP ''%s'', ''%s'', ''%s'', %d,%d',
         [self.Pais, self.Region, self.Comuna, self.CodigoCalle,FormatearNumeroCalle(self.Numero)]));

    result := trim(txt_CodigoPostal.Text) <> '';
end;

procedure TFrameDomicilio.cb_BuscarCalleKeyPress(Sender: TObject;
  var Key: Char);
begin
    if key in ['}',''''] then Key := #0;
end;

procedure TFrameDomicilio.ForzarBusqueda;
begin
    cb_BuscarCalle.SetFocus;
    cb_BuscarCalle.ForzarBusqueda;
end;

end.
