{********************************** File Header ********************************
File Name   : DMConnection.pas
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Author      :
Date        :
Description :

Revision 1
Author: ggomez
Date: 11/10/2006
Description:
  - Cambi� el uso de la BD del SOR. Agregu� el Willconnect para el SOR.
  - Quit� la BD RNUT.

Revision 2
Author: 
Date:
Description: SS 744

Revision    : 3
Author      : Alejandro Labra
Date        : 10-05-2011
Description : Se agrego Conecci�n a AMB, adem�s se tuvo que copiar las funciones
               SecureDatabase, las cuales se renombraron a SecureDatabaseAMB,
               ya que ahora utilizan contrase�a los nombre de usuario (usr_startup)
Firma       : PAR00133-Fase 2-ALA-20110427

Firma       : SS_979_ALA_20110914 - SS_979_ALA_20110929
Description : Se crea metodo que entrega el objeto TADOConection dependiendo
              del archivo install.ini.
              Se crea m�todo que entrega el Servidor y base de datos a la cual
              se encuentra conectado el sistema.

Firma       : SS_1147_MCA_20150416
Descripcion : Se reemplaza provider del CAC de SQLNCLI a SQLOLEDB

Firma       :   SS_1147_MBE_20150423
Description :   Se fuerza el usaurio "usr_startup" en el connectionstring,
                pues el usuario "sa" da error de conexi�n

Etiqueta	: 20160307 MGO
Descripci�n : Se agrega BaseBO_Master
              Se eliminan las funciones SecureDatabaseAMB
              Se agrega el m�todo DataModuleCreate    
              Se utilizan campos encriptados en install.ini para el usuario y clave de la DB

Etiqueta	: 20160603 FSI
Descripci�n : Se agrega BaseBO_Rating
              Se agrega el m�todo DataModuleCreate
              Se utilizan campos encriptados en install.ini para el usuario y clave de la DB

Etiqueta    : TASK_051_MGO_20160720
Descripci�n : Se agrega conexi�n a BO_Billing
*******************************************************************************}
unit DMConnection;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, Util, UtilProc, UtilDB, DBTables, Crypto;

type
  TDMConnections = class(TDataModule)
    BaseCOP: TADOConnection;
    BaseCAC: TADOConnection;
    cnInformes: TADOConnection;
    BasePMMOPTT: TADOConnection;
    BaseMOPCAC: TADOConnection;
    BaseSOR: TADOConnection;
    ReportesAdmin: TADOConnection;
    BaseCOPAMB: TADOConnection;
    BasePMMOPTTAMB: TADOConnection;
	  BaseBO_Master: TADOConnection;
    BaseBO_Rating: TADOConnection;
    BaseBO_Billing: TADOConnection;
    BaseBO_Dynac: TADOConnection;                         //TASK_110_JMA_20170224
    procedure BaseMOPCACWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BasePMMOPTTWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BaseCOPAMBWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BasePMMOPTTAMBWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BaseCOPWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BaseCACWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure BaseSORWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
	procedure BaseBO_MasterWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    procedure DataModuleCreate(Sender: TObject);
	procedure BaseBO_RatingWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    // INICIO : TASK_051_MGO_20160720
	procedure BaseBO_BillingWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    // FIN : TASK_051_MGO_20160720
    {INICIO: TASK_110_JMA_20170224}
     procedure BaseBO_DynacWillConnect(Connection: TADOConnection;
      var ConnectionString, UserID, Password: WideString;
      var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
    {TERMINO: TASK_110_JMA_20170224}
  private
    lPasswordHash,
    lEncryptedPassword,
    lUsernameHash,
    lEncryptedUsername: AnsiString;
  public
    procedure SetUp;
  end;

var
  DMConnections: TDMConnections;

  
implementation

{$R *.DFM}
resourcestring
    CAPTION_ASEGURAR_BASE_DATOS = 'Asegurar Base de Datos';
    MSG_ASEGURAR_BASE_DATOS_CAC = 'No ha podido asegurarse la base de datos CAC.';
    MSG_ASEGURAR_BASE_DATOS_COP = 'No ha podido asegurarse la base de datos COP.';
    MSG_ASEGURAR_BASE_DATOS_AUD_COP = 'No ha podido asegurarse la base de datos de auditor�a del COP.';
    MSG_ASEGURAR_BASE_DATOS_AUD_CAC = 'No ha podido asegurarse la base de datos de auditor�a del CAC.';
    MSG_ASEGURAR_BASE_DATOS_SOR = 'No ha podido asegurarse la base de datos SOR.';
    MSG_ASEGURAR_BASE_DATOS_COP_AMB     = 'No ha podido asegurarse la base de datos COP AMB.';
    MSG_ASEGURAR_BASE_DATOS_AUD_COP_AMB = 'No ha podido asegurarse la base de datos de auditor�a del COP AMB.';
	MSG_ASEGURAR_BASE_DATOS_MASTER = 'No ha podido asegurarse la base de datos Master.';         
	MSG_ASEGURAR_BASE_DATOS_BILLING = 'No ha podido asegurarse la base de datos Billing.';     // TASK_051_MGO_20160720

procedure TDMConnections.BaseCOPAMBWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

procedure TDMConnections.BasePMMOPTTAMBWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;


procedure TDMConnections.BaseCOPWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

procedure TDMConnections.BaseCACWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
	UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

procedure TDMConnections.BasePMMOPTTWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
	UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

procedure TDMConnections.BaseMOPCACWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
	UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

procedure TDMConnections.SetUp;
begin
    // Cargamos el login de las bases de datos
    lUsernameHash       := InstallIni.ReadString('General', 'UsernameHash', 'Cualquiera');
    lEncryptedUsername  := InstallIni.ReadString('General', 'Username', 'Cualquiera');
    lPasswordHash       := InstallIni.ReadString('General', 'PasswordHash', 'Cualquiera');
    lEncryptedPassword  := InstallIni.ReadString('General', 'Password', 'Cualquiera');

	BaseCOP.Connected := False;
	BaseCOP.KeepConnection := True;
	BaseCOP.LoginPrompt := False;
	BaseCOP.ConnectionString :=
      'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database COP', 'DatabaseName', 'COP') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database COP', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID=' + GetMachineName();

    BaseCAC.Connected := False;
	BaseCAC.KeepConnection := True;
	BaseCAC.LoginPrompt := False;
	BaseCAC.ConnectionString :=
      'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database CAC', 'DatabaseName', 'CAC') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database CAC', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID=' + GetMachineName();

    BaseBO_Master.Connected := False;
	BaseBO_Master.KeepConnection := True;
	BaseBO_Master.LoginPrompt := False;
	BaseBO_Master.ConnectionString :=
      'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database Master', 'DatabaseName', 'Master') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database Master', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID=' + GetMachineName();

    // INICIO : TASK_051_MGO_20160720
    BaseBO_Billing.Connected := False;
	BaseBO_Billing.KeepConnection := True;
	BaseBO_Billing.LoginPrompt := False;
	BaseBO_Billing.ConnectionString :=
      'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database Billing', 'DatabaseName', 'Billing') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database Billing', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID=' + GetMachineName();
    // FIN : TASK_051_MGO_20160720

{INICIO: TASK_110_JMA_20170224}
    BaseBO_Dynac.Connected := False;
	BaseBO_Dynac.KeepConnection := True;
	BaseBO_Dynac.LoginPrompt := False;
	BaseBO_Dynac.ConnectionString :=
      'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database Dynac', 'DatabaseName', 'Dynac') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database Dynac', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID=' + GetMachineName();
{TERMINO: TASK_110_JMA_20170224}

    BaseBO_Rating.Connected := False;
	BaseBO_Rating.KeepConnection := True;
	BaseBO_Rating.LoginPrompt := False;
	BaseBO_Rating.ConnectionString :=
      'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database Rating', 'DatabaseName', 'BO_Rating') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database Rating', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID=' + GetMachineName();

    cnInformes.Connected := False;
	cnInformes.KeepConnection := True;
	cnInformes.LoginPrompt := False;
	cnInformes.ConnectionString :=
	  'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database CAC', 'DatabaseName', 'CAC') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database CAC', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID=' + GetMachineName();

    ReportesAdmin.Connected := False;
	ReportesAdmin.KeepConnection := True;
	ReportesAdmin.LoginPrompt := False;
	ReportesAdmin.ConnectionString :=
	  'Provider=SQLOLEDB.1;User ID=sa;' +
	  'Initial Catalog=' +
		InstallIni.ReadString('Database CAC', 'DatabaseName', 'CAC') + ';' +
	  'Data Source=' +
		InstallIni.ReadString('Database CAC', 'Server', 'Cualquiera')+ ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID='   + GetMachineName();
end;

procedure TDMConnections.BaseSORWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

procedure TDMConnections.BaseBO_MasterWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

// INICIO : TASK_051_MGO_20160720
procedure TDMConnections.BaseBO_BillingWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;
// FIN : TASK_051_MGO_20160720

{INICIO: TASK_110_JMA_20170224}
procedure TDMConnections.BaseBO_DynacWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;
{TERMINO: TASK_110_JMA_20170224}

procedure TDMConnections.BaseBO_RatingWillConnect(Connection: TADOConnection;
  var ConnectionString, UserID, Password: WideString;
  var ConnectOptions: TConnectOption; var EventStatus: TEventStatus);
begin
    UserID := DecodePWDEx(lEncryptedUsername, lUsernameHash);
    Password := DecodePWDEx(lEncryptedPassword, lPasswordHash);
end;

procedure TDMConnections.DataModuleCreate(Sender: TObject);
begin
    BaseCOP.Connected := False;
    BaseCAC.Connected := False;
    cnInformes.Connected := False;
    BasePMMOPTT.Connected := False;
    BasePMMOPTTAMB.Connected := False;
    BaseMOPCAC.Connected := False;
    BaseSOR.Connected := False;
    BaseCOPAMB.Connected := False;
    ReportesAdmin.Connected := False;
    BaseBO_Master.Connected := False;
    BaseBO_Rating.Connected := False;   
    BaseBO_Billing.Connected := False;   // TASK_051_MGO_20160720
end;

end.
