{------------------------------------------------------------------------------
    	frmRptDetInfraccionesAnuladas

Author: mpiazza
Date: 27-mayo-2009
Description:		(Ref. facturaci�n Electr�nica)
                Despliega un detalle con las infracciones anuladas, para
                las infracciones facturadas electr�nicamente

Revision 1
Author: mbecerra
Date: 29-Junio-2009
Description:		(Ref. Facturaci�n Electr�nica)
		1.-    	Se modifica la funci�n Inicializar para que retorne un mensaje
                de error cuando falla, pues en estos momentos retorna un mensaje
                en blanco y aparece una ventana de error sin mensaje en el
                formulario frmFacturacionManual.

        2.-		Se cambia que despliegue el BumeroComprobanteFiscal en vez del
            	NumeroComprobante

        3.-		Se cambia la llamada al stored ObtenerNotaCobroInfraccionAnulada
            	Por un SELECT de campos necesarios, pues adem�s el stored no
                est� correcto ya que obtiene datos de LineasComprobantes, lo cual
                est� obsoleto.

        4.-		Se quita el usuario, pues no corresponde.

        5.-		Se corrige para que despliegue correctamente el Tipo de Documento
            	Electr�nico, pues hasta ahora la etiqueta est� fija.

Revision 2
Author: pdominguez
Date  : 05/06/2010
Description: Infractores Fase 2
    - Se modificaron los Siguientes Objetos:
        rb_ReporteDetalleInfraccionAnulada: TRBInterface

    - Se modificaron los siguientes procedimientos/funciones:
        Inicializar

Firma       : SS_1045_CQU_20120604
Date        : 18-06-2012
Description : Se modifica "Inicializar" agregandole una variable que indica si se muestra o no
              la ventana de las opciones, por defecto ser� True para no afectar otras funcionalidades

Firma       : SS_660_CQU_20121010
Date        : 13-11-2012
Description : Las NK's generadas por rut (SS_1045) se generan por patente, es decir, una patente = una NK
              y en el facturador de infracciones por morosidad se hace agrupando por rut,
              por lo tanto se tuvo que modificar la presenteci�n agregando un grupo por patente.

Firma       : SS_1045_CQU_20131108
Date        : 13/11/2013
Description : Se agrega la totalizacion por Patente


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma       : SS_1397_MGO_20151021
Description : Se usa el par�metro 'Logo_Reportes' para la imagen del logo
-----------------------------------------------------------------------------------}
unit frmRptDetInfraccionesAnuladas;

interface

uses
    //Detalle de Infracciones
    DMConnection,
    util,
    utilproc,
    UtilDb,
    PeaTypes,
    ConstParametrosGenerales,
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, ppDB, DB, ppDBPipe, ADODB, UtilRB, ppParameter, ppModule, raCodMod,
    ppBands, ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd,
    ppReport, ppStrtch, ppSubRpt, ppTypes; // SS_1045_CQU_20120604

type
  TRptDetInfraccionesAnuladas = class(TForm)
    rpt_DetalleInfraccionesAnuladas: TppReport;
    ppParameterList1: TppParameterList;
    dbpl_ObtenerInfraccionesComprobante: TppDBPipeline;
    sp_ObtenerInfraccionesAnuladasComprobante: TADOStoredProc;
    dsObtenerInfraccionesComprobante: TDataSource;
    rb_ReporteDetalleInfraccionAnulada: TRBInterface;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLabel25: TppLabel;
    ppLine3: TppLine;
    ppfechaVencimiento: TppLabel;
    ppLabel6: TppLabel;
    ppusuario: TppLabel;
    ppFechaEmision: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel4: TppLabel;
    ppNumeroComprobante: TppLabel;
    ppLabel10: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText3: TppDBText;
    // ppSumaryBand3: TppSumaryBand;    // SS_1045_CQU_20120604
    ppLabel5: TppLabel;
    ppTotal: TppLabel;
    ppCount: TppLabel;
    ppLabel11: TppLabel;
    raCodeModule3: TraCodeModule;
    spComprobante: TADOStoredProc;
    ppTipoComprobante: TppLabel;
    ppColumnaPrincipal: TppColumnHeaderBand;
    ppFooterPrincipal: TppColumnFooterBand;
    pplConcesionaria: TppLabel;
    pplPatente: TppLabel;
    txtPatente: TppDBText;
    txtNombreConcesionaria: TppDBText;
    lblFechaInfraccion: TppLabel;
    ppLineDetalle: TppLine;
    lblImporte: TppLabel;
    sumTotalImporte: TppDBCalc;
    // lblSubTotalConcesionaria: TppLabel;  // SS_1045_CQU_20120604
    ppsSombraConcesionaria: TppShape;
    ppsSombraPatente: TppShape;
    ppsSombraPatenteDato: TppShape;
    ppGrupoConcesionaria: TppGroup;
    ppGrupoConcesionariaHeader: TppGroupHeaderBand;
    ppGrupoConcesionariaFooter: TppGroupFooterBand;
    ppsSombraConcesionariaDato: TppShape;
    txtCodigoConcesionaria: TppDBText;
    // pplLineaImporte: TppLine // SS_1045_CQU_20120604
    LineaSubTotal: TppLine;
    txtConcesionariaSubTotalConcesionaria: TppLabel;                        // SS_1045_CQU_20120604
    Footer: TppFooterBand;                                                  // SS_1045_CQU_20120604
    SombraCantidad: TppShape;                                               // SS_1045_CQU_20120604
    SombraTotalTransitos: TppShape;                                         // SS_1045_CQU_20120604
    ppGrupoPatente: TppGroup;                                               // SS_660_CQU_20121010
    ppGrupoPatenteHeader: TppGroupHeaderBand;                               // SS_660_CQU_20121010
    ppGrupoPatenteFooter: TppGroupFooterBand;                               // SS_660_CQU_20121010
    ppDBCalc1: TppDBCalc;													// SS_1045_CQU_20131108
    ppLine1: TppLine;														// SS_1045_CQU_20131108
    ppLabel1: TppLabel;														// SS_1045_CQU_20131108
    procedure rb_ReporteDetalleInfraccionAnuladaExecute(Sender: TObject;
      var Cancelled: Boolean);
    procedure ppDBText1Format(Sender: TObject; DisplayFormat: string;
      DataType: TppDataType; Value: Variant; var Text: string);
  private
    { Private declarations }
    FNumeroComprobante  : Int64;
    FTipoComprobante    : STRING;
  public
    { Public declarations }
    {Rev.1		function Inicializar(Conexion: TADOConnection; TipoComprobante: STRING; NumeroComprobante: Int64): Boolean;}
    //function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: Int64; var DescError: string; Anuladas: Boolean = False): Boolean;                                 // SS_1045_CQU_20120604
    function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: Int64; var DescError: string; Anuladas: Boolean = False; MostrarOpciones : Boolean = True): Boolean; // SS_1045_CQU_20120604
  end;

var
  RptDetInfraccionesAnuladas: TRptDetInfraccionesAnuladas;

implementation

{$R *.dfm}

{ TForm1 }

{
    Function Name: Inicializar
    Parameters : Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: Int64;
        var DescError: string; Anuladas: Boolean = False
    Return Value: Boolean
    Author :
    Date Created :

    Description : Inicializaci�n de Este Formulario.

    Revision : 1
        Author: pdominguez
        Date: 01/06/2010
        Description: Infractores Fase 2
            - Se a�ade el par�metro Anuladas para saber cuando mostrar el mensaje Facturadas o Anuladas.

        Firma       :   SS_1045_CQU_20120604
        Description :   Se realizan modificaciones para indicar si mostrar o no el Dialog de impresi�n.
                        ojo que si se oculta el Dialog se debe llamar al m�todo
                        rb_ReporteDetalleInfraccionAnuladaExecute que es el encargado de obtener los datos
                        de lo contrario no obtienen nada y arroja un error por pantalla que indica lo siguiente
                        "No data found for this report."
}
function TRptDetInfraccionesAnuladas.Inicializar; //REV.1 (Conexion: TADOConnection; TipoComprobante: STRING; NumeroComprobante: Int64): Boolean;
{REV.1
var
Cancelled: boolean;}
var							// SS_1045_CQU_20120604
  Cancelled : Boolean;   	// SS_1045_CQU_20120604
  RutaLogo:AnsiString;                                                          //SS_1147_NDR_20140710
begin

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710


  	DescError := '';
    FNumeroComprobante	:= NumeroComprobante;
    FTipoComprobante	:= TipoComprobante;

    sp_ObtenerInfraccionesAnuladasComprobante.Close;        //REV.1

    //REV.1 SP_ObtenerNotaCobroInfraccionAnulada.Connection			:= Conexion;
    sp_ObtenerInfraccionesAnuladasComprobante.Connection	:= Conexion;
    //REV.1 rb_ReporteDetalleInfraccionAnulada.Execute(true);
    //rb_ReporteDetalleInfraccionAnuladaExecute(nil, Cancelled);
    //REV.1 Result := not Cancelled;

    if Anuladas then rb_ReporteDetalleInfraccionAnulada.Caption := 'Impresi�n Detalle Infracciones Anuladas'; //Rev. 2 (Infractores Fase 2)

    ObtenerParametroGeneral(Conexion, LOGO_REPORTES, RutaLogo);                 //SS_1397_MGO_20151021
    try                                                                         //SS_1397_MGO_20151021
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                            //SS_1397_MGO_20151021
    except                                                                      //SS_1397_MGO_20151021
      On E: Exception do begin                                                  //SS_1397_MGO_20151021
        Screen.Cursor := crDefault;                                             //SS_1397_MGO_20151021
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                           //SS_1397_MGO_20151021
        Screen.Cursor := crHourGlass;                                           //SS_1397_MGO_20151021
      end;                                                                      //SS_1397_MGO_20151021
    end;                                                                        //SS_1397_MGO_20151021

    //REV.1
    try
    	//rb_ReporteDetalleInfraccionAnulada.Execute( True );			// SS_1045_CQU_20120604
        if not MostrarOpciones then										// SS_1045_CQU_20120604
            rb_ReporteDetalleInfraccionAnuladaExecute(Self, Cancelled);	// SS_1045_CQU_20120604

        rb_ReporteDetalleInfraccionAnulada.Execute(MostrarOpciones);	// SS_1045_CQU_20120604
        Result := True;
    except on e:exception do begin
            DescError := e.Message;
            Result := False;
    	end;
    end;
end;

procedure TRptDetInfraccionesAnuladas.rb_ReporteDetalleInfraccionAnuladaExecute(
  Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_ERROR_INVOICE_NOT_FOUND				= 'No se encuentra el Comprobante';
    MSG_ERROR_PRINTING_THE_COLLECTION_NOTE	= 'Se ha producido un error al imprimir la Nota de Cobro';
//REV.1
    TIPO_DOCUMENTO_FACTURA_AFECTA			= 'FACTURA ELECTRONICA';
    TIPO_DOCUMENTO_FACTURA_EXENTA			= 'FACTURA NO AFECTA O EXENTA ELECTRONICA';
    TIPO_DOCUMENTO_BOLETA_AFECTA			= 'BOLETA ELECTRONICA';
    TIPO_DOCUMENTO_BOLETA_EXENTA			= 'BOLETA NO AFECTA O EXENTA ELECTRONICA';
var
    Total, Cantidad : Real;
begin
    try
    	{ ---------------- REV.1---------------------------------------
        with SP_ObtenerNotaCobroInfraccionAnulada, Parameters do begin
            Close;
            Refresh;
            ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            ParamByName('@TipoComprobante').Value := FTipoComprobante ;
            CommandTimeOut := 500;
            Open;
            if not IsEmpty then begin
                //Encabezado
                ppusuario.Caption:= QueryGetValue(dmconnections.BaseCAC,'select dbo.ObtenerNombreUsuario( ''' + usuariosistema + ''' ) ');//usuariosistema;
                ppfecha.Caption := DateToStr(Now);


                ppComprobante.Caption := FieldByName('NumeroComprobante').AsString;
                ppFechaVencimiento.Caption := DateToStr(FieldByName('FechaVencimiento').AsDateTime);
            end else begin
                Cancelled := True;
                MsgBox(MSG_ERROR_INVOICE_NOT_FOUND, Self.Caption, MB_ICONERROR);
                Exit;
            end;
        end;                         }

        with spComprobante do begin
        	Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
            Open;
            if FieldByName('TipoComprobanteFiscal').AsString = TC_BOLETA_EXENTA then ppTipoComprobante.Caption := TIPO_DOCUMENTO_BOLETA_EXENTA;
            if FieldByName('TipoComprobanteFiscal').AsString = TC_BOLETA_AFECTA then ppTipoComprobante.Caption := TIPO_DOCUMENTO_BOLETA_AFECTA;
            if FieldByName('TipoComprobanteFiscal').AsString = TC_FACTURA_EXENTA then ppTipoComprobante.Caption := TIPO_DOCUMENTO_FACTURA_EXENTA;
            if FieldByName('TipoComprobanteFiscal').AsString = TC_FACTURA_AFECTA then ppTipoComprobante.Caption := TIPO_DOCUMENTO_FACTURA_AFECTA;
            ppNumeroComprobante.Caption	:= Format('%.0n', [1.0 * FieldByName('NumeroComprobanteFiscal').AsInteger]);
            ppFechaEmision.Caption		:= FormatDateTime('dd-mm-yyyy', FieldByName('FechaEmision').AsDateTime);
            ppfechaVencimiento.Caption	:= FormatDateTime('dd-mm-yyyy', FieldByName('FechaVencimiento').AsDateTime);
        end;
        {--------------------------- FIN REV.1 -------------------------------}
        //Obtengo las infracciones incluidas en el comprobante
        with sp_ObtenerInfraccionesAnuladasComprobante, Parameters do begin
            Close;
            Refresh;
            ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            ParamByName('@TipoComprobante').Value := FTipoComprobante;
            ParamByName('@Total').Value := null;
            ParamByName('@Cantidad').Value := null;
            //ParamByName('@CantidadInfracciones').Value := 0;
            CommandTimeOut := 500;
            Open;
            {REV.1
            ppTotal.Caption := ParamByName('@Total').Value;
            ppCount.Caption := ParamByName('@Cantidad').Value;}
            Total		:= ParamByName('@Total').Value;
            Cantidad	:= ParamByName('@Cantidad').Value;
            //ppTotal.Caption := Format('%.0n', [Total]);    // SS_1045_CQU_20120604
            ppTotal.Caption := Format('$ %.0n', [Total]);    // SS_1045_CQU_20120604
            ppCount.Caption := Format('%.0n', [Cantidad]);
        end;


    except
        on E : Exception do begin
            Cancelled := True;
            MsgBoxErr(MSG_ERROR_PRINTING_THE_COLLECTION_NOTE, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name : txtTotalConcesionariaFechaHoraFormat
  Author        : CQuezadaI
  Date Created  : 13/07/2012
  Description   : Permite colocar el formato a la fecha en el reporte.
  Parameters    : Sender: TObject;
                  DisplayFormat: string;
                  DataType: TppDataType;
                  Value: Variant;
                  var Text: string
  Return Value  : None
  Firma         : SS_1045_CQU_20120604
-----------------------------------------------------------------------------}
procedure TRptDetInfraccionesAnuladas.ppDBText1Format(Sender: TObject;
  DisplayFormat: string; DataType: TppDataType; Value: Variant;
  var Text: string);
begin
    if Trim(txtCodigoConcesionaria.Text) = 'AMB' then Text := FormatDateTime('dd/mm/yyyy  hh:nn', Value)
    else  Text := FormatDateTime('dd/mm/yyyy', Value);
end;

end.
