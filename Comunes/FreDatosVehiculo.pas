unit FreDatosVehiculo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DmiCtrls, VariantComboBox, StdCtrls, Util, Peaprocs, RStrings,
  UtilProc, DB, ADODB,PeaTypes;

type
  TFrameDatosVehiculo = class(TFrame)
    Label13: TLabel;
    txt_Patente: TEdit;
    Label14: TLabel;
    cb_marca: TComboBox;
    Label20: TLabel;
    txt_DescripcionModelo: TEdit;
    lblTipoVehiculo: TLabel;
    cb_TipoVehiculo: TVariantComboBox;
    Label47: TLabel;
    txt_anio: TNumericEdit;
    Label21: TLabel;
    cb_color: TComboBox;
    VerificarFormatoPatenteChilena: TADOStoredProc;
    procedure txt_PatenteKeyPress(Sender: TObject; var Key: Char);
    procedure cb_marcaChange(Sender: TObject);
  private
    { Private declarations }
    EventoOnChangeMarca:TNotifyEvent;
    Function GetTipoPatente: AnsiString;
//    Procedure SetTipoPatente(Const Value: AnsiString);
    Function GetPatente: AnsiString;
    Procedure SetPatente(Const Value: AnsiString);
    Function GetCodigoMarca: integer;
    Procedure SetCodigoMarca(Const Value: integer);
    Function GetDescripcionMarca: AnsiString;
    Procedure SetCodigoTipo(Const Value: integer);
    Function GetCodigoTipo: integer;
    Function GetDescripcionTipo: AnsiString;
    Function GetDescripcionModelo: AnsiString;
    Procedure SetDescripcionModelo(Const Value: AnsiString);
    Function GetAnio: integer;
    Procedure SetAnio(Const Value: integer);
    Function GetCodigoColor: integer;
    Procedure SetCodigoColor(Const Value: integer);
    Function GetDescripcionColor: AnsiString;
    function GetDetalleVehiculoSimple: AnsiString;
    function GetDetalleVehiculoCompleta: AnsiString;
    function GetCodigoModelo: integer;

  public
    { Public declarations }
    Property TipoPatente: AnsiString read GetTipoPatente;
    Property Patente: AnsiString read GetPatente;
    property Marca: integer read GetCodigoMarca;
    property DescripcionMarca: AnsiString read GetDescripcionMarca;
    property CodigoTipo: integer read GetCodigoTipo;
    property DescripcionTipo: AnsiString read GetDescripcionTipo;
    property Modelo: integer read GetCodigoModelo;
    property DescripcionModelo: AnsiString read GetDescripcionModelo;
    property Anio: integer read GetAnio;
    property Color: integer read GetCodigoColor;
    property DescripcionColor: AnsiString read GetDescripcionColor;
    property DetalleVehiculoSimple: Ansistring read GetDetalleVehiculoSimple;
    property DetalleVehiculoCompleto: Ansistring read GetDetalleVehiculoCompleta;
    property OnChangeMarca: TNotifyEvent read EventoOnChangeMarca write EventoOnChangeMarca;
    function ValidarDatosVehiculo: boolean;
    Function Inicializar(): Boolean; overload; //Para el Alta.
    Function Inicializar(TipoPatente, Patente, Modelo: AnsiString; CodigoMarca, Anio, CodigoTipo, CodigoColor: integer): Boolean; overload;

  end;

implementation

uses DMConnection;

{$R *.dfm}

{ TFrame1 }

function TFrameDatosVehiculo.GetAnio: integer;
begin
    Result := txt_Anio.ValueInt;
end;

function TFrameDatosVehiculo.GetCodigoColor: integer;
begin
    if (cb_color.ItemIndex>=0) and (cb_color.Visible) then Result := Ival(StrRight(cb_Color.Text, 10))
    else result:=-1;
end;

function TFrameDatosVehiculo.GetCodigoMarca: integer;
begin
    Result := Ival(StrRight(cb_Marca.Text, 10));
end;

function TFrameDatosVehiculo.GetCodigoModelo: integer;
begin
    result:=-1;
end;

function TFrameDatosVehiculo.GetCodigoTipo: integer;
begin
    Result := Ival(cb_TipoVehiculo.Value);
end;

function TFrameDatosVehiculo.GetDescripcionColor: AnsiString;
begin
    if (cb_Color.ItemIndex < 0) or not(cb_color.Visible) then Result := ''
    else Result := Trim(StrLeft(cb_Color.Text, 40));
end;

function TFrameDatosVehiculo.GetDescripcionMarca: AnsiString;
begin
    if cb_Marca.ItemIndex < 0 then Result := ''
    else Result := Trim(StrLeft(cb_Marca.Text, 40));
end;

function TFrameDatosVehiculo.GetDescripcionModelo: AnsiString;
begin
//    if cb_Modelo.ItemIndex < 0 then Result := ''
//    else Result := Trim(StrLeft(cb_Modelo.Text, 40));
    result := Trim(txt_DescripcionModelo.Text);
end;

function TFrameDatosVehiculo.GetDescripcionTipo: AnsiString;
begin
    if cb_TipoVehiculo.ItemIndex < 0 then Result := ''
    else Result := Trim(cb_TipoVehiculo.Items[cb_TipoVehiculo.ItemIndex].Caption);
end;

function TFrameDatosVehiculo.GetDetalleVehiculoCompleta: AnsiString;
begin
    Result := DetalleVehiculoSimple + ' Patente: ' + Patente;
end;

function TFrameDatosVehiculo.GetDetalleVehiculoSimple: AnsiString;
begin
    Result := DescripcionMarca + ', ' + DescripcionModelo + ', ' + DescripcionColor;
end;

function TFrameDatosVehiculo.GetPatente: AnsiString;
begin
    Result := trim(txt_Patente.Text);
end;

function TFrameDatosVehiculo.GetTipoPatente: AnsiString;
begin
    result:=PATENTE_CHILE;
end;

function TFrameDatosVehiculo.Inicializar: Boolean;
begin
    //SetTipoPatente('CHI');
    SetCodigoMarca(1);
    SetCodigoColor(1);
    SetCodigoTipo(1);
    //txt_Patente.setFocus; //tiene que estar en el onshow del FORM
    Result := True;
end;

function TFrameDatosVehiculo.Inicializar(TipoPatente, Patente, Modelo: AnsiString; CodigoMarca, Anio, CodigoTipo, CodigoColor: integer): Boolean;
begin
//    SetTipoPatente(TipoPatente);
    SetPatente(Patente);
    SetCodigoMarca(CodigoMarca);
    SetDescripcionModelo(Modelo);
    SetCodigoColor(CodigoColor);
    SetAnio(Anio);
    SetCodigoTipo(CodigoTipo);
    Result := True;
end;

procedure TFrameDatosVehiculo.SetAnio(const Value: integer);
ResourceString
    MSG_CAPTION_VALIDAR_ANIO    = 'Validar A�o';
    MSG_ERROR_VALIDAR_ANIO      = 'El a�o especificado esta fuera del rango permitido';
begin
    if Value = Anio then Exit;
    if EsAnioCorrecto(Value) then txt_Anio.ValueInt := Value
    else MsgBoxBalloon( MSG_ERROR_VALIDAR_ANIO, MSG_CAPTION_VALIDAR_ANIO, MB_ICONSTOP, txt_anio );
end;

procedure TFrameDatosVehiculo.SetCodigoColor(const Value: integer);
begin
    if Value = Color then Exit;
    CargarColoresVehiculos(DMConnections.BaseCAC, cb_Color, Value);
end;

procedure TFrameDatosVehiculo.SetCodigoMarca(const Value: integer);
begin
    if Value = Marca then Exit;
    CargarMarcasVehiculos(DMConnections.BaseCAC, cb_marca, Value);
    //cb_Marca.OnChange(cb_Marca);
end;

procedure TFrameDatosVehiculo.SetCodigoTipo(const Value: integer);
begin
    if Value = CodigoTipo then Exit;
    CargarVehiculosTipos(DMConnections.BaseCAC, cb_TipoVehiculo, False, Value);
end;

procedure TFrameDatosVehiculo.SetDescripcionModelo(const Value: AnsiString);
begin
    txt_DescripcionModelo.Text := value;
end;

procedure TFrameDatosVehiculo.SetPatente(const Value: AnsiString);
begin
    //if Trim(Value) = Patente then Exit;
    txt_Patente.Text := Trim(Value);
end;

//procedure TFrameDatosVehiculo.SetTipoPatente(const Value: AnsiString);
//begin
//
//end;

procedure TFrameDatosVehiculo.txt_PatenteKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9', 'a'..'z', 'A'..'Z', #8]) then
        Key := #0;
end;

function TFrameDatosVehiculo.ValidarDatosVehiculo: boolean;
ResourceString
    CAPTION_VALIDAR_DATOS_VEHICULO      = 'Validar datos del Veh�culo';
    MSG_ERROR_VALIDAR_PATENTE           = 'Debe indicar patente.';
    MSG_ERROR_VALIDAR_FORMATO_PATENTE   = 'La patente especificada es incorrecta.';
    MSG_ERROR_VALIDAR_MARCA             = 'Debe indicar una marca.';
    MSG_ERROR_VALIDAR_ANIO              = 'El a�o es incorrecto. ' + #13#10 + 'Debe ser mayor a 1900 y menor al a�o siguiente al actual. ';
    MSG_ERROR_VALIDAR_TIPO              = 'Debe indicar un tipo de veh�culo.';
var
    PatenteValida: Boolean;
begin
    // Si piden con formato, agregar mcPatente, luego validar  [Trim(mcPatente.maskText) <> '' y con mcPatente.ValidateMask
    PatenteValida := true;
    if (Length(txt_Patente.Text) = 6) and
      (UpperCase(txt_Patente.text)[1] in ['A'..'Z']) and
      (UpperCase(txt_Patente.text)[2] in ['A'..'Z']) and
      (txt_Patente.text[3] in ['0'..'9']) and
      (txt_Patente.text[4] in ['0'..'9']) and
      (txt_Patente.text[5] in ['0'..'9']) and
      (txt_Patente.text[6] in ['0'..'9']) then begin
        VerificarFormatoPatenteChilena.Parameters.ParamByName('@Patente').value := txt_Patente.Text;
        VerificarFormatoPatenteChilena.ExecProc;
        PatenteValida := (VerificarFormatoPatenteChilena.parameters.paramByName('@Return_Value').value = 1);
    end;
    Result := ValidateControls(
          [txt_Patente, txt_Patente,
          cb_marca,  txt_anio],
          [Trim(txt_Patente.Text) <> '', PatenteValida,
          cb_marca.itemindex >= 0,  EsAnioCorrecto(txt_anio.ValueInt)],
          CAPTION_VALIDAR_DATOS_VEHICULO,
          [MSG_ERROR_VALIDAR_PATENTE, MSG_ERROR_VALIDAR_FORMATO_PATENTE,
          MSG_ERROR_VALIDAR_MARCA,MSG_ERROR_VALIDAR_ANIO]);
end;

procedure TFrameDatosVehiculo.cb_marcaChange(Sender: TObject);
begin
    if Assigned(EventoOnChangeMarca) then EventoOnChangeMarca(sender);
end;

end.
