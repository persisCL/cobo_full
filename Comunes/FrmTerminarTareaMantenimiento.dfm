object FormTerminarTareaMantenimiento: TFormTerminarTareaMantenimiento
  Left = 250
  Top = 160
  BorderStyle = bsDialog
  Caption = 'Terminar Tarea de Mantenimiento'
  ClientHeight = 355
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 28
    Top = 20
    Width = 89
    Height = 13
    Caption = 'Personal afectado:'
  end
  object btnEliminarLegajo: TSpeedButton
    Left = 396
    Top = 132
    Width = 23
    Height = 22
    Caption = '-'
    Enabled = False
    OnClick = btnEliminarLegajoClick
  end
  object btnAgregarLegajo: TSpeedButton
    Left = 396
    Top = 107
    Width = 23
    Height = 22
    Caption = '+'
    OnClick = btnAgregarLegajoClick
  end
  object Label2: TLabel
    Left = 28
    Top = 164
    Width = 91
    Height = 13
    Caption = 'Equipos afectados:'
  end
  object btnAgregarEquipo: TSpeedButton
    Left = 512
    Top = 251
    Width = 23
    Height = 22
    Caption = '+'
    OnClick = btnAgregarEquipoClick
  end
  object btnEliminarEquipo: TSpeedButton
    Left = 512
    Top = 276
    Width = 23
    Height = 22
    Caption = '-'
    Enabled = False
    OnClick = btnEliminarEquipoClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 314
    Width = 548
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      548
      41)
    object btnAceptar: TButton
      Left = 379
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 459
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object sgLegajosHoras: TStringGrid
    Left = 27
    Top = 40
    Width = 354
    Height = 113
    ColCount = 2
    DefaultRowHeight = 20
    FixedColor = 16444382
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    ScrollBars = ssVertical
    TabOrder = 1
    OnExit = sgLegajosHorasExit
    OnKeyPress = sgLegajosHorasKeyPress
    OnSelectCell = sgLegajosHorasSelectCell
    ColWidths = (
      260
      64)
    RowHeights = (
      20
      20)
  end
  object sgEquiposConsumo: TStringGrid
    Left = 27
    Top = 184
    Width = 470
    Height = 113
    ColCount = 3
    DefaultRowHeight = 20
    FixedColor = 16444382
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    ScrollBars = ssVertical
    TabOrder = 2
    OnExit = sgEquiposConsumoExit
    OnKeyPress = sgEquiposConsumoKeyPress
    OnSelectCell = sgEquiposConsumoSelectCell
    ColWidths = (
      260
      103
      92)
    RowHeights = (
      20
      20)
  end
  object btLegajos: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryLegajos
    OnProcess = btLegajosProcess
    OnSelect = btLegajosSelect
    Left = 448
    Top = 16
  end
  object qryLegajos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      'APELLIDO + '#39','#39'  +  NOMBRE As ApellidoNombre,'
      'Legajo'
      'FROM '
      'MaestroPersonal WITH (NOLOCK) '
      'WHERE'
      'Activo = 1')
    Left = 416
    Top = 16
  end
  object qryEquipos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'select * from Equipos WITH (NOLOCK) ')
    Left = 276
    Top = 252
  end
  object btEquipos: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryEquipos
    OnProcess = btEquiposProcess
    OnSelect = btEquiposSelect
    Left = 304
    Top = 252
  end
  object ActualizarTareaHorasEmpleado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTareaHorasEmpleado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarea'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Legajo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Horas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 440
    Top = 56
  end
  object ActualizarTareaEquiposConsumo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTareaEquiposConsumo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarea'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEquipo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConsumoKms'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConsumoHs'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 380
    Top = 248
  end
end
