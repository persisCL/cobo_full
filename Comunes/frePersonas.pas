unit frePersonas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DateEdit, StdCtrls, Buttons,PeaProcs, PeaTypes, RStrings,Util,
  DmConnection,BuscaClientes,Variants,UtilProc, DB,UtilDB, ADODB,ConstParametrosGenerales,
  Validate;

type
  TFramePersona = class(TFrame)
    lbl_Peroneria: TLabel;
    cb_Personeria: TComboBox;
    lblApellido: TLabel;
    lblNombre: TLabel;
    Label2: TLabel;
    Label10: TLabel;
    cbActividades: TComboBox;
    cbLugarNacimiento: TComboBox;
    txtNombre: TEdit;
    txtApellido: TEdit;
    btn_Buscar: TBitBtn;
    lblApellidoMaterno: TLabel;
    Label11: TLabel;
    lblSexo: TLabel;
    lblFechaNacCreacion: TLabel;
    txtFechaNacimiento: TDateEdit;
    cb_Sexo: TComboBox;
	cbSituacionIVA: TComboBox;
    txtApellidoMaterno: TEdit;
    txt_Documento: TEdit;
    lblRUT: TLabel;
    lblRazonSocial: TLabel;
    txtRazonSocial: TEdit;
    lblMail: TLabel;
    txt_email: TEdit;
    ObtenerDatosPersona: TADOStoredProc;
    procedure cb_PersoneriaChange(Sender: TObject);
    procedure btn_BuscarClick(Sender: TObject);
    procedure cbLugarNacimientoChange(Sender: TObject);
    procedure txt_DocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure txtApellidoExit(Sender: TObject);
    procedure txtRazonSocialExit(Sender: TObject);
    procedure txtNombreExit(Sender: TObject);
    procedure txtApellidoMaternoExit(Sender: TObject);
    procedure txt_DocumentoChange(Sender: TObject);
  private
	{ Private declarations }
    FMostrarPersoneria:AnsiString;
    FTipodeBusqueda:Integer;
    CodigoPersonaActual:Integer;
    FRUTValido: boolean;
    procedure Limpiar;
    procedure LimpiarSoloCampos;
    function GetPersoneria:AnsiString;
    function GetRut:String;
    function GetRazonSocial:String;
    function GetApellido:String;
    function GetApellidoMaterno:String;
    function GetNombre:String;
    function GetCodigoSituacionIVA:String;
    function GetCodigoPais:String;
    function GetSexo:String;
    function GetCodigoActividad:Integer;
    function GetEmail:String;
    function GetFechaNacimiento:TDateTime;
	function GetFechaCreacion:TDateTime;
    function GetApellidoContacto:String;
    function GetApellidoMaternoContacto:String;
    function GetNombreContacto:String;
    function GetSexoContacto:String;
    function GetCodigoPersona:Integer;

    procedure HabilitarCamposPersoneria(Personeria: AnsiString);
//    procedure SetObligatorioApellido(Valor:Boolean);
    function GetCodigoDocumento: String;
    function GetDescripPais:String;
    function GetFechaNacimientoOCreacion:TDateTime;
    procedure ValidadRUT;
  public
    { Public declarations }
    function CargarDatosPersona(CodigoPersona: integer;NumeroDocumento:String=''):Boolean;
    property Personeria:AnsiString read GetPersoneria;
    property RUT:String read GetRut;
    property RazonSocial:String read GetRazonSocial;
	property Apellido:String read GetApellido;
    property ApellidoMaterno:String read GetApellidoMaterno;
    property Nombre:String read GetNombre;
    property Sexo:String read GetSexo;
    property ApellidoContacto:String read GetApellidoContacto;
    property ApellidoMaternoContacto:String read GetApellidoMaternoContacto;
    property NombreContacto:String read GetNombreContacto;
    property SexoContacto:String read GetSexoContacto;
    property CodigoSituacionIVA:String read GetCodigoSituacionIVA;
    property CodigoPais:String read GetCodigoPais;
    property DescripPais:String read GetDescripPais;
    property CodigoActividad:Integer read GetCodigoActividad;
    property Email:String read GetEmail;
    property FechaNacimiento:TDateTime read GetFechaNacimiento;
    property FechaCreacion:TDateTime read GetFechaCreacion;
    property FechaNacimientoOCreacion:TdateTime read GetFechaNacimientoOCreacion;
    property CodigoPersona:Integer read GetCodigoPersona;
    property CodigoDocumento:String read GetCodigoDocumento;
    property RUTValido:boolean read FRUTValido default false;

    function Inicializar(MostrarPersoneria:AnsiString='';TipodeBusqueda:Integer=0):Boolean; overload;
    function Inicializar(Persona: TDatosPersonales):Boolean; overload;
    function Validar:Boolean;
  end;

implementation

{$R *.dfm}


{ TFramePersonas }

function TFramePersona.Inicializar(MostrarPersoneria:AnsiString='';TipodeBusqueda:Integer=0):Boolean;
begin
    FTipodeBusqueda:=TipodeBusqueda;
    FMostrarPersoneria:=MostrarPersoneria;
    cb_Personeria.Visible:=FMostrarPersoneria='';
    lbl_Peroneria.Visible:=cb_Personeria.Visible;

//    btn_Buscar.Visible:=cb_Personeria.Visible and (FTipodeBusqueda>=0);

    Limpiar;

    result:=True;
end;
procedure TFramePersona.HabilitarCamposPersoneria(Personeria: AnsiString);
begin
    if Personeria = PERSONERIA_JURIDICA then begin
            lblApellido.Caption         := LBL_PERSONA_APELLIDO_CONTACTO;
            lblNombre.Caption           := LBL_PERSONA_NOMBRE_CONTACTO;
            lblApellidoMaterno.Caption  := LBL_PERSONA_APELLIDO_MATERNO;
            lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_CREACION;

    end else begin
            lblApellido.Caption         := LBL_PERSONA_APELLIDO;
            lblNombre.Caption           := LBL_PERSONA_NOMBRE;
            lblApellidoMaterno.Caption  := LBL_PERSONA_APELLIDO_MATERNO;
			lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_NACIMIENTO;
    end;

    lblRazonSocial.Visible:=Personeria=PERSONERIA_JURIDICA;
    txtRazonSocial.Visible:=lblRazonSocial.Visible;
    lblMail.Visible:=false;//lblRazonSocial.Visible;
    txt_email.Visible:=lblMail.Visible;

end;

procedure TFramePersona.cb_PersoneriaChange(Sender: TObject);
begin
    HabilitarCamposPersoneria(self.Personeria);
end;

procedure TFramePersona.Limpiar;
begin
    CodigoPersonaActual:=-1;
//    if FMostrarPersoneria='' then CargarPersoneria(cb_Personeria,'',false,true)
//    else CargarPersoneria(cb_Personeria,FMostrarPersoneria,false,true);
	CargarPersoneria(cb_Personeria,PERSONERIA_FISICA);
    cb_Personeria.OnChange(cb_Personeria);

    CargarSexos(cb_Sexo,SEXO_MASCULINO);
    CargarTipoSItuacionIVA(DMConnections.BaseCAC,cbSituacionIVA);
    CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento,PAIS_CHILE);
    cbLugarNacimiento.OnChange(cbLugarNacimiento);
    CargarActividadesPersona(DMConnections.BaseCAC, cbActividades);
    txt_Documento.Clear;
    txtNombre.Clear;
    txtApellido.Clear;
    txtRazonSocial.Clear;
    txt_email.Clear;
    txtApellidoMaterno.Clear;
end;

procedure TFramePersona.btn_BuscarClick(Sender: TObject);
//var
//	f: TFormBuscaClientes;
begin
//	Application.CreateForm(TFormBuscaClientes , f);
//	if f.Inicializa('RUT',self.RUT, Trim(txtApellido.Text), Trim(txtApellidoMaterno.Text),Trim(txtNombre.Text),FTipodeBusqueda) then
//		if (f.ShowModal = mrok) and (MsgBox(MSG_QUESTION_BUSCAR_PERSONA, MSG_CAPTION_BUSCAR_PERSONA, MB_YESNO) = IDYES) then
//            CargarDatosPersona(f.Persona.CodigoPersona);
//
//	f.Release;
end;

function TFramePersona.GetPersoneria: AnsiString;
begin
    if trim(cb_Personeria.Text)<>'' then Result:=StrRight(trim(cb_Personeria.Text),1)
    else Result:='';
end;

function TFramePersona.GetApellido: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=''
	else Result:=Trim(txtApellido.Text);
end;

function TFramePersona.GetApellidoMaterno: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=''
    else Result:=Trim(txtApellidoMaterno.Text);
end;

function TFramePersona.GetCodigoActividad: Integer;
begin
    if trim(cbActividades.Text)<>'' then result:=strtoint(StrRight(cbActividades.Text,3))
    else result:=-1;
end;

function TFramePersona.GetCodigoPais: String;
begin
    if trim(cbLugarNacimiento.Text)<>'' then result:=Trim(StrRight(cbLugarNacimiento.Text,3))
    else result:='';
end;

function TFramePersona.GetCodigoSituacionIVA: String;
begin
    if trim(cbSituacionIVA.Text)<>'' then result:=Trim(StrRight(cbSituacionIVA.Text,3))
    else result:='';
end;

function TFramePersona.GetEmail: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=Trim(txt_email.Text)
    else Result:='';
end;

function TFramePersona.GetFechaNacimiento: TDateTime;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=nulldate
    else Result:=txtFechaNacimiento.Date;
end;

function TFramePersona.GetNombre: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=''
    else Result:=Trim(txtNombre.Text);
end;

function TFramePersona.GetRazonSocial: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=Trim(txtRazonSocial.Text)
    else Result:='';
end;

function TFramePersona.GetRut: String;
begin
    Result:=Trim(txt_Documento.Text);
end;

function TFramePersona.GetSexo: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=''
    else
    Result:=StrRight(trim(cb_Sexo.Text),1);
end;

function TFramePersona.GetApellidoContacto: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=Trim(txtApellido.Text)
    else Result:='';
end;

function TFramePersona.GetApellidoMaternoContacto: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=Trim(txtApellidoMaterno.Text)
    else Result:='';
end;

function TFramePersona.GetNombreContacto: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=Trim(txtNombre.Text)
    else Result:='';
end;

function TFramePersona.GetSexoContacto: String;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=StrRight(Trim(cb_Sexo.Text),1)
    else Result:='';
end;

function TFramePersona.GetFechaCreacion: TDateTime;
begin
    if self.Personeria=PERSONERIA_JURIDICA then Result:=txtFechaNacimiento.Date
    else Result:=nulldate;
end;

procedure TFramePersona.cbLugarNacimientoChange(Sender: TObject);
begin
//    SetObligatorioApellido(self.CodigoPais=PAIS_CHILE);
end;

//procedure TFramePersona.SetObligatorioApellido(Valor: Boolean);
//begin
//    if valor then begin
//        txtApellidoMaterno.Color:=$00FAEBDE;
//        lblApellidoMaterno.Font.Color:=clNavy;
//        lblApellidoMaterno.Font.Style:=[fsBold];
//    end else begin
//        txtApellidoMaterno.Color:=clWindow;
//        lblApellidoMaterno.Font.Color:=clWindowText;
//        lblApellidoMaterno.Font.Style:=[];
//    end;
//end;

function TFramePersona.CargarDatosPersona(CodigoPersona: integer;NumeroDocumento:String=''):Boolean;
begin
	result:=False;
	with ObtenerDatosPersona.Parameters do begin
    	if CodigoPersona<0 then ParamByName('@CodigoPersona').Value:=null
    	else ParamByName('@CodigoPersona').Value:=CodigoPersona;

        if trim(NumeroDocumento)<>'' then begin
        	ParamByName('@NumeroDocumento').Value:=NumeroDocumento;
        	ParamByName('@CodigoDocumento').Value:=CodigoDocumento;
        end else begin
        	ParamByName('@NumeroDocumento').Value:=null;
        	ParamByName('@CodigoDocumento').Value:=null;
		end;

    end;
	if not OpenTables([ObtenerDatosPersona]) then Exit;

	try
    	if ObtenerDatosPersona.RecordCount>0 then begin
            with ObtenerDatosPersona do begin
                //Cargar Datos Personales
				CargarPersoneria(cb_Personeria, Trim(FieldByName('Personeria').AsString),false,true);
                cb_Personeria.OnChange(cb_Personeria);
                txt_Documento.Text          := Trim(FieldByName('NumeroDocumento').AsString);
                txtNombre.Text			    := iif(self.Personeria=PERSONERIA_JURIDICA, Trim(FieldByName('NombreContactoComercial').AsString), Trim(FieldByName('Nombre').AsString));
                txtApellido.Text  		    := iif(self.Personeria=PERSONERIA_JURIDICA, Trim(FieldByName('ApellidoContactoComercial').AsString), Trim(FieldByName('Apellido').AsString));
                txtApellidoMaterno.Text  	:= iif(self.Personeria=PERSONERIA_JURIDICA, Trim(FieldByName('ApellidoMaternoContactoComercial').AsString), Trim(FieldByName('ApellidoMaterno').AsString));
                txtRazonSocial.Text         := iif(self.Personeria=PERSONERIA_JURIDICA, Trim(FieldByName('Apellido').AsString), '');

                CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento, FieldByName('LugarNacimiento').AsString);
                cbLugarNacimiento.OnChange(cbLugarNacimiento);
                txtFechaNacimiento.Date	    := FieldByName('FechaNacimiento').AsDateTime;

                CargarSexos(cb_Sexo, iif(self.Personeria=PERSONERIA_JURIDICA, Trim(FieldByName('SexoContactoComercial').AsString), Trim(FieldByName('Sexo').AsString)));
                CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Trim(FieldByName('CodigoSituacionIVA').AsString));

                CargarActividadesPersona(DMConnections.BaseCAC, cbActividades,  FieldByName('CodigoActividad').AsInteger);

                CodigoPersonaActual    := FieldByName('CodigoPersona').AsInteger;
                result:=True;
			end;
        end;
        ObtenerDatosPersona.close;
    except
        on E: Exception do begin
            ObtenerDatosPersona.Close;
            result:=False;
        end;
    end;
end;


function TFramePersona.GetCodigoPersona: Integer;
begin
    result:=CodigoPersonaActual;
end;

function TFramePersona.Validar: Boolean;
var
	EdadMinma:Integer;
begin
    if ObtenerParametroGeneral(DMConnections.BaseCAC,PS_EDAD_MINIMA,EdadMinma) and
    ValidateControls([cb_Personeria,txt_Documento,txt_Documento,txtRazonSocial,txtApellido,txtApellidoMaterno,txtNombre,cb_Sexo,txtFechaNacimiento],
                            [not(cb_Personeria.Visible) or (trim(cb_Personeria.Text)<>''),
                            trim(txt_Documento.Text)<>'',
                            (trim(txt_Documento.Text)<>'') and ((UpperCase(StrLeft(cbLugarNacimiento.Text,3))<>PAIS_CHILE) or
                            (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
                             StrLeft(txt_Documento.Text, (Length(txt_Documento.Text) -1))  +
                            ''',''' + txt_Documento.Text[Length(txt_Documento.Text)] + '''') <> '0')),
                            not(txtRazonSocial.Visible) or (trim(txtRazonSocial.Text)<>''),
                            trim(txtApellido.Text)<>'',
                            (txtApellidoMaterno.color=clWindow) or (trim(txtApellidoMaterno.Text)<>''),
                            trim(txtNombre.Text)<>'',
                            trim(cb_Sexo.Text)<>'',
                            ((self.Personeria=PERSONERIA_JURIDICA) or (txtFechaNacimiento.IsEmpty or FechaNacimientoValida(txtFechaNacimiento.Date,EdadMinma)))],
                            MSG_CAPTION_VALIDA,
                            [Format(MSG_VALIDAR_DEBE_LA,[FLD_PERSONERIA]),
                            Format(MSG_VALIDAR_DEBE_EL,[STR_RUT_RUN]),
							MSG_ERROR_DOCUMENTO,
                            Format(MSG_VALIDAR_DEBE_LA,[FLD_RAZON_SOCIAL]),
                            Format(MSG_VALIDAR_DEBE_EL,[FLD_APELLIDO]),
                            Format(MSG_VALIDAR_DEBE_EL,[FLD_APELLIDO_MATERNO]),
                            Format(MSG_VALIDAR_DEBE_EL,[FLD_NOMBRE]),
                            Format(MSG_VALIDAR_DEBE_EL,[FLD_SEXO]),
                            Format(MSG_VALIDAR_FECHA_NACIMIENTO,[EdadMinma])]
                            )
    then result:=true
    else result:=false;
end;

function TFramePersona.Inicializar(Persona: TDatosPersonales): Boolean;
begin
//    Inicializar('', TBP_PERSONAS);

    FTipodeBusqueda:=TBP_PERSONAS;
    FMostrarPersoneria:='';
    cb_Personeria.Visible:=True;
    lbl_Peroneria.Visible:=cb_Personeria.Visible;

    //Se utiliza cuando se reciben los datos de un contacto de una comunicacion
    //CargarPersoneria(cb_Personeria, Persona.Personeria,false,true);
    CargarPersoneria(cb_Personeria, Persona.Personeria,false,false);
    cb_Personeria.OnChange(cb_Personeria);
    txt_Documento.Text          := Persona.NumeroDocumento;
    txtNombre.Text			    := Persona.Nombre;
    txtApellido.Text  		    := Persona.Apellido;
    txtApellidoMaterno.Text  	:= Persona.ApellidoMaterno;
    CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento, Persona.LugarNacimiento);
    cbLugarNacimiento.OnChange(cbLugarNacimiento);
    txtFechaNacimiento.Date	    := Persona.FechaNacimiento;
    if Persona.Personeria = PERSONERIA_FISICA then
        CargarSexos(cb_Sexo, Persona.Sexo);
    //else if Persona.Personeria = PERSONERIA_JURIDICA then
    //    CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Persona.);
    //CargarActividadesPersona(DMConnections.BaseCAC, cbActividades,  FieldByName('CodigoActividad').AsInteger);
    result := true;
end;

function TFramePersona.GetCodigoDocumento: String;
begin
    result:=TIPO_DOCUMENTO_RUT;
end;

function TFramePersona.GetDescripPais: String;
begin
    result:=DescriPais(DMConnections.BaseCAC,self.CodigoPais);
end;

procedure TFramePersona.txt_DocumentoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9','K','k', #8]) then Key := #0;
end;

procedure TFramePersona.txtApellidoExit(Sender: TObject);
begin
    txtApellido.Text := PrimeraLetraMayuscula(txtApellido.Text);
end;

procedure TFramePersona.txtRazonSocialExit(Sender: TObject);
begin
    txtRazonSocial.Text :=  PrimeraLetraMayuscula(txtRazonSocial.Text);
end;

procedure TFramePersona.txtNombreExit(Sender: TObject);
begin
    txtNombre.Text := PrimeraLetraMayuscula(txtNombre.Text);
end;

procedure TFramePersona.txtApellidoMaternoExit(Sender: TObject);
begin
    txtApellidoMaterno.Text := PrimeraLetraMayuscula(txtApellidoMaterno.Text);
end;

function TFramePersona.GetFechaNacimientoOCreacion: TDateTime;
begin
    result:=iif(self.Personeria=PERSONERIA_JURIDICA,self.FechaCreacion,self.FechaNacimiento);
end;

procedure TFramePersona.ValidadRUT;
var
	aux:Boolean;
    AuxString:String;
begin
	AuxString:=txt_Documento.Text;
    //valido estos datos antes, asi no llamo al SP
    if (length(Trim(txt_Documento.Text)) < 8)  or (length(Trim(txt_Documento.Text)) >9) then begin
		if not(cb_Personeria.Enabled) then begin
        	EnableControlsInContainer(self,true);
            cb_Personeria.Enabled:=False;
            LimpiarSoloCampos;
        end;
        txt_Documento.setfocus;
        txt_Documento.SelStart:=length(txt_Documento.Text);
        txt_Documento.SelLength:=1;
        FRUTValido := False;
    	Exit;
    end;

    if (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
          StrLeft(txt_Documento.Text, (Length(txt_Documento.Text) -1)) +
          ''',''' +
          txt_Documento.Text[Length(txt_Documento.Text)] + '''') = '0') then begin
		if not(cb_Personeria.Enabled) then begin
        	EnableControlsInContainer(self,true);
            cb_Personeria.Enabled:=False;
            LimpiarSoloCampos;
        end;
        txt_Documento.setfocus;
        txt_Documento.SelStart:=length(txt_Documento.Text);
        txt_Documento.SelLength:=1;
        FRUTValido := False;
    	Exit;
	end;

    FRUTValido := True;
	aux:=CargarDatosPersona(-1,txt_Documento.Text);

    EnableControlsInContainer(self,not(aux));

    if not(aux) then LimpiarSoloCampos
    else begin
	    cb_Sexo.Enabled:=trim(cb_Sexo.Text)='';
    	lblSexo.Enabled:=cb_Sexo.Enabled;
    end;

    lblRUT.Enabled:=True;
    txt_Documento.Enabled:=True;
	txt_Documento.setfocus;
    txt_Documento.SelStart:=length(txt_Documento.Text);
    txt_Documento.SelLength:=1;
    cb_Personeria.Enabled:=False;
end;

procedure TFramePersona.txt_DocumentoChange(Sender: TObject);
begin
	if self.enabled then ValidadRUT;
end;

procedure TFramePersona.LimpiarSoloCampos;
begin
	CargarPersoneria(cb_Personeria,PERSONERIA_FISICA);
    CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento,PAIS_CHILE);
    CargarSexos(cb_Sexo,SEXO_MASCULINO);
    txtNombre.Clear;
    txtApellido.Clear;
    txtRazonSocial.Clear;
    txt_email.Clear;
    txtApellidoMaterno.Clear;
end;

end.
