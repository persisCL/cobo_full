object FormBuscaCalle: TFormBuscaCalle
  Left = 121
  Top = 206
  BorderStyle = bsDialog
  Caption = 'Buscador de Calle'
  ClientHeight = 440
  ClientWidth = 741
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 720
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 741
    Height = 57
    Align = alTop
    Caption = 'Datos de ubicaci'#243'n pol'#237'tica'
    TabOrder = 0
    DesignSize = (
      741
      57)
    object Label1: TLabel
      Left = 14
      Top = 29
      Width = 37
      Height = 13
      Caption = 'Region:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbl_Comuna: TLabel
      Left = 350
      Top = 29
      Width = 42
      Height = 13
      Caption = 'Comuna:'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object cbRegiones: TComboBox
      Left = 60
      Top = 22
      Width = 282
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbRegionesChange
      OnEnter = cbRegionesEnter
    end
    object cbComunas: TComboBox
      Left = 400
      Top = 22
      Width = 330
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbComunasChange
      OnEnter = cbComunasEnter
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 57
    Width = 741
    Height = 56
    Align = alTop
    Caption = 'Datos de la calle'
    TabOrder = 1
    object Label4: TLabel
      Left = 11
      Top = 32
      Width = 40
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txt_nombre: TEdit
      Left = 60
      Top = 24
      Width = 666
      Height = 21
      TabOrder = 0
      OnChange = txt_nombreChange
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 113
    Width = 741
    Height = 286
    Align = alClient
    Caption = 'Resultados de la busqueda'
    TabOrder = 2
    object dbl_Calles: TDBListEx
      Left = 2
      Top = 15
      Width = 737
      Height = 269
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 250
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 250
          Header.Caption = 'Alias'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Alias'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 200
          Header.Caption = 'Comuna'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Comuna'
        end>
      DataSource = DSCalle
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbl_CallesDblClick
    end
  end
  object pnl_botones: TPanel
    Left = 0
    Top = 399
    Width = 741
    Height = 41
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      741
      41)
    object lbl_No_Encontro: TLabel
      Left = 16
      Top = 11
      Width = 305
      Height = 16
      Caption = 'NO SE ENCONTRARON POSIBLES CALLES'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btn_Salir: TDPSButton
      Left = 653
      Top = 8
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btn_SalirClick
    end
    object btn_Aceptar: TDPSButton
      Left = 565
      Top = 8
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
  end
  object DSCalle: TDataSource
    DataSet = BuscarCalles
    Left = 528
    Top = 224
  end
  object BuscarCalles: TADOStoredProc
    Connection = DMConnections.BaseCAC
    AfterOpen = CambioEstadoTabla
    ProcedureName = 'BuscarCalles'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 496
    Top = 224
  end
end
