unit FrmHacerEncuesta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DB, ADODB, DMConnection,UtilDB,PeaTypes,
  Validate, DateEdit, DmiCtrls,Util,PeaProcs,UtilProc;

type
  TRegistro = record
        CodigoPregunta:Integer;
        Respuesta_Texto:String;
        CodigoOpcionesPregunta:Integer;
        Respondido:Boolean;
  end;
  TFormHacerEncuesta = class(TForm)
    pnl_BotonesGeneral: TPanel;
    ObtenerPreguntasEncuesta: TADOStoredProc;
    ObtenerOpcionesPreguntas: TADOStoredProc;
    pnl_Preguntas: TScrollBox;
    GrabarResultadoEncuesta: TADOStoredProc;
    btn_Terminar: TButton;
    procedure btn_TerminarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Respuestas:array of TRegistro;
    FSolicitudContacto:Integer;
    function PonerValoresComunes(x,y:Integer;Propietario:TWinControl;Componente:TWinControl;PosEnRegistro:Integer):TWinControl;
    function CrearLabel(x,y:Integer;Texto:String;Propietario:TWinControl):TControl;
    function CrearPanel(x,y:Integer;Propietario:TWinControl):TWinControl;
    function CrearComponente(x,y:Integer;Propietario:TWinControl;TipoDato:Integer;TipoPregunta:Integer;PosEnRegistro:Integer;Texto:String='';Combo:TWinControl=nil):TWinControl;
    procedure EventoOnChange(Sender: TObject);
  public
    { Public declarations }
    function inicializar(CodigoFuenteSolicitud,CodigoSolicitudContacto:integer):Boolean;
  end;

var
  FormHacerEncuesta: TFormHacerEncuesta;

resourcestring
    MSG_CAPTION_ENCUESTA = 'Realizacion de Encuesta.';
    MSG_ERROR_GRABAR = 'No se puedo grabar los resultados de la encuesta.';
    MSG_ERROR_INICIALIZACION_ENCUESTA = 'No se puede Mostrar el formulario de Encuesta.';

implementation

{$R *.dfm}

function TFormHacerEncuesta.inicializar(CodigoFuenteSolicitud,CodigoSolicitudContacto:integer):Boolean;
var
    i,ultima_pos:Integer;
    aux_items,aux_panel:TWinControl;
    aux_label:TControl;
    Entro:Boolean;
begin
    try
        ultima_pos:=MARGEN_SUPERIOR;
        FSolicitudContacto:=CodigoSolicitudContacto;


        ObtenerPreguntasEncuesta.parameters.ParamByName('@CodigoFuenteSolicitud').Value:=CodigoFuenteSolicitud;
        ObtenerPreguntasEncuesta.Open;

        with ObtenerPreguntasEncuesta do begin
            first;
            while not(eof) do begin
                // pregunta
                aux_label:=CrearLabel(MARGEN_IZQUIERDO,ultima_pos,FieldByName('Descripcion').AsString,pnl_Preguntas);
                // Contenedor de la respuesta
                aux_panel:=CrearPanel(MARGEN_IZQUIERDO,+aux_label.Height+aux_label.Top+ESPACIO_ENTRE_RENGLON,pnl_Preguntas);

                // abro tabla de opciones
                ObtenerOpcionesPreguntas.close;
                ObtenerOpcionesPreguntas.Parameters.ParamByName('@CodigoPregunta').Value:=FieldByName('CodigoPregunta').Value;
                ObtenerOpcionesPreguntas.open;

                Entro:=false;
                i:=ESPACIO_ENTRE_ITEM;
                ObtenerOpcionesPreguntas.First;
                aux_items:=nil;

                while not(entro) or not(ObtenerOpcionesPreguntas.Eof) do begin
                    entro:=true;
                    // pongo sus items

                    SetLength(respuestas,length(respuestas)+1);
                    with Respuestas[length(respuestas)-1] do begin // inicializo el registro de resultado para cada posible respuesta
                        CodigoPregunta:=FieldByName('CodigoPregunta').AsInteger;
                        Respuesta_Texto:='';
                        CodigoOpcionesPregunta:=iif(ObtenerOpcionesPreguntas.active,ObtenerOpcionesPreguntas.FieldByName('CodigoOpcionesPregunta').AsInteger,-1);
                    end;

                    aux_items:=CrearComponente(MARGEN_IZQUIERDO,i,aux_panel,FieldByName('CodigoTipoDato').AsInteger,FieldByName('CodigoTipoPregunta').AsInteger,length(respuestas)-1,iif(ObtenerOpcionesPreguntas.active,ObtenerOpcionesPreguntas.FieldByName('Descripcion').AsString,''),aux_items);
                    i:=aux_items.Top+aux_items.Height+ESPACIO_ENTRE_ITEM;

                    if ObtenerOpcionesPreguntas.Active then ObtenerOpcionesPreguntas.Next;
                end;
                aux_panel.Height:=i-ESPACIO_ENTRE_ITEM+1;

                ultima_pos:=aux_panel.Height+aux_panel.top+ESPACIO_ENTRE_PREGUNTA;
                next;
            end;
        end;
        result:=ObtenerPreguntasEncuesta.RecordCount>0;
        ObtenerOpcionesPreguntas.close;
        ObtenerPreguntasEncuesta.close;
    except
        On E: Exception do begin
            result:=false;
            MsgBoxErr(MSG_ERROR_INICIALIZACION_ENCUESTA, E.message, MSG_CAPTION_ENCUESTA , MB_ICONSTOP);
        end;
    end;

end;

procedure TFormHacerEncuesta.btn_TerminarClick(Sender: TObject);
begin
    close;
end;

function TFormHacerEncuesta.CrearLabel(x,y:Integer;Texto:String;Propietario:TWinControl):TControl;
var
    aux:TLabel;
begin
    aux:=TLabel.Create(Propietario);
    with aux do begin
        Parent:=Propietario;
        Left:=x;
        Top:=y;
        Caption:=Texto;
        font.Size:=TAMANO_TITULO;
        font.Name:=LETRA_TITULO;
    end;
    result:=aux;
end;

function TFormHacerEncuesta.CrearPanel(x,y:Integer;Propietario:TWinControl):TWinControl;
var
    aux:TPanel;
begin
    aux:=TPanel.Create(Propietario);
    with aux do begin
        caption:='';
        BevelOuter:=bvNone;
        Width:=Propietario.Width-(x*2);
    end;
    result:=PonerValoresComunes(x,y,Propietario,aux,-1);
end;

function TFormHacerEncuesta.CrearComponente(x,y:Integer;Propietario:TWinControl;TipoDato:Integer;TipoPregunta:Integer;PosEnRegistro:Integer;Texto:String='';Combo:TWinControl=nil):TWinControl;
var
    aux_Text:TEdit;
    aux_Check:TCheckBox;
    aux_Combo:TComboBox;
    aux_Fecha:TDateEdit;
    aux_Numerico:TNumericEdit;
begin
    case TipoDato of
        CONST_TIPO_DATO_TEXTO: begin
                    aux_text:=TEdit.Create(Propietario);
                    aux_text.MaxLength:=120;
                    aux_text.Width:=Propietario.Width-(x*2);
                    aux_Text.OnChange:=EventoOnChange;
                    Result:=PonerValoresComunes(x,y,Propietario,aux_text,PosEnRegistro);
                end;
        CONST_TIPO_DATO_FECHA: begin
                    aux_Fecha:=TDateEdit.Create(Propietario);
                    aux_Fecha.OnChange:=EventoOnChange;
                    Result:=PonerValoresComunes(x,y,Propietario,aux_Fecha,PosEnRegistro);
                end;

        CONST_TIPO_DATO_NUMERICO: begin
                    aux_Numerico:=TNumericEdit.Create(Propietario);
                    aux_Numerico.OnChange:=EventoOnChange;
                    Result:=PonerValoresComunes(x,y,Propietario,aux_Numerico,PosEnRegistro);
                end;

        CONST_TIPO_DATO_BINARIO: begin
                        if TipoPregunta=CONST_PREGUNTA_MULTIPLE then begin
                            aux_Check:=TCheckBox.Create(Propietario);
                            aux_Check.Caption:=Texto;
                            aux_Check.Width:=Propietario.Width-(x*2);
                            aux_Check.OnClick:=EventoOnChange;
                            result:=PonerValoresComunes(x,y,Propietario,aux_Check,PosEnRegistro);
                        end else begin
                            if Combo=nil then begin
                                aux_Combo:=TComboBox.Create(Propietario);
                                PonerValoresComunes(x,y,Propietario,aux_Combo,PosEnRegistro);
                                aux_Combo.Style:=csDropDownList;
                                aux_Combo.OnChange:=EventoOnChange;
                                aux_Combo.Width:=aux_Combo.Width*2;
                                combo:=aux_Combo;
                            end;
                            TComboBox(Combo).Items.Add(Texto);
                            result:=Combo;
                        end;
                end;
        else result:=nil;
    end;

end;

function TFormHacerEncuesta.PonerValoresComunes(x,y:Integer;Propietario:TWinControl;Componente:TWinControl;PosEnRegistro:Integer):TWinControl;
begin
    result:=Componente;
    with Result do begin
        Left:=x;
        Top:=y;
        Parent:=Propietario;
        tag:=PosEnRegistro;
    end;
end;

procedure TFormHacerEncuesta.EventoOnChange(Sender: TObject);
var
    i:integer;
begin
    with Respuestas[TComponent(sender).tag] do begin
        if (sender.ClassType=TEdit) or (sender.ClassType=TNumericEdit) or (sender.ClassType=TDateEdit) then begin
            Respuesta_Texto:=trim(TCustomEdit(sender).Text);
            Respondido:=(Respuesta_Texto<>'') and (Respuesta_Texto<>'/  /');
        end;

        if (sender.ClassType=TCheckBox) then Respondido:=TCheckBox(sender).Checked;

        if (sender.ClassType=TComboBox) then begin
            for i:=0 to length(Respuestas)-1 do begin
                if Respuestas[i].CodigoPregunta = CodigoPregunta then begin
                    Respuestas[i].Respondido:=false;
                end;
            end;
            Respuestas[TComponent(sender).tag + TComboBox(sender).itemindex].Respondido:=true;
        end;
    end;
end;
procedure TFormHacerEncuesta.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
    CodigoCuestionario,i:Integer;
begin
    // gravo los resultados
    try
        DMConnections.BaseCAC.BeginTrans;
        CodigoCuestionario:=-1;
        for i:=0 to length(Respuestas)-1 do begin
            with GrabarResultadoEncuesta,Respuestas[i] do begin
                close;
                Parameters.ParamByName('@CodigoPregunta').Value:=CodigoPregunta;
                Parameters.ParamByName('@CodigoSolicitudContacto').Value:=FSolicitudContacto;
                Parameters.ParamByName('@CodigoCuestionario').Value:=CodigoCuestionario;

                if Respondido then begin
                     Parameters.ParamByName('@Respuesta').Value:=Respuesta_Texto;
                     Parameters.ParamByName('@CodigoOpcionesPregunta').Value:=CodigoOpcionesPregunta;
                end else begin
                    Parameters.ParamByName('@Respuesta').Value:='';
                    Parameters.ParamByName('@CodigoOpcionesPregunta').Value:=-1;
                end;

                ExecProc;

                CodigoCuestionario:=Parameters.ParamByName('@CodigoCuestionario').Value;

            end;
        end;
        DMConnections.BaseCAC.CommitTrans;
    except
        On E: Exception  do begin
            DMConnections.BaseCAC.RollbackTrans;
            MsgBoxErr(MSG_ERROR_GRABAR, E.message, MSG_CAPTION_ENCUESTA, MB_ICONSTOP);
        end;
    end;
end;


end.
