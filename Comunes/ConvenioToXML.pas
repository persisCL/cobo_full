{********************************** File Header ********************************
File Name : ConvenioToXML.pas
Author : gcasais
Date Created: 26/10/2006
Language : ES-AR
Description :
Revision : 1
    Author : vpaszkowicz
    Date : 29/11/2006
    Description : Correcciones.
    El RUT lo restrinjo a la cantidad de caracteres que van.
    Corrijo el DV ya que estaba harcodedado en 1.
    Le coloco la constante Domicilio facturacion al domicilio de facturaci�n.
    Coloco el Nombre como Raz�n Social si la entidad es una empresa.
    Le quito los espacios en blanco a las patentes.

Firma       : SS_1330_NDR_20150702
Descripcion : Los arhcivos Entrada_1_*.* son de la forma Entrada_X_*.* donde X = ConcesionariaNativa
*******************************************************************************}
unit ConvenioToXML;

interface

uses Windows, SysUtils, Convenios, ModeloEntrada, XMLIntf, FreDomicilio, Util, DBClient,
    DateUtils, Dialogs, ConstParametrosGenerales, UtilProc, DMConnection, ADODB,
    XMLCAC, Variants;

const
    CONST_ST5_DOMICILIO_FACTURACION = 1;
    CONST_ST5_DOMICILIO_PPAL = 4;

type
    TVehiculoRNUT = record
        Patente: string;
        DV: string;
        Categoria: Integer;
        Marca: string;
        Modelo: string;
        Anio: Integer;
    end;

    TTAGRNUT = record
        Serie: String;
        Context: Integer;
        Contract: Integer;
        Caturb: Integer;
        CatInTurb: Integer;
        Fabricante: Integer;
    end;

function GenerarXMLConvenio(const Convenio: TDatosConvenio; Domicilio: TFrameDomicilio): Boolean;
function CompletarDatosVehiculo(const CodigoVehiculo: Integer; out Vehiculo: TVehiculoRNUT): Boolean;
function CompletarDatosTag(const Codigoconvenio, CodigoVehiculo: Integer; out Tag: TTAGRNUT):Boolean;
function ObtenerNombreXML: AnsiString;
implementation

uses PeaProcs;

function ObtenerNombreXML: AnsiString;
resourcestring
    MSG_ERROR_GENERAR_NOMBRE_XML = 'Error al generar el nombre para el archivo XML.';
var
    spArchivoNro: TADOStoredProc;
    ArchivoNro: Integer;
    FechaHora: AnsiString;
begin
    Result := EmptyStr;
    try
        spArchivoNro := TADOStoredProc.Create(Nil);
        try
            spArchivoNro.Connection := DmConnections.BaseCAC;
            spArchivoNro.ProcedureName := 'Convenio_NumeracionXml';
            spArchivoNro.Parameters.Refresh;
            spArchivoNro.Parameters.ParamByName('@Numeracion').Value := Null;
            spArchivoNro.ExecProc;
            ArchivoNro := spArchivoNro.Parameters.ParamByName('@Numeracion').Value;
            FechaHora := DateTimeToST5DateTime(Now, 0);
            //Result := 'entrada_1_'+ IntToStr(ArchivoNro) + '_' + FechaHora + '.xml';
            Result := 'entrada_'+ TRIM(IntToStr(ObtenerCodigoConcesionariaNativa())) +'_'+ IntToStr(ArchivoNro) + '_' + FechaHora + '.xml';     //SS_1330_NDR_20150702
        except
            on e: Exception do begin
                raise Exception(MSG_ERROR_GENERAR_NOMBRE_XML + CRLF + e.Message);
            end;
        end;

    finally
        FreeAndNil(spArchivoNro);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: GenerarXMLConvenio
  Rev 1:    vpaszkowicz
  Date:      19-Dic-2006
  Arguments: const Convenio: TDatosConvenio; Domicilio: TFrameDomicilio
  Result:    Boolean
  Description: Cambio el N�mero de Convenio por el CodigoConvenioExterno en
                la generaci�n del XML
-----------------------------------------------------------------------------}

function GenerarXMLConvenio(const Convenio: TDatosConvenio; Domicilio: TFrameDomicilio): Boolean;
var
    PathNovedadXML: AnsiString;

    function GenerarBaja(const Convenio: TDatosConvenio):Boolean;
    var
        Archivo: IXMLRNUTType;
        Contrato: IXMLContratoType;
        Save: TSaveDialog;
        NombreXML: String;
    const
        //COSTANERA_NORTE = 1;                                                  //SS_1147_MCA_20140408
        ACCION_BAJA = 3;
        ESTADO_BAJA = 3;
    begin
        Result := False;
        //Creamos la interfaz COM+ a un archivo del tipo entrada
        Archivo := NewRNUT;
        // Detallecitos de formato
        Archivo.OwnerDocument.Encoding := 'UTF-8';
        //Le agregamos un �nico contrato a este archivo, o sea, el contrato que nos pasaron.
        Contrato := Archivo.Contrato.Add;
        //Completamos las etiquetas del contrato
        //Contrato.Concesionaria := COSTANERA_NORTE;							//SS_1147_MCA_20140408
        Contrato.Concesionaria := ObtenerCodigoConcesionariaNativa;				//SS_1147_MCA_20140408
        Contrato.Accion.NodeValue := ACCION_BAJA;
        //Contrato.Codigo := Convenio.NumeroConvenio;
        Contrato.Codigo := Convenio.CodigoConvenioExterno;
        Contrato.Estado := ESTADO_BAJA;
        Contrato.Accion.FechaAccion := DateTimeToST5DateTime(Convenio.FechaBajaRNUT, 1);
        // Nos resta obtener el path en d�nde se dejan los XML
        // y un nombre de acuerdo a la numeracion del momento
        Save := TSaveDialog.Create(nil);
        Save.InitialDir := PathNovedadXML;
        // Obtener el nombre para el archivo XML.
        NombreXML := EmptyStr;
        NombreXML := ObtenerNombreXML;
        if NombreXML = EmptyStr then begin
            raise Exception.Create('Error');
            Exit;
        end;
        Save.FileName := NombreXML;
        try
            try
                if Save.Execute then begin
                    Archivo.OwnerDocument.SaveToFile(Save.FileName);
                    Result := True;
                end;
            except
                Exit;
            end;
        finally
            FreeAndNil(Save);
            Archivo := nil;
        end;
    end;

    {******************************** Function Header ******************************
    Function Name: GenerarAlta
    Author :
    Date Created :
    Description :
    Parameters : const Convenio: TDatosConvenio
    Return Value : Boolean
    Revision :
        Author : vpaszkowicz
        Date : 28/11/2006
        Description : Modifico las funciones para obtener el RUT y DV de un repre-
        sentante legal.
        Cambio el domicilio del contrato como domicilio de facturacion, o sea tiene
        el tipo 1 en lugar del 4.
        Adem�s cambio el Giro como Giro de la entidad y no como nombre de la empresa,
        Coloco el apellido paterno como campo apellidoP que tambi�n representa el
        la raz�n social.
        Revision :
            Author : vpaszkowicz
            Date : 19/12/2006
            Description : Reemplazo el NumeroConvenio por el CodigoConvenioExterno.
            Agrego fantasia como duplicado de la raz�n social.
            Comento el domicilio ppal hasta que deseen informarlo.
            Agrego el domicilio de facturaci�n.
            Cambio el Instal del Tag por la fecha de creacion de la cuenta.
    *******************************************************************************}
    function GenerarAlta(const Convenio: TDatosConvenio):Boolean;
    var
        Archivo: IXMLRNUTType;
        Contrato: IXMLContratoType;
        Entidad, Representante : IXMLEntidadType;
        Contacto : IXMLContactoType;
        Direccion: IXMLDireccionType;
        Vehiculo : IXMLVehiculoType;
        TAG : IXMLTAGType;
        Save: TSaveDialog;
        NombreXML: AnsiString;
        Vehiculos, CodigoConvenio, CodigoVehiculo : Integer;
        DatosVehiculo: TVehiculoRNUT;
        DatosTag: TTAGRNUT;
        unNumeroDocumento: string;
        DescripcionDpto: string;
    const
        //COSTANERA_NORTE = 1;                                                  //SS_1147_MCA_20140408
        ACCION_ALTA = 1;
        ESTADO_ALTA = 1;
    begin
        Result := False;
        //Creamos la interfaz COM+ a un archivo del tipo entrada
        Archivo := NewRNUT;
        // Detallecitos de formato
        Archivo.OwnerDocument.Encoding := 'UTF-8';
        //Le agregamos un �nico contrato a este archivo, o sea, el contrato que nos pasaron.
        Contrato := Archivo.Contrato.Add;
        //Completamos las etiquetas del contrato
        //Contrato.Concesionaria := COSTANERA_NORTE;							//SS_1147_MCA_20140408
        Contrato.Concesionaria := ObtenerCodigoConcesionariaNativa;				//SS_1147_MCA_20140408
        Contrato.Accion.NodeValue := ACCION_ALTA;
        //Contrato.Codigo := Convenio.NumeroConvenio;
        Contrato.Codigo := Convenio.CodigoConvenioExterno;
        Contrato.Estado := ESTADO_ALTA;
        //Ahora la parte de entidades de este contrato
        Entidad := Contrato.Entidad.Add;
        Entidad.Accion := ACCION_ALTA;
        // Obtener todos los caracteres menos el �ltimo que es el d�gito verificador.
        Entidad.RUT := Copy(Convenio.Cliente.Datos.NumeroDocumento, 1, Length(Convenio.Cliente.Datos.NumeroDocumento) - 1);
        // Obtener el �ltimo caracter, pues es el d�gito verificador.
        Entidad.DV := Copy(Convenio.Cliente.Datos.NumeroDocumento, Length(Convenio.Cliente.Datos.NumeroDocumento), 1);
        if Trim(Convenio.Cliente.Datos.Giro) <> '' then
            Entidad.Giro := Convenio.Cliente.Datos.Giro;
        case  Convenio.Cliente.Datos.Personeria of
            'F':
                begin
                    Entidad.Tipo := 1;
                    Entidad.Nombre := Convenio.Cliente.Datos.Nombre;
                    Entidad.Apellidop := Convenio.Cliente.Datos.Apellido;
                    Entidad.Apellidom := Convenio.Cliente.Datos.ApellidoMaterno;
                end;
            'J':
                begin
                    Entidad.Tipo := 2;
                    Entidad.Nombre := Convenio.Cliente.Datos.RazonSocial;
                    //Duplico la raz�n social para que el campo fantasa este completo
                    Entidad.Fantasia := Convenio.Cliente.Datos.RazonSocial;
                end;
        end;
        // Esta entidad tiene el rol cliente porque es la principal
        Entidad.Rol := 1;
        // Continuamos con la informaci�n de contacto de esta entidad principal
        // Empezamos agregando el tel�fono principal de contacto si tiene
        if Convenio.Cliente.Telefonos.CantidadMediosComunicacion > 0 then begin
            Contacto := Entidad.Contacto.Add;
            Contacto.Accion := ACCION_ALTA;
            Contacto.Dato := Convenio.Cliente.Telefonos.MediosComunicacion[0].Valor;
            Contacto.Tipo := 7;
        end;
        // Ahora el e-mail si tiene
        if Convenio.Cliente.EMail.CantidadMediosComunicacion > 0  then begin
            Contacto := Entidad.Contacto.Add;
            Contacto.Accion := ACCION_ALTA;
            Contacto.Dato := Convenio.Cliente.EMail.MediosComunicacion[0].Valor;
            Contacto.Tipo := 4;
        end;
        // Ahora tenemos que determinar si es necesario agregar otra entidad
        // con el Rol Representante Legal y el tipo correspondiente
        if (Convenio.Cliente is TPersonaJuridica) then begin
            //Efectivamente tenemos que agregarle una entidad para el RL
            Representante := Contrato.Entidad.Add;
            Representante.Accion := ACCION_ALTA;
            unNumeroDocumento := (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.NumeroDocumento;
            //Representante.RUT := (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.NumeroDocumento;
            Representante.RUT := Copy(unNumeroDocumento, 1, Length(unNumeroDocumento) - 1);
            //Representante.DV := '1';
            Representante.DV := Copy(unNumeroDocumento, Length(unNumeroDocumento), 1);
            if Trim((Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.Giro) <> '' then
                Representante.Giro := (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.Giro;
            case (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.Personeria of
                'F':
                    begin
                        Representante.Tipo := 1;
                        Representante.Nombre := (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.Nombre;
                        Representante.Apellidop := (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.Apellido;
                    end;
                'J':
                    begin
                        Representante.Tipo := 2;
                        //Representante.Giro := (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.RazonSocial;
                        Representante.Nombre := (Convenio.Cliente as TPersonaJuridica).RepresentantesLegales.Representantes[0].Datos.RazonSocial;
                    end;
            end;
            //Esta entidad va a ser siempre representante legal
            Representante.Rol := 2;
        end;
        // Continuamos con el agregado del domicilio principal del contrato
        //Por el momento lo comento hasta que decidan informarlas.
        {Direccion := Contrato.Direccion.Add;
        Direccion.Accion := ACCION_ALTA;
        Direccion.Codigo := IntToStr(Domicilio.CodigoDomicilio);}
        //Direccion.Tipo := 4;
        {Direccion.Tipo := CONST_ST5_DOMICILIO_PPAL;
        Direccion.Calle := Domicilio.DescripcionCalle;
        Direccion.Comuna := Domicilio.DescriComuna;
        Direccion.Altura := Domicilio.txt_numeroCalle.Text;
        if Domicilio.txt_Depto.Text <> EmptyStr then Direccion.Depto := Domicilio.txt_Depto.Text;
        if Domicilio.Detalle <> EmptyStr then Direccion.Comentario := Domicilio.Detalle;
        if Domicilio.txt_CodigoPostal.Text <> EmptyStr then Direccion.Zip := Domicilio.txt_CodigoPostal.Text;}
        // Por el momento solo informamos la direcci�n de facturaci�n.
        //--
        Direccion := Contrato.Direccion.Add;
        Direccion.Accion := ACCION_ALTA;
        Direccion.Codigo := IntToStr(Convenio.CodigoDomicilioFacturacion);
        Direccion.Tipo := CONST_ST5_DOMICILIO_FACTURACION;
        Direccion.Calle := Convenio.Cliente.Domicilios.DomicilioFacturacion.Descripcion;
        Direccion.Comuna := ArmarComuna(DmConnections.BaseCAC, inttostr(Convenio.CodigoDomicilioFacturacion));
        Direccion.Altura := COnvenio.Cliente.Domicilios.DomicilioFacturacion.NumeroCalleSTR;
        if Convenio.Cliente.Domicilios.DomicilioFacturacion.Piso <> 0 then
            DescripcionDpto :=  inttostr(Convenio.Cliente.Domicilios.DomicilioFacturacion.Piso);
        if (DescripcionDpto <> EmptyStr) and (Convenio.Cliente.Domicilios.DomicilioFacturacion.Dpto <> EmptyStr) then
            DescripcionDpto := DescripcionDpto  + Convenio.Cliente.Domicilios.DomicilioFacturacion.Dpto
        else if (Convenio.Cliente.Domicilios.DomicilioFacturacion.Dpto <> EmptyStr) then
             DescripcionDpto := Convenio.Cliente.Domicilios.DomicilioFacturacion.Dpto;
        if DescripcionDpto <> EmptyStr then
            Direccion.Depto := DescripcionDpto;
        if Convenio.Cliente.Domicilios.DomicilioFacturacion.Detalle <> EmptyStr then
            Direccion.Comentario := Convenio.Cliente.Domicilios.DomicilioFacturacion.Detalle;
        if Convenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal <> EmptyStr then Direccion.Zip := Convenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal;
        // Agregamos los veh�culos y sus Tags de este convenio
        // Obtenemos una vez el codigo de convenio que estamos armando
        CodigoConvenio := Convenio.CodigoConvenio;
        for Vehiculos := 0 to Convenio.Cuentas.CantidadVehiculos - 1 do begin
            if Convenio.Cuentas.Vehiculos[Vehiculos].Cuenta.FechaBajaCuenta = NullDate then begin
                // Obtenemos el Cod�go de Vehiculo
                CodigoVehiculo := Convenio.Cuentas.Vehiculos[Vehiculos].Cuenta.Vehiculo.CodigoVehiculo;
                if CompletarDatosVehiculo(CodigoVehiculo, DatosVehiculo) then begin
                    Vehiculo := Contrato.Vehiculo.Add;
                    Vehiculo.Accion.NodeValue := ACCION_ALTA;
                    Vehiculo.Patente    := DatosVehiculo.Patente;
                    Vehiculo.DV         := DatosVehiculo.DV;
                    Vehiculo.Categoria  := DatosVehiculo.Categoria;
                    Vehiculo.Catinturb  := DatosVehiculo.Categoria;
                    Vehiculo.Marca      := DatosVehiculo.Marca;
                    Vehiculo.Modelo     := DatosVehiculo.Modelo;
                    Vehiculo.Anio       := DatosVehiculo.Anio;
                    // Ahora el tag
                    if CompletarDatosTag(CodigoConvenio, CodigoVehiculo, DatosTag) then begin
                        Tag := Vehiculo.TAG.Add;
                        Tag.Accion := ACCION_ALTA;
                        Tag.Serie       := DatosTag.Serie;
                        Tag.Fabricante  := DatosTag.Fabricante;
                        Tag.Caturb      := DatosTag.Caturb;
                        Tag.Catinturb   := DatosTag.CatInTurb;
                        Tag.Context     := DatosTag.Context;
                        Tag.Contract    := DatosTag.Contract;
                        Tag.Estado      := 3; // Instalado
                        //Tag.Instal      :=  DateTimeToST5DateTime(Convenio.Cuentas.Vehiculos[Vehiculos].Cuenta.FechaAltaTag, 1);
                        Tag.Instal      :=  DateTimeToST5DateTime(Convenio.Cuentas.Vehiculos[Vehiculos].Cuenta.FechaCreacion, 1);
                    end else begin
                        Exit;
                    end;
                end else begin
                    Exit;
                end;
            end;
        end;
        // Nos resta obtener el path en d�nde se dejan los XML
        // y un nombre de acuerdo a la numeracion del momento
        Save := TSaveDialog.Create(nil);
        Save.InitialDir := PathNovedadXML;
        // Obtener el nombre para el archivo XML.
        NombreXML := EmptyStr;
        NombreXML := ObtenerNombreXML;
        if NombreXML = EmptyStr then begin
            raise Exception.Create('Error');
            Exit;
        end;

        Save.FileName := NombreXML;
        try
            try
                if Save.Execute then begin
                    Archivo.OwnerDocument.SaveToFile(Save.FileName);
                    Result := True;
                end;
            except
                Exit;
            end;
        finally
            FreeAndNil(Save);
            Archivo := nil;
        end;
    end;

resourcestring
    MSG_ERROR_GENERAL = 'Debe suministrase una instancia de un convenio';
    MSG_CONVENIO_EXTERNO = 'El convenio %s no es un convenio de CN. No se puede crear el XML ST5';
    MSG_ESTADO_DESCONOCIDO = 'El convenio %s posee un estado desconocido';
    MSG_ERROR_OBTENER_PARAMETRO_XML = 'Ha ocurrido un error al obtener el par�metro: %s.';
    MSG_CAPTION = 'Generar XML de Alta de Convenio.';
const
    ESTADO_VIGENTE = 1;
    ESTADO_BAJA = 2;
begin
    Result := False;
    // Control para el programador, al menos debe pasar un convenio
    if not Assigned (Convenio) then begin
        raise Exception.Create(MSG_ERROR_GENERAL);
        Exit;
    end;
    // El Convenio que me pasaron no es de CN. No se puede generar el XML.
    //if not Convenio.EsConvenioCN then begin									//SS_1147_MCA_20140408
    if not Convenio.EsConvenioNativo then begin    								//SS_1147_MCA_20140408
        raise Exception.Create(Format(MSG_CONVENIO_EXTERNO, [Convenio.CodigoConvenioExterno]));
        Exit;
    end;
    // Determinamos el Estado del Convenio, el cual puede ser Activo o Baja (1,2)
    if not Convenio.CodigoEstadoConvenio in [ESTADO_VIGENTE, ESTADO_BAJA] then begin
        raise Exception.Create(Format(MSG_ESTADO_DESCONOCIDO, [Convenio.CodigoConvenioExterno]));
        Exit;
    end;

    // Obtener el path en donde se dejan los archivos XML de Novedades.
    PathNovedadXML := EmptyStr;
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'PATH_ARCHIVO_SALIDA_XML', PathNovedadXML) then begin
        MsgBoxErr(MSG_CAPTION, Format(MSG_ERROR_OBTENER_PARAMETRO_XML, ['PATH_ARCHIVO_SALIDA_XML']), MSG_CAPTION, MB_ICONERROR);
        Exit;
    end;

    // Ahora generamos el XML que corresponda y listo.
    if Convenio.CodigoEstadoConvenio = ESTADO_VIGENTE then begin
        Result := GenerarAlta(Convenio);
    end else begin
        Result := GenerarBaja(Convenio);
    end;
end;

{******************************** Function Header ******************************
Function Name: CompletarDatosVehiculo
Author :
Date Created :
Description :
Parameters : const CodigoVehiculo: Integer; out Vehiculo: TVehiculoRNUT
Return Value : Boolean
Revision :
    Author : vpaszkowicz
    Date : 28/11/2006
    Description : Le agregro un Trim cuando obtiene la patente para que en XML
    la genere sin espacios.
*******************************************************************************}
function CompletarDatosVehiculo(const CodigoVehiculo: Integer; out Vehiculo: TVehiculoRNUT): Boolean;
var
    sp: TADOStoredProc;
begin
    Result := False;
    FillChar(Vehiculo, SizeOf(TVehiculoRNUT), 0);
    sp := TADOStoredProc.Create(nil); 
    try
        try
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'CAC_XML_EtiquetaVehiculo';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoVehiculo').Value := CodigoVehiculo;
            sp.Open;
            Result := (sp.RecordCount = 1);
            if Result then begin
                Vehiculo.Patente    := Trim(sp.FieldByName('Patente').AsString);
                Vehiculo.DV         := Trim(sp.FieldByName('DV').AsString);
                Vehiculo.Categoria  := sp.FieldByName('Categoria').AsInteger;
                Vehiculo.Marca      := sp.FieldByName('Marca').AsString;
                Vehiculo.Modelo     := sp.FieldByName('Modelo').AsString;
                Vehiculo.Anio       := sp.FieldByName('Anio').AsInteger;
            end;
            sp.Close;
        except
            on e: Exception do begin
                MsgBox(e.Message);
                Exit;
            end;
        end;
    finally
        FreeAndNil(sp);
    end;
end;
function CompletarDatosTag(const Codigoconvenio, CodigoVehiculo: Integer; out Tag: TTAGRNUT):Boolean;
var
    sp: TADOStoredProc;
begin
    Result := False;
    FillChar(Tag, SizeOf(TTagRnut), 0);
    sp := TADOStoredProc.Create(nil);
    try
        try
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'CAC_XML_EtiquetaTAG';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            sp.Parameters.ParamByName('@CodigoVehiculo').Value := CodigoVehiculo;
            sp.Open;
            Result := (sp.RecordCount = 1);
            if Result then begin
                Tag.Serie       := sp.FieldByName('Serie').AsString;
                Tag.Context     := sp.FieldByName('Context').AsInteger;
                Tag.Contract    := sp.FieldByName('Contract').AsInteger;
                Tag.Fabricante  := sp.FieldByName('Fabricante').AsInteger;
                Tag.Caturb      := sp.FieldByName('Caturb').AsInteger;
                Tag.CatInTurb   := Tag.Caturb;
            end;
            sp.Close;
        except
            on e: Exception do begin
                MsgBox(e.Message);
                Exit;
            end;
        end;
    finally
        FreeAndNil(sp);
    end;
end;

end.
