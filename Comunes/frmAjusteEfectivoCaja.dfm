object frmAjusteEfectivoCaja: TfrmAjusteEfectivoCaja
  Left = 0
  Top = 0
  BiDiMode = bdLeftToRight
  BorderStyle = bsDialog
  Caption = 'Ajuste Monto Efectivo '
  ClientHeight = 343
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  ParentBiDiMode = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object grpNuevoMontoCaja: TGroupBox
    Left = 3
    Top = 3
    Width = 289
    Height = 332
    Caption = 'Nuevo Monto Caja'
    TabOrder = 0
    object Label1: TLabel
      Left = 23
      Top = 27
      Width = 68
      Height = 13
      Caption = 'Nuevo Monto:'
    end
    object Label2: TLabel
      Left = 23
      Top = 55
      Width = 74
      Height = 13
      Caption = 'Motivo Cambio:'
    end
    object edtNuevoMonto: TEdit
      Left = 152
      Top = 24
      Width = 113
      Height = 21
      TabOrder = 0
      OnKeyPress = edtNuevoMontoKeyPress
    end
    object btnAceptar: TButton
      Left = 190
      Top = 295
      Width = 75
      Height = 25
      Caption = 'Aceptar'
      TabOrder = 2
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 40
      Top = 295
      Width = 75
      Height = 25
      Caption = 'Cerrar'
      TabOrder = 3
      OnClick = btnCancelarClick
    end
    object mmoMotivoCambio: TMemo
      Left = 3
      Top = 74
      Width = 283
      Height = 215
      TabStop = False
      BiDiMode = bdLeftToRight
      Ctl3D = True
      HideSelection = False
      Lines.Strings = (
        'mmoMotivoCambio')
      ParentBiDiMode = False
      ParentCtl3D = False
      TabOrder = 1
      WantReturns = False
    end
  end
end
