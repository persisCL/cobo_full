object FrameListMediosContactos: TFrameListMediosContactos
  Left = 0
  Top = 0
  Width = 701
  Height = 213
  TabOrder = 0
  DesignSize = (
    701
    213)
  object dblMedios: TDBListEx
    Left = 0
    Top = 4
    Width = 699
    Height = 173
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Tipo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescriTipoMedioContacto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 200
        Header.Caption = 'Medio de Contacto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Valor'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 250
        Header.Caption = 'Detalle'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Detalle'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 70
        Header.Caption = 'Hora desde'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'HorarioDesde'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 70
        Header.Caption = 'Hora hasta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'HorarioHasta'
      end>
    DataSource = dsMediosComunicacion
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDblClick = dblMediosDblClick
    OnDrawText = dblMediosDrawText
  end
  object btnAgregarMedioComunicacion: TButton
    Left = 475
    Top = 185
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Agregar'
    TabOrder = 1
    OnClick = btnAgregarMedioComunicacionClick
  end
  object btnEditarMedioComunicacion: TButton
    Left = 550
    Top = 185
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Editar'
    TabOrder = 2
    OnClick = btnEditarMedioComunicacionClick
  end
  object btnEliminarMedioComunicacion: TButton
    Left = 624
    Top = 185
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'E&liminar'
    TabOrder = 3
    OnClick = btnEliminarMedioComunicacionClick
  end
  object ObtenerMediosComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerMediosComunicacionPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Size = -1
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        DataType = ftWord
        Size = -1
        Value = Null
      end>
    Left = 148
    Top = 96
  end
  object dsMediosComunicacion: TDataSource
    DataSet = cdsMediosComunicacion
    Left = 176
    Top = 96
  end
  object cdsMediosComunicacion: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'IndiceMedioComunicacion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoMedioContacto'
        DataType = ftSmallint
      end
      item
        Name = 'Formato'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriTipoMedioContacto'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoArea'
        DataType = ftSmallint
      end
      item
        Name = 'Valor'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ANexo'
        DataType = ftString
        Size = 5
      end
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'DescriDomicilio'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 268
      end
      item
        Name = 'HorarioDesde'
        DataType = ftDateTime
      end
      item
        Name = 'HorarioHasta'
        DataType = ftDateTime
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoDomicilioRecNo'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'Valor'
        Fields = 'DescriTipoMedioContacto;Valor'
      end>
    IndexFieldNames = 'Valor'
    Params = <>
    ProviderName = 'dspMediosComunicacion'
    StoreDefs = True
    AfterOpen = cdsMediosComunicacionAfterDelete
    AfterInsert = cdsMediosComunicacionAfterDelete
    AfterPost = cdsMediosComunicacionAfterDelete
    AfterDelete = cdsMediosComunicacionAfterDelete
    Left = 204
    Top = 96
    object cdsMediosComunicacionIndiceMedioComunicacion: TIntegerField
      FieldName = 'IndiceMedioComunicacion'
    end
    object cdsMediosComunicacionCodigoTipoMedioContacto: TSmallintField
      FieldName = 'CodigoTipoMedioContacto'
    end
    object cdsMediosComunicacionFormato: TStringField
      FieldName = 'Formato'
      FixedChar = True
      Size = 1
    end
    object cdsMediosComunicacionDescriTipoMedioContacto: TStringField
      FieldName = 'DescriTipoMedioContacto'
      Size = 50
    end
    object cdsMediosComunicacionCodigoArea: TSmallintField
      FieldName = 'CodigoArea'
    end
    object cdsMediosComunicacionValor: TStringField
      FieldName = 'Valor'
      Size = 255
    end
    object cdsMediosComunicacionANexo: TStringField
      FieldName = 'ANexo'
      Size = 5
    end
    object cdsMediosComunicacionCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object cdsMediosComunicacionDescriDomicilio: TStringField
      FieldName = 'DescriDomicilio'
      Size = 255
    end
    object cdsMediosComunicacionDetalle: TStringField
      FieldName = 'Detalle'
      Size = 268
    end
    object cdsMediosComunicacionHorarioDesde: TDateTimeField
      FieldName = 'HorarioDesde'
    end
    object cdsMediosComunicacionHorarioHasta: TDateTimeField
      FieldName = 'HorarioHasta'
    end
    object cdsMediosComunicacionObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 100
    end
    object cdsMediosComunicacionCodigoDomicilioRecNo: TIntegerField
      FieldName = 'CodigoDomicilioRecNo'
    end
  end
  object dspMediosComunicacion: TDataSetProvider
    DataSet = ObtenerMediosComunicacionPersona
    Constraints = True
    Left = 232
    Top = 96
  end
  object cdsMediosComunicacionBorrados: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'IndiceMedioComunicacion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoMedioContacto'
        DataType = ftSmallint
      end
      item
        Name = 'Formato'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriTipoMedioContacto'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoArea'
        DataType = ftSmallint
      end
      item
        Name = 'Valor'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ANexo'
        DataType = ftString
        Size = 5
      end
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'DescriDomicilio'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 268
      end
      item
        Name = 'HorarioDesde'
        DataType = ftDateTime
      end
      item
        Name = 'HorarioHasta'
        DataType = ftDateTime
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoDomicilioRecNo'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftInteger
        Name = '@CodigoPersona'
        ParamType = ptInput
      end
      item
        DataType = ftWord
        Name = '@CodigoTipoMedioContacto'
        ParamType = ptInput
      end>
    ProviderName = 'dspMediosComunicacion'
    StoreDefs = True
    Left = 400
    Top = 176
    object cdsMediosComunicacionBorradosIndiceMedioComunicacion: TIntegerField
      FieldName = 'IndiceMedioComunicacion'
    end
    object cdsMediosComunicacionBorradosCodigoTipoMedioContacto: TSmallintField
      FieldName = 'CodigoTipoMedioContacto'
    end
    object cdsMediosComunicacionBorradosFormato: TStringField
      FieldName = 'Formato'
      FixedChar = True
      Size = 1
    end
    object cdsMediosComunicacionBorradosDescriTipoMedioContacto: TStringField
      FieldName = 'DescriTipoMedioContacto'
      Size = 50
    end
    object cdsMediosComunicacionBorradosCodigoArea: TSmallintField
      FieldName = 'CodigoArea'
    end
    object cdsMediosComunicacionBorradosValor: TStringField
      FieldName = 'Valor'
      Size = 255
    end
    object cdsMediosComunicacionBorradosANexo: TStringField
      FieldName = 'ANexo'
      Size = 5
    end
    object cdsMediosComunicacionBorradosCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object cdsMediosComunicacionBorradosDescriDomicilio: TStringField
      FieldName = 'DescriDomicilio'
      Size = 255
    end
    object cdsMediosComunicacionBorradosDetalle: TStringField
      FieldName = 'Detalle'
      Size = 268
    end
    object cdsMediosComunicacionBorradosHorarioDesde: TDateTimeField
      FieldName = 'HorarioDesde'
    end
    object cdsMediosComunicacionBorradosHorarioHasta: TDateTimeField
      FieldName = 'HorarioHasta'
    end
    object cdsMediosComunicacionBorradosObservaciones: TStringField
      FieldName = 'Observaciones'
      Size = 100
    end
    object cdsMediosComunicacionBorradosCodigoDomicilioRecNo: TIntegerField
      FieldName = 'CodigoDomicilioRecNo'
    end
  end
end
