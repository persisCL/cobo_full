object frmArbolFAQ: TfrmArbolFAQ
  Left = 104
  Top = 106
  Width = 850
  Height = 615
  Caption = #193'rbol de FAQs'
  Color = clBtnFace
  Constraints.MinHeight = 450
  Constraints.MinWidth = 750
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object splArbol: TSplitter
    Left = 306
    Top = 0
    Height = 581
    Color = clActiveCaption
    ParentColor = False
    ResizeStyle = rsLine
  end
  object panDerecho: TPanel
    Left = 309
    Top = 0
    Width = 533
    Height = 581
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlRespuestas'
    TabOrder = 0
    object Splitter3: TSplitter
      Left = 0
      Top = 330
      Width = 533
      Height = 3
      Cursor = crVSplit
      Align = alTop
      Color = clActiveCaption
      ParentColor = False
      ResizeStyle = rsLine
    end
    object panPyR: TPanel
      Left = 0
      Top = 333
      Width = 533
      Height = 205
      Align = alClient
      BevelOuter = bvNone
      Constraints.MinHeight = 50
      TabOrder = 0
      object Respuesta: TDBRichEdit
        Left = 0
        Top = 0
        Width = 533
        Height = 205
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsNone
        DataField = 'Respuesta'
        DataSource = dsPreguntasFAQ
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WantTabs = True
      end
    end
    object panArbol: TPanel
      Left = 0
      Top = 0
      Width = 533
      Height = 330
      Align = alTop
      BevelOuter = bvNone
      Constraints.MaxHeight = 330
      Constraints.MinHeight = 250
      TabOrder = 1
      object ArbolFAQ: TTreeView
        Left = 0
        Top = 0
        Width = 533
        Height = 330
        Align = alClient
        AutoExpand = True
        BevelOuter = bvNone
        BorderStyle = bsNone
        DragMode = dmAutomatic
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        HideSelection = False
        HotTrack = True
        Indent = 19
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        OnClick = ArbolFAQClick
        OnDragDrop = ArbolFAQDragDrop
        OnDragOver = ArbolFAQDragOver
      end
    end
    object panABArbol: TPanel
      Left = 0
      Top = 538
      Width = 533
      Height = 43
      Align = alBottom
      BorderStyle = bsSingle
      TabOrder = 2
      object btnAgregarEnArbol: TButton
        Left = 11
        Top = 7
        Width = 75
        Height = 25
        Hint = 'Agregar en Arbol'
        Caption = '+ &Arbol'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = btnAgregarEnArbolClick
      end
      object btnEliminarDeArbol: TButton
        Left = 101
        Top = 7
        Width = 75
        Height = 25
        Hint = 'Eliminar de Arbol'
        Caption = '- A&rbol'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnEliminarDeArbolClick
      end
    end
  end
  object panListaPreguntas: TPanel
    Left = 0
    Top = 0
    Width = 306
    Height = 581
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 1
    Constraints.MinWidth = 306
    TabOrder = 1
    object panABM: TPanel
      Left = 1
      Top = 539
      Width = 304
      Height = 41
      Align = alBottom
      BorderStyle = bsSingle
      TabOrder = 0
      object btnABMSubTitulos: TButton
        Left = 107
        Top = 6
        Width = 85
        Height = 25
        Caption = 'ABM &Subtitulos'
        TabOrder = 0
        OnClick = btnABMSubTitulosClick
      end
      object btnABMPreguntas: TButton
        Left = 205
        Top = 6
        Width = 85
        Height = 25
        Caption = 'ABM &Preguntas'
        TabOrder = 1
        OnClick = btnABMPreguntasClick
      end
      object btnABMTitulos: TButton
        Left = 10
        Top = 6
        Width = 85
        Height = 25
        Caption = 'ABM &T'#237'tulos'
        TabOrder = 2
        OnClick = btnABMTitulosClick
      end
    end
    object grdTitulos: TDPSGrid
      Left = 1
      Top = 111
      Width = 304
      Height = 120
      Hint = 'Doble click para editar'
      Align = alTop
      DataSource = dsTitulos
      Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = grdTitulosDblClick
      OnKeyPress = grdTitulosKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'Titulo'
          Title.Caption = 'T'#237'tulos'
          Width = 300
          Visible = True
        end>
    end
    object grdSubTitulos: TDPSGrid
      Left = 1
      Top = 231
      Width = 304
      Height = 120
      Hint = 'Doble click para editar'
      Align = alTop
      DataSource = dsSubTitulos
      Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = grdSubTitulosDblClick
      OnKeyPress = grdSubTitulosKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'SubTitulo'
          Title.Caption = 'SubT'#237'tulos'
          Width = 300
          Visible = True
        end>
    end
    object grdPreguntas: TDPSGrid
      Left = 1
      Top = 351
      Width = 304
      Height = 188
      Hint = 'Doble click para editar'
      Align = alClient
      DataSource = dsPreguntas
      Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = grdPreguntasDblClick
      OnKeyPress = grdPreguntasKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'Pregunta'
          Title.Caption = 'Preguntas'
          Width = 300
          Visible = True
        end>
    end
    object grdFuentes: TDPSGrid
      Left = 1
      Top = 1
      Width = 304
      Height = 110
      Align = alTop
      DataSource = dsFuentes
      Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Fuente'
          Title.Caption = 'Fuentes'
          Width = 300
          Visible = True
        end>
    end
  end
  object dsPreguntasFAQ: TDataSource
    DataSet = ObtenerPreguntasFAQ
    Left = 159
    Top = 291
  end
  object ObtenerPreguntasFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerPreguntasFAQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSubtitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Prepared = True
    Left = 191
    Top = 293
  end
  object qryPreguntas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      
        #9'P.CodigoPregunta, P.Descripcion Pregunta, (select count (Codigo' +
        'Pregunta) from FAQArbol A WITH (NOLOCK) where A.CodigoPregunta =' +
        ' P.CodigoPregunta) As Cantidad'
      'from'
      #9'FAQPreguntas P WITH (NOLOCK) '
      'order by'
      #9'P.Descripcion'
      '')
    Left = 39
    Top = 372
  end
  object dsPreguntas: TDataSource
    DataSet = qryPreguntas
    Left = 9
    Top = 372
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 9
    Top = 135
  end
  object qryTitulos: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      
        #9'T.CodigoTitulo, T.Descripcion As Titulo, (select count (CodigoT' +
        'itulo) from FAQArbol A WITH (NOLOCK) where A.CodigoTitulo = T.Co' +
        'digoTitulo) As Cantidad'
      'from'
      #9'FAQTitulos T WITH (NOLOCK) '
      'order by'
      #9'T.Descripcion'
      '')
    Left = 39
    Top = 135
    object qryTitulosCodigoTitulo: TIntegerField
      FieldName = 'CodigoTitulo'
    end
    object qryTitulosTitulo: TStringField
      FieldName = 'Titulo'
      Size = 50
    end
    object qryTitulosCantidad: TIntegerField
      FieldName = 'Cantidad'
      ReadOnly = True
    end
  end
  object dsSubTitulos: TDataSource
    DataSet = qrySubTitulos
    Left = 9
    Top = 255
  end
  object qrySubTitulos: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      
        #9'ST.CodigoSubTitulo, ST.Descripcion As SubTitulo, (select count ' +
        '(CodigoSubTitulo) from FAQArbol A WITH (NOLOCK) where A.CodigoSu' +
        'bTitulo = ST.CodigoSubTitulo) As Cantidad'
      'from'
      #9'FAQSubTitulos ST WITH (NOLOCK) '
      'order by'
      #9'ST.Descripcion')
    Left = 39
    Top = 255
  end
  object ObtenerArbolFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Filtered = True
    ProcedureName = 'ObtenerArbolFAQ;1'
    Parameters = <>
    Left = 192
    Top = 263
  end
  object spEliminarDeArbolFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarDeArbolFAQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSubtitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 255
    Top = 387
  end
  object spActualizarArbolFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarArbolFAQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuente'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSubtitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Orden'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 255
    Top = 357
  end
  object dsFuentes: TDataSource
    DataSet = qryFuentes
    Left = 9
    Top = 27
  end
  object qryFuentes: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      
        #9'F.CodigoFuenteSolicitud, F.Descripcion As Fuente, (select count' +
        ' (CodigoFuenteSolicitud) from FAQArbol A WITH (NOLOCK) where A.C' +
        'odigoFuenteSolicitud = F.CodigoFuenteSolicitud) As Cantidad'
      'from'
      #9'FuentesSolicitud F  WITH (NOLOCK) '
      'where'
      #9'F.Activo = 1'
      'order by'
      #9'F.Descripcion')
    Left = 39
    Top = 27
  end
  object spLimpiarOrdenFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'LimpiarOrdenFAQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 255
    Top = 417
  end
  object spBorrarOrdenFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BorrarOrdenFAQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 255
    Top = 447
  end
end
