{-------------------------------------------------------------------------------
 File Name: FrmReclamoFaltaVehiculo.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos

 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 29-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y SubTipoReclamo

 Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos
-------------------------------------------------------------------------------}
unit FrmReclamoFaltaVehiculo;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //NULLDATE
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  FreContactoReclamo,             //Frame contacto
  FreCompromisoOrdenServicio,     //Frame compromiso
  FreSolucionOrdenServicio,       //Frame solucion
  FreHistoriaOrdenServicio,       //Frame Historia
  FreFuenteReclamoOrdenServicio,  //Frame Fuente Reclamo
  FreNumeroOrdenServicio,         //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,      //Frame Respuesta Orden Servicio
  OrdenesServicio,                //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, {FreSubTipoReclamoOrdenServicio,}                            // _PAN_
  FreConcesionariaReclamoOrdenServicio;

type
  TFormReclamoFaltaVehiculo = class(TForm)
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
    Label1: TLabel;
    txtDetalle: TMemo;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio;
    spObtenerOrdenServicio: TADOStoredProc;
    LPatenteFaltante: TLabel;
    txtPatenteFaltante: TEdit;
    PAbajo: TPanel;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
    //FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;     // _PAN_
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FTipoOrdenServicio: Integer;
    FCodigoOrdenServicio: Integer;
    FEditando : Boolean; //Indica si estoy editando o no
    Function GetCodigoOrdenServicio : Integer;
  public
    { Public declarations }
    Function Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0): Boolean;
    Function InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
    Property CodigoOrdenServicio : Integer Read GetCodigoOrdenServicio;
  end;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoFaltaVehiculo.Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0) : Boolean;
Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo Falta Vehiculo';
    MSG_ERROR = 'Error';
Var
  	Sz: TSize;
begin
    FTipoOrdenServicio := TipoOrdenServicio;
    try
        //Centro el form
        CenterForm(Self);
        //ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);
        //titulo
        Caption := QueryGetValue(DMConnections.BaseCAC, Format(
                   'select dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
                   [TipoOrdenServicio]));
        PageControl.ActivePageIndex := 0;
        ActiveControl := txtDetalle;

        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(10) and
                                FrameSolucionOrdenServicio1.Inicializar and
                                        FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar{ and
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar};             // _PAN_
                                        //---------------------------------------------------------------

                                        
        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    FrameNumeroOrdenServicio1.Visible := False;
    FEditando:=false; //No estoy Editando
    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso := ObtenerFechaComprometidaOS(Now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, FrameCompromisoOrdenServicio1.Prioridad);
    AddOSForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:  DDeMarco
  Date Created:
  Description: Modifico Reclamo que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoFaltaVehiculo.InicializarEditando( CodigoOrdenServicio : Integer) : Boolean;

    //form read Only
    Procedure ReadOnly;
    begin
        Txtpatentefaltante.ReadOnly := True;
        //comunes
        FrameContactoReclamo1.Deshabilitar;
        FrameCompromisoOrdenServicio1.Enabled := False;
        FrameSolucionordenServicio1.Enabled := False;
        FrameRespuestaOrdenServicio1.Deshabilitar;
        TxtDetalle.ReadOnly := True;
        TxtDetalleSolucion.ReadOnly := True;
        CKRetenerOrden.Visible := False;
        BtnCancelar.Caption := 'Salir';
        AceptarBtn.Visible := False;
    end;

var
    Estado : String;
begin
    FCodigoOrdenServicio := CodigoOrdenServicio;
    spObtenerOrdenServicio.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    Result := OpenTables([spObtenerOrdenServicio]);
    if not Result then Exit;

    // Cargamos los datos
    Result := Inicializar(spObtenerOrdenServicio['TipoOrdenServicio']);

    txtDetalle.Text := spObtenerOrdenServicio.FieldByName('ObservacionesSolicitante').AsString;
    txtDetalleSolucion.Text := spObtenerOrdenServicio.FieldByName('ObservacionesEjecutante').AsString;

    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    
    //obtengo la historia para esa orden de servicio
    FrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio);

    //obtengo la fuente del reclamo
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //Obtengo el Estado de la OS para saber si permito editar o read only
    Estado := spObtenerOrdenServicio.FieldByName('Estado').AsString;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;

    //obtengo la patente del vehiculo que falta en el convenio
    txtPatenteFaltante.Text:= QueryGetValue(DMconnections.BaseCAC,'SELECT dbo.ObtenerPatenteFaltanteOS('+IntToStr(CodigoOrdenServicio)+')');

    // Listo
    spObtenerOrdenServicio.Close;

    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(IntToStr(CodigoOrdenServicio));
    CKRetenerOrden.Checked := False; //por Default no retengo la orden de servicio
    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;
    FEditando := True; //Estoy Editando

end;


{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:  DDeMarco
  Date Created:
  Description: Soluciono un Reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoFaltaVehiculo.InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.ActivePageIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormReclamoFaltaVehiculo.GetCodigoOrdenServicio: Integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFaltaVehiculo.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: registra el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFaltaVehiculo.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  Valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar: Boolean;
    resourcestring
        MSG_ERROR_PATENTE = 'Debe indicar una patente';
        MSG_ERROR_DET     = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE  = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA   = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                            'debe ser menor a la Fecha Comprometida con el Cliente';
    begin
        Result := False;

        //Validamos fuente de reclamo
        if FEditando = False then begin
             if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            Result := True;
            Exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = False then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            TxtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;


        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802


        Result := True;
    end;

Var
    OS: Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    // Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra-----------------------------------
    DMConnections.BaseCAC.BeginTrans;
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,txtDetalleSolucion.Text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaBefore,       // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaAfter,        // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteBefore,  // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteAfter    // _PAN_
                                   {FrameSubTipoReclamoOrdenServicio1}                              // _PAN_
                                   );
    //FinRev.1-------------------------------------------------------------------------

    if OS <= 0 then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Especifico de Cuenta
    if not GuardarOrdenServicioCuenta(OS, '', txtPatenteFaltante.Text, NULLDATE, 0, '', 0 , 0, 0) then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Listo
    DMConnections.BaseCAC.CommitTrans;

    //si es un reclamo nuevo
    if FEditando = False then begin

        //informo al operador el numero de Reclamo que se genero
        InformarNumeroReclamo(IntToStr(OS));
        DesbloquearOrdenServicio(OS);
    end;

    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFaltaVehiculo.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFaltaVehiculo.FormDestroy(Sender: TObject);
begin
    RemoveOSform(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: desbloqueo la orden de servicio antes de salir
               y libero el formulario de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFaltaVehiculo.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //si se esta modificando un reclamo
    if FEditando = True then begin
        //verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = False then begin
            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //lo libero de memoria
    Action := caFree;
end;

end.
