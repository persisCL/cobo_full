{-------------------------------------------------------------------------------
 File Name: FrmEstacionamientosReclamados.pas
 Author: lgisuk
 Date Created: 24/05/05
 Language: ES-AR
 Description: Informo los Estacionamientos que ya fueron reclamados
              y permito abrir un reclamo por los tránsitos
              no reclamados.
Revision : 1
    Date: 04/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
-------------------------------------------------------------------------------}
unit FrmEstacionamientosReclamados;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  OrdenesServicio,                //ObtenerDescripcionOS
  FrmReclamoEstacionamiento,             //Reclamo Estacionamiento
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Menus;

type
  TFormEstacionamientosReclamados = class(TForm)
    DataSource: TDataSource;
    lnCheck: TImageList;
    DBLEstacionamientos: TDBListEx;
    Ltitulo: TLabel;
    Lmensaje: TLabel;
    PBotonera: TPanel;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    OKBTN: TButton;
    ilCamara: TImageList;
    spObtenerEstacionamientosReclamados: TADOStoredProc;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure DBLEstacionamientosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
  private
    { Private declarations }
    fcodigoconvenio:integer;
    fListaEstacionamientos:tstringlist;
    fListaNoincluidos:tstringlist;
  public
    { Public declarations }
    function Inicializar(CodigoConvenio: Integer; ListaEstacionamientos:tstringlist ): Boolean;
  end;

var
  FormEstacionamientosReclamados: TFormEstacionamientosReclamados;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicialización de Este Formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormEstacionamientosReclamados.Inicializar(CodigoConvenio: Integer; ListaEstacionamientos:tstringlist ): Boolean;

    //Cargo la grilla de reclamos
    Function ObtenerDatosEstacionamientos(CodigoConvenio:integer; Estacionamientos: TStringList):boolean;

        //Obtengo la Enumeración
        function ObtenerEnumeracion(Lista: TStringList): String;
        var
            Enum: AnsiString;
            i: Integer;
        begin
            Enum := '';
            for i:= 0 to Lista.Count - 1 do begin
                //Enum := Enum + Lista.Strings[i] + ',';
                //Enum := Enum + Copy( Lista.Strings[i], 0 , Pos('=',Lista.Strings[i])-1) + ',';
                Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
            end;
            Delete(Enum, Length(Enum), 1);
            Result := Enum;
        end;

    begin
        with SPObtenerEstacionamientosReclamados do begin
            close;
            parameters.Refresh;
            parameters.ParamByName('@CodigoConvenio').value := IIF(CodigoConvenio = 0, NULL, CodigoConvenio);
            parameters.ParamByName('@Enumeracion').value := ObtenerEnumeracion(Estacionamientos);
            open;
        end;

        result:=true;
    end;

    {-----------------------------------------------------------------------------
        Function Name:
        Author:
        Date Created:  /  /
        Description:
        Parameters: var ListaNoIncluidos:TstringList
        Return Value: boolean

        Revision : 1
        Date: 04/03/2009
        Author: pdominguez
        Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
            los bloqueos de tablas en la lectura

    -----------------------------------------------------------------------------}
    Function ObtenerEstacionamientosNoIncluidos(var ListaNoIncluidos:TstringList):boolean;

        //me fijo si esta incluido el Estacionamiento en algun otro reclamo del cliente
        Function EstaIncluidoEstacionamiento(CodigoConvenio:integer;NumCorrEstacionamiento:string):boolean;
        var sQuery:String;
        begin
            result:=false;
            if codigoconvenio =  0 then
            begin
                sQuery := 'SELECT TOP 1 NumCorrEstacionamiento FROM OrdenesServicio OS WITH (NOLOCK), OrdenesServicioEstacionamiento OSE WITH (NOLOCK) WHERE OS.CodigoOrdenServicio = OSE.CodigoOrdenServicio AND ((OS.Estado = '''+'T'+''' AND OSE.CodigoResolucionReclamoEstacionamiento = 2) OR (OS.Estado IN ('''+'P'+''','''+'I'+''','''+'E'+'''))) AND NumCorrEstacionamiento = ' + NumCorrEstacionamiento;   // SS_1006_PDO_20120106
                result := QueryGetValue(dmconnections.BaseCAC,sQuery) <> '';
            end;
            if codigoconvenio <> 0 then
            begin
                sQuery:='SELECT TOP 1 NumCorrEstacionamiento FROM OrdenesServicio OS WITH (NOLOCK), OrdenesservicioEstacionamiento OSE WITH (NOLOCK) WHERE OS.CodigoOrdenServicio = OSE.CodigoOrdenServicio AND ((OS.Estado = '''+'T'+''' AND OSE.CodigoResolucionReclamoEstacionamiento = 2) or (OS.Estado IN ('''+'P'+''','''+'I'+''','''+'E'+'''))) AND OS.CodigoConvenio = ' +IntToStr(codigoconvenio)+' AND NumCorrEstacionamiento = ' + NumCorrEstacionamiento;   // SS_1006_PDO_20120106
                result := QueryGetValue(dmconnections.BaseCAC,sQuery) <> '';
            end;
        end;

    var
      i:integer;
    begin
        i:=0;
        while i <= FListaEstacionamientos.Count-1 do
        begin
            if not EstaIncluidoEstacionamiento(FCodigoConvenio, FListaEstacionamientos.Strings[i]) then begin
                ListaNoIncluidos.Add(FListaEstacionamientos.Strings[i]);
            end;
            inc(i);
        end;
        result:=true;
    end;

Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Estacionamientos Reclamados';
    MSG_ERROR = 'Error';
Const
    STR_MENSAJE = 'Todos los tránsitos seleccionados estan incluidos en reclamos anteriores!';
var
    ListaNoIncluidos: TstringList;
begin
    try
        //si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaEstacionamientos <> nil then begin
            //cargo la grilla con los Estacionamientos
            ObtenerDatosEstacionamientos(CodigoConvenio, ListaEstacionamientos);
        end;
        FCodigoConvenio := CodigoConvenio;
        FListaEstacionamientos := ListaEstacionamientos;
        //Centro el form
        CenterForm(Self);
        //creo la lista
        ListaNoIncluidos:= TStringList.Create;
        //cargo la lista de Estacionamientos no includos
        ObtenerEstacionamientosNoIncluidos(ListaNoIncluidos);
        //si no hay ninguno sin incluir
        if ListanoIncluidos.Text = '' then begin
            Lmensaje.caption := STR_MENSAJE;
            OKBTN.Visible :=true;
            AceptarBTN.Visible := False;
            BTNCancelar.Visible := False;
        end;
        FlistaNoIncluidos := ListaNoIncluidos;
        //titulo
        ActiveControl := dblEstacionamientos;
        Result := true;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormEstacionamientosReclamados.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLEstacionamientosDrawText
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormEstacionamientosReclamados.DBLEstacionamientosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 29/06/2005
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer):boolean;
    var
        bmp: TBitMap;
    begin
        bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            bmp.Free;
        end;
    end;

var
    I : Integer;
    Resolucion : String;
begin
    //FechaHora
    if Column = dblEstacionamientos.Columns[2] then begin
       if Text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', SPObtenerEstacionamientosReclamados.FieldByName('FechaHoraEntrada').AsDateTime);
    end;
    if Column = dblEstacionamientos.Columns[4] then begin
       if Text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', SPObtenerEstacionamientosReclamados.FieldByName('FechaHoraSalida').AsDateTime);
    end;

    //muestra si el cliente rechazo un Estacionamiento o no
  	if Column = dblEstacionamientos.Columns[5] then begin
        //verifica si esta seleccionado o no
        I := Iif(SPObtenerEstacionamientosReclamados.FieldByName('Rechaza').Asstring = 'True', 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dblEstacionamientos, lncheck, I);
  	end;

    //Muestra la resolucion de la empresa con respecto al Estacionamiento
  	if Column = dblEstacionamientos.Columns[6] then begin
        //Obtengo la resolcion
        Resolucion := SPObtenerEstacionamientosReclamados.FieldByName('CodigoResolucionReclamoEstacionamiento').AsString;
        if Resolucion = '' then begin
            Text := 'Pendiente';
        end else begin
            if Resolucion = '1' then Text := 'Pendiente';
            if Resolucion = '2' then Text := 'Aceptado';
            if Resolucion = '3' then Text := 'Denegado';
        end;
  	end;

    //muestros la descripcion del estado
    if Column = dblEstacionamientos.Columns[8] then begin
        Text := ObtenerDescripcionEstadoOS(SPObtenerEstacionamientosReclamados.FieldByName('Estado').Asstring);
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: permito abrir un reclamo por los tránsitos
               no reclamados.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormEstacionamientosReclamados.AceptarBTNClick(Sender: TObject);

    Procedure CrearReclamoEstacionamiento(CodigoConvenio : Integer ; ListaNoIncluidos : TStringList);
    var
        F:TFormReclamoEstacionamiento;
    begin
        //creo el form de reclamo por Estacionamientos no aceptados
        Application.CreateForm(TFormReclamoEstacionamiento, F);
        if F.Inicializar(217, CodigoConvenio, ListaNoIncluidos) then F.show;
    end;

begin
    //creo el reclamo con los tránsitos que no estan incluidos
    CrearReclamoEstacionamiento(FCodigoConvenio, FListaNoIncluidos);
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormEstacionamientosReclamados.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description:
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormEstacionamientosReclamados.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //lo libero de memoria
    Action := caFree;
end;


end.
