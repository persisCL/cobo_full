object FormObservaciones: TFormObservaciones
  Left = 186
  Top = 181
  BorderStyle = bsDialog
  Caption = 'Ingresar Observaciones'
  ClientHeight = 197
  ClientWidth = 568
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  DesignSize = (
    568
    197)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 8
    Width = 556
    Height = 153
  end
  object Label1: TLabel
    Left = 16
    Top = 45
    Width = 74
    Height = 13
    Caption = '&Observaciones:'
    FocusControl = MeObservaciones
  end
  object Label2: TLabel
    Left = 16
    Top = 22
    Width = 98
    Height = 13
    Caption = '&Motivo del Contacto:'
    FocusControl = cbMotivosContacto
  end
  object MeObservaciones: TMemo
    Left = 16
    Top = 61
    Width = 537
    Height = 88
    ScrollBars = ssVertical
    TabOrder = 1
    OnChange = MeObservacionesChange
  end
  object cbMotivosContacto: TVariantComboBox
    Left = 122
    Top = 18
    Width = 305
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 0
    Items = <>
  end
  object btn_Aceptar: TButton
    Left = 347
    Top = 167
    Width = 86
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 2
    OnClick = btn_AceptarClick
  end
  object btnCancelar: TButton
    Left = 440
    Top = 166
    Width = 121
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Continuar Llamada'
    TabOrder = 3
    OnClick = btn_CancelarClick
  end
  object MotivosContactoPosibles: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoComunicacion'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT'
      '  Workflows.MotivoContacto'
      'FROM '
      '  OrdenesServicio  WITH (NOLOCK) , Workflows WITH (NOLOCK) '
      'WHERE '
      '  OrdenesServicio.CodigoComunicacion = :CodigoComunicacion'
      
        '  AND OrdenesServicio.CodigoWorkflow = Workflows.CodigoWorkflow ' +
        ' ')
    Left = 96
    Top = 120
  end
  object Comunicaciones: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Comunicaciones'
    Left = 64
    Top = 120
  end
end
