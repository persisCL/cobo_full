unit FrmTerminarTarea;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, DPSControls;

type
  TFormTerminarTarea = class(TForm)
    Label1: TLabel;
    Memo1: TMemo;
    TerminarTarea: TADOStoredProc;
    OPButton1: TButton;
    OPButton2: TButton;
    procedure OPButton1Click(Sender: TObject);
  private
	{ Private declarations }
	FOrdenServicio, FTarea: Integer;
  public
	{ Public declarations }
	function Inicializa(OrdenServicio, Tarea: Integer): Boolean;
  end;

var
  FormTerminarTarea: TFormTerminarTarea;

implementation

{$R *.dfm}

function TFormTerminarTarea.Inicializa(OrdenServicio, Tarea: Integer): Boolean;
begin
	FOrdenServicio := OrdenServicio;
	FTarea := Tarea;
	Result := True;
end;

procedure TFormTerminarTarea.OPButton1Click(Sender: TObject);
begin
	With TerminarTarea do begin
		Parameters.ParamByName('@CodigoOrdenServicio').Value := FOrdenServicio;
		Parameters.ParamByName('@CodigoTarea').Value := FTarea;
		Parameters.ParamByName('@Observaciones').Value := Memo1.Lines.Text;
		try
			Screen.Cursor := crHourGlass;
			ExecProc;
		finally
			Screen.Cursor := crDefault;
		end;
	end;
	ModalResult := mrOk;
end;

end.
