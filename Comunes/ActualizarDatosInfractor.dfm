object DMActualizarDatosInfractor: TDMActualizarDatosInfractor
  OldCreateOrder = False
  Height = 166
  Width = 383
  object VerificarInfractoresBD: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarInfractoresBD'
    Parameters = <>
    Left = 184
    Top = 8
  end
  object ActualizarConsultaRNVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 300
    ProcedureName = 'ActualizarConsultaRNVM'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConsultaRNVM'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@DescripMarca'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Modelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@DescripCodigoTipoVehiculo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Color'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@IdMotor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@IdChasis'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@RUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@DescripCodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FechaAdquisicion'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable, paLong]
        DataType = ftString
        Size = 5000
        Value = Null
      end>
    Left = 64
    Top = 8
  end
  object sp_IncrementarCantidadConsultasRNVMInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'IncrementarCantidadConsultasRNVMInfraccion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 204
    Top = 76
  end
  object sp_AnularInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 288
    Top = 60
  end
  object sp_ObtenerCantidadConsultasRNVMInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCantidadConsultasRNVMInfraccion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end>
    Left = 52
    Top = 68
  end
  object spAgregarConsultaRNVM: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarConsultaRNVM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConsultaRNVM'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@DescripCodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5000
        Value = Null
      end>
    Left = 296
    Top = 8
  end
end
