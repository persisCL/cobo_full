{-----------------------------------------------------------------------------
 Unit Name  : FrmAjusteEfectivoCaja
 Author     : gleon
 Purpose    : Cambiar el monto en efectivo en caja de un turno.
 Firma      : TASK_031_GLE_20170111
 Descripci�n: Solicita al Supervisor los nuevos datos del ajuste en caja:
                nuevo monto en efectivo y motivo del cambio.
-----------------------------------------------------------------------------}
unit frmAjusteEfectivoCaja;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Util, VariantComboBox, Validate, DateEdit, UtilProc, DB,
  ADODB, DMConnection, LoginSup, BuscaTab;

type
  TfrmAjusteEfectivoCaja = class(TForm)
    grpNuevoMontoCaja: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    edtNuevoMonto: TEdit;
    btnAceptar: TButton;
    btnCancelar: TButton;
    mmoMotivoCambio: TMemo;
    procedure btnAceptarClick(Sender: TObject);
    procedure edtNuevoMontoKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    MontoInicio : Integer;
    FNumeroTurno : Integer;
    FSupervisor : string;
  public
    { Public declarations }
    AjusteOk : Boolean;
    function Inicializar (NumeroTurno: Integer; UsuarioSistema: AnsiString; MontoInicial:Integer): Boolean;

  end;

var
  FormAjusteEfectivoCaja: TfrmAjusteEfectivoCaja;


implementation

{$R *.dfm}

{ TfrmAjusteEfectivoCaja }

function TfrmAjusteEfectivoCaja.Inicializar(NumeroTurno: Integer; UsuarioSistema: AnsiString; MontoInicial: Integer): Boolean;
resourcestring
	MSG_CAPTION             = 'Ajuste de Monto en Efectivo en Caja';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
var
    SP: TFrmConfirmacionSupervisor;
begin
    Result := False;
    MontoInicio :=  MontoInicial ;
    FNumeroTurno := NumeroTurno ;
        try
            //validar usuario Supervisor
            Application.CreateForm(TFrmConfirmacionSupervisor, SP);
            SP.ShowModal;

            if SP.ModalResult = mrOk then begin
                mmoMotivoCambio.text := '';
                AjusteOk := True;
                FSupervisor :=  SP.SupervisorLogin;
            end;
            Close;

        except
            on e: exception do begin
                MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
                Result := false;
                Close;
            end;
        end

end;

procedure TfrmAjusteEfectivoCaja.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_CAPTION             = 'Ajuste de Monto en Efectivo en Caja';
var
    motivoAjuste, supLogin: string;
    nuevoMonto, diferenciaEfectivo: Integer;
    ActualizacionExitosa : Boolean;
    spActualizaTurno: TADOStoredProc;
    //f: TFrmConfirmacionSupervisor;
begin
    nuevoMonto := 0;
    diferenciaEfectivo := 0;
    ActualizacionExitosa := False;
    supLogin := '';

    //Actualizo el importe efectivo e inserto el registro en la nueva tabla hist�rico
    //ActualizarImporteEfectivoTurno si es que los datos est�n completos

    if mmoMotivoCambio.text <> ''  then begin
        motivoAjuste := mmoMotivoCambio.text;
        nuevoMonto := StrToInt(edtNuevoMonto.text);
        diferenciaEfectivo := MontoInicio - nuevoMonto;

       try
            //supLogin := f.SupervisorLogin;

            spActualizaTurno := TADOStoredProc.Create(nil);
            spActualizaTurno.Connection := DMConnections.BaseCAC;
            spActualizaTurno.ProcedureName := 'ActualizarImporteEfectivoTurno';
            spActualizaTurno.Parameters.Refresh;

            spActualizaTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
            spActualizaTurno.Parameters.ParamByName('@ImporteEfectivoAnterior').Value := MontoInicio;
            spActualizaTurno.Parameters.ParamByName('@ImporteEfectivoPosterior').Value := nuevoMonto;
            spActualizaTurno.Parameters.ParamByName('@DiferenciaEfectivo').Value := diferenciaEfectivo;
            spActualizaTurno.Parameters.ParamByName('@MotivoCambio').Value := motivoAjuste;
            spActualizaTurno.Parameters.ParamByName('@UsuarioCreacion').Value := FSupervisor; //supLogin;

            spActualizaTurno.ExecProc;

            ActualizacionExitosa := True;
        except
            on e: exception do begin
                MsgBoxErr('Error al Ajustar Monto en Caja', E.Message, MSG_CAPTION, MB_ICONERROR);
                ActualizacionExitosa := False;
                //if Assigned(f) then FreeAndNil(f);
                Close;
            end;
        end;

       if ActualizacionExitosa then begin
            spActualizaTurno.Close;
            spActualizaTurno.Free;
            MsgBox('Se ha actualizado correctamente el nuevo Importe en Efectivo.','Actualizaci�n Exitosa', MB_ICONINFORMATION);
            close;
         end;

    end
    else begin
        MsgBox('Debe ingresar un Motivo por el cual se hace el ajuste','Validaci�n Datos', MB_ICONINFORMATION);
    end;
end;

procedure TfrmAjusteEfectivoCaja.btnCancelarClick(Sender: TObject);
begin
Close;
end;

procedure TfrmAjusteEfectivoCaja.edtNuevoMontoKeyPress(Sender: TObject; var Key: Char);
begin
  //Limita la entrada a solo n�meros enteros
  // #8 is Backspace
  if not (Key in [#8, '0'..'9']) then begin
    ShowMessage('Ingresar s�lo valores num�ricos');
    // Discard the key
    Key := #0;
  end;
end;

end.
