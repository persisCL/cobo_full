object frmArbolFAQ: TfrmArbolFAQ
  Left = 132
  Top = 79
  Width = 838
  Height = 590
  Caption = 'F.A.Qs.'
  Color = clBtnFace
  Constraints.MaxHeight = 600
  Constraints.MaxWidth = 880
  Constraints.MinHeight = 500
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object splArbol: TSplitter
    Left = 306
    Top = 0
    Width = 3
    Height = 556
    Cursor = crHSplit
    Color = clActiveCaption
    ParentColor = False
    ResizeStyle = rsLine
  end
  object panDerecho: TPanel
    Left = 309
    Top = 0
    Width = 521
    Height = 556
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlRespuestas'
    TabOrder = 0
    object Splitter3: TSplitter
      Left = 217
      Top = 0
      Width = 3
      Height = 513
      Cursor = crHSplit
      Color = clActiveCaption
      ParentColor = False
      ResizeStyle = rsLine
    end
    object panPyR: TPanel
      Left = 220
      Top = 0
      Width = 301
      Height = 513
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 226
        Width = 301
        Height = 2
        Cursor = crVSplit
        Align = alBottom
        Color = clActiveCaption
        MinSize = 3
        ParentColor = False
        ResizeStyle = rsNone
      end
      object panRespuesta: TPanel
        Left = 0
        Top = 228
        Width = 301
        Height = 285
        Align = alBottom
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 0
        object Respuesta: TDBRichEdit
          Left = 0
          Top = 0
          Width = 301
          Height = 285
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsNone
          DataField = 'Respuesta'
          DataSource = dsPreguntasFAQ
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 0
          WantTabs = True
        end
      end
      object panPreguntas: TPanel
        Left = 0
        Top = 0
        Width = 301
        Height = 226
        Align = alClient
        Caption = 'panPreguntas'
        Color = clWindow
        TabOrder = 1
        object Preguntas: TDBLookupListBox
          Left = 1
          Top = 1
          Width = 299
          Height = 221
          Hint = 'Hacer doble click para ver respuesta'
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Pregunta'
          ListField = 'Pregunta'
          ListSource = dsPreguntasFAQ
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = PreguntasEnter
          OnDblClick = PreguntasDblClick
          OnEnter = PreguntasEnter
          OnKeyDown = PreguntasKeyDown
        end
      end
    end
    object panArbol: TPanel
      Left = 0
      Top = 0
      Width = 217
      Height = 513
      Align = alLeft
      BevelOuter = bvNone
      Constraints.MaxWidth = 300
      Constraints.MinWidth = 200
      TabOrder = 1
      object ArbolFAQ: TTreeView
        Left = 0
        Top = 0
        Width = 217
        Height = 513
        Align = alClient
        AutoExpand = True
        BevelOuter = bvNone
        BorderStyle = bsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        HideSelection = False
        HotTrack = True
        Indent = 19
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        OnClick = ArbolFAQClick
        OnDragDrop = ArbolFAQDragDrop
        OnDragOver = ArbolFAQDragOver
      end
    end
    object panABArbol: TPanel
      Left = 0
      Top = 513
      Width = 521
      Height = 43
      Align = alBottom
      BorderStyle = bsSingle
      TabOrder = 2
      object btnAgregarEnArbol: TDPSButton
        Left = 11
        Top = 7
        Caption = '+ &Arbol'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = btnAgregarEnArbolClick
      end
      object btnEliminarDeArbol: TDPSButton
        Left = 101
        Top = 7
        Caption = '- A&rbol'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = btnEliminarDeArbolClick
      end
    end
  end
  object panListaPreguntas: TPanel
    Left = 0
    Top = 0
    Width = 306
    Height = 556
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 1
    Constraints.MinWidth = 306
    TabOrder = 1
    object dblPreguntas: TDBListEx
      Left = 1
      Top = 259
      Width = 304
      Height = 255
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 300
          Header.Caption = 'Preguntas/Respuestas'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Descripcion'
        end>
      DataSource = dsPreguntas
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dblPreguntasDrawText
    end
    object DBListEx2: TDBListEx
      Left = 1
      Top = 118
      Width = 304
      Height = 141
      Align = alTop
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 300
          Header.Caption = 'Subt'#237'tulos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'SubTitulo'
        end>
      DataSource = dsSubTitulos
      DragReorder = True
      ParentColor = False
      TabOrder = 1
      TabStop = True
      OnDrawText = dblPreguntasDrawText
    end
    object panABM: TPanel
      Left = 1
      Top = 514
      Width = 304
      Height = 41
      Align = alBottom
      BorderStyle = bsSingle
      TabOrder = 2
      object btnABMSubTitulos: TDPSButton
        Left = 107
        Top = 6
        Width = 85
        Caption = 'ABM &Subtitulos'
        TabOrder = 0
        OnClick = btnABMSubTitulosClick
      end
      object btnABMPreguntas: TDPSButton
        Left = 205
        Top = 6
        Width = 85
        Caption = 'ABM &Preguntas'
        TabOrder = 1
        OnClick = btnABMPreguntasClick
      end
      object btnABMTitulos: TDPSButton
        Left = 10
        Top = 6
        Width = 85
        Caption = 'ABM &T'#237'tulos'
        TabOrder = 2
        OnClick = btnABMTitulosClick
      end
    end
    object DPSGrid1: TDPSGrid
      Left = 1
      Top = 1
      Width = 304
      Height = 117
      Align = alTop
      DataSource = dsTitulos
      Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Titulo'
          Title.Caption = 'T'#237'tulos'
          Width = 300
          Visible = True
        end>
    end
  end
  object dsTitulosFAQ: TDataSource
    DataSet = ObtenerTitulosFAQ
    Left = 106
    Top = 180
  end
  object dsPreguntasFAQ: TDataSource
    DataSet = ObtenerPreguntasFAQTitulo
    Left = 333
    Top = 309
  end
  object Timer: TTimer
    Interval = 300000
    OnTimer = TimerTimer
    Left = 330
    Top = 345
  end
  object ActualizarEstadisticaFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadisticaFAQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 363
    Top = 345
  end
  object ObtenerTitulosFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTitulosFAQ'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 138
    Top = 179
  end
  object ObtenerSubTitulosFAQ: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    DataSource = dsTitulosFAQ
    ProcedureName = 'ObtenerSubTitulosFAQ'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 138
    Top = 211
  end
  object ObtenerPreguntasFAQTitulo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerPreguntasFAQTitulo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSubtitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsoWEB'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Prepared = True
    Left = 365
    Top = 308
  end
  object qryPreguntas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      
        #9'P.CodigoPregunta, P.Descripcion, (select count (CodigoPregunta)' +
        ' from FAQArbol A where A.CodigoPregunta = P.CodigoPregunta) As C' +
        'antidad'
      'from'
      #9'FAQPreguntas P'
      'where'
      #9'P.UsoWEB = 0'
      'order by'
      #9'P.Descripcion'
      '')
    Left = 693
    Top = 366
  end
  object dsPreguntas: TDataSource
    DataSet = qryPreguntas
    Left = 663
    Top = 366
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 663
    Top = 63
  end
  object qryTitulos: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      
        #9'T.CodigoTitulo, T.Descripcion As Titulo, (select count (CodigoT' +
        'itulo) from FAQArbol A where A.CodigoTitulo = T.CodigoTitulo) As' +
        ' Cantidad'
      'from'
      #9'FAQTitulos T'
      'order by'
      #9'T.Descripcion'
      '')
    Left = 693
    Top = 63
  end
  object dsSubTitulos: TDataSource
    DataSet = qrySubTitulos
    Left = 663
    Top = 216
  end
  object qrySubTitulos: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      
        #9'ST.CodigoSubTitulo, ST.Descripcion As SubTitulo, (select count ' +
        '(CodigoSubTitulo) from FAQArbol A where A.CodigoSubTitulo = ST.C' +
        'odigoSubTitulo) As Cantidad'
      'from'
      #9'FAQSubTitulos ST'
      'order by'
      #9'ST.Descripcion')
    Left = 693
    Top = 216
  end
  object qryExistenTSP: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoTitulo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoSubTitulo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoPregunta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9'*'
      'from'
      #9'VW_FAQS'
      'where'
      #9'(CodigoTitulo = :CodigoTitulo) and'
      #9'(CodigoSubTitulo = :CodigoSubTitulo) and'
      #9'(CodigoPregunta = :CodigoPregunta)')
    Left = 87
    Top = 279
  end
  object qryEliminarTSP: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoTitulo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoSubTitulo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoPregunta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'delete'
      #9'FAQArbol'
      'where'
      #9'(CodigoTitulo = :CodigoTitulo) and'
      #9'(CodigoSubTitulo = :CodigoSubTitulo) and'
      #9'(CodigoPregunta = :CodigoPregunta)')
    Left = 87
    Top = 339
  end
  object qryAgregarTSP: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoTitulo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoSubTitulo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoPregunta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'insert into FAQArbol values (:CodigoTitulo, :CodigoSubTitulo, :C' +
        'odigoPregunta)')
    Left = 87
    Top = 309
  end
end
