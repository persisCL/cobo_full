{-----------------------------------------------------------------------------
 Unit Name: FormConfirmarImpresion.pas
 Author:    FSandi
 Date Created: 30/03/2007
 Language: ES-AR
 Description: Formulario que se usa para la confirmacion de la Impresión de las
            modificaciones de un convenio.

 Revision : 1
 Author : nefernandez
 Date : 06/11/2007
 Description : SS 622: Se quita el boton de cerrar del formulario
-----------------------------------------------------------------------------}
unit FormConfirmarImpresion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmConfirmarImpresion = class(TForm)
    txtEstado: TLabel;
    btnConfirmar: TButton;
    btnCancelar: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
     function Inicializar(): boolean;
  end;

var
  frmConfirmarImpresion: TfrmConfirmarImpresion;

implementation

{$R *.dfm}

{ TfrmConfirmarImpresion }

function TfrmConfirmarImpresion.Inicializar(): boolean;
begin
     Result := True;
end;

end.
