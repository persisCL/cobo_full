{********************************** File Header ********************************
File Name : ExplorarInfracciones.pas
Author : gcasais
Date Created: 16/07/2005
Language : ES-AR
Description : M�dulo que permite explorar las infracciones y editarlas

 Revision : 1
    Author   : pdominguez
    Date     : 06/05/2010
    Description:  Infracciones Fase 2
        - Se modificaron / a�adieron los siguientes objetos / Varibales / Constantes
                cbConcesionaria,
            cbEstadoInterno,
            cbEstadoExterno: TVariantComboBox.
            spObtenerEstadoExternoInfraccion,
            spObtenerEstadoInternoInfraccion: TADOStoredProc.
            Par�metro @CodigoConcesionara al objeto spExplorarInfracciones: TADOStoredProc.
            Se a�ade la columna UsuarioAnulacion al objeto dbInfracciones: TDBListEx.

        - Se modificaron / a�adieron los siguientes Procedimientos / Funciones
            CargarEstado,
            cbEstadoInternoChange
            Limpiar
            btnFiltrarClick

 Revision : 2
     Author   : pdominguez
     Date     : 06/06/2010
     Descrption: Infractores Fase 2
        - se modificaron los siguientes Procedimientos / Funciones:
            btnFiltrarClick

Firma       :   SS_660_CQU_20140310
Descripcion :   Se agrega c�digo faltante para desplegar el lblListaAmarilla

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

*******************************************************************************}
unit ExplorarInfracciones;

interface

uses
  //Explorar Infracciones
  DMConnection,
  Util,
  UtilProc,
  UtilDB,
  PeaProcs,
  PeaTypes,
  frmEdicionInfraccion,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, ExtCtrls, CollPnl, StdCtrls, DB, ADODB,
  VariantComboBox, Validate, DateEdit, DmiCtrls, CSVUtils;                      // TASK_147_MGO_20170309

type
  TfrmExplorarInfraciones = class(TForm)
    pnlDefaultFilter: TPanel;
    btnEditar: TButton;
    btnCerrar: TButton;
    dbInfracciones: TDBListEx;
    spExplorarInfracciones: TADOStoredProc;
    dsExplorarInfracciones: TDataSource;
    spObtenerEstadoExternoInfraccion: TADOStoredProc;
    gbFiltros: TGroupBox;
    lblCapPatente: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtPatente: TEdit;
    txtDesde: TDateEdit;
    txtHasta: TDateEdit;
    cbEstadoInterno: TVariantComboBox;
    cbMotivoAnulacion: TVariantComboBox;
    txtTop: TNumericEdit;
    btnFiltrar: TButton;
    btnLimpiar: TButton;
    chbNoIncluidasXML: TCheckBox;
    cbConcesionaria: TVariantComboBox;
    lblConcesionaria: TLabel;
    Label6: TLabel;
    cbEstadoExterno: TVariantComboBox;
    spObtenerEstadoInternoInfraccion: TADOStoredProc;
    btnExportarCSV: TButton;
    dlgSaveCSV: TSaveDialog;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);
    procedure cbEstadoInternoChange(Sender: TObject);
    procedure dbInfraccionesDblClick(Sender: TObject);
    procedure dbInfraccionesColumns0HeaderClick(Sender: TObject);
    procedure btnExportarCSVClick(Sender: TObject);
  private
    // C�digo del Sistema desde el cual se llam� al form.
    FCodigoSistema: Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure CargarEstado;
    procedure Limpiar;
    { Private declarations }
  public
    function Inicializar(CodigoSistema: Integer): Boolean;
  end;

var
  frmExplorarInfraciones: TfrmExplorarInfraciones;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: TfrmExplorarInfraciones.Inicializar
Author : gcasais
Date Created : 16/07/2005
Description :  Inicializaci�n de Este formulario
Parameters : None
Return Value : Boolean

*******************************************************************************}
function TfrmExplorarInfraciones.Inicializar(CodigoSistema: Integer): Boolean;
var
    S : TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    Limpiar;
    FCodigoSistema := CodigoSistema;

    // INICIO : TASK_147_MGO_20170309
    if ObtenerParametroGeneralMulticoncesionHabilitada(DMConnections.BaseCAC) then begin
        lblConcesionaria.Visible := True;
        cbConcesionaria.Visible := True;
    end else begin
        lblConcesionaria.Visible := False;
        cbConcesionaria.Visible := False;
    end;

    btnExportarCSV.Enabled := False;
    // FIN : TASK_147_MGO_20170309

    Result := True;
end;

{
    Function Name: CargarEstado
    Parameters : None
    Author :
    Date Created :

    Revision : 1
        Author : pdominguez
        Date Created : 09/05/2010
        Description : Infractores Fase 2
            - Carga los Estados de Infracciones en los Combos.
}
procedure TfrmExplorarInfraciones.CargarEstado;
    procedure CargaComboEstadosInfracciones(OrigenDatos: TADOStoredProc; Combo: TVariantComboBox; CampoClave: String);
    begin
        With OrigenDatos, Combo do begin
            try
                Open;
                Clear;
                Items.Add(SIN_ESPECIFICAR, -1);
                while not Eof do begin
                    Items.Add(FieldByName('Descripcion').AsString, FieldByName(CampoClave).AsInteger);
                    Next;
                end;
            finally
                if Active then Close;
                ItemIndex := 0;
            end;
        end;
    end;
begin
    CargaComboEstadosInfracciones(spObtenerEstadoExternoInfraccion, cbEstadoExterno, 'CodigoEstadoExterno');
    CargaComboEstadosInfracciones(spObtenerEstadoInternoInfraccion, cbEstadoInterno, 'CodigoEstadoInterno');
end;

{
    Function Name: Limpiar
    Parameters : None
    Author : ndonadio
    Date Created : 18/07/2005

    Description : Limpia el formulario.

    Revision : 1
        Author: pdominguez
        Date: 09/05/2010
        Description: Infractores Fase 2
            - Se inicializa el combo de Concesionarias.
}
procedure TfrmExplorarInfraciones.Limpiar;
var
    i: integer;
begin
    Try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        txtPatente.Clear;
        txtDesde.Clear;
        txtHasta.Date := NowBase(DMConnections.BaseCAC);
        txtDesde.Date := NowBase(DMConnections.BaseCAC) - 30;
        txtTop.Value := 100;
            spExplorarInfracciones.Close;
        // Saco las marcas de Ordenamiento de las columnas
        for i := 0 to dbInfracciones.Columns.Count -1 do begin
                   dbInfracciones.Columns[i].Sorting := csNone;
        end;
        // Cargo los combos
        CargarEstado;
        CargarComboMotivoAnulacion(DMConnections.BaseCAC, cbMotivoAnulacion, True);
        CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria, True); // Rev. 1 (Fase 2)
        cbEstadoInternoChange(Self);
        btnEditar.Enabled := False;
        chbNoIncluidasXML.Checked := False;
        txtPatente.SetFocus;
    Finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    End;
end;

{
    Function Name: cbEstadoInternoChange
    Parameters : Sender: TObject
    Author :
    Date Created :

    Revision : 1
        Author : pdominguez
        Date Created : 09/05/2010
        Description : Infractores Fase 2
            - Maneja el combo de Motivos de Anulaci�n, dependiendo de los estados seleccionados.
}
procedure TfrmExplorarInfraciones.cbEstadoInternoChange(Sender: TObject);
begin
    if (cbEstadoInterno.Items[cbEstadoInterno.ItemIndex].Value = CONST_ESTADO_INFRACCION_INTERNO_ANULADA) or
        (cbEstadoExterno.Items[cbEstadoexterno.ItemIndex].Value = CONST_ESTADO_INFRACCION_EXTERNO_ANULADA) then begin
        if not cbMotivoAnulacion.Enabled then begin
            cbMotivoAnulacion.Enabled := True;
            cbMotivoAnulacion.ItemIndex := 0;
        end;
    end
    else begin
        cbMotivoAnulacion.ItemIndex := -1;
        cbMotivoAnulacion.Enabled := False;
    end;
end;

{
    Function Name: btnBuscarClick
    Parameters: Sender: TObject
    Author:
    Date Created:

    Revision : 1
        Author : pdominguez
        Date : 09/05/2010
        Description : Infractores Fase 2
            - Se a�aden las nuevas condiciones para la b�squeda filtrada.

    Revision : 2
        Author : pdominguez
        Date : 06/06/2010
        Description : Infractores Fase 2
            - Se atienden las sugerencias de Test KTC CL:
                - Se cambia el t�tulo de la columna concesionaria
                - Se revisa el error reportado y se incluye el bloque de c�digo dentro del bloque protegido
                de Excepci�n.
                - Se a�ade un mensaje de aviso en el caso de que la consulta no devuelva resultados.
}
procedure TfrmExplorarInfraciones.btnFiltrarClick(Sender: TObject);
    resourcestring
        MSG_ERROR_LOADING_DATA  = 'No se pueden cargar las infracciones';
        MSG_ERROR_QUANTITY_TO_SHOW_TO_BE_MAJOR_OR_EQUAL_TO_ONE = 'La cantidad de infracciones a mostrar debe ser mayor o igual a uno (1).';
        MSG_FALTA_FECHA_DESDE = 'Ingrese una fecha desde para el filtro';
        MSG_FALTA_FECHA_HASTA = 'Ingrese una fecha hasta para el filtro';
        MSG_VALIDAR_ORDEN_FECHA = 'La fecha desde debe ser menor que la fecha hasta.';
        MSG_SIN_RESULTADOS = 'La Consulta con los filtros especificados, no retorn� datos.';
        MSG_SIN_RESULTADOS_CAPTION = ' Aviso resultado Consulta';
    const
        STR_VALIDATION_ERROR = 'Error de Validaci�n';
begin
    if not ValidateControls([txtTop],[(txtTop.ValueInt > 0)], STR_VALIDATION_ERROR,[(MSG_ERROR_QUANTITY_TO_SHOW_TO_BE_MAJOR_OR_EQUAL_TO_ONE)]) then Exit;

    // Revision 1: Validamos las fechas ingresadas en los filtros de busqueda.
    //Validamos las fechas:

    if (txtDesde.Date = NullDate) then begin
        MsgBoxBalloon(MSG_FALTA_FECHA_DESDE, Caption, MB_ICONSTOP, txtDesde);
        txtDesde.SetFocus;
        Exit;
    end;

    if (txtHasta.Date = NullDate) then begin
        MsgBoxBalloon(MSG_FALTA_FECHA_HASTA, Caption, MB_ICONSTOP, txtHasta);
        txtHasta.SetFocus;
        Exit;
    end;

    if (txtDesde.Date <> NullDate) and (txtHasta.Date <> NullDate) and (txtDesde.Date > txtHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA, Caption, MB_ICONSTOP, txtDesde);
        txtDesde.SetFocus;
        Exit;
    end;

    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            with spExplorarInfracciones, spExplorarInfracciones.Parameters do begin
                if Active then Close;
                
                Refresh;
                ParamByName('@Patente').Value := iif(Trim(txtPatente.Text) = '', NULL, txtPatente.Text); // Rev. 1 (Fase 2)
                ParamByName('@Desde').Value :=   txtDesde.Date;
                ParamByName('@Hasta').Value :=   txtHasta.Date;
                // Rev. 1 (Fase 2)
                ParamByName('@CodigoEstadoExterno').Value := AsignarValorComboEstado(cbEstadoExterno);
                ParamByName('@CodigoEstadoInterno').Value := AsignarValorComboEstado(cbEstadoInterno);
                if cbConcesionaria.Visible then     // TASK_147_MGO_20170309
                    ParamByName('@CodigoConcesionaria').Value := AsignarValorComboEstado(cbConcesionaria);
                // Fin Rev. 1 (Fase 2)
                if (not cbMotivoAnulacion.Enabled) or (cbMotivoAnulacion.ItemIndex = 0) then
                    ParamByName('@CodigoMotivoAnulacion').Value := NULL
                else ParamByName('@CodigoMotivoAnulacion').Value := cbMotivoAnulacion.Items[cbMotivoAnulacion.ItemIndex].Value;
        
                ParamByName('@EnviadoXML').Value := iif( chbNoIncluidasXML.Checked = True, 1, NULL);
                ParamByName('@TOP').Value := txtTop.ValueInt;
                Open;
            end;

            ActiveControl := dbInfracciones;
        except
            on E : Exception do begin
                Cursor := crDefault;
                //Informo que no se pudieron cargar las infracciones
                MsgBoxErr(MSG_ERROR_LOADING_DATA, E.Message, caption, MB_ICONERROR)
            end;
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;

        if spExplorarInfracciones.IsEmpty then MsgBox(MSG_SIN_RESULTADOS, MSG_SIN_RESULTADOS_CAPTION, MB_ICONINFORMATION);

        btnEditar.Enabled := (not spExplorarInfracciones.IsEmpty) and (FCodigoSistema <> SYS_CAC);
        btnExportarCSV.Enabled := (not spExplorarInfracciones.IsEmpty);         // TASK_147_MGO_20170309
    end;
end;

{******************************** Function Header ******************************
Function Name: btnLimpiarClick
Author : ndonadio
Date Created : 18/07/2005
Description : Llama a la rutina para limpiar el form
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmExplorarInfraciones.btnLimpiarClick(Sender: TObject);
begin
    Limpiar;
end;

{******************************** Function Header ******************************
Function Name: dbInfraccionesColumns0HeaderClick
Author : ndonadio
Date Created : 18/07/2005
Description : Permite ordenar las columnas de la grilla
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmExplorarInfraciones.dbInfraccionesColumns0HeaderClick(Sender: TObject);
begin
    //si no hay infracciones salgo
    if not spExplorarInfracciones.Active then Exit;

    //Ordeno la grilla
    case TDBListExColumn(Sender).Sorting of
        csNone, csDescending :
            begin
                //si esta en Ninguno o Descendiente
                TDBListExColumn(Sender).Sorting := csAscending;
                //Aplico el filtro sobre el sp
                spExplorarInfracciones.Sort := TDBListExColumn(Sender).FieldName;
            end;
        csAscending :
            begin
                //si estaba en Ascendence
                TDBListExColumn(Sender).Sorting := csDescending;
                //Aplico el filtro sobre el sp
                spExplorarInfracciones.Sort := TDBListExColumn(Sender).FieldName + ' DESC';
            end;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnEditarClick
Author : gcasais
Date Created : 16/07/2005
Description : Permite editar una infracci�n 
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmExplorarInfraciones.btnEditarClick(Sender: TObject);
var
    F : TFormEdicionInfraccion;
begin
    if FindFormOrCreate(TformEdicionInfraccion, F) then begin
        F.Show;
    end else begin
        //if not F.Inicializar(spExplorarInfracciones.FieldByName('CodigoInfraccion').AsInteger) then F.Release;    // SS_660_CQU_20140310
        if not F.Inicializar(spExplorarInfracciones.FieldByName('CodigoInfraccion').AsInteger,                      // SS_660_CQU_20140310
                             spExplorarInfracciones.FieldByName('CodigoTipoInfraccion').AsInteger                   // SS_660_CQU_20140310
        ) then F.Release;                                                                                           // SS_660_CQU_20140310
    end;
end;

// INICIO : TASK_147_MGO_20170309
procedure TfrmExplorarInfraciones.btnExportarCSVClick(Sender: TObject);
resourcestring
    MSG_ERROR_EXPORTAR = 'Ha ocurrido un error al exportar los registros';
    STR_FILENAME = 'Infracciones_%s_%s';
    MSG_TITLE = 'Exportar Infracciones a CSV';
    MSG_SUCCESS = 'El archivo %s ha sido creado exitosamente';
var
    FileBuffer, Error: String;
    NombreArchivo: String;
begin
    try
        Screen.Cursor := crHourGlass;
        spExplorarInfracciones.DisableControls;
        try
            if DatasetToExcelCSV(spExplorarInfracciones, FileBuffer, Error) then begin
                dlgSaveCSV.Title := 'Guardar Como...';
                NombreArchivo := Format(STR_FILENAME, [txtDesde.Text, txtHasta.Text]);

                dlgSaveCSV.FileName := NombreArchivo;

                Screen.Cursor := crDefault;
                if dlgSaveCSV.Execute then begin
                    StringToFile(FileBuffer, dlgSaveCSV.FileName);
                    MsgBox(Format(MSG_SUCCESS, [dlgSaveCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
                end;
            end else begin
                raise Exception.Create(Error);
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_EXPORTAR, e.Message, MSG_TITLE, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
        spExplorarInfracciones.EnableControls;
    end;
end;
// FIN : TASK_147_MGO_20170309

{******************************** Function Header ******************************
Function Name: dbInfraccionesDblClick
Author : ndonadio
Date Created : 18/07/2005
Description :  Permite editar la infraccion al hacer dobleclick en la grilla
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmExplorarInfraciones.dbInfraccionesDblClick(Sender: TObject);
begin
    if btnEditar.Enabled then btnEditarClick(self);
end;

{******************************** Function Header ******************************
Function Name: btnCerrarClick
Author : lgisuk
Date Created : 06/06/2006
Description :  Permito cerrar el formulario
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmExplorarInfraciones.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 06/06/2006
Description :  Lo libero de memoria
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmExplorarInfraciones.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
