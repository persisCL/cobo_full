object frmMsg3Botones: TfrmMsg3Botones
  Left = 329
  Top = 198
  BorderStyle = bsDialog
  ClientHeight = 117
  ClientWidth = 419
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Mensaje: TLabel
    Left = 30
    Top = 15
    Width = 358
    Height = 64
    AutoSize = False
    Caption = 'Mensaje'
    WordWrap = True
  end
  object btn1: TButton
    Left = 27
    Top = 84
    Width = 97
    Height = 25
    ModalResult = 6
    TabOrder = 0
  end
  object btn2: TButton
    Left = 161
    Top = 84
    Width = 97
    Height = 25
    ModalResult = 7
    TabOrder = 1
  end
  object btn3: TButton
    Left = 297
    Top = 84
    Width = 97
    Height = 25
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
  end
end
