{-------------------------------------------------------------------------------
 File Name: FrmReclamoDesconoceInfraccion.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos

  Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 29-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y SubTipoReclamo

 
 Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos. 
-------------------------------------------------------------------------------}
unit FrmReclamoTransitoDesconoceInfraccion;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  FreContactoReclamo,             //Frame contacto
  FreCompromisoOrdenServicio,     //Frame compromiso
  FreSolucionOrdenServicio,       //Frame solucion
  FreHistoriaOrdenServicio,       //Frame Historia
  FreFuenteReclamoOrdenServicio,  //Frame Fuente Reclamo
  FreNumeroOrdenServicio,         //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,      //Frame Respuesta Orden Servicio  
  ImagenesTransitos,              //Muestra la Ventana con la imagen
  FrmTransitosReclamadosAcciones, //Acciones que se hicieron a los transitos al cerrar el reclamo
  OrdenesServicio,                //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Validate,
  DateEdit, Menus, PeaTypes, {FreSubTipoReclamoOrdenServicio,}                  // _PAN_
  FreConcesionariaReclamoOrdenServicio;

type
  TFormReclamoTransitoDesconoceInfraccion = class(TForm)
    PageControl: TPageControl;
    TabSheet1: TTabSheet;
    txtDetalle: TMemo;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio;
    Label3: TLabel;
    DataSource: TDataSource;
    DBLTransitos: TDBListEx;
    lnCheck: TImageList;
    spObtenerOrdenServicio: TADOStoredProc;
    spObtenerTransitosReclamo: TADOStoredProc;
    CBMotivosDesconoceInfraccion: TVariantComboBox;
    LJustificacion: TLabel;
    PDatosJustificacion: TPanel;
    PPaseDiario: TPanel;
    LNumeroSerie: TLabel;
    ENumeroSerie: TEdit;
    nbDatosJustificacion: TNotebook;
    Label1: TLabel;
    EFechaCompra: TDateEdit;
    EPatente: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    LEtiqueta: TLabel;
    LConcesionaria: TLabel;
    LNumeroConvenio: TLabel;
    EEtiqueta: TEdit;
    Enumeroconvenio: TEdit;
    cbCategoria: TVariantComboBox;
    CBConcesionaria: TVariantComboBox;
    spObtenerOrdenServicioDesconoceInfraccion: TADOStoredProc;
    PopupResolver: TPopupMenu;
    mnu_Resolver: TMenuItem;
    Mnu_Pendiente: TMenuItem;
    Mnu_Aceptado: TMenuItem;
    Mnu_Denegado: TMenuItem;
    Mnu_Comun: TMenuItem;
    PAbajo: TPanel;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    SPAnularTransitoInfraccion: TADOStoredProc;
    ilCamara: TImageList;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
{    FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;}     // _PAN_
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBLTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure CBMotivosDesconoceInfraccionSelect(Sender: TObject);
    procedure DBLTransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
    procedure Mnu_ComunClick(Sender: TObject);
    procedure DBLTransitosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
  private
    { Private declarations }
    FTipoOrdenServicio : Integer;
    FCodigoOrdenServicio : Integer;
    FListaTransitos : TStringList;
    FEditando : Boolean; //Indica si estoy editando o no
    function  GetCodigoOrdenServicio : integer;
    Function  ListaTransitosInclude(Lista : TStringList ; Valor : String) : Boolean;
    Function  ListaTransitosExclude(Lista : TStringList ; Valor : String) : Boolean;
    Function  ListaTransitosIn(Lista : TStringList ; Valor : String) : Boolean;
    Function  ListaTransitosEmpresaIn(Valor : String; Var Resolucion : Integer) : Boolean;
    Function  ValidarEstadoTransitos : Boolean;
  public
    { Public declarations }
    function Inicializar(TipoOrdenServicio: Integer; CodigoConvenio: Integer; ListaTransitos : TStringList ) : Boolean;
    function InicializarEditando(CodigoOrdenServicio : Integer): Boolean;
    function InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
    property CodigoOrdenServicio: Integer read GetCodigoOrdenServicio;
  end;

var
  FormReclamoTransitoDesconoceInfraccion : TFormReclamoTransitoDesconoceInfraccion;
  FListaTransitosCliente : TStringList; //Guarda los transitos que el usuario no rechaza
  FListaTransitosEmpresa : TStringList; //Guarda la resolucion de la empresa sobre cada transito

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicilizaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoTransitoDesconoceInfraccion.Inicializar(TipoOrdenServicio: Integer; CodigoConvenio: Integer; ListaTransitos:tstringlist ): Boolean;

    //Cargo la grilla de reclamos
    Function ObtenerDatosTransitos(Transitos : String) : Boolean;

        //Obtengo la Enumeraci�n
        function ObtenerEnumeracion(Lista : TStringList): String;
        var
            Enum: AnsiString;
            i: Integer;
        begin
            Enum := '';
            for i:= 0 to Lista.Count - 1 do begin
                //Enum := Enum + Lista.Strings[i] + ',';
                Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
            end;
            Delete(Enum, Length(Enum), 1);
            Result := Enum;
        end;

    begin
        with SPObtenerTransitosReclamo do begin
            Close;
            Parameters.ParamByName('@CodigoOrdenServicio').Value := NULL;
            Parameters.ParamByName('@Enumeracion').Value := ObtenerEnumeracion(ListaTransitos);
            Open;
        end;
        Result := True;
    end;

    //cargo el combo con motivos desconoce infraccion
    procedure CargarMotivosDesconoceInfraccion(Combo : TVariantComboBox);
    const
        CONST_NINGUNO = 'Ninguna';
    var
        SP: TAdoStoredProc;
    begin
        try
            Combo.Items.Clear;
            Combo.Items.Add(CONST_NINGUNO, 0);
            try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerMotivosDesconoceInfraccion';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('CodigoMotivoDesconoceInfraccion').asinteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex := 0;
            except
            end;
        finally
            FreeAndNil(SP);
        end;
    end;

      //cargo el combo con las categorias
    Procedure CargarCategorias(Combo : TVariantComboBox);
    Const
        CONST_NINGUNO = 'Ninguno';
    Var
        SP: TAdoStoredProc;
    Begin
        try
            Combo.Items.Clear;
            Combo.Items.Add(CONST_NINGUNO, 0);
            try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerCategorias';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('Categoria').asinteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex := 0;
            except
            end;
        finally
            FreeAndNil(SP);
        end;
    end;

    //cargo el combo con las Concesionarias
    procedure CargarConcesionarias(Combo : TVariantComboBox);
    const
        CONST_NINGUNO = 'Ninguno';
    var
        SP: TAdoStoredProc;
    begin
        try
            Combo.Items.Clear;
            Combo.Items.Add(CONST_NINGUNO, 0);
            try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerConcesionarias';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('CodigoConcesionaria').asinteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex := 0;
            except
            end;
        finally
            FreeAndNil(SP);
        end;
    end;

Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo Desconoce Infracci�n';
    MSG_ERROR = 'Error';
Const
    STR_RECLAMO = 'Reclamo de Transito - ';
Var
  	Sz : TSize;
begin
    FTipoOrdenServicio := TipoOrdenServicio;
    try

        //si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaTransitos <> nil then begin
            FListaTransitos := ListaTransitos;
            //cargo la grilla con los transitos
            ObtenerDatosTransitos(ListaTransitos.Text);
            //obtengo el codigo de convenio
            CodigoConvenio := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.ObtenerCodigoConvenioTransito (' + spObtenerTransitosReclamo.FieldByName('NumCorrCa').AsString + ')'  );
            //la empresa aun no resolvio nada es la primera vez
            FListaTransitosEmpresa.Clear;
        end else begin
            //Creo la lista la voy a cargar una consulta
            FListaTransitos := TStringList.Create;
        end;

        //Centro el form
        CenterForm(Self);
        //ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);
        //titulo
        Caption := STR_RECLAMO + QueryGetValue(DMConnections.BaseCAC, Format(
                   'select dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
                   [TipoOrdenServicio]));
        PageControl.ActivePageIndex := 0;

        ActiveControl := txtDetalle;

        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(10) and
                                FrameSolucionOrdenServicio1.Inicializar and
                                        FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar{ and        // _PAN_
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar};             // _PAN_
                                        //---------------------------------------------------------------

        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    CargarMotivosDesconoceInfraccion(cbMotivosDesconoceInfraccion);
    CargarCategorias(cbCategoria);
    CargarConcesionarias(cbConcesionaria);
    FrameNumeroOrdenServicio1.Visible := False;
    //No estoy Editando
    FEditando := False;
    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso := ObtenerFechaComprometidaOS(now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, FrameCompromisoOrdenServicio1.Prioridad);
    //
    PDatosJustificacion.Visible := False;
    AddOSForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:  DDeMarco
  Date Created:
  Description: Modifico Reclamo que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransitoDesconoceInfraccion.InicializarEditando(CodigoOrdenServicio: Integer): Boolean;

    //form read Only
    Procedure ReadOnly;
    begin
      DblTransitos.PopupMenu := Nil;
      CbMotivosDesconoceInfraccion.Enabled := False;
      PDatosJustificacion.Enabled := False;
      //comunes
      FrameContactoReclamo1.Deshabilitar;
      FrameCompromisoOrdenServicio1.Enabled := False;
      FrameSolucionordenServicio1.Enabled := False;
      FrameRespuestaOrdenServicio1.Deshabilitar;
      TxtDetalle.ReadOnly := True;
      TxtDetalleSolucion.ReadOnly := True;
      CKRetenerOrden.Visible := False;
      BtnCancelar.Caption := 'Salir';
      AceptarBtn.Visible := False;
    end;

    Function ObtenerDatosTransitos(CodigoOrdenServicio : Integer) : Boolean;

        Function ObtenerTransitos(CodigoOrdenServicio : Integer) : Boolean;
        begin
           FListaTransitos.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              First;
              while not Eof do begin
                  FListaTransitos.Add(FieldByName('NumCorrCa').AsString);
                  Next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

        Function ObtenerTransitosAceptadosCliente(CodigoOrdenServicio:integer):boolean;
        Var
            NumCorrCa : String;
            Rechaza : Boolean;
        begin
           FListaTransitosCliente.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              First;
              while not eof do begin
                  NumCorrCa := FieldByName('NumCorrCa').AsString;
                  Rechaza := QueryGetValue(DMconnections.BaseCAC,'SELECT dbo.ObtenerRechazaOSTransito(' + IntToStr(CodigoOrdenServicio) + ',' + NumCorrCa +')'  ) = 'True';
                  if not Rechaza then ListaTransitosInclude(FListaTransitosCliente, NumCorrCa);
                  Next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

        Function ObtenerResolucionEmpresa(CodigoOrdenServicio : Integer) : Boolean;
        var
            NumCorrCA : String;
            CodigoResolucion : String;
            Bloque : String;
        begin
           FListaTransitosEmpresa.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              First;
              while not Eof do begin
                  NumCorrCA := FieldByName('NumCorrCa').AsString;
                  CodigoResolucion := FieldByName('CodigoResolucionReclamoTransito').AsString;
                  CodigoResolucion := IIF(CodigoResolucion = '', '1', CodigoResolucion);
                  Bloque := NumCorrCa + ';' + CodigoResolucion;
                  if CodigoResolucion <> '1' then ListaTransitosInclude(FListaTransitosEmpresa,Bloque);
                  Next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

    begin

        With spObtenerTransitosReclamo do begin
            close;
            parameters.ParamByName('@CodigoOrdenServicio').value := CodigoOrdenServicio;
            parameters.ParamByName('@Enumeracion').value := NULL;
            open;
        end;

        ObtenerTransitos(CodigoOrdenServicio);
        ObtenerTransitosAceptadosCliente(CodigoOrdenServicio);
        ObtenerResolucionEmpresa(CodigoOrdenServicio);

        Result := True;
    end;

var
    Estado: String;
begin
    FCodigoOrdenServicio := CodigoOrdenServicio;
    spObtenerOrdenServicio.Parameters.Refresh;
    spObtenerOrdenServicio.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    Result := OpenTables([spObtenerOrdenServicio]);
    if not Result then Exit;

    // Cargamos los datos
    Result := Inicializar(spObtenerOrdenServicio.FieldByName('TipoOrdenServicio').AsInteger, 0, Nil);

    TxtDetalle.Text := spObtenerOrdenServicio.FieldByName('ObservacionesSolicitante').AsString;
    TxtDetalleSolucion.Text := spObtenerOrdenServicio.FieldByName('ObservacionesEjecutante').AsString;

    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    
    //cargo la grilla con los transitos objetados
    ObtenerDatosTransitos(CodigoOrdenServicio);

    //obtengo la historia para esa orden de servicio
    FrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio);

    //obtengo la fuente del reclamo
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //Obtengo el Estado de la OS para saber si permito editar o read only
    Estado:=spObtenerOrdenServicio.FieldByName('Estado').AsString;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;

    // Listo
    spObtenerOrdenServicio.Close;

    //Obtengo la justificaci�n
    with spObtenerOrdenServicioDesconoceInfraccion do
    begin
      	Close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
        Open;
        if not Eof then begin
          	 cbMotivosDesconoceInfraccion.Value := FieldByName('CodigoMotivoDesconoceInfraccion').AsInteger;
             CBMotivosDesconoceInfraccionSelect(Nil);
             ENumeroSerie.Text := FieldByName('NumeroSerie').AsString;
             EFechaCompra.Date := FieldByName('FechaCompra').AsDatetime;
             CbCategoria.Value := FieldByName('CodigoCategoria').AsInteger;
             EPatente.Text := FieldByName('Patente').AsString;
             EEtiqueta.Text := FieldByName('Etiqueta').AsString;
             CbConcesionaria.Value := FieldByName('CodigoConcesionaria').AsInteger;
             ENumeroConvenio.Text := FieldByName('NumeroConvenio').AsString;
        end;
        close;
    end;

    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(IntToStr(CodigoOrdenServicio));
    //por Default no retengo la orden de servicio
    CKRetenerOrden.Checked := False;
    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;
    //Estoy Editando
    FEditando := True;
end;


{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:  DDeMarco
  Date Created:
  Description: Soluciono un Reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoTransitoDesconoceInfraccion.InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.ActivePageIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormReclamoTransitoDesconoceInfraccion.GetCodigoOrdenServicio : Integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosInclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: agrego un valor a la lista de transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransitoDesconoceInfraccion.ListaTransitosInclude(Lista : TStringList ; Valor : String ) : Boolean;
begin
    Result := False;
    try
        if not ListaTransitosIn(Lista, Valor) then Lista.Add(Valor);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosExclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: quito un valor a la lista de Transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransitoDesconoceInfraccion.ListaTransitosExclude( Lista : TStringList ; Valor : String) : Boolean;
var
    Index : Integer;
begin
    Result := False;
    try
        Index := Lista.IndexOf(Valor);
        if Index <> -1 then Lista.Delete(Index);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosIn
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: Devuelve si un valor esta en la lista transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransitoDesconoceInfraccion.ListaTransitosIn( Lista : TStringList ; Valor : String) : Boolean;
begin
    Result := not (Lista.IndexOf(Valor) = -1);
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosEmpresaIn
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: busca un transito, se fija si esta y trae su resolucion
  Parameters: valor:string; var Resolucion:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransitoDesconoceInfraccion.ListaTransitosEmpresaIn(Valor : String; Var Resolucion : Integer) : Boolean;
var
    I : Integer;
begin
    Result := False;
    Resolucion := 1;
    I := 0;
    while I <= FListaTransitosEmpresa.Count-1 do
    begin
        if ParseParamByNumber(FListaTransitosEmpresa.Strings[I],1,';') = Valor then begin
            Resolucion := StrToInt(ParseParamByNumber(FListaTransitosEmpresa.Strings[I],2,';'));
            I := FListaTransitosEmpresa.Count;
            Result := True;
        end;
        Inc(I);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormReclamoTransitoDesconoceInfraccion.CBMotivosDesconoceInfraccionDropDown
  Author:    lGisuk
  Date:      06-May-2005
  Arguments: selecciono los datos a mostrar
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.CBMotivosDesconoceInfraccionSelect(Sender: TObject);
begin
    //Ninguna Opci�n
  	if CBMotivosDesconoceInfraccion.Value = 0 then begin
    		PDatosJustificacion.Visible := False;
        Exit;
    end;
    //Como elegio alguna opcion muestro el panel
    PDatosJustificacion.Visible := True;
    //Compro pase diario
    if CBMotivosDesconoceInfraccion.Value = 1 then begin
      	PPasediario.Visible := True;
        NbDatosJustificacion.PageIndex := 0;
  	end;
    //Compro boleto de habilitaci�n tardia
    if CBMotivosDesconoceInfraccion.Value = 2 then begin
      	PPaseDiario.Visible := False;
        NbDatosJustificacion.PageIndex := 0;
    end;
    //Tengo televia
    if CBMotivosDesconoceInfraccion.Value = 3 then begin
       	PPaseDiario.Visible := False;
        NbDatosJustificacion.PageIndex := 1;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosDrawText
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.DBLTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 29/06/2005
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer) : Boolean;
    var
        Bmp : TBitMap;
    begin
        Bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            Bmp.Free;
        end;
    end;

var
    I : Integer;
    NumCorrCA : String;
    Resolucion : Integer;
begin
    //muestra la fecha
  	if Column = dblTransitos.Columns[3] then begin
	       Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerTransitosReclamo.FieldByName('FechaHora').AsDateTime)
    end;

    //muestra si el cliente rechazo un transito o no
  	if Column = dbltransitos.Columns[4] then begin
        //verifica si esta seleccionado o no
        I := Iif(ListaTransitosIn(FListaTransitosCliente,spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring), 0, 1);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos, lncheck, I);
  	end;

    //Muestra la resolucion de la empresa con respecto al transito
  	if Column = dbltransitos.Columns[5] then begin
        //Obtengo el numero de transito
        NumCorrCA := spObtenerTransitosReclamo.FieldByName('NumCorrCa').AsString;
        //Busco el transito. Si esta me devuelve su resolucion actual.
        if not ListaTransitosEmpresaIn(NumCorrCa, Resolucion) then begin
            Text := 'Pendiente';
        end else begin
            if Resolucion = 1 then text:='Pendiente';
            if Resolucion = 2 then text:='Aceptado';
            if Resolucion = 3 then text:='Denegado';
        end;
	  end;

    //si el transito tiene imagen le crea un link para que se pueda acceder a verla
    if Column = dblTransitos.Columns[6] then begin
        //Muestro el dibujito de una camara cuando hay una foto del transito
        I:= Iif(spObtenerTransitosReclamo.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos, ilCamara, i);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosCheckLink
  Author:    lgisuk
  Date Created: 18/05/2005
  Description: permito hacer click en el link o no
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.DBLTransitosCheckLink(Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    //permito hacer click en el link o no
    if  Column = dblTransitos.Columns[6] then begin
    	IsLink := (spObtenerTransitosReclamo['RegistrationAccessibility'] > 0);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosContextPopup
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: habilito el menu contextual solo si hay registros
  Parameters: Sender: TObject;MousePos: TPoint; var Handled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.DBLTransitosContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
  	if not spObtenerTransitosReclamo.IsEmpty then begin
        Handled := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosLinkClick
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: permito rechazar o aceptar un transito
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.DBLTransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Resourcestring
    MSG_TITLE   = 'Sistema de Reclamos';
    MSG_CONFIRM = 'Desea cambiar la opci�n que eligio el usuario?';
Var
  	NumCorrCa: String;
    RealizarCambio : Boolean;
    Estado: String;
begin
    //Mostrar Imagen del Transito
    if Column = dblTransitos.Columns[6] then begin
        //Si hay imagen
        if spObtenerTransitosReclamo.FieldByName('RegistrationAccessibility').AsInteger > 0 then begin
            //La muestra
            //MostrarVentanaImagen(Self,spObtenerTransitosReclamo['NumCorrCA'], spObtenerTransitosReclamo['FechaHora']);    // SS_1091_CQU_20130516
            MostrarVentanaImagen(Self,                                                                                      // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['NumCorrCA'],                                                                     // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['FechaHora'],                                                                     // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['EsItaliano']);                                                                   // SS_1091_CQU_20130516
        end;
    end;

    //Selecciono/desselecciono transito  cliente
    if dbltransitos.Columns[4].IsLink = false then exit;

    //Si la OS ya esta cerrada evito que hagan click en el link
    Estado := FrameSolucionOrdenServicio1.cbEstado.value;
    if ((Estado = 'C') or (Estado = 'T')) then Exit;

    if Column = dbltransitos.Columns[4] then begin
        //No realizar cambio hasta  confirmacion operador
        RealizarCambio := False;
        //Si es un reclamo nuevo
        if FEditando = False then begin
            RealizarCambio:= True;
        end;
        //Si se esta modificando
        if FEditando = True then begin
           //Confirmo si desea cambiar la opcion que eligio el usuario
           if MsgBox(MSG_CONFIRM, MSG_TITLE, 1) = 1 then begin
                RealizarCambio := True;
           end;
        end;
        //Realizo el cambio en la opcion del cliente
        if RealizarCambio then begin
            Numcorrca := spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring;
            if ListaTransitosin(FListaTransitosCliente, NumCorrCa) then ListaTransitosExclude(FListaTransitosCliente, NumCorrCa)
            else ListaTransitosInclude(FListaTransitosCliente, Numcorrca);
            Sender.Invalidate;
       end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Mnu_ComunClick
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: Permite cambiar la Resolucion de la empresa
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.Mnu_ComunClick(Sender: TObject);
var
  	Numcorrca : String;
    CodigoResolucionActual : String;
    CodigoResolucionAnterior : Integer;
begin
    //Obtengo codigo de Resolucion Actual
    CodigoResolucionActual := IntToStr((Sender as TMenuItem).Tag);
    //Obtengo numero de transito
    NumCorrCa := spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring;
    //Obtengo Resolucion Anterior porque la necesito para localizar el registro
    ListaTransitosEmpresaIn(NumCorrCa, CodigoResolucionAnterior);
    //Elimino Resolucion Anterior porque solo puede existir una
    ListaTransitosExclude( FListaTransitosEmpresa, NumCorrCa + ';' + IntToStr(CodigoResolucionAnterior) );
    //Inserto Nueva Resolucion
    ListaTransitosInclude( FListaTransitosEmpresa, NumCorrCa + ';' + CodigoResolucionActual );
    //Refresco la vista
    Dbltransitos.Invalidate;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarEstadoTransitos
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: valido que todos los transitos incluidos en el reclamo
               hallan sido analizados y resueltos por la empresa
               antes de dar por finalizado el reclamo
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoTransitoDesconoceInfraccion.ValidarEstadoTransitos : Boolean;
Var
    CodigoResolucion : Integer;
begin
    Result := True;
    //si el sp no esta abierto salgo
    if spObtenerTransitosReclamo.Active = False then Exit;

    with spObtenerTransitosReclamo do begin
        DisableControls;
        First;
        while not eof do begin
            ListaTransitosEmpresaIn(FieldByName('NumCorrCa').AsString,CodigoResolucion);
            //basta que uno este sin resolver no permito Guardar
            if CodigoResolucion = 1 then begin
                Result := False;
            end;
            Next;
        end;
        EnableControls;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: Registra el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  Valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar: Boolean;
    resourcestring
        MSG_ERROR_DET       = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE    = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA     = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                              'debe ser menor a la Fecha Comprometida con el Cliente';
        MSG_ERROR_TRANSITOS = 'Todos los Tr�nsitos deben estar resueltos para cerrar el reclamo';
    begin
        Result := False;
        //Validamos fuente de reclamo
        if FEditando = False then begin
            if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //Si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            Result := True;
            exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = False then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //valido que todos los transitos esten resueltos
        if (FrameSolucionOrdenServicio1.Estado = 'T') then begin
            if not ValidarEstadoTransitos then begin
                PageControl.ActivePageIndex := 0;
                MsgBoxBalloon(MSG_ERROR_TRANSITOS, STR_ERROR, MB_ICONSTOP, dbltransitos);
                Dbltransitos.SetFocus;
                Exit;
            end;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            TxtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;

        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802

        Result := True;
    end;

    {-----------------------------------------------------------------------------
      Function Name: AnularTransitoInfraccion
      Author:    lgisuk
      Date Created: 24/06/2005
      Description: Permite anular la infraccion realizada sobre ese transito
                   si la empresa le da la razon al cliente
      Parameters: NumCorrCA:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function AnularTransitoInfraccion(NumCorrCA:string):boolean;
    resourcestring
        MSG_ERROR                  = 'Error';
        MSG_ERROR_NOT_COULD_CANCEL = 'Error al anular el transito con infracci�n!';
    begin
        Result := False;
        try
            with SPAnularTransitoInfraccion do begin
                Parameters.Refresh;
                Parameters.ParamByName('@NumCorrCa').Value:= NumCorrCA;
                Parameters.ParamByName('@CodigoMotivoAnulacionInfraccion').Value := MAI_RECLAMO;
                ExecProc;
            end;
            Result := True;
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_NOT_COULD_CANCEL, E.Message, MSG_ERROR , MB_ICONSTOP);
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: InformarTransitosReclamadosAcciones
      Author:    lgisuk
      Date Created: 05/07/2005
      Description: Informo los transitos reclamados
      Parameters: ListaTransitos:tstringlist
      Return Value: None
    -----------------------------------------------------------------------------}
    Procedure InformarTransitosReclamadosAcciones(ListaTransitos:tstringlist);
    Var
        F :  TFormTransitosReclamadosAcciones;
    begin
         Application.CreateForm(TFormTransitosReclamadosAcciones, F);
         if F.Inicializar(ListaTransitos) then F.ShowModal;
    end;

Var
    OS: Integer;
    Ok: Boolean;
    CodigoResolucion : Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;
    
    // Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra ---------------------------------
    DMConnections.BaseCAC.BeginTrans;
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.Text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaBefore,       // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5txtDetalleRespuestaAfter,        // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteBefore,  // _PAN_
                                   FrameRespuestaOrdenServicio1.MD5TxtComentariosdelClienteAfter    // _PAN_
                                   {FrameSubTipoReclamoOrdenServicio1}                              // _PAN_
                                   );
    //FinRev.1------------------------------------------------------------------------

    if OS <= 0 then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Especifico desconoce infraccion
    if GuardarOrdenServicioDesconoceInfraccion(OS, CbMotivosDesconoceInfraccion.value, ENumeroSerie.text, EFechaCompra.Date, CbCategoria.Value, EPatente.Text, Eetiqueta.Text, CbConcesionaria.Value, ENumeroConvenio.Text) = False then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Especifico de Transitos
    Ok := True;
    with spObtenerTransitosReclamo do begin
        DisableControls;
        First;
        while not Eof do begin
            ListaTransitosEmpresaIn(FieldByName('NumCorrCA').AsString, CodigoResolucion);
            //Guardo cada transito en la bd
            if GuardarOrdenServicioTransito(OS, FieldByName('NumCorrCA').AsString, not (ListaTransitosIn(FListaTransitosCliente, FieldByName('NumCorrCa').AsString)), CodigoResolucion) = False then Ok := False;
            //Anulo la infracci�n sobre ese transito si fue aceptado
            //el reclamo del cliente por la empresa
            if CodigoResolucion = 2 then begin
                AnularTransitoInfraccion(FieldByName('NumCorrCA').AsString);
            end;
            Next;
        end;
        EnableControls;
    end;
    if not Ok then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Listo
    DMConnections.BaseCAC.CommitTrans;

    //Muestro un cartel con las acciones realizadas sobre
    //los transitos reclamados
    if FrameSolucionOrdenServicio1.Estado = 'T' then begin
        InformarTransitosReclamadosAcciones(FListaTransitos);
    end;

    //si es un reclamo nuevo
    if FEditando = False then begin
        //informo al operador el numero de Reclamo que se genero
        InformarNumeroReclamo(IntToStr(OS));
        DesbloquearOrdenServicio(OS);
    end;

     close;

end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 12/04/2005
  Description:  remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.FormDestroy(Sender: TObject);
begin
    RemoveOSform(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: desbloqueo la orden de servicio antes de salir
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoTransitoDesconoceInfraccion.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //si se esta modificando un reclamo
    if FEditando = True then begin
        //verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = False then begin
            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //borro la lista
    FListaTransitosCliente.Clear;
    //lo libero de memoria
    Action := caFree;
end;

initialization
    FListaTransitosCliente := TStringList.Create;
    FListaTransitosEmpresa := TStringList.Create;
finalization
    FreeAndNil(FListaTransitosCliente);
    FreeAndNil(FListaTransitosEmpresa);
end.
