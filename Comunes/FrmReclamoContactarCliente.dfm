object FormReclamoContactarCliente: TFormReclamoContactarCliente
  Left = 89
  Top = 170
  BorderStyle = bsDialog
  Caption = 'FormReclamoContactarCliente'
  ClientHeight = 514
  ClientWidth = 862
  Color = clBtnFace
  Constraints.MinHeight = 458
  Constraints.MinWidth = 870
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  DesignSize = (
    862
    514)
  PixelsPerInch = 96
  TextHeight = 13
  inline FrameContactoReclamo1: TFrameContactoReclamo
    Left = 1
    Top = 0
    Width = 627
    Height = 172
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 1
    ExplicitHeight = 172
    inherited gbUsuario: TGroupBox
      inherited LNumeroConvenio: TLabel
        Width = 63
        ExplicitWidth = 63
      end
    end
    inherited gbContacto: TGroupBox
      ExplicitHeight = 162
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 475
    Width = 862
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PDerecha: TPanel
      Left = 616
      Top = 0
      Width = 246
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object CKRetenerOrden: TCheckBox
        Left = 3
        Top = 11
        Width = 89
        Height = 17
        Caption = 'Retener Orden'
        TabOrder = 0
      end
      object AceptarBTN: TButton
        Left = 96
        Top = 6
        Width = 70
        Height = 25
        Caption = '&Aceptar'
        Default = True
        TabOrder = 1
        OnClick = AceptarBTNClick
      end
      object btnCancelar: TButton
        Left = 171
        Top = 6
        Width = 70
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        ModalResult = 2
        TabOrder = 2
        OnClick = btnCancelarClick
      end
    end
  end
  object PageControl: TPageControl
    Left = 5
    Top = 173
    Width = 610
    Height = 335
    ActivePage = TabSheetDatos
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheetDatos: TTabSheet
      Caption = 'Datos del Reclamo'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        602
        307)
      object Label1: TLabel
        Left = 3
        Top = 47
        Width = 95
        Height = 13
        Caption = 'Detalles del reclamo'
      end
      object Lreferencia: TLabel
        Left = 4
        Top = 13
        Width = 55
        Height = 13
        Caption = 'Referencia:'
      end
      object LVer: TLabel
        Left = 408
        Top = 14
        Width = 22
        Height = 13
        Cursor = crHandPoint
        Caption = 'Ver..'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = LVerClick
      end
      object txtDetalle: TMemo
        Left = 1
        Top = 64
        Width = 597
        Height = 234
        Anchors = [akLeft, akTop, akRight, akBottom]
        ReadOnly = True
        TabOrder = 0
      end
      object EReferencia: TEdit
        Left = 68
        Top = 10
        Width = 333
        Height = 21
        TabStop = False
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 1
      end
    end
    object TabSheetProgreso: TTabSheet
      Caption = 'Progreso / soluci'#243'n'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Ldetalledelprogreso: TLabel
        Left = 0
        Top = 56
        Width = 155
        Height = 13
        Align = alTop
        Caption = '  Detalles del progreso / soluci'#243'n'
      end
      object PContactarCliente: TPanel
        Left = 0
        Top = 0
        Width = 602
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object cknotificado: TCheckBox
          Left = 7
          Top = 11
          Width = 177
          Height = 17
          Caption = 'Qued'#243' Notificado'
          TabOrder = 0
        end
        object ckconforme: TCheckBox
          Left = 7
          Top = 35
          Width = 233
          Height = 17
          Caption = 'Estuvo de acuerdo con la Resoluci'#243'n'
          TabOrder = 1
        end
      end
      object Pdivisor2: TPanel
        Left = 0
        Top = 50
        Width = 602
        Height = 6
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
      object Pdivisor: TPanel
        Left = 0
        Top = 69
        Width = 602
        Height = 6
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
      end
      object txtDetalleSolucion: TMemo
        Left = 0
        Top = 75
        Width = 602
        Height = 232
        Align = alClient
        TabOrder = 3
      end
    end
    object TabSheetRespuesta: TTabSheet
      Caption = 'Respuesta'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      inline FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 307
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 307
        inherited LRespuestadelaConcesionaria: TLabel
          Width = 153
        end
        inherited LComentariosDelCliente: TLabel
          Width = 116
          ExplicitWidth = 116
        end
        inherited PContactarCliente: TPanel
          Width = 602
          ExplicitWidth = 602
        end
        inherited txtDetalleRespuesta: TMemo
          Width = 602
          ExplicitWidth = 602
        end
        inherited TxtComentariosdelCliente: TMemo
          Width = 602
          Height = 133
          ExplicitWidth = 602
          ExplicitHeight = 133
        end
      end
    end
    object TabSheetHistoria: TTabSheet
      Caption = 'Historia'
      ImageIndex = 4
      inline FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 307
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 307
        inherited DBLHistoria: TDBListEx
          Width = 602
          Height = 231
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 120
              Header.Caption = 'F. Modificaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaHoraModificacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'F. Compromiso'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaCompromiso'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Usuario'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Prioridad'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Prioridad'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescEstado'
            end>
          ExplicitWidth = 602
          ExplicitHeight = 231
        end
        inherited Pencabezado: TPanel
          Width = 602
          ExplicitWidth = 602
          inherited Pder: TPanel
            Left = 594
            ExplicitLeft = 594
          end
          inherited Parriva: TPanel
            Width = 602
            ExplicitWidth = 602
          end
        end
        inherited Pabajo: TPanel
          Top = 266
          Width = 602
          ExplicitTop = 266
          ExplicitWidth = 602
        end
      end
    end
  end
  inline FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio
    Left = 623
    Top = 69
    Width = 239
    Height = 102
    Anchors = [akTop, akRight]
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 623
    ExplicitTop = 69
    inherited GBCompromisoCliente: TGroupBox
      inherited Label1: TLabel
        Width = 44
        ExplicitWidth = 44
      end
      inherited Label2: TLabel
        Width = 103
        ExplicitWidth = 103
      end
    end
  end
  inline FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio
    Left = 622
    Top = 171
    Width = 240
    Height = 116
    Anchors = [akTop, akRight]
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 171
    ExplicitHeight = 116
    inherited GBEstadoDelReclamo: TGroupBox
      inherited Label1: TLabel
        Width = 36
        ExplicitWidth = 36
      end
      inherited PAdicional: TPanel
        inherited Label2: TLabel
          Width = 72
          ExplicitWidth = 72
        end
        inherited Label3: TLabel
          Width = 70
          ExplicitWidth = 70
        end
      end
    end
  end
  inline FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio
    Left = 622
    Top = 6
    Width = 239
    Height = 65
    Anchors = [akTop, akRight]
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 6
  end
  inline FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio
    Left = 622
    Top = 285
    Width = 239
    Height = 68
    Anchors = [akTop, akRight]
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 285
    inherited PReclamo: TGroupBox
      inherited LOrden: TLabel
        Width = 72
        ExplicitWidth = 72
      end
    end
  end
  inline FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio
    Left = 623
    Top = 354
    Width = 235
    Height = 56
    Anchors = [akTop, akRight]
    TabOrder = 7
    Visible = False
    ExplicitLeft = 623
    ExplicitTop = 354
    ExplicitWidth = 235
    ExplicitHeight = 56
    inherited GBConcesionaria: TGroupBox
      inherited lblConcesionaria: TLabel
        Width = 70
        ExplicitWidth = 70
      end
    end
  end
  object spObtenerOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenServicio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 166
  end
  object spObtenerOrdenServicioContactarCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenServicioContactarCliente;1'
    Parameters = <>
    Left = 392
    Top = 166
  end
  object spObtenerOrdenesServicioTransito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenesServicioTransito;1'
    Parameters = <>
    Left = 424
    Top = 166
  end
  object spObtenerOrdenesServicioEstacionamiento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenesServicioEstacionamiento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 456
    Top = 166
  end
end
