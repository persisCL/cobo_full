unit CobranzasRoutines;

interface

Uses
    // Delphi
    SysUtils, DBClient, ADODB, Controls,

    // Proyecto CN
    SysUtilsCN,
    CobranzasClasses,
    CobranzasResources,

    // A Revisar
    Util,
    PeaTypes,
    PeaProcs;

procedure AsignarEntradaDatos(pTipoEntradaDatos, pCodigoCanalPago: Byte; var pEntradaDatos: TEntradaDatos; pFechaPago: TDate; pDatosPersonales: TDatosPersonales; pDatosIniciales: TClientDataSet; pAlta, pEnTramite: Boolean);
function EsReciboCancelacionRefinanciacion(pConexionADO: TADOConnection; pNumeroRecibo: Int64): Boolean;

implementation

procedure AsignarEntradaDatos
    (
        pTipoEntradaDatos,
        pCodigoCanalPago  : Byte;
        var pEntradaDatos     : TEntradaDatos;
        pFechaPago        : TDate;
        pDatosPersonales  : TDatosPersonales;
        pDatosIniciales   : TClientDataSet;
        pAlta,
        pEnTramite        : Boolean
    );
begin
    with pEntradaDatos do begin

        CodigoTipoEntrada := pTipoEntradaDatos;
        CodigoCanalPago   := pCodigoCanalPago;

        case CodigoTipoEntrada of
            TIPO_ENTRADA_DATOS_PAGO_CUOTA: begin
                CodigoFormaPago        := 0;
                CodigoEntradaUsuario   := EmptyStr;
                TitularNombre          := EmptyStr;
                TitularTipoDocumento   := EmptyStr;
                TitularNumeroDocumento := EmptyStr;
                CodigoBanco            := 0;
                DocumentoNumero        := EmptyStr;
                Fecha                  := NullDate;
                FechaPago              := pFechaPago;
                CodigoTarjeta          := 0;
                TarjetaCuotas          := 0;
                Importe                := 0;
                ImportePagado          := 0;
                Persona                := pDatosPersonales;

                PersonaNombreCompleto  :=
                    ArmarNombrePersona(
                        pDatosPersonales.Personeria,
                        pDatosPersonales.Nombre,
                        pDatosPersonales.Apellido,
                        pDatosPersonales.ApellidoMaterno);

                with DatosCuota do begin
                    Saldo                 := 0;
                    CodigoCuotaAntecesora := 0;
                    CuotaNueva            := True;
                    CuotaModificada       := False;
                    EstadoImpago          := True;
                    EnTramite             := pEnTramite;
                end;

                pDatosIniciales.First;
                while not pDatosIniciales.eof do begin
                    if pDatosIniciales.FieldByName('Cobrar').AsBoolean then begin
                        Importe          := Importe + pDatosIniciales.FieldByName('Importe').AsInteger;
                        ImportePagado    := ImportePagado + pDatosIniciales.FieldByName('Saldo').AsInteger;
                        DatosCuota.Saldo := DatosCuota.Saldo + pDatosIniciales.FieldByName('Saldo').AsInteger;
                    end;

                    pDatosIniciales.Next;
                end;
                pDatosIniciales.First;
            end;

            TIPO_ENTRADA_DATOS_CUOTA: begin
                case pAlta of
                    True: begin
                        CodigoFormaPago        := 0;
                        CodigoEntradaUsuario   := EmptyStr;
                        TitularNombre          := EmptyStr;
                        TitularTipoDocumento   := EmptyStr;
                        TitularNumeroDocumento := EmptyStr;
                        CodigoBanco            := 0;
                        DocumentoNumero        := EmptyStr;
                        Fecha                  := NullDate;
                        FechaPago              := pFechaPago;
                        CodigoTarjeta          := 0;
                        TarjetaCuotas          := 0;
                        Importe                := 0;
                        ImportePagado          := 0;
                        Persona                := pDatosPersonales;

                        PersonaNombreCompleto  :=
                            ArmarNombrePersona(
                                pDatosPersonales.Personeria,
                                pDatosPersonales.Nombre,
                                pDatosPersonales.Apellido,
                                pDatosPersonales.ApellidoMaterno);

                        with DatosCuota do begin
                            Saldo                 := 0;
                            CodigoCuotaAntecesora := 0;
                            CuotaNueva            := True;
                            CuotaModificada       := False;
                            EstadoImpago          := True;
                            EnTramite             := pEnTramite;
                        end;
                    end;
                    False: begin
                        CodigoFormaPago        := pDatosIniciales.FieldByName('CodigoFormaPago').AsInteger;
                        CodigoEntradaUsuario   := pDatosIniciales.FieldByName('CodigoEntradaUsuario').AsString;
                        TitularNombre          := pDatosIniciales.FieldByName('TitularNombre').AsString;
                        TitularTipoDocumento   := pDatosIniciales.FieldByName('TitularTipoDocumento').AsString;
                        TitularNumeroDocumento := pDatosIniciales.FieldByName('TitularNumeroDocumento').AsString;
                        CodigoBanco            := pDatosIniciales.FieldByName('CodigoBanco').AsInteger;
                        DocumentoNumero        := pDatosIniciales.FieldByName('DocumentoNumero').AsString;
                        Fecha                  := pDatosIniciales.FieldByName('FechaCuota').AsDateTime;
                        FechaPago              := pFechaPago;
                        CodigoTarjeta          := pDatosIniciales.FieldByName('CodigoTarjeta').AsInteger;
                        TarjetaCuotas          := pDatosIniciales.FieldByName('TarjetaCuotas').AsInteger;
                        Importe                := pDatosIniciales.FieldByName('Importe').AsInteger;
                        ImportePagado          := pDatosIniciales.FieldByName('Saldo').AsInteger;
                        Persona                := pDatosPersonales;

                        PersonaNombreCompleto  :=
                            ArmarNombrePersona(
                                pDatosPersonales.Personeria,
                                pDatosPersonales.Nombre,
                                pDatosPersonales.Apellido,
                                pDatosPersonales.ApellidoMaterno);

                        with DatosCuota do begin
                            Saldo                 := pDatosIniciales.FieldByName('Saldo').AsInteger;
                            CodigoCuotaAntecesora := pDatosIniciales.FieldByName('CodigoCuotaAntecesora').AsInteger;
                            CuotaNueva            := pDatosIniciales.FieldByName('CuotaNueva').AsBoolean;
                            CuotaModificada       := pDatosIniciales.FieldByName('CuotaModificada').AsBoolean;
                            EstadoImpago          := pDatosIniciales.FieldByName('EstadoImpago').AsBoolean;
                            EnTramite             := pEnTramite;
                        end;
                    end;
                end;
            end;
        end;
    end;
end;

function EsReciboCancelacionRefinanciacion(pConexionADO: TADOConnection; pNumeroRecibo: Int64): Boolean;
    const
        MSG_ERROR_CAPTION         = 'Error Ejecuci�n';
        MSG_ERROR_STORE_PROCEDURE = 'Se produjo un Error al ejecutar la Funci�n dbo.%s(). Error: %s.';
    var
        StoreProcedure: TADOStoredProc;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        try
            StoreProcedure := TADOStoredProc.Create(Nil);
            with StoreProcedure do begin
                Connection    := pConexionADO;
                ProcedureName := 'EsReciboCancelacionRefinanciacion';

                Parameters.Refresh;
                Parameters.ParamByName('@NumeroComprobante').Value := pNumeroRecibo;

                ExecProc;

                Result := Parameters.ParamByName('@RETURN_VALUE').Value;
            end;
        except
            on e:Exception do begin
                raise EErrorExceptionCN.Create(
                    MSG_ERROR_CAPTION,
                    Format(
                        MSG_ERROR_STORE_PROCEDURE,
                        [StoreProcedure.ProcedureName, e.Message]));
            end;
        end;
    finally
        if Assigned(StoreProcedure) then FreeAndNil(StoreProcedure);

        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

end.
