object frmComponerMail: TfrmComponerMail
  Left = 197
  Top = 182
  BorderStyle = bsDialog
  Caption = 'Composici'#243'n de e-Mail'
  ClientHeight = 621
  ClientWidth = 851
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    851
    621)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 7
    Top = 8
    Width = 821
    Height = 393
  end
  object Label1: TLabel
    Left = 21
    Top = 20
    Width = 31
    Height = 13
    Caption = 'Para:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 22
    Top = 39
    Width = 44
    Height = 13
    Caption = 'Asunto:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblPara: TLabel
    Left = 80
    Top = 21
    Width = 32
    Height = 13
    Caption = 'lblPara'
  end
  object lblAsunto: TLabel
    Left = 81
    Top = 38
    Width = 32
    Height = 13
    Caption = 'lblPara'
  end
  object btnCerrar: TButton
    Left = 768
    Top = 583
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Cerrar'
    Default = True
    TabOrder = 1
    OnClick = btnCerrarClick
  end
  object Memo: TMemo
    Left = 16
    Top = 351
    Width = 807
    Height = 226
    TabOrder = 2
  end
  object wbEMail: TWebBrowser
    Left = 21
    Top = 58
    Width = 807
    Height = 271
    TabOrder = 3
    ControlData = {
      4C00000068530000021C00000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object btnActualizar: TButton
    Left = 648
    Top = 584
    Width = 75
    Height = 25
    Caption = '&Actualizar'
    TabOrder = 0
    OnClick = btnActualizarClick
  end
end
