object FormNoFacturarTransitosDeCuenta: TFormNoFacturarTransitosDeCuenta
  Left = 349
  Top = 216
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Eximir Facturaci'#243'n a Veh'#237'culo'
  ClientHeight = 283
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object gb_DatosVehiculo: TGroupBox
    Left = 8
    Top = 4
    Width = 361
    Height = 73
    Caption = ' Datos del Veh'#237'culo '
    TabOrder = 0
    object lbl_Patente: TLabel
      Left = 6
      Top = 20
      Width = 40
      Height = 13
      Caption = 'Patente:'
    end
    object lbl_Televia: TLabel
      Left = 6
      Top = 44
      Width = 40
      Height = 13
      Caption = 'Telev'#237'a:'
    end
    object lblPatente: TLabel
      Left = 53
      Top = 20
      Width = 58
      Height = 13
      Caption = 'lblPatente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTelevia: TLabel
      Left = 53
      Top = 44
      Width = 56
      Height = 13
      Caption = 'lblTelevia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object nb_Botones: TNotebook
    Left = 180
    Top = 247
    Width = 189
    Height = 29
    PageIndex = 1
    TabOrder = 2
    object TPage
      Left = 0
      Top = 0
      Caption = 'PageBotonesAceptarCancelar'
      object btn_Aceptar: TButton
        Left = 34
        Top = 2
        Width = 75
        Height = 25
        Caption = '&Aceptar'
        TabOrder = 0
        OnClick = btn_AceptarClick
      end
      object btn_Cancelar: TButton
        Left = 111
        Top = 2
        Width = 75
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 1
        OnClick = btn_CancelarClick
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'PageBotonSalir'
      object btn_Salir: TButton
        Left = 111
        Top = 2
        Width = 75
        Height = 25
        Cancel = True
        Caption = '&Salir'
        TabOrder = 0
        OnClick = btn_SalirClick
      end
    end
  end
  object pnlDatos: TPanel
    Left = 8
    Top = 80
    Width = 361
    Height = 161
    BevelOuter = bvLowered
    TabOrder = 1
    object lbl_FechaModificacion: TLabel
      Left = 6
      Top = 132
      Width = 62
      Height = 13
      Caption = 'Fecha Modif.'
    end
    object lbl_Usuario: TLabel
      Left = 6
      Top = 104
      Width = 36
      Height = 13
      Caption = 'Usuario'
    end
    object lbl_Motivo: TLabel
      Left = 6
      Top = 76
      Width = 39
      Height = 13
      Caption = 'Motivo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_FechaFin: TLabel
      Left = 6
      Top = 48
      Width = 61
      Height = 13
      Caption = 'Fecha Hasta'
    end
    object lbl_FechaDesde: TLabel
      Left = 6
      Top = 20
      Width = 76
      Height = 13
      Caption = 'Fecha Desde'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodUsuario: TLabel
      Left = 88
      Top = 104
      Width = 65
      Height = 13
      Caption = 'lblCodUsuario'
    end
    object lblFechaModif: TLabel
      Left = 88
      Top = 132
      Width = 66
      Height = 13
      Caption = 'lblFechaModif'
    end
    object cb_Motivos: TVariantComboBox
      Left = 90
      Top = 72
      Width = 264
      Height = 21
      Hint = 'Motivo por el cual no se facturan los tr'#225'nsitos'
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object txt_FechaFin: TDateEdit
      Left = 90
      Top = 44
      Width = 98
      Height = 21
      Hint = 'Fecha hasta la cual no facturar tr'#225'nsitos'
      AutoSelect = False
      TabOrder = 1
      Date = -693594
    end
    object txt_FechaInicio: TDateEdit
      Left = 90
      Top = 16
      Width = 98
      Height = 21
      Hint = 'Fecha desde la cual no facturar tr'#225'nsitos'
      AutoSelect = False
      Color = 16444382
      TabOrder = 0
      Date = -693594
    end
  end
end
