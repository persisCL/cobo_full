unit FrmListadoTurnos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx,UtilProc,DmConnection,
  DB, ADODB, DmiCtrls, Validate, DateEdit,peaprocs,util,UtilDB,Abm_obj,RStrings,
  FrmAperturaCierrePuntoVenta, Peatypes, frmRptCierreTurno, frmConfirmaContrasena,
  VariantComboBox, CobranzasResources, frmMuestraMensaje, PeaProcsCN;

type

  TFormListadoTurnos = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    dbl_Turnos: TDBListEx;
    ObtenerListadoTurnos: TADOStoredProc;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label2: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    dsListadoTurnos: TDataSource;
    txtUsuario: TEdit;
    cbEstadosTurno: TComboBox;
    lbl_PuntoEntrega: TLabel;
    lbl_PuntoVenta: TLabel;
    cb_PuntosEntrega: TVariantComboBox;
    cb_PuntosVenta: TVariantComboBox;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    btnImprimir: TButton;
    btnSupervisor: TButton;
    btnCerrarTurno: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure ObtenerListadoTurnosAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure dbl_SolicitudColumns0HeaderClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure txt_rutKeyPress(Sender: TObject; var Key: Char);
    procedure txt_ApellidoKeyPress(Sender: TObject; var Key: Char);
    procedure ObtenerListadoTurnosAfterScroll(DataSet: TDataSet);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnSupervisorClick(Sender: TObject);
    procedure btnCerrarTurnoClick(Sender: TObject);
    procedure cb_PuntosEntregaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FPuntoEntrega: integer;
    FPuntoVenta: integer;
    //FPermisoCerrarTurno: Boolean;
    function BuscarColumna(Nombre:String;IniciarDesde:integer):Integer;
    function DarOrderBy:String;
  public
    { Public declarations }
    function Inicializar(MDIChild: Boolean; PuntoEntrega: integer; PuntoVenta: integer): Boolean;
  end;

var
  FormListadoTurnos: TFormListadoTurnos;

implementation

uses Main;

resourceString
    CAPTION_EDITAR_SOLICITUD = 'Modificar Solicitud';
    CAPTION_VER_SOLICITUD = 'Ver Solicitud';


{$R *.dfm}

procedure TFormListadoTurnos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormListadoTurnos.FormCreate(Sender: TObject);
begin
    FFormState := FFormState - [fsVisible];
end;

function TFormListadoTurnos.Inicializar(MDIChild: Boolean; PuntoEntrega: integer;PuntoVenta: integer): Boolean;
Var S: TSize;
begin
    try
        try
            CargarConstantesCobranzasRefinanciacion;
            CargarConstantesCobranzasCanalesFormasPago;

            if MDIChild then begin
                S := GetFormClientSize(Application.MainForm);
                SetBounds(0, 0, S.cx, S.cy);
            end else begin
                FormStyle := fsNormal;
                Visible := False;
            end;
    //        btn_Limpiar.Click;
            txt_FechaHasta.Date:=Now;

            FPuntoEntrega := PuntoEntrega;
            FPuntoVenta := PuntoVenta;

            btnCerrarTurno.Visible := True;
            btnCerrarTurno.Enabled := False;

            btn_Limpiar.Click;
            Result := True;
        except
            result:=false;
        end;
    finally
        TPanelMensajesForm.OcultaPanel;
    end;
end;

procedure TFormListadoTurnos.BtnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TFormListadoTurnos.btn_FiltrarClick(Sender: TObject);
begin
    if (txt_FechaDesde.Date <> Nulldate) and (txt_FechaHasta.Date <> Nulldate) and (txt_FechaDesde.Date > txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA, MSG_CAPTION_LISTSOLICITUD, MB_ICONSTOP, txt_FechaDesde);
        Exit;
    end;

    ObtenerListadoTurnos.DisableControls;

    with ObtenerListadoTurnos do begin
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@FechaHoraDesde').Value := iif(txt_FechaDesde.Date=nulldate, null , txt_FechaDesde.Date);
        Parameters.ParamByName('@FechaHoraHasta').Value := iif(txt_FechaHasta.Date=nulldate, null, txt_FechaHasta.Date);
        Parameters.ParamByName('@CodigoPuntoEntrega').Value := iif(cb_PuntosEntrega.ItemIndex <= 0, Null, cb_PuntosEntrega.Value);
        Parameters.ParamByName('@CodigoPuntoVenta').Value := iif(cb_PuntosVenta.ItemIndex <= 0, Null, cb_PuntosVenta.Value);
        Parameters.ParamByName('@Usuario').Value := iif(Trim(txtUsuario.Text) = '', NULL, trim(txtUsuario.Text) + '%');
        Parameters.ParamByName('@Estado').Value := iif(StrRight(Trim(cbEstadosTurno.Text), 1) = '0', NULL, StrRight(trim(cbEstadosTurno.Text), 1));
        Parameters.ParamByName('@OrderBy').Value := DarOrderBy;
    end;

    if not(OpenTables([ObtenerListadoTurnos])) then MsgBox(FORMAT(MSG_ERROR_INICIALIZACION, [STR_SOLICITUD]), MSG_CAPTION_LISTSOLICITUD, MB_ICONSTOP);

    ObtenerListadoTurnos.EnableControls;

    dbl_Turnos.SetFocus;
end;

procedure TFormListadoTurnos.ObtenerListadoTurnosAfterOpen(
  DataSet: TDataSet);
begin
    btnSupervisor.Enabled := (ObtenerListadoTurnos.Active) and (ObtenerListadoTurnos.RecordCount > 0);
    btnImprimir.Enabled := btnSupervisor.Enabled;
    btnCerrarTurno.Enabled := btnSupervisor.Enabled;
end;

procedure TFormListadoTurnos.FormShow(Sender: TObject);
begin
    txt_FechaDesde.SetFocus;
    btnSupervisor.Enabled 	:= False;
    btnImprimir.Enabled 	:= False;
end;

procedure TFormListadoTurnos.dbl_SolicitudColumns0HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting=csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;
    if ObtenerListadoTurnos.Active then ObtenerListadoTurnos.Sort := TDBListExColumn(sender).FieldName  + ' ' + iif(TDBListExColumn(sender).Sorting=csAscending ,'ASC','DESC');
//    if ObtenerListadoTurnos.Active then btn_Filtrar.Click;
end;


function TFormListadoTurnos.BuscarColumna(Nombre:String;IniciarDesde:integer):Integer;
begin
    if IniciarDesde>dbl_Turnos.Columns.Count-1 then begin result:=-1; exit; end;
    if dbl_Turnos.Columns[IniciarDesde].FieldName=Nombre then result:=IniciarDesde
    else Result:=BuscarColumna(Nombre,IniciarDesde+1);
end;


function TFormListadoTurnos.DarOrderBy:String;
var
    i:Integer;
begin
    result:='';
    for i:=0 to  dbl_Turnos.Columns.Count-1 do
        if dbl_Turnos.Columns[i].Sorting <> csNone then result:=dbl_Turnos.Columns[i].FieldName+iif(dbl_Turnos.Columns[i].Sorting=csDescending,' DESC','');

    if result='' then begin
        result:='CodigoUsuario';
        dbl_Turnos.Columns[BuscarColumna('CodigoUsuario',0)].Sorting:=csAscending;
    end;
end;


procedure TFormListadoTurnos.btn_LimpiarClick(Sender: TObject);
begin
    CargarEstadosTurno(DMConnections.BaseCAC, cbEstadosTurno, 'A', True);
    CargarPuntosEntrega(DMConnections.BaseCAC, cb_PuntosEntrega, True, FPuntoEntrega);
    CargarPuntosVenta(DMConnections.BaseCAC, cb_PuntosVenta, True, FPuntoEntrega, FPuntoVenta);
    txtUsuario.Clear;
    txt_FechaDesde.Clear;
    txt_FechaHasta.Clear;
    ObtenerListadoTurnos.close;
    if Self.Showing then
        txt_FechaDesde.SetFocus;
end;

procedure TFormListadoTurnos.txt_rutKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key := UpCase(KEY);
    if not (Key  in ['0'..'9','K','k', #8,'%']) then Key := #0
end;

procedure TFormListadoTurnos.txt_ApellidoKeyPress(Sender: TObject;
  var Key: Char);
begin
    key:=UpCase(Key);
end;


procedure TFormListadoTurnos.ObtenerListadoTurnosAfterScroll(
  DataSet: TDataSet);
begin
    btnImprimir.Enabled := ObtenerListadoTurnos.FieldByName('Estado').AsString[1] = TURNO_ESTADO_CERRADO;
    btnSupervisor.Enabled := ObtenerListadoTurnos.FieldByName('Estado').AsString[1] = TURNO_ESTADO_CERRADO;
    btnCerrarTurno.Enabled := ObtenerListadoTurnos.FieldByName('Estado').AsString[1] <> TURNO_ESTADO_CERRADO;
end;

procedure TFormListadoTurnos.btnImprimirClick(Sender: TObject);
var
    f: TFormRptCierreTurno;
begin
    Application.CreateForm(TFormRptCierreTurno, f);
    if f.Inicializar(ObtenerListadoTurnos.FieldByName('CodigoPuntoEntrega').AsInteger, ObtenerListadoTurnos.FieldByName('CodigoPuntoVenta').AsInteger, Trim(ObtenerListadoTurnos.FieldByName('CodigoUsuario').AsString), ObtenerListadoTurnos.FieldByName('NumeroTurno').AsInteger, True, True) then
    f.Ejecutar;
    f.Release;
end;

procedure TFormListadoTurnos.btnSupervisorClick(Sender: TObject);
var
    f: TFormRptCierreTurno;
begin
    try
        try
            Application.CreateForm(TFormRptCierreTurno, f);
            if f.Inicializar
                (
                    ObtenerListadoTurnos.FieldByName('CodigoPuntoEntrega').AsInteger,
                    ObtenerListadoTurnos.FieldByName('CodigoPuntoVenta').AsInteger,
                    Trim(ObtenerListadoTurnos.FieldByName('CodigoUsuario').AsString),
                    ObtenerListadoTurnos.FieldByName('NumeroTurno').AsInteger,
                    True,
                    True
                ) then begin
                f.Ejecutar;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        f.Release;
    end;
end;

procedure TFormListadoTurnos.btnCerrarTurnoClick(Sender: TObject);
resourcestring
	CAPTION_CLOSE_SHIFT = 'Cerrar Turno';
var
    fCierre: TFormAperturaCierrePuntoVenta;
begin
	try
        // Antes de cerrar hago que ingrese las cantidades finales
        Application.CreateForm(TFormAperturaCierrePuntoVenta, fCierre);
        if (fCierre.Inicializar(CAPTION_CLOSE_SHIFT, UsuarioSistema,
        	ObtenerListadoTurnos.FieldByName( 'CodigoPuntoVenta' ).AsInteger, Cerrar,
            ObtenerListadoTurnos.FieldByName( 'CodigoPuntoEntrega' ).AsInteger,
            ObtenerListadoTurnos.FieldByName( 'NumeroTurno' ).AsInteger)) then
            if ( fCierre.ShowModal = mrOk ) then btn_Filtrar.Click;
        fCierre.Release;
    finally
    	fCierre.Release;
    end;

end;

procedure TFormListadoTurnos.cb_PuntosEntregaChange(Sender: TObject);
begin
    CargarPuntosVenta(DMConnections.BaseCAC, cb_PuntosVenta, True,
        cb_PuntosEntrega.Value, FPuntoVenta);
end;

end.



