object FormMediosPagos: TFormMediosPagos
  Left = 306
  Top = 130
  BorderStyle = bsDialog
  Caption = 'Gestion de Datos de Medios de Pago'
  ClientHeight = 263
  ClientWidth = 653
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Notebook: TNotebook
    Left = 0
    Top = 0
    Width = 653
    Height = 81
    Align = alTop
    PageIndex = 1
    TabOrder = 0
    object TPage
      Left = 0
      Top = 0
      Caption = 'PAT'
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 649
        Height = 73
        Caption = 'PAT'
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 24
          Width = 92
          Height = 13
          Caption = '&Tipo de Tarjeta:'
          Color = clBtnFace
          FocusControl = cb_TipoTarjeta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lblNumeroTarjetaCredito: TLabel
          Left = 16
          Top = 48
          Width = 136
          Height = 13
          Caption = '&N'#250'mero Tarjeta Cr'#233'dito:'
          FocusControl = txt_NroTarjetaCredito
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblFechaVencimiento: TLabel
          Left = 376
          Top = 48
          Width = 167
          Height = 13
          Caption = '&Fecha Vencimiento (MM/AA):'
          FocusControl = txt_FechaVencimiento
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 375
          Top = 24
          Width = 104
          Height = 13
          Caption = '&Emisor de Tarjeta:'
          Color = clBtnFace
          FocusControl = cb_EmisoresTarjetasCreditos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object cb_TipoTarjeta: TComboBox
          Left = 159
          Top = 19
          Width = 200
          Height = 21
          Hint = 'Tipo de Tarjeta'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
          OnClick = cb_TipoTarjetaClick
        end
        object txt_NroTarjetaCredito: TEdit
          Left = 159
          Top = 43
          Width = 197
          Height = 21
          Hint = 'N'#250'mero de Tarjeta de Cr'#233'dito'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 20
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object txt_FechaVencimiento: TMaskEdit
          Left = 559
          Top = 43
          Width = 37
          Height = 21
          Hint = 'Fecha de Vencimiento de la Tarjeta'
          Color = 16444382
          EditMask = '00\/00;1; '
          MaxLength = 5
          TabOrder = 3
          Text = '00-  '
        end
        object cb_EmisoresTarjetasCreditos: TComboBox
          Left = 482
          Top = 19
          Width = 161
          Height = 21
          Hint = 'Emisor de Tarjeta'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 1
          OnClick = cb_EmisoresTarjetasCreditosClick
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      HelpContext = 1
      Caption = 'PAC'
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 653
        Height = 73
        Align = alTop
        Caption = 'PAC'
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 24
          Width = 92
          Height = 13
          Caption = '&Tipo de Cuenta:'
          Color = clBtnFace
          FocusControl = cb_TipoCuenta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label8: TLabel
          Left = 322
          Top = 24
          Width = 41
          Height = 13
          Caption = '&Banco:'
          Color = clBtnFace
          FocusControl = cb_Banco
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label9: TLabel
          Left = 16
          Top = 48
          Width = 92
          Height = 13
          Caption = '&N'#250'mero Cuenta:'
          Color = clBtnFace
          FocusControl = txt_NumeroCuenta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label10: TLabel
          Left = 320
          Top = 48
          Width = 54
          Height = 13
          Caption = '&Sucursal:'
          Color = clBtnFace
          FocusControl = txt_Sucursal
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object cb_TipoCuenta: TComboBox
          Left = 115
          Top = 19
          Width = 199
          Height = 21
          Hint = 'Tipo de Cuenta'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
          OnClick = cb_TipoCuentaClick
        end
        object cb_Banco: TComboBox
          Left = 379
          Top = 19
          Width = 255
          Height = 21
          Hint = 'Nombre del banco'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 1
          OnClick = cb_BancoClick
        end
        object txt_NumeroCuenta: TEdit
          Left = 115
          Top = 44
          Width = 198
          Height = 21
          Hint = 'N'#250'mero de Cuenta'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 30
          TabOrder = 2
          OnKeyPress = txt_NumeroCuentaKeyPress
        end
        object txt_Sucursal: TEdit
          Left = 379
          Top = 44
          Width = 254
          Height = 21
          Hint = 'Sucursal del Banco'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 30
          TabOrder = 3
        end
      end
    end
  end
  object gb_DatosPersonales: TGroupBox
    Left = 0
    Top = 81
    Width = 653
    Height = 119
    Align = alTop
    Caption = 'Datos del Mandante'
    TabOrder = 1
    object lblRutRun: TLabel
      Left = 10
      Top = 27
      Width = 31
      Height = 13
      Caption = 'RUT:'
      FocusControl = txtDocumento
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 10
      Top = 51
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      FocusControl = txt_Nombre
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnSolicitudContacto: TSpeedButton
      Left = 215
      Top = 24
      Width = 23
      Height = 19
      Hint = 'Tomar datos de la solicitud'
      Glyph.Data = {
        AA040000424DAA04000000000000360000002800000013000000130000000100
        1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9EC840000840000840000840000840000840000840000840000840000D8E9
        ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
        D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
        FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
        0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
        EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
        0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
        FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
        840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
        00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
        E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
        ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
        0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
        00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
        0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
        EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
        D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
        ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
        E9ECD8E9ECD8E9ECD8E9EC000000}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnSolicitudContactoClick
    end
    object txtDocumento: TEdit
      Left = 100
      Top = 24
      Width = 107
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object txt_Nombre: TEdit
      Left = 100
      Top = 47
      Width = 525
      Height = 21
      Hint = 'Nombre del Titular de la Cuenta/Tarjeta'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 100
      TabOrder = 1
    end
    inline FrameTelefono: TFrameTelefono
      Left = 7
      Top = 69
      Width = 614
      Height = 28
      TabOrder = 2
      TabStop = True
      ExplicitLeft = 7
      ExplicitTop = 69
      ExplicitWidth = 614
      ExplicitHeight = 28
      inherited lblTelefono: TLabel
        Left = 10
        Width = 52
        Caption = 'Tel'#233'fono:'
        ExplicitLeft = 10
        ExplicitWidth = 52
      end
      inherited lblDesde: TLabel
        Left = 384
        ExplicitLeft = 384
      end
      inherited lblHasta: TLabel
        Left = 487
        ExplicitLeft = 487
      end
      inherited txtTelefono: TEdit
        Left = 150
        CharCase = ecUpperCase
        ExplicitLeft = 150
      end
      inherited cbTipoTelefono: TVariantComboBox
        Left = 246
        ExplicitLeft = 246
      end
      inherited cbDesde: TComboBox
        Left = 420
        ExplicitLeft = 420
      end
      inherited cbHasta: TComboBox
        Left = 522
        ExplicitLeft = 522
      end
      inherited cbCodigosArea: TComboBox
        Left = 93
        Hint = 'C'#243'digo de '#193'rea'
        ExplicitLeft = 93
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 203
    Width = 653
    Height = 28
    Align = alBottom
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object Me_Mensaje: TLabel
      Left = 0
      Top = 0
      Width = 653
      Height = 28
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = 
        'Este Mandato, para entrar en vigencia, deber'#225' ser firmado en ori' +
        'ginal por el Titular de la Cuenta/Tarjeta.'
      Layout = tlCenter
    end
  end
  object pnlBotton: TPanel
    Left = 0
    Top = 231
    Width = 653
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object btnAceptar: TButton
      Left = 447
      Top = 4
      Width = 95
      Height = 25
      Hint = 'Guardar Datos'
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnSalir: TButton
      Left = 552
      Top = 4
      Width = 92
      Height = 25
      Hint = 'Salir de la Carga'
      Cancel = True
      Caption = '&Salir'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
end
