unit frmMensaje3botones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls;

type
  TfrmMsg3Botones = class(TForm)
	Mensaje: TLabel;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
  private
	{ Private declarations }
  public
	{ Public declarations }
	function Inicializar(Titulo, TxtMensaje, TxtBoton1, TxtBoton2, TxtBoton3: ANSIString; mrBoton1, mrBoton2, mrBoton3: TModalResult): Boolean;
  end;

var
  frmMsg3Botones: TfrmMsg3Botones;

implementation

{$R *.dfm}

function TfrmMsg3Botones.Inicializar(Titulo, TxtMensaje, TxtBoton1, TxtBoton2, TxtBoton3: ANSIString; mrBoton1, mrBoton2, mrBoton3: TModalResult): Boolean;
begin
	Caption := Titulo;
	Mensaje.Caption := TxtMensaje;

	btn1.Caption := TxtBoton1;
	btn1.ModalResult := mrBoton1;

	btn2.Caption := TxtBoton2;
	btn2.ModalResult := mrBoton2;

	btn3.Caption := TxtBoton3;
	btn3.ModalResult := mrBoton3;

	Result := True
end;

end.
