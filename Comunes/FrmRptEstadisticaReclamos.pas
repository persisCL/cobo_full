{-------------------------------------------------------------------------------
 File Name: FrmRptEstadisticaReclamos.pas
 Author:    Lgisuk
 Date Created: 05/04/2005
 Language: ES-AR
 Description: Gestion de Reclamos - Reporte de Estadistica de Reclamos


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

-------------------------------------------------------------------------------}
Unit FrmRptEstadisticaReclamos;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ConstParametrosGenerales;                                           //SS_1147_NDR_20140710

type
  TFRptEstadisticaReclamos = class(TForm)
    rptEstadisticaReclamo: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppTipoComprobante: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    pptImporteTotal: TppLabel;
    ppCantidadTotal: TppLabel;
    ppParameterList1: TppParameterList;
	RBIListado: TRBInterface;
    ppDBPipeline: TppDBPipeline;
    DataSource: TDataSource;
    SpObtenerReporteEstadisticaReclamos: TADOStoredProc;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppLine5: TppLine;
    Ltusuario: TppLabel;
    LtFechaHora: TppLabel;
    ppusuario: TppLabel;
    ppfechahora: TppLabel;
    ppLabel2: TppLabel;
    ppDBText4: TppDBText;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte:string;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString): Boolean;
    { Public declarations }
  end;

var
  FRptEstadisticaReclamos: TFRptEstadisticaReclamos;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 13/04/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRptEstadisticaReclamos.Inicializar(NombreReporte: AnsiString): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710
    FNombreReporte:= NombreReporte;
    Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 13/04/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptEstadisticaReclamos.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
begin
    try
        SpObtenerReporteEstadisticaReclamos.Close;
        SpObtenerReporteEstadisticaReclamos.Open;
        if SpObtenerReporteEstadisticaReclamos.IsEmpty then begin
            SpObtenerReporteEstadisticaReclamos.Close;
            MsgBox('No se encontraron datos para la consulta solicitada');
            Cancelled := True;
            Exit;
        end;
    except
        on e: Exception do begin
            MsgBoxErr('Error', e.message, 'Reporte', MB_ICONERROR);
        end;
    end;
    try
        with SpObtenerReporteEstadisticaReclamos do begin
            //Asigno los valores a los campos del reporte
            ppusuario.Caption:= QueryGetValue(dmconnections.BaseCAC,'select dbo.ObtenerNombreUsuario( ''' + usuariosistema + ''' ) ');//usuariosistema;
            ppfechahora.Caption:= datetimetostr(now);
            ppCantidadTotal.Caption:= Parameters.ParamByName('@CantidadTotal').Value;
        end;
        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FnombreReporte;
    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;


end.
