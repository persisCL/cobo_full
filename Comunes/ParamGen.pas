{-----------------------------------------------------------------------------
 File Name: ParamGen
 Author:
 Date Created:
 Language: ES-AR
 Description:

 Revision : 1
 Date: 25/02/2009
 Author: mpiazza
 Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
  los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ParamGen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DMConnection,
  StdCtrls, validate, TimeEdit, Db, DBTables, UtilProc, UtilDB, ExtCtrls, Variants,
  Util, CheckLst, ComCtrls, ADODB, DmiCtrls, Buttons, PeaProcs, DPSControls,
  ListBoxEx, DBListEx, frmEditarVal, VariantComboBox, ConstParametrosGenerales,
  Provider, DBClient;

type
  TFormParam = class(TForm)
    PageControl: TPageControl;
    TabSheet1: TTabSheet;
    Lista: TDBListEx;
    dsParametrosGenerales: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    cbFiltros: TVariantComboBox;
    ButtonAceptar: TButton;
    cdsParametrosGenerales: TClientDataSet;
    dtstprParametrosGenerales: TDataSetProvider;
    spParametrosGenerales_SELECT: TADOStoredProc;
    spParametrosGenerales_UPDATE: TADOStoredProc;
    procedure ButtonCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonAceptarClick(Sender: TObject);
    procedure ListaLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure cbFiltrosChange(Sender: TObject);
    procedure ListaDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    { INICIO : TASK_128_MGO_20170131
    procedure ListaColumns0HeaderClick(Sender: TObject);
    procedure ListaColumns1HeaderClick(Sender: TObject);
    procedure ListaColumns2HeaderClick(Sender: TObject);
    procedure ListaColumns3HeaderClick(Sender: TObject);
    } // FIN : TASK_128_MGO_20170131
  private
    FClaseParametro : String;
	function CargarClasesParametros: Boolean;
    function CargarParametrosGenerales: Boolean;                                // TASK_128_MGO_20170131
  public
	{ Public declarations }
	Function Inicializar(const ClaseParametro: Char = '1'; AllowChangeFilter: Boolean = True) : Boolean;
  end;

var
  FormParam: TFormParam;

implementation

{$R *.DFM}
{******************************** Function Header ******************************
Function Name: TFormParam.Inicializar
Author : gcasais
Date Created : 11/07/2005
Description :
Parameters : const ClaseParametro: Char = '1'
Return Value : Boolean
*******************************************************************************}

function TFormParam.Inicializar(const ClaseParametro: Char = '1'; AllowChangeFilter: Boolean = True): Boolean;
begin
	CenterForm(Self);
	PageControl.TabIndex := 0;
    PageControl.ActivePageIndex := 0;
    //Result := CargarClasesParametros and OpenTables([Parametros, Generales]); // TASK_128_MGO_20170131
    Result := CargarClasesParametros and CargarParametrosGenerales;             // TASK_128_MGO_20170131
    
    if Result then begin
        if ClaseParametro in ['C', 'F', 'G', 'I', 'M', 'S', 'V'] then begin
            FClaseParametro := ClaseParametro;
            cbFiltros.ItemIndex := cbFiltros.Items.IndexOfValue(ClaseParametro);
            cbFiltros.OnChange(Self);
        end;
        cbFiltros.Enabled := AllowChangeFilter;
    end;
end;

// INICIO : TASK_128_MGO_20170131
function TFormParam.CargarParametrosGenerales: Boolean;
resourcestring
    MSG_ERROR = 'Ha ocurrido un error al inicializar el formulario';
    MSG_TITLE = 'Mantenimiento de Par�metros Generales';
begin
    Result := True;
    try
        try
            spParametrosGenerales_SELECT.Parameters.Refresh;
            if cbFiltros.Value <> '1' then
                spParametrosGenerales_SELECT.Parameters.ParamByName('@ClaseParametro').Value := cbFiltros.Value;
            spParametrosGenerales_SELECT.Open;

            if spParametrosGenerales_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spParametrosGenerales_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsParametrosGenerales.Data := dtstprParametrosGenerales.Data;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
                Result := False;
            end;
        end;
    finally    
        spParametrosGenerales_SELECT.Close;
    end;
end;
// FIN : TASK_128_MGO_20170131

procedure TFormParam.ButtonCancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormParam.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	//Parametros.Close;                                                         // TASK_128_MGO_20170131
	Action := caFree;
end;

procedure TFormParam.ButtonAceptarClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormParam.ListaLinkClick
  Author:    gcasais
  Date:      14-Oct-2004
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
resourcestring                                                                  // TASK_128_MGO_20170131
    MSG_ERROR = 'Ha ocurrido un error al actualizar el Par�metro General';      // TASK_128_MGO_20170131
    MSG_TITLE = 'Mantenimiento de Par�metros Generales';                        // TASK_128_MGO_20170131
var
    f: TfrmValueEdit;
begin
    Application.CreateForm(TfrmValueEdit, f);
    // INICIO : TASK_128_MGO_20170131
    //if f.Inicializar(Generales, Generales.RecNo) then f.ShowModal;
    try
        if f.Inicializar(cdsParametrosGenerales.FieldByName('Nombre').AsString,
                        cdsParametrosGenerales.FieldByName('Descripcion').AsString,
                        cdsParametrosGenerales.FieldByName('TipoParametro').AsString,
                        cdsParametrosGenerales.FieldByName('Valor').AsString) then begin
            if f.ShowModal = mrOk then begin
                try
                    spParametrosGenerales_UPDATE.Parameters.Refresh;
                    spParametrosGenerales_UPDATE.Parameters.ParamByName('@Nombre').Value := cdsParametrosGenerales.FieldByName('Nombre').Value;
                    spParametrosGenerales_UPDATE.Parameters.ParamByName('@CodSistema').Value := cdsParametrosGenerales.FieldByName('CodigoSistema').Value;
                    spParametrosGenerales_UPDATE.Parameters.ParamByName('@Valor').Value := f.FValor;
                    spParametrosGenerales_UPDATE.Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
                    spParametrosGenerales_UPDATE.ExecProc;

                    if spParametrosGenerales_UPDATE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                        raise Exception.Create(spParametrosGenerales_UPDATE.Parameters.ParamByName('@ErrorDescription').Value);

                    cdsParametrosGenerales.Edit;
                    cdsParametrosGenerales.FieldByName('Valor').Value := f.FValor;
                    cdsParametrosGenerales.Post;
                except
                    on e: Exception do
                        MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
                end;
            end;
        end;
    finally
        f.Release;
    end;
    // FIN : TASK_128_MGO_20170131
end;

{-----------------------------------------------------------------------------
  Procedure: TFormParam.CargarClasesParametros
  Author:    gcasais
  Date:      21-Ene-2005
  Arguments: None
  Result:    Boolean

  Revision :
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}

function TFormParam.CargarClasesParametros: Boolean;
resourcestring
    CLASE_FACTURACION   = 'Facturaci�n';
    CLASE_GENERAL       = 'General';
    CLASE_INTERFACE     = 'Interfaces';
    CLASE_MAIL          = 'Correo Electronico';
    CLASE_REPORTE       = 'Reportes';
    CLASE_SOLICITUD     = 'Solicitudes';
    CLASE_VALIDACION    = 'Validaci�n';
    CLASE_COBRANZAS     = 'Cobranzas';
    CLASE_SOR           = 'SOR';
    MSG_TODOS           = 'Todos los Sistemas';
var
    Qry: TADOQuery;
    Clase: String;
begin
    Result := False;
    Qry:= TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseBO_Master;
    Qry.SQL.Add('SELECT distinct ClaseParametro As Clase From ParametrosGenerales  WITH (NOLOCK) WHERE ClaseParametro IS NOT NULL Order By Clase');

    try
        Qry.Open;
        if Qry.IsEmpty then exit;
        cbFiltros.Items.InsertItem(MSG_TODOS, '1');
        while not Qry.Eof do begin
            Clase := Qry.FieldByName('Clase').AsString;

             if Clase = 'F' then cbFiltros.Items.InsertItem(CLASE_FACTURACION,  Clase);
             if Clase = 'G' then cbFiltros.Items.InsertItem(CLASE_GENERAL,      Clase);
             if Clase = 'I' then cbFiltros.Items.InsertItem(CLASE_INTERFACE,    Clase);
             if Clase = 'M' then cbFiltros.Items.InsertItem(CLASE_MAIL,         Clase);
             if Clase = 'R' then cbFiltros.Items.InsertItem(CLASE_REPORTE,      Clase);
             if Clase = 'S' then cbFiltros.Items.InsertItem(CLASE_SOLICITUD,    Clase);
             if Clase = 'V' then cbFiltros.Items.InsertItem(CLASE_VALIDACION,   Clase);
             if Clase = 'C' then cbFiltros.Items.InsertItem(CLASE_COBRANZAS,    Clase);
             if Clase = 'O' then cbFiltros.Items.InsertItem(CLASE_SOR,          Clase);

            Qry.Next;
        end;

        cbFiltros.ItemIndex := 0;
        Result := True;
    finally
        FreeAndNil(Qry);
    end;
end;

procedure TFormParam.cbFiltrosChange(Sender: TObject);
begin
    { INICIO : TASK_128_MGO_20170131
    if cbFiltros.Value = '1' then begin
        Generales.Filtered := False;
        Exit;
    end;
    Generales.Filtered := False;
    Generales.Filter := 'CLASEPARAMETRO = ' + QuotedStr(cbFiltros.Value);
    Generales.Filtered := True;
    }
    CargarParametrosGenerales;
    // FIN : TASK_128_MGO_20170131
end;

{-----------------------------------------------------------------------------
  Procedure: TFormParam.ListaDrawText
  Author:    gcasais
  Date:      20-Ene-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect:
  TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean
  Result:    None
-----------------------------------------------------------------------------}

procedure TFormParam.ListaDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
resourcestring
    MSG_PARAM_TYPE_INTEGER = 'Valor Entero';
    MSG_PARAM_TYPE_TEXT =    'Texto';
    MSG_PARAM_TYPE_DATETIME = 'Fecha/Hora';
    MSG_PARAM_TYPE_NUMERIC = 'Valor Num�rico';
begin
    if Column.FieldName = 'TipoParametro' then begin
        if TRIM(Text) = TP_ENTERO then  Text :=  MSG_PARAM_TYPE_INTEGER
        else begin
            if TRIM(Text) = TP_TEXTO then Text :=  MSG_PARAM_TYPE_TEXT
            else begin
                if TRIM(Text) = TP_FECHA then Text :=  MSG_PARAM_TYPE_DATETIME
                   else Text := MSG_PARAM_TYPE_NUMERIC;
            end;
        end;
    end;
end;

(* INICIO : TASK_128_MGO_20170131
{-----------------------------------------------------------------------------
  Function Name: ListaColumns0HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns0HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'Descripcion DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'Descripcion ASC';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaColumns1HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns1HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'Valor DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'Valor ASC';
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ListaColumns2HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns2HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'ValorDefault DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'ValorDefault ASC';
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ListaColumns3HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns3HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'TipoParametro DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'TipoParametro ASC';
    end;
end;   
*) // FIN : TASK_128_MGO_20170131

end.
