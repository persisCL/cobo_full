{-----------------------------------------------------------------------------
 Unit Name  : FrmAjusteCaja
 Author     : gleon
 Purpose    : Cambiar el monto en efectivo en caja de un turno.
 Firma      : TASK_031_GLE_20170111
 Descripci�n: Permite al Operador (supervisor) modificar los montos de
              dinero en efectivo asignados a un turno

-----------------------------------------------------------------------------}
unit frmAjusteCaja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppComm, ppRelatv, ppProd, ppClass, ppReport, UtilRB, DB, ppDB,
  ppDBPipe, ADODB, PeaTypes, UtilDB, Util,
  DMConnection, UtilProc, ppBands, ppCache, ppVar, ppCtrls, ppPrnabl,
  ppParameter, CobranzasResources, PeaProcsCN, frmMuestraMensaje, ppStrtch, ppSubRpt,
  ConstParametrosGenerales, PeaProcs, StdCtrls, DmiCtrls, BuscaTab, ExtCtrls,
  frmAjusteEfectivoCaja;

type
  TFormAjusteCaja = class(TForm)
    bvl_Datos: TBevel;
    pnl_Botones: TPanel;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    lbl_Usuario: TLabel;
    cb_Usuario: TComboBox;
    lbl_NumeroTurno: TLabel;
    lbl_Encabezado: TLabel;
    edt_NumeroTurno: TBuscaTabEdit;
    sp_ObtenerListaTurnos: TADOStoredProc;
    bsctbl_Turnos: TBuscaTabla;
    sp_EsTurnoCerrado: TADOStoredProc;
    procedure txt_Importe1Print(Sender: TObject);
    //procedure btn_Consulta_turnosClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    //procedure edt_NumeroTurnoChange(Sender: TObject);
    procedure edt_NumeroTurnoButtonClick(Sender: TObject);
    function bsctbl_TurnosProcess(Tabla: TDataSet; var Texto: string): Boolean;
    procedure bsctbl_TurnosSelect(Sender: TObject; Tabla: TDataSet);
  private
    { Private declarations }
    FNumeroTurno: Integer;
    FUsuarioSistema: AnsiString;
    FPuntoEntrega : Integer;
    FPuntoVenta: Integer;
    FPermitirAjusteCaja: Boolean;
  public
    { Public declarations }
	function Inicializar (NumeroTurno: Int64; UsuarioSistema: AnsiString; PuntoVenta: Int64; PermitirAjuste: Boolean): Boolean;

  end;

var
  FormAjusteCaja: TFormAjusteCaja;

implementation

{$R *.dfm}

{ TFormAjusteCaja }
{-----------------------------------------------------------------------------
  Function Name : TFormAjusteCaja.Inicializar
  Author        : gleon
  Date Created  : 11/01/2017
  Description   : inicializa form, carga valores en combos de usuario
  Parameters    : Sender: TObject
  Return Value  : None
-----------------------------------------------------------------------------}
function TFormAjusteCaja.Inicializar(NumeroTurno: Int64; UsuarioSistema: AnsiString; PuntoVenta: Int64; PermitirAjuste: Boolean): Boolean;
resourcestring
	MSG_CAPTION             = 'Ajuste de Monto en Efectivo en Caja';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    FNumeroTurno := NumeroTurno;
    FUsuarioSistema := UsuarioSistema;
    FPuntoVenta := PuntoVenta;

    Result := False;

    try
        FPermitirAjusteCaja := PermitirAjuste;

        // Se carga y muestra el combo de usuarios
            CargarCodigosUsuariosSistemas(DMConnections.BaseCAC, cb_Usuario);
            lbl_Usuario.Show;
            cb_Usuario.Show;
            Result := True;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			Result := False;
        end;

    end;

end;

{-----------------------------------------------------------------------------
  Function Name : TFormAjusteCaja.btn_AceptarClick
  Author        : gleon
  Date Created  : 11/01/2017
  Description   : abre formulario solicitando nuevo monto del turno y motivo
  Parameters    : Sender: TObject
  Return Value  : None
-----------------------------------------------------------------------------}
procedure TFormAjusteCaja.btn_AceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_NUMERO_VACIO = 'Debe ingresar un N�mero de Turno';
    MSG_ERROR_NUMERO_NEGATIVO = 'El N�mero de Turno debe ser un n�mero positivo';
    MSG_NO_EXISTE_TURNO = 'El Turno ingresado no existe.';
    MSG_TURNO_CERRADO = 'El Turno ingresado no est� cerrado.' + CRLF + 'No se permite ajustar el monto en caja.';
    MSG_TURNO_DE_OTRO_USUARIO = 'El Turno ingresado le pertenece a otro usuario.' + CRLF + 'No se permite emitir el informe.';
    MSG_ENCABEZADO  = 'Monto & Motivo del Cambio';
    MSG_ERROR       = 'Error de ejecuci�n';
var
    montoActual : Integer;
    nroTurnoStr : string;
    Result : Boolean;
    MSGE_CONFIRMA : string;
    UsuarioTurnoAModificar,
    PuntoVenta: AnsiString;
    ExisteTurno,
	EsTurnoCerrado: Boolean;

    f: TfrmAjusteEfectivoCaja;
begin
    MSGE_CONFIRMA  := 'El turno seleccionado tiene un monto en caja de $ %s. Desea realizar alg�n cambio?';
    montoActual := 0;
    Result := True;

    UsuarioTurnoAModificar := EmptyStr;
    PuntoVenta := EmptyStr;

    try
        if (edt_NumeroTurno.Text <> '-') and (edt_NumeroTurno.Text <> EmptyStr) then begin
            FNumeroTurno := StrToInt(edt_NumeroTurno.Text);
        end;

	    //Verifica que el turno ingresado no sea vac�o, ni negativo ni cero.
        if not ValidateControls([edt_NumeroTurno,
    	    edt_NumeroTurno],
    	    [edt_NumeroTurno.Text <> EmptyStr,
            FNumeroTurno > 0],
            Self.Caption,
            [MSG_ERROR_NUMERO_VACIO,
            MSG_ERROR_NUMERO_NEGATIVO]) then Exit;

	    //Verificar que el turno ingresado est� cerrado.
        with sp_EsTurnoCerrado, Parameters do begin

            ParamByName('@NumeroTurno').Value := FNumeroTurno;
            ExecProc;
            ExisteTurno := ParamByName('@ExisteTurno').Value;
            EsTurnoCerrado := ParamByName('@EsCerrado').Value;
            if ParamByName('@UsuarioTurno').Value = Null then begin
                UsuarioTurnoAModificar := EmptyStr;
            end else begin
                UsuarioTurnoAModificar := Trim(ParamByName('@UsuarioTurno').Value);
            end;

            if ParamByName('@PuntoVenta').Value = Null then begin
                PuntoVenta := EmptyStr;
            end else begin
                PuntoVenta := Trim(ParamByName('@PuntoVenta').Value);
            end;
        end;

        if not ExisteTurno then begin
            MsgBox(MSG_NO_EXISTE_TURNO, Self.Caption, MB_ICONSTOP);
            Exit;
        end;

        if not EsTurnoCerrado then begin
    	//Turno NO Cerrado, NO se puede hace ajuste en caja
            MsgBox(MSG_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end
        else begin
    	//Turno Cerrado. Se puede hacer el ajuste

            if FPermitirAjusteCaja then begin
                //Turno Cerrado. Llamar formulario de ajuste

                montoActual := QueryGetValueInt (DMConnections.BaseCAC,
                                            Format('SELECT ImporteEfectivo / 100 FROM Turnos  WITH (NOLOCK) WHERE NumeroTurno = ''%s''',[edt_NumeroTurno.Text]));

                MSGE_CONFIRMA := Format(MSGE_CONFIRMA, [IntToStr(montoActual)]);

                //Cuadro de di�logo que muestra el total del monto del turno seleccionado
                // y pide confirmaci�n para hacer el ajuste
                if MsgBox(MSGE_CONFIRMA, MSG_ENCABEZADO,
                    MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES then begin

                //try
                    //Se cierra el form porque si se hace un preview, queda este form sobre el preview.
                    Close;

                    try
                        Application.CreateForm(TfrmAjusteEfectivoCaja, f);
                        f.Inicializar(FNumeroTurno, UsuarioSistema, montoActual);

                        if f.AjusteOk then  f.ShowModal else f.release;

                   finally
                        if Assigned(f) then FreeAndNil(f);

                    end;

                end
            end
            else begin
                MsgBox(MSG_TURNO_DE_OTRO_USUARIO, Self.Caption, MB_ICONSTOP);
            end;
        end;

    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;

    end;
 end;

procedure TFormAjusteCaja.edt_NumeroTurnoButtonClick(Sender: TObject);
begin
    (* Obtener la lista con los turnos. *)
    sp_ObtenerListaTurnos.Close;
    (* Si el combo de usuario est� habilitado y hay un item v�lido seleccionado,
    pasar el usuario como par�metro al sp.*)
    if (cb_Usuario.Visible) then begin
        if (cb_Usuario.ItemIndex > 0) then begin
            sp_ObtenerListaTurnos.Parameters.ParamByName('@CodigoUsuario').Value := Trim(cb_Usuario.Text);
        end else begin
            sp_ObtenerListaTurnos.Parameters.ParamByName('@CodigoUsuario').Value := Null;
        end;
    end else begin
        (* Filtrar por el usuario logueado. *)
        sp_ObtenerListaTurnos.Parameters.ParamByName('@CodigoUsuario').Value := FUsuarioSistema;
    end; // else if

    sp_ObtenerListaTurnos.Open;
end;

{-----------------------------------------------------------------------------
  Function Name : TFormAjusteCaja.btn_AceptarClick
  Author        : gleon
  Date Created  : 11/01/2017
  Description   : abre ventana para cambiar monto en caja
  Parameters    : Sender: TObject
  Return Value  : None
-----------------------------------------------------------------------------}
procedure TFormAjusteCaja.txt_Importe1Print(Sender: TObject);   // SS_637_PDO_20110503
begin
    if TppDBText(Sender).Visible then begin
        if TppDBPipeline(TppDBText(Sender).DataPipeline).DataSource.DataSet.FieldByName(TppDBText(Sender).DataField).AsInteger < 0 then begin
            TppDBText(Sender).Font.Style := TppDBText(Sender).Font.Style + [fsBold];
        end
        else begin
            TppDBText(Sender).Font.Style := TppDBText(Sender).Font.Style - [fsBold];
        end;
    end;
end;

procedure TFormAjusteCaja.btn_CancelarClick(Sender: TObject);
begin
    Close ;
end;

function TFormAjusteCaja.bsctbl_TurnosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    Texto := PadR(Tabla.FieldByName('NumeroTurno').AsString, 10, ' ')
                + ' - ' + PadR(Tabla.FieldByName('CodigoUsuario').AsString, 20, ' ')
                + ' - ' + PadR(Tabla.FieldByName('FechaHoraCierre').AsString, 10, ' ');
    Result := True;
end;

procedure TFormAjusteCaja.bsctbl_TurnosSelect(Sender: TObject; Tabla: TDataSet);
begin
    edt_NumeroTurno.Text := Tabla.FieldByName('NumeroTurno').AsString;
end;

end.
