{-----------------------------------------------------------------------------
 File Name: ReporteFactura.pas
 Author:    ndonadio
 Date Created: 27/12/2004
 Language: ES-AR
 Description: Reporte para imprimir un comprobante tipo Nota de Cobro (NK)
    luego del proceso de facturacion manual para un convenio.
    Imprime todo, no usa preimpresos.
    28/02/2005: Modificada para usar preimpresos (ndonadio)
    01/03/2005: Agregada segunda hoja. Si son mas de 17 vehiculos
                el Detalle de Consumos va en otra hoja.
    08/04/2005: Agragada funcionalidad para tomar impresoras y bandejas del
                application.ini y consultar por diferentes opciones de impresion
    11/04/2005: Agregado forzado de formato de fecha para que no use la
                definida en la Conf. Regional del Windows.
 Revision 1:
    Author: ggomez
    Date: 25/11/2005
    Description: Cambi� la forma de obtener los Consumos por Cuenta de cada
        comprobante. Ahora se hace por pasos, pues para comprobantes con muchos
        tr�nsitos daba Timeout.

 Revision 2:
    Author: ggomez
    Date: 27/04/2006
    Description: Sub� el TimeOut de los SP a 500.

 Revision 3:
    Author: jconcheyro
    Date: 13/06/2006
    Description: Se agrega un campo con la fecha de hoy para la impresi�n din�mica

Revision 4:
    Author : ggomez
    Date : 8/4/2006
    Description : En el control de la fecha del "Total a Pagar al" sete� la
        propiedad DisplayFormat con dd'/'mm'/'yyyy.

Revision 5:
    Author: nefernandez
    Date: 22/03/2007
    Description : Se agreg� un par�metro, con un valor por defecto, para la funci�n
    ejecutar. Esto permite indicar si las opciones disponibles para este reporte
    aparecer�n checkeadas o no (por defecto).

Revision 6:
    Author: jconcheyro
    Date: 28/05/2007
    Description : Pedido urgente de Beni, la NK tarda en imprimir, entonces levantamos
    todos los transitos juntos.

Revision 7:
    Author: FSandi
    Date: 11-09-2007
    Description : SS 544 - El detalle de consumos ahora se obtiene de una tabla, no se utilizan
                los peajes para calcularlo.
Revision 9:
    Author: Nelson Droguett Sierra
    Date: 17-06-2009
    Description : Se fija el fondo del documento, que no dependa de la fecha de corte.

Firma           : SS_1332_CQU_20151106
Descripcion     : Se agrega un control al generar un comprobante en PDF,
                  ahora verifica que el archivo se haya creado en la carpeta temporal

-----------------------------------------------------------------------------}
unit ReporteFactura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppBands, ppReport, ppSubRpt, {ppChrt, ppChrtDP,} ppCtrls,
  ppStrtch, ppMemo, ppParameter, ppBarCod, jpeg, ppPrnabl, ppClass, UtilRB,
  ppCache, ppComm, ppRelatv, ppProd, ppModule, raCodMod, DB,
  ADODB, ppDB, ppDBPipe,util,utilproc, ppVar, StdCtrls, daDataModule,
  ppRegion, ppCTMain, ppTxPipe, rbSetup, TeEngine, Series, ExtCtrls,
  TeeProcs, Chart, DbChart, TXComp, ppDevice, ConstParametrosGenerales, Printers, PeaTypes, UtilDb,
  ImprimirWO, TXRB, ppPDFDevice;

type
  TTipoPago = (tpNinguno, tpPAT, tpPAC);
  TfrmReporteFactura = class(TForm)
    rptReporteFactura: TppReport;
    rbiFactura: TRBInterface;
    ppParameterList1: TppParameterList;
    spComprobantes: TADOStoredProc;
    dsComprobantes: TDataSource;
    dsUltimosDoce: TDataSource;
    dsObtenerConsumos: TDataSource;
    spUltimosDoce: TADOStoredProc;
    ppConsumos: TppDBPipeline;
    dsMensaje: TDataSource;
    spMensaje: TADOStoredProc;
    ppMensaje: TppDBPipeline;
    spMensajeTexto: TStringField;
    spComprobantesNumeroConvenioFormateado: TStringField;
    spComprobantesNumeroConvenio: TStringField;
    spComprobantesCodigoConvenio: TIntegerField;
    spComprobantesTipoComprobante: TStringField;
    spComprobantesNumeroComprobante: TLargeintField;
    spComprobantesPeriodoInicial: TDateTimeField;
    spComprobantesPeriodoFinal: TDateTimeField;
    spComprobantesFechaEmision: TDateTimeField;
    spComprobantesFechaVencimiento: TDateTimeField;
    spComprobantesNombreCliente: TStringField;
    spComprobantesDomicilio: TStringField;
    spComprobantesComuna: TStringField;
    spComprobantesComentarios: TStringField;
    spComprobantesCodigoBarras: TStringField;
    spComprobantesCodigoPostal: TStringField;
    spComprobantesTotalAPagar: TStringField;
    spObtenerCargos: TADOStoredProc;
    dsObtenerCargos: TDataSource;
    ppObtenerCargos: TppDBPipeline;
    spObtenerCargosDescripcion: TStringField;
    spObtenerCargosImporte: TLargeintField;
    spObtenerCargosDescImporte: TStringField;
    ppUltimosDoce: TppDBPipeline;
    dbcUltimosDoce: TDBChart;
    spComprobantesCodigoTipoMedioPago: TWordField;
    spComprobantesEstadoPago: TStringField;
    spObtenerConsumos: TADOStoredProc;
    spObtenerConsumosNroMatricula: TStringField;
    spObtenerConsumosDImporteCongestion: TStringField;
    spObtenerConsumosDImportePunta: TStringField;
    spObtenerConsumosDImporteNormal: TStringField;
    spObtenerConsumosDImporte: TStringField;
    Series1: TBarSeries;
    spUltimosDoceIDh: TDateTimeField;
	spUltimosDoceMes: TStringField;
    spUltimosDoceTotalAPagar: TBCDField;
    //spObtenerConsumosKilometros: TBCDField;	// SS_660_CQU_20140317
    spObtenerConsumosKilometros: TStringField;	// SS_660_CQU_20140317
    rptReporteConsumo: TppReport;
    ppDetailBand5: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    rbiConsumo: TRBInterface;
    ppHeaderBand2: TppHeaderBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine7: TppLine;
    ppLabel2: TppLabel;
    ppLblCongestion17: TppLabel;
    ppLblPunta17: TppLabel;
    pplblNormal17: TppLabel;
    pplblKm17: TppLabel;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppLabel14: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel27: TppLabel;
    ppLine5: TppLine;
    ppLine8: TppLine;
    ppLbltotal17: TppLabel;
    ppDBText14: TppDBText;
    ppRegion2: TppRegion;
    ppShape10: TppShape;
    ppShape11: TppShape;
    ppShape12: TppShape;
    ppLabel1: TppLabel;
    ppShape13: TppShape;
    ppLabel3: TppLabel;
    ppShape14: TppShape;
    ppShape15: TppShape;
    ppShape16: TppShape;
    ppLine6: TppLine;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppLabel19: TppLabel;
    ppShape19: TppShape;
    Label1: TLabel;
    Label2: TLabel;
    ppLine11: TppLine;
    ppLine12: TppLine;
    spComprobantesSaldoPendiente: TStringField;
    spComprobantesAjusteSencilloAnterior: TStringField;
    spComprobantesAjusteSencilloActual: TStringField;
    ppComprobantes: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppImageFondo: TppImage;
    ppRegion1: TppRegion;
    ppShape6: TppShape;
    ppShape5: TppShape;
    ppShape3: TppShape;
    ppLabel4: TppLabel;
    ppShape4: TppShape;
    ppLabel15: TppLabel;
    ppShape7: TppShape;
    ppShape8: TppShape;
    ppShape9: TppShape;
    ppLine4: TppLine;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppdbtPerFin: TppDBText;
    ppdbtFEmision: TppDBText;
    ppdbtPerInicio: TppDBText;
    ppdbtNroNK: TppDBText;
    ppdbtVencimiento: TppDBText;
    ppLine9: TppLine;
    ppDetailBand1: TppDetailBand;
    imgGraficoUltimosDoce: TppImage;
    ppShape18: TppShape;
    ppShape17: TppShape;
    ppdbbcConvenio: TppDBBarCode;
    ppdbtNombre: TppDBText;
    ppdbtDir1: TppDBText;
    ppdbtDir2: TppDBText;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLabel12: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppLblAnexo: TppLabel;
    raCodeModule1: TraCodeModule;
    ppdbtTotalAPagar: TppDBText;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppDBMemo1: TppDBMemo;
    ppSummaryBand2: TppSummaryBand;
    raCodeModule2: TraCodeModule;
    ppLabel25: TppLabel;
    ppSubReport3: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    raCodeModule4: TraCodeModule;
    ppLabel20: TppLabel;
    ppdbtNumConvenioFormat: TppDBText;
    ppLine3: TppLine;
    ppLabel26: TppLabel;
    pplblTotal: TppLabel;
    pplblCongestion: TppLabel;
    pplblPunta: TppLabel;
    pplblNormal: TppLabel;
    pplblKm: TppLabel;
    ppLine10: TppLine;
    ppLabel13: TppLabel;
    lblTestLegth: TppLabel;
    ppSummaryBand3: TppSummaryBand;
    ppdbbcConvenio2: TppDBBarCode;
    ppdbtTotalAPagar2: TppDBText;
    ppdbtVencimiento2: TppDBText;
    ppdbtConvenio2: TppDBText;
    ppdbtNroNK2: TppDBText;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    imgPagada: TppImage;
    imgPACPAT: TppImage;
    imgPublicidad: TppImage;
    raCodeModule3: TraCodeModule;
    ExtraOptions1: TExtraOptions;
    ppLblCopiaFiel: TppLabel;
    ppReport1: TppReport;
    ppHeaderBand3: TppHeaderBand;
    ppDetailBand6: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppBarCode2: TppBarCode;
    ppBarCode1: TppBarCode;
    ppBarCode3: TppBarCode;
    ppBarCode4: TppBarCode;
    ppBarCode5: TppBarCode;
    ppBarCode6: TppBarCode;
    ppDBText23: TppDBText;
    pplblAnulada: TppLabel;
    ppDBText24: TppDBText;
    spComprobantesFechaHoy: TDateTimeField;
    ppComprobantesppField22: TppField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rptReporteConsumoPreviewFormCreate(Sender: TObject);
    procedure rptReporteFacturaBeforePrint(Sender: TObject);
    procedure ppdbbcConvenio2Print(Sender: TObject);
    procedure ppLblCopiaFielPrint(Sender: TObject);
    procedure pplblAnuladaPrint(Sender: TObject);
    procedure rbiFacturaExecute(Sender: TObject; var Cancelled: Boolean);
   private
    { Private declarations }
    FTipoComprobante: String;
    FNumeroComprobante: Int64;
    FDirImagenPublicidad, FDirImagenPagada, FDirImagenPAT, FDirImagenPAC,
//    FDirLogo, FNombreImagenLogo,
    FNombreImagenPagada, FNombreImagenPublicidad,FNombreImagenFondo,
    FDirImagenFondo, FNombreImagenPAC,
    FNombreImagenPAT: AnsiString;
    FConexion: TADOConnection;
    FTipoPago : TTipoPago;
    FMostrarConsumoAparte: Boolean;
    FIncluirFondo: boolean;
    FIsPDF: Boolean;

    Linea: integer;
    FPreparado: boolean;
    FEsConfiguracion : boolean;
    procedure Preparar;
    procedure ObtenerConsumosComprobante(TipoCompro: AnsiString; NumeroCompro: Int64);
  public
    { Public declarations }
        FImprimioComprobante: Boolean;

        function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string; IncluirFondo: Boolean = True; EsWEB: boolean = false): Boolean;
        //function Ejecutar:boolean; //(PRN_NK, BIN_NK, PRN_DETCON, BIN_DETCON: String): Boolean;
        // Revision 5
        function Ejecutar(ActivarOpciones: Boolean = False): Boolean;
        function EjecutarPDF(var Resultado: string; var Error: String):Boolean;
        function EjecutarDetallePDF(var Resultado: string; var Error: String):Boolean;

        function ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
        function ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;

        property MostrarConsumoAparte: boolean read FMostrarConsumoAparte;

        //Indicar que es s�lo configuraci�n de la impresora
        procedure ConfigurarDestino(EsConfigurar : boolean);
  end;



var
  frmReporteFactura: TfrmReporteFactura;

implementation

{$R *.dfm}


resourcestring
    ERROR_INIT                        = 'Error de Inicializaci�n';
    ERROR_PRINTING                    = 'Error de Impresi�n';
    ERROR_INIT_DESCRIPTION            = 'No Se Encuentra El Comprobante';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ndonadio
  Date Created: 20/12/2004
  Description: Inicializa y adquiere los datos para imprimir el comprobante
  Parameters: ninguno
  Return Value: Boolean

Revision 1:
    Author : ggomez
    Date : 22/06/2006
    Description :
        - Correg� ortograf�a del resourcestring.
        - Agregu� comentarios.
        - Agregu� nolock al query que obtiene la Fecha de Creaci�n.
        - Cambi� el nombre del par�metro general que almacena el nombre de la
        imagen de fondo con el RUT Anterior.

-----------------------------------------------------------------------------}
function TfrmReporteFactura.Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string; IncluirFondo: Boolean = True; EsWEB: boolean = false): Boolean;
resourcestring
    MSG_MISSED_IMAGES_FILES = 'No existen todas las im�genes requeridas para '
        + CRLF + 'poder imprimir una Nota de Cobro en el sistema';
    MSG_ERROR = 'Error';
var
  FechaCreacion , FechaDeCorte  : TDateTime;
begin
    try
        if not Assigned(Conexion) then raise Exception.Create('Se debe especificar una Conexi�n');
        if not Conexion.Connected then Conexion.Open;
        FTipoComprobante := TipoComprobante;
        FNumeroComprobante := NumeroComprobante;
        FConexion := Conexion;
        //Asignamos la conexion que nos indicaron usar
        spComprobantes.Connection := Conexion;
        spObtenerCargos.Connection := Conexion;
        spUltimosDoce.Connection := Conexion;
        spObtenerConsumos.Connection := Conexion;
        spMensaje.Connection := Conexion;

        //revision :1
        {FechaCreacion := QueryGetValueDateTime(spComprobantes.Connection,
            ' select FechaCreacion '
            + ' from Comprobantes with (nolock)'
            + ' where (NumeroComprobante = ' + IntToStr(FNumeroComprobante) + ')'
                + ' and (TipoComprobante = ''' + FTipoComprobante + ''')'); }

        // Obtener la fecha desde cuando la concesionaria tiene un nuevo RUT.
        {ObtenerParametroGeneral(Conexion, 'FECHA_DE_CORTE_RUT', FechaDeCorte);}

        // Si la Fecha de Creaci�n del Recibo es anterior a la Fecha desde cuando la
        // concesionaria tiene nuevo RUT, entonces imprimir el RUT Anterior, sino
        // imprimir el RUT Nuevo.
        {if ( FechaCreacion < FechaDeCorte ) then begin
            ObtenerParametroGeneral(Conexion, 'IMAGEN_FONDO_NOTA_COBRO_ANTERIOR', FNombreImagenFondo);
        end else begin
            ObtenerParametroGeneral(Conexion, 'IMAGEN_FONDO_NOTA_COBRO', FNombreImagenFondo);
        end; // else if
        //fin revision :1
        }
        // Rev.9 / 17-06-2009 / Nelson Droguett Sierra ----------------------------------
        ObtenerParametroGeneral(Conexion, 'IMAGEN_FONDO_NOTA_COBRO', FNombreImagenFondo);
        // ------------------------------------------------------------------------------
        if EsWeb then begin
   		    ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPublicidad);
            ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPagada);
            ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPAT);
            ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPAC);
            ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGENES_NC', FDirImagenFondo);
        end else begin
    		ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_PUBLICIDAD', FDirImagenPublicidad);
            ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_PAGADA', FDirImagenPagada);
            ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_SELLO_PAT', FDirImagenPAT);
            ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_SELLO_PAC', FDirImagenPAC);
            ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_FONDO_NOTA_COBRO', FDirImagenFondo);
        end;


        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_PUBLICIDAD', FNombreImagenPublicidad);
        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_PAGADA', FNombreImagenPagada);
        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_SELLO_PAT', FNombreImagenPAT);
        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_SELLO_PAC', FNombreImagenPAC);

		FDirImagenPublicidad := GoodDir(FDirImagenPublicidad);
        FDirImagenPAT := GoodDir(FDirImagenPAT);
        FDirImagenPAC := GoodDir(FDirImagenPAC);
        FNombreImagenPublicidad := FDirImagenPublicidad + FNombreImagenPublicidad;
        FNombreImagenPAT := FDirImagenPAT + FNombreImagenPAT;
        FNombreImagenPAC := FDirImagenPAC + FNombreImagenPAC;
        FDirImagenPagada := GoodDir(FDirImagenPagada);
        FNombreImagenPagada := FDirImagenPagada + FNombreImagenPagada;
        FDirImagenFondo := GoodDir(FDirImagenFondo);
        FNombreImagenFondo := FDirImagenFondo + FNombreImagenFondo;
// Determina si usa el fondo o no
        FIncluirfondo := IncluirFondo;

        // Verificamos que existan las im�genes que se necesitan obligatoriamente para poder
        // imprimir una nota de cobro
        //PAT y PAC
        if (not FileExists(FNombreImagenPAT))
          or (not FileExists(FNombreImagenPAC)) then begin
            raise Exception.Create(MSG_MISSED_IMAGES_FILES);
        end;
        // Fondo
        if (not FileExists(FNombreImagenFondo)) then begin
            raise Exception.Create(MSG_MISSED_IMAGES_FILES);
        end;
        //Pagada
        if (not FileExists(FNombreImagenPagada)) then begin
            raise Exception.Create(MSG_MISSED_IMAGES_FILES);
        end;

        FPreparado := False;
        FEsConfiguracion := False;
        Result := True;
    except
        on e:Exception do begin
            Result := False;
            Error := e.Message;
        end;
    end;
end;

{--------------------------------------------------------------------------
                	ConfigurarDestino

Author:mbecerra
Date: 16-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Permite indicar que la impresi�n es s�lo de configuraci�n
----------------------------------------------------------------------------}
procedure TfrmReporteFactura.ConfigurarDestino;
begin
	FEsConfiguracion := EsConfigurar;
end;

{-----------------------------------------------------------------------------
  Function Name: Ejecutar
  Author:    ndonadio
  Date Created: 28/12/2004
  Description: Abre los origenes de datos con los parametros pasados, genera la informacion a
            mostrar y muestra el reporte.
  Parameters: TipoComprobante: string; NumeroComprobante: int64; M:Integer; EPS, Servicio: string
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmReporteFactura.Ejecutar(ActivarOpciones: Boolean = False): Boolean;
resourcestring
    MSG_DETAIL_FREE_UNTIL_DATE  = 'La facturacion detallada es gratuita hasta el d�a %s';
    MSG_DETAIL_FREE_UNTIL_ZERO  = 'Le restan %d impresiones de la facturacion detallada';
    MSG_DETAIL_CHARGED          = 'La facturaci�n detallada tiene un costo de $ %d';
    MSG_DETAIL_FREE_ALWAYS      = 'La facturacion detallada es gratuita.';
    MSG_WANT_DETAIL             = 'Desea imprimir la Facturaci�n Detallada?';
    TITLE_DETAIL                = 'Facturaci�n Detallada';
    PRINTING_COMPLETE           = 'La impresi�n ha sido satisfactoria?';
    PRINTING_TITLE              = 'Facturaci�n Detallada';

    MSG_PRINTING_DETCON          = 'El Detalle de Consumos se imprimir� en otra hoja, desa imprimirlo?';
    MSG_PRINTING_DETCON_TITLE   = 'Impresi�n de Detalle de Consumos'                        ;
    MSG_PRINT_OPTIONS_AND_PREVIEW = 'Mostrar opciones de configuraci�n y previsualizaci�n?';
    MSG_PRINT_BAKGROUND_IMAGE   = 'Incluir Fondo de la Nota de Cobro?';
    MSG_PRINTING_INVOICE_TITLE  = 'Impresi�n de comprobante';
    MSG_PRINTING_INVOICE        = 'El Comprobante se va a imprimir en: ' + crlf +'%s ( %s )'+crlf+crlf+'Confirme la impresi�n';

var
    DfltConfig: TRBConfig;
    FImp: TRespuesta;
    FActivarOpciones: Array[1..2] of Integer; //Revision 5
begin

    try
        FillChar(FImp, SizeOf(FImp), 0);
        FIsPDF := False;
        DfltConfig := rbiFactura.GetConfig;
        // Revision 5.
        if ActivarOpciones then begin
            FActivarOpciones[1] := 1;
            FActivarOpciones[2] := 1;
        end
        else begin
            FActivarOpciones[1] := 0;
            FActivarOpciones[2] := 0;
        end;

        Fimp := ImprimirWO.Ejecutar(MSG_PRINTING_INVOICE_TITLE,
        Format(MSG_PRINTING_INVOICE, [DfltConfig.PrinterName, DfltConfig.BinName]),
        ['Si','No'], [MSG_PRINT_BAKGROUND_IMAGE, MSG_PRINT_OPTIONS_AND_PREVIEW],
        FActivarOpciones, twoCHeck);

        //Si no desea imprimir, sale...
        if FImp[0] = mrCancel then begin
            Result := False;
            Exit;
        end;
        //Tengo que incluir el fondo?
        FIncluirFondo := (FImp[1] = 1);

        // Si elegi Opciones de Impresion, por defecto va a Vista Previa
        if (FImp[2]=1) then DfltConfig.DeviceType := 'Screen';
        rptReporteFactura.DeviceType :=  DfltConfig.DeviceType;

        //rbiFactura.Report.PrinterSetup.BinName := BIN_NK;
        rbiFactura.SetConfig(DfltConfig);

        //Ejecuto el report...
        Result := rbiFactura.Execute( (FImp[2]=1) );
        FImprimioComprobante := Result;

        if not Result then Exit;

        if (FMostrarConsumoAparte) then begin
            Fimp := ImprimirWO.Ejecutar( MSG_PRINTING_DETCON_TITLE,MSG_PRINTING_DETCON,['Si','No'],
              [MSG_PRINT_OPTIONS_AND_PREVIEW],[0], twoCHeck );
            if FImp[0] = mrOK then begin
                Result := rbiConsumo.Execute(FImp[1] = 1);
            end;
        end;

    except
        Result := False;
        FImprimioComprobante := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    ndonadio
  Date Created: 28/12/2004
  Description: Cierra los origenes de datos al cerrar el Form.
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmReporteFactura.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    spComprobantes.Close;
    spUltimosDoce.Close;
    spMensaje.Close;
    spObtenerConsumos.Close;
end;

procedure TfrmReporteFactura.Preparar;

var
    TmpFile: String;
    CodigoTipoMedioPago: Byte;
    Pagada : Boolean;
	OldDecimalSeparator : char;
    OldThousandSeparator : char;
    Desplazamiento: single;
    oldHeight: single;
    oldWidth: single;
    CantVehiculos: integer;
begin
    FPreparado := true;
    if spComprobantes.Active    then spComprobantes.Close;
    Linea := 1;
    if spObtenerCargos.Active   then spObtenerCargos.Close;
    Linea := 2;
    if spObtenerConsumos.Active then spObtenerConsumos.Close;
    Linea := 3;
    if spMensaje.Active         then spMensaje.Close;
    Linea := 4;
    if spUltimosDoce.Active     then spUltimosDoce.Close;
    Linea := 5;

    // Guarda los separadores decimales y los cambia
    // por "," para decimales y "." para miles
    OldDecimalSeparator	:= DecimalSeparator;
    OldThousandSeparator:= ThousandSeparator;
    DecimalSeparator	:= ',';
    ThousandSeparator	:= '.';
    spComprobantes.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
    spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
    Linea := 6;
    spComprobantes.Open;
    Linea := 7;
    //Determinamos el tipo de medio de pago del comprobante y si est� pagado
    CodigoTipoMedioPago := spComprobantes.FieldByName('CodigoTipoMedioPago').AsInteger;
    Linea := 8;
    Pagada := (spComprobantes.FieldByName('EstadoPago').AsString = 'P');
    Linea := 9;
    FTipoPago := TTipoPago(CodigoTipoMedioPago);
    Linea := 9;
    spObtenerCargos.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
    spObtenerCargos.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
    Linea := 10;
    spObtenerCargos.Open;
    Linea := 11;

//    spObtenerConsumos.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
//    spObtenerConsumos.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
    Linea := 12;
    ObtenerConsumosComprobante(FTipoComprobante, FNumeroComprobante);
//    spObtenerConsumos.Open;
    Linea := 13;

    spMensaje.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
    spMensaje.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
    Linea := 14;              
    spMensaje.Open;
    Linea := 15;

    spUltimosDoce.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
    spUltimosDoce.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
    Linea := 16;
    spUltimosDoce.Open;
    Linea := 17;

    // Creamos el gr�fico y lo guardamos como imagen a disco
    dbcUltimosDoce.RefreshData ;
    Linea := 18;
    TmpFile := GoodDir(GetTempDir) + TempFile + '.bmp';
    Linea := 19;
    dbcUltimosDoce.SaveToBitmapFile(TmpFile);
    Linea := 20;

    // Asigno la imagen del grafico al img del Report
    imgGraficoUltimosDoce.Picture.LoadFromFile(TmpFile);
    Linea := 21;
    DeleteFile(TmpFile);
    Linea := 22;

    //determinamos si lleva fondo o no, y lo cargamos
    if FIncluirFondo then begin
        ppImageFondo.Visible := True;
        ppImageFondo.Picture.LoadFromFile(FNombreImagenFondo);
    end else ppImageFondo.Visible := False;

    Linea := 23;


    //Determinamos cuales de todas las imagenes de pago tenemos que mostrar
    imgPACPAT.Visible := False;
    imgPagada.Visible := False;
    if (Pagada) and (FTipoPago = tpNinguno) and FileExists(FNombreImagenPagada) then begin
        imgPagada.Picture.LoadFromFile(FNombreImagenPagada);
        imgPagada.Visible := True;
        Linea := 24;
    end else begin
        case FTipoPago of
            tpPAC:
                begin
                    imgPACPAT.Picture.LoadFromFile(FNombreImagenPAC);
                    imgPACPAT.Visible := True;
                    Linea := 25;
                end;
            tpPAT:
                begin
                    imgPACPAT.Picture.LoadFromFile(FNombreImagenPAT);
                    imgPACPAT.Visible := True;
                    Linea := 26;
                end;
        end;
    end;
    Linea := 27;
    if FileExists(FNombreImagenPublicidad) then
      imgPublicidad.Picture.LoadFromFile(FNombreImagenPublicidad);
    Linea := 28;

    // Vuelve a dejar los separadores decimales como estaban
    DecimalSeparator  := OldDecimalSeparator;
    ThousandSeparator := OldThousandSeparator;

    if not FIsPDF then begin
        // Chequea que no haya lineas sin completar en la direccion
        Desplazamiento := 0;
        if    (  spComprobantes.FieldByName('Domicilio').IsNull)
           or (  TRIM( spComprobantes.FieldByName('Domicilio').AsString) = '')
        then begin
                Linea := 29;
                With  ppdbtDir1 do begin
                    Desplazamiento := Desplazamiento +  Height;
                    Visible := False;
                end;
                Linea := 30;
        end
        else begin
                Linea := 31;
               if Length(ppdbtDir1.Text) < 40 then begin
                    Linea := 32;
                    oldHeight   := ppdbtDir1.Height;
                    oldWidth    := ppdbtDir1.Width ;
                    ppdbtDir1.Autosize := True;
                    Desplazamiento := Desplazamiento + (oldHeight - ppdbtDir1.Height);
                    ppdbtDir1.Autosize := False;
                    ppdbtDir1.Width := oldWidth;
                    Linea := 33;
               end;
               ppdbtDir1.WordWrap := True;
               Linea := 34;
        end;
        lblTestLegth.Visible := FALSE;
        Linea := 35;

        if spComprobantes.FieldByName('Comuna').IsNull
           or (  TRIM( spComprobantes.FieldByName('Comuna').AsString) = '')
        then begin
                Linea := 36;
                    Desplazamiento := Desplazamiento +  ppdbtDir2.Height;
                    ppdbtDir2.Visible := False;
                Linea := 37;
        end
        else begin
            Linea := 38;
            ppdbtDir2.Top := ppdbtDir2.Top - Desplazamiento;
            Linea := 39;
        end;
        if spComprobantes.FieldByName('CodigoPostal').IsNull
           or (  TRIM( spComprobantes.FieldByName('CodigoPostal').AsString) = '')
        then begin
                    ppdbbcConvenio.Visible := False;
        end
        else begin
            ppdbbcConvenio.Top := ppdbbcConvenio.Top - Desplazamiento;
        end;

        Linea := 40;

        ppdbtNumConvenioFormat.Top := ppdbtNumConvenioFormat.Top - Desplazamiento - iif( ppdbbcConvenio.Visible, 0, ppdbbcConvenio.Height);
        pplabel20.Top := pplabel20.Top - Desplazamiento - iif( ppdbbcConvenio.Visible, 0, ppdbbcConvenio.Height);
        Linea := 41;

    end;
    lblTestLegth.Visible := FALSE;

    pplblKm.Caption         := iif( spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value );

                            { iif( spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
                            Trim(Format('%.2n', [real(spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value)] )));  }
    Linea := 42;
    pplblNormal.Caption     := iif( spObtenerConsumos.Parameters.ParamByName('@TotalN').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalN').Value );
    Linea := 43;
    pplblCongestion.Caption := iif( spObtenerConsumos.Parameters.ParamByName('@TotalC').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalC').Value );
    Linea := 44;
    pplblPunta.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalP').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalP').Value );
    Linea := 45;
    pplblTotal.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value );

    Linea := 46;
    CantVehiculos :=  spObtenerconsumos.RecordCount ;
    Linea := 47;
    FMostrarConsumoAparte := False;
    if CantVehiculos > 17 then begin
           Linea := 48;
           ppDBText1.Visible := False;
           ppDBText2.Visible := False;
           ppDBText3.Visible := False;
           ppDBText4.Visible := False;
           ppDBText5.Visible := False;
           ppDBText6.Visible := False;
           ppLblAnexo.Visible := True;
           ppchildReport1.Bands[1].Height := 2.2080;

            pplblKm17.Caption         := iif( spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
                                    Trim(Format('%.2n', [real(spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value)] )));
            pplblNormal17.Caption     := iif( spObtenerConsumos.Parameters.ParamByName('@TotalN').Value = null,'',
                                    spObtenerConsumos.Parameters.ParamByName('@TotalN').Value );
            pplblCongestion17.Caption := iif( spObtenerConsumos.Parameters.ParamByName('@TotalC').Value = null,'',
                                    spObtenerConsumos.Parameters.ParamByName('@TotalC').Value );
            pplblPunta17.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalP').Value = null,'',
                                    spObtenerConsumos.Parameters.ParamByName('@TotalP').Value );
            pplblTotal17.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value = null,'',
                                    spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value );

             FMostrarConsumoAparte := True;
           Linea := 49;
    end
    else begin
           Linea := 50;
           ppDBText1.Visible := True;
           ppDBText2.Visible := True;
           ppDBText3.Visible := True;
           ppDBText4.Visible := True;
           ppDBText5.Visible := True;
           ppDBText6.Visible := True;
           ppLblAnexo.Visible := False;
           ppchildReport1.Bands[1].Height := 0.1775;
           FMostrarConsumoAparte := False;
           Linea := 51;
    end;
    Linea := 52;

    // Verificamos que si es Web (IsPDF) el BarCode no se zarpe!
    // medidas en inches!
//    ppdbbcConvenio2.AutoSize := False;
//    ppdbbcConvenio2.WideBarRatio := 2;


    //Verificamos  la fechahora impreso
    //      si era null, hacemos un SET
    //      sino hacemos visible la lbl COPIA FIEL DEL ORIGINAL
    if ( QueryGetValueInt(FConexion, 'SELECT ISNULL('+
                                  '(Select 1 From Comprobantes with (nolock) '+
                                  Format(' Where TipoComprobante = ''%s'' and NumeroComprobante = %d ',
                                  [FTipoComprobante, FNumeroComprobante])+
                                  'AND FechaHoraImpreso IS NOT NULL )'+
                                  ',0)') = 0 ) then begin
            // era null --> es original
            // Le debo setear la fechahora impreso... ???
            //ppLblCopiaFiel.Top := ppdbtDir1.Top;
           Linea := 53;
           ppLblCopiaFiel.Visible := False;
    end
    else begin
           Linea := 54;
            // es una copia
            ppLblCopiaFiel.Visible := True;
    end;

    Linea := 55;
    pplblAnulada.Visible := spComprobantes.FieldByName('EstadoPago').AsString = 'A';
    ppLblCopiaFiel.Visible := ppLblCopiaFiel.Visible and not(pplblAnulada.Visible);
    Linea := 56;

    if ppImageFondo.Top <> 0 then  ppImageFondo.Top := 0;
    Linea := 100;
end;

{------------------------------------------------------------------
                          rbiFacturaExecute

Author: mbecerra
Date: 17-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Permite imprimir o no el documento
-----------------------------------------------------------------------}
procedure TfrmReporteFactura.rbiFacturaExecute(Sender: TObject;
  var Cancelled: Boolean);
begin
	Cancelled := FEsConfiguracion;
end;

function TfrmReporteFactura.EjecutarPDF(var Resultado: string; var Error: String):Boolean;
begin
    //Ejecuto el report...
    try
        FIsPDF := True;
//        Preparar;  //ahora lo llama desde el BeforePrint...
		Result := reporteAPDF(rptReporteFactura, Resultado, Error);
	except
        on e:Exception do begin
            Error := 'EjecutarPDF: ' + e.Message;
            Result := False;
        end;
    end;
end;

function TfrmReporteFactura.ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
begin
    try
        Dispositivo.Publisher := Reporte.Publisher;
        Dispositivo.Reset;
        Reporte.PrintToDevices;
        result := true;
    except
        on e: exception do begin
            result := false;
            Error := '[ExportarReporte] [' + iif(FPreparado, 'Preparado Linea: ' + inttostr(Linea), 'No Preparado') +  '] ' + e.message;
        end;
    end;
end;

function TfrmReporteFactura.ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;
var
    Exportador: TppPDFDevice;
    Salida: TStringStream;
    Salida2: TMemoryStream;
begin
    try
        Salida := TStringStream.Create('');
        Salida2 := TMemoryStream.Create;
        Error := '';
        Exportador := TppPDFDevice.Create(nil);

        Exportador.PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
        Exportador.PDFSettings.ScaleImages := False;
        Exportador.FileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz_', Now) + IntToStr(FNumeroComprobante) + '.pdf';

//    Exportador.PrintToStream := True;
//    Exportador.ReportStream := Salida;

        Result := ExportarReporte(Reporte, Exportador, Error);

        //CopyFile(PChar(Exportador.FileName), PChar('C:\' + ExtractFileName(Exportador.FileName)), False);
        if (Error = '') and FileExists(Exportador.FileName) then    // SS_1332_CQU_20151106
        begin                                                       // SS_1332_CQU_20151106
        // Aumento el tabulado                                      // SS_1332_CQU_20151106
            Salida2.LoadFromFile(Exportador.FileName);
            Salida2.Position := 0;
            Salida2.SaveToStream(Salida);

            Resultado := Salida.DataString;
        end;                                                        // SS_1332_CQU_20151106
    finally
      if Assigned(Exportador) then FreeAndNil(Exportador);
      if Assigned(Salida) then FreeAndNil(Salida);
      if Assigned(Salida2) then FreeAndNil(Salida2);
    end;
end;


procedure TfrmReporteFactura.rptReporteConsumoPreviewFormCreate(
  Sender: TObject);
begin
	rptReporteConsumo.PreviewForm.SendToBack ;
end;


procedure TfrmReporteFactura.rptReporteFacturaBeforePrint(Sender: TObject);
begin
    Preparar;
end;

procedure TfrmReporteFactura.ppdbbcConvenio2Print(Sender: TObject);
begin
{    ppdbbcConvenio2.AutoSizeFont := true;
    ppdbbcConvenio2.PrintHumanReadable := true;
    ppdbbcConvenio2.AutoSize := false;
    ppdbbcConvenio2.WideBarRatio := 2;
    ppdbbcConvenio2.Top := 35;
    ppdbbcConvenio2.Width := 73.29;
    ppdbbcConvenio2.Height := 16.30;}
end;

procedure TfrmReporteFactura.ppLblCopiaFielPrint(Sender: TObject);
var
    Conf: TRBConfig;
begin
    Conf := rbiFactura.GetConfig;
    if (Trim(Conf.DeviceType) = 'PDFFile') or FIsPDF then  begin
        ppLblCopiaFiel.Top := ppdbtDir1.Top + ppLblCopiaFiel.Height ;
    end;
end;

procedure TfrmReporteFactura.pplblAnuladaPrint(Sender: TObject);
var
    Conf: TRBConfig;
begin
    Conf := rbiFactura.GetConfig;
    if (Trim(Conf.DeviceType) = 'PDFFile') or FIsPDF  then
        pplblAnulada.Top := ppdbtDir1.Top + pplblAnulada.Height;
end;

function TfrmReporteFactura.EjecutarDetallePDF(var Resultado,
  Error: String): Boolean;
begin
    //Ejecuto el report...
    try
        FIsPDF := True;
    		Result := reporteAPDF(rptReporteConsumo, Resultado, Error);
	except
        on e:Exception do begin
            Error := 'EjecutarDetallePDF: ' + e.Message;
            Result := False;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerConsumosComprobante
Author : ggomez
Date Created : 25/11/2005
Description : Obtiene los registros del Consumo por Cuentas de la Nota de Cobro.
Parameters : TipoCompro: AnsiString; NumeroCompro: Int64
Return Value : None

Revision 1:
    Author : ggomez
    Date : 21/03/2006
    Description : Agregu� el seteo de los par�metros TipoComprobante y
        NumeroComprobante en el llamado al SP ImprimirNK_CargarTransitosDeNotaCobro.

Revision 2:
    Author : ggomez
    Date : 27/04/2006
    Description : Sub� el timeout del sp a 500.

*******************************************************************************}
procedure TfrmReporteFactura.ObtenerConsumosComprobante(TipoCompro: AnsiString;
  NumeroCompro: Int64);
{var
    sp: TADOStoredProc;
  	NumCorrCADesde: Int64;
    NumCorrCAHasta: Int64; }     //revision 7
begin
    spObtenerConsumos.Close;
 {
    sp := TADOStoredProc.Create(nil);
    try
        sp.Connection       := spObtenerConsumos.Connection;
        // Colocar 500 segundos, pues hay convenios para los que puede demorar algo m�s que 30 seg.
        sp.CommandTimeout   := 500;

        // Cargar la Tabla con Los NumCorrCA de los tr�nsitos de la NK.
        sp.ProcedureName := 'ImprimirNK_CargarNumCorrCADeNotaCobro';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
        sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
        sp.ExecProc;
        sp.Close;

        // Cargar la Tabla con el resto de los datos de tr�nsitos de la NK.
        sp.ProcedureName := 'ImprimirNK_CargarTransitosDeNotaCobro';
        sp.Parameters.Refresh;

        // Inicializar variables
        NumCorrCADesde  := -1;
        NumCorrCAHasta  := 0;
        // Mientras haya tr�nsitos para cargar.
        //Rev 6 elimina el loop
//        while (NumCorrCADesde <> NumCorrCAHasta) do begin
            NumCorrCADesde := NumCorrCaHasta + 1;

            sp.Parameters.ParamByName('@TipoComprobante').Value     := TipoCompro;
            sp.Parameters.ParamByName('@NumeroComprobante').Value   := NumeroCompro;
            sp.Parameters.ParamByName('@NumCorrCADesde').Value  := NumCorrCADesde;
            sp.Parameters.ParamByName('@NumCorrCAHasta').Value  := 0;
            sp.ExecProc;

            // Obtener el �ltimo n�mero de tr�nsito procesado
//            NumCorrCAHasta := sp.Parameters.ParamByName('@NumCorrCAHasta').Value;

            sp.Close;
//        end; // while
    finally
        sp.Close;
        sp.Free;
    end; // finally  }     //revision 7

    // Obtener los datos de los consumos.
    //spObtenerConsumos.Parameters.Refresh;
    spObtenerConsumos.Parameters.ParamByName('@TipoComprobante').Value      := TipoCompro;
    spObtenerConsumos.Parameters.ParamByName('@NumeroComprobante').Value    := NumeroCompro;
    spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value              := 0;
	spObtenerConsumos.Parameters.ParamByName('@TotalN').Value               := 0;
	spObtenerConsumos.Parameters.ParamByName('@TotalP').Value               := 0;
	spObtenerConsumos.Parameters.ParamByName('@TotalC').Value               := 0;
	spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value           := 0;
    spObtenerConsumos.Open;
end;

end.
