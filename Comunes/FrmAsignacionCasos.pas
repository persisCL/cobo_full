unit FrmAsignacionCasos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DB, ADODB, Util,
  DMConnection,                //Coneccion a base de datos OP_CAC
  UtilProc,                    //Mensajes
  UtilDB,                      //Rutinas para base de datos
  OrdenesServicio;           //Rutinas para Ordenes de Servicio;

type
  TFrAsignacionCasos = class(TForm)
    EEntidad: TVariantComboBox;
    lbl_Area: TLabel;
    lblUsuarios: TLabel;
    cbUsuarios: TVariantComboBox;
    chkBloquea: TCheckBox;
    btnAceptar: TButton;
    btnSalir: TButton;
    spObtenerAreasAtencionDeCasosTipoOrdenServicios: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure EEntidadSelect(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    FLista: string;
    Function CargarEntidades:boolean;
  public
    procedure Inicializar(Lista: String);
  end;

var
  FrAsignacionCasos: TFrAsignacionCasos;

implementation

{$R *.dfm}

procedure TFrAsignacionCasos.Inicializar(Lista: String);
var
  List: TStrings;
begin
  FLista:= Lista;
  cbUsuarios.Enabled:= False;
  chkBloquea.Checked:= False;
  cbUsuarios.Value:= 0;
  cbUsuarios.ItemIndex:= 0;
  EEntidad.ItemIndex:=0;
  CargarEntidades;
  CargarUsuarios(cbUsuarios, 0);
  ShowModal;
end;

procedure TFrAsignacionCasos.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_USUARIO         = 'Debe indicar el Usuario';
    MSG_ERROR_AREA            = 'Debe indicar el Area';
    STR_SALIR                 = 'Atenci�n';
    MSG_ERROR_UNLOCK          = 'Error al actualizar �rdenes de servicio';
var SQLStr, MSG_UPDATE_SERVICE_ORDERS: string; Usuario_, Entidad_, Bloquea_: Variant;
begin
    MSG_UPDATE_SERVICE_ORDERS:= 'Ud. est� asignando las siguientes �rdenes de servicio '+
                                '('+FLista+') al usuario '+Trim(cbUsuarios.value)+' '+
                                '�Desea continuar?';
    Usuario_:= Trim(cbUsuarios.value);
    Entidad_:= Trim(eEntidad.value);
    if chkBloquea.Checked then Bloquea_:= ''''+Usuario_+'''' else Bloquea_:= 'NULL';
    if Usuario_ = '0' then begin
        MsgBoxBalloon(MSG_ERROR_USUARIO, 'Error', MB_ICONSTOP, cbUsuarios);
        cbUsuarios.SetFocus;
        Exit;
    end;
    if Entidad_ = '0' then begin
        MsgBoxBalloon(MSG_ERROR_USUARIO, 'Error', MB_ICONSTOP, cbUsuarios);
        cbUsuarios.SetFocus;
        Exit;
    end;
    try
        case MsgBox(MSG_UPDATE_SERVICE_ORDERS, STR_SALIR, MB_ICONWARNING
          or MB_YESNOCANCEL or MB_DEFBUTTON1) of
            ID_YES:
                begin
                   SQLStr:= 'UPDATE OrdenesServicio SET Responsable = '''+Usuario_+''', '+
                      'CodigoAreaAtencionDeCaso = '+Entidad_+', '+
                      'Usuario = '+Bloquea_+' '+
                      'WHERE CodigoOrdenServicio in ('+FLista+')';
                    QueryExecute(DMConnections.BaseCAC, SQLStr);
                end;
            ID_NO:
                Exit;
            else
                Exit;
        end;

    except
        on e: exception do begin
            MsgBox(e.message, MSG_ERROR_UNLOCK, MB_ICONSTOP);
        end;
    end;
    Close;
end;

procedure TFrAsignacionCasos.btnSalirClick(Sender: TObject);
begin
  Close;
end;

Function TFrAsignacionCasos.CargarEntidades:boolean;
const
    CONST_NINGUNO = '(Seleccionar)';
var TipoOrdenServicios: Integer;
begin
    result:=false;
    eentidad.Items.Clear;
    eentidad.Items.Add(CONST_NINGUNO, 0);

    spObtenerAreasAtencionDeCasosTipoOrdenServicios.Parameters.Refresh;
    spObtenerAreasAtencionDeCasosTipoOrdenServicios.Parameters.ParamByName('@Tipo').value:= 0;
    spObtenerAreasAtencionDeCasosTipoOrdenServicios.Parameters.ParamByName('@Area').value:= -1;
    try
        spObtenerAreasAtencionDeCasosTipoOrdenServicios.Open;
        while not spObtenerAreasAtencionDeCasosTipoOrdenServicios.Eof do begin
            EEntidad.Items.Add(spObtenerAreasAtencionDeCasosTipoOrdenServicios.fieldbyname('NombreAreaAtencionDeCaso').asstring,
                               spObtenerAreasAtencionDeCasosTipoOrdenServicios.fieldbyname('CodigoAreaAtencionDeCaso').asinteger);
            spObtenerAreasAtencionDeCasosTipoOrdenServicios.Next;
        end;
        eentidad.ItemIndex:=0;
        spObtenerAreasAtencionDeCasosTipoOrdenServicios.Close;
        result:=true;
    except
    end;
end;

procedure TFrAsignacionCasos.EEntidadSelect(Sender: TObject);
begin
  cbUsuarios.Enabled:= (EEntidad.Value > 0);
  CargarUsuarios(cbUsuarios, eEntidad.value);
end;

end.
