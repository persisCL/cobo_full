object FormRecibirTagsNominados: TFormRecibirTagsNominados
  Left = 272
  Top = 75
  BorderStyle = bsDialog
  Caption = 'Recibir Telev'#237'as'
  ClientHeight = 535
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object vleIngresados: TValueListEditor
    Left = 54
    Top = 354
    Width = 34
    Height = 40
    TabStop = False
    TabOrder = 5
    Visible = False
    ColWidths = (
      103
      -75)
  end
  object vleRecibidos: TValueListEditor
    Left = 12
    Top = 354
    Width = 37
    Height = 40
    TabStop = False
    TabOrder = 4
    Visible = False
    ColWidths = (
      53
      -22)
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 454
    Height = 64
    Align = alTop
    TabOrder = 0
    object lblGuiaDespacho: TLabel
      Left = 6
      Top = 38
      Width = 94
      Height = 13
      Caption = '&Gu'#237'a de Despacho:'
      FocusControl = cbGuiaDespacho
    end
    object lblTransportista: TLabel
      Left = 6
      Top = 10
      Width = 64
      Height = 13
      Caption = '&Transportista:'
    end
    object lblAlmacenDestino: TLabel
      Left = 254
      Top = 8
      Width = 83
      Height = 13
      Caption = 'Almac'#233'n Destino:'
    end
    object cbTransportistas: TVariantComboBox
      Left = 103
      Top = 6
      Width = 133
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTransportistasChange
      Items = <>
    end
    object cbGuiaDespacho: TComboBox
      Left = 103
      Top = 34
      Width = 133
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbGuiaDespachoChange
    end
    object txtAlmacenDestino: TEdit
      Left = 252
      Top = 23
      Width = 190
      Height = 21
      Cursor = crArrow
      ReadOnly = True
      TabOrder = 2
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 489
    Width = 454
    Height = 46
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      454
      46)
    object btnAceptar: TButton
      Left = 291
      Top = 12
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 371
      Top = 11
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
    end
    object btnObservacion: TButton
      Left = 165
      Top = 11
      Width = 94
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Observaci'#243'n'
      TabOrder = 2
      OnClick = btnObservacionClick
    end
    object btnBorrarLinea: TButton
      Left = 86
      Top = 11
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Borrar l'#237'nea'
      TabOrder = 3
      OnClick = btnBorrarLineaClick
    end
    object btn_Importar: TButton
      Left = 6
      Top = 10
      Width = 75
      Height = 25
      Caption = '&Importar'
      TabOrder = 4
      Visible = False
      OnClick = btn_ImportarClick
    end
  end
  object dbgRecibirTags: TDBGrid
    Left = 0
    Top = 64
    Width = 454
    Height = 159
    Align = alTop
    DataSource = dsRecibirTags
    Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnExit = dbgRecibirTagsExit
    OnKeyDown = dbgRecibirTagsKeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'Descripcion'
        ReadOnly = True
        Title.Caption = 'Categor'#237'a'
        Width = 228
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CantidadTags'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Telev'#237'as Enviados'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CantidadTagsRecibidos'
        Title.Caption = 'Telev'#237'as Recibidos'
        Width = 100
        Visible = True
      end>
  end
  object dbgRecibidos: TDBGrid
    Left = 0
    Top = 223
    Width = 454
    Height = 266
    Align = alClient
    DataSource = dsTAGs
    Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NumeroInicialTAG'
        Title.Alignment = taCenter
        Title.Caption = 'Telev'#237'a Inicial'
        Width = 120
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NumeroFinalTAG'
        Title.Alignment = taCenter
        Title.Caption = 'Telev'#237'a Final'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CantidadTAGs'
        Title.Alignment = taRightJustify
        Title.Caption = 'Cantidad'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CategoriaTAGs'
        Title.Alignment = taRightJustify
        Title.Caption = 'Categor'#237'a'
        Width = 90
        Visible = True
      end>
  end
  object dsRecibirTags: TDataSource
    DataSet = cdsRecibirTags
    Left = 96
    Top = 153
  end
  object ActualizarStockTagsRecibidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarStockTagsRecibidos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 422
    Top = 458
  end
  object ObtenerDatosGuiaDespacho: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosGuiaDespacho'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 186
    Top = 153
  end
  object ActualizarEstadoGuiaDespacho: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadoGuiaDespacho'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Observacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@FechaAceptacionRechazo'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraRecepcion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraEnvio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 216
    Top = 153
  end
  object ObtenerGuiaDespachoCategorias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerGuiaDespachoCategorias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 156
    Top = 153
  end
  object cdsTAGs: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroInicialTAG'
        DataType = ftLargeint
      end
      item
        Name = 'NumeroFinalTAG'
        DataType = ftLargeint
      end
      item
        Name = 'CantidadTAGs'
        DataType = ftLargeint
      end
      item
        Name = 'CategoriaTAGs'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    BeforePost = cdsTAGsBeforePost
    AfterScroll = cdsTAGsAfterScroll
    OnNewRecord = cdsTAGsNewRecord
    Left = 126
    Top = 351
  end
  object dsTAGs: TDataSource
    DataSet = cdsTAGs
    Left = 96
    Top = 351
  end
  object spVerificarTAGEnArchivos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarTAGEnArchivos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 288
    Top = 105
  end
  object spObtenerDatosRangoTAGsTransporte: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosRangoTAGsTransporte'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TAGInicial'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TAGFinal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 318
    Top = 105
  end
  object PasarAMaestroTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PasarAMaestroTAGs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmbalaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroInicialTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroFinalTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenTAG'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EstadoSituacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@EstadoControl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ActualizarStock'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 357
    Top = 457
  end
  object RegistrarMovimientoStockNominado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarMovimientoStockNominado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroInicialTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroFinalTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@GuiaDespacho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraEvento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MotivoEntrega'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@ConstanciaCarabinero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaHoraConstancia'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DescripcionMotivoRegistro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ActualizarStock'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 389
    Top = 457
  end
  object spObtenerDatosPuntoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPuntoEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NombreAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@SoloNominado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 258
    Top = 105
  end
  object spRegistrarMovimientoStock: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'RegistrarMovimientoStock'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoMovimiento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEmbalaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroInicialTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroFinalTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EstadoSituacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@EstadoControl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@GuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraEvento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MotivoEntrega'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@ConstanciaCarabinero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaHoraConstancia'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DescripcionMotivoRegistro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 190
    Top = 347
  end
  object ObtenerDatosAlmacen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosAlmacen'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 352
    Top = 106
  end
  object cdsRecibirTags: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Categoria'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CantidadTags'
        DataType = ftInteger
      end
      item
        Name = 'CantidadTagsRecibidos'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterInsert = cdsRecibirTagsAfterInsert
    Left = 126
    Top = 153
  end
  object Qry_Imporar: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM Tabla_Import_TAG  WITH (NOLOCK) ')
    Left = 158
    Top = 348
  end
end
