{-------------------------------------------------------------------------------


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

--------------------------------------------------------------------------------}
unit FrmRepoLiquidacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, ppCtrls, ppDB, ppDBPipe, ppBands, ppStrtch, ppRegion,
  ppClass, ppVar, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  UtilRB, DMConnection, StdCtrls, ppSubRpt, Utildb, Util, utilproc,
  ppMemo, FrmConfigReporteLiquidacion, ppModule, raCodMod, ppParameter;

type
  TFormReporteLiquidacion = class(TForm)
    rbi_Liquidacion: TRBInterface;
    ppDetalleCobrosEfectivo: TppDBPipeline;
    dsLiquidacionEfectivo: TDataSource;
    spObtenerLiquidacionTurnoEfectivo: TADOStoredProc;
    rp_Liquidacion: TppReport;
    ppParameterList1: TppParameterList;
    dsDetallesTurno: TDataSource;
    ppDBPipelineDetalleTurno: TppDBPipeline;
    ppHeaderBand3: TppHeaderBand;
    ppImage1: TppImage;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLine1: TppLine;
    ppLblUsuario: TppLabel;
    ppLabel33: TppLabel;
    pplblTurno: TppLabel;
    ppLabel34: TppLabel;
    pplblFecha: TppLabel;
    ppLine2: TppLine;
    ppLabel37: TppLabel;
    ppDBText18: TppDBText;
    ppLabel38: TppLabel;
    ppDBText19: TppDBText;
    ppLabel42: TppLabel;
    ppDBText22: TppDBText;
    ppLabel43: TppLabel;
    ppDBText23: TppDBText;
    ppDetailBand3: TppDetailBand;
    ppRegionEfectivo: TppRegion;
    ppSubReportCupones: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppShape3: TppShape;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel13: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText15: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppShape1: TppShape;
    ppLabel9: TppLabel;
    ppSumaEfectivo: TppDBCalc;
    raCodeModule2: TraCodeModule;
    ppRegionTarjeta: TppRegion;
    ppFooterBand3: TppFooterBand;
    ppLine3: TppLine;
    ppLabel4: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel12: TppLabel;
    raCodeModule1: TraCodeModule;
    ppLabel14: TppLabel;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppDBText6: TppDBText;
    spObtenerDetalleTurno: TADOStoredProc;
    ppDBText7: TppDBText;
    ppReport1: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLabel1: TppLabel;
    ppLabel17: TppLabel;
    ppLine6: TppLine;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLine7: TppLine;
    ppLabel23: TppLabel;
    ppDBText8: TppDBText;
    ppLabel24: TppLabel;
    ppDBText9: TppDBText;
    ppLabel25: TppLabel;
    ppDBText10: TppDBText;
    ppLabel26: TppLabel;
    ppDBText11: TppDBText;
    ppDetailBand2: TppDetailBand;
    ppRegion1: TppRegion;
    ppSubReport1: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppShape2: TppShape;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppDetailBand4: TppDetailBand;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppDBText16: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppShape4: TppShape;
    ppLabel32: TppLabel;
    ppDBCalc1: TppDBCalc;
    raCodeModule3: TraCodeModule;
    ppRegion2: TppRegion;
    ppLabel35: TppLabel;
    ppDBText17: TppDBText;
    ppDBText20: TppDBText;
    ppLabel36: TppLabel;
    ppLabel39: TppLabel;
    ppDBText21: TppDBText;
    ppLabel40: TppLabel;
    ppDBText24: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppLine8: TppLine;
    ppLabel41: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel44: TppLabel;
    ppLabel45: TppLabel;
    ppLine9: TppLine;
    ppLine10: TppLine;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    raCodeModule4: TraCodeModule;
    ppParameterList2: TppParameterList;
    procedure rbi_LiquidacionSelect(Sender: TObject);
	procedure rp_LiquidacionBeforePrint(Sender: TObject);
    procedure ppDBText2GetText(Sender: TObject; var Text: String);
  private
	{ Private declarations }
	FNumeroTurno: integer;
	rptNumeroTurno: integer;
	rptCodigoUsuario: AnsiString;
  public
	{ Public declarations }
	function Inicializa: Boolean;
	function Ejecutar:Boolean;
	property NumeroTurno: integer write FNumeroTurno;
  end;

var
  FormReporteLiquidacion: TFormReporteLiquidacion;

implementation

{$R *.dfm}
resourcestring
    CAPTION_REPORTE = 'Reporte de Liquidación';

function TFormReporteLiquidacion.Ejecutar: Boolean;
resourcestring
    MSG_EJECUTAR_REPORTE = 'No se pudo ejecutar el reporte de Liquidación.';
begin
	Result := False;
    try
        try
            Result := rbi_Liquidacion.Execute(True);
        except
            On E: Exception do begin
                MsgBoxErr(MSG_EJECUTAR_REPORTE, e.message, CAPTION_REPORTE, MB_ICONSTOP);
            end;
        end;
    finally
    spObtenerLiquidacionTurnoEfectivo.Close;
    spObtenerDetalleTurno.Close;
    end;
end;

function TFormReporteLiquidacion.Inicializa: Boolean;
var
    RutaLogo,sDireccion,sLocalidad: AnsiString;                                        //SS_1147_NDR_20140710
begin
  ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
  try                                                                                                 //SS_1147_NDR_20140710
    ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
  except                                                                                              //SS_1147_NDR_20140710
    On E: Exception do begin                                                                          //SS_1147_NDR_20140710
      Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
      MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
      Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
    end;                                                                                              //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

  with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
  begin                                                                                               //SS_1147_NDR_20140710
    Close;                                                                                            //SS_1147_NDR_20140710
    Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
    Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
    ExecProc;                                                                                         //SS_1147_NDR_20140710
    sDireccion    := FieldByName('DireccionConcesionaria').AsString;                                  //SS_1147_NDR_20140710
    sLocalidad    := FieldByName('Comuna').AsString + '  ' +                                          //SS_1147_NDR_20140710
                     FieldByName('Region').AsString+ '  ' +                                           //SS_1147_NDR_20140710
                     FieldByName('Pais').AsString;                                                    //SS_1147_NDR_20140710
                                                                                                      //SS_1147_NDR_20140710
    ppLabel1.Caption := sDireccion + ' ' + sLocalidad;                                                //SS_1147_NDR_20140710
    ppLabel2.Caption := sDireccion + ' ' + sLocalidad;                                                //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710


	rptNumeroTurno   := FNumeroTurno;
	rptCodigoUsuario := UsuarioSistema;
	Result := True;
end;

procedure TFormReporteLiquidacion.rbi_LiquidacionSelect(Sender: TObject);
Var
	f: TFormConfigReporteLiquidacion;
begin
	Application.CreateForm(TFormConfigReporteLiquidacion, f);
	if f.Inicializa(False) then begin
		f.CodigoUsuario := rptCodigousuario;
		f.NumeroTurno   := rptNumeroTurno;
		if f.ShowModal = mrOk then begin
			rptNumeroTurno   := f.NumeroTurno;
			rptCodigoUsuario := f.CodigoUsuario;
		end;
	end;
	f.Release;
end;

procedure TFormReporteLiquidacion.rp_LiquidacionBeforePrint(Sender: TObject);
resourcestring
    MSG_APERTURA = 'Apertura: ';
    MSG_ABIERTO  = 'Abierto';
    MSG_CIERRE   = 'Cierre: ';
    MSG_SELECCIONAR_TURNO = 'Debe seleccionar un turno.';
begin
	if rptNumeroTurno > 0 then begin
       	spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName('@NumeroTurno').Value	:= rptNumeroTurno;
       	spObtenerDetalleTurno.Parameters.ParamByName('@NumeroTurno').Value	:= rptNumeroTurno;
		spObtenerLiquidacionTurnoEfectivo.Open;
        spObtenerDetalleTurno.Open;
        pplblTurno.Caption	:= Trim(Istr(rptNumeroTurno));
        with spObtenerDetalleTurno do
			pplblFecha.Caption:= PadR(MSG_APERTURA +
		  		FormatDateTime('dd/mm/yyyy, hh:mm', FieldByName('FechaHoraApertura').AsDateTime), 40, ' ') +
		  		PadR( iif(FieldByName('FechaHoraCierre').AsDateTime <= 0,
		  		'Turno: ' + MSG_ABIERTO, MSG_CIERRE + FormatDateTime('dd/mm/yyyy, hh:mm',
		  		FieldByName('FechaHoraCierre').AsDateTime)), 40, ' ');
    end else begin
    	MsgBox(MSG_SELECCIONAR_TURNO, CAPTION_REPORTE, MB_ICONSTOP);
    end;
end;

procedure TFormReporteLiquidacion.ppDBText2GetText(Sender: TObject;
  var Text: String);
begin
    if (Trim(Text) = EmptyStr) then
        Text := '0';
end;

end.
