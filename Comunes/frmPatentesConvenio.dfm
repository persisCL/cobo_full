object FormPatentesConvenio: TFormPatentesConvenio
  Left = 0
  Top = 0
  Caption = 'Patentes Convenio'
  ClientHeight = 271
  ClientWidth = 260
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object chklstPatentesConvenio: TCheckListBox
    Left = 0
    Top = 0
    Width = 260
    Height = 230
    Align = alClient
    ItemHeight = 13
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 230
    Width = 260
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      260
      41)
    object btnAplicarDescuentos: TBitBtn
      Left = 106
      Top = 6
      Width = 70
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      ModalResult = 1
      TabOrder = 0
      OnClick = btnAplicarDescuentosClick
      NumGlyphs = 2
      ExplicitLeft = 159
    end
    object btnCancelar: TBitBtn
      Left = 182
      Top = 6
      Width = 70
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      NumGlyphs = 2
      ExplicitLeft = 237
    end
  end
  object spObtenerPatentesConvenioDescuento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPatentesConvenioDescuento'
    Parameters = <>
    Left = 96
    Top = 48
  end
end
