object Frm_ConfigReporteComunicaciones: TFrm_ConfigReporteComunicaciones
  Left = 278
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Selecci'#243'n de Reporte'
  ClientHeight = 220
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 36
    Top = 150
    Width = 39
    Height = 13
    Caption = 'Usuario:'
  end
  object Bevel1: TBevel
    Left = 4
    Top = 8
    Width = 416
    Height = 173
  end
  object btn_Aceptar: TOPButton
    Left = 267
    Top = 189
    Caption = '&Aceptar'
    Default = True
    TabOrder = 3
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TOPButton
    Left = 344
    Top = 189
    Cancel = True
    Caption = '&Salir'
    TabOrder = 4
    OnClick = btn_CancelarClick
  end
  object cb_Usuarios: TComboBox
    Left = 93
    Top = 146
    Width = 280
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object gb_fechas: TGroupBox
    Left = 23
    Top = 17
    Width = 377
    Height = 56
    TabOrder = 0
    object Label7: TLabel
      Left = 12
      Top = 28
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object cb_Fechas: TComboBox
      Left = 68
      Top = 24
      Width = 280
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object rd_FechasPredefinidas: TRadioButton
      Left = 12
      Top = -2
      Width = 129
      Height = 17
      Caption = ' Fechas Predefinidas '
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = RangoFechas
    end
  end
  object gb_rango: TGroupBox
    Left = 23
    Top = 78
    Width = 377
    Height = 58
    TabOrder = 1
    object Label2: TLabel
      Left = 12
      Top = 26
      Width = 65
      Height = 13
      Caption = 'Fecha desde:'
    end
    object Label3: TLabel
      Left = 198
      Top = 26
      Width = 29
      Height = 13
      Caption = 'hasta:'
    end
    object rd_Rango: TRadioButton
      Left = 12
      Top = -1
      Width = 80
      Height = 17
      Caption = ' Por Rango '
      TabOrder = 0
      OnClick = RangoFechas
    end
    object dt_FechaDesde: TDateEdit
      Left = 89
      Top = 22
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594
    end
    object dt_FechaHasta: TDateEdit
      Left = 253
      Top = 22
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 2
      Date = -693594
    end
  end
  object qry_Usuarios: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'select * from UsuariosSistemas')
    Left = 36
    Top = 172
  end
end
