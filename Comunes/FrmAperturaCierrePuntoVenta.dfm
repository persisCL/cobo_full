object FormAperturaCierrePuntoVenta: TFormAperturaCierrePuntoVenta
  Left = 278
  Top = 176
  BorderStyle = bsDialog
  Caption = 'Apertura'
  ClientHeight = 517
  ClientWidth = 614
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 471
    Width = 614
    Height = 46
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      614
      46)
    object btnAceptar: TButton
      Left = 450
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 531
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object pcTurnos: TPageControl
    Left = 0
    Top = 0
    Width = 614
    Height = 471
    ActivePage = tsTelevias
    Align = alClient
    TabOrder = 0
    OnChange = pcTurnosChange
    object tsTelevias: TTabSheet
      Caption = 'Telev'#237'as'
      DesignSize = (
        606
        443)
      object lblValeTelevias: TLabel
        Left = 9
        Top = 16
        Width = 79
        Height = 13
        Caption = '&N'#250'mero de Vale:'
        FocusControl = txtValeTelevias
      end
      object dbgAperturaCierre: TDBGrid
        Left = 0
        Top = 46
        Width = 606
        Height = 397
        Anchors = [akLeft, akTop, akRight, akBottom]
        Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        PopupMenu = PopDefecto
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = dbgAperturaCierreKeyDown
        OnKeyPress = dbgAperturaCierreKeyPress
        Columns = <
          item
            Expanded = False
            FieldName = 'CategoriaInterurbana'
            ReadOnly = True
            Title.Caption = 'Categor'#237'a Interurbana'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CategoriaUrbana'
            ReadOnly = True
            Title.Caption = 'Categor'#237'a Urbana'
            Width = 240
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'Cantidad'
            Title.Alignment = taCenter
            Title.Caption = 'Cantidad Telev'#237'as'
            Width = 100
            Visible = True
          end>
      end
      object txtValeTelevias: TEdit
        Left = 101
        Top = 12
        Width = 132
        Height = 21
        Hint = 
          'Ingrese el n'#250'mero de vale correspondiente a los telev'#237'as entrega' +
          'dos (opcional)'
        MaxLength = 20
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object tsEfectivo: TTabSheet
      Caption = 'Efectivo'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Bevel1: TBevel
        Left = 8
        Top = 8
        Width = 569
        Height = 337
      end
      object Label2: TLabel
        Left = 142
        Top = 183
        Width = 33
        Height = 13
        Caption = 'Monto:'
      end
      object lblMensaje: TLabel
        Left = 142
        Top = 100
        Width = 281
        Height = 13
        Caption = 'Ingrese el importe con el que efect'#250'a la apertura de la Caja.'
      end
      object Label1: TLabel
        Left = 142
        Top = 155
        Width = 79
        Height = 13
        Caption = '&N'#250'mero de Vale:'
        FocusControl = txtValeEfectivo
      end
      object lblDisponeTelevias: TLabel
        Left = 16
        Top = 328
        Width = 275
        Height = 13
        Caption = 'Si usted dispone de telev'#237'as para abrir un turno haga click'
      end
      object lblAca: TLabel
        Left = 297
        Top = 328
        Width = 18
        Height = 13
        Cursor = crHandPoint
        Caption = 'ac'#225
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblAcaClick
      end
      object txt_Monto: TNumericEdit
        Left = 236
        Top = 179
        Width = 132
        Height = 21
        MaxLength = 8
        PopupMenu = PopDefecto
        TabOrder = 1
        OnKeyPress = txt_MontoKeyPress
      end
      object txtValeEfectivo: TEdit
        Left = 236
        Top = 152
        Width = 132
        Height = 21
        Hint = 
          'Ingrese el n'#250'mero de vale correspondiente al efectivo entregado ' +
          '(opcional).'
        TabOrder = 0
      end
    end
    object tsLiquidacionEfectivo: TTabSheet
      Caption = 'Liquidaci'#243'n Efectivo'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        606
        443)
      object Label3: TLabel
        Left = 26
        Top = 320
        Width = 147
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Cupones de Tarjeta de Cr'#233'dito:'
        ExplicitTop = 287
      end
      object Label4: TLabel
        Left = 26
        Top = 344
        Width = 145
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Cupones de Tarjeta de D'#233'bito:'
        ExplicitTop = 311
      end
      object Label5: TLabel
        Left = 26
        Top = 297
        Width = 45
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Cheques:'
        ExplicitTop = 264
      end
      object Label6: TLabel
        Left = 26
        Top = 367
        Width = 55
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Vales Vista:'
        ExplicitTop = 334
      end
      object lbl_Cantidad: TLabel
        Left = 360
        Top = 275
        Width = 51
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Cantidad'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_Importe: TLabel
        Left = 491
        Top = 275
        Width = 43
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Importe'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitTop = 242
      end
      object Label7: TLabel
        Left = 26
        Top = 391
        Width = 42
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Pagar'#233's:'
        ExplicitTop = 358
      end
      object Label8: TLabel
        Left = 26
        Top = 415
        Width = 98
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Dep'#243'sito Anticipado:'
        ExplicitTop = 382
      end
      object edCuponesCredito: TEdit
        Left = 331
        Top = 317
        Width = 82
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 4
        PopupMenu = PopDefecto
        TabOrder = 3
        OnKeyPress = edChequesKeyPress
      end
      object edCheques: TEdit
        Left = 331
        Top = 294
        Width = 82
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 4
        PopupMenu = PopDefecto
        TabOrder = 1
        OnKeyPress = edChequesKeyPress
      end
      object edCuponesDebito: TEdit
        Left = 331
        Top = 341
        Width = 82
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 4
        PopupMenu = PopDefecto
        TabOrder = 5
        OnKeyPress = edChequesKeyPress
      end
      object GrillaBilletes: TStringGrid
        Left = 7
        Top = 6
        Width = 592
        Height = 259
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 3
        DefaultColWidth = 150
        DefaultRowHeight = 18
        FixedColor = 14732467
        FixedCols = 0
        RowCount = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
        ParentFont = False
        PopupMenu = PopDefecto
        ScrollBars = ssVertical
        TabOrder = 0
        OnKeyPress = GrillaBilletesKeyPress
        OnSelectCell = GrillaBilletesSelectCell
        ColWidths = (
          340
          87
          95)
      end
      object edValesVista: TEdit
        Left = 331
        Top = 364
        Width = 82
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 4
        PopupMenu = PopDefecto
        TabOrder = 7
        OnKeyPress = edChequesKeyPress
      end
      object ne_ImporteCheques: TNumericEdit
        Left = 415
        Top = 294
        Width = 121
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 10
        PopupMenu = PopDefecto
        TabOrder = 2
        OnKeyPress = edChequesKeyPress
      end
      object ne_importeTarjetasCredito: TNumericEdit
        Left = 415
        Top = 317
        Width = 121
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 10
        PopupMenu = PopDefecto
        TabOrder = 4
        OnKeyPress = edChequesKeyPress
      end
      object ne_ImporteTarjetasDebito: TNumericEdit
        Left = 415
        Top = 341
        Width = 121
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 10
        PopupMenu = PopDefecto
        TabOrder = 6
        OnKeyPress = edChequesKeyPress
      end
      object ne_ImporteValesVista: TNumericEdit
        Left = 415
        Top = 364
        Width = 121
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 10
        PopupMenu = PopDefecto
        TabOrder = 8
        OnKeyPress = edChequesKeyPress
      end
      object edPagares: TEdit
        Left = 331
        Top = 388
        Width = 82
        Height = 21
        Anchors = [akLeft, akBottom]
        PopupMenu = PopDefecto
        TabOrder = 9
        OnKeyPress = edChequesKeyPress
      end
      object ne_ImportePagares: TNumericEdit
        Left = 415
        Top = 388
        Width = 121
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 10
        PopupMenu = PopDefecto
        TabOrder = 10
        OnKeyPress = edChequesKeyPress
      end
      object edDepositoAnticipado: TEdit
        Left = 331
        Top = 412
        Width = 82
        Height = 21
        Anchors = [akLeft, akBottom]
        PopupMenu = PopDefecto
        TabOrder = 11
        OnKeyPress = edChequesKeyPress
      end
      object ne_ImporteDepositoAnticipado: TNumericEdit
        Left = 415
        Top = 412
        Width = 121
        Height = 21
        Anchors = [akLeft, akBottom]
        MaxLength = 10
        PopupMenu = PopDefecto
        TabOrder = 12
        OnKeyPress = edChequesKeyPress
      end
    end
  end
  object ActualizarTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@UsuarioCierre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 289
    Top = 472
  end
  object spVerificarPuntoEntregaVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarPuntoEntregaVenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Valido'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 157
    Top = 472
  end
  object spCerrarTurnoLiquidacionCaja: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CerrarTurnoLiquidacionCaja;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 69
    Top = 472
  end
  object spObtenerLiquidacionTurnoEfectivo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerLiquidacionTurnoEfectivo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 126
    Top = 472
  end
  object spAgregarLiquidacionEfectivo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarLiquidacionEfectivo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDenominacionMoneda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ValorMonedaLocal'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 23
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 43
    Top = 472
  end
  object spAgregarLiquidacionCupones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarLiquidacionCupones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadCheques'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadCuponesTarjetaDebito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadCuponesTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadValesVista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteCheques'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ImporteTarjetasDebito'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ImporteTarjetasCredito'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ImporteValesVista'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@ImporteEfectivo'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadPagares'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImportePagares'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadDepositosAnticipados'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteDepositosAnticipados'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 11
    Top = 472
  end
  object spActualizarTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTurno;1'
    Parameters = <>
    Left = 98
    Top = 472
  end
  object PopDefecto: TPopupMenu
    Left = 352
    Top = 471
  end
  object spObtenerCuadraturaCierre: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCuadraturaCierre'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@Detalle'
        DataType = ftBoolean
        Value = False
      end>
    Left = 384
    Top = 472
  end
end
