object FormOrdenTrabajo: TFormOrdenTrabajo
  Left = 57
  Top = 61
  Width = 898
  Height = 658
  Caption = 'Nueva '#211'rden de Trabajo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBotones: TPanel
    Left = 0
    Top = 590
    Width = 890
    Height = 41
    Align = alBottom
    TabOrder = 0
    object btnAceptar: TButton
      Left = 725
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 804
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object pnlDetalleOrdenServicio: TPanel
    Left = 0
    Top = 0
    Width = 890
    Height = 192
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 32
      Top = 22
      Width = 64
      Height = 13
      Caption = 'Tipo de Falla:'
    end
    object Label2: TLabel
      Left = 405
      Top = 19
      Width = 51
      Height = 13
      Caption = 'Ubicaci'#243'n:'
    end
    object sbMemo: TScrollBox
      Left = 36
      Top = 52
      Width = 832
      Height = 105
      TabOrder = 0
      object mmObservaciones: TMemo
        Left = 0
        Top = 0
        Width = 804
        Height = 196
        BorderStyle = bsNone
        ReadOnly = True
        TabOrder = 0
      end
    end
    object cbTiposFallas: TComboBox
      Left = 108
      Top = 15
      Width = 240
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
    object cbUbicaciones: TComboBox
      Left = 472
      Top = 14
      Width = 345
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
    end
  end
  inline FrameAltaOrdenServicio1: TFrameAltaOrdenServicio
    Left = 0
    Top = 192
    Width = 890
    Height = 398
    HorzScrollBar.Visible = False
    VertScrollBar.Visible = False
    Align = alBottom
    Anchors = [akLeft]
    AutoScroll = False
    Constraints.MinHeight = 325
    Constraints.MinWidth = 890
    TabOrder = 2
    inherited PageControl: TPageControl
      Width = 713
      Height = 398
      inherited tab_facturacion: TTabSheet
        inherited GroupBox12: TGroupBox
          Width = 694
          Height = 378
          inherited txt_fact_observaciones: TMemo
            Width = 675
            Height = 267
          end
        end
      end
      inherited tab_Otros: TTabSheet
        inherited GroupBox6: TGroupBox
          Width = 694
          Height = 378
          inherited txt_Otros_observaciones: TMemo
            Width = 675
            Height = 350
          end
        end
      end
    end
    inherited tvOrdenesServicio: TTreeView
      Height = 398
      OnClick = FrameAltaOrdenServicio1tvOrdenesServicioClick
    end
  end
end
