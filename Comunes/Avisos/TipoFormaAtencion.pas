{-----------------------------------------------------------------------------
File Name      : TipoFormaAtencion.pas
Author         : Alejandro Labra
Date Created   : 11-04-2011
Description    : Enum que contiene la definición de las formas de atención que
                 puede existir entre CN y el cliente.
-----------------------------------------------------------------------------}
unit TipoFormaAtencion;

interface

type
    TTipoFormaAtencion = (Presencial, Telefonico);

implementation

end.
