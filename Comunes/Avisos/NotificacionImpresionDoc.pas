{-----------------------------------------------------------------------------
File Name      : NotificacionImpresionDoc.pas
Author         : Alejandro Labra
Date Created   : 11-04-2011
Description    : Implementaci�n de la interface INotificacion. Esta implementaci�n
                 imprime el documento a trav�s de la impresora PDF.

Revision       : 1
Author         : Alejandro Labra
Date           : 26-04-2011
Firma          : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description    : Se arregla impresi�n de pdf, ahora se imprime solo 1 vez el
                 documento original y la copia.
Firma          : SS-960-ALA-20110423

Revision       : 2
Author         : Alejandro Labra
Date           : 27-04-2011
Firma          : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description    : Se implementa la facultad de imprimir m�ltiples plantillas en
                 una sola impresi�n.
Firma          : SS-960-ALA-20110427

Description    : Se arregla problema con la impresi�n en pdf, el cual estaba
                 generando de manera desordenada los anexos del cliente.
Firma          : SS-960-ALA-20110713
-----------------------------------------------------------------------------}
unit NotificacionImpresionDoc;

interface
uses
    Notificacion,       //Interface que define el contrato para las notificaciones
    Diccionario,        //Contiene la definici�n de una lista de TClaveValor.
    frmMuestraMensaje,  // Muestra mensajes descriptivos al usuario.
    frmMuestraPDF,      // Muestra el pdf
    Util, utilhttp, SysUtils, SysUtilsCN, UtilProc,
    Forms, Classes;

type
    TNotificacionImpresionDoc = class(TInterfacedObject, INotificacion)
        private
            _plantillas     : TStringList;                                                                          //SS-960-ALA-20110427
            _variables      : TList;
            _copiaCliente   : Boolean;
            _tagCopia       : String;
            function GenerarPDF(texto, rutaAdjunto : String) : String;                                              //SS-960-ALA-20110427
            procedure Imprimir(pathPDF : String);
        public
            function ObtenerPlantillas : TStringList;                                                               //SS-960-ALA-20110427
            procedure AsignarPlantillas(plantillas : TStringList);                                                  //SS-960-ALA-20110427
            property Plantillas : TStringList read ObtenerPlantillas write AsignarPlantillas;                       //SS-960-ALA-20110427

            function ObtenerVariables : TList;                                                                      //SS-960-ALA-20110427
            procedure AsignarVariables(variables : TList);                                                          //SS-960-ALA-20110427
            property Variables : TList read ObtenerVariables write AsignarVariables;                                //SS-960-ALA-20110427

            function ObtenerConCopiaCliente : Boolean;
            procedure AsignarConCopiaCliente(conCopia : Boolean);
            property ConCopiaCliente : Boolean read ObtenerConCopiaCliente write AsignarConCopiaCliente;

            function ObtenerTagCopia : String;
            procedure AsignarTagCopia(tagCopia : String);
            property TagCopia : String read ObtenerTagCopia write AsignarTagCopia;
            
            function Notificar : Boolean;
            Destructor Destroy; override;
    end;
implementation

{ TNotificacionImpresionDoc }
{-----------------------------------------------------------------------------
Function Name   : AsignarPlantilla
Author          : Alejandro Labra
Date Created    : 08-04-2011
Description     : Setea el valor a la propiedad Plantilla.                      //SS-960-ALA-20110427
Parameter       : plantilla, la cual contiene la ruta de la plantilla a ser     //SS-960-ALA-20110427
                  cargada.
-----------------------------------------------------------------------------}
procedure TNotificacionImpresionDoc.AsignarPlantillas(plantillas: TStringList); //SS-960-ALA-20110427
begin
    _plantillas := plantillas;                                                  //SS-960-ALA-20110427
end;

{-----------------------------------------------------------------------------
Function Name   : AsignarVariables
Author          : Alejandro Labra
Date Created    : 08-04-2011
Description     : Setea el valor a la propiedad ConCopiaCliente.
Parameter       : conCopia, boolean que indica si la impresi�n es con copia al
                  cliente o no.
-----------------------------------------------------------------------------}
procedure TNotificacionImpresionDoc.AsignarConCopiaCliente(conCopia: Boolean);
begin
    _copiaCliente := conCopia;
end;

{-----------------------------------------------------------------------------
Function Name   : AsignarVariables
Author          : Alejandro Labra
Date Created    : 08-04-2011
Description     : Setea el valor a la propiedad Variables.
Parameter       : variales, contiene la lista de objetos ClaveValor, almacenados
                  en un TDiccionario.
-----------------------------------------------------------------------------}
procedure TNotificacionImpresionDoc.AsignarVariables(variables: TList);         //SS-960-ALA-20110427
begin
    _variables := variables;
end;

{-----------------------------------------------------------------------------
Function Name   : AsignarVariables
Author          : Alejandro Labra
Date Created    : 08-04-2011
Description     : Setea el valor a la propiedad TagCopia.
Parameter       : tagCopia, string que contiene el tag a ser reemplazado en el
                  documento.
-----------------------------------------------------------------------------}
procedure TNotificacionImpresionDoc.AsignarTagCopia(tagCopia: String);
begin
    _tagCopia := tagCopia;
end;

{-----------------------------------------------------------------------------
Function Name  : Destroy
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : Sobrecarga del m�todo Destroy, en el cual se libera de memoria
                 el objeto variables.
-----------------------------------------------------------------------------}
destructor TNotificacionImpresionDoc.Destroy;
begin
    FreeAndNil(_variables);
    inherited;
end;

{-----------------------------------------------------------------------------
Function Name   : Notificar
Author          : Alejandro Labra
Date Created    : 08-04-2011
Description     : Implementaci�n del m�todo Notificar, el cual imprimir� la plantilla
                 reemplazando las variables.

Revision        : 1
Author          : Alejandro Labra
Date            : 27-04-2011
Firma           : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description     : Se implementa opci�n de imprimir un conjunto de plantillas 
                  de una sola vez.
Firma           : SS-960-ALA-20110427
-----------------------------------------------------------------------------}

function TNotificacionImpresionDoc.Notificar: Boolean;
resourcestring
    TAG_COPIA_CLIENTE               = 'Copia Cliente';
    TAG_COPIA_CONCESIONARIA         = 'Copia Costanera Norte';
    MSG_ERROR_NO_EXISTE_PLANTILLA   = 'No existe la plantilla, por favor contactese con el administrador.';
var
    i                   : Integer;
    k                   : Integer;                                                                                         //SS-960-ALA-20110427
    TextoPlantilla      : String;
    TextoPlantillaCopia : String;
    PathPDFOriginal     : String;
    PathPDFCopia        : String;
begin
    try
        TPanelMensajesForm.MuestraMensaje('Generando Documento en PDF ...', true);                                          //SS-960-ALA-20110427
        for k := _plantillas.Count - 1 downto 0 do begin                                                                    //SS-960-ALA-20110427
            if FileExists(_plantillas[k]) then begin

                TextoPlantilla := FileToString(_plantillas[k] , fmShareDenyNone);
                for i := 0 to TDiccionario(_variables.Items[k]).Count - 1 do begin                                           //SS-960-ALA-20110427
                    TextoPlantilla := ReplaceTag(TextoPlantilla, TDiccionario(_variables.Items[k]).Items[i].Clave, TDiccionario(_variables.Items[k]).Items[i].Valor);   //SS-960-ALA-20110427
                end;

                if _copiaCliente then begin
                    TextoPlantillaCopia :=  ReplaceTag(TextoPlantilla, _tagCopia, TAG_COPIA_CLIENTE);
                    TextoPlantilla := ReplaceTag(TextoPlantilla, _tagCopia, TAG_COPIA_CONCESIONARIA);

                    //Si nos encontramos en la �ltima plantilla a imprimir entonces concatenamos                                //SS-960-ALA-20110423
                    //el original con la copia                                                                                  //SS-960-ALA-20110427
                    if k = 0 then begin                                                                                         //SS-960-ALA-20110427
                        if PathPDFOriginal = ''  then begin                                                                     //SS-960-ALA-20110713
                            PathPDFCopia    := GenerarPDF(TextoPlantillaCopia, EmptyStr);                                       //SS-960-ALA-20110713
                            PathPDFOriginal := GenerarPDF(TextoPlantilla, '.|' +  PathPDFCopia);                                //SS-960-ALA-20110713
                        end                                                                                                     //SS-960-ALA-20110713
                        else begin                                                                                              //SS-960-ALA-20110713
                            PathPDFCopia    := '|' + GenerarPDF(TextoPlantillaCopia, EmptyStr) +'|'+ PathPDFCopia;              //SS-960-ALA-20110713
                            PathPDFOriginal := GenerarPDF(TextoPlantilla, '.|' + PathPDFOriginal + PathPDFCopia);               //SS-960-ALA-20110713
                        end;
                    end
                    else begin //De lo contrario solamente generamos la nueva plantilla con el pdf generado.                    //SS-960-ALA-20110427
                        if PathPDFOriginal = ''  then begin                                                                     //SS-960-ALA-20110713
                            PathPDFOriginal := GenerarPDF(TextoPlantilla, EmptyStr);                                            //SS-960-ALA-20110713
                            PathPDFCopia    := GenerarPDF(TextoPlantillaCopia, EmptyStr);                                       //SS-960-ALA-20110713
                        end
                        else begin
                            PathPDFOriginal := GenerarPDF(TextoPlantilla, EmptyStr) + '|' +  PathPDFOriginal;                   //SS-960-ALA-20110713
                            PathPDFCopia    := GenerarPDF(TextoPlantillaCopia, EmptyStr) + '|' + PathPDFCopia;                  //SS-960-ALA-20110713
                        end;
                    end;
                end
                else begin
                    //Si nos encontramos en la �ltima plantilla a imprimir entonces concatenamos                                //SS-960-ALA-20110427
                    //el original
                    if k = 0 then begin
                        PathPDFOriginal := GenerarPDF(TextoPlantilla, '.' + PathPDFOriginal);                                   //SS-960-ALA-20110427
                    end
                    else begin //De lo contrario solamente generamos la nueva plantilla con el pdf generado.
                        PathPDFOriginal := PathPDFOriginal + '|' + GenerarPDF(TextoPlantilla, EmptyStr);                        //SS-960-ALA-20110427
                    end;
                end;
            end
            else begin
                MsgBoxErr(MSG_ERROR_NO_EXISTE_PLANTILLA, 'Ruta plantilla: ' + _plantillas[k] ,'Generando Documento PDF', 0);
                Result := false;
            end;
        end;
        //Imprimimos el documento
        Imprimir(PathPDFOriginal);
        Result := true;                                                                                               //SS-960-ALA-20110427
    finally
        TPanelMensajesForm.OcultaPanel;
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : Imprimir
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Realiza la impresi�n de una documento en pdf
-----------------------------------------------------------------------------}
procedure TNotificacionImpresionDoc.Imprimir(pathPDF: String);
begin
    try
        //Mostrando documento a imprimir
        if FileExists(pathPDF) then begin
            TMuestraPDFForm.MuestraPDF(pathPDF, 'Impresi�n Documento', False, True);
        end;
    finally
        //Eliminamos si existe el archivo                                                                             //SS-960-ALA-20110423
        if FileExists (pathPDF) then DeleteFile(pathPDF);
        TPanelMensajesForm.OcultaPanel;
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : GenerarPDF
Author         : Alejandro Labra   
Date Created   : 27-04-2011
Description    : Realiza una impresi�n de un texto en pdf, con la posibilidad
                 de concatenar alg�n documento pdf existente.
-----------------------------------------------------------------------------}
function TNotificacionImpresionDoc.GenerarPDF(texto,
  rutaAdjunto: String): String;
var
    TempFileName    : string;
begin
    try
        TempFileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz', Now);
        StringToFile(texto, TempFileName + '.rtf');
        if Trim(rutaAdjunto) <> '' then begin
            Word2PDF(TempFileName + '.rtf', TempFileName + '.pdf', rutaAdjunto , 'Bullzip PDF Printer', '', 0);
        end
        else begin
            Word2PDF(TempFileName + '.rtf', TempFileName + '.pdf', EmptyStr , 'Bullzip PDF Printer', '', 0);
        end;

        while GetFileSize(TempFileName + '.pdf') <= 0 do begin
            TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Documento en PDF ...');
            Sleep(150);
            Application.ProcessMessages;
        end;

        //Eliminamos el archivo temporal rtf
        if FileExists(TempFileName + '.rtf') then DeleteFile(TempFileName + '.rtf');
        //Eliminamos si existe el archivo adjunto                                                                             
        if FileExists (rutaAdjunto) then DeleteFile(rutaAdjunto);

        Result := TempFileName+'.pdf';
    finally
        TPanelMensajesForm.OcultaPanel;
    end;
end;
{-----------------------------------------------------------------------------
Function Name  : ObtenerPlantilla
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna el valor de la propiedad Plantillas.
-----------------------------------------------------------------------------}
function TNotificacionImpresionDoc.ObtenerPlantillas: TStringList;              //SS-960-ALA-20110427
begin
    Result := _plantillas;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerConCopiaCliente
Author         : Alejandro Labra
Date Created   : 04-08-2011
Description    : Retorna el valor de la propiedad ConCopiaCliente.
-----------------------------------------------------------------------------}
function TNotificacionImpresionDoc.ObtenerConCopiaCliente: Boolean;
begin
    Result := _copiaCliente;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerTagCopia
Author         : Alejandro Labra
Date Created   : 04-08-2011
Description    : Retorna el valor de la propiedad TagCopia.
-----------------------------------------------------------------------------}
function TNotificacionImpresionDoc.ObtenerTagCopia: String;
begin
    Result := _tagCopia;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerVariables
Author         : Alejandro Labra   
Date Created   : 04-08-2011
Description    : Retorna el valor de la propiedad Variables.
-----------------------------------------------------------------------------}
function TNotificacionImpresionDoc.ObtenerVariables: TList;                     //SS-960-ALA-20110427
begin
    Result:= _variables;
end;

end.
