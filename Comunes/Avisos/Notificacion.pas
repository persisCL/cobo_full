{-----------------------------------------------------------------------------
File Name      : Notificacion.pas
Author         : Alejandro Labra
Date Created   : 11-04-2011
Description    : Definici�n de la interfaz INotificacion, la cual permitir�
                 realizar diferentes tipos de notificiaciones de alg�n aviso
                 al usuario.

Revision       : 1
Author         : Alejandro Labra
Date           : 27-04-2011
Firma          : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description    : Implementa la posibilidad de tener un conjunto de plantillas.
Firma          : SS-960-ALA-20110427
-----------------------------------------------------------------------------}
unit Notificacion;

interface

uses    Classes,
        Diccionario;   //Contiene la definici�n de una lista de TClaveValor.

type
    INotificacion = interface(IInterface)
        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra   
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad Plantillas, la cual contiene                  //SS-960-ALA-20110427
                         una lista de las rutas donde se encuentra la plantillas a utilizar.       //SS-960-ALA-20110427
        -----------------------------------------------------------------------------}             //SS-960-ALA-20110427
        function ObtenerPlantillas : TStringList;                                                  //SS-960-ALA-20110427
        procedure AsignarPlantillas(plantillas : TStringList);                                     //SS-960-ALA-20110427
        property Plantillas : TStringList read ObtenerPlantillas write AsignarPlantillas;          //SS-960-ALA-20110427

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad Variables, la cual contiene
                         la lista de variables y sus respectivos valores, que
                         se reemplazar�n en los documentos.
        -----------------------------------------------------------------------------}
        function ObtenerVariables : TList;                                                          //SS-960-ALA-20110427
        procedure AsignarVariables(variables : TList);                                              //SS-960-ALA-20110427
        property Variables : TList read ObtenerVariables write AsignarVariables;                    //SS-960-ALA-20110427

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad ConCopiaCliente, la cual
                         indica si se debe crear dos copias del documento, una
                         para el cliente y otra para la consecionaria
        -----------------------------------------------------------------------------}
        function ObtenerConCopiaCliente : Boolean;
        procedure AsignarConCopiaCliente(conCopia : Boolean);
        property ConCopiaCliente : Boolean read ObtenerConCopiaCliente write AsignarConCopiaCliente;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad TagCopia, la cual
                         indica el tag a reemplazar del documento que indicar� si
                         es copia del cliente o de la concesionaria.
        -----------------------------------------------------------------------------}
        function ObtenerTagCopia : String;
        procedure AsignarTagCopia(tagCopia : String);
        property TagCopia : String read ObtenerTagCopia write AsignarTagCopia;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra   
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la funci�n que Notificar� al usuario.
        -----------------------------------------------------------------------------}
        function Notificar : Boolean;
    end;

implementation

end.
