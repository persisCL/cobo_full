{-------------------------------------------------------------------------------
File Name       :   FrmInicioConsultaConvenio.pas
Author          :   gcasais
Date Created    :   21/04/2005
Language        :   ES-AR
Description     :   Cartola

Revision        :   1
Author          :   DCepeda
Date            :   17-Marzo-2010
Description     :   SS-857, Desplegar los mensajes asociados a cada comprobante,
                    se usa el SP: ObtenerMensajesComprobanteParticular
                    Se agrega una solapa: Mensajes Comprobantes

Revision		:	2
Author			:	Nelson Droguett Sierra
Date			:	28-Abril-2010
Description		:	Valida la repeticion del pedido de envio de detalle por mail.

Revision		:	3
Author			:	Nelson Droguett Sierra
Date			:	03-Mayo-2010
Description		:	Muestra en la grilla de Vehiculos el valor de la columna
					HabilitadoAMB.

Revision		:	4
Author			:	pdominguez
Date			:	01/07/2010
Description		:	SS 887 - Mejorar Performance - Cuentas Duplicadas
                    - Se modificaron los siguientes procedimientos/funciones:
                        lblEditarClick

Revision		:	5
Author			:	jjofre
Date			:	12/08/2010
Description		:	SS 690 - Semaforos
                    - Se modificaron los siguientes procedimientos/funciones:
                        peNumeroDocumentoButtonClick
                        MostrarMensajeTipoClienteSistema
                        CargarDatosCliente
                        Inicializar
                        peNumeroDocumentoKeyUp

Revision		:	6
Author			:	Nelson Droguett Sierra
Date			:	27-Octubre-2010
Description		:	SS 877
                    En la cartola, se debe incorporar el Ultimo Ajuste del convenio
                    en lugar de ajuste sencillo actual y anterior. (MVI)
Firma			: 	SS-877-NDR-20101027

Revision		:	5
Author			:	Nelson Droguett Sierra
Date			:	07-Junio-2011
Description		:	(Ref.Fase2)SS-377
                    procedure dblTransitosLinkClick
                        La columna de la imagen en la grilla de transacciones ahora es 12
Firma           :   SS-377-NDR-20110607

Revision    : 7
Author      : Nelson Droguett Sierra
Date        : 24-Junio-2011
Description : Se corrige que los combos de concesionarias filtren como se espera y que segun la solapa
            en que esten desplieguen la concesionaria segun el tipoconcesionaria
Firma       : PAR00133-FASE2-NDR-20110624

Revision    : 8
Author      : Alejandro Labra
Date        : 04-Julio-2011
Description : Se quita confirmaci�n cuando ya existe mail pendiente de envio de un
             Comprobante o el detalle de comprobante, para que no se vuelva a enviar. 
Firma       : PAR00133-FASE2-ALA-20110704

Revision        :   11
Author          :   Alejandro Labra
Date            :   11-abril-2011
Firma           :   Rev.960 SS Ventana de mensajes de TAGs vencidos
Description     :   Se agrego ventana de mensajes en el caso de encontrar TAGs con fecha de garant�a vencida,
                    al consultar un RUT.
                    Esta se agrega cuando:
                      a.- Al ingresar un Rut en la ventana de Inicio Consulta Convenio
                      b.- Al Presionar "Mas datos" en la ventana Inicio Consulta Convenio
Firma           :   SS-960-ALA-20110411

Firma			:	SS_740_MBE_20110722
Description		:	Se quita que invoque a Comprobantes emitidos para los comprobantes
                	TD y TC
Firma			:	SS_740_NDR_20110725
Description		:	Se pone que la solapa por defecto al inicializar sea la de observaciones.

Firma       : SS-842-NDR-20110912
Description : Se reemplaza el campo memo de las observaciones por una grilla.
            Se habilita un rango de fechas para filtrar y se incluyen en la busqueda las observaciones y el historico.

Firma       : SS_997_PDO_20111014
Description : Se corrige la inicializaci�n de la calse TTipoMedioComunicacion,
                al crearla para almacenar e-mails.

Firma       : SS_842_PDO_20111212
Description :
    - Se a�ade el objeto chkObservacionesBaja,
 
Firma       : SS-1006-NDR-20111105
Description : Se implementa el bit HabilitadoPA para las listas de acceso de parque arauco
              Si se modifica el Bit del convenio, este afecta todas las cuentas
                que descienden de el. Si se cambia el Bit de una cuenta, solo
                afecta a la cuenta..

  Firma       : SS_1021_HUR_20120328
  Descripcion : Se agregan los campos Velocidad y Carril.
                Se modifican los indices de las columnas en el procedimiento dblTransitosDrawText

Firma       : SS_1046_CQU_20120522
Descripcion : Se agrega opci�n para bajar el resumen de la cuenta corriente a Excel.

Firma       : SS_1046_CQU_20120524
Descripcion : Coloca nombre de archivo por defecto

Firma       : SS_1046_CQU_20120525
Descripcion : Al nombre de archivo le agega el N�mero de Convenio.

Firma       : SS_1046_CQU_20120530
Descripcion : Agrega el permiso al nuevo bot�n de Exportaci�n Excel.

Firma       : SS-1006-NDR-20120614
Descripcion : El Bit HabilitadoPA ahora se llama AdheridoPA
              Mostrar el valor de AdheridoPA,Inhabilitado y el motivo en la pantalla inicial
                

Firma       : SS_1006_MBE_20120731
Description :   Se agrega un �cono para consultar si la patente o Telev�a est�
                en lista de acceso a Parque Arauco. S�lo si el bit Adherido PA
                est� encendido.

Firma       : SS-1006-NDR-20120812
Descripcion : Limpiar los filtros de la solapa ListasDeAcceso al cambiar de cliente

Firma       : SS-1006-NDR-20120814
Descripcion : En la solapa de listas de acceso el filtro por patente no funcionaba ya que lo hacia por indicevehiculo

Firma       : SS 1006 / 1015_MCO_20120828
Descripcion : Se comenta codigo en referencia al campo Mostrar Hasta que se elimino, ademas se agrego al exportar CVS sugerencia de nombre

Firma       : SS-1006-1015-MCO-20120903
Descripcion : se cambia foco cuando se cierra mensaje informativo con enter debido a que muestra el mensaje si se cierra con la tecla enter

Firma       : SS_842_CQU_20120913
Descripcion : Se agrega que al momento de cambiar el cliente, se limpie la pesta�a de Observaciones.

Firma       : SS_1068_NDR_20120910_CQU
Descripcion : Al procedimiento BuscarMediosPagosAnteriores se modifica para que
              env�e false como par�metro en la consulta de SPObtenerMediosPagoAnterioresConvenio
              con esto logramos que SIEMPRE enmascare la tarjeta PAC / PAT.

Firma       : SS_1006_1015_MCO_20120927
Descripcion : Se modifica el icono de la consulta pak para que se muestre en caso de que
              la persona haya tenido en alg�n momento alguna cuenta habilitada en lista de Acceso..

Firma       : SS-1006-1015-MCO-20120927
Descripcion : se corrige error reportado desde el formulario de reclamos, se producia cuando hacia setfocus
              y el objeto no habia sido inicializado o visualizado. se valida si esta mostrandose para hacer el setfocus

Firma       : SS_660_MCO_20130221
Description : Mostrar cuando el convenio est� en lista amarilla

Firma       : SS_660_CQU_20121010
Description : Despliega el hist�rico de cartas enviadas p�r Lista Amarilla

Firma       : SS_1006_MDI_20130410
Description : Se a�aden campos Usuario Alta y Usuario Baja a Pesta�a "Listas De Acceso"

Firma       : SS_1006L_MDI_20130416
Descripcion : Se cambia orden y nombres a columnas en pesta�a Listas de Acceso
              Se implementa ordenamiento de columnas en pesta�a Estacionamentos

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

Author      : Manuel Cabello
Date        : 29-Julio-2013
Firma       : SS_660_MCA_20130729
Description : Se agrega columna Concesionaria a la solapa de lista amarilla
              se obtiene el historico de cartas dependiendo del historico Lista Amarilla seleccionado.

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

Firma       : SS_1112_NDR_20130819
Descripcion : Error en el titulo del formulario de modificacion de convenio cuando viene "desde" comprobantesemitidos

 Firma       :   SS_1120_MVI_20130820
 Descripcion :  Se modifica el procedimiento cbConvenioDrawItem para que llame al procedimiento generico ItemComboBoxColor.
                Se comenta el procedimiento ItemComboBoxColor, ya que con el cambio no se utilizara.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                Se acorta tama�o del lblEnListaAmarilla.
                Se soluciona problema, ya que enviaba Caption cuando se pinchaba el link mas datos, ahora se trae
                el n�mero convenio que se obtiene por el c�digo de convenio.

Firma       :   SS_660_MCA_20130912
Descripcion :   Se agrega bit MMINotOk al apartado de vehiculos

Firma       :   SS_1170_MCA_20140214
Descripcion :   se agregan 2 columnas a la solapa Transacciones, Illegal Trampety (TR) y Not In Holder (NH)
            	ademas se modifico el nombre de la columna MMICO y se dejo con el nombre CO

Firma       :  SS_1006P_NDR_20140715
Descripcion : Limpiar la lista de estacionamientos reclamados.

Firma       :   SS_1151_CQU_20140722
Descripcion :   A ra�z de las pruebas del WebServicePAK fue detectado un problema con la grilla de veh�culos,
                una de las validaciones las hac�a por ID de columna y no por el nombre, alguien agreg� un campo
                lo que le cambi� el ID a la columna y ya no se hac�a la validaci�n donde correspond�a.

Firma       :   SS_1193_B_MBE_20140929
Description :   Se cambia el redibujado de la grilla de comprobantes.
                Todos los links se validar�n al momento de ahcer clic y no al momento
                de redibujar la grilla

Author      : Claudio Quezada Ib��ez
Date        : 17-Septiembre-2013
Firma       : SS_1214_CQU_20140917
Description : Se agrega Columna para desplegar la imagen de referencia de una cuenta.

Firma       : SS_1147Q_20141202
Descripcion : Corrige error de despliegue del caption del formulario cuando cambia de convenio en el combo.


Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_MCA_20150422
Descripcion : se agrega label con el nombre corto de la concesionaria nativa


Firma       : SS_1301_NDR_20150609
Descripcion : Al iniciar debe partir con el cursor en el RUT y tener activa la primera solapa.




Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

Firma       : SS_1235_MGO_20151113
Descripcion : Se agrega variable FEnviar_NK_Via_Mail para ocultar las opciones de env�o en la grilla de comprobantes

Firma       : SS_1419_NDR_20151130
Descripcion : Se envia parametro @CodigoUsuario a SP InicializarNotificacionesTAGsPersona para grabar nuevos campos de auditoria
            en tabla Personas y en HistoricoPersonas

Firma       : TASK_007_ECA_20160428
Descripcion : Se agrega validacion por tipo de convenio y mostrar el tipo de convenio en el combo convenio

Etiqueta    : 20160531 MGO
Descripci�n : Se agrega par�metro TipoLetra a la composici�n de email
-------------------------------------------------------------------------------}
unit FrmInicioConsultaConvenio;

interface

uses
    //Cartola
    DMConnection            ,   //coneccion a base de datos OP_CAC
    UtilDB                  ,   //Rutinas para base de datos
    Util                    ,   //IIF
    UtilProc                ,   //Mensajes
    OrdenesServicio         ,   //Rutinas para Ordenes de Servicio
    BuscaClientes           ,   //Busca Clientes
    PeaProcs                ,   //Cargar Convenios Rut
    MenuesContextuales      ,   //Menues Contextuales
    peaTypes                ,   //scNormal
    RStrings                ,   //Resource Strings
    ComprobantesEmitidos    ,
    ImagenesTransitos       ,
    FrmModificacionConvenio ,
    FrmChecklist            ,
    FrmUltimosConsumos      ,
    frmVisorDocumentos      ,
    ComponerEMail           ,
    MensajesEmail           ,
    CobranzasResources,
    frmMuestraMensaje,
    frmImagenesSPC,
    //General
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons, DmiCtrls, validate,
    Dateedit, DB, ADODB, ImgList, Menus, DBClient, ListBoxEx, DBListEx,
    ComCtrls, CollPnl, VariantComboBox,  DPSPageControl, math, DPSControls,
    CheckLst, DateUtils, BuscaTab, TimeEdit, CSVUtils, DMMensaje, DdeMan, StrUtils, ShellApi,
    RBSetup, ReporteRecibo, UtilMails, convenios, PeaProcsCN, SysUtilsCN, Provider,
    frmConsultaAltaPArauco, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdGlobal,
    frmConsultaAltaOtraConsecionaria, PleaseWait;   // TASK_015_RME_20170118

  Const
    COLOR_DEFAULT = '00FFFFFF';

  type
  TFormInicioConsultaConvenio = class(TForm)
    dsObtenerCuentasCartola: TDataSource;
    ObtenerResumenDeudaConvenio: TADOStoredProc;
    dsReclamos: TDataSource;
  	spReclamos: TADOStoredProc;
    popReclamosGenerales: TPopupMenu;
    dsObtenerPagosConvenio: TDataSource;
    spObtenerPagosConvenio: TADOStoredProc;
    Checklist: TClientDataSet;
    dsChecklist: TDataSource;
  	popChecklist: TPopupMenu;
    mnuAgregarChecklist: TMenuItem;
    mnuQuitarChecklist: TMenuItem;
    mnuEditarCheckList: TMenuItem;
    spObtenerDatosCliente: TADOStoredProc;
    spObtenerCuentasCartola: TADOStoredProc;
    spObtenerTransitosConvenio: TADOStoredProc;
    dsObtenerTransitosConvenio: TDataSource;
    dsObtenerComprobantesConvenio: TDataSource;
    spObtenerComprobantesConvenio: TADOStoredProc;
    spObtenerCheckListConvenioCartola: TADOStoredProc;
    spActualizarConvenioCheckList: TADOStoredProc;
    Imagenes: TImageList;
    SPObtenerUltimosConsumos: TADOStoredProc;
    dsUltConsumos: TDataSource;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    ObtenerConvenioCartola: TADOStoredProc;
    lnCheck: TImageList;
    ObtenerConvenioObservacion: TADOStoredProc;
    cpVehiculo: TCollapsablePanel;
    ilCamara: TImageList;
    spFechasProximaFacturacion: TADOStoredProc;
    dsfechasProximaFacturacion: TDataSource;
    ObtenerHistoricoMedioPago: TADOStoredProc;
    daObtenerHistoricoMedioPago: TDataSource;
    daObtenerHistoricoDomicilios: TDataSource;
    ObtenerHistoricoDomicilios: TADOStoredProc;
    sdGuardarCSV: TSaveDialog;
    cpBusquedaConvenio: TCollapsablePanel;
    pnlDatosConvenio: TPanel;
    Shape4: TShape;
    Shape2: TShape;
    Shape1: TShape;
    Label7: TLabel;
    Label15: TLabel;
    Label3: TLabel;
    lblDescuentos: TLabel;
    lblPeajeDelPeriodo: TLabel;
    Bevel3: TBevel;
    lblTitTotalAPagar: TLabel;
    lblTotalAPagar: TLabel;
    Label4: TLabel;
    Bevel5: TBevel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    lblEmail: TLabel;
    lblTelParticular: TLabel;
    lblTelMovil: TLabel;
    lblTelComercial: TLabel;
    Bevel6: TBevel;
    Label18: TLabel;
    Label21: TLabel;
    Label23: TLabel;
    Label2: TLabel;
    lblEditar: TLabel;
    Led1: TLed;
    Led2: TLed;
    Led3: TLed;
    Label17: TLabel;
    lblAjusteSencilloActual: TLabel;
    lblConcesionaria: TLabel;
    Shape3: TShape;
    lblEstadoConvenio: TLabel;
    lblVerImagenScan: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lblFechaProximaFacturacion: TLabel;
    lblInicioProximaFacturacion: TLabel;
    lblfinProximaFacturacion: TLabel;
    Label24: TLabel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    lSaldoAnterior: TBevel;
    lblTitSaldoConvenio: TLabel;
    lblSaldoConvenio: TLabel;
    peNumeroDocumento: TPickEdit;
    cbConvenio: TVariantComboBox;
    Panel6: TPanel;
    pnlEstadoCuenta: TPanel;
    Panel12: TPanel;
    Panel1: TPanel;
    Bevel1: TBevel;
    PageControl: TPageControl;
    tsTransacciones: TTabSheet;
    dblTransitos: TDBListEx;
    PFiltroTransitos: TPanel;
    LTransitosDesde: TLabel;
    LTransitosHasta: TLabel;
    lblTransitosReestablecer: TLabel;
    lblCSV: TLabel;
    deTransitosDesde: TDateEdit;
    deTransitosHasta: TDateEdit;
    teTransitosHoraDesde: TTimeEdit;
    teTransitosHoraHasta: TTimeEdit;
    cbTransitoSoloVehiculo: TCheckBox;
    btnTransitosFiltrar: TButton;
    tsComprobantes: TTabSheet;
    dblComprobantes: TDBListEx;
    tsPagos: TTabSheet;
    dblPagos: TDBListEx;
    tsReclamos: TTabSheet;
    dblReclamos: TDBListEx;
    tsUltimosConsumos: TTabSheet;
    dblUltConsumos: TDBListEx;
    PFiltroUC: TPanel;
    lblUCFechaDesde: TLabel;                                                    //TASK_060_GLE_20170307
    dedtUCFechaDesde: TDateEdit;                                                //TASK_060_GLE_20170307
    btnUCFiltrar: TButton;
    cbUCSoloVehiculo: TCheckBox;
    tsHistoricoMediosPagos: TTabSheet;
    dblHistoricoMedioPago: TDBListEx;
    tsHistoricoDomicilios: TTabSheet;
    dblHistoricoDomicilios: TDBListEx;
    tsMails: TTabSheet;
    cpOpciones: TCollapsablePanel;
    rgOtros: TRadioGroup;
    DBListEx2: TDBListEx;
    Label5: TLabel;
    neNotificacionesTop: TNumericEdit;
    Label6: TLabel;
    deNotificacionesDesde: TDateEdit;
    deNotificacionesHasta: TDateEdit;
    btnBuscar: TButton;
    Label12: TLabel;
    spExplorarEMails: TADOStoredProc;
    dsEMails: TDataSource;
    mnuComponer: TPopupMenu;
    mnuComponerEsteMail: TMenuItem;
    Label13: TLabel;
    Label14: TLabel;
    Label19: TLabel;
    lblIntereses: TLabel;
    lblOtrosConceptos: TLabel;
    lblAjusteSencilloAnterior: TLabel;
    tsAccionesExternas: TTabSheet;
    dblAccionesExtrenas: TDBListEx;
    spObtenerAccionesCobranzaSerbanc: TADOStoredProc;
    dsObtenerAccionesCobranzaSerbanc: TDataSource;
    spCierraLogComincaciones: TADOStoredProc;
    spAgregarLogComunicacioes: TADOStoredProc;
    Shape5: TShape;
    pnlTitUltimaNK: TPanel;
    Label1: TLabel;
    lblPeajePeriodoAnt: TLabel;
    tsResumenCC: TTabSheet;
    dbResumenCC: TDBListEx;
    dsResumenCuentaCorriente: TDataSource;
    ObtenerResumenCuentaCorriente: TADOStoredProc;
    Panel4: TPanel;
    BtnFiltrarComprobantes: TButton;
    Panel5: TPanel;
    BtnFiltrarPagos: TButton;
    Panel7: TPanel;
    BtnCtaCte: TButton;
    Panel8: TPanel;
    BtnReclamos: TButton;
    Panel9: TPanel;
    BtnHistoricoMP: TButton;
    Panel10: TPanel;
    BtnHistoricoDom: TButton;
    Panel11: TPanel;
    BtnAccionesExternas: TButton;
    DSObtenerMediosPagoAnterioresConvenio: TDataSource;
    lblTitDeudaComprobantesCuotas: TLabel;
    lblDeudaComprobantesCuotas: TLabel;
    RgTipos: TRadioGroup;
    tsIndemnizaciones: TTabSheet;
    pnIndemnizaciones: TPanel;
    btnIndemnizaciones: TButton;
    dblIndemnizaciones: TDBListEx;
    dsIndemnizaciones: TDataSource;
    spObtenerIndemnizacionesConvenio: TADOStoredProc;
    SPObtenerMediosPagoAnterioresConvenio: TADOStoredProc;
    Label8: TLabel;
    Label16: TLabel;
    lblGrupo: TLabel;
    lblRNUT: TLabel;
    spInsertarConvenioAEnviarDetalle: TADOStoredProc;
    //REV.1
    tsMensajesComprobantes : TTabSheet;
    dsObtenerMensajesComprobanteParticular : TDataSource;
    spObtenerMensajesComprobanteParticular : TADOStoredProc;
    pnlMensajesComprobantes : TPanel;
    btnMensajesComprobantes : TButton;
    DBLMensajesComprobantes : TDBListEx;
    lblDeudaRef: TLabel;
    tsRefinanciaciones: TTabSheet;
    Panel13: TPanel;
    btnRefinanciaciones: TButton;
    spObtenerRefinanciacionResumenCuentaCorriente: TADOStoredProc;
    dsRefinanResumenCC: TDataSource;
    Label22: TLabel;
    lblUltimoAjusteSencillo: TLabel;
    spObtenerRefinanciacionesTotalesPersona: TADOStoredProc;
    DBLstExRefinanResumenCC: TDBListEx;
    cdsRefinanResumenCC: TClientDataSet;
    dspRefinanResumenCC: TDataSetProvider;
    cdsRefinanResumenCCOrden: TIntegerField;
    cdsRefinanResumenCCFecha: TDateTimeField;
    cdsRefinanResumenCCTipo: TSmallintField;
    cdsRefinanResumenCCCodigoRefinanciacion: TIntegerField;
    cdsRefinanResumenCCCodigoCuota: TSmallintField;
    cdsRefinanResumenCCNumeroRecibo: TIntegerField;
    cdsRefinanResumenCCDescripcionTipo: TStringField;
    cdsRefinanResumenCCNumeroTipo: TIntegerField;
    cdsRefinanResumenCCDescripcionFormaPago: TStringField;
    cdsRefinanResumenCCCodigoEstadoCuota: TSmallintField;
    cdsRefinanResumenCCDescripcionEstadoCuota: TStringField;
    cdsRefinanResumenCCEstadoImpago: TBooleanField;
    cdsRefinanResumenCCImporteDesc: TStringField;
    cdsRefinanResumenCCSaldoCCDesc: TStringField;
    cdsRefinanResumenCCNumeroConvenio: TStringField;
    cdsRefinanResumenCCSaldoDesc: TStringField;
    Panel14: TPanel;
    dblCuentas: TDBListEx;
    cb_MostrarBajas: TCheckBox;
    spObtenerDeudaComprobantesVencidos: TADOStoredProc;
    //Fin REV.1
    vcbConcesionariasTransa: TVariantComboBox;
    spObtenerConcesionarias: TADOStoredProc;
    lblDeudaRefLabel: TLabel;
    lblUltimoAjustesencilloLabel: TLabel;
    tsEstacionamiento: TTabSheet;
    pnlEstacionamiento: TPanel;
    deEstacDesde: TDateEdit;
    deEstacHasta: TDateEdit;
    teEstacDesde: TTimeEdit;
    teEstacHasta: TTimeEdit;
    lblEstacDesde: TLabel;
    lblEstacHasta: TLabel;
    dblEstacionamientos: TDBListEx;
    lblEstacConcesionaria: TLabel;
    vcbConcesionariasEstacionamiento: TVariantComboBox;
    btnEstacBuscar: TButton;
    spObtenerEstacionamientos: TADOStoredProc;
    dsEstacionamientos: TDataSource;                                       //SS-842-NDR-20110912
    dsConvenioObservacion: TDataSource;
    lblReimprimirNotifTAGsVencidos: TLabel;
    spInicializarNotificacionesTAGsPersona: TADOStoredProc;
    btnExcel: TButton;                                          // SS_1046_CQU_20120522
    dlgGrabarExcel: TSaveDialog;                                // SS_1046_CQU_20120522
    pnlListaAcceso: TPanel;
    chk_SuscritoListaBlanca: TCheckBox;
    lblMotivoInhabilitacion: TLabel;
    tsListasDeAcceso: TTabSheet;
    pnlBusqueda: TPanel;
    Panel17: TPanel;
    pnlAdhesion: TPanel;
    Splitter1: TSplitter;
    pnlInhabilitacion: TPanel;
    lblTituloAdhesion: TLabel;
    lblTituloInhabilitacion: TLabel;
    DBLEAdhesionPA: TDBListEx;
    DBLEInhabilitaciones: TDBListEx;
    deHistListaAccesoDesde: TDateEdit;
    deHistListaAccesoHasta: TDateEdit;
    Label20: TLabel;
    Label25: TLabel;
    btnBuscarHistoricosListasDeAcceso: TButton;
    spObtenerHistoricoListaDeAccesoConvenio: TADOStoredProc;
    spObtenerHistoricoConveniosInhabilitados: TADOStoredProc;
    dsHistoricoListaDeAccesoConvenio: TDataSource;
    dsHistoricoConveniosInhabilitados: TDataSource;
    cbbPatentes: TVariantComboBox;
    lblPatente: TLabel;
    btnSuscribirListaBlanca: TButton;
    lblEnListaAmarilla: TLabel;                                                     //SS_660_MCO_20130121
    tsListaAmarilla: TTabSheet;                                                     //SS_660_MCO_20130121
    pnl1: TPanel;                                                                   //SS_660_MCO_20130121
    lbl1: TLabel;                                                                   //SS_660_MCO_20130121
    lbl2: TLabel;                                                                   //SS_660_MCO_20130121
    deHistLADesde: TDateEdit;                                                       //SS_660_MCO_20130121
    deHistLAHasta: TDateEdit;                                                       //SS_660_MCO_20130121
    btnHistoricoListaAmarilla: TButton;                                             //SS_660_MCO_20130121
    pnl2: TPanel;                                                                   //SS_660_MCO_20130121
    spl1: TSplitter;                                                                //SS_660_MCO_20130121
    pnl3: TPanel;                                                                   //SS_660_MCO_20130121
    lbl3: TLabel;
    dblHistoricoListaAmarilla: TDBListEx;                                           //SS_660_MCO_20130121
    pnl4: TPanel;                                                                   //SS_660_MCO_20130121
    lbl4: TLabel;                                                                   //SS_660_MCO_20130121
    spHistoricoListaAmarilla: TADOStoredProc;                                       //SS_660_MCO_20130121
    spHistoricoCartasListaAmarilla: TADOStoredProc;                                 //SS_660_MCO_20130121
    dsHistoricoListaAmarilla: TDataSource;                                          //SS_660_MCO_20130121
    dsHistoricoCartas: TDataSource;                                                 //SS_660_MCO_20130121
    dblHistoricoCartas: TDBListEx;
    tsPagosAutomaticos: TTabSheet;
    pnlFiltros: TPanel;
    dblPagosAutomaticos: TDBListEx;
    lblMostrar: TLabel;
    cbbMostrarPagosAutomaticos: TVariantComboBox;
    lblFiltroPatente: TLabel;
    btnBuscarVehiculoPorPatente: TButton;
    edtFiltrarPatente: TEdit;
    btnMostrarEmail: TButton;
    lblNovedad: TLabel;
    shpNovedad: TShape;
    pnlNovedad: TPanel;
    lblConvenioCastigado: TLabel;           //TASK_049_GLE_20170207
    chkTransitoSoloGratuidad: TCheckBox;	//SS_1147_MCA_20150422
    tsDescuentos: TTabSheet;			//TASK_103_CFU_20170209
    Panel18: TPanel;		//TASK_103_CFU_20170209
    btnAdministrarDescuentos: TButton;		//TASK_103_CFU_20170209
    DBLDescuentos: TDBListEx;		//TASK_103_CFU_20170209
    spObtenerDescuentosConvenio: TADOStoredProc;		//TASK_103_CFU_20170209
    dsObtenerDescuentosConvenio: TDataSource;
    chkMuestraConsumosNoFacturados: TCheckBox;          //TASK_103_CFU_20170209
    lblUCFechaHasta: TLabel;                                                     //TASK_060_GLE_20170307
    dedtUCFechaHasta: TDateEdit;                                                 //TASK_060_GLE_20170307

	//procedure IntentosCHKClick(Sender: TObject);                               //TASK_064_GLE_20170329
    procedure dblComprobantesMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    //procedure BTNAnterioresMPClick(Sender: TObject);                           //TASK_064_GLE_20170329
    procedure PageControlChange(Sender: TObject);
    //procedure txtObservacionesEnter(Sender: TObject);                          //TASK_066_GLE_20170409
    procedure peNumeroDocumentoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnAccionesExternasClick(Sender: TObject);
    procedure BtnHistoricoDomClick(Sender: TObject);
    procedure BtnHistoricoMPClick(Sender: TObject);
    procedure BtnReclamosClick(Sender: TObject);
    procedure BtnCtaCteClick(Sender: TObject);
    procedure BtnFiltrarPagosClick(Sender: TObject);
    procedure BtnFiltrarComprobantesClick(Sender: TObject);
    //procedure BtnFiltrarObservacionesClick(Sender: TObject);                  //TASK_066_GLE_20170409
    procedure mnuComponerPopup(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mnuComponerEsteMailClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure lblEditarClick(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure cbConvenioChange(Sender: TObject);
  	procedure cb_MostrarBajasClick(Sender: TObject);
    //check list
    procedure dblChecklistDblClick(Sender: TObject);
    procedure popChecklistPopup(Sender: TObject);
    procedure mnuAgregarChecklistClick(Sender: TObject);
    procedure mnuQuitarChecklistClick(Sender: TObject);
    procedure mnuEditarCheckListClick(Sender: TObject);
    //Cuentas
    procedure dblCuentasDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure dblCuentasContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    //Comprobantes
    procedure dblComprobantesDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure dblComprobantesLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure dblComprobantesContextPopup(Sender: TObject;MousePos: TPoint; var Handled: Boolean);
    procedure lblEmailClick(Sender: TObject);
    //Transitos
    procedure dblTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure dblTransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
    procedure dblTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure dblTransitosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    //Reclamos
    procedure LNuevoReclamoGeneralClick(Sender: TObject);
    procedure dblReclamosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure dblReclamosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    //Salir
    procedure btn_cerrarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lblVerImagenScanClick(Sender: TObject);
{    procedure neCantidadTransitosChange(Sender: TObject);}                     //SS 1006 / 1015_MCO_20120828
    procedure dblCuentasDblClick(Sender: TObject);
    procedure btnTransitosFiltrarClick(Sender: TObject);
    procedure lblTransitosReestablecerClick(Sender: TObject);
    procedure lblUCReestablecerClick(Sender: TObject);
    procedure btnUCFiltrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblPagosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    procedure lblCSVClick(Sender: TObject);
    procedure IrAConvenio(const CodigoConvenio: Integer);
    procedure IrARUT(const NumeroRUT: Ansistring);
    procedure OrdenarColumna(Sender: TObject);
    // Desglozamiento de MostrarConvenio
    //Procedure BuscarObservaciones;                                            //TASK_066_GLE_20170409
    Procedure BuscarComprobantes;
    Procedure BuscarPagosConvenio;
    Procedure BuscarDatosCuentasCorrientes;
    Procedure BuscarReclamos;
    procedure BuscarHistoricoListaAmarilla;                                     // SS_660_MCO_20130225
    Procedure BuscarHistoricoMediosPagos;
    //procedure BuscarMediosPagosAnteriores;                                    //TASK_064_GLE_20170329
    Procedure BuscarHistoricoDomicilios;
    Procedure BuscarAccionesExternas;
    Procedure LimpiarDatos;
    Procedure CargarDatosBasicos(pMostrarTAGsVencidos: Boolean = True);			// SS_960_PDO_20110608
    procedure btnIndemnizacionesClick(Sender: TObject);
    //REV.1
    procedure btnMensajesComprobantesClick(Sender: TObject);
    Procedure BuscarMensajesComprobantesParticular;
    procedure btnRefinanciacionesClick(Sender: TObject);
    procedure DBLstExRefinanResumenCCDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure DBLstExRefinanResumenCCDrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure DBListEx1Columns0HeaderClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  	//Fin REV.1
    procedure vcbConcesionariasTransaChange(Sender: TObject);
    procedure btnEstacBuscarClick(Sender: TObject);
    procedure vcbConcesionariasEstacionamientoChange(Sender: TObject);
    procedure deHistoricoDesdeChange(Sender: TObject);      // SS_842_PDO_20111212
    procedure DBLEConvenioObservacionesColumns0HeaderClick(Sender: TObject);
    procedure DBLEConvenioObservacionesColumns1HeaderClick(Sender: TObject);
    //procedure ObtenerConvenioObservacionAfterOpen(DataSet: TDataSet);         //TASK_066_GLE_20170409
    //procedure ObtenerConvenioObservacionAfterScroll(DataSet: TDataSet);       //TASK_066_GLE_20170409
    //procedure ObtenerConvenioObservacionAfterClose(DataSet: TDataSet);        //TASK_066_GLE_20170409
    //procedure DBLEConvenioObservacionesDrawText(Sender: TCustomDBListEx;      //TASK_066_GLE_20170409
    //  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;           //TASK_066_GLE_20170409
    //  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;           //TASK_066_GLE_20170409
    //  var DefaultDraw: Boolean);                                              //TASK_066_GLE_20170409
    procedure dblEstacionamientosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dblEstacionamientosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dblEstacionamientosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure lblReimprimirNotifTAGsVencidosClick(Sender: TObject);
    procedure chkObservacionesBajaClick(Sender: TObject);      // SS_960_PDO_20120115
    procedure btnExcelClick(Sender: TObject);                  // SS_1046_CQU_20120522
    Procedure BuscarHistoricosListasDeAcceso;                               //SS-1006-NDR-20120614
    procedure btnBuscarHistoricosListasDeAccesoClick(Sender: TObject);      //SS-1006-NDR-20120614
    procedure DBLEAdhesionPAColumns0HeaderClick(Sender: TObject);
    procedure btnSuscribirListaBlancaClick(Sender: TObject);
    procedure dblCuentasLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure cbbPatentesChange(Sender: TObject);
    procedure dbResumenCCDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure Panel7Resize(Sender: TObject);
    procedure cbConvenioDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure btnHistoricoListaAmarillaClick(Sender: TObject);
    procedure dblEstacionamientosColumnsHeaderClick(Sender: TObject);  // SS_1006L_MDI_20130416
    procedure dblHistoricoCartasDrawText(Sender: TCustomDBListEx;     // SS_660_CQU_20130604
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;   // SS_660_CQU_20130604
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;   // SS_660_CQU_20130604
      var DefaultDraw: Boolean);
    procedure dsHistoricoListaAmarillaDataChange(Sender: TObject;
      Field: TField);
    procedure dblTransitosColumns21HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dblTransitosColumns22HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dblTransitosColumns15HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dblTransitosColumns16HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dblTransitosColumns17HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dblTransitosColumns18HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dblTransitosColumns19HeaderClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure dblTransitosColumns20HeaderClick(Sender: TObject);
    procedure PageControlDrawTab(Control: TCustomTabControl; TabIndex: Integer;
      const Rect: TRect; Active: Boolean);


    procedure dblPagosAutomaticosClick(Sender: TObject);
    procedure cbbMostrarPagosAutomaticosChange(Sender: TObject);
    procedure btnBuscarVehiculoPorPatenteClick(Sender: TObject);
    procedure btnMostrarEmailClick(Sender: TObject);                //SS_1170_MCA_20140214
    procedure btnAdministrarDescuentosClick(Sender: TObject);		//TASK_103_CFU_20170209
	procedure dblIndemnizacionesColumns0HeaderClick(Sender: TObject);
    procedure chkMuestraConsumosNoFacturadosClick(Sender: TObject);                //SS_1170_MCA_20140214 //TASK_058_GLE_20170308

  private
    FListaTransitos: TStringList;
    FListaEstacionamientos: TStringList;    // SS_1006_PDO_20120106
    FCodigoPersona: Integer;
    FUltimaBusqueda: TBusquedaCliente;
    FPuntoVenta, FPuntoEntrega: Integer;
    //Indica que vehiculo esta seleccionado
    FIndiceVehiculoSeleccionado:integer;
    //Filtro de Transitos
    FTransitosDesde, FTransitosHasta : TDateTime;
    FCantidadTransitos: Integer;
    //Filtro de Ultimos Consumos
    //FUCCorte: TDateTime;                                                       //TASK_060_GLE_20170307
    FUCCorteDesde: TDateTime;                                                    //TASK_060_GLE_20170307
    FUCCorteHasta: TDateTime;                                                    //TASK_060_GLE_20170307
    FMaxNotificacionesReintentos: Integer;
    FAhora: TDate;
    FColumnaAgrandada : Boolean;                                                        // SS_660_CQU_20130604
    FEnviar_NK_Via_Mail : Boolean;                                                      //SS_1235_MGO_20151113

    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;
{INICIO: TASK_006_JMA_20160420}
    spPagosAutomaticos: TADOStoredProc;
    dsPagosAutomaticos: TDataSource;
    procedure BuscarPagosAutomaticosConvenio;

{TERMINO: TASK_006_JMA_20160420}
    procedure CargarDatosCliente;
  	procedure OSChanged;
    procedure ActualizarDomicilioFacturacion;
    Function  ListaTransitosInclude(valor:string):boolean;
    Function  ListaEstacionamientosInclude(valor:string):boolean;                       // SS_1006_PDO_20120106
    procedure ListaTransitosExclude(Indice: Integer);
    procedure ListaEstacionamientosExclude(Indice: Integer);                            // SS_1006_PDO_20120106
    Function  ListaTransitosIn(valor:string; var IndexValue: Integer): Boolean;
    Function  ListaEstacionamientosIn(valor:string; var IndexValue: Integer): Boolean;  // SS_1006_PDO_20120106
    //Transitos
    procedure EstablecerFechasTransitos(var Inicial: TDateTime; var Final: TDateTime);
    //procedure FiltrarTransitos(const Datos: TADOStoredProc; CodigoConvenio, IndiceVehiculo, CodigoConcesionaria: Variant);                        // TASK_112_MGO_20170110
    procedure FiltrarTransitos(const Datos: TADOStoredProc; CodigoConvenio, IndiceVehiculo, CodigoConcesionaria: Variant; SoloGratuidad: Boolean);  // TASK_112_MGO_20170110
    function  ValidarFiltroTransitos : Boolean;
    //Ultimos Consumos
    procedure EstablecerFechaUC(var Cierre: TDateTime);
    //procedure FiltrarUltimosConsumos(const Datos: TADOStoredProc; CodigoConvenio, IndiceVehiculo, CodigoConcesionaria: Variant);              //TASK_060_GLE_20170307
    procedure FiltrarUltimosConsumos(const Datos: TADOStoredProc; CodigoConvenio, Patente: Variant);                                            //TASK_060_GLE_20170307

    function  ValidarFiltroUC: Boolean;
    {INICIO: 	20160707 CFU
    procedure FiltrarCuentas(const Datos: TADOStoredProc; CodigoConvenio: Integer; MostrarBajas: Boolean);
    }
    procedure FiltrarCuentas(const Datos: TADOStoredProc; CodigoConvenio: Integer; Patente: string; MostrarBajas: Boolean);
    //TERMINO:	20160707 CFU
    procedure MostrarConvenio(Const CodigoConvenio: Integer);
    function ValidarParametrosNotificaciones: Boolean;
    procedure AplicarOtrosFiltrosNotificaciones;
    procedure FiltrarNotificaciones;
    procedure ComponerEmail;
    procedure BuscarIndemnizaciones;
    function MostrarGrupoFacturacion(const Texto: string; const CodigoConvenio: Integer): string;
    procedure MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
    procedure MostrarMensajeTipoClienteSistema(NumeroDocumento: String); //REV.5
    procedure CargarParametrosGenerales();
    procedure GuardarMailPersona(CodigoPersona: Integer; eMail : string) ;
    procedure ReimprimeNotificacionTAGSVencidos;
    function CargarComboPatentes:Boolean;                                       //SS-1006-NDR-20120614
   // procedure ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor; Bold: Boolean);    //SS_1120_MVI_20130820
    procedure ObtenerHistoricoDeCartas(CodigoConvenio : Integer; IdConvenioInhabilitado: Integer);   // SS_660_MCA_20130729 // SS_660_CQU_20121010
    procedure BuscarDescuentos;										//TASK_103_CFU_20170209-CRM_Ver_Descuentos

  protected
  public
	  { Public declarations }
	  function Inicializar(CodigoConvenio: Integer; PuntoVenta, PuntoEntrega: Integer; TabIndex: Integer = 0): Boolean;

      procedure RefrescarActionListCuenta(CodigoConvenio, IndiceVehiculo: Integer);
  end;

var
    FormInicioConsultaConvenio  : TFormInicioConsultaConvenio;
    mousePosicion               : TPoint;
    AnchoCelda                  : integer;
    PosCelda                    : TRect;

    CODIGO_TIPO_CONVENIO        : Integer;    //TASK_007_ECA_20160428

    CUENTA_COMERCIAL_PREDETERMINADA : Integer;    //TASK_012_ECA_20160514
    TIPO_CONVENIO_RNUT              : Integer;    //TASK_034_ECA_20160611
    SuscribirListaBlanca            : Boolean;    //TASK_034_ECA_20160611
const
    STR_ENVIO_MAIL              = 'e-Mail NK';
    HEADER_NOTIFICAR            = 'Notif. Comprobante';
    STR_ENVIO_MAIL_DET          = 'e-Mail Det';
    HEADER_NOTIFICAR_DET        = 'Notif. Detalle';
    HEADER_DETALLES             = 'Detalles';
    //--------------------------------------------------------------------------------

ResourceString
    ESTADO_CONVENIO             = 'Convenio: ';
    ESTADO_RNUT                 = 'Convenio RNUT: ';
    MSG_ESTADO_CUENTA_AL        = 'Estado de cuenta al %s, %s';

    MSG_AVISO_REF_DEUDA_VENCIDA_TITULO   = 'Validaci�n Cuotas Vencidas';
    MSG_AVISO__REF_DEUDA_VENCIDA_MENSAJE = 'El cliente tiene Cuotas Vencidas por Refinanciaci�n.';

const
    CONST_COLUMNA_IMAGEN = 14;      // SS_1021_PDO_20120421

implementation

uses Types, frmDatoPesona, ConstParametrosGenerales,
	//INICIO	: CFU 20170210 TASK_103_CFU_20170209-CRM_Ver_Descuentos
	frmAdministrarDescuentos;
	//TERMINO	: CFU 20170210 TASK_103_CFU_20170209-CRM_Ver_Descuentos

{$R *.dfm}

{
    Revision : 4
        Author: pdominguez
        Date  : 01/07/2010
        Description: SS 887 - Mejorar la Performance - Cuentas Duplicadas
            - Se crea el formulario a partir del m�todo create de la clase.
            - Se libera el formulario explicitamente y no con un Release.
}
procedure TFormInicioConsultaConvenio.lblEditarClick(Sender: TObject);
resourcestring
    NAME_INVOICING_EXECUTABLE = 'FACTURACION.EXE';
    SQL_OBTENER_CONVENIO = 'SELECT dbo.ObtenerNumeroConvenio(%s)';               //SS_1120_MVI_20130909
var
    f: TFormModificacionConvenio;
    AuxCodigoConvenio: Integer;
    aModoPantalla: TStateConvenio;
    NumeroConvenio     : string;                                                 //SS_1120_MVI_20130909
begin

    if cbConvenio.Items.Count < 1 then exit;

    Application.CreateForm(TfrmPleaseWait,frmPleaseWait);    //TASK_015_RME_20170118
    Cursor := crHourGlass;                                  //TASK_015_RME_20170118

    try
      (Sender as TLabel).Enabled := False;
      Application.ProcessMessages;
        // Rev. 4 (SS 887)
        //Application.CreateForm(TFormModificacionConvenio, f);
        f := TFormModificacionConvenio.Create(Self);
        // Fin Rev. 4 (SS 887)

        NumeroConvenio := QueryGetValue(DMConnections.BaseCAC, Format(SQL_OBTENER_CONVENIO, [cbConvenio.Value]));             //SS_1120_MVI_20130909
        if UpperCase(COPY(Application.ExeName,Length(Application.ExeName)-14, 15)) = NAME_INVOICING_EXECUTABLE then
          aModoPantalla := scNormal
        else aModoPantalla := scModi;
 //       if f.Inicializa(false, Caption, StringReplace(cbConvenio.Items[cbConvenio.ItemIndex].Caption, '-', '', [rfReplaceAll]), FPuntoVenta, FPuntoEntrega, aModoPantalla) then begin  //SS_1120_MVI_20130909	// SS_390_PDO_20120221

        if f.Inicializa(false, Caption, NumeroConvenio, FPuntoVenta, FPuntoEntrega, aModoPantalla) then begin
            frmPleaseWait.Close;  //TASK_015_RME_20170118              //SS_1120_MVI_20130909
            Cursor := crDefault;  //TASK_015_RME_20170118
            if (f.ShowModal = mrOK) then begin																																			// SS_390_PDO_20120221
              // Se Refresca
              CargarDatosBasicos(False);                  // SS_960_PDO_20110608
              AuxCodigoConvenio   := cbConvenio.Value;
              cbConvenio.Value    := AuxCodigoConvenio;
              cbConvenio.OnChange(cbConvenio);
            end;																																										// SS_390_PDO_20120221
        end else begin
          frmPleaseWait.Close;  //TASK_015_RME_20170118
          Cursor := crDefault;  //TASK_015_RME_20170118
        end;


    finally
        // Rev. 4 (SS 887)
        //f.Release;
        if Assigned(f) then FreeAndNil(f);
        // Fin Rev. 4 (SS 887)
        (Sender as TLabel).Enabled := True;
    end;


end;

{-----------------------------------------------------------------------------
Function Name   :   cb_MostrarBajasClick
Author          :
Date Created    :
Description     :   volver a cargar los datos para incluir las bajas
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.cb_MostrarBajasClick(Sender: TObject);
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    {INICIO:	20160707 CFU
    FiltrarCuentas(spObtenerCuentasCartola, Integer(cbConvenio.Value), cb_MostrarBajas.Checked);
    }
    //if Trim(TrimLeft(TrimRight(edtFiltrarPatente.Text))) = '' then
    //	FiltrarCuentas(spObtenerCuentasCartola, Integer(cbConvenio.Value), '', cb_MostrarBajas.Checked)
    //else
    	FiltrarCuentas(spObtenerCuentasCartola, Integer(cbConvenio.Value), edtFiltrarPatente.Text, cb_MostrarBajas.Checked);
    //TERMINO:	20160707 CFU
    // limpia las pantallas que se pudieran estar mostrando
    if spObtenerCuentasCartola.RecordCount > 0 then begin
        //txtObservaciones.Clear;                                               //SS-842-NDR-20110912
        Checklist.EmptyDataSet;
        // Solapa Transacciones
        spObtenerTransitosConvenio.close;
        // Solapa Comprobantes
        spObtenerComprobantesConvenio.Close;
        // Solapa Pagos
        spObtenerPagosConvenio.Close;
        // Solapa Cuentas Corrientes
        ObtenerResumenCuentaCorriente.Close;
        // Solapa Reclamos Recibidos
        spReclamos.Close;
        // Ultimos consumos
        spObtenerUltimosConsumos.Close;
        //Historico Medios de Pago
        ObtenerHistoricoMedioPago.Close;
        //Historico Domicilios
        ObtenerHistoricoDomicilios.Close;
        //Notificacion de emails
        spExplorarEMails.Close;
        //Acciones Externas
        spObtenerAccionesCobranzaSerbanc.Close;
        //indemnizaciones de televias
        spObtenerIndemnizacionesConvenio.Close;
        //HistoricoCartas                       // SS_660_CQU_20121010
        spHistoricoCartasListaAmarilla.Close;   // SS_660_CQU_20121010
    end;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

//INICIA: TASK_060_GLE_20170307
//si el checkbox est� tickeado (Mostrar solo Mov. no Facturados), el filtro de fecha queda habilitado, de lo contrario inhabilitar
procedure TFormInicioConsultaConvenio.chkMuestraConsumosNoFacturadosClick(Sender: TObject);
begin
    try
        if not chkMuestraConsumosNoFacturados.checked then begin
            lblUCFechaDesde.enabled := True;
            dedtUCFechaDesde.enabled := True;

            lblUCFechaHasta.enabled := True;
            dedtUCFechaHasta.enabled := True;
        end
        else begin
            lblUCFechaDesde.enabled := False;
            dedtUCFechaDesde.enabled := False;

            lblUCFechaHasta.enabled := False;
            dedtUCFechaHasta.enabled := False;
        end;
    except
        on e: exception do begin
            MsgBoxErr('Error.', e.Message, Self.Caption, MB_ICONSTOP);
        end;
    end;
end;
//TERMINA: TASK_060_GLE_20170307

procedure TFormInicioConsultaConvenio.chkObservacionesBajaClick(Sender: TObject);
begin
    if (Sender as TCheckBox).Enabled then begin
        if ObtenerConvenioObservacion.Active then ObtenerConvenioObservacion.Close;
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   peNumeroDocumentoButtonClick
Author          :
Date Created    :
Description     :   Busca la persona y obtiene su rut
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.peNumeroDocumentoButtonClick(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
            FUltimaBusqueda := F.UltimaBusqueda;
			peNumeroDocumento.Text := Trim(f.Persona.NumeroDocumento);
            
            if f.Persona.CodigoPersona <> 0 then                                //SS_1408_MCA_20151027
                EstaClienteConMensajeEspecial(DMConnections.BaseCAC, Trim(peNumeroDocumento.Text));           //SS_1408_MCA_20151027

            CargarDatosBasicos;

            if FCodigoPersona <> 0 then
                MostrarMensajeRUTConvenioCuentas(Trim(peNumeroDocumento.Text));
                MostrarMensajeTipoClienteSistema(Trim(peNumeroDocumento.Text));
	   	end;
	end;
    f.Release;
end;

{----------------------------------------------------------------------------------
Function Name   :   peNumeroDocumentoChange
Author          :   
Date Created    :
Description     :   Al cambiar el numero de documento cambian los datos de la cartola
-----------------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.peNumeroDocumentoChange(Sender: TObject);
begin
    cbConvenio.Items.Clear;
    LimpiarDatos;
    lblNombre.Caption        := '';
    lblDomicilio.Caption     := '';
    lblConcesionaria.Caption := '';
    lblTelParticular.Caption := '';
    lblTelComercial.Caption  := '';
    lblTelMovil.Caption      := '';
    lblEmail.Caption         := '';
    lblEditar.Enabled        := false;
    lblVerImagenScan.Enabled := false;
    lblReimprimirNotifTAGsVencidos.Enabled := False;    // SS_960_PDO_20120113
    chk_SuscritoListaBlanca.Enabled     := False;

    lblIntereses.Caption       := '';
    lblPeajeDelPeriodo.Caption := '';
    lblPeajePeriodoAnt.Caption := '';
    lblTotalAPagar.Caption     := '';
    lblDescuentos.Caption      := '';
    lblOtrosConceptos.Caption  := '';
    lblTitTotalAPagar.Caption  := '';
    lblAjusteSencilloActual.Caption    := '';
    lblAjusteSencilloAnterior.Caption  := '';
    pnlEstadoCuenta.Caption            := '';
    lblSaldoConvenio.Caption           := '';
    lblSaldoConvenio.Font.Color        := clBlack;
    lblDeudaComprobantesCuotas.Caption := '';
    lblFechaProximaFacturacion.Caption := '';
    lblfinProximaFacturacion.Caption   := '';
    lblEstadoConvenio.Caption          := '';
    lblRNUT.Caption := '';
    lblUltimoAjusteSencillo.Caption		:= '';		//SS-877-NDR-20101027
    lblDeudaRef.Caption := EmptyStr;
    lblDeudaRef.Font.Color := clBlack;
    lblDeudaRef.Font.Style := lblDeudaRef.Font.Style - [fsBold];
    //deHistoricoDesde.Clear;     // SS_842_PDO_20111212    //TASK_066_GLE_20170409
    //deHistoricoHasta.Clear;     // SS_842_PDO_20111212    //TASK_066_GLE_20170409
    lblEnListaAmarilla.Visible:=False;                  // SS_660_MCO_20130221
    vcbConcesionariasEstacionamiento.ItemIndex := 0;	// SS_1006_PDO_20120712
    deHistListaAccesoDesde.Clear;                       // SS-1006-NDR-20120812
    deHistListaAccesoHasta.Clear;                       // SS-1006-NDR-20120812
    cbbPatentes.Clear;                                  // SS-1006-NDR-20120812
    ObtenerConvenioObservacion.Close;                   // SS_842_CQU_20120913
end;


{-----------------------------------------------------------------------------
Function Name   :   CargarDatosCliente
Author          :   
Date Created    :
Description     :   Carga los datos del cliente
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.CargarDatosCliente;
var AuxColor: TColor;
begin
    spObtenerDatosCliente.Close;
    spObtenerDatosCliente.Parameters.ParamByName('@CodigoCliente').Value := FCodigoPersona;
    spObtenerDatosCliente.Open;
    lblDomicilio.Caption := '';  //Se carga en el OnChange de cbConvenio
    if spObtenerDatosCliente.Eof then begin
        lblNombre.Caption           := '';
        lblTelParticular.Caption    := '';
        lblTelComercial.Caption     := '';
        lblTelMovil.Caption         := '';
        lblEmail.Caption            := '';
        lblEditar.Enabled           := false;
        lblVerImagenScan.Enabled    := false;
        lblReimprimirNotifTAGsVencidos.Enabled := False;    // SS_960_PDO_20120113
        chk_SuscritoListaBlanca.Enabled := False;
    end else begin
        lblNombre.Caption           := Trim(spObtenerDatosCliente.FieldByName('Nombre').AsString);
        lblTelParticular.Caption    := spObtenerDatosCliente.FieldByName('TelefonoParticular').AsString;
        lblTelComercial.Caption     := spObtenerDatosCliente.FieldByName('TelefonoComercial').AsString;
        lblTelMovil.Caption         := spObtenerDatosCliente.FieldByName('TelefonoCelular').AsString;
        lblEmail.Caption            := Trim(spObtenerDatosCliente.FieldByName('EMail').AsString);
      	lblEditar.Enabled           := True;

        lblReimprimirNotifTAGsVencidos.Enabled :=                                                   // SS_960_PDO_20120113
            ExisteAcceso('inicia_notif_tags') and                                                   // SS_960_PDO_20120113
            not spObtenerDatosCliente.FieldByName('InicializarNotificacionesTAGs').AsBoolean and    // SS_960_PDO_20120113
            spObtenerDatosCliente.FieldByName('TieneNotificacionesTAGs').AsBoolean;                 // SS_960_PDO_20120113

        try
            AuxColor        := HexToInt(spObtenerDatosCliente.FieldByName('ColorSemaforo1').AsString);
            if AuxColor < 0 then AuxColor := HexToInt(COLOR_DEFAULT); // SS_690_20100901
            Led1.ColorOn    := AuxColor;
            Led1.Hint       := spObtenerDatosCliente.FieldByName('DescripcionSemaforo1').AsString;

            AuxColor        := HexToInt(spObtenerDatosCliente.FieldByName('ColorCliente').AsString);
            if AuxColor < 0 then AuxColor := HexToInt(COLOR_DEFAULT); // SS_690_20100901
            Led2.ColorOn    := AuxColor;
            Led2.Hint       := spObtenerDatosCliente.FieldByName('Descripcion').AsString;

            AuxColor        := HexToInt(spObtenerDatosCliente.FieldByName('ColorSemaforo3').AsString);
            if AuxColor < 0 then AuxColor := HexToInt(COLOR_DEFAULT); // SS_690_20100901
            Led3.ColorOn    := AuxColor;
            Led3.Hint       := spObtenerDatosCliente.FieldByName('DescripcionSemaforo3').AsString;
        except
            Led1.Hint   := '';
            Led2.Hint   := '';
            Led3.Hint   := '';
        end;
    end;
    spObtenerDatosCliente.Close;
end;
{-----------------------------------------------------------------------------
Function Name   :   CargarParametrosGenerales
Author          :   lcanteros
Date            :   19-Mar-2008
Description     :   Carga Indicadores(Viajero Frecuente, Tipo de Cliente, Infracciones)
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.CargarParametrosGenerales;
var ValorParamGeneral: string;
begin
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'SEMAFORO_1', ValorParamGeneral) then begin
        Label18.Caption := ValorParamGeneral;
    end;
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'SEMAFORO_2', ValorParamGeneral) then begin
        Label23.Caption := ValorParamGeneral;
    end;
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'SEMAFORO_3', ValorParamGeneral) then begin
        Label21.Caption := ValorParamGeneral;
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   cbConvenioChange
Author          :
Date Created    :   14/04/2005
Description     :   Carga todos los detalles del convenio
-----------------------------------------------------------------------------}


procedure TFormInicioConsultaConvenio.cbbMostrarPagosAutomaticosChange(
  Sender: TObject);
begin
    if Assigned(cbConvenio) and (cbConvenio.Items.Count >0) AND (cbConvenio.ItemIndex>=0) then
        BuscarPagosAutomaticosConvenio();
end;

procedure TFormInicioConsultaConvenio.cbbPatentesChange(Sender: TObject);
begin
	BuscarHistoricosListasDeAcceso;
end;

procedure TFormInicioConsultaConvenio.cbConvenioChange(Sender: TObject);
begin
    LimpiarDatos;
    MostrarConvenio(cbConvenio.Value);
{INICIO: TASK_006_JMA_20160420}
    PageControlChange(PageControl);
{TERMINO: TASK_006_JMA_20160420}
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
procedure TFormInicioConsultaConvenio.cbConvenioDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenio.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;
  //INICIO: TASK_012_ECA_20160514
  if Pos( 'Predeterminado', cbConvenio.Items[Index].Caption ) > 0 then
  begin
     CUENTA_COMERCIAL_PREDETERMINADA:= Integer(cbConvenio.Value);
     ItemComboBoxColor(Control, Index, Rect, State, clBlue, true);
  end;
  //FIN: TASK_012_ECA_20160514

end;
//	const
//    	cSQLEsConvenioDeBaja = 'SELECT dbo.EsConvenioDeBaja(%d)';
//    var
//    	CodigoConvenio: Integer;
//        FontColor,
//        BackGroundColor: TColor;
//        FontBold: Boolean;
//begin
//	CodigoConvenio := cbConvenio.Items[Index].Value;
//
//	if SysUtilsCN.QueryGetBooleanValue(DMConnections.BaseCAC, Format(cSQLEsConvenioDeBaja,[CodigoConvenio])) then begin
//
//        BackGroundColor := clWhite;
//        FontColor 		:= clRed;
//	    FontBold 		:= True;
//
//    	if (Control as TVariantComboBox).DroppedDown then begin
//            if (odselected in State) then begin
//	        	BackGroundColor := clHighlight;
//		        FontColor 		:= clWhite;
//            end;
//        end;
//    end
//    else begin
//
//        BackGroundColor := clWhite;
//        FontColor		:= clBlack;
//        FontBold 		:= False;
//
//    	if (Control as TVariantComboBox).DroppedDown then begin
//            if (odselected in State) then begin
//	        	BackGroundColor := clHighlight;
//		        FontColor 		:= clWhite;
//	        end;
//        end;
//    end;
//
//	ItemCBColor(Control, Index, Rect, BackGroundColor, FontColor, FontBold);
//end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------

{-----------------------------------------------------------------------------
Function Name   :   ActualizarDomicilioFacturacion
Author          :
Date Created    :
Description     :   Carga Domicilio Facturaci�n
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.ActualizarDomicilioFacturacion;
resourcestring
	MSG_ERROR                   = 'Error';
	MSG_ERROR_GETTING_ADDRESS   = 'Error obteniendo el domicilio del cliente';
begin
    if cbConvenio.ItemIndex >= 0 then begin
		with spObtenerDomicilioFacturacion, Parameters do begin
			// Mostramos el domicilio de Facturaci�n
	        Close;
    		ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        	try
        		ExecProc;
                lblDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value + ' - ' + ParamByName('@Region').Value;
			except
        		on e: exception do begin
            		MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            	end;
        	end;
    	end;
    end else lblDomicilio.Caption := EmptyStr;
end;

{-----------------------------------------------------------------------------
Function Name   :   dblComprobantesDrawText
Author          :   ddiaz
Date Created    :   02-10-2006
Description     :   Maneja el "linkeado" de las columnas Ver...(Detalle) y Notificar...(Enviar Mail)
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblComprobantesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
resourcestring
    STR_VER         = 'Ver...';
    HEADER_DETALLES = 'Detalles';
    MSG_NO_DISP     = 'No Disponible';
begin
// ************************* Inicio bloque ****************************************
//
//              SS_1193_B_MBE_20140929
//
// ********************************************************************************
//
//    //se usa el Header.Caption porque esta columna no tiene asignado un campo.
//    //SS_740_MBE_20110722
    if Column.Header.Caption = HEADER_DETALLES then  begin
        if	(dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString <> TC_TRASPASO_CREDITO) and                       //SS_1235_MGO_20151113
        	(dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString <> TC_TRASPASO_DEBITO) then                       //SS_1235_MGO_20151113
    	      Text := STR_VER;
    end;                                                                                                                                  //SS_1235_MGO_20151113
//
//    // FIN SS_740_MBE_20110722
//
    if Column.Header.Caption = HEADER_NOTIFICAR then begin
        if FEnviar_NK_Via_Mail then begin //Verifica si el usuario tiene acceso a enviar mails                                            //SS_1235_MGO_20151113
            if ((dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO) or                           //SS_1235_MGO_20151113
               (dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_CREDITO) or                          //SS_1235_MGO_20151113
               (dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_CREDITO_A_COBRO)) then begin         //SS_1235_MGO_20151113
                Column.Font.Color := clBlue;                                                                                              //SS_1235_MGO_20151113
                Text := STR_ENVIO_MAIL;
            end else begin                                                                                                                //SS_1235_MGO_20151113
                Text := '';                                                                                                               //SS_1235_MGO_20151113
            end;                                                                                                                          //SS_1235_MGO_20151113
        end else begin                                                                                                                    //SS_1235_MGO_20151113
            Column.Font.Color := clGray;                                                                                                  //SS_1235_MGO_20151113
            Text := MSG_NO_DISP;                                                                                                          //SS_1235_MGO_20151113
        end;                                                                                                                              //SS_1235_MGO_20151113
//        mousePosicion :=  Mouse.CursorPos;
//        //obtengo los margenes sup e inferior del RECT de la columna
//        if Sender.DataSource.DataSet.RecNo = 1 then begin
//            PosCelda.Top := TxtRect.Top         //superior
//        end;
//        PosCelda.Bottom := TxtRect.Bottom; //ac� va quedando el inferior
//        PosCelda.Left   := Column.Left;
//        PosCelda.Right  := Column.Right;
//        AnchoCelda      := ItemWidth;
    end;
//    // Rev.21 / 23-Febrero-2010 / Nelson Droguett Sierra ------------------------------------------------------------
    if Column.Header.Caption = HEADER_NOTIFICAR_DET then begin
        if FEnviar_NK_Via_Mail then begin //Verifica si el usuario tiene acceso a enviar mails                                            //SS_1235_MGO_20151113
            if (dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO) AND                           //SS_1235_MGO_20151113
              (dblComprobantes.DataSource.DataSet.FieldByName('TieneConceptosPeaje').AsBoolean = True) then begin                         //SS_1235_MGO_20151113
                Column.Font.Color := clBlue;                                                                                              //SS_1235_MGO_20151113
                Text := STR_ENVIO_MAIL_DET;
            end else begin                                                                                                                //SS_1235_MGO_20151113
                Text := '';                                                                                                               //SS_1235_MGO_20151113
            end;                                                                                                                          //SS_1235_MGO_20151113
        end else begin                                                                                                                    //SS_1235_MGO_20151113
            Column.Font.Color := clGray;                                                                                                  //SS_1235_MGO_20151113
            Text := MSG_NO_DISP;                                                                                                          //SS_1235_MGO_20151113
        end;                                                                                                                              //SS_1235_MGO_20151113
//        mousePosicion :=  Mouse.CursorPos;
//        //obtengo los margenes sup e inferior del RECT de la columna
//        if Sender.DataSource.DataSet.RecNo = 1 then begin
//            PosCelda.Top := TxtRect.Top         //superior
//        end;
//        PosCelda.Bottom := TxtRect.Bottom; //ac� va quedando el inferior
//        PosCelda.Left   := Column.Left;
//        PosCelda.Right  := Column.Right;
//        AnchoCelda      := ItemWidth;
    end;
//    //---------------------------------------------------------------------------------------------------------------
//
// ************************* Fin bloque ****************************************
//
//              SS_1193_B_MBE_20140929
//
// ********************************************************************************
end;


{-----------------------------------------------------------------------------
Function Name   :   dblComprobantesLinkClick
Author          :
Date Created    :
Description     :
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblComprobantesLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
resourcestring
    MSG_ERROR_INI_REPO      = 'Error al intentar inicializar el Reporte. Documento Tipo %s N�mero %s';
    MSG_REQ_SEND_MAIL_OK    = 'Se gener� solicitud de env�o de mail al cliente con documento adjunto %s %s';
    MSG_REQ_SEND_MAIL_ERR   = 'Ha ocurrido un error en la generaci�n de solicitud de env�o de mail al Cliente.';
    //MSG_EXISTE_MAIL         = 'Existe un e-mail pendiente de env�o para el n�mero de comprobante %d. Desea solicitar un nuevo env�o?';                                 //PAR00133-FASE2-ALA-20110704
    MSG_CONFIRMA_ENVIO      = 'Desea solicitar el env�o por e-mail del comprobante n�mero %d ?';
    MSG_EMAIL_TITLE         = 'Env�o de Documentos por e-mail';
    MSG_SIN_MAIL            = 'El Convenio %s no posse un e-mail establecido. Desea indicar uno ahora para enviar este e-mail?';
    MSG_NEW_MAIL            = 'Ingreso de direcci�n de e-mail';
    MSG_NEW_MAIL_INPUT      = 'Introduzca una direcci�n de e-mail: ';
    MSG_MAIL                = 'e-Mail:';
    MSG_INSERT_ERROR        = 'La solicitud de env�o de e-mail para el comprobante n�mero %d para %s no se ha podido realizar. Detalle: %s';
    MSG_MAIL_ERROR          = 'Ha ocurrido un error al registrar un e-mail para ser enviado';
    MSG_MAIL_REGISTERED     = 'El e-mail solicitado se ha registrado y ser� enviado a la brevedad posible';
    MSG_CONFIRMA_ENVIO_DET  = 'Desea solicitar el env�o por e-mail del detalle de tr�nsito del comprobante n�mero %d ?';
    MSG_INSERT_ERROR_DET    = 'La solicitud de env�o de e-mail para el detalle de transitos del comprobante n�mero %d para %s no se ha podido realizar. Detalle: %s';
    //Rev.2 / 28-Abril-2010 / Nelson Droguett Sierra
    //MSG_EXISTE_MAIL_DET     = 'Existe un e-mail de detalle pendiente de env�o para el n�mero de comprobante %d. Desea solicitar un nuevo env�o de detalle?';           //PAR00133-FASE2-ALA-20110704
    MSG_EXISTE_MAIL_DET     = 'Ya existe este detalle de comprobante, para ser enviado por e-mail.';
    MSG_EXISTE_MAIL         = 'Ya existe este comprobante, para ser enviado por e-mail.';
    MSG_SIN_PERMISOS        = 'Usuario no posee permisos para ejecutar esta opci�n';                       // SS_1193_B_MBE_20140929
var
    f                   : TFormComprobantesEmitidos;
    NumeroComprobante   : Int64;
    DescriErr, TipoComp : string;
    eMailAddr           : string;
	ImpresoraFiscal     : Integer;
    InputEmailCancelado : Boolean;
    //Rev.2 / 28-Abril-2010 / Nelson Droguett Sierra
    CodigoConvenio		: Integer;
begin
    if (Column.Header.Caption = HEADER_NOTIFICAR) and
      ((dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO) or
      (dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_CREDITO) or
      (dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_CREDITO_A_COBRO)) then begin

        if not ExisteAcceso('Enviar_NK_Via_Mail') then begin                            // SS_1193_B_MBE_20140929
            MsgBox(MSG_SIN_PERMISOS, Caption, MB_ICONWARNING);                          // SS_1193_B_MBE_20140929
            Exit;     //Verifica si el usuario tiene acceso a enviar mails
        end;                                                                            // SS_1193_B_MBE_20140929

        DescriErr           := EmptyStr;
        TipoComp            := dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString;
        NumeroComprobante   := dblComprobantes.DataSource.DataSet.FieldByName('NumeroComprobante').AsInteger;
        If Not TryStrtoInt(dblComprobantes.DataSource.DataSet.FieldByName('CodigoImpresoraFiscal').AsString,ImpresoraFiscal) Then ImpresoraFiscal := -1;
        eMailAddr := lblEmail.Caption;

        if MsgBox(Format(MSG_CONFIRMA_ENVIO, [NumeroComprobante]), MSG_EMAIL_TITLE, MB_ICONQUESTION or MB_YESNO) = IDYES then begin

            // Tenemos un e-mail en el convenio?
            if eMailAddr = EmptyStr then begin
                if MsgBox(Format(MSG_SIN_MAIL, [cbConvenio.Text]), MSG_EMAIL_TITLE, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
                    InputEmailCancelado := False;

                    while (not IsValidEmail(eMailAddr)) and (not InputEmailCancelado) do begin
                        eMailAddr := EmptyStr;
                        InputEmailCancelado := not InputQuery(MSG_NEW_MAIL,MSG_NEW_MAIL_INPUT,eMailAddr);
                    end;

                    if InputEmailCancelado then
                        eMailAddr := EmptyStr
                    else begin
                        if IsValidEmail(eMailAddr) then begin
                            GuardarMailPersona( fCodigoPersona , eMailAddr);
                            lblEmail.Caption := eMailAddr;
                        end
                        else eMailAddr := EmptyStr;
                      end;
                end;
            end;

            if eMailAddr <> EmptyStr then begin
                if not ExisteMailPendienteComprobante(DMConnections.BaseCAC, TipoComp, NumeroComprobante, ImpresoraFiscal) then  begin                      //PAR00133-FASE2-ALA-20110704
                        //if MsgBox(Format(MSG_EXISTE_MAIL, [NumeroComprobante]), MSG_EMAIL_TITLE, MB_ICONQUESTION or MB_YESNO) = IDNO then Exit;           //PAR00133-FASE2-ALA-20110704
                    // Listo, creo el pedido del nuevo mail
                    if not InsertarEMailComprobante(DMConnections.BaseCAC, TipoComp, NumeroComprobante, ImpresoraFiscal, eMailAddr, UsuarioSistema,cbConvenio.value,DescriErr) then begin
                        MsgBoxErr(MSG_MAIL_ERROR, Format(MSG_INSERT_ERROR, [NumeroComprobante, eMailAddr, DescriErr]), Caption, MB_ICONERROR);
                    end else MsgBox(MSG_MAIL_REGISTERED);
                end                                                                                                                                          //PAR00133-FASE2-ALA-20110704
                else                                                                                                                                         //PAR00133-FASE2-ALA-20110704
                    MsgBox(MSG_EXISTE_MAIL, MSG_EMAIL_TITLE, MB_ICONWARNING or MB_OK);                                                                       //PAR00133-FASE2-ALA-20110704
            end;
        end;

    end
    else if Column.Header.Caption = HEADER_DETALLES then begin
    	// SS_740_MBE_20110722
        if	(spObtenerComprobantesConvenio.FieldByName('TipoComprobante').AsString <> TC_TRASPASO_CREDITO) and
        	(spObtenerComprobantesConvenio.FieldByName('TipoComprobante').AsString <> TC_TRASPASO_DEBITO) then begin

            if FindFormOrCreate(TFormComprobantesEmitidos, f) then begin
                f.Show
            end else begin
                NumeroComprobante := StrToInt(copy(spObtenerComprobantesConvenio.FieldByName('NumeroComprobante').AsString,
                  pos('-', spObtenerComprobantesConvenio.FieldByName('NumeroComprobante').AsString) + 1, 20));
                if not f.Inicializar(0, spObtenerComprobantesConvenio.FieldByName('TipoComprobante').AsString,
                    NumeroComprobante, spObtenerComprobantesConvenio.FieldByName('NumeroComprobanteFiscal').AsInteger,
                    spObtenerComprobantesConvenio.FieldByName('CodigoImpresoraFiscal').AsInteger) then begin
                    f.Release;
                end;
                //INICIO: 	20170306 TASK_104_CFU_20170213
                f.peNumeroDocumento.Text		:= peNumeroDocumento.Text;
                f.cbTipoComprobante.Value		:= spObtenerComprobantesConvenio.FieldByName('TipoComprobante').AsString;
                f.BTNBuscarComprobanteClick(dblComprobantes);
                //TERMINO: 	20170306 TASK_104_CFU_20170213
            end;
    	end;
        //FIN SS_740_MBE_20110722
    end
    else if (Column.Header.Caption = HEADER_NOTIFICAR_DET)  and
      (dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO) and
      (dblComprobantes.DataSource.DataSet.FieldByName('TieneConceptosPeaje').AsBoolean) then begin
        if not ExisteAcceso('Enviar_NK_Via_Mail') then begin                            // SS_1193_B_MBE_20140929
            MsgBox(MSG_SIN_PERMISOS, Caption, MB_ICONWARNING);                          // SS_1193_B_MBE_20140929
            Exit;     //Verifica si el usuario tiene acceso a enviar mails
        end;                                                                            // SS_1193_B_MBE_20140929

        DescriErr := EmptyStr;
        //Rev.2 / 28-Abril-2010 / Nelson Droguett Sierra
        CodigoConvenio := cbConvenio.Value;
        TipoComp := dblComprobantes.DataSource.DataSet.FieldByName('TipoComprobante').AsString;
        NumeroComprobante := dblComprobantes.DataSource.DataSet.FieldByName('NumeroComprobante').AsInteger;
        eMailAddr := lblEmail.Caption;

        if MsgBox(Format(MSG_CONFIRMA_ENVIO_DET, [NumeroComprobante]), MSG_EMAIL_TITLE, MB_ICONQUESTION or MB_YESNO) = IDYES then begin

            // Tenemos un e-mail en el convenio?
            if eMailAddr = EmptyStr then begin
                if MsgBox(Format(MSG_SIN_MAIL, [cbConvenio.Text]), MSG_EMAIL_TITLE, MB_ICONQUESTION or MB_YESNO) = IDYES then begin
                    InputEmailCancelado := False;

                    while (not IsValidEmail(eMailAddr)) and (not InputEmailCancelado) do begin
                        eMailAddr           := EmptyStr;
                        InputEmailCancelado := not InputQuery(MSG_NEW_MAIL,MSG_NEW_MAIL_INPUT,eMailAddr);
                    end;

                    if InputEmailCancelado then
                        eMailAddr := EmptyStr
                    else begin
                        if IsValidEmail(eMailAddr) then begin
                            GuardarMailPersona( fCodigoPersona , eMailAddr);
                            lblEmail.Caption := eMailAddr;
                        end
                        else eMailAddr := EmptyStr;
                      end;
                end;
            end;
            if eMailAddr <> EmptyStr then begin
				//Rev.2 / 28-Abril-2010 / Nelson Droguett Sierra
                if not ExisteConvenioAEnviarDetalle(DMConnections.BaseCAC,                                                                               //PAR00133-FASE2-ALA-20110704
                								CodigoConvenio,
                                                TipoComp,
                                                NumeroComprobante) then begin                                                                        //PAR00133-FASE2-ALA-20110704
                    //if MsgBox(Format(MSG_EXISTE_MAIL_DET, [NumeroComprobante]), MSG_EMAIL_TITLE, MB_ICONQUESTION or MB_YESNO) = IDNO then Exit;    //PAR00133-FASE2-ALA-20110704
                    // Listo, creo el pedido del nuevo mail
                    if not InsertarConvenioAEnviarDetalle(	DMConnections.BaseCAC,
                                                            CodigoConvenio,
                                                            TipoComp,
                                                            NumeroComprobante,
                                                            UsuarioSistema,
                                                            DescriErr) then begin
                        MsgBoxErr(MSG_MAIL_ERROR, Format(MSG_INSERT_ERROR, [NumeroComprobante, eMailAddr, DescriErr]), Caption, MB_ICONERROR);
                    end else begin
                        MsgBox(MSG_MAIL_REGISTERED);
                    end;
                end                                                                                                                                          //PAR00133-FASE2-ALA-20110704
                else                                                                                                                                         //PAR00133-FASE2-ALA-20110704
                    MsgBox(MSG_EXISTE_MAIL_DET, MSG_EMAIL_TITLE, MB_ICONWARNING or MB_OK);                                                                   //PAR00133-FASE2-ALA-20110704
            end;
			//FIN Rev.2 / 28-Abril-2010 / Nelson Droguett Sierra
        end;
    end;
    // --------------------------------------------------------------------------------
end;


{-----------------------------------------------------------------------------
Function Name   :   dblComprobantesContextPopup
Author          :
Date Created    :   
Description     :
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblComprobantesContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
	if not spObtenerComprobantesConvenio.IsEmpty then Handled :=
      MostrarMenuContextualComprobante(dblComprobantes.ClientToScreen(MousePos),
      spObtenerComprobantesConvenio['TipoComprobante'],
      spObtenerComprobantesConvenio['NumeroComprobante']);
end;

{-----------------------------------------------------------------------------
Function Name   :   OSChanged
Author          :
Date Created    :   
Description     :   Refresca Stored Procedure spReclamos
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.OSChanged;
begin
    if spReclamos.Active then spReclamos.Requery;
end;

{-----------------------------------------------------------------------------
Function Name   :   dblCuentasContextPopup
Author          :
Date Created    :   
Description     :   asigna el pop menu a la grilla cuentas
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblCuentasContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
	if not spObtenerCuentasCartola.IsEmpty then begin
        Handled := MostrarMenuContextualCuenta
                        (
                            dblCuentas.ClientToScreen(MousePos),
                            spObtenerCuentasCartola['CodigoConvenio'],
                            spObtenerCuentasCartola['IndiceVehiculo'],
                            Self,
                            iif(spObtenerCuentasCartola['Etiqueta']=null,0,spObtenerCuentasCartola['Etiqueta'])  //TASK_053_ECA_20160708
                        );
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   popChecklistPopup
Author          :
Date Created    :
Description     :   Visualiza opciones
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.popChecklistPopup(Sender: TObject);
begin
    mnuAgregarChecklist.Visible := cbConvenio.ItemIndex >= 0;
    mnuEditarCheckList.Visible  := not Checklist.IsEmpty;
    mnuQuitarChecklist.Visible  := mnuEditarCheckList.Visible;
end;

{-----------------------------------------------------------------------------
Function Name   :   mnuAgregarChecklistClick
Author          :
Date Created    :   
Description     :   Levanta Form Item Independiente
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.mnuAgregarChecklistClick(Sender: TObject);
var
    f: TFormChecklist;
begin
    Application.CreateForm(TFormChecklist, f);
    if f.Inicializar(cbConvenio.Value) and (f.ShowModal = mrOk) then begin
        spActualizarConvenioCheckList.Parameters.ParamByName('@CodigoConvenio').Value   := cbConvenio.Value;
        spActualizarConvenioCheckList.Parameters.ParamByName('@CodigoChecklist').Value  := f.cbChecklist.Value;
        spActualizarConvenioCheckList.Parameters.ParamByName('@Vencimiento').Value      := iif(f.txtVencimiento.Date = NULLDATE, NULL, f.txtVencimiento.Date);
        spActualizarConvenioCheckList.Parameters.ParamByName('@Observaciones').Value    := iif(Trim(f.txtObservaciones.Text) = '', NULL, Trim(f.txtObservaciones.Text));
        spActualizarConvenioCheckList.ExecProc;
        Checklist.Append;
        Checklist.FieldByName('CodigoChecklist').AsString   := f.cbChecklist.Value;
        Checklist.FieldByName('Descripcion').AsString       := f.cbChecklist.Items[f.cbChecklist.ItemIndex].Caption;
        Checklist.FieldByName('Vencimiento').Value          := spActualizarConvenioCheckList.Parameters.ParamByName('@Vencimiento').Value;
        Checklist.FieldByName('Observaciones').Value        := spActualizarConvenioCheckList.Parameters.ParamByName('@Observaciones').Value;
        CheckList.Post;
    end;
    f.Release;
end;

{-----------------------------------------------------------------------------
Function Name   :   mnuQuitarChecklistClick
Author          :
Date Created    :
Description     :   Quitar Item Independiente
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.mnuQuitarChecklistClick(Sender: TObject);
resourcestring
    MSG_CONF_DELETE = '�Est� seguro de eliminar este �tem?';
    MSG_DELETE_ITEM = 'Eliminar item pendiente';
begin
    if MsgBox(MSG_CONF_DELETE, MSG_DELETE_ITEM,
      MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) <> IDYES then Exit;
    QueryExecute(DMConnections.BaseCAC, Format(
      'DELETE FROM ConvenioCheckList WHERE CodigoConvenio = %d AND CodigoCheckList = %d',
      [Integer(cbConvenio.Value), CheckList.FieldByName('CodigoCheckList').AsInteger]));
	CheckList.Delete;
end;

{-----------------------------------------------------------------------------
Function Name   :   mnuEditarCheckListClick
Author          :
Date Created    :   
Description     :   Editar Item Independiente
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.mnuEditarCheckListClick(Sender: TObject);
var
    f: TFormChecklist;
begin
    Application.CreateForm(TFormChecklist, f);
    if f.Inicializar(0) then begin
        f.cbChecklist.Value     := CheckList.FieldByName('CodigoCheckList').AsInteger;
        f.cbChecklist.Enabled   := False;
        f.txtVencimiento.Date   := iif(Checklist.FieldByName('Vencimiento').IsNull, NULLDATE, Checklist.FieldByName('Vencimiento').AsDateTime);
        f.txtObservaciones.Text := Checklist.FieldByName('Observaciones').AsString;
        f.ActiveControl         := f.txtVencimiento;
        if (f.ShowModal = mrOk) then begin
            spActualizarConvenioCheckList.Parameters.ParamByName('@CodigoConvenio').Value   := cbConvenio.Value;
            spActualizarConvenioCheckList.Parameters.ParamByName('@CodigoChecklist').Value  := f.cbChecklist.Value;
            spActualizarConvenioCheckList.Parameters.ParamByName('@Vencimiento').Value      := iif(f.txtVencimiento.Date = NULLDATE, NULL, f.txtVencimiento.Date);
            spActualizarConvenioCheckList.Parameters.ParamByName('@Observaciones').Value    := iif(Trim(f.txtObservaciones.Text) = '', NULL, Trim(f.txtObservaciones.Text));
			spActualizarConvenioCheckList.ExecProc;
            Checklist.Edit;
            Checklist.FieldByName('Vencimiento').Value := spActualizarConvenioCheckList.Parameters.ParamByName('@Vencimiento').Value;
            Checklist.FieldByName('Observaciones').Value := spActualizarConvenioCheckList.Parameters.ParamByName('@Observaciones').Value;
            CheckList.Post;
        end;
    end;
    f.Release;
end;

{-----------------------------------------------------------------------------
Function Name   :   dblChecklistDblClick
Author          :
Date Created    :   
Description     :   Activa opci�n que edita Item Independiente
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblChecklistDblClick(Sender: TObject);
begin
    if not Checklist.IsEmpty then mnuEditarCheckList.Click;
end;

{-----------------------------------------------------------------------------
Function Name   :   dblCuentasDrawText
Author          :
Date Created    :   
Description     :   Dibuja indicadores tipo Led en colores
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblCuentasDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp, Blanco: TBitMap;
begin

    // Dibujo los led de las listas
    with Sender do begin
        if (Column.FieldName = 'SetGreenList') then begin
            DefaultDraw := False;
            Text        := '';
            canvas.FillRect(rect);
            bmp         := TBitMap.Create;
            Blanco      := TBitMap.Create;
            if (spObtenerCuentasCartola.FieldByName('SetGreenList').AsBoolean ) then begin
				Imagenes.GetBitmap(1, Bmp);
            end else begin
                Imagenes.GetBitmap(0, Bmp);
            end;

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo balnco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;

        if (Column.FieldName = 'SetBlackList') then begin
            DefaultDraw := False;
            Text        := '';
            canvas.FillRect(rect);
            bmp         := TBitMap.Create;
            Blanco      := TBitMap.Create;

            if (spObtenerCuentasCartola.FieldByName('SetBlackList').AsBoolean ) then begin
                //esta en lista negra
          	    Imagenes.GetBitmap(2, Bmp);
            end else begin
                //no esta en lista negra
                Imagenes.GetBitmap(0, Bmp);
            end;

             Imagenes.GetBitmap(5, Blanco);
             Blanco.Width := Rect.Right - Rect.Left;
             Canvas.Draw(Rect.Left, rect.Top, Blanco);
             Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp);
             Bmp.Free;
             Blanco.Free;
        end;

        if (Column.FieldName = 'SetYellowList') then begin
            DefaultDraw := False;
            Text        := '';
            canvas.FillRect(rect);
            bmp         := TBitMap.Create;
            Blanco      := TBitMap.Create;
            if (spObtenerCuentasCartola.FieldByName('SetYellowList').AsBoolean ) then begin
          	    Imagenes.GetBitmap(3, Bmp);
            end else begin
				Imagenes.GetBitmap(0, Bmp);
            end;

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width    := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo balnco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;

        if (Column.FieldName = 'SetGrayList') then begin
            DefaultDraw := False;
            Text        := '';
            canvas.FillRect(rect);
            bmp         := TBitMap.Create;
            Blanco      := TBitMap.Create;
            if (spObtenerCuentasCartola.FieldByName('SetGrayList').AsBoolean ) then begin
          	    Imagenes.GetBitmap(4, Bmp);
            end else begin
                Imagenes.GetBitmap(0, Bmp);
            end;

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width    := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp);
            Bmp.Free;
            Blanco.Free;
        end;

        if (Column.FieldName = 'ExceptionHandling') then begin
            DefaultDraw := False;
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (spObtenerCuentasCartola.FieldByName('ExceptionHandling').AsBoolean ) then begin
				Imagenes.GetBitmap(10, Bmp);
            end else begin
                Imagenes.GetBitmap(0, Bmp);
            end;

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrega un fondo blanco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;

        if (Column.FieldName = 'MMIContactOperator') then begin
            DefaultDraw := False;
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (spObtenerCuentasCartola.FieldByName('MMIContactOperator').AsBoolean ) then begin
				Imagenes.GetBitmap(10, Bmp);
            end else begin
                Imagenes.GetBitmap(0, Bmp);
            end;

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo blanco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;
        // begin SS_660_MCA_20130912
        if (Column.FieldName = 'MMINotOk') then begin
            DefaultDraw := False;
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            Blanco := TBitMap.Create;
            if (spObtenerCuentasCartola.FieldByName('MMINotOk').AsBoolean ) then begin
				        Imagenes.GetBitmap(3, Bmp);
            end else begin
                Imagenes.GetBitmap(0, Bmp);
            end;

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo blanco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;
        // end SS_660_MCA_20130912
        // Si es un vehiculo dado de baja, el televia puede estar asociada a otra cuenta, por eso no muestro su estado
        if (Column.FieldName = 'EstadosConservacionTags') and
            (not spObtenerCuentasCartola.FieldByName('FechaBajaCuenta').IsNull) then begin
                Text := '';
        end;
    end;

    // Si es un vehiculo dado de baja lo pinto de azul (igual que en Modif Convenio)
    if (not spObtenerCuentasCartola.FieldByName('FechaBajaCuenta').IsNull) then begin
        if odSelected in State then begin
            Sender.Canvas.Font.Color := clYellow;
		end else begin
            Sender.Canvas.Font.Color := clBlue;
        end;
    end else begin
        if odSelected in State then begin
            Sender.Canvas.Font.Color := clWindow;
        end else begin
            Sender.Canvas.Font.Color := clWindowText;
        end;
    end;

    //INICIO: TASK_009_ECA_20160506
    //Rev.3 / 05-Mayo-2010 / Nelson Droguett Sierra
    //if Column.FieldName='HabilitadoAMB' then begin                                                                                                //SS-1006-NDR-20111105
    //  lnCheck.Draw(dblCuentas.Canvas, Rect.Left + 3, Rect.Top + 1, IfThen(spObtenerCuentasCartola.FieldByName('HabilitadoAMB').AsBoolean,1,0));   //SS-1006-NDR-20111105
    with Sender do begin

//    	if Column.FieldName='AdheridoPA' then begin
//                                                                                                           //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
//      DefaultDraw := False;
//	        Text        := '';
//
//	        Canvas.FillRect(Rect);
//            try
//	            bmp := TBitMap.Create;
//                if spObtenerCuentasCartola.FieldByName('AdheridoPA').AsBoolean then  begin
//	                lnCheck.GetBitmap(1, bmp);
//                end
//                else begin
//    	            lnCheck.GetBitmap(0, bmp);
//                end;
//
//				Canvas.Draw(((Rect.Right - Rect.Left) div 2) - bmp.Width + Rect.Left, Rect.Top + 1, bmp);
//            finally
//                bmp.Free;
//            end;
//
////      lnCheck.Draw(dblCuentas.Canvas, Rect.Left + 3, Rect.Top + 1, IfThen(spObtenerCuentasCartola.FieldByName('AdheridoPA').AsBoolean,1,0));        //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
////      DefaultDraw := False;
////      ItemWidth   := lnCheck.Width + 2;
//	    end;

        if Column.FieldName='SuscritoListaBlanca' then
        begin
            DefaultDraw := False;
	        Text        := '';

	        Canvas.FillRect(Rect);
            try
	            bmp := TBitMap.Create;
                if spObtenerCuentasCartola.FieldByName('SuscritoListaBlanca').AsBoolean then
                begin
	                lnCheck.GetBitmap(1, bmp);
                end
                else
                begin
    	            lnCheck.GetBitmap(0, bmp);
                end;
				Canvas.Draw(((Rect.Right - Rect.Left) div 2) - bmp.Width + Rect.Left, Rect.Top + 1, bmp);
            finally
                bmp.Free;
            end;
	    end;
        //INICIO: TASK_034_ECA_20160611
        if Column.FieldName='HabilitadoListaBlanca' then
        begin
            DefaultDraw := False;
	        Text        := '';

	        Canvas.FillRect(Rect);
            try
	            bmp := TBitMap.Create;
                if spObtenerCuentasCartola.FieldByName('HabilitadoListaBlanca').AsBoolean then
                begin
	                lnCheck.GetBitmap(1, bmp);
                end
                else
                begin
    	            lnCheck.GetBitmap(0, bmp);
                end;
				Canvas.Draw(((Rect.Right - Rect.Left) div 2) - bmp.Width + Rect.Left, Rect.Top + 1, bmp);
            finally
                bmp.Free;
            end;
	    end;
        //FIN: TASK_034_ECA_20160611
    end;
    //FinRev.3
//    //SS_1006_MBE_20120731
//    with Sender do begin
//    //if Column = dblCuentas.Columns[19] then begin // SS_1151_CQU_20140722
//    if Column.FieldName='ConsultaPak' then begin    // SS_1151_CQU_20140722
//        DefaultDraw := False;
//        Text := '';
//
//            Canvas.FillRect(Rect);
//            try
//	            bmp := TBitMap.Create;
//
//                if spObtenerCuentasCartola.FieldByName('ConsultaPak').AsBoolean then  begin
//	                lnCheck.GetBitmap(2, bmp);
//                end
//                else begin
//	                lnCheck.GetBitmap(0, bmp);
//                end;
//
//                bmp.TransparentColor := clYellow;
//                bmp.Transparent := True;
//
//				Canvas.Draw(((Rect.Right - Rect.Left) div 2) - bmp.Width + Rect.Left, Rect.Top + 1, bmp);
//            finally
//                bmp.Free;
//            end;
//                {
//        ilCamara.Draw(dblCuentas.Canvas, Rect.Left + 3, Rect.Top + 1, IfThen(spObtenerCuentasCartola.FieldByName('ConsultaPak').AsBoolean,2,0)); //  SS_1006_1015_MCO_20120927
////        ilCamara.Draw(dblCuentas.Canvas, Rect.Left + 3, Rect.Top + 1, IfThen(spObtenerCuentasCartola.FieldByName('AdheridoPA').AsBoolean,2,0));   SS_1006_1015_MCO_20120927
//        ItemWidth := ilCamara.Width + 2;
//        }
//	    end;
//    end;

    //fin SS_1006_MBE_20120731
    //FIN:TASK_009_ECA_20160506
end;

{------------------------------------------------------------------
                dblCuentasLinkClick

Author      : mbecerra
Date        : 01-Agosto-2012
Description :   (Ref SS 1006)
                Implementa el env�o de consulta por una patente o Telev�a
                en el Web Service de PA

                Inicio SS_1006_MBE_20120731
----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblCuentasLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
resourcestring
//    PARAM_URL_WEB_SERVICE       = 'URL_WEB_SERVICE_PAK';                //par�metro general que contiene la URL del Web Service de PA
//    PARAM_OWNER_WEB_SERVICE     = 'CODE_OWNER_LISTA_ACCESO';            //par�metro general que contiene el owner del Web Service de PA';
//    PARAM_USER_WEB_SERVICE      = 'USERNAME_LISTA_ACCESO';              //par�metro general que contiene el usuario del Web Service de PA';
//    PARAM_PASSWORD_WEB_SERVICE  = 'USERPASSWORD_LISTA_ACCESO';          //par�metro general que contiene la clave del Web Service de PA';
//
//    MSG_PREGUNTA_POR_CONSULTA   = 'Se enviar� un mensaje a Parque Arauco, consultando si el telev�a est� en Lista de Acceso. �Desea continuar?';
//    MSG_ERROR_URL               = 'No se pudo obtener el par�metro general: %s';
//    MSG_ERROR_WEB_SERVICE       = 'Error al invocar el Web Service de parque Areauco';
//    MSG_TAG_ENCONTRADO          = 'Telev�a %s encontrado y activo en PA';
//    MSG_TAG_NO_ENCONTRADO       = 'Telev�a %s no encontrado en PA. %s';
//    MSG_TAG_ESTA_DE_BAJA        = 'Telev�a %s encontrado en PA pero est� de BAJA. Desde el "%s",  Motivo: %s';


    IP_TCPSERVER_LISTA_ACCESO = 'IP_TCPSERVER_LISTA_ACCESO';
    PORT_TCPSERVER_LISTA_ACCESO = 'PORT_TCPSERVER_LISTA_ACCESO';

    MSG_ERROR_PARAMETRO_NO_EXISTE  = 'No se pudo obtener el par�metro general: %s';
    MSG_ERROR_CONEXION_TCPSERVER = 'No se pudo conectar con el servicio de Env�o de Lista de Acceso en la direcci�n %s:%d. ' + #13#10 + 'Error: %s.' + #13#10 + #13#10 + 'Contacte con Sistemas para informar del error';

    MSG_CONSULTA_TITULO = 'Consulta PAK';
    MSG_CONSULTA_IMG_REF    =   'Imagen de Referencia';             // SS_1214_CQU_20140917
    MSG_NO_EXISTE_IMG_REF   =   'No Existe Imagen de Referencia';   // SS_1214_CQU_20140917

    MensajeTCPServer = '001%s*%s*%s*';
var
    ContractSerialNumber        : int64;
    FechaDeLaBajaString,
    Etiqueta,
    ManufacturerID,
//    URLWebService,
//    UserWebService,
//    PasswordWebService,
//    OwnerWebService             : string;
//    ConsultaPorTelevia          : AccessListWSSoap;
//    RespuestaConsultaPorTelevia : GetListOfVehicleActionListResponseDTOF;

    IPEquipo,
    IPServicioListaAcceso,
    ErrorSocket,
    RespuestaTCPServer          : string;
    ConexionTCP                 : TIdTCPClient;
    PuertoServicioListaAcceso,
    CodigoRespuestaTCPServer,
    Estilo                      : Integer;

begin
//	if (Column.Position = 19) and (spObtenerCuentasCartola.FieldByName('AdheridoPA').AsBoolean) then try --SS_1006_1015_MCO_20120927
    //if (Column.Position = 19) and (spObtenerCuentasCartola.FieldByName('ConsultaPak').AsBoolean) then try             // SS_1214_CQU_20140917 // --SS_1006_1015_MCO_20120927
    if (Column.FieldName = 'ConsultaPak') and (spObtenerCuentasCartola.FieldByName('ConsultaPak').AsBoolean) then try   // SS_1214_CQU_20140917
    	try
        	TPanelMensajesForm.MuestraMensaje('Cargando Par�metros ...', True);

            IPEquipo := GETIPAddress;
//        	if not GetIPFromHost(EmptyStr, IPEquipo, ErrorSocket) then begin
//            	raise Exception.Create(ErrorSocket);
//            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, IP_TCPSERVER_LISTA_ACCESO, IPServicioListaAcceso) then begin
                raise Exception.Create(Format(MSG_ERROR_PARAMETRO_NO_EXISTE, [IP_TCPSERVER_LISTA_ACCESO]));
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, PORT_TCPSERVER_LISTA_ACCESO, PuertoServicioListaAcceso) then begin
                raise Exception.Create(Format(MSG_ERROR_PARAMETRO_NO_EXISTE, [PORT_TCPSERVER_LISTA_ACCESO]));
            end;

            TPanelMensajesForm.MuestraMensaje('Estableciendo conexi�n con el servicio de Consulta ...');
            ConexionTCP := TIdTCPClient.Create(Nil);
            with ConexionTCP do try
                Host           := IPServicioListaAcceso;
                Port           := PuertoServicioListaAcceso;
                BoundIP        := IPEquipo;
                ConnectTimeout := 2000;
                IPVersion      := Id_IPv4;
                Connect;

	            TPanelMensajesForm.MuestraMensaje('Conectado. Efectuando consulta. Espere por Favor ...');

                ContractSerialNumber := Trunc(spObtenerCuentasCartola.FieldByName('ContractSerialNumber').AsCurrency);
                ManufacturerID       := spObtenerCuentasCartola.FieldByName('ManufacturerID').AsString;
                Etiqueta             := spObtenerCuentasCartola.FieldByName('Etiqueta').AsString;


                ConexionTCP.IOHandler.WriteLn(Format(MensajeTCPServer, [CurrToStr(ContractSerialNumber), ManufacturerID, Etiqueta]));
            	RespuestaTCPServer := ConexionTCP.IOHandler.ReadLn;

                CodigoRespuestaTCPServer := StrToInt(Copy(RespuestaTCPServer, 1, 3));

                case CodigoRespuestaTCPServer of
                	0: begin    // TAG Encontrado
                    	Estilo := MB_ICONINFORMATION;
                    end;
                	1: begin    // TAG Encontrado pero de Baja
                    	Estilo := MB_ICONEXCLAMATION;
                    end;
                    2, 3: begin  // Errores
                    	Estilo := MB_ICONERROR;
                    end;
                end;

                TPanelMensajesForm.OcultaPanel;
               	ShowMsgBoxCN(MSG_CONSULTA_TITULO, Copy(RespuestaTCPServer, 5, Length(RespuestaTCPServer) - 4), Estilo, Self);
            except
            	on e: Exception do begin
                	raise Exception.Create(Format(MSG_ERROR_CONEXION_TCPSERVER, [IPServicioListaAcceso, PuertoServicioListaAcceso, e.Message]));
                end;
            end;
        except
        	on e: Exception do begin
            	TPanelMensajesForm.OcultaPanel;
            	ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
       	TPanelMensajesForm.OcultaPanel;
    	if Assigned(ConexionTCP) then begin
        	if ConexionTCP.Connected then ConexionTCP.Disconnect;
        	FreeAndNil(ConexionTCP);
        end;
    end;

    if (Column.FieldName = 'ImagenReferencia') then begin                                                       // SS_1214_CQU_20140917
        MostrarVentanaImagenReferencia(Self, Trim(spObtenerCuentasCartola.FieldByName('Patente').AsString));    // SS_1214_CQU_20140917
    end;                                                                                                        // SS_1214_CQU_20140917
   {

    if (spObtenerCuentasCartola.FieldByName('AdheridoPA').AsBoolean) and (MsgBox(MSG_PREGUNTA_POR_CONSULTA, Caption, MB_ICONQUESTION + MB_YESNO)= IDYES) then begin
        //obtener la URL del web service
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_URL_WEB_SERVICE, URLWebService) then begin
            MsgBox(Format(MSG_ERROR_URL, [PARAM_URL_WEB_SERVICE]), Caption, MB_ICONERROR);
            Exit;
        end;

        //obtener el owner
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_OWNER_WEB_SERVICE, OwnerWebService) then begin
            MsgBox(Format(MSG_ERROR_URL, [PARAM_OWNER_WEB_SERVICE]), Caption, MB_ICONERROR);
            Exit;
        end;

        //obtener el usuario
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_USER_WEB_SERVICE, UserWebService) then begin
            MsgBox(Format(MSG_ERROR_URL, [PARAM_USER_WEB_SERVICE]), Caption, MB_ICONERROR);
            Exit;
        end;

        //obtener la clave
        if not ObtenerParametroGeneral(DMConnections.BaseCAC, PARAM_PASSWORD_WEB_SERVICE, PasswordWebService) then begin
            MsgBox(Format(MSG_ERROR_URL, [PARAM_PASSWORD_WEB_SERVICE]), Caption, MB_ICONERROR);
            Exit;
        end;

        //hacer la consulta
        try
            ContractSerialNumber        := Trunc(spObtenerCuentasCartola.FieldByName('ContractSerialNumber').AsCurrency);	// SS_1006_MBE_20120830
            ManufacturerID              := spObtenerCuentasCartola.FieldByName('ManufacturerID').AsString;
            Etiqueta                    := spObtenerCuentasCartola.FieldByName('Etiqueta').AsString;
            
            ConsultaPorTelevia          := GetAccessListWSSoap(False, URLWebService);
            RespuestaConsultaPorTelevia := ConsultaPorTelevia.GetListOfVehicleActionList(nil, nil, '', OwnerWebService, ContractSerialNumber, ManufacturerID, UserWebService, PasswordWebService, OwnerWebService);
            if RespuestaConsultaPorTelevia.NumberOfItemsFound > 0 then begin
                //dado que fue encontrado, validar que su estado sea de alta
                //si es de baja, desplegar la raz�n
                if RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].Action = 'A' then begin
                    MsgBox(Format(MSG_TAG_ENCONTRADO, [Etiqueta]), Caption, MB_ICONINFORMATION);
                end
                else begin
                    FechaDeLaBajaString :=  Format('%-02d-%-02d-%04d %-02d:%-02d:%-02d', [   RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].ActionDate.Day,
                                                                                        RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].ActionDate.Month,
                                                                                        RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].ActionDate.Year,
                                                                                        RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].ActionDate.Hour,
                                                                                        RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].ActionDate.Minute,
                                                                                        RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].ActionDate.Second
                                                                                    ]);

                    MsgBox(Format(MSG_TAG_ESTA_DE_BAJA, [Etiqueta, FechaDeLaBajaString, RespuestaConsultaPorTelevia.ListOfVehicleActionList[0].ActionReason]), Caption, MB_ICONEXCLAMATION);
end;

            end
            else if RespuestaConsultaPorTelevia.Error <> nil then begin
                MsgBox(Format(MSG_TAG_NO_ENCONTRADO, [Etiqueta, 'Error : ' + RespuestaConsultaPorTelevia.Error.Message_]), Caption, MB_ICONEXCLAMATION);
            end
            else MsgBox(Format(MSG_TAG_NO_ENCONTRADO, [Etiqueta, EmptyStr]), Caption, MB_ICONEXCLAMATION);
        except on e:exception do begin
                MsgBoxErr(MSG_ERROR_WEB_SERVICE, e.Message, Caption, MB_ICONERROR);
            end;

end;
    end;
        }
end;
    // FIN SS_1006_MBE_20120731
    

//INICIO BLOQUE : SS-1006-NDR-20120614
procedure TFormInicioConsultaConvenio.DBLEAdhesionPAColumns0HeaderClick(
  Sender: TObject);
begin
    DBListExColumnHeaderClickGenerico(Sender);
end;
//FIN BLOQUE : SS-1006-NDR-20120614

procedure TFormInicioConsultaConvenio.DBLEConvenioObservacionesColumns0HeaderClick(Sender: TObject);    // SS_842_PDO_20111212
begin                                                                                                   // SS_842_PDO_20111212
    DBListExColumnHeaderClickGenerico(Sender);                                                          // SS_842_PDO_20111212
end;

procedure TFormInicioConsultaConvenio.DBLEConvenioObservacionesColumns1HeaderClick(Sender: TObject);    // SS_842_PDO_20111212
begin                                                                                                   // SS_842_PDO_20111212
    DBListExColumnHeaderClickGenerico(Sender);                                                          // SS_842_PDO_20111212
end;

//INICIA: TASK_066_GLE_20170409
{
procedure TFormInicioConsultaConvenio.DBLEConvenioObservacionesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;      // SS_842_PDO_20111212
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);                          // SS_842_PDO_20111212
begin                                                                                                                                       // SS_842_PDO_20111212
    if ObtenerConvenioObservacion.Active then begin                                                                                         // SS_842_PDO_20111212
        if ObtenerConvenioObservacion.RecordCount > 0 then begin                                                                            // SS_842_PDO_20111212
            if not ObtenerConvenioObservacion.FieldByName('FechaBaja').IsNull then begin                                                    // SS_842_PDO_20111212
                Sender.Canvas.Font.Color := clRed;                                                                                          // SS_842_PDO_20111212
            end;                                                                                                                            // SS_960_PDO_20120104
		    if Column.FieldName = 'FechaObservacion' then begin                                                                             // SS_960_PDO_20120104
		        if ObtenerConvenioObservacion.FieldByName('FechaObservacion').IsNull then begin                                             // SS_960_PDO_20120104
		            Text := ''                                                                                                              // SS_960_PDO_20120104
		        end                                                                                                                         // SS_960_PDO_20120104
		        else begin                                                                                                                  // SS_960_PDO_20120104
		            Text := FormatDateTime('dd/mm/yyyy hh:mm:ss',ObtenerConvenioObservacion.FieldByName('FechaObservacion').AsDateTime);    // SS_960_PDO_20120104
		        end;                                                                                                                        // SS_960_PDO_20120104
		    end;                                                                                                                            // SS_960_PDO_20120104
        end;                                                                                                                                // SS_842_PDO_20111212
    end;                                                                                                                                    // SS_842_PDO_20111212
end;
}
//TERMINA: TASK_066_GLE_20170409


{------------------------------------------------------------------
                dblEstacionamientosColumnsHeaderClick

Author      : MDI
Date        : 16-04-2013
Description :   (Ref SS 1006L)
                Implementa el ordenamiento de todas las columnas de la pesta�a Estacionamientos que invoquen este procedure

                Inicio SS_1006L_MDI_20130416
----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblEstacionamientosColumnsHeaderClick(Sender: TObject);
var order : string;
begin
	  if spObtenerEstacionamientos.IsEmpty then Exit;

	  if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        order := ' ASC';
    end
    else begin
        TDBListExColumn(sender).Sorting := csAscending;
        order := ' DESC';
    end;
	  spObtenerEstacionamientos.Sort := TDBListExColumn(Sender).FieldName + order;
end;
// FIN SS_1006L_MDI_20130416

procedure TFormInicioConsultaConvenio.dblEstacionamientosContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);             // SS_1006_PDO_20120106
begin
	                                                                                                                                // SS_1006_PDO_20120106
    if not spObtenerEstacionamientos.IsEmpty then begin                                                                                     // SS_1006_PDO_20120106
        if FListaEstacionamientos.Count = 1 then begin                                                                                      // SS_1006_PDO_20120106
            Handled :=                                                                                                                      // SS_1006_PDO_20120106
                MostrarMenuContextualEstacionamiento(                                                                                       // SS_1006_PDO_20120106
                    dblEstacionamientos.ClientToScreen(MousePos),                                                                           // SS_1006_PDO_20120106
                    StrToInt64(FListaEstacionamientos[0]));                                                                                        // SS_1006_PDO_20120106
        end                                                                                                                                 // SS_1006_PDO_20120106
        else begin                                                                                                                          // SS_1006_PDO_20120106
            Handled :=                                                                                                                      // SS_1006_PDO_20120106
                MostrarMenuContextualMultiplesEstacionamientos(                                                                             // SS_1006_PDO_20120106
                    dblEstacionamientos.ClientToScreen(MousePos),                                                                           // SS_1006_PDO_20120106
                    FListaEstacionamientos);                                                                                                // SS_1006_PDO_20120106
        end;                                                                                                                                // SS_1006_PDO_20120106
    end;                                                                                                                                    // SS_1006_PDO_20120106

end;

procedure TFormInicioConsultaConvenio.dblEstacionamientosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;            // SS_1006_PDO_20120106
    State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);                        // SS_1006_PDO_20120106
var                                                                                                                                         // SS_1006_PDO_20120106
    i, Indice: Integer;                                                                                                                     // SS_1006_PDO_20120106
begin                                                                                                                                       // SS_1006_PDO_20120106
    //muestra si esta seleccionado para realizar un reclamo o no                                                                            // SS_1006_PDO_20120106
	if Column = dblEstacionamientos.Columns[0] then begin                                                                                   // SS_1006_PDO_20120106
        //verifica si esta seleccionado o no                                                                                                // SS_1006_PDO_20120106
        i:= Util.iif(not ListaEstacionamientosIn(spObtenerEstacionamientos.FieldByName('NumCorrEstacionamiento').Asstring, Indice), 0, 1);       // SS_1006_PDO_20120106
		lnCheck.Draw(dblEstacionamientos.Canvas, Rect.Left + 3, Rect.Top + 1, i);                                                           // SS_1006_PDO_20120106
		DefaultDraw := False;                                                                                                               // SS_1006_PDO_20120106
		ItemWidth   := lnCheck.Width + 2;                                                                                                   // SS_1006_PDO_20120106
	end;																																	// SS_1006_PDO_20120106

    if Column.FieldName = 'Importe' then begin																								// SS_1006_PDO_20120712
        Text := FormatFloat(FORMATO_IMPORTE, spObtenerEstacionamientos.FieldByName('Importe').AsFloat);										// SS_1006_PDO_20120712
    end;                                                                                                    								// SS_1006_PDO_20120712
end;                                                                                                                                        

procedure TFormInicioConsultaConvenio.dblEstacionamientosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);                       // SS_1006_PDO_20120106
Var                                                                                                                                         // SS_1006_PDO_20120106
  	NumCorrEstacionamiento: String;                                                                                                         // SS_1006_PDO_20120106
    Indice: Integer;                                                                                                                        // SS_1006_PDO_20120106
begin                                                                                                                                       // SS_1006_PDO_20120106
    //selecciono/desselecciono el estacionamiento                                                                                           // SS_1006_PDO_20120106
    if Column = dblEstacionamientos.Columns[0] then begin                                                                                   // SS_1006_PDO_20120106
    	NumCorrEstacionamiento := spObtenerEstacionamientos.FieldByName('NumCorrEstacionamiento').Asstring;                                 // SS_1006_PDO_20120106
	    if ListaEstacionamientosIn(NumCorrEstacionamiento, Indice) then ListaEstacionamientosExclude(Indice)                                // SS_1006_PDO_20120106
	        else ListaEstacionamientosInclude(NumCorrEstacionamiento);                                                                      // SS_1006_PDO_20120106
  	    Sender.Invalidate;                                                                                                                  // SS_1006_PDO_20120106
        Sender.Repaint;                                                                                                                     // SS_1006_PDO_20120106
    end;                                                                                                                                    // SS_1006_PDO_20120106
end;                                                                                                                                        // SS_1006_PDO_20120106

procedure TFormInicioConsultaConvenio.DBListEx1Columns0HeaderClick(Sender: TObject);
begin
    DBListExColumnHeaderClickGenerico(Sender);
end;

procedure TFormInicioConsultaConvenio.DBLstExRefinanResumenCCDrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    {
    if cdsRefinanResumenCC.Active then begin
        with Sender do begin
            if ((cdsRefinanResumenCCFecha.AsDateTime < FAhora) and cdsRefinanResumenCCEstadoImpago.AsBoolean) or
                (cdsRefinanResumenCCCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) then begin
                if (odSelected in State) then Canvas.Brush.Color := clMaroon;
            end;
        end;
    end;
    }
end;

procedure TFormInicioConsultaConvenio.DBLstExRefinanResumenCCDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    {
    if cdsRefinanResumenCC.Active then begin
        with Sender do begin
            if ((cdsRefinanResumenCCFecha.AsDateTime < FAhora) and cdsRefinanResumenCCEstadoImpago.AsBoolean) or
                (cdsRefinanResumenCCCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) then begin

                if Not (odSelected in State) then begin
                    Canvas.Font.Color := clRed;
                end;

                if Column.FieldName = 'DescripcionEstadoCuota' then begin
                    if cdsRefinanResumenCCCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA] then
                        Canvas.Font.Style := Canvas.Font.Style + [fsBold];
                end;
            end;
        end;
    end;
    }
end;

{-----------------------------------------------------------------------------
Function Name   :   ListaTransitosInclude
Author          :   lgisuk
Date Created    :   14/04/2005
Description     :   agrega un valor a la lista de transitos
-----------------------------------------------------------------------------}
Function TFormInicioConsultaConvenio.ListaTransitosInclude(Valor:string):boolean;
var
    Indice: Integer;
begin
    if not ListaTransitosIn(Valor, Indice) then FListaTransitos.Add(Valor);
    Result := True;
end;

Function TFormInicioConsultaConvenio.ListaEstacionamientosInclude(Valor:string):boolean;    // SS_1006_PDO_20120106
var                                                                                         // SS_1006_PDO_20120106
    Indice: Integer;                                                                        // SS_1006_PDO_20120106
begin                                                                                       // SS_1006_PDO_20120106
    if not ListaEstacionamientosIn(Valor, Indice) then FListaEstacionamientos.Add(Valor);   // SS_1006_PDO_20120106
    Result := True;                                                                         // SS_1006_PDO_20120106
end;                                                                                        // SS_1006_PDO_20120106

{-----------------------------------------------------------------------------
Function Name   :   ListaTransitosExclude
Author          :   lgisuk
Date Created    :   14/04/2005
Description     :   Quita un valor a la lista de Transitos
-----------------------------------------------------------------------------}
Procedure TFormInicioConsultaConvenio.ListaTransitosExclude(Indice: Integer);
begin
    Flistatransitos.Delete(Indice);
end;

Procedure TFormInicioConsultaConvenio.ListaEstacionamientosExclude(Indice: Integer);         // SS_1006_PDO_20120106
begin                                                                                        // SS_1006_PDO_20120106
    FListaEstacionamientos.Delete(Indice);                                                   // SS_1006_PDO_20120106
end;                                                                                         // SS_1006_PDO_20120106

{-----------------------------------------------------------------------------
Function Name   :   ListaTransitosIn
Author          :   lgisuk
Date Created    :   14/04/2005
Description     :   Devuelve si un valor esta en la lista transitos
-----------------------------------------------------------------------------}
Function TFormInicioConsultaConvenio.ListaTransitosIn(valor:string; var IndexValue: Integer): Boolean;
begin
    IndexValue  := FListaTransitos.IndexOf(Valor);
    Result      := (IndexValue > -1);
end;

Function TFormInicioConsultaConvenio.ListaEstacionamientosIn(valor:string; var IndexValue: Integer): Boolean;   // SS_1006_PDO_20120106
begin                                                                                                           // SS_1006_PDO_20120106
    IndexValue  := FListaEstacionamientos.IndexOf(Valor);                                                       // SS_1006_PDO_20120106
    Result      := (IndexValue > -1);                                                                           // SS_1006_PDO_20120106
end;                                                                                                            // SS_1006_PDO_20120106


{-----------------------------------------------------------------------------
Function Name   :   dblTransitosDrawText
Author          :
Date Created    :   14/04/2005
Description     :   Da formato a la grilla
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    i, Indice: Integer;
begin
    //muestra si esta seleccionado para realizar un reclamo o no
	if Column = dbltransitos.Columns[0] then begin
        //verifica si esta seleccionado o no
        i:= Util.iif(not ListaTransitosIn(spObtenerTransitosConvenio.FieldByName('numcorrca').Asstring, Indice), 0, 1);
		lnCheck.Draw(dbltransitos.Canvas, Rect.Left + 3, Rect.Top + 1, i);
		DefaultDraw := False;
		ItemWidth   := lnCheck.Width + 2;
	end;

    //fecha/hora
	if Column = dblTransitos.Columns[1] then
	   Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerTransitosConvenio.FieldByName('FechaHora').AsDateTime)
	else begin
        //kms
		if Column = dblTransitos.Columns[10] then                                       // SS_1021_HUR_20120328
			Text := RStr(spObtenerTransitosConvenio.FieldByName('Kms').AsFloat, 10, 2)
		else if (Column = dblTransitos.Columns[CONST_COLUMNA_IMAGEN]) and            // SS_1021_HUR_20120328    // SS_1021_PDO_20120421
				(spObtenerTransitosConvenio.FieldByName('RegistrationAccessibility').AsInteger > 0) then begin
			ilCamara.Draw(dblTransitos.Canvas, Rect.Left + 20, Rect.Top + 1,
							Util.iif(spObtenerTransitosConvenio.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0));
			DefaultDraw := False;
			ItemWidth   := ilCamara.Width + 2;
		end
	end;
    // se muestra el estado del tag en ese transito
    if Column = dblTransitos.Columns[15] then begin                                 // SS_1021_HUR_20120328
          DefaultDraw := False;
		  if spObtenerTransitosConvenio.FieldByName('LV').AsInteger = 0 then
              Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)
          else
              Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 1);
          ItemWidth := Imagenes.Width;
    end;
    // se muestra el estado del tag en ese transito
    if Column = dblTransitos.Columns[16] then begin                                 // SS_1021_HUR_20120328
          DefaultDraw := False;
		  if spObtenerTransitosConvenio.FieldByName('LA').AsInteger = 0 then
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)
          else
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 3);
          ItemWidth := Imagenes.Width;
    end;
    // se muestra el estado del tag en ese transito
    if Column = dblTransitos.Columns[17] then begin                                 // SS_1021_HUR_20120328
          DefaultDraw := False;
		  if spObtenerTransitosConvenio.FieldByName('LG').AsInteger = 0 then
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)
          else
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 4);
          ItemWidth := Imagenes.Width;
    end;
    // se muestra el estado del tag en ese transito
    if Column = dblTransitos.Columns[18] then begin                                 // SS_1021_HUR_20120328
          DefaultDraw := False;
		  if spObtenerTransitosConvenio.FieldByName('LN').AsInteger = 0 then
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)
          else
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 2);
          ItemWidth := Imagenes.Width;
    end;
    // se muestra el estado del tag en ese transito
    if Column = dblTransitos.Columns[19] then begin                                 // SS_1021_HUR_20120328
          DefaultDraw := False;
		  if spObtenerTransitosConvenio.FieldByName('EH').AsInteger = 0 then
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)
          else
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 10);
          ItemWidth := Imagenes.Width;
    end;

     // se muestra el estado del tag en ese transito
    if Column = dblTransitos.Columns[20] then begin                                 // SS_1021_HUR_20120328
          DefaultDraw := False;
		  if spObtenerTransitosConvenio.FieldByName('MMICO').AsInteger = 0 then
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)
          else
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 10);
          ItemWidth := Imagenes.Width;
    end;

    if Column = dblTransitos.Columns[21] then begin                                 //SS_1170_MCA_20140214
          DefaultDraw := False;                                                     //SS_1170_MCA_20140214
		  if spObtenerTransitosConvenio.FieldByName('TR').AsInteger = 0 then        //SS_1170_MCA_20140214
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)            //SS_1170_MCA_20140214
          else                                                                      //SS_1170_MCA_20140214
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 10);          //SS_1170_MCA_20140214
          ItemWidth := Imagenes.Width;                                              //SS_1170_MCA_20140214
    end;                                                                            //SS_1170_MCA_20140214

    if Column = dblTransitos.Columns[22] then begin                                 //SS_1170_MCA_20140214
          DefaultDraw := False;                                                     //SS_1170_MCA_20140214
		  if spObtenerTransitosConvenio.FieldByName('NH').AsInteger = 0 then        //SS_1170_MCA_20140214
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 0)            //SS_1170_MCA_20140214
          else                                                                      //SS_1170_MCA_20140214
            Imagenes.Draw(Sender.Canvas, Rect.Left + 2, Rect.Top + 1, 10);          //SS_1170_MCA_20140214
          ItemWidth := Imagenes.Width;                                              //SS_1170_MCA_20140214
    end;                                                                            //SS_1170_MCA_20140214
end;

{-----------------------------------------------------------------------------
Function Name   :   dblTransitosCheckLink
Author          :
Date Created    :   14/04/2005
Description     :   verifica si puede hacer click en el link
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblTransitosCheckLink(Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    //permito hacer click en el link o no
    if  Column = dblTransitos.Columns[CONST_COLUMNA_IMAGEN] then begin     // SS_1021_PDO_20120421
    	IsLink := (spObtenerTransitosConvenio['RegistrationAccessibility'] > 0);
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   dblTransitosLinkClick
Author          :
Date Created    :   14/04/2005
Description     :   abre una imagen al hacer click en el link
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblTransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Var
  	Numcorrca: String;
    Indice: Integer;
    ImgSPCForm : TImagenesSPCForm;
begin
    //selecciono/desselecciono transito
    if Column = dbltransitos.Columns[0] then begin
    	Numcorrca := spObtenerTransitosConvenio.FieldByName('numcorrca').Asstring;
	    if ListaTransitosIn(Numcorrca, Indice) then ListaTransitosExclude(Indice)
	        else ListaTransitosInclude(Numcorrca);
  	    Sender.Invalidate;
        Sender.Repaint;
    end;
//    if Column = dblTransitos.Columns[CONST_COLUMNA_IMAGEN] then begin             //SS-377-NDR-20110607
//        //MostrarVentanaImagen(Self,spObtenerTransitosConvenio['NumCorrCA'], spObtenerTransitosConvenio['FechaHora']);  // SS_1091_CQU_20130516
//        MostrarVentanaImagen(Self,                                                                                      // SS_1091_CQU_20130516
//            spObtenerTransitosConvenio['NumCorrCA'],                                                                    // SS_1091_CQU_20130516
//            spObtenerTransitosConvenio['FechaHora'],                                                                    // SS_1091_CQU_20130516
//            spObtenerTransitosConvenio['EsItaliano']                                                                    // SS_1091_CQU_20130516
//            );                                                                                                          // SS_1091_CQU_20130516
//    end;

    if Column = dblTransitos.Columns[CONST_COLUMNA_IMAGEN] then begin

        if spObtenerTransitosConvenio['RegistrationAccessibility'] > 0 then try
            ImgSPCForm := TImagenesSPCForm.Create(Self);

            if ImgSPCForm.Inicializar(spObtenerTransitosConvenio['numcorrca'], spObtenerTransitosConvenio['RegistrationAccessibility']) then begin
                ImgSPCForm.ShowModal;
            end
            else ShowMsgBoxCN('Im�genes del Tr�nsito','No existen Im�genes disponibles para el Tr�nsito seleccionado.', MB_ICONINFORMATION, Self);
        finally
            if Assigned(ImgSPCForm) then FreeAndNil(ImgSPCForm);
        end;
    end;
end;

procedure TFormInicioConsultaConvenio.dbResumenCCDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
	if not ((odSelected in State))   then begin
        if Sender.DataSource.DataSet.RecNo mod 2 <> 0 then begin
            Sender.Canvas.Brush.Color := $00FFD7AE; // $00FFD1A4; //$00C9FFB7;
        end;
    end;
end;

procedure TFormInicioConsultaConvenio.deHistoricoDesdeChange(Sender: TObject);          // SS_842_PDO_20111212
begin                                                                                   // SS_842_PDO_20111212
    if ObtenerConvenioObservacion.Active then ObtenerConvenioObservacion.Close;         // SS_842_PDO_20111212
    //chkObservacionesBaja.Enabled := (Sender as TDateEdit).ValidDate and (not (Sender as TDateEdit).IsEmpty);                    // SS_842_PDO_20111212   //TASK_066_GLE_20170409
    //if not chkObservacionesBaja.Enabled then chkObservacionesBaja.Checked := False;     // SS_842_PDO_20111212                                           //TASK_066_GLE_20170409
end;                                                                                    // SS_842_PDO_20111212



{-----------------------------------------------------------------------------
Function Name   :   dsHistoricoListaAmarillaDataChange
Author          :   mcabello
Date Created    :   29/07/2013
Description     :   Obtiene el Historico de CArtas asociado al id convenio inhabilitado
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dsHistoricoListaAmarillaDataChange(
  Sender: TObject; Field: TField);
begin
  if not spHistoricoListaAmarilla.eof then      // SS_660_MCA_20130729
    ObtenerHistoricoDeCartas(cbConvenio.Value, spHistoricoListaAmarilla.FieldByName('IdConvenioInhabilitado').Value); // SS_660_MCA_20130729
end;


{-----------------------------------------------------------------------------
Function Name   :   dblTransitosContextPopup
Author          :   lgisuk
Date Created    :   14/04/2005
Description     :   asigna el pop menu a la grilla
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblTransitosColumns15HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dblTransitos.DataSource.DataSet.Eof then                             //SS_1170_MCA_20140214
    MsgBoxBalloon('Lista Verde',                                   				//SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormInicioConsultaConvenio.dblTransitosColumns16HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dblTransitos.DataSource.DataSet.Eof then                             //SS_1170_MCA_20140214
    MsgBoxBalloon('Lista Amarilla',                                   			//SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormInicioConsultaConvenio.dblTransitosColumns17HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dblTransitos.DataSource.DataSet.Eof then                             //SS_1170_MCA_20140214
    MsgBoxBalloon('Lista Gris',                                   				//SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormInicioConsultaConvenio.dblTransitosColumns18HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dblTransitos.DataSource.DataSet.Eof then                             //SS_1170_MCA_20140214
    MsgBoxBalloon('Lista Negra',                                   				//SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormInicioConsultaConvenio.dblTransitosColumns19HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dblTransitos.DataSource.DataSet.Eof then                             //SS_1170_MCA_20140214
    MsgBoxBalloon('Tomar Fotograf�a',                                   		 //SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormInicioConsultaConvenio.dblTransitosColumns20HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
	if not dblTransitos.DataSource.DataSet.Eof then                             //SS_1170_MCA_20140214
    MsgBoxBalloon('Cont�cte al Operador',                                   	//SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;

procedure TFormInicioConsultaConvenio.dblTransitosColumns21HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
    if not dblTransitos.DataSource.DataSet.Eof then								//SS_1170_MCA_20140214
    MsgBoxBalloon('Telev�a Removido',                                           //SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormInicioConsultaConvenio.dblTransitosColumns22HeaderClick(         //SS_1170_MCA_20140214
  Sender: TObject);                                                             //SS_1170_MCA_20140214
begin                                                                           //SS_1170_MCA_20140214
    if not dblTransitos.DataSource.DataSet.Eof then                             //SS_1170_MCA_20140214
    MsgBoxBalloon('Telev�a fuera de su base',                                   //SS_1170_MCA_20140214
                    '',                                        					//SS_1170_MCA_20140214
                    MB_ICONQUESTION,                                            //SS_1170_MCA_20140214
                    Mouse.CursorPos.X,                                          //SS_1170_MCA_20140214
                    Mouse.CursorPos.Y);                                         //SS_1170_MCA_20140214
end;                                                                            //SS_1170_MCA_20140214

procedure TFormInicioConsultaConvenio.dblTransitosContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
    if not spObtenerTransitosConvenio.IsEmpty then begin
        Handled := MostrarMenuContextualMultiplesTransitos(dblTransitos.ClientToScreen(MousePos),Flistatransitos);
    end;
end;


{-----------------------------------------------------------------------------
Function Name   :   LNuevoReclamoGeneralClick
Author          :   lgisuk
Date Created    :   14/04/2005
Description     :   Abre un nuevo Reclamo General
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.LNuevoReclamoGeneralClick(Sender: TObject);
begin
    if cbConvenio.ItemIndex <> -1 then GenerarMenuReclamosGenerales(popReclamosGenerales.Items, cbConvenio.Value)
      else GenerarMenuReclamosGenerales(popReclamosGenerales.Items);
    popReclamosGenerales.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

{-----------------------------------------------------------------------------
Function Name   :   dblReclamosDrawText
Author          :   lgisuk
Date Created    :   05/04/2005
Description     :   formato a la grilla de Reclamos
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblReclamosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
resourcestring
    NO_DETERMINADA =  'No Determinada';
Var
    Estado, Usuario: AnsiString;
begin
    //Candado
    Usuario := spReclamos.FieldByName('Usuario').AsString;
    Estado := spReclamos.FieldByName('Estado').AsString;
    if Column = dblReclamos.Columns[0] then begin
		if ((Estado = 'P') or (Estado = 'I') or (Estado = 'E')) and (Usuario <> '') then begin  //inclui pendientes internas y externas
            if Usuario = UsuarioSistema then
                Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GetLockBitmap)
            else begin
                Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GetLockOwnerBitmap);
            end;
            ItemWidth := GetLockBitmap.Width + 4;
        end;
    end;
    //FechaHoraCreacion
    if Column = dblreclamos.Columns[3] then
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spReclamos.FieldByName('FechaHoraCreacion').AsDateTime);
    //FechaCompromiso
    if Column = dblreclamos.Columns[4] then
        if text = '' then Text := NO_DETERMINADA;
    //FechaHoraFin
    if Column = dblreclamos.Columns[5] then
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spReclamos.FieldByName('FechaHoraFin').AsDateTime);
    //Prioridad
    if Column = dblreclamos.Columns[6] then
	   Text := ObtenerDescripcionPrioridadOS(spReclamos.FieldByName('Prioridad').Asinteger);//Obtengo la descripci�n de la prioridad
    //Estado
    if Column = dblreclamos.Columns[7] then
       Text := ObtenerDescripcionEstadoOS(spReclamos.FieldByName('Estado').AsString);//Obtengo la descripci�n de la prioridad
    // Ultima Modificaci�n
    if Column = dblreclamos.Columns[10] then
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spReclamos.FieldByName('UltimaFechaModificacion').AsDateTime);
end;

{-----------------------------------------------------------------------------
Function Name   :   dblReclamosContextPopup
Author          :   lgisuk
Date Created    :   05/04/2005
Description     :   Asigna el menu contextual a la grilla reclamos
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblReclamosContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
    if not spReclamos.IsEmpty then Handled := MostrarMenuContextualOrdenServicio(
        dblReclamos.ClientToScreen(MousePos), spReclamos['CodigoOrdenServicio'], FPuntoEntrega, FPuntoVenta);
end;

{-----------------------------------------------------------------------------
Function Name   :   btn_cerrarClick
Author          :   lgisuk
Date Created    :   05/04/2005
Description     :   Cierro el formulario
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.btn_cerrarClick(Sender: TObject);
begin
	  Close;
end;

{-----------------------------------------------------------------------------
Function Name   :   FormDestroy
Author          :   lgisuk
Date Created    :   14/04/2005
Description     :   cierra la ventana de imagen y pido no ser mas notificado
                    si cambio una orden de servicio
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.FormDestroy(Sender: TObject);
begin
    CerrarVentanaImagen(Self);
    RemoveOSNotification(OSChanged);
end;

procedure TFormInicioConsultaConvenio.FormShow(Sender: TObject);
begin
end;

{-----------------------------------------------------------------------------
Function Name   :   GuardarMailPersona
Author          :   mpiazza
Date Created    :   20/01/2010
Description     :   Guarda el mail en medio de comunicacion de ser necesario
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.GuardarMailPersona(CodigoPersona: Integer; eMail : string) ;
var
    MediosComunicacionPersona : TMediosComunicacionPersona;
    TipoMedioComunicacion : TTipoMedioComunicacion;
begin
    //cargo los datos de tipo medio de comunicacion de acuerdo al tipo de medio e-Mail
    TipoMedioComunicacion.CodigoTipoMedioContacto   := TIPO_MEDIO_CONTACTO_E_MAIL;
    TipoMedioComunicacion.valor                     := eMail;
    TipoMedioComunicacion.Principal                 := False;
    TipoMedioComunicacion.indice                    := -1; //0.0

    TipoMedioComunicacion.CodigoArea                := -1;          // SS_997_PDO_20111014
    TipoMedioComunicacion.Anexo                     := -1;          // SS_997_PDO_20111014
    TipoMedioComunicacion.HoraDesde                 := NullDate;    // SS_997_PDO_20111014
    TipoMedioComunicacion.HoraHasta                 := NullDate;    // SS_997_PDO_20111014

    //creo la clase TMediosComunicacionPersona
    MediosComunicacionPersona   := TMediosComunicacionPersona.Create(CodigoPersona,TMC_EMAIL );
    MediosComunicacionPersona.AgregarMedioComunicacion(TipoMedioComunicacion);
    MediosComunicacionPersona.Guardar;
    MediosComunicacionPersona.free;
end;

{-----------------------------------------------------------------------------
Function Name   :   lblEmailClick
Author          :
Date Created    :
Description     :   Abre el correo electronico
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.lblEmailClick(Sender: TObject);
begin
    if not InvocarCorreoPredeterminado(Self, lblEmail.Caption , '', '') then begin
        MsgBoxErr(MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO, MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO, Caption, MB_ICONERROR);
    end;

end;

// Inicio Bloque: SS_960_PDO_20120115
procedure TFormInicioConsultaConvenio.lblReimprimirNotifTAGsVencidosClick(Sender: TObject);
    resourcestring
        MSG_REIMPRIMIR_NOTIFICACION_TITULO = 'Reimprimir Notficaci�n TAGs Vencidos';
        MSG_REIMPRIMIR_NOTIFICACION_TEXTO  = '� Desea reimprimir la Notificaci�n de TAGs Vencidos para el cliente seleccionado ?';
begin
    if ShowMsgBoxCN(MSG_REIMPRIMIR_NOTIFICACION_TITULO, MSG_REIMPRIMIR_NOTIFICACION_TEXTO, MB_ICONQUESTION, Self) = mrOk then begin
        ReimprimeNotificacionTAGSVencidos;
    end;
end;
// Fin Bloque: SS_960_PDO_20120115

{-----------------------------------------------------------------------------
Function Name   :   lblVerImagenScanClick
Author          :
Date Created    :
Description     :   Permite acceder al Visor de im�genes escaneadas de documentos
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.lblVerImagenScanClick(Sender: TObject);
var
    f: TformVisorDocumentos;
begin
    if FindFormOrCreate(TformVisorDocumentos, f) then
		f.Show
	else begin
		if not f.Inicializar(Caption, cbConvenio.Value, True) then f.Release;
	end;
end;

{-----------------------------------------------------------------------------
Function Name   :   EstablecerFechasTransitos
Author          :   gcasais
Date Created    :   21/04/2005
Description     :   Establece un fecha Inicial y Final para el filtro de busqueda
                    de Transitos
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.EstablecerFechasTransitos(var Inicial, Final: TDateTime);
const
    VALOR = -2;
var
    Year, Month, Day : Word;
begin
    //Fecha Incial: 2 meses Atras
    DecodeDate(Now, Year, Month, Day);
    IncAMonth(Year, Month, Day, VALOR);
    Inicial := EncodeDate(Year, Month, Day);
    //Fecha Final: Hoy.
    Final := Now;
end;

{-----------------------------------------------------------------------------
Function Name   :   EstablecerFechasUC
Author          :   lgisuk
Date Created    :   21/04/2005
Description     :   Establece la fecha cierre para el filtro de busqueda
                    de ultimos consumos
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.EstablecerFechaUC(var Cierre: TDateTime);
begin
    //asigna la fecha de hoy
    Cierre := Now;
end;

{-----------------------------------------------------------------------------
Function Name   :   neCantidadTransitosChange
Author          :   gcasais
Date Created    :   22/04/2005
Description     :   Asigna la cantidad de transitos que desea ver en el grilla
                    a una variable global
-----------------------------------------------------------------------------}
{ // //SS 1006 / 1015_MCO_20120828
procedure TFormInicioConsultaConvenio.neCantidadTransitosChange(Sender: TObject);
begin
    FCantidadTransitos := neCantidadTransitos.ValueInt;
end;
 }
{-----------------------------------------------------------------------------
Function Name   :   dblCuentasDblClick
Author          :   gcasais
Date Created    :   21/04/2005
Description     :   El usuario seleccion� una cuenta
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblCuentasDblClick(Sender: TObject);
begin
    try
        //si la consulta de vehiculos no esta activa salgo
        if (Sender as TDBListEx).DataSource.Dataset.Active = false then exit;
        //asigna el vehiculo seleccionado a una variable global
        FIndiceVehiculoSeleccionado := (Sender as TDBListEx).DataSource.Dataset.FieldByName('IndiceVehiculo').Value;
        //al hacer doble click realiza la misma accion que si fuera al check y lo selecionara
        cbTransitoSoloVehiculo.Checked := not cbTransitoSoloVehiculo.Checked;
        cbUCSoloVehiculo.Checked := not cbUCSoloVehiculo.Checked;
    except
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   FiltrarTransitos
Author          :   gcasais
Date Created    :   21/04/2005
Description     :   Filtra los tr�nsitos seg�n los par�metros indicados
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.FiltrarTransitos(
  //const Datos: TADOStoredProc; CodigoConvenio, IndiceVehiculo, CodigoConcesionaria: Variant );                        // TASK_112_MGO_20170110
  const Datos: TADOStoredProc; CodigoConvenio, IndiceVehiculo, CodigoConcesionaria: Variant; SoloGratuidad: Boolean );  // TASK_112_MGO_20170110
resourcestring
    MSG_ERROR       = 'Error';
    MSG_DESCRIPTION = 'Error Obteniendo Tr�nsitos';
var
    Year, Month, Day, Hour, Minute, Second, MSec: Word;
    Inicio, Fin: TDateTime;
begin
    FListaTransitos.Clear;
    //Fecha y Hora de Inicio
    DecodeTime(teTransitosHoraDesde.Time, Hour, Minute, Second, MSec);
    DecodeDate(deTransitosDesde.Date, Year, Month, Day);
    Inicio := EncodeDateTime(Year, Month, Day, Hour, Minute, Second, MSec);
    //Fecha y Hora de Fin
    DecodeTime(teTransitosHoraHasta.Time, Hour, Minute, Second, MSec);
    DecodeDate(deTransitosHasta.Date, Year, Month, Day);
    Fin := EncodeDateTime(Year, Month, Day, Hour, Minute, Second, MSec);
    //cargo los parametros
    Datos.Close;
    Datos.Parameters.Refresh;
    Datos.Parameters.ParamByName('@Codigoconvenio').Value       := CodigoConvenio;
    Datos.Parameters.ParamByName('@IndiceVehiculo').Value       := IndiceVehiculo;
    Datos.Parameters.ParamByName('@FechaHoraInicial').Value     := Inicio;
    Datos.Parameters.ParamByName('@FechaHoraFinal').Value       := Fin;
    Datos.Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
    Datos.Parameters.ParamByName('@SoloGratuidad').Value        := SoloGratuidad;               // TASK_112_MGO_20170110

    try
        Datos.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, MSG_DESCRIPTION, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   btnTransitosFiltrarClick
Author          :   gcasais
Date Created    :   22/04/2005
Description     :   Trae los transitos en funci�n de las condiciones del filtro
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.btnRefinanciacionesClick(Sender: TObject);
    var
        TotalDeudasRefinanciacion,
        DeudaVencida: Extended;
begin

    if (peNumeroDocumento.Text <> EmptyStr) and (cbConvenio.Items.Count <> 0) then try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            with spObtenerRefinanciacionesTotalesPersona do begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value  := FCodigoPersona;
                Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
                Open;

                TotalDeudasRefinanciacion := FieldByName('TotalDeuda').AsFloat - FieldByName('TotalPagado').AsFloat;
                DeudaVencida              := FieldByName('DeudaVencida').AsFloat;
            end;

            lblDeudaRef.Caption := FormatFloat(FORMATO_IMPORTE, TotalDeudasRefinanciacion);
            if DeudaVencida > 0 then begin
                lblDeudaRef.Font.Color := clRed;
                lblDeudaRef.Font.Style := lblDeudaRef.Font.Style + [fsBold];
                //ShowMsgBoxCN(MSG_AVISO_REF_DEUDA_VENCIDA_TITULO, MSG_AVISO__REF_DEUDA_VENCIDA_MENSAJE, MB_ICONWARNING, Self);
            end;

            if cdsRefinanResumenCC.Active then cdsRefinanResumenCC.Close;

            with spObtenerRefinanciacionResumenCuentaCorriente do begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value        := FCodigoPersona;
            end;

            cdsRefinanResumenCC.Open;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;


{    if peNumeroDocumento.Text = '' then Exit;

    spRefinanciacionBuscarTodas.Close;
    spRefinanciacionBuscarTodas.Parameters.ParamByName('@Rut').Value := peNumeroDocumento.Text;
    spRefinanciacionBuscarTodas.Open;}
end;

procedure TFormInicioConsultaConvenio.btnTransitosFiltrarClick(Sender: TObject);
var
	CodigoConcesionaria, IndiceVehiculo : Variant;
begin
    if not ValidarFiltroTransitos then Exit;
    if spObtenerCuentasCartola.Active = False then Exit;


    //REV.23
    if vcbConcesionariasTransa.ItemIndex > 0 then CodigoConcesionaria := vcbConcesionariasTransa.Value
    else CodigoConcesionaria := NULL;

    if cbTransitoSoloVehiculo.Checked then IndiceVehiculo := spObtenerCuentasCartola.FieldbyName('IndiceVehiculo').Value
    else IndiceVehiculo := NULL;

    FiltrarTransitos(	spObtenerTransitosConvenio,
    					cbConvenio.Value,
                        IndiceVehiculo,
                        CodigoConcesionaria,                                    // TASK_112_MGO_20170110
                        chkTransitoSoloGratuidad.Checked    );                  // TASK_112_MGO_20170110
                        {neCantidadTransitos.ValueInt}          //SS 1006 / 1015_MCO_20120828
	//FIN REV.23
    dblCuentas.SelectedIndex := spObtenerCuentasCartola.RecNo;
end;

{-----------------------------------------------------------------------------
Function Name   :   lblTransitosReestablecerClick
Author          :   gcasais
Date Created    :   22/04/2005
Description     :   Trae fecha de transitos (desde y hasta)
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.lblTransitosReestablecerClick(Sender: TObject);
begin
    EstablecerFechasTransitos(FTransitosDesde, FTransitosHasta);
    deTransitosDesde.Date := FTransitosDesde;
    deTransitosHasta.Date := FTransitosHasta;
end;

{-----------------------------------------------------------------------------
Function Name   :   ValidarFiltroTransitos
Author          :   gcasais
Date Created    :   22/04/2005
Description     :   Valida los par�metros aplicados al filtro de tr�nsitos
-----------------------------------------------------------------------------}
function TFormInicioConsultaConvenio.ValidarFiltroTransitos: Boolean;
resourcestring
    MSG_INVALID_DATE            = 'Fecha Inv�lida';
    MSG_INVALID_DATE_DESC       = 'La Fecha Indicada No Es V�lida';
    MSG_INVALID_ROW_COUNT       = 'Cantidad de Tr�nsitos Inv�lida';
    MSG_INVALID_ROW_COUNT_DESC  = 'La Cantidad de Tr�nsitos a Mostrar No Puede ser Inferior a 1';
var
    Year, Month, Day: Word;
begin
    Result := False;
    DecodeDate(deTransitosDesde.Date, Year, Month, Day);
    if not ValidateControls([deTransitosDesde], [IsValidDate(Year, Month, Day)],
     MSG_INVALID_DATE, [MSG_INVALID_DATE_DESC ]) then Exit;
    DecodeDate(deTransitosHasta.Date, Year, Month, Day);
    if not ValidateControls([deTransitosHasta], [IsValidDate(Year, Month, Day)],
      MSG_INVALID_DATE, [MSG_INVALID_DATE_DESC ]) then Exit;
    {if not ValidateControls([neCantidadTransitos], [neCantidadTransitos.ValueInt > 0],
     MSG_INVALID_ROW_COUNT, [MSG_INVALID_ROW_COUNT_DESC]) then Exit;       }    //SS 1006 / 1015_MCO_20120828
    Result := True;
end;

{-----------------------------------------------------------------------------
Function Name   :   lblUCReestablecerClick
Author          :   lgisuk
Date Created    :   22/04/2005
Description     :   Restablece la fecha de busqueda original que es
                    la fecha actual
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.lblUCReestablecerClick( Sender: TObject);
begin
    //INICIA: TASK_060_GLE_20170307
    //EstablecerFechaUC(FUCCorte);
    //deUCCorte.Date := FUCCorte;

    EstablecerFechaUC(FUCCorteDesde);
    dedtUCFechaDesde.Date := FUCCorteDesde;

    EstablecerFechaUC(FUCCorteHasta);
    dedtUCFechaHasta.Date := FUCCorteHasta;
    //TERMINA: TASK_060_GLE_20170307
end;

{-----------------------------------------------------------------------------
Function Name   :   FiltrarUltimosConsumos
Author          :   lgisuk
Date Created    :   22/04/2005
Description     :   Filtra los tr�nsitos seg�n los par�metros indicados
-----------------------------------------------------------------------------}
//procedure TFormInicioConsultaConvenio.FiltrarUltimosConsumos(const Datos: TADOStoredProc; CodigoConvenio, IndiceVehiculo, CodigoConcesionaria: Variant);  //TASK_060_GLE_20170307
procedure TFormInicioConsultaConvenio.FiltrarUltimosConsumos(const Datos: TADOStoredProc; CodigoConvenio, Patente: Variant);                                //TASK_060_GLE_20170307
resourcestring
    MSG_ERROR       = 'Error';
    MSG_DESCRIPTION = 'Error Obteniendo Ultimos Consumos';
var
    Year, Month, Day, Hour, Minute, Second, MSec: Word;
    //Corte: TDateTime;                                                                                         //TASK_060_GLE_20170307
    UCFechaDesde, UCFechaHasta: TDateTime;                                                                      //TASK_060_GLE_20170307
    MovFact : Boolean;                                                                                          //TASK_060_GLE_20170307
begin
    if Datos.Active then Datos.Close;
    //Fecha de Corte
    //INICIA: TASK_060_GLE_20170307
    //DecodeTime(teUCCorte.Time, Hour, Minute, Second, MSec);
    //DecodeDate(deUCCorte.Date, Year, Month, Day);
    //Corte := EncodeDateTime(Year, Month, Day, Hour, Minute, Second, MSec);
    DecodeDate(dedtUCFechaDesde.Date, Year, Month, Day);
    UCFechaDesde := EncodeDate(Year, Month, Day);

    DecodeDate(dedtUCFechaHasta.Date, Year, Month, Day);
    UCFechaHasta := EncodeDate(Year, Month, Day);

    if chkMuestraConsumosNoFacturados.checked then MovFact := True else MovFact := False;

    //asigno los parametros
    {
    with Datos.Parameters do begin
        Datos.Parameters.Refresh;
        ParamByName('@CodigoConvenio').Value      := CodigoConvenio;
        ParamByName('@IndiceVehiculo').Value      := IndiceVehiculo;
        ParamByName('@FechaCorte').Value        := Corte;
        ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
    end;
    }
    with Datos.Parameters do begin
        Datos.Parameters.Refresh;
        ParamByName('@ConvenioFacturacion').Value   := CodigoConvenio;
        ParamByName('@FechaDesde').Value            := iif(chkMuestraConsumosNoFacturados.checked, UCFechaDesde, null);
        ParamByName('@FechaHasta').Value            := iif(chkMuestraConsumosNoFacturados.checked, UCFechaHasta, null);
        ParamByName('@Patente').Value               := Patente;
        ParamByName('@SoloNOFacturado').Value       := iif(MovFact, True, False);
    end;
    //TERMINA: TASK_060_GLE_20170307
    //abro la consulta
    try
        Datos.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, MSG_DESCRIPTION, MB_ICONERROR);
        end;
    end;
end;


{-----------------------------------------------------------------------------
Function Name   :   ValidarFiltroUC
Author          :   gcasais
Date Created    :   22/04/2005
Description     :   Valida los par�metros aplicados al filtro de Ultimos Consumos
-----------------------------------------------------------------------------}
function TFormInicioConsultaConvenio.ValidarFiltroUC: Boolean;
resourcestring
    MSG_INVALID_DATE        = 'Fecha Inv�lida';
    MSG_INVALID_DATE_DESC   = 'La Fecha Indicada No Es V�lida';
var
    Year, Month, Day: Word;
begin
    Result := False;
    //valido la fecha de corte
    //INICIA: TASK_060_GLE_20170307
    //DecodeDate(deUCCorte.Date, Year, Month, Day);
    //if not ValidateControls([deUCCorte], [IsValidDate(Year, Month, Day)],
    //MSG_INVALID_DATE, [MSG_INVALID_DATE_DESC ]) then Exit;

    //Valida fecha desde
    DecodeDate(dedtUCFechaDesde.Date, Year, Month, Day);
    if not ValidateControls([dedtUCFechaDesde], [IsValidDate(Year, Month, Day)],
        MSG_INVALID_DATE, [MSG_INVALID_DATE_DESC ]) then Exit;

    //Valida fecha hasta
    DecodeDate(dedtUCFechaHasta.Date, Year, Month, Day);
    if not ValidateControls([dedtUCFechaHasta], [IsValidDate(Year, Month, Day)],
        MSG_INVALID_DATE, [MSG_INVALID_DATE_DESC ]) then Exit;

    //Verifica que la fecha desde sea menor que la fecha hasta
    if dedtUCFechaHasta.Date < dedtUCFechaDesde.Date then begin
        MsgBoxErr('La Fecha Desde debe ser menor que la Fecha Hasta', 'Fechas No V�lidas', Self.Caption, MB_ICONERROR);
        Exit;
    end;
    //TERMINA: TASK_060_GLE_20170307

    Result := True;
end;

{-----------------------------------------------------------------------------
Function Name   :   btnUCFiltrarClick
Author          :   lgisuk
Date Created    :   22/04/2005
Description     :   Trae los ultimos consumos en funci�n de las condiciones del filtro
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.btnUCFiltrarClick(Sender: TObject);
var
    //IndiceVehiculo, CodigoConcesionaria : Variant;                            //TASK_060_GLE_20170307
    Patente, CodigoConcesionaria : Variant;                                     //TASK_060_GLE_20170307
begin
    try
         Cursor := crHourGlass;                                                 //TASK_060_GLE_20170307
        //valida que las condiciones del filtro sean correctas sino salgo
        if not ValidarFiltroUC then Exit;
        //verifica si quiere los ultimos consumos de todos los vehiculos
        //o de uno en particular

        //REV.23
        //INICIA: TASK_060_GLE_20170307
        {
        if cbUCSoloVehiculo.Checked then IndiceVehiculo := spObtenerCuentasCartola.FieldbyName('IndiceVehiculo').Value
        else IndiceVehiculo := NULL;
                                                                                                        // PAR00133_CORRECCIONES_PDO_20110705
        if vcbConcesionariasUC.ItemIndex > 0 then CodigoConcesionaria := vcbConcesionariasUC.Value      // PAR00133_CORRECCIONES_PDO_20110705
        else CodigoConcesionaria := NULL;                                                               // PAR00133_CORRECCIONES_PDO_20110705
                                                                                                        // PAR00133_CORRECCIONES_PDO_20110705
        CodigoConcesionaria := NULL;                                                                    // PAR00133_CORRECCIONES_PDO_20110705
        }

        if cbUCSoloVehiculo.Checked then Patente := spObtenerCuentasCartola.FieldbyName('Patente').Value
        else Patente := NULL;
        //obtengo los ultimos consumos de todos los vehiculos
        //FiltrarUltimosConsumos(spObtenerUltimosConsumos, cbConvenio.Value, IndiceVehiculo, CodigoConcesionaria);
        FiltrarUltimosConsumos(spObtenerUltimosConsumos, cbConvenio.Value, Patente);
        Cursor := crDefault;
    except
        MsgBoxErr('Error al filtrar los �ltimos consumos ', 'Error', self.Caption, MB_ICONERROR);
        Cursor := crDefault;
    end;
    //TERMINA: TASK_060_GLE_20170307
end;

procedure TFormInicioConsultaConvenio.btnSuscribirListaBlancaClick(Sender: TObject);
begin
//INICIO: TASK_034_ECA_20160611
	try
        try
        	FormConsultaAltaOtraConsecionaria := TFormConsultaAltaOtraConsecionaria.ModalCreate(nil);
            if FormConsultaAltaOtraConsecionaria.Inicializar(cbConvenio.Value,SuscribirListaBlanca) then begin
	            FormConsultaAltaOtraConsecionaria.ShowModal;
                cbConvenioChange(Sender);
            end;
        except
        	on e: Exception do begin
            	ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	if Assigned(FormConsultaAltaOtraConsecionaria) then FreeAndNil(FormConsultaAltaOtraConsecionaria);
    end;
//FIN: TASK_034_ECA_20160611
end;

{-----------------------------------------------------------------------------
Function Name   :   Inicializar
Author          :
Date            :
Description     :   Carga Datos del cliente en Pantalla
-----------------------------------------------------------------------------}
function TFormInicioConsultaConvenio.Inicializar(CodigoConvenio, PuntoVenta, PuntoEntrega, TabIndex: Integer): Boolean;
resourcestring

    MSG_ERROR_INICIALIZAR   = 'Error al inicializar el formulario. Error: %s.';
    MSG_CAPTION             = 'Datos del Cliente';

    CONST_CODIGO_TIPO_CONVENIO_RNUT='SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_RNUT() ';                    //TASK_004_ECA_20160411
    CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL= 'SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL() '; //TASK_004_ECA_20160411
    CONST_CODIGO_TIPO_CONVENIO_INFRACTOR= 'SELECT dbo.CONST_CODIGO_TIPO_CONVENIO_INFRACTOR() ';         //TASK_004_ECA_20160411
    MSG_SIN_PERMISOS        = 'Usuario no posee permisos para ejecutar esta opci�n';                    //TASK_004_ECA_20160411
var
    SZ: TSize;
    page : Integer;
begin
    try
        TIPO_CONVENIO_RNUT:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_RNUT); //TASK_034_ECA_20160611

        if ExisteAcceso('TIPO_CONVENIO_TODOS') then
             CODIGO_TIPO_CONVENIO:= 0
        else
            if ExisteAcceso('TIPO_CONVENIO_RNUT') then
                CODIGO_TIPO_CONVENIO:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_RNUT)
            else
                if ExisteAcceso('TIPO_CONVENIO_CTA_COMERCIAL') then
                    CODIGO_TIPO_CONVENIO:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_CTA_COMERCIAL)
                else
                    if ExisteAcceso('TIPO_CONVENIO_INFRACTOR') then
                        CODIGO_TIPO_CONVENIO:= QueryGetValueInt(DMConnections.BaseCAC,CONST_CODIGO_TIPO_CONVENIO_INFRACTOR)
                    else begin
                          MsgBox(MSG_SIN_PERMISOS, Caption, MB_ICONWARNING);
                          Result := False;
                          Exit;
                    end;

        for page := 0 to PageControl.PageCount - 1 do
        begin
            if PageControl.Pages[page].Name='tsEstacionamiento' then
               PageControl.Pages[page].TabVisible := false;
            if PageControl.Pages[page].Name='tsListasDeAcceso' then
               PageControl.Pages[page].TabVisible := false;
            //INICIO: TASK_045_ECA_20160702
            if PageControl.Pages[page].Name='tsListaAmarilla' then
               PageControl.Pages[page].TabVisible := false;
            if PageControl.Pages[page].Name='tsRefinanciaciones' then
               PageControl.Pages[page].TabVisible := false;
            //FIN: TASK_045_ECA_20160702
        end;
        PageControl.TabIndex:=0;
        //FIN:TASK_007_ECA_20160428

        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;
                                                                                          //SS_1147_NDR_20141216

        Result := False;
        CambiarEstadoCursor(CURSOR_RELOJ);

        try
            TPanelMensajesForm.MuestraMensaje('Cargando Cartola ...', True);

            SZ := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, sz.cx, SZ.cy);
            // Inicialmente no hay ning�n vehiculo seleccionado
            FListaTransitos             := TStringList.Create;
            FListaEstacionamientos      := TStringList.Create;      // SS_1006_PDO_20120106
            FIndiceVehiculoSeleccionado := -1;
            FEnviar_NK_Via_Mail         := ExisteAcceso('Enviar_NK_Via_Mail');  //SS_1235_MGO_20151113
            // Armamos la fecha y la hora de los transitos inicial
            // y asigno la cantidad de transitos a visualizar
            teTransitosHoraDesde.Time   := StrToTime('00' + TIMESEPARATOR + '00');
            teTransitosHoraHasta.Time   := StrToTime('23' + TIMESEPARATOR + '59');
            EstablecerFechasTransitos(FTransitosDesde, FTransitosHasta);
            deTransitosDesde.Date       := FTransitosDesde;
            deTransitosHasta.Date       := FTransitosHasta;
{            FCantidadTransitos          := neCantidadTransitos.ValueInt;}      //SS 1006 / 1015_MCO_20120828
            //teUCCorte.Time              := EncodeTime(23, 59, 59, 00);        //TASK_060_GLE_20170307
            //EstablecerFechaUC(FUCCorte);                                      //TASK_060_GLE_20170307
            EstablecerFechaUC(FUCCorteDesde);                                   //TASK_060_GLE_20170307
            EstablecerFechaUC(FUCCorteHasta);                                   //TASK_060_GLE_20170307
            //deUCCorte.Date              := FUCCorte;                          //TASK_060_GLE_20170307
            dedtUCFechaDesde.Date       := FUCCorteDesde;                       //TASK_060_GLE_20170307
            dedtUCFechaHasta.Date       := FUCCorteHasta;                       //TASK_060_GLE_20170307
            PageControl.ActivePageIndex := 0;

            TPanelMensajesForm.MuestraMensaje('Cargando Par�metros Sem�foros ...');
            CargarParametrosGenerales;

            CargarConstantesCobranzasRefinanciacion;

            TPanelMensajesForm.MuestraMensaje('Inicializando Objetos Cartola ...');

            Fahora := Trunc(NowBase(DMConnections.BaseCAC));

            lblConvenioCastigado.Visible := False; //TASK_020_GLE_20161121

            // Limpia los Captions
            lblConcesionaria.Caption            := EmptyStr;
            lblEmail.Caption                    := EmptyStr;
            lblEditar.Enabled                   := False;
            lblVerImagenScan.Enabled            := False;
            lblReimprimirNotifTAGsVencidos.Enabled := False;    // SS_960_PDO_20120113
            chk_SuscritoListaBlanca.Enabled := False;

            lblSaldoConvenio.Caption            := EmptyStr;
            lblSaldoConvenio.Font.Color         := clBlack;
            lblUltimoAjusteSencillo.Caption		:= EmptyStr; //-SS-877-NDR-20101027
            lblDeudaComprobantesCuotas.Caption  := EmptyStr;
            lblDeudaRef.Caption		           	:= EmptyStr;
            lblDeudaRef.Font.Color              := clBlack;
            lblDeudaRef.Font.Style              := lblDeudaRef.Font.Style - [fsBold];

            lblIntereses.Caption                := EmptyStr;
            lblPeajeDelPeriodo.Caption          := EmptyStr;
            lblPeajePeriodoAnt.Caption          := EmptyStr;
            lblDescuentos.Caption               := EmptyStr;
            lblOtrosConceptos.Caption           := EmptyStr;
            lblAjusteSencilloAnterior.Caption   := EmptyStr;
            lblTotalAPagar.Caption              := EmptyStr;
            lblAjusteSencilloActual.Caption     := EmptyStr;

            lblFechaProximaFacturacion.Caption  := EmptyStr;
            lblInicioProximaFacturacion.Caption := EmptyStr;
            lblFinProximaFacturacion.Caption    := EmptyStr;

            lblEstadoConvenio.Caption           := ESTADO_CONVENIO + ' --- ';
            lblRNUT.Caption                     := ESTADO_RNUT + ' --- ';


            CheckList.CreateDataSet;
            pnlEstadoCuenta.Caption := Format(MSG_ESTADO_CUENTA_AL,
              [FormatDateTime('dd mmm', NowBase(DMConnections.BaseCAC)), FormatDateTime('hh:nn', Now)]);

            // Asignamos permisos a label de Ver Tansitos en Formato CSVr
            lblCSV.Visible := ExisteAcceso('VER_TRANSITOS_FORMATO_CSV');


            FPuntoVenta     := PuntoVenta;
            FPuntoEntrega   := PuntoEntrega;

            peNumeroDocumento.Text := Trim(QueryGetValue(DMConnections.BaseCAC, Format(
                'SELECT Personas.NumeroDocumento FROM Personas WITH (NOLOCK) , Convenio  WITH (NOLOCK) ' +
                'WHERE Personas.CodigoPersona = Convenio.CodigoCliente ' +
                'AND Convenio.CodigoConvenio = %d', [CodigoConvenio])));

            //Solo muestro los datos del cliente si hay un RUT cargado
            if peNumeroDocumento.text <> EmptyStr then begin
                CargarDatosBasicos;
                cbConvenio.Value := CodigoConvenio;
                cbConvenioChange(cbConvenio);
            end;

            if cbConvenio.Items.Count > 0 then begin
                Caption := MSG_CAPTION + ' - ' + cbConvenio.Items[cbConvenio.ItemIndex].Caption;
            end else begin
                Caption := MSG_CAPTION;
            end;
            // Algunos seteos por Defecto finales...
            PageControl.ActivePageIndex     := TabIndex;
            neNotificacionesTop.ValueInt    := 100;
            deNotificacionesDesde.Date      := IncMonth(NowBase(DMConnections.BaseCAC), -1);
            deNotificacionesHasta.Date      := NowBase(DMConnections.BaseCAC);
            cpOpciones.Animated             := False;
            cpOpciones.Open                 := False;
            cpBusquedaConvenio.Animated     := False;
            cpBusquedaConvenio.Open         := True;

            cpVehiculo.Open := True;
            cpVehiculo.Open := False;
            cpVehiculo.Animated             := False;

            DBListExResaltaCamposIndex(DBLstExRefinanResumenCC);

            //REV.23
    	// Rev.22 / 07-Mayo-2010 / Nelson Droguett Sierra
    	//CargarConcesionarias(DMConnections.BaseCAC,cbConcesionaria,0,True);
            CargarComboConcesionarias(DMConnections.BaseCAC, vcbConcesionariasTransa, QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.CONST_TIPO_CONCESIONARIA_AUTOPISTA()'), True);                                 //PAR00133-FASE2-NDR-20110624
    //vcbConcesionariasUC.Items.Assign(vcbConcesionariasTransa.Items);                                                                                                                                          //PAR00133-FASE2-NDR-20110624
    //vcbConcesionariasEstacionamiento.Items.Assign(vcbConcesionariasTransa.Items);                                                                                                                             //PAR00133-FASE2-NDR-20110624
//    CargarComboConcesionarias(DMConnections.BaseCAC, vcbConcesionariasUC, True);                                                                                                                                //PAR00133-FASE2-NDR-20110624 // PAR00133_CORRECCIONES_PDO_20110705
            CargarComboConcesionarias(DMConnections.BaseCAC, vcbConcesionariasEstacionamiento, QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.CONST_TIPO_CONCESIONARIA_ESTACIONAMIENTO()'), True);                  //PAR00133-FASE2-NDR-20110624
            //FIN REV.23

            PageControl.TabIndex:=0;  //SS-740-NDR-20110725
            Result := True;

            
        except
            on e: Exception do begin
                ShowMsgBoxCN(Self.Caption, Format(MSG_ERROR_INICIALIZAR, [e.Message]), MB_ICONERROR, Self);
//                MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, Self.Caption, MB_ICONERROR);
            end;
        end; // except
        
    finally
        Led1.ShowHint := true;
        Led2.ShowHint := true;
        Led3.ShowHint := true;
        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO);
        //PageControl.ActivePage:=tsObservaciones;                                //SS_1301_NDR_20150609    //TASK_066_GLE_20170409
        PageControl.ActivePage:= tsTransacciones;                                                           //TASK_066_GLE_20170409
        ActiveControl := peNumeroDocumento;                                     //SS_1301_NDR_20150609
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   FormClose
Author          :
Date Created    :   /  /
Description     :   Cierra el Form
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    if Assigned(FListaTransitos) then FreeAndNil(FListaTransitos);
    if Assigned(FListaEstacionamientos) then FreeAndNil(FListaEstacionamientos);
    Action := caFree;
end;

{-----------------------------------------------------------------------------
Function Name   :   dblPagosContextPopup
Author          :
Date Created    :   /  /
Description     :   Muestra el men� Cotextual para Pagos
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblPagosAutomaticosClick(Sender: TObject);
begin
    Sender := Sender;
end;

procedure TFormInicioConsultaConvenio.dblPagosContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
	if (not dblPagos.DataSource.DataSet.IsEmpty)
            and (not dblPagos.DataSource.DataSet.FieldByName('NumeroRecibo').IsNull) then begin

        Handled := MostrarMenuContextualPago(dblPagos.ClientToScreen(MousePos),
            dblPagos.DataSource.DataSet.FieldByName('NumeroRecibo').Value, spObtenerPagosConvenio);
    end;
end;

{******************************************************************************
Function Name   :   TFormInicioConsultaConvenio.lblCSVClick
Author          :   gcasais
Date Created    :   15/07/2005
Description     :   Exporta el resultado de la consulta a formato CSV
*******************************************************************************}
procedure TFormInicioConsultaConvenio.lblCSVClick(Sender: TObject);
resourcestring
    MSG_TITLE   = 'Exportar CSV';
    MSG_ERROR   = 'ERROR';
    MSG_SUCCESS = 'El archivo %s fu� creado exitosamente';
var
    FileBuffer, Error: String;
    NombreArchivo: String;
begin
    if dblTransitos.DataSource.DataSet.IsEmpty then Exit;
    if DatasetToExcelCSV(dblTransitos.DataSource.DataSet, FileBuffer,
        Error) then begin

        NombreArchivo := peNumeroDocumento.Text
            + '_' + cbConvenio.Text
            + '_Transacciones_del_'
            + FormatDateTime ('yyyyMMdd', deTransitosDesde.Date)
            + '_al_' + FormatDateTime ('yyyyMMdd', deTransitosHasta.Date)
            + '.csv';
            sdGuardarCSV.FileName := NombreArchivo;


        if sdGuardarCSV.Execute then begin
            StringToFile(FileBuffer, sdGuardarCSV.FileName);
            MsgBox(Format(MSG_SUCCESS, [sdGuardarCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
        end;
    end else begin
        MsgBoxErr(MSG_ERROR, Error, MSG_ERROR, MB_ICONERROR);
    end;
    sdGuardarCSV.FileName := EmptyStr;
end;

{******************************************************************************
Function Name   :   FiltrarCuentas
Author          :   gcasais
Date Created    :   27/07/2005
Description     :
*******************************************************************************}
{INICIO: 	20160707 CFU
procedure TFormInicioConsultaConvenio.FiltrarCuentas(const Datos: TADOStoredProc; CodigoConvenio: Integer; MostrarBajas: Boolean);
}
procedure TFormInicioConsultaConvenio.FiltrarCuentas(const Datos: TADOStoredProc; CodigoConvenio: Integer; Patente: string; MostrarBajas: Boolean);
//TERMINO:	20160707 CFU
begin
    Datos.Close;
    Datos.Parameters.ParamByName('@CodigoConvenio').Value   := CodigoConvenio;
    //INICIO:	20160707 CFU
    Datos.Parameters.ParamByName('@Patente').Value       	:= Patente;
    //TERMINO:	20160707 CFU
    Datos.Parameters.ParamByName('@TraerBajas').Value       := MostrarBajas;
    Datos.Open;
end;

{******************************************************************************
Function Name   :   IrAConvenio
Author          :
Date Created    :
Description     :   Muestra Convenio si esta activa la condici�n
*******************************************************************************}
procedure TFormInicioConsultaConvenio.IrAConvenio(const CodigoConvenio: Integer);
begin
    if CodigoConvenio = cbConvenio.Value then Exit;
    MostrarConvenio(CodigoConvenio);
end;

{******************************************************************************
Function Name   :   MostrarConvenio
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   Se obtienen solo los datos de los vehiculos y la facturacion.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.MostrarConvenio(const CodigoConvenio: Integer);

const
    MSG_GRUPO_FACT = '%d, %s';
    //SQLDeudaConvenios = 'SELECT dbo.ObtenerSaldoDeLosConvenios(%d)';                      //SS-1006-NDR-20120614 // ***NO SE USA***
    SQLMotivoMorosidad = 'SELECT dbo.CONST_CODIGO_MOTIVO_INHABILITACION_POR_MOROSIDAD()';   //SS-1006-NDR-20120614
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'')';      // SS_660_CQU_20130711  // SS_660_MCO_20130221
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'', NULL)';  //SS_660_MVI_20130729 // SS_660_CQU_20130711
    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d) ';                                                //SS_660_MVI_20130729
var
    Empresa, EstadoListaAmarilla: string;                                                                                                    //SS_660_MVI_20130729
    TotalDeudasRefinanciacion,
    DeudaRefinanciacionVencida,
    DeudaComprobantesVencida: Extended;
    FHayConvenioCastigado, CodConvCastigado : Integer;                                                        //TASK_049_GLE_20170207
    NumConvenio : string;                                                                                     //TASK_049_GLE_20170207
resourcestring													                                                    //SS_1147Q_20141202
    MSG_CAPTION             = 'Datos del Cliente';				                                  //SS_1147Q_20141202
begin


    if cbConvenio.Items.Count > 0 then begin                                               //SS_1147Q_20141202
       Caption := MSG_CAPTION + ' - ' + cbConvenio.Items[cbConvenio.ItemIndex].Caption;    //SS_1147Q_20141202
    end else begin                                                                         //SS_1147Q_20141202
       Caption := MSG_CAPTION;                                                             //SS_1147Q_20141202
    end;                                                                                   //SS_1147Q_20141202

    Screen.Cursor := crHourglass;
    try
		ActualizarDomicilioFacturacion;
        //
        ObtenerConvenioCartola.Close;
		    ObtenerConvenioCartola.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        ObtenerConvenioCartola.Open;

        if ObtenerConvenioCartola.RecordCount > 0 then begin

        //INICIA: TASK_049_GLE_20170207
            //Busca si el convenio seleccionado tiene comprobantes castigados para visualizar mensaje de castigo
            CodConvCastigado := QueryGetValueInt(DMConnections.BaseCAC, Format(
                'SELECT CodigoConvenio FROM Convenio (NOLOCK) WHERE NumeroConvenio = ''%s''',
                [ObtenerConvenioCartola.FieldByName('NumeroConvenio').AsString]));
            FHayConvenioCastigado := QueryGetValueInt(DMConnections.BaseCAC, Format(
                'SELECT COUNT(*) FROM ComprobantesCastigados (NOLOCK) WHERE EstadoPago = ''X'' AND CodigoConvenio = ''%d''',
                [CodConvCastigado]));

            if FHayConvenioCastigado >0 then begin
                lblConvenioCastigado.visible := True;
            end else begin
                lblConvenioCastigado.visible := False;
            end;
        //TERMINA: TASK_049_GLE_20170207

            lblEstadoConvenio.Caption := ESTADO_CONVENIO +
              ObtenerConvenioCartola.FieldByName('DescripcionEstadoConvenio').AsString;

            lblRNUT.Caption := ESTADO_RNUT + ObtenerConvenioCartola.FieldByName('CodigoConvenioExterno').AsString;

            //INICIO: TASK_012_ECA_20160514
            if CodigoConvenio=CUENTA_COMERCIAL_PREDETERMINADA then
               lblEstadoConvenio.Caption := ESTADO_CONVENIO + ObtenerConvenioCartola.FieldByName('DescripcionEstadoConvenio').AsString + ' Predeterminado';

            if Pos('COMERCIAL',cbConvenio.Text) > 0   then
               lblRNUT.Caption := 'Cuenta Comercial: ' + ObtenerConvenioCartola.FieldByName('NumeroConvenio').AsString;

            if Pos('INFRACTOR',cbConvenio.Text) > 0   then
               lblRNUT.Caption := 'Convenio Infractor: ' + ObtenerConvenioCartola.FieldByName('NumeroConvenio').AsString;
            //FIN: TASK_012_ECA_20160514

            lblConcesionaria.Caption := ObtenerConvenioCartola.FieldByName('DescripConcesionaria').AsString;
            lblUltimoAjusteSencillo.Caption := FormatFloat(FORMATO_IMPORTE, (ObtenerConvenioCartola.FieldByName('UltimoAjusteSencillo').AsVariant div 100)); //SS-877-NDR-20101027

            lblNovedad.Caption := iif(ObtenerConvenioCartola.FieldByName('Novedad').Value = '', 'Sin novedad',ObtenerConvenioCartola.FieldByName('Novedad').Value);      //TASK_078_JMA_20161128
        end else begin
            lblEstadoConvenio.Caption   := ESTADO_CONVENIO + ' --- ';
            lblRNUT.Caption             := ESTADO_RNUT + ' --- ';
            lblConcesionaria.Caption    := EmptyStr;
            lblNovedad.Caption := 'Sin novedad';                                                       //TASK_078_JMA_20161128
        end;

        //INICIO:TASK_034_ECA_20160611
        chk_SuscritoListaBlanca.Checked:=ObtenerConvenioCartola.FieldByName('SuscritoListaBlanca').AsBoolean; //TASK_034_ECA_20160611                                 //SS-1006-NDR-20120614

        //INICIO: TASK_045_ECA_20160702
        btnSuscribirListaBlanca.Enabled := False;
        btnSuscribirListaBlanca.Visible := False;
        chk_SuscritoListaBlanca.Visible := False;
        if (ObtenerConvenioCartola.FieldByName('CodigoTipoConvenio').AsInteger=TIPO_CONVENIO_RNUT) and (UpperCase(ObtenerConvenioCartola.FieldByName('DescripcionEstadoConvenio').AsString)='VIGENTE') then
        begin
            if not ObtenerConvenioCartola.FieldByName('ConvenioNativo').AsBoolean then
                if chk_SuscritoListaBlanca.Checked then
                begin
                   SuscribirListaBlanca:=False;
                   btnSuscribirListaBlanca.Caption:='Desuscribir Lista Blanca';
                   btnSuscribirListaBlanca.Enabled := True;
                end
                else
                begin
                   SuscribirListaBlanca:=True;
                   btnSuscribirListaBlanca.Caption:='Suscribir Lista Blanca';
                   btnSuscribirListaBlanca.Enabled := True;
                end;
            btnSuscribirListaBlanca.Visible := True;
            chk_SuscritoListaBlanca.Visible := True;
        end;
        //FIN: TASK_045_ECA_20160702
        //FIN: TASK_034_ECA_20160611

        //cb_Inhabilitado.Checked:=ObtenerConvenioCartola.FieldByName('CodigoMotivoInhabilitacion').AsInteger>0;              //SS-1006-NDR-20120614 TASK_001_ECA_20160304
        lblMotivoInhabilitacion.Caption:=ObtenerConvenioCartola.FieldByName('DescripcionMotivoInhabilitacion').AsString;    //SS-1006-NDR-20120614
        lblMotivoInhabilitacion.Font.Color:=IfThen(ObtenerConvenioCartola.FieldByName('CodigoMotivoInhabilitacion').AsInteger=QueryGetValueInt(DMConnections.BaseCAC, SQLMotivoMorosidad),clRed,clWindowText);    //SS-1006-NDR-20120614

        lblEnListaAmarilla.Color:=clYellow;                                                                                                  //SS_660_MCO_20130211
        //lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                                                             //SS_660_MVI_20130729  //SS_660_MCO_20130221
        //                                                Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                            //SS_660_MVI_20130729  //SS_660_MCO_20130221
        //                                                        [   CodigoConvenio,                                                        //SS_660_MVI_20130729  //SS_660_MCO_20130221
        //                                                            FormatDateTime('YYYYMMDD HH:NN:SS',NowBaseCN(DMConnections.BaseCAC))   //SS_660_MVI_20130729  //SS_660_MCO_20130221
        //                                                        ]                                                                          //SS_660_MVI_20130729  //SS_660_MCO_20130221
        //                                                      )                                                                            //SS_660_MVI_20130729  //SS_660_MCO_20130221
        //                                             )='True';                                                                             //SS_660_MVI_20130729  //SS_660_MCO_20130221


        EstadoListaAmarilla:= QueryGetValue(   DMConnections.BaseCAC,                                                                         //SS_660_MVI_20130729
                                                 Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                         [   CodigoConvenio                                                                 //SS_660_MVI_20130729
                                                         ]                                                                                   //SS_660_MVI_20130729
                                                       )                                                                                     //SS_660_MVI_20130729
                                                       );                                                                                    //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
//        if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
        if EstadoListaAmarilla <> '' then begin                                                                                              //SS_660_MVI_20130909                                               //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
        lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end                                                                                                                                  //SS_660_MVI_20130729
        else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end;                                                                                                                                 //SS_660_MVI_20130729



        //Permito ver imagen si tiene
        lblVerImagenScan.Enabled := ObtenerConvenioCartola.FieldByName('TieneImagenScan').AsBoolean;

        ObtenerConvenioCartola.Close;

        //Obtengo los vehiculos para ese convenio (Cuentas)
        {INICIO:	20160707 CFU
        FiltrarCuentas(spObtenerCuentasCartola, Integer(cbConvenio.Value), cb_MostrarBajas.Checked);
        }
        FiltrarCuentas(spObtenerCuentasCartola, Integer(cbConvenio.Value), edtFiltrarPatente.Text, cb_MostrarBajas.Checked);
        //TERMINO:	20160707 CFU
        CargarComboPatentes;                                                    //SS-1006-NDR-20120614
        //Obtengo la Proxima Facturacion del Convenio
   		spFechasProximaFacturacion.Close ;
        spFechasProximaFacturacion.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        spFechasProximaFacturacion.Open;
        if (spFechasProximaFacturacion.IsEmpty) OR
           (spFechasProximaFacturacion.FieldByName('Ejecucion').IsNull)
           then begin
            lblFechaProximaFacturacion.Caption  := '';
            lblInicioProximaFacturacion.Caption := '';
            lblFinProximaFacturacion.Caption    := '';
           end else begin
            lblFechaProximaFacturacion.Caption  := FormatDateTime('dd/mm/yyyy',spFechasProximaFacturacion.FieldByName('Ejecucion').AsDateTime);
            lblInicioProximaFacturacion.Caption := FormatDateTime('dd/mm/yyyy',spFechasProximaFacturacion.FieldByName('Inicio').AsDateTime);
            lblFinProximaFacturacion.Caption    := FormatDateTime('dd/mm/yyyy',spFechasProximaFacturacion.FieldByName('Corte').AsDateTime);
        end  ;

        with spObtenerDeudaComprobantesVencidos do begin
            if Active then Close;
            Parameters.Refresh;

            Parameters.ParamByName('@CodigoConvenio').Value := Null;
            Parameters.ParamByName('@CodigoCliente').Value  := FCodigoPersona;
            Open;

            DeudaComprobantesVencida := FieldByName('DeudaComprobantesVencidos').AsFloat;
        end;


        //obtengo el resumen de deuda de ese convenio
        with ObtenerResumenDeudaConvenio do begin
            Close;
            Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
            Open;

            pnlEstadoCuenta.Caption := Format(MSG_ESTADO_CUENTA_AL,
                [FormatDateTime('dd mmm', Now), FormatDateTime('hh:nn', Now)]);

            lblSaldoConvenio.Caption            := FormatFloat(FORMATO_IMPORTE, (FieldByName('DeudaAnterior').AsVariant div 100));
            if DeudaComprobantesVencida > 0 then begin
                lblSaldoConvenio.Font.Color := clRed;
                ShowMsgBoxCN('Validaci�n Comprobantes Vencidos', 'El Cliente tiene Comprobantes pendientes de Pago Vencidos, en este u en otro(s) Convenios(s).', MB_ICONWARNING, Self);
            end;

            lblDeudaComprobantesCuotas.Caption  := FormatFloat(FORMATO_IMPORTE, (FieldByName('DeudaComprobantesDeDeuda').AsVariant div 100));
            lblIntereses.Caption                := FormatFloat(FORMATO_IMPORTE, (FieldByName('DeudaIntereses').AsVariant div 100));
            lblPeajeDelPeriodo.Caption          := FormatFloat(FORMATO_IMPORTE, (FieldByName('DeudaPeaje').AsVariant div 100));
            lblDescuentos.Caption               := FormatFloat(FORMATO_IMPORTE, (FieldByName('DeudaDescuentos').AsVariant div 100));
            lblOtrosConceptos.Caption           := FormatFloat(FORMATO_IMPORTE, FieldByName('DeudaOtros').AsVariant div 100);
            lblAjusteSencilloAnterior.Caption   := FormatFloat(FORMATO_IMPORTE, FieldByName('AjusteSencilloAnterior').AsInteger div 100);

            lblPeajePeriodoAnt.Caption          := FormatFloat(FORMATO_IMPORTE, FieldByName('DeudaPeajeAnteriores').AsVariant div 100);
			lblTotalAPagar.Caption              := FormatFloat(FORMATO_IMPORTE, FieldByName('TotalComprobante').AsVariant div 100);
            lblAjusteSencilloActual.Caption     := FormatFloat(FORMATO_IMPORTE,FieldByName('AjusteSencilloActual').AsInteger div 100);
            Close;
            if ChequearConvenioEnCarpetaLegal(cbConvenio.Value) then begin
                Empresa := Trim(ObtenerEmpresaConvenioEnCarpetaLegal(cbConvenio.Value));
                if Empresa = '' then
                    MsgBox(MSG_CLIENTE_EN_PROCESO_LEGAL, self.Caption, MB_ICONWARNING)
                else MsgBox(Format(MSG_CLIENTE_EN_PROCESO_LEGAL_ASIGNADO_A_EMPRESA, [Empresa]), self.Caption, MB_ICONWARNING)
            end;
        end;
        // Muestra el grupo de facturaci�n de este convenio
        lblGrupo.Caption := MostrarGrupoFacturacion(MSG_GRUPO_FACT, CodigoConvenio);

            with spObtenerRefinanciacionesTotalesPersona do begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value  := FCodigoPersona;
                Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
                Open;

                TotalDeudasRefinanciacion  := FieldByName('TotalDeuda').AsFloat - FieldByName('TotalPagado').AsFloat;
                DeudaRefinanciacionVencida := FieldByName('DeudaVencida').AsFloat;
            end;

            lblDeudaRef.Caption := FormatFloat(FORMATO_IMPORTE, TotalDeudasRefinanciacion);
            if DeudaRefinanciacionVencida > 0 then begin
                lblDeudaRef.Font.Color := clred;
                lblDeudaRef.Font.Style := lblDeudaRef.Font.Style + [fsBold];
                ShowMsgBoxCN(MSG_AVISO_REF_DEUDA_VENCIDA_TITULO, MSG_AVISO__REF_DEUDA_VENCIDA_MENSAJE, MB_ICONWARNING, Self)
            end;


{        spRefinanciacionBuscarPendientes.Close;
        spRefinanciacionBuscarPendientes.Parameters.ParamByName('@RUT').Value := peNumeroDocumento.Text;
        spRefinanciacionBuscarPendientes.Parameters.ParamByName('@DeudaTotal').Value := Null;
        spRefinanciacionBuscarPendientes.Open;
        lblDeudaRef.Caption := FormatFloat(FORMATO_IMPORTE,spRefinanciacionBuscarPendientes.Parameters.ParamByName('@DeudaTotal').Value);
        spRefinanciacionBuscarPendientes.Close;}
        if cbConvenio.Showing then                                              //SS-1006-1015-MCO-20120927
        cbConvenio.SetFocus;                                                    //SS-1006-1015-MCO-20120903  

        //BuscarObservaciones;                                                    //SS-842-NDR-20110912     //TASK_066_GLE_20170409
        BuscarHistoricosListasDeAcceso;                                         //SS-1006-NDR-20120614
        BuscarHistoricoListaAmarilla;                                           // SS_660_MCO_20132502



    finally
    	Screen.Cursor := crDefault;

    end;
end;

{******************************************************************************
Function Name   :   MostrarGrupoFacturacion
Author          :
Date Created    :
Description     :   Muestra Grupo Facturaci�n seg�n Codigo Convenio
*******************************************************************************}
function TFormInicioConsultaConvenio.MostrarGrupoFacturacion(
  const Texto: string; const CodigoConvenio: Integer): string;
var
    sp: TADOStoredProc;
begin
    sp                  := TADOStoredProc.Create(nil);
    sp.Connection       := DMConnections.BaseCAC;
    sp.ProcedureName    := 'CartolaObtenerGrupoFacturacionConvenio';
    try
        try
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            sp.Open;
            if not sp.IsEmpty then begin
                Result := Format(Texto, [sp.FieldByName('Grupo').AsInteger, sp.FieldByName('Descripcion').AsString]);
            end else begin
                Result := 'Sin grupo asignado';
            end;
        except
            on e: exception do begin
                Result := 'Error dbo.CartolaObtenerGrupoFacturacionConvenio!';
            end;
        end;
    finally
        sp.Close;
        FreeAndNil(sp);
    end;
end;

{******************************************************************************
Function Name   :   btnBuscarClick
Author          :
Date Created    :
Description     :   Llamado a la funci�n Que Filtra Notificaciones
*******************************************************************************}
procedure TFormInicioConsultaConvenio.btnBuscarClick(Sender: TObject);
begin
    FiltrarNotificaciones;
end;

{******************************************************************************
Function Name   :   ValidarParametrosNotificaciones
Author          :
Date Created    :
Description     :   Funci�n que valida par�metros Notificaciones
*******************************************************************************}
function TFormInicioConsultaConvenio.ValidarParametrosNotificaciones: Boolean;
resourcestring
    MSG_TITLE                           = 'Validaciones';
    MSG_END_DATE_ERROR                  = 'La fecha "Hasta" es inv�lida';
    MSG_START_DATE_ERROR                = 'La fecha "Desde" es inv�lida';
    MSG_FILTER_ERROR                    = 'La cantidad de registros debe ser mayor a cero';
    MSG_END_DATE_LOWER_THAN_START_DATE  = 'La fecha "Hasta" debe ser posterior o igual a la fecha "Desde"';
begin
   Result := ValidateControls([neNotificacionesTop, deNotificacionesDesde,
   deNotificacionesHasta, deNotificacionesHasta],
        [(neNotificacionesTop.ValueInt > 0), (UtilProc.IsValidDate(DateToStr(deNotificacionesDesde.Date))),
            (UtilProc.IsValidDate(DateToStr(deNotificacionesHasta.Date))), (deNotificacionesHasta.Date >= deNotificacionesDesde.Date)],
        MSG_TITLE,
        [MSG_FILTER_ERROR, MSG_START_DATE_ERROR,
            MSG_END_DATE_ERROR, MSG_END_DATE_LOWER_THAN_START_DATE]);
end;

{******************************************************************************
Function Name   :   AplicarOtrosFiltrosNotificaciones
Author          :
Date Created    :
Description     :   Funci�n que se llama desde Filtros Notificaciones para
                    funcionalidad de mails.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.AplicarOtrosFiltrosNotificaciones;
begin
 spExplorarEmails.Filtered := False;
    case rgOtros.ItemIndex of
        0: Exit;
        1:
            begin
                spExplorarEmails.Filter     := 'Enviado = ' + QuotedStr('S�');
                spExplorarEmails.Filtered   := True;

            end;
        2:
            begin
                spExplorarEmails.Filter     := 'Enviado = ' + QuotedStr('No');
                spExplorarEmails.Filtered   := True;
            end;
        3:
            begin
                spExplorarEmails.Filter     := 'Reintentos >= ' +
                  IntToStr(FMaxNotificacionesReintentos);
                spExplorarEmails.Filtered   := True;
            end;
    end;
end;

{******************************************************************************
Function Name   :   mnuComponerEsteMailClick
Author          :
Date Created    :
Description     :   Hace la llamada a funci�n que permite redactar un mail.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.mnuComponerEsteMailClick(Sender: TObject);
begin
    ComponerEmail;
end;

{******************************************************************************
Function Name   :   FiltrarNotificaciones
Author          :
Date Created    :
Description     :
*******************************************************************************}
procedure TFormInicioConsultaConvenio.FiltrarNotificaciones;
resourcestring
    MSG_ERROR = 'Error';
    MSG_TITLE = 'B�squeda de e-Mails';
begin
    if nenotificacionestop.ValueInt < 1 then begin
        spExplorarEmails.Close;
        exit;
    end;
    if not ValidarParametrosNotificaciones then Exit;
    if cbConvenio.Value<=0 then exit;
    spExplorarEmails.Close;
    spExplorarEmails.Parameters.Refresh;
    case RgTipos.ItemIndex of
        0:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'T';
            end;
        1:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'N';
            end;
        2:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'F';
            end;
        3:
            begin
                spExplorarEmails.Parameters.ParamByName('@Tipo').Value := 'E';
            end;
    end;
    spExplorarEmails.Parameters.ParamByName('@TopFilter').Value         := neNotificacionesTop.ValueInt;
    spExplorarEmails.Parameters.ParamByName('@FechaDesde').Value        := FormatDateTime('yyyymmdd 00:00:00', deNotificacionesDesde.Date);
    spExplorarEmails.Parameters.ParamByName('@FechaHasta').Value        := FormatDateTime('yyyymmdd 23:59:59', deNotificacionesHasta.Date);
    spExplorarEmails.Parameters.ParamByName('@CodigoConvenio').Value    := cbConvenio.Value;
    Cursor := crHourGlass;
    try
        try
            spExplorarEmails.Open;
            //INICIO:	20160707 CFU
            if spExplorarEmails.RecordCount > 0 then
            	btnMostrarEmail.Visible := True
            else
            	btnMostrarEmail.Visible := False;
            //TERMINO:	20160707 CFU
            AplicarOtrosFiltrosNotificaciones;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
            end;
        end;
    finally
        Cursor := crDefault;
        if spExplorarEmails.IsEmpty then begin
            spExplorarEmails.close;
        end;
    end;
end;

{******************************************************************************
Function Name   :   ComponerEmail
Author          :
Date Created    :
Description     :   Levanta Pantalla para redactar un mail.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.ComponerEmail;
var
    f: TfrmComponerMail;
    dm: TDMMensajes;
    MailCompuesto: TMensaje;
    ErrorMsg: String;
begin
    Application.CreateForm(TfrmComponerMail, f);
    dm := TDMMensajes.Create(nil);
    try
        if spExplorarEMails.IsEmpty then  exit;
        if spExplorarEMails.FieldByName('Tipo').Value<>'Email Nota de Cobro' then begin
            { INICIO : 20160531 MGO
            if not f.Inicializar(dm, spExplorarEMails.FieldByName('CodigoMensaje').Value,
              Mailcompuesto, ErrorMsg) then begin
            }
            if not f.Inicializar(dm, spExplorarEMails.FieldByName('CodigoMensaje').Value,
              spExplorarEMails.FieldByName('TipoLetra').Value, Mailcompuesto, ErrorMsg) then begin
            // FIN : 20160531 MGO
                MsgBoxErr('Error', ErrorMsg, 'Componer e-Mail', MB_ICONERROR);
            end else begin
                f.ShowModal;
            end;
        end;
    finally
        FreeAndNil(DM);
        f.Release;
    end;
end;


{******************************************************************************
Function Name   :   IrARUT
Author          :
Date Created    :
Description     :   Configura nuevo Rut en pantalla
*******************************************************************************}
procedure TFormInicioConsultaConvenio.IrARUT(const NumeroRUT: Ansistring);
begin
    if WindowState <> wsNormal then WindowState := wsNormal;
    if peNumeroDocumento.Text                   = NumeroRut then Exit;
    peNumeroDocumento.Text                      := NumeroRut;
    peNumeroDocumento.OnChange(Self);
end;

{******************************************************************************
Function Name   :   FormCreate
Author          :
Date Created    :
Description     :   Evento que Permite mostrar Pantalla
*******************************************************************************}
procedure TFormInicioConsultaConvenio.FormCreate(Sender: TObject);
begin
    FFormState := FFormState - [fsVisible];

//    self.Show;
end;

{******************************************************************************
Function Name   :   mnuComponerPopup
Author          :
Date Created    :
Description     :   Permite Habilitar la opci�n que Compone E-mail
*******************************************************************************}
procedure TFormInicioConsultaConvenio.mnuComponerPopup(Sender: TObject);
begin
    mnuComponer.Items[0].Enabled :=
      (dsEmails.DataSet.Active) and (dsEmails.DataSet.RecordCount > 0)
end;

{******************************************************************************
Function Name   :   OrdenarColumna
Author          :
Date Created    :
Description     :   Funci�n que permite Ordenar la grilla por columna seleccionada
*******************************************************************************}
//INICIA: TASK_066_GLE_20170409
{
procedure TFormInicioConsultaConvenio.ObtenerConvenioObservacionAfterClose(
  DataSet: TDataSet);
begin
    memObservaciones.Lines.Clear;
end;

procedure TFormInicioConsultaConvenio.ObtenerConvenioObservacionAfterOpen(
  DataSet: TDataSet);
begin
    if ObtenerConvenioObservacion.RecordCount > 0 then begin
        memObservaciones.Lines.Text := ObtenerConvenioObservacion.FieldByName('Observacion').AsString;
    end
    else memObservaciones.Lines.Clear;
end;

procedure TFormInicioConsultaConvenio.ObtenerConvenioObservacionAfterScroll(
  DataSet: TDataSet);
begin
    memObservaciones.Lines.Text := ObtenerConvenioObservacion.FieldByName('Observacion').AsString;
end;
}
//TERMINA: TASK_066_GLE_20170409

procedure TFormInicioConsultaConvenio.OrdenarColumna(sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;

{*******************************************************************************
Function Name   :   BuscarObservaciones
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   Busca las observaciones del convenio solicitado
*******************************************************************************}
//INICIO: TASK_066_GLE_20170409
{
procedure TFormInicioConsultaConvenio.BuscarObservaciones;
ResourceString
    TXT_ERROR_OBSERVACIONES = 'Error buscando las observaciones del Convenio ';
    MSG_ERROR               = 'Error';
begin
    try
        //txtObservaciones.Clear;                                                                                               //SS-842-NDR-20110912
        ObtenerConvenioObservacion.Close;
        ObtenerConvenioObservacion.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        if deHistoricoDesde.Date <> NullDate then                                                              //SS-842-NDR-20110912
            ObtenerConvenioObservacion.Parameters.ParamByName('@FechaDesde').Value := deHistoricoDesde.Date                     //SS-842-NDR-20110912
        else                                                                                                                    //SS-842-NDR-20110912
            ObtenerConvenioObservacion.Parameters.ParamByName('@FechaDesde').Value := Null;                                     //SS-842-NDR-20110912

        if deHistoricoHasta.Date <> NullDate then                                                              //SS-842-NDR-20110912
            ObtenerConvenioObservacion.Parameters.ParamByName('@FechaHasta').Value := deHistoricoHasta.Date                     //SS-842-NDR-20110912
        else                                                                                                                    //SS-842-NDR-20110912
            ObtenerConvenioObservacion.Parameters.ParamByName('@FechaHasta').Value := Null;                                     //SS-842-NDR-20110912

        ObtenerConvenioObservacion.Parameters.ParamByName('@IncluirDeBaja').Value := chkObservacionesBaja.Checked;              //SS_842_PDO_20111212

        ObtenerConvenioObservacion.Open;
        //while not ObtenerConvenioObservacion.Eof do begin                                                                     //SS-842-NDR-20110912
            //txtObservaciones.Lines.Add(ObtenerConvenioObservacion.FieldByName('Observacion').AsString);                       //SS-842-NDR-20110912
        //    ObtenerConvenioObservacion.Next;                                                                                  //SS-842-NDR-20110912
        //end;                                                                                                                  //SS-842-NDR-20110912
        //Obtengo el Checklist
        CheckList.Close;
        CheckList.CreateDataSet;
        spObtenerCheckListConvenioCartola.Close;
    		spObtenerCheckListConvenioCartola.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        spObtenerCheckListConvenioCartola.Open;
        while not spObtenerCheckListConvenioCartola.Eof do begin
            Checklist.Append;
            CheckList.FieldByName('CodigoCheckList').Assign(spObtenerCheckListConvenioCartola.FieldByName('CodigoCheckList'));
            CheckList.FieldByName('Descripcion').Assign(spObtenerCheckListConvenioCartola.FieldByName('Descripcion'));
            CheckList.FieldByName('Vencimiento').Assign(spObtenerCheckListConvenioCartola.FieldByName('Vencimiento'));
            CheckList.FieldByName('Observaciones').Assign(spObtenerCheckListConvenioCartola.FieldByName('Observaciones'));
            Checklist.Post;
            spObtenerCheckListConvenioCartola.Next;
        end;
        spObtenerCheckListConvenioCartola.Close;

    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_OBSERVACIONES, e.Message, MSG_ERROR, MB_ICONWARNING);
        end;
    end;
    //ObtenerConvenioObservacion.Close;
end;
}
//TASK_066_GLE_20170409

{******************************************************************************
Function Name   :   BtnFiltrarObservacionesClick
Author          :
Date Created    :
Description     :   Permite hacer llamada a la funci�n que
                    Busca Observaciones asociados al convenio
*******************************************************************************}
//INICIA: TASK_066_GLE_20170409
{
procedure TFormInicioConsultaConvenio.BtnFiltrarObservacionesClick(
  Sender: TObject);
begin
    BuscarObservaciones;
end;
}
//TERMINA: TASK_066_GLE_20170409

{******************************************************************************
Function Name   :   BuscarComprobantes
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   Busca los comprobantes asociados al convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarComprobantes;
ResourceString
    TXT_COMPROBANTES_ERROR = 'Error buscando Comprobantes del Convenio';
    TXT_ERROR              = 'Error';
begin
    try
        //Obtengo los Comprobantes de ese convenio
        spObtenerComprobantesConvenio.Close;
        spObtenerComprobantesConvenio.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        spObtenerComprobantesConvenio.Open;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_COMPROBANTES_ERROR, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;

end;

{******************************************************************************
Function Name       :   BuscarPagosConvenio
Author              :   llazarte
Date Created        :   14/03/2006
Description         :   Busca los pagos que tiene aplicado el convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarPagosConvenio;
ResourceString
    TXT_ERROR_PAGOS = 'Error buscando los pagos del Convenio';
    TXT_ERROR       = 'Error';
begin
    try
        spObtenerPagosConvenio.Close;
        spObtenerPagosConvenio.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        spObtenerPagosConvenio.Open;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_PAGOS, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;

end;


{INICIO: TASK_006_JMA_20160420}
procedure TFormInicioConsultaConvenio.BuscarPagosAutomaticosConvenio;
ResourceString
    TXT_ERROR_PAGOS = 'Error buscando los pagos automaticos del Convenio';
    TXT_ERROR       = 'Error';
begin
    if (cbConvenio.Items.Count> 0) and (cbConvenio.ItemIndex >= 0) then
    begin
        try
            if Assigned(spPagosAutomaticos) then
                spPagosAutomaticos.Close;
            spPagosAutomaticos := TADOStoredProc.Create(nil);
            spPagosAutomaticos.Connection := DMConnections.BaseCAC;
            spPagosAutomaticos.ProcedureName := 'CRM_ObtenerDetalleComprobantesRecibidos_SELECT';
            spPagosAutomaticos.Parameters.Refresh;
            spPagosAutomaticos.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
            spPagosAutomaticos.Parameters.ParamByName('@EstadoPago').Value := iif(cbbMostrarPagosAutomaticos.Value = -1 , Null, cbbMostrarPagosAutomaticos.Value);
            spPagosAutomaticos.Parameters.ParamByName('@ErrorDescription').Value := Null;

            dsPagosAutomaticos := TDataSource.Create(nil);
            dsPagosAutomaticos.DataSet := spPagosAutomaticos;

            dblPagosAutomaticos.DataSource := dsPagosAutomaticos;
            spPagosAutomaticos.Open;

        except
            on e: Exception do begin
                MsgBoxErr(TXT_ERROR_PAGOS, e.Message, TXT_ERROR, MB_ICONWARNING);
            end;
        end;
    end;
end;



{TERMINO: TASK_006_JMA_20160420}

{*******************************************************************************
Function Name   :   BtnFiltrarComprobantesClick
Author          :
Date Created    :
Description     :   Permite hacer la llamada a la funci�n que obtiene los
                    comprobantes de un c�digo de convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BtnFiltrarComprobantesClick(
  Sender: TObject);
begin
    BuscarComprobantes;
end;

procedure TFormInicioConsultaConvenio.BtnFiltrarPagosClick(Sender: TObject);
begin
    BuscarPagosConvenio;
end;

{******************************************************************************
Function Name   :   BuscarDatosCuentasCorrientes
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   Busca los datos de la cuenta corriente

Firma           :   SS_1046_CQU_20120522
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarDatosCuentasCorrientes;
ResourceString
    TXT_ERROR_CTACTE = 'Error buscando datos de cuenta corriente';
    TXT_ERROR        = 'Error';
begin
    try
        ObtenerResumenCuentaCorriente.Close;
        ObtenerResumenCuentaCorriente.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        ObtenerResumenCuentaCorriente.Open;
        dbResumenCC.Columns[0].Sorting := csAscending;
        OrdenarColumna(dbResumenCC.Columns[0]);
        btnExcel.Enabled := iif((ObtenerResumenCuentaCorriente.RecordCount > 0),True,False);    // SS_1046_CQU_20120522
        btnExcel.Visible := ExisteAcceso('Boton_Exportar_CtaCte_Excel');                        // SS_1046_CQU_20120530
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_CTACTE, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;
end;

{******************************************************************************
Function Name   :   BtnCtaCteClick
Author          :
Date Created    :
Description     :   Bot�n que hace la llamada a la funci�n que Trae los datos
                    De cuenta corriente de un c�digo de convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BtnCtaCteClick(Sender: TObject);
begin
    BuscarDatosCuentasCorrientes;
end;

{******************************************************************************
Function Name   :   BuscarReclamos
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   Busca los reclamos asociados a un c�digo de convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarReclamos;
ResourceString
    TXT_ERROR_RECLAMOS = 'Error buscando los reclamos asociados al convenio';
    TXT_ERROR          = 'Error';
begin
    try
        spReclamos.Close;
        if cbConvenio.ItemIndex >= 0 then begin
            spReclamos.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
            spReclamos.Open;
        end;
    except
        on e: exception do begin
            MsgBoxErr(TXT_ERROR_RECLAMOS, e.Message, TXT_ERROR, MB_ICONERROR);
        end;
    end;

end;

{******************************************************************************
Function Name   :   BuscarHistoricoListaAmarilla
Author          :   mcabello
Firma           :   SS_660_MCO_20121214
Date Created    :   14/12/2012
Description     :   Busca el Historico de Lista Amarilla de un convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarHistoricoListaAmarilla;
ResourceString
    TXT_ERROR_HISTORICO = 'Error buscando el Hist�rico de Lista Amarilla';
    TXT_ERROR          = 'Error';
begin
    if cbConvenio.Value > 0 then begin
        try
            spHistoricoListaAmarilla.Close;
            spHistoricoListaAmarilla.Parameters.Refresh;
            spHistoricoListaAmarilla.Parameters.ParamByName('@FechaAltaInicio').Value := NULL;
            spHistoricoListaAmarilla.Parameters.ParamByName('@FechaBajaInicio').Value := NULL;

            if cbConvenio.ItemIndex >= 0 then begin
                spHistoricoListaAmarilla.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
            end;

            //Fecha Desde
            if deHistLADesde.Date <> NullDate then begin
                spHistoricoListaAmarilla.Parameters.ParamByName('@FechaAltaInicio').Value := deHistLADesde.Date;
            end;

            //fecha hasta
            if deHistLAHasta.Date <> NullDate then begin
                spHistoricoListaAmarilla.Parameters.ParamByName('@FechaBajaInicio').Value := deHistLAHasta.Date;
            end;

            spHistoricoListaAmarilla.Open;

            //ObtenerHistoricoDeCartas(cbConvenio.Value); // SS_660_MCA_20130729 // SS_660_CQU_20121010

        except
            on e: Exception do begin
                MsgBoxErr(TXT_ERROR_HISTORICO, e.Message, TXT_ERROR, MB_ICONERROR);
            end;
        end;
    end;
end;


{******************************************************************************
Function Name   :   BtnReclamosClick
Author          :
Date Created    :
Description     :   Bot�n que hace la llamada a funci�n que trae los reclamos
                    asociados a un c�digo de convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BtnReclamosClick(Sender: TObject);
begin
    BuscarReclamos;
end;

{******************************************************************************
Function Name   :   BuscarHistoricoMediosPagos
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   Busca los datos historicos de medios de pago
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarHistoricoMediosPagos;
ResourceString
    TXT_ERROR_HISTORICO_MP = 'Error buscando los datos hist�ricos de Medios de Pago';
    TXT_ERROR              = 'Error';
begin
    try
//         Historico Medio de Pago
        ObtenerHistoricoMedioPago.Close;
        ObtenerHistoricoMedioPago.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        ObtenerHistoricoMedioPago.Open;
        dblHistoricoMedioPago.Columns[0].Sorting := csAscending;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_HISTORICO_MP, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;

end;

{******************************************************************************
Function Name   :   BtnHistoricoMPClick
Author          :
Date Created    :
Description     :   Bot�n que hace la llamada a la funci�n que permite traer
                    los datos historicos de medios de pago
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BtnHistoricoMPClick(Sender: TObject);
begin
    BuscarHistoricoMediosPagos;
end;

{******************************************************************************
Function Name   :   btnIndemnizacionesClick
Author          :
Date Created    :
Description     :   Bot�n que hace la llamada a funci�n que permite traer
                    los datos de las indemnizaciones de televia
*******************************************************************************}
procedure TFormInicioConsultaConvenio.btnIndemnizacionesClick(Sender: TObject);
begin
    BuscarIndemnizaciones;
end;

//REV.1
{*******************************************************************************
Function Name   :   BuscarMensajesComprobantesParticular
Author          :   dcepeda
Date Created    :   17/03/2010
Description     :   Busca los mensajes ya filtrados con campo MensajeParticular = 1
                    asociados a los Comprobantes.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarMensajesComprobantesParticular;
ResourceString
    TXT_MENSAJESCOMPROBANTES_ERROR  = 'Error buscando Mensajes Comprobantes del Convenio';
    TXT_ERROR                       = 'Error';
begin
    try
        //Obtengo los Comprobantes de ese convenio
        spObtenerMensajesComprobanteParticular.Close;
        spObtenerMensajesComprobanteParticular.Parameters.Refresh;
        spObtenerMensajesComprobanteParticular.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        spObtenerMensajesComprobanteParticular.Open;
        DBLMensajesComprobantes.Columns[2].Sorting := csAscending;
        OrdenarColumna(DBLMensajesComprobantes.Columns[2]);
    except
        on e: Exception do begin
            MsgBoxErr(TXT_MENSAJESCOMPROBANTES_ERROR, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;
end;
//Fin REV.1

//REV.1
{******************************************************************************
Function Name   :   btnMensajesComprobantes
Author          :   dcepeda
Date Created    :   18/03/2010
Description     :   Bot�n que es accionado para buscar la funci�n que trae los
                    registros Comprobantes con su(s) mensaje(s)
*******************************************************************************}
procedure TFormInicioConsultaConvenio.btnMensajesComprobantesClick(
  Sender: TObject);
begin
    BuscarMensajesComprobantesParticular;
end;

//INICIO:	20160707 CFU
procedure TFormInicioConsultaConvenio.btnMostrarEmailClick(Sender: TObject);
begin
	ComponerEmail;
end;
//TERMINO:	20160707 CFU

//Fin REV.1

{******************************************************************************
Function Name   :   BuscarIndemnizaciones
Author          :   jconcheyro
Date Created    :   19/12/2006
Description     :   busca los datos de las indemnizaciones de televia
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarIndemnizaciones;
ResourceString
    TXT_ERROR_INDEMNIZACIONES   =   'Error buscando datos de indemnizaciones del convenio';
    TXT_ERROR                   =   'Error';
begin
    try
        // Historico indemnizaciones
        spObtenerIndemnizacionesConvenio.Close;
        spObtenerIndemnizacionesConvenio.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        spObtenerIndemnizacionesConvenio.Open;
        dblIndemnizaciones.Columns[0].Sorting := csAscending;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_INDEMNIZACIONES, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;
end;

{******************************************************************************
Function Name   :   BuscarHistoricoDomicilios
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   busca los datos historicos del domicilio del convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarHistoricoDomicilios;
ResourceString
    TXT_ERROR_HISTORICO_DOM = 'Error buscando datos hist�ricos de domicilio';
    TXT_ERROR               = 'Error';
begin
    try
        // Historico Direccion
        ObtenerHistoricoDomicilios.Close;
        ObtenerHistoricoDomicilios.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        ObtenerHistoricoDomicilios.Open;
        dblHistoricoDomicilios.Columns[0].Sorting := csAscending;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_HISTORICO_DOM, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;

end;

{******************************************************************************
Function Name   :   BtnHistoricoDomClick
Author          :
Date Created    :
Description     :   Bot�n que permite hacer la llamada a funci�n que trae
                    los datos historicos del domicilio asociados al convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BtnHistoricoDomClick(Sender: TObject);
begin
    BuscarHistoricoDomicilios;
end;

{*******************************************************************************
Function Name   :   btnHistoricoListaAmarillaClick
Author          :   MCabelloO
Date Created    :   14/12/2012
Firma           :   SS_660_MCO_20121214
Description     :   Obtiene los datos historicos de Lista Amarilla
*******************************************************************************}
procedure TFormInicioConsultaConvenio.btnHistoricoListaAmarillaClick(Sender: TObject);
begin
    BuscarHistoricoListaAmarilla;
end;

{******************************************************************************
Function Name   :   BuscarAccionesExternas
Author          :   llazarte
Date Created    :   14/03/2006
Description     :   Busca los datos de las acciones de cobranzas Serbanc
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BuscarAccionesExternas;
ResourceString
    TXT_ERROR_ACCIONES_EXTERNAS = 'Error buscando datos de Acciones Cobranzas Serbanc';
    TXT_ERROR                   = 'Error';
begin
    try
        // Obtengo las acciones de Serbanc
        spObtenerAccionesCobranzaSerbanc.Close;
        spObtenerAccionesCobranzaSerbanc.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
        spObtenerAccionesCobranzaSerbanc.Open;
        dblAccionesExtrenas.Columns[0].Sorting := csAscending;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_ACCIONES_EXTERNAS, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;
end;

{******************************************************************************
Function Name   :   BtnAccionesExternasClick
Author          :
Date Created    :
Description     :   Bot�n que permite hacer la llamada a la funci�n que trae
                    los datos de las acciones de cobranzas Serbanc asociadas
                    a un convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.BtnAccionesExternasClick(Sender: TObject);
begin
    BuscarAccionesExternas;
end;

//INICIO	: 20170210 CFU TASK_103_CFU_20170209-CRM_Ver_Descuentos
{******************************************************************************
Function Name   :   BuscarDescuentos
Author          :   cfuentes
Date Created    :   09/02/2017
Description     :   Busca los datos de descuentos del conmvenio/cuentas
*******************************************************************************}
//INICIO	: CFU 20170209 TASK_103_CFU_20170209-CRM_Ver_Descuentos
procedure TFormInicioConsultaConvenio.BuscarDescuentos;
ResourceString
    TXT_ERROR_DESCUENTOS_CONVENIO	= 'Error buscando datos de Descuentos';
    TXT_ERROR                   	= 'Error';
begin
	try
    	with spObtenerDescuentosConvenio, Parameters do begin
            if Active then
            	Close;
            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
            Open;
        end;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_DESCUENTOS_CONVENIO, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;
end;

procedure TFormInicioConsultaConvenio.btnAdministrarDescuentosClick(
  Sender: TObject);
var
	f: TFormAdministrarDescuentos;
begin
	Application.CreateForm(TFormAdministrarDescuentos, f);
    if f.Inicializa(cbConvenio.Value) then begin
    	if (f.ShowModal = mrOk) then begin
        	BuscarDescuentos;
	   	end;
	end;
    f.Release;
end;
//TERMINO	: 20170210 CFU TASK_103_CFU_20170209-CRM_Ver_Descuentos

{*******************************************************************************
Function Name   :   LimpiarDatos
Author          :   dcepeda         (llazarte)
Date Created    :   12-Abril-2010   (14/03/2006)
Description     :   Limpia todas las grillas que se pudieran estar mostrando con datos

Firma           :   SS_1046_CQU_20120525
Description     :   Deshabilita el bot�n Excel cuando se limpia la grilla de Cta. Cte.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.LimpiarDatos;
begin
    // Solapa Observaciones
    //txtObservaciones.Clear;                                                   //SS-842-NDR-20110912
    ObtenerConvenioObservacion.Close;                                           //SS-842-NDR-20110912
    Checklist.EmptyDataSet;
    // Solapa Transacciones
    spObtenerTransitosConvenio.close;
    // Solapa Comprobantes
    spObtenerComprobantesConvenio.Close;
    // Solapa Pagos
    spObtenerPagosConvenio.Close;
{INICIO: TASK_006_JMA_20160420}
    // Solapa Pagos Automaticos
    if Assigned(spPagosAutomaticos) then
        spPagosAutomaticos.Close;
{TERMINO: TASK_006_JMA_20160420}
    // Solapa Cuentas Corrientes
    ObtenerResumenCuentaCorriente.Close;
    btnExcel.Enabled := False;                                                  // SS_1046_CQU_20120525
    // Solapa Reclamos Recibidos
    spReclamos.Close;
    // Ultimos consumos
    spObtenerUltimosConsumos.Close;
    //Historico Medios de Pago
    ObtenerHistoricoMedioPago.Close;
    //Medios de Pago Anteriores
    //SPObtenerMediosPagoAnterioresConvenio.Close;                              //TASK_064_GLE_20170329
    //Historico Domicilios
    ObtenerHistoricoDomicilios.Close;
    //Notificacion de emails
    spExplorarEMails.Close;
    //Acciones Externas
    spObtenerAccionesCobranzaSerbanc.Close;
    // Lista de Cuentas
    spObtenerCuentasCartola.Close;
    //Indemnizaciones de televias
    spObtenerIndemnizacionesConvenio.Close;

    spObtenerMensajesComprobanteParticular.Close;   //REV.1 : Solapa Mensajes Comprobantes

    spObtenerRefinanciacionesTotalesPersona.Close;
    cdsRefinanResumenCC.Close;
    spObtenerEstacionamientos.Close;            // SS_1006_PDO_20120712
    spHistoricoListaAmarilla.Close;             // SS_660_MCO_20121214
    spHistoricoCartasListaAmarilla.Close;       // SS_660_CQU_20121010

    spObtenerDescuentosConvenio.Close;			//TASK_103_CFU_20170209-CRM_Ver_Descuentos

    lblConvenioCastigado.visible:= False;       //TASK_049_GLE_20170207
end;

{-----------------------------------------------------------------------------
Function Name   :   peNumeroDocumentoKeyUp
Author          :   nefernandez
Date            :   11/02/2008
Description     :   Recuperar los datos del cliente a partir del RUT
                    ingresado, Verificar si existen observaciones a mostrar
                    para las cuentas de dicho Cliente.
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.peNumeroDocumentoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
resourcestring													//SS_1112_NDR_20130819
    MSG_CAPTION             = 'Datos del Cliente';				//SS_1112_NDR_20130819
var
    Texto: AnsiString;
    FHayConvenioCastigado : Integer;                             //TASK_020_GLE_20161121
    //FEstadoPagoCastigado  : string;                             //TASK_020_GLE_20161121
begin
    if Key = 13 then begin
        Key := 0;
        //FEstadoPagoCastigado := 'X';

        FCodigoPersona := QueryGetValueInt(DMConnections.BaseCAC, Format(                      //SS_1408_MCA_20151027
          'SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s''',   //SS_1408_MCA_20151027
          [PadL(Trim(peNumeroDocumento.Text), 9, '0')]));                                      //SS_1408_MCA_20151027

        if FCodigoPersona <> 0 then                                                             //SS_1408_MCA_20151027
            EstaClienteConMensajeEspecial(DMConnections.BaseCAC, Trim(peNumeroDocumento.Text)); //SS_1408_MCA_20151027


        //TASK_020_GLE_20161121
        FHayConvenioCastigado := QueryGetValueInt(DMConnections.BaseCAC, Format(
            'SELECT COUNT(*) FROM ComprobantesCastigados (NOLOCK) WHERE EstadoPago = ''X'' AND NumeroDocumento = ''%s''',
            [PadL(Trim(peNumeroDocumento.Text), 9, '0')]));

        if FHayConvenioCastigado >0 then
            lblConvenioCastigado.Visible := True;
        //TASK_020_GLE_20161121


        CargarDatosBasicos;
        if cbConvenio.Items.Count > 0 then begin                                               //SS_1112_NDR_20130819
           Caption := MSG_CAPTION + ' - ' + cbConvenio.Items[cbConvenio.ItemIndex].Caption;    //SS_1112_NDR_20130819
        end else begin                                                                         //SS_1112_NDR_20130819
           Caption := MSG_CAPTION;                                                             //SS_1112_NDR_20130819
        end;                                                                                   //SS_1112_NDR_20130819


        if FCodigoPersona <> 0 then
            MostrarMensajeRUTConvenioCuentas(Trim(peNumeroDocumento.Text));
            MostrarMensajeTipoClienteSistema(Trim(peNumeroDocumento.Text));

    end;
end;

{******************************************************************************
Function Name   :   CargarDatosBasicos
Author          :   llazarte
Date Created    :   15/03/2006
Description     :   Se realiza la carga de los datos de la cuenta
*******************************************************************************}
procedure TFormInicioConsultaConvenio.CargarDatosBasicos(pMostrarTAGsVencidos: Boolean = True);
    resourcestring                                          // SS_960_PDO_20110608
        NAME_INVOICING_EXECUTABLE = 'FACTURACION.EXE';      // SS_960_PDO_20110608
        SQLValidarClienteListaAmarilla  = 'SELECT dbo.EstaPersonaEnListaAmarilla(%d)';                    //SS_660_MVI_20130909
        MSG_ALERTA ='Validaci�n Cliente';                                                                 //SS_660_MVI_20130909
        MSG_ALERTA_CLIENTE_LISTA_AMARILLA='EL Cliente posee al menos un convenio en lista amarilla';      //SS_660_MVI_20130909
    var                                                     // SS_960_PDO_20120104
        Notificado: Boolean;                                // SS_960_PDO_20120104
        PersonaEnListaAmarilla: Integer;                                                                  //SS_660_MVI_20130909
begin
    if Trim(peNumeroDocumento.text) = '' then begin
        peNumeroDocumento.OnButtonClick(nil);
        lblConvenioCastigado.Visible := False;              // TASK_020_GLE_20161121
    end else begin
        FCodigoPersona := QueryGetValueInt(DMConnections.BaseCAC, Format(
          'SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s''',
          [PadL(Trim(peNumeroDocumento.Text), 9, '0')]));
        if FCodigoPersona = 0 then Exit;
        LimpiarDatos;
        CargarDatosCliente;

        //INICIO: TASK_007_ECA_20160428
        //CargarConveniosRUT(DMCOnnections.BaseCAC, cbConvenio, 'RUT', peNumeroDocumento.Text);
        CargarConveniosRUT(DMCOnnections.BaseCAC, cbConvenio, 'RUT', peNumeroDocumento.Text,False,0,False,True,False, CODIGO_TIPO_CONVENIO);
        //FIN: TASK_007_ECA_20160428
        
        if cbConvenio.Items.Count > 0 then begin
            cbConvenio.ItemIndex := 0;
            cbConvenioChange(cbConvenio);
        end else begin
            MostrarConvenio(0);
        end;
        if UpperCase(COPY(Application.ExeName,Length(Application.ExeName)-14, 15)) <> NAME_INVOICING_EXECUTABLE then begin     // SS_960_PDO_20110608
            if pMostrarTAGsVencidos then begin                                                                                 // SS_960_PDO_20110608
                Notificado := False;
                MostrarTagsVencidos(Trim(peNumeroDocumento.Text), DMConnections.BaseCAC, Notificado); //SS-960-ALA-20110411    // SS_960_PDO_20110608   // SS_960_PDO_20120104
                                                                                                                               // SS_960_PDO_20120104
                if Notificado then begin                                                                                       // SS_960_PDO_20120104
                    if ObtenerConvenioObservacion.Active then ObtenerConvenioObservacion.Close;                                // SS_960_PDO_20120104
                    ObtenerConvenioObservacion.Open;                                                                           // SS_960_PDO_20120104
                    lblReimprimirNotifTAGsVencidos.Enabled := True;                                                            // SS_960_PDO_20120116
                end;                                                                                                           // SS_960_PDO_20120104
            end;                                                                                                               // SS_960_PDO_20110608
        end;
        PersonaEnListaAmarilla := QueryGetIntegerValue(DMConnections.BaseCAC, Format(SQLValidarClienteListaAmarilla, [FCodigoPersona]));  //SS_660_MVI_20130909  //SS_660_MVI_20130909
      if PersonaEnListaAmarilla = 1 then begin                                                                                                          //SS_660_MVI_20130909
         ShowMsgBoxCN(MSG_ALERTA, MSG_ALERTA_CLIENTE_LISTA_AMARILLA, MB_ICONWARNING, Self);                                                             //SS_660_MVI_20130909
      end;                                                                                                                                              //SS_660_MVI_20130909
    end;
    // por defecto me posiciono en la solapa de observaciones y se traen solo estos datos
    PageControl.ActivePageIndex := 0;
    //if not ObtenerConvenioObservacion.Active then BtnFiltrarObservaciones.Click;  //TASK_066_GLE_20170409
end;

{-----------------------------------------------------------------------------
Function Name   :   MostrarMensajeRUTConvenioCuentas
Author          :   nefernandez
Date Created    :   11/02/2008
Description     :   Muestra un aviso con las observaciones de las cuentas
                    del Cliente (RUT) en cuesti�n.
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeRUTConvenioCuentas(DMCOnnections.BaseCAC, NumeroDocumento);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   MostrarMensajeTipoClienteSistema
Author          :   jjofre
Date Created    :   12/08/2010
Description     :   Muestra los mensajes personalizados de acuerdo a los
                    Semaforos definidos en el ABM Tipos de Cliente mensajes.
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.MostrarMensajeTipoClienteSistema(NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeTipoCliente(DMCOnnections.BaseCAC, NumeroDocumento, SistemaActual);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;


{-----------------------------------------------------------------------------
Function Name   :   txtObservacionesEnter
Author          :
Date Created    :
Description     :   Permite llamar a la funci�n que Busca las observaciones
                    del convenio solicitado
-----------------------------------------------------------------------------}
//INICIA: TASK_066_GLE_20170409
{
procedure TFormInicioConsultaConvenio.txtObservacionesEnter(Sender: TObject);
begin
    BuscarObservaciones;
end;
}
//TERMINA: TASK_066_GLE_20170409

{-----------------------------------------------------------------------------
Function Name   :   PageControlChange
Author          :
Date Created    :
Description     :   Controlador de solapas, permite activar solapa seleccionada
-----------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.PageControlChange(Sender: TObject);
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        //INICIA: TASK_066_GLE_20170409
        {
        if (Sender as TPageControl).ActivePage.Name = 'tsObservaciones' then begin
            BuscarObservaciones;
        end else if (Sender as TPageControl).ActivePage.Name = 'tsComprobantes' then begin
        end;
        }
        If (Sender as TPageControl).ActivePage.Name = 'tsComprobantes' then begin
        //TERMINA: TASK_066_GLE_20170409
            if spObtenerComprobantesConvenio.Active then Exit;
            btnFiltrarComprobantes.Click;
        end else if (Sender as TPageControl).ActivePage.Name = 'tsPagos' then begin
            if spObtenerPagosConvenio.Active then Exit;
            BuscarPagosConvenio;
{INICIO: TASK_006_JMA_20160420}
        end else if (Sender as TPageControl).ActivePage.Name = 'tsPagosAutomaticos' then begin
            if Assigned(spObtenerPagosConvenio) and spObtenerPagosConvenio.Active then Exit;
                BuscarPagosAutomaticosConvenio;
{TERMINO: TASK_006_JMA_20160420}
        end else if (Sender as TPageControl).ActivePage.Name = 'tsResumenCC' then begin
            if ObtenerResumenCuentaCorriente.Active then Exit;
            BuscarDatosCuentasCorrientes;
        end else if (Sender as TPageControl).ActivePage.Name = 'tsReclamos' then begin
            if spReclamos.Active then Exit;
            BuscarReclamos;
        end else if (Sender as TPageControl).ActivePage.Name = 'tsListaAmarilla' then begin             // SS_660_MCO_20130225
            BuscarHistoricoListaAmarilla;                                                               // SS_660_MCO_20130225
        end else if (Sender as TPageControl).ActivePage.Name = 'tsUltimosConsumos' then begin
            if spObtenerUltimosConsumos.Active then Exit;
            //btnUCFiltrar.Click;                                                                      //TASK_060_GLE_20170307
        end else if (Sender as TPageControl).ActivePage.Name = 'tsHistoricoMediosPagos' then begin
            if ObtenerHistoricoMedioPago.Active then Exit;
            BuscarHistoricoMediosPagos;
        //end else if (Sender as TPageControl).ActivePage.Name = 'TsMediosPagoAnteriores' then begin     //TASK_064_GLE_20170329
        //    if SPObtenerMediosPagoAnterioresConvenio.Active then Exit;                               //TASK_064_GLE_20170329
        //    BuscarMediosPagosAnteriores;                                                             //TASK_064_GLE_20170329
        end else if (Sender as TPageControl).ActivePage.Name = 'tsHistoricoDomicilios' then begin
            if ObtenerHistoricoDomicilios.Active then Exit;
            BuscarHistoricoDomicilios;
        end else if (Sender as TPageControl).ActivePage.Name = 'tsAccionesExternas' then begin
            if spObtenerAccionesCobranzaSerbanc.Active then Exit;
            BuscarAccionesExternas;
        end else if (Sender as TPageControl).ActivePage.Name = 'tsMails' then begin
            FiltrarNotificaciones;
        end else if (Sender as TPageControl).ActivePage.Name = 'tsIndemnizaciones' then begin
            if spObtenerIndemnizacionesconvenio.Active then Exit;
            BuscarIndemnizaciones;
        //REV.1
        end else if (Sender as TPageControl).ActivePage.Name = 'tsMensajesComprobantes' then begin
            if spObtenerMensajesComprobanteParticular.Active then Exit;
            btnMensajesComprobantes.Click;
        end else if PageControl.ActivePage = tsRefinanciaciones then begin
            if not cdsRefinanResumenCC.Active then begin
                btnRefinanciacionesClick(nil);
            end;
        end
        //INICIO	: CFU 20170209 TASK_103_CFU_20170209-CRM_Ver_Descuentos
        else if PageControl.ActivePage = tsDescuentos then begin
            if spObtenerDescuentosConvenio.Active then Exit;
                BuscarDescuentos;
        end;
        //TERMINO	: CFU 20170209 TASK_103_CFU_20170209-CRM_Ver_Descuentos
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
    //Fin REV.1
end;

procedure TFormInicioConsultaConvenio.PageControlDrawTab(
  Control: TCustomTabControl; TabIndex: Integer; const Rect: TRect;
  Active: Boolean);
var
  AText: string;
  APoint: TPoint;
begin
  with (Control as TPageControl).Canvas do
  begin
        if Active then
        begin
          Brush.Color := FColorMenuSel;
          Font.Color := FColorFontSel;
        end
        else
        begin
          Brush.Color := FColorMenu;
          Font.Color := FColorFont;
        end;
        FillRect(Rect);
        AText := TPageControl(Control).Pages[TabIndex].Caption;
        with Control.Canvas do
        begin
          APoint.x := (Rect.Right - Rect.Left) div 2 - TextWidth(AText) div 2;
          APoint.y := (Rect.Bottom - Rect.Top) div 2 - TextHeight(AText) div 2;
          TextRect(Rect, Rect.Left + APoint.x, Rect.Top + APoint.y, AText);
        end;
  end;
end;

procedure TFormInicioConsultaConvenio.Panel7Resize(Sender: TObject);
	var
        ColumnsWidth,
        TotalWidth,
        ColumnCounter,
        LastColumn: Integer;
begin
    dbResumenCC.Invalidate;

    TotalWidth   := dbResumenCC.Width;
    LastColumn   := dbResumenCC.Columns.Count - 1;
    ColumnsWidth := 0;

    for ColumnCounter := 0 to dbResumenCC.Columns.Count - 1 do begin
        if LastColumn = ColumnCounter then begin
        	if TotalWidth > ColumnsWidth then begin
	        	dbResumenCC.Columns[ColumnCounter].Width := TotalWidth - ColumnsWidth;
            end
            else dbResumenCC.Columns[ColumnCounter].Width := 0;

            dbResumenCC.Invalidate;
        end
        else ColumnsWidth := ColumnsWidth + dbResumenCC.Columns[ColumnCounter].Width;
    end;
end;

{******************************************************************************
Function Name   :   BuscarMediosPagosAnteriores
Author          :   lgisuk
Date Created    :   29/05/2006
Description     :   Busca los datos de los medios de pago anteriores
*******************************************************************************}
//INICIA: TASK_064_GLE_20170329
{
procedure TFormInicioConsultaConvenio.BuscarMediosPagosAnteriores;
ResourceString
    TXT_ERROR_HISTORICO_MP = 'Error buscando los datos Medios de Pago Anteriores';
    TXT_ERROR              = 'Error';
var
    VerPAT  : Boolean;
begin
    VerPAT  := False;
    try
        //Historico Medio de Pago
        SPObtenerMediosPagoAnterioresConvenio.Close;
        SPObtenerMediosPagoAnterioresConvenio.Parameters.Refresh;
        SPObtenerMediosPagoAnterioresConvenio.Parameters.ParamByName('@CodigoConvenio').Value   := cbConvenio.Value;
        //if ExisteAcceso('Ver_Numero_PAT_PAC') then VerPAT                                       := True;  // SS_1068_NDR_20120910_CQU
        SPObtenerMediosPagoAnterioresConvenio.Parameters.ParamByName('@PermisoVerPAT').Value    := VerPAT;
        SPObtenerMediosPagoAnterioresConvenio.Parameters.ParamByName('@Intento').Value          := Util.IIF(IntentosCHK.Checked = True, 1, 0);
        SPObtenerMediosPagoAnterioresConvenio.Open;
        dblMediosPagoAnteriores.Columns[0].Sorting := csAscending;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_HISTORICO_MP, e.Message, TXT_ERROR, MB_ICONWARNING);
        end;
    end;
end;
}
//TERMINA: TASK_064_GLE_20170329

{******************************************************************************
Function Name   :   BTNAnterioresMPClick
Author          :   lgisuk
Date Created    :   29/05/2006
Description     :   Obtengo los medios de pago anteriores
*******************************************************************************}
//INICIA: TASK_064_GLE_20170329
{
procedure TFormInicioConsultaConvenio.BTNAnterioresMPClick(Sender: TObject);
begin
   BuscarMediosPagosAnteriores;
end;
}
//TERMINA: TASK_064_GLE_20170329

{******************************************************************************
Function Name   :   RefrescarActionListCuenta
Author          :   ggomez
Date Created    :   30/06/2006
Description     :   Dada una cuenta refresca los action list del Telev�a, con los
                    datos de los pedidos de env�o.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.RefrescarActionListCuenta(CodigoConvenio,
  IndiceVehiculo: Integer);
begin
    // Obtener nuevamente los datos de las Cuentas.
    spObtenerCuentasCartola.Close;
    spObtenerCuentasCartola.Open;
end;

{******************************************************************************
Function Name   :   dblComprobantesMouseMove
Author          :
Date Created    :
Description     :   Permite cambiar de una figura cursor de flecha a mano.
*******************************************************************************}
procedure TFormInicioConsultaConvenio.dblComprobantesMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    if (PosCelda.Left <= X) and (PosCelda.Right >= X) and
       (PosCelda.Top + 23 < Y) and (PosCelda.Bottom + 23 > Y) then begin
        Screen.Cursor := crHandPoint;
    end else begin
        Screen.Cursor := crArrow;
    end;
end;

{******************************************************************************
Function Name   :   IntentosCHKClick
Author          :   lgisuk
Date Created    :   21/09/2007
Description     :   Actualiza la grilla, al hacer click en el checkbox
********************************************************************************}
//INICIA:
{
procedure TFormInicioConsultaConvenio.IntentosCHKClick(Sender: TObject);
begin
  BuscarMediosPagosAnteriores;
end;
}
//TERMINA:

procedure TFormInicioConsultaConvenio.vcbConcesionariasEstacionamientoChange(               //PAR00133-FASE2-NDR-20110624
  Sender: TObject);                                                                         //PAR00133-FASE2-NDR-20110624
begin                                                                                       //PAR00133-FASE2-NDR-20110624
    btnEstacBuscar.Click;                                                                   //PAR00133-FASE2-NDR-20110624
end;                                                                                        //PAR00133-FASE2-NDR-20110624

{******************************************************************************
						btnEstacBuscarClick
Author          :  mbecerra
Date Created    :  21-Junio-2010
Description     :   Despliega los estacionamientos asociados a un convenio
*******************************************************************************}
procedure TFormInicioConsultaConvenio.btnEstacBuscarClick(Sender: TObject);
begin
    FListaEstacionamientos.Clear;                                               //SS_1006P_NDR_20140715
    if cbConvenio.Items.Count > 0 then begin
        with spObtenerEstacionamientos do try
            CambiarEstadoCursor(CURSOR_RELOJ);

            Close;
            Parameters.ParamByName('@CodigoConvenio').Value :=  cbConvenio.Value;
            Parameters.ParamByName('@CodigoConcesionaria').Value := NULL;
            Parameters.ParamByName('@FechaDesde').Value := NULL;
            Parameters.ParamByName('@FechaHasta').Value := NULL;
            //Concesionaria
            if vcbConcesionariasEstacionamiento.ItemIndex >= 0 then begin
                Parameters.ParamByName('@CodigoConcesionaria').Value := vcbConcesionariasEstacionamiento.Value;
            end;

            //Fecha Desde
            if deEstacDesde.Date <> NullDate then begin
                Parameters.ParamByName('@FechaDesde').Value := deEstacDesde.Date + teEstacDesde.Time;
            end;

            //fecha hasta
            if deEstacHasta.Date <> NullDate then begin
                Parameters.ParamByName('@FechaHasta').Value := deEstacHasta.Date + teEstacHasta.Time;
            end;


            Open;
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO);
        end;
    end;
end;
{
Procedure Name	: cbConcesionariaChange
Author			: Nelson Droguett Sierra
Date			: 08-Mayo-2010
Description		: Si cambia el valor del combo, realiza el filtro nuevamente.
}
procedure TFormInicioConsultaConvenio.vcbConcesionariasTransaChange(Sender: TObject);
begin
	btnTransitosFiltrar.Click;
end;

procedure TFormInicioConsultaConvenio.ReimprimeNotificacionTAGSVencidos;
    resourcestring
        MSG_ERROR_INICIALIZAR_NOTIFICACIONES_TAG = 'Se produjo un Error al Inicializar las Notificaciones de TAGs Vencidos. %s.';
    var
        Notificado: Boolean;
begin
    with spInicializarNotificacionesTAGsPersona do try												
        if Active then Close;																		
        Parameters.Refresh;																			
                                                                                                    
        Parameters.ParamByName('@CodigoPersona').Value :=  FCodigoPersona;
        Parameters.ParamByName('@CodigoUsuario').Value :=  UsuarioSistema;      //SS_1419_NDR_20151130
        ExecProc;
                                                                                                    
        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin							
            raise Exception.Create(Format(MSG_ERROR_INICIALIZAR_NOTIFICACIONES_TAG,['']));			
        end;

        Notificado := False;
        MostrarTagsVencidos(Trim(peNumeroDocumento.Text), DMConnections.BaseCAC, Notificado, True);

        if Notificado then begin
            if ObtenerConvenioObservacion.Active then ObtenerConvenioObservacion.Close;
            ObtenerConvenioObservacion.Open;
        end;
    except																							
        on e:Exception do begin																		
            raise Exception.Create(e.Message);														
        end;																						
    end;
end;
{
Procedure Name  : btnExcelClick
Author          : Claudio Quezada Ib��ez
Date            : 22-Mayo-2012
Description     : Exporta a Excel el detalle de la cuenta corriente
Firma           : SS_1046_CQU_20120522

Firma           : SS_1046_CQU_20120524
Descripcion     : Coloca nombre de archivo por defecto

Firma           : SS_1046_CQU_20120525
Descripcion     : Al nombre de archivo le agerga el n�mero de convenio,
                  adicionalmente se corrige una validaci�n.
}
procedure TFormInicioConsultaConvenio.btnExcelClick(Sender: TObject);
resourcestring
    MSG_ERROR           = 'No se pudo guardar archivo';
  	MSG_ERROR_FILE_DOES_NOT_EXIST = 'Ocurri� un error al generar el archivo %s';
const
    SEPARADOR_CAMPO = ';';
    FILE_TITLE      = ' El archivo: ';
    FILE_EXISTS     = ' ya existe en el directorio de destino. �Desea reemplazarlo?';
    FILE_WARNING    = 'Atenci�n';
    FILE_OK         = ' fue generado exitosamente';
var
    Linea, NombreArchivo, Campo : String;
    I : Integer;
    FLista : TStringList;
    GrabarArchivo : Boolean;
begin
    //if (dbResumenCC.Columns.Count > 0) then begin                    // SS_1046_CQU_20120525
    if (dbResumenCC.DataSource.DataSet.RecordCount > 0) then begin     // SS_1046_CQU_20120525
        dbResumenCC.DataSource.DataSet.DisableControls;
        FLista := TStringList.Create;
        // Coloco la cabecera
        Linea := '';
        for I := 0 to dbResumenCC.Columns.Count - 1 do begin
            if dbResumenCC.Columns[I].Showing then begin
            	if dbResumenCC.Columns[I].FieldName <> EmptyStr then begin
                Linea := Linea + dbResumenCC.Columns[I].DisplayName + SEPARADOR_CAMPO;
        end;
            end;
        end;
        FLista.Add(Linea);
        // Obtengo los registros
        dbResumenCC.DataSource.DataSet.First;
        while not dbResumenCC.DataSource.DataSet.Eof do begin
            Linea := '';
            for I := 0 to dbResumenCC.Columns.Count - 1 do begin
                Campo := dbResumenCC.Columns[I].FieldName;
                if Campo <> EmptyStr then begin
                Linea := Linea + dbResumenCC.DataSource.DataSet.FieldByName(Campo).AsString + SEPARADOR_CAMPO;
            end;
            end;
            FLista.Add(Linea);
            dbResumenCC.DataSource.DataSet.Next;
        end;
        // Grabo el archivo
		try
            // Inicio Bloque SS_1046_CQU_20120525
            //dlgGrabarExcel.FileName := peNumeroDocumento.Text + '_ResumenCC_al_' + FormatDateTime ('yyyyMMdd', NowBaseCN(DMConnections.BaseCAC)) + '.csv';    // SS_1046_CQU_20120524
            NombreArchivo := peNumeroDocumento.Text
            + '_' + cbConvenio.Text
            + '_ResumenCC_al_'
            + FormatDateTime ('yyyyMMdd', NowBaseCN(DMConnections.BaseCAC))
            + '.csv';
            dlgGrabarExcel.FileName := NombreArchivo;
            // Fin Bloque SS_1046_CQU_20120525
            if dlgGrabarExcel.Execute then begin
                NombreArchivo := dlgGrabarExcel.FileName;
                if not (FileExists(NombreArchivo)) then GrabarArchivo := True
                else begin
                    if (MsgBox(FILE_TITLE + NombreArchivo + FILE_EXISTS, FILE_WARNING, MB_YESNO + MB_ICONQUESTION) = IDYES) then
                        GrabarArchivo := True
                    else
                        GrabarArchivo := False;
                end;
                if GrabarArchivo then begin
                    FLista.SaveToFile(NombreArchivo);
                    MsgBox(FILE_TITLE + NombreArchivo + FILE_OK, FILE_WARNING, MB_OK + MB_ICONINFORMATION);
                end;
            end;
		except
			on e: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_FILE_DOES_NOT_EXIST, [dlgGrabarExcel.FileName]), e.Message, MSG_ERROR, MB_ICONERROR);
				Exit;
			end;
		end;
        dbResumenCC.DataSource.DataSet.EnableControls;
    end;
end;
//INICIO BLOQUE : SS-1006-NDR-20120614
procedure TFormInicioConsultaConvenio.BuscarHistoricosListasDeAcceso;
ResourceString
    TXT_ERROR_HISTORICO_LISTASDEACCESO = 'Error buscando el historico de adhesion a lista de acceso ';
    TXT_ERROR_HISTORICO_INHABILITACIONES = 'Error buscando el historico de inhabilitaciones ';
    MSG_ERROR           = 'Error';
begin
  With spObtenerHistoricoListaDeAccesoConvenio do
  begin
    try
      Close;
      Parameters.Refresh;                                                       //SS-1006-NDR-20120812
      Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
      if deHistListaAccesoDesde.Date <> NullDate then
          Parameters.ParamByName('@FechaDesde').Value := deHistListaAccesoDesde.Date
      else
          Parameters.ParamByName('@FechaDesde').Value := Null;

      if deHistListaAccesoHasta.Date <> NullDate then
          Parameters.ParamByName('@FechaHasta').Value := deHistListaAccesoHasta.Date
      else
          Parameters.ParamByName('@FechaHasta').Value := Null;


      //if cbbPatentes.ItemIndex>0 then                                                             //SS-1006-NDR-20120814
      //    Parameters.ParamByName('@IndiceVehiculo').Value := cbbPatentes.Value                    //SS-1006-NDR-20120814
      //else                                                                                        //SS-1006-NDR-20120814
      //    Parameters.ParamByName('@IndiceVehiculo').Value := Null;                                //SS-1006-NDR-20120814
      if cbbPatentes.ItemIndex>0 then                                                               //SS-1006-NDR-20120814
          Parameters.ParamByName('@Patente').Value := cbbPatentes.Text                              //SS-1006-NDR-20120814
      else                                                                                          //SS-1006-NDR-20120814
          Parameters.ParamByName('@Patente').Value := Null;                                         //SS-1006-NDR-20120814

      Open;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_HISTORICO_LISTASDEACCESO, e.Message, MSG_ERROR, MB_ICONWARNING);
        end;
    end;
  end;
  With spObtenerHistoricoConveniosInhabilitados do
  begin
    try
      Close;
      Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
      if deHistListaAccesoDesde.Date <> NullDate then
          Parameters.ParamByName('@FechaDesde').Value := deHistListaAccesoDesde.Date
      else
          Parameters.ParamByName('@FechaDesde').Value := Null;

      if deHistListaAccesoHasta.Date <> NullDate then
          Parameters.ParamByName('@FechaHasta').Value := deHistListaAccesoHasta.Date
      else
          Parameters.ParamByName('@FechaHasta').Value := Null;

      Open;
    except
        on e: Exception do begin
            MsgBoxErr(TXT_ERROR_HISTORICO_INHABILITACIONES, e.Message, MSG_ERROR, MB_ICONWARNING);
        end;
    end;
  end;
end;
//FIN BLOQUE : SS-1006-NDR-20120614

//INICIO BLOQUE : SS-1006-NDR-20120614
procedure TFormInicioConsultaConvenio.btnBuscarHistoricosListasDeAccesoClick(
  Sender: TObject);
begin
  BuscarHistoricosListasDeAcceso;
end;

//INICIO:	20160707 CFU
procedure TFormInicioConsultaConvenio.btnBuscarVehiculoPorPatenteClick(
  Sender: TObject);
begin
	FiltrarCuentas(spObtenerCuentasCartola, Integer(cbConvenio.Value), edtFiltrarPatente.Text, cb_MostrarBajas.Checked);
end;
//TERMINO:	20160707 CFU

//FIN BLOQUE : SS-1006-NDR-20120614

//INICIO BLOQUE : SS-1006-NDR-20120614
function TFormInicioConsultaConvenio.CargarComboPatentes: Boolean;
resourcestring
	MSG_ERROR            = 'Error';
	MSG_ERROR_PATENTES   = 'Error obteniendo el patentes del convenio';
var
    spObtenerPatentesConvenio: TADOStoredProc;
begin
    try
        try
            spObtenerPatentesConvenio := TADOStoredProc.Create(nil);
            with spObtenerPatentesConvenio do begin
                Connection    := DMConnections.BaseCAC;
                ProcedureName := 'ObtenerPatentesConvenioListasDeAcceso';
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value := cbConvenio.Value;
                Open;

		        cbbPatentes.Clear;
    	    	cbbPatentes.Items.Add('Todas', -1);

	        	while not Eof do begin
    			    if cbbPatentes.Items.IndexOf(FieldByName('Patente').AsString) = -1 then begin
				        cbbPatentes.Items.Add(FieldByName('Patente').AsString, 0);
			        end;

			        Next;
		        end;

		        cbbPatentes.ItemIndex := 0;
        	end;
        except
            on e: exception do begin
	            MsgBoxErr( MSG_ERROR_PATENTES, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    finally
    	if Assigned(spObtenerPatentesConvenio) then FreeAndNil(spObtenerPatentesConvenio);
    end;
end;
//FIN BLOQUE : SS-1006-NDR-20120614

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
//procedure TFormInicioConsultaConvenio.ItemCBColor(cmbBox: TWinControl; pos: Integer; R: TRect; ColorFondo, ColorTexto: TColor; Bold: Boolean);
//begin
//    with (cmbBox as TVariantComboBox) do begin
//        Canvas.Brush.color := ColorFondo;
//        Canvas.FillRect(R);
//        Canvas.Font.Color := ColorTexto;
//        if Bold then begin
//        	Canvas.Font.Style := Canvas.Font.Style + [fsBold]
//        end
//        else begin
//        	Canvas.Font.Style := Canvas.Font.Style - [fsBold]
//        end;
//
//        Canvas.TextOut(R.Left, R.Top, Items[pos].Caption + Util.iif(Bold, ' (Baja)', ''));
//   end;
//end;
//END:SS_1120_MVI_20130820 -------------------------------------------------
{*******************************************************************************
Function Name   :   ObtenerHistoricoDeCartas
Author          :   CQuezadaI
Date Created    :   26-Febrero-2012
Firma           :   SS_660_CQU_20121010
Description     :   Obtiene los datos historicos de env�o de cartas
*******************************************************************************}
procedure TFormInicioConsultaConvenio.ObtenerHistoricoDeCartas(CodigoConvenio : Integer; IdConvenioInhabilitado: Integer); // SS_660_MCA_20130729
resourcestring
    ERROR_OBT_HIST_CARTAS = 'Error obteniendo el historial de cartas enviadas';
begin
    Screen.Cursor := crHourGlass;
    FColumnaAgrandada := False; // SS_660_CQU_20130604
    try
        try
            with spHistoricoCartasListaAmarilla do begin
                Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConvenio').Value         := CodigoConvenio;
                Parameters.ParamByName('@IDConvenioInhabilitado').Value := IdConvenioInhabilitado;   // SS_660_MCA_20130729
                Open;
            end;
        except
            on E: Exception do begin
                screen.Cursor := crDefault;
                MsgBoxErr(ERROR_OBT_HIST_CARTAS, e.message, caption, MB_ICONSTOP);
            end;
        end;
    finally
        dblHistoricoCartas.Invalidate;
        Screen.Cursor := crDefault;
    end;
end;

{---------------------------------------------------------------------------------
Function Name   : dblHistoricoCartasDrawText
Author          : CQuezadaI
Date Created    : 27/06/2013
Description     : Agranda la columna Observacion del dblListEx si supera los 500
Parameters      : Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
                  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
                  var ItemWidth: Integer; var DefaultDraw: Boolean
Return Value    : None
Firma           : SS_660_CQU_20130604
----------------------------------------------------------------------------------}
procedure TFormInicioConsultaConvenio.dblHistoricoCartasDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
if ((Column.FieldName = 'Observacion') and (Length(Text) > 200) and not FColumnaAgrandada)
  then begin
    Column.MinWidth   :=  500;
    Column.MaxWidth   :=  2500;
    Column.Width      :=  2000;
    FColumnaAgrandada :=  True;
  end;
end;

//INICIA: TASK_058_GLE_20170308
procedure TFormInicioConsultaConvenio.dblIndemnizacionesColumns0HeaderClick(
  Sender: TObject);
begin
  If (dblIndemnizaciones.Columns[0].Sorting = csAscending) then begin
    dblIndemnizaciones.Columns[0].Sorting := csDescending;
  end
  else if (dblIndemnizaciones.Columns[0].Sorting = csDescending) then begin
    dblIndemnizaciones.Columns[0].Sorting := csAscending;
  end;
 end;
//TERMINA: TASK_058_GLE_20170308
end.


