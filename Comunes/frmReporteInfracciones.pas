{------------------------------------------------------------------------

Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

----------------------------------------------------------------------------}
unit frmReporteInfracciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ppProd, ppClass, ppReport, UtilRB, ppComm,
  ppRelatv, ppDB, ppDBPipe, DMConnection, ppBands, ppCache, ppCtrls, ppVar,
  ppPrnabl, ppParameter, ppModule, raCodMod, daDataModule,
  ConstParametrosGenerales, UtilProc, PeaProcs;                                           //SS_1147_NDR_20140710

type
  TFormReporteInfracciones = class(TForm)
    ppDBPipelineObtenerTransitosInfracciones: TppDBPipeline;
	RBInterface: TRBInterface;
    ppReporteInfracciones: TppReport;
    dsObtenerTransitosInfracciones: TDataSource;
    ObtenerTransitosInfracciones: TADOStoredProc;
    ObtenerTransitosInfraccionesPatente: TStringField;
    ObtenerTransitosInfraccionesFecha: TDateTimeField;
    ObtenerTransitosInfraccionesJuzgado: TStringField;
    ppParameterList1: TppParameterList;
    ObtenerTransitosInfraccionesHora: TStringField;
    ObtenerTransitosInfraccionesCategoria: TStringField;
    ObtenerTransitosInfraccionesPuntoCobro: TStringField;
    ppHeaderBand2: TppHeaderBand;
    ppImage1: TppImage;
    Titulo: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppLabel7: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppLabel2: TppLabel;
    ppLabel5: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppLine2: TppLine;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppGroup4: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText1: TppDBText;
    ppGroupFooterBand4: TppGroupFooterBand;
    daDataModule1: TdaDataModule;
    ppLabel6: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppLabel10: TppLabel;
    spObtenerMaestroConcesionaria: TADOStoredProc;
    procedure TituloGetText(Sender: TObject; var Text: String);
  private
    FTitulo: Ansistring;
  public
    procedure Execute(CodigoInfraccionInicial: longint; CodigoInfraccionFinal: longint; Estado: string; showInterface: Boolean; titulo: AnsiString);
  end;

var
  FormReporteInfracciones: TFormReporteInfracciones;

implementation

{$R *.dfm}

procedure TFormReporteInfracciones.Execute(CodigoInfraccionInicial: longint; CodigoInfraccionFinal: longint; Estado: string; showInterface: Boolean; titulo: AnsiString);
var                                                                                                     //SS_1147_NDR_20140710
  RutaLogo, sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;                                        //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
    begin                                                                                               //SS_1147_NDR_20140710
      Close;                                                                                            //SS_1147_NDR_20140710
      Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
      Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
      ExecProc;                                                                                         //SS_1147_NDR_20140710
      sDireccion    := FieldByName('DireccionConcesionaria').AsString;                                  //SS_1147_NDR_20140710
      sLocalidad    := FieldByName('Comuna').AsString + '  ' +                                          //SS_1147_NDR_20140710
                       FieldByName('Region').AsString+ '  ' +                                           //SS_1147_NDR_20140710
                       FieldByName('Pais').AsString;                                                    //SS_1147_NDR_20140710
                                                                                                        //SS_1147_NDR_20140710
      ppLabel4.Caption := sDireccion+' '+sLocalidad;                                                    //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    ObtenerTransitosInfracciones.close;
    ObtenerTransitosInfracciones.Parameters.ParamByName('@CodigoInfraccionInicial').value := CodigoInfraccionInicial;
    ObtenerTransitosInfracciones.Parameters.ParamByName('@CodigoInfraccionFinal').value := CodigoInfraccionFinal;
    ObtenerTransitosInfracciones.Parameters.ParamByName('@Estado').value := Estado;
    ObtenerTransitosInfracciones.open;

    if ObtenerTransitosInfracciones.RecordCount > 0 then begin
        FTitulo := Titulo;
        RBInterface.Caption := Titulo;
        RBInterface.Execute(ShowInterface);
    end;
end;

procedure TFormReporteInfracciones.TituloGetText(Sender: TObject;
  var Text: String);
begin
    Text := FTitulo;
end;

end.
