object frmImageOverview: TfrmImageOverview
  Left = 610
  Top = 260
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'frmImageOverview'
  ClientHeight = 136
  ClientWidth = 251
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 251
    Height = 136
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnHighlight
    TabOrder = 0
    DesignSize = (
      251
      136)
    object ImageOverview1: TImage
      Left = 2
      Top = 2
      Width = 247
      Height = 132
      Anchors = [akLeft, akTop, akRight, akBottom]
      Stretch = True
    end
    object ImageOverview2: TImage
      Left = 2
      Top = 2
      Width = 247
      Height = 132
      Anchors = [akLeft, akTop, akRight, akBottom]
      Stretch = True
    end
    object ImageOverview3: TImage
      Left = 2
      Top = 2
      Width = 247
      Height = 132
      Anchors = [akLeft, akTop, akRight, akBottom]
      Stretch = True
    end
  end
end
