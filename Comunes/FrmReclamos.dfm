object FReclamos: TFReclamos
  Left = 39
  Top = 104
  Caption = 'B'#250'squeda de Casos'
  ClientHeight = 508
  ClientWidth = 1074
  Color = clBtnFace
  Constraints.MinHeight = 458
  Constraints.MinWidth = 870
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PBotones: TPanel
    Left = 0
    Top = 467
    Width = 1074
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object LCantidadTotal: TLabel
      Left = 645
      Top = 16
      Width = 3
      Height = 13
      Alignment = taRightJustify
    end
    object PBotonesDerecha: TPanel
      Left = 889
      Top = 0
      Width = 185
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object EditarBTN: TButton
        Left = 22
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Editar'
        TabOrder = 1
        OnClick = EditarBTNClick
      end
      object SalirBTN: TButton
        Left = 102
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Salir'
        TabOrder = 0
        OnClick = SalirBTNClick
      end
    end
    object AsignarBtn: TButton
      Left = 720
      Top = 8
      Width = 171
      Height = 25
      Caption = 'Selecci'#243'n M'#250'ltiple'
      TabOrder = 1
      OnClick = AsignarBtnClick
    end
  end
  object DBLReclamos: TDBListEx
    Left = 0
    Top = 170
    Width = 1074
    Height = 297
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 20
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Nro Caso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'CodigoOrdenServicio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 160
        Header.Caption = 'Caso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Descripcion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Creaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaHoraCreacion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 95
        Header.Caption = 'F. Compromiso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaCompromiso'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Cierre'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaHoraFin'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'RUT Cliente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Rut'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 120
        Header.Caption = 'Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'NumeroConvenio'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Prioridad'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Prioridad'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'DescEstado'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Fuente del Caso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FuenteCaso'
      end
      item
        Alignment = taCenter
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'Area Asignada'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'NombreAreaAtencionDeCaso'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 110
        Header.Caption = 'Usuario Asignado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'NombreUsuario'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 80
        Header.Caption = 'Bloqueado por'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Usuario'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = DBLReclamosDblClick
    OnDrawText = DBLReclamosDrawText
    OnMouseMove = DBLReclamosMouseMove
  end
  object GBCriterios: TGroupBox
    Left = 0
    Top = 0
    Width = 1074
    Height = 170
    Align = alTop
    Caption = 'Criterios de B'#250'squeda  '
    TabOrder = 0
    object LFecha: TLabel
      Left = 362
      Top = 32
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object LRut: TLabel
      Left = 15
      Top = 60
      Width = 78
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object LNumeroConvenio: TLabel
      Left = 363
      Top = 59
      Width = 48
      Height = 13
      Caption = 'Convenio:'
    end
    object LNumeroReclamo: TLabel
      Left = 15
      Top = 32
      Width = 54
      Height = 13
      Caption = 'N'#176' de Caso'
    end
    object Ldesde: TLabel
      Left = 665
      Top = 13
      Width = 67
      Height = 13
      Caption = 'Desde Fecha '
    end
    object LHasta: TLabel
      Left = 762
      Top = 13
      Width = 61
      Height = 13
      Caption = 'Hasta Fecha'
    end
    object LTipoReclamo: TLabel
      Left = 15
      Top = 89
      Width = 66
      Height = 13
      Caption = 'Tipo de Caso:'
    end
    object LPrioridad: TLabel
      Left = 665
      Top = 62
      Width = 44
      Height = 13
      Caption = 'Prioridad:'
    end
    object LLimitar: TLabel
      Left = 665
      Top = 90
      Width = 42
      Height = 13
      Caption = 'Limitar a:'
    end
    object LEstado: TLabel
      Left = 16
      Top = 118
      Width = 36
      Height = 13
      Caption = 'Estado:'
    end
    object LFuenteDelReclamo: TLabel
      Left = 363
      Top = 118
      Width = 80
      Height = 13
      Caption = 'Fuente del Caso:'
    end
    object lblConcesionaria: TLabel
      Left = 17
      Top = 144
      Width = 70
      Height = 13
      Caption = 'Concesionaria:'
      Visible = False
    end
    object lblUsuarios: TLabel
      Left = 668
      Top = 146
      Width = 44
      Height = 13
      Caption = 'Usuarios:'
    end
    object lb_Area: TLabel
      Left = 366
      Top = 144
      Width = 70
      Height = 13
      Caption = 'Area Atenci'#243'n:'
    end
    object EFechaDesde: TDateEdit
      Left = 665
      Top = 29
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 4
      Date = -693594.000000000000000000
    end
    object EFechaHasta: TDateEdit
      Left = 762
      Top = 29
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 5
      Date = -693594.000000000000000000
    end
    object PCriterios: TPanel
      Left = 990
      Top = 15
      Width = 82
      Height = 153
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 11
      object BuscarBTN: TButton
        Left = 3
        Top = 13
        Width = 69
        Height = 25
        Caption = 'Buscar'
        Default = True
        TabOrder = 0
        OnClick = BuscarBTNClick
      end
      object LimpiarBTN: TButton
        Left = 3
        Top = 40
        Width = 69
        Height = 25
        Caption = 'Limpiar'
        TabOrder = 1
        OnClick = LimpiarBTNClick
      end
    end
    object EPrioridad: TVariantComboBox
      Left = 731
      Top = 59
      Width = 121
      Height = 21
      Style = vcsDropDownList
      DropDownCount = 10
      ItemHeight = 13
      TabOrder = 7
      OnKeyDown = EPrioridadKeyDown
      Items = <
        item
          Caption = '(Seleccionar)'
          Value = '0'
        end
        item
          Caption = 'Muy Alta'
          Value = 1
        end
        item
          Caption = 'Alta'
          Value = 2
        end
        item
          Caption = 'Media'
          Value = 3
        end
        item
          Caption = 'Baja'
          Value = 4
        end>
    end
    object CKcerrado: TCheckBox
      Left = 801
      Top = 90
      Width = 143
      Height = 17
      Caption = 'Incluir Casos Cerrados'
      TabOrder = 10
    end
    object peNumeroDocumento: TPickEdit
      Left = 97
      Top = 56
      Width = 248
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 1
      OnChange = ERutChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object txt_Top: TNumericEdit
      Left = 731
      Top = 88
      Width = 57
      Height = 21
      TabOrder = 9
    end
    object EnumeroReclamo: TNumericEdit
      Left = 97
      Top = 28
      Width = 138
      Height = 21
      TabOrder = 0
    end
    object EEstado: TVariantComboBox
      Left = 96
      Top = 113
      Width = 249
      Height = 21
      Style = vcsDropDownList
      DropDownCount = 10
      ItemHeight = 13
      TabOrder = 8
      OnKeyDown = EEstadoKeyDown
      OnSelect = EEstadoSelect
      Items = <
        item
          Caption = '(Seleccionar)'
          Value = '0'
        end
        item
          Caption = 'Pendiente'
          Value = 1
        end
        item
          Caption = 'An'#225'lisis'
          Value = '2'
        end
        item
          Caption = 'Respuesta'
          Value = '3'
        end
        item
          Caption = 'Resuelta'
          Value = 4
        end
        item
          Caption = 'Cancelada'
          Value = 5
        end>
    end
    object EtipoFecha: TVariantComboBox
      Left = 463
      Top = 28
      Width = 194
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnKeyDown = EtipoFechaKeyDown
      Items = <
        item
          Caption = '(Seleccionar)'
          Value = '(Seleccionar)'
        end
        item
          Caption = 'Creaci'#243'n'
          Value = 'Creaci'#243'n'
        end
        item
          Caption = 'Compromiso'
          Value = 'Compromiso'
        end
        item
          Caption = 'Cierre'
          Value = 'Cierre'
        end>
    end
    object ETipoReclamo: TVariantComboBox
      Left = 97
      Top = 86
      Width = 560
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 3
      OnChange = ETipoReclamoChange
      OnKeyDown = ETipoReclamoKeyDown
      Items = <>
    end
    object EFuenteReclamo: TVariantComboBox
      Left = 463
      Top = 113
      Width = 194
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 12
      OnKeyDown = EFuenteReclamoKeyDown
      Items = <>
    end
    object PAdicional: TPanel
      Left = 731
      Top = 113
      Width = 124
      Height = 21
      BevelOuter = bvNone
      TabOrder = 13
      Visible = False
      object Label2: TLabel
        Left = 52
        Top = 2
        Width = 72
        Height = 13
        Caption = 'F. Compromiso:'
        Visible = False
      end
      object EFechaCompromiso: TDateEdit
        Left = 130
        Top = -4
        Width = 194
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Visible = False
        Date = -693594.000000000000000000
      end
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 96
      Top = 140
      Width = 249
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 14
      Visible = False
      Items = <>
    end
    object cbUsuarios: TVariantComboBox
      Left = 731
      Top = 140
      Width = 213
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 15
      Items = <>
    end
    object enumeroconvenio: TVariantComboBox
      Left = 463
      Top = 56
      Width = 194
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 6
      OnDrawItem = enumeroconvenioDrawItem
      Items = <>
    end
    object EEntidad: TVariantComboBox
      Left = 463
      Top = 140
      Width = 194
      Height = 21
      Style = vcsDropDownList
      DropDownCount = 10
      ItemHeight = 13
      TabOrder = 16
      OnDropDown = EEntidadDropDown
      OnSelect = EEntidadSelect
      Items = <
        item
          Caption = '(Seleccionar)'
          Value = '0'
        end>
    end
  end
  object GRAsignar: TDBGrid
    Left = 0
    Top = 170
    Width = 1074
    Height = 297
    Align = alClient
    DataSource = DataSource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgMultiSelect]
    ParentFont = False
    PopupMenu = pmAsignarMasiva
    TabOrder = 3
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        Width = 20
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CodigoOrdenServicio'
        Title.Alignment = taCenter
        Title.Caption = 'Nro Caso'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Alignment = taCenter
        Title.Caption = 'Caso'
        Width = 160
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'FechaHoraCreacion'
        Title.Alignment = taCenter
        Title.Caption = 'F. Creaci'#243'n'
        Width = 100
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'FechaCompromiso'
        Title.Alignment = taCenter
        Title.Caption = 'F. Compromiso'
        Width = 95
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'FechaHoraFin'
        ImeName = 'F. Cierre'
        Title.Alignment = taCenter
        Width = 100
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Rut'
        Title.Alignment = taCenter
        Title.Caption = 'RUT Cliente'
        Width = 80
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NumeroConvenio'
        Title.Alignment = taCenter
        Title.Caption = 'Convenio'
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Prioridad'
        Title.Alignment = taCenter
        Width = 60
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DescEstado'
        Title.Alignment = taCenter
        Title.Caption = 'Estado'
        Width = 60
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'FuenteCaso'
        Title.Alignment = taCenter
        Title.Caption = 'Fuente del Caso'
        Width = 90
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NombreAreaAtencionDeCaso'
        Title.Alignment = taCenter
        Title.Caption = 'Area Asignada'
        Width = 90
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NombreUsuario'
        Title.Alignment = taCenter
        Title.Caption = 'Usuario Asignado'
        Width = 110
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Usuario'
        Title.Caption = 'Bloquedo por'
        Width = 80
        Visible = True
      end>
  end
  object SpObtenerReclamos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerReclamos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HastaFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Limite'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Creacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Compromiso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cierre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Pendiente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoReclamo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Prioridad'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFuenteReclamo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCompromisoInterna'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoAreaAtencionDeCaso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CantidadTotal'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Usuario'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 744
    Top = 205
  end
  object DataSource: TDataSource
    DataSet = SpObtenerReclamos
    Left = 792
    Top = 205
  end
  object SPObtenerConveniosxNumeroDocumento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosxNumeroDocumento;1'
    Parameters = <>
    Left = 680
    Top = 197
  end
  object Timer: TTimer
    Interval = 100
    OnTimer = TimerTimer
    Left = 568
    Top = 207
  end
  object SpObtenerTiposOrdenServicioReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposOrdenServicioReclamo;1'
    Parameters = <>
    Left = 712
    Top = 205
  end
  object SpObtenerEntidades: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEntidades;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 632
    Top = 207
  end
  object spObtenerAreasAtencionDeCasosTipoOrdenServicios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerAreasAtencionDeCasosTipoOrdenServicios;1'
    Parameters = <
      item
        Name = '@Tipo'
        DataType = ftInteger
        Value = Null
      end>
    Left = 304
    Top = 128
  end
  object pmAsignarMasiva: TPopupMenu
    Left = 376
    Top = 368
    object AsignaraUsuarioArea1: TMenuItem
      Caption = 'Asignar a Usuario/Area'
      OnClick = AsignaraUsuarioArea1Click
    end
  end
end
