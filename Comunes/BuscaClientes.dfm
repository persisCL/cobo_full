object FormBuscaClientes: TFormBuscaClientes
  Left = 97
  Top = 123
  ActiveControl = cbPersoneria
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'B'#250'squeda de Personas'
  ClientHeight = 266
  ClientWidth = 846
  Color = clBtnFace
  Constraints.MinHeight = 291
  Constraints.MinWidth = 770
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Visible = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlDeAbajo: TPanel
    Left = 0
    Top = 226
    Width = 846
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object pnlBotones: TPanel
      Left = 586
      Top = 0
      Width = 260
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object bt_aceptar: TButton
        Left = 89
        Top = 9
        Width = 75
        Height = 25
        Caption = '&Aceptar'
        Enabled = False
        TabOrder = 0
        OnClick = bt_aceptarClick
      end
      object bt_cancelar: TButton
        Left = 167
        Top = 9
        Width = 75
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object gbFiltros: TGroupBox
    Left = 0
    Top = 0
    Width = 846
    Height = 89
    Align = alTop
    Caption = '  Filtros de B'#250'squeda  '
    TabOrder = 0
    DesignSize = (
      846
      89)
    object lblRUT: TLabel
      Left = 268
      Top = 23
      Width = 26
      Height = 13
      Caption = '&RUT:'
      FocusControl = txt_rut
    end
    object lblPersoneria: TLabel
      Left = 68
      Top = 23
      Width = 55
      Height = 13
      Caption = 'Personer&'#237'a:'
      FocusControl = cbPersoneria
    end
    object lblApellido: TLabel
      Left = 10
      Top = 54
      Width = 114
      Height = 13
      Caption = 'Apellido / Raz'#243'n Social:'
    end
    object lbl_apellidoMaterno: TLabel
      Left = 279
      Top = 54
      Width = 82
      Height = 13
      Caption = 'Apellido Materno:'
    end
    object lbl_nombre: TLabel
      Left = 529
      Top = 54
      Width = 40
      Height = 13
      Caption = 'Nombre:'
    end
    object lblPatente: TLabel
      Left = 423
      Top = 23
      Width = 40
      Height = 13
      Caption = '&Patente:'
      FocusControl = txt_Patente
    end
    object lblTAG: TLabel
      Left = 543
      Top = 23
      Width = 25
      Height = 13
      Caption = '&TAG:'
      FocusControl = txt_TAG
    end
    object txt_rut: TEdit
      Left = 298
      Top = 19
      Width = 91
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object cbPersoneria: TComboBox
      Left = 127
      Top = 19
      Width = 127
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnExit = cbPersoneriaExit
    end
    object txt_apellido: TEdit
      Left = 127
      Top = 50
      Width = 143
      Height = 21
      MaxLength = 30
      TabOrder = 4
    end
    object txt_apellidoMaterno: TEdit
      Left = 364
      Top = 50
      Width = 158
      Height = 21
      MaxLength = 30
      TabOrder = 5
    end
    object txt_nombre: TEdit
      Left = 573
      Top = 50
      Width = 173
      Height = 21
      MaxLength = 30
      TabOrder = 6
    end
    object txt_Patente: TEdit
      Left = 466
      Top = 19
      Width = 56
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 2
    end
    object txt_TAG: TEdit
      Left = 573
      Top = 19
      Width = 173
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 11
      TabOrder = 3
      OnKeyPress = txt_TAGKeyPress
    end
    object btn_Limpiar: TButton
      Left = 761
      Top = 46
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Limpiar'
      TabOrder = 8
      OnClick = btn_LimpiarClick
    end
    object btnBuscar: TButton
      Left = 761
      Top = 19
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Buscar'
      Default = True
      TabOrder = 7
      OnClick = btnBuscarClick
    end
  end
  object dbl_Personas: TDBListEx
    Left = 0
    Top = 89
    Width = 846
    Height = 137
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'RUT'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroDocumento'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 298
        Header.Caption = 'Nombre Completo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreCompleto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Personer'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'PersoneriaDescrip'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 180
        Header.Caption = 'N'#250'mero Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroConvenio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Patente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Alta Cuenta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
      end>
    DataSource = dsBuscarClienteContacto
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnClick = dbl_PersonasClick
    OnDblClick = dbl_PersonasDblClick
    OnDrawText = dbl_PersonasDrawText
    ExplicitLeft = 24
    ExplicitTop = 83
  end
  object BuscarClienteContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = BuscarClienteContactoAfterOpen
    ProcedureName = 'BuscarClienteContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Etiqueta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 84
    Top = 172
  end
  object dsBuscarClienteContacto: TDataSource
    DataSet = BuscarClienteContacto
    Left = 115
    Top = 172
  end
end
