object MuestraPDFForm: TMuestraPDFForm
  Left = 0
  Top = 0
  Caption = 'MuestraPDFForm'
  ClientHeight = 454
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlAcciones: TPanel
    Left = 0
    Top = 414
    Width = 750
    Height = 40
    Align = alBottom
    Anchors = []
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      750
      40)
    object btnSalir: TButton
      Left = 670
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 750
    Height = 414
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object AcroPDF1: TAcroPDF
      Left = 1
      Top = 1
      Width = 748
      Height = 412
      Align = alClient
      TabOrder = 0
      ExplicitLeft = 224
      ExplicitTop = 32
      ExplicitWidth = 192
      ExplicitHeight = 192
      ControlData = {000900004F4D0000952A0000}
    end
  end
end
