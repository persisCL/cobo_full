unit FrmSeleccionarTipoDeCliente;
{*******************************************************************************
    Firma : TASK_124_CFU_20170320
    Author : Claudio Fuentes
    Date : 21/03/2017
    Description : Unidad para seleccionar Tipos de Cliente de Cobranza 
*******************************************************************************}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, frmImprimirConvenio, RStrings, PeaProcs,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Convenios,  ConstParametrosGenerales, Util, UtilProc,
  ADODB, SysUtilsCN, PeaProcsCN, frmReporteContratoAdhesionPA, Peatypes, RBSetUp, PRinters, UtilDB,
  DB;
type
  TFormSeleccionarTipoCliente = class(TForm)
    gb_Convenio: TGroupBox;
    sb_Convenio: TScrollBox;
    Panel10: TPanel;
    btn_Cancelar: TButton;
    btn_ok: TButton;
    rgTiposCliente: TRadioGroup;
    spTiposDeClientesDeCobranza_Listar: TADOStoredProc;
    procedure btn_okClick(Sender: TObject);

  private

  public
  	ListaCodigosTiposCliente: TStringList;
    function Inicializar(): Boolean;
  end;

var
  FormSeleccionarTipoCliente: TFormSeleccionarTipoCliente;

implementation

uses DMConnection;

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}

function TFormSeleccionarTipoCliente.Inicializar(): Boolean;
const
	CONST_SEMAFORO_COBRANZA	= 2;
begin
	ListaCodigosTiposCliente := TStringList.Create;
	with spTiposDeClientesDeCobranza_Listar, Parameters do begin
        if Active then
        	Close;
        Parameters.Refresh;
        ParamByName('@Semaforo').Value	:= CONST_SEMAFORO_COBRANZA;
        Open;
        if spTiposDeClientesDeCobranza_Listar.RecordCount > 20 then begin
            Width					:= 1200;
            rgTiposCliente.Columns	:= 3;
        end
        else if spTiposDeClientesDeCobranza_Listar.RecordCount > 10 then begin
            Width					:= 800;
            rgTiposCliente.Columns	:= 2;
        end
        else begin
            Width					:= 400;
            rgTiposCliente.Columns	:= 1;
        end;

        while not Eof do begin
        	rgTiposCliente.Items.Add(FieldByName('Descripcion').AsString);
            ListaCodigosTiposCliente.Add(FieldByName('CodigoTipoCliente').AsString);
        	Next;
        	end;
        Close;
    end;
    Result := True;
end;

procedure TFormSeleccionarTipoCliente.btn_okClick(Sender: TObject);
resourcestring
	MSG_SIN_SELECCION = 'No ha seleccionado ning�n Tipo de Cliente';
begin
    try
        if rgTiposCliente.ItemIndex < 0 then begin
        	MsgBox(MSG_SIN_SELECCION, Caption, MB_ICONERROR);
            Exit;
        end;

        ModalResult := mrOk;
    finally
    end;
end;

end.
