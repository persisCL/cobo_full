{********************************** File Header ********************************
File Name   : Login.pas
Author      : Diego Tinao <dtinao@dpsautomation.com>
Date Created: 20/08/2003
Language    : ES-AR
Description : Unit de Login de las Aplocaciones del Proyecto Oriente-Poniente.
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 14-Abr-2004
Author		: Calani, Daniel <dcalani@dpsautomation.com>
Description : Se agrego la funcionalidad de pedirle al usuario que cambie su
              password si es la primera vez que se logonea.

Firma       : SS_1436_NDR_20151230
Descripcion : Dejar un registro en RegistroOperaciones cuando el usuario sale del sistema  o cuando trata de ingresar con password erronea

Etiqueta	: 20160307 MGO
Descripci�n	: Se usa BaseBO_Master para ValidarLogin
*******************************************************************************}


unit Login;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DmiCtrls, DB, DBTables, UtilDB, UtilProc, Util,
  ComCtrls, ADODB, Variants, PeaProcs, PeaTypes, DPSControls, CambioPassword,
  RStrings, DMConnection, LoginSetup;

type
  TLoginForm = class(TForm)
	Panel1: TPanel;
	Label1: TLabel;
	Label2: TLabel;
	txt_password: TEdit;
	Image1: TImage;
    ValidarLogin: TADOStoredProc;
    txt_usuario: THistoryEdit;
    btn_ok: TButton;
    btn_cancel: TButton;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btn_okClick(Sender: TObject);
    procedure txt_usuarioChange(Sender: TObject);
	procedure FormShow(Sender: TObject);
  private
	{ Private declarations }
	FNombreUsuario: AnsiString;
	FCodigoUsuario: AnsiString;
    FCodigoSistema: integer;
    FUpdateLog: Boolean;
  public
	{ Public declarations }
	Function Inicializar(Connection: TADOConnection; CodigoSistema: integer; UpdateSysLog: Boolean = True): Boolean;
	property NombreUsuario: AnsiString read FNombreUsuario;
	property CodigoUsuario: AnsiString read FCodigoUsuario;
  end;

var
  LoginForm: TLoginForm;

implementation


{$R *.DFM}

{******************************** Function Header *******************************
  Function Name: TLoginForm.Inicializar
  Author:        dtinao
  Date Created:  20-Ago-2003
  Parameters:    Connection: TADOConnection; CodigoSistema: integer
  Return Value:  Boolean
  	True: si se inicializaron los par�metros correctamente.
    False: si no se pudo cargar alg�n par�metro.
  Description:
  	Carga a las variables locales los valores que fueron pasados como
    par�metro.
********************************************************************************}
Function TLoginForm.Inicializar(Connection: TADOConnection; CodigoSistema: integer; UpdateSysLog: Boolean = True): Boolean;
resourcestring
    MSG_MISSED_INSTALL_FILE = 'No existe el archivo de instalaci�n';
    MSG_INSTALL_ERROR = 'Error de Instalaci�n';
begin
    //La variable FUpdateSysLog indica si a continuaci�n del login se hace o no
    //un registro en el log de operaciones
    Result := False;
    MoveToMyOwnDir;
    if not FileExists('.\Install.ini') then begin
        MsgBox(MSG_MISSED_INSTALL_FILE, MSG_INSTALL_ERROR, MB_ICONWARNING);
        Exit;
    end;
	FCodigoSistema 			:= CodigoSistema;
	{ INICIO : 20160307 MGO
	ValidarLogin.Connection := Connection;
	} // FIN : 20160307 MGO
	txt_usuario.text 		:= GetUserName;
    FUpdateLog              := UpdateSysLog;
	Result 					:= True;
end;

{******************************** Function Header *******************************
  Function Name: TLoginForm.btn_okClick
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  20-Ago-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:
  	Verifica el Login ingresado utilizando un procedimiento almacenado
    "ValidarLogin" dejando un Registro de Log de los ingresos fallidos y de los
    logueos exitosos utilizando la funci�n "CrearRegistroOperaciones"
  Revision :1
      Author : vpaszkowicz
      Date : 10/12/2007
      Description :Agrego que si se conecta de una base que no es CAC, que no
      intente actualizar las comunicaciones.
********************************************************************************}
procedure TLoginForm.btn_okClick(Sender: TObject);
resourcestring
    MSG_ERROR_ACCESO		= 'No se ha podido acceder a la Base de Datos.';
	CAPTION_ERROR_ACCESO	= 'Ingreso al Sistema';
	MSG_LOGIN_EXITOSO 		= 'Login exitoso';
	MSG_REGISTRO_CAPTION	= 'Error al Grabar Registro de Operaciones';
	MSG_ERROR_USUARIO_O_CLAVE	= 'Usuario o clave incorrectos, o Usuario Deshabilitado.';
var
	DescriError: array[0..255] of char;
	f:TFormCambioPassword;
begin
	Screen.Cursor := crHourGlass;
    DMConnections.SetUp;
	try
		txt_usuario.SaveHistory;
		ValidarLogin.Parameters.ParamByName('@CodigoUsuario').Value  := txt_usuario.text;
		ValidarLogin.Parameters.ParamByName('@Password').Value       := txt_password.text;
		ValidarLogin.Parameters.ParamByName('@NombreCompleto').Value := NULL;
		ValidarLogin.Parameters.ParamByName('@DescriError').Value 	 := NULL;
		ValidarLogin.Parameters.ParamByName('@PedirCambiarPassword').Value:= NULL;
        ValidarLogin.ExecProc;
		Screen.Cursor := crDefault;
	except
		on e: Exception do
	    begin
			Screen.Cursor := crDefault;
			MsgBox(MSG_ERROR_ACCESO + CRLF + e.Message, CAPTION_ERROR_ACCESO, MB_ICONSTOP);
			Exit;
		end;
	end;

  // Validamos el usuario
  if ValidarLogin.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
  begin

    if FUpdateLog then                                                                                //SS_1436_NDR_20161230
    begin                                                                                             //SS_1436_NDR_20161230
        if CrearRegistroOperaciones(                                                                  //SS_1436_NDR_20161230
            DMCOnnections.BaseCAC,                                                                    //SS_1436_NDR_20161230
            FCodigoSistema,                                                                           //SS_1436_NDR_20161230
            RO_MOD_LOGIN,                                                                             //SS_1436_NDR_20161230
            RO_AC_LOGIN,                                                                              //SS_1436_NDR_20161230
            SEV_INFO,                                                                                 //SS_1436_NDR_20161230
            GetMachineName,                                                                           //SS_1436_NDR_20161230
            txt_usuario.text,                                                                         //SS_1436_NDR_20161230
            ValidarLogin.Parameters.ParamByName('@DescriError').Value,                                //SS_1436_NDR_20161230
            0,0,0,0,                                                                                  //SS_1436_NDR_20161230
            DescriError) = -1 then                                                                    //SS_1436_NDR_20161230
            MsgBoxErr(MSG_ERROR_ACCESO, StrPas(DescriError), MSG_REGISTRO_CAPTION, MB_ICONSTOP);      //SS_1436_NDR_20161230
    end;                                                                                              //SS_1436_NDR_20161230


  	MsgBox(MSG_ERROR_USUARIO_O_CLAVE, CAPTION_ERROR_ACCESO, MB_ICONSTOP);

    txt_Password.Clear;
    Exit;
	end;

	// Todo Ok
	FNombreUsuario := ValidarLogin.Parameters.ParamByName('@NombreCompleto').Value;
	FCodigoUsuario := txt_usuario.text;
  //Me aseguro de que no queden comunicaciones en estado A para el usuario que acaba de loguearse.
  //Esto podr�a suceder si hay un "abnormal program teermination" cuando hay una comunicacion abierta.
  if Trim(ValidarLogin.Connection.Name) = DmConnections.BaseCAC.Name then
  begin
    QueryExecute(DMConnections.BaseCAC,'UPDATE Comunicaciones '+
                                       'SET Estado =''F'', Observaciones=''Terminaci�n anormal de la comunicaci�n.'''+
                                       'WHERE Estado=''A'' and CodigoUsuario='+QuotedStr(FCodigoUsuario));
  end;

  //Guardo el Login del sistema

  // Verifico si es la primera vez que se logea
	if ValidarLogin.Parameters.ParamByName('@PedirCambiarPassword').Value then
  	begin
		Application.CreateForm(TFormCambioPassword, f);

		if f.Inicializa(ValidarLogin.Connection, self.CodigoUsuario) then
    	begin
			if f.ShowModal<>mrOk then
      		begin
				f.Release;
				exit;
			end;
	      	try
				  QueryExecute(ValidarLogin.Connection,format('UPDATE USUARIOSSISTEMAS SET PedirCambiarPassword=0 WHERE CODIGOUSUARIO=''%s''',[self.CodigoUsuario]));
			Except
	        	On E: Exception do
	        	begin
	          		MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[self.NombreUsuario]), e.message, format(MSG_USER,[self.NombreUsuario]), MB_ICONSTOP);
	        	end;
	      	end;
		end;
	  	f.Release;
  	end;


  //Si nos pidieron actualizar el log de operaciones lo hacemos.
  //Esto t�picamente se hace con aplicaciones del CAC

  if FUpdateLog then
  begin
      if CrearRegistroOperaciones(
          DMCOnnections.BaseCAC,
          FCodigoSistema,
          RO_MOD_LOGIN,
          RO_AC_LOGIN,
          SEV_INFO,
          GetMachineName,
          FCodigoUsuario,
          MSG_LOGIN_EXITOSO,
          0,0,0,0,
          DescriError) <> -1 then
      begin
          ModalResult := mrOk
      end
      else
      begin
          MsgBoxErr(MSG_ERROR_ACCESO, StrPas(DescriError), MSG_REGISTRO_CAPTION, MB_ICONSTOP);
      end;
  end
  else
  begin
      ModalResult := mrOk;
  end;
end;

procedure TLoginForm.txt_usuarioChange(Sender: TObject);
begin
	btn_ok.enabled := txt_usuario.text <> '';
end;


procedure TLoginForm.FormShow(Sender: TObject);
begin
	txt_usuario.OnChange(txt_usuario);
end;

procedure TLoginForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    f: TfrmLoginSetup;
begin
  if (ssShift in Shift) and (ssCTrl in Shift) then begin
        if Key = VK_F1 then begin
            Application.CreateForm(TfrmLoginSetup, f);
            if f.Inicializar(False) then f.ShowModal;
            f.Release;
        end;
  end;
end;

end.
