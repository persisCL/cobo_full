{

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

}


unit ActualizarDatosInfractor;

interface

uses
  SysUtils, Classes, DBListEx, RVMTypes, PeaProcs, DMConnection, ConstParametrosGenerales,
  Util, Dialogs, UtilProc, Windows, DB, ADODB, Variants, Controls;

type
  TDMActualizarDatosInfractor = class(TDataModule)
    VerificarInfractoresBD: TADOStoredProc;
    ActualizarConsultaRNVM: TADOStoredProc;
    sp_IncrementarCantidadConsultasRNVMInfraccion: TADOStoredProc;
    sp_AnularInfraccion: TADOStoredProc;
    sp_ObtenerCantidadConsultasRNVMInfraccion: TADOStoredProc;
    spAgregarConsultaRNVM: TADOStoredProc;
  private
    FServiceURL: String;
    FServicePort: Integer;
    FServiceID: String;
    FDir: String;
    FCantMaximaConsultasRNVM: Integer;
  public
    function ConfigurarParametrosGenerales(var DescriError: String): Boolean;
    function Actualizar(SoloConsulta: Boolean; Patente: String; CodigoInfractor: Integer; Usuario, Password: String; CodigoSistema: Integer; var RespuestaConsulta: TRVMInformation; var TextoError: String; VerificarConsultas: Boolean): Boolean;
  end;

var
  DMActualizarDatosInfractor: TDMActualizarDatosInfractor;

implementation

const
    FILE_EXTENSION = '.xml';

resourcestring
	MSG_INIT_ERROR     = 'Error al Inicializar';
	MSG_ERROR          = 'Error en la Consulta al RNVM';
    MSG_ERROR_CONSULTA = 'No se pudo Obtener los datos del RNVM';
    MSG_ERROR_DIR      = 'No existe el directorio %s indicado en Par�metros Generales (RVM_Directorio_Respuestas)';

{$R *.dfm}

{ TDMActualizarDatosInfractor }


{******************************** Function Header ******************************
Function Name: TDMActualizarDatosInfractor.Actualizar
Author : gcasais
Date Created : 1/10/2005
Description :  Realiza consultas al RNMV y actualiza los datos de los infractores
Parameters : Patente: String; CodigoInfractor: Integer; Usuario, Password: String; CodigoSistema: Integer; RaiseError: Boolean = False
Return Value : Boolean

Revision 1:
    Author: nefernandez
    Date:   20/06/2007
    Description: Le agregu� una variable para tomar el string del XML de respuesta
    del RNVM

Revision 2:
    Author: nefernandez
    Date:   22/02/2008
    Description: Cuando se setea para actualizar los datos de infractores, se llamaba
    al store ActualizarConsultaRNVM (que insertaba la respuesta de la consulta en la
    tabla ConsultaRNVM y registraba los datos de infracci�n). Se modifico para que
    primero llame al store AgregarConsultaRNVM (que solo inserta la respuesta del RNVM)
    y despues llame al store ActualizarConsultaRNVM (que a partir de este cambio registra
    los datos de infracci�n). Ambas llamadas son realizadas en transacciones diferentes
    de manera a asegurar por lo menos que se registre la respuesta del RNVM, porque el
    error de Time Out se est� dando en alg�n punto de la registraci�n de infracci�n.



  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

*******************************************************************************}
function TDMActualizarDatosInfractor.Actualizar(SoloConsulta: Boolean; Patente: String;
  CodigoInfractor: Integer; Usuario, Password: String; CodigoSistema: Integer;
   var RespuestaConsulta:TRVMInformation; var TextoError: String; VerificarConsultas: Boolean): Boolean;
resourcestring
    MSG_UPDATED = 'La patente %s no ser� consultada porque la informaci�n de la misma es vigente';
    MSG_MAX_REACHED = 'La patente no ser� consultada porque se ha superado la cantidad de consultas permitidas cuya cantidad es %d';
var
    Pregunta: TRNVMQuery;
    Respuesta: TRVMInformation;
    NombreViejo, NombreNuevo :string;
    txtErrorConsulta, TxtErrorIncremento, TxtErrorActualizacion: String;
    ExistePatente: Boolean;
    Resultado: TRVMQueryResult;
    Infraccion: TEnforcementData;
    TxtRNVM, TxtResultado: WideString;
    CodigoConsultaRNVM: Integer;

    function ExisteConsulta(Infractor: Integer): Boolean;
    begin
        VerificarInfractoresBD.Close;
        VerificarInfractoresBD.Parameters.Refresh;                                                          // SS_RNVM_PDO_20110914
        VerificarInfractoresBD.Parameters.ParamByName('@CodigoInfraccion').Value := Infractor;
        VerificarInfractoresBD.Parameters.ParamByName('@Existe').Value := Null;
        VerificarInfractoresBD.Parameters.ParamByName('@CodigoUsuarioAnulacion').Value := UsuarioSistema;   // SS_RNVM_PDO_20110914
        VerificarInfractoresBD.ExecProc;
        Result := (VerificarInfractoresBD.Parameters.ParamByName('@Existe').Value);
        VerificarInfractoresBD.Close;
    end;


    function VerificarCantidadConsultas(Infractor: Integer): Boolean;
    var
        CantConsultasInfraccion: Integer;
    begin
        sp_ObtenerCantidadConsultasRNVMInfraccion.Close;
        sp_ObtenerCantidadConsultasRNVMInfraccion.Parameters.ParamByName('@CodigoInfraccion').Value := CodigoInfractor;
        sp_ObtenerCantidadConsultasRNVMInfraccion.ExecProc;
        CantConsultasInfraccion := sp_ObtenerCantidadConsultasRNVMInfraccion.Parameters.ParamByName('@Cantidad').Value;
        Result := (CantConsultasInfraccion < FCantMaximaConsultasRNVM);
        sp_ObtenerCantidadConsultasRNVMInfraccion.Close;
    end;

    procedure ArmarPregunta;
    begin
        //Configuramos la pregunta que vamos a hacer
        Pregunta.Parameters.ServiceURL := FServiceURL;
        Pregunta.Parameters.ServicePort:= FServicePort;
        Pregunta.Parameters.ServiceID  := FServiceID;
        Pregunta.Parameters.SaveDirectory := FDir;
        Pregunta.Parameters.UserName := Usuario;
        Pregunta.Parameters.PassWord := Password;
        Pregunta.LicensePlate := Patente;
        // Si estoy en modo de consulta como nombre de archivo
        // uso la patente, sino uso el c�digo de infractor y la patente
        if SoloConsulta then begin
            Pregunta.Parameters.FileName := Patente + FILE_EXTENSION;;
        end else begin
            Pregunta.Parameters.FileName := Patente + '-'+ IntToStr(CodigoInfractor) + FILE_EXTENSION;
        end;
        { INICIO : 20160315 MGO
        Pregunta.Parameters.RequestUseProxy := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
        if Pregunta.Parameters.RequestUseProxy then begin
            Pregunta.Parameters.RequestProxyServer := Trim(ApplicationIni.ReadString('RNVM', 'PROXYSERVER', ''));
            Pregunta.Parameters.RequestProxyServerPort := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
        end;
        }
        Pregunta.Parameters.RequestUseProxy := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
        if Pregunta.Parameters.RequestUseProxy then begin
            Pregunta.Parameters.RequestProxyServer := Trim(InstallIni.ReadString('RNVM', 'PROXYSERVER', ''));
            Pregunta.Parameters.RequestProxyServerPort := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
        end;
        // FIN : 20160315 MGO
    end;

{******************************** Function Header ******************************
Function Name: ActualizarConsulta
Author :
Date Created :
Description :
Parameters : ObtuvoPatente: Boolean; Infractor: Integer;  var TextoError: String
Return Value : Boolean

Revision 1:
    Author: ggomez
    Date:   01/12/2005
    Description: cambi� la llamada al SP ActualizarConsultaRNVM, ahora se hace
        la transacci�n desde Delphi. Esto se hizo pues con la Transacci�n en
        el SP se produc�an bloqueos que no se liberaban.
Revision 3:
    Author: nefernandez
    Date: 20/06/2007
    Description:
    - Agregu� el par�metro TextoResultado (respuesta del RNVM) en la llamada al store.
    - Si solo se eligi� para realizar consulta, tambi�n se guarda en la base de datos
    el resultado de dicha consulta (tabla: ConsultaRNVM)
Revision : 4
    Author : vpaszkowicz
    Date : 26/06/2007
    Description : Agregu� en fechaAdquisicion que si el d�a y mes vienen en cero que les asigne 1
    para que grabe algo si solo se conoce el a�o.
Revision : 5
    Author : nefernandez
    Date : 18/02/2008
    Description : Agregu� el par�metro ErrorExcepcion a la funci�n "AgregarConsultaRNVM"
    para registrar tambi�n los errores producidos por excepci�n de la aplicaci�n
Revision : 6
    Author : nefernandez
    Date : 22/02/2008
    Description : Cuando se actualiza los datos de infractores, primero se registra
    la respuesta en la tabla ConsultaRNVM y se retorna el valor del CodigoConsultaRNVM
    resultante; despues se env�a este valor al store ActualizarConsultaRNVM para
    registrar los datos de infraci�n
*******************************************************************************}
    function ActualizarConsulta(ObtuvoPatente: Boolean; Infractor: Integer;  var TextoError: String; TextoResultado: WideString): Boolean;
    resourcestring
        MSG_ADQ_DATE_ERROR = 'La Fecha de Adquisici�n %s de la Patente %s no es v�lida. Se grabar� con fecha de adquisici�n vac�a.';
        MSG_QUERY_ERROR = 'Ha ocurrido un error de base de datos con la patente %s. La Fecha de Adquisici�n Informada por el RNVM es %s.';
        MSG_DATABASE_ERROR = 'El Error devuelto por el motor de base de datos debido a este error es "%s".';
        MSG_ERROR_ACTUALIZARCONSULTARNVM    = 'Error al Actualizar la Consulta RNVM: "%s"';
    const
        NO_EXISTE_PATENTE = 'NE';
        DATOS_INCOMPLETOS = 'SD';
        CONSULTA_OK       = 'OK';
        ERROR_DESCONOCIDO = 'ED';
    var
        DiaAdquisicion, MesAdquisicion: integer;
        AnioAdquisicion: integer;
    begin
        TextoError := '';
        Result := False;
        ActualizarConsultaRNVM.Close;
        try
            //ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value := NULL;
            //Revision 6: Se pasa el valor CodigoConsultaRNVM al store ActualizarConsultaRNVM
            //(este valor es obtenido al agregar la respuesta en la tabla ConsultaRNVM

            ActualizarConsultaRNVM.Parameters.Refresh;  // SS_970_PDO_20110906

            ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value := CodigoConsultaRNVM;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Patente').Value := Patente;
            ValidarResultadoConsultaRNMVM(Respuesta, ObtuvoPatente, Resultado, Infraccion);
            case Resultado of
                qrRNVMUnknown       : ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := NO_EXISTE_PATENTE;
                qrRNVMIncomplete    : ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := DATOS_INCOMPLETOS;
                qrRNVMOK            : ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := CONSULTA_OK;
                else begin
                    TextoError := 'Error Interno. Estado del resultado de la consulta Indefinido';
                    //Se registra la consulta aunque no se haya podido validar el resultado
                    //Exit;
                    ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := ERROR_DESCONOCIDO;
                end;
            end;
            ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoUsuario').Value   := UsuarioSistema;
            ActualizarConsultaRNVM.Parameters.ParamByName('@DescripMarca').Value    := Infraccion.Vehicle.Manufacturer;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Modelo').Value          := Infraccion.Vehicle.Model;
            if trim(Infraccion.Vehicle.Year) = '' then begin
                ActualizarConsultaRNVM.Parameters.ParamByName('@AnioVehiculo').Value := null
            end else begin
                ActualizarConsultaRNVM.Parameters.ParamByName('@AnioVehiculo').Value := StrToInt(Infraccion.Vehicle.Year);
            end;
            ActualizarConsultaRNVM.Parameters.ParamByName('@DescripCodigoTipoVehiculo').Value   := Infraccion.Vehicle.Category;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Color').Value                       := Infraccion.Vehicle.Color;
            ActualizarConsultaRNVM.Parameters.ParamByName('@IdMotor').Value                     := Infraccion.Vehicle.EngineID;
            ActualizarConsultaRNVM.Parameters.ParamByName('@IdChasis').Value                    := Infraccion.Vehicle.ChassisID;
            ActualizarConsultaRNVM.Parameters.ParamByName('@RUT').Value                         := Infraccion.Owner.ID;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Nombre').Value                      := Infraccion.Owner.FullName;
            ActualizarConsultaRNVM.Parameters.ParamByName('@DescripCodigoComuna').Value         := Infraccion.Owner.Commune;
            ActualizarConsultaRNVM.Parameters.ParamByName('@CalleDesnormalizada').Value         := Infraccion.Owner.Street;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Numero').Value                      := Infraccion.Owner.DoorNumber;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Detalle').Value                     := Infraccion.Owner.Details;
            try
                if Infraccion.Registration.AcquisitionDate <> '' then begin
                    DiaAdquisicion := StrToInt(StrLeft(Infraccion.Registration.AcquisitionDate, 2));
                    MesAdquisicion := StrToInt(Copy(Infraccion.Registration.AcquisitionDate, 4, 2));
                    AnioAdquisicion := StrToInt(StrRight(Infraccion.Registration.AcquisitionDate, 4));
                    ActualizarConsultaRNVM.Parameters.ParamByName('@FechaAdquisicion').Value :=
                      EncodeDate(AnioAdquisicion,
                      iif(MesAdquisicion = 0, 1, MesAdquisicion),
                      iif(MesAdquisicion = 0, 1, DiaAdquisicion));
                end else begin
                     ActualizarConsultaRNVM.Parameters.ParamByName('@FechaAdquisicion').Value := Null;
                end;
            except
                // Como pueder venir una fecha invalida (ej: 00/00/1984), asignamos null al campo, para que se
                // guarde el registro en la base de datos y se analice despu�s con los campos Resultado y Respuesta
                on e: Exception do begin
                    //Si ocurri� un error muestro el mensaje pero grabo un Null igual.
                    TextoError := Format(MSG_ADQ_DATE_ERROR, [Infraccion.Registration.AcquisitionDate, Patente]);
//                    Exit;
                    ActualizarConsultaRNVM.Parameters.ParamByName('@FechaAdquisicion').Value := Null;
                end;
            end;
            ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;

            // Revision 3
            ActualizarConsultaRNVM.Parameters.ParamByName('@Respuesta').Value := TextoResultado;

            ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoUsuarioAnulacion').Value := UsuarioSistema;   // SS_970_PDO_20110906

            // Como el SP no tiene Transacci�n, comenzarla desde la app.
            //DMConnections.BaseCAC.BeginTrans;                                       //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN ActualizarDatosInfractor');     //SS_1385_NDR_20150922
            try
                ActualizarConsultaRNVM.ExecProc;
                //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('COMMIT TRAN ActualizarDatosInfractor');					//SS_1385_NDR_20150922
            except
                on E: Exception do begin
                    //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION ActualizarDatosInfractor END');	//SS_1385_NDR_20150922
                     // Propagar la Excepci�n.
                    raise Exception.Create(Format(MSG_ERROR_ACTUALIZARCONSULTARNVM, [E.Message]));
                end;
            end; // except

            NombreViejo := FDir + IntToStr(CodigoInfractor) + FILE_EXTENSION;
            NombreNuevo := FDir + IntToStr(ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value) + '_' + Patente  + FILE_EXTENSION;
            ActualizarConsultaRNVM.Close;
            while FileExists(NombreNuevo) do  NombreNuevo := StrLeft(NombreNuevo, Length(NombreNuevo) - Length(FILE_EXTENSION)) + 'x' + FILE_EXTENSION;
            RenameFile(NombreViejo, NombreNuevo);
            Result := True;
        except
            on e: Exception do begin
                TextoError := Format(MSG_QUERY_ERROR, [Patente, Infraccion.Registration.AcquisitionDate]) + Space(1) +
                 Format(MSG_DATABASE_ERROR, [e.message]);
            end;
        end;
    end;

    function IncrementarCantidadConsultas(Infractor: Integer; var DescriError: string): Boolean;
    resourcestring
        MSG_SUM_ERROR = 'Ha ocurido un error incrementando la cantidad de consultas para esta infracci�n';
    begin
        DescriError := '';
        Result := False;
        try
            sp_IncrementarCantidadConsultasRNVMInfraccion.Close;
            sp_IncrementarCantidadConsultasRNVMInfraccion.Parameters.ParamByName('@CodigoInfraccion').Value := Infractor;
            sp_IncrementarCantidadConsultasRNVMInfraccion.ExecProc;
            Result := True;
            sp_IncrementarCantidadConsultasRNVMInfraccion.Close;
        except
            on e: Exception do begin
                DescriError := MSG_SUM_ERROR + Space(1) + e.Message;
            end;
        end;
    end;

{*******************************************************************************
Revision : 6
    Author : nefernandez
    Date : 22/02/2008
    Description : Se recupera el valor CodigoConsultaRNVM al agregar la respuesta en
    la tabla ConsultaRNVM. Dicho valor es utilizado por el store ActualizarConsultaRNVM
    si se tiene que actualizar los datos de infracci�n
*******************************************************************************}
    function AgregarConsultaRNVM(ObtuvoPatente: Boolean; Infractor: Integer;  var TextoError: String; TextoResultado: WideString; ErrorExcepcion: String = ''): Boolean;
    resourcestring
        MSG_ADQ_DATE_ERROR = 'La Fecha de Adquisici�n %s de la Patente %s no es v�lida';
        MSG_QUERY_ERROR = 'Ha ocurrido un error de base de datos con la patente %s. La Fecha de Adquisici�n Informada por el RNVM es %s.';
        MSG_DATABASE_ERROR = 'El Error devuelto por el motor de base de datos debido a este error es "%s".';
        MSG_ERROR_ACTUALIZARCONSULTARNVM    = 'Error al Actualizar la Consulta RNVM: "%s"';
        MSG_TIME_OUT = 'Connection timed out';
    const
        NO_EXISTE_PATENTE = 'NE';
        DATOS_INCOMPLETOS = 'SD';
        CONSULTA_OK       = 'OK';
        TIME_OUT          = 'TO';
        ERROR_DESCONOCIDO = 'ED';
    begin
        TextoError := '';
        CodigoConsultaRNVM := 0;
        Result := False;
        spAgregarConsultaRNVM.Close;
        try
            spAgregarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value := NULL;
            spAgregarConsultaRNVM.Parameters.ParamByName('@Patente').Value := Patente;
            spAgregarConsultaRNVM.Parameters.ParamByName('@Respuesta').Value := TextoResultado;
            if ( Trim(ErrorExcepcion) <> '' ) then begin // Revision 5: Se produjo un error debido a una excepci�n de la aplicaci�n
                if (pos(lowercase(MSG_TIME_OUT), lowercase(ErrorExcepcion)) > 0) then // Si el error contiene la descripcion de Time Out 
                    spAgregarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := TIME_OUT
                else
                    spAgregarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := ERROR_DESCONOCIDO;
                // Revision 5: Si no hay respuesta y se produjo una excepci�n, se guarda en la tabla
                // el mensaje de error
                if (Trim(TextoResultado) = '') then
                    spAgregarConsultaRNVM.Parameters.ParamByName('@Respuesta').Value := ErrorExcepcion;
            end else begin // Se busca la validaci�n de la respuesta del RNVM
                ValidarResultadoConsultaRNMVM(Respuesta, ObtuvoPatente, Resultado, Infraccion);
                case Resultado of
                    qrRNVMUnknown       : spAgregarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := NO_EXISTE_PATENTE;
                    qrRNVMIncomplete    : spAgregarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := DATOS_INCOMPLETOS;
                    qrRNVMOK            : spAgregarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := CONSULTA_OK;
                    else begin
                        TextoError := 'Error Interno. Estado del resultado de la consulta Indefinido';
                        // Revision 1: Se registra la consulta aunque no se haya podido validar el resultado
                        //Exit;
                        spAgregarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := ERROR_DESCONOCIDO;
                    end;
                end;
            end;
            spAgregarConsultaRNVM.Parameters.ParamByName('@CodigoUsuario').Value   := UsuarioSistema;
            spAgregarConsultaRNVM.Parameters.ParamByName('@DescripCodigoComuna').Value         := Infraccion.Owner.Commune;
            spAgregarConsultaRNVM.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;

            // Como el SP no tiene Transacci�n, comenzarla desde la app.
            //DMConnections.BaseCAC.BeginTrans;                                       //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('BEGIN TRAN ActualizarDatosInfractorII');     //SS_1385_NDR_20150922
            try
                spAgregarConsultaRNVM.ExecProc;
                //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('COMMIT TRAN ActualizarDatosInfractorII');					//SS_1385_NDR_20150922
                //Revision 6
                CodigoConsultaRNVM := spAgregarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value;
            except
                on E: Exception do begin
                    //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
                    DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION ActualizarDatosInfractorII END');	//SS_1385_NDR_20150922
                    // Propagar la Excepci�n.
                    raise Exception.Create(Format(MSG_ERROR_ACTUALIZARCONSULTARNVM, [E.Message]));
                end;
            end; // except

            NombreViejo := FDir + IntToStr(CodigoInfractor) + FILE_EXTENSION;
            NombreNuevo := FDir + IntToStr(spAgregarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value) + '_' + Patente  + FILE_EXTENSION;
            spAgregarConsultaRNVM.Close;
            while FileExists(NombreNuevo) do  NombreNuevo := StrLeft(NombreNuevo, Length(NombreNuevo) - Length(FILE_EXTENSION)) + 'x' + FILE_EXTENSION;
            RenameFile(NombreViejo, NombreNuevo);
            Result := True;
        except
            on e: Exception do begin
                TextoError := Format(MSG_QUERY_ERROR, [Patente, Infraccion.Registration.AcquisitionDate]) + Space(1) +
                 Format(MSG_DATABASE_ERROR, [e.message]);
            end;
        end;
    end;

begin
    TextoError := '';
    TxtRNVM := '';
    Result := False;
    // Si nos piden solamente una consulta la base de datos no la tocamos
    // y no validamos nada repecto a las cantidadades de consultas y dias entre ellas
    // Solo mostramos devolvemos el resultado en "RespuestaConsulta"
    // para que alguien la muestre por pantalla
    if SoloConsulta then begin
        ArmarPregunta;
        if ConsultarPatenteRNVM(Pregunta, Respuesta, ExistePatente, TxtErrorConsulta, TxtRNVM) then begin
            RespuestaConsulta := Respuesta;
            //La consulta se hizo bien pero de todos modos pudo tener alg�n error
            if TxtErrorConsulta <> '' then TextoError := TxtErrorConsulta;
            // Revisi�n 3: Aunque se indique "solo la consulta al RNVM", de todos
            // modos se guarda el resultado de la consulta en la base datos
            TxtResultado := '';
            if (TxtRNVM <> '') then
                TxtResultado := TxtRNVM
            else
                TxtResultado := TxtErrorConsulta;
            if AgregarConsultaRNVM(ExistePatente, CodigoInfractor, TxtErrorActualizacion, TxtResultado) then begin
                Result := True;
            end else begin
                TextoError := TxtErrorActualizacion;
                Exit;
            end;

            Result := True;
        end else begin
            TextoError := TxtErrorConsulta;
            // Revision 5: Se agrega un registro a la tabla ConsultaRNVM aunque no se haya podido realizar
            // exitosamente la consulta al RNVM. Es decir, se produjo una exepcion de la aplicaci�n
            AgregarConsultaRNVM(ExistePatente, CodigoInfractor, TxtErrorActualizacion, TxtResultado, TextoError);
            Exit;
        end;
    end else begin
        // Si no piden que hagamos la consulta y adem�s que actulicemos datos
        // entonces validamos  si esta consulta se puede hacer, si corresponde
        // la hacemos, y finalmente si todo sali� bi�n actualizamos la base.

        if VerificarConsultas then begin
            // Validamos que no haya una consulta dentro del tiempo establecido
            if ExisteConsulta(CodigoInfractor) then begin
                TextoError := Format(MSG_UPDATED, [Patente]);
                Exit;
            end;
            // Validamos que no se haya excedido la cantidad de consultas
            if not VerificarCantidadConsultas(CodigoInfractor) then begin
               TextoError := Format(MSG_MAX_REACHED, [FCantMaximaConsultasRNVM] );
               Exit;
            end;
        end;
        // Todo Ok para hacer la consulta, la hacemos!
        ArmarPregunta;
        if ConsultarPatenteRNVM(Pregunta, Respuesta, ExistePatente, TxtErrorConsulta, TxtRNVM) then begin
            // La consulta se hizo, por lo tanto incrementamos el contador
            if IncrementarCantidadConsultas(CodigoInfractor, TxtErrorIncremento) then begin
                Result := True;
            end else begin
                TextoError := TxtErrorIncremento;
                Exit;
            end;
            // Revision 3: Se env�a el resultado de la Consulta al RNVM para almacenar en la base de datos
            TxtResultado := '';
            if (TxtRNVM <> '') then
                TxtResultado := TxtRNVM
            else
                TxtResultado := TxtErrorConsulta;
            // Revision 2
            if not AgregarConsultaRNVM(ExistePatente, CodigoInfractor, TxtErrorActualizacion, TxtResultado) then begin
                TextoError := TxtErrorActualizacion;
                Exit;
            end;
            // Fin Revision 2

            if ActualizarConsulta(ExistePatente, CodigoInfractor, TxtErrorActualizacion, TxtResultado) then begin
                Result := True;
            end else begin
                TextoError := TxtErrorActualizacion;
                Exit;
            end;
            // De todos modos siempre devolvemos el resultado de la consulta
            Respuestaconsulta := Respuesta;
        end else begin
            TextoError := TxtErrorConsulta;
            // Revision 5: Se agrega un registro a la tabla ConsultaRNVM aunque no se haya podido realizar
            // exitosamente la consulta al RNVM. Es decir, se produjo una exepcion de la aplicaci�n
            AgregarConsultaRNVM(ExistePatente, CodigoInfractor, TxtErrorActualizacion, TxtResultado, TextoError);
        end;
    end;
end;

function TDMActualizarDatosInfractor.ConfigurarParametrosGenerales(var DescriError: String): Boolean;
resourcestring
    MSG_ERROR_OBTENER_PARAMETRO_GENERAL = 'Error al obtener informaci�n de par�metros generales';
    MSG_DIR_ERROR = 'No es posible crear el directorio %s.';    // SS_970_PDO_20110906
begin
    Result := False;
    DescriError := '';
    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'RNVM_SERVICE_URL', FServiceURL) then begin
        DescriError := MSG_ERROR_OBTENER_PARAMETRO_GENERAL + Space (1) + 'RVM_SERVICE_URL';
        Exit;
    end;

    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'RNVM_SERVICE_PORT', FServicePort) then begin
        DescriError := MSG_ERROR_OBTENER_PARAMETRO_GENERAL + Space (1) + 'RVM_SERVICE_PORT';
        Exit;
    end;

    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'RNVM_SERVICE_ID', FServiceID) then begin
        MsgBox(MSG_ERROR_OBTENER_PARAMETRO_GENERAL + Space (1) + 'RVM_SERVICE_ID', MSG_ERROR);
        Exit;
    end;

    if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'RNVM_CANT_MAX_CONSULTAS_INFRACCION', FCantMaximaConsultasRNVM) then begin
        DescriError := MSG_ERROR_OBTENER_PARAMETRO_GENERAL + Space (1) + 'RNVM_CANT_MAX_CONSULTAS_INFRACCION';
        Exit;
    end;

    if not ObtenerParametroGeneral(DMConnections.BaseCAC,'RNVM_Directorio_Respuestas', FDir) then begin
        DescriError := MSG_ERROR_OBTENER_PARAMETRO_GENERAL + Space (1) + 'RNVM_Directorio_Respuestas';
        Exit;
    end;

    FDir := GoodDir (FDir);
    FDir := GoodDir (FDir + FormatDateTime('YYYYMM', NowBase(DMConnections.BaseCAC)));  // SS_970_PDO_20110906

    if not DirectoryExists(FDir) then begin                                             // SS_970_PDO_20110906
        if not ForceDirectories(FDir) then begin                                        // SS_970_PDO_20110906
            DescriError := Format(MSG_DIR_ERROR, [FDir]);                               // SS_970_PDO_20110906
            Exit;                                                                       // SS_970_PDO_20110906
        end;
    end;

    Result := True;
end;



end.





