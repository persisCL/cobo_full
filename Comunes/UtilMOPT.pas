unit UtilMOPT;

interface

uses xmldom, msxmldom, XMLDoc, xmlintf, classes, ContenedorXML, PeaTypes,
     SysUtils, Util, UtilDB, DB, AdoDB;

implementation

procedure ProcesarRespuesta(Connection : TCustomConnection; Archivo : string);
var
    mensajeTotalRegistros, msjLog: AnsiString;
    NombreArchivoLog: AnsiString;

    ContenedorXML: IXMLDocument;
    Cantidad,
    CantidadError, CantidadCorrectos, i: integer;
    AuxNodoConvenio: IXMLNode;

    Convenio : Integer;
    CodigoEnvioRNUT : integer;
    NumeroConvenio : string;
    CodigoRespuesta : integer;
    Complemento : string;


    NombreArchivoOriginal : string;
    ConveniosDistintos : TStringList;
begin
    mensajeTotalRegistros := '';
    msjLog := '';
    NombreArchivoLog := '';

    Convenio := 0;
    Cantidad := 0; CantidadError := 0; CantidadCorrectos := 0;

    ConveniosDistintos := TStringList.Create;


    // Archivo de entrada de datos
    ContenedorXML:=CrearXML;
    ContenedorXML.LoadFromFile(Archivo);

    NombreArchivoLog := ExtractFilePath(Archivo) +
                        copy(ExtractFileName(Archivo)
                        , 1, pos('.',ExtractFileName(Archivo)) - 1) + '.log';
//    FLog := TStringList.Create;

    //Reconstruyo el nombre del archivo original en funcion del nombre del
    //archivo de respuesta.
    NombreArchivoOriginal := 'entrada_' +
      ParseParamByNumber(Archivo,2,'_') + '_' +
      ParseParamByNumber(Archivo,3,'_') + '_' +
      ParseParamByNumber(Archivo,4,'_');
      //+ '.xml'; //Saqu� la b�squeda por extensi�n, ya que se utilizaba en la primer versi�n pero luego no.

    CodigoEnvioRNUT := QueryGetValueInt(Connection, format('select dbo.ObtenerCodigoEnvioRNUT (''%s'')',[NombreArchivoOriginal]));

    if CodigoEnvioRNUT = 0 then raise Exception.Create('No se encontr� el envio correspondiente a la respuesta.')
    else begin //Actualizo todos los registros a ok. si alguno contiene error se actualizar� debidamente.
        QueryExecute(Connection, format('update ConvenioEnviosRNUT set  Estado = ''O'' where Estado = ''P'' and CodigoEnvioConvenioRNUT = %d',[CodigoEnvioRNUT]));
    end;

    try
        with ContenedorXML.DocumentElement.ChildNodes do begin
            for i:=0 to Count - 1  do begin //Contratos
             if not (Connection as TADOConnection).InTransaction then (Connection as TADOConnection).BeginTrans;
             try
                Inc(cantidad);
                AuxNodoConvenio := ObtenerGrupo(ContenedorXML.DocumentElement, i, XML_CONTRATO);
                NumeroConvenio  := AuxNodoConvenio.ChildNodes[XML_CODIGO].Text;
                CodigoRespuesta := AuxNodoConvenio.ChildNodes[XML_RESPUESTA].NodeValue;
                Convenio := QueryGetValueInt(Connection,format('select CodigoConvenio from convenio where NumeroConvenio = ''%s''',[NumeroConvenio]));
                Complemento := AuxNodoConvenio.ChildNodes[XML_COMPLEMENTO].NodeValue;

                if ConveniosDistintos.IndexOf(NumeroConvenio) < 0 then
                  ConveniosDistintos.Add(NumeroConvenio);

                if Convenio = 0 then raise Exception.Create('No se encontr� el C�digo de Convenio para el Nro.Convenio: ' + NumeroConvenio);


                ActualizarEstadoConvenio(Convenio,CodigoEnvioRNUT;

                with ActualizarEstadoConvenioEnviosRNUT, ActualizarEstadoConvenioEnviosRNUT.Parameters do begin
                    ParamByName('@CodigoConvenio').Value := Convenio;
                    ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioRNUT;
                    ParamByName('@estado').Value := iif(CodigoRespuesta = 1, XML_PROCESADO_OK, XML_PROCESADO_ERROR);
                    ExecProc;
                end;

                with ActualizarRespuestaConvenioEnviosRNUT, ActualizarRespuestaConvenioEnviosRNUT.Parameters do begin
                    ParamByName('@CodigoConvenio').Value := Convenio;
                    ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioRNUT;
                    ParamByName('@CodigoRespuesta').Value := CodigoRespuesta;
                    ParamByName('@Complemento').Value := Complemento;
                    ExecProc;
                end;
                Inc(CantidadCorrectos);

              except
                  on E: Exception do begin
                          if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                          msjLog := E.Message;
                          Inc(CantidadError);
                          FLog.Add('Convenio: ' + Inttostr(Convenio) + ' - Complemento: ' + Complemento + ' - ' + msjLog);
                  end;
              end;
              if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
              msjLog := '';

            end; //for i;
        end; //with
    finally
        cursor := crDefault;
        mensajeTotalRegistros := 'Resultados: ' + CRLF +
          '   - Total Rechazos en Archivo :' + #9 + #9+ IntToStr(Cantidad) +  CRLF +
          '   - Total Rechazos Importados Correctamente:' + #9 + #9+ IntToStr(CantidadCorrectos) + CRLF +
          '   - Total Rechazos Importados Con Error:' + #9 + #9 + IntToStr(CantidadError) + CRLF +
          '   - Total Convenios Rechazados Diferentes :' + #9 + #9 + IntToStr(ConveniosDistintos.Count);
        FLog.Add(mensajeTotalRegistros);
        try
            FLog.SaveToFile(NombreArchivoLog);
        finally
            FreeAndNil(Flog);
        end;
        if CantidadError = 0 then begin
            MsgBox('El archivo se proces� correctamente. ' + CRLF +
                   mensajeTotalRegistros + CRLF +  CRLF);
        end else begin
            MsgBox('El proceso se proceso con errores. ' + CRLF +
                   mensajeTotalRegistros + CRLF +  CRLF +
                   'Se gener� el archivo de log ' + NombreArchivoLog, self.Caption);
        end;
        ConveniosDistintos.Free;

        btnProcesar.Enabled := true;
    end;
end;


end.
