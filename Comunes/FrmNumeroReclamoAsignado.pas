{-------------------------------------------------------------------------------
 File Name: FrmNumeroReclamoAsignado.pas
 Author: lgisuk
 Date Created: 01/02/05
 Language: ES-AR
 Description: Informo el numero de Reclamo Asignado
-------------------------------------------------------------------------------}
unit FrmNumeroReclamoAsignado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UtilProc, FrmRptReclamo;

type
  TFNumeroReclamoAsignado = class(TForm)
    AceptarBTN: TButton;
    Ltitulo: TLabel;
    LNumero: TLabel;
    P1: TPanel;
    P2: TPanel;
    procedure AceptarBTNClick(Sender: TObject);
  private
    { Private declarations }
    FNumeroReclamo: String;
  public
    function Inicializar(NumeroReclamo:string): Boolean;
    { Public declarations }
  end;

var
  FNumeroReclamoAsignado: TFNumeroReclamoAsignado;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 01/04/2005
  Description:  Informo el numero de Reclamo Asignado
  Parameters: NumeroReclamo:string
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFNumeroReclamoAsignado.Inicializar(NumeroReclamo:string): Boolean;
begin
    FNumeroReclamo  := NumeroReclamo;
    LNumero.Caption := FNumeroReclamo;
    Result          := True;
end;


{******************************** Function Header ******************************
Function Name: LContactarClienteClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision 1:
    Author : ggomez
    Date : 23/02/2006
    Description : Agregu� lo posibilidad de realizar la impresi�n de un reporte
        con los datos del reclamo.

*******************************************************************************}
procedure TFNumeroReclamoAsignado.AceptarBTNClick(Sender: TObject);
resourcestring
    MSG_NOMBRE_REPORTE          = 'Reclamo';
    MSG_DESEA_IMPRIMIR_RECLAMO  = '�Desea imprimir los datos del reclamo?';
    MSG_ERROR_CONVERTIR_NRO_RECLAMO = 'Ha ocurrido un error al convertir a entero el N�mero de Reclamo';
var
    f: TFRptReclamo;
    NumReclamo: Integer;
begin
    if MsgBox(MSG_DESEA_IMPRIMIR_RECLAMO, Caption, MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then begin
        try
            NumReclamo := StrToInt(FNumeroReclamo);
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_CONVERTIR_NRO_RECLAMO, E.Message, Caption, MB_ICONSTOP);
                Exit;
            end;
        end; // except

        f := TFRptReclamo.Create(Self);
        try
            f.Inicializar(MSG_NOMBRE_REPORTE, NumReclamo);
        finally
            f.Release;
        end; // finally

    end; // if
end;

end.
