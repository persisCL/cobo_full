object frm_AnulacionRecibos: Tfrm_AnulacionRecibos
  Left = 280
  Top = 140
  BorderStyle = bsSingle
  Caption = 'Anulaci'#243'n de Recibos'
  ClientHeight = 478
  ClientWidth = 739
  Color = clBtnFace
  Constraints.MinHeight = 445
  Constraints.MinWidth = 586
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  DesignSize = (
    739
    478)
  PixelsPerInch = 96
  TextHeight = 13
  object gbPorCliente: TGroupBox
    Left = 5
    Top = 2
    Width = 728
    Height = 64
    Anchors = [akLeft, akTop, akRight]
    Caption = ' B'#250'squeda de Recibos'
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 169
      Top = 14
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object lblNumeroConvenio: TLabel
      Left = 317
      Top = 14
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object lblNumeroRecibo: TLabel
      Left = 12
      Top = 13
      Width = 74
      Height = 13
      Caption = 'N'#250'mero Recibo'
    end
    object lblEnListaAmarilla: TLabel
      Left = 426
      Top = 8
      Width = 200
      Height = 14
      Alignment = taCenter
      AutoSize = False
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
    object peNumeroDocumento: TPickEdit
      Left = 169
      Top = 28
      Width = 145
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 1
      OnChange = peNumeroDocumentoChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenios: TVariantComboBox
      Left = 317
      Top = 28
      Width = 209
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbConveniosChange
      OnDrawItem = cbConveniosDrawItem
      Items = <>
    end
    object edNumeroRecibo: TNumericEdit
      Left = 12
      Top = 28
      Width = 143
      Height = 21
      TabOrder = 0
      OnChange = edNumeroReciboChange
    end
    object btnFiltrar: TButton
      Left = 641
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Filtrar'
      Default = True
      TabOrder = 3
      OnClick = btnFiltrarClick
    end
  end
  object gbDatosRecibo: TGroupBox
    Left = 4
    Top = 70
    Width = 729
    Height = 131
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Datos Recibo '
    TabOrder = 1
    DesignSize = (
      729
      131)
    object DBListEx2: TDBListEx
      Left = 6
      Top = 16
      Width = 710
      Height = 113
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Numero Recibo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumeroRecibo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHora'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescImporte'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescEstado'
        end>
      DataSource = dsRecibos
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object gbDatosDetalleRecibo: TGroupBox
    Left = 4
    Top = 208
    Width = 729
    Height = 154
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Detalle del Recibo '
    TabOrder = 2
    DesignSize = (
      729
      154)
    object DBListEx1: TDBListEx
      Left = 6
      Top = 16
      Width = 710
      Height = 129
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Tipo Compr.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescTipoComprobante'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'N'#186' Compr.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumeroComprobante'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHora'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescImporte'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Convenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'RUT'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 200
          Header.Caption = 'Cliente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NombreCliente'
        end>
      DataSource = dsComprobantesRecibo
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object gbAnulacion: TGroupBox
    Left = 6
    Top = 367
    Width = 729
    Height = 74
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' Anulaci'#243'n '
    TabOrder = 3
    DesignSize = (
      729
      74)
    object lblFechaAnulacion: TLabel
      Left = 10
      Top = 21
      Width = 100
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Fecha Anulaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMotivoAnulacion: TLabel
      Left = 8
      Top = 46
      Width = 103
      Height = 13
      Caption = 'Motivo Anulaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCanalPago: TLabel
      Left = 460
      Top = 46
      Width = 88
      Height = 13
      Caption = 'Canal de Pago:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object deFechaAnulacion: TDateEdit
      Left = 115
      Top = 18
      Width = 94
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object cbMotivosAnulacion: TVariantComboBox
      Left = 116
      Top = 42
      Width = 209
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object cbCanalesDePago: TVariantComboBox
      Left = 552
      Top = 42
      Width = 163
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
  end
  object btnAnular: TButton
    Left = 5
    Top = 449
    Width = 196
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Anular (F9)'
    TabOrder = 4
    OnClick = btnAnularClick
  end
  object btnSalir: TButton
    Left = 658
    Top = 449
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    ModalResult = 2
    TabOrder = 5
    OnClick = btnSalirClick
  end
  object spObtenerRecibosParaAnulacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRecibosParaAnulacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 162
    Top = 152
  end
  object spObtenerComprobantesRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerComprobantesRecibo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 40
      end>
    Left = 196
    Top = 279
  end
  object dsComprobantesRecibo: TDataSource
    DataSet = spObtenerComprobantesRecibo
    Left = 155
    Top = 280
  end
  object spAnularRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 120
    ProcedureName = 'AnularRecibo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaAnulacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoMotivoAnulacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoAnulacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 295
    Top = 280
  end
  object dsRecibos: TDataSource
    DataSet = spObtenerRecibosParaAnulacion
    OnDataChange = dsRecibosDataChange
    Left = 203
    Top = 152
  end
  object spCrearNotaDebitoDK: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CrearNotaDebitoDK;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroComprobanteNIPagado'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaAnulacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroNotaDebitoDK'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 351
    Top = 280
  end
  object spObtenerCanalPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCanalPago;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCanalPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 259
    Top = 312
  end
  object SPObtenerMotivosAnulacionPagos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMotivosAnulacionPagos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrigenPedido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 240
    Top = 272
  end
  object spActualizarInfraccionesPagoAnulado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesPagoAnulado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 496
    Top = 280
  end
end
