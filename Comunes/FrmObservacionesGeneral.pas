unit FrmObservacionesGeneral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, BuscaTab, StdCtrls, DmiCtrls, UtilDb, Util,
  ExtCtrls, DBTables, UtilProc, VariantComboBox, PeaProcs, DMConnection,
  DPSControls, PeaTypes;

type
  TFormObservacionesGeneral = class(TForm)
    lblObservaciones: TLabel;
    txtObservaciones: TMemo;
    Bevel1: TBevel;
    btn_Terminar: TButton;
    btnLimpiar: TButton;
    btnCancelar: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_TerminarClick(Sender: TObject);
//    function GetCodigoMotivo: integer;
    procedure FormShow(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    FObligatorio: boolean;
    function GetObservaciones: string;
    { Private declarations }
  public
    { Public declarations }
    Function Inicializa(Observaciones: string = ''; Obligatorio: boolean = True; Modo : TStateConvenio = scAlta): Boolean;
//    Function Inicializa(Tipo: string; VerMotivo: boolean; Observaciones: string = ''): Boolean;
//    property CodigoMotivo: integer read GetCodigoMotivo;
    property Observaciones: string read GetObservaciones;
  end;

var
  FormObservacionesGeneral: TFormObservacionesGeneral;

implementation

{$R *.dfm}

function TFormObservacionesGeneral.Inicializa(Observaciones: string = ''; Obligatorio: boolean = True; Modo : TStateConvenio = scAlta): Boolean;
begin
    FObligatorio := Obligatorio;
    if not Obligatorio then txtObservaciones.Color := clWindow;
    txtObservaciones.Text := Observaciones;
	txtObservaciones.ReadOnly := Modo = scNormal;
	btnLimpiar.Enabled := not txtObservaciones.ReadOnly;
	btnCancelar.Enabled := (Modo <> scNormal) and (Not Obligatorio);

	Result := True;
end;


procedure TFormObservacionesGeneral.btn_CancelarClick(Sender: TObject);
begin
	Close;
    ModalResult := mrCancel;
end;

procedure TFormObservacionesGeneral.btn_TerminarClick(Sender: TObject);
resourcestring
	MSG_OBSERVACIONES = 'No se pudieron ingresar las Observaciones.';
    CAPTION_OBSERVACIONES = 'Ingresar Observaciones';
    CAPTION_VALIDAR_OBSERVACIONES = 'Validar Observaciones';
    MSG_MOTIVO_OBSERVACION = 'Debe especificarse una observación.';

begin
    if FObligatorio then
        if not ValidateControls([txtObservaciones],
          [Trim(txtObservaciones.Text) <> ''],
          CAPTION_VALIDAR_OBSERVACIONES,
          [MSG_MOTIVO_OBSERVACION]) then exit;
	Close;
	ModalResult := mrOk;
end;

(*
function TFormObservacionesGeneral.GetCodigoMotivo: integer;
begin
    Result := cbMotivos.Value;
end;
*)

function TFormObservacionesGeneral.GetObservaciones: string;
begin
    result := Trim(txtObservaciones.Text);
end;

procedure TFormObservacionesGeneral.FormShow(Sender: TObject);
begin
//    if not cbMotivos.Visible then txtObservaciones.SetFocus;
end;

procedure TFormObservacionesGeneral.btnLimpiarClick(Sender: TObject);
resourcestring
    MSG_CONFIRMACION_OBSERVACION = 'Esta seguro de eliminar la observación actual ?';
begin
    if MsgBox(MSG_CONFIRMACION_OBSERVACION, caption, MB_OKCANCEL) = mrOk then
        txtObservaciones.Clear;
end;

procedure TFormObservacionesGeneral.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
	if Key = #27 then begin
		Key := #0;
		
		Close;
		ModalResult := mrCancel
	end
end;

end.
