{********************************** File Header ********************************
File Name   : utilImg.pas
Revision    : 1
Author      : Alejandro Labra
Date        : 26-05-2011
Description : (Fase 2)
             Se arregla problema cuando el punto de cobro era mayor que el
             array de coordenadas de posiciones para las im�genes a mostrar.
Firma       : PAR00133-Fase 2-ALA-20110427

Revisi�n   : 2
Autor      : Nelson Droguett Sierra
Fecha      : 07-Junio-2011
Descripci�n: Se copi� y renombr� de ImgProcs.CargarImagenPendientes
           a CargarImagenDeArchivoIndividual para evitar Referencias Circulares
           en el cambio en el formato de almacenamiento.
Firma       : SS-377-NDR-20110607

Firma       :   SS_1091_CQU_20130516
Descripcion :   Se cambia un ciclo en la funci�n DibujarViajeValidacion de 17
                a el largo del arreglo ListaPuntosCobro

Firma       :   SS_1147_CQU_20140408
Descripcion :   El objeto TPuntoCobroCN cambi� por TPuntoCobro y uno de sus campos tambien cambi�

Firma       :   SS_1409_CQU_20151102
descripcion :   Se modifica el m�mico y la forma de pintarlo, ahora se pintan los porticos y no una l�nea
                con este cambio se pretende eliminar el error de AccessViolation que se produce por la diferencia
                en la cantidad de p�rticos existentes entre concesionarias.
                Las coordenadas de la imagen est� en la tabla PuntosCobro.
*******************************************************************************}
unit utilImg;

interface

uses
    Windows, SysUtils, util, Graphics, Jpeg, JpegPlus, imgTypes, Peatypes, ExtCtrls, Controls, Types;

function FileOpenRetry(FileName: AnsiString; Mode: LongWord): Integer;
function ObtenerImagen(FIleName: AnsiString; var Imagen: TJPEGPLusImage;
  var DataImage: TDataImage; var DescriError: AnsiString): Boolean; overload;
function ObtenerImagen(FIleName: AnsiString; var Imagen: TBitmap;
  var DataImage: TDataImage; var DescriError: AnsiString): Boolean; overload;
function ObtenerImagen(FileName: AnsiString; var Imagen: TJPEGPLusImage;
  var DataImage: TDataImage; var DescriError: AnsiString; var ImagenCorrupta: Boolean): Boolean; overload;

function CargarImagenDeArchivoIndividual(HFile: Integer; var Imagen: TJpegPlusImage; var DataImage: TDataImage;             //SS-377-NDR-20110607
  var DescriError: AnsiString): Boolean; overload;                                                                          //SS-377-NDR-20110607
function CargarImagenDeArchivoIndividual(HFile: Integer; var Imagen: TBitmap; var DataImage: TDataImage;                    //SS-377-NDR-20110607
  var DescriError: AnsiString): Boolean; overload;                                                                          //SS-377-NDR-20110607

procedure DibujarViajeValidacion(ImagenViaje: TBitMap; ListaPuntosCobro: TListaPuntosCobro;
            //PuntoCobro: Integer = 0; ImgListMimico : TImageList = nil; imgListIzquierda : TImageList = nil);                                              //  SS_1409_CQU_2015110
            PuntoCobro: Integer = 0; ImgListMimico : TImageList = nil; imgListIzquierda : TImageList = nil); overload;                                      //  SS_1409_CQU_2015110

/// <summary>
/// Prende, en el m�mico de viajes, el p�rtico por el cual pas� un veh�culo.
/// </summary>
/// <param name="ImagenViaje">Imagen con el M�mico Cargado</param>
/// <param name="ListaPuntosCobro">Lista de Puntos de Cobro (array de TPuntoCobroCN)</param>
/// <param name="RutaON">Imagen con el p�rtico encendido</param>
/// <param name="RutaOff">Imagen con el p�rtico apagado</param>
/// <param name="PuntoCobro">N�mero del punto de cobro a encender</param>
procedure DibujarViajeValidacion(ImagenViaje : TBitMap; ListaPuntosCobro: TListaPuntosCobro;  RutaON, RutaOff : TImage; PuntoCobro : Integer); overload;    //  SS_1409_CQU_20151102

implementation



function FileOpenRetry(FileName: AnsiString; Mode: LongWord): Integer;
Const
	RETRY_TIMEOUT = 5000; // 10 seg.
Var
	Timeout: Int64;
begin
	if not FileExists(FileName) then begin
		Result := -1;
		Exit;
	end;
	Timeout := GetMSCounter + RETRY_TIMEOUT;
	Result := -1;
	While (Result < 0) and (GetMSCounter < Timeout) do begin
		Result := FileOpen(FileName, Mode);
		if Result < 0 then Sleep(50);
	end;
end;


function ObtenerImagen(FileName: AnsiString; var Imagen: TJPEGPLusImage;
  var DataImage: TDataImage; var DescriError: AnsiString): Boolean;
var
	Tamanio, H: Integer;
    jpegAux: TJpegImage;
begin
    Result := False;
    jpegaux := TJPegImage.Create;
    try
        try
            Imagen.LoadFromFile(fileName);
            // Cargamos los datos de la imagen
            H := FileOpen(FileName, fmOpenRead);
            Tamanio := FileSeek(H, 0, 2);
            FileSeek(H, tamanio - BYTES_EXTRAS_VR, 0);
            FileRead(H, DataImage.DataImageVR, BYTES_EXTRAS_VR);
            FIleCLose(H);
            //
            DataImage.DataImageVR.TipoImagen := swap(DataImage.DataImageVR.TipoImagen);
            DataImage.DataImageVR.PointID    := swap(DataImage.DataImageVR.PointID);
            DataImage.DataImageVR.SegmentoID := swap(DataImage.DataImageVR.SegmentoID);
            DataImage.DataImageVR.Camera     := swap(DataImage.DataImageVR.Camera);
            DataImage.DataImageVR.PosicionVehiculo := swap(DataImage.DataImageVR.PosicionVehiculo);
            DataImage.DataImageVR.PosicionPatente := swap(DataImage.DataImageVR.PosicionPatente);
            DataImage.DataImageVR.UpperLeftLPN.X  := swap(DataImage.DataImageVR.UpperLeftLPN.X);
            DataImage.DataImageVR.UpperLeftLPN.Y  := swap(DataImage.DataImageVR.UpperLeftLPN.Y);
            DataImage.DataImageVR.UpperRigthLPN.X := swap(DataImage.DataImageVR.UpperRigthLPN.X);
            DataImage.DataImageVR.UpperRigthLPN.Y := swap(DataImage.DataImageVR.UpperRigthLPN.Y);
            DataImage.DataImageVR.LowerRigthLPN.X := swap(DataImage.DataImageVR.LowerRigthLPN.X);
            DataImage.DataImageVR.LowerRigthLPN.Y := swap(DataImage.DataImageVR.LowerRigthLPN.Y);
            DataImage.DataImageVR.LowerLeftLPN.X  := swap(DataImage.DataImageVR.LowerLeftLPN.X);
            DataImage.DataImageVR.LowerLeftLPN.Y  := swap(DataImage.DataImageVR.LowerLeftLPN.Y);
            //
            result := True;
        except
            on e: Exception do
                DescriError := E.Message;
        end;
    finally
		jpegAux.Free;
	end;
end;

function ObtenerImagen(FileName: AnsiString; var Imagen: TJPEGPLusImage;
  var DataImage: TDataImage; var DescriError: AnsiString; var ImagenCorrupta: Boolean): Boolean;
var
	Tamanio, H: Integer;
    jpegAux: TJpegImage;
begin
    ImagenCorrupta := False;
    Result := False;
    jpegaux := TJPegImage.Create;
    try
        try
            try
                Imagen.LoadFromFile(fileName);
            except
                on e: exception do begin
                    if(e.ClassName = 'EJPEG') then
                        ImagenCorrupta := True;
                    DescriError := E.Message;
                    Exit;
                end;
            end;
            // Cargamos los datos de la imagen
            H := FileOpen(FileName, fmOpenRead);
            Tamanio := FileSeek(H, 0, 2);
            FileSeek(H, tamanio - BYTES_EXTRAS_VR, 0);
            FileRead(H, DataImage.DataImageVR, BYTES_EXTRAS_VR);
            FIleCLose(H);
            //
            DataImage.DataImageVR.TipoImagen := swap(DataImage.DataImageVR.TipoImagen);
            DataImage.DataImageVR.PointID    := swap(DataImage.DataImageVR.PointID);
            DataImage.DataImageVR.SegmentoID := swap(DataImage.DataImageVR.SegmentoID);
            DataImage.DataImageVR.Camera     := swap(DataImage.DataImageVR.Camera);
            DataImage.DataImageVR.PosicionVehiculo := swap(DataImage.DataImageVR.PosicionVehiculo);
            DataImage.DataImageVR.PosicionPatente := swap(DataImage.DataImageVR.PosicionPatente);
            DataImage.DataImageVR.UpperLeftLPN.X  := swap(DataImage.DataImageVR.UpperLeftLPN.X);
            DataImage.DataImageVR.UpperLeftLPN.Y  := swap(DataImage.DataImageVR.UpperLeftLPN.Y);
            DataImage.DataImageVR.UpperRigthLPN.X := swap(DataImage.DataImageVR.UpperRigthLPN.X);
            DataImage.DataImageVR.UpperRigthLPN.Y := swap(DataImage.DataImageVR.UpperRigthLPN.Y);
            DataImage.DataImageVR.LowerRigthLPN.X := swap(DataImage.DataImageVR.LowerRigthLPN.X);
            DataImage.DataImageVR.LowerRigthLPN.Y := swap(DataImage.DataImageVR.LowerRigthLPN.Y);
            DataImage.DataImageVR.LowerLeftLPN.X  := swap(DataImage.DataImageVR.LowerLeftLPN.X);
            DataImage.DataImageVR.LowerLeftLPN.Y  := swap(DataImage.DataImageVR.LowerLeftLPN.Y);
            //
            result := True;
        except
            on e: Exception do
                DescriError := E.Message;
        end;
    finally
		jpegAux.Free;
	end;
end;

function ObtenerImagen(FileName: AnsiString; var Imagen: TBitmap;
  var DataImage: TDataImage; var DescriError: AnsiString): Boolean;
var
	Tamanio, H, i: Integer;
    ajpeg: TJPegImage;
begin
    Result := False;
    ajpeg := TJPegImage.Create;
    try
        try
            ajpeg.LoadFromFile(fileName);
            imagen.Assign(ajpeg);
            // Cargamos los datos de la imagen
            H := FileOpen(FileName, fmOpenRead);
            Tamanio := FileSeek(H, 0, 2);
            FileSeek(H, tamanio - BYTES_EXTRAS_OVERVIEW, 0);
            FileRead(H, DataImage.DataOverView, BYTES_EXTRAS_OVERVIEW);
            FileCLose(H);
            //
            DataImage.DataOverView.TipoImagen   := swap(DataImage.DataOverView.TipoImagen);
            DataImage.DataOverView.FactorEscala := swap(DataImage.DataOverView.FactorEscala);
            DataImage.DataOverView.upperLeftX   := swap(DataImage.DataOverView.upperLeftX);
            DataImage.DataOverView.upperLeftY   := swap(DataImage.DataOverView.upperLeftY);
            DataImage.DataOverView.PointID      := swap(DataImage.DataOverView.PointID);
            DataImage.DataOverView.SegmentoID   := swap(DataImage.DataOverView.SegmentoID);
            DataImage.DataOverView.FechaHora    := swapDWord(DataImage.DataOverView.FechaHora);
            DataImage.DataOverView.TiempoCaptura := swapDWord(DataImage.DataOverView.TiempoCaptura);
            DataImage.DataOverView.VDCSensor    := swap(DataImage.DataOverView.VDCSensor);
            DataImage.DataOverView.VDCX         := swap(DataImage.DataOverView.VDCX);
            DataImage.DataOverView.Actual.RegistrationID := swapDWord(DataImage.DataOverView.Actual.RegistrationID);
            DataImage.DataOverView.Actual.VehicleID := swap(DataImage.DataOverView.Actual.VehicleID);
            DataImage.DataOverView.Actual.Largo     := swap(DataImage.DataOverView.Actual.Largo);
            DataImage.DataOverView.Actual.Ancho     := swap(DataImage.DataOverView.Actual.Ancho);
            DataImage.DataOverView.Actual.FrontX    := swap(DataImage.DataOverView.Actual.FrontX);
            DataImage.DataOverView.Actual.FrontY    := swap(DataImage.DataOverView.Actual.FrontY);

            for i := 1 to 24 do begin
                DataImage.DataOverView.Vehiculos[i].RegistrationID := swapDWord(DataImage.DataOverView.Vehiculos[i].RegistrationID);
                DataImage.DataOverView.Vehiculos[i].VehicleID   := swap(DataImage.DataOverView.Vehiculos[i].VehicleID);
                DataImage.DataOverView.Vehiculos[i].Largo       := swap(DataImage.DataOverView.Vehiculos[i].Largo);
                DataImage.DataOverView.Vehiculos[i].Ancho       := swap(DataImage.DataOverView.Vehiculos[i].Ancho);
                DataImage.DataOverView.Vehiculos[i].FrontX      := swap(DataImage.DataOverView.Vehiculos[i].FrontX);
                DataImage.DataOverView.Vehiculos[i].FrontY      := swap(DataImage.DataOverView.Vehiculos[i].FrontY);
            end;

            result := True;
        except
            on e: Exception do
                DescriError := E.Message;
        end;
    finally
        ajpeg.Free;
    end;
end;


procedure DibujarViajeValidacion(ImagenViaje: TBitMap; ListaPuntosCobro: TListaPuntosCobro;
            PuntoCobro: Integer = 0; ImgListMimico : TImageList = nil; imgListIzquierda : TImageList = nil);

    function GetColorClaroSentidoAscendente: TColor;
    begin
        // Verde claro.
        Result := RGB(96, 200, 61);
    end;

    function GetColorOscuroSentidoAscendente: TColor;
    begin
        // Verde oscuro.
        Result := RGB(96, 150, 61);
    end;

    function GetColorClaroSentidoDescendente: TColor;
    begin
        // Anaranjado claro
        Result := RGB(255, 183, 0);
    end;

    function GetColorOscuroSentidoDescendente: TColor;
    begin
        // Anaranjado oscuro
        Result := RGB(255, 150, 0);
    end;

var
    X, Y, i, j: Integer;
    aLeft, aTop: Integer;
    bmpSource: TBitMap;
//    IndiceImagenPortico: Integer;
const
    WIDTH_ICONO = 38;
    HEIGHT_ICONO = 24;
    SENTIDO_ASCENDENTE = 1;
    SENTIDO_DESCENDENTE = 2;
    INDICE_BASE_IMAGEN_ASCENDENTE = 9;
    INDICE_BASE_IMAGEN_DESCENDENTE = 0;
begin
    // Recorro todos los puntos de cobro
    //for i:= 1 to 17 do begin                      				// SS_1091_CQU_20130516
    for i:= 1 to Length(ListaPuntosCobro) do begin  				// SS_1091_CQU_20130516
        // Por cada Punto de cobro pinto lo que hace falta
        if ListaPuntosCobro[i].EsTransito then begin

            if (ListaPuntosCobro[i].Sentido = 1) then begin
                (* Pintar con otro color el Tramo de la imagen que pasa por el
                p�rtico PuntoCobro. *)
                if PuntoCobro = i then
                    // Verde m�s oscuro
                    ImagenViaje.Canvas.Brush.Color := GetColorOscuroSentidoAscendente
                else
                    // Verde m�s claro
                    ImagenViaje.Canvas.Brush.Color := GetColorClaroSentidoAscendente;
            end else begin
                (* Pintar con otro color el Tramo de la imagen que pasa por el
                p�rtico PuntoCobro. *)
                if PuntoCobro = i then
                    // Anaranjado m�s oscuro
                    ImagenViaje.Canvas.Brush.Color := GetColorOscuroSentidoDescendente
                else
                    // Anaranjado m�s claro
                    ImagenViaje.Canvas.Brush.Color := GetColorClaroSentidoDescendente;
            end;

            j := 1;
            // while TramosPuntosCobro[ListaPuntosCobro[i].CodigoCN, ListaPuntosCobro[i].Sentido, j, 1] <> 0 do begin           // SS_1147_CQU_20140408
            while TramosPuntosCobro[ListaPuntosCobro[i].CodigoConcesionaria, ListaPuntosCobro[i].Sentido, j, 1] <> 0 do begin   // SS_1147_CQU_20140408
                (* Obtener la coordenada X del punto en el cual hacer FloodFill. *)
                //X := TramosPuntosCobro[ListaPuntosCobro[i].CodigoCN, ListaPuntosCobro[i].Sentido, j, 1];                      // SS_1147_CQU_20140408
                X := TramosPuntosCobro[ListaPuntosCobro[i].CodigoConcesionaria, ListaPuntosCobro[i].Sentido, j, 1];             // SS_1147_CQU_20140408
                (* Obtener la coordenada Y del punto en el cual hacer FloodFill. *)
                //Y := TramosPuntosCobro[ListaPuntosCobro[i].codigoCN, ListaPuntosCobro[i].Sentido, j, 2];                      // SS_1147_CQU_20140408
                Y := TramosPuntosCobro[ListaPuntosCobro[i].CodigoConcesionaria, ListaPuntosCobro[i].Sentido, j, 2];             // SS_1147_CQU_20140408
                // Pintar el tramo.
                ImagenViaje.Canvas.FloodFill(X, Y, ImagenViaje.Canvas.Pixels[X, Y], fsSurface);

                Inc(j);
            end;

        end;

    end;

    // Finalmente pintamos los nudos
    // Primero desde el 8 al 3
    if ListaPuntosCobro[14].EsTransito and ListaPuntosCobro[5].EsTransito then begin
       ImagenViaje.Canvas.Brush.Color := GetColorClaroSentidoAscendente;
       for i := 1 to 5 do begin
           ImagenViaje.Canvas.FloodFill(TramosOchoATres[i, 1], TramosOchoATres[i, 2],
             ImagenViaje.Canvas.Pixels[TramosOchoATres[i, 1], TramosOchoATres[i, 2]], fsSurface);
       end;
    end;
    // Primero desde el 3 al 8
    if ListaPuntosCobro[6].EsTransito and ListaPuntosCobro[15].EsTransito then begin
       ImagenViaje.Canvas.Brush.Color := GetColorClaroSentidoDescendente;
       for i := 1 to 1 do begin
           ImagenViaje.Canvas.FloodFill(TramosTresAOcho[i, 1], TramosTresAOcho[i, 2],
             ImagenViaje.Canvas.Pixels[TramosTresAOcho[i, 1], TramosTresAOcho[i, 2]], fsSurface);
       end;
    end;
    // Segundo desde el 3 al 2
    if ListaPuntosCobro[6].EsTransito and ListaPuntosCobro[4].EsTransito then begin
       ImagenViaje.Canvas.Brush.Color := GetColorClaroSentidoDescendente;
       for i := 1 to 6 do begin
           ImagenViaje.Canvas.FloodFill(TramosTresADos[i, 1], TramosTresADos[i, 2],
             ImagenViaje.Canvas.Pixels[TramosTresADos[i, 1], TramosTresADos[i, 2]], fsSurface);
       end;
    end;

    // Se�alizamos el Punto de Cobro actual
    if PuntoCobro <> 0 then begin

        bmpSource := TBitmap.create;
        try
            if High(CoordenadasPuntosCobro) >= PuntoCobro then begin            //PAR00133-Fase 2-ALA-20110427
                // Primero seleccionamos la posicion de dibujo del icono
                //aleft := CoordenadasPuntosCobro[ListaPuntosCobro[PuntoCobro].CodigoCN, 1];                    // SS_1147_CQU_20140408
                //aTop := CoordenadasPuntosCobro[ListaPuntosCobro[PuntoCobro].CodigoCN, 2];                     // SS_1147_CQU_20140408
                aleft := CoordenadasPuntosCobro[ListaPuntosCobro[PuntoCobro].CodigoConcesionaria, 1];           // SS_1147_CQU_20140408
                aTop := CoordenadasPuntosCobro[ListaPuntosCobro[PuntoCobro].CodigoConcesionaria, 2];            // SS_1147_CQU_20140408
                (* Despu�s seleccionamos la imagen desde la lista que corresponda
                seg�n el sentido. *)
                if ListaPuntosCobro[PuntoCobro].Sentido = SENTIDO_ASCENDENTE then
                    //imgListIzquierda.GetBitmap(ListaPuntosCobro[PuntoCobro].CodigoCN - 1, bmpSource)          // SS_1147_CQU_20140408
                    imgListIzquierda.GetBitmap(ListaPuntosCobro[PuntoCobro].CodigoConcesionaria - 1, bmpSource) // SS_1147_CQU_20140408
                else
                    //ImgListMimico.GetBitmap(ListaPuntosCobro[PuntoCobro].CodigoCN - 1, bmpSource);            // SS_1147_CQU_20140408
                    ImgListMimico.GetBitmap(ListaPuntosCobro[PuntoCobro].CodigoConcesionaria - 1, bmpSource);   // SS_1147_CQU_20140408

    { 'Se podr�a Cambiar el c�digo para utilizar una sola ImgList.'}
    (*            IndiceImagenPortico := -1;
                if ListaPuntosCobro[PuntoCobro].Sentido = SENTIDO_ASCENDENTE then
                    IndiceImagenPortico := ListaPuntosCobro[PuntoCobro].CodigoCN - 1 + INDICE_BASE_IMAGEN_ASCENDENTE
                else
                    IndiceImagenPortico := ListaPuntosCobro[PuntoCobro].CodigoCN - 1 + INDICE_BASE_IMAGEN_DESCENDENTE;

                ImgListMimico.GetBitmap(IndiceImagenPortico, bmpSource);
    *)

                ImagenViaje.Canvas.CopyRect(Rect(aLeft, aTop, aleft + WIDTH_ICONO, aTop + HEIGHT_ICONO),
                  bmpSource.Canvas , Rect(0, 0, WIDTH_ICONO, HEIGHT_ICONO));
            end;
        finally
            bmpSource.free;
        end;
    end;
end;

function CargarImagenDeArchivoIndividual(HFile: Integer; var Imagen: TJpegPlusImage; var DataImage: TDataImage;               //SS-377-NDR-20110607
  var DescriError: AnsiString): Boolean;                                                                                      //SS-377-NDR-20110607
var                                                                                                                           //SS-377-NDR-20110607
	Buffer: PChar;                                                                                                            //SS-377-NDR-20110607
	ArchiAux: AnsiString;                                                                                                     //SS-377-NDR-20110607
	H, Tamanio: Integer;                                                                                                      //SS-377-NDR-20110607
begin                                                                                                                         //SS-377-NDR-20110607
	// Si tenemos la seguridad que las fotos son J12, directamente podemos cargar                                             //SS-377-NDR-20110607
	// un TPicture. Pero en las demos, seguramente tenemos jpegs, entonces, probamos.                                         //SS-377-NDR-20110607
	Result := False;                                                                                                          //SS-377-NDR-20110607
    try                                                                                                                       //SS-377-NDR-20110607
        // Generamos un archivo temporal                                                                                      //SS-377-NDR-20110607
        ArchiAux := GetTempDir + TempFile + '.j12';                                                                           //SS-377-NDR-20110607
        H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);                                                           //SS-377-NDR-20110607
        if H = -1 then raise exception.Create('Error al abrir archivo.');                                                     //SS-377-NDR-20110607
        FileSeek(H, 0, 0);                                                                                                    //SS-377-NDR-20110607
        Tamanio := FileSeek(HFile, 0, 2);                                                                                     //SS-377-NDR-20110607
        FileSeek(HFile, 0, 0);                                                                                                //SS-377-NDR-20110607
        Buffer  := AllocMem(Tamanio);                                                                                         //SS-377-NDR-20110607
        FileRead(HFile, Buffer^, Tamanio);                                                                                    //SS-377-NDR-20110607
        FileWrite(H, Buffer^, Tamanio);                                                                                       //SS-377-NDR-20110607
        FileClose(H);                                                                                                         //SS-377-NDR-20110607
        FreeMem(Buffer);                                                                                                      //SS-377-NDR-20110607
        // Obtenemos la imagen                                                                                                //SS-377-NDR-20110607
        if not ObtenerImagen(ArchiAux, Imagen, DataImage, DescriError) then begin                                             //SS-377-NDR-20110607
            DeleteFile(ArchiAux);                                                                                             //SS-377-NDR-20110607
            result := false;                                                                                                  //SS-377-NDR-20110607
        end else begin                                                                                                        //SS-377-NDR-20110607
            Result := True;                                                                                                   //SS-377-NDR-20110607
        end;                                                                                                                  //SS-377-NDR-20110607
    except                                                                                                                    //SS-377-NDR-20110607
        on e: exception do begin                                                                                              //SS-377-NDR-20110607
            DescriError := e.Message;                                                                                         //SS-377-NDR-20110607
        end;                                                                                                                  //SS-377-NDR-20110607
    end;                                                                                                                      //SS-377-NDR-20110607
end;                                                                                                                          //SS-377-NDR-20110607

function CargarImagenDeArchivoIndividual(HFile: Integer; var Imagen: TBitmap; var DataImage: TDataImage;                      //SS-377-NDR-20110607
  var DescriError: AnsiString): Boolean;                                                                                      //SS-377-NDR-20110607
var                                                                                                                           //SS-377-NDR-20110607
	Buffer: PChar;                                                                                                            //SS-377-NDR-20110607
	ArchiAux: AnsiString;                                                                                                     //SS-377-NDR-20110607
	H, Tamanio: Integer;                                                                                                      //SS-377-NDR-20110607
begin                                                                                                                         //SS-377-NDR-20110607
	// Si tenemos la seguridad que las fotos son J12, directamente podemos cargar                                             //SS-377-NDR-20110607
	// un TPicture. Pero en las demos, seguramente tenemos jpegs, entonces, probamos.                                         //SS-377-NDR-20110607
	Result := False;                                                                                                          //SS-377-NDR-20110607
    try                                                                                                                       //SS-377-NDR-20110607
        // Generamos un archivo temporal                                                                                      //SS-377-NDR-20110607
        ArchiAux := GetTempDir + TempFile + '.j12';                                                                           //SS-377-NDR-20110607
        H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);                                                           //SS-377-NDR-20110607
        if H = -1 then raise exception.Create('Error al abrir archivo.');                                                     //SS-377-NDR-20110607
        FileSeek(H, 0, 0);                                                                                                    //SS-377-NDR-20110607
        Tamanio := FileSeek(HFile, 0, 2);                                                                                     //SS-377-NDR-20110607
        FileSeek(HFile, 0, 0);                                                                                                //SS-377-NDR-20110607
        Buffer  := AllocMem(Tamanio);                                                                                         //SS-377-NDR-20110607
        FileRead(HFile, Buffer^, Tamanio);                                                                                    //SS-377-NDR-20110607
        FileWrite(H, Buffer^, Tamanio);                                                                                       //SS-377-NDR-20110607
        FileClose(H);                                                                                                         //SS-377-NDR-20110607
        FreeMem(Buffer);                                                                                                      //SS-377-NDR-20110607
        // Obtenemos la imagen                                                                                                //SS-377-NDR-20110607
        if not ObtenerImagen(ArchiAux, Imagen, DataImage, DescriError) then begin                                             //SS-377-NDR-20110607
            DeleteFile(ArchiAux);                                                                                             //SS-377-NDR-20110607
            result := false;                                                                                                  //SS-377-NDR-20110607
        end else begin                                                                                                        //SS-377-NDR-20110607
            Result := True;                                                                                                   //SS-377-NDR-20110607
        end;                                                                                                                  //SS-377-NDR-20110607
    except                                                                                                                    //SS-377-NDR-20110607
        on e: exception do begin                                                                                              //SS-377-NDR-20110607
            DescriError := e.Message;                                                                                         //SS-377-NDR-20110607
        end;                                                                                                                  //SS-377-NDR-20110607
    end;                                                                                                                      //SS-377-NDR-20110607
end;                                                                                                                          //SS-377-NDR-20110607

procedure DibujarViajeValidacion(ImagenViaje : TBitMap; ListaPuntosCobro: TListaPuntosCobro;  RutaON, RutaOff : TImage; PuntoCobro : Integer);  //  SS_1409_CQU_20151102
var                                                                                                                                             //  SS_1409_CQU_20151102
    i, x, y : Integer;                                                                                                                          //  SS_1409_CQU_20151102
begin                                                                                                                                           //  SS_1409_CQU_20151102
    // Recorro todos los puntos de cobro                                                                                                        //  SS_1409_CQU_20151102
    for i:= 0 to Length(ListaPuntosCobro) - 1 do begin                                                                                          //  SS_1409_CQU_20151102
        x := ListaPuntosCobro[i].X;                                                                                                             //  SS_1409_CQU_20151102
        y := ListaPuntosCobro[i].Y;                                                                                                             //  SS_1409_CQU_20151102
        // Sin dimensiones, continuo con el siguiente                                                                                           //  SS_1409_CQU_20151102
        if (X = 0) and (Y = 0) then Continue;                                                                                                   //  SS_1409_CQU_20151102
        // Por cada Punto de cobro pinto el portico                                                                                             //  SS_1409_CQU_20151102
        if (ListaPuntosCobro[i].EsTransito) or ((PuntoCobro > 0) and (ListaPuntosCobro[i].NumeroPuntoCobro = PuntoCobro)) then                  //  SS_1409_CQU_20151102
            ImagenViaje.Canvas.Draw(x, y, RutaON.Picture.Bitmap);                                                                               //  SS_1409_CQU_20151102
    end;                                                                                                                                        //  SS_1409_CQU_20151102
end;                                                                                                                                            //  SS_1409_CQU_20151102

end.
