object FormInicioConsultaConvenio: TFormInicioConsultaConvenio
  Left = 96
  Top = 72
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  ActiveControl = peNumeroDocumento
  Caption = 'Datos del Cliente'
  ClientHeight = 742
  ClientWidth = 1282
  Color = clWhite
  Constraints.MinHeight = 492
  Constraints.MinWidth = 652
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cpVehiculo: TCollapsablePanel
    Left = 0
    Top = 245
    Width = 1282
    Height = 167
    Align = alTop
    Color = clBtnFace
    ParentColor = False
    Caption = 'Veh'#237'culos'
    BarColorStart = 14071199
    BarColorEnd = 14071199
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    Style = cpsWinXP
    InternalSize = 144
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    object Panel14: TPanel
      Left = 0
      Top = 23
      Width = 1282
      Height = 144
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        1282
        144)
      object lblFiltroPatente: TLabel
        Left = 24
        Top = 12
        Width = 83
        Height = 13
        Caption = 'Filtrar por Patente'
      end
      object dblCuentas: TDBListEx
        Left = 0
        Top = 40
        Width = 1282
        Height = 107
        Align = alCustom
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 60
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'Patente'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 85
            Header.Caption = 'Telev'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'Etiqueta'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clHotLight
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Img Ref'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'ImagenReferencia'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 26
            Header.Caption = 'LV'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'SetGreenList'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 26
            Header.Caption = 'LN'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'SetBlackList'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 26
            Header.Caption = 'LA'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'SetYellowList'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 27
            Header.Caption = 'EH'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'ExceptionHandling'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 28
            Header.Caption = 'CO'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'MMIContactOperator'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 36
            Header.Caption = 'NOK'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'MMINotOk'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Fecha Alta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'FechaAltaCuenta'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 95
            Header.Caption = 'Fecha Baja'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'FechaBajaCuenta'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Fecha Suspensi'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'Suspendida'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Fecha Actualizaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'FechaHoraActualizacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Actualiz'#243
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'UsuarioActualizacion'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Estado Telev'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'EstadosConservacionTags'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Almacen'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'CodigoAlmacen'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Categor'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'CodigoCategoria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Marca y modelo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'MarcaModeloColor'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Vto Garantia'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = ordenarColumna
            FieldName = 'FechaVencimientoTAG'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 125
            Header.Caption = 'Suscrito Lista Blanca'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'SuscritoListaBlanca'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 130
            Header.Caption = 'Habilitado Lista Blanca'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'HabilitadoListaBlanca'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Convenio de Facturaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'ConvenioFacturacion'
          end>
        DataSource = dsObtenerCuentasCartola
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnContextPopup = dblCuentasContextPopup
        OnDblClick = dblCuentasDblClick
        OnDrawText = dblCuentasDrawText
        OnLinkClick = dblCuentasLinkClick
      end
      object cb_MostrarBajas: TCheckBox
        Left = 1098
        Top = 3
        Width = 184
        Height = 17
        Anchors = [akTop, akRight]
        Caption = '&Mostrar Veh'#237'culos dados de Baja'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = cb_MostrarBajasClick
      end
      object btnBuscarVehiculoPorPatente: TButton
        Left = 224
        Top = 6
        Width = 75
        Height = 25
        Caption = 'Buscar'
        TabOrder = 2
        OnClick = btnBuscarVehiculoPorPatenteClick
      end
      object edtFiltrarPatente: TEdit
        Left = 118
        Top = 8
        Width = 85
        Height = 21
        TabOrder = 1
      end
    end
  end
  object cpBusquedaConvenio: TCollapsablePanel
    Left = 0
    Top = 0
    Width = 1282
    Height = 245
    Align = alTop
    Caption = 'Convenio'
    BarColorStart = 14071199
    BarColorEnd = 10646097
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    Style = cpsWinXP
    InternalSize = 222
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    object pnlDatosConvenio: TPanel
      Left = 0
      Top = 23
      Width = 1282
      Height = 225
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object Shape5: TShape
        Left = 526
        Top = 164
        Width = 272
        Height = 52
      end
      object Shape4: TShape
        Left = 802
        Top = 107
        Width = 236
        Height = 53
      end
      object Shape2: TShape
        Left = 802
        Top = 21
        Width = 236
        Height = 84
      end
      object Shape1: TShape
        Left = 526
        Top = 21
        Width = 272
        Height = 123
      end
      object Label7: TLabel
        Left = 533
        Top = 23
        Width = 46
        Height = 13
        Caption = 'Intereses:'
      end
      object Label15: TLabel
        Left = 533
        Top = 67
        Width = 60
        Height = 13
        Caption = 'Descuentos:'
      end
      object Label3: TLabel
        Left = 533
        Top = 37
        Width = 88
        Height = 13
        Caption = 'Peaje del Per'#237'odo:'
      end
      object lblDescuentos: TLabel
        Left = 699
        Top = 67
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Descuentos'
      end
      object lblPeajeDelPeriodo: TLabel
        Left = 699
        Top = 37
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PeajeDelPer'#237'odo'
      end
      object Bevel3: TBevel
        Left = 530
        Top = 107
        Width = 263
        Height = 8
        Shape = bsBottomLine
      end
      object lblTitTotalAPagar: TLabel
        Left = 532
        Top = 100
        Width = 120
        Height = 13
        Caption = 'Total Nota de Cobro:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTotalAPagar: TLabel
        Left = 676
        Top = 100
        Width = 114
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Total a Pagar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 13
        Top = 32
        Width = 26
        Height = 13
        Caption = 'RUT:'
        Color = 16776699
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Bevel5: TBevel
        Left = 11
        Top = 51
        Width = 494
        Height = 8
        Shape = bsBottomLine
      end
      object lblNombre: TLabel
        Left = 15
        Top = 62
        Width = 380
        Height = 13
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDomicilio: TLabel
        Left = 15
        Top = 73
        Width = 380
        Height = 28
        AutoSize = False
        WordWrap = True
      end
      object lblEmail: TLabel
        Left = 37
        Top = 199
        Width = 94
        Height = 13
        Cursor = crHandPoint
        Caption = 'Direcci'#243'n de Correo'
        Constraints.MaxWidth = 290
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblEmailClick
      end
      object lblTelParticular: TLabel
        Left = 37
        Top = 146
        Width = 285
        Height = 13
        AutoSize = False
      end
      object lblTelMovil: TLabel
        Left = 37
        Top = 163
        Width = 285
        Height = 13
        AutoSize = False
      end
      object lblTelComercial: TLabel
        Left = 37
        Top = 181
        Width = 285
        Height = 13
        AutoSize = False
      end
      object Bevel6: TBevel
        Left = 11
        Top = 131
        Width = 494
        Height = 8
        Shape = bsBottomLine
      end
      object Label18: TLabel
        Left = 809
        Top = 33
        Width = 83
        Height = 13
        Caption = 'Viajero Frecuente'
      end
      object Label21: TLabel
        Left = 809
        Top = 81
        Width = 58
        Height = 13
        Caption = 'Infracciones'
      end
      object Label23: TLabel
        Left = 809
        Top = 57
        Width = 71
        Height = 13
        Caption = 'Tipo de Cliente'
      end
      object Label2: TLabel
        Left = 181
        Top = 32
        Width = 48
        Height = 13
        Caption = 'Convenio:'
        Color = 16776699
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object lblEditar: TLabel
        Left = 447
        Top = 183
        Width = 58
        Height = 13
        Cursor = crHandPoint
        Caption = 'M'#225's datos...'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentColor = False
        ParentFont = False
        OnClick = lblEditarClick
      end
      object Led1: TLed
        Left = 955
        Top = 34
        Width = 16
        Height = 16
        Blinking = False
        Diameter = 15
        Enabled = True
        ColorOn = clWindow
        ColorOff = clBtnFace
      end
      object Led2: TLed
        Left = 955
        Top = 56
        Width = 16
        Height = 15
        Blinking = False
        Diameter = 15
        Enabled = True
        ColorOn = clWhite
        ColorOff = clBtnFace
      end
      object Led3: TLed
        Left = 955
        Top = 80
        Width = 16
        Height = 16
        Blinking = False
        Diameter = 15
        Enabled = True
        ColorOn = clWhite
        ColorOff = clBtnFace
      end
      object Label17: TLabel
        Left = 532
        Top = 117
        Width = 105
        Height = 13
        Caption = 'Ajuste Sencillo Actual:'
        Visible = False
      end
      object lblAjusteSencilloActual: TLabel
        Left = 699
        Top = 117
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Ajuste Senc. Act.'
        Visible = False
      end
      object lblConcesionaria: TLabel
        Left = 15
        Top = 107
        Width = 380
        Height = 13
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Shape3: TShape
        Left = 802
        Top = 162
        Width = 236
        Height = 54
      end
      object lblEstadoConvenio: TLabel
        Left = 807
        Top = 167
        Width = 226
        Height = 13
        AutoSize = False
        Caption = 'Estado del Convenio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblVerImagenScan: TLabel
        Left = 440
        Top = 165
        Width = 65
        Height = 13
        Cursor = crHandPoint
        Caption = 'Ver Im'#225'genes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblVerImagenScanClick
      end
      object Label9: TLabel
        Left = 807
        Top = 111
        Width = 99
        Height = 13
        Caption = 'Pr'#243'xima Facturaci'#243'n:'
      end
      object Label10: TLabel
        Left = 807
        Top = 128
        Width = 41
        Height = 13
        Caption = 'Per'#237'odo:'
      end
      object lblFechaProximaFacturacion: TLabel
        Left = 917
        Top = 112
        Width = 58
        Height = 13
        Caption = '99/99/9999'
      end
      object lblInicioProximaFacturacion: TLabel
        Left = 853
        Top = 128
        Width = 58
        Height = 13
        Caption = '99/99/9999'
      end
      object lblfinProximaFacturacion: TLabel
        Left = 918
        Top = 128
        Width = 58
        Height = 13
        Caption = '99/99/9999'
      end
      object Label24: TLabel
        Left = 909
        Top = 129
        Width = 9
        Height = 13
        Caption = ' - '
      end
      object Image1: TImage
        Left = 15
        Top = 197
        Width = 16
        Height = 16
        Hint = 'Direcci'#243'n de correo electr'#243'nico'
        AutoSize = True
        ParentShowHint = False
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          0000100000000100180000000000000300000000000000000000000000000000
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D4A4A4D
          4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AFF00FFFF
          00FFFF00FFFF00FF4A4D4A4A4D4A21E7FF21E7FFADF3FFADF3FFADF3FFADF3FF
          ADF3FF21E7FF21E7FF4A4D4A4A4D4AFF00FFFF00FFFF00FF4A4D4A21E7FF4A4D
          4A21E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF21E7FF4A4D4A21E7FF4A4D4AFF
          00FFFF00FFFF00FF4A4D4A21E7FFADF3FF4A4D4A21E7FFADF3FF4A4D4AADF3FF
          21E7FF4A4D4AADF3FF21E7FF4A4D4AFF00FFFF00FFFF00FF4A4D4A21E7FFADF3
          FFFFFFFF4A4D4A4A4D4A21E7FF4A4D4A4A4D4A21E7FFFFFFFF21E7FF4A4D4AFF
          00FFFF00FFFF00FF4A4D4A21E7FFADF3FFFFFFFF4A4D4AADF3FFFFFFFFADF3FF
          4A4D4A21E7FFFFFFFF21E7FF4A4D4AFF00FFFF00FFFF00FF4A4D4A21E7FFADF3
          FF4A4D4A21E7FFFFFFFFFFFFFFFFFFFF21E7FF4A4D4AADF3FF21E7FF4A4D4AFF
          00FFFF00FFFF00FF4A4D4A21E7FF4A4D4AADF3FFADF3FFADF3FFADF3FFADF3FF
          ADF3FF21E7FF4A4D4A21E7FF4A4D4AFF00FFFF00FFFF00FF4A4D4A4A4D4A21E7
          FF21E7FF21E7FF21E7FF21E7FF21E7FF21E7FF21E7FF21E7FF4A4D4A4A4D4AFF
          00FFFF00FFFF00FFFF00FF4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A
          4A4D4A4A4D4A4A4D4A4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF}
        ShowHint = True
        Stretch = True
        Transparent = True
      end
      object Image2: TImage
        Left = 15
        Top = 162
        Width = 16
        Height = 16
        Hint = 'Tel'#233'fono m'#243'vil'
        AutoSize = True
        ParentShowHint = False
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          0000100000000100180000000000000300000000000000000000000000000000
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D
          4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF4A4D4A00EFFF00E3FF00E3FF18E7FF18E7FF00E3FF
          21E7FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D4A42EB
          FF8CF3FFE7F7FFC6FFFFC6FFFFA5F7FF21D7FF4A4D4AFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF4A4D4A21E7FF4A4D4AEFFBFF4A4D4AFFFFFF4A4D4A
          21D7FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D4A21E7
          FFEFFBFFEFFBFFFFFFFFFFFFFFB5FFFF21E7FF4A4D4AFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF4A4D4A21E7FF4A4D4AFFFFFF4A4D4AFFFFFF4A4D4A
          21E7FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D4A39EB
          FF9CF3FFADF3FFADF3FFADF3FFB5EBFF21E7FF4A4D4AFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF4A4D4A21E7FF4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A
          21E7FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D4A21E7
          FF4A4D4ACECFCECECFCECECFCE4A4D4A21E7FF4A4D4AFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF4A4D4A21E7FF4A4D4ACECFCECECFCECECFCE4A4D4A
          21E7FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D4A21E7
          FF4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A21E7FF4A4D4AFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF4A4D4A21E7FF6BF3FF6BF3FF6BF3FF6BF3FF6BF3FF
          21E7FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D
          4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4A4D
          4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF}
        ShowHint = True
        Stretch = True
        Transparent = True
      end
      object Image3: TImage
        Left = 15
        Top = 180
        Width = 16
        Height = 16
        Hint = 'Tel'#233'fono comercial'
        AutoSize = True
        ParentShowHint = False
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          0000100000000100180000000000000300000000000000000000000000000000
          0000840084840084840084840084840084840084840084840084840084840084
          8400848400848400848400848400848400848400848475848482848482848482
          848482848482848482848482848482848482844A4D4A4A4D4A4A4D4A84758484
          0084840084848284FFFFFF848284848284FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF4A4D4A00EBFF00EBFF4A4D4A840084840084848284FFFFFF8482848482
          84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4AD6FBFF00EBFF00EBFF4A
          4D4A840084848284FFFFFF848284848284FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF4A4D4AD6FBFFBDEFFF00EBFF4A4D4A840084848284FFFFFF8482848482
          84FFFFFF848284FFFFFFFFFFFF848284FFFFFFFFFFFF4A4D4A4A4D4A00EBFF4A
          4D4A840084848284FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF4A4D4A00EBFF4A4D4A840084848284FFFFFF848284FFFF
          FFFFFFFF848284FFFFFFFFFFFF848284FFFFFFFFFFFF8482844A4D4A00EBFF4A
          4D4A840084848284FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF4A4D4A00EBFF4A4D4A840084848284FFFFFF848284FFFF
          FFFFFFFF848284FFFFFFFFFFFF848284FFFFFF4A4D4A4A4D4A4A4D4A00EBFF4A
          4D4A840084848284FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF4A4D4AD6FBFFD6FBFF00EBFF4A4D4A840084847584FFFFFFFFFFFFFFFF
          FF848284848284FFFFFFFFFFFF8482848482844A4D4AD6FBFFB5FBFF00EBFF4A
          4D4A840084847584FFFFFFFFFFFF848284840084848284FFFFFF848284840084
          8482844A4D4A00EBFF00EBFF4A4D4A840084840084847584FFFFFF8475848400
          848400848482848482848400848400848482848482844A4D4A4A4D4A84008484
          0084840084847584847584840084840084840084848284840084840084840084
          8482848400848400848400848400848400848400848400848400848400848400
          8484008484008484008484008484008484008484008484008484008484008484
          0084}
        ShowHint = True
        Stretch = True
        Transparent = True
      end
      object Image4: TImage
        Left = 15
        Top = 145
        Width = 16
        Height = 16
        Hint = 'Tel'#233'fono particular'
        AutoSize = True
        ParentShowHint = False
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          0000100000000100180000000000000300000000000000000000000000000000
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF8482848482
          848482848482848482848482848482844A4D4A4A4D4A4A4D4AFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF848284FFFFFFFFFFFFFFFFFF848284848284FFFFFF
          4A4D4A00EBFF00EBFF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FF848284FFFF
          FFFFFFFFFFFFFF848284848284FFFFFF4A4D4AFFFFFFC6FBFF00EBFF4A4D4AFF
          00FFFF00FFFF00FFFF00FF848284FFFFFFFFFFFFFFFFFF848284848284FFFFFF
          4A4D4AFFFFFF00EBFF00EBFF4A4D4AFF00FFFF00FFFF00FFFF00FF848284FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4A4A4D4A00EBFF4A4D4AFF
          00FFFF00FFFF00FFFF00FF848284FFFFFF848284848284FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF4A4D4A00EBFF4A4D4AFF00FFFF00FFFF00FFFF00FF848284FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4A00EBFF4A4D4AFF
          00FFFF00FF848284848284848284848284848284848284848284848284848284
          8482848482844A4D4A00EBFF4A4D4AFF00FFFF00FFFF00FF848284FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4A4A4D4A4A4D4A00EBFF4A4D4AFF
          00FFFF00FFFF00FFFF00FF848284FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          4A4D4A00EBFF00EBFF00EBFF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FF8482
          84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4AFFFFFFC6FBFF00EBFF4A4D4AFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FF848284FFFFFFFFFFFFFFFFFFFFFFFF
          4A4D4A21E7FF21E7FF4A4D4AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FF848284FFFFFFFFFFFF848284FF00FF4A4D4A4A4D4AFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF848284848284FF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF}
        ShowHint = True
        Stretch = True
        Transparent = True
      end
      object lSaldoAnterior: TBevel
        Left = 530
        Top = 93
        Width = 263
        Height = 8
        Shape = bsBottomLine
      end
      object lblTitSaldoConvenio: TLabel
        Left = 533
        Top = 167
        Width = 111
        Height = 16
        Caption = 'Deuda Exigible:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblSaldoConvenio: TLabel
        Left = 653
        Top = 167
        Width = 137
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SaldoConvenio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 533
        Top = 82
        Width = 82
        Height = 13
        Caption = 'Otros Conceptos:'
      end
      object Label19: TLabel
        Left = 532
        Top = 114
        Width = 111
        Height = 13
        Caption = 'Ajuste Sencillo Anterior:'
        Visible = False
      end
      object lblIntereses: TLabel
        Left = 699
        Top = 23
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Intereses'
      end
      object lblOtrosConceptos: TLabel
        Left = 699
        Top = 82
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'OtrosConceptos'
      end
      object lblAjusteSencilloAnterior: TLabel
        Left = 699
        Top = 114
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AjusteSencilloAnterior'
        Visible = False
      end
      object Label1: TLabel
        Left = 533
        Top = 52
        Width = 94
        Height = 13
        Caption = 'Peaje Per'#237'odos ant:'
      end
      object lblPeajePeriodoAnt: TLabel
        Left = 699
        Top = 52
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PeajePer'#237'odoAnt'
      end
      object lblTitDeudaComprobantesCuotas: TLabel
        Left = 533
        Top = 182
        Width = 142
        Height = 13
        Caption = 'Deuda Comprobantes Cuotas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblDeudaComprobantesCuotas: TLabel
        Left = 685
        Top = 182
        Width = 105
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Deuda Cuotas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 807
        Top = 143
        Width = 91
        Height = 13
        Caption = 'Grupo Facturaci'#243'n:'
      end
      object Label16: TLabel
        Left = 907
        Top = 113
        Width = 9
        Height = 13
        Caption = ' - '
      end
      object lblGrupo: TLabel
        Left = 901
        Top = 143
        Width = 40
        Height = 13
        Caption = 'Ninguno'
      end
      object lblRNUT: TLabel
        Left = 807
        Top = 182
        Width = 41
        Height = 13
        Caption = 'lblRNUT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblDeudaRefLabel: TLabel
        Left = 533
        Top = 198
        Width = 109
        Height = 13
        Caption = 'Deuda Refinanciaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblDeudaRef: TLabel
        Left = 681
        Top = 198
        Width = 109
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Deuda Refinanciacion'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblUltimoAjustesencilloLabel: TLabel
        Left = 533
        Top = 128
        Width = 128
        Height = 13
        Caption = 'Ultimo Ajuste Sencillo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblUltimoAjusteSencillo: TLabel
        Left = 699
        Top = 128
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Ultimo Ajuste Sencillo'
      end
      object lblReimprimirNotifTAGsVencidos: TLabel
        Left = 321
        Top = 200
        Width = 184
        Height = 13
        Cursor = crHandPoint
        Caption = 'Reimprimir Notificaci'#243'n TAGs Vencidos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblReimprimirNotifTAGsVencidosClick
      end
      object lblEnListaAmarilla: TLabel
        Left = 304
        Top = 145
        Width = 200
        Height = 14
        Alignment = taCenter
        AutoSize = False
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = False
        Visible = False
      end
      object shpNovedad: TShape
        Left = 1044
        Top = 21
        Width = 219
        Height = 49
      end
      object lblNovedad: TLabel
        Left = 1049
        Top = 24
        Width = 209
        Height = 41
        AutoSize = False
        Caption = 'Sin novedad'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object lblConvenioCastigado: TLabel
        Left = 235
        Top = 9
        Width = 114
        Height = 13
        Caption = 'Convenio Castigado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object peNumeroDocumento: TPickEdit
        Left = 43
        Top = 28
        Width = 134
        Height = 21
        CharCase = ecUpperCase
        Enabled = True
        TabOrder = 0
        OnChange = peNumeroDocumentoChange
        OnKeyUp = peNumeroDocumentoKeyUp
        EditorStyle = bteTextEdit
        OnButtonClick = peNumeroDocumentoButtonClick
      end
      object cbConvenio: TVariantComboBox
        Left = 235
        Top = 28
        Width = 267
        Height = 21
        Style = vcsOwnerDrawFixed
        Color = clWhite
        ItemHeight = 15
        TabOrder = 1
        OnChange = cbConvenioChange
        OnDrawItem = cbConvenioDrawItem
        Items = <>
      end
      object Panel6: TPanel
        Left = 12
        Top = 4
        Width = 195
        Height = 16
        Align = alCustom
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 'Datos Personales'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object pnlEstadoCuenta: TPanel
        Left = 530
        Top = 145
        Width = 258
        Height = 16
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 'Estado de cuenta al %s, %s'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 3
      end
      object Panel12: TPanel
        Left = 804
        Top = 4
        Width = 106
        Height = 16
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 'Indicadores'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 4
      end
      object pnlTitUltimaNK: TPanel
        Left = 528
        Top = 2
        Width = 258
        Height = 16
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = #218'ltima Nota de Cobro Impaga'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 5
      end
      object pnlListaAcceso: TPanel
        Left = 352
        Top = 87
        Width = 154
        Height = 39
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 6
        object lblMotivoInhabilitacion: TLabel
          Left = 10
          Top = 44
          Width = 95
          Height = 14
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object chk_SuscritoListaBlanca: TCheckBox
          Left = 8
          Top = 5
          Width = 141
          Height = 17
          Caption = 'Suscrito Lista Blanca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
      object btnSuscribirListaBlanca: TButton
        Left = 376
        Top = 114
        Width = 126
        Height = 18
        Caption = 'Suscribir Lista Blanca'
        Enabled = False
        TabOrder = 7
        OnClick = btnSuscribirListaBlancaClick
      end
      object pnlNovedad: TPanel
        Left = 1044
        Top = 2
        Width = 106
        Height = 16
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 'Novedad'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 8
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 412
    Width = 1282
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 2
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 1282
      Height = 9
      Align = alTop
      Shape = bsSpacer
      Style = bsRaised
      ExplicitWidth = 937
    end
    object PageControl: TPageControl
      Left = 0
      Top = 9
      Width = 1282
      Height = 321
      ActivePage = tsTransacciones
      Align = alClient
      OwnerDraw = True
      TabOrder = 0
      OnChange = PageControlChange
      OnDrawTab = PageControlDrawTab
      object tsTransacciones: TTabSheet
        Caption = 'Transacciones'
        ImageIndex = 3
        object PFiltroTransitos: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object LTransitosDesde: TLabel
            Left = 4
            Top = 6
            Width = 34
            Height = 13
            Caption = 'Desde:'
          end
          object LTransitosHasta: TLabel
            Left = 204
            Top = 6
            Width = 31
            Height = 13
            Caption = 'Hasta:'
          end
          object lblTransitosReestablecer: TLabel
            Left = 393
            Top = 6
            Width = 93
            Height = 13
            Cursor = crHandPoint
            Hint = 'Haga Click ac'#225' para reestablecer las fechas por defecto'
            Caption = 'Fechas por defecto'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsUnderline]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = lblTransitosReestablecerClick
          end
          object lblCSV: TLabel
            Left = 802
            Top = 6
            Width = 62
            Height = 13
            Cursor = crHandPoint
            Hint = 'Exportar esta consulta a formato CSV'
            Caption = 'Formato CSV'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = lblCSVClick
          end
          object Label22: TLabel
            Left = 880
            Top = 6
            Width = 70
            Height = 13
            Caption = 'Concesionaria:'
          end
          object deTransitosDesde: TDateEdit
            Left = 41
            Top = 2
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 0
            Date = -693594.000000000000000000
          end
          object deTransitosHasta: TDateEdit
            Left = 239
            Top = 2
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 1
            Date = -693594.000000000000000000
          end
          object teTransitosHoraDesde: TTimeEdit
            Left = 133
            Top = 2
            Width = 60
            Height = 21
            AutoSelect = False
            TabOrder = 2
            AllowEmpty = False
            ShowSeconds = False
          end
          object teTransitosHoraHasta: TTimeEdit
            Left = 329
            Top = 2
            Width = 60
            Height = 21
            AutoSelect = False
            TabOrder = 3
            AllowEmpty = False
            ShowSeconds = False
          end
          object cbTransitoSoloVehiculo: TCheckBox
            Left = 637
            Top = 5
            Width = 158
            Height = 16
            Caption = 'S'#243'lo Veh'#237'culo Seleccionado'
            TabOrder = 6
          end
          object btnTransitosFiltrar: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 4
            OnClick = btnTransitosFiltrarClick
          end
          object vcbConcesionariasTransa: TVariantComboBox
            Left = 956
            Top = 2
            Width = 222
            Height = 21
            Style = vcsDropDownList
            ItemHeight = 13
            TabOrder = 7
            OnChange = vcbConcesionariasTransaChange
            Items = <>
          end
          object chkTransitoSoloGratuidad: TCheckBox
            Left = 498
            Top = 5
            Width = 131
            Height = 16
            Caption = 'S'#243'lo Gratuidad Lateral'
            TabOrder = 5
          end
        end
        object dblTransitos: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 25
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = True
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Fecha / Hora'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaHora'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Concesionaria'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'NombreCorto'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'P. Cobro'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescriPuntosCobro'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 50
              Header.Caption = 'Carril'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'NumeroCarril'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 120
              Header.Caption = 'Velocidad (Km./Hr.)'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'Velocidad'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 70
              Header.Caption = 'Pat. Conv.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Patente'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 70
              Header.Caption = 'Pat. Port.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'PatentePortico'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Categor'#237'a'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Categoria'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Telev'#237'a'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Etiqueta'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              Header.Caption = 'Kms.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Kms'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              Header.Caption = 'Tarifa'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'TipoHorario'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 80
              Header.Caption = 'Monto'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'DescriImporte'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              Header.Caption = 'Nota de Cobro'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescTipoComprobanteFiscal'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsUnderline]
              Width = 60
              Header.Caption = 'Imagen'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = True
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'LV'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblTransitosColumns15HeaderClick
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'LA'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblTransitosColumns16HeaderClick
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'LG'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblTransitosColumns17HeaderClick
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'LN'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblTransitosColumns18HeaderClick
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'EH'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblTransitosColumns19HeaderClick
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'CO'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblTransitosColumns20HeaderClick
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'TR'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblTransitosColumns21HeaderClick
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 28
              Header.Caption = 'NH'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblTransitosColumns22HeaderClick
            end>
          DataSource = dsObtenerTransitosConvenio
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnContextPopup = dblTransitosContextPopup
          OnCheckLink = dblTransitosCheckLink
          OnDrawText = dblTransitosDrawText
          OnLinkClick = dblTransitosLinkClick
        end
      end
      object tsComprobantes: TTabSheet
        Caption = 'Comprobantes'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dblComprobantes: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsUnderline]
              Width = 80
              Header.Caption = 'Notif. Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = True
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsUnderline]
              Width = 80
              Header.Caption = 'Notif. Detalle'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = True
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Fecha Emisi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaEmision'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'TipoComprobanteFiscalDescr'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Nro. Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'NumeroComprobanteFiscal'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaVencimiento'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 110
              Header.Caption = 'Total Compr.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'TotalComprobanteDescri'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 105
              Header.Caption = 'Saldo Pendiente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'SaldoPendienteDescr'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Inicio Per'#237'odo '
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'PeriodoInicial'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Fin Per'#237'odo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'PeriodoFinal'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescriEstado'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsUnderline]
              Width = 50
              Header.Caption = 'Detalles'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = True
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 120
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'TipoComprobanteDescr'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'UsuarioCreacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Descripcion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Devuelto Correo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriDevueltoCorreo'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Impresora Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'CodigoImpresoraFiscal'
            end>
          DataSource = dsObtenerComprobantesConvenio
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnContextPopup = dblComprobantesContextPopup
          OnDrawText = dblComprobantesDrawText
          OnLinkClick = dblComprobantesLinkClick
          OnMouseMove = dblComprobantesMouseMove
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object BtnFiltrarComprobantes: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = BtnFiltrarComprobantesClick
          end
        end
      end
      object tsPagos: TTabSheet
        Caption = 'Pagos'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dblPagos: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaHora'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              Header.Caption = 'Recibo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroRecibo'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 100
              Header.Caption = 'Monto'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'ImporteDescri'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 105
              Header.Caption = 'Saldo Pendiente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'SaldoDescri'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 110
              Header.Caption = 'Canal de Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'CanalPago'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 110
              Header.Caption = 'Forma de Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FormaPago'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoComprobanteFiscalDescr'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              Header.Caption = 'Punto de Venta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'PuntoVenta'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 180
              Header.Caption = 'POS'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroPos'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Estado'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Usuario'
            end>
          DataSource = dsObtenerPagosConvenio
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnContextPopup = dblPagosContextPopup
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object BtnFiltrarPagos: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = BtnFiltrarPagosClick
          end
        end
      end
      object tsPagosAutomaticos: TTabSheet
        Caption = 'Pagos Autom'#225'ticos'
        ImageIndex = 19
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object pnlFiltros: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 41
          Align = alTop
          TabOrder = 0
          object lblMostrar: TLabel
            Left = 16
            Top = 13
            Width = 38
            Height = 13
            Caption = 'Mostrar:'
          end
          object cbbMostrarPagosAutomaticos: TVariantComboBox
            Left = 60
            Top = 9
            Width = 143
            Height = 21
            Style = vcsOwnerDrawFixed
            Color = clWhite
            ItemHeight = 15
            ItemIndex = 0
            TabOrder = 0
            OnChange = cbbMostrarPagosAutomaticosChange
            Items = <
              item
                Caption = 'Todos'
                Value = '-1'
              end
              item
                Caption = 'Aprobados'
                Value = '1'
              end
              item
                Caption = 'Rechazados'
                Value = '0'
              end>
            Value = '-1'
            Text = 'Todos'
          end
        end
        object dblPagosAutomaticos: TDBListEx
          Left = 0
          Top = 41
          Width = 1274
          Height = 252
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'TipoComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Numero Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Importe'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Estado Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'EstadoPago'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha Operaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaHOra'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Forma de Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FormaPago'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 400
              Header.Caption = 'Observaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Observacion'
            end>
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
        end
      end
      object tsResumenCC: TTabSheet
        Caption = 'Resumen CC'
        ImageIndex = 10
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dbResumenCC: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 65
              Header.Caption = 'Orden'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Orden'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Fecha Emisi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaEmision'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'TipoComprobanteDescr'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 100
              Header.Caption = 'Cargos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'TotalComprobanteDebeDescri'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 100
              Header.Caption = 'Abonos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'TotalComprobanteHaberDescri'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 100
              Header.Caption = 'Saldo Convenio'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'SaldoAFechaDescri'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Tipo Comp. Aplicado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'TipoComprobanteAplicadoDescri'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Nro. Comp. Aplicado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroComprobanteAplicado'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 115
              Header.Caption = 'Saldo Documento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'SaldoPendienteDescri'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescriEstado'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaVencimiento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Tipo Comp. Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoComprobanteFiscalDescr'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Nro. Comp. Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'NumeroComprobanteFiscal'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 250
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
            end>
          DataSource = dsResumenCuentaCorriente
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnContextPopup = dblComprobantesContextPopup
          OnDrawText = dbResumenCCDrawText
          OnLinkClick = dblComprobantesLinkClick
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          OnResize = Panel7Resize
          DesignSize = (
            1274
            26)
          object BtnCtaCte: TButton
            Left = 1205
            Top = -1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = BtnCtaCteClick
          end
          object btnExcel: TButton
            Left = 1119
            Top = -1
            Width = 81
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Exportar Excel'
            TabOrder = 1
            OnClick = btnExcelClick
          end
        end
      end
      object tsReclamos: TTabSheet
        Caption = 'Reclamos Recibidos'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dblReclamos: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 25
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 50
              Header.Caption = 'Orden'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'CodigoOrdenServicio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 240
              Header.Caption = 'Reclamo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescripcionTipoOrdenServicio'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 110
              Header.Caption = 'F. Creaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaHoraCreacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 105
              Header.Caption = 'F. Comprometida'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaCompromiso'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 110
              Header.Caption = 'F. Cierre'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaHoraFin'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 60
              Header.Caption = 'Prioridad'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Prioridad'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 70
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Estado'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Convenio'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'NumeroConvenioFormateado'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Usuario Creador'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'UsuarioCreacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Ultima Modificaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'UltimaFechaModificacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Usuario Modificador'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'UsuarioModificador'
            end>
          DataSource = dsReclamos
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnContextPopup = dblReclamosContextPopup
          OnDrawText = dblReclamosDrawText
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object BtnReclamos: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = BtnReclamosClick
          end
        end
      end
      object tsUltimosConsumos: TTabSheet
        Caption = 'Consumos'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dblUltConsumos: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 300
              Header.Caption = 'Tipo Consumo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescripcionTipoRegistro'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 200
              Header.Caption = 'Descripci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Descripcion'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Patente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Patente'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Fecha'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Importe Base'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Base'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Afecto Iva'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'AfectoIva'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Importe IVA'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'ImporteIVA'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Porcentaje IVA'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'PorcentajeIVA'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Total'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Total'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'ComprobanteFiscalTipo'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Nro. Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobanteFiscal'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha Emisi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaEmision'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Periodo Inicial'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'PeriodoInicial'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Periodo Final'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'PeriodoFinal'
            end>
          DataSource = dsUltConsumos
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object PFiltroUC: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object lblUCFechaDesde: TLabel
            Left = 4
            Top = 7
            Width = 67
            Height = 13
            Caption = 'Fecha Desde:'
          end
          object lblUCFechaHasta: TLabel
            Left = 173
            Top = 7
            Width = 64
            Height = 13
            Caption = 'Fecha Hasta:'
          end
          object dedtUCFechaDesde: TDateEdit
            Left = 77
            Top = 2
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 0
            Date = -693594.000000000000000000
          end
          object btnUCFiltrar: TButton
            Left = 1206
            Top = -1
            Width = 66
            Height = 21
            Anchors = [akRight, akBottom]
            Caption = 'Buscar'
            Default = True
            TabOrder = 1
            OnClick = btnUCFiltrarClick
          end
          object cbUCSoloVehiculo: TCheckBox
            Left = 368
            Top = 4
            Width = 158
            Height = 16
            Caption = 'S'#243'lo Veh'#237'culo Seleccionado'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object chkMuestraConsumosNoFacturados: TCheckBox
            Left = 543
            Top = 3
            Width = 224
            Height = 17
            Caption = 'Mostrar S'#243'lo Consumos No Facturados'
            TabOrder = 3
            OnClick = chkMuestraConsumosNoFacturadosClick
          end
          object dedtUCFechaHasta: TDateEdit
            Left = 243
            Top = 2
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
        end
      end
      object tsHistoricoMediosPagos: TTabSheet
        Caption = 'Historico Medios de Pagos'
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dblHistoricoMedioPago: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Vigente Hasta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaActualizacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 70
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'TipoMedio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 350
              Header.Caption = 'Descripci'#243'n Medio de Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescripcionMedio'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'RUT Mandante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroDocumentoMedioPago'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 240
              Header.Caption = 'Mandante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Mandante'
            end>
          DataSource = daObtenerHistoricoMedioPago
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object BtnHistoricoMP: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = BtnHistoricoMPClick
          end
        end
      end
      object tsHistoricoDomicilios: TTabSheet
        Caption = 'Historico Domicilios'
        ImageIndex = 7
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dblHistoricoDomicilios: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Vigente Desde'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaActualizacionAnt'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 110
              Header.Caption = 'Vigente Hasta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaActualizacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Tipo'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Regi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Region'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Comuna'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Comuna'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Calle'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescripcionCalle'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Numero'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Detalle'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Detalle'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 90
              Header.Caption = 'Codigo Postal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'CodigoPostal'
            end>
          DataSource = daObtenerHistoricoDomicilios
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object BtnHistoricoDom: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = BtnHistoricoDomClick
          end
        end
      end
      object tsMails: TTabSheet
        Caption = 'Notificaciones - e-mails'
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cpOpciones: TCollapsablePanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 145
          Align = alTop
          Animated = False
          Caption = 'M'#225's Opciones'
          BarColorStart = 14071199
          BarColorEnd = 10646097
          RightMargin = 0
          LeftMargin = 0
          ArrowLocation = cpaRightTop
          Style = cpsWinXP
          InternalSize = 122
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          DesignSize = (
            1274
            145)
          object Label5: TLabel
            Left = 3
            Top = 34
            Width = 93
            Height = 13
            Caption = 'Mostrar los primeros'
          end
          object Label6: TLabel
            Left = 3
            Top = 75
            Width = 83
            Height = 13
            Caption = 'Creados entre el :'
          end
          object Label12: TLabel
            Left = 206
            Top = 76
            Width = 16
            Height = 13
            Caption = 'y el'
          end
          object Label13: TLabel
            Left = 163
            Top = 34
            Width = 86
            Height = 13
            Caption = 'e-mails registrados'
          end
          object rgOtros: TRadioGroup
            Left = 481
            Top = 24
            Width = 168
            Height = 69
            Caption = 'Filtros Adicionales'
            ItemIndex = 0
            Items.Strings = (
              'Ninguno'
              'S'#243'lo los que se enviaron'
              'S'#243'lo los que no se enviaron')
            TabOrder = 0
          end
          object neNotificacionesTop: TNumericEdit
            Left = 107
            Top = 32
            Width = 49
            Height = 21
            MaxLength = 4
            TabOrder = 2
          end
          object deNotificacionesDesde: TDateEdit
            Left = 107
            Top = 72
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 3
            Date = -693594.000000000000000000
          end
          object deNotificacionesHasta: TDateEdit
            Left = 235
            Top = 72
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
          object btnBuscar: TButton
            Left = 671
            Top = 68
            Width = 75
            Height = 25
            Anchors = [akLeft, akBottom]
            Caption = '&Buscar'
            Default = True
            TabOrder = 5
            OnClick = btnBuscarClick
          end
          object RgTipos: TRadioGroup
            Left = 335
            Top = 24
            Width = 140
            Height = 86
            Caption = 'Tipos de Email'
            ItemIndex = 0
            Items.Strings = (
              'Todos'
              'Notas de Cobro'
              'Facturaci'#243'n'
              'Env'#237'o de Clave')
            TabOrder = 6
          end
          object btnMostrarEmail: TButton
            Left = 769
            Top = 68
            Width = 75
            Height = 25
            Caption = 'Mostrar e-mail'
            TabOrder = 7
            Visible = False
            OnClick = btnMostrarEmailClick
          end
        end
        object DBListEx2: TDBListEx
          Left = 0
          Top = 145
          Width = 1274
          Height = 148
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha Creaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaCreacion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'e-Mail Destino'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'email'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Asunto'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Asunto'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'N'#250'mero Convenio'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroConvenio'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Enviado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Enviado'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CodigoUsuario'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Reintentos'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Reintentos'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fecha de Env'#237'o'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FechaEnvio'
            end>
          DataSource = dsEMails
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
        end
      end
      object tsAccionesExternas: TTabSheet
        Caption = 'Acciones Externas'
        ImageIndex = 9
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dblAccionesExtrenas: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Sorting = csDescending
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaAccion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Resp. Externo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'ResponsableExterno'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 140
              Header.Caption = 'Tipo Proc. de Cobranza'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescTipoCobranza'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 220
              Header.Caption = 'Acci'#243'n de Cobranza'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'DescAccion'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Fecha de Registro'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaRegistro'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              Header.Caption = 'Nro. Proc.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroProceso'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Usuario Resp.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'UsuarioResponsable'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Observaciones'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'Observaciones'
            end>
          DataSource = dsObtenerAccionesCobranzaSerbanc
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            1274
            26)
          object BtnAccionesExternas: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = BtnAccionesExternasClick
          end
        end
      end
      object tsIndemnizaciones: TTabSheet
        Caption = 'Indemnizaciones'
        ImageIndex = 12
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object pnIndemnizaciones: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            1274
            26)
          object btnIndemnizaciones: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = btnIndemnizacionesClick
          end
        end
        object dblIndemnizaciones: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Etiqueta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = dblIndemnizacionesColumns0HeaderClick
              FieldName = 'Etiqueta'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaHora'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 300
              Header.Caption = 'Motivo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'DescripcionMotivo'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 130
              Header.Caption = 'Tipo Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'TipoComprobante'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 140
              Header.Caption = 'Numero Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'NumeroComprobante'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Estado Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'EstadoPago'
            end>
          DataSource = dsIndemnizaciones
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
        end
      end
      object tsMensajesComprobantes: TTabSheet
        Caption = 'Mensajes Comprobantes'
        ImageIndex = 13
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object pnlMensajesComprobantes: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          DesignSize = (
            1274
            26)
          object btnMensajesComprobantes: TButton
            Left = 1206
            Top = 1
            Width = 66
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Buscar'
            TabOrder = 0
            OnClick = btnMensajesComprobantesClick
          end
        end
        object DBLMensajesComprobantes: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 0
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'TipoComprobanteFiscal'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Tipo Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Descripcion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Fecha Emisi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'FechaEmision'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Nro. Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = ordenarColumna
              FieldName = 'NumeroComprobanteFiscal'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 600
              Header.Caption = 'Mensaje'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Texto'
            end>
          DataSource = dsObtenerMensajesComprobanteParticular
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
        end
      end
      object tsDescuentos: TTabSheet
        Caption = 'Descuentos'
        ImageIndex = 19
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel18: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            1274
            26)
          object btnAdministrarDescuentos: TButton
            Left = 1131
            Top = 2
            Width = 140
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Administrar Descuentos'
            TabOrder = 0
            OnClick = btnAdministrarDescuentosClick
          end
        end
        object DBLDescuentos: TDBListEx
          Left = 0
          Top = 26
          Width = 1274
          Height = 267
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'C'#243'digo descuento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'CodigoDescuento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 300
              Header.Caption = 'Nombre descuento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'NombreDescuento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 200
              Header.Caption = 'Tipo descuento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'TipoDescuento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 450
              Header.Caption = 'Aplicado a'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'AplicaA'
            end>
          DataSource = dsObtenerDescuentosConvenio
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
        end
      end
      object tsEstacionamiento: TTabSheet
        Caption = 'Estacionamientos'
        ImageIndex = 14
        object pnlEstacionamiento: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 41
          Align = alTop
          TabOrder = 0
          Visible = False
          object lblEstacDesde: TLabel
            Left = 16
            Top = 14
            Width = 34
            Height = 13
            Caption = 'Desde:'
          end
          object lblEstacHasta: TLabel
            Left = 247
            Top = 14
            Width = 31
            Height = 13
            Caption = 'Hasta:'
          end
          object lblEstacConcesionaria: TLabel
            Left = 520
            Top = 14
            Width = 70
            Height = 13
            Caption = 'Concesionaria:'
          end
          object deEstacDesde: TDateEdit
            Left = 56
            Top = 10
            Width = 85
            Height = 21
            AutoSelect = False
            TabOrder = 0
            Date = -693594.000000000000000000
          end
          object deEstacHasta: TDateEdit
            Left = 284
            Top = 10
            Width = 85
            Height = 21
            AutoSelect = False
            TabOrder = 1
            Date = -693594.000000000000000000
          end
          object teEstacDesde: TTimeEdit
            Left = 141
            Top = 10
            Width = 57
            Height = 21
            AutoSelect = False
            TabOrder = 2
            AllowEmpty = False
            ShowSeconds = False
          end
          object teEstacHasta: TTimeEdit
            Left = 368
            Top = 10
            Width = 57
            Height = 21
            AutoSelect = False
            TabOrder = 3
            AllowEmpty = False
            ShowSeconds = False
          end
          object vcbConcesionariasEstacionamiento: TVariantComboBox
            Left = 597
            Top = 10
            Width = 197
            Height = 21
            Style = vcsDropDownList
            ItemHeight = 0
            TabOrder = 4
            OnChange = vcbConcesionariasEstacionamientoChange
            Items = <>
          end
          object btnEstacBuscar: TButton
            Left = 800
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Buscar'
            TabOrder = 5
            OnClick = btnEstacBuscarClick
          end
        end
        object dblEstacionamientos: TDBListEx
          Left = 0
          Top = 41
          Width = 1274
          Height = 252
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 25
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = True
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Fecha Hora Entrada'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'FechaHoraEntrada'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Lugar Entrada'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'EntradaDescr'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Fecha Hora Salida'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'FechaHoraSalida'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Lugar Salida'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'SalidaDescr'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Concesionaria'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'ConcesionariaDescr'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Patente'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'Patente'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Telev'#237'a'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'Etiqueta'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 90
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'Importe'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 200
              Header.Caption = 'Nota de Cobro'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              OnHeaderClick = dblEstacionamientosColumnsHeaderClick
              FieldName = 'DescTipoComprobanteFiscal'
            end>
          DataSource = dsEstacionamientos
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
          Visible = False
          OnContextPopup = dblEstacionamientosContextPopup
          OnDrawText = dblEstacionamientosDrawText
          OnLinkClick = dblEstacionamientosLinkClick
        end
      end
      object tsListasDeAcceso: TTabSheet
        Caption = 'Listas de Acceso'
        ImageIndex = 17
        object pnlBusqueda: TPanel
          Left = 0
          Top = 35
          Width = 1274
          Height = 258
          Align = alClient
          Caption = 'pnlVista'
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 801
            Top = 1
            Height = 256
            Color = clGrayText
            ParentColor = False
            ExplicitLeft = 624
            ExplicitTop = 16
            ExplicitHeight = 100
          end
          object pnlAdhesion: TPanel
            Left = 1
            Top = 1
            Width = 800
            Height = 256
            Align = alLeft
            TabOrder = 0
            object lblTituloAdhesion: TLabel
              Left = 1
              Top = 1
              Width = 152
              Height = 13
              Align = alTop
              Caption = 'Adhesion a Parque Arauco'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object DBLEAdhesionPA: TDBListEx
              Left = 1
              Top = 14
              Width = 798
              Height = 241
              Align = alClient
              BorderStyle = bsSingle
              Columns = <
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 60
                  Header.Caption = 'Patente'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'Patente'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 80
                  Header.Caption = 'Etiqueta'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'Etiqueta'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 125
                  Header.Caption = 'Alta Parking'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'FechaHoraAltaConcesionaria'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 125
                  Header.Caption = 'Baja Parking'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'FechaHoraBajaConcesionaria'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 125
                  Header.Caption = 'Parking'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'Descripcion'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 125
                  Header.Caption = 'Alta CAC'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'FechaHoraAltaListaAcceso'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  Header.Caption = 'Usuario Alta'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  FieldName = 'UsuarioAlta'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 125
                  Header.Caption = 'Baja CAC'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'FechaHoraBajaListaAcceso'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  Header.Caption = 'Usuario Baja'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  FieldName = 'UsuarioBaja'
                end>
              DataSource = dsHistoricoListaDeAccesoConvenio
              DragReorder = True
              ParentColor = False
              TabOrder = 0
              TabStop = True
            end
          end
          object pnlInhabilitacion: TPanel
            Left = 804
            Top = 1
            Width = 469
            Height = 256
            Align = alClient
            TabOrder = 1
            object lblTituloInhabilitacion: TLabel
              Left = 1
              Top = 1
              Width = 90
              Height = 13
              Align = alTop
              Caption = 'Inhabilitaciones'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object DBLEInhabilitaciones: TDBListEx
              Left = 1
              Top = 14
              Width = 467
              Height = 241
              Align = alClient
              BorderStyle = bsSingle
              Columns = <
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 125
                  Header.Caption = 'Desde'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'FechaInicio'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 125
                  Header.Caption = 'Hasta'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'FechaFinalizacion'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 120
                  Header.Caption = 'Concesionaria'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'DescripcionConcesionaria'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 130
                  Header.Caption = 'Motivo'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  OnHeaderClick = DBLEAdhesionPAColumns0HeaderClick
                  FieldName = 'DescripcionMotivo'
                end>
              DataSource = dsHistoricoConveniosInhabilitados
              DragReorder = True
              ParentColor = False
              TabOrder = 0
              TabStop = True
            end
          end
        end
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 35
          Align = alTop
          TabOrder = 1
          DesignSize = (
            1274
            35)
          object Label20: TLabel
            Left = 856
            Top = 11
            Width = 31
            Height = 13
            Caption = 'Hasta:'
            Visible = False
          end
          object Label25: TLabel
            Left = 707
            Top = 11
            Width = 34
            Height = 13
            Caption = 'Desde:'
            Visible = False
          end
          object lblPatente: TLabel
            Left = 1032
            Top = 11
            Width = 43
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Patente :'
            ExplicitLeft = 1005
          end
          object deHistListaAccesoDesde: TDateEdit
            Left = 749
            Top = 8
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 0
            Visible = False
            Date = -693594.000000000000000000
          end
          object deHistListaAccesoHasta: TDateEdit
            Left = 894
            Top = 8
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 1
            Visible = False
            Date = -693594.000000000000000000
          end
          object btnBuscarHistoricosListasDeAcceso: TButton
            Left = 1195
            Top = 4
            Width = 66
            Height = 25
            Anchors = [akRight, akBottom]
            Caption = 'Buscar'
            Default = True
            TabOrder = 2
            OnClick = btnBuscarHistoricosListasDeAccesoClick
          end
          object cbbPatentes: TVariantComboBox
            Left = 1081
            Top = 8
            Width = 88
            Height = 21
            Style = vcsDropDownList
            Anchors = [akRight, akBottom]
            ItemHeight = 0
            TabOrder = 3
            OnChange = cbbPatentesChange
            Items = <>
          end
        end
      end
      object tsRefinanciaciones: TTabSheet
        Caption = 'Refinanciaciones'
        ImageIndex = 14
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 41
          Align = alTop
          TabOrder = 0
          DesignSize = (
            1274
            41)
          object btnRefinanciaciones: TButton
            Left = 1186
            Top = 8
            Width = 75
            Height = 25
            Anchors = [akRight, akBottom]
            Caption = 'Buscar'
            Default = True
            TabOrder = 0
            OnClick = btnRefinanciacionesClick
          end
        end
        object DBLstExRefinanResumenCC: TDBListEx
          Left = 0
          Top = 41
          Width = 1274
          Height = 252
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 65
              Header.Caption = 'Orden'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              Sorting = csDescending
              IsLink = False
              OnHeaderClick = DBListEx1Columns0HeaderClick
              FieldName = 'Orden'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 95
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = DBListEx1Columns0HeaderClick
              FieldName = 'Fecha'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 225
              Header.Caption = 'Tipo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = DBListEx1Columns0HeaderClick
              FieldName = 'DescripcionTipo'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'N'#250'mero'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'NumeroTipo'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Forma Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = DBListEx1Columns0HeaderClick
              FieldName = 'DescripcionFormaPago'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              OnHeaderClick = DBListEx1Columns0HeaderClick
              FieldName = 'DescripcionEstadoCuota'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 100
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'ImporteDesc'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Saldo Documento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'SaldoDesc'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Saldo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'SaldoCCDesc'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Refinanciaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              OnHeaderClick = DBListEx1Columns0HeaderClick
              FieldName = 'CodigoRefinanciacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 135
              Header.Caption = 'N'#250'mero Convenio'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'NumeroConvenio'
            end>
          DataSource = dsRefinanResumenCC
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
          OnDrawBackground = DBLstExRefinanResumenCCDrawBackground
          OnDrawText = DBLstExRefinanResumenCCDrawText
        end
      end
      object tsListaAmarilla: TTabSheet
        Caption = 'Lista Amarilla'
        ImageIndex = 17
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object pnl1: TPanel
          Left = 0
          Top = 0
          Width = 1274
          Height = 35
          Align = alTop
          TabOrder = 0
          DesignSize = (
            1274
            35)
          object lbl1: TLabel
            Left = 158
            Top = 14
            Width = 31
            Height = 13
            Caption = 'Hasta:'
          end
          object lbl2: TLabel
            Left = 16
            Top = 14
            Width = 34
            Height = 13
            Caption = 'Desde:'
          end
          object deHistLADesde: TDateEdit
            Left = 56
            Top = 9
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 0
            Date = -693594.000000000000000000
          end
          object deHistLAHasta: TDateEdit
            Left = 199
            Top = 8
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 1
            Date = -693594.000000000000000000
          end
          object btnHistoricoListaAmarilla: TButton
            Left = 402
            Top = 6
            Width = 66
            Height = 25
            Anchors = [akRight, akBottom]
            Caption = 'Buscar'
            Default = True
            TabOrder = 2
            OnClick = btnHistoricoListaAmarillaClick
          end
        end
        object pnl2: TPanel
          Left = 0
          Top = 35
          Width = 1274
          Height = 258
          Align = alClient
          Caption = 'pnlVista'
          TabOrder = 1
          object spl1: TSplitter
            Left = 801
            Top = 1
            Width = 1
            Height = 256
            Color = clGrayText
            ParentColor = False
            ExplicitLeft = 808
            ExplicitHeight = 287
          end
          object pnl3: TPanel
            Left = 1
            Top = 1
            Width = 800
            Height = 256
            Align = alLeft
            TabOrder = 0
            object lbl3: TLabel
              Left = 1
              Top = 1
              Width = 130
              Height = 13
              Align = alTop
              Caption = 'Hist'#243'rico Lista Amarilla'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object dblHistoricoListaAmarilla: TDBListEx
              Left = 1
              Top = 14
              Width = 798
              Height = 241
              Align = alClient
              BorderStyle = bsSingle
              Columns = <
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 120
                  Header.Caption = 'Fecha Alta'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaAlta'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 120
                  Header.Caption = 'Fecha Baja'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaBaja'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  Header.Caption = 'Estado'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  FieldName = 'Descripcion'
                end
                item
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 90
                  Header.Caption = 'Concesionaria'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'CodigoConcesionaria'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  Header.Caption = 'Usuario Creaci'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  FieldName = 'UsuarioCreacion'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 120
                  Header.Caption = 'Fecha Creaci'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaCreacion'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  Header.Caption = 'Usuario Modificaci'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  FieldName = 'UsuarioModificacion'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 120
                  Header.Caption = 'Fecha Modificaci'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaModificacion'
                end>
              DataSource = dsHistoricoListaAmarilla
              DragReorder = True
              ParentColor = False
              TabOrder = 0
              TabStop = True
            end
          end
          object pnl4: TPanel
            Left = 802
            Top = 1
            Width = 471
            Height = 256
            Align = alClient
            TabOrder = 1
            object lbl4: TLabel
              Left = 1
              Top = 1
              Width = 129
              Height = 13
              Align = alTop
              Caption = 'Hist'#243'rico Env'#237'o Cartas'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
            end
            object dblHistoricoCartas: TDBListEx
              Left = 1
              Top = 14
              Width = 469
              Height = 241
              Align = alClient
              BorderStyle = bsSingle
              Columns = <
                item
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 100
                  MinWidth = 100
                  MaxWidth = 270
                  Header.Caption = 'Fecha de Env'#237'o'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  OnHeaderClick = ordenarColumna
                  FieldName = 'FechaEnvio'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  MinWidth = 100
                  MaxWidth = 200
                  Header.Caption = 'Empresa de Correos'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'Nombre'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  MinWidth = 100
                  MaxWidth = 270
                  Header.Caption = 'Motivo del Rechazo'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'Detalle'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 200
                  MinWidth = 100
                  MaxWidth = 500
                  Header.Caption = 'Observaci'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'Observacion'
                end
                item
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 120
                  MinWidth = 100
                  MaxWidth = 270
                  Header.Caption = 'Fecha Creaci'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaCreacion'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  MinWidth = 100
                  MaxWidth = 250
                  Header.Caption = 'Usuario de Creaci'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'UsuarioCreacion'
                end
                item
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  MinWidth = 100
                  MaxWidth = 270
                  Header.Caption = 'Fecha Rechazo'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaModificacion'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 150
                  MinWidth = 100
                  MaxWidth = 200
                  Header.Caption = 'Usuario Rechazo'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -12
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = []
                  IsLink = False
                  FieldName = 'UsuarioModificacion'
                end>
              DataSource = dsHistoricoCartas
              DragReorder = True
              ParentColor = False
              TabOrder = 0
              TabStop = True
              OnDrawText = dblHistoricoCartasDrawText
            end
          end
        end
      end
    end
  end
  object dsObtenerCuentasCartola: TDataSource
    DataSet = spObtenerCuentasCartola
    Left = 480
    Top = 336
  end
  object ObtenerResumenDeudaConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerResumenDeudaConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 272
    Top = 600
  end
  object dsReclamos: TDataSource
    DataSet = spReclamos
    Left = 93
    Top = 352
  end
  object spReclamos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOrdenesServicioConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 85
    Top = 480
  end
  object popReclamosGenerales: TPopupMenu
    Left = 30
    Top = 592
  end
  object dsObtenerPagosConvenio: TDataSource
    DataSet = spObtenerPagosConvenio
    Left = 213
    Top = 320
  end
  object spObtenerPagosConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerPagosConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1205
    Top = 96
  end
  object Checklist: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoChecklist'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        Attributes = [faRequired]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Vencimiento'
        DataType = ftDate
      end
      item
        Name = 'Observaciones'
        DataType = ftMemo
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 33
    Top = 648
  end
  object dsChecklist: TDataSource
    DataSet = Checklist
    Left = 51
    Top = 376
  end
  object popChecklist: TPopupMenu
    OnPopup = popChecklistPopup
    Left = 145
    Top = 568
    object mnuAgregarChecklist: TMenuItem
      Caption = 'Agregar...'
      OnClick = mnuAgregarChecklistClick
    end
    object mnuEditarCheckList: TMenuItem
      Caption = 'Editar...'
      OnClick = mnuEditarCheckListClick
    end
    object mnuQuitarChecklist: TMenuItem
      Caption = 'Quitar...'
      OnClick = mnuQuitarChecklistClick
    end
  end
  object spObtenerDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = 'NULL'
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = 'NULL'
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 24
    Top = 496
  end
  object spObtenerCuentasCartola: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCuentasCartola;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '@TraerBajas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 400
    Top = 568
  end
  object spObtenerTransitosConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTransitosConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CantidadDeRegistros'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@SoloGratuidad'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 40
    Top = 696
  end
  object dsObtenerTransitosConvenio: TDataSource
    DataSet = spObtenerTransitosConvenio
    Left = 432
    Top = 352
  end
  object dsObtenerComprobantesConvenio: TDataSource
    DataSet = spObtenerComprobantesConvenio
    Left = 136
    Top = 368
  end
  object spObtenerComprobantesConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerComprobantesConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 176
    Top = 328
  end
  object spObtenerCheckListConvenioCartola: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCheckListConvenioCartola'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 6
      end>
    Left = 222
    Top = 544
  end
  object spActualizarConvenioCheckList: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarConvenioCheckList'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCheckList'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Vencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 30
    Top = 544
  end
  object Imagenes: TImageList
    Left = 323
    Top = 504
    Bitmap = {
      494C01010B001000880310001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084738400848484008484
      8400848484008484840084848400848484008484840084848400848484004A4A
      4A004A4A4A004A4A4A0084738400000000000000000000000000000000008484
      84008484840084848400848484008484840084848400848484004A4A4A004A4A
      4A004A4A4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF008484
      840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A
      4A0000EFFF0000EFFF004A4A4A00000000000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF008484840084848400FFFFFF004A4A4A0000EF
      FF0000EFFF004A4A4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF008484
      840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A
      4A00D6FFFF0000EFFF0000EFFF004A4A4A000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF008484840084848400FFFFFF004A4A4A00FFFF
      FF00C6FFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF008484
      840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A
      4A00D6FFFF00BDEFFF0000EFFF004A4A4A000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF008484840084848400FFFFFF004A4A4A00FFFF
      FF0000EFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      00000000000000000000666666FF553F2AFF666666FFCCCCCCFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF008484
      840084848400FFFFFF0084848400FFFFFF00FFFFFF0084848400FFFFFF00FFFF
      FF004A4A4A004A4A4A0000EFFF004A4A4A000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A
      4A004A4A4A0000EFFF004A4A4A00000000000000000000000000000000000000
      000000000000553F2AFF666666FF666666FF666666FF333366FFCCCCCCFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF004A4A4A0000EFFF004A4A4A000000000000000000000000008484
      8400FFFFFF008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF004A4A4A0000EFFF004A4A4A00000000000000000000000000000000000000
      0000333366FF666666FF666666FF0000CCFF666666FF666666FF666633FFCCCC
      CCFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF008484
      8400FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF0084848400FFFFFF00FFFF
      FF00848484004A4A4A0000EFFF004A4A4A000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF004A4A4A0000EFFF004A4A4A00000000000000000000000000000000006666
      66FF666666FF666666FF0000FFFF0099FFFF0000FFFF666666FF666666FF6666
      66FFCCCCCCFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF004A4A4A0000EFFF004A4A4A000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      84004A4A4A0000EFFF004A4A4A0000000000000000000000000000000000553F
      2AFF666666FF0000CCFF0099FFFF0000FFFF0099FFFF0000CCFF666666FF3333
      66FFCCCCCCFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF008484
      8400FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF0084848400FFFFFF004A4A
      4A004A4A4A004A4A4A0000EFFF004A4A4A00000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A4A004A4A
      4A004A4A4A0000EFFF004A4A4A00000000000000000000000000000000006666
      66FF666666FF666666FF0000FFFF0099FFFF0000FFFF666666FF666666FF6666
      66FFCCCCCCFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A
      4A00D6FFFF00D6FFFF0000EFFF004A4A4A000000000000000000000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A4A0000EF
      FF0000EFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      0000336666FF666666FF666666FF0000CCFF666666FF666666FF336633FFCCCC
      CCFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084738400FFFFFF00FFFF
      FF00FFFFFF008484840084848400FFFFFF00FFFFFF0084848400848484004A4A
      4A00D6FFFF00B5FFFF0000EFFF004A4A4A000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A4A00FFFF
      FF00C6FFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      000000000000553F2AFF666666FF666666FF666666FF553F2AFFCCCCCCFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084738400FFFFFF00FFFF
      FF00848484000000000084848400FFFFFF008484840000000000848484004A4A
      4A0000EFFF0000EFFF004A4A4A00000000000000000000000000000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF004A4A4A0021E7
      FF0021E7FF004A4A4A0000000000000000000000000000000000000000000000
      00000000000000000000666666FF666666FF666666FFCCCCCCFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084738400FFFFFF008473
      8400000000000000000084848400848484000000000000000000848484008484
      84004A4A4A004A4A4A0000000000000000000000000000000000000000000000
      0000000000000000000084848400FFFFFF00FFFFFF0084848400000000004A4A
      4A004A4A4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084738400847384000000
      0000000000000000000084848400000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004A4A
      4A0000EFFF0000E7FF0000E7FF0018E7FF0018E7FF0000E7FF0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A0000000000000000000000000000000000000000004A4A
      4A0042EFFF008CF7FF00E7F7FF00C6FFFF00C6FFFF00A5F7FF0021D6FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C9C9C00848484009C9C9C00CECECE00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A004A4A
      4A0021E7FF0021E7FF00ADF7FF00ADF7FF00ADF7FF00ADF7FF00ADF7FF0021E7
      FF0021E7FF004A4A4A004A4A4A00000000000000000000000000000000004A4A
      4A0021E7FF004A4A4A00EFFFFF004A4A4A00FFFFFF004A4A4A0021D6FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      000000000000848484009C9C9C009C9C9C009C9C9C0084848400CECECE000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A0021E7
      FF004A4A4A0021E7FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0021E7
      FF004A4A4A0021E7FF004A4A4A00000000000000000000000000000000004A4A
      4A0021E7FF00EFFFFF00EFFFFF00FFFFFF00FFFFFF00B5FFFF0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000848484009C9C9C009C9C9C00B5B5B5009C9C9C009C9C9C0084848400CECE
      CE000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A0021E7
      FF00ADF7FF004A4A4A0021E7FF00ADF7FF004A4A4A00ADF7FF0021E7FF004A4A
      4A00ADF7FF0021E7FF004A4A4A00000000000000000000000000000000004A4A
      4A0021E7FF004A4A4A00FFFFFF004A4A4A00FFFFFF004A4A4A0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000009C9C
      9C009C9C9C009C9C9C00FFFFFF00B5B5B500FFFFFF009C9C9C009C9C9C009C9C
      9C00CECECE0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A0021E7
      FF00ADF7FF00FFFFFF004A4A4A004A4A4A0021E7FF004A4A4A004A4A4A0021E7
      FF00FFFFFF0021E7FF004A4A4A00000000000000000000000000000000004A4A
      4A0039EFFF009CF7FF00ADF7FF00ADF7FF00ADF7FF00B5EFFF0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000008484
      84009C9C9C00B5B5B500B5B5B500FFFFFF00B5B5B500B5B5B5009C9C9C008484
      8400CECECE0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A0021E7
      FF00ADF7FF00FFFFFF004A4A4A00ADF7FF00FFFFFF00ADF7FF004A4A4A0021E7
      FF00FFFFFF0021E7FF004A4A4A00000000000000000000000000000000004A4A
      4A0021E7FF004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000009C9C
      9C009C9C9C009C9C9C00FFFFFF00B5B5B500FFFFFF009C9C9C009C9C9C009C9C
      9C00CECECE0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A0021E7
      FF00ADF7FF004A4A4A0021E7FF00FFFFFF00FFFFFF00FFFFFF0021E7FF004A4A
      4A00ADF7FF0021E7FF004A4A4A00000000000000000000000000000000004A4A
      4A0021E7FF004A4A4A00CECECE00CECECE00CECECE004A4A4A0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000848484009C9C9C009C9C9C00B5B5B5009C9C9C009C9C9C0084848400CECE
      CE000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A0021E7
      FF004A4A4A00ADF7FF00ADF7FF00ADF7FF00ADF7FF00ADF7FF00ADF7FF0021E7
      FF004A4A4A0021E7FF004A4A4A00000000000000000000000000000000004A4A
      4A0021E7FF004A4A4A00CECECE00CECECE00CECECE004A4A4A0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      000000000000848484009C9C9C009C9C9C009C9C9C0084848400CECECE000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A004A4A
      4A0021E7FF0021E7FF0021E7FF0021E7FF0021E7FF0021E7FF0021E7FF0021E7
      FF0021E7FF004A4A4A004A4A4A00000000000000000000000000000000004A4A
      4A0021E7FF004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C9C9C00848484009C9C9C00CECECE00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A0000000000000000000000000000000000000000004A4A
      4A0021E7FF006BF7FF006BF7FF006BF7FF006BF7FF006BF7FF0021E7FF004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A4A4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A4A4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000848484006363630084848400CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000029B50000219C000029B50000CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000636363004A4A4A0063636300CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052CEFF0018ADFF0052CEFF00CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000063636300E7E7E700E7E7E700E7E7E70063636300CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219C000029B5000029B5000029B50000219C0000CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A4A4A006363630063636300636363004A4A4A00CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018ADFF0052CEFF0052CEFF0052CEFF0018ADFF00CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063636300E7E7E700FFFFFF00FFFFFF00FFFFFF00E7E7E70063636300CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000219C000029B5000029B5000000E7730029B5000029B50000219C0000CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      00004A4A4A0063636300636363009C9C9C0063636300636363004A4A4A00CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      000018ADFF0052CEFF008CE7FF00CEE7EF008CE7FF0052CEFF0018ADFF00CECE
      CE00000000000000000000000000000000000000000000000000000000006363
      6300E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7008484
      8400CECECE0000000000000000000000000000000000000000000000000029B5
      000029B5000029B50000FFFFFF0000E77300FFFFFF0029B5000029B5000029B5
      0000CECECE000000000000000000000000000000000000000000000000006363
      63006363630063636300FFFFFF009C9C9C00FFFFFF0063636300636363006363
      6300CECECE0000000000000000000000000000000000000000000000000052CE
      FF0052CEFF0084DEFF00FFFFFF00CEE7EF00FFFFFF008CE7FF0052CEFF0052CE
      FF00CECECE000000000000000000000000000000000000000000000000009C9C
      9C00E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7006363
      6300CECECE00000000000000000000000000000000000000000000000000219C
      000029B5000000E7730000E77300FFFFFF0000E7730000E7730029B50000219C
      0000CECECE000000000000000000000000000000000000000000000000004A4A
      4A00636363009C9C9C009C9C9C00FFFFFF009C9C9C009C9C9C00636363004A4A
      4A00CECECE0000000000000000000000000000000000000000000000000018AD
      FF0052CEFF00C6E7F700C6E7F700FFFFFF00CEE7EF00CEE7EF0052CEFF0018AD
      FF00CECECE000000000000000000000000000000000000000000000000006363
      6300E7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7008484
      8400CECECE0000000000000000000000000000000000000000000000000029B5
      000029B5000029B50000FFFFFF0000E77300FFFFFF0029B5000029B5000029B5
      0000CECECE000000000000000000000000000000000000000000000000006363
      63006363630063636300FFFFFF009C9C9C00FFFFFF0063636300636363006363
      6300CECECE0000000000000000000000000000000000000000000000000052CE
      FF0052CEFF008CE7FF00FFFFFF00CEE7EF00FFFFFF008CE7FF0052CEFF0052CE
      FF00CECECE000000000000000000000000000000000000000000000000000000
      000063636300E7E7E700FFFFFF00FFFFFF00FFFFFF00E7E7E70063636300CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000219C000029B5000029B5000000E7730029B5000029B50000219C0000CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      00004A4A4A0063636300636363009C9C9C0063636300636363004A4A4A00CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      000018ADFF0052CEFF008CE7FF00CEE7EF008CE7FF0052CEFF0018ADFF00CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      00000000000063636300E7E7E700E7E7E700E7E7E70063636300CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000219C000029B5000029B5000029B50000219C0000CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A4A4A006363630063636300636363004A4A4A00CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018ADFF0052CEFF0052CEFF0052CEFF0018ADFF00CECECE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000636363009C9C9C0063636300CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000029B50000219C000029B50000CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000636363004A4A4A0063636300CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052CEFF0018ADFF0052CEFF00CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF00008001E007FFFF0000
      8001E003FFFF00008000E001FFFF00008000E001FC3F00008000E001F81F0000
      8000E001F00F00008000E001E007000080008001E00700008000C001E0070000
      8000E001F00F00008000F001F81F00008441F803FC3F00008CC3FC27FFFF0000
      9DDFFE7FFFFF0000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF8001FFFFF01F
      FFFF8001FFFFE00FFFFF8001E003E00FFC3F8001C001E00FF81F8001C001E00F
      F00F8001C001E00FE0078001C001E00FE0078001C001E00FE0078001C001E00F
      F00F8001C001E00FF81F8001C001E00FFC3F8001E003E00FFFFF8001FFFFF01F
      FFFF8001FFFFF7FFFFFFFFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3FFC3FFC3FFC3FF81FF81FF81FF81F
      F00FF00FF00FF00FE007E007E007E007E007E007E007E007E007E007E007E007
      F00FF00FF00FF00FF81FF81FF81FF81FFC3FFC3FFC3FFC3FFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object SPObtenerUltimosConsumos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerUltimosConsumosSinConcesionaria'
    Parameters = <>
    Left = 792
    Top = 672
  end
  object dsUltConsumos: TDataSource
    DataSet = SPObtenerUltimosConsumos
    Left = 392
    Top = 336
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 514
    Top = 545
  end
  object ObtenerConvenioCartola: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConvenioCartola'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 640
    Top = 552
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 280
    Top = 504
    Bitmap = {
      494C010103001000B8030F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF000EC9FF000EC9
      FF00FFFF0000FFFF0000FF000000FF0000000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000EC9FF000EC9FF00FFFF0000FFFF0000FFFF0000FF00
      00000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000EC9FF000EC9
      FF00FFFF0000FFFF0000FFFF0000FF0000000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000EC9FF000EC9FF00FFFF0000FFFF0000FFFF0000FF00
      00000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000EC9FF000EC9
      FF00FFFF0000FFFF0000FFFF0000FF0000000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000EC9FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000EC9FF000000
      0000FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000
      FF00FFFFFF000000FF00FFFFFF00000000000EC9FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF000EC9FF0000000000FFFFFF000000FF00FFFFFF000000
      FF00FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000
      00000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF000EC9FF000000
      0000FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000
      FF00FFFFFF000000FF00FFFFFF00000000000EC9FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF000EC9FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000EC9FF000EC9
      FF00FFFF0000FFFF0000FFFF0000FF0000000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000EC9FF000EC9FF00FFFF0000FFFF0000FFFF0000FF00
      00000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF000EC9FF000EC9
      FF00FFFF0000FFFF0000FFFF0000FF0000000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9FF000EC9
      FF000EC9FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC000000008003000400000000BFFB7FF400000000BFFB77F400000000
      BFFB63F400000000BFFB41F400000000BFFB48F400000000BFFB5C7400000000
      BFFB7E3400000000BFFB7F1400000000BFFB7F9400000000BFFB7FD400000000
      BFFB7FF4000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
  object ObtenerConvenioObservacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConvenioObservacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@IncluirDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 672
    Top = 552
  end
  object ilCamara: TImageList
    Height = 15
    Width = 15
    Left = 365
    Top = 504
    Bitmap = {
      494C01010300200188030F000F00FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A000000000000000000FFFF0000FFFF0000FFFF0000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4A4A0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EF
      FF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF004A4A4A0000000000FFFF
      0000FFFF0000FFFF0000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4A4A0000EFFF00B5FFFF00B5FF
      FF00B5FFFF00848484004A4A4A004A4A4A004A4A4A00B5FFFF00B5FFFF00B5FF
      FF0000EFFF004A4A4A0000000000FFFF0000FFFF0000FFFF0000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4A4A0000EFFF00B5FFFF00FFFFFF004A4A4A0084848400B5B5B500B5B5
      B500848484004A4A4A00FFFFFF00B5FFFF0000EFFF004A4A4A0000000000FFFF
      0000FFFF0000FFFF0000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4A4A0000EFFF00B5FFFF00FFFF
      FF004A4A4A008484840084848400848484004A4A4A004A4A4A00FFFFFF00B5FF
      FF0000EFFF004A4A4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4A4A0000EFFF00B5FFFF00FFFFFF004A4A4A0084848400B5B5B500B5B5
      B500848484004A4A4A00FFFFFF00B5FFFF0000EFFF004A4A4A0000000000FFFF
      FF000000FF000000FF00FFFFFF000000FF000000FF00FFFFFF000000FF000000
      FF00FFFFFF000000FF000000FF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4A4A0000EFFF00B5FFFF00B5FF
      FF00B5FFFF004A4A4A004A4A4A004A4A4A004A4A4A00B5FFFF00B5FFFF00B5FF
      FF0000EFFF004A4A4A0000000000FFFFFF000000FF000000FF00FFFFFF000000
      FF000000FF00FFFFFF000000FF000000FF00FFFFFF000000FF000000FF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4A4A0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF0000EF
      FF0000EFFF0000EFFF0000EFFF0000EFFF0000EFFF004A4A4A0000000000FFFF
      FF000000FF000000FF00FFFFFF000000FF000000FF00FFFFFF000000FF000000
      FF00FFFFFF000000FF000000FF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4A4A004A4A4A004A4A
      4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A
      4A004A4A4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000004A4A4A0000EFFF0000EFFF004A4A4A00000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4A4A004A4A
      4A004A4A4A004A4A4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFF0000FFFF0000FFFF0000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      8001FFFFFFF800008001FFFE1FF80000800180061FF80000800100021FF80000
      800100021FF80000800100021FF8000080010000000000008001000000000000
      8001000000000000800100000000000080018004000000008001C3FE1FF80000
      8001C3FE1FF800008001FFFE1FF80000FFFFFFFFFFF800000000000000000000
      0000000000000000000000000000}
  end
  object spFechasProximaFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFechasProximaFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 424
    Top = 513
  end
  object dsfechasProximaFacturacion: TDataSource
    DataSet = spFechasProximaFacturacion
    Left = 192
    Top = 352
  end
  object ObtenerHistoricoMedioPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerHistoricoMedioPago'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 704
    Top = 552
  end
  object daObtenerHistoricoMedioPago: TDataSource
    DataSet = ObtenerHistoricoMedioPago
    Left = 272
    Top = 376
  end
  object daObtenerHistoricoDomicilios: TDataSource
    DataSet = ObtenerHistoricoDomicilios
    Left = 728
    Top = 552
  end
  object ObtenerHistoricoDomicilios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerHistoricoDomicilios'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 152
    Top = 496
  end
  object sdGuardarCSV: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 'Archivos Comma Separated Value|*.csv|Todos los Archivos|*.*'
    InitialDir = '.'
    Title = 'Guardar Como...'
    Left = 584
    Top = 512
  end
  object spExplorarEMails: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ExplorarEMails'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TopFilter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 13
        Value = Null
      end>
    Left = 1056
    Top = 104
  end
  object dsEMails: TDataSource
    DataSet = spExplorarEMails
    Left = 1096
    Top = 104
  end
  object mnuComponer: TPopupMenu
    OnPopup = mnuComponerPopup
    Left = 1208
    Top = 344
    object mnuComponerEsteMail: TMenuItem
      Caption = 'Componer este e-Mail'
      OnClick = mnuComponerEsteMailClick
    end
  end
  object spObtenerAccionesCobranzaSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerAccionesCobranzaSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 967
    Top = 652
  end
  object dsObtenerAccionesCobranzaSerbanc: TDataSource
    DataSet = spObtenerAccionesCobranzaSerbanc
    Left = 903
    Top = 612
  end
  object spCierraLogComincaciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CerrarLogComunicacioes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 312
  end
  object spAgregarLogComunicacioes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarLogComunicacioes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodPersona'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = '@CodUsuario'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '@CodComunicacion'
        DataType = ftInteger
        Direction = pdOutput
        Value = 0
      end>
    Left = 112
    Top = 312
  end
  object dsResumenCuentaCorriente: TDataSource
    DataSet = ObtenerResumenCuentaCorriente
    Left = 1128
    Top = 336
  end
  object ObtenerResumenCuentaCorriente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerResumenCuentaCorriente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 1160
    Top = 360
  end
  object DSObtenerMediosPagoAnterioresConvenio: TDataSource
    DataSet = SPObtenerMediosPagoAnterioresConvenio
    Left = 616
    Top = 659
  end
  object dsIndemnizaciones: TDataSource
    DataSet = spObtenerIndemnizacionesConvenio
    Left = 856
    Top = 608
  end
  object spObtenerIndemnizacionesConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerIndemnizacionesConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1056
    Top = 648
  end
  object SPObtenerMediosPagoAnterioresConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMediosPagoAnterioresConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PermisoVerPAT'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Intento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 488
    Top = 616
  end
  object spInsertarConvenioAEnviarDetalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'InsertarConvenioAEnviarDetalle'
    Parameters = <>
    Left = 184
    Top = 680
  end
  object dsObtenerMensajesComprobanteParticular: TDataSource
    DataSet = spObtenerMensajesComprobanteParticular
    Left = 392
    Top = 640
  end
  object spObtenerMensajesComprobanteParticular: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMensajesComprobanteParticular;1'
    Parameters = <>
    Left = 512
    Top = 680
  end
  object spObtenerRefinanciacionResumenCuentaCorriente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionResumenCuentaCorriente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1056
    Top = 160
  end
  object dsRefinanResumenCC: TDataSource
    DataSet = cdsRefinanResumenCC
    Left = 1152
    Top = 176
  end
  object spObtenerRefinanciacionesTotalesPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionesTotalesPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1152
    Top = 224
  end
  object cdsRefinanResumenCC: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Orden'
        DataType = ftInteger
      end
      item
        Name = 'Fecha'
        DataType = ftDateTime
      end
      item
        Name = 'Tipo'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoRefinanciacion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'NumeroRecibo'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionTipo'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 37
      end
      item
        Name = 'NumeroTipo'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'DescripcionFormaPago'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoEstadoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionEstadoCuota'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'EstadoImpago'
        DataType = ftBoolean
      end
      item
        Name = 'ImporteDesc'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SaldoDesc'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SaldoCCDesc'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroConvenio'
        Attributes = [faReadonly, faFixed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoCuotaAplicada'
        Attributes = [faReadonly]
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionFormaPagoCuotaAplicada'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <
      item
        Name = 'Orden'
        Fields = 'Orden'
      end
      item
        Name = 'OrdenDESC'
        Fields = 'Orden'
        Options = [ixDescending]
      end
      item
        Name = 'Fecha'
        Fields = 'Fecha'
      end
      item
        Name = 'FechaDESC'
        Fields = 'Fecha'
        Options = [ixDescending]
      end
      item
        Name = 'DescripcionTipo'
        Fields = 'DescripcionTipo'
      end
      item
        Name = 'DescripcionTipoDESC'
        Fields = 'DescripcionTipo'
        Options = [ixDescending]
      end
      item
        Name = 'DescripcionFormaPago'
        Fields = 'DescripcionFormaPago'
      end
      item
        Name = 'DescripcionFormaPagoDESC'
        Fields = 'DescripcionFormaPago'
        Options = [ixDescending]
      end
      item
        Name = 'DescripcionEstadoCuota'
        Fields = 'DescripcionEstadoCuota'
      end
      item
        Name = 'DescripcionEstadoCuotaDESC'
        Fields = 'DescripcionEstadoCuota'
        Options = [ixDescending]
      end
      item
        Name = 'CodigoRefinanciacion'
        Fields = 'CodigoRefinanciacion;Orden'
      end
      item
        Name = 'CodigoRefinanciacionDESC'
        Fields = 'CodigoRefinanciacion;Orden'
        Options = [ixDescending]
      end>
    IndexName = 'OrdenDESC'
    Params = <>
    ProviderName = 'dspRefinanResumenCC'
    StoreDefs = True
    Left = 1144
    Top = 120
    object cdsRefinanResumenCCOrden: TIntegerField
      FieldName = 'Orden'
    end
    object cdsRefinanResumenCCFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object cdsRefinanResumenCCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object cdsRefinanResumenCCCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsRefinanResumenCCCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsRefinanResumenCCNumeroRecibo: TIntegerField
      FieldName = 'NumeroRecibo'
    end
    object cdsRefinanResumenCCDescripcionTipo: TStringField
      FieldName = 'DescripcionTipo'
      ReadOnly = True
      Size = 13
    end
    object cdsRefinanResumenCCNumeroTipo: TIntegerField
      FieldName = 'NumeroTipo'
      ReadOnly = True
    end
    object cdsRefinanResumenCCDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsRefinanResumenCCCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsRefinanResumenCCDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      ReadOnly = True
      Size = 30
    end
    object cdsRefinanResumenCCEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsRefinanResumenCCImporteDesc: TStringField
      FieldName = 'ImporteDesc'
      ReadOnly = True
    end
    object cdsRefinanResumenCCSaldoDesc: TStringField
      FieldName = 'SaldoDesc'
      ReadOnly = True
    end
    object cdsRefinanResumenCCSaldoCCDesc: TStringField
      FieldName = 'SaldoCCDesc'
      ReadOnly = True
    end
    object cdsRefinanResumenCCNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
  end
  object dspRefinanResumenCC: TDataSetProvider
    DataSet = spObtenerRefinanciacionResumenCuentaCorriente
    Left = 1056
    Top = 208
  end
  object spObtenerDeudaComprobantesVencidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDeudaComprobantesVencidos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1072
    Top = 592
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 1064
    Top = 608
  end
  object spObtenerEstacionamientos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEstacionamientosConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 352
    Top = 696
  end
  object dsEstacionamientos: TDataSource
    DataSet = spObtenerEstacionamientos
    Left = 288
    Top = 656
  end
  object dsConvenioObservacion: TDataSource
    DataSet = ObtenerConvenioObservacion
    Left = 672
    Top = 584
  end
  object spInicializarNotificacionesTAGsPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InicializarNotificacionesTAGsPersona'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 148
    Top = 628
  end
  object dlgGrabarExcel: TSaveDialog
    DefaultExt = '.csv'
    FileName = '*.csv'
    Filter = '*.csv'
    Title = 'Nombre Archivo Excel a Exportar'
    Left = 992
    Top = 416
  end
  object spObtenerHistoricoListaDeAccesoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerHistoricoListaDeAccesoConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 1208
    Top = 592
  end
  object spObtenerHistoricoConveniosInhabilitados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerHistoricoConveniosInhabilitados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 1200
    Top = 608
  end
  object dsHistoricoListaDeAccesoConvenio: TDataSource
    DataSet = spObtenerHistoricoListaDeAccesoConvenio
    Left = 1160
    Top = 600
  end
  object dsHistoricoConveniosInhabilitados: TDataSource
    DataSet = spObtenerHistoricoConveniosInhabilitados
    Left = 1120
    Top = 600
  end
  object spHistoricoListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerHistoricoListaAmarilla;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaAltaInicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaBajaInicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 936
    Top = 520
  end
  object spHistoricoCartasListaAmarilla: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistoricoCartasListaAmarilla;1'
    Parameters = <>
    Left = 968
    Top = 520
  end
  object dsHistoricoListaAmarilla: TDataSource
    DataSet = spHistoricoListaAmarilla
    OnDataChange = dsHistoricoListaAmarillaDataChange
    Left = 936
    Top = 552
  end
  object dsHistoricoCartas: TDataSource
    DataSet = spHistoricoCartasListaAmarilla
    Left = 968
    Top = 552
  end
  object spObtenerDescuentosConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDescuentosConvenio'
    Parameters = <>
    Left = 1088
    Top = 504
  end
  object dsObtenerDescuentosConvenio: TDataSource
    DataSet = spObtenerDescuentosConvenio
    Left = 1176
    Top = 504
  end
end
