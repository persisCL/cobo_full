unit OPScripts;

interface

Uses
	SysUtils, Evaluate, DB, AdoDB;

function CrearEvaluadorOrdenServicio(Conn: TAdoConnection; CodigoOrdenServicio: Integer): TExpressionEvaluator;

implementation

function CrearEvaluadorOrdenServicio(Conn: TAdoConnection; CodigoOrdenServicio: Integer): TExpressionEvaluator;
Var
	i: Integer;
	Field: TField;
	Qry: TAdoQuery;
begin
	Result := TExpressionEvaluator.Create;
	Qry := TAdoQuery.Create(nil);
	Qry.Connection := Conn;
	Qry.SQL.Text := 'EXECUTE ObtenerVariablesOrdenServicio ' + IntToStr(CodigoOrdenServicio);
	try
		Qry.Open;
		// Agregamos las variables que nos devuelve el query
		for i := 0 to Qry.FieldCount - 1 do begin
			Field := Qry.Fields[i];
			Result.AddVariable(Field.FieldName);
			case Field.DataType of
				ftSmallint, ftFloat, ftInteger, ftWord, ftCurrency, ftBCD, ftAutoInc,
				  ftFMTBcd, ftLargeint:
					Result.Variables[Field.FieldName].Value := Qry.Fields[i].AsFloat;
				ftBoolean:
					Result.Variables[Field.FieldName].Value := Qry.Fields[i].AsBoolean;
				ftDate, ftTime, ftDateTime, ftTimeStamp:
					Result.Variables[Field.FieldName].Value := Double(Qry.Fields[i].AsDateTime);
				else
					Result.Variables[Field.FieldName].Value := Trim(Qry.Fields[i].AsString);
			end;
		end;
	except
		Result.Free;
		Result := nil;
	end;
	Qry.Close;
	Qry.Free;
end;

end.
