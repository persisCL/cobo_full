object frmAgregarItemFacturacion: TfrmAgregarItemFacturacion
  Left = 346
  Top = 189
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Agregar item de facturaci'#243'n'
  ClientHeight = 167
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    384
    167)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 369
    Height = 120
  end
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 30
    Height = 13
    Caption = 'Fecha'
  end
  object Label2: TLabel
    Left = 33
    Top = 50
    Width = 46
    Height = 13
    Caption = 'Concepto'
  end
  object Label3: TLabel
    Left = 33
    Top = 76
    Width = 33
    Height = 13
    Caption = 'Detalle'
  end
  object Label4: TLabel
    Left = 33
    Top = 103
    Width = 35
    Height = 13
    Caption = 'Importe'
  end
  object edFecha: TDateEdit
    Left = 90
    Top = 20
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    Date = -693594
  end
  object cbConcepto: TVariantComboBox
    Left = 90
    Top = 45
    Width = 243
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items = <>
  end
  object btnCancelar: TButton
    Left = 303
    Top = 136
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 6
  end
  object btnAceptar: TButton
    Left = 224
    Top = 136
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 5
    OnClick = btnAceptarClick
  end
  object edImporte: TNumericEdit
    Left = 90
    Top = 98
    Width = 94
    Height = 21
    MaxLength = 7
    TabOrder = 3
    Decimals = 0
  end
  object edDetalle: TEdit
    Left = 90
    Top = 71
    Width = 243
    Height = 21
    TabOrder = 2
  end
  object chk_ImporteIncluyeIVA: TCheckBox
    Left = 202
    Top = 99
    Width = 129
    Height = 17
    Hint = 'Indica si el importe ingresado ya incluye el I.V.A.'
    Caption = 'Importe incluye I.V.A:'
    TabOrder = 4
    Visible = False
  end
  object spObtenerConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConceptosComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 44
    Top = 169
  end
end
