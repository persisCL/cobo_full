object frmImagenesTransitos: TfrmImagenesTransitos
  Left = 411
  Top = 237
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeToolWin
  Caption = 'Imagenes del Tr'#225'nsito'
  ClientHeight = 302
  ClientWidth = 411
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pcImagenes: TPageControl
    Left = 0
    Top = 0
    Width = 411
    Height = 302
    ActivePage = tsFrontal1
    Align = alClient
    TabOrder = 0
    object tsFrontal1: TTabSheet
      Caption = 'Frontal 1'
      object imgFrontal: TImage
        Left = 0
        Top = 0
        Width = 403
        Height = 274
        Align = alClient
        Stretch = True
        ExplicitWidth = 411
        ExplicitHeight = 282
      end
    end
    object tsPosterior1: TTabSheet
      Caption = 'Posterior 1'
      ImageIndex = 1
      object imgPosterior: TImage
        Left = 0
        Top = 0
        Width = 618
        Height = 382
        Align = alClient
        Stretch = True
      end
    end
    object tsOverview1: TTabSheet
      Caption = 'Vista Superior 1'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgOverview1: TImage
        Left = 0
        Top = 0
        Width = 411
        Height = 274
        Align = alClient
        Stretch = True
      end
    end
    object tsOverview2: TTabSheet
      Caption = 'Vista Superior 2'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgOverview2: TImage
        Left = 0
        Top = 0
        Width = 618
        Height = 382
        Align = alClient
        Stretch = True
      end
    end
    object tsOverview3: TTabSheet
      Caption = 'Vista Superior 3'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgOverview3: TImage
        Left = 0
        Top = 0
        Width = 618
        Height = 382
        Align = alClient
        Stretch = True
      end
    end
    object tsFrontal2: TTabSheet
      Caption = 'Frontal 2'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgFrontal2: TImage
        Left = 0
        Top = 0
        Width = 618
        Height = 382
        Align = alClient
        Stretch = True
      end
    end
    object tsPosterior2: TTabSheet
      Caption = 'Posterior 2'
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object imgPosterior2: TImage
        Left = 0
        Top = 0
        Width = 618
        Height = 382
        Align = alClient
        Stretch = True
      end
    end
  end
end
