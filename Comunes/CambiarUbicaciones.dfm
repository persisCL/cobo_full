object frmCambiarUbicaciones: TfrmCambiarUbicaciones
  Left = 266
  Top = 207
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Cambiar la Ubicaci'#243'n de TAGs'
  ClientHeight = 503
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    484
    503)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 11
    Top = 100
    Width = 98
    Height = 13
    Caption = 'Cantidades Telev'#237'as'
  end
  object btn_aceptar: TButton
    Left = 318
    Top = 471
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 2
    OnClick = btn_aceptarClick
  end
  object btn_cancelar: TButton
    Left = 398
    Top = 471
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 3
    OnClick = btn_cancelarClick
  end
  object gbAlmacenes: TGroupBox
    Left = 9
    Top = 5
    Width = 470
    Height = 89
    Caption = 'Almacenes'
    TabOrder = 0
    object Label2: TLabel
      Left = 14
      Top = 24
      Width = 94
      Height = 13
      Caption = 'Almac'#233'n Origen:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 14
      Top = 56
      Width = 100
      Height = 13
      Caption = 'Almac'#233'n Destino:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbOrigen: TVariantComboBox
      Left = 192
      Top = 20
      Width = 266
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbOrigenChange
      Items = <>
    end
    object cbDestino: TVariantComboBox
      Left = 192
      Top = 52
      Width = 266
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
  end
  inline frameMovimientoStock: TframeMovimientoStock
    Left = 9
    Top = 117
    Width = 470
    Height = 342
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 9
    ExplicitTop = 117
    ExplicitWidth = 470
    ExplicitHeight = 342
    inherited pnlEntradaDatos: TPanel
      Top = 246
      Width = 470
      ExplicitTop = 246
      ExplicitWidth = 470
      inherited gbAgregaTelevias: TGroupBox
        inherited Label1: TLabel
          Width = 67
          ExplicitWidth = 67
        end
        inherited Label2: TLabel
          Width = 62
          ExplicitWidth = 62
        end
        inherited Label3: TLabel
          Width = 42
          ExplicitWidth = 42
        end
        inherited btnGrabar: TButton
          OnClick = frameMovimientoStockbtnGrabarClick
        end
      end
    end
    inherited Panel1: TPanel
      Width = 470
      Height = 246
      ExplicitWidth = 470
      ExplicitHeight = 246
      inherited dbgRecibirTags: TDBGrid
        Width = 470
        TitleFont.Name = 'MS Sans Serif'
      end
      inherited dbgTags: TDBGrid
        Height = 165
        TitleFont.Name = 'MS Sans Serif'
      end
    end
    inherited cdsTAGs: TClientDataSet
      Active = False
      BeforeEdit = frameMovimientoStockcdsTAGsBeforeEdit
      AfterPost = frameMovimientoStockcdsTAGsAfterPost
      AfterCancel = frameMovimientoStockcdsTAGsAfterPost
      OnNewRecord = frameMovimientoStockcdsTAGsNewRecord
    end
  end
  object btnBorrarLinea: TButton
    Left = 167
    Top = 470
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Borrar l'#237'nea'
    Enabled = False
    TabOrder = 4
    OnClick = btnBorrarLineaClick
  end
  object btnAgregarLinea: TButton
    Left = 11
    Top = 470
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&A'#241'adir L'#237'nea'
    TabOrder = 5
    OnClick = btnAgregarLineaClick
  end
  object btnEditarLinea: TButton
    Left = 89
    Top = 470
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Editar L'#237'nea'
    TabOrder = 6
    OnClick = btnEditarLineaClick
  end
  object qryOrigen: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOperacion'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'CodigoBodegaOrigen'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoBodegaOrigenAux'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select DISTINCT'
      '  MaestroAlmacenes.CodigoAlmacen, MaestroAlmacenes.Descripcion'
      'from'
      
        '  AlmacenesOrigenOperacion  WITH (NOLOCK) , MaestroAlmacenes  WI' +
        'TH (NOLOCK) '
      'where'
      
        '  AlmacenesOrigenOperacion.CodigoAlmacen = MaestroAlmacenes.Codi' +
        'goAlmacen'
      
        '  AND AlmacenesOrigenOperacion.CodigoOperacion = :CodigoOperacio' +
        'n'
      
        '  AND ((MaestroAlmacenes.CodigoBodega = :CodigoBodegaOrigen) OR ' +
        '(:CodigoBodegaOrigenAux = 0))')
    Left = 206
    Top = 11
  end
  object qryDestino: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    Left = 207
    Top = 46
  end
end
