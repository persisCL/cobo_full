unit validarRangos;

interface

implementation


function ValidarRangosSuperpuestos(CdsRangos: TClientDataSet; ConMsg: Boolean=False): Boolean;
var
    bm: TBookmark;
    NumeroInicialTAG, NumeroFinalTAG: DWORD;
begin
    result := true;
    cursor := crHourGlass;
    cdsRangos.disableControls;
    try
        with cdsRangos do begin
            while not eof do begin
                // Tomamos el registro actual para no verificar
                Actual := RecNo;
                NumeroInicialTAG := EtiquetaToContractSerialNumber(fieldByName('NumeroInicialTAG').asString);
                NumeroFinalTAG := EtiquetaToContractSerialNumber(fieldByName('NumeroFinalTAG').asString);
                bm := GetBookMark;
                try
                    // Recorremos el datase verificando el n�mero
                    first;
                    while not eof do begin
                        if Actual = RecNo then
                            next;
                        else begin
                            if ((NumeroInicialTAG >= EtiquetaToContractSerialNumber(fieldByName('NumeroInicialTAG').asString)) and
                              (NumeroInicialTAG <= EtiquetaToContractSerialNumber(fieldByName('NumeroFinalTAG').asString))) or
                               ((NumeroFinalTAG >= EtiquetaToContractSerialNumber(fieldByName('NumeroInicialTAG').asString)) and
                              (NumeroFinalTAG <= EtiquetaToContractSerialNumber(fieldByName('NumeroFinalTAG').asString))) then begin
                                result := false;
                                break;
                            end
                    end;
                    GotoBookMark(bm);
                    next;
                finally
                    FreeBookMark(bm);
                end;
            end;
        end;
    finally
        cdsRangos.disableControls;
        cursor := crDefault;
    end;
end;

end.
