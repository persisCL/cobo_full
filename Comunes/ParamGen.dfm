object FormParam: TFormParam
  Left = 165
  Top = 140
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Par'#225'metros Generales del Sistema Central de Peaje'
  ClientHeight = 467
  ClientWidth = 825
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  DesignSize = (
    825
    467)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 825
    Height = 425
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Par'#225'metros'
      object Lista: TDBListEx
        Left = 0
        Top = 41
        Width = 817
        Height = 356
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 250
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            Width = 200
            Header.Caption = 'Valor'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Color = clSilver
            IsLink = True
            FieldName = 'Valor'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 250
            Header.Caption = 'Por Defecto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'ValorDefault'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'TipoParametro'
          end>
        DataSource = dsParametrosGenerales
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDrawText = ListaDrawText
        OnLinkClick = ListaLinkClick
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 817
        Height = 41
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        BorderStyle = bsSingle
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 12
          Width = 214
          Height = 13
          Caption = 'S'#243'lo ver par'#225'metros configurables relativos a:'
        end
        object cbFiltros: TVariantComboBox
          Left = 228
          Top = 9
          Width = 241
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbFiltrosChange
          Items = <>
        end
      end
    end
  end
  object ButtonAceptar: TButton
    Left = 746
    Top = 435
    Width = 71
    Height = 24
    Anchors = [akRight, akBottom]
    Caption = '&Cerrar'
    Default = True
    TabOrder = 1
    OnClick = ButtonAceptarClick
    ExplicitTop = 433
  end
  object dsParametrosGenerales: TDataSource
    DataSet = cdsParametrosGenerales
    Left = 128
    Top = 288
  end
  object cdsParametrosGenerales: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprParametrosGenerales'
    Left = 128
    Top = 240
  end
  object dtstprParametrosGenerales: TDataSetProvider
    DataSet = spParametrosGenerales_SELECT
    Left = 128
    Top = 192
  end
  object spParametrosGenerales_SELECT: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ParametrosGenerales_SELECT'
    Parameters = <>
    Left = 128
    Top = 144
  end
  object spParametrosGenerales_UPDATE: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ParametrosGenerales_UPDATE'
    Parameters = <>
    Left = 592
    Top = 208
  end
end
