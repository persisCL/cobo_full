unit frmMedioEnvioMail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ppParameter, ppCtrls, ppPrnabl, ppClass, ppBands, ppCache, ppProd, ppReport, ppDB, ppComm, ppRelatv,
  ppDBPipe, UtilRB, DB, ADODB, SysUtilsCN, PeaProcs, PeaProcsCN, UtilProc;

type
  TFormMedioEnvioMail = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    tpMedioEnvio: TPanel;
    txtEmail: TEdit;
    Label1: TLabel;
    pnl_ConvenioNoTieneEmail: TPanel;
    lblEstadoClienteEMail: TLabel;
    lblEstadoConvenioEnvioPorEMail: TLabel;
    grp1: TGroupBox;
    chkNotaCobroPorEMail: TCheckBox;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCLienteTieneEMail,
    FClienteAdheridoEnvioEMail: Boolean;
    FEMailOriginal: string;
    function Inicializar(pEMail: string; pClienteAdheridoEnvioEMail: Boolean): Boolean;
end;


implementation

{$R *.dfm}


procedure TFormMedioEnvioMail.btnAceptarClick(Sender: TObject);
	resourcestring
    	rsTituloValidacion  = 'Validaci�n E-Mail';
        rsErrorEMailVacio   = 'Debe ingresar un E-Mail.';
        rsErrorEMailFormato = 'El E-Mail introducido NO es v�lido.';
        rsErrorBorrarEMail  = 'No puede eliminar un E-Mail ya existente, s�lo puede modificarlo.';
    const
        MaxArray = 3;
    var
        vControles  : Array [1..MaxArray] of TControl;
        vCondiciones: Array [1..MaxArray] of Boolean;
        vMensajes   : Array [1..MaxArray] of String;
        EMail       : string;
begin
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);

        EMail := Trim(txtEmail.Text);

        vControles[01] := txtEmail;
        vControles[02] := txtEmail;
        vControles[03] := txtEmail;

        vCondiciones[01] := (not chkNotaCobroPorEMail.Checked) or (chkNotaCobroPorEMail.Checked and (EMail <> EmptyStr));
        vCondiciones[02] := (FEMailOriginal = EmptyStr) or ((FEMailOriginal <> EmptyStr) and (EMail <> EmptyStr));
        vCondiciones[03] := (EMail = EmptyStr) or (EMail <> EmptyStr) and (IsValidEMailCN(EMail));

        vMensajes[01] := rsErrorEMailVacio;
        vMensajes[02] := rsErrorBorrarEMail;
        vMensajes[03] := rsErrorEMailFormato;

    	try
        	if ValidateControls(vControles, vCondiciones, rsTituloValidacion, vMensajes) then begin
            	ModalResult := mrOk;
            end
            else begin
            	if not vCondiciones[02] then txtEmail.Text := FEMailOriginal;
            end;
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
    	CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

function TFormMedioEnvioMail.Inicializar(pEMail: string; pClienteAdheridoEnvioEMail: Boolean): Boolean;
	resourcestring
        rsClienteSinEMail = 'El Cliente NO tiene E-Mail';
        rsClienteConEMail = 'El Cliente tiene E-Mail';
        rsClienteAdheridoEnvioEMail   = 'El Cliente est� adherido al env�o de Documentos v�a E-Mail';
        rsClienteNoAdheridoEnvioEMail = 'El Cliente NO est� adherido al env�o de Documentos v�a E-Mail';
begin
    FEMailOriginal := pEMail;

	if pEMail <> EmptyStr then begin
    	lblEstadoClienteEMail.Caption := rsClienteConEMail;
        lblEstadoClienteEMail.Font.Color := clGreen;
        txtEmail.Text := pEMail;
    end
    else begin
    	lblEstadoClienteEMail.Caption := rsClienteSinEMail;
        lblEstadoClienteEMail.Font.Color := clRed;
    end;

    if pClienteAdheridoEnvioEMail then begin
    	lblEstadoConvenioEnvioPorEMail.Caption := rsClienteAdheridoEnvioEMail;
        lblEstadoConvenioEnvioPorEMail.Font.Color := clGreen;
        chkNotaCobroPorEMail.Checked := True;
        chkNotaCobroPorEMail.Enabled := False;
    end
    else begin
    	lblEstadoConvenioEnvioPorEMail.Caption := rsClienteNoAdheridoEnvioEMail;
        lblEstadoConvenioEnvioPorEMail.Font.Color := clRed;
    end;

    Result := True;
end;

//INICIO: TASK_044_ECA_20160630
procedure TFormMedioEnvioMail.btnCancelarClick(Sender: TObject);
begin
  if MsgBox('Si cancela no recibir� el correo de Bienvenida de la Concecionaria �Est� seguro de querer cancelar?', 'Medio de env�o por E-Mail', MB_YESNO or MB_ICONWARNING) = IDYES then
    Close;
end;
//FIN: TASK_044_ECA_20160630
end.
