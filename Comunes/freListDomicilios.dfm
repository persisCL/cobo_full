object FrameListDomicilios: TFrameListDomicilios
  Left = 0
  Top = 0
  Width = 662
  Height = 202
  Anchors = []
  TabOrder = 0
  DesignSize = (
    662
    202)
  object dblDomicilios: TDBListEx
    Left = 0
    Top = 0
    Width = 662
    Height = 161
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 400
        Header.Caption = 'Direcci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DireccionCompleta'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 110
        Header.Caption = 'Domicilio de Entrega'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
      end>
    DataSource = dsDomicilios
    DragReorder = True
    ParentColor = False
    TabOrder = 3
    TabStop = True
    OnDblClick = dblDomiciliosDblClick
    OnDrawText = dblDomiciliosDrawText
  end
  object btnAgregarDomicilio: TButton
    Left = 427
    Top = 166
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Agregar'
    TabOrder = 0
    OnClick = btnAgregarDomicilioClick
  end
  object btnEditarDomicilio: TButton
    Left = 502
    Top = 166
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Editar'
    TabOrder = 1
    OnClick = btnEditarDomicilioClick
  end
  object btnEliminarDomicilio: TButton
    Left = 577
    Top = 166
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'E&liminar'
    TabOrder = 2
    OnClick = btnEliminarDomicilioClick
  end
  object ObtenerDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftSmallint
        Direction = pdReturnValue
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = '@CodigoTipoDomicilio'
        DataType = ftWord
        Size = -1
        Value = Null
      end>
    Left = 16
    Top = 160
  end
  object dsDomicilios: TDataSource
    DataSet = cdsDomicilios
    Left = 44
    Top = 160
  end
  object cdsDomicilios: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoDomicilio'
        DataType = ftSmallint
      end
      item
        Name = 'DescriTipoDomicilio'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DescripcionCiudad'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoCalle'
        DataType = ftInteger
      end
      item
        Name = 'Numero'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Piso'
        DataType = ftSmallint
      end
      item
        Name = 'Dpto'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CalleDesnormalizada'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPostal'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DomicilioEntrega'
        DataType = ftBoolean
      end
      item
        Name = 'PaisCalleRelacionadaUno'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'RegionCalleRelacionadaUno'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'ComunaCalleRelacionadaUno'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'PaisCalleRelacionadaDos'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'RegionCalleRelacionadaDos'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'ComunaCalleRelacionadaDos'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'Normalizado'
        DataType = ftInteger
      end
      item
        Name = 'DireccionCompleta'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'UbicacionGeografica'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <
      item
        Name = 'DireccionCompleta'
        Expression = 'DireccionCompleta'
        Options = [ixExpression]
      end>
    IndexName = 'DireccionCompleta'
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end
      item
        DataType = ftInteger
        Name = '@CodigoPersona'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftWord
        Name = '@CodigoTipoDomicilio'
        ParamType = ptInput
      end>
    ProviderName = 'dspDomicilios'
    StoreDefs = True
    AfterOpen = cdsDomiciliosAfterOpen
    AfterInsert = cdsDomiciliosAfterOpen
    AfterPost = cdsDomiciliosAfterOpen
    AfterDelete = cdsDomiciliosAfterOpen
    Left = 72
    Top = 160
    object cdsDomiciliosCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object cdsDomiciliosCodigoPersona: TIntegerField
      FieldName = 'CodigoPersona'
    end
    object cdsDomiciliosCodigoTipoDomicilio: TSmallintField
      FieldName = 'CodigoTipoDomicilio'
    end
    object cdsDomiciliosDescriTipoDomicilio: TStringField
      FieldName = 'DescriTipoDomicilio'
      Size = 50
    end
    object cdsDomiciliosCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriPais: TStringField
      FieldName = 'DescriPais'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriRegion: TStringField
      FieldName = 'DescriRegion'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriComuna: TStringField
      FieldName = 'DescriComuna'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosCodigoCiudad: TStringField
      FieldName = 'CodigoCiudad'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosDescriCiudad: TStringField
      FieldName = 'DescriCiudad'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosDescripcionCiudad: TStringField
      FieldName = 'DescripcionCiudad'
      Size = 50
    end
    object cdsDomiciliosCodigoCalle: TIntegerField
      FieldName = 'CodigoCalle'
    end
    object cdsDomiciliosNumero: TStringField
      FieldName = 'Numero'
      FixedChar = True
      Size = 10
    end
    object cdsDomiciliosPiso: TSmallintField
      FieldName = 'Piso'
    end
    object cdsDomiciliosDpto: TStringField
      FieldName = 'Dpto'
      Size = 10
    end
    object cdsDomiciliosCalleDesnormalizada: TStringField
      FieldName = 'CalleDesnormalizada'
      Size = 100
    end
    object cdsDomiciliosDetalle: TStringField
      FieldName = 'Detalle'
      Size = 50
    end
    object cdsDomiciliosCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      FixedChar = True
      Size = 10
    end
    object cdsDomiciliosDomicilioEntrega: TBooleanField
      FieldName = 'DomicilioEntrega'
    end
    object cdsDomiciliosPaisCalleRelacionadaUno: TStringField
      FieldName = 'PaisCalleRelacionadaUno'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosRegionCalleRelacionadaUno: TStringField
      FieldName = 'RegionCalleRelacionadaUno'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosComunaCalleRelacionadaUno: TStringField
      FieldName = 'ComunaCalleRelacionadaUno'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosCalleRelacionadaUno: TIntegerField
      FieldName = 'CalleRelacionadaUno'
    end
    object cdsDomiciliosNumeroCalleRelacionadaUno: TIntegerField
      FieldName = 'NumeroCalleRelacionadaUno'
    end
    object cdsDomiciliosPaisCalleRelacionadaDos: TStringField
      FieldName = 'PaisCalleRelacionadaDos'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosRegionCalleRelacionadaDos: TStringField
      FieldName = 'RegionCalleRelacionadaDos'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosComunaCalleRelacionadaDos: TStringField
      FieldName = 'ComunaCalleRelacionadaDos'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosCalleRelacionadaDos: TIntegerField
      FieldName = 'CalleRelacionadaDos'
    end
    object cdsDomiciliosNumeroCalleRelacionadaDos: TIntegerField
      FieldName = 'NumeroCalleRelacionadaDos'
    end
    object cdsDomiciliosNormalizado: TIntegerField
      FieldName = 'Normalizado'
    end
    object cdsDomiciliosDireccionCompleta: TStringField
      FieldName = 'DireccionCompleta'
      Size = 200
    end
    object cdsDomiciliosUbicacionGeografica: TStringField
      FieldName = 'UbicacionGeografica'
      Size = 100
    end
  end
  object dspDomicilios: TDataSetProvider
    DataSet = ObtenerDomiciliosPersona
    Constraints = True
    Left = 104
    Top = 160
  end
  object cdsDomiciliosBorrados: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoDomicilio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoDomicilio'
        DataType = ftSmallint
      end
      item
        Name = 'DescriTipoDomicilio'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriPais'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriRegion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriComuna'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DescriCiudad'
        Attributes = [faFixed]
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DescripcionCiudad'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoCalle'
        DataType = ftInteger
      end
      item
        Name = 'Numero'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Piso'
        DataType = ftSmallint
      end
      item
        Name = 'Dpto'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CalleDesnormalizada'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoPostal'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DomicilioEntrega'
        DataType = ftBoolean
      end
      item
        Name = 'PaisCalleRelacionadaUno'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'RegionCalleRelacionadaUno'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'ComunaCalleRelacionadaUno'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaUno'
        DataType = ftInteger
      end
      item
        Name = 'PaisCalleRelacionadaDos'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'RegionCalleRelacionadaDos'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'ComunaCalleRelacionadaDos'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'NumeroCalleRelacionadaDos'
        DataType = ftInteger
      end
      item
        Name = 'Normalizado'
        DataType = ftInteger
      end
      item
        Name = 'DireccionCompleta'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'UbicacionGeografica'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspDomicilios'
    StoreDefs = True
    Left = 392
    Top = 160
    object cdsDomiciliosBorradosCodigoDomicilio: TIntegerField
      FieldName = 'CodigoDomicilio'
    end
    object cdsDomiciliosBorradosCodigoPersona: TIntegerField
      FieldName = 'CodigoPersona'
    end
    object cdsDomiciliosBorradosCodigoTipoDomicilio: TSmallintField
      FieldName = 'CodigoTipoDomicilio'
    end
    object cdsDomiciliosBorradosDescriTipoDomicilio: TStringField
      FieldName = 'DescriTipoDomicilio'
      Size = 50
    end
    object cdsDomiciliosBorradosCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosDescriPais: TStringField
      FieldName = 'DescriPais'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosBorradosCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosDescriRegion: TStringField
      FieldName = 'DescriRegion'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosBorradosCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosDescriComuna: TStringField
      FieldName = 'DescriComuna'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosBorradosCodigoCiudad: TStringField
      FieldName = 'CodigoCiudad'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosDescriCiudad: TStringField
      FieldName = 'DescriCiudad'
      FixedChar = True
      Size = 100
    end
    object cdsDomiciliosBorradosDescripcionCiudad: TStringField
      FieldName = 'DescripcionCiudad'
      Size = 50
    end
    object cdsDomiciliosBorradosCodigoCalle: TIntegerField
      FieldName = 'CodigoCalle'
    end
    object cdsDomiciliosBorradosNumero: TStringField
      FieldName = 'Numero'
      FixedChar = True
      Size = 10
    end
    object cdsDomiciliosBorradosPiso: TSmallintField
      FieldName = 'Piso'
    end
    object cdsDomiciliosBorradosDpto: TStringField
      FieldName = 'Dpto'
      Size = 10
    end
    object cdsDomiciliosBorradosCalleDesnormalizada: TStringField
      FieldName = 'CalleDesnormalizada'
      Size = 100
    end
    object cdsDomiciliosBorradosDetalle: TStringField
      FieldName = 'Detalle'
      Size = 50
    end
    object cdsDomiciliosBorradosCodigoPostal: TStringField
      FieldName = 'CodigoPostal'
      FixedChar = True
      Size = 10
    end
    object cdsDomiciliosBorradosDomicilioEntrega: TBooleanField
      FieldName = 'DomicilioEntrega'
    end
    object cdsDomiciliosBorradosPaisCalleRelacionadaUno: TStringField
      FieldName = 'PaisCalleRelacionadaUno'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosRegionCalleRelacionadaUno: TStringField
      FieldName = 'RegionCalleRelacionadaUno'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosComunaCalleRelacionadaUno: TStringField
      FieldName = 'ComunaCalleRelacionadaUno'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosCalleRelacionadaUno: TIntegerField
      FieldName = 'CalleRelacionadaUno'
    end
    object cdsDomiciliosBorradosNumeroCalleRelacionadaUno: TIntegerField
      FieldName = 'NumeroCalleRelacionadaUno'
    end
    object cdsDomiciliosBorradosPaisCalleRelacionadaDos: TStringField
      FieldName = 'PaisCalleRelacionadaDos'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosRegionCalleRelacionadaDos: TStringField
      FieldName = 'RegionCalleRelacionadaDos'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosComunaCalleRelacionadaDos: TStringField
      FieldName = 'ComunaCalleRelacionadaDos'
      FixedChar = True
      Size = 3
    end
    object cdsDomiciliosBorradosCalleRelacionadaDos: TIntegerField
      FieldName = 'CalleRelacionadaDos'
    end
    object cdsDomiciliosBorradosNumeroCalleRelacionadaDos: TIntegerField
      FieldName = 'NumeroCalleRelacionadaDos'
    end
    object cdsDomiciliosBorradosNormalizado: TIntegerField
      FieldName = 'Normalizado'
    end
    object cdsDomiciliosBorradosDireccionCompleta: TStringField
      FieldName = 'DireccionCompleta'
      Size = 200
    end
    object cdsDomiciliosBorradosUbicacionGeografica: TStringField
      FieldName = 'UbicacionGeografica'
      Size = 100
    end
  end
end
