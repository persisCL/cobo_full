object FrameAltaOSMultiple: TFrameAltaOSMultiple
  Left = 0
  Top = 0
  Width = 750
  Height = 526
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Align = alClient
  AutoScroll = False
  Constraints.MinHeight = 510
  Constraints.MinWidth = 750
  TabOrder = 0
  object Label15: TLabel
    Left = 310
    Top = 192
    Width = 19
    Height = 13
    Caption = 'Rol:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 750
    Height = 526
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlBotones: TPanel
      Left = 0
      Top = 486
      Width = 750
      Height = 40
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Panel3: TPanel
        Left = 473
        Top = 0
        Width = 277
        Height = 40
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object btnSiguiente: TDPSButton
          Left = 114
          Top = 8
          Caption = '&Siguiente >>'
          Default = True
          TabOrder = 0
          OnClick = btnSiguienteClick
        end
        object btnAnterior: TDPSButton
          Left = 36
          Top = 8
          Caption = '<< Anterior'
          Enabled = False
          TabOrder = 1
          OnClick = btnAnteriorClick
        end
        object btnCancelar: TDPSButton
          Left = 192
          Top = 8
          Cancel = True
          Caption = '&Cancelar'
          ModalResult = 2
          TabOrder = 2
        end
      end
    end
    object PageControl: TPageControl
      Left = 0
      Top = 0
      Width = 750
      Height = 486
      ActivePage = tab_cuentas
      Align = alClient
      TabOrder = 1
      OnGetImageIndex = PageControlGetImageIndex
      object tab_cuentas: TTabSheet
        Caption = '&Cuentas'
        ImageIndex = 1
        TabVisible = False
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 742
          Height = 476
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object pc_Adhesion: TPageControl
            Left = 0
            Top = 0
            Width = 742
            Height = 476
            ActivePage = ts_DatosCliente
            Align = alClient
            TabIndex = 0
            TabOrder = 0
            object ts_DatosCliente: TTabSheet
              Caption = 'Datos del C&liente'
              object GBDomicilios: TGroupBox
                Left = 7
                Top = 200
                Width = 778
                Height = 105
                Caption = 'Domicilios'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                OnClick = GBDomiciliosClick
                object lblnada: TLabel
                  Left = 15
                  Top = 18
                  Width = 63
                  Height = 13
                  Cursor = crHandPoint
                  Hint = 'Domicilio particular'
                  Caption = 'Particular: '
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                end
                object Label2: TLabel
                  Left = 15
                  Top = 46
                  Width = 60
                  Height = 13
                  Cursor = crHandPoint
                  Hint = 'Domicilio particular'
                  Caption = 'Comercial:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                end
                object LblDomicilioComercial: TLabel
                  Left = 78
                  Top = 46
                  Width = 450
                  Height = 26
                  AutoSize = False
                  Caption = 'Ingrese el domicilio'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsItalic]
                  ParentFont = False
                end
                object LblDomicilioParticular: TLabel
                  Left = 78
                  Top = 18
                  Width = 450
                  Height = 26
                  AutoSize = False
                  Caption = 'Ingrese el domicilio'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsItalic]
                  ParentFont = False
                  WordWrap = True
                end
                object Label1: TLabel
                  Left = 16
                  Top = 82
                  Width = 96
                  Height = 13
                  Caption = 'Domicilio de entrega'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object btnBorrarDomicilioParticular: TSpeedButton
                  Left = 603
                  Top = 19
                  Width = 21
                  Height = 20
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    04000000000000010000120B0000120B00001000000000000000000000000000
                    800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
                    3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
                    33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
                    33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
                    333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
                    03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
                    33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
                    0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
                    3333333337FFF7F3333333333000003333333333377777333333}
                  NumGlyphs = 2
                  OnClick = btnBorrarDomicilioParticularClick
                end
                object btnBorrarDomicilioComercial: TSpeedButton
                  Left = 604
                  Top = 51
                  Width = 21
                  Height = 20
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    04000000000000010000120B0000120B00001000000000000000000000000000
                    800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
                    3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
                    33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
                    33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
                    333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
                    03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
                    33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
                    0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
                    3333333337FFF7F3333333333000003333333333377777333333}
                  NumGlyphs = 2
                  OnClick = btnBorrarDomicilioParticularClick
                end
                object BtnDomicilioComercialAccion: TDPSButton
                  Left = 538
                  Top = 47
                  Width = 61
                  Hint = 'Domicilio comercial'
                  Caption = 'Alta'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  OnClick = BtnDomicilioComercialAccionClick
                end
                object BtnDomicilioParticularAccion: TDPSButton
                  Left = 537
                  Top = 16
                  Width = 61
                  Hint = 'Domicilio particular'
                  Caption = 'Alta'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  OnClick = BtnDomicilioParticularAccionClick
                end
                object RGDomicilioEntrega: TRadioGroup
                  Left = 120
                  Top = 69
                  Width = 206
                  Height = 32
                  Columns = 2
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  Items.Strings = (
                    '&Particular'
                    '&Comercial')
                  ParentFont = False
                  TabOrder = 2
                end
              end
              object GBMediosComunicacion: TGroupBox
                Left = 7
                Top = 308
                Width = 778
                Height = 117
                Caption = 'Medios de comunicaci'#243'n'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                object Label35: TLabel
                  Left = 10
                  Top = 79
                  Width = 28
                  Height = 13
                  Caption = 'Email:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object txtEmailParticular: TEdit
                  Left = 119
                  Top = 72
                  Width = 232
                  Height = 21
                  Hint = 'direcci'#243'n de e-mail'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
                inline FMTelefonoPrincipal: TFrameTelefono
                  Left = 5
                  Top = 16
                  Width = 701
                  Height = 28
                  TabOrder = 0
                end
                inline FMTelefonoAlternativo: TFrameTelefono
                  Left = 5
                  Top = 42
                  Width = 701
                  Height = 26
                  TabOrder = 1
                  inherited lblTelefono: TLabel
                    Font.Color = clWindowText
                  end
                  inherited txtTelefono: TEdit
                    Color = clWindow
                  end
                  inherited cbCodigosArea: TComboBox
                    Color = clWindow
                  end
                end
              end
              inline FMPersona: TFramePersona
                Left = 6
                Top = 5
                Width = 803
                Height = 196
                TabOrder = 2
              end
            end
            object ts_Vehiculos: TTabSheet
              Caption = 'Veh'#237'culos'
              ImageIndex = 1
              object pnlVehiculos: TPanel
                Left = 0
                Top = 0
                Width = 734
                Height = 448
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object GBDetalleVehiculo: TGroupBox
                  Left = 4
                  Top = 260
                  Width = 657
                  Height = 178
                  Caption = 'Datos del veh'#237'culo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  object Label9: TLabel
                    Left = 8
                    Top = 53
                    Width = 33
                    Height = 13
                    Caption = 'Roles'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object Label10: TLabel
                    Left = 16
                    Top = 16
                    Width = 43
                    Height = 13
                    Caption = 'Patente: '
                  end
                  object lblPatente: TLabel
                    Left = 64
                    Top = 16
                    Width = 89
                    Height = 13
                    AutoSize = False
                  end
                  object Label11: TLabel
                    Left = 160
                    Top = 16
                    Width = 36
                    Height = 13
                    Caption = 'Marca: '
                  end
                  object lblMarca: TLabel
                    Left = 200
                    Top = 16
                    Width = 145
                    Height = 13
                    AutoSize = False
                  end
                  object Label13: TLabel
                    Left = 360
                    Top = 16
                    Width = 41
                    Height = 13
                    Caption = 'Modelo: '
                  end
                  object lblModelo: TLabel
                    Left = 408
                    Top = 16
                    Width = 238
                    Height = 13
                    AutoSize = False
                  end
                  object Label14: TLabel
                    Left = 16
                    Top = 32
                    Width = 25
                    Height = 13
                    Caption = 'A'#241'o: '
                  end
                  object lblAnio: TLabel
                    Left = 64
                    Top = 32
                    Width = 89
                    Height = 13
                    AutoSize = False
                  end
                  object label100: TLabel
                    Left = 160
                    Top = 32
                    Width = 30
                    Height = 13
                    Caption = 'Color: '
                  end
                  object lblColor: TLabel
                    Left = 200
                    Top = 32
                    Width = 145
                    Height = 13
                    AutoSize = False
                  end
                  object btnEditarRolVehiculo: TDPSButton
                    Left = 491
                    Top = 141
                    Caption = 'E&ditar'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                  object btnEliminarRolVehiculo: TDPSButton
                    Left = 568
                    Top = 141
                    Caption = 'Eli&minar'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 2
                  end
                  object btnIngresarRolVehiculo: TDPSButton
                    Left = 414
                    Top = 141
                    Caption = 'A&gregar'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                  end
                  object dblRolesCuenta: TDBListEx
                    Left = 8
                    Top = 66
                    Width = 640
                    Height = 69
                    BorderStyle = bsSingle
                    Columns = <
                      item
                        Alignment = taLeftJustify
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        Width = 0
                        Header.Caption = 'Rol'
                        Header.Font.Charset = DEFAULT_CHARSET
                        Header.Font.Color = clWindowText
                        Header.Font.Height = -11
                        Header.Font.Name = 'Tahoma'
                        Header.Font.Style = []
                        IsLink = False
                        FieldName = 'DescripcionRol'
                      end
                      item
                        Alignment = taLeftJustify
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        Width = 0
                        Header.Caption = 'Documento'
                        Header.Font.Charset = DEFAULT_CHARSET
                        Header.Font.Color = clWindowText
                        Header.Font.Height = -11
                        Header.Font.Name = 'Tahoma'
                        Header.Font.Style = []
                        IsLink = False
                        FieldName = 'NumeroDocumento'
                      end
                      item
                        Alignment = taLeftJustify
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        Width = 0
                        Header.Caption = 'Persona'
                        Header.Font.Charset = DEFAULT_CHARSET
                        Header.Font.Color = clWindowText
                        Header.Font.Height = -11
                        Header.Font.Name = 'Tahoma'
                        Header.Font.Style = []
                        IsLink = False
                        FieldName = 'NombrePersona'
                      end>
                    DataSource = dsOrdenesServicioAdhesionRolesCuentas
                    DragReorder = True
                    ParentColor = False
                    TabOrder = 3
                    TabStop = True
                  end
                end
                object GBVehiculos: TGroupBox
                  Left = 7
                  Top = 1
                  Width = 653
                  Height = 255
                  Caption = 'Veh'#237'culos '
                  TabOrder = 1
                  inline FMVehiculos: TFrameListVehiculos
                    Left = 4
                    Top = 14
                    Width = 637
                    Height = 233
                    TabOrder = 0
                    inherited dblVehiculos: TDBListEx
                      Width = 637
                      Height = 184
                      Columns = <
                        item
                          Alignment = taLeftJustify
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -11
                          Font.Name = 'MS Sans Serif'
                          Font.Style = []
                          Width = 150
                          Header.Font.Charset = DEFAULT_CHARSET
                          Header.Font.Color = clWindowText
                          Header.Font.Height = -12
                          Header.Font.Name = 'Tahoma'
                          Header.Font.Style = []
                          IsLink = False
                        end>
                    end
                    inherited btnIngresarVehiculo: TDPSButton
                      Left = 406
                      Top = 205
                    end
                    inherited btnEditarVehiculo: TDPSButton
                      Left = 483
                      Top = 205
                    end
                    inherited btnEliminarVehiculo: TDPSButton
                      Left = 560
                      Top = 205
                    end
                  end
                end
              end
            end
            object ts_FormaPago: TTabSheet
              Caption = 'Forma de Pago'
              ImageIndex = 5
              object Label22: TLabel
                Left = 45
                Top = 43
                Width = 60
                Height = 13
                Caption = 'Debitar de'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object lbl_entidad: TLabel
                Left = 45
                Top = 68
                Width = 45
                Height = 13
                Caption = 'Tarjeta:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label23: TLabel
                Left = 45
                Top = 93
                Width = 48
                Height = 13
                Caption = 'N'#250'mero:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Bevel1: TBevel
                Left = 12
                Top = 10
                Width = 321
                Height = 159
              end
              object Bevel2: TBevel
                Left = 12
                Top = 172
                Width = 321
                Height = 50
              end
              object rb_pospagoautomatico: TRadioButton
                Left = 32
                Top = 17
                Width = 181
                Height = 17
                Caption = 'Po&spago con d'#233'bito autom'#225'tico'
                Checked = True
                TabOrder = 0
                TabStop = True
                OnClick = chk_contagClick
              end
              object cb_tipodebito: TComboBox
                Left = 121
                Top = 39
                Width = 181
                Height = 21
                Style = csDropDownList
                Color = 16444382
                ItemHeight = 13
                ItemIndex = 0
                TabOrder = 1
                Text = 'Tarjeta de Cr'#233'dito'
                OnChange = cb_tipodebitoChange
                Items.Strings = (
                  'Tarjeta de Cr'#233'dito'
                  'Cuenta Bancaria')
              end
              object cb_entidaddebito: TComboBox
                Left = 121
                Top = 64
                Width = 181
                Height = 21
                Style = csDropDownList
                Color = 16444382
                ItemHeight = 13
                TabOrder = 2
              end
              object txt_cuentadebito: TEdit
                Left = 121
                Top = 89
                Width = 139
                Height = 21
                Color = 16444382
                MaxLength = 16
                TabOrder = 3
              end
              object rb_pospagomanual: TRadioButton
                Left = 32
                Top = 116
                Width = 161
                Height = 17
                Caption = 'Po&spago con pago manual'
                TabOrder = 4
                OnClick = chk_contagClick
              end
              object rb_prepago: TRadioButton
                Left = 32
                Top = 134
                Width = 113
                Height = 17
                Caption = '&Tarjeta Prepaga'
                TabOrder = 5
                Visible = False
                OnClick = chk_contagClick
              end
              object chk_enviofacturas: TCheckBox
                Left = 25
                Top = 190
                Width = 149
                Height = 17
                Caption = 'Enviar facturas a domicilio'
                TabOrder = 6
              end
            end
            object ts_Contrato: TTabSheet
              Caption = 'Datos del Contrato'
              ImageIndex = 5
              DesignSize = (
                734
                448)
              object GBDomicilioEntrega: TGroupBox
                Left = 7
                Top = 215
                Width = 718
                Height = 88
                Caption = 'Domicilio de entrega de la documentaci'#243'n del contrato'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                DesignSize = (
                  718
                  88)
                object Label6: TLabel
                  Left = 92
                  Top = 57
                  Width = 450
                  Height = 17
                  AutoSize = False
                  Caption = 'Ingrese el domicilio'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsItalic]
                  ParentFont = False
                end
                object btnBorrarDomicilioDocumentacion: TSpeedButton
                  Left = 694
                  Top = 56
                  Width = 19
                  Height = 23
                  Hint = 'Limpiar domicilio'
                  Anchors = [akTop, akRight]
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    04000000000000010000120B0000120B00001000000000000000000000000000
                    800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
                    3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
                    33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
                    33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
                    333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
                    03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
                    33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
                    0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
                    3333333337FFF7F3333333333000003333333333377777333333}
                  NumGlyphs = 2
                  OnClick = btnBorrarDomicilioParticularClick
                end
                object RBDomicilioEntregaParticular: TRadioButton
                  Left = 13
                  Top = 16
                  Width = 73
                  Height = 17
                  Caption = 'Particular'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                end
                object RBDomicilioEntregaComercial: TRadioButton
                  Left = 13
                  Top = 36
                  Width = 73
                  Height = 17
                  Caption = 'Comercial'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                end
                object RBDomicilioEntregaOtros: TRadioButton
                  Left = 14
                  Top = 57
                  Width = 73
                  Height = 18
                  Caption = 'Otros'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
                object DPSButton4: TDPSButton
                  Left = 615
                  Top = 54
                  Height = 26
                  Hint = 'Domicilio de entrega de la documentaci'#243'n'
                  Anchors = [akTop, akRight]
                  Caption = 'Alta'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 3
                  OnClick = BtnDomicilioParticularAccionClick
                end
              end
              object DPSButton1: TDPSButton
                Left = 4229
                Top = 66
                Anchors = [akTop, akRight]
                Caption = 'E&liminar'
                TabOrder = 0
              end
              object DPSButton2: TDPSButton
                Left = 4229
                Top = 38
                Anchors = [akTop, akRight]
                Caption = '&Editar'
                TabOrder = 1
              end
              object GBRolesContrato: TGroupBox
                Left = 8
                Top = 8
                Width = 715
                Height = 193
                Caption = 'Roles '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 2
                DesignSize = (
                  715
                  193)
                object DPSButton3: TDPSButton
                  Left = 551
                  Top = 110
                  Anchors = [akTop, akRight]
                  Caption = '&Editar'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                end
                object DPSButton5: TDPSButton
                  Left = 627
                  Top = 110
                  Anchors = [akTop, akRight]
                  Caption = 'E&liminar'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                end
                object DBListEx1: TDBListEx
                  Left = 6
                  Top = 16
                  Width = 697
                  Height = 90
                  BorderStyle = bsSingle
                  Columns = <
                    item
                      Alignment = taLeftJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      Width = 0
                      Header.Caption = 'Rol'
                      Header.Font.Charset = DEFAULT_CHARSET
                      Header.Font.Color = clWindowText
                      Header.Font.Height = -11
                      Header.Font.Name = 'Tahoma'
                      Header.Font.Style = []
                      IsLink = False
                      FieldName = 'DescripcionRol'
                    end
                    item
                      Alignment = taLeftJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      Width = 0
                      Header.Caption = 'Documento'
                      Header.Font.Charset = DEFAULT_CHARSET
                      Header.Font.Color = clWindowText
                      Header.Font.Height = -11
                      Header.Font.Name = 'Tahoma'
                      Header.Font.Style = []
                      IsLink = False
                      FieldName = 'NumeroDocumento'
                    end
                    item
                      Alignment = taLeftJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      Width = 0
                      Header.Caption = 'Persona'
                      Header.Font.Charset = DEFAULT_CHARSET
                      Header.Font.Color = clWindowText
                      Header.Font.Height = -11
                      Header.Font.Name = 'Tahoma'
                      Header.Font.Style = []
                      IsLink = False
                      FieldName = 'NombrePersona'
                    end>
                  DataSource = dsOrdenesServicioAdhesionRolesCuentas
                  DragReorder = True
                  ParentColor = False
                  TabOrder = 2
                  TabStop = True
                end
                object DPSButton6: TDPSButton
                  Left = 475
                  Top = 110
                  Anchors = [akTop, akRight]
                  Caption = '&Agregar'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 3
                end
                object GBDetalleRolContrato: TGroupBox
                  Left = 8
                  Top = 136
                  Width = 697
                  Height = 49
                  Caption = 'Detalle del rol'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 4
                  object Label7: TLabel
                    Left = 14
                    Top = 24
                    Width = 19
                    Height = 13
                    Caption = 'Rol:'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                  end
                  object Label16: TLabel
                    Left = 256
                    Top = 24
                    Width = 42
                    Height = 13
                    Caption = 'Persona:'
                  end
                  object cb_Roles: TComboBox
                    Left = 40
                    Top = 17
                    Width = 205
                    Height = 24
                    Style = csDropDownList
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ItemHeight = 16
                    ParentFont = False
                    TabOrder = 0
                  end
                  object ComboBox1: TComboBox
                    Left = 304
                    Top = 17
                    Width = 313
                    Height = 24
                    Style = csDropDownList
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ItemHeight = 16
                    ParentFont = False
                    TabOrder = 1
                  end
                  object BtnAceptar: TBitBtn
                    Left = 621
                    Top = 16
                    Width = 30
                    Height = 25
                    Default = True
                    TabOrder = 2
                    Glyph.Data = {
                      DE010000424DDE01000000000000760000002800000024000000120000000100
                      0400000000006801000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                      3333333333333333333333330000333333333333333333333333F33333333333
                      00003333344333333333333333388F3333333333000033334224333333333333
                      338338F3333333330000333422224333333333333833338F3333333300003342
                      222224333333333383333338F3333333000034222A22224333333338F338F333
                      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
                      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
                      0000333333333A222433333333333338F338F33300003333333333A222433333
                      333333338F338F33000033333333333A222433333333333338F338F300003333
                      33333333A222433333333333338F338F00003333333333333A22433333333333
                      3338F38F000033333333333333A223333333333333338F830000333333333333
                      333A333333333333333338330000333333333333333333333333333333333333
                      0000}
                    NumGlyphs = 2
                  end
                  object BitBtn2: TBitBtn
                    Left = 656
                    Top = 16
                    Width = 30
                    Height = 25
                    Cancel = True
                    TabOrder = 3
                    Glyph.Data = {
                      DE010000424DDE01000000000000760000002800000024000000120000000100
                      0400000000006801000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                      333333333333333333333333000033338833333333333333333F333333333333
                      0000333911833333983333333388F333333F3333000033391118333911833333
                      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
                      911118111118333338F3338F833338F3000033333911111111833333338F3338
                      3333F8330000333333911111183333333338F333333F83330000333333311111
                      8333333333338F3333383333000033333339111183333333333338F333833333
                      00003333339111118333333333333833338F3333000033333911181118333333
                      33338333338F333300003333911183911183333333383338F338F33300003333
                      9118333911183333338F33838F338F33000033333913333391113333338FF833
                      38F338F300003333333333333919333333388333338FFF830000333333333333
                      3333333333333333333888330000333333333333333333333333333333333333
                      0000}
                    NumGlyphs = 2
                  end
                end
              end
            end
            object ts_Observaciones: TTabSheet
              Caption = 'Observaciones'
              ImageIndex = 5
              object txtObservaciones: TMemo
                Left = 11
                Top = 11
                Width = 670
                Height = 286
                Lines.Strings = (
                  '')
                TabOrder = 0
              end
            end
          end
        end
      end
    end
  end
  object ObtenerOrdenesServicioAdhesionDomicilios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenesServicioAdhesionDomicilios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IndiceDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 119
    Top = 452
  end
  object ObtenerOrdenServicioAdhesionCuentas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOrdenServicioAdhesionCuentas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@IndiceCuentas'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = 1
      end>
    Left = 17
    Top = 495
  end
  object dsCuentas: TDataSource
    DataSet = ObtenerOrdenServicioAdhesionCuentas
    Left = 80
    Top = 494
  end
  object ObtenerOrdenesServicioAdhesionPersonas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenesServicioAdhesionPersonas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 18
    Top = 452
  end
  object ObtenerDatosComunicacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosComunicacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 425
    Top = 450
  end
  object ObtenerOrdenesServicioAdhesionMediosComunicacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOrdenesServicioAdhesionMediosComunicacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndicePersona'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@IndiceMedioComunicacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 216
    Top = 451
  end
  object dsOrdenesServicioAdhesionRoles: TDataSource
    DataSet = QryOrdenesServicioAdhesionRoles
    Left = 378
    Top = 451
  end
  object cdsOrdenesServicioAdhesionPersonas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuenta'
        DataType = ftInteger
      end
      item
        Name = 'CodigoVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'TipoCliente'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionTipoCliente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PlanComercial'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionPlanComercial'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Largo'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Ancho'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Alto'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'LargoAdicionalVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'LargoTotal'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Activo'
        Attributes = [faReadonly]
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 49
    Top = 452
    object IntegerField12: TIntegerField
      FieldName = 'IndicePersona'
    end
    object IntegerField10: TIntegerField
      FieldName = 'CodigoPersona'
    end
  end
  object dsObtenerOrdenesServicioAdhesionPersonas: TDataSource
    DataSet = ObtenerOrdenesServicioAdhesionPersonas
    Left = 80
    Top = 452
  end
  object cdsOrdenesServicioAdhesionCuentas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuenta'
        DataType = ftInteger
      end
      item
        Name = 'CodigoVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'TipoCliente'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionTipoCliente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PlanComercial'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionPlanComercial'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Largo'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Ancho'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Alto'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'LargoAdicionalVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'LargoTotal'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Activo'
        Attributes = [faReadonly]
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 49
    Top = 495
    object IntegerField14: TIntegerField
      FieldName = 'IndicePersona'
    end
    object IntegerField15: TIntegerField
      FieldName = 'CodigoPersona'
    end
  end
  object cdsOrdenesServicioAdhesionDomicilios: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuenta'
        DataType = ftInteger
      end
      item
        Name = 'CodigoVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'TipoCliente'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionTipoCliente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PlanComercial'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionPlanComercial'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Largo'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Ancho'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Alto'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'LargoAdicionalVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'LargoTotal'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Activo'
        Attributes = [faReadonly]
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 148
    Top = 452
    object IntegerField16: TIntegerField
      FieldName = 'IndicePersona'
    end
    object IntegerField17: TIntegerField
      FieldName = 'CodigoPersona'
    end
  end
  object dsOrdenesServicioAdhesionDomicilios: TDataSource
    DataSet = ObtenerOrdenesServicioAdhesionPersonas
    Left = 179
    Top = 452
  end
  object cdsOrdenesServicioAdhesionMediosComunicacion: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuenta'
        DataType = ftInteger
      end
      item
        Name = 'CodigoVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'TipoCliente'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionTipoCliente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PlanComercial'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionPlanComercial'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Largo'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Ancho'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Alto'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'LargoAdicionalVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'LargoTotal'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Activo'
        Attributes = [faReadonly]
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 245
    Top = 451
    object IntegerField18: TIntegerField
      FieldName = 'IndicePersona'
    end
    object IntegerField19: TIntegerField
      FieldName = 'CodigoPersona'
    end
  end
  object dsOrdenesServicioAdhesionMediosComunicacion: TDataSource
    DataSet = ObtenerOrdenesServicioAdhesionPersonas
    Left = 276
    Top = 451
  end
  object cdsOrdenesServicioAdhesionRolesCuentas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuenta'
        DataType = ftInteger
      end
      item
        Name = 'CodigoVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'TipoCliente'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionTipoCliente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PlanComercial'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionPlanComercial'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Largo'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Ancho'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Alto'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'LargoAdicionalVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'LargoTotal'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Activo'
        Attributes = [faReadonly]
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 148
    Top = 495
    object IntegerField20: TIntegerField
      FieldName = 'IndicePersona'
    end
    object IntegerField21: TIntegerField
      FieldName = 'CodigoPersona'
    end
  end
  object dsOrdenesServicioAdhesionRolesCuentas: TDataSource
    DataSet = qryOrdenesServicioAdhesionRolesCuentas
    Left = 179
    Top = 494
  end
  object cdsOrdenesServicioAdhesionRoles: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuenta'
        DataType = ftInteger
      end
      item
        Name = 'CodigoVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'TipoCliente'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionTipoCliente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PlanComercial'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionPlanComercial'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Largo'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Ancho'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Alto'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'LargoAdicionalVehiculo'
        DataType = ftInteger
      end
      item
        Name = 'LargoTotal'
        Attributes = [faReadonly]
        DataType = ftInteger
      end
      item
        Name = 'Activo'
        Attributes = [faReadonly]
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 349
    Top = 451
    object IntegerField22: TIntegerField
      FieldName = 'IndicePersona'
    end
    object IntegerField23: TIntegerField
      FieldName = 'CodigoPersona'
    end
  end
  object QryOrdenesServicioAdhesionRoles: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'SELECT '#9'OSA.CodigoOrdenServicio, OSA.IndicePersona, '
      #9'OSA.CodigoRol, R.Descripcion as DescripcionRol,'
      
        #9'dbo.ObtenerApellidoNombrePersonaAdhesion(CodigoOrdenServicio, O' +
        'SA.IndicePersona) as NombrePersona,'
      
        #9'dbo.ObtenerDocumentoPersonaAdhesion(CodigoOrdenServicio, OSA.In' +
        'dicePersona) as NumeroDocumento'
      'FROM '#9'OrdenesServicioAdhesionRoles OSA, Roles R'
      'WHERE '#9'CodigoOrdenServicio = :@CodigoOrdenServicio'
      '  AND '#9'OSA.CodigoRol = R.CodigoRol')
    Left = 320
    Top = 450
  end
  object qryOrdenesServicioAdhesionRolesCuentas: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'SELECT '#9'OSA.CodigoOrdenServicio, OSA.IndiceCuentas,'
      #9'OSA.IndicePersona, '
      #9'OSA.CodigoRol, R.Descripcion as DescripcionRol,'
      
        #9'dbo.ObtenerApellidoNombrePersonaAdhesion(CodigoOrdenServicio, O' +
        'SA.IndicePersona) as NombrePersona,'
      
        #9'dbo.ObtenerDocumentoPersonaAdhesion(CodigoOrdenServicio, OSA.In' +
        'dicePersona) as NumeroDocumento'
      'FROM '#9'OrdenesServicioAdhesionRolesCuentas OSA, Roles R'
      'WHERE '#9'CodigoOrdenServicio = :@CodigoOrdenServicio'
      '  AND '#9'OSA.CodigoRol = R.CodigoRol')
    Left = 120
    Top = 495
  end
end
