object FormMontoApertura: TFormMontoApertura
  Left = 296
  Top = 209
  BorderStyle = bsDialog
  Caption = 'Monto Apertura de Caja'
  ClientHeight = 131
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 4
    Top = 4
    Width = 297
    Height = 93
  end
  object Label2: TLabel
    Left = 14
    Top = 56
    Width = 38
    Height = 13
    Caption = 'Importe:'
  end
  object lblMensaje: TLabel
    Left = 14
    Top = 19
    Width = 273
    Height = 13
    Caption = 'Ingrese el importe con el que efect'#250'a la apertura de  Caja.'
  end
  object btn_Aceptar: TDPSButton
    Left = 144
    Top = 101
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btn_AceptarClick
  end
  object btn_Omitir: TDPSButton
    Left = 224
    Top = 101
    Cancel = True
    Caption = '&Omitir'
    TabOrder = 2
    OnClick = btn_OmitirClick
  end
  object txt_Monto: TNumericEdit
    Left = 58
    Top = 52
    Width = 107
    Height = 21
    MaxLength = 10
    TabOrder = 0
    Decimals = 0
  end
end
