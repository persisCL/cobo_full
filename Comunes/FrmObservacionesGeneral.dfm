object FormObservacionesGeneral: TFormObservacionesGeneral
  Left = 204
  Top = 174
  ActiveControl = txtObservaciones
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Ingresar Observaciones'
  ClientHeight = 178
  ClientWidth = 568
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    568
    178)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 8
    Width = 556
    Height = 130
  end
  object lblObservaciones: TLabel
    Left = 16
    Top = 17
    Width = 89
    Height = 13
    Caption = '&Observaciones:'
    FocusControl = txtObservaciones
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object txtObservaciones: TMemo
    Left = 16
    Top = 33
    Width = 537
    Height = 88
    Color = 16444382
    MaxLength = 255
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object btn_Terminar: TButton
    Left = 472
    Top = 148
    Width = 86
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Terminar'
    Default = True
    TabOrder = 2
    OnClick = btn_TerminarClick
  end
  object btnLimpiar: TButton
    Left = 376
    Top = 148
    Width = 86
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Limpiar'
    TabOrder = 1
    OnClick = btnLimpiarClick
  end
  object btnCancelar: TButton
    Left = 280
    Top = 148
    Width = 86
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 3
  end
end
