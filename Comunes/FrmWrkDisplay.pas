{********************************** File Header ********************************
File Name   : FrmWrkDisplay.pas
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Author      :
Date        :
Description :
*******************************************************************************}
unit FrmWrkDisplay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, WorkflowComp, DMConnection, UtilDB;

type
  TFormWorkflowDisplay = class(TForm)
    Viewer: TWorkflowDisplay;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
  public
	{ Public declarations }
	function Inicializa(OrdenServicio, Workflow: Integer): Boolean;
  end;

var
  FormWorkflowDisplay: TFormWorkflowDisplay;

implementation

{$R *.dfm}

{ TFormWorkflowDisplay }

function TFormWorkflowDisplay.Inicializa(OrdenServicio, Workflow: Integer): Boolean;
begin
	Caption := Trim(QueryGetValue(DMConnections.BaseCAC, Format(
	  'SELECT RTRIM(Descripcion) + '' - '' + dbo.ObtenerNombreContacto(CodigoComunicacion) ' +
	  'FROM OrdenesServicio (NOLOCK), Workflows (NOLOCK )' +
	  'WHERE OrdenesServicio.CodigoOrdenServicio = %d ' +
	  'AND Workflows.CodigoWorkflow = OrdenesServicio.CodigoWorkflow',
	  [OrdenServicio])));
	Viewer.ServiceOrder := OrdenServicio;
	Viewer.WorkflowID 	:= Workflow;
	Viewer.Connection 	:= DMConnections.BaseCAC;
	Top := Application.MainForm.Top + Application.MainForm.Height - Height - 176;
	Left := Screen.Width - Width - 4;
	Result := True;
end;

procedure TFormWorkflowDisplay.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

end.
