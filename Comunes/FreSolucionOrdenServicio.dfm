object FrameSolucionOrdenServicio: TFrameSolucionOrdenServicio
  Left = 0
  Top = 0
  Width = 243
  Height = 180
  TabOrder = 0
  object GBEstadoDelReclamo: TGroupBox
    Left = 3
    Top = 0
    Width = 238
    Height = 177
    Caption = 'Estado del Caso'
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 23
      Width = 37
      Height = 13
      Caption = 'Estado:'
    end
    object cbEstado: TVariantComboBox
      Left = 118
      Top = 20
      Width = 108
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnSelect = cbEstadoSelect
      Items = <
        item
          Caption = 'Pendiente'
          Value = 'P'
        end
        item
          Caption = 'An'#225'lisis'
          Value = 'I'
        end
        item
          Caption = 'Respuesta'
          Value = 'E'
        end
        item
          Caption = 'Resuelta'
          Value = 'T'
        end
        item
          Caption = 'Cancelada'
          Value = 'C'
        end>
    end
    object PAdicional: TPanel
      Left = 5
      Top = 42
      Width = 224
      Height = 132
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 4
        Top = 11
        Width = 75
        Height = 13
        Caption = 'F. Compromiso:'
      end
      object Label3: TLabel
        Left = 3
        Top = 40
        Width = 72
        Height = 13
        Caption = 'Area Atencion:'
      end
      object lblResponsable: TLabel
        Left = 3
        Top = 66
        Width = 65
        Height = 13
        Caption = 'Responsable:'
      end
      object lblUsuario: TLabel
        Left = 0
        Top = 96
        Width = 40
        Height = 13
        Caption = 'Usuario:'
      end
      object EFechaCompromiso: TDateEdit
        Left = 114
        Top = 8
        Width = 107
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object EEntidad: TVariantComboBox
        Left = 83
        Top = 36
        Width = 138
        Height = 21
        Style = vcsDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 1
        OnDropDown = EEntidadDropDown
        OnSelect = EEntidadSelect
        Items = <>
      end
      object txtResponsable: TEdit
        Left = 74
        Top = 63
        Width = 146
        Height = 21
        Enabled = False
        TabOrder = 2
      end
      object cbUsuario: TVariantComboBox
        Left = 49
        Top = 90
        Width = 171
        Height = 21
        Style = vcsDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 3
        Items = <>
      end
    end
  end
  object SpObtenerEntidades: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEntidades;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 88
    Top = 232
  end
  object spObtenerAreasAtencionDeCasosTipoOrdenServicios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerAreasAtencionDeCasosTipoOrdenServicios;1'
    Parameters = <
      item
        Name = '@Tipo'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@Area'
        DataType = ftInteger
        Value = Null
      end>
    Left = 160
    Top = 184
  end
end
