unit Legacy;

interface

Uses
	Graphics, DMIGDI, BatchR32;

// Dise�o de Reportes
Function DrawReportPicture(Repo: TDMIReport): Boolean;

implementation

// Dise�o de Reportes
Function DrawReportPicture(Repo: TDMIReport): Boolean;
Var
	PWidth: Single;
	PColor: TColor;
	Pos: TRealPoint;
begin
	With Repo do begin
		PWidth := Pen.Width;
		PColor := Pen.Color;
		Pos := PenPos;
		//
    	Pen.Width := 8;
   	    Pen.Color := clWhite;
    	Line(0.5, 0.20, Ancho, 0.20);
    	Pen.Width := 2.6;
  	    Pen.color := $00419876;  //Color verde costanera norte
//     	Line(0.5, 0.19, Ancho, 0.19);
//    	Line(0.5, 0.25, Ancho, 0.25);
    	Line(0.5, 0.28, Ancho, 0.28);
		DrawPicture('ReportLogo.bmp', 0, -0.05, 0.75, 0.35);
		//
		MoveTo(Pos.X, Pos.Y);
		Pen.Width := PWidth;
		Pen.Color := PColor;
	end;
	Result := True;
end;

end.
