object formEditarDomicilio: TformEditarDomicilio
  Left = 168
  Top = 167
  BorderStyle = bsDialog
  Caption = 'Editar Domicilio'
  ClientHeight = 187
  ClientWidth = 824
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_TipoDomicilio: TLabel
    Left = 18
    Top = 8
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo :'
    FocusControl = cbTipoDomicilio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inline FrameDomicilio: TFrameDomicilio
    Left = -2
    Top = 24
    Width = 828
    Height = 103
    HorzScrollBar.Visible = False
    VertScrollBar.Visible = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = -2
    ExplicitTop = 24
    ExplicitWidth = 828
    ExplicitHeight = 103
    inherited Pnl_Arriba: TPanel
      Width = 828
      Height = 49
      ExplicitWidth = 828
      ExplicitHeight = 49
      inherited txt_CodigoPostal: TEdit
        Text = ''
      end
    end
    inherited Pnl_Abajo: TPanel
      Top = 49
      Width = 828
      ExplicitTop = 49
      ExplicitWidth = 828
    end
    inherited Pnl_Medio: TPanel
      Top = 74
      Width = 828
      Visible = True
      ExplicitTop = 74
      ExplicitWidth = 828
    end
  end
  object cbTipoDomicilio: TComboBox
    Left = 59
    Top = 4
    Width = 197
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 1
  end
  object pnlInferior: TPanel
    Left = 0
    Top = 147
    Width = 824
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 2
    DesignSize = (
      824
      40)
    object BtnAceptar: TButton
      Left = 662
      Top = 8
      Width = 75
      Height = 26
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtnAceptarClick
    end
    object BtnCancelar: TButton
      Left = 740
      Top = 8
      Width = 75
      Height = 26
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = BtnCancelarClick
    end
  end
  object chkDomicilioEntrega: TCheckBox
    Left = 8
    Top = 126
    Width = 128
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Domicilio de Entrega:'
    TabOrder = 3
  end
end
