object FormPropTarea: TFormPropTarea
  Left = 353
  Top = 206
  BorderStyle = bsDialog
  Caption = 'Propiedades de la tarea'
  ClientHeight = 244
  ClientWidth = 435
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 56
    Height = 13
    Caption = 'Descripci'#243'n'
  end
  object Label2: TLabel
    Left = 16
    Top = 54
    Width = 25
    Height = 13
    Caption = #193'rea:'
  end
  object Label3: TLabel
    Left = 16
    Top = 74
    Width = 73
    Height = 31
    AutoSize = False
    Caption = 'Duraci'#243'n (m'#225'x, horas):'
    WordWrap = True
  end
  object Label4: TLabel
    Left = 20
    Top = 180
    Width = 47
    Height = 13
    Caption = 'Procesos:'
  end
  object Edit1: TEdit
    Left = 96
    Top = 20
    Width = 329
    Height = 21
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 112
    Width = 409
    Height = 57
    Caption = ' Tipo de Tarea '
    TabOrder = 3
    object RadioButton1: TRadioButton
      Left = 24
      Top = 24
      Width = 73
      Height = 17
      Caption = '&Normal'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object RadioButton2: TRadioButton
      Left = 144
      Top = 24
      Width = 73
      Height = 17
      Caption = '&Opcional'
      TabOrder = 1
    end
    object RadioButton3: TRadioButton
      Left = 264
      Top = 24
      Width = 89
      Height = 17
      Caption = 'De &resultado'
      TabOrder = 2
    end
  end
  object cb_Area: TComboBox
    Left = 96
    Top = 48
    Width = 289
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object NumericEdit1: TNumericEdit
    Left = 96
    Top = 75
    Width = 49
    Height = 21
    MaxLength = 6
    TabOrder = 2
    Decimals = 2
  end
  object cb_TiposProcesos: TComboBox
    Left = 96
    Top = 176
    Width = 288
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
  end
  object Button1: TDPSButton
    Left = 264
    Top = 211
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object Button2: TDPSButton
    Left = 352
    Top = 211
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 6
  end
  object Areas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM Areas')
    Left = 12
    Top = 208
  end
end
