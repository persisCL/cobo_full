object frmOCR: TfrmOCR
  Left = 240
  Top = 232
  BorderStyle = bsDialog
  Caption = 'Umbral de Confiabilidad OCR'
  ClientHeight = 154
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 425
    Height = 105
  end
  object Label1: TLabel
    Left = 104
    Top = 52
    Width = 128
    Height = 13
    Caption = 'Umbral de Confiabilidad (%)'
  end
  object btn_Aceptar: TButton
    Left = 279
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    Default = True
    TabOrder = 0
    OnClick = btn_AceptarClick
  end
  object Button2: TButton
    Left = 359
    Top = 120
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cerrar'
    TabOrder = 1
    OnClick = Button2Click
  end
  object neConf: TNumericEdit
    Left = 242
    Top = 48
    Width = 39
    Height = 21
    TabOrder = 2
    Decimals = 0
  end
end
