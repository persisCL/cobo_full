object FormPuntosVenta: TFormPuntosVenta
  Left = 105
  Top = 160
  Width = 769
  Height = 542
  Caption = 'Mantenimiento de Puntos de Venta'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 761
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object alPuntoVenta: TAbmList
    Left = 0
    Top = 74
    Width = 761
    Height = 167
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'63'#0'C'#243'digo  '
      #0'233'#0'Punto de Venta     '
      #0'111'#0'Direcci'#243'n IP   '
      #0'68'#0'Tel'#233'fono      ')
    HScrollBar = True
    Table = PuntosVenta
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alPuntoVentaClick
    OnProcess = alPuntoVentaProcess
    OnDrawItem = alPuntoVentaDrawItem
    OnRefresh = alPuntoVentaRefresh
    OnInsert = alPuntoVentaInsert
    OnDelete = alPuntoVentaDelete
    OnEdit = alPuntoVentaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object gbPuntoVenta: TPanel
    Left = 0
    Top = 241
    Width = 761
    Height = 228
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 24
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 76
      Width = 45
      Height = 13
      Caption = 'Tel'#233'fono:'
    end
    object Label4: TLabel
      Left = 16
      Top = 102
      Width = 61
      Height = 13
      Caption = 'Direcci'#243'n IP:'
    end
    object Label5: TLabel
      Left = 16
      Top = 52
      Width = 139
      Height = 13
      Caption = 'Tipo de Lugar de Venta:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 126
      Width = 33
      Height = 13
      Caption = 'Activo:'
    end
    object txtDescripcion: TEdit
      Left = 160
      Top = 20
      Width = 317
      Height = 21
      Color = 16444382
      MaxLength = 30
      TabOrder = 0
    end
    object txt_Telefono: TEdit
      Left = 160
      Top = 72
      Width = 121
      Height = 21
      MaxLength = 20
      TabOrder = 2
    end
    object txt_DireccionIP: TEdit
      Left = 160
      Top = 98
      Width = 121
      Height = 21
      MaxLength = 20
      TabOrder = 3
    end
    object cbTipoLugarVenta: TComboBox
      Left = 160
      Top = 46
      Width = 220
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
    end
    object chkActivo: TCheckBox
      Left = 161
      Top = 126
      Width = 32
      Height = 17
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 469
    Width = 761
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 564
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 31
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object pnlPuntosVenta: TPanel
    Left = 0
    Top = 33
    Width = 761
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object Label8: TLabel
      Left = 16
      Top = 12
      Width = 94
      Height = 13
      Caption = 'Operador Log'#237'stico:'
    end
    object Label7: TLabel
      Left = 402
      Top = 12
      Width = 76
      Height = 13
      Caption = 'Lugar de Venta:'
    end
    object cbOperadorLogistico: TComboBox
      Left = 117
      Top = 9
      Width = 260
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbOperadorLogisticoChange
    end
    object cbLugarDeVenta: TComboBox
      Left = 485
      Top = 9
      Width = 260
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbLugarDeVentaChange
    end
  end
  object ActualizarDatosPuntoVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosPuntoVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Telefono'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@DireccionIP'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoTipoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 412
    Top = 96
  end
  object btLugarDeVenta: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryLugaresDeVentaPorOperador
    Left = 491
    Top = 314
  end
  object qryLugaresDeVentaPorOperador: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'Operador'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      'NombreDeFantasia,'
      'CodigoLugarDeVenta'
      'FROM'
      'VW_LugaresDeVenta'
      '--WHERE'
      '--CodigoOperadorLogistico = :Operador')
    Left = 544
    Top = 96
  end
  object PuntosVenta: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'VW_PuntosVenta'
    Left = 216
    Top = 116
  end
  object EliminarPuntoVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarPuntoVenta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 416
    Top = 148
  end
end
