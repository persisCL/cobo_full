unit ABMTramos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls;

type
  TFormTramos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    Tramos: TADOTable;
    Label15: TLabel;
    txt_CodigoTramo: TNumericEdit;
    Label3: TLabel;
    cb_SectoresPorEje: TComboBox;
    Label4: TLabel;
    txt_Longitud: TNumericEdit;
    Label5: TLabel;
    qry_Sectores: TADOQuery;
    qry_MaxTramo: TADOQuery;
    cbConcesionarias: TComboBox;
    Label2: TLabel;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cbConcesionariasChange(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
    function PuedeGrabar: boolean;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormTramos: TFormTramos;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Tramo?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Tramo porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Tramo';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Tramo.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Tramo';
    MSG_CONCESIONARIAS      = 'Debe indicar la concesionaria.';
    MSG_SECTORESPOREJE      = 'Debe indicar el sector.';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n.';
    MSG_LONGITUD            = 'Debe completar la longitud.';

{$R *.DFM}

function TFormTramos.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;

	if not OpenTables([Tramos, qry_Sectores]) then exit;
    CargarConcesionarias(DMConnections.BaseCAC, cbConcesionarias, 0);
    cbConcesionarias.ItemIndex := 0;
    CargarSectoresConcesionaria(DMConnections.BaseCAC, cb_SectoresPorEje,
                                Ival(StrRight(cbConcesionarias.Text, 20)));

    (*
    while not qry_Sectores.Eof do begin
		cb_SectoresPorEje.Items.Add(
			PadR(qry_Sectores.FieldByName('Descripcion').AsString, 200, ' ') +
            Istr(qry_Sectores.FieldByName('Sector').AsInteger, 10));
    	qry_Sectores.Next;
    end;
    cb_SectoresPorEje.ItemIndex := 0;
	qry_Sectores.Close;
    *)

    Notebook.PageIndex := 0;
    DBList1.Reload;
   	Result := True;
end;

procedure TFormTramos.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormTramos.DBList1Click(Sender: TObject);
var
	i: integer;
begin
	with (Sender AS TDbList).Table do begin
	    txt_CodigoTramo.Value	:= Tramos.FieldByName('CodigoTramo').AsInteger;

        cbConcesionarias.ItemIndex := -1;
        for i := 0 to cbConcesionarias.Items.Count - 1 do begin
        	if Ival(StrRight(cbConcesionarias.Items[i], 20)) =  FieldByName('CodigoConcesionaria').AsInteger then begin
            	cbConcesionarias.ItemIndex := i;
            	Break;
            end;
        end;
        cb_SectoresPorEje.Clear;
//        cb_SectoresPorEje.ItemIndex := -1;
        CargarSectoresConcesionaria(DMConnections.BaseCAC, cb_SectoresPorEje,
                            FieldByName('CodigoConcesionaria').AsInteger,
                            FieldByName('Sector').AsInteger);

        (*for i := 0 to cb_SectoresPorEje.Items.Count - 1 do begin
        	if Ival(Copy(cb_SectoresPorEje.Items[i], 201, 10)) =  FieldByName('Sector').AsInteger then begin
            	cb_SectoresPorEje.ItemIndex := i;
            	Break;
            end;
        end;*)

		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
        txt_Longitud.Value		:= FieldByName('Longitud').AsFloat;
	end;
end;

procedure TFormTramos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
//var
//	i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);

        //Concesionaria
        TextOut(Cols[0], Rect.Top,
          Trim(QueryGetValue(Tramos.Connection,
            'SELECT Descripcion FROM Concesionarias WHERE CodigoConcesionaria = '
            + FieldByName('CodigoConcesionaria').AsString)));
		TextOut(Cols[1], Rect.Top, Tabla.FieldbyName('CodigoTramo').AsString);
		TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('Descripcion').AsString);
		TextOut(Cols[3], Rect.Top,
          Trim(QueryGetValue(Tramos.Connection,
            ' SELECT Descripcion FROM Sectores WHERE CodigoConcesionaria = ' +
                FieldbyName('CodigoConcesionaria').AsString +
                ' AND Sector = '+ FieldByName('Sector').AsString)));
        TextOut(Cols[4], Rect.Top, Rstr(Tabla.FieldbyName('Longitud').AsFloat, 8, 2));
	end;
end;

procedure TFormTramos.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    txt_CodigoTramo.Enabled := False;
    cb_SectoresPorEje.setFocus;
end;

procedure TFormTramos.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormTramos.Limpiar_Campos();
begin
	txt_codigoTramo.Clear;
	txt_Descripcion.Clear;
    txt_Longitud.Clear;
    cb_SectoresPorEje.ItemIndex := 0;
end;

procedure TFormTramos.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTramos.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormTramos.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoTramo.Enabled := False;
	cb_SectoresPorEje.SetFocus;
end;

procedure TFormTramos.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
    if PuedeGrabar then begin
        With DbList1.Table do begin
            Try
                if DbList1.Estado = Alta then begin
                    Append;
                    qry_MaxTramo.Open;
                    txt_CodigoTramo.Value :=  qry_MaxTramo.FieldByNAme('CodigoTramo').AsInteger + 1;
                    qry_MaxTramo.Close;
                end else begin
                    Edit;
                end;
                FieldByName('CodigoConcesionaria').AsInteger := Ival(StrRight(cbConcesionarias.Text, 20));
                FieldByName('CodigoTramo').AsInteger 	:= Trunc(txt_CodigoTramo.Value);
                FieldByName('Sector').AsInteger			:= Ival(StrRight(cb_SectoresPorEje.Text, 20));
                FieldByName('Longitud').AsFloat		 	:= txt_Longitud.Value;
                FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
                Post;
            except
                On E: EDataBaseError do begin
                    Cancel;
                    MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
        Volver_Campos;
    end;
	Screen.Cursor := crDefault;
end;

procedure TFormTramos.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormTramos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTramos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormTramos.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
    txt_CodigoTramo.Enabled := True;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFormTramos.cbConcesionariasChange(Sender: TObject);
begin
    CargarSectoresConcesionaria(DMConnections.BaseCAC, cb_SectoresPorEje,
                                Ival(StrRight(cbConcesionarias.Text, 20)));
end;

function TFormTramos.PuedeGrabar: boolean;
begin
    result := ValidateControls([cbConcesionarias,
                            cb_SectoresPorEje,
                            txt_Descripcion,
                            txt_longitud],
                            [trim(cbConcesionarias.text) <> '',
                            (trim(cb_SectoresPorEje.text) <> ''),
                            trim(txt_Descripcion.text) <> '',
                            trim(txt_longitud.text) <> ''],
                            MSG_ACTUALIZAR_CAPTION,
                            [MSG_CONCESIONARIAS,
                             MSG_SECTORESPOREJE,
                             MSG_DESCRIPCION,
                             MSG_LONGITUD]);
end;

end.
