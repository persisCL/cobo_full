unit formInformeCuadratura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, PeaProcs, Util, UtilProc, SelecFechaTransXCategoria, Series,
  ExtCtrls, TeeProcs, Chart, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd, ppClass,
  ppReport, ppCtrls, ppBands, ppVar, ppPrnabl, ppCache, ADODB, TeEngine, UtilRB;

type
  TdmTransXCategoria = class(TForm)
	repoTransitosXCategorias: TADOStoredProc;
    RepoTransXCategoria: TppReport;
	ppDBPipeline1: TppDBPipeline;
	DataSource1: TDataSource;
	ppHeaderBand1: TppHeaderBand;
	ppLine1: TppLine;
	lbl_usuario: TppLabel;
	lbl_titulo: TppLabel;
    DetailBand: TppDetailBand;
	ppFooterBand1: TppFooterBand;
	lbl_pagina: TppSystemVariable;
	ppLabel1: TppLabel;
	RBInterface: TRBInterface;
    Grupo: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppLabel2: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppLabel3: TppLabel;
	ppDBText3: TppDBText;
	Chart: TChart;
	Series1: TPieSeries;
	ppImage1: TppImage;
    ppLine2: TppLine;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLine4: TppLine;
	ppLine5: TppLine;
	ppLine6: TppLine;
	ppLine7: TppLine;
	ppDBText4: TppDBText;
	ppDBText5: TppDBText;
	ppDBText6: TppDBText;
	ppDBText7: TppDBText;
    ppLine3: TppLine;
	procedure GrupoGetBreakValue(Sender: TObject; var aBreakValue: String);
	procedure ppDBPipeline1RecordPositionChange(Sender: TObject);
	procedure RBInterfaceSelect(Sender: TObject);
    procedure repoTransitosXCategoriasBeforeOpen(DataSet: TDataSet);
    procedure RepoTransXCategoriaBeforePrint(Sender: TObject);
  private
	{ Private declarations }
	FPuntoCobro: Integer;
	FFechaInicial, FFechaFinal: TDateTime;
  public
	{ Public declarations }
	function Inicializa: Boolean;
	function Ejecutar: Boolean;
  end;

var
  dmTransXCategoria: TdmTransXCategoria;

implementation

{$R *.DFM}

function TdmTransXCategoria.Inicializa: Boolean;
begin
	FFechaInicial := Date - 1;
	FFechaFinal := Date - 1;
	FPuntoCobro := 0;
	Result := True;
end;

function TdmTransXCategoria.Ejecutar: Boolean;
begin
	RBInterface.Execute;
	Result := True;
end;

procedure TdmTransXCategoria.GrupoGetBreakValue(Sender: TObject; var aBreakValue: String);
begin
	if ppImage1.Visible then
		aBreakValue :=
		  repoTransitosXCategorias.FieldByName('NumeroPuntoCobro').AsString +
		  repoTransitosXCategorias.FieldByName('Fecha').AsString
	else begin
		aBreakValue := 'X';
	end;
end;

procedure TdmTransXCategoria.ppDBPipeline1RecordPositionChange(Sender: TObject);
resourcestring
    CAPTION_CATEGORIA_1 = 'Categor�a 1';
    CAPTION_CATEGORIA_2 = 'Categor�a 2';
    CAPTION_CATEGORIA_3 = 'Categor�a 3';
Var
	Img: AnsiString;
begin
	Img := GetTempDir + TempFile + '.wmf';
	Series1.Clear;
	Series1.AddPie(
	  repoTransitosXCategorias.FieldByName('Cantidad1').AsInteger, CAPTION_CATEGORIA_1, clGreen);
	Series1.AddPie(
	  repoTransitosXCategorias.FieldByName('Cantidad2').AsInteger, CAPTION_CATEGORIA_2, clYellow);
	Series1.AddPie(
	  repoTransitosXCategorias.FieldByName('Cantidad3').AsInteger, CAPTION_CATEGORIA_3, clRed);
	Chart.SaveToMetafile(Img);
	ppImage1.Picture.LoadFromFile(Img);
	DeleteFile(Img);
end;

procedure TdmTransXCategoria.RBInterfaceSelect(Sender: TObject);
resourcestring
    CAPTION_REPORTE = 'Reporte de Tr�nsitos por Categor�a';
var
	f: TfrmSelFechaTransXCategoria;
begin
	Application.CreateForm(TfrmSelFechaTransXCategoria, f);
	f.Caption := CAPTION_REPORTE;
	if f.Inicializa then begin
		f.FechaInicial := FFechaInicial;
		f.FechaFinal := FFechaFinal;
		f.PuntoCobro := FPuntoCobro;
		f.MostrarGrafico := ppImage1.Visible;
		if (f.ShowModal = mrOk) then begin
			FFechaInicial := f.FechaInicial;
			FFechaFinal := f.FechaFinal;
			FPuntoCobro := f.PuntoCobro;
			ppImage1.Visible := f.MostrarGrafico;
		end;
	end;
	f.Release;
end;

procedure TdmTransXCategoria.repoTransitosXCategoriasBeforeOpen(DataSet: TDataSet);
begin
	With repoTransitosXCategorias.Parameters do begin
		ParamByName('@FechaInicial').Value := FFechaInicial;
		ParamByName('@FechaFinal').Value := FFechaFinal;
		ParamByName('@NumeroPuntoCobro').Value := FPuntoCobro;
	end;
end;

procedure TdmTransXCategoria.RepoTransXCategoriaBeforePrint(Sender: TObject);
begin
	if not ppImage1.Visible then DetailBand.Height := 1.25;
end;

end.


