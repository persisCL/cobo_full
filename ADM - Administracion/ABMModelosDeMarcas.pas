{-----------------------------------------------------------------------------
 File Name: ABMModelosDeMarcas
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ABMModelosDeMarcas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, VariantComboBox, Variants,
  BuscaTab;

type
  TFormModelosDeMarcas = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    Modelos: TADOTable;
    lblModelo: TLabel;
    txt_CodigoModelo: TNumericEdit;
    qry_MaxModelo: TADOQuery;
    Label2: TLabel;
    cb_TipoVehiculo: TVariantComboBox;
    lblMarca: TLabel;
    qry_Marcas: TADOQuery;
    BuscaTablaMarcas: TBuscaTabla;
    cb_marca: TVariantComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    CodigoMarca: integer;
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
	function Inicializa(MDIChild: Boolean; Marca: integer = -1; Descripcion: AnsiString = ''): boolean;
  end;

var
  FormModelosDeMarcas: TFormModelosDeMarcas;

implementation

resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Modelo seleccionado?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar el Modelo seleccionado.';
    MSG_DELETE_CAPTION		= 'Eliminar Modelo';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Modelo seleccionado.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Modelo de la marca';
    MSG_MARCA               = 'Debe ingresar la marca';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';
    MSG_TIPO_VEHICULO       = 'Debe ingresar el tipo de veh�culo';

{$R *.DFM}

function TFormModelosDeMarcas.Inicializa(MDIChild: Boolean; Marca: integer = -1; Descripcion: AnsiString = ''): boolean;
Var
	S: TSize;
begin
    CodigoMarca := -1;
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		//BorderStyle := bsDialog;
		//Width := Screen.Width div 2;
		//Height := Screen.Height div 2;
		CenterForm(Self);
	end;
    Result := False;
    //CargarVehiculosTipos(DMConnections.BaseCAC, cb_TipoVehiculo);
    if (Marca <> -1) then begin
        FormModelosDeMarcas.Caption :=  FormModelosDeMarcas.Caption + ': ' + Descripcion;
        CodigoMarca	:= Marca;
        Modelos.Filter := 'CodigoMarca = ' + IntToStr(Marca);
        cb_marca.Color := txt_CodigoModelo.Color;
        lblMarca.Color := lblModelo.Color;
     end;
	if not OpenTables([Modelos]) then exit;

    CargarMarcasComboVariant(DMConnections.BaseCAC, cb_marca);
    
    CargarVehiculosTipos(DMConnections.BaseCAC, cb_TipoVehiculo);
    Notebook.PageIndex := 0;
    DBList1.Reload;
   	Result := True;
end;

procedure TFormModelosDeMarcas.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormModelosDeMarcas.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
        MarcarItemComboVariant(cb_marca, FieldByName('CodigoMarca').AsInteger);
	    txt_CodigoModelo.Value	:= FieldByName('CodigoModelo').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
        MarcarItemComboVariant(cb_TipoVehiculo, FieldByName('TipoVehiculo').AsInteger);
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: DBList1DrawItem
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: Sender: TDBList; Tabla: TDataSet; Rect: TRect;
              State: TOwnerDrawState; Cols: TColPositions
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormModelosDeMarcas.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var
    DescMarca, DescTipoVehiculo: AnsiString;
begin
    DescTipoVehiculo := '';
    DescMarca := '';
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        DescMarca := QueryGetValue(DMConnections.BaseCAC,
                       'SELECT Descripcion FROM VehiculosMarcas  WITH (NOLOCK) ' +
                       'WHERE CodigoMarca = ' +
                       Tabla.FieldByName('CodigoMarca').AsString);
		TextOut(Cols[0], Rect.Top, DescMarca);
		TextOut(Cols[1], Rect.Top, Istr(Tabla.FieldbyName('CodigoModelo').AsInteger, 10));
		TextOut(Cols[2], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));

        if (Tabla.FieldByName('TipoVehiculo').AsString <> '') then
            DescTipoVehiculo := QueryGetValue(DMConnections.BaseCAC,
                                'SELECT Descripcion FROM VehiculosTipos  WITH (NOLOCK) ' +
                                'WHERE CodigoTipoVehiculo = ' +
                                Tabla.FieldByName('TipoVehiculo').AsString)
        else DescTipoVehiculo := '';
        TextOut(Cols[3], Rect.Top, DescTipoVehiculo);
	end;
end;

procedure TFormModelosDeMarcas.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    cb_marca.Enabled        := false;
    cb_marca.Color          := clBtnFace;
    txt_CodigoModelo.Enabled := False;
   	txt_CodigoModelo.color := clBtnFace;
    txt_Descripcion.setFocus;
end;

procedure TFormModelosDeMarcas.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormModelosDeMarcas.Limpiar_Campos();
begin
    cb_marca.ItemIndex := 0;
	txt_CodigoModelo.Clear;
	txt_Descripcion.Clear;
    cb_TipoVehiculo.ItemIndex := 0;
end;

procedure TFormModelosDeMarcas.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormModelosDeMarcas.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormModelosDeMarcas.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoModelo.Enabled := False;
	txt_CodigoModelo.color := clBtnFace;
    cb_marca.Enabled := true;
	cb_marca.SetFocus;
    cb_TipoVehiculo.ItemIndex := 0;
end;

procedure TFormModelosDeMarcas.BtnAceptarClick(Sender: TObject);
begin

    if not ValidateControls(
        [cb_marca, txt_Descripcion, cb_TipoVehiculo],
        [cb_marca.itemindex > 0,
        Trim(txt_Descripcion.text) <> '',
        cb_TipoVehiculo.itemindex > 0],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_MARCA, MSG_DESCRIPCION, MSG_TIPO_VEHICULO]) then exit;
    Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
        CodigoMarca := cb_marca.value;
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                qry_MaxModelo.Parameters.ParamByName('CodigoMarca').Value := CodigoMarca;
                qry_MaxModelo.Open;
                txt_CodigoModelo.Value := qry_MaxModelo.FieldByNAme('CodigoModelo').AsInteger + 1;
				qry_MaxModelo.Close;
			end else begin
            	Edit;
            end;
            FieldByName('CodigoMarca').AsInteger 	:= CodigoMarca;
			FieldByName('CodigoModelo').AsInteger	:= Trunc(txt_CodigoModelo.Value);
			if Trim(txt_Descripcion.text) = '' then
            	FieldByName('Descripcion').Clear
            else
				FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);

            //El item 0 es "(Sin especificar)"
            if (cb_TipoVehiculo.ItemIndex = 0) then
                FieldByName('TipoVehiculo').Clear
            else
                FieldByName('TipoVehiculo').AsString := iif(cb_TipoVehiculo.ItemIndex = 0, null,
                                                        IntToStr(cb_TipoVehiculo.value));
			Post;
		except
			On E: Exception do begin
                Screen.Cursor := crDefault;
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor := crDefault;
end;

procedure TFormModelosDeMarcas.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormModelosDeMarcas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormModelosDeMarcas.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormModelosDeMarcas.Volver_Campos;
begin
	DbList1.Estado      := Normal;
	DbList1.Enabled     := True;
    DBList1.Reload;
	DbList1.SetFocus;
    cb_marca.Color      := txt_Descripcion.Color;
    cb_marca.Enabled    := true;
    txt_CodigoModelo.Enabled := True;
   	txt_CodigoModelo.color := clWindow;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

end.
