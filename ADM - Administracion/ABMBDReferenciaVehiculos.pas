unit ABMBDReferenciaVehiculos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, ToolWin, ExtCtrls, ImgList, DMConnection,
  Provider, DB, ADODB, DBClient, UtilProc;

type
  TfrmABMBDReferenciaVehiculos = class(TForm)
    ilImagenes: TImageList;
    pnl1: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnEliminar: TToolButton;
    btn2: TToolButton;
    btnBuscar: TToolButton;
    dbgrdReferenciaVehiculos: TDBGrid;
    spTransitosPendientesValidacionSemiAutomatica_SELECT: TADOStoredProc;
    dtstprReferenciaVehiculos: TDataSetProvider;
    cdsReferenciaVehiculos: TClientDataSet;
    dsReferenciaVehiculos: TDataSource;
    spTransitosPendientesValidacionSemiAutomatica_DELETE: TADOStoredProc;
    procedure btnBuscarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FPatente: String;

    function CargarDatos: Boolean;
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  frmABMBDReferenciaVehiculos: TfrmABMBDReferenciaVehiculos;

implementation

{$R *.dfm}

procedure TfrmABMBDReferenciaVehiculos.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_CAPTION = 'Buscar Patente';
    MSG_PROMPT = 'Ingrese una Patente (o parte de una) para filtrar el listado';
var
    FiltroPatente: String;
begin
    FiltroPatente := InputBox(MSG_CAPTION, MSG_PROMPT, FPatente);

    if FiltroPatente <> FPatente then begin
        FPatente := FiltroPatente;
        CargarDatos;
    end;
end;

function TfrmABMBDReferenciaVehiculos.Inicializar: Boolean;
begin
    Result := CargarDatos;
end;

procedure TfrmABMBDReferenciaVehiculos.btnEliminarClick(Sender: TObject);
resourcestring
    MSG_TITLE = 'Baja de registro';
    MSG_CAPTION = 'El registro se ha dado de baja';
    MSG_CONFIRMATION = '�Desea dar de baja el registro de la Base de Datos de Referencia de Veh�culos?';
    MSG_ERROR = 'Ha ocurrido un error al dar de baja el registro';

begin
    if MsgBox(MSG_CONFIRMATION, MSG_TITLE, MB_ICONQUESTION+MB_YESNO) = ID_NO then Exit;    

    try
        try
            spTransitosPendientesValidacionSemiAutomatica_DELETE.Parameters.Refresh;
            spTransitosPendientesValidacionSemiAutomatica_DELETE.Parameters.ParamByName('@Patente').Value := cdsReferenciaVehiculos.FieldByName('Patente').Value;
            spTransitosPendientesValidacionSemiAutomatica_DELETE.Parameters.ParamByName('@IDCategoriaClase').Value := cdsReferenciaVehiculos.FieldByName('IDCategoriaClase').Value;
            spTransitosPendientesValidacionSemiAutomatica_DELETE.Parameters.ParamByName('@CodigoTAG').Value := cdsReferenciaVehiculos.FieldByName('CodigoTAG').Value;
            spTransitosPendientesValidacionSemiAutomatica_DELETE.Parameters.ParamByName('@Categoria').Value := cdsReferenciaVehiculos.FieldByName('Categoria').Value;
            spTransitosPendientesValidacionSemiAutomatica_DELETE.ExecProc;

            if spTransitosPendientesValidacionSemiAutomatica_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spTransitosPendientesValidacionSemiAutomatica_DELETE.Parameters.ParamByName('@ErrorDescription').Value);

            MsgBox(MSG_CAPTION, MSG_TITLE, MB_ICONINFORMATION);
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        spTransitosPendientesValidacionSemiAutomatica_DELETE.Close;
    end;

    CargarDatos;
end;

function TfrmABMBDReferenciaVehiculos.CargarDatos: Boolean;
resourcestring
    MSG_ERROR_CAPTION = 'Ha ocurrido un error al obtener la Base de Datos de Referencia de Veh�culos';
    MSG_ERROR_TITLE = 'Error';
begin
    Result := True;
    Screen.Cursor := crHourGlass;
    try
        try
            spTransitosPendientesValidacionSemiAutomatica_SELECT.Parameters.Refresh;
            spTransitosPendientesValidacionSemiAutomatica_SELECT.Parameters.ParamByName('@Patente').Value := FPatente;
            spTransitosPendientesValidacionSemiAutomatica_SELECT.Parameters.ParamByName('@IncluirIncompletos').Value := False;
            spTransitosPendientesValidacionSemiAutomatica_SELECT.Parameters.ParamByName('@IncluirBajas').Value := False;
            spTransitosPendientesValidacionSemiAutomatica_SELECT.Open;

            if spTransitosPendientesValidacionSemiAutomatica_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spTransitosPendientesValidacionSemiAutomatica_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsReferenciaVehiculos.Data := dtstprReferenciaVehiculos.Data;
            cdsReferenciaVehiculos.First;

            btnEliminar.Enabled := (cdsReferenciaVehiculos.RecordCount > 0);
        except
            on e: Exception do begin
                Result := False;
                btnEliminar.Enabled := False;
                MsgBoxErr(MSG_ERROR_CAPTION, e.Message, MSG_ERROR_TITLE, MB_ICONERROR);
            end;
        end;
    finally
        spTransitosPendientesValidacionSemiAutomatica_SELECT.Close;
        Screen.Cursor := crDefault;
    end;
end;

procedure TfrmABMBDReferenciaVehiculos.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMBDReferenciaVehiculos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
