{-----------------------------------------------------------------------------
 Unit Name: ABMMaestroMensajesComprobantes
 Author:    ndonadio
 Purpose:   Mantener la lista de Mensajes
 History:   Modificaciones realizadas sobre el ABM existente. Ahora se permite
            editar el IDMensaje que ya no es Identity.

Autor       :   CQuezadaI
Fecha       :   17 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se deshabilita el campo neIDMensaje ya que no se permite editar
                el IDMensaje porque es Identity,
                tambien se cambia en el stored InsertarMaestroMensajesComprobante
                Se agrega la obtenci�n del siguiente ID al momento de querer insertar.
-----------------------------------------------------------------------------}


unit ABMMaestroMensajesComprobantes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, AbmModelos, ADODB, DPSControls, Peatypes;


type
  TFormABMMaestroMensajesComprobantes = class(TForm)
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    txt_Texto: TEdit;
    Panel2: TPanel;
    Notebook: TNotebook;
    Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
    Panel3: TPanel;
    CB_IncluirEnGeneral: TCheckBox;
    InsertarMensajesComprobantes: TADOStoredProc;
    BorrarMaestroMensjesComprobante: TADOStoredProc;
    ActualizarMaestroMensajesComprobante: TADOStoredProc;
    ObtenerMaestroMensajesComprobantes: TADOStoredProc;
    neIDMensaje: TNumericEdit;
    Label2: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure AbmToolbar1Close(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure AltaMaestroMensajesComprobantes(IDMensaje: integer; Texto:String;esGeneral:Boolean);
    procedure BajaMaestroMensajesComprobantes(ID:Integer);
    procedure ModificacionMaestroMensajesComprobantes(ID, IDNew:Integer;Texto:String;esGeneral:Boolean);
    procedure Limpiar_Campos();
    procedure Volver_Campos;
  public
    { Public declarations }
    function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  FormABMMaestroMensajesComprobantes: TFormABMMaestroMensajesComprobantes;

implementation

uses DMConnection;

resourcestring
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Mensajes';
    MSG_TEXTO               = 'Debe ingresar el Texto';
    SQL_OBTENER_ID          = 'SELECT MAX(IDMensaje) + 1 FROM MaestroMensajesComprobantes WITH(NOLOCK)';    // SS_1147_CQU_20140714

{$R *.dfm}

function TFormABMMaestroMensajesComprobantes.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([ObtenerMaestroMensajesComprobantes]) then exit;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	Result := True;
end;

procedure TFormABMMaestroMensajesComprobantes.AbmToolbar1Close(Sender: TObject);
begin
    Close;
end;

procedure TFormABMMaestroMensajesComprobantes.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    cb_IncluirEnGeneral.Enabled := True;
    groupb.Enabled     := False;
end;

procedure TFormABMMaestroMensajesComprobantes.BtnCancelarClick(
  Sender: TObject);
begin
    Volver_Campos;
end;

procedure TFormABMMaestroMensajesComprobantes.Limpiar_Campos();
begin
    neIDMensaje.Clear;
	txt_Texto.Clear;
    CB_IncluirEnGeneral.Checked:=False;
end;


procedure TFormABMMaestroMensajesComprobantes.DBList1Insert(
  Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
    DbList1.Estado     := Alta;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    neIDMensaje.Value  := QueryGetValueInt(DMConnections.BaseCAC, SQL_OBTENER_ID);  // SS_1147_CQU_20140714
	txt_Texto.SetFocus;
end;

procedure TFormABMMaestroMensajesComprobantes.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;

    txt_Texto.setFocus;
end;

procedure TFormABMMaestroMensajesComprobantes.DBList1Delete(
  Sender: TObject);

    function ExistenMensajesRelaccionados: Boolean;
    //Devuelve True si existen vehiculos relacionados con el tipo actual
    begin
        Result := StrtoBool(QueryGetValue(DMConnections.BaseCAC,
                                          'select dbo.MaestroMensajesConMensajes(''' +
                                          ObtenerMaestroMensajesComprobantes.FieldbyName('IDMensaje').AsString + ''')'));
    end;

resourcestring
    MSG_ELIMINARMENSAJE       = '�Est� seguro de querer eliminar el Mensaje seleccionado?';
    MSG_ERRORELIMINARMENSAJE  = 'No se pudo eliminar el Mensaje seleccionado';
    CAPTION_ELIMINARMENSAJE   = 'Eliminar Mensajes';
    MSG_ERROR_RELATED_RECORDS = 'porque existen datos relacionados';
var
    primerRg:Boolean;
begin
	Screen.Cursor := crHourGlass;
    try
        If MsgBox(MSG_ELIMINARMENSAJE, CAPTION_ELIMINARMENSAJE, MB_YESNO + MB_ICONQUESTION) =
            IDYES then begin
            try
                primerRg:=ObtenerMaestroMensajesComprobantes.Bof;
                BajaMaestroMensajesComprobantes(ObtenerMaestroMensajesComprobantes.FieldbyName('IDMensaje').Value);
                if primerRg then begin
                    ObtenerMaestroMensajesComprobantes.Close;
                    ObtenerMaestroMensajesComprobantes.Open;
                end
                else
                    ObtenerMaestroMensajesComprobantes.MoveBy(-1);
            Except
                On E: Exception do begin
                    MsgBox(MSG_ERRORELIMINARMENSAJE + CrLf + MSG_ERROR_RELATED_RECORDS, CAPTION_ELIMINARMENSAJE, MB_ICONERROR);
                end;
            end;
            DbList1.Reload;
        end;
        DbList1.Estado     := Normal;
        DbList1.Enabled    := True;
    finally
	    Screen.Cursor := crDefault;
    end;
end;

procedure TFormABMMaestroMensajesComprobantes.DBList1Click(
  Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
        neIDMensaje.ValueInt := FieldByName('IDMensaje').AsInteger;
		txt_Texto.text	:= FieldByName('Texto').AsString;
        CB_IncluirEnGeneral.Checked:=FieldByName('IncluirEnGeneral').AsBoolean;
	end;
end;

procedure TFormABMMaestroMensajesComprobantes.DBList1Refresh(
  Sender: TObject);
begin
	 if DBList1.Empty then begin
     	Limpiar_Campos();
     end;
end;

procedure TFormABMMaestroMensajesComprobantes.DBList1DrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, Trim(FieldbyName('IDMensaje').AsString));
		TextOut(Cols[1], Rect.Top, Trim(FieldbyName('Texto').AsString));
	end;
end;

procedure TFormABMMaestroMensajesComprobantes.AltaMaestroMensajesComprobantes(IDMensaje: integer; Texto:String; esGeneral:Boolean);
resourcestring
    MSG_ERROR   =   'Error al intentar agregar el Mensaje.';
begin
    try
       with InsertarMensajesComprobantes.Parameters do
       begin
            ParamByName('@IDMensaje').Value         := IDMensaje;
            ParamByName('@Texto').Value             := Texto;
            ParamByName('@IncluirEnGeneral').Value  := esGeneral;
       end;
       InsertarMensajesComprobantes.ExecProc;
    except
        on e:exception do begin
            MsgBoxErr(MSG_ERROR,e.Message, caption, MB_ICONERROR);
        end;
    end;
end;

procedure TFormABMMaestroMensajesComprobantes.ModificacionMaestroMensajesComprobantes(ID, IDNew:Integer; Texto:String; esGeneral:Boolean);
resourcestring
    MSG_ERROR   =   'Error al actualizar el Mensaje';
begin
    try
       with ActualizarMaestroMensajesComprobante.Parameters do
       begin
            ParamByName('@Id').Value                := ID;
            ParamByName('@Id_New').Value            := IDNew;
            ParamByName('@Texto').Value             := Texto;
            ParamByName('@IncluirEnGeneral').Value  := esGeneral;
       end;
       ActualizarMaestroMensajesComprobante.ExecProc;
    except
        on e:exception do begin
            MsgBoxErr(MSG_ERROR,e.Message, caption, MB_ICONERROR);
        end;
    end;
end;

procedure TFormABMMaestroMensajesComprobantes.BajaMaestroMensajesComprobantes(ID:Integer);
begin
   BorrarMaestroMensjesComprobante.Parameters.ParamByName('@IdMensaje').Value:=Id;
   BorrarMaestroMensjesComprobante.ExecProc;
end;

procedure TFormABMMaestroMensajesComprobantes.BtnAceptarClick(
  Sender: TObject);
resourcestring
    MSG_UPDATE_ERROR    = 'No se pudieron actualizar los datos del Mensaje seleccionado.';
    MSG_ADD_ERROR       = 'No se puede agregar el Mensaje. Verifique el N�mero de Mensaje: no puede repetirse.';
    CAPTION_ADD_MSG     = 'Agregar Mensaje';
    CAPTION_UPD_MSG     = 'Actualizar Mensaje';
    CAPTION_INVALID     = 'El valor ingresado es incorrecto';
    MSG_INVALID_ID      = 'El N�mero de Mensaje debe ser mayor o igual a 1.';
    MSG_INVALID_TXT     = 'El texto del Mensaje no puede estar en blanco.';
var
    MSG_ERROR: AnsiString;
    CAPTION_ERROR: AnsiString;
begin
    if not ValidateControls([neIDMensaje, txt_Texto],
                [(neIDMensaje.ValueInt > 0),
                (trim(txt_Texto.text) <> '')],
                CAPTION_INVALID,
                [MSG_INVALID_ID,MSG_INVALID_TXT]) then exit;

 	Screen.Cursor := crHourGlass;
    try
        With DbList1.Table do begin
            try
                if DbList1.Estado = Alta then begin
                    MSG_ERROR := MSG_ADD_ERROR;
                    CAPTION_ERROR := CAPTION_ADD_MSG;
                    AltaMaestroMensajesComprobantes(neIDMensaje.ValueInt, Trim(txt_Texto.text),CB_IncluirEnGeneral.Checked);
                end else begin
                    MSG_ERROR := MSG_UPDATE_ERROR;
                    CAPTION_ERROR := CAPTION_UPD_MSG;
                    ModificacionMaestroMensajesComprobantes(FieldByName('IDMensaje').asInteger,neIDMensaje.ValueInt,Trim(txt_Texto.text),CB_IncluirEnGeneral.Checked);
                end;
            except
                On e: Exception do begin
                     MsgBox(MSG_ERROR,CAPTION_ERROR, MB_ICONERROR);
                end;
            end;
        end;
    finally
        Volver_Campos;
        Screen.Cursor 	   := crDefault;
    end;

end;

procedure TFormABMMaestroMensajesComprobantes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:=caFree;
end;

procedure TFormABMMaestroMensajesComprobantes.BtnSalirClick(
  Sender: TObject);
begin
    Close;
end;

end.
