object FABMTransportes: TFABMTransportes
  Left = 93
  Top = 101
  Caption = 'Mantenimiento de Transportistas'
  ClientHeight = 567
  ClientWidth = 809
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 9
    Top = 20
    Width = 43
    Height = 13
    Caption = 'N'#250'mero :'
  end
  object Label3: TLabel
    Left = 9
    Top = 49
    Width = 36
    Height = 13
    Caption = 'Fecha :'
  end
  object GroupB: TPanel
    Left = 0
    Top = 461
    Width = 809
    Height = 67
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Label7: TLabel
      Left = 10
      Top = 11
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 11
      Top = 38
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 452
      Top = 38
      Width = 108
      Height = 13
      Caption = '&Almac'#233'n asociado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 58
      Top = 8
      Width = 105
      Height = 21
      Color = 16444382
      Enabled = False
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 87
      Top = 36
      Width = 340
      Height = 21
      Color = 16444382
      TabOrder = 1
    end
    object cbAlmacenes: TVariantComboBox
      Left = 564
      Top = 36
      Width = 241
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 528
    Width = 809
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 612
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 176
    Top = 2
    Width = 104
    Height = 27
    BevelOuter = bvNone
    TabOrder = 2
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 809
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbar1Close
  end
  object ListaTransportistas: TAbmList
    Left = 0
    Top = 33
    Width = 809
    Height = 428
    TabStop = True
    TabOrder = 4
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'74'#0'C'#243'digo           '
      #0'64'#0'Descripci'#243'n')
    HScrollBar = True
    RefreshTime = 100
    Table = Transportistas
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaTransportistasClick
    OnProcess = ListaTransportistasProcess
    OnDrawItem = ListaTransportistasDrawItem
    OnRefresh = ListaTransportistasRefresh
    OnInsert = ListaTransportistasInsert
    OnDelete = ListaTransportistasDelete
    OnEdit = ListaTransportistasEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object spActualizarTransportistas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTransportista;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@AlmacenAsociado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 588
    Top = 104
  end
  object dsTransportistas: TDataSource
    DataSet = Transportistas
    Left = 470
    Top = 160
  end
  object Transportistas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from MaestroTransportistas WITH (NOLOCK) ')
    Left = 456
    Top = 88
  end
  object EliminarTransportista: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTransportista;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTransportista'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 568
    Top = 200
  end
end
