{-----------------------------------------------------------------------------
 Unit Name: ABMAreasAtencionDeCasos
 Author:  CFU
 Description: ABM de �reas de Atenci�n de Casos
-----------------------------------------------------------------------------}
unit ABMAreasAtencionDeCasos;

interface

uses
  //ABM Areas de Atencion
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants, DBClient, VariantComboBox, AreasAtenciosDeCasos, StrUtils;      //TASK_121_JMA_20170328

type
  TFABMAreasAtencionDeCasos = class(TForm)
    PAbajo: TPanel;
	Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    PanelOpciones: TPanel;
    btnTEditar: TSpeedButton;
    btnTInsertar: TSpeedButton;
    btnTEliminar: TSpeedButton;
    sbtnSalir: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    pgcAtencionCasos: TPageControl;
    tsGeneral: TTabSheet;
    tsUsuarios: TTabSheet;
    tsTiposCasos: TTabSheet;
    GroupB: TPanel;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtCodigo: TNumericEdit;
    txtDescripcion: TEdit;
    txtNombreArea: TEdit;
    txtTelefono: TEdit;
    txtEmail: TEdit;
    dblUsuariosSistema: TDBListEx;
    pnlUsuarios: TPanel;
    dblUsuariosAsignados: TDBListEx;
    btnAgregarUnUsuario: TButton;
    btnAgregarTodosUsuarios: TButton;
    btnQuitarUnUsuario: TButton;
    btnQuitarTodosUsuarios: TButton;
    dblTiposCasosSistema: TDBListEx;
    pnlTiposCasos: TPanel;
    btnAgregarUnTipoCaso: TButton;
    btnAgregarTodosTipoCaso: TButton;
    btnQuitarUnTipoCaso: TButton;
    btnQuitarTodosTipoCaso: TButton;
    dblTiposCasosAsignados: TDBListEx;
    ListaAreasAtencion: TDBListEx;
    cbbResponsable: TVariantComboBox;
    //procedure FormShow(Sender: TObject);                                   //TASK_121_JMA_20170328
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnTEliminarClick(Sender: TObject);
    procedure btnTInsertarClick(Sender: TObject);
    //procedure FormCreate(Sender: TObject);                                  //TASK_121_JMA_20170328
    procedure btnTEditarClick(Sender: TObject);
    procedure AreasAtencionDeCasos_AfterScroll(DataSet: TDataSet);
    procedure btnAgregarUnUsuarioClick(Sender: TObject);
    procedure btnAgregarTodosUsuariosClick(Sender: TObject);
    procedure btnQuitarUnUsuarioClick(Sender: TObject);
    procedure btnQuitarTodosUsuariosClick(Sender: TObject);
    procedure btnAgregarUnTipoCasoClick(Sender: TObject);
    procedure btnAgregarTodosTipoCasoClick(Sender: TObject);
    procedure btnQuitarUnTipoCasoClick(Sender: TObject);
    procedure btnQuitarTodosTipoCasoClick(Sender: TObject);
    procedure pgcAtencionCasosChange(Sender: TObject);
    procedure cbbResponsableChange(Sender: TObject);
  private
	{ Private declarations }
{INICIO: TASK_121_JMA_20170328}
    oAreasAtencion: TAreasAtencionDeCasos;
    oUsuarioAsignados: TAreasAtencionDeCasosUsuarios;
    oTiposCasosAsignados: TAreasAtencionDeCasosTiposCasos;
    Cargando: Boolean;                              
    function CargarUsuariosAsignados(CodigoAreaAtencionDeCasos: Integer): boolean;
    function CargarTiposCasosAsignados(CodigoAreaAtencionDeCasos: Integer): Boolean;
    procedure CargarUsuariosSistema;
    procedure CargarTiposCasosSistema;
    procedure FiltrarUsuariosSistema();
    procedure FiltrarTiposCasosSistama();
    procedure PermitirEdicion(permitir: boolean);
    procedure CargarCbbResponsable();
	procedure CargarAreasAtencionDeCasos;
{TERMINO: TASK_121_JMA_20170328
	ListaTiposDeCasos: TStringList;
}
	procedure LimpiarCampos;
	//Procedure Volver_Campos;                                              //TASK_121_JMA_20170328
    //procedure ListaAreasReload;											//TASK_121_JMA_20170328
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMAreasAtencionDeCasos: TFABMAreasAtencionDeCasos;

resourcestring
	STR_MAESTRO_AreasAtencion	= 'Maestro de �reas de Atenci�n';


implementation

{$R *.DFM}
{$REGION 'TASK_121_JMA_20170328'}
{INICIO: TASK_121_JMA_20170328
procedure TFABMAreasAtencionDeCasos.FormCreate(Sender: TObject);
begin
	ListaTiposDeCasos := TStringList.Create;
	cbTipo.Items.Clear;
	with spObtenerTiposOrdenServicio do begin
        if Active then
        	Close;
        Open;
        while not Eof do begin
	        cbTipo.Items.Add(FieldByName('Descripcion').AsString);
            ListaTiposDeCasos.Add(FieldByName('TipoOrdenServicio').AsString);
        	Next;
        end;
        Close;
    end;

end;

function TFABMAreasAtencionDeCasos.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    try
    	spObtenerAreasAtencionDeCasos.Open;
    except
    	on E:Exception do begin
        	MessageDlg('Error al obtener los datos' + #13 + E.Message, mtError, [mbOK], 0);
			Exit;
        end;
    end;
	Notebook.PageIndex := 0;
	ListaAreasReload;
	Result := True;
end;

procedure TFABMAreasAtencionDeCasos.LimpiarCampos;
begin
	txtCodigo.Clear;
    txtNombreArea.Clear;
    cbTipo.ItemIndex := -1;
	txtDescripcion.Clear;
    txtResponsable.Clear;
    txtTelefono.Clear;
    txtEmail.Clear;
end;

procedure TFABMAreasAtencionDeCasos.FormShow(Sender: TObject);
begin
	ListaAreasReload;
end;

procedure TFABMAreasAtencionDeCasos.Volver_Campos;
begin
	ListaAreasAtencion.Enabled:= True;

	ActiveControl       := ListaAreasAtencion;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaAreasReload;
end;

procedure TFABMAreasAtencionDeCasos.ListaAreasReload;
begin
	if spObtenerAreasAtencionDeCasos.Active then
    	spObtenerAreasAtencionDeCasos.Close;
    
    try
    	spObtenerAreasAtencionDeCasos.Open;
    except
    	on E:Exception do begin
        	MessageDlg('Error al obtener los datos' + #13 + E.Message, mtError, [mbOK], 0);
			Exit;
        end;
    end;
end;


procedure TFABMAreasAtencionDeCasos.BtnAceptarClick(Sender: TObject);
var
	rt: Word;
begin
	if not ValidateControls([txtDescripcion, txtNombreArea, txtResponsable, txtTelefono, txtEmail],
	  [(Trim(txtDescripcion.Text) <> ''), (Trim(txtNombreArea.Text) <> ''), (Trim(txtResponsable.Text) <> ''), (Trim(txtTelefono.Text) <> ''), (Trim(txtEmail.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_AreasAtencion]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
		Exit;
	end;

    if cbTipo.ItemIndex < 0  then begin
    	rt := MessageDlg('�Seguro no asocia un tipo de caso al �rea de atenci�n?', mtConfirmation, [mbYes, mbNo, mbCancel], 0);
        if rt <> mrYes then
        	Exit;
    end;

	with ListaAreasAtencion do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarAreasAtencionDeCasos, Parameters do begin
                    Parameters.Refresh;
                    if cbTipo.ItemIndex < 0 then
                    	ParamByName('@CodigoTipoDeCaso').Value		:= Null
                    else
                    	ParamByName('@CodigoTipoDeCaso').Value		:= ListaTiposDeCasos.Strings[cbTipo.ItemIndex];
                    ParamByName('@CodigoAreaAtencionDeCaso').Value	:= txtCodigo.Value;
                    ParamByName('@Nombre').Value					:= txtNombreArea.Text;
                    ParamByName('@Responsable').Value				:= txtResponsable.Text;
                    ParamByName('@Telefono').Value					:= txtTelefono.Text;
                    ParamByName('@Email').Value						:= txtEmail.Text;
                    ParamByName('@Descripcion').Value				:= txtDescripcion.Text;
                    ParamByName('@Usuario').Value					:= UsuarioSistema;
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_AreasAtencion]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_AreasAtencion]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            ListaAreasReload;
		end;
	end;
end;

procedure TFABMAreasAtencionDeCasos.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMAreasAtencionDeCasos.SpeedButton1Click(Sender: TObject);
begin
	Close;
end;

procedure TFABMAreasAtencionDeCasos.spObtenerAreasAtencionDeCasosAfterScroll(
  DataSet: TDataSet);
begin
	if spObtenerAreasAtencionDeCasos.FieldByName('DescripcionTipo').Value = null then
    	cbTipo.ItemIndex	:= -1
    else
    	cbTipo.ItemIndex	:= cbTipo.Items.IndexOf(spObtenerAreasAtencionDeCasos.FieldByName('DescripcionTipo').Value);
    txtCodigo.Value			:= spObtenerAreasAtencionDeCasos.FieldByName('CodigoAreaAtencionDeCaso').Value;
    txtNombreArea.Text		:= spObtenerAreasAtencionDeCasos.FieldByName('NombreAreaAtencionDeCaso').Value;
    txtResponsable.Text		:= spObtenerAreasAtencionDeCasos.FieldByName('Responsable').Value;
    txtTelefono.Text		:= spObtenerAreasAtencionDeCasos.FieldByName('Telefono').Value;
    txtEmail.Text			:= spObtenerAreasAtencionDeCasos.FieldByName('Email').Value;
    txtDescripcion.Text		:= spObtenerAreasAtencionDeCasos.FieldByName('DescripcionArea').Value;

end;

procedure TFABMAreasAtencionDeCasos.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMAreasAtencionDeCasos.btnTEditarClick(Sender: TObject);
begin
	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtNombreArea;
end;

procedure TFABMAreasAtencionDeCasos.btnTEliminarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_AreasAtencion]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with spEliminarAreasAtencionDeCasos, Parameters do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoAreaAtencionDeCaso').Value := spObtenerAreasAtencionDeCasos.FieldByName('CodigoAreaAtencionDeCaso').Value;
                Parameters.ParamByName('@CodigoTipoDeCaso').Value := spObtenerAreasAtencionDeCasos.FieldByName('TipoOrdenServicio').Value;
                ExecProc;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_AreasAtencion]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_AreasAtencion]), MB_ICONSTOP);
			end
		end
	end;

	ListaAreasReload;
	ListaAreasAtencion.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;

end;

procedure TFABMAreasAtencionDeCasos.btnTInsertarClick(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaAreasAtencion.Enabled	:= False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtNombreArea;
end;

procedure TFABMAreasAtencionDeCasos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

}
{$ENDREGION}

function TFABMAreasAtencionDeCasos.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
    Cargando := True;

    FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

	Notebook.PageIndex := 0;

    CargarUsuariosSistema();
    CargarTiposCasosSistema();
    CargarAreasAtencionDeCasos();

    Cargando := False;

    AreasAtencionDeCasos_AfterScroll(nil);
	Result := True;
end;

procedure TFABMAreasAtencionDeCasos.CargarUsuariosSistema;
var
    spAux : TADOStoredProc;
    cds: TClientDataSet;
begin
    try
        try
            cds := TClientDataSet.Create(nil);              
            cds.FieldDefs.Add('CodigoUsuario', ftString, 20, false);
            cds.FieldDefs.Add('NombreUsuario', ftString, 200, false);
            cds.CreateDataSet;
            cds.Filtered := True;

            spAux := TADOStoredProc.Create(nil);
            spAux.Connection := DMConnections.BaseBO_Master;
            spAux.ProcedureName := 'UsuariosSistemas_ObtenerInfo';
            spAux.Parameters.Refresh;
            spAux.Open;

            if spAux.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                raise Exception.Create(spAux.Parameters.ParamByName('@ErrorDescription').Value);

            spAux.First;
            while not spAux.Eof do
            begin
                cds.Append;
                cds.FieldByName('CodigoUsuario').Value := spAux.FieldByName('CodigoUsuario').Value;
                cds.FieldByName('NombreUsuario').Value := spAux.FieldByName('NombreUsuario').Value;
                cds.Post;
                spAux.Next;
            end;

            dblUsuariosSistema.DataSource := TDataSource.Create(nil);
            dblUsuariosSistema.DataSource.DataSet := cds;

        except
            on e: Exception do
            begin
                MsgBoxErr('Error Iniciando tablas', e.Message, 'Error de carga de datos', MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        FreeAndNil(spAux);
    end;
end;

procedure TFABMAreasAtencionDeCasos.CargarTiposCasosSistema;
var
    spAux : TADOStoredProc;
    cds: TClientDataSet;
begin
    try
        try                 
            cds := TClientDataSet.Create(nil);
            cds.FieldDefs.Add('CodigoTipoDeCaso', ftInteger, 0, false);
            cds.FieldDefs.Add('Descripcion', ftString, 200, false);
            cds.CreateDataSet;
            cds.Filtered := True;

            spAux := TADOStoredProc.Create(nil);
            spAux.Connection := DMConnections.BaseCAC;
            spAux.ProcedureName := 'ObtenerTiposOrdenServicioReclamo';
            spAux.Parameters.Refresh;
            spAux.Open;

            if spAux.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                raise Exception.Create(spAux.Parameters.ParamByName('@ErrorDescription').Value);

            spAux.First;
            while not spAux.Eof do
            begin
                cds.Append;
                cds.FieldByName('CodigoTipoDeCaso').Value := spAux.FieldByName('TipoOrdenServicio').Value;
                cds.FieldByName('Descripcion').Value := spAux.FieldByName('Descripcion').Value;
                cds.Post;
                spAux.Next;
            end;

            dblTiposCasosSistema.DataSource := TDataSource.Create(nil);
            dblTiposCasosSistema.DataSource.DataSet := cds;

        except
            on e: Exception do
            begin
                MsgBoxErr('Error Iniciando tablas', e.Message, 'Error de carga de datos', MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        FreeAndNil(spAux);
    end;       
end;

procedure TFABMAreasAtencionDeCasos.CargarAreasAtencionDeCasos;
begin      
    try
        if Assigned(ListaAreasAtencion.DataSource) then
            ListaAreasAtencion.DataSource.Free;
        ListaAreasAtencion.DataSource := TDataSource.Create(nil);

        if not Assigned(oAreasAtencion) then
            oAreasAtencion := TAreasAtencionDeCasos.Create;

        ListaAreasAtencion.DataSource.DataSet := oAreasAtencion.Obtener();
        oAreasAtencion.ClientDataSet.AfterScroll := AreasAtencionDeCasos_AfterScroll;     
    except
        on e: Exception do
        MsgBoxErr('Error cargando Areas Atencion de Casos', e.Message, 'Error en carda de datos', MB_ICONERROR);
    end;
end;

procedure TFABMAreasAtencionDeCasos.CargarCbbResponsable();
var
    indice, i:Integer;
begin
    if cbbResponsable.Tag = 0 then
    begin
    
        cbbResponsable.Tag := 1;
        cbbResponsable.Items.Clear;
        indice := -1;
        i := -1;
    
        try
            if oUsuarioAsignados.ClientDataSet.RecordCount > 0 then
            begin
                oUsuarioAsignados.ClientDataSet.DisableControls;
                oUsuarioAsignados.ClientDataSet.First;
                while not oUsuarioAsignados.ClientDataSet.eof do
                begin
                    i:= i+1;
                    cbbResponsable.Items.InsertItem(oUsuarioAsignados.NombreUsuario, oUsuarioAsignados.CodigoUsuario);
                    if oUsuarioAsignados.Principal then
                        indice := i;
                    oUsuarioAsignados.ClientDataSet.Next;
                end;

                if cbbResponsable.Items.Count >0  then
                    cbbResponsable.ItemIndex := indice;
            end;
        finally
            cbbResponsable.Tag := 0;
            oUsuarioAsignados.ClientDataSet.EnableControls;
        end;
    end;
end;

procedure TFABMAreasAtencionDeCasos.AreasAtencionDeCasos_AfterScroll(DataSet: TDataSet);
begin
    if not Cargando  then
    begin
        txtCodigo.Value			:= oAreasAtencion.CodigoAreaAtencionDeCaso;
        txtNombreArea.Text		:= oAreasAtencion.NombreAreaAtencionDeCaso;
        cbbResponsable.Value    := oAreasAtencion.Responsable;
        txtTelefono.Text		:= oAreasAtencion.Telefono;
        txtEmail.Text			:= oAreasAtencion.Email;
        txtDescripcion.Text		:= oAreasAtencion.DescripcionArea;

        CargarUsuariosAsignados(oAreasAtencion.CodigoAreaAtencionDeCaso);
        FiltrarUsuariosSistema();

        CargarTiposCasosAsignados(oAreasAtencion.CodigoAreaAtencionDeCaso);
        FiltrarTiposCasosSistama();

        CargarCbbResponsable();
    end;
end;

procedure TFABMAreasAtencionDeCasos.FiltrarUsuariosSistema();
var
filtro : string;
begin
    filtro := '';
    oUsuarioAsignados.ClientDataSet.DisableControls;
    oUsuarioAsignados.ClientDataSet.First;
    while not oUsuarioAsignados.ClientDataSet.Eof do
    begin
        if filtro <> '' then
            filtro := Filtro + ' and ';
        filtro := filtro + ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario);
        oUsuarioAsignados.ClientDataSet.Next;
    end;
    oUsuarioAsignados.ClientDataSet.First;
    oUsuarioAsignados.ClientDataSet.EnableControls;
    dblUsuariosSistema.DataSource.DataSet.Filter := filtro;
end;

procedure TFABMAreasAtencionDeCasos.FiltrarTiposCasosSistama();
var
    filtro : string;
begin
    filtro := '';
    oTiposCasosAsignados.ClientDataSet.DisableControls;
    oTiposCasosAsignados.ClientDataSet.First;
    while not oTiposCasosAsignados.ClientDataSet.Eof do
    begin
        if filtro <> '' then
            filtro := Filtro + ' and ';
        filtro := filtro + ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso));
        oTiposCasosAsignados.ClientDataSet.Next;
    end;
    oTiposCasosAsignados.ClientDataSet.First;
    oTiposCasosAsignados.ClientDataSet.EnableControls;
    dblTiposCasosSistema.DataSource.DataSet.Filter := filtro;
end;

function TFABMAreasAtencionDeCasos.CargarUsuariosAsignados(CodigoAreaAtencionDeCasos: Integer): Boolean;
begin
    Result := True;

    try
        if not Assigned(oUsuarioAsignados) then
            oUsuarioAsignados := TAreasAtencionDeCasosUsuarios.Create;

        if not Assigned(dblUsuariosAsignados.DataSource) then
             dblUsuariosAsignados.DataSource := TDataSource.Create(nil);

        dblUsuariosAsignados.DataSource.DataSet := oUsuarioAsignados.Obtener(CodigoAreaAtencionDeCasos);

    except
        on e: Exception do
        begin
            MsgBoxErr('Error cargando Usuarios Asignados', e.Message, 'Error de carga de datos', MB_ICONERROR);
            Result:= False;
        end;
    end;

end;

function TFABMAreasAtencionDeCasos.CargarTiposCasosAsignados(CodigoAreaAtencionDeCasos: Integer): Boolean;
begin
    Result := True;

    try
        if not Assigned(oTiposCasosAsignados) then
            oTiposCasosAsignados := TAreasAtencionDeCasosTiposCasos.Create;

        if not Assigned(dblTiposCasosAsignados.DataSource) then
             dblTiposCasosAsignados.DataSource := TDataSource.Create(nil);
        
        dblTiposCasosAsignados.DataSource.DataSet := oTiposCasosAsignados.Obtener(CodigoAreaAtencionDeCasos);

    except
        on e: Exception do
        begin
            MsgBoxErr('Error cargando Tipo de Casos Asignados', e.Message, 'Error de carga de datos', MB_ICONERROR);
            Result:= False;
        end;
    end;

end;




procedure TFABMAreasAtencionDeCasos.LimpiarCampos;
begin
	txtCodigo.Clear;
    txtNombreArea.Clear;
   	txtDescripcion.Clear;
    cbbResponsable.ItemIndex := -1;
    txtTelefono.Clear;
    txtEmail.Clear;
end;

procedure TFABMAreasAtencionDeCasos.PermitirEdicion(permitir: boolean);
begin

    groupb.Enabled := permitir;
    PanelOpciones.Enabled := not permitir;
	ListaAreasAtencion.Enabled	:= not permitir;

    btnAgregarUnUsuario.Enabled := permitir;
    btnAgregarTodosUsuarios.Enabled := permitir;
    btnQuitarUnUsuario.Enabled := permitir;
    btnQuitarTodosUsuarios.Enabled := permitir;

    btnAgregarUnTipoCaso.Enabled := permitir;
    btnAgregarTodosTipoCaso.Enabled := permitir;
    btnQuitarUnTipoCaso.Enabled := Permitir;
    btnQuitarTodosTipoCaso.Enabled := permitir;

    if permitir then
    begin
        Notebook.PageIndex := 1;
        if pgcAtencionCasos.ActivePageIndex = 0 then
    	    ActiveControl := txtNombreArea;
    end
    else
    begin
        Notebook.PageIndex := 0;
        AreasAtencionDeCasos_AfterScroll(nil);
      	ActiveControl := ListaAreasAtencion;
    end;
end;

procedure TFABMAreasAtencionDeCasos.pgcAtencionCasosChange(Sender: TObject);
begin
    if pgcAtencionCasos.ActivePage.Name = 'tsGeneral' then
        CargarCbbResponsable();
end;

procedure TFABMAreasAtencionDeCasos.cbbResponsableChange(Sender: TObject);
begin
    if (cbbResponsable.Tag = 0) and (cbbResponsable.ItemIndex >= 0) then
    begin
        if Assigned(oUsuarioAsignados) and (oUsuarioAsignados.ClientDataSet.RecordCount > 0) then
        begin
            try
                oUsuarioAsignados.ClientDataSet.DisableControls;
                oUsuarioAsignados.ClientDataSet.First;
                while not oUsuarioAsignados.ClientDataSet.Eof do
                begin
                    oUsuarioAsignados.Principal := False;
                    if oUsuarioAsignados.CodigoUsuario = cbbResponsable.Value then
                        oUsuarioAsignados.Principal := True;
                    oUsuarioAsignados.ClientDataSet.Next;
                end;
            finally
                oUsuarioAsignados.ClientDataSet.EnableControls;
                oUsuarioAsignados.ClientDataSet.First;
            end;
        end;
    end;
end;

procedure TFABMAreasAtencionDeCasos.BtnAceptarClick(Sender: TObject);
var
    bm: TBookmark;
    CodigoAreaAtencionDeCaso: integer;
begin
    if not ValidateControls([txtNombreArea,
                             cbbResponsable,
                             txtTelefono,
                             txtEmail],
                              [(Trim(txtNombreArea.Text) <> ''),
                               cbbResponsable.ItemIndex >= 0,
                               (Trim(txtTelefono.Text) <> ''),
                               (Trim(txtEmail.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_AreasAtencion]),
	  ['Ingrese el nombre del Area',
       'Seleccione un Responsable',
       'Ingrese un Tel�fono',
       'Ingrese un Email']) then begin
		Exit;
	end;

    Screen.Cursor := crHourGlass;
    try
        try
            if txtCodigo.Text = '' then //Agregando
            begin
                oAreasAtencion.Insertar(CodigoAreaAtencionDeCaso, Trim(txtNombreArea.Text), cbbResponsable.Value, Trim(txtTelefono.text), Trim(txtEmail.text), Trim(txtDescripcion.text),
                                         oUsuarioAsignados.ClientDataSet, oTiposCasosAsignados.ClientDataSet, UsuarioSistema);
            end
            else
            begin
                CodigoAreaAtencionDeCaso := StrToInt(txtCodigo.Text);
                oAreasAtencion.Modificar(CodigoAreaAtencionDeCaso, Trim(txtNombreArea.Text), cbbResponsable.Value, Trim(txtTelefono.text), Trim(txtEmail.text), Trim(txtDescripcion.text),
                                         oUsuarioAsignados.ClientDataSet, oTiposCasosAsignados.ClientDataSet, UsuarioSistema);
            end;

            CargarAreasAtencionDeCasos();
            AreasAtencionDeCasos_AfterScroll(nil);
            PermitirEdicion(False);

            bm:= oAreasAtencion.BuscarEnClientDataSet('CodigoAreaAtencionDeCaso', IntToStr(CodigoAreaAtencionDeCaso));
            if oAreasAtencion.ClientDataSet.BookmarkValid(bm) then
                oAreasAtencion.ClientDataSet.GotoBookmark(bm);

        except
            On E: EDataBaseError do begin
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_AreasAtencion]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_AreasAtencion]), MB_ICONSTOP);
            end;
        end;
    finally
         if oAreasAtencion.ClientDataSet.BookmarkValid(bm) then
            oAreasAtencion.ClientDataSet.FreeBookmark(bm);
        Screen.Cursor:= crDefault;
    end;

end;

procedure TFABMAreasAtencionDeCasos.BtnCancelarClick(Sender: TObject);
begin
	PermitirEdicion(False);
end;

procedure TFABMAreasAtencionDeCasos.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMAreasAtencionDeCasos.btnTInsertarClick(Sender: TObject);
begin
    LimpiarCampos;
    dblUsuariosSistema.DataSource.DataSet.Filter := '';
    dblTiposCasosSistema.DataSource.DataSet.Filter := '';
    dblUsuariosAsignados.DataSource.DataSet := oUsuarioAsignados.Obtener(-1);
    dblTiposCasosAsignados.DataSource.DataSet := oTiposCasosAsignados.Obtener(-1);
    cbbResponsable.Items.Clear;
    PermitirEdicion(True);
end;

procedure TFABMAreasAtencionDeCasos.btnTEditarClick(Sender: TObject);
begin
    PermitirEdicion(True);
end;

procedure TFABMAreasAtencionDeCasos.btnTEliminarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;

    try
        If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_AreasAtencion]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            try
                oAreasAtencion.Eliminar(oAreasAtencion.CodigoAreaAtencionDeCaso);
                CargarAreasAtencionDeCasos;
                AreasAtencionDeCasos_AfterScroll(nil);
            Except
                On E: Exception do begin
                    MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_AreasAtencion]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_AreasAtencion]), MB_ICONSTOP);
                end
            end
        end;
    finally
         Screen.Cursor      := crDefault;
    end;
    
end;

procedure TFABMAreasAtencionDeCasos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     FreeAndNil(oAreasAtencion);
     FreeAndNil(oUsuarioAsignados);
     FreeAndNil(oTiposCasosAsignados);

     dblUsuariosSistema.DataSource.DataSet.Free;
     dblUsuariosSistema.DataSource.Free;
     dblTiposCasosSistema.DataSource.DataSet.Free;
     dblTiposCasosSistema.DataSource.Free;

     dblUsuariosAsignados.DataSource.Free;
     dblTiposCasosAsignados.DataSource.Free;

	 action := caFree;
end;



{$REGION 'Mover Usuarios'}
procedure TFABMAreasAtencionDeCasos.btnAgregarUnUsuarioClick(Sender: TObject);
var
    ds : TDataSet;
    filtro: string;
begin
    ds := dblUsuariosSistema.DataSource.DataSet;
    if ds.RecordCount > 0 then
    begin
        oUsuarioAsignados.ClientDataSet.Append;
        oUsuarioAsignados.CodigoAreaAtencionDeCaso := oAreasAtencion.CodigoAreaAtencionDeCaso;
        oUsuarioAsignados.CodigoUsuario := ds.FieldByName('CodigoUsuario').Value;
        oUsuarioAsignados.NombreUsuario := ds.FieldByName('NombreUsuario').Value;
        oUsuarioAsignados.Principal := False;
        oUsuarioAsignados.ClientDataSet.Post;
        filtro := ds.Filter;
        if filtro <> '' then
            filtro := filtro + ' and ';
        ds.Filter := filtro + ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario);
    end;
end;

procedure TFABMAreasAtencionDeCasos.btnAgregarTodosUsuariosClick(Sender: TObject);
var
    ds : TDataSet;
    filtro: string;
begin
    ds := dblUsuariosSistema.DataSource.DataSet;
    if ds.RecordCount > 0 then
    begin
        filtro := ds.Filter;
        ds.first;
        while not ds.Eof do
        begin
            oUsuarioAsignados.ClientDataSet.Append;
            oUsuarioAsignados.CodigoAreaAtencionDeCaso := oAreasAtencion.CodigoAreaAtencionDeCaso;
            oUsuarioAsignados.CodigoUsuario := ds.FieldByName('CodigoUsuario').Value;
            oUsuarioAsignados.NombreUsuario := ds.FieldByName('NombreUsuario').Value;
            oUsuarioAsignados.Principal := False;
            oUsuarioAsignados.ClientDataSet.Post;
            if filtro <> '' then
                filtro := filtro + ' and ';
            filtro := filtro + ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario);
            ds.Next;
        end;
        ds.Filter := filtro;
    end;
end;

procedure TFABMAreasAtencionDeCasos.btnQuitarUnUsuarioClick(Sender: TObject);
var
    ds : TDataSet;
    CodigoUsuario : string;
begin
    if oUsuarioAsignados.ClientDataSet.RecordCount > 0 then
    begin
        ds := dblUsuariosSistema.DataSource.DataSet;
        if ContainsStr(ds.Filter, ' and ' + ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario)) then
            ds.Filter := ReplaceStr(ds.Filter, ' and ' + ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario), '')
        else if ContainsStr(ds.Filter, ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario) + ' and ') then
            ds.Filter := ReplaceStr(ds.Filter, ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario) + ' and ', '')
        else if ContainsStr(ds.Filter, ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario)) then
            ds.Filter := ReplaceStr(ds.Filter, ' CodigoUsuario <> ' + QuotedStr(oUsuarioAsignados.CodigoUsuario), '');

        CodigoUsuario := oUsuarioAsignados.CodigoUsuario;
        oUsuarioAsignados.ClientDataSet.Delete;
        ds.First;
        while not ds.Eof do
        begin
            if ds.FieldByName('CodigoUsuario').Value = CodigoUsuario then
                Break;
            ds.Next;
        end;
    end;
end;

procedure TFABMAreasAtencionDeCasos.btnQuitarTodosUsuariosClick(Sender: TObject);
begin
    if oUsuarioAsignados.ClientDataSet.RecordCount > 0 then
    begin
        oUsuarioAsignados.ClientDataSet.Last;
        while not oUsuarioAsignados.ClientDataSet.Bof do
        begin
            oUsuarioAsignados.ClientDataSet.Delete;
            oUsuarioAsignados.ClientDataSet.Prior;
        end;
        oUsuarioAsignados.ClientDataSet.Delete;
        dblUsuariosSistema.DataSource.DataSet.Filter := '';
    end;
end;
{$ENDREGION}

{$REGION 'Mover TiposCasos'}
procedure TFABMAreasAtencionDeCasos.btnAgregarUnTipoCasoClick(Sender: TObject);
var
    ds : TDataSet;
    filtro: string;
begin
    ds := dblTiposCasosSistema.DataSource.DataSet;
    if ds.RecordCount > 0 then
    begin
        oTiposCasosAsignados.ClientDataSet.Append;
        oTiposCasosAsignados.CodigoAreaAtencionDeCaso := oAreasAtencion.CodigoAreaAtencionDeCaso;
        oTiposCasosAsignados.CodigoTipoDeCaso := ds.FieldByName('CodigoTipoDeCaso').Value;
        oTiposCasosAsignados.Descripcion := ds.FieldByName('Descripcion').Value;
        oTiposCasosAsignados.ClientDataSet.Post;
        filtro := ds.Filter;
        if filtro <> '' then
            filtro := filtro + ' and ';
        ds.Filter := filtro + ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso));
    end;
end;

procedure TFABMAreasAtencionDeCasos.btnAgregarTodosTipoCasoClick(Sender: TObject);
var
    ds : TDataSet;
    filtro: string;
begin
    ds := dblTiposCasosSistema.DataSource.DataSet;
    if ds.RecordCount > 0 then
    begin
        filtro := ds.Filter;
        ds.first;
        while not ds.Eof do
        begin
            oTiposCasosAsignados.ClientDataSet.Append;
            oTiposCasosAsignados.CodigoAreaAtencionDeCaso := oAreasAtencion.CodigoAreaAtencionDeCaso;
            oTiposCasosAsignados.CodigoTipoDeCaso := ds.FieldByName('CodigoTipoDeCaso').Value;
            oTiposCasosAsignados.Descripcion := ds.FieldByName('Descripcion').Value;
            oTiposCasosAsignados.ClientDataSet.Post;
            if filtro <> '' then
                filtro := filtro + ' and ';
            filtro := filtro + ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso));
            ds.Next;
        end;
        ds.Filter := filtro;
    end;
end;

procedure TFABMAreasAtencionDeCasos.btnQuitarUnTipoCasoClick(Sender: TObject);
var
    ds : TDataSet;
    CodigoTipoDeCaso : integer;
begin
    if oTiposCasosAsignados.ClientDataSet.RecordCount > 0 then
    begin
        ds := dblTiposCasosSistema.DataSource.DataSet;
        if ContainsStr(ds.Filter, ' and ' + ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso))) then
            ds.Filter := ReplaceStr(ds.Filter, ' and ' + ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso)), '')
        else if ContainsStr(ds.Filter, ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso)) + ' and ') then
            ds.Filter := ReplaceStr(ds.Filter, ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso)) + ' and ', '')
        else if ContainsStr(ds.Filter, ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso))) then
            ds.Filter := ReplaceStr(ds.Filter, ' CodigoTipoDeCaso <> ' + QuotedStr(IntToStr(oTiposCasosAsignados.CodigoTipoDeCaso)), '');

        CodigoTipoDeCaso := oTiposCasosAsignados.CodigoTipoDeCaso;

        oTiposCasosAsignados.ClientDataSet.Delete;
        ds.First;
        while not ds.Eof do
        begin
            if ds.FieldByName('CodigoTipoDeCaso').Value = CodigoTipoDeCaso then
                Break;
            ds.Next;
        end;
    end;
end;

procedure TFABMAreasAtencionDeCasos.btnQuitarTodosTipoCasoClick(Sender: TObject);
begin
    if oTiposCasosAsignados.ClientDataSet.RecordCount > 0 then
    begin
        oTiposCasosAsignados.ClientDataSet.Last;
        while not oTiposCasosAsignados.ClientDataSet.Bof do
        begin
            oTiposCasosAsignados.ClientDataSet.Delete;
            oTiposCasosAsignados.ClientDataSet.Prior;
        end;
        oTiposCasosAsignados.ClientDataSet.Delete;
        dblTiposCasosSistema.DataSource.DataSet.Filter := '';
    end;
end;
{$ENDREGION}
end.

