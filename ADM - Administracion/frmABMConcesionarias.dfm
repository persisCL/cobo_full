object ABMConcesionariasForm: TABMConcesionariasForm
  Left = 0
  Top = 0
  Caption = 'ABMConcesionariasForm'
  ClientHeight = 590
  ClientWidth = 836
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 836
    Height = 33
    Align = alTop
    TabOrder = 0
    object btnSalir: TSpeedButton
      Left = 8
      Top = 4
      Width = 25
      Height = 25
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
        03333377777777777F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F3333333F7F333301111111B10333337F333333737F33330111111111
        0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
        0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
        0333337F377777337F333301111111110333337F333333337F33330111111111
        0333337FFFFFFFFF7F3333000000000003333377777777777333}
      NumGlyphs = 2
      OnClick = btnSalirClick
    end
    object btnInsertar: TSpeedButton
      Left = 51
      Top = 4
      Width = 25
      Height = 25
      Enabled = False
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = btnInsertarClick
    end
    object btnEliminar: TSpeedButton
      Left = 82
      Top = 4
      Width = 25
      Height = 25
      Enabled = False
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      Visible = False
      OnClick = btnEliminarClick
    end
    object btnModificar: TSpeedButton
      Left = 113
      Top = 4
      Width = 25
      Height = 25
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      OnClick = btnModificarClick
    end
  end
  object dblConcesionarias: TDBListEx
    Left = 0
    Top = 33
    Width = 836
    Height = 125
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'Codigo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'CodigoConcesionaria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        Header.Caption = 'Descripcion'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 50
        Header.Caption = 'Nativa'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'EsConcesionariaNativa'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 80
        Header.Caption = 'Abreviaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreCorto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Per'#237'odo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoPeriodo'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Per'#237'odo Anterior'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoPeriodoAnterior'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Ajuste Cobro de Peaje'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoAjusteCobroPeaje'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Boleto Prejudicial'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInfraBoletoPrejudicial'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Boleto Morosidad Cat 1'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInfraBoletoMorosidadCat1'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Concepto Boleto Morosidad Cat 2'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInfraBoletoMorosidadCat2'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Concepto Boleto Morosidad Cat 3'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInfraBoletoMorosidadCat3'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Concepto Boleto Morosidad Cat 4'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInfraBoletoMorosidadCat4'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Gastos Cobranza Infractor'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInfraGastosCobranza'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Intereses Infractor'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInfraIntereses'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Intereses Exentos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInteresesExentos'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        Header.Caption = 'Concepto Intereses Afectos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ConceptoInteresesAfectos'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Orden C'#225'lculo Intereses'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'OrdenCalculoIntereses'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Tipo Concesionaria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'TipoConcesionaria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Permite Reclamos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'PermiteReclamos'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Informar Folios Y Pagos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'InformarFoliosYPagos'
      end>
    DataSource = dsConcesionarias
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDrawText = dblConcesionariasDrawText
  end
  object pcConcesionarias: TPageControl
    Left = 0
    Top = 158
    Width = 836
    Height = 391
    ActivePage = tsConcesionarias
    Align = alBottom
    TabOrder = 2
    OnChanging = pcConcesionariasChanging
    object tsConcesionarias: TTabSheet
      Caption = 'Datos Concesionaria'
      object Label1: TLabel
        Left = 179
        Top = 11
        Width = 37
        Height = 13
        Caption = 'Codigo:'
      end
      object Label2: TLabel
        Left = 310
        Top = 35
        Width = 58
        Height = 13
        Caption = 'Descripci'#243'n:'
      end
      object Label6: TLabel
        Left = 145
        Top = 35
        Width = 71
        Height = 13
        Caption = 'Nombre Corto:'
      end
      object Label5: TLabel
        Left = 139
        Top = 92
        Width = 77
        Height = 13
        Caption = 'Ruta Im'#225'genes:'
      end
      object Label7: TLabel
        Left = 95
        Top = 119
        Width = 121
        Height = 13
        Caption = 'Concepto per'#237'odo actual:'
      end
      object Label8: TLabel
        Left = 86
        Top = 142
        Width = 130
        Height = 13
        Caption = 'Concepto per'#237'odo anterior:'
      end
      object Label10: TLabel
        Left = 122
        Top = 336
        Width = 94
        Height = 13
        Caption = 'Tipo Concesionaria:'
      end
      object Label11: TLabel
        Left = 87
        Top = 194
        Width = 129
        Height = 13
        Caption = 'Concepto Boleto Infractor:'
      end
      object Label15: TLabel
        Left = 9
        Top = 215
        Width = 207
        Height = 13
        Caption = 'Concepto Gtos. Cobranza Boleto Infractor:'
      end
      object Label18: TLabel
        Left = 38
        Top = 238
        Width = 178
        Height = 13
        Caption = 'Concepto Intereses Boleto Infractor:'
      end
      object Label19: TLabel
        Left = 80
        Top = 262
        Width = 136
        Height = 13
        Caption = 'Concepto Intereses Exento:'
      end
      object Label20: TLabel
        Left = 82
        Top = 286
        Width = 134
        Height = 13
        Caption = 'Concepto Intereses Afecto:'
      end
      object Label21: TLabel
        Left = 96
        Top = 308
        Width = 120
        Height = 13
        Caption = 'Orden C'#225'lculo Intereses:'
      end
      object lblConceptoAjusteCobroPeaje: TLabel
        Left = 70
        Top = 166
        Width = 146
        Height = 13
        Caption = 'Concepto Ajuste Cobro Peaje:'
      end
      object edtCodigo: TEdit
        Left = 222
        Top = 8
        Width = 77
        Height = 21
        Color = clBtnFace
        Enabled = False
        TabOrder = 0
      end
      object edtDescripcion: TEdit
        Left = 374
        Top = 32
        Width = 277
        Height = 21
        MaxLength = 50
        TabOrder = 2
        OnKeyPress = edtDescripcionKeyPress
      end
      object edtNombreCorto: TEdit
        Left = 222
        Top = 32
        Width = 77
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 1
        OnKeyPress = edtDescripcionKeyPress
      end
      object chkFacturacionInterna: TCheckBox
        Left = 115
        Top = 62
        Width = 120
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Facturacion Interna:'
        TabOrder = 3
      end
      object chkTarifaUnica: TCheckBox
        Left = 250
        Top = 62
        Width = 81
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Tarifa '#250'nica:'
        TabOrder = 4
      end
      object edtRuta: TEdit
        Left = 222
        Top = 89
        Width = 499
        Height = 21
        MaxLength = 255
        TabOrder = 8
        OnKeyPress = edtRutaKeyPress
      end
      object vcbConceptoPeriodoActual: TVariantComboBox
        Left = 222
        Top = 116
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 9
        Items = <>
      end
      object vcbConceptoPeriodoAnterior: TVariantComboBox
        Left = 222
        Top = 139
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 10
        Items = <>
      end
      object vcbTipoConcesionaria: TVariantComboBox
        Left = 222
        Top = 333
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 11
        Items = <>
      end
      object chkFacturaCN: TCheckBox
        Left = 559
        Top = 335
        Width = 265
        Height = 25
        Caption = '100% Tr'#225'nsitos son facturados en Concesionaria Nativa'
        TabOrder = 12
        WordWrap = True
      end
      object vcbConceptoBoletoInfractor: TVariantComboBox
        Left = 222
        Top = 189
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 13
        Items = <>
      end
      object vcbConceptoGastosCobranzaInfractor: TVariantComboBox
        Left = 222
        Top = 212
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 14
        Items = <>
      end
      object vcbConceptoInteresesInfractor: TVariantComboBox
        Left = 222
        Top = 235
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 15
        Items = <>
      end
      object vcbConceptoInteresesExento: TVariantComboBox
        Left = 222
        Top = 259
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 17
        Items = <>
      end
      object vcbConceptoInteresesAfecto: TVariantComboBox
        Left = 222
        Top = 282
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 18
        Items = <>
      end
      object nedtOrdenCalculoIntereses: TNumericEdit
        Left = 222
        Top = 305
        Width = 77
        Height = 21
        TabOrder = 20
      end
      object chkPermiteReclamo: TCheckBox
        Left = 347
        Top = 62
        Width = 110
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Permite Reclamos:'
        TabOrder = 5
      end
      object chkInformarFoliosYPagos: TCheckBox
        Left = 475
        Top = 62
        Width = 141
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Informar Folios Y Pagos:'
        TabOrder = 6
      end
      object vcbConceptoAjusteCobroPeaje: TVariantComboBox
        Left = 222
        Top = 162
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 19
        Items = <>
      end
      object grpCodigoConceptoMorosos: TGroupBox
        Left = 562
        Top = 116
        Width = 257
        Height = 195
        Caption = 'Concepto Boleto Infractor por Morosidad'
        TabOrder = 16
        object lblConceptoBoletoInfractorMorosidadCat1: TLabel
          Left = 8
          Top = 19
          Width = 56
          Height = 13
          Caption = 'Categor'#237'a 1'
        end
        object lblConceptoBoletoInfractorMorosidadCat2: TLabel
          Left = 8
          Top = 59
          Width = 56
          Height = 13
          Caption = 'Categor'#237'a 2'
        end
        object lblConceptoBoletoInfractorMorosidadCat3: TLabel
          Left = 8
          Top = 101
          Width = 56
          Height = 13
          Caption = 'Categor'#237'a 3'
        end
        object lblConceptoBoletoInfractorMorosidadCat4: TLabel
          Left = 8
          Top = 143
          Width = 56
          Height = 13
          Caption = 'Categor'#237'a 4'
        end
        object vcbConceptoBoletoInfractorMorosidadCat1: TVariantComboBox
          Left = 6
          Top = 36
          Width = 243
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          Items = <>
        end
        object vcbConceptoBoletoInfractorMorosidadCat2: TVariantComboBox
          Left = 6
          Top = 77
          Width = 243
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 1
          Items = <>
        end
        object vcbConceptoBoletoInfractorMorosidadCat3: TVariantComboBox
          Left = 6
          Top = 118
          Width = 243
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 2
          Items = <>
        end
        object vcbConceptoBoletoInfractorMorosidadCat4: TVariantComboBox
          Left = 6
          Top = 161
          Width = 243
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 3
          Items = <>
        end
      end
      object chkPuedeListaAmarilla: TCheckBox
        Left = 634
        Top = 62
        Width = 163
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Puede Inhabilitar Lista Amarilla'
        TabOrder = 7
      end
      object chkEsNativa: TCheckBox
        Left = 331
        Top = 9
        Width = 56
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Nativa:'
        TabOrder = 21
      end
    end
    object tsDatosBasico: TTabSheet
      Caption = 'Informaci'#243'n B'#225'sica'
      Enabled = False
      ImageIndex = 3
      object lblRUT: TLabel
        Left = 193
        Top = 35
        Width = 24
        Height = 13
        Caption = 'RUT:'
      end
      object lblRazonSocial: TLabel
        Left = 153
        Top = 62
        Width = 64
        Height = 13
        Caption = 'Raz'#243'n Social:'
      end
      object lblGiro: TLabel
        Left = 193
        Top = 89
        Width = 23
        Height = 13
        Caption = 'Giro:'
      end
      object lblCodigoSII: TLabel
        Left = 162
        Top = 143
        Width = 54
        Height = 13
        Caption = 'C'#243'digo SII:'
      end
      object lblDireccion: TLabel
        Left = 158
        Top = 170
        Width = 58
        Height = 13
        Caption = 'Descripci'#243'n:'
      end
      object lblPais: TLabel
        Left = 193
        Top = 197
        Width = 23
        Height = 13
        Caption = 'Pa'#237's:'
      end
      object lblRegion: TLabel
        Left = 179
        Top = 224
        Width = 37
        Height = 13
        Caption = 'Regi'#243'n:'
      end
      object lblComuna: TLabel
        Left = 173
        Top = 251
        Width = 43
        Height = 13
        Caption = 'Comuna:'
      end
      object lblCodigoActividadEconomica: TLabel
        Left = 79
        Top = 116
        Width = 137
        Height = 13
        Caption = 'C'#243'digo Actividad Econ'#243'mica:'
      end
      object txt_RUT: TEdit
        Left = 222
        Top = 32
        Width = 77
        Height = 21
        AutoSize = False
        CharCase = ecUpperCase
        MaxLength = 9
        TabOrder = 0
        OnKeyPress = edtDescripcionKeyPress
      end
      object txt_DV: TEdit
        Left = 305
        Top = 32
        Width = 19
        Height = 21
        AutoSize = False
        CharCase = ecUpperCase
        MaxLength = 1
        TabOrder = 1
        OnKeyPress = edtDescripcionKeyPress
      end
      object txt_RazonSocial: TEdit
        Left = 222
        Top = 59
        Width = 507
        Height = 21
        AutoSize = False
        CharCase = ecUpperCase
        MaxLength = 100
        TabOrder = 2
        OnKeyPress = edtDescripcionKeyPress
      end
      object txt_Giro: TEdit
        Left = 222
        Top = 86
        Width = 507
        Height = 21
        AutoSize = False
        CharCase = ecUpperCase
        MaxLength = 100
        TabOrder = 3
        OnKeyPress = edtDescripcionKeyPress
      end
      object txt_CodigoSII: TEdit
        Left = 222
        Top = 140
        Width = 77
        Height = 21
        AutoSize = False
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 5
        OnKeyPress = edtDescripcionKeyPress
      end
      object txt_Direccion: TEdit
        Left = 222
        Top = 167
        Width = 419
        Height = 21
        AutoSize = False
        CharCase = ecUpperCase
        MaxLength = 70
        TabOrder = 6
        OnKeyPress = edtDescripcionKeyPress
      end
      object cbbPais: TVariantComboBox
        Left = 222
        Top = 194
        Width = 331
        Height = 21
        Style = vcsDropDownList
        Color = clBtnFace
        Enabled = False
        ItemHeight = 13
        TabOrder = 7
        Items = <>
      end
      object cbbRegion: TVariantComboBox
        Left = 222
        Top = 221
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 8
        OnChange = cbbRegionChange
        Items = <>
      end
      object cbbComuna: TVariantComboBox
        Left = 222
        Top = 248
        Width = 331
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 9
        Items = <>
      end
      object txt_CodigoActividadEconomica: TEdit
        Left = 222
        Top = 113
        Width = 77
        Height = 21
        AutoSize = False
        CharCase = ecUpperCase
        MaxLength = 6
        TabOrder = 4
        OnKeyPress = edtDescripcionKeyPress
      end
    end
    object tsEntradas: TTabSheet
      Caption = 'Entradas Estacionamiento'
      ImageIndex = 1
      object pnlEntradas: TPanel
        Left = 0
        Top = 239
        Width = 828
        Height = 124
        Align = alBottom
        TabOrder = 0
        object Label3: TLabel
          Left = 78
          Top = 14
          Width = 37
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label4: TLabel
          Left = 44
          Top = 41
          Width = 71
          Height = 13
          Caption = 'Nombre Corto:'
        end
        object Label9: TLabel
          Left = 57
          Top = 68
          Width = 58
          Height = 13
          Caption = 'Descripcion:'
        end
        object Label17: TLabel
          Left = 37
          Top = 95
          Width = 78
          Height = 13
          Caption = 'C'#243'digo Externo:'
        end
        object chkEntradaActivo: TCheckBox
          Left = 358
          Top = 14
          Width = 59
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Activo:'
          TabOrder = 4
        end
        object edtEntradaCod: TEdit
          Left = 136
          Top = 10
          Width = 81
          Height = 21
          Enabled = False
          TabOrder = 0
          Text = 'edtEntradaCod'
        end
        object edtEntradaNombreCorto: TEdit
          Left = 136
          Top = 37
          Width = 121
          Height = 21
          TabOrder = 1
          Text = 'edtEntradaNombreCorto'
        end
        object edtEntradaDescripcion: TEdit
          Left = 136
          Top = 64
          Width = 281
          Height = 21
          TabOrder = 2
          Text = 'edtEntradaDescripcion'
        end
        object edtEntradaCodExterno: TEdit
          Left = 136
          Top = 91
          Width = 281
          Height = 21
          TabOrder = 3
          Text = 'edtEntradaCodExterno'
        end
      end
      object dblEntradas: TDBListEx
        Left = 0
        Top = 33
        Width = 828
        Height = 206
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'C'#243'digo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'CodigoEntradaEstacionamiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Nombre Corto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NombreCorto'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 250
            Header.Caption = 'Descripcion'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'C'#243'digo Externo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CodigoExterno'
          end>
        DataSource = dsEntradas
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 828
        Height = 33
        Align = alTop
        TabOrder = 2
        object btnEntraInsertar: TSpeedButton
          Left = 51
          Top = 4
          Width = 25
          Height = 25
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = btnEntraInsertarClick
        end
        object btnEntraEliminar: TSpeedButton
          Left = 82
          Top = 4
          Width = 25
          Height = 25
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = btnEntraEliminarClick
        end
        object btnEntraModificar: TSpeedButton
          Left = 113
          Top = 4
          Width = 25
          Height = 25
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          OnClick = btnEntraModificarClick
        end
      end
    end
    object tsSalidas: TTabSheet
      Caption = 'Salidas Estacionamiento'
      ImageIndex = 2
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 828
        Height = 33
        Align = alTop
        TabOrder = 0
        object btnSalidaInsertar: TSpeedButton
          Left = 51
          Top = 4
          Width = 25
          Height = 25
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = btnSalidaInsertarClick
        end
        object btnSalidaEliminar: TSpeedButton
          Left = 82
          Top = 4
          Width = 25
          Height = 25
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = btnSalidaEliminarClick
        end
        object btnSalidaModificar: TSpeedButton
          Left = 113
          Top = 4
          Width = 25
          Height = 25
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          OnClick = btnSalidaModificarClick
        end
      end
      object dblSalidas: TDBListEx
        Left = 0
        Top = 33
        Width = 828
        Height = 206
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'C'#243'digo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'CodigoSalidaEstacionamiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Nombre Corto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NombreCorto'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 250
            Header.Caption = 'Descripcion'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'C'#243'digo Externo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CodigoExterno'
          end>
        DataSource = dsSalidas
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
      object pnlSalidas: TPanel
        Left = 0
        Top = 239
        Width = 828
        Height = 124
        Align = alBottom
        TabOrder = 2
        object Label12: TLabel
          Left = 78
          Top = 14
          Width = 37
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label13: TLabel
          Left = 44
          Top = 41
          Width = 71
          Height = 13
          Caption = 'Nombre Corto:'
        end
        object Label14: TLabel
          Left = 57
          Top = 68
          Width = 58
          Height = 13
          Caption = 'Descripcion:'
        end
        object Label16: TLabel
          Left = 37
          Top = 95
          Width = 78
          Height = 13
          Caption = 'C'#243'digo Externo:'
        end
        object chkSalidaActivo: TCheckBox
          Left = 358
          Top = 19
          Width = 59
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Activo:'
          TabOrder = 4
        end
        object edtSalidaCodigo: TEdit
          Left = 136
          Top = 10
          Width = 81
          Height = 21
          Enabled = False
          TabOrder = 0
          Text = 'edtSalidaCodigo'
        end
        object edtSalidaNombreCorto: TEdit
          Left = 136
          Top = 37
          Width = 121
          Height = 21
          TabOrder = 1
          Text = 'edtSalidaNombreCorto'
        end
        object edtSalidaDescripcion: TEdit
          Left = 136
          Top = 64
          Width = 281
          Height = 21
          TabOrder = 2
          Text = 'edtSalidaDescripcion'
        end
        object edtSalidaCodExterno: TEdit
          Left = 136
          Top = 91
          Width = 281
          Height = 21
          TabOrder = 3
          Text = 'edtSalidaCodExterno'
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 549
    Width = 836
    Height = 41
    Align = alBottom
    TabOrder = 3
    object nbGrabar: TNotebook
      Left = 638
      Top = 1
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object aqryConcesionarias: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      #9'con.CodigoConcesionaria,'
      #9'con.Descripcion,'
      #9'con.FacturacionInterna,'
      #9'con.LogoConcesionaria,'
      #9'con.PathImagenes,'
      #9'con.NombreCorto,'
      #9'con.CodigoConceptoPeriodo,'
      #9'con.CodigoConceptoPeriodoAnterior,'
      #9'con.CodigoConceptoInfraBoletoPrejudicial,'
      #9'con.CodigoConceptoInfraGastosCobranza,'
      #9'con.CodigoConceptoInfraIntereses,'
      #9'con.CodigoConceptoInteresAfecto,'
      #9'con.CodigoConceptoInteresExento,'
      #9'con.OrdenCalculoIntereses,'
      #9'con.TarifaUnica,'
      #9'con.CodigoTipoConcesionaria,'
      #9'con.TransitosLosFacturaCN,'
      #9'ISNULL(cmp.Descripcion,'#39#39') AS ConceptoPeriodo,'
      #9'ISNULL(cmpa.Descripcion,'#39#39') AS ConceptoPeriodoAnterior,'
      
        #9'ISNULL(cmpBoleto.Descripcion,'#39#39') AS ConceptoInfraBoletoPrejudic' +
        'ial,'
      
        #9'ISNULL(cmpGastos.Descripcion,'#39#39') AS ConceptoInfraGastosCobranza' +
        ','
      #9'ISNULL(cmpIntereses.Descripcion,'#39#39') AS ConceptoInfraIntereses,'
      
        #9'ISNULL(cmpInteresesExentos.Descripcion,'#39#39') AS ConceptoIntereses' +
        'Exentos,'
      
        #9'ISNULL(cmpInteresesAfectos.Descripcion,'#39#39') AS ConceptoIntereses' +
        'Afectos,'
      #9'tc.Descripcion  AS TipoConcesionaria,'
      #9'ISNULL(con.PermiteReclamos,0) AS PermiteReclamos,'
      #9'ISNULL(con.InformarFoliosYPagos,0) AS InformarFoliosYPagos,'
      
        #9'ISNULL(cmpAjusteCobroPeaje.Descripcion, '#39#39') AS ConceptoAjusteCo' +
        'broPeaje, -- SS_1006_1015_CQU_20121001'
      
        #9'con.CodigoConceptoAjusteCobroPeaje AS CodigoAjusteCobroPeaje, -' +
        '- SS_1006_1015_CQU_20121001'
      
        #9'ISNULL(cmpBoletoMorosoCat1.Descripcion, '#39#39') AS ConceptoInfraBol' +
        'etoMorosidadCat1,      -- SS_660_CQU_20121010'
      
        #9'con.CodigoConceptoInfraBoletoMorosidadCat1 AS CodigoConceptoInf' +
        'raBoletoMorosidadCat1, -- SS_660_CQU_20121010'
      
        #9'ISNULL(cmpBoletoMorosoCat2.Descripcion, '#39#39') AS ConceptoInfraBol' +
        'etoMorosidadCat2,      -- SS_660_CQU_20121010'
      
        #9'con.CodigoConceptoInfraBoletoMorosidadCat2 AS CodigoConceptoInf' +
        'raBoletoMorosidadCat2, -- SS_660_CQU_20121010'
      
        #9'ISNULL(cmpBoletoMorosoCat3.Descripcion, '#39#39') AS ConceptoInfraBol' +
        'etoMorosidadCat3,      -- SS_660_CQU_20121010'
      
        #9'con.CodigoConceptoInfraBoletoMorosidadCat3 AS CodigoConceptoInf' +
        'raBoletoMorosidadCat3, -- SS_660_CQU_20121010'
      
        #9'ISNULL(cmpBoletoMorosoCat4.Descripcion, '#39#39') AS ConceptoInfraBol' +
        'etoMorosidadCat4,      -- SS_660_CQU_20121010'
      
        #9'con.CodigoConceptoInfraBoletoMorosidadCat4 AS CodigoConceptoInf' +
        'raBoletoMorosidadCat4, -- SS_660_CQU_20121010'
      
        #9'ISNULL(con.PermiteInhabilitarListaAmarilla, 0) AS PermiteInhabi' +
        'litarListaAmarilla,     -- SS_660_CQU_20130711'
      '  con.RUT,'
      '  con.DigitoVerificador,'
      '  con.RazonSocial,'
      '  con.Giro,'
      '  con.CodigoActividadEconomica,'
      '  con.CodigoSIISucursal,'
      '  con.Direccion,'
      '  con.CodigoComuna,'
      '  con.CodigoRegion,'
      '  con.CodigoPais,'
      '  con.EsConcesionariaNativa'
      ''
      '  '
      #9'FROM Concesionarias con (NOLOCK)'
      #9
      #9'LEFT JOIN ConceptosMovimiento cmp (NOLOCK)'
      #9#9'ON con.CodigoConceptoPeriodo = cmp.CodigoConcepto'
      ''
      #9'LEFT JOIN ConceptosMovimiento cmpa (NOLOCK)'
      #9#9'ON con.CodigoConceptoPeriodoAnterior = cmpa.CodigoConcepto'
      ''
      #9'LEFT JOIN ConceptosMovimiento cmpBoleto (NOLOCK)'
      
        #9#9'ON con.CodigoConceptoInfraBoletoPrejudicial = cmpBoleto.Codigo' +
        'Concepto'
      ''
      #9'LEFT JOIN ConceptosMovimiento cmpGastos (NOLOCK)'
      
        #9#9'ON con.CodigoConceptoInfraGastosCobranza = cmpGastos.CodigoCon' +
        'cepto'
      ''
      #9'LEFT JOIN ConceptosMovimiento cmpIntereses (NOLOCK)'
      
        #9#9'ON con.CodigoConceptoInfraIntereses = cmpIntereses.CodigoConce' +
        'pto'
      #9#9
      #9'LEFT JOIN ConceptosMovimiento cmpInteresesExentos (NOLOCK)'
      
        #9#9'ON con.CodigoConceptoInteresExento = cmpInteresesExentos.Codig' +
        'oConcepto'#9
      #9#9
      #9'LEFT JOIN ConceptosMovimiento cmpInteresesAfectos (NOLOCK)'
      
        #9#9'ON con.CodigoConceptoInteresAfecto = cmpInteresesAfectos.Codig' +
        'oConcepto'#9#9#9#9#9
      ''
      
        #9'LEFT JOIN ConceptosMovimiento cmpAjusteCobroPeaje (NOLOCK) -- S' +
        'S_1006_1015_CQU_20121001'
      
        #9#9'ON con.CodigoConceptoAjusteCobroPeaje = cmpAjusteCobroPeaje.Co' +
        'digoConcepto -- SS_1006_1015_CQU_20121001'
      ''
      
        #9'LEFT JOIN ConceptosMovimiento cmpBoletoMorosoCat1 (NOLOCK) -- S' +
        'S_660_CQU_20121010'
      
        #9#9'ON con.CodigoConceptoInfraBoletoMorosidadCat1 = cmpBoletoMoros' +
        'oCat1.CodigoConcepto -- SS_660_CQU_20121010'
      ''
      
        #9'LEFT JOIN ConceptosMovimiento cmpBoletoMorosoCat2 (NOLOCK) -- S' +
        'S_660_CQU_20121010'
      
        #9#9'ON con.CodigoConceptoInfraBoletoMorosidadCat2 = cmpBoletoMoros' +
        'oCat2.CodigoConcepto -- SS_660_CQU_20121010'
      ''
      
        ' '#9'LEFT JOIN ConceptosMovimiento cmpBoletoMorosoCat3 (NOLOCK) -- ' +
        'SS_660_CQU_20121010'
      
        #9#9'ON con.CodigoConceptoInfraBoletoMorosidadCat3 = cmpBoletoMoros' +
        'oCat3.CodigoConcepto -- SS_660_CQU_20121010'
      ''
      
        #9'LEFT JOIN ConceptosMovimiento cmpBoletoMorosoCat4 (NOLOCK) -- S' +
        'S_660_CQU_20121010'
      
        #9#9'ON con.CodigoConceptoInfraBoletoMorosidadCat4 = cmpBoletoMoros' +
        'oCat4.CodigoConcepto -- SS_660_CQU_20121010'
      ''
      #9'INNER JOIN TipoConcesionaria tc (NOLOCK)'
      #9#9'ON con.CodigoTipoConcesionaria = tc.CodigoTipoConcesionaria')
    Left = 80
    Top = 72
  end
  object dsConcesionarias: TDataSource
    DataSet = aqryConcesionarias
    OnDataChange = dsConcesionariasDataChange
    Left = 248
    Top = 72
  end
  object aqryTiposConcesionarias: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      #9'CodigoTipoConcesionaria,'
      #9'Descripcion'
      'FROM'
      #9'TipoConcesionaria (NOLOCK)'
      ''
      'ORDER BY Descripcion')
    Left = 176
    Top = 72
  end
  object aqryEntradas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    DataSource = dsConcesionarias
    Parameters = <
      item
        Name = 'CodigoConcesionaria'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      #9#9'CodigoEntradaEstacionamiento,'
      #9#9'NombreCorto,'
      #9#9'Descripcion,'
      #9#9'CodigoConcesionaria,'
      #9#9'CodigoExterno,'
      #9#9'Activo'
      'FROM'
      #9#9'EntradasEstacionamiento (NOLOCK)'
      'WHERE'
      #9#9'CodigoConcesionaria = :"CodigoConcesionaria"')
    Left = 112
    Top = 72
  end
  object aqrySalidas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    DataSource = dsConcesionarias
    Parameters = <
      item
        Name = 'CodigoConcesionaria'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      #9#9'CodigoSalidaEstacionamiento,'
      #9#9'NombreCorto,'
      #9#9'Descripcion,'
      #9#9'CodigoConcesionaria,'
      #9#9'CodigoExterno,'
      #9#9'Activo'
      'FROM'
      #9#9'SalidasEstacionamiento (NOLOCK)'
      'WHERE'
      #9#9'CodigoConcesionaria = :"CodigoConcesionaria"')
    Left = 144
    Top = 72
  end
  object dsEntradas: TDataSource
    DataSet = aqryEntradas
    OnDataChange = dsEntradasDataChange
    Left = 280
    Top = 72
  end
  object dsSalidas: TDataSource
    DataSet = aqrySalidas
    OnDataChange = dsSalidasDataChange
    Left = 312
    Top = 72
  end
  object spAgregarEntradaEstacionamiento: TADOStoredProc
    Connection = DMConnections.BaseCOP
    ProcedureName = 'AgregarEntradaEstacionamiento'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEntradaEstacionamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NombreCorto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoExterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 480
    Top = 72
  end
  object spAgregarSalidaEstacionamiento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarSalidaEstacionamiento'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSalidaEstacionamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NombreCorto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoExterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 512
    Top = 72
  end
  object spObtenerConceptosComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConceptosComprobante'
    Parameters = <>
    Left = 416
    Top = 72
  end
  object spObtenerConceptosInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConceptosInfractores'
    Parameters = <>
    Left = 544
    Top = 72
  end
  object lnCheck: TImageList
    Left = 600
    Top = 72
    Bitmap = {
      494C010102000800440010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000000001000100000000
      0001000100000000000100010000000000010001000000000001000100000000
      0001000100000000000100010000000000010001000000000001000100000000
      0001000100000000000100010000000000010001000000000001000100000000
      0001000100000000000100010000000000000000000000000000000000000000
      000000000000}
  end
end
