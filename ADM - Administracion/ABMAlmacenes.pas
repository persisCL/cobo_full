{********************************** File Header ********************************
File Name   : ABMTrasnportistas
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 26-MAY-2004
Language    : ES-AR
Description : ABM de Transportistas de TAGs
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		:
Author		:
Description :
*******************************************************************************}

unit ABMAlmacenes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaTypes,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, ListBoxEx, DBListEx, Variants,
  RStrings;

type
  TFABMTransportistas = class(TForm)
	GroupB: TPanel;
	Panel2: TPanel;
	Notebook: TNotebook;
	BtnSalir: TDPSButton;
	BtnAceptar: TDPSButton;
	BtnCancelar: TDPSButton;
    tblTransportistas: TADOTable;
    txtCodigo: TNumericEdit;
	Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
	Label2: TLabel;
    Label3: TLabel;
    spActualizarTransportistas: TADOStoredProc;
	Label7: TLabel;
	Label10: TLabel;
    ListaTransportistas: TAbmList;
    dsTransportistas: TDataSource;
    txtDescripcion: TEdit;
	procedure BtnCancelarClick(Sender: TObject);
	procedure ListaTransportistasClick(Sender: TObject);
	procedure ListaTransportistasDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaTransportistasEdit(Sender: TObject);
	procedure ListaTransportistasRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure ListaTransportistasDelete(Sender: TObject);
	procedure ListaTransportistasInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function ListaTransportistasProcess(Tabla: TDataSet; var Texto: String): Boolean;
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMTransportistas: TFABMTransportistas;

implementation

{$R *.DFM}

function TFABMTransportistas.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblTransportistas]) then exit;
	Notebook.PageIndex := 0;
	ListaTransportistas.Reload;
	Result := True;
end;

procedure TFABMTransportistas.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMTransportistas.ListaTransportistasClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		:= FieldByName('CodigoTransportista').AsInteger;
	   txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
	end;
end;

procedure TFABMTransportistas.ListaTransportistasDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoTransportista').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFABMTransportistas.ListaTransportistasEdit(Sender: TObject);
begin
	ListaTransportistas.Enabled:= False;
	ListaTransportistas.Estado := modi;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion
end;

procedure TFABMTransportistas.ListaTransportistasRefresh(Sender: TObject);
begin
	 if ListaTransportistas.Empty then LimpiarCampos;
end;

procedure TFABMTransportistas.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
end;

procedure TFABMTransportistas.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

procedure TFABMTransportistas.ListaTransportistasDelete(Sender: TObject);

begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_ALMACENES]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;

				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_ALMACENES]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_ALMACENES]), MB_ICONSTOP);
			end
		end
	end;

	ListaTransportistas.Reload;
	ListaTransportistas.Estado := Normal;
	ListaTransportistas.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFABMTransportistas.ListaTransportistasInsert(Sender: TObject);
begin
	LimpiarCampos;

    groupb.Enabled          	:= True;
	ListaTransportistas.Enabled := False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDescripcion;
end;

procedure TFABMTransportistas.BtnAceptarClick(Sender: TObject);

begin
	if not ValidateControls([txtDescripcion],
	  [(Trim(txtDescripcion.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_ALMACENES]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
		Exit;
	end;

	with ListaTransportistas do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarTransportistas, Parameters do begin
					ParamByName('@CodigoTransportista').Value := txtCodigo.ValueInt;
					ParamByName('@Descripcion').Value := Trim(txtDescripcion.Text);

					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_ALMACENES]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_ALMACENES]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TFABMTransportistas.FormShow(Sender: TObject);
begin
	ListaTransportistas.Reload;
end;

procedure TFABMTransportistas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TFABMTransportistas.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMTransportistas.Volver_Campos;
begin
	ListaTransportistas.Estado := Normal;
	ListaTransportistas.Enabled:= True;

	ActiveControl       := ListaTransportistas;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaTransportistas.Reload;
end;

function TFABMTransportistas.ListaTransportistasProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Result := True;
end;


end.

