{********************************** File Header *********************************
File Name   : frmPrincipal.pas
Author      : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created: 08/11/2002
Language    : ES-AR
Description : Formulario principal del proyecto.
********************************************************************************}
unit frmPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, Grids, DBGrids, DB, ADODB,
  ToolWin, ExtCtrls, UtilProc, DPSAbout, ImgList, ActnMenus,
  ActnCtrls, StdActns, ShellApi, ppBands, ppCache, ppClass,
  ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppDB, ppDBPipe, ppCtrls,
  ppPrnabl, ppVar, Util, DPSGrid, CollPnl, StdCtrls, ppParameter;

type
  TFiltro = record
    SeveridadEvento: (Todos, Baja, Normal, Media, Alta, Extrema);
    CodigoGrupoPuntosDeAccesoEvento: integer;
    TipoDeEvento: AnsiString;
  end;

type
  TFormPrincipal = class(TForm)
    MainMenu: TMainMenu;
    StatusBar: TStatusBar;
    mnuSistema: TMenuItem;
    mnuSistemaSalir: TMenuItem;
    mnuEvento: TMenuItem;
    mnuReporte: TMenuItem;
    mnuAyuda: TMenuItem;
    mnuAyudaDPSEnLaWeb: TMenuItem;
    mnuSeparador3: TMenuItem;
    mnuAyudaAcercaDe: TMenuItem;
    mnuEventoAtender: TMenuItem;
    mnuEventoBuscar: TMenuItem;
    mnuEventoFiltrar: TMenuItem;
    Timer: TTimer;
    DataSourceEventos: TDataSource;
    ADMIN_GuardarEventoAtendido: TADOStoredProc;
    ADMIN_BuscarPersona: TADOStoredProc;
    ADMIN_BuscarEventos: TADOStoredProc;
    ImageListMenu: TImageList;
    ToolBar: TToolBar;
    RBInterface: TRBInterface;
    rptEventos: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand: TppFooterBand;
    ppDBPipeline: TppDBPipeline;
    labTitulo: TppLabel;
    imgLogo: TppImage;
    txtDescripcionPuntoAcceso: TppDBText;
    txtDescripcionTipoEvento: TppDBText;
    txtFechaHoraEvento: TppDBText;
    txtPagina: TppSystemVariable;
    labPagina: TppLabel;
    Linea: TppLine;
    labFechaHoraEvento: TppLabel;
    labDescripcionPuntoAcceso: TppLabel;
    labDescripcionTipoEvento: TppLabel;
    txtFechaHora: TppSystemVariable;
    ImageListEventos: TImageList;
    ADMIN_GuardarSistemaEvento: TADOStoredProc;
    mnuMantenimiento: TMenuItem;
    mnuMantenimientoCambiarPassword: TMenuItem;
    cplDetalle: TCollapsablePanel;
    pnlEventos: TPanel;
    grdEventos: TDPSGrid;
    pnlDetalleEvento: TPanel;
    txtDescripcionEvento: TMemo;
    labDescripcionEvento: TLabel;
    pnlDetallePersona: TPanel;
    labDocumento: TLabel;
    labNombre: TLabel;
    labEmpresa: TLabel;
    txtDocumento: TEdit;
    txtNombre: TEdit;
    txtEmpresa: TEdit;
    pnlFoto: TPanel;
    imgFoto: TImage;
    txtPlanEvento: TMemo;
    labPlanEvento: TLabel;
    ADMIN_ListarSistemaConfiguracion: TADOStoredProc;
    ImageListToolbar: TImageList;
    btnSeparador1: TToolButton;
    btnEventoAtender: TToolButton;
    btnEventoFiltrar: TToolButton;
    btnEventoBuscar: TToolButton;
    btnSeparador2: TToolButton;
    labSinFoto: TLabel;
    PopupMenuEvento: TPopupMenu;
    pumEventoAtender: TMenuItem;
    procedure mnuSistemaSalirClick(Sender: TObject);
    procedure mnuEventoAtenderClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure mnuAyudaAcercaDeClick(Sender: TObject);
    procedure mnuAyudaDPSEnLaWebClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure mnuEventoBuscarClick(Sender: TObject);
    procedure mnuEventoFiltrarClick(Sender: TObject);
    procedure mnuReporteClick(Sender: TObject);
    procedure grdEventosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DataSourceEventosDataChange(Sender: TObject; Field: TField);
    procedure mnuMantenimientoCambiarPasswordClick(Sender: TObject);
    procedure PopupMenuEventoPopup(Sender: TObject);
    procedure grdEventosDblClick(Sender: TObject);
  private
    { Private declarations }
    RutaImagenes: AnsiString;
	procedure CargarEventos;
    procedure ConfigurarToolbar;
    procedure CargarDetalleEvento;
	procedure AbrirPaginaWeb(PaginaWeb: AnsiString);
    procedure GuardarEvento(TipoEvento: integer; SubTipoEvento: integer; DescripcionEvento: AnsiString);
  public
    { Public declarations }
    Filtro: TFiltro;
    function Inicializar: Boolean;
  end;

var
  formPrincipal: TFormPrincipal; CodigoUsuario: AnsiString; NombreUsuario: AnsiString; NombreCompletoUsuario: AnsiString;

implementation

uses dm, frmBuscar, frmFiltrar, frmLogin, frmCambioPassword, EventTypes;

{$R *.dfm}

procedure TFormPrincipal.mnuSistemaSalirClick(Sender: TObject);
begin
	Application.Terminate;
end;

procedure TFormPrincipal.mnuEventoAtenderClick(Sender: TObject);
resourcestring
    MSG_ERROR_ATENDER_EVENTO = 'No se ha podido marcar el evento como atendido.';
var
	i: integer;
begin
	Screen.Cursor := crHourGlass;
    // Marcamos todos los eventos seleccionados como atendidos
    // por el usuario actual.
    for i := 0 to (grdEventos.SelectedRows.Count - 1) do
    begin
		DataSourceEventos.DataSet.GotoBookmark(pointer(grdEventos.SelectedRows.Items[i]));
		if not (DataSourceEventos.DataSet.FieldByName('IdEvento_Oculto').Value = -1) then begin
		    try
    		    ADMIN_GuardarEventoAtendido.Parameters.ParamByName('@IdEvento').Value := DataSourceEventos.DataSet.FieldByName('IdEvento_Oculto').Value;
			    ADMIN_GuardarEventoAtendido.Parameters.ParamByName('@CodigoUsuarioAtencion').Value := StrToInt(CodigoUsuario);
			    ADMIN_GuardarEventoAtendido.ExecProc;
		    except
    			on e:exception do begin
					Screen.Cursor := crDefault;
	    		    MsgBoxErr(MSG_ERROR_ATENDER_EVENTO, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
					Exit;
		        end;
		    end;
        end;
    end;
	ADMIN_BuscarEventos.Requery;
    ConfigurarToolbar;
	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.TimerTimer(Sender: TObject);
var
    Bookmark: TBookmark;
begin
	Screen.Cursor := crHourGlass;
	// Actualizamos los registros cada n segundos y dejamos el cursor en donde estaba ubicado.
	try
	    Bookmark := DataSourceEventos.DataSet.GetBookmark;
    	try
	   	    ADMIN_BuscarEventos.Requery;
			if not ((DataSourceEventos.DataSet.Bof = True) and (DataSourceEventos.DataSet.Eof = True)) then begin
   			    DataSourceEventos.DataSet.GotoBookmark(Bookmark);
		    end;
    	except
	    end;
    except
    end;
    ConfigurarToolbar;
	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.mnuAyudaAcercaDeClick(Sender: TObject);
var
	f: TAboutFrm;
begin
	Screen.Cursor := crHourGlass;
	// Desplegamos la pantalla de informaci�n del sistema.
	Application.CreateForm(TAboutFrm, f);
	if (f.Inicializa(Application.Title) = True) then begin
		Screen.Cursor := crDefault;
    	f.ShowModal;
		Screen.Cursor := crHourGlass;
    end;
	f.Release;
   	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.mnuAyudaDPSEnLaWebClick(Sender: TObject);
begin
	AbrirPaginaWeb('www.dpsautomation.com');
end;

procedure TFormPrincipal.FormResize(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	// Ajustamos el tama�o de la �ltima columna para que no se vea la barra de
    // desplazamiento horizontal.
	grdEventos.Columns[3].Width := grdEventos.Width - (grdEventos.Columns[0].Width + grdEventos.Columns[1].Width + grdEventos.Columns[2].Width);
	try
		if (Round(grdEventos.Height / 33) < DataSourceEventos.DataSet.RecordCount) then begin
			grdEventos.Columns[3].Width := (grdEventos.Columns[3].Width - 17);
	    end;
    except
    end;
	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.mnuEventoBuscarClick(Sender: TObject);
resourcestring
    MSG_ERROR_TIPO_DATO_CAMPO	 = 'El tipo de dato del campo es incorrecto.';
    MSG_ERROR_TIPO_DATO_FECHA    = 'El dato ingresado no es de tipo fecha.';
    MSG_ERROR_TIPO_DATO_NUMERICO = 'El dato ingresado no es de tipo num�rico.';
    MSG_ERROR_DATO_NO_ENCONTRADO = 'El tipo de dato del campo es incorrecto.';
    MSG_ERROR_CONDICION_BUSQUEDA = 'La condici�n de b�squeda es incorrecta.';
var
	f: TFormBuscar;
    miBookmark: TBookmark;
begin
	Screen.Cursor := crHourGlass;
	// Buscamos un registro en particular seg�n la selecci�n que halla hecho el usuario.
    miBookmark := DataSourceEventos.DataSet.GetBookmark;
	Application.CreateForm(TFormBuscar, f);
	f.cbDirecciones.Value := 1;
	Screen.Cursor := crDefault;
   	if (f.ShowModal = mrOk) then begin
		case DataSourceEventos.DataSet.FieldByName(f.cbCampos.Value).DataType of
			ftString, ftMemo, ftFmtMemo, ftFixedChar, ftWideString:
            	if (f.cbCondiciones.Text = '=') then begin
                	ADMIN_BuscarEventos.Recordset.Find(f.cbCampos.Value + f.cbCondiciones.Text + '''' + f.cbValores.Text + '''', 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ADMIN_BuscarEventos.Recordset.Bookmark));
                end
                else if (f.cbCondiciones.Text = 'PARECIDO') then begin
					ADMIN_BuscarEventos.Recordset.Find(f.cbCampos.Value + ' LIKE ''*' + f.cbValores.Text + '*''', 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ADMIN_BuscarEventos.Recordset.Bookmark));
				end
                else begin
					Screen.Cursor := crDefault;
                    MsgBox(MSG_ERROR_CONDICION_BUSQUEDA, Application.Title, MB_ICONINFORMATION + MB_OK);
                    Exit;
                end;
			ftDate, ftTime, ftDateTime, ftTimeStamp:
            	if (IsValidDate(f.cbValores.Text) = true) then begin
	            	if ((f.cbCondiciones.Text = '=') or (f.cbCondiciones.Text = '>') or (f.cbCondiciones.Text = '<')) then begin
        	        	ADMIN_BuscarEventos.Recordset.Find(f.cbCampos.Value + f.cbCondiciones.Text + '#' + f.cbValores.Text + '#', 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ADMIN_BuscarEventos.Recordset.Bookmark));
	                end
    	            else begin
						Screen.Cursor := crDefault;
                	    MsgBox(MSG_ERROR_CONDICION_BUSQUEDA, Application.Title, MB_ICONINFORMATION + MB_OK);
                        Exit;
	                end;
    			end
                else begin
					Screen.Cursor := crDefault;
               	    MsgBox(MSG_ERROR_TIPO_DATO_FECHA, Application.Title, MB_ICONINFORMATION + MB_OK);
                    Exit;
                end;
            ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD, ftAutoInc, ftFMTBcd:
            	try
                	f.cbValores.Text := IntToStr(StrToInt(f.cbValores.Text));
	            	if ((f.cbCondiciones.Text = '=') or (f.cbCondiciones.Text = '>') or (f.cbCondiciones.Text = '<')) then begin
        	        	ADMIN_BuscarEventos.Recordset.Find(f.cbCampos.Value + f.cbCondiciones.Text + ' ' + f.cbValores.Text, 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ADMIN_BuscarEventos.Recordset.Bookmark));
	                end
    	            else begin
						Screen.Cursor := crDefault;
                	    MsgBox(MSG_ERROR_CONDICION_BUSQUEDA, Application.Title, MB_ICONINFORMATION + MB_OK);
                        Exit;
	                end;
                except
					Screen.Cursor := crDefault;
               	    MsgBox(MSG_ERROR_TIPO_DATO_NUMERICO, Application.Title, MB_ICONINFORMATION + MB_OK);
                    Exit;
                end;
        	else
				Screen.Cursor := crDefault;
          	    MsgBox(MSG_ERROR_TIPO_DATO_CAMPO, Application.Title, MB_ICONINFORMATION + MB_OK);
                Exit;
        	end;
		// Se posiciona en el registro buscado si fue encontrado o en el registro
        // inicial si no fue as�.
		if (ADMIN_BuscarEventos.Recordset.EOF = true) then begin
    	    DataSourceEventos.DataSet.GotoBookmark(miBookmark);
			Screen.Cursor := crDefault;
	   	    MsgBox(MSG_ERROR_DATO_NO_ENCONTRADO, Application.Title, MB_ICONINFORMATION + MB_OK);
    	    Screen.Cursor := crHourGlass;
	    end
    	else begin
		    DataSourceEventos.DataSet.Resync([rmExact]);
    	end;
    end;
    f.Release;
	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.mnuEventoFiltrarClick(Sender: TObject);
var
	f: TFormFiltrar;
begin
	Screen.Cursor := crHourGlass;
	// Filtramos los registros seg�n la selecci�n que halla hecho el usuario.
	Application.CreateForm(TFormFiltrar, f);
	case Filtro.SeveridadEvento of
    	Todos:   f.cbSeveridades.ItemIndex := 0;
      	Baja:    f.cbSeveridades.ItemIndex := 1;
      	Normal:  f.cbSeveridades.ItemIndex := 2;
		Media:   f.cbSeveridades.ItemIndex := 3;
        Alta:    f.cbSeveridades.ItemIndex := 4;
        Extrema: f.cbSeveridades.ItemIndex := 5;
    end;
    f.cbGruposPuntosDeAcceso.Value := Filtro.CodigoGrupoPuntosDeAccesoEvento;
	f.cbTiposEventos.ItemIndex := f.cbTiposEventos.Items.IndexOf(Filtro.TipoDeEvento);
	Screen.Cursor := crDefault;
   	if (f.ShowModal = mrOk) then begin
	    Screen.Cursor := crHourGlass;
		case f.cbSeveridades.ItemIndex of
	    	0: Filtro.SeveridadEvento := Todos;
    	  	1: Filtro.SeveridadEvento := Baja;
	   		2: Filtro.SeveridadEvento := Normal;
			3: Filtro.SeveridadEvento := Media;
    	    4: Filtro.SeveridadEvento := Alta;
        	5: Filtro.SeveridadEvento := Extrema;
	    end;
    	Filtro.CodigoGrupoPuntosDeAccesoEvento := f.cbGruposPuntosDeAcceso.Items[iif((f.cbGruposPuntosDeAcceso.ItemIndex = -1), 0, f.cbGruposPuntosDeAcceso.ItemIndex)].Value;
	   	Filtro.TipoDeEvento := f.cbTiposEventos.Text;
	    CargarEventos;
		Screen.Cursor := crDefault;
   	end;
    Screen.Cursor := crHourGlass;
  	f.Release;
	Screen.Cursor := crDefault;    
end;

procedure TFormPrincipal.mnuReporteClick(Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    // Generamos el reporte con todos los eventos desplegados en pantalla.
	try
	    imgLogo.Picture.LoadFromFile(RutaImagenes + '\Logo.bmp');
    except
    end;
	ADMIN_BuscarEventos.DisableControls;
	Screen.Cursor := crDefault;
	RBInterface.Execute(True);
	ADMIN_BuscarEventos.EnableControls;
end;

procedure TFormPrincipal.grdEventosDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
	bmp: TBitmap;
begin
	if not ((DataSourceEventos.DataSet.Bof = True) and (DataSourceEventos.DataSet.Eof = True)) then begin
    	Screen.Cursor := crHourGlass;
        // Colocamos el icono que identifica al tipo de evento en la grilla.
		with (Sender as TDBGrid) do begin
			if (Column.Index = 0) then begin
				canvas.FillRect(rect);
				bmp := TBitMap.Create;
				case DataSourceEventos.DataSet.FieldByName('CodigoSeveridadTipoEvento').AsInteger of
    	        	0: ImageListEventos.GetBitmap(0, bmp);
        	      	1: ImageListEventos.GetBitmap(1, bmp);
            	  	2: ImageListEventos.GetBitmap(2, bmp);
              		3: ImageListEventos.GetBitmap(3, bmp);
	                4: ImageListEventos.GetBitmap(4, bmp);
    	        end;
				if (Rect.Right <= bmp.Width) then begin
					canvas.StretchDraw(Rect, bmp)
    	        end
				else begin
	            	canvas.Draw(Rect.left + ((Rect.Right - Rect.left) - bmp.Width) div 2, Rect.top, bmp);
    	        end;
				bmp.Free;
			end;
		end;
		Screen.Cursor := crDefault;
    end;
end;

procedure TFormPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
resourcestring
    MSG_SALIDA_USUARIO = 'El usuario %s sali� del sistema.';
begin
   	Screen.Cursor := crHourGlass;
	GuardarEvento(EVT_TYPE_SESSION, EVT_SES_END, Format(MSG_SALIDA_USUARIO, [NombreUsuario]));
	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.DataSourceEventosDataChange(Sender: TObject;
  Field: TField);
begin
   	Screen.Cursor := crHourGlass;
	ConfigurarToolbar;
    CargarDetalleEvento;
	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.mnuMantenimientoCambiarPasswordClick(
  Sender: TObject);
var
	f: TFormCambioPassword;
begin
	Screen.Cursor := crHourGlass;
	// Desplegamos la pantalla de cambio de password.
	Application.CreateForm(TFormCambioPassword, f);
	if (f.Inicializar(dm.DM3Access.DB3Access, NombreUsuario) = True) then begin
		Screen.Cursor := crDefault;
    	f.ShowModal;
		Screen.Cursor := crHourGlass;
    end;
	f.Release;
   	Screen.Cursor := crDefault;
end;

procedure TFormPrincipal.PopupMenuEventoPopup(Sender: TObject);
begin
	pumEventoAtender.Enabled := mnuEventoAtender.Enabled;
end;

procedure TFormPrincipal.grdEventosDblClick(Sender: TObject);
begin
    cplDetalle.Open := Not (cplDetalle.Open)
end;

{******************************** Function Header *******************************
Function Name: Inicializar
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 08/11/2002
Description  : Realiza la inicializaci�n de todas las variables y la
               configuraci�n visual del sistema seg�n la �ltima selecci�n del
               usuario.
Parameters   : N/A
Return Value : True:  El sistema se inicializ� correctamente.
               False: El sistema no se inicializ� correctamente.
********************************************************************************}
function TFormPrincipal.Inicializar: Boolean;
resourcestring
    MSG_INGRESO_USUARIO = 'El usuario %s ingres� del sistema.';
    MSG_ERROR_CARGAR_CONFIGURACION = 'No se ha podido cargar la configuraci�n del sistema.';
var
    f: TFormLogin;
begin
	Screen.Cursor := crHourGlass;
    Result := False;
	// Cerramos el panel en forma manual por un bug, ya que si el panel est�
    // cerrado en tiempo de dise�o, los anchors derechos de los paneles interiores
    // no son respetados.
	cplDetalle.Open := False;
	if (WindowsVersion > 5) then begin
   		cplDetalle.Style := cpsWinXP;
    end;
	Application.CreateForm(TFormLogin, f);
    Self.Show;
	if ((f.Inicializar = True) and (f.ShowModal = mrOk)) then begin
        try
		    ADMIN_ListarSistemaConfiguracion.Open;
            if not ((ADMIN_ListarSistemaConfiguracion.Eof = True) and (ADMIN_ListarSistemaConfiguracion.Bof = True)) then begin
			    RutaImagenes := ADMIN_ListarSistemaConfiguracion.FieldByName('RutaImagenesSistema').AsString;
			end
            else begin
            	RutaImagenes := '';
            end;
	    except
   			on e:exception do begin
				Screen.Cursor := crDefault;
    		    MsgBoxErr(MSG_ERROR_CARGAR_CONFIGURACION, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
				Screen.Cursor := crHourGlass;
	        end;
   		end;
		mnuAyuda.Visible := not (InstallIni.ReadBool('Configuraci�n', 'VersionOEM', False));
	    Filtro.SeveridadEvento := Todos;
    	Filtro.CodigoGrupoPuntosDeAccesoEvento := -1;
	    Filtro.TipoDeEvento := '(Todos)';
        NombreUsuario := f.NombreUsuario;
        CodigoUsuario := f.CodigoUsuario;
        StatusBar.SimpleText := ' Usuario: ' + NombreUsuario;
		GuardarEvento(EVT_TYPE_SESSION, EVT_SES_START, Format(MSG_INGRESO_USUARIO, [NombreUsuario]));
		CargarEventos;
		Timer.Interval := (1000 * InstallIni.ReadInteger('Configuraci�n', 'CantSegundosEspera', 20));
		Timer.Enabled := True;
        Result := True;
	end;
    f.Release;
    Screen.Cursor := crDefault;
end;

{******************************** Function Header *******************************
Function Name: CargarEventos
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 08/11/2002
Description  : Busca en la base de datos los tipos de eventos seleccionados.
Parameters   : N/A
Return Value : N/A
********************************************************************************}
procedure TFormPrincipal.CargarEventos();
resourcestring
    MSG_ERROR_CARGAR_EVENTOS = 'No se han podido cargar los eventos.';
begin
    try
        ADMIN_BuscarEventos.Close;
    	ADMIN_BuscarEventos.Parameters.ParamByName('@CodigoGrupo').Value := Filtro.CodigoGrupoPuntosDeAccesoEvento;
		case Filtro.SeveridadEvento of
    		Todos:   ADMIN_BuscarEventos.Parameters.ParamByName('@CodigoSeveridadTipoEvento').Value := Null;
      		Baja:    ADMIN_BuscarEventos.Parameters.ParamByName('@CodigoSeveridadTipoEvento').Value := 0;
	      	Normal:  ADMIN_BuscarEventos.Parameters.ParamByName('@CodigoSeveridadTipoEvento').Value := 1;
			Media:   ADMIN_BuscarEventos.Parameters.ParamByName('@CodigoSeveridadTipoEvento').Value := 2;
        	Alta:    ADMIN_BuscarEventos.Parameters.ParamByName('@CodigoSeveridadTipoEvento').Value := 3;
	        Extrema: ADMIN_BuscarEventos.Parameters.ParamByName('@CodigoSeveridadTipoEvento').Value := 4;
    	end;
	    ADMIN_BuscarEventos.Parameters.ParamByName('@DescripcionTipoEvento').Value := iif((Filtro.TipoDeEvento = '(Todos)'), '', Filtro.TipoDeEvento);
		ADMIN_BuscarEventos.Open;
        if ((DataSourceEventos.DataSet.Bof = True) and (DataSourceEventos.DataSet.Eof = True)) then begin
            pnlDetalleEvento.Visible := False;
            pnlDetallePersona.Visible := False;
		end;
        ConfigurarToolbar;
    except
    	on e:exception do begin
            pnlDetalleEvento.Visible := False;
            pnlDetallePersona.Visible := False;
			Screen.Cursor := crDefault;
	        MsgBoxErr(MSG_ERROR_CARGAR_EVENTOS, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
        end;
    end;
    FormResize(Self);
end;

{******************************** Function Header *******************************
Function Name: ConfigurarToolbar
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 08/11/2002
Description  : Habilita o deshabilita los menues y los botones de la barra de
               herramientas seg�n el tipo de operaci�n realizada.
Parameters   : N/A
Return Value : N/A
********************************************************************************}
procedure TFormPrincipal.ConfigurarToolbar();
begin
    if not ((DataSourceEventos.DataSet.bof = True) and (DataSourceEventos.DataSet.Eof = True)) then begin
		mnuEventoAtender.Enabled := iif((DataSourceEventos.DataSet.FieldByName('IdEvento_OCULTO').IsNull = True), False, True);
        mnuEventoBuscar.Enabled := True;
        mnuReporte.Enabled := True;
    end
    else begin
		mnuEventoAtender.Enabled := False;
        mnuEventoBuscar.Enabled := False;
        mnuReporte.Enabled := False;
    end;
	btnEventoAtender.Enabled := mnuEventoAtender.Enabled;
    btnEventoBuscar.Enabled := mnuEventoBuscar.Enabled;
end;

{******************************** Function Header *******************************
Function Name: GuardarEvento
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 08/11/2002
Description  : Guarda un evento en la tabla de eventos del sistema.
Parameters   : TipoEvento:        Valor que indica tipo de evento que se va a
                                  guardar.
               SubTipoEvento:     Valor que indica el subtipo de evento que se
                                  va a guardar.
               DescripcionEvento: Descripci�n del evento.
Return Value : N/A
********************************************************************************}
procedure TFormPrincipal.GuardarEvento(TipoEvento: integer; SubTipoEvento: integer; DescripcionEvento: AnsiString);
resourcestring
    MSG_ERROR_GUARDAR_EVENTO = 'No se ha podido guardar el evento.';
begin
    try
   	    ADMIN_GuardarSistemaEvento.Parameters.ParamByName('@CodigoTipoEvento').Value := TipoEvento;
    	ADMIN_GuardarSistemaEvento.Parameters.ParamByName('@CodigoSubtipoEvento').Value := SubTipoEvento;
    	ADMIN_GuardarSistemaEvento.Parameters.ParamByName('@DetalleEvento').Value := DescripcionEvento;
	    ADMIN_GuardarSistemaEvento.ExecProc;
    except
   		on e:exception do begin
			Screen.Cursor := crDefault;
    	    MsgBoxErr(MSG_ERROR_GUARDAR_EVENTO, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
        end;
   	end;
end;

{******************************** Function Header *******************************
Function Name: AbrirPaginaWeb
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 08/11/2002
Description  : Muestra una pagina web en el navegador de internet
               predeterminado.
Parameters   : PaginaWeb: Direcci�n de la p�gina web a mostrar.
Return Value : N/A
********************************************************************************}
procedure TFormPrincipal.AbrirPaginaWeb(PaginaWeb: AnsiString);
resourcestring
    MSG_ERROR_FNF              = 'El archivo no fue encontrado.';
    MSG_ERROR_OOM              = 'Windows no tiene suficiente memoria para completar la operaci�n.';
    MSG_ERROR_PNF              = 'La ruta no fue encontrada.';
    MSG_ERROR_SHARE            = 'Ocurri� un error de compartici�n.';
    MSG_ERROR_NOASSOC          = 'No hay ning�n programa asociado con el tipo de archivo especificado.';
    MSG_ERROR_DDEBUSY          = 'La acci�n DDE no puede ser ejecutada porque existen otras acciones DDE que est� siendo procesadas.';
    MSG_ERROR_DDEFAIL          = 'La transacci�n DDE fall�.';
    MSG_ERROR_DDETIMEOUT       = 'La transacci�n DDE no fue completada porque se ha excedido el tiempo especificado.';
    MSG_ERROR_DLLNOTFOUND      = 'Un archivo DLL requerido no fue encontrado.';
    MSG_ERROR_BAD_FORMAT       = 'El archivo ejecutable especificado es inv�lido.';
    MSG_ERROR_ACCESSDENIED	   = 'Acceso denegado.';
    MSG_ERROR_ASSOCINCOMPLETE  = 'La asociaci�n del archivo est� incompleta o es inv�lida.';
    MSG_ERROR_ABRIR_PAGINA_WEB = 'Ocurri� un error al abrir la p�gina web.';
var
 	Resultado: integer;
begin
	Screen.Cursor := crHourGlass;
	Resultado := ShellExecute(Handle, 'open', PChar(PaginaWeb), Nil, Nil, SW_MAXIMIZE);
   	Screen.Cursor := crDefault;
    case Resultado of
        ERROR_BAD_FORMAT: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_BAD_FORMAT, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_ACCESSDENIED: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_ACCESSDENIED, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_ASSOCINCOMPLETE: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_ASSOCINCOMPLETE, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_DDEBUSY: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_DDEBUSY, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_DDEFAIL: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_DDEFAIL, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_DDETIMEOUT: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_DDETIMEOUT, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_DLLNOTFOUND: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_DLLNOTFOUND, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_FNF: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_FNF, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_NOASSOC: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_NOASSOC, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_OOM: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_OOM, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_PNF: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_PNF, Application.Title, MB_ICONSTOP + MB_OK);
        SE_ERR_SHARE: MsgBoxErr(MSG_ERROR_ABRIR_PAGINA_WEB, MSG_ERROR_SHARE, Application.Title, MB_ICONSTOP + MB_OK);
    end;
end;

{******************************** Function Header *******************************
Function Name: CargarDetalleEvento
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 08/11/2002
Description  : Carga el detalle del evento o de la persona.
Parameters   : N/A.
Return Value : N/A
********************************************************************************}
procedure TFormPrincipal.CargarDetalleEvento();
begin
	if (DataSourceEventos.DataSet.FieldByName('IdEvento_OCULTO').IsNull = True) then begin
    	if not (DataSourceEventos.DataSet.Fields.FieldByName('IdPersona_Oculto').IsNull = True) then begin
		    try
    		    ADMIN_BuscarPersona.Parameters.ParamByName('@IdPersona').Value := DataSourceEventos.DataSet.Fields.FieldByName('IdPersona_Oculto').Value;
			    ADMIN_BuscarPersona.Open;
	            if not ((ADMIN_BuscarPersona.Eof = True) and (ADMIN_BuscarPersona.Bof = True)) then begin
					txtDocumento.Text := ADMIN_BuscarPersona.FieldByName('DescripcionTipoDocumento').AsString + ' ' + ADMIN_BuscarPersona.FieldByName('NumeroDocumentoPersona').AsString;
        	    	txtNombre.Text := ADMIN_BuscarPersona.FieldByName('ApellidoPersona').AsString + ' ' + ADMIN_BuscarPersona.FieldByName('NombrePersona').AsString;
            		txtEmpresa.Text := iif((ADMIN_BuscarPersona.FieldByName('DescripcionEmpresa').IsNull = True), '', ADMIN_BuscarPersona.FieldByName('DescripcionEmpresa').Value);
                    if (FileExists(RutaImagenes + '\p' + ADMIN_BuscarPersona.FieldByName('IdPersona').AsString) = True) then begin
					    imgFoto.Picture.LoadFromFile(RutaImagenes + '\p' + ADMIN_BuscarPersona.FieldByName('IdPersona').AsString);
	                    imgFoto.Visible := True;
    	                labSinFoto.Visible := False;
                    end
					else begin
	    	            imgFoto.Picture := Nil;
    	                imgFoto.Visible := False;
        	            labSinFoto.Visible := True;
            		end;
	            end
    	        else begin
					txtDocumento.Text := '';
            		txtNombre.Text := '';
	            	txtEmpresa.Text := '';
    	            imgFoto.Picture := Nil;
                    imgFoto.Visible := False;
                    labSinFoto.Visible := True;
        	    end;
				ADMIN_BuscarPersona.Close;
		    except
    			on e:exception do begin
					txtDocumento.Text := '';
            		txtNombre.Text := '';
	            	txtEmpresa.Text := '';
    	            imgFoto.Picture := Nil;
                    imgFoto.Visible := False;
                    labSinFoto.Visible := True;
		        end;
    		end;
			pnlDetalleEvento.Visible := False;
    	    pnlDetallePersona.Visible := True;
	    end
        else begin
			txtDescripcionEvento.Text := DataSourceEventos.DataSet.FieldByName('DescripcionTipoEvento').AsString;
			pnlDetalleEvento.Visible := True;
    	    pnlDetallePersona.Visible := False;
        end;
    end
    else begin
        txtDescripcionEvento.Text := DataSourceEventos.DataSet.FieldByName('DetalleEvento_OCULTO').AsString;
		txtPlanEvento.Text := DataSourceEventos.DataSet.FieldByName('PlanAccionTipoEvento_OCULTO').AsString;
		pnlDetalleEvento.Visible := True;
        pnlDetallePersona.Visible := False;
    end;
end;

end.
