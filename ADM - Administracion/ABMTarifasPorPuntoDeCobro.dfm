object FormTarifasPorPuntoDeCobro: TFormTarifasPorPuntoDeCobro
  Left = 212
  Top = 112
  Caption = 'Mantenimiento de Tarifas por punto de cobro'
  ClientHeight = 621
  ClientWidth = 1198
  Color = clBtnFace
  Constraints.MinHeight = 488
  Constraints.MinWidth = 783
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBOTTOM: TPanel
    Left = 0
    Top = 573
    Width = 1198
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object lblTotalRegistros: TLabel
      Left = 504
      Top = 24
      Width = 69
      Height = 13
      Caption = 'Total registros:'
    end
    object lblShowTotalRegistros: TLabel
      Left = 584
      Top = 24
      Width = 105
      Height = 13
      Caption = 'lblShowTotalRegistros'
    end
    object pbSinAsignar: TPaintBox
      Left = 12
      Top = 6
      Width = 120
      Height = 30
      Color = clWhite
      Constraints.MinHeight = 30
      Constraints.MinWidth = 120
      ParentColor = False
    end
    object nbGuardar: TNotebook
      Left = 1014
      Top = 6
      Width = 179
      Height = 40
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PagSalir'
        DesignSize = (
          179
          40)
        object btnSalir: TButton
          Left = 96
          Top = 6
          Width = 75
          Height = 27
          Anchors = [akTop, akRight]
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PagGuardar'
        object btnGuardarPlan: TButton
          Left = 7
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Guardar'
          TabOrder = 0
          OnClick = btnGuardarPlanClick
        end
        object btnCancelarPlan: TButton
          Left = 88
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarPlanClick
        end
      end
    end
  end
  object pnlTOP: TPanel
    Left = 0
    Top = 0
    Width = 1198
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object pnlConcesionaria: TPanel
      Left = 0
      Top = 0
      Width = 353
      Height = 33
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object lblConcesionaria: TLabel
        Left = 8
        Top = 7
        Width = 71
        Height = 13
        Caption = 'Concecionaria:'
      end
      object cbbConcesionaria: TVariantComboBox
        Left = 85
        Top = 6
        Width = 262
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbbConcesionaria_Change
        Items = <>
      end
    end
    object pnlCategorias: TPanel
      Left = 353
      Top = 0
      Width = 845
      Height = 33
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object lblCategoriaClases: TLabel
        Left = 6
        Top = 8
        Width = 79
        Height = 13
        Caption = 'Clase Categor'#237'a:'
      end
      object cbbClaseCategorias: TVariantComboBox
        Left = 89
        Top = 6
        Width = 156
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbbCategoriaClase_Changed
        Items = <>
      end
      object btnCrearVersion: TButton
        Left = 280
        Top = 4
        Width = 116
        Height = 22
        Caption = 'Crear Nueva Versi'#243'n'
        TabOrder = 1
        OnClick = btnCrearVersionClick
      end
      object btnGenerarPlan: TButton
        Left = 402
        Top = 4
        Width = 90
        Height = 22
        Caption = 'Generar Copia'
        TabOrder = 2
        OnClick = btnCopiarPlanClick
      end
      object btnExpToExcel: TButton
        Left = 737
        Top = 5
        Width = 100
        Height = 22
        Caption = 'Exportar a Excel'
        TabOrder = 3
        OnClick = btnExpToExcelClick
      end
      object btnEditarPlan: TButton
        Left = 498
        Top = 4
        Width = 90
        Height = 22
        Caption = 'Editar Plan'
        TabOrder = 4
        OnClick = btnEditarPlanClick
      end
      object btnEliminarPlan: TButton
        Left = 594
        Top = 4
        Width = 90
        Height = 22
        Caption = 'Eliminar Plan'
        TabOrder = 5
        OnClick = btnEliminarPlanClick
      end
    end
  end
  object pnlTarifas: TPanel
    Left = 0
    Top = 33
    Width = 1198
    Height = 158
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object dblPlanesVersiones: TDBListEx
      Left = 0
      Top = 0
      Width = 1198
      Height = 158
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Clase de Categor'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'CategoriaClase'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          Header.Caption = 'Versi'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'CodigoVersion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 110
          Header.Caption = 'Fecha Activaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaActivacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 70
          Header.Caption = 'Habilitado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = True
          FieldName = 'Habilitado'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Fecha Habilitaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaHabilitacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Fecha Autorizaci'#243'n MOP'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = True
          FieldName = 'FechaAutorizacionMOP'
        end>
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dblPlanesVersionesDrawText
      OnLinkClick = dblPlanesVersionesLinkClick
    end
  end
  object pnlDetalleTarifa: TPanel
    Left = 0
    Top = 233
    Width = 1198
    Height = 340
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object dblListaPlanTarifarioPuntoCobro: TDBListEx
      Left = 0
      Top = 0
      Width = 1198
      Height = 295
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 300
          Header.Caption = 'Punto Cobro'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripcionPuntoCobro'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Categor'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripcionCategoria'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 170
          Header.Caption = 'Tipo de D'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripcionDiaTipo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo de Tarifa'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescripcionTarifaTipo'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Hora Desde'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'HoraDesde'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Hora Hasta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'HoraHasta'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Importe'
        end>
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = dblListaPlanTarifarioPuntoCobroDrawText
    end
    object pnlPaintBoxHorario: TPanel
      Left = 0
      Top = 295
      Width = 1198
      Height = 45
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object PaintBoxHorario: TPaintBox
        Left = 12
        Top = 0
        Width = 1174
        Height = 31
        Align = alClient
        ExplicitLeft = 18
        ExplicitTop = 13
        ExplicitWidth = 764
        ExplicitHeight = 24
      end
      object pnl2: TPanel
        Left = 1186
        Top = 0
        Width = 12
        Height = 31
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
      end
      object pnl3: TPanel
        Left = 0
        Top = 0
        Width = 12
        Height = 31
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
      end
      object pnl1: TPanel
        Left = 0
        Top = 31
        Width = 1198
        Height = 14
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object lbl_24_00: TLabel
          Left = 1171
          Top = 0
          Width = 27
          Height = 14
          Align = alRight
          Caption = '23:59'
          ExplicitHeight = 13
        end
        object lbl_00_00: TLabel
          Left = 0
          Top = 0
          Width = 27
          Height = 14
          Align = alLeft
          Alignment = taRightJustify
          Caption = '00:00'
          ExplicitHeight = 13
        end
      end
    end
  end
  object pnlOpcionesDetalle: TPanel
    Left = 0
    Top = 191
    Width = 1198
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object pnlEdicion1: TPanel
      Left = 0
      Top = 0
      Width = 1198
      Height = 29
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lblPuntoCobro: TLabel
        Left = 8
        Top = 10
        Width = 62
        Height = 13
        Caption = 'Punto Cobro:'
      end
      object lblDiaTipo: TLabel
        Left = 452
        Top = 10
        Width = 45
        Height = 13
        Caption = 'Tipo D'#237'a:'
      end
      object btnElimina: TSpeedButton
        Left = 1138
        Top = 6
        Width = 23
        Height = 22
        Hint = 'Elimina registro actual'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          300033FFFFFF3333377739999993333333333777777F3333333F399999933333
          3300377777733333337733333333333333003333333333333377333333333333
          3333333333333333333F333333333333330033333F33333333773333C3333333
          330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
          333333377F33333333FF3333C333333330003333733333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnEliminaClick
      end
      object btnInserta: TSpeedButton
        Left = 1109
        Top = 6
        Width = 23
        Height = 22
        Hint = 'Inserta nuevo registro'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333FF33333333FF333993333333300033377F3333333777333993333333
          300033F77FFF3333377739999993333333333777777F3333333F399999933333
          33003777777333333377333993333333330033377F3333333377333993333333
          3333333773333333333F333333333333330033333333F33333773333333C3333
          330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
          333333333337733333FF3333333C333330003333333733333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnInsertaClick
      end
      object btnModifica: TSpeedButton
        Left = 1167
        Top = 6
        Width = 23
        Height = 22
        Hint = 'Modifica registro'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnModificaClick
      end
      object lblCategorias: TLabel
        Left = 216
        Top = 10
        Width = 50
        Height = 13
        Caption = 'Categor'#237'a:'
      end
      object lblTipoTarifa: TLabel
        Left = 692
        Top = 9
        Width = 54
        Height = 13
        Caption = 'Tipo Tarifa:'
      end
      object cbbPuntoCobro: TVariantComboBox
        Left = 76
        Top = 6
        Width = 130
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbb_PuntoCobro_Categoria_TipoDia_TipoTarifa_Changed
        Items = <>
      end
      object cbbTipoDia: TVariantComboBox
        Left = 501
        Top = 6
        Width = 175
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnChange = cbb_PuntoCobro_Categoria_TipoDia_TipoTarifa_Changed
        Items = <>
      end
      object cbbCategorias: TVariantComboBox
        Left = 271
        Top = 6
        Width = 175
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbb_PuntoCobro_Categoria_TipoDia_TipoTarifa_Changed
        Items = <>
      end
      object cbbTipoTarifa: TVariantComboBox
        Left = 755
        Top = 6
        Width = 150
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 3
        OnChange = cbb_PuntoCobro_Categoria_TipoDia_TipoTarifa_Changed
        Items = <
          item
            Caption = 'Todos'
          end
          item
            Caption = 'Alta'
            Value = 'A'
          end
          item
            Caption = 'Normal'
            Value = 'N'
          end>
      end
    end
  end
  object sdGuardarCSV: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 'Archivos Comma Separated Value|*.csv|Todos los Archivos|*.*'
    Title = 'Guardar como...'
    Left = 856
    Top = 576
  end
end
