object FormNumerosTerminal: TFormNumerosTerminal
  Left = 158
  Top = 147
  Width = 687
  Height = 522
  ActiveControl = GrillaNumerosTerminal
  Caption = 'Mantenimiento de N'#250'meros de Terminal'
  Color = clBtnFace
  Constraints.MinHeight = 450
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  Visible = True
  OnClose = FormClose
  DesignSize = (
    679
    495)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 124
    Width = 570
    Height = 13
    Caption = 
      'Ingrese los datos de los C'#243'digos de Comercio para cada una de la' +
      's empresas de Tarjetas de Cr'#233'dito'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 19
    Top = 92
    Width = 87
    Height = 13
    Caption = 'Tarjeta de Cr'#233'dito:'
  end
  object Label4: TLabel
    Left = 19
    Top = 18
    Width = 52
    Height = 13
    Caption = 'Aplicaci'#243'n:'
  end
  object Label2: TLabel
    Left = 19
    Top = 43
    Width = 90
    Height = 13
    Caption = 'Operador log'#237'stico:'
  end
  object Label5: TLabel
    Left = 19
    Top = 68
    Width = 75
    Height = 13
    Caption = 'Lugar de venta:'
  end
  object GrillaNumerosTerminal: TStringGrid
    Left = 15
    Top = 141
    Width = 649
    Height = 296
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 3
    DefaultRowHeight = 18
    FixedColor = 14732467
    FixedCols = 0
    RowCount = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 2
    OnEnter = GrillaNumerosTerminalEnter
    OnKeyPress = GrillaNumerosTerminalKeyPress
    OnSelectCell = GrillaNumerosTerminalSelectCell
    ColWidths = (
      207
      201
      205)
  end
  object cb_Tarjetas: TComboBox
    Left = 114
    Top = 87
    Width = 321
    Height = 21
    Hint = 'Tarjeta de cr'#233'dito'
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnClick = ActualizarGrilla
  end
  object Panel1: TPanel
    Left = 0
    Top = 454
    Width = 679
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook1: TNotebook
      Left = 414
      Top = 0
      Width = 265
      Height = 41
      Align = alRight
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'Default'
        object btn_Aceptar: TDPSButton
          Left = 94
          Top = 8
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btn_AceptarClick
        end
        object btn_Cancelar: TDPSButton
          Left = 174
          Top = 8
          Cancel = True
          Caption = '&Salir'
          TabOrder = 1
          OnClick = btn_CancelarClick
        end
      end
    end
  end
  object cb_AplicacionesVenta: TComboBox
    Left = 114
    Top = 11
    Width = 321
    Height = 21
    Hint = 'Aplicaci'#243'n'
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnClick = ActualizarGrilla
  end
  object cb_OperadorLogistico: TComboBox
    Left = 114
    Top = 36
    Width = 321
    Height = 21
    Hint = 'Operador Log'#237'stico'
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    OnChange = cb_OperadorLogisticoChange
  end
  object cb_LugarDeVenta: TComboBox
    Left = 114
    Top = 61
    Width = 321
    Height = 21
    Hint = 'Lugar de venta'
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    OnClick = ActualizarGrilla
  end
  object ds_CodigosComercio: TDataSource
    DataSet = ObtenerNumerosTerminal
    Left = 88
    Top = 180
  end
  object ObtenerNumerosTerminal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerNumerosTerminal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 84
    Top = 282
  end
  object ActualizarDatosNumeroTerminal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosNumeroTerminal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@NumeroTerminal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
      end>
    Left = 88
    Top = 230
  end
end
