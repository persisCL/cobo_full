{-----------------------------------------------------------------------------
 File Name: VersionesSW.pas
 Author:    gcasais
 Date Created: 02/05/2005
 Language: ES-AR
 Description: Muestra las versiones de los módulos del sistema.
-----------------------------------------------------------------------------}
unit VersionesSW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, StdCtrls, DB, ADODB, ExtCtrls, ListBoxEx, DBListEx,
  Util, UtilProc;

type
  TfrmVersionesSoftware = class(TForm)
    DBListEx1: TDBListEx;
    Panel1: TPanel;
    Button1: TButton;
    spObtenerVersionesSistemas: TADOStoredProc;
    DSVersionesSoftware: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    function Inicializa: Boolean;
  end;

var
  frmVersionesSoftware: TfrmVersionesSoftware;

implementation

{$R *.dfm}

{ TfrmVersionesSoftware }
{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:    gcasais
  Date Created: 02/05/2005
  Description: Módulo de inicialización
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmVersionesSoftware.Inicializa: Boolean;
resourcestring
    MSG_ERROR = 'Error';
    MSG_TITLE = 'Versiones de Software del Sistema Central';
Var
	S: TSize;
begin
  	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    try
        spObtenerVersionesSistemas.Open;
        Result := True;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
            Result := False;
        end;
    end;
end;

procedure TfrmVersionesSoftware.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmVersionesSoftware.Button1Click(Sender: TObject);
begin
    Release;
end;

end.
