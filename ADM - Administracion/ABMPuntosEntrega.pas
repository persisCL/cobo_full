{-----------------------------------------------------------------------------
 Unit Name: ABMPuntosEntrega
 Author:    ggomez
 Purpose:
 History:
-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------
  Revision by:    ndonadio
  Revision Date:  16/02/2005
  Description:

  Revision : 2
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
unit ABMPuntosEntrega;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes, ListBoxEx, DBListEx,
  Provider, DBClient,RStrings, DateUtils, Validate, DateEdit, IdDateTimeStamp,
  VariantComboBox, UtilRB, Grids, DBGrids,FormPuntosVenta,Clipbrd;

type

  TFormABMPuntosEntrega = class(TForm)
    ATPuntoEntrega: TAbmToolbar;
    pnl_BotonesGeneral: TPanel;
    Notebook: TNotebook;
    ObtenerPuntosEntrega: TADOStoredProc;
    ActualizarPuntoEntrega: TADOStoredProc;
    EliminarPuntoEntrega: TADOStoredProc;
    PageControl: TPageControl;
    tsGeneral: TTabSheet;
    Label2: TLabel;
	txt_descripcion: TEdit;
    dblPuntosEntrega: TAbmList;
    Label15: TLabel;
    txtCodigoPuntoEntrega: TNumericEdit;
    cbActivo: TCheckBox;
    cbDefault: TCheckBox;
    Label9: TLabel;
    Label6: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    tsPuntosVenta: TTabSheet;
    dblPuntosVenta: TDBListEx;
    dsPuntosVenta: TDataSource;
    ObtenerPuntosVenta: TADOStoredProc;
    cdsPuntosVenta: TClientDataSet;
    ActualizarPuntoVenta: TADOStoredProc;
	AlmacenesValidos: TADOQuery;
    qryGenerarAlmacenes: TADOQuery;
    tsParametrosStock: TTabSheet;
    dbgParamStock: TDBGrid;
	dsParamStock: TDataSource;
    spObtenerParametrosStockPuntoEntrega: TADOStoredProc;
    spActualizarParametrosStockPuntoEntrega: TADOStoredProc;
	Label13: TLabel;
	txtPeriodoReposicion: TNumericEdit;
    cdsParamStock: TClientDataSet;
    spObtenerParametrosStockPuntoEntregaCodigoPuntoEntrega: TIntegerField;
    spObtenerParametrosStockPuntoEntregaCategoria: TWordField;
    spObtenerParametrosStockPuntoEntregaStockDeseado: TIntegerField;
    spObtenerParametrosStockPuntoEntregaStockMinimo: TIntegerField;
    spObtenerParametrosStockPuntoEntregaConsumoEstimado: TIntegerField;
    spObtenerParametrosStockPuntoEntregaDescripcion: TStringField;
    Label5: TLabel;
    Label1: TLabel;
    bteSupervisor: TBuscaTabEdit;
    BuscaTablaSupervisor: TBuscaTabla;
    qrySupervisor: TADOQuery;
    qrySupervisorCodigoUsuario: TStringField;
    qrySupervisorCOLUMN1: TStringField;
    qryAlmProveedor: TADOQuery;
    BuscaTablaAlmProveedor: TBuscaTabla;
    BuscaTablaAlmRecepcion: TBuscaTabla;
    qryAlmRecepcion: TADOQuery;
    BuscaTablaAlmDevolucion: TBuscaTabla;
    qryAlmDevolucion: TADOQuery;
	BuscaTablaDestinoVentas: TBuscaTabla;
    qryDestinoVentas: TADOQuery;
    BuscaTablaDestinoComodato: TBuscaTabla;
    qryDestinoComodato: TADOQuery;
    bteAlmProveedor: TBuscaTabEdit;
    bteAlmRecepcion: TBuscaTabEdit;
    bteAlmDevolucion: TBuscaTabEdit;
    bteDestinoVentas: TBuscaTabEdit;
    bteDestinoComodato: TBuscaTabEdit;
    spBorrarPuntoVenta: TADOStoredProc;
    LFuenteReclamo: TLabel;
    cbFuenteReclamo: TVariantComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    btnAgregarPuntoVenta: TButton;
    btnEditarPuntoVenta: TButton;
    btnBorrarPuntoVenta: TButton;
    procedure dblPuntosEntregaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblPuntosEntregaEdit(Sender: TObject);
    procedure dblPuntosEntregaRefresh(Sender: TObject);
    procedure ATPuntoEntregaClose(Sender: TObject);
    procedure dblPuntosEntregaDelete(Sender: TObject);
    procedure dblPuntosEntregaInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function Dbl_CallesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure BtnCancelarClick(Sender: TObject);
    procedure dblPuntosEntregaClick(Sender: TObject);
    procedure cdsPuntosVentaAfterOpen(DataSet: TDataSet);
	procedure btnAgregarPuntoVentaClick(Sender: TObject);
    procedure btnEditarPuntoVentaClick(Sender: TObject);
    procedure btnBorrarPuntoVentaClick(Sender: TObject);
    procedure dblPuntosVentaDblClick(Sender: TObject);
    procedure dbgParamStockKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BuscaTablaSupervisorSelect(Sender: TObject; Tabla: TDataSet);
    procedure BuscaTablaDestinoVentasSelect(Sender: TObject;
      Tabla: TDataSet);
    procedure BuscaTablaDestinoComodatoSelect(Sender: TObject;
      Tabla: TDataSet);
    procedure BuscaTablaAlmProveedorSelect(Sender: TObject;
      Tabla: TDataSet);
    procedure BuscaTablaAlmDevolucionSelect(Sender: TObject;
      Tabla: TDataSet);
    procedure BuscaTablaAlmRecepcionSelect(Sender: TObject;
      Tabla: TDataSet);
    procedure bteSupervisorKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure TraerPuntosVenta;
    procedure TraerPuntosVentaParametrosStock;
    procedure HabilitarBotonesPuntosVenta;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
    procedure Limpiar_Campos;
	procedure Volver_Campos;
    procedure GrabarPuntosVenta(CodigoPuntoEntrega : integer);
    procedure GrabarPuntosVentaParametrosStock(CodigoPuntoEntrega : integer);
    procedure CargarMaestroAlmacenes(Conn: TADOConnection; Qry: TAdoQuery; TipoAlmacen : integer; CodigoAlmacen: integer = 0);
    function  ObtenerCodigoAlmacen(qry: TADOQuery) : variant;
    function  BuscarDescripcionAlmacen(Codigo: integer; Qry: TADOQuery):string;
  public
    { Public declarations }
    function Inicializar(Caption: TCaption; MDIChild: Boolean): Boolean;
  end;

  TCurrentPunto = record
                    AlmProveedor,
                    AlmRecepcion,
                    AlmDevolucion,
                    DestinoComodato,
                    DestinoVentas: integer;
                    CodSupervisor: string;
  end;

var
  FormABMPuntosEntrega: TFormABMPuntosEntrega;
  CurrentPunto: TCurrentPunto;

implementation

resourcestring
	MSG_DELETE_QUESTION			= '�Est� seguro de querer eliminar el Punto de Entrega seleccionado?' + CRLF +
									'Puede marcarlo alternativamente como NO ACTIVO para una baja l�gica.';
    MSG_DELETE_FECHA_QUESTION	= '�Est� seguro de querer eliminar la Fecha seleccionada?';
    MSG_DELETE_FECHA_ATENCION 	= 'Tiene %s convenios para ese d�a. Se perder� el registro.' ;
    MSG_DELETE_FECHA_CAPTION  	= 'Eliminar Fecha';
    MSG_DELETE_ERROR		  	= 'No se puede eliminar el Punto de Entrega. Verifique que este no se est� referenciado.';
    MSG_DELETE_CAPTION	      	= 'Eliminar Punto de Entrega';
    MSG_ACTUALIZAR_ERROR      	= 'No se pudieron actualizar los datos del Punto de Entrega seleccionado.';
    MSG_ACTUALIZAR_FECHA_ERROR	= 'No se pudieron actualizar los datos de la Fecha seleccionada.';
    MSG_ACTUALIZAR_CITA_ERROR	= 'No se pudieron actualizar los datos de Cita Seleccionada.';
    MSG_ACTUALIZAR_CAPTION	  	= 'Actualizar Punto Venta';
    MSG_ACTUALIZAR_FECHA_CAPTION= 'Actualizar Fecha';
    MSG_INSERT_FECHA_ERROR    	= 'No se pudo insertar la Fecha.';
	MSG_INSERT_CITA_ERROR    	= 'No se pudo insertar la Fecha.';
	MSG_ERROR_INSERT_POS    	= 'Error al agregar Punto de Venta.';
	MSG_DELETE_POS		    	= 'Error al borrar Punto de Venta.';
	MSG_DESCRIPCION           	= 'Debe ingresar el descripci�n';
    MSG_CAPTION_FECHA         	= 'Edici�n de Fechas.';
    MSG_PROVEEDOR       		= 'Debe ingresar el almac�n proveedor';
    MSG_RECEPCION       		= 'Debe ingresar el almac�n recepci�n';
    MSG_DEVOLUCION				= 'Debe ingresar el almac�n devoluci�n';
    MSG_VENTAS       			= 'Debe ingresar el destino de ventas';
    MSG_COMODATO       			= 'Debe ingresar el destino de comodato';


{$R *.DFM}

function TFormABMPuntosEntrega.Inicializar(Caption: TCaption; MDIChild: Boolean): Boolean;

    {-----------------------------------------------------------------------------
      Function Name: CargarFuentesReclamo
      Author:    lgisuk
      Date Created: 20/04/2005
      Description: cargo las fuentes del reclamo
      Parameters: Combo:TVariantComboBox
      Return Value: None
    -----------------------------------------------------------------------------}
    procedure CargarFuentesReclamo(Combo:TVariantComboBox);
    const
        CONST_NINGUNO = '(Seleccionar)';
    var
        SP: TAdoStoredProc;
    begin
        try
            Combo.Items.Add(CONST_NINGUNO, 0);
            try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerFuentesReclamo';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('CodigoFuenteReclamo').asinteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex:=0;
            except
            end;
        finally
            FreeAndNil(SP);
        end;
    end;

Var
	S: TSize;
    CurrCursor: TCursor;
begin
    CurrCursor := Screen.Cursor ;
    try

        Screen.Cursor := crHourGlass;
         //PageControl.Visible := False;
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;
        dblPuntosEntrega.Estado:=Normal;
        self.Caption := Caption;

        ObtenerPuntosEntrega.Close;
        ObtenerPuntosEntrega.Open;
		cdsPuntosVenta.Close;
        cdsPuntosVenta.CreateDataSet;
    	cdsParamStock.Close;
	    cdsParamStock.CreateDataSet;

        //cargo los usuarios marcando que no tiene ninguno
        qrySupervisor.Open;

        //Cargo los queries de los almacenes...
        CargarMaestroAlmacenes(DMConnections.BaseCAC, qryAlmProveedor,1);
        CargarMaestroAlmacenes(DMConnections.BaseCAC, qryAlmRecepcion,2);
        CargarMaestroAlmacenes(DMConnections.BaseCAC, qryAlmDevolucion,5);
        CargarMaestroAlmacenes(DMConnections.BaseCAC, qryDestinoVentas,3);
        CargarMaestroAlmacenes(DMConnections.BaseCAC, qryDestinoComodato,4);

        //Cargo las fuentes de reclamo
        CargarFuentesReclamo(CBFuenteReclamo);
        cbfuentereclamo.Value:=0;

        Volver_Campos;
       	Notebook.PageIndex := 0;
        Result := True;
    finally
        PageControl.ActivePageIndex := 0;
        dblPuntosEntrega.Reload;
        Screen.Cursor := CurrCursor;//crDefault;
        //PageControl.Enabled := False;
    end;
end;

procedure TFormABMPuntosEntrega.dblPuntosEntregaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
    if not Tabla.FieldByName('Activo').AsBoolean then Sender.Canvas.Font.Color := clRed;
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top,FieldByName('CodigoPuntoEntrega').AsString);
        TextOut(Cols[1], Rect.Top,FieldByName('Descripcion').AsString);
        TextOut(Cols[2], Rect.Top,iif(Tabla.FieldByName('PuntoEntregaDefault').AsBoolean,MSG_SI,MSG_NO));
    end;
end;

procedure TFormABMPuntosEntrega.dblPuntosEntregaEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    txt_Descripcion.setFocus;
end;

procedure TFormABMPuntosEntrega.dblPuntosEntregaRefresh(Sender: TObject);
begin
	if dblPuntosEntrega.Empty then Limpiar_Campos;
end;


procedure TFormABMPuntosEntrega.ATPuntoEntregaClose(Sender: TObject);
begin
     close;
end;

procedure TFormABMPuntosEntrega.dblPuntosEntregaDelete(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        if MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
            IDYES then begin
            try
                with EliminarPuntoEntrega do begin
                    Parameters.ParamByName('@CodigoPuntoEntrega').Value := dblPuntosEntrega.Table.FieldByName('CodigoPuntoEntrega').AsInteger;
                    ExecProc;
                    close;
                end;
            Except
                On E: Exception do begin
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
    finally
        Volver_Campos;
		dblPuntosEntrega.Reload;
        dblPuntosEntrega.Estado:=Normal;
        dblPuntosEntrega.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

procedure TFormABMPuntosEntrega.dblPuntosEntregaInsert(Sender: TObject);
begin
	HabilitarCamposEdicion(True);
	Limpiar_Campos;
	ObtenerPuntosVenta.Close;
	txt_Descripcion.setFocus;
	TraerPuntosVentaParametrosStock;
end;

procedure TFormABMPuntosEntrega.BtnAceptarClick(Sender: TObject);
var
    CodigoPuntoEntrega: integer;
begin

    if not ValidateControls(
        [txt_descripcion,
         bteAlmProveedor,
         bteAlmRecepcion,
         bteAlmDevolucion,
		 bteDestinoVentas,
         bteDestinoComodato],
        [Trim(txt_descripcion.Text) <> '',
         trim(bteAlmProveedor.text) <> '',
         trim(bteAlmRecepcion.text) <> '',
         trim(bteAlmDevolucion.text) <> '',
         trim(bteDestinoVentas.text) <> '',
         trim(bteDestinoComodato.text) <> ''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_DESCRIPCION,
        MSG_PROVEEDOR,
        MSG_RECEPCION,
        MSG_DEVOLUCION,
        MSG_VENTAS,
        MSG_COMODATO]) then exit;

  	Screen.Cursor := crHourGlass;
    Try
        Try
            DMConnections.BaseCAC.BeginTrans;
            with ActualizarPuntoEntrega do begin // actualizo Puntos de Entrega
                close;
                parameters.Refresh;
                if dblPuntosEntrega.Estado = Alta then
                    Parameters.ParamByName('@CodigoPuntoEntrega').Value := -1
                else
					Parameters.ParamByName('@CodigoPuntoEntrega').Value             := dblPuntosEntrega.Table.FieldByName('CodigoPuntoEntrega').AsInteger;
                    Parameters.ParamByName('@Descripcion').Value                    := Trim(txt_descripcion.Text);
                    Parameters.ParamByName('@PeriodoReposicion').Value              := txtPeriodoReposicion.Value;
                    Parameters.ParamByName('@OrdenMostrar').Value                   := 1;
                    Parameters.ParamByName('@CodigoAlmacenRecepcion').Value         := CurrentPunto.AlmRecepcion;
                    Parameters.ParamByName('@CodigoAlmacenDevolucion').Value        := CurrentPunto.AlmDevolucion ;
                    Parameters.ParamByName('@CodigoAlmacenProveedor').Value         := CurrentPunto.AlmProveedor ;
                    Parameters.ParamByName('@CodigoAlmacenDestinoVentas').Value     := CurrentPunto.DestinoVentas ;
                    Parameters.ParamByName('@CodigoAlmacenDestinoComodato').Value   := CurrentPunto.DestinoComodato ;
                    Parameters.ParamByName('@CodigoUsuarioSupervisor').Value        := iif( length(trim(bteSupervisor.Text)) > 0, CurrentPunto.CodSupervisor ,null);
                    Parameters.ParamByName('@PuntoEntregaDefault').Value            := cbDefault.Checked;
                    Parameters.ParamByName('@Activo').Value                         := cbActivo.Checked;
                    Parameters.ParamByName('@CodigoFuenteReclamo').Value            := cbFuenteReclamo.value;
                    ExecProc;
                    CodigoPuntoEntrega:=Parameters.ParamByName('@CodigoPuntoEntrega').Value;
            end;

			GrabarPuntosVenta(CodigoPuntoEntrega);
			GrabarPuntosVentaParametrosStock(CodigoPuntoEntrega);
            DMConnections.BaseCAC.CommitTrans;
        except
            On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
            end;
        end;
	finally
        HabilitarCamposEdicion(False); // deshabilitos los combos
        dblPuntosEntrega.Reload;
        dblPuntosEntrega.Estado:=Normal;
        dblPuntosEntrega.SetFocus;
        Screen.Cursor  := crDefault;
//        PageControl.Enabled := False;
    end;
end;

procedure TFormABMPuntosEntrega.GrabarPuntosVenta(CodigoPuntoEntrega : integer);
var
     CodigoPuntoVenta : string;
begin
    with cdsPuntosVenta do begin
        First;
        while not(eof) do begin
            ActualizarPuntoVenta.Parameters.ParamByName('@CodigoPuntoEntrega').Value := CodigoPuntoEntrega;
            ActualizarPuntoVenta.Parameters.ParamByName('@CodigoPuntoVenta').Value :=  iif( FieldbyName('CodigoPuntoVenta').asinteger > 0, FieldbyName('CodigoPuntoVenta').asinteger, -1);
            ActualizarPuntoVenta.Parameters.ParamByName('@Descripcion').Value := Trim(FieldbyName('Descripcion').Value);
            if cdsPuntosVenta.FieldByName('NumeroPos').AsInteger > 0 then begin
                ActualizarPuntoVenta.Parameters.ParamByName('@NumeroPOS').Value := FieldByName('NumeroPos').AsInteger;
            end
            else begin
                ActualizarPuntoVenta.Parameters.ParamByName('@NumeroPOS').Value := Null;
			end;
            ActualizarPuntoVenta.ExecProc;
            CodigoPuntoVenta := ActualizarPuntoVenta.Parameters.ParamByName('@CodigoPuntoVenta').Value;
            Next;
        end;
    end;
    HabilitarBotonesPuntosVenta;
end;

procedure TFormABMPuntosEntrega.GrabarPuntosVentaParametrosStock(CodigoPuntoEntrega : integer);
begin
    with cdsParamStock do begin
        First;
        while not EoF do begin
            spActualizarParametrosStockPuntoEntrega.Close;
            with spActualizarParametrosStockPuntoEntrega.Parameters do begin
                Refresh;
                ParamByName('@CodigoPuntoEntrega').Value := CodigoPuntoEntrega;
                ParamByName('@Categoria').Value := FieldByName('Categoria').AsInteger;
				ParamByName('@StockDeseado').Value := FieldByName('StockDeseado').AsInteger;
				ParamByName('@StockMinimo').Value := FieldByName('StockMinimo').AsInteger;
				ParamByName('@ConsumoEstimado').Value := FieldByName('ConsumoEstimado').AsInteger;
            end;
            spActualizarParametrosStockPuntoEntrega.ExecProc;

			next;
        end;
    end;
end;


procedure TFormABMPuntosEntrega.FormShow(Sender: TObject);
begin
   	dblPuntosEntrega.Reload;
end;

procedure TFormABMPuntosEntrega.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    action := caFree;
end;

procedure TFormABMPuntosEntrega.BtnSalirClick(Sender: TObject);
begin
    close;
end;

function TFormABMPuntosEntrega.Dbl_CallesProcess(Tabla: TDataSet;
var
    Texto: String): Boolean;
begin
	Result := True;
end;

procedure TFormABMPuntosEntrega.BtnCancelarClick(Sender: TObject);
begin
    HabilitarCamposEdicion(False);
    Limpiar_Campos;
    dblPuntosEntrega.Estado:=Normal;
    HabilitarBotonesPuntosVenta;
    dblPuntosEntrega.setfocus;
//    PageControl.Enabled := False;
end;

procedure TFormABMPuntosEntrega.dblPuntosEntregaClick(Sender: TObject);
begin

    txtCodigoPuntoEntrega.Value:= ObtenerPuntosEntrega.fieldbyname('CodigoPuntoEntrega').AsInteger;
    txt_descripcion.Text:=trim(ObtenerPuntosEntrega.fieldbyname('Descripcion').AsString);
    txtPeriodoReposicion.Value:=ObtenerPuntosEntrega.fieldbyname('PeriodoReposicion').AsInteger;
    if ObtenerPuntosEntrega.fieldbyname('CodigoUsuarioSupervisor').IsNull then
         bteSupervisor.Text := ''
	else bteSupervisor.Text := TRIM(qrySupervisor.Lookup('CodigoUsuario',ObtenerPuntosEntrega.fieldbyname('CodigoUsuarioSupervisor').asString, 'Nombre'));

    if ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenProveedor').IsNull then
         bteAlmProveedor.text := ''
	else bteAlmProveedor.text := BuscarDescripcionAlmacen(ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenProveedor').AsInteger, qryAlmProveedor);

    if ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenRecepcion').IsNull then
         bteAlmRecepcion.text := ''
    else bteAlmRecepcion.text := BuscarDescripcionAlmacen(ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenRecepcion').AsInteger, qryAlmRecepcion);

    if ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDevolucion').IsNull then
         bteAlmDevolucion.text := ''
    else bteAlmDevolucion.text := BuscarDescripcionAlmacen(ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDevolucion').AsInteger, qryAlmDevolucion);

    if ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDestinoVentas').IsNull then
         bteDestinoVentas.text := ''
    else bteDestinoVentas.text := BuscarDescripcionAlmacen(ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDestinoVentas').AsInteger, qryDestinoVentas);

    if ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDestinoComodato').IsNull then
         bteDestinoComodato.text := ''
    else bteDestinoComodato.text := BuscarDescripcionAlmacen(ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDestinoComodato').AsInteger, qryDestinoComodato);

    cbfuentereclamo.value:=ObtenerPuntosEntrega.fieldbyname('CodigoFuenteReclamo').AsInteger;

    cbActivo.Checked:=ObtenerPuntosEntrega.fieldbyname('Activo').AsBoolean;
    cbDefault.Checked:=ObtenerPuntosEntrega.fieldbyname('PuntoEntregaDefault').AsBoolean;
    TraerPuntosVenta;
	TraerPuntosVentaParametrosStock;
	dblPuntosVenta.Columns[0].Width := 80;
	dblPuntosVenta.Columns[1].Width := 300;
end;

procedure TFormABMPuntosEntrega.HabilitarCamposEdicion(habilitar : Boolean);
begin
    txt_descripcion.Enabled :=habilitar;
    cbDefault.Enabled:= habilitar;
    cbActivo.Enabled:= habilitar;
    txtPeriodoReposicion.Enabled:= habilitar;
    bteSupervisor.Enabled := habilitar;
    bteAlmProveedor.Enabled:= habilitar;
    bteAlmRecepcion.Enabled:= habilitar;
    bteAlmDevolucion.Enabled:= habilitar;
    bteDestinoVentas.Enabled:= habilitar;
    bteDestinoComodato.Enabled:= habilitar;
    // blanqueo los combos
    if NOT habilitar then begin
        bteSupervisor.Text      := '';
        bteAlmProveedor.Text    := '';
        bteAlmRecepcion.Text    := '';
        bteAlmDevolucion.Text   := '';
        bteDestinoVentas.Text   := '';
        bteDestinoComodato.Text := '';
    end
    else begin
        CurrentPunto.CodSupervisor   := (ObtenerPuntosEntrega.FieldByName('CodigoUsuarioSupervisor').asString);
        CurrentPunto.AlmProveedor    :=  (ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenProveedor').asInteger);
		CurrentPunto.AlmRecepcion    :=  (ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenRecepcion').asInteger);
        CurrentPunto.AlmDevolucion  :=  (ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDevolucion').asInteger);
        CurrentPunto.DestinoVentas   :=  (ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDestinoVentas').asInteger);
        CurrentPunto.DestinoComodato :=  (ObtenerPuntosEntrega.fieldbyname('CodigoAlmacenDestinoComodato').asInteger);
    end;
    //Campos y datos de las Fechas del punto de entrega.
    btnAgregarPuntoVenta.Enabled     := habilitar;
    btnEditarPuntoVenta.Enabled       :=(habilitar) and (cdsPuntosVenta.active) and (dblPuntosEntrega.Enabled) and (cdsPuntosVenta.RecordCount>0);
    btnBorrarPuntoVenta.Enabled       := btnEditarPuntoVenta.Enabled;

    dbgParamStock.Enabled := Habilitar;
    if Habilitar then
    	dbgParamStock.Options := dbgParamStock.Options + [dgEditing]
    else
    	dbgParamStock.Options := dbgParamStock.Options - [dgEditing];

    dblPuntosEntrega.Enabled := not(habilitar);
    ATPuntoEntrega.Enabled          := not(habilitar);
    Notebook.PageIndex       := ord(habilitar);
    cbfuentereclamo.Enabled:= Habilitar;
    PageControl.ActivePageIndex := 0;
//    PageControl.Enabled :=  habilitar;
end;

procedure TFormABMPuntosEntrega.Limpiar_Campos;
begin
	txtCodigoPuntoEntrega.Clear;
    txt_descripcion.Clear;
    txtPeriodoReposicion.Clear;
    CurrentPunto.AlmProveedor    := 0;
    CurrentPunto.AlmRecepcion    := 0;
    CurrentPunto.AlmDevolucion   := 0;
    CurrentPunto.DestinoVentas   := 0;
    CurrentPunto.DestinoComodato := 0;
    CurrentPunto.CodSupervisor   := '';
    bteSupervisor.Text      := '';
    bteAlmProveedor.Text    := '';
    bteAlmRecepcion.Text    := '';
    bteAlmDevolucion.Text   := '';
    bteDestinoVentas.Text   := '';
    bteDestinoComodato.Text := '';
    bteSupervisor.Text      := '';
    cbDefault.Checked := False;
    cbActivo.Checked := true;
    cbfuentereclamo.Value:=0;
    if cdsPuntosVenta.Active then cdsPuntosVenta.EmptyDataSet;
    if cdsParamStock.Active then cdsParamStock.EmptyDataSet;
end;

procedure TFormABMPuntosEntrega.Volver_Campos;
begin
    HabilitarCamposEdicion(false);
	Limpiar_Campos;
    dblPuntosEntrega.SetFocus;
end;

procedure TFormABMPuntosEntrega.TraerPuntosVenta;
begin
    if (not ObtenerPuntosEntrega.Active) then exit;
    with ObtenerPuntosVenta do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoPuntoEntrega').Value := ObtenerPuntosEntrega.fieldbyname('CodigoPuntoEntrega').AsInteger;
    end;
    if not OpenTables([ObtenerPuntosVenta]) then exit;
    cdsPuntosVenta.EmptyDataSet;
    while not ObtenerPuntosVenta.eof do begin
        cdsPuntosVenta.AppendRecord([ObtenerPuntosVenta.FieldByName('CodigoPuntoEntrega').asInteger,
        ObtenerPuntosVenta.FieldByName('CodigoPuntoVenta').asInteger,
        ObtenerPuntosVenta.FieldByName('Descripcion').asString,
        ObtenerPuntosVenta.FieldByName('CodigoPuntoVenta').asInteger, ObtenerPuntosVenta.FieldByName('NumeroPOS').AsInteger]);
        ObtenerPuntosVenta.next;
    end;
    cdsPuntosVenta.First;
end;


procedure TFormABMPuntosEntrega.TraerPuntosVentaParametrosStock;
begin
	if (not ObtenerPuntosEntrega.Active) then exit;

	with spObtenerParametrosStockPuntoEntrega do begin
		close;
		Parameters.Refresh;
		Parameters.ParamByName('@CodigoPuntoEntrega').Value := ObtenerPuntosEntrega.fieldbyname('CodigoPuntoEntrega').AsInteger;
	end;

	if not OpenTables([spObtenerParametrosStockPuntoEntrega]) then exit;

    cdsParamStock.EmptyDataSet;
    with spObtenerParametrosStockPuntoEntrega do begin
	    while not spObtenerParametrosStockPuntoEntrega.EoF do begin
			cdsParamStock.AppendRecord([FieldByName('CodigoPuntoEntrega').AsInteger,
										FieldByName('Categoria').AsInteger,
										FieldByName('Descripcion').AsString,
										FieldByName('StockDeseado').AsInteger,
										FieldByName('StockMinimo').AsInteger,
										FieldByName('ConsumoEstimado').AsInteger]);
        	Next
        end;
    end;
    cdsParamStock.First;
end;


procedure TFormABMPuntosEntrega.HabilitarBotonesPuntosVenta;
var ModoEdicion: boolean;
begin
    ModoEdicion := (dblPuntosEntrega.Estado=Alta) or (dblPuntosEntrega.Estado=Modi);
    btnAgregarPuntoVenta.Enabled     := ModoEdicion;
    btnEditarPuntoVenta.Enabled       := (ModoEdicion) and (cdsPuntosVenta.RecordCount > 0);
    btnBorrarPuntoVenta.Enabled       := btnEditarPuntoVenta.Enabled;
end;


procedure TFormABMPuntosEntrega.cdsPuntosVentaAfterOpen(DataSet: TDataSet);
begin
    btnEditarPuntoVenta.Enabled := ((dblPuntosEntrega.Estado = Modi) or
      (dblPuntosEntrega.Estado = Alta)) and (cdsPuntosVenta.RecordCount > 0);
    btnBorrarPuntoVenta.Enabled := btnEditarPuntoVenta.Enabled;

end;

procedure TFormABMPuntosEntrega.btnAgregarPuntoVentaClick(Sender: TObject);
var
 f : TFormPuntosDeVenta;
begin
	Application.CreateForm(TFormPuntosDeVenta,f);
	try
//		  ???????????
//        if ObtenerPuntosVenta.active then pos := ObtenerPuntosVenta.FieldByName('NumeroPos').AsInteger
//        else pos := -1;

		if f.Inicializar('','', -1) then begin
            if f.showmodal = mrOk then
            try
                with cdsPuntosVenta do begin
                    Append;
					FieldByName('DescCodigo').AsString  := '<< Nuevo >>';
					FieldByName('Descripcion').AsString := f.Descripcion;
                    FieldByName('NumeroPOS').AsInteger  := f.Pos;
                    Post;
                 end;
            except
                On E: Exception do begin
					MsgBoxErr(MSG_ERROR_INSERT_POS,e.message, MSG_ERROR_INSERT_POS, MB_ICONSTOP);
                end;
            end;
        end;
    finally
        f.Release;
        dblPuntosVenta.SetFocus;
        cdsPuntosVenta.First;
        cdsPuntosVenta.EnableControls;
        HabilitarBotonesPuntosVenta;
    end;
end;

procedure TFormABMPuntosEntrega.btnEditarPuntoVentaClick(Sender: TObject);
var
 f : TFormPuntosDeVenta;
begin
    Application.CreateForm(TFormPuntosDeVenta,f);
    try
        if f.Inicializar(cdsPuntosVenta.FieldByName('DescCodigo').asstring,
            cdsPuntosVenta.FieldByName('Descripcion').AsString,
            cdsPuntosVenta.FieldByName('NumeroPOS').AsInteger ) then begin
            if f.showmodal = mrOk then
            try
                with cdsPuntosVenta do begin
                    Edit;
                    FieldByName('Descripcion').AsString := f.Descripcion;
                    FieldByName('NumeroPOS').AsInteger  := f.Pos;
                    post;
                end;
            except
				On E: Exception do begin
					MsgBoxErr(MSG_INSERT_CITA_ERROR,e.message, MSG_INSERT_CITA_ERROR, MB_ICONSTOP);
				end;
			end;
		end;
	finally
		f.Release;
		dblPuntosVenta.SetFocus;
        cdsPuntosVenta.First;
        cdsPuntosVenta.EnableControls;
        HabilitarBotonesPuntosVenta;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBorrarPuntoVentaClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormABMPuntosEntrega.btnBorrarPuntoVentaClick(Sender: TObject);
resourcestring
    MSG_DELETE_PUNTOVENTA_QUESTION = '�Est� seguro de querer eliminar el punto de venta?';
    MSG_DELETE_PUNTOVENTA_CAPTION  = 'Eliminar Punto de Venta.';
    MSG_DELETE_ERROR = 'No es posible eliminar los puntos de ventas porque existen turnos asociados.';
begin
 try
    if  MsgBox(MSG_DELETE_PUNTOVENTA_QUESTION, MSG_DELETE_FECHA_CAPTION, MB_YESNO + MB_ICONEXCLAMATION)
        = IDYES then begin
        if QueryGetValueInt(DMConnections.BaseCAC, 'SELECT ISNULL((SELECT TOP 1 NumeroTurno FROM Turnos  WITH (NOLOCK) WHERE CodigoPuntoEntrega = ' +
			intToStr(cdsPuntosVenta.fieldByName('CodigoPuntoEntrega').asInteger) + ' AND CodigoPuntoVenta = ' + CRLF +
			intToStr(cdsPuntosVenta.fieldByName('CodigoPuntoVenta').asInteger) + '), 0)') <> 0 then
		begin
			MsgBox(MSG_DELETE_ERROR, self.caption, MB_ICONSTOP);
			exit;
		end;

		try
			with spBorrarPuntoVenta, Parameters do begin
				ParamByName ('@CodigoPuntoEntrega').Value 	:= cdsPuntosVenta.fieldByName('CodigoPuntoEntrega').AsInteger;
				ParamByName ('@CodigoPuntoVenta').Value 	:= cdsPuntosVenta.fieldByName('CodigoPuntoVenta').AsInteger;

				ExecProc
			end;

			cdsPuntosVenta.Delete
		except
			On E: Exception do begin
				MsgBoxErr(MSG_DELETE_POS,e.message, MSG_DELETE_POS, MB_ICONSTOP);
			end;
		end;
	end;
 finally
    HabilitarBotonesPuntosVenta;
 end;
end;

procedure TFormABMPuntosEntrega.dblPuntosVentaDblClick(Sender: TObject);
begin
    if btnEditarPuntoVenta.Enabled then btnEditarPuntoVenta.Click;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarMaestroAlmacenes
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Qry: TAdoQuery; TipoAlmacen : integer; CodigoAlmacen: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormABMPuntosEntrega.CargarMaestroAlmacenes(Conn: TADOConnection; Qry: TAdoQuery; TipoAlmacen : integer; CodigoAlmacen: integer = 0);
ResourceString
    MSG_ERROR = 'Se ha producido un error al cargar los almacenes'  ;
Var
    Valido : string;
begin
	try
		Qry.Connection := Conn;
        case TipoAlmacen of
            1 : Valido := ' where OT.ValidoAlmacenProveedor = 1 ';
            2 : Valido := ' where OT.ValidoAlmacenRecepcion = 1 ';
            3 : Valido := ' where OT.ValidoDestinoVenta     = 1 ';
            4 : Valido := ' where OT.ValidoDestinoComodato  = 1 ';
            5 : Valido := ' where OT.ValidoAlmacenDevolucion= 1 ';
        end;

		Qry.SQL.Text := '	select  distinct MA.CodigoAlmacen, Ma.Descripcion ' +
                        '	from MaestroAlmacenes MA  WITH (NOLOCK) '                  +
                        '   Inner Join AlmacenesOrigenOperacion AO WITH (NOLOCK) on ' +
                        '	(AO.CodigoAlmacen = MA.CodigoAlmacen) '     +
                        '	Inner Join OperacionesTags OT WITH (NOLOCK) on '          +
                        '	(OT.CodigoOperacion = AO.CodigoOperacion) ' +
                        Valido +
                        '	Union 	select distinct MA.CodigoAlmacen, Ma.Descripcion ' +
                        '	from MaestroAlmacenes MA WITH (NOLOCK) '                   +
                        '	Inner Join AlmacenesDestinoOperacion AO WITH (NOLOCK) on ' +
                        '	(AO.CodigoAlmacen = MA.CodigoAlmacen) '      +
                        '	Inner Join OperacionesTags OT WITH (NOLOCK) on '           +
                        '	(OT.CodigoOperacion = AO.CodigoOperacion) '  +
                        Valido;
		Qry.Open;

        if CodigoAlmacen > 0 then begin
            Qry.Lookup('CodigoAlmacen',CodigoAlmacen,'CodigoAlmacen');
        end;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR,e.Message,'ERROR',0)
        end;
	end;
end;


function TFormABMPuntosEntrega.ObtenerCodigoAlmacen(qry: TADOQuery): variant;
begin
    if NOT((qry.Eof) AND (qry.Bof)) then  Result := qry.FieldByName('CodigoAlmacen').AsInteger
    else  Result := null;

end;


procedure TFormABMPuntosEntrega.dbgParamStockKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    dbgParamStock.Options := dbgParamStock.Options + [dgEditing];
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
    if (key = VK_DOWN) then dbgParamStock.Options := dbgParamStock.Options - [dgEditing];
end;
procedure TFormABMPuntosEntrega.BuscaTablaSupervisorSelect(Sender: TObject;
  Tabla: TDataSet);
begin
    CurrentPunto.CodSupervisor := qrySupervisor.FieldByName('CodigoUsuario').AsString;
    bteSupervisor.Text := qrySupervisor.FieldByName('Nombre').AsString ;
end;


procedure TFormABMPuntosEntrega.BuscaTablaDestinoVentasSelect(
  Sender: TObject; Tabla: TDataSet);
begin
    CurrentPunto.DestinoVentas := ObtenerCodigoAlmacen(qryDestinoVentas);
    bteDestinoVentas.Text := qryDestinoVentas.FieldByName('Descripcion').asString;
end;

procedure TFormABMPuntosEntrega.BuscaTablaDestinoComodatoSelect(
  Sender: TObject; Tabla: TDataSet);
begin
    CurrentPunto.DestinoComodato := ObtenerCodigoAlmacen(qryDestinoComodato);
    bteDestinoComodato.Text := qryDestinoComodato.FieldByName('Descripcion').asString;
end;

procedure TFormABMPuntosEntrega.BuscaTablaAlmProveedorSelect(
  Sender: TObject; Tabla: TDataSet);
begin
    CurrentPunto.AlmProveedor := ObtenerCodigoAlmacen(qryAlmProveedor);
    bteAlmProveedor.Text := qryAlmProveedor.FieldByName('Descripcion').asString;
end;

procedure TFormABMPuntosEntrega.BuscaTablaAlmDevolucionSelect(
  Sender: TObject; Tabla: TDataSet);
begin
    CurrentPunto.AlmDevolucion := ObtenerCodigoAlmacen(qryAlmDevolucion);
    bteAlmDevolucion.Text := qryAlmDevolucion.FieldByName('Descripcion').asString;
end;

procedure TFormABMPuntosEntrega.BuscaTablaAlmRecepcionSelect(
  Sender: TObject; Tabla: TDataSet);
begin
    CurrentPunto.AlmRecepcion := ObtenerCodigoAlmacen(qryAlmRecepcion);
    bteAlmRecepcion.Text := qryAlmRecepcion.FieldByName('Descripcion').asString;

end;

function  TFormABMPuntosEntrega.BuscarDescripcionAlmacen(Codigo: integer; Qry: TADOQuery):string;
begin
    try
        Result := qry.Lookup('CodigoAlmacen',Codigo,'Descripcion');
        if qry.EOF then Result := '';
    except
        // En general aca puede fallar si el Codigo no existe en el
        // query... por tanto devuelve un string vacio...
        Result := '';
   end;
end;

procedure TFormABMPuntosEntrega.bteSupervisorKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (ORD(Key) = VK_DELETE) OR (ORD(Key) = VK_BACK) then begin
        bteSupervisor.Text := '';
        Exit;
    end;

    if ORD(Key) > 32 then Key := CHR(0);

end;

end.
