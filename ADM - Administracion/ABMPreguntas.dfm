object FormPreguntas: TFormPreguntas
  Left = 211
  Top = 107
  Width = 750
  Height = 583
  Caption = 'Mantenimiento de Preguntas de Encuesta'
  Color = clBtnFace
  Constraints.MinHeight = 550
  Constraints.MinWidth = 737
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 249
    Width = 742
    Height = 261
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 393
      Top = 105
      Width = 349
      Height = 156
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 349
        Height = 156
        Align = alClient
        Caption = 'Fuentes por Preguntas'
        TabOrder = 0
        DesignSize = (
          349
          156)
        object dbl_Categoria: TDBListEx
          Left = 8
          Top = 19
          Width = 241
          Height = 131
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Fuente Solicitud'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'FuentesSolicitud'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Probabilidad'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Probabilidad'
            end>
          DataSource = DSSolicitud
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object btn_InsertarCategoria: TButton
          Left = 262
          Top = 20
          Width = 79
          Height = 26
          Anchors = [akTop, akRight]
          Caption = '<< &Insertar'
          TabOrder = 1
          OnClick = btn_InsertarCategoriaClick
        end
        object btn_BorrarCategoria: TButton
          Left = 262
          Top = 84
          Width = 79
          Height = 26
          Anchors = [akTop, akRight]
          Caption = '>> &Borrar'
          TabOrder = 3
          OnClick = btn_BorrarCategoriaClick
        end
        object btn_ModificarCategoria: TButton
          Left = 262
          Top = 52
          Width = 79
          Height = 26
          Anchors = [akTop, akRight]
          Caption = '<> &Modificar'
          TabOrder = 2
          OnClick = btn_ModificarCategoriaClick
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 105
      Width = 393
      Height = 156
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 393
        Height = 156
        Align = alClient
        Caption = 'Items de Preguntas'
        TabOrder = 0
        DesignSize = (
          393
          156)
        object dbl_OpcionesPreguntas: TDBListEx
          Left = 8
          Top = 19
          Width = 266
          Height = 131
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 250
              Header.Caption = 'Descripcion'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'MS Sans Serif'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'Descripcion'
            end>
          DataSource = dsPregunta
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
        end
        object btn_Orden: TUpDown
          Left = 278
          Top = 21
          Width = 16
          Height = 89
          ArrowKeys = False
          Min = -1000
          Max = 1000
          TabOrder = 4
          OnClick = btn_OrdenClick
        end
        object btn_Insertar: TButton
          Left = 306
          Top = 20
          Width = 79
          Height = 26
          Anchors = [akTop, akRight]
          Caption = '<< &Insertar'
          TabOrder = 1
          OnClick = btn_InsertarClick
        end
        object btn_Borrar: TButton
          Left = 306
          Top = 84
          Width = 79
          Height = 26
          Anchors = [akTop, akRight]
          Caption = '>> &Borrar'
          TabOrder = 3
          OnClick = btn_BorrarClick
        end
        object btn_Modificar: TButton
          Left = 306
          Top = 52
          Width = 79
          Height = 26
          Anchors = [akTop, akRight]
          Caption = '<> &Modificar'
          TabOrder = 2
          OnClick = btn_ModificarClick
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 742
      Height = 105
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 742
        Height = 105
        Align = alClient
        Caption = 'Datos Grupo Preguntas'
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 27
          Width = 72
          Height = 13
          Caption = '&Descripci'#243'n:'
          FocusControl = txt_descripcion
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 10
          Top = 55
          Width = 103
          Height = 13
          Caption = '&Tipo de Pregunta:'
          FocusControl = cb_TipoPregunta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 281
          Top = 55
          Width = 109
          Height = 13
          Caption = 'Tipo de &Resultado:'
          FocusControl = cb_TipoDato
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 10
          Top = 81
          Width = 94
          Height = 13
          Caption = 'Vigencia &Desde:'
          FocusControl = txt_desde
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 282
          Top = 81
          Width = 75
          Height = 13
          Caption = 'Vigencia &Hasta:'
          FocusControl = txt_hasta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object txt_descripcion: TEdit
          Left = 115
          Top = 20
          Width = 430
          Height = 21
          Hint = 'Nombre de la calle'
          Color = 16444382
          MaxLength = 100
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object cb_TipoPregunta: TComboBox
          Left = 115
          Top = 47
          Width = 150
          Height = 21
          Hint = 'Tipo de Pregunta'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          MaxLength = 4
          TabOrder = 1
          OnChange = cb_TipoPreguntaChange
        end
        object cb_TipoDato: TComboBox
          Left = 396
          Top = 47
          Width = 150
          Height = 21
          Hint = 'Tipo de dato de la REspuesta'
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          MaxLength = 4
          TabOrder = 2
        end
        object txt_desde: TDateEdit
          Left = 116
          Top = 74
          Width = 90
          Height = 21
          AutoSelect = False
          Color = 16444382
          TabOrder = 3
          Date = -693594.000000000000000000
        end
        object txt_hasta: TDateEdit
          Left = 396
          Top = 74
          Width = 90
          Height = 21
          AutoSelect = False
          TabOrder = 4
          Date = -693594.000000000000000000
        end
      end
    end
  end
  object pnl_BotonesGeneral: TPanel
    Left = 0
    Top = 510
    Width = 742
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label6: TLabel
      Left = 35
      Top = 13
      Width = 48
      Height = 13
      Caption = 'Eliminada.'
    end
    object Label7: TLabel
      Left = 123
      Top = 13
      Width = 89
      Height = 13
      Caption = 'Fuera de Vigencia.'
    end
    object Notebook: TNotebook
      Left = 545
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
    object Panel6: TPanel
      Left = 16
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
    object Panel7: TPanel
      Left = 104
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clGreen
      TabOrder = 2
    end
  end
  object ATGrupoPreguntas: TAbmToolbar
    Left = 0
    Top = 0
    Width = 742
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = ATGrupoPreguntasClose
  end
  object Panel2: TPanel
    Left = 0
    Top = 33
    Width = 742
    Height = 216
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 3
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 742
      Height = 216
      Align = alClient
      Caption = 'Preguntas'
      TabOrder = 0
      DesignSize = (
        742
        216)
      object Dbl_Preguntas: TAbmList
        Left = 2
        Top = 40
        Width = 738
        Height = 181
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabStop = True
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWindowText
        HeaderFont.Height = -11
        HeaderFont.Name = 'MS Sans Serif'
        HeaderFont.Style = []
        SubTitulos.Sections = (
          
            #0'331'#0'Descripci'#243'n                                                ' +
            '                                         '
          #0'104'#0'Tipos de Pregunta   '
          #0'81'#0'Tipos de Dato  '
          #0'101'#0'Pregunta Realizada'
          #0'78'#0'Vigente Desde'
          #0'75'#0'Vigente Hasta')
        HScrollBar = True
        RefreshTime = 100
        Table = ObtenerPreguntas
        Style = lbOwnerDrawFixed
        ItemHeight = 14
        OnClick = Dbl_PreguntasClick
        OnProcess = Dbl_PreguntasProcess
        OnDrawItem = Dbl_PreguntasDrawItem
        OnRefresh = Dbl_PreguntasRefresh
        OnInsert = Dbl_PreguntasInsert
        OnDelete = Dbl_PreguntasDelete
        OnEdit = Dbl_PreguntasEdit
        Access = [accAlta, accBaja, accModi]
        Estado = Normal
        ToolBar = ATGrupoPreguntas
      end
      object cb_PreguntasBorradas: TCheckBox
        Left = 8
        Top = 16
        Width = 217
        Height = 17
        Caption = '&Ver tambien preguntas eliminadas.'
        TabOrder = 0
        OnClick = cb_PreguntasBorradasClick
      end
    end
  end
  object ObtenerOpcionesPreguntas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOpcionesPreguntas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 450
  end
  object dsPregunta: TDataSource
    DataSet = cdsPregunta
    Left = 95
    Top = 450
  end
  object dspPregunta: TDataSetProvider
    DataSet = ObtenerOpcionesPreguntas
    Left = 156
    Top = 450
  end
  object cdsPregunta: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Descripcion'
        Expression = 'Descripcion'
        Options = [ixExpression]
      end>
    IndexName = 'Descripcion'
    Params = <>
    ProviderName = 'dspPregunta'
    StoreDefs = True
    AfterOpen = cdsPreguntaAfterDelete
    AfterPost = cdsPreguntaAfterDelete
    AfterDelete = cdsPreguntaAfterDelete
    Left = 125
    Top = 450
    object cdsPreguntaCodigoPregunta: TIntegerField
      FieldName = 'CodigoPregunta'
    end
    object cdsPreguntaCodigoOpcionesPregunta: TSmallintField
      FieldName = 'CodigoOpcionesPregunta'
    end
    object cdsPreguntaDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
  end
  object ObtenerPreguntas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPreguntas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Baja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 32
    Top = 96
  end
  object ObtenerFuentesxPregunta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerFuentesxPregunta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 416
    Top = 408
  end
  object DSSolicitud: TDataSource
    DataSet = cdsSolicitud
    Left = 448
    Top = 408
  end
  object cdsSolicitud: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'FuentesSolicitud'
        Expression = 'FuentesSolicitud'
        Options = [ixExpression]
      end>
    IndexName = 'FuentesSolicitud'
    Params = <>
    ProviderName = 'dspSolicitud'
    StoreDefs = True
    AfterOpen = cdsSolicitudAfterDelete
    AfterPost = cdsSolicitudAfterDelete
    AfterDelete = cdsSolicitudAfterDelete
    Left = 480
    Top = 408
    object cdsSolicitudCodigoPregunta: TIntegerField
      FieldName = 'CodigoPregunta'
    end
    object cdsSolicitudCodigoFuenteSolicitud: TIntegerField
      FieldName = 'CodigoFuenteSolicitud'
    end
    object cdsSolicitudProbabilidad: TIntegerField
      FieldName = 'Probabilidad'
    end
    object cdsSolicitudFuentesSolicitud: TStringField
      FieldName = 'FuentesSolicitud'
      Size = 50
    end
  end
  object dspSolicitud: TDataSetProvider
    DataSet = ObtenerFuentesxPregunta
    Left = 511
    Top = 408
  end
  object ActualizarMaestroPreguntas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMaestroPreguntas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoTipoPregunta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Baja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@FechaInicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaFin'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoPreguntaSalida'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 416
    Top = 440
  end
  object EliminarMaestroPreguntas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarMaestroPreguntas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MotivoError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 100
        Value = Null
      end>
    Left = 648
    Top = 56
  end
  object ActualizarMaestroOpcionesPreguntas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMaestroOpcionesPreguntas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 450
        Value = Null
      end
      item
        Name = '@CodigoOpcionesPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Orden'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoOpcionesPreguntaSalida'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end>
    Left = 448
    Top = 440
  end
  object ActualizarPreguntasFuenteSolicitud: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarPreguntasFuenteSolicitud;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Probabilidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 440
  end
end
