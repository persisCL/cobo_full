object FormVehiculosTipos: TFormVehiculosTipos
  Left = 181
  Top = 206
  Caption = 'Mantenimiento de Tipos de Veh'#237'culos'
  ClientHeight = 365
  ClientWidth = 584
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 584
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 584
    Height = 148
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'86'#0'C'#243'digo               '
      #0'187'#0'Descripci'#243'n                                         '
      #0'55'#0'Categor'#237'a')
    HScrollBar = True
    RefreshTime = 100
    Table = VehiculosTipos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 181
    Width = 584
    Height = 145
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 15
      Top = 38
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 15
      Top = 12
      Width = 39
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_CodigoTipoVehiculo
    end
    object Label2: TLabel
      Left = 15
      Top = 65
      Width = 61
      Height = 13
      Caption = '&Categor'#237'a:'
      FocusControl = cb_Categoria
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 15
      Top = 92
      Width = 85
      Height = 13
      Caption = 'Cat. Aco&plado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 15
      Top = 119
      Width = 54
      Height = 13
      Caption = 'Cat. &Tag:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 102
      Top = 34
      Width = 259
      Height = 21
      Hint = 'Descripci'#243'n del Tipo de Veh'#237'culo'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 40
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object txt_CodigoTipoVehiculo: TNumericEdit
      Left = 102
      Top = 8
      Width = 69
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
    end
    object cb_Categoria: TComboBox
      Left = 102
      Top = 61
      Width = 259
      Height = 21
      Hint = 'Categor'#237'a del tipo de veh'#237'culo'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object cb_Categoria_Acoplado: TVariantComboBox
      Left = 102
      Top = 88
      Width = 259
      Height = 21
      Hint = 'Categor'#237'a del tipo de veh'#237'culo con Acoplado'
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Items = <>
    end
    object cb_Categoria_Tag: TVariantComboBox
      Left = 102
      Top = 115
      Width = 259
      Height = 21
      Hint = 'Categor'#237'a del tipo de veh'#237'culo con Acoplado'
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 326
    Width = 584
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 387
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object VehiculosTipos: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'VehiculosTipos'
    Left = 242
    Top = 110
  end
end
