unit BoxFechaActivacionVersionPlanTarifario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, peaprocsCN, ADODB, Db, Util, UtilProc;

type
  TFrmFechaActivacionVersionPlanTarifario = class(TForm)
    dtFechaActivacion: TDateEdit;
    Label1: TLabel;
    btnAcepta: TButton;
    btnCancela: TButton;
    procedure btnAceptaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Function Inicializa(_fechaActivacion: TdateTime): Boolean;
  end;

var
  FrmFechaActivacionVersionPlanTarifario: TFrmFechaActivacionVersionPlanTarifario;
  dtfrmBoxFechaActivacionFecha : TDateTime;

implementation

Uses
  DMConnection;

ResourceString

	MSG_CAPTION_VALIDAR				      = 'Validar datos ingresados';
  MSG_ERROR_FECHA_INCORRECTA		  = 'La Fecha de Activación es incorrecta o es igual o anterior a la fecha actual';



{$R *.dfm}

Function TFrmFechaActivacionVersionPlanTarifario.Inicializa(_fechaActivacion: TDateTime): Boolean;
begin

  dtFechaActivacion.Date := _fechaActivacion;

  Result := True;

end;

procedure TFrmFechaActivacionVersionPlanTarifario.btnAceptaClick(Sender: TObject);
begin

  if not dtFechaActivacion.IsEmpty then begin
    if (dtFechaActivacion.Date > Date) then begin
      dtfrmBoxFechaActivacionFecha := dtFechaActivacion.Date;
      ModalResult := mrOK;
    end else begin
      MsgBox('La fecha de habilitación debe ser mayor a la fecha actual', 'Atención');
    end;
  end else begin
    MsgBox('Debe ingresar una fecha de habilitación valida ', 'Atención');
  end;

end;

end.
