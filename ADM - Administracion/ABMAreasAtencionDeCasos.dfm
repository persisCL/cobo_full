object FABMAreasAtencionDeCasos: TFABMAreasAtencionDeCasos
  Left = 107
  Top = 114
  Caption = 'Mantenimiento de '#193'reas de Atenci'#243'n de Casos'
  ClientHeight = 660
  ClientWidth = 1232
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PAbajo: TPanel
    Left = 0
    Top = 621
    Width = 1232
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Notebook: TNotebook
      Left = 1035
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object PanelOpciones: TPanel
    Left = 0
    Top = 0
    Width = 1232
    Height = 36
    Align = alTop
    BevelInner = bvRaised
    TabOrder = 1
    object btnTEditar: TSpeedButton
      Left = 90
      Top = 5
      Width = 25
      Height = 25
      Hint = 'Modificar'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      OnClick = btnTEditarClick
    end
    object btnTInsertar: TSpeedButton
      Left = 42
      Top = 5
      Width = 25
      Height = 25
      Hint = 'Insertar'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = btnTInsertarClick
    end
    object btnTEliminar: TSpeedButton
      Left = 65
      Top = 5
      Width = 25
      Height = 25
      Hint = 'Eliminar'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = btnTEliminarClick
    end
    object sbtnSalir: TSpeedButton
      Left = 11
      Top = 5
      Width = 25
      Height = 25
      Hint = 'Salir'
      Flat = True
      Glyph.Data = {
        76020000424D760200000000000036000000280000000B000000100000000100
        18000000000040020000C40E0000C40E0000000000000000000000001F000023
        00002400002800002700002A00002F00002F00002B00001F05000E0000000000
        2B0F12491214551111570F0F5510115B0C0F60080C5F0D0E5E1A185300000E00
        000000002E100C58100C6C0A056E09056C09077103077800037902057805065C
        000015000000000032100C5E0A047504007B06017A04017C0000800000850000
        8407096D00001800000000002B0E0E5A05027503008109058205017E00007E00
        0081000182040A6D0000190000000000270E115C07047700008202018100007D
        03008000018000017B000E6900001900000000002D0D116007047700007F0000
        7E00007E01018300037E000F7B00125F0000150000000000310A0D5E05037404
        017F00028200007F01028200027C9AC5FF00115C00001300000000002E0D1160
        07077304017C06027F02007C02048000007900037C010A640000190000000000
        2E1011550D0A521915561F1845211C3C1C1F3B171B4C0D0A71100A6900001500
        0000000031221C4D2C282EC0BC8BDBD663E6E34BDDE142C8CD711513470C0A63
        0000110000000000351A134028261BCCCC7EE4E045ECEB25EEF126D5D85D1B1B
        430F0F690000140000000000331A1B4E181F32B2BDA3CACD7ED3D26AD5D06DC0
        BC93111054080A69000019000000000029121548121D43172132252819302D11
        302B1627253111115F070B5E00001A00000000001F181741161C4B1318452321
        452520401A1C3B1417431115560B0F4900001400000000001500001A00002500
        002A00002D00002B00002B00002A00002B00002300000A000000}
      OnClick = BtnSalirClick
    end
    object SpeedButton2: TSpeedButton
      Left = 121
      Top = 5
      Width = 25
      Height = 25
      Enabled = False
      Flat = True
      Glyph.Data = {
        66030000424D6603000000000000360000002800000010000000110000000100
        1800000000003003000000000000000000000000000000000000F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0F0F000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000F0F0F0000000BFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBF000000000000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000FFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFF
        FFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBF000000000000BFBFBF
        FFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFFFF
        FF0000FFFFFFFF000000000000FFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFF
        FFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBF000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000F0F0F0F0F0F0F0F0F0000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0000000FFFFFF000000000000000000000000FFFFFF000000FFFFFF0000
        00F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0000000FFFFFF000000000000FFFFFF0000000000000000000000000000
        00F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000FFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFFFFFFFFFF000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0000000FFFFFF000000BFBFBFFFFFFF000000FFFFFF000000F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000FFFFFFFFFFFFFFFFFFFF
        FFFF000000000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0000000000000000000000000000000000000F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0}
    end
    object SpeedButton3: TSpeedButton
      Left = 151
      Top = 5
      Width = 25
      Height = 25
      Enabled = False
      Flat = True
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        1800000000000003000000000000000000000000000000000000F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000000000000000F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F00000
        00000000000000000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0000000000000000000F0F0F0000000F0F0F0F0F0F0
        F0F0F0808080000000000000000000000000808080F0F0F00000000000000000
        00F0F0F0F0F0F0000000F0F0F0F0F0F0000000000000F0F0F0F0F0F0F0F0F0F0
        F0F0000000000000000000000000F0F0F0F0F0F0F0F0F0000000F0F0F0000000
        808080F0F0F0F0F0F0F0F0F0F0F0F0FFFF00FFFF00808080000000F0F0F0F0F0
        F0F0F0F0F0F0F0000000808080000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0FFFF00C0C0C0000000808080F0F0F0F0F0F0F0F0F0000000000000F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0FFFF00F0F0F0000000F0F0
        F0F0F0F0F0F0F0000000000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0000000000000F0F0F0
        FFFF00F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0
        F0F0F0F0F0F0F0000000000000F0F0F0FFFF00F0F0F0F0F0F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0000000808080000000
        FFFF00FFFF00F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000808080F0F0
        F0F0F0F0F0F0F0000000F0F0F0000000808080FFFF00FFFF00FFFF00F0F0F0F0
        F0F0F0F0F0808080000000F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
        000000000000F0F0F0F0F0F0F0F0F0F0F0F0000000000000F0F0F0F0F0F0F0F0
        F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F080808000000000000000000000
        0000808080F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000}
    end
  end
  object pgcAtencionCasos: TPageControl
    Left = 0
    Top = 296
    Width = 1232
    Height = 325
    ActivePage = tsGeneral
    Align = alBottom
    TabOrder = 2
    OnChange = pgcAtencionCasosChange
    object tsGeneral: TTabSheet
      Caption = 'General'
      object GroupB: TPanel
        Left = 0
        Top = 0
        Width = 1224
        Height = 297
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Lcodigo: TLabel
          Left = 123
          Top = 9
          Width = 44
          Height = 13
          Caption = 'C'#243'digo:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Ldescripcion: TLabel
          Left = 97
          Top = 140
          Width = 72
          Height = 13
          Caption = '&Descripci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 19
          Top = 34
          Width = 150
          Height = 13
          Caption = '&Nombre '#193'rea de Atenci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 89
          Top = 60
          Width = 78
          Height = 13
          Caption = '&Responsable:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 57
          Top = 86
          Width = 110
          Height = 13
          Caption = '&Tel'#233'fono Contacto:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 131
          Top = 111
          Width = 36
          Height = 13
          Caption = '&EMail:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txtCodigo: TNumericEdit
          Left = 173
          Top = 6
          Width = 64
          Height = 21
          Color = 16444382
          Enabled = False
          TabOrder = 0
        end
        object txtDescripcion: TEdit
          Left = 175
          Top = 136
          Width = 676
          Height = 21
          MaxLength = 255
          TabOrder = 5
        end
        object txtNombreArea: TEdit
          Left = 173
          Top = 30
          Width = 390
          Height = 21
          Color = 16444382
          MaxLength = 30
          TabOrder = 1
        end
        object txtTelefono: TEdit
          Left = 173
          Top = 82
          Width = 390
          Height = 21
          Color = 16444382
          MaxLength = 100
          TabOrder = 3
        end
        object txtEmail: TEdit
          Left = 173
          Top = 109
          Width = 390
          Height = 21
          Color = 16444382
          MaxLength = 100
          TabOrder = 4
        end
        object cbbResponsable: TVariantComboBox
          Left = 173
          Top = 55
          Width = 390
          Height = 21
          Style = vcsDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 2
          OnChange = cbbResponsableChange
          Items = <>
        end
      end
    end
    object tsUsuarios: TTabSheet
      Caption = 'Usuarios'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dblUsuariosSistema: TDBListEx
        Left = 0
        Top = 0
        Width = 561
        Height = 297
        Align = alLeft
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 400
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NombreUsuario'
          end>
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
      object pnlUsuarios: TPanel
        Left = 561
        Top = 0
        Width = 67
        Height = 297
        Align = alLeft
        TabOrder = 1
        object btnAgregarUnUsuario: TButton
          Left = 16
          Top = 24
          Width = 33
          Height = 25
          Caption = '>'
          Enabled = False
          TabOrder = 0
          OnClick = btnAgregarUnUsuarioClick
        end
        object btnAgregarTodosUsuarios: TButton
          Left = 16
          Top = 55
          Width = 33
          Height = 25
          Caption = '>>'
          Enabled = False
          TabOrder = 1
          OnClick = btnAgregarTodosUsuariosClick
        end
        object btnQuitarUnUsuario: TButton
          Left = 16
          Top = 119
          Width = 33
          Height = 25
          Caption = '<'
          Enabled = False
          TabOrder = 2
          OnClick = btnQuitarUnUsuarioClick
        end
        object btnQuitarTodosUsuarios: TButton
          Left = 16
          Top = 150
          Width = 33
          Height = 25
          Caption = '<<'
          Enabled = False
          TabOrder = 3
          OnClick = btnQuitarTodosUsuariosClick
        end
      end
      object dblUsuariosAsignados: TDBListEx
        Left = 628
        Top = 0
        Width = 596
        Height = 297
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 400
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NombreUsuario'
          end>
        DragReorder = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
      end
    end
    object tsTiposCasos: TTabSheet
      Caption = 'Tipos de Casos'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dblTiposCasosSistema: TDBListEx
        Left = 0
        Top = 0
        Width = 561
        Height = 297
        Align = alLeft
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 400
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end>
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
      object pnlTiposCasos: TPanel
        Left = 561
        Top = 0
        Width = 67
        Height = 297
        Align = alLeft
        TabOrder = 1
        object btnAgregarUnTipoCaso: TButton
          Left = 16
          Top = 24
          Width = 33
          Height = 25
          Caption = '>'
          Enabled = False
          TabOrder = 0
          OnClick = btnAgregarUnTipoCasoClick
        end
        object btnAgregarTodosTipoCaso: TButton
          Left = 16
          Top = 55
          Width = 33
          Height = 25
          Caption = '>>'
          Enabled = False
          TabOrder = 1
          OnClick = btnAgregarTodosTipoCasoClick
        end
        object btnQuitarUnTipoCaso: TButton
          Left = 16
          Top = 119
          Width = 33
          Height = 25
          Caption = '<'
          Enabled = False
          TabOrder = 2
          OnClick = btnQuitarUnTipoCasoClick
        end
        object btnQuitarTodosTipoCaso: TButton
          Left = 16
          Top = 150
          Width = 33
          Height = 25
          Caption = '<<'
          Enabled = False
          TabOrder = 3
          OnClick = btnQuitarTodosTipoCasoClick
        end
      end
      object dblTiposCasosAsignados: TDBListEx
        Left = 628
        Top = 0
        Width = 596
        Height = 297
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 400
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end>
        DragReorder = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
      end
    end
  end
  object ListaAreasAtencion: TDBListEx
    Left = 0
    Top = 36
    Width = 1232
    Height = 260
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'C'#243'digo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CodigoAreaAtencionDeCaso'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        Header.Caption = #193'reas de Atenci'#243'n de Casos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreAreaAtencionDeCaso'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Responsable de '#193'rea'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Responsable'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Tel'#233'fono de Contacto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Telefono'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        Header.Caption = 'Correo Electr'#243'nico'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Email'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 300
        Header.Caption = 'Descripci'#243'n '#193'rea de Atenci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescripcionArea'
      end>
    DragReorder = True
    ParentColor = False
    TabOrder = 3
    TabStop = True
  end
end
