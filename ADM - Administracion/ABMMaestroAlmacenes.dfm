object FormABMMaestroAlmacenes: TFormABMMaestroAlmacenes
  Left = 44
  Top = 40
  Width = 872
  Height = 608
  Caption = 'Mantenimiento de Almacenes'
  Color = clBtnFace
  Constraints.MinHeight = 545
  Constraints.MinWidth = 870
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 864
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 864
    Height = 280
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'62'#0'C'#243'digo       '
      
        #0'265'#0'Descripci'#243'n                                                ' +
        '                   '
      #0'59'#0'Nominado '
      #0'165'#0'Bodega                                        '
      #0'157'#0'Supervisor                                 ')
    HScrollBar = True
    RefreshTime = 100
    Table = ObtenerMaestroAlmacenes
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnProcess = DBList1Process
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object Panel2: TPanel
    Left = 0
    Top = 535
    Width = 864
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 667
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object pgMaestroAlmacenes: TPageControl
    Left = 0
    Top = 313
    Width = 864
    Height = 222
    ActivePage = tsAlmacenes
    Align = alBottom
    TabOrder = 3
    object tsAlmacenes: TTabSheet
      Caption = 'Almacen'
      object GroupB: TPanel
        Left = 0
        Top = 1
        Width = 856
        Height = 193
        Align = alBottom
        BevelInner = bvLowered
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 9
          Top = 36
          Width = 72
          Height = 13
          Caption = '&Descripci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label15: TLabel
          Left = 9
          Top = 11
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label6: TLabel
          Left = 10
          Top = 62
          Width = 37
          Height = 13
          Caption = 'Bodega'
        end
        object Label9: TLabel
          Left = 428
          Top = 62
          Width = 50
          Height = 13
          Caption = 'Supervisor'
        end
        object txt_Descripcion: TEdit
          Left = 83
          Top = 32
          Width = 326
          Height = 21
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 50
          TabOrder = 1
        end
        object txt_CodigoAlmacen: TNumericEdit
          Left = 83
          Top = 7
          Width = 105
          Height = 21
          TabStop = False
          Color = clBtnFace
          MaxLength = 3
          ReadOnly = True
          TabOrder = 0
          Decimals = 0
        end
        object cbBodega: TComboBox
          Left = 83
          Top = 57
          Width = 327
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 0
          TabOrder = 3
        end
        object cbUsuarioSupervisor: TVariantComboBox
          Left = 508
          Top = 57
          Width = 327
          Height = 21
          Hint = 'Usuario supervisor'
          Style = vcsDropDownList
          ItemHeight = 13
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Items = <>
        end
        object cbSoloNominado: TCheckBox
          Left = 426
          Top = 37
          Width = 95
          Height = 15
          Alignment = taLeftJustify
          Caption = 'Solo nominado'
          TabOrder = 2
        end
        object GBDomicilios: TGroupBox
          Left = 7
          Top = 85
          Width = 842
          Height = 100
          Caption = 'Domicilio '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          inline freDomicilio: TFrameDomicilio
            Left = 2
            Top = 11
            Width = 836
            Height = 75
            HorzScrollBar.Visible = False
            VertScrollBar.Visible = False
            TabOrder = 0
            TabStop = True
            inherited Pnl_Arriba: TPanel
              Width = 836
              inherited txt_DescripcionCiudad: TEdit
                MaxLength = 50
              end
            end
            inherited Pnl_Abajo: TPanel
              Width = 836
            end
            inherited Pnl_Medio: TPanel
              Width = 836
            end
          end
        end
      end
    end
    object tsOperacionesDestino: TTabSheet
      Caption = 'Operaciones Recepci'#243'n'
      ImageIndex = 1
      object chkOperacionesRecepcion: TCheckListBox
        Left = 0
        Top = 0
        Width = 856
        Height = 194
        OnClickCheck = chkOperacionesRecepcionClickCheck
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object tsOperacionesOrigen: TTabSheet
      Caption = 'Operaciones Envio'
      ImageIndex = 2
      object chkOperacionesEnvio: TCheckListBox
        Left = 0
        Top = 0
        Width = 856
        Height = 194
        OnClickCheck = chkOperacionesEnvioClickCheck
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
  end
  object ObtenerMaestroAlmacenes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMaestroAlmacenes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 144
    Top = 96
  end
  object ActualizarMaestroAlmacenes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMaestroAlmacenes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoBodega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SoloNominado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuarioSupervisor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 232
  end
  object EliminarMaestroAlmacen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarMaestroAlmacen'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 184
  end
  object ActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarDomicilio'
    Parameters = <>
    Left = 136
    Top = 144
  end
  object ObtenerOperacionesOrigen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerAlmacenesOrigenOperaciones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 304
    Top = 96
  end
  object ObtenerOperacionesDestino: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerAlmacenesDestinoOperaciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 304
    Top = 144
  end
  object ObtenerOperacionesTags: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOperacionesTags'
    Parameters = <>
    Left = 304
    Top = 192
  end
  object ActualizarOperacionesOrigen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarAlmacenesOrigenOperacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 168
  end
  object ActualizarOperacionesDestino: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarAlmacenesDestinoOperacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 216
  end
  object ValidarAlmacenNoAsignado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ValidarAlmacenNoAsignado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ValidoAlmacenProveedor'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ValidoAlmacenRecepcion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ValidoVenta'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ValidoComodato'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 264
    Top = 432
  end
  object AlmacenValidoSinOperacion: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@CodigoAlmacen'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      '-- select * from '
      
        '-- AlmacenValidoSinOperacionDestino(:@CodigoAlmacen,:@CodigoOper' +
        'acion)'
      ''
      '')
    Left = 192
    Top = 360
  end
  object ObtenerSaldosTAGsAlmacen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerSaldosTAGsAlmacen'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 304
    Top = 248
  end
end
