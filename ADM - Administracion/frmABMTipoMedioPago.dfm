object ABMTipoMedioPagoForm: TABMTipoMedioPagoForm
  Left = 0
  Top = 0
  Caption = 'Mantenimiento de Tipos de Medios de Pagos'
  ClientHeight = 531
  ClientWidth = 844
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object tbTipoMedioPago: TAbmToolbar
    Left = 0
    Top = 0
    Width = 844
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = tbTipoMedioPagoClose
  end
  object lbTipoMedioPago: TAbmList
    Left = 0
    Top = 35
    Width = 844
    Height = 121
    TabStop = True
    TabOrder = 1
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'244'#0'Descripci'#243'n'
      #0'101'#0'Pago Autom'#225'tico'
      #0'285'#0'Concepto Pago'
      #0'82'#0'C'#243'digo Servicio')
    HScrollBar = True
    RefreshTime = 300
    Table = TipoMedioPago
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lbTipoMedioPagoClick
    OnDrawItem = lbTipoMedioPagoDrawItem
    OnRefresh = lbTipoMedioPagoRefresh
    OnInsert = lbTipoMedioPagoInsert
    OnDelete = lbTipoMedioPagoDelete
    OnEdit = lbTipoMedioPagoEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = tbTipoMedioPago
  end
  object pnlTipoMedioPago: TPanel
    Left = 0
    Top = 156
    Width = 844
    Height = 334
    Align = alBottom
    TabOrder = 2
    object lblCodigoTipoMedioPago: TLabel
      Left = 56
      Top = 18
      Width = 41
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDescripcion: TLabel
      Left = 56
      Top = 40
      Width = 68
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMedioPagoAutomatico: TLabel
      Left = 56
      Top = 67
      Width = 154
      Height = 13
      Caption = 'Medio de Pago Autom'#225'tico:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoConceptoPago: TLabel
      Left = 56
      Top = 94
      Width = 104
      Height = 13
      Caption = 'Concepto de Pago:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFuncion: TLabel
      Left = 56
      Top = 121
      Width = 46
      Height = 13
      Caption = 'Funci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoDeServicio: TLabel
      Left = 56
      Top = 148
      Width = 106
      Height = 13
      Caption = 'C'#243'digo de Servicio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoFormaPago: TLabel
      Left = 56
      Top = 172
      Width = 93
      Height = 13
      Caption = 'Formas de Pago:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFormasPago: TLabel
      Left = 598
      Top = 152
      Width = 156
      Height = 13
      Caption = 'Formas de Pago disponibles'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigoTipoMedioPago: TNumericEdit
      Left = 217
      Top = 10
      Width = 57
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
    end
    object txtDescripcion: TEdit
      Left = 217
      Top = 37
      Width = 385
      Height = 21
      Color = 16444382
      MaxLength = 30
      TabOrder = 1
    end
    object txtFuncion: TEdit
      Left = 217
      Top = 118
      Width = 385
      Height = 21
      MaxLength = 40
      TabOrder = 4
    end
    object txtCodigoDeServicio: TEdit
      Left = 217
      Top = 145
      Width = 121
      Height = 21
      MaxLength = 6
      TabOrder = 5
    end
    object cbCodigoConceptoPago: TVariantComboBox
      Left = 217
      Top = 91
      Width = 322
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 3
      Items = <>
    end
    object btnAgregarFormasPagoCanal: TButton
      Left = 511
      Top = 179
      Width = 75
      Height = 25
      Caption = '<< A&gregar'
      TabOrder = 6
      OnClick = btnAgregarFormasPagoCanalClick
    end
    object btnQuitarFormasPagoCanal: TButton
      Left = 511
      Top = 210
      Width = 75
      Height = 25
      Caption = '>> Quitar'
      TabOrder = 7
      OnClick = btnQuitarFormasPagoCanalClick
    end
    object cbMedioPagoAutomatico: TVariantComboBox
      Left = 217
      Top = 64
      Width = 145
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object dbleFormasPagoAsociadas: TDBListEx
      Left = 217
      Top = 172
      Width = 284
      Height = 150
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 175
          Header.Caption = 'Forma Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 85
          Header.Caption = 'Pago Parcial'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'AceptaPagoParcial'
        end>
      DataSource = dsFormasPagoAsociadas
      DragReorder = True
      ParentColor = False
      TabOrder = 8
      TabStop = True
      OnDblClick = dbleFormasPagoAsociadasDblClick
      OnDrawText = dbleFormasPagoAsociadasDrawText
    end
    object dbleFormasPagoNoAsociadas: TDBListEx
      Left = 596
      Top = 171
      Width = 200
      Height = 151
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 175
          Header.Caption = 'Forma Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Descripcion'
        end>
      DataSource = dsFormasPagoNoAsociadas
      DragReorder = True
      ParentColor = False
      TabOrder = 9
      TabStop = True
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 490
    Width = 844
    Height = 41
    Align = alBottom
    TabOrder = 3
    object Notebook: TNotebook
      Left = 646
      Top = 1
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 76
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 29
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object TipoMedioPago: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'TipoMedioPago'
    Left = 120
    Top = 112
  end
  object qryConceptosMovimiento: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      
        'SELECT CodigoConcepto, Descripcion FROM ConceptosMovimiento (NOL' +
        'OCK)')
    Left = 368
    Top = 120
  end
  object qryFormasPagoCanal: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTipoMedioPago'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT FormasPago.CodigoFormaPago, FormasPago.Descripcion, Forma' +
        'sPagoCanal.AceptaPagoParcial '
      'FROM FormasPagoCanal (NOLOCK) INNER JOIN FormasPago (NOLOCK)'
      #9'ON FormasPagoCanal.CodigoFormaPago = FormasPago.CodigoFormaPago'
      'WHERE FormasPagoCanal.CodigoTipoMedioPago = :CodigoTipoMedioPago')
    Left = 400
    Top = 120
  end
  object spEliminarTipoMedioPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTipoMedioPago;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 224
    Top = 112
  end
  object spActualizarTipoMedioPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTipoMedioPago;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@MedioPagoAutomatico'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoConceptoPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Funcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@CodigoDeServicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end>
    Left = 192
    Top = 112
  end
  object qryTemp: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 464
    Top = 120
  end
  object qryFormasPago: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTipoMedioPago'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT CodigoFormaPago, Descripcion '
      'FROM FormasPago (NOLOCK)'
      'WHERE CodigoFormaPago NOT IN'
      #9'(SELECT FormasPagoCanal.CodigoFormaPago'
      #9'FROM FormasPagoCanal (NOLOCK) INNER JOIN FormasPago (NOLOCK)'
      
        #9#9'ON FormasPagoCanal.CodigoFormaPago = FormasPago.CodigoFormaPag' +
        'o'
      
        #9'WHERE FormasPagoCanal.CodigoTipoMedioPago = :CodigoTipoMedioPag' +
        'o)'
      '')
    Left = 432
    Top = 120
  end
  object cdsFormasPagoAsociadas: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftSmallint
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'AceptaPagoParcial'
        DataType = ftBoolean
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    IndexFieldNames = 'Descripcion'
    Params = <>
    StoreDefs = True
    Left = 568
    Top = 120
    Data = {
      630000009619E0BD010000001800000003000000000003000000630006436F64
      69676F02000100000000000B4465736372697063696F6E010049000000010005
      5749445448020002001E00114163657074615061676F5061726369616C020003
      00000000000000}
    object cdsFormasPagoAsociadasCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object cdsFormasPagoAsociadasDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 30
    end
    object cdsFormasPagoAsociadasAceptaPagoParcial: TBooleanField
      FieldName = 'AceptaPagoParcial'
    end
  end
  object cdsFormasPagoNoAsociadas: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftSmallint
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    IndexFieldNames = 'Descripcion'
    Params = <>
    StoreDefs = True
    Left = 680
    Top = 88
    Data = {
      490000009619E0BD010000001800000002000000000003000000490006436F64
      69676F02000100000000000B4465736372697063696F6E010049000000010005
      5749445448020002001E000000}
    object cdsFormasPagoNoAsociadasCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object cdsFormasPagoNoAsociadasDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 30
    end
  end
  object dsFormasPagoAsociadas: TDataSource
    DataSet = cdsFormasPagoAsociadas
    Left = 588
    Top = 136
  end
  object dsFormasPagoNoAsociadas: TDataSource
    DataSet = cdsFormasPagoNoAsociadas
    Left = 692
    Top = 104
  end
  object Img_Tilde: TImageList
    Left = 708
    Top = 212
    Bitmap = {
      494C010102000400080010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
end
