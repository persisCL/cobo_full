unit ABMMaestroAlarmas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util, DBClient, DBCtrls, Mask, Provider, StrUtils, DSIntf, PatronBusqueda, XMLIntf, XMLDoc;


resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
    MSG_GET_DATA     = 'Cargando datos, por favor espere...';
    HINT_SALIR       = 'Salir del ABM';
    HINT_AGREGAR     = 'Agregar Nuevo Tipo de Alarma';
    HINT_ELIMINAR    = 'Eliminar Tipo de Alarma';
    HINT_EDITAR      = 'Editar Tipo de Alarma';
    ANSW_ELIMINAR    = 'Est� seguro de eliminar este Registro de Tipo de Alarma?';
    ANSW_SALIR       = 'Confirma que desea salir?';

    PARAMS_IGNORE    = '@PBL, @CodigoIncidenciaNew, @CodigoVersionPBLNew, @UsuarioCreacion, @FechaHoraCreacion, @UsuarioModificacion, @FechaHoraModificacion,';
    FIELDS_TINYINT   = 'NivelIncidencia1, NivelIncidencia2,';
    FIELDS_SMALLNT   = 'CodigoVersionPBL,';

    PROCEDURE_SELECT = 'ADM_MaestroAlarmas_SELECT';
    PROCEDURE_INSERT = 'ADM_MaestroAlarmas_INSERT';
    PROCEDURE_UPDATE = 'ADM_MaestroAlarmas_UPDATE';
    PROCEDURE_DELETE = 'ADM_MaestroAlarmas_DELETE';

    CAPTION_FORM     = 'ABM de Maestro de Alarmas';





type

  TStatusWindowHandle = type HWND;

  TClientDataSetAccess = Class(TClientDataSet);

  TfrmABMMaestroAlarmas = class(TForm)
    procSelect: TADOStoredProc;
    dsGrid: TDataSource;
    Grid: TDBGrid;
    pnlSuperior: TPanel;
    pnlControles: TPanel;
    pnlInferior: TPanel;
    Botonera: TToolBar;
    Imagenes: TImageList;
    btnSalir: TToolButton;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    btnGuardar: TButton;
    btnCancelar: TButton;
    procInsert: TADOStoredProc;
    procDelete: TADOStoredProc;
    procUpdate: TADOStoredProc;
    CDS: TClientDataSet;
    pg01: TPageControl;
    tabGeneral: TTabSheet;
    lbl01: TLabel;
    lbl02: TLabel;
    edCodIncid: TDBEdit;
    edDescripcion: TDBEdit;
    procSelectCodigoIncidencia: TIntegerField;
    procSelectCodigoVersionPBL: TSmallintField;
    procSelectDescripcion: TStringField;
    procSelectNivelIncidencia1: TWordField;
    procSelectNivelIncidencia2: TWordField;
    procSelectUsuarioCreacion: TStringField;
    procSelectFechaHoraCreacion: TDateTimeField;
    procSelectUsuarioModificacion: TStringField;
    procSelectFechaHoraModificacion: TDateTimeField;
    CDSCodigoIncidencia: TIntegerField;
    CDSCodigoVersionPBL: TSmallintField;
    CDSDescripcion: TStringField;
    CDSNivelIncidencia1: TSmallintField;
    CDSNivelIncidencia2: TSmallintField;
    CDSUsuarioCreacion: TStringField;
    CDSFechaHoraCreacion: TDateTimeField;
    CDSUsuarioModificacion: TStringField;
    CDSFechaHoraModificacion: TDateTimeField;
    lbl03: TLabel;
    qryPBL: TADOQuery;
    qryPBLCodigoVersionPBL: TSmallintField;
    qryPBLDescripcion: TStringField;
    edVersionPBL: TDBLookupComboBox;
    dsPBL: TDataSource;
    edNivel1: TDBEdit;
    edNivel2: TDBEdit;
    lbl04: TLabel;
    lbl05: TLabel;
    CDSPBL: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure PonerFocoEnPrimerControl;
    procedure ValidarTextoDeControl(Sender: TField; const Text: string);
    procedure HabilitarDeshabilitarControles(Estado : Boolean);
    procedure btnBuscarClick(Sender: TObject);
    function GetCamposPK:string;
    procedure RefrescaRegistroActual;
  private
    { Private declarations }
    CantidadRegistros           : integer;
    Posicion                    : TBookmark;
    Accion                      : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno                     : Integer;
    MensajeError                : string;
    OldCodigoIncidencia         : Integer;
    OldCodigoVersionPBL         : Integer;
    FIELDS_PK                   : string;

  public
    { Public declarations }
  published

  end;

var
  frmABMMaestroAlarmas: TfrmABMMaestroAlarmas;

implementation

uses DMConnection;

{$R *.dfm}

function CreateStatusWindow(const Text: string): TStatusWindowHandle;
var
  FormWidth,
  FormHeight: integer;
begin
  FormWidth := 400;
  FormHeight := 164;
  result := CreateWindow('STATIC',
                         PChar(Text),
                         WS_OVERLAPPED or WS_POPUPWINDOW or WS_THICKFRAME or SS_CENTER or SS_CENTERIMAGE,
                         (Screen.Width - FormWidth) div 2,
                         (Screen.Height - FormHeight) div 2,
                         FormWidth,
                         FormHeight,
                         Application.MainForm.Handle,
                         0,
                         HInstance,
                         nil);
  ShowWindow(result, SW_SHOWNORMAL);
  UpdateWindow(result);
end;

procedure RemoveStatusWindow(StatusWindow: TStatusWindowHandle);
begin
  DestroyWindow(StatusWindow);
end;

procedure TfrmABMMaestroAlarmas.RefrescaRegistroActual;
var
  Campos    : TStrings;
  i         : integer;
  spAux     : TADOStoredProc;

begin

  Campos                    := TStringList.Create;
  Campos.Clear;
  Campos.Delimiter          := ',';
  Campos.StrictDelimiter    := True;
  Campos.DelimitedText      := FIELDS_PK;

  spAux                     := TADOStoredProc.Create(nil);
  spAux.Connection          :=DMConnections.BaseBO_Master;
  spAux.ProcedureName       := 'ADM_MaestroAlarmas_SELECT_INDIVIDUAL';
  spAux.Parameters.Refresh;

  for i := 0 to Campos.Count - 2 do
    spAux.Parameters.ParamByName('@' + TRIM(Campos.Strings[i])).Value := CDS.FieldByName(Trim(Campos.Strings[i])).AsString;

  spAux.Open;

  CDS.Edit;
  for i := 0 to spAux.Fields.Count - 1 do
    CDS.FieldByName(spAux.Fields[i].FieldName).Value := spAux.FieldByName(spAux.Fields[i].FieldName).Value;
  CDS.Post;

  spAux.Close;

  FreeAndNil(spAux);
  FreeAndNil(Campos);

end;

function TfrmABMMaestroAlarmas.GetCamposPK : string;
var
  qry           : TADOQuery;
  XML, Salida   : string;
  DocXML        : IXMLDocument;
  Nodo          : IXMLNode;
begin

  qry := TADOQuery.Create(nil);
  qry.Connection := DMConnections.BaseBO_Master;
  qry.SQL.Text :=   'SELECT                                                                                                          ' +
                    '   CAST(                                                                                                        ' +
                    '       (SELECT COLUMN_NAME AS Nombre                                                                            ' +
                    '           FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A                                                          ' +
                    '               LEFT JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ON A.CONSTRAINT_NAME = B.CONSTRAINT_NAME  ' +
                    '           where A.TABLE_SCHEMA + ''.'' + A.TABLE_NAME = ''dbo.MaestroAlarmas'' and CONSTRAINT_TYPE = ''PRIMARY KEY'' ' +
                    '       FOR XML PATH(''Campo''), ROOT(''Campos''), TYPE)                                                         ' +
                    '       AS VARCHAR(8000)) AS Campos                                                                              ' ;
  qry.Open;
  XML := qry.FieldByName('Campos').AsString;
  qry.Close;
  FreeAndNil(qry);

  DocXML          := TXMLDocument.Create(nil);
  DocXML.XML.Text := XML;
  DocXML.Active   := True;

  Nodo := DocXML.DocumentElement.ChildNodes.FindNode('Campo');

  Salida := EmptyStr;

  repeat
    if Nodo <> nil then begin
      Salida := Salida  + Nodo.ChildNodes.Nodes[0].NodeValue + ', ';
      Nodo:= Nodo.NextSibling;
    end;
  until Nodo = nil;

  DocXML.Active   := False;
  FreeAndNil(Nodo);
//  FreeAndNil(DocXml);

  Result          := Salida;

end;

Procedure MakeReadWrite (Const Field :TField);
  Begin
    Field.ReadOnly := False;
    With TClientDataSetAccess (Field.DataSet As TClientDataSet) Do
      Check (DSCursor.SetProp (CURProp (4) { curpropFld_MakeRW },
             Field.FieldNo));
  End;

procedure TfrmABMMaestroAlarmas.ValidarTextoDeControl(Sender: TField; const Text: string);
var
  NombreCampo           : string;
  Valida                : Boolean;
  ValorCampo            : LongInt;
begin

  Valida := True;

  if TField(Sender).ClassName = 'TDateTimeField'  then begin
      try
        StrToDateTime(Text);
      except
          raise Exception.Create('Error en valor del campo, reintente');
      end;
  end;

  if TField(Sender).ClassName = 'TSmallintField'  then begin
    try
      ValorCampo := StrToInt(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if ContainsText(FIELDS_TINYINT, NombreCampo) then
      if not ((ValorCampo >= 0) and (ValorCampo <= 255)) then
          Valida := False;

    if ContainsText(FIELDS_SMALLNT, NombreCampo) then
       if not ((ValorCampo >= -32768) and (ValorCampo <= 32767)) then
          Valida := False;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;

  if TField(Sender).ClassName = 'TIntegerField'  then begin
    try
      ValorCampo := StrToInt(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if not ((ValorCampo >= -32768) and (ValorCampo <= 32767)) then
       Valida := False;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;

  if TField(Sender).ClassName = 'TBCDField'  then begin
    try
      StrToFloat(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;


  TField(Sender).Value := Text;

end;

function TfrmABMMaestroAlarmas.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    Position := poMainFormCenter;
	Result := True;
end;


function TfrmABMMaestroAlarmas.TraeRegistros;
var
  Resultado : Boolean;
  i         : Integer;
  status    : TStatusWindowHandle;
begin

  status := CreateStatusWindow(MSG_GET_DATA);

  try

    qryPBL.Close;
    qryPBL.Open;

    procSelect.Parameters.Refresh;
    procSelect.Open;
    Retorno := procSelect.Parameters.ParamByName('@RETURN_VALUE').Value;

    if Retorno <> 0 then begin
      MensajeError := procSelect.Parameters.ParamByName('@ErrorDescription').Value;
      Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
    end;

    CantidadRegistros := procSelect.RecordCount;

    CDS.Active   := False;

    for i := 0 to CDS.Fields.Count - 1 do
      CDS.Fields[i].ReadOnly := False;

    try
      CDS.EmptyDataSet;
    except
    end;
    CDS.CreateDataSet;
    CDS.Active   := True;
    CDS.ReadOnly := False;

    procSelect.First;
    while not procSelect.eof do begin
      CDS.Append;
      for i := 0 to procSelect.Fields.Count - 1 do
        CDS.Fields[i].Value := procSelect.Fields[i].Value;
      CDS.Post;
      procSelect.Next;
    end;

    CDS.ReadOnly := True;

    procSelect.Close;

    try
      CDS.GotoBookmark(Posicion);
    except
      CDS.First;
    end;

    Resultado := True;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      Resultado := False;
    end;
  end;

  HabilitarDeshabilitarControles(False);

  RemoveStatusWindow(status);

  Result := Resultado;
end;

procedure TfrmABMMaestroAlarmas.btnAgregarClick(Sender: TObject);
begin
  HabilitaBotones('000001110');
  try
    Posicion := CDS.GetBookmark;
  except
  end;

  CDS.ReadOnly := False;
  CDS.Filtered := False;
  CDS.Append;
  CDS.Post;
  CDS.Edit;

  pg01.ActivePage := tabGeneral;

  HabilitarDeshabilitarControles(True);
  PonerFocoEnPrimerControl;

  Accion := 1;
end;

procedure TfrmABMMaestroAlarmas.btnBuscarClick(Sender: TObject);
begin
  if not CDS.ReadOnly then
    Exit;
  TfrmPatronBusqueda.Create(self).ShowModal;
end;

procedure TfrmABMMaestroAlarmas.btnCancelarClick(Sender: TObject);
begin
  CDS.Cancel;
  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');

end;

procedure TfrmABMMaestroAlarmas.PonerFocoEnPrimerControl;
var
  i, ControlIndex, TabOrderMenor : Integer;
begin

  ControlIndex  := 99;
  TabOrderMenor := 99;

  for i := 0 to pg01.ActivePage.ControlCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox',TWinControl(pg01.ActivePage.Controls[i]).ClassType.ClassName) then
      if (TWinControl(pg01.ActivePage.Controls[i]).TabOrder < TabOrderMenor) then begin
        TabOrderMenor := TWinControl(pg01.ActivePage.Controls[i]).TabOrder;
        ControlIndex  := i;
      end;

  if ControlIndex < 99 then
    TWinControl(pg01.ActivePage.Controls[ControlIndex]).SetFocus;

end;

procedure TfrmABMMaestroAlarmas.HabilitarDeshabilitarControles(Estado : Boolean);
var
  i : Integer;
begin

   for i := 0 to ComponentCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox', Components[i].ClassName) then begin

      if Components[i].ClassName = 'TDBEdit' then
        TDBEdit(Components[i]).ReadOnly := not Estado;

      if Components[i].ClassName = 'TDBLookupComboBox' then
        TDBLookupComboBox(Components[i]).ReadOnly := not Estado;

      if Components[i].ClassName = 'TDBCheckBox' then
        TDBCheckBox(Components[i]).ReadOnly := not Estado;

    end;

end;


procedure TfrmABMMaestroAlarmas.btnEditarClick(Sender: TObject);
var
  i : Integer;
begin

  HabilitaBotones('000001110');

  Posicion                      := CDS.GetBookmark;
  OldCodigoIncidencia           := CDS.FieldByName('CodigoIncidencia').Value;
  OldCodigoVersionPBL           := CDS.FieldByName('CodigoVersionPBL').Value;
  CDS.ReadOnly                  := False;

  HabilitarDeshabilitarControles(True);
  
  for i := 0 to CDS.Fields.Count - 1 do
    if ContainsStr(FIELDS_PK, CDS.Fields[i].FieldName + ',') then
      CDS.Fields[i].ReadOnly := True;


  for i := 0 to ComponentCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox', Components[i].ClassName) then begin

      if Components[i].ClassName = 'TDBEdit' then
        if ContainsText(FIELDS_PK, TDBEdit(Components[i]).DataField) then
          TDBEdit(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBLookupComboBox' then
        if ContainsText(FIELDS_PK, TDBLookupComboBox(Components[i]).DataField) then
          TDBLookupComboBox(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBCheckBox' then
        if ContainsText(FIELDS_PK, TDBCheckBox(Components[i]).DataField) then
          TDBCheckBox(Components[i]).ReadOnly := True;

    end;

  CDS.Edit;

  PonerFocoEnPrimerControl;

  Accion := 3;

end;

procedure TfrmABMMaestroAlarmas.btnEliminarClick(Sender: TObject);

begin

  HabilitaBotones('000000000');

  try
    Posicion := CDS.GetBookmark;
  except
  end;

  if Application.MessageBox(PChar(ANSW_ELIMINAR),'Pregunta',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    with procDelete do begin
      Parameters.Refresh;
      Parameters.ParamByName('@CodigoIncidencia').Value  := CDS.FieldByName('CodigoIncidencia').Value;
      Parameters.ParamByName('@CodigoVersionPBL').Value  := CDS.FieldByName('CodigoVersionPBL').Value;

      try
        CDS.ReadOnly := False;
        CDS.Delete;
        CDS.ReadOnly := True;
        ExecProc;
        Retorno := procDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
          MensajeError := procDelete.Parameters.ParamByName('@ErrorDescription').Value;
          MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
        end;
      except
        on E : Exception do begin
          MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
        end;
      end;
  end;

 // TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');

end;

procedure TfrmABMMaestroAlarmas.btnGuardarClick(Sender: TObject);
var
  i     : integer;
  Exito : Boolean;
begin

  Exito := True;

  try
    for i := 0 to CDS.Fields.Count - 1 do
      MakeReadWrite (CDS.FieldByName (CDS.Fields[i].FieldName));
    CDS.Post;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
      Exit;
    end;
  end;

  HabilitaBotones('000000000');

  if Accion = 1 then begin
    with procInsert do begin
        Parameters.Refresh;
        for i:=0 to  CDS.Fields.Count - 1 do
           if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName + ',') then
             Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;
        Parameters.ParamByName('@UsuarioCreacion').Value        := UsuarioSistema;

        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
             Exito := False;
             MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
             MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
           end;
          except
            on E : Exception do begin
             Exito := False;
             MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
            end;
        end;

    end;
  end;

  if Accion = 3 then begin

    with procUpdate do begin
      Parameters.Refresh;
      for i:=0 to  CDS.Fields.Count - 1 do
        if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName + ',') then
          Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;

      Parameters.ParamByName('@UsuarioModificacion').Value       := UsuarioSistema;
      Parameters.ParamByName('@CodigoIncidenciaNew').Value       := CDS.FieldByName('CodigoIncidencia').Value;
      Parameters.ParamByName('@CodigoVersionPBLNew').Value       := CDS.FieldByName('CodigoVersionPBL').Value;
      Parameters.ParamByName('@CodigoIncidencia').Value          := OldCodigoIncidencia;
      Parameters.ParamByName('@CodigoVersionPBL').Value          := OldCodigoVersionPBL;

      try
        ExecProc;
        Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
          Exito := False;
          MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
          MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
        end;
      except
        on E : Exception do begin
          Exito := False;
          MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
        end;
      end;

    end;
  end;

  HabilitarDeshabilitarControles(False);

  if not Exito then
    TraeRegistros
  else
    RefrescaRegistroActual;

  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');
end;

procedure TfrmABMMaestroAlarmas.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMMaestroAlarmas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Application.MessageBox(PChar(ANSW_SALIR),'Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    Action := caFree
  else
    Action := caNone;
end;

procedure TfrmABMMaestroAlarmas.FormCreate(Sender: TObject);
var
  i         : Integer;
  SP_Aux    : TADOStoredProc;
begin

{
  SP_Aux := TADOStoredProc.Create(nil);
  with SP_Aux do begin
    Connection    := DMConnections.BaseBO_Master;
    ProcedureName := 'KTCCore_ComparaContenidoTablas';
    Parameters.Refresh;
    Parameters.ParamByName('@BDD1').Value       := 'BO_OBO';
    Parameters.ParamByName('@BDD2').Value       := 'BO_Master';
    Parameters.ParamByName('@TABLA1').Value     := 'MaestroAlarmas';
    Parameters.ParamByName('@TABLA2').Value     := 'MaestroAlarmas';
    Parameters.ParamByName('@RESULTADO').Value  := NULL;

    try
       ExecProc;
       Retorno := Parameters.ParamByName('@Resultado').Value;
       Free;
       if Retorno <> 0 then
          raise Exception.Create('Esta tabla no est� sincronizada, Comun�quse con el administrador del sistema.');

    except
       on E : Exception do begin
         MsgBoxErr('Error en los datos de esta tabla.', E.Message, Caption, MB_ICONERROR);
       end;
     end;

  end;

  }


  Caption                       := CAPTION_FORM;
  btnSalir.Hint                 := HINT_SALIR;
  btnAgregar.Hint               := HINT_AGREGAR;
  btnEliminar.Hint              := HINT_ELIMINAR;
  btnEditar.Hint                := HINT_EDITAR;

  procSelect.Close;
  procInsert.Close;
  procUpdate.Close;
  procDelete.Close;

  procSelect.ProcedureName      := PROCEDURE_SELECT;
  procInsert.ProcedureName      := PROCEDURE_INSERT;
  procUpdate.ProcedureName      := PROCEDURE_UPDATE;
  procDelete.ProcedureName      := PROCEDURE_DELETE;

  procSelect.Connection         := DMConnections.BaseBO_Master;
  procInsert.Connection         := DMConnections.BaseBO_Master;
  procUpdate.Connection         := DMConnections.BaseBO_Master;
  procDelete.Connection         := DMConnections.BaseBO_Master;

  qryPBL.Connection             := DMConnections.BaseCOP;

  pg01.ActivePage := tabGeneral;

  TraeRegistros;

  FIELDS_PK := GetCamposPK;

  CDS.First;

  for i := 0 to CDS.Fields.Count - 1 do
    CDS.Fields[i].OnSetText := ValidarTextoDeControl;


  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');
end;

procedure TfrmABMMaestroAlarmas.HabilitaBotones(Botones : string);
begin
  btnSalir.Enabled      := Botones[1] = '1';
  btnAgregar.Enabled    := Botones[2] = '1';
  btnEliminar.Enabled   := Botones[3] = '1';
  btnEditar.Enabled     := Botones[4] = '1';
  btnImprimir.Enabled   := Botones[5] = '1';
  btnBuscar.Enabled     := Botones[6] = '1';
  btnGuardar.Enabled    := Botones[7] = '1';
  btnCancelar.Enabled   := Botones[8] = '1';
  Grid.Enabled          := Botones[9] = '1';
end;

end.



