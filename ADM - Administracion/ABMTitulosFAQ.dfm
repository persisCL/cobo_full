object frmABMTitulosFAQ: TfrmABMTitulosFAQ
  Left = 224
  Top = 115
  Width = 737
  Height = 541
  Caption = 'Mantenimiento T'#237'tulos de FAQs'
  Color = clBtnFace
  Constraints.MinHeight = 145
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object panEdicion: TPanel
    Left = 0
    Top = 396
    Width = 729
    Height = 111
    Align = alBottom
    Constraints.MaxHeight = 137
    Constraints.MinHeight = 111
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 70
      Width = 727
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 530
        Top = 0
        Width = 197
        Height = 39
        Align = alRight
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 110
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
    object GroupB: TPanel
      Left = 1
      Top = 1
      Width = 727
      Height = 69
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 18
        Top = 39
        Width = 72
        Height = 13
        Caption = '&Descripci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 18
        Top = 9
        Width = 64
        Height = 13
        Caption = 'C'#243'digo T'#237'tulo'
      end
      object txt_Descripcion: TEdit
        Left = 123
        Top = 36
        Width = 456
        Height = 21
        Color = 16444382
        MaxLength = 255
        TabOrder = 1
      end
      object txt_CodigoTitulo: TNumericEdit
        Left = 123
        Top = 6
        Width = 105
        Height = 21
        TabStop = False
        Color = clBtnFace
        MaxLength = 3
        ReadOnly = True
        TabOrder = 0
        Decimals = 0
      end
    end
  end
  object panABM: TPanel
    Left = 0
    Top = 0
    Width = 729
    Height = 396
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object abmTitulos: TAbmToolbar
      Left = 0
      Top = 0
      Width = 729
      Height = 33
      Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
      OnClose = abmTitulosClose
    end
    object dblTitulos: TAbmList
      Left = 0
      Top = 33
      Width = 729
      Height = 363
      TabStop = True
      TabOrder = 1
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      SubTitulos.Sections = (
        #0'53'#0'C'#243'digo    '
        #0'64'#0'Descripci'#243'n')
      HScrollBar = True
      RefreshTime = 100
      Table = TitulosFAQ
      Style = lbOwnerDrawFixed
      ItemHeight = 14
      OnClick = dblTitulosClick
      OnProcess = dblTitulosProcess
      OnDrawItem = dblTitulosDrawItem
      OnRefresh = dblTitulosRefresh
      OnInsert = dblTitulosInsert
      OnDelete = dblTitulosDelete
      OnEdit = dblTitulosEdit
      Access = [accAlta, accBaja, accModi]
      Estado = Normal
      ToolBar = abmTitulos
    end
  end
  object TitulosFAQ: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'FAQTitulos'
    Left = 220
    Top = 84
  end
  object qry_MaxTitulo: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT ISNULL(MAX(CodigoTitulo),0) AS CodigoTitulo FROM FAQTitul' +
        'os  WITH (NOLOCK) ')
    Left = 253
    Top = 84
  end
end
