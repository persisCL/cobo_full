object FormTarjetasDebito: TFormTarjetasDebito
  Left = 124
  Top = 163
  Width = 769
  Height = 542
  Caption = 'Mantenimiento de Tipos de tarjeta de d'#233'bito'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 761
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object alTarjetaDebito: TAbmList
    Left = 0
    Top = 33
    Width = 761
    Height = 239
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'109'#0'Descripci'#243'n               '
      #0'86'#0'Caracter inicial   '
      #0'88'#0'Longitud banda  '
      #0'74'#0'Valida prefijo  '
      #0'44'#0'Activo  ')
    HScrollBar = True
    RefreshTime = 300
    Table = TarjetasDebito
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alTarjetaDebitoClick
    OnProcess = alTarjetaDebitoProcess
    OnDrawItem = alTarjetaDebitoDrawItem
    OnRefresh = alTarjetaDebitoRefresh
    OnInsert = alTarjetaDebitoInsert
    OnDelete = alTarjetaDebitoDelete
    OnEdit = alTarjetaDebitoEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object gbTarjetaDebito: TPanel
    Left = 0
    Top = 272
    Width = 761
    Height = 197
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label2: TLabel
      Left = 29
      Top = 23
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 29
      Top = 48
      Width = 91
      Height = 13
      Caption = '&Primer car'#225'cter:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 222
      Top = 48
      Width = 112
      Height = 13
      Caption = '&Longitud de Banda:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 29
      Top = 75
      Width = 47
      Height = 13
      Caption = 'Prefijos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 139
      Top = 19
      Width = 318
      Height = 21
      Color = 16444382
      MaxLength = 30
      TabOrder = 0
    end
    object txt_caracterinicialbanda: TNumericEdit
      Left = 139
      Top = 44
      Width = 27
      Height = 21
      Color = 16444382
      MaxLength = 2
      TabOrder = 1
      Decimals = 0
    end
    object txt_longitudbanda: TNumericEdit
      Left = 340
      Top = 44
      Width = 27
      Height = 21
      Color = 16444382
      MaxLength = 2
      TabOrder = 2
      Decimals = 0
    end
    object lb_prefijos: TListBox
      Left = 139
      Top = 72
      Width = 230
      Height = 89
      ItemHeight = 13
      TabOrder = 3
    end
    object chkActivo: TCheckBox
      Left = 29
      Top = 167
      Width = 122
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Activo:'
      TabOrder = 6
    end
    object btnAgregarPrefijo: TButton
      Left = 376
      Top = 73
      Width = 80
      Height = 26
      Caption = '<< A&gregar'
      TabOrder = 4
      OnClick = btnAgregarPrefijoClick
    end
    object btnQuitarPrefijo: TButton
      Left = 376
      Top = 101
      Width = 80
      Height = 26
      Caption = '>> &Quitar'
      TabOrder = 5
      OnClick = btnQuitarPrefijoClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 469
    Width = 761
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 564
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 31
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object ActualizarDatosTarjetaDebito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosTarjetaDebito;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaDebito'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CaracterInicialBanda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@LongitudBanda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ValidarPrefijo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 416
    Top = 120
  end
  object TarjetasDebito: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'TarjetasDebito'
    Left = 386
    Top = 120
  end
  object EliminarTarjetaDebito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTarjetaDebito;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjetaDebito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 416
    Top = 148
  end
  object qry_prefijos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTipoTarjetaDebito'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM'
      '  TarjetasDebitoPrefijos  WITH (NOLOCK) '
      'WHERE'
      '  CodigoTipoTarjetaDebito = :CodigoTipoTarjetaDebito')
    Left = 386
    Top = 148
  end
  object QueryTemp: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 387
    Top = 179
  end
end
