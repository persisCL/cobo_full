object FAbmTasasInteresReajustables: TFAbmTasasInteresReajustables
  Left = 516
  Top = 165
  Caption = 'Tasas de Interes Reajustables'
  ClientHeight = 327
  ClientWidth = 322
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 322
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object AbmList: TAbmList
    Left = 0
    Top = 35
    Width = 322
    Height = 194
    Ctl3D = True
    ParentCtl3D = False
    TabStop = True
    TabOrder = 1
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'128'#0'Fecha                              '
      
        #0'298'#0'Tasa de Interes Anual                                      ' +
        '                        ')
    RefreshTime = 300
    Table = tblTasasInteres
    Style = lbOwnerDrawFixed
    OnClick = AbmListClick
    OnDrawItem = AbmListDrawItem
    OnRefresh = abmListRefresh
    OnInsert = AbmListInsert
    OnDelete = AbmListDelete
    OnEdit = AbmListEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object pnlEditing: TPanel
    Left = 0
    Top = 229
    Width = 322
    Height = 48
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 2
    object LPorcentaje: TLabel
      Left = 270
      Top = 23
      Width = 12
      Height = 16
      Caption = '%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LfechadeInicio: TLabel
      Left = 9
      Top = 5
      Width = 76
      Height = 13
      Caption = 'Fecha  de Inicio'
    end
    object LTasadeInteresAnual: TLabel
      Left = 126
      Top = 5
      Width = 163
      Height = 13
      Caption = 'Tasa de Interes Anual Reajustable'
    end
    object neTasaInteres: TNumericEdit
      Left = 126
      Top = 20
      Width = 138
      Height = 21
      TabOrder = 0
      Decimals = 2
    end
    object deFechaInicio: TDateEdit
      Left = 8
      Top = 20
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
  end
  object pnlButtons: TPanel
    Left = 0
    Top = 277
    Width = 322
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      322
      50)
    object Notebook: TNotebook
      Left = 116
      Top = 8
      Width = 201
      Height = 38
      Anchors = [akRight, akBottom]
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'Salir'
        object btnSalir: TButton
          Left = 120
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Editing'
        object btnAceptar: TButton
          Left = 45
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 120
          Top = 8
          Width = 75
          Height = 25
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
  end
  object tblTasasInteres: TADOTable
    Connection = DMConnections.BaseCAC
    IndexFieldNames = 'FechaInicio DESC'
    TableName = 'TasasInteresReajustables'
    Left = 56
    Top = 120
    object tblTasasInteresFechaInicio: TDateTimeField
      FieldName = 'FechaInicio'
    end
    object tblTasasInteresTasaInteresAnual: TBCDField
      FieldName = 'TasaInteresAnual'
      Precision = 18
      Size = 8
    end
  end
end
