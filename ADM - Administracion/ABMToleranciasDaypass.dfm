object frmABMToleranciasDaypass: TfrmABMToleranciasDaypass
  Left = 274
  Top = 174
  Width = 400
  Height = 452
  Caption = 'frmABMToleranciasDaypass'
  Color = clBtnFace
  Constraints.MinHeight = 450
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 392
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
  end
  object gbEdicion: TPanel
    Left = 0
    Top = 244
    Width = 392
    Height = 135
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 5
      Top = 45
      Width = 100
      Height = 13
      Caption = '&Fecha Activaci'#243'n'
      FocusControl = deFechaActivacion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 5
      Top = 12
      Width = 81
      Height = 13
      Caption = 'Concesionaria'
      FocusControl = deFechaActivacion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 5
      Top = 76
      Width = 105
      Height = 13
      Caption = 'Tolerancia Inferior'
      FocusControl = deFechaActivacion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 5
      Top = 106
      Width = 112
      Height = 13
      Caption = 'Tolerancia Superior'
      FocusControl = deFechaActivacion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 187
      Top = 76
      Width = 37
      Height = 13
      Caption = 'minutos'
    end
    object Label6: TLabel
      Left = 187
      Top = 106
      Width = 37
      Height = 13
      Caption = 'minutos'
    end
    object deFechaActivacion: TDateEdit
      Left = 120
      Top = 41
      Width = 121
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object neInferior: TNumericEdit
      Left = 120
      Top = 72
      Width = 49
      Height = 21
      Color = 16444382
      MaxLength = 4
      TabOrder = 2
      Decimals = 0
    end
    object neSuperior: TNumericEdit
      Left = 120
      Top = 102
      Width = 49
      Height = 21
      Color = 16444382
      MaxLength = 4
      TabOrder = 4
      Decimals = 0
    end
    object UpDownSuperior: TUpDown
      Left = 169
      Top = 102
      Width = 16
      Height = 21
      Associate = neSuperior
      Max = 1440
      TabOrder = 5
    end
    object UpDownInferior: TUpDown
      Left = 169
      Top = 72
      Width = 16
      Height = 21
      Associate = neInferior
      Max = 1440
      TabOrder = 3
    end
    object cbConcesionaria: TVariantComboBox
      Left = 120
      Top = 8
      Width = 249
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 379
    Width = 392
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 195
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 31
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object alTolerancias: TAbmList
    Left = 0
    Top = 33
    Width = 392
    Height = 211
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      
        #0'216'#0'Concesionaria                                              ' +
        ' '
      #0'124'#0'Activaci'#243'n                      ')
    HScrollBar = True
    RefreshTime = 300
    Table = spObtenerListadoToleranciasDiaDaypass
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alToleranciasClick
    OnDrawItem = alToleranciasDrawItem
    OnInsert = alToleranciasInsert
    OnDelete = alToleranciasDelete
    OnEdit = alToleranciasEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 96
  end
  object spObtenerListadoToleranciasDiaDaypass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListadoToleranciasDiaDaypass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 64
  end
  object spActualizarToleranciaBHTU: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarToleranciaBHTU'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConcesionariaActual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaActivacionActual'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Concesionaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaActivacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ToleranciaInferior'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ToleranciaSuperior'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Error'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 255
        Value = Null
      end>
    Left = 32
    Top = 136
  end
  object spObtenerFechasToleranciaBHTUVigente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFechasToleranciaBHTUVigente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 176
  end
  object spEliminarToleranciaBHTU: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarToleranciaBHTU'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaActivacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 72
    Top = 176
  end
end
