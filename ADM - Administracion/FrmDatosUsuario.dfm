object FormDatosUsuario: TFormDatosUsuario
  Left = 232
  Top = 180
  BorderStyle = bsDialog
  Caption = 'Datos del Usuario'
  ClientHeight = 498
  ClientWidth = 941
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 29
    Top = 13
    Width = 93
    Height = 13
    Caption = 'C'#243'digo de Usuario :'
  end
  object Label2: TLabel
    Left = 79
    Top = 64
    Width = 43
    Height = 13
    Caption = 'Apellido :'
  end
  object Bevel1: TBevel
    Left = 8
    Top = 273
    Width = 531
    Height = 2
    Shape = bsTopLine
  end
  object Label5: TLabel
    Left = 8
    Top = 281
    Width = 144
    Height = 13
    Caption = 'Grupos en que est'#225' el usuario:'
  end
  object Label6: TLabel
    Left = 325
    Top = 281
    Width = 94
    Height = 13
    Caption = 'Grupos Disponibles:'
  end
  object Bevel2: TBevel
    Left = 8
    Top = 458
    Width = 925
    Height = 3
    Shape = bsTopLine
  end
  object Label7: TLabel
    Left = 79
    Top = 40
    Width = 43
    Height = 13
    Caption = 'Nombre :'
  end
  object Label8: TLabel
    Left = 37
    Top = 83
    Width = 85
    Height = 13
    Caption = 'Apellido Materno :'
  end
  object lbl1: TLabel
    Left = 32
    Top = 107
    Width = 90
    Height = 13
    Caption = 'Correo Electronico:'
  end
  object Bevel3: TBevel
    Left = 545
    Top = 8
    Width = 7
    Height = 444
    Shape = bsLeftLine
  end
  object Label9: TLabel
    Left = 558
    Top = 12
    Width = 128
    Height = 13
    Caption = 'Env'#237'o de Alertas por Email:'
  end
  object txt_codigousuario: TEdit
    Left = 129
    Top = 9
    Width = 144
    Height = 21
    CharCase = ecLowerCase
    MaxLength = 20
    TabOrder = 0
    OnChange = txt_codigousuarioChange
  end
  object lb_gruposusuario: TListBox
    Left = 8
    Top = 307
    Width = 209
    Height = 145
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 9
    OnDblClick = btn_quitarClick
  end
  object lb_grupos: TListBox
    Left = 325
    Top = 307
    Width = 209
    Height = 145
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 12
    OnDblClick = btn_agregarClick
  end
  object cb_Deshabilitado: TCheckBox
    Left = 52
    Top = 136
    Width = 90
    Height = 17
    Alignment = taLeftJustify
    Caption = '&Deshabilitado:'
    TabOrder = 5
  end
  object cb_CambioPass: TCheckBox
    Left = 359
    Top = 250
    Width = 169
    Height = 17
    Alignment = taLeftJustify
    Caption = '&Solicitar Cambio de Password:'
    Checked = True
    Enabled = False
    State = cbChecked
    TabOrder = 8
  end
  object gb_CambiarContrasena: TGroupBox
    Left = 8
    Top = 190
    Width = 345
    Height = 77
    Enabled = False
    TabOrder = 7
    object Label3: TLabel
      Left = 15
      Top = 23
      Width = 57
      Height = 13
      Caption = 'Contrase'#241'a:'
    end
    object Label4: TLabel
      Left = 15
      Top = 50
      Width = 93
      Height = 13
      Caption = 'Repetir contrase'#241'a:'
    end
    object txt_password: TEdit
      Left = 137
      Top = 17
      Width = 184
      Height = 21
      Color = clBtnFace
      MaxLength = 20
      PasswordChar = '*'
      TabOrder = 0
    end
    object txt_password2: TEdit
      Left = 137
      Top = 44
      Width = 184
      Height = 21
      Color = clBtnFace
      MaxLength = 20
      PasswordChar = '*'
      TabOrder = 1
    end
  end
  object cb_CambiarContrasena: TCheckBox
    Left = 8
    Top = 167
    Width = 117
    Height = 17
    Caption = 'Cambiar contrase'#241'a'
    TabOrder = 6
    OnClick = cb_CambiarContrasenaClick
  end
  object btn_agregar: TButton
    Left = 232
    Top = 346
    Width = 75
    Height = 25
    Caption = '<  Agregar'
    TabOrder = 10
    OnClick = btn_agregarClick
  end
  object btn_quitar: TButton
    Left = 232
    Top = 386
    Width = 75
    Height = 25
    Caption = 'Quitar  >'
    TabOrder = 11
    OnClick = btn_quitarClick
  end
  object btn_ok: TButton
    Left = 777
    Top = 465
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    TabOrder = 13
    OnClick = btn_okClick
  end
  object btn_cancel: TButton
    Left = 858
    Top = 465
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 14
  end
  object edtApellido: TEdit
    Left = 129
    Top = 58
    Width = 240
    Height = 21
    MaxLength = 20
    TabOrder = 2
    OnChange = txt_codigousuarioChange
  end
  object edtApellidoMaterno: TEdit
    Left = 129
    Top = 80
    Width = 240
    Height = 21
    MaxLength = 20
    TabOrder = 3
    OnChange = txt_codigousuarioChange
  end
  object edtNombre: TEdit
    Left = 128
    Top = 36
    Width = 241
    Height = 21
    MaxLength = 20
    TabOrder = 1
    OnChange = txt_codigousuarioChange
  end
  object edtEmail: TEdit
    Left = 128
    Top = 102
    Width = 241
    Height = 21
    TabOrder = 4
  end
  object dbgrdAlertas: TDBGrid
    Left = 558
    Top = 31
    Width = 375
    Height = 421
    DataSource = dsAlertas
    TabOrder = 15
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = dbgrdAlertasCellClick
    OnColEnter = dbgrdAlertasColEnter
    OnColExit = dbgrdAlertasColExit
    OnDrawColumnCell = dbgrdAlertasDrawColumnCell
    OnKeyDown = dbgrdAlertasKeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Caption = 'M'#243'dulo'
        Width = 246
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EnvioEventos'
        Title.Caption = 'Eventos'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EnvioAlertas'
        Title.Caption = 'Alertas'
        Width = 44
        Visible = True
      end>
  end
  object RegistrarUsuarioSistema: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'RegistrarUsuarioSistema;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Deshabilitado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 85
    Top = 315
  end
  object RegistrarUsuarioSistemaNuevo: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'RegistrarUsuarioSistemaNuevo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Deshabilitado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@PedirCambiarPassword'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Email'
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 80
    Top = 363
  end
  object spUsuario: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_UsuariosSistemas_SELECT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 424
    Top = 64
  end
  object spGrupos: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposSistemas_SELECT_1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 480
    Top = 64
  end
  object spDELGruposUsuarios: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposUsuarios_DELETE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 432
    Top = 16
  end
  object spINSGruposUsuarios: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposUsuarios_INSERT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 424
    Top = 160
  end
  object dsAlertas: TDataSource
    DataSet = cdsAlertas
    Left = 768
    Top = 288
  end
  object spADM_UsuariosSistemasEnvioAlarmas_SELECT: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_UsuariosSistemasEnvioAlarmas_SELECT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Value = 0
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4000
        Value = Null
      end>
    Left = 736
    Top = 120
  end
  object cdsAlertas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoSistema'
        DataType = ftInteger
      end
      item
        Name = 'CodigoSistemaModulo'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'EnvioEventos'
        DataType = ftBoolean
      end
      item
        Name = 'EnvioAlertas'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 704
    Top = 288
  end
  object spADM_UsuariosSistemasEnvioAlarmas_INSERT: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_UsuariosSistemasEnvioAlarmas_INSERT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Value = 0
      end
      item
        Name = '@Usuario'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Sistema'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@SistemaModulo'
        DataType = ftString
        Value = Null
      end
      item
        Name = '@EnvioEventos'
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@EnvioAlertas'
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4000
        Value = Null
      end>
    Left = 736
    Top = 168
  end
  object spUsuariosSistemas_1: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_UsuariosSistemas_SELECT_1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 424
    Top = 112
  end
  object spADM_UsuariosSistemasEnvioAlarmas_DELETE: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_UsuariosSistemasEnvioAlarmas_DELETE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Value = 0
      end
      item
        Name = '@Usuario'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4000
        Value = Null
      end>
    Left = 736
    Top = 216
  end
end
