object FormDiaDefault: TFormDiaDefault
  Left = 202
  Top = 141
  Width = 600
  Height = 400
  Caption = 'Mantenimiento de D'#237'as por Defecto'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 215
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'50'#0'D'#237'a        '
      #0'261'#0'Descripci'#243'n      '
      #0'164'#0'Tipo de D'#237'a                                 ')
    HScrollBar = True
    RefreshTime = 100
    Table = TiposDiaDefault
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnEdit = DBList1Edit
    Estado = Normal
    ToolBar = AbmToolbar1
    Access = [accAlta, accBaja, accModi]
  end
  object GroupB: TPanel
    Left = 0
    Top = 248
    Width = 592
    Height = 86
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label15: TLabel
      Left = 11
      Top = 61
      Width = 55
      Height = 13
      Caption = '&Tipo D'#237'a:'
      Color = clBtnFace
      FocusControl = cb_TiposDia
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label1: TLabel
      Left = 11
      Top = 12
      Width = 89
      Height = 13
      Caption = 'D'#237'a de la Semana:'
    end
    object Label2: TLabel
      Left = 11
      Top = 37
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Color = clBtnFace
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object txt_DiaSemana: TNumericEdit
      Left = 112
      Top = 8
      Width = 121
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      Decimals = 0
    end
    object txt_Descripcion: TEdit
      Left = 112
      Top = 33
      Width = 229
      Height = 21
      Color = clBtnFace
      MaxLength = 60
      ReadOnly = True
      TabOrder = 1
    end
    object cb_TiposDia: TComboBox
      Left = 112
      Top = 57
      Width = 229
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 334
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 395
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object TiposDiaDefault: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'TiposDiaDefault'
    Left = 188
    Top = 80
  end
  object qry_TiposDia: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM TIPOSDIA WITH (NOLOCK)')
    Left = 216
    Top = 80
  end
end
