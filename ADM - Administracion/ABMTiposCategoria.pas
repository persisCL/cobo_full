{-----------------------------------------------------------------------------
 File Name: ABMTiposCategoria
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ABMTiposCategoria;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DPSControls, StrUtils, Variants, VariantComboBox, Categoria,
  Grids, DBGrids, DBClient, Provider, ToolWin, ImgList, CategoriasCambio;              //TASK_094_JMA_20160111

type
{INICIO: TASK_094_JMA_20160111}
  TABMCategorias = class(TForm)
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    txt_Categoria: TNumericEdit;
    txt_Factor: TNumericEdit;
    lblFactor: TLabel;
    lblClases: TLabel;
    BtnCancelar: TButton;
    BtnAceptar: TButton;
    txt_Clase: TEdit;
    lblCambioCategoria: TLabel;
    dbgrdCategorias: TDBGrid;
    cbbCategoriaCambio: TVariantComboBox;
    ilActivos: TImageList;
    pnlSuperior: TPanel;
    Botonera: TToolBar;
    btnSalir: TToolButton;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    procedure BtnCancelarClick(Sender: TObject);
{INICIO: TASK_094_JMA_20160111
    procedure dblCategoriasClick(Sender: TObject);                                                       
    procedure dblCategoriasDrawItem(Sender: TDBList; Tabla: TDataSet;                                    
    Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);                                         
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);                                                     
    procedure AbmToolbar1Close(Sender: TObject);                                                          
    procedure dblCategoriasDelete(Sender: TObject);                                                       
    procedure dblCategoriasInsert(Sender: TObject);                                                       
TERMINO: TASK_094_JMA_20160111}
    procedure BtnAceptarClick(Sender: TObject);
    //procedure FormShow(Sender: TObject);                                                                  //TASK_094_JMA_20160111
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure dbgrdCategoriasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
{INICO: TASK_094_JMA_20160111
    procedure chkAplicaValidacionAutomaticaClick(Sender: TObject);                                       
    procedure MedidaToleranciaExit(Sender: TObject);                                                     
    procedure txt_FactorExit(Sender: TObject);                                                           
TERMINO: TASK_094_JMA_20160111}
  private
    { Private declarations }
    objCategorias: TCategoria;                                                                         //TASK_094_JMA_20160111
    IDCategoriaUrbana, IDCategoriaInterurbana: Integer;                                                //TASK_094_JMA_20160111
    
    procedure Categorias_AfterScroll(DataSet: TDataSet);                                               //TASK_094_JMA_20160111
    procedure CargarCategorias();                                                                      //TASK_094_JMA_20160111
    procedure CargarCategoriaCambio(CodigoCategoria: Byte; CategoriaClase: Integer);                   //TASK_094_JMA_20160111
    procedure PermitirEdicion(valor: Boolean);                                                         //TASK_094_JMA_20160111
    
    procedure Limpiar_Campos;
{INICIO: TASK_094_JMA_20160111    
    Procedure Volver_Campos;                                                             
    function  PuedeGrabar: boolean;
    procedure HabilitarValidacionAutomatica(habil: boolean);                             
    function DescripcionValidacionAutomatica(Lr, An, Al: boolean): AnsiString;           
TERMINO: TASK_094_JMA_20160111}
  public
   
    { Public declarations }
    function Inicializa: boolean;
  end;
 
var
  ABMCategorias: TABMCategorias;

implementation

uses DMConnection;
resourcestring
    //MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Tipo de Categor�a?';                            //TASK_094_JMA_20160111
    //MSG_DELETE_ERROR		= 'No se puede eliminar el Tipo de Categor�a porque hay datos que dependen de �l.';         //TASK_094_JMA_20160111
    //MSG_DELETE_CAPTION 		= 'Eliminar Categoria';                                                                 //TASK_094_JMA_20160111
    //MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Tipo de Categor�a.';                             //TASK_094_JMA_20160111
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Categor�a';                                                                   //TASK_094_JMA_20160111
    //MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';                                                         //TASK_094_JMA_20160111
    //MSG_FACTOR              = 'Debe ingresar el factor';                                                              //TASK_094_JMA_20160111
    //MSG_MEDIDA_TOLERANCIA   = 'Debe ingresar la medidad de tolerancia';                                               //TASK_094_JMA_20160111
    //MSG_UNIDAD_TOLERANCIA   = 'Debe seleccionar a unidad de tolerancia';                                              //TASK_094_JMA_20160111
    //NO_PERMITIR_CAMBIO_CATEGORIA = '< No permite el cambio >';                                                        //TASK_094_JMA_20160111

{$R *.DFM}

{INICIO: TASK_094_JMA_20160111
function TABMCategorias.Inicializa: boolean;
Var
	S: TSize;
begin

	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;
    CargarUnidadesToleranciaCategoria(CB_unidadTolerancia, '');
    CargarCategoriasVehiculosVariant(DMConnections.BaseCAC,
                     cb_CategoriaCambio, NO_PERMITIR_CAMBIO_CATEGORIA);

	if not OpenTables([Categorias]) then exit;
   	dblCategorias.Reload;
    Notebook.PageIndex := 0;

   	Result := True;

end;

procedure TABMCategorias.BtnCancelarClick(Sender: TObject);
begin
    Volver_Campos;
end;

procedure TABMCategorias.dblCategoriasClick(Sender: TObject);
begin

	with (Sender AS TDbList).Table do begin
		txt_Categoria.Value		:= FieldByName('Categoria').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
		txt_Factor.Value		:= FieldByName('FactorTarifa').AsFloat;

        MarcarItemComboVariant(cb_CategoriaCambio, FieldByName('CategoriaCambio').AsInteger);

        chkAplicaValidacionAutomatica.Checked := FieldByName('ValidacionAutomaticaLargo').AsBoolean or
                                                 FieldByName('ValidacionAutomaticaAncho').AsBoolean or
                                                 FieldByName('ValidacionAutomaticaAlto').AsBoolean;

        chkLargo.Checked := FieldByName('ValidacionAutomaticaLargo').AsBoolean;
        chkAncho.Checked := FieldByName('ValidacionAutomaticaAncho').AsBoolean;
        chkAlto.Checked  := FieldByName('ValidacionAutomaticaAlto').AsBoolean;

        MedidaTolerancia.Value := FieldByName('MedidaTolerancia').AsInteger;
        //Centimetros o Porcentaje

		if FieldByName('UnidadTolerancia').AsString = 'C' then
			CB_unidadTolerancia.itemindex := 0
        else CB_unidadTolerancia.itemindex := 1;
        MemoComentarios.Text := FieldByName('Comentarios').AsString;
	end;
end;
TERMINO: TASK_094_JMA_20160111}

{INICIO: TASK_094_JMA_20160111
procedure TABMCategorias.dblCategoriasDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(FieldByName('Categoria').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
        TextOut(Cols[2], Rect.Top, Rstr(Tabla.FieldbyName('FactorTarifa').AsFloat, 10, 2));
        TextOut(Cols[3], Rect.Top, Tabla.FieldbyName('DescCategoriaCambio').AsString);
        TextOut(Cols[4], Rect.Top, DescripcionValidacionAutomatica(
                                        FieldByName('ValidacionAutomaticaLargo').AsBoolean,
                                        FieldByName('ValidacionAutomaticaAncho').AsBoolean,
                                        FieldByName('ValidacionAutomaticaAlto').AsBoolean));
        TextOut(Cols[5], Rect.Top, Trim(DescripcionTolerancia(Tabla.FieldbyName('MedidaTolerancia').AsInteger,
                                                              Tabla.FieldbyName('UnidadTolerancia').AsString)));
	end;
end;

procedure TABMCategorias.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    txt_Descripcion.SetFocus;
    txt_Descripcion.Text := trim(txt_Descripcion.Text);
    //es char y completa con espacios que no dejan escribir
end;

procedure TABMCategorias.DBList1Refresh(Sender: TObject);
begin
	 if dblCategorias.Empty then Limpiar_Campos();
end;

procedure TABMCategorias.Limpiar_Campos();
begin
    txt_Categoria.Clear;
	txt_Descripcion.Clear;
    txt_Factor.Clear;
    MemoComentarios.Clear;
end;

procedure TABMCategorias.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;
   
procedure TABMCategorias.dblCategoriasDelete(Sender: TObject);
resourceString
	MSG_ELIMINAR 		= 'Eliminar';
    MSG_ERROR_ELIMINAR  = 'No se pudo eliminar la categor�a';
begin
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION)
        =  IDYES then begin
        try
            screen.Cursor	:= crHourGlass;
            try
                with EliminarCategorias do begin
                    Parameters.ParamByName('@Categoria').Value := Categorias.fieldByName('Categoria').AsInteger;
                    ExecProc;
                end;
                EliminarCategorias.Close;
            except
                On E: Exception do begin
                    EliminarCategorias.Close;
                    MsgBox(MSG_ERROR_ELIMINAR, MSG_ELIMINAR, MB_ICONSTOP);
                    exit;
                end;
            end;
            Screen.Cursor := crDefault;
            DBList.Reload;
        finally

        end;
        Volver_Campos;
    end;
    Screen.Cursor := crDefault;
    
end;

procedure TABMCategorias.dblCategoriasInsert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DBList.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_Categoria.ReadOnly := false;
    txt_categoria.Color := txt_Descripcion.Color;
    txt_Categoria.SetFocus;
end;
}

{INICIO:  TASK_094_JMA_20160111
procedure TABMCategorias.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos de la categor�a.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar categor�a';
var
	CategoriaActual: TBookmark;
    EstadoLista: TAbmState;
begin
 	if PuedeGrabar then begin
        EstadoLista := DBList1.Estado;
        CategoriaActual := DBList1.Table.getBookMark;
        try
            Screen.Cursor := crHourGlass;
            try
                With ActualizarCategorias do begin
                    Parameters.ParamByName('@Categoria').Value := Trunc(txt_Categoria.Value);
                    Parameters.ParamByName('@Descripcion').Value := Trim(txt_Descripcion.text);
                    Parameters.ParamByName('@FactorTarifa').Value := txt_Factor.Value;
                    Parameters.ParamByName('@MedidaTolerancia').Value := MedidaTolerancia.ValueInt;
                    Parameters.ParamByName('@UnidadTolerancia').Value := Trim(RightStr(CB_unidadTolerancia.Text, 10));
                    if chkAplicaValidacionAutomatica.Checked then begin
                        Parameters.ParamByName('@ValidacionAutomaticaLargo').Value := chkLargo.Checked;
                        Parameters.ParamByName('@ValidacionAutomaticaAncho').Value := chkAncho.Checked;
                        Parameters.ParamByName('@ValidacionAutomaticaAlto').Value  := chkAlto.Checked;
                    end
                    else begin                        
                        Parameters.ParamByName('@ValidacionAutomaticaLargo').Value := false;
                        Parameters.ParamByName('@ValidacionAutomaticaAncho').Value := false;
                        Parameters.ParamByName('@ValidacionAutomaticaAlto').Value  := false;
                    end;

                    Parameters.ParamByName('@Comentarios').Value := Trim(MemoComentarios.text);
                    Parameters.ParamByName('@CategoriaCambio').Value  := iif(cb_CategoriaCambio.ItemIndex = 0, null,
                                                        IntToStr(cb_CategoriaCambio.value));
                end;
                ActualizarCategorias.ExecProc;
                ActualizarCategorias.Close;
            except
                On E: exception do begin
                    MsgBoxErr(MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                    ActualizarCategorias.Close;
                    Exit;
                    Screen.Cursor := crDefault;
                end;
            end;

            if EstadoLista = Modi then begin
                DBList1.Table.GotoBookmark(CategoriaActual);
            end else begin
                DBList1.Table.Close;
                DBList1.Table.Open;
                DBList1.Table.DisableControls;
                DBList1.Table.Last;
                DBList1.Table.EnableControls;
            end;
        finally
            DBList1.Table.FreeBookmark(CategoriaActual);
        end;
        Volver_Campos;
    end;
	Screen.Cursor 	   := crDefault;
end;

procedure TABMCategorias.FormShow(Sender: TObject);
begin
   	dblCategorias.Reload;
end;

procedure TABMCategorias.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    action := caFree;
end;

procedure TABMCategorias.BtnSalirClick(Sender: TObject);
begin
     close;
end;  

procedure TABMCategorias.Volver_Campos;
begin
   	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    CargarCategoriasVehiculosVariant(DMConnections.BaseCAC,
                                    cb_CategoriaCambio,
                                    NO_PERMITIR_CAMBIO_CATEGORIA);
    groupb.Enabled     := False;
    txt_Categoria.ReadOnly := true;
    txt_categoria.Color := clBtnFace;
end;

function TABMCategorias.PuedeGrabar: boolean;
resourcestring
    MSG_VALIDACION_AUTOMATICA = 'Si selecciona que aplica validaci�n autom�tica debe indicar ' + #13#10 +
                                'cuales son las medidas involucradas (Largo, Ancho, Alto).';
var
    ok: Boolean;
begin
    ok := ValidateControls([txt_Descripcion,
                            txt_Factor,
                            MedidaTolerancia],
                            [trim(txt_Descripcion.text) <> '',
                            (trim(txt_Factor.text) <> '')],
                            trim(MedidaTolerancia.text) <> ''],
                            MSG_ACTUALIZAR_CAPTION,
                            [MSG_DESCRIPCION,
                             MSG_FACTOR,
                             MSG_MEDIDA_TOLERANCIA]);
    if ok then begin
        if chkAplicaValidacionAutomatica.Checked then
        // si eligio ValidacionAutomatica, alguno debe estar chequeado
        if not (chkLargo.Checked or
                chkAncho.Checked or
                chkAlto.Checked) then begin
            MsgBox(MSG_VALIDACION_AUTOMATICA, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
            ok := false;
        end;
    end;
    result := ok;
end;    

procedure TABMCategorias.chkAplicaValidacionAutomaticaClick(
  Sender: TObject);
begin
    if not chkAplicaValidacionAutomatica.Checked then begin
        chkLargo.Checked := false;
        chkAncho.Checked := false;
        chkAlto.Checked  := false;
    end;
    HabilitarValidacionAutomatica(chkAplicaValidacionAutomatica.Checked);
end;

procedure TABMCategorias.HabilitarValidacionAutomatica(habil: boolean);
begin
    chkLargo.Enabled := habil;
    chkAncho.Enabled := habil;
    chkAlto.Enabled := habil;
end;

function TABMCategorias.DescripcionValidacionAutomatica(Lr, An, Al: boolean): AnsiString;
resourcestring
    LARGO = 'Largo';
    ANCHO = 'Ancho';
    ALTO = 'Alto';
var
    desc: Ansistring;
begin
    desc := '';
    if Lr then Desc := LARGO + ', ';
    if An then Desc := Desc + ANCHO + ', ';
    if Al then Desc := Desc + ALTO + ', ';
    if Desc <> '' then Desc := Copy (desc,1, length(trim(desc)) - 1);
    result := Desc;
end;

procedure TABMCategorias.MedidaToleranciaExit(Sender: TObject);
begin
    if trim(MedidaTolerancia.Text) = '' then
        MedidaTolerancia.ValueInt := 0;
end;

procedure TABMCategorias.txt_FactorExit(Sender: TObject);
begin
    if (trim(txt_Factor.Text) = '') then txt_Factor.valueint := 0; 
end;
}
function TABMCategorias.Inicializa: boolean;
begin
    Result := False;
    objCategorias:= TCategoria.Create;
    IDCategoriaUrbana := objCategorias.ObtenerIDCategoriaClase('Urbana');
    IDCategoriaInterurbana := objCategorias.ObtenerIDCategoriaClase('Interurbana');
    CargarCategorias();
   	Result := True;
end;

procedure TABMCategorias.CargarCategorias();
begin
    try
        if Assigned(dbgrdCategorias.DataSource) then
            dbgrdCategorias.DataSource.Free;
        dbgrdCategorias.DataSource := TDataSource.Create(nil);
        dbgrdCategorias.DataSource.DataSet := objCategorias.ObtenerCategorias(0,'');
        objCategorias.ClientDataSet.AfterScroll := Categorias_AfterScroll;
        Categorias_AfterScroll(TDataSet(objCategorias.ClientDataSet));
    except
    on e: Exception do
        MsgBoxErr('Error cargando Categor�as', e.Message, 'Error de Carga', MB_ICONSTOP);
    end;
end;

procedure TABMCategorias.Categorias_AfterScroll(DataSet: TDataSet);
begin
    if Assigned(DataSet) and (DataSet.RecordCount > 0) and not DataSet.Eof then
    begin
        txt_Categoria.Value		:= objCategorias.CodigoCategoria;
        txt_Descripcion.text	:= objCategorias.Descripcion;
        txt_Clase.text          := objCategorias.Clase;
        txt_Factor.Value		:= objCategorias.FactorTarifa;
        CargarCategoriaCambio(objCategorias.CodigoCategoria, objCategorias.IDCategoriaClase);
        cbbCategoriaCambio.Value := objCategorias.CategoriaCambio;
    end;
end;

{INICIO:TASK_114_JMA_20170313}
procedure TABMCategorias.dbgrdCategoriasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
    if Column.FieldName = 'FactorTarifa' then
        TFloatField(Column.Field).DisplayFormat  := '##0.00'
end;
{TERMINO:TASK_114_JMA_20170313}

procedure TABMCategorias.CargarCategoriaCambio(CodigoCategoria: Byte; CategoriaClase: Integer);
var
    dc: TDataSource;
    I: Integer;
    objCategCab: TCategoriasCambio;
begin
    try
        try
            dc := TDataSource.Create(nil);
            objCategCab:= TCategoriasCambio.Create;
            dc.DataSet := objCategCab.ObtenerCategoriasCambioDisponibles(CodigoCategoria);
            dc.DataSet.First;
            cbbCategoriaCambio.Items.Clear;
            cbbCategoriaCambio.Items.InsertItem('', 0);
            if CategoriaClase = 2 then
            begin
                for I := 1 to dc.DataSet.RecordCount do
                begin
                    cbbCategoriaCambio.Items.InsertItem(IntToStr(objCategCab.CodigoCategoria), objCategCab.CodigoCategoria);
                    dc.DataSet.Next;
                end;
            end;
        except
        on e: Exception do
            MsgBoxErr('Error cargando Categor�as Cambio Disponibles', e.Message, 'Error de Carga', MB_ICONSTOP);
        end;
    finally
        objCategCab.Free;
        if Assigned(dc) then
        begin
            if Assigned(dc.DataSet) then
                dc.DataSet.Free;
            dc.Free
        end;
    end;
end;

procedure TABMCategorias.PermitirEdicion(valor: Boolean);
begin
    txt_Factor.Enabled := valor;
    if objCategorias.IDCategoriaClase = IDCategoriaInterurbana then
       cbbCategoriaCambio.Enabled := valor;
    Botonera.Enabled := not valor;
    dbgrdCategorias.Enabled := not valor;
    BtnAceptar.Enabled := valor;
    BtnCancelar.Enabled:= valor;    
end;

procedure TABMCategorias.btnEditarClick(Sender: TObject);
begin
    if Assigned(dbgrdCategorias.DataSource.DataSet) and (dbgrdCategorias.DataSource.DataSet.RecordCount > 0) then
    begin
        PermitirEdicion(True);
        txt_Factor.SetFocus;
    end;    
end;

procedure TABMCategorias.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TABMCategorias.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos de la categor�a.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar categor�a';
    MSG_FACTOR              = 'Debe ingresar el factor';
var
	CategoriaCambio: integer;
begin
    if not ValidateControls([txt_Factor],
                        [(Trim(txt_Factor.text) <> '') and IsFloat(txt_Factor.text)],
                        MSG_ACTUALIZAR_CAPTION,
                        [MSG_FACTOR]) then
                        Exit;
             
    if MsgBox('�Confirma actualizar la categor�a?', 'Confirmaci�n', MB_YESNO) = IDNO then
    begin
        BtnCancelarClick(nil);
        Exit;
    end;                             

    try
        Screen.Cursor := crHourGlass;
        try
            CategoriaCambio := 0;
            If cbbCategoriaCambio.ItemIndex >= 0 then
                CategoriaCambio := cbbCategoriaCambio.Items[cbbCategoriaCambio.ItemIndex].Value;

            if objCategorias.ActualizarCategorias(StrToInt(txt_Categoria.Text),
                                                    objCategorias.IDCategoriaClase,
                                                    StrToFloat(txt_Factor.Text),
                                                    CategoriaCambio) then
            begin

                objCategorias.FactorTarifa := StrToFloat(txt_Factor.Text);
                if cbbCategoriaCambio.ItemIndex <= 0  then
                    objCategorias.CategoriaCambio := null
                else if cbbCategoriaCambio.ItemIndex > 0  then
                    objCategorias.CategoriaCambio := StrToInt(cbbCategoriaCambio.Value);

                //objCategorias.AceptarCambios;         //TASK_109_JMA_20170215
                objCategorias.ClientDataSet.Post;       //TASK_109_JMA_20170215

                PermitirEdicion(False);
            end;
        except
            On E: exception do begin
                MsgBoxErr(MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);                    
                Exit;
            end;
        end;
    finally
        Screen.Cursor 	   := crDefault;
    end   
end;

procedure TABMCategorias.Limpiar_Campos();
begin
    txt_Categoria.Clear;
	txt_Descripcion.Clear;
    txt_Factor.Clear;
    txt_Clase.Clear;
    cbbCategoriaCambio.Items.Clear;
end;

procedure TABMCategorias.BtnCancelarClick(Sender: TObject);
begin
    Limpiar_Campos;
    PermitirEdicion(False);
    Categorias_AfterScroll(dbgrdCategorias.DataSource.DataSet);
end;

procedure TABMCategorias.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    objCategorias.Free;
    dbgrdCategorias.DataSource.Free;
    action := caFree;
end;
{TERMINO: TASK_094_JMA_20160111}


end.


