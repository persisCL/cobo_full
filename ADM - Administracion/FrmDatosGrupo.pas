{-----------------------------------------------------------------------------
 File Name: FrmDatosGrupo
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit FrmDatosGrupo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, UtilProc, UtilDB, DPSControls, DB, ADODB;

type
  TFormDatosGrupo = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    txt_codigogrupo: TEdit;
    txt_descripcion: TEdit;
    btn_ok: TButton;
    Button2: TButton;
    spGruposSistemas: TADOStoredProc;
    spUsuariosSistemas_1: TADOStoredProc;
    spINSGruposSistemas: TADOStoredProc;
    spUPDUsuariosSistemas: TADOStoredProc;
    procedure txt_codigogrupoChange(Sender: TObject);
    procedure btn_okClick(Sender: TObject);
  private
	{ Private declarations }
	FGrupo: AnsiString;
  public
	{ Public declarations }
	function Inicializa(Grupo: AnsiString): Boolean;
  end;

var
  FormDatosGrupo: TFormDatosGrupo;

implementation

uses DMConnection;

resourcestring
    MSG_ERROR_EXISTE_GRUPO	= 'El codigo de grupo indicado ya existe.';
    MSG_VALIDAR_CAPTION 	= 'Validar Grupo';
    
{$R *.DFM}

{ TFormDatosGrupo }

{-----------------------------------------------------------------------------
  Function Name:Inicializa
  Author:
  Date Created:  /  /
  Description:
  Parameters: Grupo: AnsiString
  Return Value: Boolean

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormDatosGrupo.Inicializa(Grupo: AnsiString): Boolean;
begin
	FGrupo := Grupo;
	if Grupo <> '' then begin
		txt_CodigoGrupo.Text := TrimRight(Grupo);
		txt_CodigoGrupo.Color := clBtnFace;
		txt_CodigoGrupo.ReadOnly := True;
         // JLO 20160513
        spGruposSistemas.Parameters.ParamByName('@CodigoGrupo').Value :=  Grupo;
        spGruposSistemas.Open;
        txt_Descripcion.Text := Trim(spGruposSistemas.FieldByName('Descripcion').AsString);
        spGruposSistemas.Close;
        { //JLO 20160513
		txt_Descripcion.Text := TrimRight(QueryGetValue(DMConnections.BaseBO_Master,
		  'SELECT Descripcion FROM GruposSistemas  WITH (NOLOCK) WHERE CodigoGrupo = ''' +
		  Grupo + ''''));
        }
		ActiveControl := txt_Descripcion;
	end;
	Result := True;
end;

procedure TFormDatosGrupo.txt_codigogrupoChange(Sender: TObject);
begin
	btn_ok.enabled := (Trim(txt_codigogrupo.text) <> '');
end;

{-----------------------------------------------------------------------------
  Function Name: btn_okClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormDatosGrupo.btn_okClick(Sender: TObject);
begin
	// Si es un grupo nuevo, validamos los datos

	if FGrupo = '' then begin
        // JLO 20160513
        spUsuariosSistemas_1.Parameters.ParamByName('@CodigoGrupo').Value :=  Trim(txt_codigogrupo.text);
        spUsuariosSistemas_1.ExecProc;

         if spUsuariosSistemas_1.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin

            MsgBox( MSG_ERROR_EXISTE_GRUPO, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
			Exit;

         end;
         { //JLO 20160513
		if QueryGetValue(DMConnections.BaseBO_Master,
		  'SELECT 1 FROM GruposSistemas  WITH (NOLOCK) WHERE CodigoGrupo = ''' +
		    Trim(txt_codigogrupo.text) + '''') <> '' then begin
			MsgBox( MSG_ERROR_EXISTE_GRUPO, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
			Exit;
		end;
        }

		// Todo Ok. Damos de Alta el grupo.
        //JLO 20160513
        spINSGruposSistemas.Parameters.ParamByName('@CodigoGrupo').Value :=  txt_codigogrupo.text;
        spINSGruposSistemas.ExecProc;

        { //JLO 20160513
		QueryExecute(DMConnections.BaseBO_Master,
		  'INSERT INTO GruposSistemas (CodigoGrupo) ' +
		  'VALUES (''' + txt_codigogrupo.text + ''')');
        }
		FGrupo := txt_codigogrupo.text; 
	end;
	// Grabamos los datos del Grupo
     spUPDUsuariosSistemas.Parameters.ParamByName('@CodigoGrupo').Value :=  FGrupo;
     spUPDUsuariosSistemas.Parameters.ParamByName('@Descripcion').Value :=  Trim(txt_descripcion.text);
     spUPDUsuariosSistemas.ExecProc;
    {
	QueryExecute(DMConnections.BaseBO_Master, 'UPDATE GruposSistemas SET Descripcion = ''' +
	  txt_descripcion.text + ''' WHERE CodigoGrupo = ''' + FGrupo + '''');
    }
	// Listo
	ModalResult := mrOk;
end;

end.
