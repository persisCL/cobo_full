unit FrmEditarTarifaPuntoCobro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, TimeEdit, VariantComboBox,
  Categoria, PlanesTarifarios, UtilProc, PuntosCobro, PeaProcs;

type
  TFormEditarTarifaPuntoCobro = class(TForm)
    lblPuntoCobro: TLabel;
    cbbPuntosCobro: TVariantComboBox;
    lblCategoria: TLabel;
    lblTipoDia: TLabel;
    cbbTipoDia: TVariantComboBox;
    grp_Hora_Desde_Hasta: TGroupBox;
    lblHoraDesde: TLabel;
    lblHoraHasta: TLabel;
    edtHoraDesde: TTimeEdit;
    edtHoraHasta: TTimeEdit;
    lblTipoTarifa: TLabel;
    cbbTipoTarifa: TVariantComboBox;
    lblImporte: TLabel;
    txtImporte: TEdit;
    btnGuardar: TButton;
    btnCancelar: TButton;
    cbbCategoria: TVariantComboBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure cbbTipoTarifaChange(Sender: TObject);
  private
    { Private declarations }
    oPlan: TPlanTarifarioVersion;
    FAccion:Byte;
    FCodigoCategoriaInterurbana: Integer;
    procedure CargarComboPuntoCobro(oPuntosCobro: TPuntoCobro);
    procedure CargarComboCategoria(oCategorias: TCategoria; IdCategoriaClases: Integer);
    procedure CargarComboTipoDia(oTiposDias: TTarifasDiasTipos);
    procedure CargarComboTarifaTipo(oTiposTarifas: TTarifasTipos);
  public
    { Public declarations }
    function Inicializar(Plan: TPlanTarifarioVersion; oCategorias: TCategoria; oPuntosCobro: TPuntoCobro; oTiposDias: TTarifasDiasTipos; oTiposTarifas: TTarifasTipos; CodigoCategoriaInterurbana: Integer; accion: Byte): Boolean;
  end;

var
  FormEditarTarifaPuntoCobro: TFormEditarTarifaPuntoCobro;

implementation

{$R *.dfm}


function TFormEditarTarifaPuntoCobro.Inicializar(Plan: TPlanTarifarioVersion; oCategorias: TCategoria; oPuntosCobro: TPuntoCobro; oTiposDias: TTarifasDiasTipos; oTiposTarifas: TTarifasTipos; CodigoCategoriaInterurbana: Integer; accion: Byte): Boolean;
begin
    oPlan := Plan;
    FAccion := accion;
    FCodigoCategoriaInterurbana:= CodigoCategoriaInterurbana;

    CargarComboPuntoCobro(oPuntosCobro);
    CargarComboCategoria(oCategorias, Plan.ID_CategoriasClases);
    CargarComboTipoDia(oTiposDias);
    CargarComboTarifaTipo(oTiposTarifas);


    Result := False;
    if cbbPuntosCobro.Items.Count > 0 then
    begin
        if accion = 2 then
        begin
            cbbPuntosCobro.Value := Plan.oTarifas.NumeroPuntoCobro;
            cbbPuntosCobro.Enabled := False;
            cbbCategoria.Value := Plan.oTarifas.Categoria;
            cbbTipoDia.Value := Plan.oTarifas.CodigoDiaTipo;
            cbbTipoTarifa.Value := Plan.oTarifas.CodigoTarifaTipo;
            edtHoraDesde.Time := StrToTime(Plan.oTarifas.HoraDesde);
            edtHoraHasta.Time := StrToTime(Plan.oTarifas.HoraHasta);
            txtImporte.Text := FloatToStr(Plan.oTarifas.Importe);

            if (oPlan.oTarifas.HoraDesde = '00:00') and (oPlan.oTarifas.HoraHasta = '23:59') then
                cbbTipoTarifa.Enabled := False;
        end;

        Result := True
    end
    else
        MsgBox('No existen puntos de cobro para la categor�a de plan seleccionado', 'Sin punto de cobro');

    cbbTipoTarifaChange(nil);
end;


procedure TFormEditarTarifaPuntoCobro.CargarComboPuntoCobro(oPuntosCobro: TPuntoCobro);
begin
    cbbPuntosCobro.Items.Clear;

    try
        try
            oPuntosCobro.ClientDataSet.Filter := ' IDCategoriaClase = ' + QuotedStr(IntToStr(oPlan.ID_CategoriasClases));
            oPuntosCobro.ClientDataSet.First;
            while not oPuntosCobro.ClientDataSet.Eof do            
            begin
                cbbPuntosCobro.Items.InsertItem(oPuntosCobro.Descripcion, oPuntosCobro.NumeroPuntoCobro);
                oPuntosCobro.ClientDataSet.Next;
            end;
        except
            on e:Exception do
                MsgBoxErr('Error cargando Puntos de Cobro', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally        
        if cbbPuntosCobro.Items.Count > 0 then
            cbbPuntosCobro.ItemIndex := 0;
    end;
end;

procedure TFormEditarTarifaPuntoCobro.CargarComboCategoria(oCategorias: TCategoria; IdCategoriaClases: Integer);
begin
    cbbCategoria.Items.Clear;

    try
        try
            oCategorias.ClientDataSet.Filter := ' IDCategoriaClase = ' + QuotedStr(IntToStr(oPlan.ID_CategoriasClases));
            oCategorias.ClientDataSet.First;
            while not oCategorias.ClientDataSet.Eof do            
            begin
                if oCategorias.IDCategoriaClase = IdCategoriaClases then
                   cbbCategoria.Items.InsertItem(oCategorias.Descripcion, oCategorias.CodigoCategoria);
                oCategorias.ClientDataSet.Next;
            end;
        except
            on e:Exception do
                MsgBoxErr('Error cargando Categorias', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        if cbbCategoria.Items.Count > 0 then
            cbbCategoria.ItemIndex := 0;
    end;
end;

procedure TFormEditarTarifaPuntoCobro.CargarComboTipoDia(oTiposDias: TTarifasDiasTipos);
begin

    cbbTipoDia.Items.Clear;
    
    try
        try
            oTiposDias.ClientDataSet.First;
            while not oTiposDias.ClientDataSet.Eof do            
            begin
                cbbTipoDia.Items.InsertItem(oTiposDias.Descripcion, oTiposDias.CodigoDiaTipo);
                oTiposDias.ClientDataSet.Next;
            end;

        except
            on e:Exception do
                MsgBoxErr('Error cargando Tipos de Dias', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        if cbbTipoDia.Items.Count > 0 then
            cbbTipoDia.ItemIndex:= 0;
    end;

end;

procedure TFormEditarTarifaPuntoCobro.CargarComboTarifaTipo(oTiposTarifas: TTarifasTipos);
begin

    cbbTipoTarifa.Items.Clear;

    try
        try

            oTiposTarifas.ClientDataSet.Filter := ' ID_CategoriasClases = ' + QuotedStr(IntToStr(oPlan.ID_CategoriasClases)); 
            oTiposTarifas.ClientDataSet.First;
            while not oTiposTarifas.ClientDataSet.Eof do            
            begin
                cbbTipoTarifa.Items.InsertItem(oTiposTarifas.Descripcion, oTiposTarifas.CodigoTarifaTipo);
                oTiposTarifas.ClientDataSet.Next;
            end;

        except
            on e:Exception do
                MsgBoxErr('Error cargando Tipos de Dias', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        if cbbTipoTarifa.Items.Count > 0 then
            cbbTipoTarifa.ItemIndex:= 0;
    end;
end;


procedure TFormEditarTarifaPuntoCobro.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TFormEditarTarifaPuntoCobro.btnGuardarClick(Sender: TObject);
var
    m: string;     
begin
    if not ValidateControls([edtHoraDesde, edtHoraHasta, edtHoraHasta, txtImporte],
                     [edtHoraDesde.Text <> '',
                     edtHoraHasta.Text <> '',
                     edtHoraHasta.Time > edtHoraDesde.Time,
                     (Trim(txtImporte.Text) <> '') and IsFloat(txtImporte.Text)],
                     'Atenci�n',
                     ['Indique Hora Desde',
                     'Indique Hora Hasta',
                     'Hora Hasta debe ser mayor a Hora Desde',
                     'Indique Importe v�lido']) then
                     Exit;


    try
        oPlan.oTarifas.ActivarAfterScroll(False);
        m:= oPlan.oTarifas.ClientDataSet.Bookmark;
        oPlan.oTarifas.ClientDataSet.First;
        while not oPlan.oTarifas.ClientDataSet.Eof do
        begin
            if (oPlan.oTarifas.Estado <> 3) then          //NO eliminado, solo por seguridad pues no se procesan los filtrados
            begin
                if (FAccion = 1) or ((FAccion = 2) and (m <> oPlan.oTarifas.ClientDataSet.Bookmark)) then   //si estoy editando no chequeo la misma tarifa
                begin
                    if  (oPlan.oTarifas.NumeroPuntoCobro = cbbPuntosCobro.Value)
                        and (oPlan.oTarifas.Categoria = cbbCategoria.Value)
                        and (oPlan.oTarifas.CodigoDiaTipo = cbbTipoDia.Value)
                        and (oPlan.oTarifas.CodigoTarifaTipo = cbbTipoTarifa.Value)
                         then
                    begin
                        if (FCodigoCategoriaInterurbana <> oPlan.ID_CategoriasClases) and (oPlan.oTarifas.CodigoTarifaTipo = 'TBFP') then
                        begin
                            MsgBox('Ya existe la tarifa TBFP para: ' + Chr(13)
                            + oPlan.oTarifas.DescripcionPuntoCobro + Chr(13)
                            + oPlan.oTarifas.DescripcionCategoria + Chr(13)
                            + oPlan.oTarifas.DescripcionDiaTipo + Chr(13)
                            + oPlan.oTarifas.DescripcionTarifaTipo + Chr(13)
                            + oPlan.oTarifas.HoraDesde + Chr(13)
                            + oPlan.oTarifas.HoraHasta + Chr(13)
                            + FloatToStr(oPlan.oTarifas.Importe), 'Atenci�n');
                            Exit;
                        end
                        else if (
                            (edtHoraDesde.Time >= StrToTime(oPlan.oTarifas.HoraDesde)) and (edtHoraDesde.Time <= StrToTime(oPlan.oTarifas.HoraHasta))
                            or
                            (edtHoraHasta.Time >= StrToTime(oPlan.oTarifas.HoraDesde)) and (edtHoraHasta.Time <= StrToTime(oPlan.oTarifas.HoraHasta))
                            or
                            (edtHoraDesde.Time < StrToTime(oPlan.oTarifas.HoraDesde)) and (edtHoraHasta.Time > StrToTime(oPlan.oTarifas.HoraHasta))
                            )
                        then
                        begin
                            MsgBox('Las Horas ingresadas chocan con la de la tarifa: ' + Chr(13)
                            + oPlan.oTarifas.DescripcionPuntoCobro + Chr(13)
                            + oPlan.oTarifas.DescripcionCategoria + Chr(13)
                            + oPlan.oTarifas.DescripcionDiaTipo + Chr(13)
                            + oPlan.oTarifas.DescripcionTarifaTipo + Chr(13)
                            + oPlan.oTarifas.HoraDesde + Chr(13)
                            + oPlan.oTarifas.HoraHasta + Chr(13)
                            + FloatToStr(oPlan.oTarifas.Importe), 'Atenci�n');
                            Exit;
                        end;
                    end;
                end;
            end;
            oPlan.oTarifas.ClientDataSet.Next;
        end;

        ModalResult := mrOK;
    finally                              
        oPlan.oTarifas.ClientDataSet.Bookmark := m;
        oPlan.oTarifas.ActivarAfterScroll(True);
        oPlan.oTarifas.ClientDataSet.EnableControls;
    end;


end;

procedure TFormEditarTarifaPuntoCobro.cbbTipoTarifaChange(Sender: TObject);
begin
    edtHoraDesde.Enabled := True;
    edtHoraHasta.Enabled := True;
    if (cbbTipoTarifa.Value = 'TBFP') or (cbbTipoTarifa.Value = 'TBI') then
    begin
        edtHoraDesde.Enabled := False;
        edtHoraHasta.Enabled := False;
        edtHoraDesde.Time:=StrToTime('00:00');
        edtHoraHasta.Time:=StrToTime('23:59');
    end;
end;

end.
