unit ABMConfiguracionNotificaciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, UtilDB, Variants,
  UtilProc, DmiCtrls, ADODB, DMConnection, Util, PeaTypes, DPSControls, PeaProcs,
  ComCtrls;

type
  TFormConfiguracionNotificaciones = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    Panel2: TPanel;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    ConfiguracionNotificaciones: TADOTable;
    PageControl: TPageControl;
    tab_General: TTabSheet;
    tab_TextoAEnviar: TTabSheet;
    txt_TextoAEnviar: TMemo;
    Label15: TLabel;
    txt_Codigo: TNumericEdit;
    Label1: TLabel;
    txt_Descripcion: TEdit;
    Label3: TLabel;
    txt_Dias: TNumericEdit;
    Label4: TLabel;
    txt_MontoMinimo: TNumericEdit;
    Label2: TLabel;
    cb_MediosContacto: TComboBox;
    chk_Activado: TCheckBox;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;
  end;

var
  FormConfiguracionNotificaciones: TFormConfiguracionNotificaciones;

implementation

resourcestring
    MSG_ACTUALIZAR_ERROR  	= 'No se pudieron actualizar los datos de la Configuración.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Configuración';

{$R *.DFM}

procedure TFormConfiguracionNotificaciones.VolverCampos;
begin
	Tab_General.Enabled      := False;
	Tab_TextoAEnviar.Enabled := False;
	DbList1.Estado           := Normal;
	DbList1.Enabled          := True;
	DbList1.SetFocus;
	Notebook.PageIndex       := 0;
    txt_Codigo.Enabled       := True;
end;

function TFormConfiguracionNotificaciones.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([ConfiguracionNotificaciones]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormConfiguracionNotificaciones.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormConfiguracionNotificaciones.DBList1Click(Sender: TObject);
begin
	with ConfiguracionNotificaciones do begin
		txt_Codigo.Value	            := FieldByName('ID').AsInteger;
		txt_Descripcion.Text            := FieldByName('DescripcionAccion').AsString;
        txt_Dias.ValueInt	            := FieldByName('NumeroDias').AsInteger;
        txt_MontoMinimo.ValueInt        := FieldByName('MontoMinimo').AsInteger;
        CargarMediosContacto(DMConnections.BaseCAC, cb_MediosContacto, FieldByName('CodigoMedioContacto').AsInteger, True);
        txt_TextoAEnviar.Text           := FieldByName('TextoAEnviar').AsString;
        chk_Activado.Checked            := FieldByName('Activado').AsBoolean;
	end;
end;

procedure TFormConfiguracionNotificaciones.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormConfiguracionNotificaciones.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado := modi;
end;

procedure TFormConfiguracionNotificaciones.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormConfiguracionNotificaciones.Limpiar_Campos;
begin
	txt_Codigo.Clear;
	txt_Descripcion.Clear;
end;

procedure TFormConfiguracionNotificaciones.AbmToolbar1Close(Sender: TObject);
begin
    Close;
end;

procedure TFormConfiguracionNotificaciones.BtnAceptarClick(Sender: TObject);
var
    CodigoMedioContacto: Integer;
begin
 	Screen.Cursor := crHourGlass;
    CodigoMedioContacto := Ival(StrRight(cb_MediosContacto.Text, 10));
	With ConfiguracionNotificaciones do begin
		Try
			Edit;
			FieldByName('DescripcionAccion').AsString := Trim(txt_Descripcion.Text);
			FieldByName('NumeroDias').AsInteger		  := txt_Dias.ValueInt;
			FieldByName('MontoMinimo').AsInteger	  := txt_MontoMinimo.ValueInt;
            FieldByName('CodigoMedioContacto').Value  := IIf ((CodigoMedioContacto = 0), NULL, CodigoMedioContacto);
  			FieldByName('TextoAEnviar').Value         := IIf((Trim(txt_TextoAEnviar.Text) = ''), NULL, Trim(txt_TextoAEnviar.Text));
			FieldByName('Activado').AsBoolean	      := chk_Activado.Checked;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
                Screen.Cursor := crDefault;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor := crDefault;
end;

procedure TFormConfiguracionNotificaciones.FormShow(Sender: TObject);
begin
    DBList1.Reload;
end;

procedure TFormConfiguracionNotificaciones.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormConfiguracionNotificaciones.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormConfiguracionNotificaciones.HabilitarCampos;
begin
	Tab_General.Enabled      := True;
	Tab_TextoAEnviar.Enabled := True;
	DbList1.Enabled          := False;
	Notebook.PageIndex       := 1;
    txt_Codigo.Enabled       := False;
end;

end.
