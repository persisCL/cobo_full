{-----------------------------------------------------------------------------
 File Name: FormAgregarCategoriaDayPass.pas
 Author:    flamas
 Date Created: 21/02/2005
 Language: ES-AR
 Description: Carga las categorías de los Pases Diarios
-----------------------------------------------------------------------------}
unit FormAgregarCategoriaDayPass;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, DB, ADODB, ListBoxEx, DBListEx, DMConnection,
  UtilProc;

type
  TFrmAgregarCategoriaDayPass = class(TForm)
    spObtenerCategorias: TADOStoredProc;
    dbCategoria: TDBListEx;
    dsObtenerCategorias: TDataSource;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar : boolean;
  end;

var
  FrmAgregarCategoriaDayPass: TFrmAgregarCategoriaDayPass;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/02/2005
  Description: Inicializa el Form de Carga de Categorías de Pase Diario
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFrmAgregarCategoriaDayPass.Inicializar : boolean;
resourcestring
	MSG_ERROR_LOADING_CATEGORIES = 'Error cargando Categorías';
    MSG_ERROR = 'Error';
begin
	try
    	spObtenerCategorias.Open;
        result := not spObtenerCategorias.IsEmpty;
    except
    	on e: exception do begin
        	MsgBoxErr(MSG_ERROR_LOADING_CATEGORIES, E.message, MSG_ERROR, MB_ICONSTOP);
        	result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    flamas
  Date Created: 21/02/2005
  Description: Confirma la selección
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrmAgregarCategoriaDayPass.BtnAceptarClick(Sender: TObject);
begin
	ModalResult := mrOk;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    flamas
  Date Created: 21/02/2005
  Description: Libera el form de memoria
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrmAgregarCategoriaDayPass.FormDestroy(Sender: TObject);
begin
    spObtenerCategorias.Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    flamas
  Date Created: 21/02/2005
  Description: Cierra el Form
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrmAgregarCategoriaDayPass.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action:= caFree;
end;


end.
