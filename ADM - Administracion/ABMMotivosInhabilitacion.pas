unit ABMMotivosInhabilitacion;
//------------------------------------------------------------------
// firma        : SS-1006-NDR-20120712
// Description  : Mantener los motivos de ihabilitacion de convenios
//------------------------------------------------------------------
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB, DMConnection,
  CheckLst, ComCtrls, variants, DPSControls;

type
  TFormMotivosInhabilitacion = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Panel1: TPanel;
    Notebook: TNotebook;
    MotivosInhabilitacion: TADOTable;
    AgregarMotivosInhabilitacion: TADOStoredProc;
    Panel3: TPanel;
    Label15: TLabel;
    neCodigoMotivoInhabilitacion: TNumericEdit;
    txtDescripcion: TEdit;
    Label1: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormMotivosInhabilitacion  : TFormMotivosInhabilitacion;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Motivo de Inhabilitacion?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Motivo de Inhabilitacion porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Motivo de Inhabilitacion';
    MSG_TIPO_ORDEN_SERVICIO = 'No se pudieron actualizar los Tipos de Orden de Servicio asociados al Motivo de Inhabilitacion.';

    MSG_DESCRIPCION         = 'Debe especificarse la descripci�n del Motivo de Inhabilitacion';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Motivo de Inhabilitacion.';
    CAPTION_VALIDAR_MOTIVO_Inhabilitacion	= 'Actualizar Motivo de Inhabilitacion';


    CAPTION_TIPO_ORDEN_SERVICIO = 'Actualizar Tipos de Orden de Servicio';
    MSG_INSERTAR_MOTIVO_Inhabilitacion = 'No se pudo insertar el nuevo Motivo de Inhabilitacion.';
    CAPTION_INSERTAR_MOTIVO_Inhabilitacion = 'Insertar Motivo Inhabilitacion';



{$R *.DFM}

function TFormMotivosInhabilitacion.Inicializa: boolean;
Var S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

	if not OpenTables([MotivosInhabilitacion]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
        Lista.Enabled := true;
        Lista.SetFocus;
	end;
   	Notebook.PageIndex := 0;
end;

procedure TFormMotivosInhabilitacion.Limpiar_Campos();
begin
	neCodigoMotivoInhabilitacion.Clear;
	txtDescripcion.Clear;
end;

function TFormMotivosInhabilitacion.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
	Texto :=  PadR(Trim(Tabla.FieldByName('CodigoMotivoInhabilitacion').AsString), 4, ' ' ) + ' '+
	  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormMotivosInhabilitacion.BtSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormMotivosInhabilitacion.ListaInsert(Sender: TObject);
begin
	Screen.Cursor   := crHourGlass;
	Lista.Estado    := Alta;
	Limpiar_Campos;
    txtDescripcion.Enabled 	:= true;
	Lista.Enabled   := False;
	Notebook.PageIndex := 1;
   	txtDescripcion.SetFocus;
	Screen.Cursor   := crDefault;
end;

procedure TFormMotivosInhabilitacion.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
 	txtDescripcion.Enabled 	        := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
   	txtDescripcion.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormMotivosInhabilitacion.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) = IDYES then begin
        DMConnections.BaseCAC.BeginTrans;
        try
            try
                MotivosInhabilitacion.Delete;
                DMConnections.BaseCAC.CommitTrans;
            except
                On E: EDataBaseError do begin
                    MotivosInhabilitacion.Cancel;
                    DMConnections.BaseCAC.RollbackTrans;
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                end else
                    DMConnections.BaseCAC.RollbackTrans;;
            end;
        finally
            Lista.Estado     	:= Normal;
            Lista.Enabled    	:= True;
            txtDescripcion.Enabled := False;
            Notebook.PageIndex 	:= 0;
            Screen.Cursor      	:= crDefault;
		    Lista.Reload;
        end;
    end;
end;

procedure TFormMotivosInhabilitacion.ListaClick(Sender: TObject);
begin
	with MotivosInhabilitacion do begin
        neCodigoMotivoInhabilitacion.Value := FieldByName('CodigoMotivoInhabilitacion').AsInteger;
        txtDescripcion.text	 := Trim(FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormMotivosInhabilitacion.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormMotivosInhabilitacion.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormMotivosInhabilitacion.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('CodigoMotivoInhabilitacion').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormMotivosInhabilitacion.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormMotivosInhabilitacion.BtnAceptarClick(Sender: TObject);

  function execAgregarMotivosInhabilitacion:Integer;
  begin
      result := 0;
      try
          with AgregarMotivosInhabilitacion do begin
              Parameters.Refresh;
              Parameters.ParamByName('@Descripcion').Value := txtDescripcion.Text;
              Parameters.ParamByName('@CodigoMotivoInhabilitacion').Value := null;
              execProc;
              result := Parameters.ParamByName('@CodigoMotivoInhabilitacion').value;
          end;
      except
          On E: Exception do begin
              MsgBoxErr(MSG_INSERTAR_MOTIVO_Inhabilitacion, E.message, CAPTION_INSERTAR_MOTIVO_Inhabilitacion, MB_ICONSTOP);
          end;
      end;
  end;

  function ActualizarMotivoInhabilitacion: Boolean;
  begin
      result := true;
      with MotivosInhabilitacion do begin
          edit;
	      try
    		  FieldByName('Descripcion').AsString 	  := Trim(txtDescripcion.Text);
			  Post;
   		  except
              On E: Exception do begin
                  Result := False;
 			      Cancel;
			      MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, CAPTION_VALIDAR_MOTIVO_Inhabilitacion, MB_ICONSTOP);
              end;
	   	  end;
	  end;
  end;

begin
	Screen.Cursor := crHourGlass;
    if not ValidateControls([txtDescripcion],
      [Trim(txtDescripcion.text) <> ''],
      CAPTION_VALIDAR_MOTIVO_Inhabilitacion,
      [MSG_DESCRIPCION]) then exit;

	Screen.Cursor := crHourGlass;
    try
        DMConnections.BaseCAC.BeginTrans;
        // Ahora agrego o modifico el motivo de Inhabilitacion.
        if (Lista.Estado = Alta) then begin
            neCodigoMotivoInhabilitacion.valueint := ExecAgregarMotivosInhabilitacion;
            if (neCodigoMotivoInhabilitacion.ValueInt = 0) then begin
                DMConnections.BaseCAC.RollbackTrans;
                exit;
            end;
        end else
            if not ActualizarMotivoInhabilitacion then begin
                DMConnections.BaseCAC.RollbackTrans;
                exit;
            end;
        DMConnections.BaseCAC.CommitTrans;
    finally
        txtDescripcion.Enabled 	:= False;
	    Lista.Estado     := Normal;
    	Lista.Enabled    := True;
	    Lista.SetFocus;
    	Notebook.PageIndex := 0;
	    Screen.Cursor 	   := crDefault;
    end;
end;

procedure TFormMotivosInhabilitacion.BtnCancelarClick(Sender: TObject);
begin
 	txtDescripcion.Enabled 	        := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormMotivosInhabilitacion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormMotivosInhabilitacion.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
