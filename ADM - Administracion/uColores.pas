unit uColores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TfrmColores = class(TForm)
    ColorDialog1: TColorDialog;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmColores: TfrmColores;

implementation

{$R *.dfm}

procedure TfrmColores.FormShow(Sender: TObject);
begin
  ColorDialog1.Execute;
end;

end.
