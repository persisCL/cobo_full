unit frmHabilitarPlanTarifario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, PlanesTarifarios;

type
  TFormHabilitarPlanTarifario = class(TForm)
    lblHabilitadoPor: TLabel;
    lstHabilitadores: TListBox;
    lblHabilitar: TLabel;
    cbbHabilitar: TComboBox;
    btnAceptar: TButton;
    btnCancelar: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(Plan: TPlanTarifarioVersion; FechaActual: TDateTime): Boolean;
  end;

var
  FormHabilitarPlanTarifario: TFormHabilitarPlanTarifario;

implementation

{$R *.dfm}

function TFormHabilitarPlanTarifario.Inicializar(Plan: TPlanTarifarioVersion; FechaActual: TDateTime):Boolean;
begin

     lstHabilitadores.Items.Clear;
     if Plan.UsuarioHabilitacion1 <> '' then
        lstHabilitadores.Items.Add(plan.UsuarioHabilitacion1);
     if Plan.UsuarioHabilitacion2 <> '' then
        lstHabilitadores.Items.Add(plan.UsuarioHabilitacion2);
     if Plan.UsuarioHabilitacion3 <> '' then
        lstHabilitadores.Items.Add(plan.UsuarioHabilitacion3);

     cbbHabilitar.ItemIndex := 0;

     if Plan.Habilitado then
     begin
         if (Plan.FechaActivacion<= FechaActual) then
            btnAceptar.Enabled := False
         else
         begin
             cbbHabilitar.ItemIndex:= 1;
             cbbHabilitar.Enabled:= false;
         end;
     end;



     Result:= True;
end;

procedure TFormHabilitarPlanTarifario.btnAceptarClick(Sender: TObject);
begin
    ModalResult:= mrOk;
end;

procedure TFormHabilitarPlanTarifario.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

end.
