object FormCodigosComercio: TFormCodigosComercio
  Left = 181
  Top = 122
  Width = 615
  Height = 491
  ActiveControl = GrillaCodigosComercio
  Caption = 'Mantenimiento de C'#243'digos de Comercio'
  Color = clBtnFace
  Constraints.MinHeight = 450
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  Visible = True
  OnClose = FormClose
  DesignSize = (
    607
    464)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 129
    Width = 570
    Height = 13
    Caption = 
      'Ingrese los datos de los C'#243'digos de Comercio para cada una de la' +
      's empresas de Tarjetas de Cr'#233'dito'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 19
    Top = 40
    Width = 90
    Height = 13
    Caption = 'Operador log'#237'stico:'
  end
  object Label3: TLabel
    Left = 19
    Top = 89
    Width = 87
    Height = 13
    Caption = 'Tarjeta de Cr'#233'dito:'
  end
  object Label4: TLabel
    Left = 19
    Top = 15
    Width = 52
    Height = 13
    Caption = 'Aplicaci'#243'n:'
  end
  object Label5: TLabel
    Left = 19
    Top = 65
    Width = 75
    Height = 13
    Caption = 'Lugar de venta:'
  end
  object GrillaCodigosComercio: TStringGrid
    Left = 15
    Top = 147
    Width = 578
    Height = 266
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 2
    DefaultRowHeight = 18
    FixedColor = 14732467
    FixedCols = 0
    RowCount = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 3
    OnEnter = GrillaCodigosComercioEnter
    OnKeyPress = GrillaCodigosComercioKeyPress
    OnSelectCell = GrillaCodigosComercioSelectCell
    ColWidths = (
      213
      225)
  end
  object cb_OperadorLogistico: TComboBox
    Left = 114
    Top = 33
    Width = 321
    Height = 21
    Hint = 'Operador Log'#237'stico'
    Style = csDropDownList
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnChange = cb_OperadorLogisticoChange
  end
  object cb_Tarjetas: TComboBox
    Left = 114
    Top = 84
    Width = 321
    Height = 21
    Hint = 'Tarjeta de cr'#233'dito'
    Style = csDropDownList
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = ActualizarGrilla
  end
  object Panel1: TPanel
    Left = 0
    Top = 423
    Width = 607
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    DesignSize = (
      607
      41)
    object btn_Aceptar: TDPSButton
      Left = 444
      Top = 7
      Anchors = [akTop, akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TDPSButton
      Left = 520
      Top = 7
      Anchors = [akTop, akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object cb_AplicacionesVenta: TComboBox
    Left = 114
    Top = 8
    Width = 321
    Height = 21
    Hint = 'Aplicaci'#243'n'
    Style = csDropDownList
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = ActualizarGrilla
  end
  object cb_LugarDeVenta: TComboBox
    Left = 114
    Top = 58
    Width = 321
    Height = 21
    Hint = 'Lugar de venta'
    Style = csDropDownList
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnClick = ActualizarGrilla
  end
  object ds_CodigosComercio: TDataSource
    DataSet = ObtenerCodigosComercio
    Left = 84
    Top = 171
  end
  object ObtenerCodigosComercio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCodigosComercio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperadorLogistico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 112
    Top = 171
  end
  object ActualizarCodigoComercio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarCodigoComercio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoLugarDeVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoComercio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 140
    Top = 171
  end
end
