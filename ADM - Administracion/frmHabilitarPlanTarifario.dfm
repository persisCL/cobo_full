object FormHabilitarPlanTarifario: TFormHabilitarPlanTarifario
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Habilitar Plan'
  ClientHeight = 162
  ClientWidth = 244
  Color = clBtnFace
  Constraints.MaxHeight = 200
  Constraints.MaxWidth = 260
  Constraints.MinHeight = 200
  Constraints.MinWidth = 260
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblHabilitadoPor: TLabel
    Left = 8
    Top = 8
    Width = 70
    Height = 13
    Caption = 'Habilitado Por:'
  end
  object lblHabilitar: TLabel
    Left = 43
    Top = 90
    Width = 35
    Height = 13
    Caption = 'Acci'#243'n:'
  end
  object lstHabilitadores: TListBox
    Left = 84
    Top = 8
    Width = 143
    Height = 73
    ItemHeight = 13
    TabOrder = 0
  end
  object cbbHabilitar: TComboBox
    Left = 84
    Top = 87
    Width = 143
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 1
    Text = 'Habilitar'
    Items.Strings = (
      'Habilitar'
      'Deshabilitar')
  end
  object btnAceptar: TButton
    Left = 37
    Top = 130
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 128
    Top = 130
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btnCancelarClick
  end
end
