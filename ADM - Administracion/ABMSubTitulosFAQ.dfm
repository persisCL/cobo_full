object frmABMSubTitulosFAQ: TfrmABMSubTitulosFAQ
  Left = 215
  Top = 47
  Width = 710
  Height = 679
  Caption = 'Mantenimiento Subt'#237'tulos de FAQs'
  Color = clBtnFace
  Constraints.MinHeight = 145
  Constraints.MinWidth = 710
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 534
    Width = 702
    Height = 111
    Align = alBottom
    Constraints.MaxHeight = 136
    Constraints.MinHeight = 111
    TabOrder = 0
    object GroupB: TPanel
      Left = 1
      Top = 1
      Width = 700
      Height = 66
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 13
        Top = 39
        Width = 72
        Height = 13
        Caption = '&Descripci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 13
        Top = 11
        Width = 79
        Height = 13
        Caption = 'C'#243'digo Subt'#237'tulo'
      end
      object txt_Descripcion: TEdit
        Left = 114
        Top = 35
        Width = 467
        Height = 21
        Color = 16444382
        MaxLength = 255
        TabOrder = 1
      end
      object txt_CodigoSubTitulo: TNumericEdit
        Left = 114
        Top = 7
        Width = 105
        Height = 21
        TabStop = False
        Color = clBtnFace
        MaxLength = 3
        ReadOnly = True
        TabOrder = 0
        Decimals = 0
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 67
      Width = 700
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Notebook: TNotebook
        Left = 503
        Top = 0
        Width = 197
        Height = 39
        Align = alRight
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 110
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object panABM: TPanel
    Left = 0
    Top = 0
    Width = 702
    Height = 534
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object abmSubTitulos: TAbmToolbar
      Left = 1
      Top = 1
      Width = 700
      Height = 33
      Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    end
    object dblSubTitulos: TAbmList
      Left = 1
      Top = 34
      Width = 700
      Height = 499
      TabStop = True
      TabOrder = 1
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      SubTitulos.Sections = (
        #0'53'#0'C'#243'digo    '
        #0'64'#0'Descripci'#243'n')
      HScrollBar = True
      RefreshTime = 100
      Table = SubTitulosFAQ
      Style = lbOwnerDrawFixed
      ItemHeight = 14
      OnClick = dblSubTitulosClick
      OnProcess = dblSubTitulosProcess
      OnDrawItem = dblSubTitulosDrawItem
      OnRefresh = dblSubTitulosRefresh
      OnInsert = dblSubTitulosInsert
      OnDelete = dblSubTitulosDelete
      OnEdit = dblSubTitulosEdit
      Access = [accAlta, accBaja, accModi]
      Estado = Normal
      ToolBar = abmSubTitulos
    end
  end
  object SubTitulosFAQ: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'FAQSubTitulos'
    Left = 220
    Top = 84
  end
  object qry_MaxSubTitulo: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT ISNULL(MAX(CodigoSubTitulo),0) AS CodigoSubTitulo FROM FA' +
        'QSubTitulos WITH (NOLOCK) ')
    Left = 253
    Top = 84
  end
end
