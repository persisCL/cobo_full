unit ABMAlias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Mask,Util,UtilProc,RStrings;

type
  TFormABMAlias = class(TForm)
    pnl_BotonesGeneral: TPanel;
    Label1: TLabel;
    txt_Alias: TMaskEdit;
    BtnCancelar: TButton;
    BtnAceptar: TButton;
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function GetAlias:String;
  public
    { Public declarations }
    Function Inicializa(Alias:String=''):Boolean;
    property Alias:String read GetAlias;

  end;

var
  FormABMAlias: TFormABMAlias;

implementation

{$R *.dfm}



{ TFormABMAlias }

function TFormABMAlias.Inicializa(Alias: String=''): Boolean;
begin
    txt_Alias.Text:=Alias;
    result:=True;
end;

procedure TFormABMAlias.BtnAceptarClick(Sender: TObject);
begin
    if self.Alias<>'' then
        ModalResult:=mrOk
    else
        MsgBoxBalloon(format(MSG_VALIDAR_DEBE_EL,[STR_ALIAS]),format(MSG_CAPTION_VALIDAR,[STR_ALIAS]),MB_ICONSTOP,txt_Alias);
end;

function TFormABMAlias.GetAlias: String;
begin
    txt_Alias.Text:=trim(UpperCase(txt_Alias.Text));
    result:=txt_Alias.Text;
end;

procedure TFormABMAlias.BtnCancelarClick(Sender: TObject);
begin
    close;
end;

procedure TFormABMAlias.FormShow(Sender: TObject);
begin
    txt_Alias.SetFocus;
end;

end.
