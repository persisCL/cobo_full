unit ABMVehiculos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, VariantComboBox, Variants,
  BuscaTab, StrUtils;

type
  TFormVehiculos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Panel2: TPanel;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    BuscaTablaVehiculos: TBuscaTabla;
    cb_modelo: TComboBox;
    Label20: TLabel;
    Label14: TLabel;
    GroupDimensiones: TGroupBox;
    Label3: TLabel;
    MedidaTolerancia: TNumericEdit;
    CB_unidadTolerancia: TComboBox;
    Label1: TLabel;
    largo: TNumericEdit;
    Label2: TLabel;
    ancho: TNumericEdit;
    Label4: TLabel;
    alto: TNumericEdit;
    Label5: TLabel;
    anio: TNumericEdit;
    Vehiculos: TADOQuery;
    dsVehiculos: TDataSource;
    ActualizarVehiculos: TADOStoredProc;
    EliminarVehiculo: TADOStoredProc;
    cb_marca: TVariantComboBox;
    lblAnioHasta: TLabel;
    AnioHasta: TNumericEdit;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    function BuscaTablaVehiculosProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure cb_marcaChange(Sender: TObject);
    procedure anioExit(Sender: TObject);
    procedure AnioHastaExit(Sender: TObject);
    procedure largoExit(Sender: TObject);
    procedure anchoExit(Sender: TObject);
    procedure altoExit(Sender: TObject);
  private
    { Private declarations }
    CodigoMarca: integer;
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
    procedure HabilitarAnioHasta(habil: boolean);
  public
    { Public declarations }
	function Inicializa(MDIChild: Boolean): boolean;
  end;

var
  FormVehiculos: TFormVehiculos;

implementation

resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Veh�culo seleccionado?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar el Veh�culo seleccionado.';
    MSG_DELETE_CAPTION		= 'Eliminar Veh�culo';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Veh�culo seleccionado.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Veh�culo';
    MSG_MARCA               = 'Debe ingresar la marca';
    MSG_MODELO              = 'Debe ingresar el modelo';
    MSG_ANIO                = 'El a�o es incorrecto. ' + #13#10 +
                              'Debe ser mayor a 1900 y menor al a�o siguiente al actual. ';
    MSG_LARGO               = 'El largo es incorrecto';
    MSG_ANCHO               = 'El ancho es incorrecto';
    MSG_ALTO                = 'El alto es incorrecto';
    MSG_ANIO_CAPTION        = 'Ingreso del a�o ';
    MSG_ANIO_ERROR          = 'El a�o "hasta" no puede ser menor que el A�o indicado.';

{$R *.DFM}

function TFormVehiculos.Inicializa(MDIChild: Boolean): boolean;
Var
	S: TSize;
begin
    CodigoMarca := -1;
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		CenterForm(Self);
	end;
    Result := False;
    CargarMarcasComboVariant(DMConnections.BaseCAC, cb_marca);
    CargarUnidadesToleranciaCategoria(CB_unidadTolerancia, '');
	if not OpenTables([Vehiculos]) then exit;
    Notebook.PageIndex := 0;
    DBList1.Reload;
   	Result := True;
end;

procedure TFormVehiculos.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormVehiculos.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
        MarcarItemComboVariant(cb_marca, FieldByName('Marca').AsInteger);
        CargarModelosVehiculos(DMConnections.BaseCAC, cb_modelo,
                        iif(cb_marca.ItemIndex = 0, null,
                        IntToStr(cb_marca.value)), FieldByName('Modelo').AsInteger);
        anio.ValueInt := FieldByName('Anio').asInteger;
        
        //Dimensiones
        largo.Value := FieldByName('Largo').AsInteger;
        ancho.Value := FieldByName('Ancho').AsInteger;
        Alto.Value := FieldByName('Alto').AsInteger;

        //Tolerancia
        if FieldByName('MedidaTolerancia').IsNull then
             MedidaTolerancia.Clear
        else MedidaTolerancia.ValueInt := FieldByName('MedidaTolerancia').AsInteger;

        //Centimetros o Porcentaje
		if FieldByName('UnidadTolerancia').AsString = 'C' then
			CB_unidadTolerancia.itemindex := 0
        else if FieldByName('UnidadTolerancia').AsString = 'P' then
                CB_unidadTolerancia.itemindex := 1
             else CB_unidadTolerancia.itemindex := -1;

	end;
end;

procedure TFormVehiculos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var
    DescTolerancia: AnsiString;
begin
    DescTolerancia := '';
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Tabla.FieldByName('DescMarca').AsString);
		TextOut(Cols[1], Rect.Top, Tabla.FieldbyName('DescModelo').AsString);
		TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('Anio').AsString);
		TextOut(Cols[3], Rect.Top, Tabla.FieldbyName('Largo').AsString);
		TextOut(Cols[4], Rect.Top, Tabla.FieldbyName('Ancho').AsString);
		TextOut(Cols[5], Rect.Top, Tabla.FieldbyName('Alto').AsString);
        TextOut(Cols[6], Rect.Top, Trim(DescripcionTolerancia(Tabla.FieldbyName('MedidaTolerancia').AsInteger,
                                                              Tabla.FieldbyName('UnidadTolerancia').AsString)));
	end;
end;

procedure TFormVehiculos.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    Dblist1.Estado     		:= modi;
    HabilitarAnioHasta(true);
    AnioHasta.ValueInt      := anio.ValueInt;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    cb_marca.setFocus;
end;

procedure TFormVehiculos.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormVehiculos.Limpiar_Campos();
begin
    cb_marca.ItemIndex := 0;
    cb_modelo.Clear;
    anio.Clear;
    AnioHasta.Clear;
    largo.Value := 0;
    ancho.Value := 0;
    alto.Value  := 0;
    MedidaTolerancia.Clear;
end;

procedure TFormVehiculos.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormVehiculos.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
        try
            screen.Cursor	:= crHourGlass;
            try
                with EliminarVehiculo do begin
                    Parameters.ParamByName('@Marca').Value := Vehiculos.fieldByName('Marca').AsInteger;
                    Parameters.ParamByName('@Modelo').Value := Vehiculos.fieldByName('Modelo').AsInteger;
                    Parameters.ParamByName('@Anio').Value := Vehiculos.fieldByName('Anio').AsInteger;
                    ExecProc;
                end;
                EliminarVehiculo.Close;
            except
                On E: Exception do begin
                    EliminarVehiculo.Close;
                    MsgBox(MSG_DELETE_CAPTION, MSG_DELETE_ERROR, MB_ICONSTOP);
                    exit;
                end;
            end;
            Screen.Cursor := crDefault;
            DbList1.Reload;
        finally
            Screen.Cursor := crDefault;
        end;
        Volver_Campos;
    end;
    //estas tres lineas van?
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;

    Screen.Cursor := crDefault;
end;

procedure TFormVehiculos.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled      := True;
	Limpiar_Campos;
	DbList1.Enabled     := False;
    HabilitarAnioHasta(true);
	Notebook.PageIndex  := 1;
	cb_marca.SetFocus;
end;


procedure TFormVehiculos.BtnAceptarClick(Sender: TObject);
var
    AnioActual: Int64;
begin
    if trim(AnioHasta.Text) = '' then begin
        AnioHasta.ValueInt := Anio.ValueInt;
    end;

    if not ValidateControls(
        [cb_marca, cb_modelo, anio, anioHasta, largo, ancho, alto],
        [cb_marca.ItemIndex > 0,
         cb_modelo.ItemIndex >= 0,
         EsAnioCorrecto(Anio.valueInt),
         EsAnioCorrecto(AnioHasta.valueInt),
         Largo.Value > 0,
         Ancho.Value > 0,
         Alto.Value > 0],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_MARCA, MSG_MODELO, MSG_ANIO, MSG_ANIO, MSG_LARGO, MSG_ANCHO, MSG_ALTO]) then exit;

    if (trim(AnioHasta.text) = '') or (AnioHasta.ValueInt < anio.ValueInt) then begin
        MsgBox(MSG_ANIO_ERROR, MSG_ANIO_CAPTION, MB_ICONSTOP);
        exit;
    end;

    // Este SP Hace el INSERT o UPDATE segun encuentre el registro o no.
	With ActualizarVehiculos do begin
		Try
            //if EstadoLista = Modi then VehiculoActual := DBList1.Table.getBookMark;
            Parameters.ParamByName('@Marca').Value := cb_marca.value;
            Parameters.ParamByName('@Modelo').Value := Ival(rightStr(cb_modelo.Text, 10));
            Parameters.ParamByName('@Anio').Value := anio.ValueInt;
            Parameters.ParamByName('@Largo').Value := largo.ValueInt;
            Parameters.ParamByName('@Ancho').Value := ancho.ValueInt;
            Parameters.ParamByName('@Alto').Value := alto.ValueInt;
            if (MedidaTolerancia.Value > 0) then begin
                Parameters.ParamByName('@UnidadTolerancia').Value := Trim(RightStr(CB_unidadTolerancia.Text, 10));
                Parameters.ParamByName('@MedidaTolerancia').Value := MedidaTolerancia.Value;
            end
            else begin
                Parameters.ParamByName('@UnidadTolerancia').value := NULL;
                Parameters.ParamByName('@MedidaTolerancia').value := NULL;
            end;

            AnioActual := anio.valueInt;
            while (AnioActual <= AnioHasta.ValueInt) do begin
                Parameters.ParamByName('@Anio').Value := AnioActual;
                Inc(AnioActual);
                ExecProc;
            end;
		except
			On E: Exception do begin
                Screen.Cursor := crDefault;
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Volver_Campos;
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor := crDefault;
end;

procedure TFormVehiculos.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormVehiculos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormVehiculos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormVehiculos.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
    HabilitarAnioHasta(false);
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

function TFormVehiculos.BuscaTablaVehiculosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	texto := PadR(Tabla.fieldbyname('Descripcion').AsString, 200, ' ') +
		Tabla.fieldbyname('CodigoMarca').AsString;
    Result := True;
end;

procedure TFormVehiculos.cb_marcaChange(Sender: TObject);
begin
    if (cb_marca.ItemIndex <> -1) and (cb_marca.ItemIndex <> 0) then begin
        CargarModelosVehiculos(DMConnections.BaseCAC, cb_modelo,
                                iif(cb_marca.ItemIndex = 0, null,
                                IntToStr(cb_marca.value)));
    end
    else begin
        cb_modelo.Clear;
    end;
end;

procedure TFormVehiculos.HabilitarAnioHasta(habil: boolean);
begin
    lblAnioHasta.Visible:= habil;
    AnioHasta.Visible   := habil;
end;

procedure TFormVehiculos.anioExit(Sender: TObject);
begin
    if (trim(AnioHasta.text) = '') or (AnioHasta.ValueInt < anio.ValueInt) then begin
        AnioHasta.ValueInt := anio.ValueInt;
    end;
end;

procedure TFormVehiculos.AnioHastaExit(Sender: TObject);
begin
    if (trim(AnioHasta.text) = '') or (AnioHasta.ValueInt < anio.ValueInt) then begin
        MsgBox(MSG_ANIO_ERROR, MSG_ANIO_CAPTION, MB_ICONSTOP);
    end;
end;

procedure TFormVehiculos.largoExit(Sender: TObject);
begin
    if trim(largo.Text) = '' then largo.ValueInt := 0;
end;

procedure TFormVehiculos.anchoExit(Sender: TObject);
begin
    if trim(ancho.Text) = '' then ancho.ValueInt := 0;
end;

procedure TFormVehiculos.altoExit(Sender: TObject);
begin
    if trim(alto.Text) = '' then alto.ValueInt := 0;
end;

end.
