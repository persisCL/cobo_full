unit ABMGruposTareas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB,
  DPSControls;

type
  TFormGruposTareas = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    GruposTareas: TADOTable;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    txt_Descripcion: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
	txt_codigo: TNumericEdit;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormGruposTareas  : TFormGruposTareas;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Grupo de Tareas?';
    MSG_DELETE_ERROR		= 'No se puede eliminar este Grupo de Tareas porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Grupo de Tareas';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Grupo de Tareas.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Grupo de Tareas';

{$R *.DFM}

function TFormGruposTareas.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([GruposTareas]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;
end;

procedure TFormGruposTareas.Limpiar_Campos();
begin
	txt_Codigo.Clear;
	txt_Descripcion.Clear;
end;

function TFormGruposTareas.ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
begin
	Result := True;
	Texto := Tabla.FieldByName('CodigoGrupo').AsString + ' ' +
	  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormGruposTareas.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFormGruposTareas.ListaInsert(Sender: TObject);
begin
	Lista.Estado       := Alta;
	Limpiar_Campos;
	GroupB.Enabled     := True;
	Lista.Enabled      := False;
	Notebook.PageIndex := 1;
	txt_Codigo.SetFocus;
end;

procedure TFormGruposTareas.ListaEdit(Sender: TObject);
begin
	Lista.Estado       := Modi;
	GroupB.Enabled     := True;
	Lista.Enabled      := False;
	Notebook.PageIndex := 1;
	txt_Codigo.SetFocus;
end;

procedure TFormGruposTareas.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO or MB_ICONWARNING) = IDYES then begin
		try
			GruposTareas.Delete;
		except
			On E: EDataBaseError do begin
				GruposTareas.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormGruposTareas.ListaClick(Sender: TObject);
begin
	 with GruposTareas do begin
		  txt_Codigo.Value     := FieldByName('CodigoGrupo').AsInteger;
		  txt_Descripcion.text := Trim(FieldByName('Descripcion').AsString);
	 end;
end;

procedure TFormGruposTareas.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos;
end;

procedure TFormGruposTareas.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormGruposTareas.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('CodigoGrupo').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormGruposTareas.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormGruposTareas.BtnAceptarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	With GruposTareas do begin
		Try
			if Lista.Estado = Alta then Append else Edit;
			FieldByName('CodigoGrupo').AsFloat  := txt_Codigo.Value;
			FieldByName('Descripcion').AsString := Trim(txt_Descripcion.Text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormGruposTareas.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormGruposTareas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormGruposTareas.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
