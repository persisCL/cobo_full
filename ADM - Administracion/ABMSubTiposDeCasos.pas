{-----------------------------------------------------------------------------
 Unit Name: ABMSubTiposDeCasos
 Author:  CFU
 Description: ABM de SubTipos de Casos
-----------------------------------------------------------------------------}
unit ABMSubTiposDeCasos;

interface

uses
  //ABM SubTipos de Casos
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants, VariantComboBox;

type
  TFABMSubTiposDeCasos = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    txtDescripcion: TEdit;
    dsSubTiposDeCasos: TDataSource;
    spActualizarSubTiposDeCasos: TADOStoredProc;
    spEliminarSubTiposDeCasos: TADOStoredProc;
    Ltipo: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    spObtenerTiposDeCasos: TADOStoredProc;
    spObtenerSubTiposDeCasos: TADOStoredProc;
    ListaSubTipos: TDBListEx;
    cbTipo: TComboBox;
    PanelOpciones: TPanel;
    btnTEditar: TSpeedButton;
    btnTInsertar: TSpeedButton;
    btnTEliminar: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    procedure FormShow(Sender: TObject);
   	function  ListaSubTiposProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaSubTiposDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaSubTiposInsert(Sender: TObject);
	procedure ListaSubTiposEdit(Sender: TObject);
	procedure ListaSubTiposDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure spObtenerSubTiposDeCasosAfterScroll(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnTInsertarClick(Sender: TObject);
    procedure btnTEliminarClick(Sender: TObject);
    procedure btnTEditarClick(Sender: TObject);
  private
	{ Private declarations }
	ListaTiposDeCasos: TStringList;
	procedure LimpiarCampos;
	procedure Volver_Campos;
    procedure ListaSubTiposReload;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMSubTiposDeCasos: TFABMSubTiposDeCasos;

resourcestring
	STR_MAESTRO_SubTipos	= 'Maestro de SubTipos de Casos';


implementation

{$R *.DFM}

function TFABMSubTiposDeCasos.Inicializa:Boolean;
var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    try
    	spObtenerSubTiposDeCasos.Open;
    except
    	on E:Exception do begin
        	MessageDlg('Error al obtener los datos' + #13 + E.Message, mtError, [mbOK], 0);
			Exit;
        end;
    end;
	Notebook.PageIndex := 0;
	Result := True;
end;

procedure TFABMSubTiposDeCasos.FormCreate(Sender: TObject);
begin
	ListaTiposDeCasos := TStringList.Create;
	cbTipo.Items.Clear;
	with spObtenerTiposDeCasos do begin
        if Active then
        	Close;
        Open;
        while not Eof do begin
	        cbTipo.Items.Add(FieldByName('Descripcion').AsString);
            ListaTiposDeCasos.Add(FieldByName('CodigoTipoDeCaso').AsString);
        	Next;
        end;
        Close;
    end;
end;

procedure TFABMSubTiposDeCasos.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
    cbTipo.ItemIndex := -1;
end;

procedure TFABMSubTiposDeCasos.FormShow(Sender: TObject);
begin
	ListaSubTiposReload;
end;

procedure TFABMSubTiposDeCasos.SpeedButton1Click(Sender: TObject);
begin
	Close;
end;

procedure TFABMSubTiposDeCasos.spObtenerSubTiposDeCasosAfterScroll(
  DataSet: TDataSet);
begin
	txtCodigo.Value			:= spObtenerSubTiposDeCasos.FieldByName('CodigoSubTipoDeCaso').Value;
    txtDescripcion.Text		:= spObtenerSubTiposDeCasos.FieldByName('Descripcion').Value;
    cbTipo.ItemIndex		:= cbTipo.Items.IndexOf(spObtenerSubTiposDeCasos.FieldByName('DSCTipoCaso').Value);
end;

procedure TFABMSubTiposDeCasos.Volver_Campos;
begin
	ListaSubTipos.Enabled:= True;

	ActiveControl       := ListaSubTipos;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;

	ListaSubTiposReload;
end;

procedure TFABMSubTiposDeCasos.ListaSubTiposReload;
begin
	if spObtenerSubTiposDeCasos.Active then
    	spObtenerSubTiposDeCasos.Close;
    
    try
    	spObtenerSubTiposDeCasos.Open;
    except
    	on E:Exception do begin
        	MessageDlg('Error al obtener los datos' + #13 + E.Message, mtError, [mbOK], 0);
			Exit;
        end;
    end;
end;

procedure TFABMSubTiposDeCasos.ListaSubTiposDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoSubTipoDeCaso').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
        TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('DSCTipoCaso').AsString);
	end;
end;

function TFABMSubTiposDeCasos.ListaSubTiposProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

procedure TFABMSubTiposDeCasos.ListaSubTiposInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaSubTipos.Enabled 		:= False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDescripcion;
end;

procedure TFABMSubTiposDeCasos.ListaSubTiposEdit(Sender: TObject);
begin
	ListaSubTipos.Enabled:= False;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion
end;

procedure TFABMSubTiposDeCasos.ListaSubTiposDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_SubTipos]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with spEliminarSubTiposDeCasos do begin
                Parameters.Refresh;
                //Parameters.ParamByName('@CodigoEntidad').Value := tblSubTiposDeCasos.FieldbyName('CodigoEntidad').Value;
                ExecProc;
                close;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_SubTipos]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_SubTipos]), MB_ICONSTOP);
			end
		end
	end;

	ListaSubTipos.Enabled	:= True;
	Notebook.PageIndex 		:= 0;
	Screen.Cursor      		:= crDefault;
end;

procedure TFABMSubTiposDeCasos.BtnAceptarClick(Sender: TObject);

begin
	if not ValidateControls([txtDescripcion],
	  [(Trim(txtDescripcion.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_SubTipos]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
		Exit;
	end;

    if cbTipo.ItemIndex < 0 then begin
        msgbox('Tipo de Caso No valido!');
        Exit;
    end;

	with ListaSubTipos do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarSubTiposDeCasos, Parameters do begin
                    Parameters.Refresh;
					ParamByName('@CodigoSubTipoDeCaso').Value	:= txtCodigo.Value;
					ParamByName('@Descripcion').Value 			:= Trim(txtDescripcion.Text);
                    ParamByName('@CodigoTipoDeCaso').Value 		:= ListaTiposDeCasos.Strings[cbTipo.ItemIndex];
                    ParamByName('@Usuario').Value				:= UsuarioSistema;
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_SubTipos]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_SubTipos]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
		end;
	end;
end;

procedure TFABMSubTiposDeCasos.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMSubTiposDeCasos.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMSubTiposDeCasos.btnTEditarClick(Sender: TObject);
begin
	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion;
end;

procedure TFABMSubTiposDeCasos.btnTEliminarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_SubTipos]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with spEliminarSubTiposDeCasos do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoSubTipoDeCaso').Value := spObtenerSubTiposDeCasos.FieldbyName('CodigoTipoDeCaso').Value;
                ExecProc;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_SubTipos]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_SubTipos]), MB_ICONSTOP);
			end
		end
	end;

    Volver_Campos;
	ListaSubTipos.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFABMSubTiposDeCasos.btnTInsertarClick(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaSubTipos.Enabled		:= False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDescripcion;
end;

procedure TFABMSubTiposDeCasos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	ListaTiposDeCasos.Free;
    action := caFree;
end;

end.

