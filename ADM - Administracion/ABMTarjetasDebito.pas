{********************************** File Header *********************************
File Name   : ABMTarjetasDebito
Author      : Gonzalez, Damian <dgonzalez@dpsautomation.com>
Date Created: 02-Mar-2004
Language    : ES-AR
Description : Esta Unit realiza la administraci�n de los tipos de tarjetas de
                debito. Por ejemplo, Visa Electron, etc.                      

Revision :
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura
********************************************************************************}
unit ABMTarjetasDebito;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  Util, UtilProc, OleCtrls, DmiCtrls, ComCtrls, PeaProcs, DMConnection,
  ADODB, variants, DPSControls, PeaTypes;

ResourceString
	MSG_CAPTION_VALIDAR_PREFIJO 		= 'Validar Prefijos de Tarjeta de d�bito';
  	MSG_ERROR_SELECCIONAR_ITEM			= 'Debe seleccionar un item de la lista.';
	MSG_NUEVO_PREFIJO_QUERY				= 'Nuevo Prefijo: ';
    MSG_NUEVO_PREFIJO_CAPTION			= 'Agregar Prefijo';

type
  TFormTarjetasDebito = class(TForm)
    AbmToolbar1: TAbmToolbar;
    alTarjetaDebito: TAbmList;
    gbTarjetaDebito: TPanel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Label2: TLabel;
    Notebook: TNotebook;
    ActualizarDatosTarjetaDebito: TADOStoredProc;
    TarjetasDebito: TADOTable;
    EliminarTarjetaDebito: TADOStoredProc;
    Label1: TLabel;
    txt_caracterinicialbanda: TNumericEdit;
    Label5: TLabel;
    txt_longitudbanda: TNumericEdit;
    Label6: TLabel;
    lb_prefijos: TListBox;
    chkActivo: TCheckBox;
    qry_prefijos: TADOQuery;
    QueryTemp: TADOQuery;
    btnAgregarPrefijo: TButton;
    btnQuitarPrefijo: TButton;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure alTarjetaDebitoDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure alTarjetaDebitoEdit(Sender: TObject);
    procedure alTarjetaDebitoRefresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure alTarjetaDebitoDelete(Sender: TObject);
    procedure alTarjetaDebitoInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure alTarjetaDebitoClick(Sender: TObject);
    function alTarjetaDebitoProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure btnAgregarPrefijoClick(Sender: TObject);
    procedure btnQuitarPrefijoClick(Sender: TObject);
  private
	{ Private declarations }
    CodigoTipoTarjetaDebito: integer;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
    procedure Volver_Campos;
    procedure RefrescarCamposRegistroActual;
  public
	{ Public declarations }
    function Inicializar(MDIChild: Boolean = True): Boolean;
  end;

var
  FormTarjetasDebito: TFormTarjetasDebito;

implementation

resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del tipo de Tarjeta de d�bito.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar tipo de Tarjeta de d�bito';

{$R *.DFM}

procedure TFormTarjetasDebito.HabilitarCamposEdicion(habilitar: Boolean);
begin
    alTarjetaDebito.Enabled    := not Habilitar;
	gbTarjetaDebito.Enabled    := Habilitar;
	Notebook.PageIndex 		:= ord(habilitar);
end;

procedure TFormTarjetasDebito.Volver_Campos ;
begin
	alTarjetaDebito.Estado     := Normal;
    lb_prefijos.Clear;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex      := 0;
end;

function TFormTarjetasDebito.Inicializar(MDIChild: Boolean = True): Boolean;
ResourceString
    LBL_FORM_CAPTION = 'Tipos de tarjetas de %s';
Var
	S: TSize;
    DescripcionTarjeta: AnsiString;
begin
    DescripcionTarjeta := '';
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;

	if not OpenTables([TarjetasDebito]) then exit;
    CodigoTipoTarjetaDebito := -1;
	Volver_Campos;
    alTarjetaDebito.Reload;
   	Notebook.PageIndex := 0;
	Result := True;
end;

procedure TFormTarjetasDebito.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
    alTarjetaDebito.Reload;
end;

procedure TFormTarjetasDebito.alTarjetaDebitoDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
        FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
        end;
        With Tabla do begin
            TextOut(Cols[0], Rect.Top, Fieldbyname('Descripcion').AsString);
            TextOut(Cols[1], Rect.Top, Fieldbyname('CaracterInicialBanda').AsString);
            TextOut(Cols[2], Rect.Top, Fieldbyname('LongitudBanda').AsString);
            TextOut(Cols[3], Rect.Top, iif(Fieldbyname('ValidarPrefijo').AsBoolean, MSG_SI, MSG_NO));
            TextOut(Cols[4], Rect.Top, iif(Fieldbyname('Activo').AsBoolean, MSG_SI, MSG_NO));
        end;
    end;
end;

procedure TFormTarjetasDebito.alTarjetaDebitoEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    alTarjetaDebito.Estado     := modi;
	txt_Descripcion.SetFocus;
end;

procedure TFormTarjetasDebito.alTarjetaDebitoRefresh(Sender: TObject);
begin
	if alTarjetaDebito.Empty then Limpiar_Campos;
end;

procedure TFormTarjetasDebito.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTarjetasDebito.alTarjetaDebitoDelete(Sender: TObject);
resourcestring
	MSG_ELIMINAR_TIPO_TD_CAPTION            = 'Eliminar Tipo de tarjeta de d�bito';
	MSG_ELIMINAR_TIPO_TD_ERROR              = 'No se puede eliminar este tipo de tarjeta de d�bito porque hay datos que dependen de �l.';
	MSG_ELIMINAR_TIPO_TD_QUESTION           = '�Est� seguro de querer eliminar este tipo de tarjeta de d�bito';
	MSG_ELIMINAR_TIPO_TD_QUESTION_CAPTION   = 'Confirmaci�n...';
begin
	Screen.Cursor := crHourGlass;
    If MsgBox(MSG_ELIMINAR_TIPO_TD_QUESTION, MSG_ELIMINAR_TIPO_TD_CAPTION, MB_YESNO) = IDYES then begin
        try
            EliminarTarjetaDebito.Parameters.ParamByName('@CodigoTipoTarjetaDebito').Value := TarjetasDebito.FieldByName('CodigoTipoTarjetaDebito').AsInteger;
            EliminarTarjetaDebito.ExecProc;
            EliminarTarjetaDebito.Close;
        Except
            On E: exception do begin
                MsgBoxErr(MSG_ELIMINAR_TIPO_TD_ERROR, E.message, MSG_ELIMINAR_TIPO_TD_CAPTION, MB_ICONSTOP);
                EliminarTarjetaDebito.Close;
            end;
        end;
    end;
	Volver_Campos;
	Screen.Cursor      := crDefault;
end;

procedure TFormTarjetasDebito.alTarjetaDebitoInsert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCamposEdicion(True);

    alTarjetaDebito.Estado := alta;
    chkActivo.Checked  := True;
	txt_Descripcion.SetFocus;
end;

procedure TFormTarjetasDebito.BtnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION		    = 'Actualizar tipo de tarjeta de d�bito';
    MSG_ACTUALIZAR_ERROR		    = 'No se pudieron actualizar los datos del tipo de tarjeta de d�bito';
    MSG_VALIDAR_CAPTION 		    = 'Validar datos del tipo de tarjeta de d�bito';
    MSG_VALIDAR_TARJETA             = 'Debe ingresar la tarjeta de d�bito';
	MSG_VALIDAR_DESCRIPCION 	    = 'Debe ingresar una Descripci�n';
    MSG_VALIDAR_CARACTER_INICIAL    = 'Debe ingresar el caracter inicial de la banda';
    MSG_VALIDAR_LONGITUD_BANDA      = 'Debe ingresar la longitud de la banda';
    MSG_ACTUALIZAR_CAPTION_PREFIJO	= 'Actualizar Prefijo de Tarjeta de d�bito';
var
    FTarjetaDebito: integer;
    TodoOk: Boolean;
    i: integer;
begin
	if (trim(txt_caracterinicialbanda.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_CARACTER_INICIAL, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_caracterinicialbanda);
    	Exit;
    end;

	if (trim(txt_longitudbanda.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_LONGITUD_BANDA, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_longitudbanda);
    	Exit;
    end;

   	if (Trim(txt_Descripcion.Text) = '') then begin
		MsgBoxBalloon( MSG_VALIDAR_DESCRIPCION, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txt_Descripcion);
		Exit;
    end;

	// HASTA ACA TODO BIEN
	Screen.Cursor := crHourGlass;
    TodoOk := False;
    try
    	DMConnections.BaseCAC.BeginTrans;
        Try
            if alTarjetaDebito.Estado = Alta then begin
                FTarjetaDebito := -1;
            end else begin
                FTarjetaDebito := TarjetasDebito.fieldByName('CodigoTipoTarjetaDebito').AsInteger;
            end;

            with ActualizarDatosTarjetaDebito.Parameters do begin
                ParamByName('@CodigoTipoTarjetaDebito').Value := iif(FTarjetaDebito = -1, null, FTarjetaDebito);
                ParamByName('@Descripcion').Value		    := iif(Trim(txt_Descripcion.Text)= '', null, Trim(txt_Descripcion.Text));
                ParamByName('@CaracterInicialBanda').Value  := iif(txt_CaracterInicialBanda.Valueint < 1, null, txt_CaracterInicialBanda.Valueint);
                ParamByName('@LongitudBanda').Value         := iif(txt_LongitudBanda.Valueint < 1, null, txt_LongitudBanda.Valueint);
                ParamByName('@ValidarPrefijo').Value        := (lb_prefijos.Items.Count > 0);
                ParamByName('@Activo').Value                := chkActivo.Checked;
            end;
            ActualizarDatosTarjetaDebito.ExecProc;
            FTarjetaDebito := ActualizarDatosTarjetaDebito.Parameters.ParamByName('@CodigoTipoTarjetaDebito').Value;
            ActualizarDatosTarjetaDebito.Close;

            if alTarjetaDebito.Estado = Modi then begin
                // Para grabar la lista de prefijos, primero borro los anteriores.
                // en el alta no los borro, obviamente!
                try
                   querytemp.close;
                   querytemp.SQL.clear;
                   querytemp.SQL.add('DELETE FROM TarjetasDebitoPrefijos WHERE CodigoTipoTarjetaDebito =  ' + IntToStr(FTarjetaDebito));
                   querytemp.execsql;
                except
                    On E: EDataBaseError do begin
                        raise Exception.Create(MSG_ACTUALIZAR_CAPTION_PREFIJO + #13#10 +
                                                E.message);
                    end;
                end;
            end;

            // Agregar los nuevos
            for i:= 0 to lb_prefijos.items.count -1 do begin
                // Escribir uno por uno.
                try
                    querytemp.close;
                    querytemp.SQL.clear;
                    querytemp.SQL.add('INSERT INTO TarjetasDebitoPrefijos (CodigoTipoTarjetaDebito, Prefijo)');
                    querytemp.SQL.add('VALUES ( ' + IntToStr(FTarjetaDebito));
                    querytemp.SQL.add(', ' + trim(lB_prefijos.items[i]) +  ')');
                    querytemp.execsql;
                except
                    On E: Exception do begin
                        raise Exception.Create(MSG_ACTUALIZAR_CAPTION_PREFIJO + #13#10 +
                                                E.message);
                    end;
                 end;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                ActualizarDatosTarjetaDebito.Close;
                Exit;
            end;
        end;
        TodoOk := True;
    finally
    	if TodoOk then begin
        	DMConnections.BaseCAC.CommitTrans;
            Volver_Campos;
            Screen.Cursor := crDefault;
        end else begin
	        DMConnections.BaseCAC.RollbackTrans;
            Screen.Cursor := crDefault;
		end;
        alTarjetaDebito.Reload;
        RefrescarCamposRegistroActual;
    end;
end;

procedure TFormTarjetasDebito.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTarjetasDebito.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormTarjetasDebito.Limpiar_Campos;
begin
	txt_Descripcion.clear;
    txt_caracterinicialbanda.Clear;
    txt_longitudbanda.Clear;
    lb_prefijos.Clear;
    chkActivo.Checked := true;
end;

procedure TFormTarjetasDebito.alTarjetaDebitoClick(Sender: TObject);
begin
    if TarjetasDebito.FieldByName('CodigoTipoTarjetaDebito').AsInteger = CodigoTipoTarjetaDebito then Exit;

    TarjetasDebito.DisableControls;
    RefrescarCamposRegistroActual;
end;

function TFormTarjetasDebito.alTarjetaDebitoProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    Result := True;
end;

procedure TFormTarjetasDebito.btnAgregarPrefijoClick(Sender: TObject);
Var
	Prefijo: AnsiString;
begin
	Prefijo := '';
	if InputQuery( MSG_NUEVO_PREFIJO_CAPTION, MSG_NUEVO_PREFIJO_QUERY, Prefijo) and
	  (Prefijo <> '') then begin
		lb_Prefijos.Items.Add(Prefijo);
	end;
end;

procedure TFormTarjetasDebito.btnQuitarPrefijoClick(Sender: TObject);
begin
	if lb_prefijos.itemindex < 0 then begin
		MsgBox( MSG_ERROR_SELECCIONAR_ITEM, MSG_CAPTION_VALIDAR_PREFIJO, MB_ICONSTOP);
	end else begin
		lb_prefijos.items.delete(lb_prefijos.itemindex);
	end;
end;

procedure TFormTarjetasDebito.RefrescarCamposRegistroActual;
begin
	With TarjetasDebito do begin
		txt_descripcion.text  	   := Trim(FieldByName('Descripcion').AsString);
        CodigoTipoTarjetaDebito := FieldByName('CodigoTipoTarjetaDebito').AsInteger;
        //Datos propios de la Tarjeta
		txt_caracterinicialbanda.value  := FieldByName('CaracterInicialBanda').AsInteger;
		txt_longitudbanda.value         := FieldByName('LongitudBanda').AsInteger;
		// Prefijos
		qry_prefijos.Parameters.ParamByName('CodigoTipoTarjetaDebito').Value :=  FieldByName('CodigoTipoTarjetaDebito').AsInteger;
		qry_prefijos.Open;
		lb_Prefijos.Clear;
		While not qry_prefijos.Eof do begin
			lb_prefijos.Items.Add(qry_prefijos.FieldByName('Prefijo').AsString);
			qry_prefijos.Next;
		end;
		qry_prefijos.Close;
        chkActivo.Checked := FieldByName('Activo').AsBoolean;
	end;
end;

end.
