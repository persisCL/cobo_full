object FormABMMensajes: TFormABMMensajes
  Left = 249
  Top = 161
  Width = 600
  Height = 456
  Caption = 'Mantenimiento de Mensajes'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object dbl_Mensaje: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 192
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'47'#0'Codigo  '
      #0'195'#0'T'#237'tulo                                                     '
      #0'61'#0'Comentario')
    HScrollBar = True
    RefreshTime = 100
    Table = qry_Mensajes
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dbl_MensajeClick
    OnDrawItem = dbl_MensajeDrawItem
    OnRefresh = dbl_MensajeRefresh
    OnEdit = dbl_MensajeEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 225
    Width = 592
    Height = 158
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    DesignSize = (
      592
      158)
    object Label1: TLabel
      Left = 13
      Top = 38
      Width = 37
      Height = 13
      Caption = '&Titulo:'
      FocusControl = txt_titulo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 13
      Top = 14
      Width = 48
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_CodigoMensaje
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 13
      Top = 62
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 13
      Top = 132
      Width = 56
      Height = 13
      Caption = 'C&omentario:'
      FocusControl = txt_comentario
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txt_titulo: TEdit
      Left = 96
      Top = 34
      Width = 239
      Height = 21
      Color = 16444382
      MaxLength = 50
      TabOrder = 1
    end
    object txt_CodigoMensaje: TEdit
      Left = 96
      Top = 10
      Width = 29
      Height = 21
      Color = 16444382
      Enabled = False
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
    end
    object txt_comentario: TEdit
      Left = 96
      Top = 126
      Width = 471
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      MaxLength = 100
      TabOrder = 3
    end
    object txt_descripcion: TMemo
      Left = 96
      Top = 59
      Width = 470
      Height = 61
      Anchors = [akLeft, akTop, akRight]
      Color = 16444382
      Lines.Strings = (
        'txt_descripcion')
      ScrollBars = ssVertical
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 383
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Panel1: TPanel
      Left = 270
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object qry_Mensajes: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Mensajes'
    Left = 418
    Top = 86
    object qry_MensajesCodigoMensaje: TWordField
      FieldName = 'CodigoMensaje'
    end
    object qry_MensajesTituloMensaje: TStringField
      FieldName = 'TituloMensaje'
      Size = 50
    end
    object qry_MensajesComentario: TStringField
      FieldName = 'Comentario'
      Size = 100
    end
    object qry_MensajesDescripcion: TMemoField
      FieldName = 'Descripcion'
      BlobType = ftMemo
    end
  end
end
