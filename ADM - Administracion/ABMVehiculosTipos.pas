{-----------------------------------------------------------------------------
 File Name: 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura


Firma       :   SS_1147_MBE_20140812
Description :   Se corrige el evento "DBList1.Click" ya que por cada registro iba a
                llenar las combox de nuevo
-----------------------------------------------------------------------------}
unit ABMVehiculosTipos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls, VariantComboBox;

type
  TFormVehiculosTipos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    VehiculosTipos: TADOTable;
    txt_CodigoTipoVehiculo: TNumericEdit;
    Notebook: TNotebook;
    cb_Categoria: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    cb_Categoria_Acoplado: TVariantComboBox;
    Label4: TLabel;
    cb_Categoria_Tag: TVariantComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormVehiculosTipos: TFormVehiculosTipos;

implementation

resourcestring
	MSG_DELETE_QUESTION		  = '�Est� seguro de querer eliminar este tipo de veh�culo?';
    MSG_DELETE_ERROR		  = 'No se puede eliminar el Tipo de Veh�culo';
    MSG_ERROR_RELATED_RECORDS = 'No se puede eliminar el Tipo de Veh�culo porque existen Veh�culos relacionados';
    MSG_DELETE_CAPTION		  = 'Eliminar Tipo de veh�culo';
    MSG_ACTUALIZAR_ERROR  	  = 'No se pudieron actualizar los datos del tipo de veh�culo';
    MSG_ACTUALIZAR_CAPTION	  = 'Actualizar Tipo de Veh�culo';
    MSG_DESCRIPCION           = 'Debe ingresar la descripci�n';
    MSG_CATEGORIA             = 'Debe seleccionar la categor�a';
    MSG_CATEGORIA_ACOPLADO    = 'Debe seleccionar la categor�a con Acoplado';
    MSG_CATEGORIA_TAG         = 'Debe seleccionar la categor�a de Tag';


{$R *.DFM}

{--------------------------------------------------------------                                                                 //SS_1147_MBE_20140812
        IndexComboPorCodigo                                                                                                     //SS_1147_MBE_20140812
                                                                                                                                //SS_1147_MBE_20140812
Author      : mbecerra                                                                                                          //SS_1147_MBE_20140812
Date        : 12-Agosto-2014                                                                                                    //SS_1147_MBE_20140812
Description : Devuelva el valor de la posici�n en que se encuentra el registro                                                  //SS_1147_MBE_20140812
                cuyo c�digo coincide con c�digo pasado por par�metro                                                            //SS_1147_MBE_20140812
                                                                                                                                //SS_1147_MBE_20140812
                cada registro de la comboBox se asume del tipo  Glosa Space(200) Codigo                                         //SS_1147_MBE_20140812
}                                                                                                                               //SS_1147_MBE_20140812
function IndexComboPorCodigo(cb : TComboBox; Codigo : string) : Integer;                                                        //SS_1147_MBE_20140812
var                                                                                                                             //SS_1147_MBE_20140812
  Index : Integer;                                                                                                              //SS_1147_MBE_20140812
begin                                                                                                                           //SS_1147_MBE_20140812
	Index := cb.Items.Count - 1;                                                                                                //SS_1147_MBE_20140812
	While (Index <> -1) And (Trim(Copy(Cb.Items[Index], 200, Length(cb.Items[index]))) <> Codigo) do  Dec (Index);              //SS_1147_MBE_20140812
                                                                                                                                //SS_1147_MBE_20140812
	if (Index < 0) then Index := -1;                                                                                            //SS_1147_MBE_20140812
                                                                                                                                //SS_1147_MBE_20140812
    Result := Index;                                                                                                            //SS_1147_MBE_20140812
                                                                                                                                //SS_1147_MBE_20140812
end;                                                                                                                            //SS_1147_MBE_20140812


procedure TFormVehiculosTipos.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txt_CodigoTipoVehiculo.Enabled	:= True;
end;

function TFormVehiculosTipos.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([VehiculosTipos]) then
		Result := False
	else begin
    	CargarCategoriasVehiculos(DMConnections.BaseCAC, cb_Categoria);
    	CargarCategoriasVehiculos(DMConnections.BaseCAC, cb_Categoria_Acoplado);
        CargarCategoriasVehiculos(DMConnections.BaseCAC, cb_Categoria_Tag);
		Result := True;
		DbList1.Reload;
	end;
   	Notebook.PageIndex := 0;
end;

procedure TFormVehiculosTipos.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormVehiculosTipos.DBList1Click(Sender: TObject);
begin
	with VehiculosTipos do begin
		txt_CodigoTipoVehiculo.Value	:= FieldByName('CodigoTipoVehiculo').AsInteger;
		txt_Descripcion.text	        := FieldByName('Descripcion').AsString;
{                                                                                                                               //SS_1147_MBE_20140812
        CargarCategoriasVehiculos(DMConnections.BaseCAC, cb_Categoria, FieldByName('Categoria').AsInteger);                     //SS_1147_MBE_20140812
        CargarCategoriasVehiculos(DMConnections.BaseCAC, cb_Categoria_Acoplado, FieldByName('CategoriaAcoplado').AsInteger);    //SS_1147_MBE_20140812
        CargarCategoriasVehiculos(DMConnections.BaseCAC, cb_Categoria_Tag, FieldByName('CategoriaTag').AsInteger);              //SS_1147_MBE_20140812
}                                                                                                                               //SS_1147_MBE_20140812
        cb_Categoria.ItemIndex          := IndexComboPorCodigo(cb_Categoria, FieldByName('Categoria').AsString);                //SS_1147_MBE_20140812
        cb_Categoria_Acoplado.ItemIndex := IndexComboPorCodigo(cb_Categoria, FieldByName('CategoriaAcoplado').AsString);        //SS_1147_MBE_20140812
        cb_Categoria_Tag.ItemIndex      := IndexComboPorCodigo(cb_Categoria, FieldByName('CategoriaTag').AsString);             //SS_1147_MBE_20140812
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBList1DrawItem
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormVehiculosTipos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var DescCategoria: AnsiString;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoTipoVehiculo').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
        DescCategoria := QueryGetValue(DMConnections.BaseCAC,
                            'SELECT Descripcion FROM Categorias  WITH (NOLOCK) ' +
                            'WHERE Categoria = ' + Tabla.FieldByName('Categoria').AsString);
        TextOut(Cols[2], Rect.Top, DescCategoria);
	end;
end;

procedure TFormVehiculosTipos.DBList1Edit(Sender: TObject);
begin
    dblist1.Estado     := modi;
    HabilitarCampos; 
end;

procedure TFormVehiculosTipos.DBList1Insert(Sender: TObject);
begin
    dblist1.Estado  := Alta;
	  Limpiar_Campos;
    HabilitarCampos;
end;

procedure TFormVehiculosTipos.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormVehiculosTipos.Limpiar_Campos;
begin
	txt_CodigoTipoVehiculo.Clear;
	txt_Descripcion.Clear;
    cb_Categoria.ItemIndex := 0;
end;

procedure TFormVehiculosTipos.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormVehiculosTipos.DBList1Delete(Sender: TObject);

    function ExistenVehiculosRelaccionados: Boolean;
    //Devuelve True si existen vehiculos relacionados con el tipo actual
    begin
        Result := StrtoBool(QueryGetValue(DMConnections.BaseCAC,
                                          'select dbo.TipoVehiculoConVehiculos(''' + txt_CodigoTipoVehiculo.Text + ''')'));
    end;

begin
    if (ExistenVehiculosRelaccionados) then begin
        MsgBox(MSG_ERROR_RELATED_RECORDS, MSG_DELETE_CAPTION, MB_ICONSTOP);
        VolverCampos;
        Exit;
    end;

	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			VehiculosTipos.Delete;
		Except
			On E: Exception do begin
				VehiculosTipos.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;
{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormVehiculosTipos.BtnAceptarClick(Sender: TObject);
begin
    if not ValidateControls([txt_Descripcion, cb_Categoria, cb_Categoria_Acoplado, cb_Categoria_Tag],
                [trim(txt_Descripcion.text) <> '',
                (cb_Categoria.ItemIndex >= 0),
                (cb_Categoria_Acoplado.ItemIndex >= 0),
                (cb_Categoria_Tag.ItemIndex >= 0)],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION, MSG_CATEGORIA, MSG_CATEGORIA_ACOPLADO, MSG_CATEGORIA_TAG]) then exit;

 	Screen.Cursor := crHourGlass;
	With VehiculosTipos do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                txt_CodigoTipoVehiculo.value := QueryGetValueInt(DMConnections.BaseCAC,
                	'Select ISNULL(MAX(CodigoTipoVehiculo), 0) + 1 FROM VehiculosTipos WITH (NOLOCK) ');
                FieldByName('CodigoTipoVehiculo').value		:= txt_CodigoTipoVehiculo.value;
			end else Edit;
			FieldByName('Descripcion').AsString	       := txt_Descripcion.text;
			FieldByName('Categoria').AsInteger	       := Ival(StrRight(cb_Categoria.Text, 10));
			FieldByName('CategoriaAcoplado').AsInteger := cb_Categoria_Acoplado.Value;
            FieldByName('CategoriaTag').AsInteger	   := cb_Categoria_Tag.Value;

			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormVehiculosTipos.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormVehiculosTipos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormVehiculosTipos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormVehiculosTipos.HabilitarCampos;
begin

	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_CodigoTipoVehiculo.Enabled	:= False;
    txt_Descripcion.SetFocus;

    if (dblist1.Estado = modi) then
      begin
           cb_Categoria.Enabled             := False;
           cb_Categoria_Acoplado.Enabled    := False;
           cb_Categoria_Tag.Enabled         := False;
      end
      else 
      begin
           cb_Categoria.Enabled             := True;
           cb_Categoria_Acoplado.Enabled    := True;
           cb_Categoria_Tag.Enabled         := True;
      end;
end;

end.
