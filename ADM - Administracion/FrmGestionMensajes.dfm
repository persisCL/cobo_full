object FormGestionMensajes: TFormGestionMensajes
  Left = 253
  Top = 206
  Width = 696
  Height = 480
  Caption = 'FormGestionMensajes'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 414
    Width = 688
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Notebook: TNotebook
      Left = 491
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
        end
      end
    end
  end
  object DBList: TDBListEx
    Left = 0
    Top = 97
    Width = 688
    Height = 317
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Mensaje'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescripComponente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Obligatorio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Obligatorio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Hint'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Hint'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Tiene Imagen'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'TieneImagen'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Titulo Mensaje'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'TituloMensaje'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Descripcion Mensaje'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Comentario Mensaje'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Comentario'
      end>
    DataSource = ds_Mensaje
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
  end
  object pnl_Top: TGroupBox
    Left = 0
    Top = 0
    Width = 688
    Height = 97
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 67
      Height = 13
      Caption = '&Tipo Mensaje:'
      FocusControl = cb_Mensaje
    end
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 40
      Height = 13
      Caption = '&Sistema:'
      FocusControl = cb_Sistema
    end
    object Label3: TLabel
      Left = 16
      Top = 72
      Width = 51
      Height = 13
      Caption = '&Formulario:'
      FocusControl = cb_Formaulario
    end
    object cb_Mensaje: TComboBox
      Left = 96
      Top = 16
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = 'cb_Mensaje'
    end
    object cb_Sistema: TComboBox
      Left = 96
      Top = 40
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Text = 'cb_Sistema'
    end
    object cb_Formaulario: TComboBox
      Left = 96
      Top = 64
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Text = 'cb_Formaulario'
    end
  end
  object ds_Mensaje: TDataSource
    DataSet = vw_mensajesayuda
    Left = 104
    Top = 136
  end
  object vw_mensajesayuda: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'vw_mensajesayuda'
    Left = 144
    Top = 136
  end
end
