{-----------------------------------------------------------------------------
 Unit Name: ABMBancos
 Author:
 Purpose:
 History:

 Revision 1:
  Author:    ggomez
  Date:      19-Abr-2005
  Description:
	- Agregu� componentes para dos campos nuevos en la tabla de Bancos.
	Los componentes nuevos son chb_HabilitadoPAC y chb_HabilitadoPagoVentanilla.
	- Agregu� c�digo para que los valores de la tabla se seteen con los valores
	de los componentes.
	- Elimin� el componente chb_HabilitadoPAC ya que se elimin� el campo
	asociado en la tabla Bancos.
	- Agregu� c�digo para verificar que el c�digo SBEI ingresado no exista.
-----------------------------------------------------------------------------
 Revision 2:
  Author:    rcastro
  Date:      26-Abr-2005
  Description: Indicador de habilitaci�n como Emisor de T.Cr�dito y PAC

	Revision 3:
	Date: 20/02/2009
	Author: mpiazza
	Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
		los bloqueos de tablas en la lectura


Firma       :   SS_1147_MBE_20140811
Description :   Se corrige unproblema con el mantenedor: arroja
                error si el c�digo SBEI est� vac�o.

Autor       :   CQuezadaI
Fecha       :   14 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se elimina "Hint" del objeto txt_Descripcion                
-----------------------------------------------------------------------------}

unit ABMBancos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls, CheckLst, VariantComboBox, ComCtrls, Variants, StrUtils;      //TASK_120_JMA_20170323

type
  TFormBancos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Panel2: TPanel;
    Bancos: TADOTable;
    Notebook: TNotebook;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    PcBancos: TPageControl;
    Label1: TLabel;
	TsDatosBanco: TTabSheet;
	TsCuentas: TTabSheet;
    txt_Descripcion: TEdit;
    cb_Activo: TCheckBox;
	txt_codigo: TNumericEdit;
    Label15: TLabel;
    Label2: TLabel;
    ActualizarCuentasBancariasxBancos: TADOStoredProc;
    Cb_Cuentas: TVariantCheckListBox;
    Label3: TLabel;
    Panel1: TPanel;
    Label4: TLabel;
    neCodSBEI: TNumericEdit;
    chb_HabilitadoPagoVentanilla: TCheckBox;
    chb_HabilitadoEmisorTC: TCheckBox;
    chb_HabilitadoEmisorPAC: TCheckBox;
    spActualizarEmisoresTarjetaCredito: TADOStoredProc;
    BtnSalir: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
    function AgregarAuditoria(CodigoBanco: Integer;	Descripcion: string; Activo: boolean; CodigoBancoSBEI: SmallInt;            //TASK_120_JMA_20170323
    HabilitadoPagoVentanilla, HabilitadoEmisorTC, HabilitadoEmisorPAC: Boolean; PrefijoIDTRX, Accion: string): Boolean;         //TASK_120_JMA_20170323
  public
    { Public declarations }
    function Inicializar: boolean;

  end;

var
  FormBancos: TFormBancos;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Banco?';
	MSG_DELETE_ERROR		= 'No se puede eliminar el Banco porque hay datos que dependen del mismo.';
    MSG_DELETE_CAPTION		= 'Eliminar Banco';
	MSG_ACTUALIZAR_ERROR  	= 'No se pudieron actualizar los datos del Banco.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Banco';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';
    MSG_CATEGORIA           = 'Debe seleccionar la categor�a';
    MSG_CODIGOSBEI          = 'Debe ingresar el c�digo SBEI';
    MSG_CODIGOSBEI_EXISTE   = 'El c�digo SBEI ingresado ya existe.';


{$R *.DFM}

procedure TFormBancos.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    TsDatosBanco.Enabled        := False;
    TsCuentas.Enabled           := False;
    txt_codigo.Enabled	:= True;
	PcBancos.ActivePageIndex := 0;
end;

function TFormBancos.Inicializar: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    PcBancos.ActivePageIndex := 0;
	Notebook.PageIndex 			:= 0;
	if not OpenTables([Bancos]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormBancos.BtnCancelarClick(Sender: TObject);
begin
	VolverCampos;
end;

procedure TFormBancos.DBList1Click(Sender: TObject);
begin
	with Bancos do begin
		txt_codigo.Value	 := FieldByName('CodigoBanco').AsInteger;
		txt_Descripcion.text := FieldByName('Descripcion').AsString;
        neCodSBEI.Value      := FieldByName('CodigoBancoSBEI').AsInteger;
		CargarCuentasBancarias(DMConnections.BaseCAC, cb_Cuentas, FieldByName('CodigoBanco').AsInteger);
        cb_Activo.Checked := FieldByName('Activo').AsBoolean;
		chb_HabilitadoPagoVentanilla.Checked := FieldByName('HabilitadoPagoVentanilla').AsBoolean;
		chb_HabilitadoEmisorTC.Checked := FieldByName('HabilitadoEmisorTC').AsBoolean;
		chb_HabilitadoEmisorPAC.Checked := FieldByName('HabilitadoEmisorPAC').AsBoolean;
	end;
end;

procedure TFormBancos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas do begin
    	FillRect(Rect);
	    if Tabla.FieldByName('Activo').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
        end;

      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoBanco').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
	end;
end;

procedure TFormBancos.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormBancos.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
    CargarCuentasBancarias(DMConnections.BaseCAC, cb_Cuentas);
end;

procedure TFormBancos.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormBancos.Limpiar_Campos;
begin
	txt_codigo.Clear;
	txt_Descripcion.Clear;
	neCodSBEI.Clear;
end;

procedure TFormBancos.AbmToolbar1Close(Sender: TObject);
begin
	close;
end;

procedure TFormBancos.DBList1Delete(Sender: TObject);
resourcestring
	MSG_BORRAR_EMISOR_TC_ERROR = 'No puede borrarse el Banco ya que est� siendo referenciado como emisor de Tarjetas de Cr�dito';
begin
	Screen.Cursor := crHourGlass;

	try
		If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
			try
				//try                                                                                                                      //TASK_120_JMA_20170323
                    {INICIO: TASK_120_JMA_20170323}
                DMConnections.BaseCAC.BeginTrans;

                AgregarAuditoria(Bancos.FieldByName('CodigoBanco').value,
                                Bancos.FieldByName('Descripcion').value,
                                Bancos.FieldByName('Activo').value,
                                Bancos.FieldByName('CodigoBancoSBEI').value,
                                Bancos.FieldByName('HabilitadoPagoVentanilla').value,
                                Bancos.FieldByName('HabilitadoEmisorTC').value,
                                Bancos.FieldByName('HabilitadoEmisorPAC').value,
                                IIf(Bancos.FieldByName('PrefijoIDTRX').value = NULL, '', Bancos.FieldByName('PrefijoIDTRX').value),
                                'D');
                {TERMINO: TASK_120_JMA_20170323}

                with spActualizarEmisoresTarjetaCredito, Parameters do begin
                    Refresh;                                                                                                               //TASK_120_JMA_20170323
                    ParamByName('@CodigoBanco').Value := Bancos.FieldByName('CodigoBanco').value;
                    ParamByName('@Descripcion').Value := '';
                    ParamByName('@HabilitarComoEmisor').Value := false;

                    ExecProc
                end;


				{INICIO: TASK_120_JMA_20170323
                    except
					On E: Exception do begin
						Screen.Cursor := crDefault;
						MsgBoxErr(MSG_BORRAR_EMISOR_TC_ERROR, E.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
						Exit
					end
				end;
                TERMINO: TASK_120_JMA_20170323}

				QueryExecute(DMConnections.BaseCAC, 'DELETE FROM TiposCuentasBancariasxBancos WHERE CodigoBanco = ' + inttostr(Bancos.FieldByName('CodigoBanco').value));

				Bancos.Delete;

                DMConnections.BaseCAC.CommitTrans;         //TASK_120_JMA_20170323
			Except
				On E: Exception do begin
					Screen.Cursor := crDefault;
					Bancos.Cancel;
{INICIO: TASK_120_JMA_20170323
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
}
                    DMConnections.BaseCAC.RollbackTrans;
                    if AnsiContainsStr(e.Message, 'REFERENCE') then
                    begin
                         if MsgBox('El banco no se puede eliminar porque ya ha sido utilizado en el sistema' + Chr(13) +
                                '�Desea desactivarlo?','Atenci�n', MB_YESNO) = mrYes then
                         begin
                             dblist1.Estado := modi;
                             cb_Activo.Checked := False;
                             BtnAceptarClick(nil);
                         end;
                    end
                    else
    					MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
{TERMINO: TASK_120_JMA_20170323}
				end;
			end;
			DbList1.Reload;
		end;
	finally
		VolverCampos;
		Screen.Cursor := crDefault
	end
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 20/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormBancos.BtnAceptarClick(Sender: TObject);
resourcestring
	MSG_ACTUALIZAR_EMISOR_TC_ERROR = 'Actualizaci�n Emisores de Tarjetas de Cr�dito';
var
	i: integer;
	TodoOk: Boolean;
begin

{                                                                                       //SS_1147_MBE_20140811
	if not ValidateControls([txt_Descripcion,                                           //SS_1147_MBE_20140811
			neCodSBEI,                                                                  //SS_1147_MBE_20140811
			neCodSBEI],                                                                 //SS_1147_MBE_20140811
            [trim(txt_Descripcion.text) <> EmptyStr,                                    //SS_1147_MBE_20140811
            neCodSBEI.Text <> EmptyStr,                                                 //SS_1147_MBE_20140811
                                                                                        //SS_1147_MBE_20140811
            ( (DBList1.Estado = Modi)                                                   //SS_1147_MBE_20140811
                (* Si el c�digo de banco retornado seg�n el c�digo SBEI es el           //SS_1147_MBE_20140811
                mismo que el del banco que se est� modificando. *)                      //SS_1147_MBE_20140811
                and ((Trim(QueryGetValue(DMConnections.BaseCAC,                         //SS_1147_MBE_20140811
                    Format('SELECT dbo.ObtenerCodigoBanco(%s)',                         //SS_1147_MBE_20140811
                    [Trim(neCodSBEI.Text)]))) = Trim(txt_Codigo.Text))) )               //SS_1147_MBE_20140811
                                                                                        //SS_1147_MBE_20140811
                (* Verificar que el c�digo SBEI NO exista. *)                           //SS_1147_MBE_20140811
                or (QueryGetValue(DMConnections.BaseCAC,                                //SS_1147_MBE_20140811
                        Format('SELECT dbo.ExisteBancoConCodigoSBE(%s)',                //SS_1147_MBE_20140811
                        [Trim(neCodSBEI.Text)])) = 'False')],                           //SS_1147_MBE_20140811
            MSG_ACTUALIZAR_CAPTION,                                                     //SS_1147_MBE_20140811
            [MSG_DESCRIPCION,                                                           //SS_1147_MBE_20140811
            MSG_CODIGOSBEI,                                                             //SS_1147_MBE_20140811
            MSG_CODIGOSBEI_EXISTE]) then Exit;                                          //SS_1147_MBE_20140811
}

    if not ValidateControls (                                                           //SS_1147_MBE_20140811
        [txt_Descripcion, neCodSBEI],                                                   //SS_1147_MBE_20140811
        [Trim(txt_Descripcion.Text) <> EmptyStr, neCodSBEI.Text <> EmptyStr],           //SS_1147_MBE_20140811
        MSG_ACTUALIZAR_CAPTION,                                                         //SS_1147_MBE_20140811
        [MSG_DESCRIPCION, MSG_CODIGOSBEI] ) then Exit;                                  //SS_1147_MBE_20140811

{INICIO: TASK_120_JMA_20170323 - LA VALIDACI�N SE HACE POR BASE DE DATOS CAMPO UNICO
                                                                                        //SS_1147_MBE_20140811
    if (DBList1.Estado = Modi) and (not                                                    //SS_1147_MBE_20140811
            ValidateControls(                                                           //SS_1147_MBE_20140811
                [neCodSBEI],                                                            //SS_1147_MBE_20140811
                (* Si el c�digo de banco retornado seg�n el c�digo SBEI es el           //SS_1147_MBE_20140811
                mismo que el del banco que se est� modificando. *)                      //SS_1147_MBE_20140811
                [(Trim(QueryGetValue(DMConnections.BaseCAC,                            //SS_1147_MBE_20140811
                    Format('SELECT dbo.ObtenerCodigoBanco(%s)',                         //SS_1147_MBE_20140811
                    [Trim(neCodSBEI.Text)]))) = Trim(txt_Codigo.Text))                 //SS_1147_MBE_20140811
                                                                                        //SS_1147_MBE_20140811
                (* Verificar que el c�digo SBEI NO exista. *)                           //SS_1147_MBE_20140811
                or (QueryGetValue(DMConnections.BaseCAC,                                //SS_1147_MBE_20140811
                        Format('SELECT dbo.ExisteBancoConCodigoSBE(%s)',                //SS_1147_MBE_20140811
                        [Trim(neCodSBEI.Text)])) = 'False')],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_CODIGOSBEI_EXISTE] )
        ) then Exit;
TERMINO: TASK_120_JMA_20170323}

 	Screen.Cursor := crHourGlass;
    TodoOk := False;
	try
		DMConnections.BaseCAC.BeginTrans;

		With Bancos do begin
			Try
				if DbList1.Estado = Alta then begin
					Append;
					txt_codigo.value := QueryGetValueInt(DMConnections.BaseCAC,
						'Select ISNULL(MAX(CodigoBanco), 0) + 1 FROM Bancos WITH (NOLOCK) ');
					FieldByName('CodigoBanco').value		:= txt_codigo.value;
				end else Edit;

				FieldByName('Descripcion').AsString	 := txt_Descripcion.text;
				FieldByName('CodigoBancoSBEI').value := neCodSBEI.Value;
				FieldByName('Activo').AsBoolean	:= cb_Activo.Checked;
				FieldByName('HabilitadoPagoVentanilla').AsBoolean := chb_HabilitadoPagoVentanilla.Checked;
				FieldByName('HabilitadoEmisorTC').AsBoolean := chb_HabilitadoEmisorTC.Checked;
				FieldByName('HabilitadoEmisorPAC').AsBoolean := chb_HabilitadoEmisorPAC.Checked;
				Post;

				with ActualizarCuentasBancariasxBancos do begin
					for i:=0 to  Cb_Cuentas.Items.Count-1 do begin
						  Close;
                          Parameters.Refresh;           //TASK_120_JMA_20170323
						  Parameters.ParamByName('@CodigoTipoCuentaBancaria').Value := Cb_Cuentas.Items[i].Value;
						  Parameters.ParamByName('@CodigoBanco').Value := Bancos.FieldByName('CodigoBanco').value;
						  Parameters.ParamByName('@Activo').Value := Cb_Cuentas.Checked[i];
						  Parameters.ParamByName('@Formato').Value := null;
						  ExecProc;
					end;
				end;

				try
					with spActualizarEmisoresTarjetaCredito, Parameters do begin
						Refresh;
						ParamByName('@CodigoBanco').Value := txt_codigo.value;
						ParamByName('@Descripcion').Value := txt_Descripcion.text;
						ParamByName('@HabilitarComoEmisor').Value := chb_HabilitadoEmisorTC.Checked;

						ExecProc
					end;
				except
					On E: Exception do begin
						if Pos ('DELETE', E.Message) = 0
							then MsgBoxErr(MSG_ACTUALIZAR_EMISOR_TC_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
					end
				end;

{INICIO: TASK_120_JMA_20170323}
                AgregarAuditoria(StrToInt(txt_codigo.text), txt_Descripcion.text, cb_Activo.Checked, StrToInt(neCodSBEI.Text),
                                    chb_HabilitadoPagoVentanilla.Checked, chb_HabilitadoEmisorTC.Checked, chb_HabilitadoEmisorPAC.Checked,
                                    '', IIf(DbList1.Estado = Alta, 'I', 'U'));
{TERMINO: TASK_120_JMA_20170323}
				TodoOk := True;
			except
				On E: Exception do begin
					Screen.Cursor := crDefault;
					Cancel;
{INICIO: TASK_120_JMA_20170323
                    MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
}
                    if AnsiContainsStr(e.Message,'IX_Bancos') then
                        MsgBoxBalloon('El SBIF ingresardo ya existe', 'Error', MB_OK, neCodSBEI)
                    else
    					MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
{TERMINO: TASK_120_JMA_20170323}
					Exit;
				end;
			end;
		end;
	finally
		if TodoOk then DMConnections.BaseCAC.CommitTrans
		else DMConnections.BaseCAC.RollbackTrans;

		Screen.Cursor := crDefault;
	end;

	VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormBancos.FormShow(Sender: TObject);
begin
	DBList1.Reload;
end;

procedure TFormBancos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormBancos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormBancos.HabilitarCampos;
begin
    PcBancos.ActivePageIndex    := 0;
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    TsDatosBanco.Enabled        := True;
    TsCuentas.Enabled           := True;
    txt_codigo.Enabled	:= False;
    txt_Descripcion.SetFocus;
end;

{INICIO: TASK_120_JMA_20170323}
function TFormBancos.AgregarAuditoria(CodigoBanco: Integer;	Descripcion: string; Activo: boolean; CodigoBancoSBEI: SmallInt;
    HabilitadoPagoVentanilla, HabilitadoEmisorTC, HabilitadoEmisorPAC: Boolean; PrefijoIDTRX, Accion: string): Boolean;
var
    spAuditoria : TADOStoredProc;
begin
    Result := False;
    try
        spAuditoria := TADOStoredProc.Create(nil);
        spAuditoria.Connection := DMConnections.BaseCAC;
        spAuditoria.ProcedureName := 'Bancos_Auditoria_INSERT';
        spAuditoria.Parameters.Refresh;
        spAuditoria.Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;
        spAuditoria.Parameters.ParamByName('@Descripcion').Value := Descripcion;
        spAuditoria.Parameters.ParamByName('@Activo').Value := Activo;
        spAuditoria.Parameters.ParamByName('@CodigoBancoSBEI').Value := CodigoBancoSBEI;
        spAuditoria.Parameters.ParamByName('@HabilitadoPagoVentanilla').Value := HabilitadoPagoVentanilla;
        spAuditoria.Parameters.ParamByName('@HabilitadoEmisorTC').Value := HabilitadoEmisorTC;
        spAuditoria.Parameters.ParamByName('@HabilitadoEmisorPAC').Value := HabilitadoEmisorPAC;
        spAuditoria.Parameters.ParamByName('@PrefijoIDTRX').Value := PrefijoIDTRX;
        spAuditoria.Parameters.ParamByName('@Accion').Value := Accion;
        spAuditoria.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spAuditoria.ExecProc;

        if spAuditoria.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
           raise Exception.Create(spAuditoria.Parameters.ParamByName('@ErrorDescription').Value);

        Result := True;

    finally
        FreeAndNil(spAuditoria);
    end;
end;
{TERMINO: TASK_120_JMA_20170323}

end.
