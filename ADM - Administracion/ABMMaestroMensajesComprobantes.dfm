object FormABMMaestroMensajesComprobantes: TFormABMMaestroMensajesComprobantes
  Left = 115
  Top = 93
  Caption = 'Mantenimiento de Mensajes Comprobantes'
  ClientHeight = 464
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 799
    Height = 316
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'98'#0'Nro. de Mensaje    '
      #0'35'#0'Texto')
    HScrollBar = True
    RefreshTime = 100
    Table = ObtenerMaestroMensajesComprobantes
    Style = lbOwnerDrawFixed
    ItemHeight = 20
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 349
    Width = 799
    Height = 76
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 1
    object Label1: TLabel
      Left = 122
      Top = 7
      Width = 37
      Height = 13
      Caption = '&Texto:'
      FocusControl = txt_Texto
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 7
      Width = 94
      Height = 13
      Caption = '&Nro. de Mensaje'
      FocusControl = txt_Texto
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Texto: TEdit
      Left = 122
      Top = 24
      Width = 671
      Height = 21
      Hint = 'Texto'
      Color = clBtnHighlight
      MaxLength = 255
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object CB_IncluirEnGeneral: TCheckBox
      Left = 80
      Top = 48
      Width = 129
      Height = 17
      Caption = 'Incluir en General'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
    object neIDMensaje: TNumericEdit
      Left = 8
      Top = 24
      Width = 105
      Height = 21
      Enabled = False
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 425
    Width = 799
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 602
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 176
    Top = 2
    Width = 104
    Height = 27
    BevelOuter = bvNone
    TabOrder = 3
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 799
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object Panel3: TPanel
    Left = 179
    Top = 2
    Width = 49
    Height = 28
    BevelOuter = bvNone
    TabOrder = 5
  end
  object InsertarMensajesComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarMaestroMensajesComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDMensaje'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Texto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@IncluirEnGeneral'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 208
    Top = 88
  end
  object BorrarMaestroMensjesComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BorrarMaestroMensjesComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IdMensaje'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 304
    Top = 200
  end
  object ActualizarMaestroMensajesComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMaestroMensajesComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@ID_New'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Texto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@IncluirEnGeneral'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 504
    Top = 184
  end
  object ObtenerMaestroMensajesComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMaestroMensajesComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 392
    Top = 112
  end
end
