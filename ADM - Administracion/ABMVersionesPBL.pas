unit ABMVersionesPBL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util, DBClient, DBCtrls, Mask, Provider, StrUtils, DSIntf, PatronBusqueda, XMLIntf, XMLDoc;


resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
    HINT_SALIR       = 'Salir del ABM';
    HINT_AGREGAR     = 'Agregar Nuevo C�digo PBL';
    HINT_ELIMINAR    = 'Eliminar C�digo PBL';
    HINT_EDITAR      = 'Editar C�digo PBL';
    ANSW_ELIMINAR    = 'Est� seguro de eliminar este Registro de C�digo PBL?';
    ANSW_SALIR       = 'Confirma que desea salir?';

    PARAMS_IGNORE    = '@CodigoVersionPBLNew, @UsuarioCreacion, @FechaHoraCreacion, @UsuarioModificacion, @FechaHoraModificacion,';
    FIELDS_TINYINT   = '';
    FIELDS_SMALLNT   = 'CodigoVersionPBL,';

    PROCEDURE_SELECT = 'ADM_VersionesPBL_SELECT';
    PROCEDURE_INSERT = 'ADM_VersionesPBL_INSERT';
    PROCEDURE_UPDATE = 'ADM_VersionesPBL_UPDATE';
    PROCEDURE_DELETE = 'ADM_VersionesPBL_DELETE';

    CAPTION_FORM     = 'ABM de Versiones PBL';





type

  TClientDataSetAccess = Class(TClientDataSet);

  TfrmABMVersionesPBL = class(TForm)
    procSelect: TADOStoredProc;
    dsGrid: TDataSource;
    Grid: TDBGrid;
    pnlSuperior: TPanel;
    pnlControles: TPanel;
    pnlInferior: TPanel;
    Botonera: TToolBar;
    btnSalir: TToolButton;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    btnGuardar: TButton;
    btnCancelar: TButton;
    procInsert: TADOStoredProc;
    procDelete: TADOStoredProc;
    procUpdate: TADOStoredProc;
    CDS: TClientDataSet;
    pg01: TPageControl;
    tabGeneral: TTabSheet;
    lbl01: TLabel;
    edCodIncid: TDBEdit;
    edDescripcion: TDBEdit;
    lbl03: TLabel;
    dsPBL: TDataSource;
    procSelectCodigoVersionPBL: TSmallintField;
    procSelectDescripcion: TStringField;
    procSelectUsuarioCreacion: TStringField;
    procSelectFechaHoraCreacion: TDateTimeField;
    procSelectUsuarioModificacion: TStringField;
    procSelectFechaHoraModificacion: TDateTimeField;
    CDSCodigoVersionPBL: TSmallintField;
    CDSDescripcion: TStringField;
    CDSUsuarioCreacion: TStringField;
    CDSFechaHoraCreacion: TDateTimeField;
    CDSUsuarioModificacion: TStringField;
    CDSFechaHoraModificacion: TDateTimeField;
    procedure FormCreate(Sender: TObject);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure PonerFocoEnPrimerControl;
    procedure ValidarTextoDeControl(Sender: TField; const Text: string);
    procedure HabilitarDeshabilitarControles(Estado : Boolean);
    procedure btnBuscarClick(Sender: TObject);
    function GetCamposPK : string;
  private
    { Private declarations }
    CantidadRegistros           : integer;
    Posicion                    : TBookmark;
    Accion                      : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno                     : Integer;
    MensajeError                : string;
    OldCodigoVersionPBL         : Integer;
    FIELDS_PK                   : string;
  public
    { Public declarations }
  published

  end;

var
  frmABMVersionesPBL: TfrmABMVersionesPBL;

implementation

uses DMConnection;

{$R *.dfm}


function TfrmABMVersionesPBL.GetCamposPK : string;
var
  qry           : TADOQuery;
  XML, Salida   : string;
  DocXML        : IXMLDocument;
  Nodo          : IXMLNode;
begin

  qry := TADOQuery.Create(nil);
  qry.Connection := DMConnections.BaseBO_Master;
  qry.SQL.Text :=   'SELECT                                                                                                          ' +
                    '   CAST(                                                                                                        ' +
                    '       (SELECT COLUMN_NAME AS Nombre                                                                            ' +
                    '           FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A                                                          ' +
                    '               LEFT JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ON A.CONSTRAINT_NAME = B.CONSTRAINT_NAME  ' +
                    '           where A.TABLE_SCHEMA + ''.'' + A.TABLE_NAME = ''dbo.VersionesPBL'' and CONSTRAINT_TYPE = ''PRIMARY KEY'' ' +
                    '       FOR XML PATH(''Campo''), ROOT(''Campos''), TYPE)                                                         ' +
                    '       AS VARCHAR(8000)) AS Campos                                                                              ' ;
  qry.Open;
  XML := qry.FieldByName('Campos').AsString;
  qry.Close;
  FreeAndNil(qry);

  DocXML          := TXMLDocument.Create(nil);
  DocXML.XML.Text := XML;
  DocXML.Active   := True;

  Nodo := DocXML.DocumentElement.ChildNodes.FindNode('Campo');

  Salida := EmptyStr;

  repeat
    if Nodo <> nil then begin
      Salida := Salida  + Nodo.ChildNodes.Nodes[0].NodeValue + ', ';
      Nodo:= Nodo.NextSibling;
    end;
  until Nodo = nil;

  DocXML.Active   := False;
  FreeAndNil(Nodo);
//  FreeAndNil(DocXml);

  Result          := Salida;

end;


Procedure MakeReadWrite (Const Field :TField);
  Begin
    Field.ReadOnly := False;
    With TClientDataSetAccess (Field.DataSet As TClientDataSet) Do
      Check (DSCursor.SetProp (CURProp (4) { curpropFld_MakeRW },
             Field.FieldNo));
  End;

procedure TfrmABMVersionesPBL.ValidarTextoDeControl(Sender: TField; const Text: string);
var
  NombreCampo           : string;
  Valida                : Boolean;
  ValorCampo            : LongInt;
begin

  Valida := True;

  if TField(Sender).ClassName = 'TDateTimeField'  then begin
      try
        StrToDateTime(Text);
      except
          raise Exception.Create('Error en valor del campo, reintente');
      end;
  end;

  if TField(Sender).ClassName = 'TSmallintField'  then begin
    try
      ValorCampo := StrToInt(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if ContainsText(FIELDS_TINYINT, NombreCampo) then
      if not ((ValorCampo >= 0) and (ValorCampo <= 255)) then
          Valida := False;

    if ContainsText(FIELDS_SMALLNT, NombreCampo) then
       if not ((ValorCampo >= -32768) and (ValorCampo <= 32767)) then
          Valida := False;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;

  if TField(Sender).ClassName = 'TIntegerField'  then begin
    try
      ValorCampo := StrToInt(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if not ((ValorCampo >= -32768) and (ValorCampo <= 32767)) then
       Valida := False;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;

  if TField(Sender).ClassName = 'TBCDField'  then begin
    try
      StrToFloat(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;


  TField(Sender).Value := Text;

end;

function TfrmABMVersionesPBL.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    Position := poMainFormCenter;
	Result := True;
end;


function TfrmABMVersionesPBL.TraeRegistros;
var
  Resultado : Boolean;
  i         : Integer;
begin

  try

    procSelect.Parameters.Refresh;
    procSelect.Open;
    Retorno := procSelect.Parameters.ParamByName('@RETURN_VALUE').Value;

    if Retorno <> 0 then begin
      MensajeError := procSelect.Parameters.ParamByName('@ErrorDescription').Value;
      Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
    end;

    CantidadRegistros := procSelect.RecordCount;

    CDS.Active   := False;

    for i := 0 to CDS.Fields.Count - 1 do
      CDS.Fields[i].ReadOnly := False;

    try
      CDS.EmptyDataSet;
    except
    end;
    CDS.CreateDataSet;
    CDS.Active   := True;
    CDS.ReadOnly := False;

    procSelect.First;
    while not procSelect.eof do begin
      CDS.Append;
      for i := 0 to procSelect.Fields.Count - 1 do
        CDS.Fields[i].Value := procSelect.Fields[i].Value;
      CDS.Post;
      procSelect.Next;
    end;

    CDS.ReadOnly := True;

    procSelect.Close;

    try
      CDS.GotoBookmark(Posicion);
    except
      CDS.First;
    end;

    Resultado := True;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      Resultado := False;
    end;
  end;

  HabilitarDeshabilitarControles(False);

  Result := Resultado;
end;

procedure TfrmABMVersionesPBL.btnAgregarClick(Sender: TObject);
begin
  HabilitaBotones('000001110');
  try
    Posicion := CDS.GetBookmark;
  except
  end;

  CDS.ReadOnly := False;
  CDS.Filtered := False;
  CDS.Append;
  CDS.Post;
  CDS.Edit;

  pg01.ActivePage := tabGeneral;

  HabilitarDeshabilitarControles(True);
  PonerFocoEnPrimerControl;

  Accion := 1;
end;

procedure TfrmABMVersionesPBL.btnBuscarClick(Sender: TObject);
begin
  TfrmPatronBusqueda.Create(self).ShowModal;

end;

procedure TfrmABMVersionesPBL.btnCancelarClick(Sender: TObject);
begin
  CDS.Cancel;
  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');

end;

procedure TfrmABMVersionesPBL.PonerFocoEnPrimerControl;
var
  i, ControlIndex, TabOrderMenor : Integer;
begin

  ControlIndex  := 99;
  TabOrderMenor := 99;

  for i := 0 to pg01.ActivePage.ControlCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox',TWinControl(pg01.ActivePage.Controls[i]).ClassType.ClassName) then
      if TWinControl(pg01.ActivePage.Controls[i]).TabOrder < TabOrderMenor then begin
        TabOrderMenor := TWinControl(pg01.ActivePage.Controls[i]).TabOrder;
        ControlIndex  := i;
      end;

  if ControlIndex < 99 then
    TWinControl(pg01.ActivePage.Controls[ControlIndex]).SetFocus;

end;

procedure TfrmABMVersionesPBL.HabilitarDeshabilitarControles(Estado : Boolean);
var
  i : Integer;
begin

   for i := 0 to ComponentCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox', Components[i].ClassName) then begin

      if Components[i].ClassName = 'TDBEdit' then
        TDBEdit(Components[i]).ReadOnly := not Estado;

      if Components[i].ClassName = 'TDBLookupComboBox' then
        TDBLookupComboBox(Components[i]).ReadOnly := not Estado;

      if Components[i].ClassName = 'TDBCheckBox' then
        TDBCheckBox(Components[i]).ReadOnly := not Estado;

    end;

end;


procedure TfrmABMVersionesPBL.btnEditarClick(Sender: TObject);
var
  i : integer;
begin

  HabilitaBotones('000001110');

  Posicion                      := CDS.GetBookmark;
  OldCodigoVersionPBL           := CDS.FieldByName('CodigoVersionPBL').Value;
  CDS.ReadOnly                  := False;

  CDS.Edit;

  HabilitarDeshabilitarControles(True);

   for i := 0 to CDS.Fields.Count - 1 do
    if ContainsStr(FIELDS_PK, CDS.Fields[i].FieldName + ',') then
      CDS.Fields[i].ReadOnly := True;

  for i := 0 to ComponentCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox', Components[i].ClassName) then begin

      if Components[i].ClassName = 'TDBEdit' then
        if ContainsText(FIELDS_PK, TDBEdit(Components[i]).DataField) then
          TDBEdit(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBLookupComboBox' then
        if ContainsText(FIELDS_PK, TDBLookupComboBox(Components[i]).DataField) then
          TDBLookupComboBox(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBCheckBox' then
        if ContainsText(FIELDS_PK, TDBCheckBox(Components[i]).DataField) then
          TDBCheckBox(Components[i]).ReadOnly := True;
    end;

  PonerFocoEnPrimerControl;

  Accion := 3;

end;

procedure TfrmABMVersionesPBL.btnEliminarClick(Sender: TObject);

begin

  HabilitaBotones('000000000');

  try
    Posicion := CDS.GetBookmark;
  except
  end;

  if Application.MessageBox(PChar(ANSW_ELIMINAR),'Pregunta',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    with procDelete do begin
      Parameters.Refresh;
      Parameters.ParamByName('@CodigoVersionPBL').Value  := CDS.FieldByName('CodigoVersionPBL').Value;

      try
        ExecProc;
        Retorno := procDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
          MensajeError := procDelete.Parameters.ParamByName('@ErrorDescription').Value;
          MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
        end;
      except
        on E : Exception do begin
          MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
        end;
      end;
  end;

  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');

end;

procedure TfrmABMVersionesPBL.btnGuardarClick(Sender: TObject);
resourcestring                                                                  // TASK_150_MGO_20170313
    MSG_ERROR_DESCRIPCION = 'Debe ingresar una Descripci�n';                    // TASK_150_MGO_20170313
var
  i : integer;
begin

  try
    for i := 0 to CDS.Fields.Count - 1 do
      MakeReadWrite (CDS.FieldByName (CDS.Fields[i].FieldName));

    CDS.Post;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
      Exit;
    end;
  end;

  // INICIO : TASK_150_MGO_20170313
  if Trim(edDescripcion.Text) = EmptyStr then begin
      MsgBoxBalloon(MSG_ERROR_DESCRIPCION, 'Error', MB_ICONSTOP, edDescripcion);
      Exit;
  end;
  // FIN : TASK_150_MGO_20170313

  HabilitaBotones('000000000');

  if Accion = 1 then begin
    with procInsert do begin
        Parameters.Refresh;
        for i:=0 to  CDS.Fields.Count - 1 do
           if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName + ',') then
             Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;
        Parameters.ParamByName('@UsuarioCreacion').Value        := UsuarioSistema;

        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
             MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
             MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
           end;
          except
            on E : Exception do begin
             MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
            end;
        end;

    end;
  end;

  if Accion = 3 then begin

    with procUpdate do begin
      Parameters.Refresh;
      for i:=0 to  CDS.Fields.Count - 1 do
        if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName + ',') then
          Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;

      Parameters.ParamByName('@UsuarioModificacion').Value       := UsuarioSistema;
      Parameters.ParamByName('@CodigoVersionPBLNew').Value       := CDS.FieldByName('CodigoVersionPBL').Value;
      Parameters.ParamByName('@CodigoVersionPBL').Value          := OldCodigoVersionPBL;

      try
        ExecProc;
        Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
          MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
          MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
        end;
      except
        on E : Exception do begin
          MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
        end;
      end;

    end;
  end;

  TraeRegistros;
  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');
end;

procedure TfrmABMVersionesPBL.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMVersionesPBL.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Application.MessageBox(PChar(ANSW_SALIR),'Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    Action := caFree
  else
    Action := caNone;
end;

procedure TfrmABMVersionesPBL.FormCreate(Sender: TObject);
var
  i         : Integer;
  SP_Aux    : TADOStoredProc;
begin

{
  SP_Aux := TADOStoredProc.Create(nil);
  with SP_Aux do begin
    Connection    := DMConnections.BaseBO_Master;
    ProcedureName := 'KTCCore_ComparaContenidoTablas';
    Parameters.Refresh;
    Parameters.ParamByName('@BDD1').Value       := 'BO_OBO';
    Parameters.ParamByName('@BDD2').Value       := 'BO_Master';
    Parameters.ParamByName('@TABLA1').Value     := 'VersionesPBL';
    Parameters.ParamByName('@TABLA2').Value     := 'VersionesPBL';
    Parameters.ParamByName('@RESULTADO').Value  := NULL;

    try
       ExecProc;
       Retorno := Parameters.ParamByName('@Resultado').Value;
       Free;
       if Retorno <> 0 then
          raise Exception.Create('Esta tabla no est� sincronizada, Comun�quse con el administrador del sistema.');

    except
       on E : Exception do begin
         MsgBoxErr('Error en los datos de esta tabla.', E.Message, Caption, MB_ICONERROR);
       end;
     end;

  end;

 }


  Caption                       := CAPTION_FORM;
  btnSalir.Hint                 := HINT_SALIR;
  btnAgregar.Hint               := HINT_AGREGAR;
  btnEliminar.Hint              := HINT_ELIMINAR;
  btnEditar.Hint                := HINT_EDITAR;

  procSelect.Close;
  procInsert.Close;
  procUpdate.Close;
  procDelete.Close;

  procSelect.ProcedureName      := PROCEDURE_SELECT;
  procInsert.ProcedureName      := PROCEDURE_INSERT;
  procUpdate.ProcedureName      := PROCEDURE_UPDATE;
  procDelete.ProcedureName      := PROCEDURE_DELETE;

  procSelect.Connection         := DMConnections.BaseBO_Master;
  procInsert.Connection         := DMConnections.BaseBO_Master;
  procUpdate.Connection         := DMConnections.BaseBO_Master;
  procDelete.Connection         := DMConnections.BaseBO_Master;

  pg01.ActivePage := tabGeneral;

  FIELDS_PK := GetCamposPK;

  TraeRegistros;

  CDS.First;

  for i := 0 to CDS.Fields.Count - 1 do
    CDS.Fields[i].OnSetText := ValidarTextoDeControl;


  if CantidadRegistros > 0 then
    HabilitaBotones('111101001')
  else
    HabilitaBotones('110001001');
end;

procedure TfrmABMVersionesPBL.HabilitaBotones(Botones : string);
begin
  btnSalir.Enabled      := Botones[1] = '1';
  btnAgregar.Enabled    := Botones[2] = '1';
  btnEliminar.Enabled   := Botones[3] = '1';
  btnEditar.Enabled     := Botones[4] = '1';
  btnImprimir.Enabled   := Botones[5] = '1';
  //btnBuscar.Enabled     := Botones[6] = '1';                                  // TASK_150_MGO_20170313
  btnGuardar.Enabled    := Botones[7] = '1';
  btnCancelar.Enabled   := Botones[8] = '1';
  Grid.Enabled          := Botones[9] = '1';
end;

end.



