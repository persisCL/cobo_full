unit ABMTipoConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util;


resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';

type
  TfrmABMTipoConvenio = class(TForm)
    procSelect: TADOStoredProc;
    dsGrid: TDataSource;
    Grid: TDBGrid;
    pnlSuperior: TPanel;
    pnlControles: TPanel;
    pnlInferior: TPanel;
    Botonera: TToolBar;
    Imagenes: TImageList;
    btnSalir: TToolButton;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    procSelectCodigoTipoConvenio: TIntegerField;
    procSelectDescripcion: TStringField;
    procSelectUsuarioCreacion: TStringField;
    procSelectFechaCreacion: TDateTimeField;
    procSelectUsuarioModificacion: TStringField;
    procSelectFechaModificacion: TDateTimeField;
    procSelectHabilitado: TBooleanField;
    procSelectHabilitado2: TStringField;
    edDescripcion: TEdit;
    chkHabilitado: TCheckBox;
    lbl01: TLabel;
    btnGuardar: TButton;
    btnCancelar: TButton;
    procInsert: TADOStoredProc;
    procDelete: TADOStoredProc;
    procUpdate: TADOStoredProc;
    Label1: TLabel;
    edCodigoConvenio: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure procSelectCalcFields(DataSet: TDataSet);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure procSelectAfterScroll(DataSet: TDataSet);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
  private
    { Private declarations }
    CantidadRegistros : integer;
    Posicion          : TBookmark;
    Accion            : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno           : Integer;
    MensajeError      : string;

  public
    { Public declarations }
  end;

var
  frmABMTipoConvenio: TfrmABMTipoConvenio;

implementation

{$R *.dfm}

function TfrmABMTipoConvenio.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    Position := poMainFormCenter;
	Result := True;
end;


function TfrmABMTipoConvenio.TraeRegistros;
begin
  try
    procSelect.Close;
    procSelect.Open;
    Retorno := procSelect.Parameters.ParamByName('@RETURN_VALUE').Value;
    if Retorno <> 0 then begin
      MensajeError := procSelect.Parameters.ParamByName('@ErrorDescription').Value;
      Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
    end;
    CantidadRegistros := procSelect.RecordCount;
    try
      procSelect.GotoBookmark(Posicion);
    except
    end;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
    end;
  end;
end;

procedure TfrmABMTipoConvenio.btnAgregarClick(Sender: TObject);
begin
  HabilitaBotones('000000110');
  chkHabilitado.Checked := True;
  with edCodigoConvenio do begin
      Enabled := True;
      Text    := '';
      SetFocus;
  end;
  with edDescripcion do begin
      Enabled := True;
      Text    := '';
  end;
  Accion := 1;
end;

procedure TfrmABMTipoConvenio.btnCancelarClick(Sender: TObject);
begin
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
  try
    procSelect.GotoBookmark(Posicion);
  except
  end;
  with chkHabilitado do begin
    Enabled := False;
    Checked := procSelect.FieldByName('Habilitado').AsBoolean
  end;

  with edDescripcion do begin
    Enabled := False;
    Text    := procSelect.FieldByName('Descripcion').AsString; 
  end;

  with edCodigoConvenio do begin
    Enabled := False;
    Text    := procSelect.FieldByName('CodigoTipoConvenio').AsString;
  end;

end;

procedure TfrmABMTipoConvenio.btnEditarClick(Sender: TObject);
begin
  HabilitaBotones('000000110');
  chkHabilitado.Enabled := True;
  with edDescripcion do begin
      Enabled := True;
  end;
  with edCodigoConvenio do begin
      Enabled := True;
      SetFocus;
  end;
  Accion := 3;
end;

procedure TfrmABMTipoConvenio.btnEliminarClick(Sender: TObject);
begin
  HabilitaBotones('000000000');
  Accion := 2;
  try
    Posicion := procSelect.GetBookmark;
  except
  end;
  if Application.MessageBox(PChar('Est� seguro de eliminar este Tipo de Convenio? - ' + procSelect.FieldByName('Descripcion').AsString),'Pregunta',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    with procDelete do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoTipoConvenio').Value  := procSelect.FieldByName('CodigoTipoConvenio').AsInteger;
        Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
        try
          ExecProc;
          Retorno := procDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := procDelete.Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
    TraeRegistros;
    if CantidadRegistros > 0 then
      HabilitaBotones('111100001')
    else
      HabilitaBotones('110000001');
end;

procedure TfrmABMTipoConvenio.btnGuardarClick(Sender: TObject);
begin
  if Length(edDescripcion.Text) = 0 then begin
    Application.MessageBox('Ingrese la Descripci�n del Tipo de Convenio','Problema', MB_ICONEXCLAMATION);
    Exit;
  end;

  try
    StrToInt(edCodigoConvenio.Text);
  except
    Application.MessageBox('Ingrese el C�digo del Tipo de Convenio','Problema', MB_ICONEXCLAMATION);
    Exit;
  end;


  try
    Posicion := procSelect.GetBookmark;
  except
  end;
  if Accion = 1 then begin
    with procInsert do begin
        Parameters.Refresh;
        Parameters.ParamByName('@Descripcion').Value        := edDescripcion.Text;
        Parameters.ParamByName('@CodigoTipoConvenio').Value := StrToInt(edCodigoConvenio.Text);
        Parameters.ParamByName('@UsuarioCreacion').Value    := UsuarioSistema;
        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
  end;

  if Accion = 3 then begin
    with procUpdate do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoTipoConvenio').Value     := procSelect.FieldByName('CodigoTipoConvenio').Value;
        Parameters.ParamByName('@CodigoTipoConvenioNew').Value  := StrToInt(edCodigoConvenio.Text);
        Parameters.ParamByName('@Descripcion').Value            := edDescripcion.Text;
        Parameters.ParamByName('@Habilitado').Value             := chkHabilitado.Checked;
        Parameters.ParamByName('@UsuarioModificacion').Value    := UsuarioSistema;
        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
  end;

  edDescripcion.Enabled := False;
  edCodigoConvenio.Enabled := False;
  chkHabilitado.Enabled := False;
  TraeRegistros;
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMTipoConvenio.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMTipoConvenio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Application.MessageBox('Confirma que desea salir?','Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    Action := caFree
  else
    Action := caNone;
end;

procedure TfrmABMTipoConvenio.FormCreate(Sender: TObject);
begin
  TraeRegistros;
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMTipoConvenio.HabilitaBotones(Botones : string);
begin
  btnSalir.Enabled      := Botones[1] = '1';
  btnAgregar.Enabled    := Botones[2] = '1';
  btnEliminar.Enabled   := Botones[3] = '1';
  btnEditar.Enabled     := Botones[4] = '1';
  btnImprimir.Enabled   := Botones[5] = '1';
  btnBuscar.Enabled     := Botones[6] = '1';
  btnGuardar.Enabled    := Botones[7] = '1';
  btnCancelar.Enabled   := Botones[8] = '1';
  Grid.Enabled          := Botones[9] = '1';
end;

procedure TfrmABMTipoConvenio.procSelectAfterScroll(DataSet: TDataSet);
begin
  edDescripcion.Text    := DataSet.FieldByName('Descripcion').AsString;
  edCodigoConvenio.Text := DataSet.FieldByName('CodigoTipoConvenio').AsString;
  chkHabilitado.Checked := DataSet.FieldByName('Habilitado').AsBoolean;

end;

procedure TfrmABMTipoConvenio.procSelectCalcFields(DataSet: TDataSet);
begin
  if DataSet.FieldByName('Habilitado').AsBoolean then  
    DataSet.FieldByName('Habilitado2').AsString := 'S�'
  else
    DataSet.FieldByName('Habilitado2').AsString := 'No'
end;

end.



