{********************************** File Header *********************************
File Name   : FrmTarjetasDeCredito
Author      : Tinao, Diego <dtinao@dpsautomation.com>
Date Created: 04-Sep-2003
Language    : ES-AR
Description : Esta Unit realiza la administraci�n de las Tarjetas de cr�dito.


Firma       : SS_1419_NDR_20151130
Descripcion : El SP ActualizarDatosPersona tiene el nuevo parametro @CodigoUsuario
              para grabarlo en los campos de auditoria de la tabla Personas y en
              HistoricoPersonas
********************************************************************************}
unit FrmTarjetasDeCredito;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, Abm_obj, OleCtrls, DmiCtrls, Grids,DmConnection, ADODB,
  Variants, ComCtrls, PeaProcs, Validate, BuscaTab, MaskCombo,
  DPSControls, Peatypes, TimeEdit, Provider, DBClient, ListBoxEx, DBListEx,
  FrmEditarDomicilio, FrmEditarMedioComunicacion, DBGrids, BuscaClientes,
  ABMTarjetasCreditoTipos;


ResourceString
	MSG_CAPTION_VALIDAR_PREFIJO 		= 'Validar Prefijos de Tarjeta de Cr�dito';
  	MSG_ERROR_SELECCIONAR_ITEM			= 'Debe seleccionar un item de la lista.';
	MSG_NUEVO_PREFIJO_QUERY				= 'Nuevo Prefijo: ';
    MSG_NUEVO_PREFIJO_CAPTION			= 'Agregar Prefijo';

type
  TFormTarjetasDeCredito = class(TForm)
    pnlInferior: TPanel;
	AbmToolbar1: TAbmToolbar;
    Notebook: TNotebook;
    alTarjetasCredito: TAbmList;
    TarjetasCredito: TADOTable;
    pcDatosPersonal: TPageControl;
    tsCliente: TTabSheet;
    tsDomicilio: TTabSheet;
    tsContacto: TTabSheet;
    pnlDatosPersonal: TPanel;
    lblApellido: TLabel;
    lblFechaNacCreacion: TLabel;
    dtFechaNacimiento: TDateEdit;
    txtApellido: TEdit;
    lblNombre: TLabel;
    txtNombre: TEdit;
    mcTipoNumeroDocumento: TMaskCombo;
    Label7: TLabel;
    lblSexo: TLabel;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    cbPersoneria: TComboBox;
    Label23: TLabel;
    txtApellidoMaterno: TEdit;
    lblApellidoMaterno: TLabel;
    cbSexo: TComboBox;
    cbSituacionIVA: TComboBox;
    ObtenerMediosComunicacionPersona: TADOStoredProc;
    ActualizarDatosTarjetaCredito: TADOStoredProc;
    Label2: TLabel;
    cbLugarNacimiento: TComboBox;
    Label10: TLabel;
    cbActividades: TComboBox;
    Label11: TLabel;
    pnlDomicilio: TPanel;
    dblDomicilios: TDBListEx;
    btnAgregarDomicilio: TDPSButton;
    btnEditarDomicilio: TDPSButton;
    btnEliminarDomicilio: TDPSButton;
    QueryTemp: TADOQuery;
    dsMediosComunicacion: TDataSource;
    cdsMediosComunicacion: TClientDataSet;
    dspMediosComunicacion: TDataSetProvider;
    pnlMediosContacto: TPanel;
    dblMediosComunicacion: TDBListEx;
    btnAgregarMedioComunicacion: TDPSButton;
    btnEditarMedioComunicacion: TDPSButton;
    btnEliminarMedioComunicacion: TDPSButton;
    cdsMediosComunicacionCodigoTipoOrigenDato: TSmallintField;
    cdsMediosComunicacionCodigoMedioContacto: TSmallintField;
    cdsMediosComunicacionDescriMedioContacto: TStringField;
    cdsMediosComunicacionTipoMedio: TStringField;
    cdsMediosComunicacionDescriTipoOrigenDato: TStringField;
    cdsMediosComunicacionValor: TStringField;
    cdsMediosComunicacionCodigoDomicilio: TIntegerField;
    cdsMediosComunicacionDescriDomicilio: TStringField;
    cdsMediosComunicacionDetalle: TStringField;
    cdsMediosComunicacionHorarioDesde: TDateTimeField;
    cdsMediosComunicacionHorarioHasta: TDateTimeField;
    cdsMediosComunicacionObservaciones: TStringField;
    cdsMediosComunicacionCodigoDomicilioRecNo: TIntegerField;
    EliminarTarjetaCredito: TADOStoredProc;
    qryObtenerDLLPago: TADOQuery;
    ActualizarDLLOperacion: TADOStoredProc;
    Panel3: TPanel;
    Label8: TLabel;
    tsDLLsPago: TTabSheet;
    pnlDLLsPago: TPanel;
    Label3: TLabel;
    cbOperadoresLogisticos: TComboBox;
    sgDllOperacion: TStringGrid;
    ActualizarDatosPersona: TADOStoredProc;
    ActualizarMedioComunicacionPersona: TADOStoredProc;
    ObtenerDatosPersona: TADOStoredProc;
    EliminarDomiciliosPersona: TADOStoredProc;
    EliminarTodosDomiciliosPersona: TADOStoredProc;
    ActualizarDomicilio: TADOStoredProc;
    ActualizarDomicilioEntregaPersona: TADOStoredProc;
    ActualizarDomicilioRelacionado: TADOStoredProc;
    ObtenerDomiciliosPersona: TADOStoredProc;
    dsDomicilios: TDataSource;
    cdsDomicilios: TClientDataSet;
    cdsDomiciliosCodigoDomicilio: TIntegerField;
    cdsDomiciliosCodigoPersona: TIntegerField;
    cdsDomiciliosCodigoTipoOrigenDato: TSmallintField;
    cdsDomiciliosDescriTipoOrigenDato: TStringField;
    cdsDomiciliosCodigoCalle: TIntegerField;
    cdsDomiciliosNumero: TIntegerField;
    cdsDomiciliosPiso: TSmallintField;
    cdsDomiciliosDpto: TStringField;
    cdsDomiciliosCalleDesnormalizada: TStringField;
    cdsDomiciliosDetalle: TStringField;
    cdsDomiciliosCodigoPostal: TStringField;
    cdsDomiciliosCodigoTipoEdificacion: TSmallintField;
    cdsDomiciliosDescriTipoEdificacion: TStringField;
    cdsDomiciliosDomicilioEntrega: TBooleanField;
    cdsDomiciliosCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNormalizado: TIntegerField;
    cdsDomiciliosCodigoTipoCalle: TSmallintField;
    cdsDomiciliosDescriCalle: TStringField;
    cdsDomiciliosCodigoPais: TStringField;
    cdsDomiciliosDescriPais: TStringField;
    cdsDomiciliosCodigoRegion: TStringField;
    cdsDomiciliosDescriRegion: TStringField;
    cdsDomiciliosCodigoComuna: TStringField;
    cdsDomiciliosDescriComuna: TStringField;
    cdsDomiciliosCodigoCiudad: TStringField;
    cdsDomiciliosDescriCiudad: TStringField;
    cdsDomiciliosDireccionCompleta: TStringField;
    cdsDomiciliosDomicilioCompleto: TStringField;
    dspDomicilios: TDataSetProvider;
    btnBuscar: TBitBtn;
    Panel1: TPanel;
    btnTiposTarjetaCredito: TSpeedButton;
    chkActivo: TCheckBox;
	procedure AbmToolbar1Close(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnCancelarClick(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
    procedure alTarjetasCreditoClick(Sender: TObject);
    procedure alTarjetasCreditoDelete(Sender: TObject);
    procedure alTarjetasCreditoDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure alTarjetasCreditoEdit(Sender: TObject);
    procedure alTarjetasCreditoInsert(Sender: TObject);
    procedure alTarjetasCreditoRefresh(Sender: TObject);
    procedure cbPersoneriaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Agregar(Sender: TObject);
    procedure btnEliminarDomicilioClick(Sender: TObject);
    procedure btnEditarDomicilioClick(Sender: TObject);
    procedure dblDomiciliosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblMediosComunicacionDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnEliminarMedioComunicacionClick(Sender: TObject);
    procedure btnAgregarMedioComunicacionClick(Sender: TObject);
    procedure btnEditarMedioComunicacionClick(Sender: TObject);
    procedure cbOperadoresLogisticosChange(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnTiposTarjetaCreditoClick(Sender: TObject);

  private
	{ Private declarations }
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
	procedure Volver_Campos;
    procedure HabilitarCamposPersoneria(Personeria: Char);
    procedure HabilitarBotonesDomicilios;
    procedure HabilitarBotonesMediosComunicacion;
    procedure CargarMediosComunicacionPersona(CodigoPersona: integer; TipoOrigenDato: integer);
    procedure CargarDomiciliosPersona(CodigoPersona: integer; TipoOrigenDato: integer);
    Procedure CargarGrillaDllPagos(OperadorLogistico, TarjetaCredito: integer);
    procedure CargarDatosPersona(CodigoPersona: integer);      
  public
	{ Public declarations }
	Function Inicializar(MDIChild: Boolean = True): Boolean;

  end;

var
    FormTarjetasDeCredito:    TFormTarjetasDeCredito;
    CodigoPersonaActual:    integer;
    RecNoDomicilioEntrega:  integer;
    CodigoDomicilioEntrega: integer;

implementation
{$R *.DFM}


procedure TFormTarjetasDeCredito.CargarMediosComunicacionPersona(CodigoPersona: integer; TipoOrigenDato: integer);
begin
    //Primero intento abrir el Stored Procedure
    ObtenerMediosComunicacionPersona.Close;
    ObtenerMediosComunicacionPersona.Parameters.ParamByName('@CodigoPersona').Value         := CodigoPersona;
    ObtenerMediosComunicacionPersona.Parameters.ParamByName('@CodigoTipoOrigenDato').Value  := iif(TipoOrigenDato < 1, null, TipoOrigenDato);
    if not OpenTables([ObtenerMediosComunicacionPersona]) then Exit;
    cdsMediosComunicacion.Data := dspMediosComunicacion.Data;
    ObtenerMediosComunicacionPersona.Close;
    HabilitarBotonesMediosComunicacion;
end;


procedure TFormTarjetasDeCredito.HabilitarBotonesDomicilios;
begin
    btnEditarDomicilio.Enabled      := iif( cdsDomicilios.Active, cdsDomicilios.RecordCount > 0, False);
    btnEliminarDomicilio.Enabled    := btnEditarDOmicilio.Enabled;
end;

procedure TFormTarjetasDeCredito.HabilitarBotonesMediosComunicacion;
begin
    btnEditarMedioComunicacion.Enabled      := iif( cdsMediosComunicacion.Active, cdsMediosComunicacion.RecordCount > 0, False);
    btnEliminarMedioComunicacion.Enabled    := btnEditarMedioComunicacion.Enabled;
end;

procedure TFormTarjetasDeCredito.CargarDomiciliosPersona(CodigoPersona: integer; TipoOrigenDato: integer);
begin
    //Primero intento abrir el Stored Procedure
    RecNoDomicilioEntrega := -1;
    ObtenerDomiciliosPersona.Close;
    ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoPersona').Value         := CodigoPersona;
    ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoTipoOrigenDato').Value  := iif(TipoOrigenDato < 1, null, TipoOrigenDato);
    if not OpenTables([ObtenerDomiciliosPersona]) then Exit;
    while not ObtenerDomiciliosPersona.Eof do begin
        if ObtenerDomiciliosPersona.FieldByName('DomicilioEntrega').AsBoolean then begin
            RecNoDomicilioEntrega := ObtenerDomiciliosPersona.RecNo;
            break;
        end;
        ObtenerDomiciliosPersona.Next;
    end;
    ObtenerDomiciliosPersona.First;
    cdsDomicilios.Data := dspDomicilios.Data;
    ObtenerDomiciliosPersona.Close;
    HabilitarBotonesDomicilios;
end;

procedure TFormTarjetasDeCredito.HabilitarCamposPersoneria(Personeria: char);
ResourceString
    LBL_PERSONA_RAZON_SOCIAL        = 'Raz�n Social';
    LBL_PERSONA_NOMBRE_FANTASIA     = 'Nombre de Fantas�a:';
    LBL_PERSONA_SITUACION_IVA       = 'Situaci�n de IVA:';
    LBL_PERSONA_FECHA_CREACION      = 'Fecha de Creaci�n:';
    LBL_PERSONA_APELLIDO            = 'Apellido:';
    LBL_PERSONA_NOMBRE              = 'Nombre:';
    LBL_PERSONA_APELLIDO_MATERNO    = 'Apellido Materno:';
    LBL_PERSONA_CONTACTO_COMERCIAL  = 'Contacto Comercial:';
    LBL_PERSONA_SEXO                = 'Sexo:';
    LBL_PERSONA_FECHA_NACIMIENTO    = 'Fecha de Nacimiento:';

begin
    Case Personeria of
        PERSONERIA_JURIDICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_RAZON_SOCIAL;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE_FANTASIA;
                lblApellidoMaterno.Caption  := LBL_PERSONA_CONTACTO_COMERCIAL;
                lblSexo.Visible             := False;
                cbSexo.Visible              := False;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_CREACION;
            end;
        PERSONERIA_FISICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_APELLIDO;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE;
                lblApellidoMaterno.Caption  := LBL_PERSONA_APELLIDO_MATERNO;
                lblSexo.Caption             := LBL_PERSONA_SEXO;
                lblSexo.Visible             := True;
                cbSexo.Visible              := True;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_NACIMIENTO;
            end;
    end;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Volver_Campos
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Vuelve al estado Inicial los controles del Form
********************************************************************************}
procedure TFormTarjetasDeCredito.Volver_Campos ;
begin
    CodigoPersonaActual := -1;
    btnBuscar.Visible   := False;    
	alTarjetasCredito.Estado := Normal;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex		:= 0;
	alTarjetasCredito.Reload;
	alTarjetasCredito.Refresh;
	alTarjetasCredito.SetFocus;
    Screen.Cursor := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Inicializar
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    MDIChild: Boolean = True
  Return Value:  Boolean
  Description:   Inicializa el Form de Maestro de Personal. Primero determina el
    	formato en que se va a mostrar la pantalla seg�n el par�metro MDIChild y
    	luego intenta abrir la tabla "MaestroPersonal"
********************************************************************************}
function TFormTarjetasDeCredito.Inicializar(MDIChild: Boolean = True): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;

    if not OpenTables([TarjetasCredito]) then Exit;
    CargarOperadoresLogisticos( TarjetasCredito.Connection, cbOperadoresLogisticos, 0, True);
    CargarSexos(cbSexo);
    CargarTipoSituacionIVA(DMConnections.BaseCAC, cbSituacionIVA);
    CargarPersoneria(cbPersoneria, PERSONERIA_JURIDICA);
    cbPersoneria.OnChange(cbPersoneria);
    CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento);
	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento);
    pcDatosPersonal.ActivePageIndex := 0;
    cdsDomicilios.CreateDataSet;
    Volver_Campos;
	Result := True;
end;



{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Limpiar_Campos
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Limpia los valores que figuran en los controles.
********************************************************************************}
procedure TFormTarjetasDeCredito.Limpiar_Campos;
begin
    //Datos De la Persona
    cbPersoneria.ItemIndex := 0;
    CargarPersoneria(cbPersoneria, PERSONERIA_JURIDICA);
    cbPersoneria.OnChange(cbPersoneria);
    txtNombre.Clear;
    txtApellido.Clear;
    txtApellidoMaterno.Clear;
    mcTipoNumeroDocumento.MaskText  := '';
    cbSexo.ItemIndex                := 0;
    cbSituacionIVA.ItemIndex        := 0;
    CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento);
    dtFechaNacimiento.Clear;
    chkActivo.Checked	            := False;

    CargarDomiciliosPersona(-1, 0);
    CargarMediosComunicacionPersona(-1, 0);
    CodigoPersonaActual := -1;
    cbOperadoresLogisticos.OnChange(nil);
end;


procedure TFormTarjetasDeCredito.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;


procedure TFormTarjetasDeCredito.BtnSalirClick(Sender: TObject);
begin
	Close;
end;


procedure TFormTarjetasDeCredito.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	Action := caFree;
end;


procedure TFormTarjetasDeCredito.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;






{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.BtnAceptarClick
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Inserta o Modifica los datos de un Empleado utilizando el
  	procedimiento almacenado "ActualizarMaestroPersonal".
    Antes de Realizar dichas operaciones verifica que esten completos los
    compos considerados obligatorios.
********************************************************************************}
procedure TFormTarjetasDeCredito.BtnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION   = 'Actualizar Tarjeta de Cr�dito';
    MSG_ACTUALIZAR_ERROR	    = 'No se pudieron actualizar los datos de la Tarjeta de Cr�dito';

	MSG_ACTUALIZAR_DOMICILIO_CAPTION            = 'Actualizar Datos del Domicilio';
    MSG_ACTUALIZAR_DOMICILIO_ERROR	            = 'No se pudieron actualizar los datos del Domicilio';

	MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION   = 'Actualizar Medio de Contacto';
    MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR	    = 'No se pudieron actualizar los datos del Medio de Contacto';

	MSG_ACTUALIZAR_TARJETA_CREDITO_CAPTION  = 'Actualizar Datos de la Tarjeta de Cr�dito';
	MSG_ACTUALIZAR_TARJETA_CREDITO_ERROR	   = 'No se pudieron actualizar los datos de la Tarjeta de Cr�dito';

    MSG_VALIDAR_CAPTION 		    = 'Validar datos de la Tarjeta de Cr�dito';
    MSG_VALIDAR_APELLIDO		    = 'Debe ingresar un Apellido para detallar la Tarjeta de Cr�dito';
    MSG_VALIDAR_NOMBRE			    = 'Debe ingresar un Nombre para detallar la Tarjeta de Cr�dito';
    MSG_VALIDAR_SEXO                = 'Debe indicar el g�nero para detallar la Tarjeta de Cr�dito';
    MSG_VALIDAR_SITUACION_IVA       = 'Debe indicar la Situaci�n con Impositiva de la Tarjeta de Cr�dito';
    MSG_VALIDAR_RUT                 = 'Debe ingresar un C�digo de Documento detallar la Tarjeta de Cr�dito';
    MSG_VALIDAR_CANTIDAD_DOMICILIOS         = 'Debe ingresar al menos un domicilio';
    MSG_VALIDAR_CANTIDAD_MEDIOS             = 'Debe ingresar al menos un medio de contacto';
    MSG_ERROR_DOMICILIO_RELACIONADO         = 'Actualizar Domicilio Realacionado';
    MSG_CAPTION_DOMICILIO_RELACIONADO       = 'No se pudo actualizar el domicilio relacionado';
    MSG_VALIDAR_DOMICILIOS_RELACIONADOS     = 'Debe especificar al menos un domicilio relacionado';

    //Propios de la Tarjeta
    MSG_VALIDAR_PRIMER_CARACATER        = 'Debe ingresar el primer caracter de la Banda';
    MSG_VALIDAR_LONGITUD_BANDA          = 'Debe ingresar la Longitud de la Banda';
	MSG_ACTUALIZAR_CAPTION_PREFIJO		= 'Actualizar Prefijo de Tarjeta de Cr�dito';
    MSG_ERROR_VALIDAR_PREFIJO			= 'El prefijo no puede superar los 10 caracteres.';
    MSG_ACTUALIZAR_CAPTION_DLLOPERACION = 'Actualizar DLL de Pago';
var
    TodoOk: Boolean;
    CodigosEliminar, CodigosNoEliminar: AnsiString;
    ListaDomicilios: TStringList;
    i, FCodigoDomicilio: integer;
begin
	if (Trim(txtApellido.Text) = '') then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_APELLIDO, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtApellido);
    	Exit;
    end;

	if (Trim(txtNombre.Text) = '') then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_NOMBRE, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtNombre);
    	Exit;
    end;

    if (StrRight(cbPersoneria.Text, 1) = PERSONERIA_FISICA) and
        (cbSexo.ItemIndex < 0) then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_SEXO, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbSexo);
    	Exit;
    end;

    if (StrRight(cbPersoneria.Text, 1) = PERSONERIA_JURIDICA) and
        (cbSituacionIVA.ItemIndex < 0) then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_SITUACION_IVA, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbSituacionIVA);
    	Exit;
    end;

    if (Trim(mcTipoNumeroDocumento.MaskText) = '') or not ( mcTipoNUmeroDocumento.ValidateMask()) then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_RUT, MSG_VALIDAR_CAPTION, MB_ICONSTOP, mcTipoNumeroDocumento);
    	Exit;
    end;


	if (dblDomicilios.DataSource.DataSet.RecordCount = 0) then begin
    	pcDatosPersonal.ActivePageIndex := 1;
		MsgBoxBalloon( MSG_VALIDAR_CANTIDAD_DOMICILIOS, MSG_VALIDAR_CAPTION, MB_ICONSTOP, dblDomicilios);
    	Exit;
    end;

	if (dblMediosComunicacion.DataSource.DataSet.RecordCount = 0) then begin
    	pcDatosPersonal.ActivePageIndex := 2;
		MsgBoxBalloon( MSG_VALIDAR_CANTIDAD_MEDIOS, MSG_VALIDAR_CAPTION, MB_ICONSTOP, dblMediosComunicacion);
    	Exit;
    end;


    TodoOK          := False;
    ListaDomicilios := TStringList.Create;

	//HASTA ACA TODO BIEN
	Screen.Cursor   := crHourGlass;
    DMConnections.BaseCAC.BeginTrans;
    try
        try
            ActualizarDatosPersona.Close;
            With ActualizarDatosPersona.Parameters do begin

                ParamByName('@Personeria').Value        := iif( cbPersoneria.ItemIndex < 0, null,  Trim(StrRight( cbPersoneria.Text, 1)));
                ParamByName('@Nombre').Value			:= iif(Trim(txtNombre.Text)= '', null, Trim(txtNombre.Text));
                ParamByName('@Apellido').Value			:= iif(Trim(txtApellido.Text)= '', null, Trim(txtApellido.Text));

                if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
                    ParamByName('@ApellidoMaterno').Value	    := iif(Trim(txtApellidoMaterno.Text)= '', null, Trim(txtApellidoMaterno.Text));
                    ParamByName('@Sexo').Value 				    := iif( cbSexo.ItemIndex < 0, null, StrRight( Trim(cbSexo.Text), 1));
                    ParamByName('@ContactoComercial').Value	    := null;
                end else if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_JURIDICA then begin
                    ParamByName('@ApellidoMaterno').Value	    := null;
                    ParamByName('@Sexo').Value 				    := null;
                    ParamByName('@ContactoComercial').Value	    := iif(Trim(txtApellidoMaterno.Text)= '', null, Trim(txtApellidoMaterno.Text));
                end;

                ParamByName('@CodigoDocumento').Value	    := Trim(StrRight(mcTipoNumeroDocumento.ComboText, 10));
                ParamByName('@NumeroDocumento').Value	    := iif(Trim(mcTipoNumeroDocumento.MaskText)= '', null, Trim(mcTipoNumeroDocumento.MaskText));

                ParamByName('@LugarNacimiento').Value 	    := iif( cbLugarNacimiento.ItemIndex < 0, null,  Trim( StrRight(cbLugarNacimiento.Text, 10)));
                ParamByName('@FechaNacimiento').Value 	    := iif( dtFechaNAcimiento.date < 0, null, dtFechaNacimiento.Date);
                ParamByName('@CodigoSituacionIVA').Value    := iif( cbSituacionIVA.ItemIndex < 0, null,  Trim(StrRight( cbSituacionIVA.Text, 2)));
                ParamByName('@CodigoActividad').Value 	    := iif( cbActividades.ItemIndex < 0, null,  Ival(StrRight( Trim(cbActividades.Text), 10)));
                ParamByName('@CodigoPersona').Value 	    := CodigoPersonaActual;
                ParamByName('@CodigoUsuario').Value 	    := UsuarioSistema;          //SS_1419_NDR_20151130
            end;
            ActualizarDatosPersona.ExecProc;
            CodigoPersonaActual :=  ActualizarDatosPersona.Parameters.ParamByName('@CodigoPersona').Value;
            ActualizarDatosPersona.Close;
        except
            On E: Exception do begin
                ActualizarDatosPersona.Close;
                MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;

        //Ahora grabo los domicilios
        //  Borrar todos los domicilios que figuran en la base que ya no pertenecen al cliente.
        if  alTarjetasCredito.Estado = Modi then begin

            cdsDomicilios.First;
            CodigosNoEliminar := '';
            while not cdsDomicilios.Eof do begin
                if (dblDomicilios.DataSource.DataSet.FieldByName('CodigoDomicilio').AsInteger > 0) then begin
                    CodigosNoEliminar := CodigosNoEliminar + ',' + Trim(IntToStr(dblDomicilios.DataSource.DataSet.FieldByName('CodigoDomicilio').AsInteger));
                end;
                cdsDomicilios.Next;
            end;
            CodigosNoEliminar := Trim(Copy(CodigosNoEliminar, 2, length(CodigosNoEliminar)));
            CodigosNoEliminar := '(' + CodigosNoEliminar + ')';
            if CodigosNoEliminar <> '()' then begin
                QueryTemp.Close;
                QueryTemp.SQL.Text :=
                    'SELECT CodigoDomicilio FROM Domicilios ' +
                    ' WHERE CodigoPersona = ' + IntToStr(CodigoPersonaActual) +
                    ' AND CodigoDomicilio not in ' + CodigosNoEliminar;
                QueryTemp.open;

                if QueryTemp.RecordCount > 0 then begin
                    CodigosEliminar := '';
                    While not QueryTemp.Eof do begin
                        CodigosEliminar := CodigosEliminar + ',' + Trim(IntToStr(QueryTemp.FieldByName('CodigoDomicilio').AsInteger));
                        QueryTemp.Next;
                    end;
                    CodigosEliminar := Trim(Copy(CodigosEliminar, 2, length(CodigosEliminar)));

                    try
                        with EliminarDomiciliosPersona.Parameters do begin
                            ParamByName('@ListaDomicilios').Value := CodigosEliminar;
                        end;
                        EliminarDomiciliosPersona.ExecProc;
                        EliminarDomiciliosPersona.Close;
                    except
                        On E: Exception do begin
                            EliminarDomiciliosPersona.Close;
                            MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                            Screen.Cursor := crDefault;
                            Exit;
                        end;
                    end;
                end;
                QueryTemp.Close;
            end else begin
                try
                    EliminarTodosDomiciliosPersona.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersonaActual;
                    EliminarTodosDomiciliosPersona.ExecProc;
                    EliminarTodosDomiciliosPersona.Close;
                except
                    On E: Exception do begin
                        EliminarTodosDomiciliosPersona.Close;
                        MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                        Screen.Cursor := crDefault;
                        Exit;
                    end;
                end;
            end;
        end;

        //Ahora grabo los domicilios que quedaron
        CodigoDomicilioEntrega := -1;
        cdsDomicilios.First;
        While not cdsDomicilios.Eof do begin
            try
                with ActualizarDomicilio.Parameters do begin
                	ParamByName('@CodigoPersona').Value         := CodigoPersonaActual;
                	ParamByName('@CodigoTipoOrigenDato').Value  := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
                	ParamByName('@CodigoPais').Value            := cdsDomicilios.FieldByName('CodigoPais').AsString;
                	ParamByName('@CodigoRegion').Value          := cdsDomicilios.FieldByName('CodigoRegion').AsString;
                	ParamByName('@CodigoComuna').Value          := cdsDomicilios.FieldByName('CodigoComuna').AsString;
                	ParamByName('@Calle').Value                 := cdsDomicilios.FieldByName('Calle').AsString;
                	ParamByName('@Numero').Value                := cdsDomicilios.FieldByName('Numero').AsString;
                	ParamByName('@Detalle').Value               := cdsDomicilios.FieldByName('Detalle').AsString;
                	ParamByName('@CodigoPostal').Value          := cdsDomicilios.FieldByName('CodigoPostal').AsString;
                	ParamByName('@Activo').Value                := cdsDomicilios.FieldByName('Activo').AsBoolean;
                	ParamByName('@CodigoDomicilio').Value       := iif(cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger < 1, null, cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger);
                end;
                ActualizarDomicilio.ExecProc;

                ListaDomicilios.Add(IntToStr(cdsDomicilios.RecNo) + Space(200) + IntToStr(ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value));

                if RecNoDomicilioEntrega = cdsDomicilios.RecNo then CodigoDomicilioEntrega :=  ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
                ActualizarDomicilio.Close;
            except
                On E: Exception do begin
                    ActualizarDomicilio.Close;
                    MsgBoxErr( MSG_ACTUALIZAR_DOMICILIO_ERROR, e.message, MSG_ACTUALIZAR_DOMICILIO_CAPTION, MB_ICONSTOP);
                    Screen.Cursor := crDefault;
                    Exit;
                end;
            end;
            cdsDomicilios.Next;
        end;
        //Fin Domicilios.

        //Actualizo en los datos de la persona el Codigo de Domicilio de Entrega.
        try
            with ActualizarDomicilioEntregaPersona.Parameters do begin
                ParamByName('@CodigoPersona').Value             := CodigoPersonaActual;
                ParamByName('@CodigoDomicilioEntrega').Value    := iif(CodigoDomicilioEntrega < 1, null, CodigoDomicilioEntrega);
            end;
            ActualizarDomicilioEntregaPersona.ExecProc;
            ActualizarDomicilioEntregaPersona.Close;
        except
            On E: Exception do begin
                ActualizarDomicilioEntregaPersona.Close;
                MsgBoxErr( MSG_ACTUALIZAR_DOMICILIO_ERROR, e.message, MSG_ACTUALIZAR_DOMICILIO_CAPTION, MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;


        //Ahora grabo los Medios de Contacto
        QueryExecute( DMConnections.BaseCAC, 'DELETE FROM MediosComunicacion WHERE CodigoPersona = ' + IntToStr(CodigoPersonaActual));

        cdsMediosComunicacion.First;
        While not cdsMediosComunicacion.Eof do begin
            try
                FCodigoDomicilio := -1;
                if (Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) = '') then begin
                    // estoy grabando un Medio de Contacto con Domicilio.
                    FCodigoDomicilio := cdsMediosComunicacion.fieldByName('CodigoDomicilio').AsInteger;
                    if (FCodigoDomicilio < 0) then begin
                        for i:= 0 to ListaDomicilios.Count -1 do begin
                            if Ival(StrLeft(Trim(ListaDomicilios.Strings[i]), 20)) = cdsMediosComunicacion.FieldByName('CodigoDomicilioRecNo').AsInteger then begin
                                FCodigoDomicilio := Ival(StrRight(Trim(ListaDomicilios.Strings[i]), 20));
                                break;
                            end;
                        end;
                    end;
                end;

                if (FCodigoDomicilio > 0) or (Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) <> '') then begin
                    with ActualizarMedioComunicacionPersona.Parameters do begin
                        ParamByName('@CodigoPersona').Value         := CodigoPersonaActual;
                        ParamByName('@CodigoMedioContacto').Value   := cdsMediosComunicacion.fieldByName('CodigoMedioContacto').AsInteger;
                        ParamByName('@CodigoTipoOrigenDato').Value  := cdsMediosComunicacion.fieldByName('CodigoTipoOrigenDato').AsInteger;
                        ParamByName('@Valor').Value                 := iif( Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) = '', null, Trim(cdsMediosComunicacion.fieldByName('Valor').AsString));
                        ParamByName('@CodigoDomicilio').Value       := iif( FCodigoDomicilio < 1, null, FCodigoDomicilio);
                        ParamByName('@HorarioDesde').Value          := iif( cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime <= nulldate, null, cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime);
                        ParamByName('@HorarioHasta').Value          := iif( cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime <= nulldate, null, cdsMediosComunicacion.fieldByName('HorarioHasta').AsDateTime);
                        ParamByName('@Observaciones').Value         := iif( Trim(cdsMediosComunicacion.fieldByName('Observaciones').AsString) = '', null, Trim(cdsMediosComunicacion.fieldByName('Observaciones').AsString));
                        ParamByName('@Activo').Value                := cdsMediosComunicacion.fieldByName('Activo').AsBoolean;
                    end;
                    ActualizarMedioComunicacionPersona.ExecProc;
                    ActualizarMedioComunicacionPersona.Close;
                end;
            except
                on E: exception do begin
                    MsgBoxErr( MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR, e.message, MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION, MB_ICONSTOP);
                    ActualizarMedioComunicacionPersona.Close;
                    Screen.Cursor := crDefault;
                    Exit;
                end;
            end;
            cdsMediosComunicacion.Next;
        end;


        //Ahora grabo los datos propios del empleado
        try
            with ActualizarDatosTarjetaCredito.Parameters do begin
                ParamByName('@CodigoPersona').Value             := CodigoPersonaActual;
                ParamByName('@Activo').Value                    := chkActivo.Checked;
            end;
            ActualizarDatosTarjetaCredito.ExecProc;
            ActualizarDatosTarjetaCredito.Close;
        except
            On E: Exception do begin
                ActualizarDatosTarjetaCredito.Close;
                MsgBoxErr( MSG_ACTUALIZAR_TARJETA_CREDITO_ERROR, e.message, MSG_ACTUALIZAR_TARJETA_CREDITO_CAPTION, MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;
        //Fin Datos de la tarjeta de cr�dito

        //Ahora Grabo las DLLs que se van a usar en Cada Lugar de Venta.
        for i:= 1 to sgDLLOperacion.RowCount -1 do begin
            try
                with ActualizarDLLOperacion do begin
                    Parameters.ParamByName('@CodigoLugarDeVenta').Value         := Ival(StrRight(sgDLLOperacion.Cells[0, i], 10));
                    Parameters.ParamByName('@CodigoTarjeta').Value              := CodigoPersonaActual;
                    Parameters.ParamByName('@DLLOperacion').Value               := Trim(sgDLLOperacion.Cells[1, i]);
                    ExecProc;
                    Close;
                end;
            except
                On E: EDataBaseError do begin
                    MsgBox(E.message, MSG_ACTUALIZAR_CAPTION_DLLOPERACION, MB_ICONSTOP);
                    Screen.Cursor   := crDefault;
                    Exit;
                end;
             end;
        end;

        CodigoPersonaActual := -1;

        TodoOK := True;
    finally
        if TodoOK then DMConnections.BaseCAC.CommitTrans
        else DMConnections.BaseCAC.RollbackTrans;
        FreeAndNil(ListaDomicilios);
    end;
	Volver_Campos;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalClick
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Completa los campos en pantalla utilizando la informaci�n del registro
    actual de la tabla "MaestroPersonal"
********************************************************************************}
procedure TFormTarjetasDeCredito.alTarjetasCreditoClick(Sender: TObject);
begin

    if TarjetasCredito.FieldByName('CodigoPersona').AsInteger = CodigoPersonaActual then Exit;

    TarjetasCredito.DisableControls;

	with TarjetasCredito do begin

        //Cargar Datos Personales
        CargarPersoneria(cbPersoneria, Trim(FieldByName('Personeria').AsString));
        cbPersoneria.OnChange(cbPersoneria);

		txtNombre.Text			    := Trim(FieldByName('Nombre').AsString);
		txtApellido.Text  		    := Trim(FieldByName('Apellido').AsString);
		txtApellidoMaterno.Text  	:= Trim(FieldByName('ApellidoMaterno').AsString);

        CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento, FieldByName('LugarNacimiento').AsString);
        dtFechaNacimiento.Date	    := FieldByName('FechaNacimiento').AsDateTime;

        if Trim(FieldByName('Personeria').AsString) = PERSONERIA_FISICA then
        	CargarSexos(cbSexo, Trim(FieldByName('Sexo').AsString))
        else if Trim(FieldByName('Personeria').AsString) = PERSONERIA_JURIDICA then
        	CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Trim(FieldByName('CodigoSituacionIVA').AsString));

       	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, Trim(FieldByName('CodigoDocumento').AsString));

		mcTipoNumeroDocumento.MaskText	:= Trim(FieldByName('NumeroDocumento').AsString);

        CargarActividadesPersona(DMConnections.BaseCAC, cbActividades,  FieldByName('CodigoActividad').AsInteger);
        chkActivo.Checked		:= FieldByName('Activo').AsBoolean;

        // Cargamos los domicilios
        CargarDomiciliosPersona(FieldByName('CodigoPersona').AsInteger, 0);

        // Cargamos los Medios de Comunicacion
        CargarMediosComunicacionPersona( FieldByName('CodigoPersona').AsInteger, 0);

        CargarGrillaDllPagos( Ival(StrRight(cbOperadoresLogisticos.Text, 10)), TarjetasCredito.FieldByName('CodigoPersona').AsInteger );
	end;
    TarjetasCredito.EnableControls;
    CodigoPersonaActual := TarjetasCredito.FieldByName('CodigoPersona').AsInteger;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalDelete
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Elimina un registro de la tabla "MaestroPersonal".
    Primero intenta eliminarlo, en caso de no ser posible debido a la integridad
    referencial, modifica el estado del registro marc�ndolo como inactivo
    mediante la ejecuci�n de un query.
********************************************************************************}
procedure TFormTarjetasDeCredito.alTarjetasCreditoDelete(Sender: TObject);
resourcestring
	MSG_ELIMINAR_TARJETA_CREDITO_CAPTION 	        = 'Dar de baja la Tarjeta de Cr�dito';
	MSG_ELIMINAR_TARJETA_CREDITO_ERROR 		        = 'No se puede dar de baja esta Tarjeta de Cr�dito porque hay datos que dependen de �l.';
	MSG_ELIMINAR_TARJETA_CREDITO_QUESTION	        = '�Est� seguro de querer dar de baja la Tarjeta de Cr�dito';
	MSG_ELIMINAR_TARJETA_CREDITO_QUESTION_CAPTION   = 'Confirmaci�n...';

begin
	Screen.Cursor := crHourGlass;

    If MsgBox(MSG_ELIMINAR_TARJETA_CREDITO_QUESTION, MSG_ELIMINAR_TARJETA_CREDITO_CAPTION, MB_YESNO) = IDYES then begin
        try
            EliminarTarjetaCredito.Parameters.ParamByName('@CodigoPersona').Value := TarjetasCredito.FieldByName('CodigoPersona').AsInteger;
            EliminarTarjetaCredito.ExecProc;
            EliminarTarjetaCredito.Close;
        Except
            On E: exception do begin
                MsgBoxErr(MSG_ELIMINAR_TARJETA_CREDITO_ERROR, E.message, MSG_ELIMINAR_TARJETA_CREDITO_CAPTION, MB_ICONSTOP);
                EliminarTarjetaCredito.Close;
            end;
        end;
    end;
	Volver_Campos;

	Screen.Cursor      := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalDrawItem
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TDBList; Tabla: TDataSet; Rect: TRect;
  				 State: TOwnerDrawState; Cols: TColPositions
  Return Value:  None
  Description:   Dibuja el la grilla los valores del registro actual de la tabla
  				 "MaestroEmpleado"
********************************************************************************}
procedure TFormTarjetasDeCredito.alTarjetasCreditoDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
		FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;

        with Tabla do begin
            TextOut(Cols[0], Rect.Top, IStr(FieldByName('CodigoPersona').AsInteger, 10));
            TextOut(Cols[1], Rect.Top, Trim(FieldByName('Apellido').AsString));
            TextOut(Cols[2], Rect.Top, Trim(FieldByName('Nombre').AsString));
		end;
	end;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalEdit
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la Modificaci�n de un registro.
********************************************************************************}
procedure TFormTarjetasDeCredito.alTarjetasCreditoEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    altarjetasCredito.Estado	:= modi;
    pcDatosPErsonal.ActivePage      := tsCliente;
	txtApellido.SetFocus;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalInsert
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la inserci�n de un registro.
********************************************************************************}
procedure TFormTarjetasDeCredito.alTarjetasCreditoInsert(Sender: TObject);
begin
	Limpiar_Campos;
    CodigoPersonaActual         := -1;
    btnBuscar.Visible           := true;
    HabilitarCamposEdicion(True);
    alTarjetasCredito.Estado    := alta;
    chkActivo.Checked           := True;
    pcDatosPersonal.ActivePage  := tsCliente;
    cbOperadoresLogisticos.OnChange(cbOperadoresLogisticos);
	txtApellido.SetFocus;
end;

procedure TFormTarjetasDeCredito.alTarjetasCreditoRefresh(Sender: TObject);
begin
	if alTarjetasCredito.Empty then Limpiar_Campos;
end;

procedure TFormTarjetasDeCredito.HabilitarCamposEdicion(habilitar: Boolean);
begin
	alTarjetasCredito.Enabled	    := not Habilitar;
    pcDatosPersonal.Enabled	        := True;
    pnlDatosPersonal.Enabled        := Habilitar;
    pnlDomicilio.Enabled            := Habilitar;
	pnlMediosContacto.Enabled       := Habilitar;
	Notebook.PageIndex 		        := ord(habilitar);
end;


procedure TFormTarjetasDeCredito.cbPersoneriaChange(Sender: TObject);
begin
    HabilitarCamposPersoneria( StrRight(Trim(cbPersoneria.Text), 1)[1]);
end;

procedure TFormTarjetasDeCredito.FormShow(Sender: TObject);
begin
    CodigoPersonaActual := -1;
end;

procedure TFormTarjetasDeCredito.Agregar(Sender: TObject);
ResourceString
    MSG_AGREGAR_CAPTION = 'Agrear Domicilio';
    MSG_ERROR_AGREGAR   = 'No se ha podido agregar el domicilio';

var
	f: TFormEditarDomicilio;
begin
(*
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar() and (f.ShowModal = mrOK) then begin
        try
            with cdsDomicilios do begin
                Append;
                FieldByName('CodigoDomicilio').AsInteger        := -1;
                FieldByName('CodigoPersona').AsInteger          := CodigoPersonaActual;
                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoDomicilio;
                FieldByName('DescriTipoOrigenDato').AsString    := f.DescriTipoDomicilio;
                FieldByName('CodigoCalle').AsInteger            := f.CodigoCalle;
                FieldByName('DescriCalle').AsString             := f.DescripcionCalle;
                FieldByName('Numero').AsInteger                 := f.Numero;
                FieldByName('Piso').AsInteger                   := f.Piso;
                FieldByName('Dpto').AsString                    := f.Depto;
                FieldByName('CalleDesnormalizada').AsString     := f.DescripcionCalle;
                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('CodigoPostal').AsString            := f.CodigoPostal;
//                FieldByName('Activo').AsBoolean                 := f.Activo;
                FieldByName('CodigoPais').AsString              := f.Pais;
                FieldByName('CodigoRegion').AsString            := f.Region;
                FieldByName('CodigoComuna').AsString            := f.Comuna;
                FieldByName('DescriPais').AsString              := f.DescriPais;
                FieldByName('DescriRegion').AsString            := f.DescriRegion;
                FieldByName('DescriComuna').AsString            := f.DescriComuna;
                FieldByName('DireccionCompleta').AsString       := f.DireccionCompleta;
                FieldByName('CodigoTipoEdificacion').AsInteger  := f.TipoEdificacion;
                FieldByName('DescriTipoEdificacion').AsString   := f.DescriTipoEdificacion;
                FieldByName('DomicilioEntrega').AsBoolean       := f.DomicilioEntrega;
                FieldByName('Normalizado').AsInteger            := Ord(f.CodigoCalle > 0);
                FieldByName('CalleRelacionadaUno').AsInteger        := f.CalleRelacionadaUno;
                FieldByName('NumeroCalleRelacionadaUno').AsInteger  := f.NumeroCalleRelacionadaUno;
                FieldByName('CalleRelacionadaDos').AsInteger        := f.CalleRelacionadaDos;
                FieldByName('NumeroCalleRelacionadaDos').AsInteger  := f.NumeroCalleRelacionadaDos;
                FieldByName('DomicilioCompleto').AsString           := f.DomicilioCompleto;
                Post;
                if f.DomicilioEntrega then RecNoDomicilioEntrega   := dsDomicilios.DataSet.RecNo;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR, E.message, MSG_AGREGAR_CAPTION, MB_ICONSTOP);
                dsDomicilios.DataSet.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesDomicilios;
    *)
end;

procedure TFormTarjetasDeCredito.btnEliminarDomicilioClick(Sender: TObject);
ResourceString
    MSG_DELETE_DOMICILIO_QUESTION   = '�Desea eliminar este Domicilio?';
    MSG_DELETE_DOMICILIO_CAPTION    = 'Eliminar Domicilio';
    MSG_DELETE_DOMICILIO_ERROR      = 'No se ha podido eliminar el domicilio';

begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(MSG_DELETE_DOMICILIO_QUESTION, MSG_DELETE_DOMICILIO_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			dsDomicilios.DataSet.Delete;
		Except
			On E: Exception do begin
				dsDomicilios.DataSet.Cancel;
				MsgBoxErr( MSG_DELETE_DOMICILIO_ERROR, e.message, MSG_DELETE_DOMICILIO_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
    HabilitarBotonesDomicilios;    
end;

procedure TFormTarjetasDeCredito.btnEditarDomicilioClick(Sender: TObject);
ResourceString
    MSG_EDITAR_CAPTION = 'Modificar Domicilio';
    MSG_ERROR_EDITAR   = 'No se ha podido modificar el domicilio';
var
	f: TFormEditarDomicilio;
    i: integer;
begin
(*
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar(
        dsDomicilios.DataSet.FieldByName('CodigoDomicilio').AsInteger,
        dsDomicilios.DataSet.FieldByName('CodigoTipoOrigenDato').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('CodigoPais').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoRegion').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoComuna').AsString),
        dsDomicilios.DataSet.FieldByName('CodigoCalle').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('CalleDesnormalizada').AsString),
        dsDomicilios.DataSet.FieldByName('Numero').AsString,
        dsDomicilios.DataSet.FieldByName('Piso').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('Dpto').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('Detalle').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoPostal').AsString),
        dsDomicilios.DataSet.FieldByName('CodigoTipoEdificacion').AsInteger,
        iif(dsDomicilios.DataSet.RecNo = RecNoDomicilioEntrega, true, false),
//        dsDomicilios.DataSet.FieldByName('Activo').AsBoolean,
        dsDomicilios.DataSet.FieldByName('CalleRelacionadaUno').AsInteger,
        dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaUno').AsInteger,
        dsDomicilios.DataSet.FieldByName('CalleRelacionadaDos').AsInteger,
        dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaDos').AsInteger
        ) and (f.ShowModal = mrOK) then begin
        try
            with cdsDomicilios do begin
                for i := 0 to cdsDomicilios.FieldCount -1 do cdsDomicilios.FieldDefs[i].Attributes  := [];
                Edit;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('CodigoPersona').AsInteger          := CodigoPersonaActual;
                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoDomicilio;
                FieldByName('DescriTipoOrigenDato').AsString    := f.DescriTipoDomicilio;
                FieldByName('CodigoCalle').AsInteger            := f.CodigoCalle;
                FieldByName('DescriCalle').AsString             := f.DescripcionCalle;
                FieldByName('Numero').AsInteger                 := f.Numero;
                FieldByName('Piso').AsInteger                   := f.Piso;
                FieldByName('Dpto').AsString                    := f.Depto;
                FieldByName('CalleDesnormalizada').AsString     := f.DescripcionCalle;
                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('CodigoPostal').AsString            := f.CodigoPostal;
                FieldByName('CodigoPais').AsString              := f.Pais;
                FieldByName('CodigoRegion').AsString            := f.Region;
                FieldByName('CodigoComuna').AsString            := f.Comuna;
                FieldByName('CodigoCiudad').AsString            := f.Ciudad;
                FieldByName('DescriPais').AsString              := f.DescriPais;
                FieldByName('DescriRegion').AsString            := f.DescriRegion;
                FieldByName('DescriComuna').AsString            := f.DescriComuna;
                FieldByName('DireccionCompleta').AsString       := f.DireccionCompleta;
                FieldByName('CodigoTipoEdificacion').AsInteger  := f.TipoEdificacion;
                FieldByName('DescriTipoEdificacion').AsString   := f.DescriTipoEdificacion;
                FieldByName('DomicilioEntrega').AsBoolean       := f.DomicilioEntrega;
                FieldByName('Normalizado').AsInteger            := Ord(f.CodigoCalle > 0);
                FieldByName('CalleRelacionadaUno').AsInteger        := f.CalleRelacionadaUno;
                FieldByName('NumeroCalleRelacionadaUno').AsInteger  := f.NumeroCalleRelacionadaUno;
                FieldByName('CalleRelacionadaDos').AsInteger        := f.CalleRelacionadaDos;
                FieldByName('NumeroCalleRelacionadaDos').AsInteger  := f.NumeroCalleRelacionadaDos;
                FieldByName('DomicilioCompleto').AsString           := f.DomicilioCompleto;                
                Post;
                if f.DomicilioEntrega then RecNoDomicilioEntrega   := cdsDomicilios.RecNo;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR, E.message, MSG_EDITAR_CAPTION, MB_ICONSTOP);
                cdsDomicilios.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesDomicilios;
    *)
end;


procedure TFormTarjetasDeCredito.dblDomiciliosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
	With Sender.Canvas do begin
		FillRect(Rect);
        if dblDomicilios.DataSource.DataSet.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;
	end;
    if (Column.Index = 4) and (RecNoDomicilioEntrega = dsDomicilios.DataSet.RecNo) then Text := 'Si';

end;

procedure TFormTarjetasDeCredito.dblMediosComunicacionDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
	With Sender.Canvas do begin
		FillRect(Rect);
        if dblMediosComunicacion.DataSource.DataSet.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;
	end;
    if Column.Index = 3 then Text := Formatdatetime('hh:nn', dblMediosComunicacion.DataSource.DataSet.FieldByName('HorarioDesde').AsDateTime);
    if Column.Index = 4 then Text := Formatdatetime('hh:nn', dblMediosComunicacion.DataSource.DataSet.FieldByName('HorarioHasta').AsDateTime);
end;

procedure TFormTarjetasDeCredito.btnEliminarMedioComunicacionClick(
  Sender: TObject);
ResourceString
    MSG_DELETE_MEDIO_CONTACTO_QUESTION   = '�Desea eliminar este Medio de Contacto?';
    MSG_DELETE_MEDIO_CONTACTO_CAPTION    = 'Eliminar Medio de Contacto';
    MSG_DELETE_MEDIO_CONTACTO_ERROR      = 'No se ha podido eliminar el Medio de Contacto';

begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(MSG_DELETE_MEDIO_CONTACTO_QUESTION, MSG_DELETE_MEDIO_CONTACTO_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			dsMediosComunicacion.DataSet.Delete;
		Except
			On E: Exception do begin
				dsMediosComunicacion.DataSet.Cancel;
				MsgBoxErr( MSG_DELETE_MEDIO_CONTACTO_ERROR, e.message, MSG_DELETE_MEDIO_CONTACTO_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
    HabilitarBotonesMediosComunicacion;
end;

procedure TFormTarjetasDeCredito.btnAgregarMedioComunicacionClick(
  Sender: TObject);
ResourceString
    MSG_AGREGAR_MEDIO_CAPTION = 'Agrear Medio de Contacto';
    MSG_ERROR_AGREGAR_MEDIO   = 'No se ha podido agregar el Medio de Contacto';

var
	f: TFormEditarMedioComunicacion;
begin
    Application.CreateForm(TFormEditarMedioComunicacion, f);
    cdsDomicilios.First;
    {
    While not cdsDomicilios.Eof do begin
        with f.Tabla do begin
            Append;
            FieldByName('CodigoDomicilio').AsInteger        := cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger;
            FieldByName('CodigoPersona').AsInteger          := cdsDomicilios.FieldByName('CodigoPersona').AsInteger;
            FieldByName('CodigoTipoOrigenDato').AsInteger   := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
            FieldByName('DescriTipoOrigenDato').AsString    := cdsDomicilios.FieldByName('DescriTipoOrigenDato').AsString;
            FieldByName('CodigoTipoCalle').AsInteger        := cdsDomicilios.FieldByName('CodigoTipoCalle').AsInteger;
            FieldByName('CodigoCalle').AsInteger            := cdsDomicilios.FieldByName('CodigoCalle').AsInteger;
            FieldByName('DescriCalle').AsString             := cdsDomicilios.FieldByName('DescriCalle').AsString;
            FieldByName('Numero').AsInteger                 := cdsDomicilios.FieldByName('Numero').AsInteger;
            FieldByName('Piso').AsInteger                   := cdsDomicilios.FieldByName('Piso').AsInteger;
            FieldByName('Dpto').AsString                    := cdsDomicilios.FieldByName('Dpto').AsString;
            FieldByName('CalleDesnormalizada').AsString     := cdsDomicilios.FieldByName('CalleDesnormalizada').AsString;
            FieldByName('Detalle').AsString                 := cdsDomicilios.FieldByName('Detalle').AsString;
            FieldByName('CodigoPostal').AsString            := cdsDomicilios.FieldByName('CodigoPostal').AsString;
            FieldByName('Activo').AsBoolean                 := cdsDomicilios.FieldByName('Activo').AsBoolean;
            FieldByName('CodigoPais').AsString              := cdsDomicilios.FieldByName('CodigoPais').AsString;
            FieldByName('CodigoRegion').AsString            := cdsDomicilios.FieldByName('CodigoRegion').AsString;
            FieldByName('CodigoComuna').AsString            := cdsDomicilios.FieldByName('CodigoComuna').AsString;
            FieldByName('CodigoCiudad').AsString            := cdsDomicilios.FieldByName('CodigoCiudad').AsString;
            FieldByName('DescriPais').AsString              := cdsDomicilios.FieldByName('DescriPais').AsString;
            FieldByName('DescriRegion').AsString            := cdsDomicilios.FieldByName('DescriRegion').AsString;
            FieldByName('DescriComuna').AsString            := cdsDomicilios.FieldByName('DescriComuna').AsString;
            FieldByName('DescriCiudad').AsString            := cdsDomicilios.FieldByName('DescriCiudad').AsString;
            FieldByName('DireccionCompleta').AsString       := cdsDomicilios.FieldByName('DireccionCompleta').AsString;
            FieldByName('CodigoTipoEdificacion').AsInteger  := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
            FieldByName('DescriTipoEdificacion').AsString   := cdsDomicilios.FieldByName('DescriTipoEdificacion').AsString;
            FieldByName('DomicilioEntrega').AsBoolean       := cdsDomicilios.FieldByName('DomicilioEntrega').AsBoolean;
            FieldByName('Normalizado').AsInteger            := cdsDomicilios.FieldByName('Normalizado').AsInteger;
            FieldByName('CalleRelacionadaUno').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaUno').AsInteger;
            FieldByName('NumeroCalleRelacionadaUno').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaUno').AsInteger;
            FieldByName('CalleRelacionadaDos').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaDos').AsInteger;
            FieldByName('NumeroCalleRelacionadaDos').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaDos').AsInteger;
            Post;
        end;
        cdsDomicilios.Next
    end;

    if f.Inicializar(1, 1, 1, '', 0, 0, '', '', 0) and (f.ShowModal = mrOK) then begin
        try
            with dsMediosComunicacion.DataSet do begin
                Append;
//                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoOrigenDato;
//                FieldByName('CodigoMedioContacto').AsInteger    := f.MedioContacto;
//                FieldByName('DescriMedioContacto').AsString     := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MediosContacto WHERE CodigoMedioContacto = ' + IntToStr(f.MedioContacto)));
//                FieldByName('TipoMedio').AsString               := f.TipoMedio;
//                FieldByName('DescriTipoOrigenDato').AsString    := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TiposOrigenDatos WHERE CodigoTipoOrigenDato = ' + IntToStr(f.TipoOrigenDato)));
                FieldByName('Valor').AsString                   := f.Valor;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('DescriDomicilio').AsString         := f.DescriDomicilio;
//                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('HorarioDesde').AsDateTime          := f.HoraDesde;
                FieldByName('HorarioHasta').AsDateTime          := f.HoraHasta;
                FieldByName('Observaciones').AsString           := f.Observaciones;
//                FieldByName('Activo').AsBoolean                 := f.Activo;
                FieldByName('CodigoDomicilioRecNo').AsInteger   := f.CodigoDomicilioRecNo;
                Post;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR_MEDIO, E.message, MSG_AGREGAR_MEDIO_CAPTION, MB_ICONSTOP);
                dsMediosComunicacion.DataSet.Cancel;
            end;
        end;
    end;
    }
    f.Release;
    HabilitarBotonesMediosComunicacion;
end;

procedure TFormTarjetasDeCredito.btnEditarMedioComunicacionClick(
  Sender: TObject);
ResourceString
    MSG_EDITAR_MEDIO_CAPTION = 'Modificar Medio de Contacto';
    MSG_ERROR_EDITAR_MEDIO   = 'No se ha podido modificar el Medio de Contacto';

var
	f: TFormEditarMedioComunicacion;
begin
    Application.CreateForm(TFormEditarMedioComunicacion, f);
    cdsDomicilios.First;
    {
    While not cdsDomicilios.Eof do begin
        f.Tabla.Open;
        with f.Tabla do begin
            Append;
            FieldByName('CodigoDomicilio').AsInteger        := cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger;
            FieldByName('CodigoPersona').AsInteger          := cdsDomicilios.FieldByName('CodigoPersona').AsInteger;
            FieldByName('CodigoTipoOrigenDato').AsInteger   := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
            FieldByName('DescriTipoOrigenDato').AsString    := cdsDomicilios.FieldByName('DescriTipoOrigenDato').AsString;
            FieldByName('CodigoTipoCalle').AsInteger        := cdsDomicilios.FieldByName('CodigoTipoCalle').AsInteger;
            FieldByName('CodigoCalle').AsInteger            := cdsDomicilios.FieldByName('CodigoCalle').AsInteger;
            FieldByName('DescriCalle').AsString             := cdsDomicilios.FieldByName('DescriCalle').AsString;
            FieldByName('Numero').AsInteger                 := cdsDomicilios.FieldByName('Numero').AsInteger;
            FieldByName('Piso').AsInteger                   := cdsDomicilios.FieldByName('Piso').AsInteger;
            FieldByName('Dpto').AsString                    := cdsDomicilios.FieldByName('Dpto').AsString;
            FieldByName('CalleDesnormalizada').AsString     := cdsDomicilios.FieldByName('CalleDesnormalizada').AsString;
            FieldByName('Detalle').AsString                 := cdsDomicilios.FieldByName('Detalle').AsString;
            FieldByName('CodigoPostal').AsString            := cdsDomicilios.FieldByName('CodigoPostal').AsString;
            FieldByName('Activo').AsBoolean                 := cdsDomicilios.FieldByName('Activo').AsBoolean;
            FieldByName('CodigoPais').AsString              := cdsDomicilios.FieldByName('CodigoPais').AsString;
            FieldByName('CodigoRegion').AsString            := cdsDomicilios.FieldByName('CodigoRegion').AsString;
            FieldByName('CodigoComuna').AsString            := cdsDomicilios.FieldByName('CodigoComuna').AsString;
            FieldByName('CodigoCiudad').AsString            := cdsDomicilios.FieldByName('CodigoCiudad').AsString;
            FieldByName('DescriPais').AsString              := cdsDomicilios.FieldByName('DescriPais').AsString;
            FieldByName('DescriRegion').AsString            := cdsDomicilios.FieldByName('DescriRegion').AsString;
            FieldByName('DescriComuna').AsString            := cdsDomicilios.FieldByName('DescriComuna').AsString;
            FieldByName('DescriCiudad').AsString            := cdsDomicilios.FieldByName('DescriCiudad').AsString;
            FieldByName('DireccionCompleta').AsString       := cdsDomicilios.FieldByName('DireccionCompleta').AsString;
            FieldByName('CodigoTipoEdificacion').AsInteger  := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
            FieldByName('DescriTipoEdificacion').AsString   := cdsDomicilios.FieldByName('DescriTipoEdificacion').AsString;
            FieldByName('DomicilioEntrega').AsBoolean       := cdsDomicilios.FieldByName('DomicilioEntrega').AsBoolean;
            FieldByName('Normalizado').AsInteger            := cdsDomicilios.FieldByName('Normalizado').AsInteger;
            FieldByName('CalleRelacionadaUno').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaUno').AsInteger;
            FieldByName('NumeroCalleRelacionadaUno').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaUno').AsInteger;
            FieldByName('CalleRelacionadaDos').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaDos').AsInteger;
            FieldByName('NumeroCalleRelacionadaDos').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaDos').AsInteger;
            Post;
        end;
        cdsDomicilios.Next
    end;

    if f.Inicializar(
        dsMediosComunicacion.DataSet.FieldByName('CodigoTipoOrigenDato').AsInteger,
        dsMediosComunicacion.DataSet.FieldByName('CodigoMedioContacto').AsInteger,
        1,
        dsMediosComunicacion.DataSet.FieldByName('Valor').AsString,
        dsMediosComunicacion.DataSet.FieldByName('HorarioDesde').AsDateTime,
        dsMediosComunicacion.DataSet.FieldByName('HorarioHasta').AsDateTime,
        dsMediosComunicacion.DataSet.FieldByName('Observaciones').AsString,
        '',
        dsMediosComunicacion.DataSet.FieldByName('CodigoDomicilio').AsInteger
        //,dsMediosComunicacion.DataSet.FieldByName('Activo').AsBoolean
        ) and (f.ShowModal = mrOK) then begin
        try
            with dsMediosComunicacion.DataSet do begin
                Edit;
//                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoOrigenDato;
//               FieldByName('CodigoMedioContacto').AsInteger    := f.MedioContacto;
//                FieldByName('DescriMedioContacto').AsString     := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MediosContacto WHERE CodigoMedioContacto = ' + IntToStr(f.MedioContacto)));
//                FieldByName('TipoMedio').AsString               := f.TipoMedio;
//                FieldByName('DescriTipoOrigenDato').AsString    := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TiposOrigenDatos WHERE CodigoTipoOrigenDato = ' + IntToStr(f.TipoOrigenDato)));
                FieldByName('Valor').AsString                   := f.Valor;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('DescriDomicilio').AsString         := f.DescriDomicilio;
//                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('HorarioDesde').AsDateTime          := f.HoraDesde;
                FieldByName('HorarioHasta').AsDateTime          := f.HoraHasta;
                FieldByName('Observaciones').AsString           := f.Observaciones;
//                FieldByName('Activo').AsBoolean                 := f.Activo;
                FieldByName('CodigoDomicilioRecNo').AsInteger   := f.CodigoDomicilioRecNo;
                Post;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR_MEDIO, E.message, MSG_EDITAR_MEDIO_CAPTION, MB_ICONSTOP);
                dsMediosComunicacion.DataSet.Cancel;
            end;
        end;
    end;
    }
    f.Release;
    HabilitarBotonesMediosComunicacion;
end;

Procedure TFormTarjetasDeCredito.CargarGrillaDllPagos(OperadorLogistico, TarjetaCredito: integer);
ResourceString
    MSG_LUGAR_DE_VENTA  = 'Lugar de Venta';
    MSG_DLL_OPERACION   = 'DLL de Pago';

var
    ARow: integer;
    Texto: AnsiString;
begin
    sgDllOperacion.RowCount     := 2;
    sgDllOperacion.Cells[0, 0]  := MSG_LUGAR_DE_VENTA;
    sgDllOperacion.Cells[1, 0]  := MSG_DLL_OPERACION;
    sgDllOperacion.FixedRows    := 1;

    qryObtenerDLLPago.Parameters.ParamByName('OperadorLogistico1').Value := OperadorLogistico;
    qryObtenerDLLPago.Parameters.ParamByName('OperadorLogistico2').Value := OperadorLogistico;
    qryObtenerDLLPago.Parameters.ParamByName('Tarjeta').Value := TarjetaCredito;
    if not OpenTables([qryObtenerDLLPago]) then Exit;


    sgDllOperacion.RowCount := qryObtenerDLLPago.RecordCount + 1;
    ARow    := 1;
    qryObtenerDLLPago.First;
    while not qryObtenerDLLPago.Eof do begin

        Texto := iif( OperadorLogistico = 0,  Trim(qryObtenerDLLPago.FieldByName('RazonSocialOperadorLogistico').AsString) + ', ', '' );
        Texto := Texto + Trim(qryObtenerDLLPago.FieldByName('RazonSocialLugarDeVenta').AsString) + Space(200) + Istr(qryObtenerDLLPago.FieldByName('CodigoOperadorLOgistico').AsInteger, 10) +  Istr(qryObtenerDLLPago.FieldByName('CodigoLugarDeVenta').AsInteger, 10);

        sgDllOperacion.Cells[0, ARow] := Texto;
        sgDllOperacion.Cells[1, ARow] := Trim(qryObtenerDLLPago.FieldByName('DLLOperacion').AsString);

        qryObtenerDLLPago.Next;
        inc(ARow);
    end;
    qryObtenerDLLPago.Close;

end;


procedure TFormTarjetasDeCredito.cbOperadoresLogisticosChange(
  Sender: TObject);
begin
    CargarGrillaDllPagos(
        Ival(StrRight(cbOperadoresLogisticos.Text, 10)),
        iif( alTarjetasCredito.Estado = alta, -1, TarjetasCredito.FieldByName('CodigoPersona').AsInteger));
end;

procedure TFormTarjetasDeCredito.btnBuscarClick(Sender: TObject);
ResourceString
    MSG_CAPTION_BUSCAR_PERSONA = 'Buscar Persona';
    MSG_QUESTION_BUSCAR_PERSONA = '�Desea cargar los datos de la persona?' + CRLF +
                    'Al hacerlo se perder�n los datos cargados hasta el momento';
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes , f);
	if f.Inicializa(mcTipoNumeroDocumento.MaskText, Trim(StrRight( mcTipoNumeroDocumento.ComboText, 20)),
	  Trim(TxtApellido.Text), Trim(TxtApellidoMaterno.Text), Trim(TxtNombre.Text))then begin
		if (f.ShowModal = mrok) and (MsgBox( MSG_QUESTION_BUSCAR_PERSONA, MSG_CAPTION_BUSCAR_PERSONA, MB_YESNO) = IDYES) then begin
            CodigoPersonaActual := f.Persona.CodigoPersona;
            CargarDatosPersona(CodigoPersonaActual);
		end;
	end;
	f.Release;
end;

procedure TFormTarjetasDeCredito.CargarDatosPersona(CodigoPersona: integer);
begin
    ObtenerDatosPersona.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
    if not OpenTables([ObtenerDatosPersona]) then Exit;

    try
        with ObtenerDatosPersona do begin
            //Cargar Datos Personales
            CargarPersoneria(cbPersoneria, Trim(FieldByName('Personeria').AsString));
            cbPersoneria.OnChange(cbPersoneria);
            txtNombre.Text			    := Trim(FieldByName('Nombre').AsString);
            txtApellido.Text  		    := Trim(FieldByName('Apellido').AsString);
            txtApellidoMaterno.Text  	:= Trim(FieldByName('ApellidoMaterno').AsString);

            CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento, FieldByName('LugarNacimiento').AsString);
            dtFechaNacimiento.Date	    := FieldByName('FechaNacimiento').AsDateTime;

            if Trim(FieldByName('Personeria').AsString) = PERSONERIA_FISICA then
                CargarSexos(cbSexo, Trim(FieldByName('Sexo').AsString))
            else if Trim(FieldByName('Personeria').AsString) = PERSONERIA_JURIDICA then
                CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Trim(FieldByName('CodigoSituacionIVA').AsString));

            CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, Trim(FieldByName('CodigoDocumento').AsString));

            mcTipoNumeroDocumento.MaskText	:= Trim(FieldByName('NumeroDocumento').AsString);

            CargarActividadesPersona(DMConnections.BaseCAC, cbActividades,  FieldByName('CodigoActividad').AsInteger);
            chkActivo.Checked		:= FieldByName('Activo').AsBoolean;

            // Cargamos los domicilios
            CargarDomiciliosPersona(FieldByName('CodigoPersona').AsInteger, 0);

            // Cargamos los Medios de Comunicacion
            CargarMediosComunicacionPersona( FieldByName('CodigoPersona').AsInteger, 0);

            //txtCUIT.Text			:= FieldByName('CUIT').AsString;
            CodigoPersonaActual     := FieldByName('CodigoPersona').AsInteger;
            CodigoDomicilioEntrega  := FieldByName('CodigoDomicilioEntrega').AsInteger;
        end;
        ObtenerDatosPersona.Close;
    except
        on E: Exception do begin
            ObtenerDatosPersona.Close;
        end;
    end;
end;


procedure TFormTarjetasDeCredito.btnTiposTarjetaCreditoClick(
  Sender: TObject);
var
	f: TFormTarjetasCreditoTipo;
begin
    Application.CreateForm(TFormTarjetasCreditoTipo, f);
    if f.Inicializar(False, TarjetasCredito.FieldByName('CodigoPersona').AsInteger) then  f.ShowModal;
    f.Release;
end;

end.
