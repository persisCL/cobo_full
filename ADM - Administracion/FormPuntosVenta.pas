{-----------------------------------------------------------------------------
 File Name: FormPuntosVenta 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit FormPuntosVenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DmiCtrls, StdCtrls, DPSControls, UtilProc, PeaProcs, Util,
  DB, ADODB, BuscaTab, UtilDB, Grids, DBGrids, DMConnection;

type
  TFormPuntosDeVenta = class(TForm)
    GBDetalle: TGroupBox;
	Label5: TLabel;
    Label7: TLabel;
    txt_Descripcion: TEdit;
    txt_codigo: TEdit;
    Label1: TLabel;
    btPOS: TBuscaTabla;
    btePos: TBuscaTabEdit;
    tbPOS: TADOTable;
    btnDesasignar: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    function btPOSProcess(Tabla: TDataSet; var Texto: String): Boolean;
	procedure btPOSSelect(Sender: TObject; Tabla: TDataSet);
	procedure btnDesasignarClick(Sender: TObject);
	function SetearAsignacionPOS (NumeroPOS: Integer; Estado: Byte): Boolean;
  private
	{ Private declarations }
	FPOSInicial: Integer;
	FPOS: Integer;
	function GetCodigo: string;
	function GetDescripcion : string;
	function GetPOS: Integer;
	procedure SetPos(const Value: Integer);
  public
    { Public declarations }
    function Inicializar(Codigo, Descripcion : string; Pos: Integer):boolean;
    property codigo: string read GetCodigo;
    property descripcion: string read GetDescripcion;
    property Pos: Integer read GetPOS write SetPos;
  end;

var
  FormPuntosDeVenta: TFormPuntosDeVenta;

implementation

resourcestring
    MSG_CAPTION_PUNTOSDEVENTA    = 'Edici�n Punto de Venta.';
    MSG_ERROR_CARGA              = 'Error al inicializar.';
    MSG_ERROR_CAPTION            = 'No se puede modificar este punto de venta.';
    MSG_DESCRIPCION              = 'Debe ingresar la descripci�n';

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:  /  /
  Description:
  Parameters: Codigo, Descripcion : string; Pos: Integer
  Return Value: Boolean

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormPuntosDeVenta.Inicializar(Codigo, Descripcion : string; Pos: Integer):Boolean;
begin
	Result := OpenTables([tbPos]);
	if not Result then Exit;
	FPosInicial:= Pos;
	FPos:= Pos;
	txt_Codigo.Text := Codigo;
    txt_Descripcion.text := Descripcion;
    btePos.Text := QueryGetValue(DMConnections.BaseCAC, 'SELECT DESCRIPCION FROM POS  WITH (NOLOCK) ' +
	 'WHERE NUMEROPOS = ' + IntToStr(FPos));
	btnDesasignar.Enabled := Pos > 0
end;


function TFormPuntosDeVenta.GetCodigo: string;
begin
    result := txt_Codigo.text;
end;

function TFormPuntosDeVenta.GetDescripcion: string;
begin
    result := txt_Descripcion.Text;
end;


procedure TFormPuntosDeVenta.BtnCancelarClick(Sender: TObject);
begin
	SetearAsignacionPOS (FPOSInicial, 1);
	close;
end;

function TFormPuntosDeVenta.SetearAsignacionPOS (NumeroPOS: Integer; Estado: Byte): Boolean;
begin
	QueryExecute(DMConnections.BaseCAC, 'UPDATE POS SET ASIGNADO = ' + IStr0(Estado,1) +
										' WHERE NUMEROPOS = ' + IntToStr(NumeroPOS));

	result := true
end;

procedure TFormPuntosDeVenta.BtnAceptarClick(Sender: TObject);
begin
	if not(ValidateControls([txt_descripcion],
		[trim(txt_descripcion.text) <> ''],
		MSG_CAPTION_PUNTOSDEVENTA ,
		[MSG_DESCRIPCION])) then exit;
   if trim(btePos.Text) = '' then begin
		SetearAsignacionPOS (FPOS, 0);
		Pos := 0;
   end else begin
		SetearAsignacionPOS (FPOS, 1)
   end;
    ModalResult:=mrOk;
end;

function TFormPuntosDeVenta.GetPOS: Integer;
begin
    Result := FPos;
end;

function TFormPuntosDeVenta.btPOSProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Texto := Tabla.FieldByName('Descripcion').AsString;
	Result := True;
end;

procedure TFormPuntosDeVenta.btPOSSelect(Sender: TObject; Tabla: TDataSet);
begin
	SetearAsignacionPOS (FPOS, 0);
	FPOS := Tabla.FieldByName('NumeroPos').AsInteger;
	btePos.Text := Tabla.FieldByName('Descripcion').AsString;
	btnDesasignar.Enabled := FPos > 0
end;

procedure TFormPuntosDeVenta.SetPos(const Value: Integer);
begin
	if Value = FPOS then Exit;
	FPOS := Value;
end;

procedure TFormPuntosDeVenta.btnDesasignarClick(Sender: TObject);
begin
	SetearAsignacionPOS (FPOS, 0);
	FPOS := 0;

	btePos.Text := '';
	btnDesasignar.Enabled := false
end;

end.
