unit WorkflowDsgn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Scada, ExtCtrls, WorkflowComp, UtilProc, Buttons,
  Menus, Util, DB, ADODB, UtilDB, FrmPropTask, FrmPropLink;

type
  TWorkflowDesigner = class(TForm)
    Panel1: TPanel;
    Bevel1: TBevel;
    SpeedButton1: TSpeedButton;
    pop_tarea: TPopupMenu;
    Eliminar1: TMenuItem;
    mnu_Propiedades: TMenuItem;
	N1: TMenuItem;
	Viewer: TWorkflowViewer;
	CrearWorkflow: TADOStoredProc;
    ActualiarTareaWorkflow: TADOStoredProc;
	btn_deltask: TSpeedButton;
	Panel2: TPanel;
	Label2: TLabel;
	Label3: TLabel;
	CrearSecuenciaTareas: TADOStoredProc;
    qry_Nombre: TADOQuery;
    btn_save: TSpeedButton;
	procedure FormCreate(Sender: TObject);
	procedure SpeedButton1Click(Sender: TObject);
	procedure Eliminar1Click(Sender: TObject);
	procedure pop_tareaPopup(Sender: TObject);
	procedure ViewerMouseDown(Sender: TObject; Button: TMouseButton;
	  Shift: TShiftState; X, Y: Integer);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure btn_deltaskClick(Sender: TObject);
	procedure mnu_PropiedadesClick(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
	procedure FormKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
    procedure btn_saveClick(Sender: TObject);
  private
	{ Private declarations }
	FModificado: Boolean;
	FNewVersion: Boolean;
	FCurrentWorkflow: Integer;
	procedure ItemMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
	procedure TaskMoved(Sender: TObject);
	procedure TaskPredChanged(Sender: TObject);
	procedure TaskDblClick(Sender: TObject);
	procedure CargarWorkflow(Wrk: Integer);
	procedure SetupComponents;
    procedure ActualizarWorkflow(CodigoWorkflow: integer);
	function  CheckSaveWorkflow: Boolean;
	function  GuardarWorkflow: Boolean;
	function  ObtenerLayout(Tarea: TWorkflowTask): AnsiString;
  public
	{ Public declarations }
	function Inicializa(CodigoWorkflow: integer): Boolean;
  end;

var
  WorkflowDesigner: TWorkflowDesigner;

implementation

resourcestring
    MSG_DELETE_WORKFLOW	  			= 'Eliminar Workflow';
    MSG_DELETE_WORKFLOW_QUESTION  	= '�Est� seguro de que desea eliminar este workflow?';
    MSG_DELETE_ERROR				= 'Hay otros datos que dependen de este Workflow, por lo cual no puede eliminarse.';
	MSG_NUEVA_TAREA  				= 'Nueva Tarea';
	MSG_GUARDAR_CAMBIOS_QUESTION	= '�Desea guardar los cambios al Workflow?';
	MSG_GUARDAR_QUESTION 			= 'Una o m�s tareas han sido eliminadas o modificadas sustancialmente, por lo cual se generar� una nueva versi�n de este Workflow.' + CRLF + '�Est� seguro de que desea continuar?';
	MSG_GUARDAR_WORKFLOW_CAPTION   	= 'Guardar Workflow';
	MSG_INPUT_QUERY_NUEVO_WORKFLOW	= 'Descripci�n: ';
    MSG_ERROR_DESCRIPCION			= 'Debe indicar una descripci�n';
    MSG_ERROR_AREA 					= 'Al menos una tarea no tiene �rea';
    MSG_CHANGE_NAME_CAPTION			= 'Cambiar Nombre';
    MSG_INPUT_QUERY_NAME_WORKFLOW	= 'Nuevo nombre del Workflow: ';


{$R *.dfm}

{ TWorkflowDesigner }

function TWorkflowDesigner.Inicializa(CodigoWorkflow: integer): Boolean;
begin
	FCurrentWorkflow := CodigoWorkflow;
    ActualizarWorkflow(FCurrentWorkflow);
	Result := True;
end;

procedure TWorkflowDesigner.FormCreate(Sender: TObject);
begin
	Width := Round(Screen.Width * 0.9);
	Height := Round(Screen.Height * 0.6);
	CenterForm(Self);
end;

procedure TWorkflowDesigner.SpeedButton1Click(Sender: TObject);

Var
	Tarea: TWorkflowTask;
begin
	Tarea := TWorkflowTask.Create(nil);
	With Tarea do begin
		Viewer := Self.Viewer;
		Width := 150;
		Height := 60;
		Caption := MSG_NUEVA_TAREA;
		MaxDuration := 12 * 60;
	end;
	SetupComponents;
	ItemMouseDown(Tarea, mbLeft, [], 0, 0);
	FModificado := True;
end;

procedure TWorkflowDesigner.ItemMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Var
	i: Integer;
begin
	if (Button <> mbLeft) then Exit;
	if not (ssShift in Shift) and not (TWorkflowItem(Sender).Selected) then begin
		for i := 0 to Viewer.ItemCount - 1 do begin
			TWorkflowItem(Viewer.Items[i]).Selected := False;
		end;
	end;
	TWorkflowItem(Sender).Selected := True;
	btn_deltask.enabled := true;
end;

procedure TWorkflowDesigner.Eliminar1Click(Sender: TObject);
begin
	btn_deltask.Click;
end;

procedure TWorkflowDesigner.pop_tareaPopup(Sender: TObject);
begin
	if not TWorkflowItem(TPopupMenu(Sender).PopupComponent).Selected then begin
		ItemMouseDown(TWorkflowItem(TPopupMenu(Sender).PopupComponent), mbLeft, [], 0, 0);
	end;
end;

procedure TWorkflowDesigner.ViewerMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Var
	i: Integer;
begin
	for i := 0 to Viewer.ItemCount - 1 do begin
		TWorkflowItem(Viewer.Items[i]).Selected := False;
	end;
	btn_deltask.enabled := false;
end;

procedure TWorkflowDesigner.CargarWorkflow(Wrk: Integer);
begin
	Screen.Cursor := crHourGlass;
	LoadWorkflow(Wrk, 0, CrearWorkflow.Connection, Viewer);
	SetupComponents;
	FCurrentWorkflow := Wrk;
	FModificado := False;
	Screen.Cursor := crDefault;
end;

function TWorkflowDesigner.CheckSaveWorkflow: Boolean;
begin
	if not FModificado then begin
		Result := True;
		Exit;
	end;
	case MsgBox(MSG_GUARDAR_CAMBIOS_QUESTION, MSG_GUARDAR_WORKFLOW_CAPTION, MB_ICONWARNING or MB_YESNOCANCEL) of
		ID_NO: Result := True;
		ID_YES: Result := GuardarWorkflow;
		ID_CANCEL: Result := False;
		else Result := False;
	end;
end;

function TWorkflowDesigner.GuardarWorkflow: Boolean;
Var
	Descri: AnsiString;
	Link: TWorkflowLink;
	Tarea: TWorkflowTask;
	i, MaxTar, Version: Integer;
begin
	Result := False;

	// Checkeamos si es necesario guardar una nueva versi�n
	if FNewVersion and (FCurrentWorkflow > 0) then begin
		if MsgBox( MSG_GUARDAR_CAMBIOS_QUESTION, MSG_GUARDAR_WORKFLOW_CAPTION, MB_YESNO or MB_ICONWARNING) <> IDYES then Exit;
	end;

	// Si es un Workflow nuevo, lo creamos en la BD
	if FCurrentWorkflow <= 0 then begin
		if not InputQuery( MSG_INPUT_QUERY_NUEVO_WORKFLOW, MSG_GUARDAR_WORKFLOW_CAPTION, Descri) then Exit;
		if Trim(Descri) = '' then begin
			MsgBox( MSG_ERROR_DESCRIPCION, MSG_GUARDAR_WORKFLOW_CAPTION, MB_ICONSTOP);
			Exit;
		end;
		CrearWorkflow.Parameters.ParamByName('@Descripcion').Value := Descri;
		CrearWorkflow.ExecProc;
		FCurrentWorkflow := CrearWorkflow.Parameters.ParamByName('@CodigoWorkflow').Value;
		Version := 1;
	end else begin
		// Obtenemos la Versi�n, y si es necesario, la incrementamos
		Screen.Cursor := crHourGlass;
		Version := QueryGetValueInt(CrearWorkflow.Connection, 'SELECT VersionActual ' +
		  'FROM Workflows WHERE CodigoWorkflow = ' + IntToStr(FCurrentWorkflow));
		if FNewVersion then begin
			Inc(Version);
			QueryExecute(CrearWorkflow.Connection, 'UPDATE Workflows SET VersionActual = ' +
			  IntToStr(Version) + ' WHERE CodigoWorkflow = ' + IntToStr(FCurrentWorkflow));
			FNewVersion := False;
		end;
		Screen.Cursor := crDefault;
	end;


	// Calculamos los c�digos de Tarea, y validamos cosas varias
	MaxTar := 0;
	for i := 0 to Viewer.ItemCount - 1 do begin
		if Viewer.Items[i] is TWorkflowTask then begin
			Tarea := TWorkflowTask(Viewer.Items[i]);
			if Tarea.Tag > MaxTar then MaxTar := Tarea.Tag;
			if Tarea.Area <= 0 then begin
				MsgBox( MSG_ERROR_AREA, MSG_GUARDAR_WORKFLOW_CAPTION, MB_ICONSTOP);
				Exit;
			end;
		end;
	end;
	// Guardamos las tareas
	Screen.Cursor := crHourGlass;
	for i := 0 to Viewer.ItemCount - 1 do begin
		if Viewer.Items[i] is TWorkflowTask then begin
			Tarea := TWorkflowTask(Viewer.Items[i]);
			With ActualiarTareaWorkflow do begin
				// Si la tarea no tiene c�digo, se lo asignamos
				if Tarea.Tag = 0 then begin
					Inc(MaxTar);
					Tarea.Tag := MaxTar;
				end;
				// Guardamos los datos generales
				Parameters.ParamByName('@CodigoTarea').Value := Tarea.Tag;
				Parameters.ParamByName('@CodigoWorkflow').Value := FCurrentWorkflow;
				Parameters.ParamByName('@Descripcion').Value := Tarea.Caption;
				Parameters.ParamByName('@TipoTarea').Value := IIf(Tarea.TaskType = ttResult, 'R',
				  IIf(Tarea.TaskType = ttOptional, 'O', 'N'));
				Parameters.ParamByName('@CodigoArea').Value := Tarea.Area;
				Parameters.ParamByName('@DuracionMaxima').Value := Tarea.MaxDuration;
                Parameters.ParamByName('@CodigoProceso').Value := iif(Tarea.Proceso = 0, null, Tarea.Proceso);
				// Limpiamos los campos que no queremos que se guarden en el layout
				Tarea.PopupMenu := nil;
				Tarea.Tag := 0;
				Tarea.Caption := '';
				Tarea.MaxDuration := 0;
				Tarea.Area := 0;
                Tarea.Proceso := 0;
				Tarea.TaskType := ttNormal;
				// Creamos el layout
				Tarea.Tag := Parameters.ParamByName('@CodigoTarea').Value;
				Parameters.ParamByName('@Layout').Value := ObtenerLayout(Tarea);
				ExecProc;
			end;
			Tarea.PopupMenu := pop_Tarea;
		end;
	end;
	// Guardamos la secuencia
	QueryExecute(CrearWorkflow.Connection, 'DELETE FROM SecuenciaTareasWorkflow WHERE ' +
	  'CodigoWorkflow = ' + IntToStr(FCurrentWorkflow) + ' AND Version = ' + IntToStr(Version));
	for i := 0 to Viewer.ItemCount - 1 do begin
		if Viewer.Items[i] is TWorkflowLink then begin
			Link := TWorkflowLink(Viewer.Items[i]);
			With CrearSecuenciaTareas do begin
				Parameters.ParamByName('@CodigoWorkflow').Value := FCurrentWorkflow;
				Parameters.ParamByName('@CodigoTarea').Value := Link.EndTask.Tag;
				Parameters.ParamByName('@CodigoTareaAnterior').Value := Link.StartTask.Tag;
				Parameters.ParamByName('@Condicion').Value := Link.Condition;
				ExecProc;
			end;
		end;
	end;
	// Listo
	FModificado := False;
	FNewVersion := False;
	Screen.Cursor := crDefault;
	Result := True;
end;

function TWorkflowDesigner.ObtenerLayout(Tarea: TWorkflowTask): AnsiString;
var
	BinStream:TMemoryStream;
	StrStream: TStringStream;
begin
	BinStream := TMemoryStream.Create;
	try
		StrStream := TStringStream.Create('');
		try
			BinStream.WriteComponent(Tarea);
			BinStream.Seek(0, soFromBeginning);
			ObjectBinaryToText(BinStream, StrStream);
			StrStream.Seek(0, soFromBeginning);
			Result := StrStream.DataString;
		finally
			StrStream.Free;
		end;
	finally
		BinStream.Free
	end;
end;

procedure TWorkflowDesigner.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckSaveWorkflow;
end;

procedure TWorkflowDesigner.TaskMoved(Sender: TObject);
begin
	FModificado := True;
end;

procedure TWorkflowDesigner.btn_deltaskClick(Sender: TObject);
Var
	i: Integer;
begin
	i := 0;
	While i < Viewer.ItemCount do begin
		if TWorkflowItem(Viewer.Items[i]).Selected then begin
			if (Viewer.Items[i] is TWorkflowTask)
			  and (TWorkflowTask(Viewer.Items[i]).Tag <> 0) then FNewVersion := True;
			Viewer.Items[i].Free;
		end else begin
			Inc(i);
		end;
	end;
	btn_deltask.Enabled := False;
	FModificado := True;
end;

procedure TWorkflowDesigner.SetupComponents;
Var
	i: Integer;
begin
	for i := 0 to Viewer.ItemCount - 1 do begin
		TWorkflowItem(Viewer.Items[i]).DesignMode := True;
		TWorkflowItem(Viewer.Items[i]).OnMouseDown := ItemMouseDown;
		TWorkflowItem(Viewer.Items[i]).PopupMenu := pop_tarea;
		if Viewer.Items[i] is TWorkflowTask then begin
			TWorkflowTask(Viewer.Items[i]).OnDblClick := TaskDblClick;
			TWorkflowTask(Viewer.Items[i]).OnMoved := TaskMoved;
			TWorkflowTask(Viewer.Items[i]).OnPredecessorsChanged := TaskPredChanged;
		end;
	end;
end;

procedure TWorkflowDesigner.TaskPredChanged(Sender: TObject);
begin
	SetupComponents;
	FModificado := True;
end;

procedure TWorkflowDesigner.mnu_PropiedadesClick(Sender: TObject);
Var
	g: TFormPropLink;
	f: TFormPropTarea;
	Tarea: TWorkflowTask;
	Link: TWorkflowLink;
begin
	if TWorkflowItem(pop_tarea.PopupComponent) is TWorkflowTask then begin
		// Propiedades de una tarea
		Application.CreateForm(TFormPropTarea, f);
		if f.Inicializa then begin
			Tarea := TWorkflowTask(pop_tarea.PopupComponent);
			f.Descripcion := Tarea.Caption;
			f.TaskType := Tarea.TaskType;
			f.DuracionMaxima := Tarea.MaxDuration;
			f.Area := Tarea.Area;
            f.Proceso := Tarea.Proceso;
			if (f.ShowModal = mrOk) then begin
				Tarea.Caption := f.Descripcion;
				Tarea.TaskType := f.TaskType;
				Tarea.MaxDuration := f.DuracionMaxima;
                Tarea.Proceso		:= f.Proceso;
				if (Tarea.Area <> f.Area) then begin
					Tarea.Area := f.Area;
					if Tarea.Tag <> 0 then FNewVersion := True;
				end;
				FModificado := True;
			end;
		end;
		f.Release;
	end else begin
		// Propiedades de un link
		Application.CreateForm(TFormPropLink, g);
		if g.Inicializa then begin
			Link := TWorkflowLink(pop_tarea.PopupComponent);
			g.Conditions := Link.Condition;
			if (g.ShowModal = mrOk) then begin
				Link.Condition := g.Conditions;
				FModificado := True;
			end;
		end;
		g.Release;
	end;
end;

procedure TWorkflowDesigner.FormDestroy(Sender: TObject);
begin
	While Viewer.ItemCount > 0 do Viewer.Items[0].Free;
end;

procedure TWorkflowDesigner.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
	if (Key = VK_DELETE) and (Shift = []) and (btn_deltask.enabled) then btn_deltask.Click;
end;

procedure TWorkflowDesigner.TaskDblClick(Sender: TObject);
begin
	pop_tarea.PopupComponent := TWorkflowTask(Sender);
	mnu_Propiedades.Click;
end;

procedure TWorkflowDesigner.ActualizarWorkflow(CodigoWorkflow: integer);
begin
	if not CheckSaveWorkflow then Exit;
	CargarWorkflow(CodigoWorkflow);
end;

procedure TWorkflowDesigner.btn_saveClick(Sender: TObject);
begin
	GuardarWorkflow;
	CargarWorkflow(FCurrentWorkflow);
end;

initialization
	RegisterClass(TWorkflowTask);
end.
