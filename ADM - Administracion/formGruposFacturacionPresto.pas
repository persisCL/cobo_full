unit formGruposFacturacionPresto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate,
  Dateedit, Util, ADODB,DPSControls, VariantComboBox, DMConnection;

type
  TfrmGruposFacturacionPresto = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    GruposFacturacionPresto: TADOTable;
    Label2: TLabel;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    edDiaFacturacion: TNumericEdit;
    cbGruposFacturacion: TVariantComboBox;
    spObtenerGruposFacturacion: TADOStoredProc;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure Volver_Campos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  frmGruposFacturacionPresto: TfrmGruposFacturacionPresto;

implementation

resourcestring
	MSG_DELETE_QUESTION	    = '�Est� seguro de querer eliminar la asignaci�n de Grupo de Facturaci�n?';
    MSG_DELETE_CAPTION 	    = 'Eliminar asignaci�n de Grupo de Facturaci�n';
    MSG_DELETE_ERROR        = 'La Asignaci�n de Grupo de Facturaci�n no se pudo eliminar';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos de asignaci�n de Grupo de Facturaci�n.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar asignaci�n de Grupo de Facturaci�n';

{$R *.DFM}

function TfrmGruposFacturacionPresto.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;
	if not OpenTables([GruposFacturacionPresto, spObtenerGruposFacturacion]) then Exit;
    cbGruposFacturacion.Items.Clear;
    While not spObtenerGruposFacturacion.Eof do begin
        cbGruposFacturacion.Items.Add(  spObtenerGruposFacturacion.FieldByName('Descripcion').AsString,
                                        spObtenerGruposFacturacion.FieldByName('CodigoGrupoFacturacion').AsInteger );
		spObtenerGruposFacturacion.Next;
    end;
    spObtenerGruposFacturacion.Close;
	Result := True;
	DbList1.Reload;
end;

procedure TfrmGruposFacturacionPresto.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TfrmGruposFacturacionPresto.DBList1Click(Sender: TObject);
begin
	 with GruposFacturacionPresto do begin
		  edDiaFacturacion.ValueInt := FieldByName('DiaFacturacion').AsInteger;
          cbGruposFacturacion.Value := FieldByName('CodigoGrupoFacturacion').AsInteger;
     end;
end;

procedure TfrmGruposFacturacionPresto.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('DiaFacturacion').AsString);
        TextOut(Cols[1], Rect.Top, QueryGetValue( DMCOnnections.BaseCAC,
                    Format( 'SELECT dbo.ObtenerDescripcionGrupoFacturacion(%d)',
                    [Tabla.FieldByName('CodigoGrupoFacturacion').AsInteger])));
	end;
end;

procedure TfrmGruposFacturacionPresto.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    edDiaFacturacion.Enabled := False;
    cbGruposFacturacion.SetFocus;
end;

procedure TfrmGruposFacturacionPresto.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TfrmGruposFacturacionPresto.Limpiar_Campos();
begin
	edDiaFacturacion.Clear;
	cbGruposFacturacion.ItemIndex := -1;
end;

procedure TfrmGruposFacturacionPresto.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TfrmGruposFacturacionPresto.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			GruposFacturacionPresto.Delete;
		Except
			On E: Exception do begin
				GruposFacturacionPresto.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor := crDefault;
end;

procedure TfrmGruposFacturacionPresto.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
	edDiaFacturacion.SetFocus;
end;

procedure TfrmGruposFacturacionPresto.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_INVOICE_DAY_MUST_HAVE_A_VALUE   = 'El d�a de facturaci�n no puede quedar en blanco';
    MSG_INVALID_INVOICE_DAY             = 'El d�a de facturaci�n es inv�lido';
    MSG_INVOICE_DAY_ALREADY_EXISTS      = 'El d�a de facturaci�n ya est� ingresado';
    MSG_INVOICE_GROUP_MUST_HAVE_A_VALUE = 'El grupo de facturaci�n no puede quedar en blanco';
begin
    if not ValidateControls ([  edDiaFacturacion,
                                edDiaFacturacion,
                                edDiaFacturacion,
                                cbGruposFacturacion],
						    [   Trim(edDiaFacturacion.Text) <> '',
                                (edDiaFacturacion.ValueInt >= 1) and (edDiaFacturacion.ValueInt <= 32),
                                (DbList1.Estado = Modi) Or (QueryGetValue(DMConnections.BaseCAC,
                                    Format('SELECT dbo.ExisteGrupoFacturacionPresto(%d)', [edDiaFacturacion.ValueInt])) = 'False'),
						        cbGruposFacturacion.ItemIndex <> -1],
						   Caption,
						   [MSG_INVOICE_DAY_MUST_HAVE_A_VALUE,
                            MSG_INVALID_INVOICE_DAY,
                            MSG_INVOICE_DAY_ALREADY_EXISTS,
                            MSG_INVOICE_GROUP_MUST_HAVE_A_VALUE]) then Exit;
                            
 	Screen.Cursor := crHourGlass;
	With GruposFacturacionPresto do begin
		Try
			if DbList1.Estado = Alta then Append
				else Edit;
			FieldByName('DiaFacturacion').AsInteger:= edDiaFacturacion.ValueInt;
			FieldByName('CodigoGrupoFacturacion').AsInteger:= cbGruposFacturacion.Value;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TfrmGruposFacturacionPresto.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TfrmGruposFacturacionPresto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TfrmGruposFacturacionPresto.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TfrmGruposFacturacionPresto.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

end.
