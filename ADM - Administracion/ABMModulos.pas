unit ABMModulos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls;

type
  TFormModulos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Modulos: TADOTable;
    Notebook: TNotebook;
    Label3: TLabel;
    cb_CategoriasModulo: TComboBox;
    neCodigoModulo: TNumericEdit;
    Label2: TLabel;
    qry_MaxCodigoModulo: TADOQuery;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
	procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
	procedure DBList1Delete(Sender: TObject);
	procedure DBList1Insert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
  private
	{ Private declarations }
    EsConsulta: boolean;
	procedure Limpiar_Campos;
	procedure VolverCampos;
	procedure HabilitarCampos;
  public
	{ Public declarations }
	function Inicializa: boolean;

  end;

var
  FormModulos: TFormModulos;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este M�dulo de Sistema?';
    MSG_DELETE_ERROR		= 'No se puede eliminar este M�dulo de Sistema porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar M�dulo de Sistema';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del M�dulo de Sistema.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar M�dulo de Sistema';

{$R *.DFM}

procedure TFormModulos.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
	groupb.Enabled     			:= False;
end;

function TFormModulos.Inicializa: boolean;
Var
	S: TSize;
begin
    //Como no es un ABM, lo dejo as�... el dia de ma�ana si se quiere usar
    //como ABM ver qu� se hace con esta variable y listo (por ahora es privada, puede ser parametro)
    EsConsulta := true;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([Modulos]) then
		Result := False
	else begin
		CargarCategoriasModulos(DMConnections.BaseCAC, cb_CategoriasModulo);
		Result := True;
		DbList1.Reload;
	end;
    if EsConsulta then begin
        txt_Descripcion.Color := neCodigoModulo.Color;
        cb_CategoriasModulo.Color := neCodigoModulo.Color;
        AbmToolbar1.Habilitados := [btSalir, btBuscar];
    end
    else
        AbmToolbar1.Habilitados := [btSalir, btBuscar, btAlta, btBaja, btModi];

   	Notebook.PageIndex := 0;

end;

procedure TFormModulos.BtnCancelarClick(Sender: TObject);
begin
	VolverCampos;
end;

procedure TFormModulos.DBList1Click(Sender: TObject);
begin
	with Modulos do begin
        neCodigoModulo.Value	:= FieldByName('CodigoModulo').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
		CargarCategoriasModulos(DMConnections.BaseCAC, cb_CategoriasModulo, FieldByName('CodigoCategoria').AsInteger)
	end;
end;

procedure TFormModulos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		for i:=0 to sender.SubTitulos.Count - 1 do
			TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormModulos.DBList1Edit(Sender: TObject);
begin
    if EsConsulta then begin
        //Si no se agrega esto, deshabilita la puertita de salir y la lupa de buscar
        DBList1.Estado := Normal;
    end
    else begin
	    HabilitarCampos;
	    dblist1.Estado := modi;
    end;
end;

procedure TFormModulos.DBList1Insert(Sender: TObject);
begin
    if EsConsulta then begin
        //Si no se agrega esto, deshabilita la puertita de salir y la lupa de buscar
        DBList1.Estado := Normal;
    end
    else begin
        Limpiar_Campos;
        HabilitarCampos;
        dblist1.Estado     := Alta;
    end;
end;

procedure TFormModulos.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormModulos.Limpiar_Campos;
begin
    neCodigoModulo.Clear;
	txt_Descripcion.Clear;
	cb_CategoriasModulo.ItemIndex := 0;
end;

procedure TFormModulos.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

procedure TFormModulos.DBList1Delete(Sender: TObject);
begin
    if EsConsulta then begin
        //Si no se agrega esto, deshabilita la puertita de salir y la lupa de buscar
        DBList1.Estado := Normal;
        exit;
    end;    
	Screen.Cursor := crHourGlass;
	try
		If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
			IDYES then begin
			try
				Modulos.Delete;
			Except
				On E: Exception do begin
					Modulos.Cancel;
					MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
				end;
			end;
			DbList1.Reload;
			VolverCampos;
		end;
	finally
		Screen.Cursor      := crDefault;
	end;
end;

procedure TFormModulos.BtnAceptarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	try
		With Modulos do begin
			Try
				if (DbList1.Estado = Alta) then begin
					Append;
                    qry_MaxCodigoModulo.Open;
                    neCodigoModulo.Value := qry_MaxCodigoModulo.FieldByNAme('CodigoModulo').AsInteger + 1;
				    qry_MaxCodigoModulo.Close;
					FieldByName('CodigoModulo').value  := neCodigoModulo.Value;
				end else
					Edit;
				FieldByName('Descripcion').AsString			:= txt_Descripcion.text;
				FieldByName('CodigoCategoria').AsInteger	:= Ival(StrRight(cb_CategoriasModulo.Text, 10));
				Post;
			except
				On E: EDataBaseError do begin
					Cancel;
					MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
					Exit;
				end;
			end;
			VolverCampos;
		end;
	finally
		Screen.Cursor	:= crDefault;
	end;
end;

procedure TFormModulos.FormShow(Sender: TObject);
begin
	DBList1.Reload;
end;

procedure TFormModulos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TFormModulos.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFormModulos.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
	groupb.Enabled     			:= True;
    txt_Descripcion.setfocus;
end;

end.
