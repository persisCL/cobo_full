object FrmBuscarMedioPago: TFrmBuscarMedioPago
  Left = 143
  Top = 269
  BorderStyle = bsDialog
  Caption = 'y'
  ClientHeight = 144
  ClientWidth = 448
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 433
    Height = 97
  end
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 77
    Height = 13
    Caption = 'N'#250'mero de Tag:'
  end
  object Label2: TLabel
    Left = 16
    Top = 50
    Width = 32
    Height = 13
    Caption = 'Titular:'
  end
  object Label3: TLabel
    Left = 16
    Top = 76
    Width = 40
    Height = 13
    Caption = 'Patente:'
  end
  object txt_bandaiso: TEdit
    Left = 120
    Top = 21
    Width = 121
    Height = 21
    MaxLength = 16
    TabOrder = 0
  end
  object txt_titular: TEdit
    Left = 120
    Top = 47
    Width = 289
    Height = 21
    MaxLength = 16
    TabOrder = 1
  end
  object txt_patente: TEdit
    Left = 120
    Top = 73
    Width = 119
    Height = 21
    MaxLength = 16
    TabOrder = 2
  end
  object Button1: TDPSButton
    Left = 272
    Top = 112
    Width = 78
    Height = 28
    Caption = '&Buscar'
    Default = True
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TDPSButton
    Left = 364
    Top = 112
    Width = 78
    Height = 28
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
  end
  object BuscarMediosPago: TStoredProc
    DatabaseName = 'BaseGestion'
    StoredProcName = 'dbo.BuscarMediosPago'
    Left = 8
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Result'
        ParamType = ptResult
      end
      item
        DataType = ftString
        Name = '@BandaISO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@NombreTitular'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@Patente'
        ParamType = ptInput
      end>
  end
end
