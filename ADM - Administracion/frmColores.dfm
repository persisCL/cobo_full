object ColoresForm: TColoresForm
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'ColoresForm'
  ClientHeight = 294
  ClientWidth = 650
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlPrincipal: TPanel
    Left = 0
    Top = 0
    Width = 650
    Height = 253
    Align = alClient
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 305
      Top = 1
      Height = 251
      Color = clMedGray
      ParentColor = False
      ExplicitLeft = 273
      ExplicitTop = 9
      ExplicitHeight = 177
    end
    object pnlEjemplos: TPanel
      Left = 308
      Top = 1
      Width = 341
      Height = 251
      Align = alClient
      ParentBackground = False
      TabOrder = 0
      object pnlEjemploIngreso: TPanel
        Left = 1
        Top = 1
        Width = 339
        Height = 104
        Align = alTop
        ParentColor = True
        TabOrder = 0
        object lblTexto: TLabel
          Left = 16
          Top = 16
          Width = 37
          Height = 13
          Caption = 'Ingreso'
        end
        object lblCombo: TLabel
          Left = 16
          Top = 43
          Width = 69
          Height = 13
          Caption = 'Lista Selecci'#243'n'
        end
        object EEjemplo: TEdit
          Left = 104
          Top = 13
          Width = 225
          Height = 21
          TabOrder = 0
          Text = 'Texto Igresado'
        end
        object CBEjemplo: TComboBox
          Left = 104
          Top = 40
          Width = 225
          Height = 21
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 1
          Text = 'Opcion 1'
          Items.Strings = (
            'Opcion 1'
            'Opcion 2'
            'Opcion 3')
        end
        object btnBoton: TButton
          Left = 252
          Top = 67
          Width = 75
          Height = 25
          Caption = 'Boton'
          TabOrder = 2
        end
      end
      object PCEjemplo: TPageControl
        Left = 1
        Top = 105
        Width = 339
        Height = 145
        ActivePage = TSEjemplo1
        Align = alClient
        OwnerDraw = True
        TabOrder = 1
        OnDrawTab = PCEjemploDrawTab
        object TSEjemplo1: TTabSheet
          Caption = 'Solapa 1'
        end
        object TSEjemplo2: TTabSheet
          Caption = 'Solapa 2'
          ImageIndex = 1
        end
      end
    end
    object pnlEsquemasColores: TPanel
      Left = 1
      Top = 1
      Width = 304
      Height = 251
      Align = alLeft
      Caption = 'pnl1'
      TabOrder = 1
      object DBGEsquemasColores: TDBGrid
        Left = 1
        Top = 1
        Width = 302
        Height = 249
        Align = alClient
        DataSource = dsEsquemasColores
        Options = [dgTitles, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = DBGEsquemasColoresDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'IdColor'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'Descripcion'
            Title.Caption = 'Esquema de Colores'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ColorNormal'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ColorTexto'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ColorSeleccionado'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ColorTextoSeleccionado'
            Visible = False
          end>
      end
    end
  end
  object pnlAbajo: TPanel
    Left = 0
    Top = 253
    Width = 650
    Height = 41
    Align = alBottom
    TabOrder = 1
    object Panel1: TPanel
      Left = 561
      Top = 1
      Width = 88
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnGrabar: TButton
        Left = 7
        Top = 5
        Width = 75
        Height = 25
        Caption = 'Grabar'
        TabOrder = 0
        OnClick = btnGrabarClick
      end
    end
  end
  object MainMenu: TMainMenu
    OwnerDraw = True
    Left = 264
    Top = 8
    object Opcion11: TMenuItem
      Caption = 'Opcion 1'
      OnDrawItem = DrawMenuItem
      object Opcion111: TMenuItem
        Caption = 'Opcion 1 1'
        OnDrawItem = DrawMenuItem
      end
      object Opcion121: TMenuItem
        Caption = 'Opcion 1 2'
        OnDrawItem = DrawMenuItem
      end
      object Opcion131: TMenuItem
        Caption = 'Opcion 1 3'
        OnDrawItem = DrawMenuItem
      end
    end
    object Opcion21: TMenuItem
      Caption = 'Opcion 2'
      OnDrawItem = DrawMenuItem
      object Opcion211: TMenuItem
        Caption = 'Opcion 2 1'
        OnDrawItem = DrawMenuItem
      end
      object Opcion221: TMenuItem
        Caption = 'Opcion 2 2'
        OnDrawItem = DrawMenuItem
      end
      object Opcion231: TMenuItem
        Caption = 'Opcion 2 3'
        OnDrawItem = DrawMenuItem
        object Opcion2311: TMenuItem
          Caption = 'Opcion 2 3 1'
          OnDrawItem = DrawMenuItem
        end
        object Opcion2321: TMenuItem
          Caption = 'Opcion 2 3 2'
          OnDrawItem = DrawMenuItem
        end
        object Opcion2331: TMenuItem
          Caption = 'Opcion 2 3 3 '
          OnDrawItem = DrawMenuItem
        end
      end
    end
  end
  object qryEsquemasColores: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'#9'Esquemascolores.IdColor,'
      #9#9'EsquemasColores.Descripcion,'
      #9#9'ColorNormal.Valor AS ColorNormal,'
      #9#9'ColorTexto.Valor AS ColorTexto,'
      #9#9'ColorSeleccionado.Valor AS ColorSeleccionado,'
      #9#9'ColorTextoSeleccionado.Valor AS ColorTextoSeleccionado'
      #9' FROM EsquemasColores'
      
        'INNER JOIN Colores ColorNormal ON ColorNormal.IDColor=EsquemasCo' +
        'lores.IDColorNormal'
      
        'INNER JOIN Colores ColorTexto ON ColorTexto.IDColor=EsquemasColo' +
        'res.IDColorTexto'
      
        'INNER JOIN Colores ColorSeleccionado ON ColorSeleccionado.IDColo' +
        'r=EsquemasColores.IDColorSeleccionado'
      
        'INNER JOIN Colores ColorTextoSeleccionado ON ColorTextoSeleccion' +
        'ado.IDColor=EsquemasColores.IDColorTextoSeleccionado')
    Left = 264
    Top = 40
    object qryEsquemasColoresIdColor: TLargeintField
      FieldName = 'IdColor'
      ReadOnly = True
    end
    object qryEsquemasColoresDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object qryEsquemasColoresColorNormal: TStringField
      FieldName = 'ColorNormal'
      Size = 10
    end
    object qryEsquemasColoresColorTexto: TStringField
      FieldName = 'ColorTexto'
      Size = 10
    end
    object qryEsquemasColoresColorSeleccionado: TStringField
      FieldName = 'ColorSeleccionado'
      Size = 10
    end
    object qryEsquemasColoresColorTextoSeleccionado: TStringField
      FieldName = 'ColorTextoSeleccionado'
      Size = 10
    end
  end
  object dsEsquemasColores: TDataSource
    DataSet = qryEsquemasColores
    Left = 264
    Top = 72
  end
  object qryFijarEsquema: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Parametro'
        Attributes = [paSigned]
        DataType = ftBoolean
        Precision = 19
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE Esquemascolores'
      'SET Seleccionado = 1'
      'WHERE IdColor = :Parametro')
    Left = 264
    Top = 104
    object LargeintField1: TLargeintField
      FieldName = 'IdColor'
      ReadOnly = True
    end
    object StringField1: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object StringField2: TStringField
      FieldName = 'ColorNormal'
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'ColorTexto'
      Size = 10
    end
    object StringField4: TStringField
      FieldName = 'ColorSeleccionado'
      Size = 10
    end
    object StringField5: TStringField
      FieldName = 'ColorTextoSeleccionado'
      Size = 10
    end
  end
  object qryLimpiarEsquemas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'IdColor'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE Esquemascolores'
      'SET Seleccionado=0'
      '')
    Left = 264
    Top = 136
    object LargeintField2: TLargeintField
      FieldName = 'IdColor'
      ReadOnly = True
    end
    object StringField6: TStringField
      FieldName = 'Descripcion'
      Size = 50
    end
    object StringField7: TStringField
      FieldName = 'ColorNormal'
      Size = 10
    end
    object StringField8: TStringField
      FieldName = 'ColorTexto'
      Size = 10
    end
    object StringField9: TStringField
      FieldName = 'ColorSeleccionado'
      Size = 10
    end
    object StringField10: TStringField
      FieldName = 'ColorTextoSeleccionado'
      Size = 10
    end
  end
end
