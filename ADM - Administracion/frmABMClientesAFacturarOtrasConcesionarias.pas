{------------------------------------------------------------------------
                   frmABMClientesAFacturarOtrasConcesionarias

    Author      : mbecerra
    Date:       : 01-Febrero-2011
    Description :       (Ref Fase II)
                    ABM para la tabla ClientesAFacturarOtrasConcesionarias

Autor       :   CQuezadaI
Fecha       :   18 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se quita un "Clear" y se coloca por dfm el "Prepare" para qryEjecutar
---------------------------------------------------------------------------}
unit frmABMClientesAFacturarOtrasConcesionarias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DbList, Abm_obj, DB, ADODB, Validate, DateEdit,
  DmiCtrls, VariantComboBox, PeaProcs, Util, UtilDB, UtilProc, DMConnection, BuscaClientes,
  ListBoxEx, DBListEx, Buttons;

type
  TABMClientesAFacturarOtrasConcesionariasForm = class(TForm)
    pnlCampos: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    vcbConcesionaria: TVariantComboBox;
    Label3: TLabel;
    peNumeroDocumento: TPickEdit;
    deFechaInicio: TDateEdit;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    spObtenerConcesionarias: TADOStoredProc;
    Label4: TLabel;
    deFechaFin: TDateEdit;
    Label5: TLabel;
    lblCliente: TLabel;
    dblClientesAFacturar: TDBListEx;
    qryClientes: TADOQuery;
    dsClientes: TDataSource;
    pnlToolbar: TPanel;
    btnExit: TSpeedButton;
    btnInsertar: TSpeedButton;
    btnEliminar: TSpeedButton;
    btnModificar: TSpeedButton;
    btnBuscar: TSpeedButton;
    btnRefrescar: TSpeedButton;
    neRegistros: TNumericEdit;
    peNumDocABuscar: TPickEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    lblTotalRegistros: TLabel;
    qryEjecutar: TADOQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure dsClientesDataChange(Sender: TObject; Field: TField);
    procedure btnInsertarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
  private
    { Private declarations }
    FUltimaBusqueda : TBusquedaCliente;
    FEstadoSQL,
    FCantidadTotal : Integer;
  public
    { Public declarations }
    function Inicializar : boolean;
    procedure LimpiarCampos;
    procedure HabilitarCampos( Habilitar : boolean);
    procedure EjecutarConsultaSQL(cCualConsulta : Byte);
    procedure ObtenerTotalRegistros;
  end;

var
  ABMClientesAFacturarOtrasConcesionariasForm: TABMClientesAFacturarOtrasConcesionariasForm;

implementation

{$R *.dfm}


const
    cConsultaSQLCantidad = 1;
    cConsultaSQLNumeroDoc = 2;
    EstadoInsertar = 1;
    EstadoModificar = 2;
    EstadoVisualizar = 3;


{--------------------------------------------------------------------
            Inicializar

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Inicializa el formulario

------------------------------------------------------------------------}
function TABMClientesAFacturarOtrasConcesionariasForm.Inicializar;
resourcestring
    MSG_CAPTION = 'Clientes a Facturar Otras Concesionarias';
    MSG_SQL     = 'SELECT dbo.CONST_TIPO_CONCESIONARIA_AUTOPISTA()';

var
    TipoAutopista : Integer;
begin
    Caption     := MSG_CAPTION;
    Position    := poScreenCenter;
    qryClientes.Connection := DMConnections.BaseCAC;

    //Cargamos las concesionarias
    TipoAutopista := QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL);
    with spObtenerConcesionarias do begin
        Connection := DMConnections.BaseCAC;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoTipoConcesionaria').Value := TipoAutopista;
        Open;
        while not EOF do begin
            vcbConcesionaria.Items.Add( FieldByName('Descripcion').AsString, FieldByName('CodigoConcesionaria').AsInteger);
            Next;
        end;
    end;
    spObtenerConcesionarias.Close;

    //obtenemos la cantidad total de registros existentes
    ObtenerTotalRegistros();

    //ejecutamos la consulta
    neRegistros.ValueInt := 100;
    EjecutarConsultaSQL(cConsultaSQLCantidad);
    HabilitarCampos(False);
    Result := True;

end;

{--------------------------------------------------------------------
            EjecutarConsultaSQL

    Author      : mbecerra
    Date        : 20-Abril-2011
    Description :       (Ref Fase 2)
                    Realiza la consulta SQl que trae una cantidad fija de
                    registros o una b�squeda por Rut
------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.EjecutarConsultaSQL(cCualConsulta: Byte);
var
    cSQL : string;
begin
	if cCualConsulta = cConsultaSQLCantidad then cSQL := 'SELECT TOP ' + IntToStr(neRegistros.ValueInt)
    else cSQL := 'SELECT  ';

    //campos
    cSQL := cSQL +	' cli.IDClienteAFacturar, ' +
					' cli.CodigoCliente, ' +
					' cli.NumeroDocumento, ' +
					' cli.CodigoDocumento, ' +
					' cli.CodigoConcesionaria, ' +
					' cli.FechaInicioVigencia, ' +
					' cli.FechaFinVigencia, ' +
                    ' RTRIM(LTRIM(ISNULL(p.Nombre, ''''))) + '' '' + RTRIM(LTRIM(ISNULL(p.Apellido, ''''))) + '' '' + RTRIM(LTRIM(ISNULL(p.ApellidoMaterno,''''))) AS NombreCliente, ' +
                    ' co.Descripcion AS NombreConcesionaria ';

    //from
    cSQL := cSQL + ' FROM ClientesAFacturarOtrasConcesionarias cli (NOLOCK) '
                 + '		INNER JOIN Personas p (NOLOCK) ON cli.CodigoCliente = p.CodigoPersona '
                 + '		INNER JOIN Concesionarias co (NOLOCK) ON cli.CodigoConcesionaria = co.CodigoConcesionaria';

    //where
    if cCualConsulta = cConsultaSQLNumeroDoc then begin
        cSQL := cSQL + ' WHERE cli.NumeroDocumento = ''' + peNumDocABuscar.Text + ''' ';
    end;

    qryClientes.Close;
    qryClientes.SQL.Text := cSQL;
    qryClientes.Open;
    FEstadoSQL := EstadoVisualizar;
    dsClientesDataChange(nil, nil);
end;

{--------------------------------------------------------------------
            EjecutarConsultaSQL

    Author      : mbecerra
    Date        : 20-Abril-2011
    Description :       (Ref Fase 2)
                    Realiza la consulta SQl que trae una cantidad fija de
                    registros o una b�squeda por Rut
------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.ObtenerTotalRegistros;
resourcestring
    MSG_SQL = 'SELECT ISNULL(COUNT(*),0) FROM ClientesAFacturarOtrasConcesionarias (NOLOCK)';
begin
	FCantidadTotal := QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL);
    lblTotalRegistros.Caption := Format('%.0n', [FCantidadTotal * 1.0]) + ' registros';
end;

{--------------------------------------------------------------------
            btnBuscarClick

    Author      : mbecerra
    Date        : 20-Abril-2011
    Description :       (Ref Fase 2)
                    Busca Clientes a Facturar en base al Numero de Documento

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnBuscarClick(
  Sender: TObject);
resourcestring
	MSG_DEBE_NUMDOC = 'Debe ingresar el Numero de Documento';
begin
    if not ValidateControls([peNumDocABuscar], [peNumDocABuscar.Text <> ''], Caption, [MSG_DEBE_NUMDOC]) then Exit;

    EjecutarConsultaSQL(cConsultaSQLNumeroDoc);
    
end;
{--------------------------------------------------------------------
            HabilitarCampos

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Coloca los campos en modo edici�n o inserci�n

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.HabilitarCampos;
begin
    pnlCampos.Enabled := Habilitar;
    pnlToolbar.Enabled := not Habilitar;
    if Habilitar then begin
        Notebook.PageIndex := 1;
        peNumeroDocumento.SetFocus;
    end
    else begin
        Notebook.PageIndex := 0;
    end;

end;

{--------------------------------------------------------------------
            LimpiarCampos

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Limpia los campos editables

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.LimpiarCampos;
begin
    lblCliente.Caption := '';
    peNumeroDocumento.Clear;
    vcbConcesionaria.ItemIndex := -1;
    deFechaInicio.Clear;
    deFechaFin.Clear;
end;

{--------------------------------------------------------------------
            peNumeroDocumentoButtonClick

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Permite la busqueda de clientes

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.peNumeroDocumentoButtonClick(
  Sender: TObject);
begin
    FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);

    Application.createForm(TFormBuscaClientes, FormBuscaClientes);
	if FormBuscaClientes.Inicializa(FUltimaBusqueda) then begin
        FormBuscaClientes.ShowModal;
        if FormBuscaClientes.ModalResult = idOk then begin
            FUltimaBusqueda := FormBuscaClientes.UltimaBusqueda;
            peNumeroDocumento.Text :=  FormBuscaClientes.Persona.NumeroDocumento;
            lblCliente.Caption :=   Trim(FormBuscaClientes.Persona.Nombre) + ' ' +
                                    Trim(FormBuscaClientes.Persona.Apellido) + ' ' +
                                    Trim(FormBuscaClientes.Persona.ApellidoMaterno);
            peNumeroDocumento.setFocus;
        end;
    end;
    FormBuscaClientes.free;
end;


{--------------------------------------------------------------------
            btnAceptarClick

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Graba la edici�n o la inserci�n

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnAceptarClick(
  Sender: TObject);
resourcestring
    MSG_ERROR           = 'Error al intentar grabar';
    MSG_CLI_NO_EXISTE   = 'No se encontr� el cliente de rut %s';
    MSG_CLI_NO_INGRE    = 'Debe indicar el cliente';
    MSG_CON_NO_EXISTE   = 'Debe indicar la concesionaria';
    MSG_FEC_NO_EXISTE   = 'Debe indicar la fecha de inicio de vigencia';
    MSG_CLI_YA_ESTA		= 'El cliente ya est� activo. Debe primero poner fin a su vigencia actual';
    MSG_CLI_FECHAS		= 'Fechas de Vigencia se superponen con un registro anterior para el mismo cliente';
    MSG_FECHAS_RANGO	= 'La Fecha de Fin de Vigencia no puede ser inferior a la fecha de inicio de vigencia';

    MSG_SQL_CLI         = 'SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s'' ';
    MSG_SQL_YA_EXISTE	= 'SELECT IDClienteAFacturar FROM ClientesAFacturarOtrasConcesionarias (nolock) WHERE NumeroDocumento = ''%s'' AND FechaFinVigencia IS NULL';


    MSG_SQL_INSERT		= 'INSERT INTO ClientesAFacturarOtrasConcesionarias ( '+
    					  'CodigoCliente, NumeroDocumento, CodigoDocumento, CodigoConcesionaria, ' +
                    	  'FechaInicioVigencia, FechaFinVigencia, FechaHoraCreacion, UsuarioCreacion, ' +
                    	  'FechaHoraModificacion, UsuarioModificacion ) ' +
                          ' VALUES ( :cli, :numdoc, ''RUT'', :conce, :fecini, :fecfin, GETDATE(), :user1, GETDATE(), :user2 )';
                          
    MSG_SQL_UPDATE		= 'UPDATE ClientesAFacturarOtrasConcesionarias ' +
    					  ' SET CodigoCliente = :cli, NumeroDocumento = :numdoc, CodigoConcesionaria = :conce, FechaInicioVigencia = :fecini, ' +
                          ' FechaFinVigencia = :fecfin, FechaHoraModificacion = GETDATE(), UsuarioModificacion = :user1 ' +
                          ' WHERE IDClienteAFacturar = %d';

    MSG_SQL_CLI_FECHAS	= 'SELECT 1 FROM ClientesAFacturarOtrasConcesionarias (NOLOCK) WHERE FechaInicioVigencia <= ''%s'' AND  FechaFinVigencia >= ''%s'' AND IDClienteAFacturar <> %d';
     
var
    CodigoPersona, IDTabla : Integer;
begin
    try
        //Validar que el cliente exista
        CodigoPersona := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_CLI, [peNumeroDocumento.Text]));
        if not ValidateControls([	peNumeroDocumento,
        							peNumeroDocumento,
                                    vcbConcesionaria,
                                    deFechaInicio
        						],
                                [	peNumeroDocumento.text <> '',
                                	CodigoPersona > 0,
                                	vcbConcesionaria.ItemIndex >= 0,
                                    deFechaInicio.Date <> NullDate
                                ],
                                Caption,
                                [   MSG_CLI_NO_INGRE,
                                    Format(MSG_CLI_NO_EXISTE, [peNumeroDocumento.Text]),
                                    MSG_CON_NO_EXISTE,
                                    MSG_FEC_NO_EXISTE
                                ] ) then Exit;

        //Validar si est� ingresando un nuevo cliente, no est� vigente
        if FEstadoSQL = EstadoInsertar then begin
        	IDTabla := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_YA_EXISTE, [peNumeroDocumento.Text]));
            if IDTabla > 0 then begin
                MsgBoxBalloon(MSG_CLI_YA_ESTA,Caption, MB_ICONEXCLAMATION, peNumeroDocumento);
                Exit;
            end;
        end;

        //validar que si ingres� fecha de fin, no sea inferior a la fecha inicial
        if (deFechaFin.Date <> NullDate) and (deFechaFin.Date < deFechaInicio.Date) then begin
            MsgBoxBalloon(MSG_FECHAS_RANGO, Caption, MB_ICONEXCLAMATION, deFechaFin);
            Exit;
        end;

        //Ya sea un insert o un update, si est� grabando un cliente vigente,
        //ver que las fechas no se traslapen
        if deFechaFin.Date = NullDate then begin
        	IDTabla := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_CLI_FECHAS, [FormatDateTime('yyyymmdd', deFechaInicio.Date), FormatDateTime('yyyymmdd', deFechaInicio.Date), IIf(FEstadoSQL = EstadoInsertar, 0, qryClientes.FieldByName('CodigoCliente').AsInteger)]));
            if IDTabla > 0 then begin
            	MsgBoxBalloon(MSG_CLI_FECHAS,Caption, MB_ICONEXCLAMATION, peNumeroDocumento);
                Exit;
            end;

        end;


        //grabar
        qryEjecutar.Close;
        qryEjecutar.Connection := DMConnections.BaseCAC;
        //qryEjecutar.Parameters.Clear; // SS_1147_CQU_20140714
        if FEstadoSQL = EstadoInsertar then begin
            qryEjecutar.SQL.Text := MSG_SQL_INSERT;
            qryEjecutar.Parameters.ParamByName('cli').Value		:= CodigoPersona;
            qryEjecutar.Parameters.ParamByName('numdoc').Value	:= peNumeroDocumento.Text;
            qryEjecutar.Parameters.ParamByName('conce').Value	:= vcbConcesionaria.Value;
            qryEjecutar.Parameters.ParamByName('fecini').Value	:= deFechaInicio.Date;
            qryEjecutar.Parameters.ParamByName('fecfin').Value	:= IIf(deFechaFin.Date > 0, deFechaFin.Date, null);
            qryEjecutar.Parameters.ParamByName('user1').Value	:= UsuarioSistema;
            qryEjecutar.Parameters.ParamByName('user2').Value	:= UsuarioSistema;
        end
        else begin
            qryEjecutar.SQL.Text := Format(MSG_SQL_UPDATE, [qryClientes.FieldByName('IDClienteAFacturar').AsInteger]);
            qryEjecutar.Parameters.ParamByName('cli').Value		:= CodigoPersona;
            qryEjecutar.Parameters.ParamByName('numdoc').Value	:= peNumeroDocumento.Text;
            qryEjecutar.Parameters.ParamByName('conce').Value	:= vcbConcesionaria.Value;
            qryEjecutar.Parameters.ParamByName('fecini').Value	:= deFechaInicio.Date;
            qryEjecutar.Parameters.ParamByName('fecfin').Value	:= IIf(deFechaFin.Date > 0, deFechaFin.Date, null);
            qryEjecutar.Parameters.ParamByName('user1').Value	:= UsuarioSistema;
        end;

        qryEjecutar.ExecSQL;
        qryClientes.Close;
        qryClientes.Open;

        FEstadoSQL := EstadoVisualizar;
        dsClientesDataChange(nil, nil);
    except on e:Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
        end;

    end;

    HabilitarCampos(False);
end;

{--------------------------------------------------------------------
            btnCancelarClick

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Cancela la edici�n o la inserci�n

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnCancelarClick(
  Sender: TObject);
begin
    HabilitarCampos(False);
    FEstadoSQL := EstadoVisualizar;
    dsClientesDataChange(nil, nil);
end;

{--------------------------------------------------------------------
            btnEliminarClick

    Author      : mbecerra
    Date        : 26-Abril-2011
    Description :       (Ref Fase 2)
                    Permite eliminar un cliente

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnEliminarClick(
  Sender: TObject);
resourcestring
	MSG_ESTA_SEGURO		= '�Est� seguro(a) de eliminar el registro actual?';
    MSG_REG_ELIMINADO	= 'El registro ha sido eliminado';
    MSG_SQL_ERROR		= 'Error al eliminar el registro';

    MSG_SQL_DELETE		= 'DELETE FROM ClientesAFacturarOtrasConcesionarias WHERE IDClienteAFacturar = %d';

begin
    if (not qryClientes.Eof) and (not qryClientes.Fields[0].IsNull) and (MsgBox(MSG_ESTA_SEGURO, Caption, MB_ICONQUESTION + MB_YESNO) = IDYES) then begin
    	try
    		qryEjecutar.Close;
        	qryEjecutar.Connection := DMConnections.BaseCAC;
        	qryEjecutar.Parameters.Clear;
        	qryEjecutar.SQL.Text := Format(MSG_SQL_DELETE, [qryClientes.FieldByName('IDClienteAFacturar').AsInteger]);
        	qryEjecutar.ExecSQL;
            qryClientes.Close;
            qryClientes.Open;
            MsgBox(MSG_REG_ELIMINADO, Caption, MB_ICONINFORMATION);
        except on e:exception do begin
                MsgBoxErr(MSG_SQL_ERROR, e.Message, Caption, MB_ICONERROR);
        	end;
        end;
    end;

end;

{--------------------------------------------------------------------
            btnInsertarClick

    Author      : mbecerra
    Date        : 22-Abril-2011
    Description :       (Ref Fase 2)
                    Permite ingresar un nuevo cliente

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnInsertarClick(
  Sender: TObject);
begin
    FEstadoSQL := EstadoInsertar;
    HabilitarCampos(True);
    LimpiarCampos();
    peNumeroDocumento.SetFocus;
end;

{--------------------------------------------------------------------
            btnModificarClick

    Author      : mbecerra
    Date        : 22-Abril-2011
    Description :       (Ref Fase 2)
                    Permite modificar un cliente existente

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnModificarClick(
  Sender: TObject);
begin
    FEstadoSQL := EstadoModificar;
    HabilitarCampos(True);
    peNumeroDocumento.SetFocus;
end;

{--------------------------------------------------------------------
            btnRefrescarClick

    Author      : mbecerra
    Date        : 20-Abril-2011
    Description :       (Ref Fase 2)
                    Busca Clientes a Facturar en base a un TOP <numero>

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnRefrescarClick(
  Sender: TObject);
resourcestring
	MSG_DEBE_CANTIDAD = '�Est� seguro de querer visualizar los %d registros?';
begin
    if (neRegistros.ValueInt = 0) and (MsgBox(Format(MSG_DEBE_CANTIDAD, [FCantidadTotal]), Caption, MB_ICONQUESTION + MB_YESNO) = IDNO)then Exit;
    EjecutarConsultaSQL(cConsultaSQLCantidad);
end;

{--------------------------------------------------------------------
            btnSalirClick

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Sale de la ventana

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.btnSalirClick(
  Sender: TObject);
begin
    Close;
end;

{--------------------------------------------------------------------
            dsClientesDataChange

    Author      : mbecerra
    Date        : 20-Abril-2011
    Description :       (Ref Fase 2)
                    Visualiza los datos del registro.

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.dsClientesDataChange(
  Sender: TObject; Field: TField);
begin
    if (Field = nil) and (FEstadoSQL = EstadoVisualizar) then begin
        lblCliente.Caption			:= qryClientes.FieldByName('NombreCliente').AsString;
        peNumeroDocumento.Text		:= qryClientes.FieldByName('NumeroDocumento').AsString;
        vcbConcesionaria.ItemIndex	:= vcbConcesionaria.Items.IndexOfValue(IIf(qryClientes.FieldByName('CodigoConcesionaria').AsString = EmptyStr, 0, qryClientes.FieldByName('CodigoConcesionaria').AsString));
        deFechaInicio.Date			:= qryClientes.FieldByName('FechaInicioVigencia').AsDateTime;
        deFechaFin.Date				:= qryClientes.FieldByName('FechaFinVigencia').AsDateTime;
    end;
    
end;

{--------------------------------------------------------------------
            FormClose

    Author      : mbecerra
    Date        : 01-Febrero-2011
    Description :       (Ref Fase 2)
                    Quita la ventana de memorias.

------------------------------------------------------------------------}
procedure TABMClientesAFacturarOtrasConcesionariasForm.FormClose(
  Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
