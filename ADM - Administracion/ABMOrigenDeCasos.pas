{-----------------------------------------------------------------------------
 Unit Name: ABMOrigenDeCasos
 Author:  CFU
 Description: ABM de Origen de Casos
 	La tabla ese llama FuentesReclamo
-----------------------------------------------------------------------------}
unit ABMOrigenDeCasos;

interface

uses
  //ABM Origen de Casos
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants;

type
  TFABMOrigenDeCasos = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    txtDescripcion: TEdit;
    tblOrigenCasos: TADOTable;
    dsOrigenDeCasos: TDataSource;
    spActualizarFuentesReclamo: TADOStoredProc;
    spEliminarFuentesReclamo: TADOStoredProc;
    ListaEntidades: TAbmList;
    Ltipo: TLabel;
    txtNombre: TEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure FormShow(Sender: TObject);
   	procedure ListaEntidadesRefresh(Sender: TObject);
   	function  ListaEntidadesProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaEntidadesDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaEntidadesClick(Sender: TObject);
	procedure ListaEntidadesInsert(Sender: TObject);
	procedure ListaEntidadesEdit(Sender: TObject);
	procedure ListaEntidadesDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMOrigenDeCasos: TFABMOrigenDeCasos;

resourcestring
	STR_MAESTRO_Origenes	= 'Maestro de Or�genes de Casos';


implementation

{$R *.DFM}

function TFABMOrigenDeCasos.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblOrigenCasos]) then exit;
	Notebook.PageIndex := 0;
	ListaEntidades.Reload;
	Result := True;
end;

procedure TFABMOrigenDeCasos.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
  txtNombre.clear;
end;

procedure TFABMOrigenDeCasos.FormShow(Sender: TObject);
begin
	ListaEntidades.Reload;
end;

procedure TFABMOrigenDeCasos.ListaEntidadesRefresh(Sender: TObject);
begin
	 if ListaEntidades.Empty then LimpiarCampos;
end;

procedure TFABMOrigenDeCasos.Volver_Campos;
begin
	ListaEntidades.Estado := Normal;
	ListaEntidades.Enabled:= True;

	ActiveControl       := ListaEntidades;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaEntidades.Reload;
end;


procedure TFABMOrigenDeCasos.ListaEntidadesDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoFuenteReclamo').AsInteger, 10));
    TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('NombreOrigenDeCaso').AsString));
		TextOut(Cols[2], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFABMOrigenDeCasos.ListaEntidadesClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		  := FieldByName('CodigoFuenteReclamo').AsInteger;
	   txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
     txtNombre.Text       := FieldByname('NombreOrigenDeCaso').AsString;
     //TASK_052_FSI_201703 :Los elementos NO Editables, deben estar bloqueados.
     txtCodigo.Enabled         := FieldbyName('Editable').Value;
     txtDescripcion.Enabled    := FieldbyName('Editable').Value;
     txtNombre.Enabled         := FieldbyName('Editable').Value;
	end;
end;

function TFABMOrigenDeCasos.ListaEntidadesProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

procedure TFABMOrigenDeCasos.ListaEntidadesInsert(Sender: TObject);
begin
  LimpiarCampos;
  groupb.Enabled          	:= True;
  ListaEntidades.Enabled 		:= False;
  Notebook.PageIndex      	:= 1;
  ActiveControl           	:= txtNombre;
end;
//TASK_052_FSI_201703 : se agregan filtro de editar o no.
procedure TFABMOrigenDeCasos.ListaEntidadesEdit(Sender: TObject);
begin

  if(txtNombre.Enabled) then begin
    ListaEntidades.Enabled:= False;
    ListaEntidades.Estado := modi;

    Notebook.PageIndex := 1;

    groupb.Enabled     := True;
    ActiveControl:= txtDescripcion
    end
  else begin

    MsgBoxErr('No se puede editar dato maestro: '+txtNombre.Text,txtDescripcion.Text, Format(MSG_CAPTION_EDITAR,[STR_MAESTRO_Origenes]), MB_ICONSTOP);
    Volver_Campos;
    Screen.Cursor:= crDefault;
    ListaEntidades.Reload;
    ListaEntidades.Estado := Normal;
    ListaEntidades.Enabled:= True;
  end;



end;
//TASK_052_FSI_201703 : se agregan filtro de eliminar o no.
procedure TFABMOrigenDeCasos.ListaEntidadesDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
  //TASK_052_FSI_201703 : Los elementos NO Editables, tampoco se pueden eliminar.
  if(tblOrigenCasos.FieldbyName('Editable').Value)
    then begin
      If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_Origenes]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try
                with spEliminarFuentesReclamo, Parameters do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoFuenteReclamo').Value := tblOrigenCasos.FieldbyName('CodigoFuenteReclamo').Value;
                    ExecProc;
                end;
        Except
          On E: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_Origenes]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_Origenes]), MB_ICONSTOP);
          end
        end
      end;
    end
  else MsgBoxErr('No se puede eliminar dato maestro: '+tblOrigenCasos.FieldbyName('Descripcion').Value,tblOrigenCasos.FieldbyName('Descripcion').Value, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_Origenes]), MB_ICONSTOP);

	ListaEntidades.Reload;
	ListaEntidades.Estado := Normal;
	ListaEntidades.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFABMOrigenDeCasos.BtnAceptarClick(Sender: TObject);

begin

      if not ValidateControls([txtDescripcion, txtNombre],
        [(Trim(txtDescripcion.Text) <> ''), (Trim(txtNombre.Text) <> '')],
        Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_Origenes]),
        [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
        Exit;
      end;

      with ListaEntidades do begin
        Screen.Cursor := crHourGlass;
        try
          try
            with spActualizarFuentesReclamo, Parameters do begin
                Parameters.Refresh;
                ParamByName('@CodigoFuenteReclamo').Value	:= txtCodigo.Value;
                ParamByName('@Nombre').Value				:= Trim(txtNombre.Text);
                ParamByName('@Descripcion').Value			:= Trim(txtDescripcion.Text);
                ParamByName('@Usuario').Value				:= UsuarioSistema;
                ExecProc;
            end;
          except
            On E: EDataBaseError do begin
              MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_Origenes]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_Origenes]), MB_ICONSTOP);
            end;
          end;
        finally
                Volver_Campos;
                Screen.Cursor:= crDefault;
                Reload;
        end;
      end;

end;

procedure TFABMOrigenDeCasos.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMOrigenDeCasos.AbmToolbarClose(Sender: TObject);
begin
	 close;
end;

procedure TFABMOrigenDeCasos.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMOrigenDeCasos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

end.

