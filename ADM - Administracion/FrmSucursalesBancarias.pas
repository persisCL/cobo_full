{********************************** File Header *********************************
File Name   : FrmMaestroPersonal
Author      : Tinao, Diego <dtinao@dpsautomation.com>
Date Created: 04-Sep-2003
Language    : ES-AR
Description : Esta Unit realiza la administraci�n del Maestro de Personal
    		  que realizar� las tareas de Mantenimiento.

Firma       : SS_1419_NDR_20151130
Descripcion : El SP ActualizarDatosPersona tiene el nuevo parametro @CodigoUsuario
              para grabarlo en los campos de auditoria de la tabla Personas y en
              HistoricoPersonas          
********************************************************************************}
unit FrmSucursalesBancarias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, Abm_obj, OleCtrls, DmiCtrls, Grids,DmConnection, ADODB,
  Variants, ComCtrls, PeaProcs, Validate, BuscaTab, MaskCombo,
  DPSControls, Peatypes, TimeEdit, Provider, DBClient, ListBoxEx, DBListEx,
  FrmEditarDomicilio, FrmEditarMedioComunicacion, DBGrids, BuscaClientes;

type
  TFormSucursalesBancarias = class(TForm)
    pnlInferior: TPanel;
	AbmToolbar1: TAbmToolbar;
    Notebook: TNotebook;
    alEntidadesBancarias: TAbmList;
    SucursalesBancarias: TADOTable;
    pcDatosPersonal: TPageControl;
    tsCliente: TTabSheet;
    tsDomicilio: TTabSheet;
    tsContacto: TTabSheet;
    pnlDatosPersonal: TPanel;
    lblApellido: TLabel;
    lblFechaNacCreacion: TLabel;
    dtFechaNacimiento: TDateEdit;
    txtApellido: TEdit;
    lblNombre: TLabel;
    txtNombre: TEdit;
    Label7: TLabel;
    lblSexo: TLabel;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    cbPersoneria: TComboBox;
    Label23: TLabel;
    txtApellidoMaterno: TEdit;
    lblApellidoMaterno: TLabel;
    cbSexo: TComboBox;
    cbSituacionIVA: TComboBox;
    ObtenerMediosComunicacionPersona: TADOStoredProc;
    ActualizarDatosBancoSucursal: TADOStoredProc;
    Label2: TLabel;
    cbLugarNacimiento: TComboBox;
    Label11: TLabel;
    pnlDomicilio: TPanel;
    dblDomicilios: TDBListEx;
    btnAgregarDomicilio: TDPSButton;
    btnEditarDomicilio: TDPSButton;
    btnEliminarDomicilio: TDPSButton;
    QueryTemp: TADOQuery;
    dsMediosComunicacion: TDataSource;
    cdsMediosComunicacion: TClientDataSet;
    dspMediosComunicacion: TDataSetProvider;
    pnlMediosContacto: TPanel;
    dblMediosComunicacion: TDBListEx;
    btnAgregarMedioComunicacion: TDPSButton;
    btnEditarMedioComunicacion: TDPSButton;
    btnEliminarMedioComunicacion: TDPSButton;
    cdsMediosComunicacionCodigoTipoOrigenDato: TSmallintField;
    cdsMediosComunicacionCodigoMedioContacto: TSmallintField;
    cdsMediosComunicacionDescriMedioContacto: TStringField;
    cdsMediosComunicacionTipoMedio: TStringField;
    cdsMediosComunicacionDescriTipoOrigenDato: TStringField;
    cdsMediosComunicacionValor: TStringField;
    cdsMediosComunicacionCodigoDomicilio: TIntegerField;
    cdsMediosComunicacionDescriDomicilio: TStringField;
    cdsMediosComunicacionDetalle: TStringField;
    cdsMediosComunicacionHorarioDesde: TDateTimeField;
    cdsMediosComunicacionHorarioHasta: TDateTimeField;
    cdsMediosComunicacionObservaciones: TStringField;
    cdsMediosComunicacionCodigoDomicilioRecNo: TIntegerField;
    EliminarBancoSucursal: TADOStoredProc;
    Panel3: TPanel;
    Label1: TLabel;
    ObtenerDomiciliosPersona: TADOStoredProc;
    dsDomicilios: TDataSource;
    cdsDomicilios: TClientDataSet;
    dspDomicilios: TDataSetProvider;
    ActualizarDatosPersona: TADOStoredProc;
    ActualizarMedioComunicacionPersona: TADOStoredProc;
    ObtenerDatosPersona: TADOStoredProc;
    EliminarDomiciliosPersona: TADOStoredProc;
    EliminarTodosDomiciliosPersona: TADOStoredProc;
    ActualizarDomicilio: TADOStoredProc;
    ActualizarDomicilioEntregaPersona: TADOStoredProc;
    ActualizarDomicilioRelacionado: TADOStoredProc;
    btnBuscar: TBitBtn;
    chkActivo: TCheckBox;
    pnlBancos: TPanel;
    cbBancos: TComboBox;
    Label3: TLabel;
    cdsDomiciliosCodigoDomicilio: TIntegerField;
    cdsDomiciliosCodigoPersona: TIntegerField;
    cdsDomiciliosCodigoTipoOrigenDato: TSmallintField;
    cdsDomiciliosDescriTipoOrigenDato: TStringField;
    cdsDomiciliosCodigoCalle: TIntegerField;
    cdsDomiciliosNumero: TIntegerField;
    cdsDomiciliosPiso: TSmallintField;
    cdsDomiciliosDpto: TStringField;
    cdsDomiciliosCalleDesnormalizada: TStringField;
    cdsDomiciliosDetalle: TStringField;
    cdsDomiciliosCodigoPostal: TStringField;
    cdsDomiciliosCodigoTipoEdificacion: TSmallintField;
    cdsDomiciliosDescriTipoEdificacion: TStringField;
    cdsDomiciliosDomicilioEntrega: TBooleanField;
    cdsDomiciliosCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNormalizado: TIntegerField;
    cdsDomiciliosCodigoTipoCalle: TSmallintField;
    cdsDomiciliosDescriCalle: TStringField;
    cdsDomiciliosCodigoPais: TStringField;
    cdsDomiciliosDescriPais: TStringField;
    cdsDomiciliosCodigoRegion: TStringField;
    cdsDomiciliosDescriRegion: TStringField;
    cdsDomiciliosCodigoComuna: TStringField;
    cdsDomiciliosDescriComuna: TStringField;
    cdsDomiciliosCodigoCiudad: TStringField;
    cdsDomiciliosDescriCiudad: TStringField;
    cdsDomiciliosDireccionCompleta: TStringField;
    cdsDomiciliosDomicilioCompleto: TStringField;
    cbTipoDocumento: TComboBox;
    mcTipoNumeroDocumento: TEdit;
	procedure AbmToolbar1Close(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnCancelarClick(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
    procedure alEntidadesBancariasClick(Sender: TObject);
    procedure alEntidadesBancariasDelete(Sender: TObject);
    procedure alEntidadesBancariasDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure alEntidadesBancariasEdit(Sender: TObject);
    procedure alEntidadesBancariasInsert(Sender: TObject);
    procedure alEntidadesBancariasRefresh(Sender: TObject);
    procedure cbPersoneriaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Agregar(Sender: TObject);
    procedure btnEliminarDomicilioClick(Sender: TObject);
    procedure btnEditarDomicilioClick(Sender: TObject);
    procedure dblDomiciliosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblMediosComunicacionDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnEliminarMedioComunicacionClick(Sender: TObject);
    procedure btnAgregarMedioComunicacionClick(Sender: TObject);
    procedure btnEditarMedioComunicacionClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure cbBancosChange(Sender: TObject);
    procedure mcTipoNumeroDocumentoKeyPress(Sender: TObject;
      var Key: Char);

  private
	{ Private declarations }
    FCodigoBanco: integer;
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
	procedure Volver_Campos;
    procedure HabilitarCamposPersoneria(Personeria: Char);
    procedure HabilitarBotonesDomicilios;
    procedure HabilitarBotonesMediosComunicacion;
    procedure CargarMediosComunicacionPersona(CodigoPersona: integer; TipoOrigenDato: integer);
    procedure CargarDomiciliosPersona(CodigoPersona: integer; TipoOrigenDato: integer);
    procedure CargarDatosPersona(CodigoPersona: integer);
  public
	{ Public declarations }
	Function Inicializar(MDIChild: Boolean = True; CodigoBanco: integer = 0): Boolean;

  end;

var
    FormSucursalesBancarias:    TFormSucursalesBancarias;
    CodigoPersonaActual:    integer;
    RecNoDomicilioEntrega:  integer;
    CodigoDomicilioEntrega: integer;

implementation
{$R *.DFM}


procedure TFormSucursalesBancarias.CargarMediosComunicacionPersona(CodigoPersona: integer; TipoOrigenDato: integer);
begin
    //Primero intento abrir el Stored Procedure
    ObtenerMediosComunicacionPersona.Close;
    ObtenerMediosComunicacionPersona.Parameters.ParamByName('@CodigoPersona').Value         := CodigoPersona;
    ObtenerMediosComunicacionPersona.Parameters.ParamByName('@CodigoTipoOrigenDato').Value  := iif(TipoOrigenDato < 1, null, TipoOrigenDato);
    if not OpenTables([ObtenerMediosComunicacionPersona]) then Exit;
    cdsMediosComunicacion.Data := dspMediosComunicacion.Data;
    ObtenerMediosComunicacionPersona.Close;
    HabilitarBotonesMediosComunicacion;
end;


procedure TFormSucursalesBancarias.HabilitarBotonesDomicilios;
begin
    btnEditarDomicilio.Enabled      := iif( cdsDomicilios.Active, cdsDomicilios.RecordCount > 0, False);
    btnEliminarDomicilio.Enabled    := btnEditarDOmicilio.Enabled;
end;

procedure TFormSucursalesBancarias.HabilitarBotonesMediosComunicacion;
begin
    btnEditarMedioComunicacion.Enabled      := iif( cdsMediosComunicacion.Active, cdsMediosComunicacion.RecordCount > 0, False);
    btnEliminarMedioComunicacion.Enabled    := btnEditarMedioComunicacion.Enabled;
end;

procedure TFormSucursalesBancarias.CargarDomiciliosPersona(CodigoPersona: integer; TipoOrigenDato: integer);
begin
    //Primero intento abrir el Stored Procedure
    RecNoDomicilioEntrega := -1;
    ObtenerDomiciliosPersona.Close;
    ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoPersona').Value         := CodigoPersona;
    ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoTipoOrigenDato').Value  := iif(TipoOrigenDato < 1, null, TipoOrigenDato);
    if not OpenTables([ObtenerDomiciliosPersona]) then Exit;
    while not ObtenerDomiciliosPersona.Eof do begin
        if ObtenerDomiciliosPersona.FieldByName('DomicilioEntrega').AsBoolean then begin
            RecNoDomicilioEntrega := ObtenerDomiciliosPersona.RecNo;
            break;
        end;
        ObtenerDomiciliosPersona.Next;
    end;
    ObtenerDomiciliosPersona.First;
    cdsDomicilios.Data := dspDomicilios.Data;
    ObtenerDomiciliosPersona.Close;
    HabilitarBotonesDomicilios;
end;

procedure TFormSucursalesBancarias.HabilitarCamposPersoneria(Personeria: char);
ResourceString
    LBL_PERSONA_RAZON_SOCIAL        = 'Raz�n Social';
    LBL_PERSONA_NOMBRE_FANTASIA     = 'Nombre de Fantas�a:';
    LBL_PERSONA_SITUACION_IVA       = 'Situaci�n de IVA:';
    LBL_PERSONA_FECHA_CREACION      = 'Fecha de Creaci�n:';
    LBL_PERSONA_APELLIDO            = 'Apellido:';
    LBL_PERSONA_NOMBRE              = 'Nombre:';
    LBL_PERSONA_APELLIDO_MATERNO    = 'Apellido Materno:';
    LBL_PERSONA_CONTACTO_COMERCIAL  = 'Contacto Comercial:';
    LBL_PERSONA_SEXO                = 'Sexo:';
    LBL_PERSONA_FECHA_NACIMIENTO    = 'Fecha de Nacimiento:';

begin
    Case Personeria of
        PERSONERIA_JURIDICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_RAZON_SOCIAL;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE_FANTASIA;
                lblApellidoMaterno.Caption  := LBL_PERSONA_CONTACTO_COMERCIAL;
                lblSexo.Visible             := False;
                cbSexo.Visible              := False;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_CREACION;
            end;
        PERSONERIA_FISICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_APELLIDO;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE;
                lblApellidoMaterno.Caption  := LBL_PERSONA_APELLIDO_MATERNO;
                lblSexo.Caption             := LBL_PERSONA_SEXO;
                lblSexo.Visible             := True;
                cbSexo.Visible              := True;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_NACIMIENTO;
            end;
    end;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Volver_Campos
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Vuelve al estado Inicial los controles del Form
********************************************************************************}
procedure TFormSucursalesBancarias.Volver_Campos;
begin
    CodigoPersonaActual         := -1;
    btnBuscar.Visible           := False;
	alEntidadesBancarias.Estado := Normal;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex		    := 0;
	alEntidadesBancarias.Reload;
	alEntidadesBancarias.Refresh;
//	alEntidadesBancarias.SetFocus;
    Screen.Cursor               := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Inicializar
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    MDIChild: Boolean = True
  Return Value:  Boolean
  Description:   Inicializa el Form de Maestro de Personal. Primero determina el
    	formato en que se va a mostrar la pantalla seg�n el par�metro MDIChild y
    	luego intenta abrir la tabla "MaestroPersonal"
********************************************************************************}
function TFormSucursalesBancarias.Inicializar(MDIChild: Boolean = True; CodigoBanco: integer = 0): Boolean;
ResourceString
    LBL_FORM_CAPTION = 'Sucursales del Banco %s';
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result          := False;
    CargarBancos(DMConnections.BaseCAC, cbBancos, CodigoBanco);
    FCodigoBanco    := CodigoBanco;
    
    if CodigoBanco > 0 then begin
        pnlBancos.Visible               := False;
        SucursalesBancarias.Filtered    := True;
        Caption                         := Format(LBL_FORM_CAPTION, [Trim(StrLeft( cbBancos.Text, 40))]);
    end else begin
        pnlBancos.Visible               := True;
        SucursalesBancarias.Filtered    := False;
        SucursalesBancarias.Filter      := '';
        if cbBancos.Items.Count > 0 then begin
            cbBancos.ItemIndex := 0;
            FCodigoBanco := Ival(StrRight(cbBancos.text, 10));
        end;
    end;

    SucursalesBancarias.Filtered    := True;
    SucursalesBancarias.Filter := 'CodigoBanco = ' + intToStr(FCodigoBanco);

    if not OpenTables([SucursalesBancarias]) then Exit;

    CargarSexos(cbSexo);
    CargarTipoSituacionIVA(DMConnections.BaseCAC, cbSituacionIVA);
    CargarPersoneria(cbPersoneria, PERSONERIA_JURIDICA);
    cbPersoneria.OnChange(cbPersoneria);
    CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento);
  //	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento);
    pcDatosPersonal.ActivePageIndex := 0;
    if SucursalesBancarias.RecordCount > 0 then cdsDomicilios.CreateDataSet;
    Volver_Campos;    
	Result := True;
end;



{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.Limpiar_Campos
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Limpia los valores que figuran en los controles.
********************************************************************************}
procedure TFormSucursalesBancarias.Limpiar_Campos;
begin
    //Datos De la Persona
    cbPersoneria.ItemIndex := 0;
    CargarPersoneria(cbPersoneria, PERSONERIA_JURIDICA);
    cbPersoneria.OnChange(cbPersoneria);
    txtNombre.Clear;
    txtApellido.Clear;
    txtApellidoMaterno.Clear;
    //mcTipoNumeroDocumento.MaskText  := '';
    cbSexo.ItemIndex                := 0;
    cbSituacionIVA.ItemIndex        := 0;
    CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento);
    dtFechaNacimiento.Clear;
    chkActivo.Checked	            := False;

    CargarDomiciliosPersona(-1, 0);
    CargarMediosComunicacionPersona(-1, 0);

end;


procedure TFormSucursalesBancarias.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;


procedure TFormSucursalesBancarias.BtnSalirClick(Sender: TObject);
begin
	Close;
end;


procedure TFormSucursalesBancarias.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	Action := caFree;
end;


procedure TFormSucursalesBancarias.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;






{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.BtnAceptarClick
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Inserta o Modifica los datos de un Empleado utilizando el
  	procedimiento almacenado "ActualizarMaestroPersonal".
    Antes de Realizar dichas operaciones verifica que esten completos los
    compos considerados obligatorios.
********************************************************************************}
procedure TFormSucursalesBancarias.BtnAceptarClick(Sender: TObject);
ResourceString
	MSG_ACTUALIZAR_CAPTION   = 'Actualizar Sucursal Bancaria';
    MSG_ACTUALIZAR_ERROR	    = 'No se pudieron actualizar los datos de la Sucursal Bancaria';

	MSG_ACTUALIZAR_DOMICILIO_CAPTION            = 'Actualizar Datos del Domicilio';
    MSG_ACTUALIZAR_DOMICILIO_ERROR	            = 'No se pudieron actualizar los datos del Domicilio';

	MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION   = 'Actualizar Medio de Contacto';
    MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR	    = 'No se pudieron actualizar los datos del Medio de Contacto';

	MSG_ACTUALIZAR_BANCO_SUCURSAL_CAPTION  = 'Actualizar Datos de la Sucursal Bancaria';
    MSG_ACTUALIZAR_BANCO_SUCURSAL_ERROR	   = 'No se pudieron actualizar los datos de la Sucursal Bancaria';

    MSG_VALIDAR_CAPTION 		= 'Validar datos de la Sucursal Bancaria';
    MSG_VALIDAR_APELLIDO		= 'Debe ingresar un Apellido para detallar la Sucursal Bancaria';
    MSG_VALIDAR_NOMBRE			= 'Debe ingresar un Nombre para detallar la Sucursal Bancaria';
    MSG_VALIDAR_SEXO            = 'Debe indicar el g�nero para detallar la Sucursal Bancaria';
    MSG_VALIDAR_SITUACION_IVA   = 'Debe indicar la Situaci�n con Impositiva de la Sucursal Bancaria';
    MSG_VALIDAR_RUT             = 'Debe ingresar un C�digo de Documento detallar la Sucursal Bancaria';

    MSG_VALIDAR_CANTIDAD_DOMICILIOS         = 'Debe ingresar al menos un domicilio';
    MSG_VALIDAR_CANTIDAD_MEDIOS             = 'Debe ingresar al menos un medio de contacto';
    MSG_ERROR_DOMICILIO_RELACIONADO         = 'Actualizar Domicilio Realacionado';
    MSG_CAPTION_DOMICILIO_RELACIONADO       = 'No se pudo actualizar el domicilio relacionado';
    MSG_VALIDAR_DOMICILIOS_RELACIONADOS     = 'Debe especificar al menos un domicilio relacionado';
var
    TodoOk: Boolean;
    CodigosEliminar, CodigosNoEliminar: AnsiString;
    ListaDomicilios: TStringList;
    i, FCodigoDomicilio: integer;
begin
	if (Trim(txtApellido.Text) = '') then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_APELLIDO, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtApellido);
    	Exit;
    end;

	if (Trim(txtNombre.Text) = '') then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_NOMBRE, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtNombre);
    	Exit;
    end;

    if (StrRight(cbPersoneria.Text, 1) = PERSONERIA_FISICA) and
        (cbSexo.ItemIndex < 0) then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_SEXO, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbSexo);
    	Exit;
    end;

    if (StrRight(cbPersoneria.Text, 1) = PERSONERIA_JURIDICA) and
        (cbSituacionIVA.ItemIndex < 0) then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon( MSG_VALIDAR_SITUACION_IVA, MSG_VALIDAR_CAPTION, MB_ICONSTOP, cbSituacionIVA);
    	Exit;
    end;
(* todo: Leonardo  //  Modificacion: Leonardo
 //   if (Trim(mcTipoNumeroDocumento.MaskText) = '') or not ( mcTipoNumeroDocumento.ValidateMask()) then begin *)
 //   if  (Trim(txtTipoNumeroDocumento.Text) = '') then begin
 //   	pcDatosPersonal.ActivePageIndex := 0;
 //		//MsgBoxBalloon( MSG_VALIDAR_RUT, MSG_VALIDAR_CAPTION, MB_ICONSTOP, mcTipoNumeroDocumento);
 //       MsgBoxBalloon( MSG_VALIDAR_RUT, MSG_VALIDAR_CAPTION, MB_ICONSTOP, txtTipoNumeroDocumento);
 //   	Exit;
 //   end;

	if (dblDomicilios.DataSource.DataSet.RecordCount = 0) then begin
    	pcDatosPersonal.ActivePageIndex := 1;
		MsgBoxBalloon( MSG_VALIDAR_CANTIDAD_DOMICILIOS, MSG_VALIDAR_CAPTION, MB_ICONSTOP, dblDomicilios);
    	Exit;
    end;

	if (dblMediosComunicacion.DataSource.DataSet.RecordCount = 0) then begin
    	pcDatosPersonal.ActivePageIndex := 2;
		MsgBoxBalloon( MSG_VALIDAR_CANTIDAD_MEDIOS, MSG_VALIDAR_CAPTION, MB_ICONSTOP, dblMediosComunicacion);
    	Exit;
    end;

    TodoOK          := False;
    ListaDomicilios := TStringList.Create;

	//HASTA ACA TODO BIEN
	Screen.Cursor   := crHourGlass;
    DMConnections.BaseCAC.BeginTrans;
    try
        try
            ActualizarDatosPersona.Close;
            With ActualizarDatosPersona.Parameters do begin

                ParamByName('@Personeria').Value        := iif( cbPersoneria.ItemIndex < 0, null,  Trim(StrRight( cbPersoneria.Text, 1)));
                ParamByName('@Nombre').Value			:= iif(Trim(txtNombre.Text)= '', null, Trim(txtNombre.Text));
                ParamByName('@Apellido').Value			:= iif(Trim(txtApellido.Text)= '', null, Trim(txtApellido.Text));

                if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
                    ParamByName('@ApellidoMaterno').Value	    := iif(Trim(txtApellidoMaterno.Text)= '', null, Trim(txtApellidoMaterno.Text));
                    ParamByName('@Sexo').Value 				    := iif( cbSexo.ItemIndex < 0, null, StrRight( Trim(cbSexo.Text), 1));
                    ParamByName('@ContactoComercial').Value	    := null;
                end else if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_JURIDICA then begin
                    ParamByName('@ApellidoMaterno').Value	    := null;
                    ParamByName('@Sexo').Value 				    := null;
                    ParamByName('@ContactoComercial').Value	    := iif(Trim(txtApellidoMaterno.Text)= '', null, Trim(txtApellidoMaterno.Text));
                end;

//                ParamByName('@CodigoDocumento').Value	    := Trim(StrRight(mcTipoNumeroDocumento.ComboText, 10));
//                ParamByName('@NumeroDocumento').Value	    := iif(Trim(txtTipoNumeroDocumento.Text)= '', null, Trim(txtTipoNumeroDocumento.Text));//iif(Trim(mcTipoNumeroDocumento.MaskText)= '', null, Trim(mcTipoNumeroDocumento.MaskText));

                ParamByName('@LugarNacimiento').Value 	    := iif( cbLugarNacimiento.ItemIndex < 0, null,  Trim( StrRight(cbLugarNacimiento.Text, 10)));
                ParamByName('@FechaNacimiento').Value 	    := iif( dtFechaNAcimiento.date < 0, null, dtFechaNacimiento.Date);
                ParamByName('@CodigoSituacionIVA').Value    := iif( cbSituacionIVA.ItemIndex < 0, null,  Trim(StrRight( cbSituacionIVA.Text, 2)));
//                ParamByName('@CodigoActividad').Value 	    := iif( cbActividades.ItemIndex < 0, null,  Ival(StrRight( Trim(cbActividades.Text), 10)));
                ParamByName('@CodigoPersona').Value 	    := CodigoPersonaActual;
                ParamByName('@CodigoUsuario').Value 	    := UsuarioSistema;          //SS_1419_NDR_20151130
            end;
            ActualizarDatosPersona.ExecProc;
            CodigoPersonaActual :=  ActualizarDatosPersona.Parameters.ParamByName('@CodigoPersona').Value;
            ActualizarDatosPersona.Close;
        except
            On E: Exception do begin
                ActualizarDatosPersona.Close;
                MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;

        //Ahora grabo los domicilios
        //  Borrar todos los domicilios que figuran en la base que ya no pertenecen al cliente.
        if  alEntidadesBancarias.Estado = Modi then begin

            cdsDomicilios.First;
            CodigosNoEliminar := '';
            while not cdsDomicilios.Eof do begin
                if (dblDomicilios.DataSource.DataSet.FieldByName('CodigoDomicilio').AsInteger > 0) then begin
                    CodigosNoEliminar := CodigosNoEliminar + ',' + Trim(IntToStr(dblDomicilios.DataSource.DataSet.FieldByName('CodigoDomicilio').AsInteger));
                end;
                cdsDomicilios.Next;
            end;
            CodigosNoEliminar := Trim(Copy(CodigosNoEliminar, 2, length(CodigosNoEliminar)));
            CodigosNoEliminar := '(' + CodigosNoEliminar + ')';
            if CodigosNoEliminar <> '()' then begin
                QueryTemp.Close;
                QueryTemp.SQL.Text :=
                    'SELECT CodigoDomicilio FROM Domicilios ' +
                    ' WHERE CodigoPersona = ' + IntToStr(CodigoPersonaActual) +
                    ' AND CodigoDomicilio not in ' + CodigosNoEliminar;
                QueryTemp.open;

                if QueryTemp.RecordCount > 0 then begin
                    CodigosEliminar := '';
                    While not QueryTemp.Eof do begin
                        CodigosEliminar := CodigosEliminar + ',' + Trim(IntToStr(QueryTemp.FieldByName('CodigoDomicilio').AsInteger));
                        QueryTemp.Next;
                    end;
                    CodigosEliminar := Trim(Copy(CodigosEliminar, 2, length(CodigosEliminar)));

                    try
                        with EliminarDomiciliosPersona.Parameters do begin
                            ParamByName('@ListaDomicilios').Value := CodigosEliminar;
                        end;
                        EliminarDomiciliosPersona.ExecProc;
                        EliminarDomiciliosPersona.Close;
                    except
                        On E: Exception do begin
                            EliminarDomiciliosPersona.Close;
                            MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                            Screen.Cursor := crDefault;
                            Exit;
                        end;
                    end;
                end;
                QueryTemp.Close;
            end else begin
                try
                    EliminarTodosDomiciliosPersona.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersonaActual;
                    EliminarTodosDomiciliosPersona.ExecProc;
                    EliminarTodosDomiciliosPersona.Close;
                except
                    On E: Exception do begin
                        EliminarTodosDomiciliosPersona.Close;
                        MsgBoxErr( MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                        Screen.Cursor := crDefault;
                        Exit;
                    end;
                end;
            end;
        end;

        //Ahora grabo los domicilios que quedaron
        CodigoDomicilioEntrega := -1;
        cdsDomicilios.First;
        While not cdsDomicilios.Eof do begin
            try
                with ActualizarDomicilio.Parameters do begin
                	ParamByName('@CodigoPersona').Value         := CodigoPersonaActual;
                	ParamByName('@CodigoTipoOrigenDato').Value  := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
                	ParamByName('@CodigoTipoEdificacion').Value := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
					ParamByName('@CodigoCalle').Value           := iif( cdsDomicilios.FieldByName('CodigoCalle').Asinteger < 1, null, cdsDomicilios.FieldByName('CodigoCalle').Asinteger);
					ParamByName('@CalleDesnormalizada').Value   := iif( Trim(cdsDomicilios.FieldByName('CalleDesnormalizada').AsString) = '', null, Trim(cdsDomicilios.FieldByName('CalleDesnormalizada').AsString));
					ParamByName('@Numero').Value                := cdsDomicilios.FieldByName('Numero').Asinteger;
					ParamByName('@Piso').Value                  := iif( cdsDomicilios.FieldByName('piso').Asinteger < 1, null, cdsDomicilios.FieldByName('piso').Asinteger);;
					ParamByName('@Depto').Value                 := iif( Trim(cdsDomicilios.FieldByName('Dpto').AsString) = '', null, Trim(cdsDomicilios.FieldByName('Dpto').AsString));
					ParamByName('@CodigoPostal').Value          := iif( Trim(cdsDomicilios.FieldByName('CodigoPostal').AsString) = '', null, Trim(cdsDomicilios.FieldByName('CodigoPostal').AsString));
					ParamByName('@Detalle').Value               := iif( Trim(cdsDomicilios.FieldByName('Detalle').AsString) = '', null, Trim(cdsDomicilios.FieldByName('Detalle').AsString));
                	ParamByName('@CodigoDomicilio').Value       := iif(cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger < 1, null, cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger);
                end;
                ActualizarDomicilio.ExecProc;

                ListaDomicilios.Add(IntToStr(cdsDomicilios.RecNo) + Space(200) + IntToStr(ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value));

                if RecNoDomicilioEntrega = cdsDomicilios.RecNo then CodigoDomicilioEntrega :=  ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
                ActualizarDomicilio.Close;
            except
                On E: Exception do begin
                    ActualizarDomicilio.Close;
                    MsgBoxErr( MSG_ACTUALIZAR_DOMICILIO_ERROR, e.message, MSG_ACTUALIZAR_DOMICILIO_CAPTION, MB_ICONSTOP);
                    Screen.Cursor := crDefault;
                    Exit;
                end;
            end;
            cdsDomicilios.Next;
        end;
        //Fin Domicilios.

        //Actualizo en los datos de la persona el Codigo de Domicilio de Entrega.
        try
            with ActualizarDomicilioEntregaPersona.Parameters do begin
                ParamByName('@CodigoPersona').Value             := CodigoPersonaActual;
                ParamByName('@CodigoDomicilioEntrega').Value    := iif(CodigoDomicilioEntrega < 1, null, CodigoDomicilioEntrega);
            end;
            ActualizarDomicilioEntregaPersona.ExecProc;
            ActualizarDomicilioEntregaPersona.Close;
        except
            On E: Exception do begin
                ActualizarDomicilioEntregaPersona.Close;
                MsgBoxErr( MSG_ACTUALIZAR_DOMICILIO_ERROR, e.message, MSG_ACTUALIZAR_DOMICILIO_CAPTION, MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;


        //Ahora grabo los Medios de Contacto
        QueryExecute( DMConnections.BaseCAC, 'DELETE FROM MediosComunicacion WHERE CodigoPersona = ' + IntToStr(CodigoPersonaActual));

        cdsMediosComunicacion.First;
        While not cdsMediosComunicacion.Eof do begin
            try
                FCodigoDomicilio := -1;
                if (Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) = '') then begin
                    // estoy grabando un Medio de Contacto con Domicilio.
                    FCodigoDomicilio := cdsMediosComunicacion.fieldByName('CodigoDomicilio').AsInteger;
                    if (FCodigoDomicilio < 0) then begin
                        for i:= 0 to ListaDomicilios.Count -1 do begin
                            if Ival(StrLeft(Trim(ListaDomicilios.Strings[i]), 20)) = cdsMediosComunicacion.FieldByName('CodigoDomicilioRecNo').AsInteger then begin
                                FCodigoDomicilio := Ival(StrRight(Trim(ListaDomicilios.Strings[i]), 20));
                                break;
                            end;
                        end;
                    end;
                end;

                if (FCodigoDomicilio > 0) or (Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) <> '') then begin
                    with ActualizarMedioComunicacionPersona.Parameters do begin
                        ParamByName('@CodigoPersona').Value         := CodigoPersonaActual;
                        ParamByName('@CodigoMedioContacto').Value   := cdsMediosComunicacion.fieldByName('CodigoMedioContacto').AsInteger;
                        ParamByName('@CodigoTipoOrigenDato').Value  := cdsMediosComunicacion.fieldByName('CodigoTipoOrigenDato').AsInteger;
                        ParamByName('@Valor').Value                 := iif( Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) = '', null, Trim(cdsMediosComunicacion.fieldByName('Valor').AsString));
                        ParamByName('@CodigoDomicilio').Value       := iif( FCodigoDomicilio < 1, null, FCodigoDomicilio);
                        ParamByName('@HorarioDesde').Value          := iif( cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime <= nulldate, null, cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime);
                        ParamByName('@HorarioHasta').Value          := iif( cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime <= nulldate, null, cdsMediosComunicacion.fieldByName('HorarioHasta').AsDateTime);
                        ParamByName('@Observaciones').Value         := iif( Trim(cdsMediosComunicacion.fieldByName('Observaciones').AsString) = '', null, Trim(cdsMediosComunicacion.fieldByName('Observaciones').AsString));
                    end;
                    ActualizarMedioComunicacionPersona.ExecProc;
                    ActualizarMedioComunicacionPersona.Close;
                end;
            except
                on E: exception do begin
                    MsgBoxErr( MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR, e.message, MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION, MB_ICONSTOP);
                    ActualizarMedioComunicacionPersona.Close;
                    Screen.Cursor := crDefault;
                    Exit;
                end;
            end;
            cdsMediosComunicacion.Next;
        end;


        //Ahora grabo los datos propios de la Sucursal
        try
            with ActualizarDatosBancoSucursal.Parameters do begin
                ParamByName('@CodigoPersona').Value             := CodigoPersonaActual;
                ParamByName('@CodigoBanco').Value               := FCodigoBanco;
                ParamByName('@Activo').Value                    := chkActivo.Checked;
            end;
            ActualizarDatosBancoSucursal.ExecProc;
            ActualizarDatosBancoSucursal.Close;
        except
            On E: Exception do begin
                ActualizarDatosBancoSucursal.Close;
                MsgBoxErr( MSG_ACTUALIZAR_BANCO_SUCURSAL_ERROR, e.message, MSG_ACTUALIZAR_BANCO_SUCURSAL_CAPTION, MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;
        //Fin Datos Sucursal

        CodigoPersonaActual := -1;

        TodoOK := True;
    finally
        if TodoOK then DMConnections.BaseCAC.CommitTrans
        else DMConnections.BaseCAC.RollbackTrans;
        FreeAndNil(ListaDomicilios);
    end;
	Volver_Campos;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalClick
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Completa los campos en pantalla utilizando la informaci�n del registro
    actual de la tabla "MaestroPersonal"
********************************************************************************}
procedure TFormSucursalesBancarias.alEntidadesBancariasClick(Sender: TObject);
begin

    if SucursalesBancarias.FieldByName('CodigoPersona').AsInteger = CodigoPersonaActual then Exit;

    SucursalesBancarias.DisableControls;

	with SucursalesBancarias do begin

        //Cargar Datos Personales
        CargarPersoneria(cbPersoneria, Trim(FieldByName('Personeria').AsString));
        cbPersoneria.OnChange(cbPersoneria);

		txtNombre.Text			    := Trim(FieldByName('Nombre').AsString);
		txtApellido.Text  		    := Trim(FieldByName('Apellido').AsString);
		txtApellidoMaterno.Text  	:= Trim(FieldByName('ApellidoMaterno').AsString);

        CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento, FieldByName('LugarNacimiento').AsString);
        dtFechaNacimiento.Date	    := FieldByName('FechaNacimiento').AsDateTime;

        if Trim(FieldByName('Personeria').AsString) = PERSONERIA_FISICA then
        	CargarSexos(cbSexo, Trim(FieldByName('Sexo').AsString))
        else if Trim(FieldByName('Personeria').AsString) = PERSONERIA_JURIDICA then
        	CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Trim(FieldByName('CodigoSituacionIVA').AsString));

(* todo: Leonardo
     	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, Trim(FieldByName('CodigoDocumento').AsString));
		mcTipoNumeroDocumento.MaskText	:= Trim(FieldByName('NumeroDocumento').AsString);
*)


//        CargarActividadesPersona(DMConnections.BaseCAC, cbActividades,  FieldByName('CodigoActividad').AsInteger);
        chkActivo.Checked		:= FieldByName('Activo').AsBoolean;

        // Cargamos los domicilios
        CargarDomiciliosPersona(FieldByName('CodigoPersona').AsInteger, 0);

        // Cargamos los Medios de Comunicacion
        CargarMediosComunicacionPersona( FieldByName('CodigoPersona').AsInteger, 0);

	end;
    SucursalesBancarias.EnableControls;
    CodigoPersonaActual := SucursalesBancarias.FieldByName('CodigoPersona').AsInteger;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalDelete
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Elimina un registro de la tabla "MaestroPersonal".
    Primero intenta eliminarlo, en caso de no ser posible debido a la integridad
    referencial, modifica el estado del registro marc�ndolo como inactivo
    mediante la ejecuci�n de un query.
********************************************************************************}
procedure TFormSucursalesBancarias.alEntidadesBancariasDelete(Sender: TObject);
resourcestring
	MSG_ELIMINAR_BANCO_SUCURSAL_CAPTION 	        = 'Dar de baja la Sucursal';
	MSG_ELIMINAR_BANCO_SUCURSAL_ERROR 		        = 'No se puede dar de baja esta Sucursal porque hay datos que dependen de ella.';
	MSG_ELIMINAR_BANCO_SUCURSAL_QUESTION	        = '�Est� seguro de querer dar de baja la Sucursal';
	MSG_ELIMINAR_BANCO_SUCURSAL_QUESTION_CAPTION    = 'Confirmaci�n...';

begin
	Screen.Cursor := crHourGlass;

    If MsgBox(MSG_ELIMINAR_BANCO_SUCURSAL_QUESTION, MSG_ELIMINAR_BANCO_SUCURSAL_CAPTION, MB_YESNO) = IDYES then begin
        try
            EliminarBancoSucursal.Parameters.ParamByName('@CodigoPersona').Value := SucursalesBancarias.FieldByName('CodigoPersona').AsInteger;
            EliminarBancoSucursal.ExecProc;
            EliminarBancoSucursal.Close;
        Except
            On E: exception do begin
                MsgBoxErr(MSG_ELIMINAR_BANCO_SUCURSAL_ERROR, E.message, MSG_ELIMINAR_BANCO_SUCURSAL_CAPTION, MB_ICONSTOP);
                EliminarBancoSucursal.Close;
            end;
        end;
    end;
	Volver_Campos;

	Screen.Cursor      := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalDrawItem
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TDBList; Tabla: TDataSet; Rect: TRect;
  				 State: TOwnerDrawState; Cols: TColPositions
  Return Value:  None
  Description:   Dibuja el la grilla los valores del registro actual de la tabla
  				 "MaestroEmpleado"
********************************************************************************}
procedure TFormSucursalesBancarias.alEntidadesBancariasDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
		FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;

        with Tabla do begin
            TextOut(Cols[0], Rect.Top, IStr(FieldByName('CodigoPersona').AsInteger, 10));
            TextOut(Cols[1], Rect.Top, Trim(FieldByName('Apellido').AsString));
            TextOut(Cols[2], Rect.Top, Trim(FieldByName('Nombre').AsString));
		end;
	end;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalEdit
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la Modificaci�n de un registro.
********************************************************************************}
procedure TFormSucursalesBancarias.alEntidadesBancariasEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    alEntidadesBancarias.Estado	:= modi;
    pcDatosPErsonal.ActivePage      := tsCliente;
	txtApellido.SetFocus;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalInsert
  Author:        Tinao, Diego <dtinao@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la inserci�n de un registro.
********************************************************************************}
procedure TFormSucursalesBancarias.alEntidadesBancariasInsert(Sender: TObject);
begin
	Limpiar_Campos;
    CodigoPersonaActual         := -1;
    btnBuscar.Visible           := True;
    HabilitarCamposEdicion(True);
    alEntidadesBancarias.Estado := alta;
    chkActivo.Checked           := True;
    pcDatosPersonal.ActivePage  := tsCliente;
	txtApellido.SetFocus;
end;

procedure TFormSucursalesBancarias.alEntidadesBancariasRefresh(Sender: TObject);
begin
	if alEntidadesBancarias.Empty then Limpiar_Campos;
end;

procedure TFormSucursalesBancarias.HabilitarCamposEdicion(habilitar: Boolean);
begin
    alEntidadesBancarias.Enabled	:= not Habilitar;
    pcDatosPersonal.Enabled	        := True;
    pnlDatosPersonal.Enabled        := Habilitar;
    pnlDomicilio.Enabled            := Habilitar;
	pnlMediosContacto.Enabled       := Habilitar;
    pnlBancos.Enabled               := not Habilitar;
	Notebook.PageIndex 		        := ord(habilitar);
end;


procedure TFormSucursalesBancarias.cbPersoneriaChange(Sender: TObject);
begin
    HabilitarCamposPersoneria( StrRight(Trim(cbPersoneria.Text), 1)[1]);
end;

procedure TFormSucursalesBancarias.FormShow(Sender: TObject);
begin
    CodigoPersonaActual := -1;
end;

procedure TFormSucursalesBancarias.Agregar(Sender: TObject);
ResourceString
    MSG_AGREGAR_CAPTION = 'Agrear Domicilio';
    MSG_ERROR_AGREGAR   = 'No se ha podido agregar el domicilio';

//var
//	f: TFormEditarDomicilio;
begin
(*
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar() and (f.ShowModal = mrOK) then begin
        try
            with cdsDomicilios do begin
                Append;
                FieldByName('CodigoDomicilio').AsInteger            := -1;
                FieldByName('CodigoPersona').AsInteger              := CodigoPersonaActual;
    //          FieldByName('CodigoTipoOrigenDato').AsInteger       := f.TipoOrigenDato;
//              FieldByName('DescriTipoOrigenDato').AsString        := f.DescriTipoOrigenDato;
                //FieldByName('CodigoTipoCalle').AsInteger            := f.TipoCalle;
                FieldByName('CodigoCalle').AsInteger                := f.CodigoCalle;
                FieldByName('DescriCalle').AsString                 := f.DescripcionCalle;
//                FieldByName('Numero').AsInteger                     := f.Numero;
                FieldByName('Piso').AsInteger                       := f.Piso;
                FieldByName('Dpto').AsString                        := f.Depto;
                FieldByName('CalleDesnormalizada').AsString         := f.DescripcionCalle;
                FieldByName('Detalle').AsString                     := f.Detalle;
                FieldByName('CodigoPostal').AsString                := f.CodigoPostal;
                FieldByName('CodigoPais').AsString                  := f.Pais;
                FieldByName('CodigoRegion').AsString                := f.Region;
                FieldByName('CodigoComuna').AsString                := f.Comuna;
                FieldByName('CodigoCiudad').AsString                := f.Ciudad;
                FieldByName('DescriPais').AsString                  := f.DescriPais;
                FieldByName('DescriRegion').AsString                := f.DescriRegion;
                FieldByName('DescriComuna').AsString                := f.DescriComuna;
//                FieldByName('DescriCiudad').AsString                := f.DescriCiudad;
                FieldByName('DireccionCompleta').AsString           := f.DireccionCompleta;
                //FieldByName('CodigoTipoEdificacion').AsInteger      := f.TipoEdificacion;
                //FieldByName('DescriTipoEdificacion').AsString       := f.DescriTipoEdificacion;
                FieldByName('DomicilioEntrega').AsBoolean           := f.DomicilioEntrega;
                FieldByName('Normalizado').AsInteger                := Ord(f.CodigoCalle > 0);
                FieldByName('CalleRelacionadaUno').AsInteger        := f.CalleRelacionadaUno;
                FieldByName('NumeroCalleRelacionadaUno').AsInteger  := f.NumeroCalleRelacionadaUno;
                FieldByName('CalleRelacionadaDos').AsInteger        := f.CalleRelacionadaDos;
                FieldByName('NumeroCalleRelacionadaDos').AsInteger  := f.NumeroCalleRelacionadaDos;
                FieldByName('DomicilioCompleto').AsString           := f.DomicilioCompleto;                
                Post;
                if f.DomicilioEntrega then RecNoDomicilioEntrega    := dsDomicilios.DataSet.RecNo;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR, E.message, MSG_AGREGAR_CAPTION, MB_ICONSTOP);
                dsDomicilios.DataSet.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesDomicilios;
*)
end;

procedure TFormSucursalesBancarias.btnEliminarDomicilioClick(Sender: TObject);
ResourceString
    MSG_DELETE_DOMICILIO_QUESTION   = '�Desea eliminar este Domicilio?';
    MSG_DELETE_DOMICILIO_CAPTION    = 'Eliminar Domicilio';
    MSG_DELETE_DOMICILIO_ERROR      = 'No se ha podido eliminar el domicilio';

begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(MSG_DELETE_DOMICILIO_QUESTION, MSG_DELETE_DOMICILIO_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			dsDomicilios.DataSet.Delete;
		Except
			On E: Exception do begin
				dsDomicilios.DataSet.Cancel;
				MsgBoxErr( MSG_DELETE_DOMICILIO_ERROR, e.message, MSG_DELETE_DOMICILIO_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
    HabilitarBotonesDomicilios;    
end;

procedure TFormSucursalesBancarias.btnEditarDomicilioClick(Sender: TObject);
ResourceString
    MSG_EDITAR_CAPTION = 'Modificar Domicilio';
    MSG_ERROR_EDITAR   = 'No se ha podido modificar el domicilio';

//var
//	f: TFormEditarDomicilio;
//    i: integer;
begin
(*   Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar(
        dsDomicilios.DataSet.FieldByName('CodigoDomicilio').AsInteger,
        dsDomicilios.DataSet.FieldByName('CodigoTipoOrigenDato').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('CodigoPais').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoRegion').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoComuna').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoCiudad').AsString),
        dsDomicilios.DataSet.FieldByName('CodigoTipoCalle').AsInteger,
        dsDomicilios.DataSet.FieldByName('CodigoCalle').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('CalleDesnormalizada').AsString),
        dsDomicilios.DataSet.FieldByName('Numero').AsInteger,
        dsDomicilios.DataSet.FieldByName('Piso').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('Dpto').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('Detalle').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoPostal').AsString),
        dsDomicilios.DataSet.FieldByName('CodigoTipoEdificacion').AsInteger,
        iif(dsDomicilios.DataSet.RecNo = RecNoDomicilioEntrega, true, false),
        dsDomicilios.DataSet.FieldByName('CalleRelacionadaUno').AsInteger,
        dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaUno').AsInteger,
        dsDomicilios.DataSet.FieldByName('CalleRelacionadaDos').AsInteger,
        dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaDos').AsInteger) and (f.ShowModal = mrOK) then begin
        try
            with cdsDomicilios do begin
                for i := 0 to cdsDomicilios.FieldCount -1 do cdsDomicilios.FieldDefs[i].Attributes  := [];
                Edit;
                FieldByName('CodigoDomicilio').AsInteger            := f.CodigoDomicilio;
                FieldByName('CodigoPersona').AsInteger              := CodigoPersonaActual;
                FieldByName('CodigoTipoOrigenDato').AsInteger       := f.TipoOrigenDato;
                FieldByName('DescriTipoOrigenDato').AsString        := f.DescriTipoOrigenDato;
                FieldByName('CodigoTipoCalle').AsInteger            := f.TipoCalle;
                FieldByName('CodigoCalle').AsInteger                := f.CodigoCalle;
                FieldByName('DescriCalle').AsString                 := f.DescripcionCalle;
                FieldByName('Numero').AsInteger                     := f.Numero;
                FieldByName('Piso').AsInteger                       := f.Piso;
                FieldByName('Dpto').AsString                        := f.Depto;
                FieldByName('CalleDesnormalizada').AsString         := f.DescripcionCalle;
                FieldByName('Detalle').AsString                     := f.Detalle;
                FieldByName('CodigoPostal').AsString                := f.CodigoPostal;
                FieldByName('CodigoPais').AsString                  := f.Pais;
                FieldByName('CodigoRegion').AsString                := f.Region;
                FieldByName('CodigoComuna').AsString                := f.Comuna;
                FieldByName('CodigoCiudad').AsString                := f.Ciudad;
                FieldByName('DescriPais').AsString                  := f.DescriPais;
                FieldByName('DescriRegion').AsString                := f.DescriRegion;
                FieldByName('DescriComuna').AsString                := f.DescriComuna;
                FieldByName('DescriCiudad').AsString                := f.DescriCiudad;
                FieldByName('DireccionCompleta').AsString           := f.DireccionCompleta;
                FieldByName('CodigoTipoEdificacion').AsInteger      := f.TipoEdificacion;
                FieldByName('DescriTipoEdificacion').AsString       := f.DescriTipoEdificacion;
                FieldByName('DomicilioEntrega').AsBoolean           := f.DomicilioEntrega;
                FieldByName('Normalizado').AsInteger                := Ord(f.CodigoCalle > 0);
                FieldByName('CalleRelacionadaUno').AsInteger        := f.CalleRelacionadaUno;
                FieldByName('NumeroCalleRelacionadaUno').AsInteger  := f.NumeroCalleRelacionadaUno;
                FieldByName('CalleRelacionadaDos').AsInteger        := f.CalleRelacionadaDos;
                FieldByName('NumeroCalleRelacionadaDos').AsInteger  := f.NumeroCalleRelacionadaDos;
                FieldByName('DomicilioCompleto').AsString           := f.DomicilioCompleto;
                Post;
                if f.DomicilioEntrega then RecNoDomicilioEntrega    := cdsDomicilios.RecNo;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR, E.message, MSG_EDITAR_CAPTION, MB_ICONSTOP);
                cdsDomicilios.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesDomicilios;
    *)
end;


procedure TFormSucursalesBancarias.dblDomiciliosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if (Column.Index = 4) and (RecNoDomicilioEntrega = dsDomicilios.DataSet.RecNo) then Text := 'Si';

end;

procedure TFormSucursalesBancarias.dblMediosComunicacionDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    if Column.Index = 3 then Text := Formatdatetime('hh:nn', dblMediosComunicacion.DataSource.DataSet.FieldByName('HorarioDesde').AsDateTime);
    if Column.Index = 4 then Text := Formatdatetime('hh:nn', dblMediosComunicacion.DataSource.DataSet.FieldByName('HorarioHasta').AsDateTime);
end;

procedure TFormSucursalesBancarias.btnEliminarMedioComunicacionClick(
  Sender: TObject);
ResourceString
    MSG_DELETE_MEDIO_CONTACTO_QUESTION   = '�Desea eliminar este Medio de Contacto?';
    MSG_DELETE_MEDIO_CONTACTO_CAPTION    = 'Eliminar Medio de Contacto';
    MSG_DELETE_MEDIO_CONTACTO_ERROR      = 'No se ha podido eliminar el Medio de Contacto';

begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(MSG_DELETE_MEDIO_CONTACTO_QUESTION, MSG_DELETE_MEDIO_CONTACTO_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			dsMediosComunicacion.DataSet.Delete;
		Except
			On E: Exception do begin
				dsMediosComunicacion.DataSet.Cancel;
				MsgBoxErr( MSG_DELETE_MEDIO_CONTACTO_ERROR, e.message, MSG_DELETE_MEDIO_CONTACTO_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
    HabilitarBotonesMediosComunicacion;
end;

procedure TFormSucursalesBancarias.btnAgregarMedioComunicacionClick(
  Sender: TObject);
ResourceString
    MSG_AGREGAR_MEDIO_CAPTION = 'Agrear Medio de Contacto';
    MSG_ERROR_AGREGAR_MEDIO   = 'No se ha podido agregar el Medio de Contacto';

//var
//	f: TFormEditarMedioComunicacion;
begin
(*
    Application.CreateForm(TFormEditarMedioComunicacion, f);
    cdsDomicilios.First;
    While not cdsDomicilios.Eof do begin
        with f.Tabla do begin
            Append;
            FieldByName('CodigoDomicilio').AsInteger        := cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger;
            FieldByName('CodigoPersona').AsInteger          := cdsDomicilios.FieldByName('CodigoPersona').AsInteger;
            FieldByName('CodigoTipoOrigenDato').AsInteger   := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
            FieldByName('DescriTipoOrigenDato').AsString    := cdsDomicilios.FieldByName('DescriTipoOrigenDato').AsString;
            FieldByName('CodigoTipoCalle').AsInteger        := cdsDomicilios.FieldByName('CodigoTipoCalle').AsInteger;
            FieldByName('CodigoCalle').AsInteger            := cdsDomicilios.FieldByName('CodigoCalle').AsInteger;
            FieldByName('DescriCalle').AsString             := cdsDomicilios.FieldByName('DescriCalle').AsString;
            FieldByName('Numero').AsInteger                 := cdsDomicilios.FieldByName('Numero').AsInteger;
            FieldByName('Piso').AsInteger                   := cdsDomicilios.FieldByName('Piso').AsInteger;
            FieldByName('Dpto').AsString                    := cdsDomicilios.FieldByName('Dpto').AsString;
            FieldByName('CalleDesnormalizada').AsString     := cdsDomicilios.FieldByName('CalleDesnormalizada').AsString;
            FieldByName('Detalle').AsString                 := cdsDomicilios.FieldByName('Detalle').AsString;
            FieldByName('CodigoPostal').AsString            := cdsDomicilios.FieldByName('CodigoPostal').AsString;
            FieldByName('CodigoPais').AsString              := cdsDomicilios.FieldByName('CodigoPais').AsString;
            FieldByName('CodigoRegion').AsString            := cdsDomicilios.FieldByName('CodigoRegion').AsString;
            FieldByName('CodigoComuna').AsString            := cdsDomicilios.FieldByName('CodigoComuna').AsString;
            FieldByName('CodigoCiudad').AsString            := cdsDomicilios.FieldByName('CodigoCiudad').AsString;
            FieldByName('DescriPais').AsString              := cdsDomicilios.FieldByName('DescriPais').AsString;
            FieldByName('DescriRegion').AsString            := cdsDomicilios.FieldByName('DescriRegion').AsString;
            FieldByName('DescriComuna').AsString            := cdsDomicilios.FieldByName('DescriComuna').AsString;
            FieldByName('DescriCiudad').AsString            := cdsDomicilios.FieldByName('DescriCiudad').AsString;
            FieldByName('DireccionCompleta').AsString       := cdsDomicilios.FieldByName('DireccionCompleta').AsString;
            FieldByName('CodigoTipoEdificacion').AsInteger  := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
            FieldByName('DescriTipoEdificacion').AsString   := cdsDomicilios.FieldByName('DescriTipoEdificacion').AsString;
            FieldByName('DomicilioEntrega').AsBoolean       := cdsDomicilios.FieldByName('DomicilioEntrega').AsBoolean;
            FieldByName('Normalizado').AsInteger            := cdsDomicilios.FieldByName('Normalizado').AsInteger;
            FieldByName('CalleRelacionadaUno').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaUno').AsInteger;
            FieldByName('NumeroCalleRelacionadaUno').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaUno').AsInteger;
            FieldByName('CalleRelacionadaDos').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaDos').AsInteger;
            FieldByName('NumeroCalleRelacionadaDos').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaDos').AsInteger;
            Post;
        end;
        cdsDomicilios.Next
    end;

    if f.Inicializar(1, 1, '', 0, 0, '', 0) and (f.ShowModal = mrOK) then begin
        try
            with dsMediosComunicacion.DataSet do begin
                Append;
                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoOrigenDato;
                FieldByName('CodigoMedioContacto').AsInteger    := f.MedioContacto;
                FieldByName('DescriMedioContacto').AsString     := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MediosContacto WHERE CodigoMedioContacto = ' + IntToStr(f.MedioContacto)));
                FieldByName('TipoMedio').AsString               := f.TipoMedio;
                FieldByName('DescriTipoOrigenDato').AsString    := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TiposOrigenDatos WHERE CodigoTipoOrigenDato = ' + IntToStr(f.TipoOrigenDato)));
                FieldByName('Valor').AsString                   := f.Valor;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('DescriDomicilio').AsString         := f.DescriDomicilio;
                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('HorarioDesde').AsDateTime          := f.HoraDesde;
                FieldByName('HorarioHasta').AsDateTime          := f.HoraHasta;
                FieldByName('Observaciones').AsString           := f.Observaciones;
                FieldByName('CodigoDomicilioRecNo').AsInteger   := f.CodigoDomicilioRecNo;
                Post;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR_MEDIO, E.message, MSG_AGREGAR_MEDIO_CAPTION, MB_ICONSTOP);
                dsMediosComunicacion.DataSet.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesMediosComunicacion;
    *)
end;

procedure TFormSucursalesBancarias.btnEditarMedioComunicacionClick(
  Sender: TObject);

ResourceString
    MSG_EDITAR_MEDIO_CAPTION = 'Modificar Medio de Contacto';
    MSG_ERROR_EDITAR_MEDIO   = 'No se ha podido modificar el Medio de Contacto';

//var
//	f: TFormEditarMedioComunicacion;
begin
(*    Application.CreateForm(TFormEditarMedioComunicacion, f);
    cdsDomicilios.First;
    While not cdsDomicilios.Eof do begin
        f.Tabla.Open;
        with f.Tabla do begin
            Append;
            FieldByName('CodigoDomicilio').AsInteger        := cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger;
            FieldByName('CodigoPersona').AsInteger          := cdsDomicilios.FieldByName('CodigoPersona').AsInteger;
            FieldByName('CodigoTipoOrigenDato').AsInteger   := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
            FieldByName('DescriTipoOrigenDato').AsString    := cdsDomicilios.FieldByName('DescriTipoOrigenDato').AsString;
            FieldByName('CodigoTipoCalle').AsInteger        := cdsDomicilios.FieldByName('CodigoTipoCalle').AsInteger;
            FieldByName('CodigoCalle').AsInteger            := cdsDomicilios.FieldByName('CodigoCalle').AsInteger;
            FieldByName('DescriCalle').AsString             := cdsDomicilios.FieldByName('DescriCalle').AsString;
            FieldByName('Numero').AsInteger                 := cdsDomicilios.FieldByName('Numero').AsInteger;
            FieldByName('Piso').AsInteger                   := cdsDomicilios.FieldByName('Piso').AsInteger;
            FieldByName('Dpto').AsString                    := cdsDomicilios.FieldByName('Dpto').AsString;
            FieldByName('CalleDesnormalizada').AsString     := cdsDomicilios.FieldByName('CalleDesnormalizada').AsString;
            FieldByName('Detalle').AsString                 := cdsDomicilios.FieldByName('Detalle').AsString;
            FieldByName('CodigoPostal').AsString            := cdsDomicilios.FieldByName('CodigoPostal').AsString;
            FieldByName('CodigoPais').AsString              := cdsDomicilios.FieldByName('CodigoPais').AsString;
            FieldByName('CodigoRegion').AsString            := cdsDomicilios.FieldByName('CodigoRegion').AsString;
            FieldByName('CodigoComuna').AsString            := cdsDomicilios.FieldByName('CodigoComuna').AsString;
            FieldByName('CodigoCiudad').AsString            := cdsDomicilios.FieldByName('CodigoCiudad').AsString;
            FieldByName('DescriPais').AsString              := cdsDomicilios.FieldByName('DescriPais').AsString;
            FieldByName('DescriRegion').AsString            := cdsDomicilios.FieldByName('DescriRegion').AsString;
            FieldByName('DescriComuna').AsString            := cdsDomicilios.FieldByName('DescriComuna').AsString;
            FieldByName('DescriCiudad').AsString            := cdsDomicilios.FieldByName('DescriCiudad').AsString;
            FieldByName('DireccionCompleta').AsString       := cdsDomicilios.FieldByName('DireccionCompleta').AsString;
            FieldByName('CodigoTipoEdificacion').AsInteger  := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
            FieldByName('DescriTipoEdificacion').AsString   := cdsDomicilios.FieldByName('DescriTipoEdificacion').AsString;
            FieldByName('DomicilioEntrega').AsBoolean       := cdsDomicilios.FieldByName('DomicilioEntrega').AsBoolean;
            FieldByName('Normalizado').AsInteger            := cdsDomicilios.FieldByName('Normalizado').AsInteger;
            FieldByName('CalleRelacionadaUno').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaUno').AsInteger;
            FieldByName('NumeroCalleRelacionadaUno').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaUno').AsInteger;
            FieldByName('CalleRelacionadaDos').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaDos').AsInteger;
            FieldByName('NumeroCalleRelacionadaDos').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaDos').AsInteger;
            Post;
        end;
        cdsDomicilios.Next
    end;

    if f.Inicializar(
        dsMediosComunicacion.DataSet.FieldByName('CodigoTipoOrigenDato').AsInteger,
        dsMediosComunicacion.DataSet.FieldByName('CodigoMedioContacto').AsInteger,
        dsMediosComunicacion.DataSet.FieldByName('Valor').AsString,
        dsMediosComunicacion.DataSet.FieldByName('HorarioDesde').AsDateTime,
        dsMediosComunicacion.DataSet.FieldByName('HorarioHasta').AsDateTime,
        dsMediosComunicacion.DataSet.FieldByName('Observaciones').AsString,
        dsMediosComunicacion.DataSet.FieldByName('CodigoDomicilio').AsInteger) and (f.ShowModal = mrOK) then begin
        try
            with dsMediosComunicacion.DataSet do begin
                Edit;
                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoOrigenDato;
                FieldByName('CodigoMedioContacto').AsInteger    := f.MedioContacto;
                FieldByName('DescriMedioContacto').AsString     := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MediosContacto WHERE CodigoMedioContacto = ' + IntToStr(f.MedioContacto)));
                FieldByName('TipoMedio').AsString               := f.TipoMedio;
                FieldByName('DescriTipoOrigenDato').AsString    := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TiposOrigenDatos WHERE CodigoTipoOrigenDato = ' + IntToStr(f.TipoOrigenDato)));
                FieldByName('Valor').AsString                   := f.Valor;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('DescriDomicilio').AsString         := f.DescriDomicilio;
                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('HorarioDesde').AsDateTime          := f.HoraDesde;
                FieldByName('HorarioHasta').AsDateTime          := f.HoraHasta;
                FieldByName('Observaciones').AsString           := f.Observaciones;
                FieldByName('CodigoDomicilioRecNo').AsInteger   := f.CodigoDomicilioRecNo;
                Post;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR_MEDIO, E.message, MSG_EDITAR_MEDIO_CAPTION, MB_ICONSTOP);
                dsMediosComunicacion.DataSet.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesMediosComunicacion;
    *)
end;

procedure TFormSucursalesBancarias.btnBuscarClick(Sender: TObject);
ResourceString
    MSG_CAPTION_BUSCAR_PERSONA = 'Buscar Persona';
    MSG_QUESTION_BUSCAR_PERSONA = '�Desea cargar los datos de la persona?' + CRLF +
                    'Al hacerlo se perder�n los datos cargados hasta el momento';
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes , f);
    (* todo: Leonardo
	if f.Inicializa(mcTipoNumeroDocumento.MaskText, Trim(StrRight( mcTipoNumeroDocumento.ComboText, 20)),
      if f.Inicializa(txtTipoNumeroDocumento.Text,Trim(StrRight(cbTipoNumeroDocumento.Text, 20)),
	  Trim(TxtApellido.Text), Trim(TxtApellidoMaterno.Text), Trim(TxtNombre.Text)) then begin
		if (f.ShowModal = mrok) and (MsgBox( MSG_QUESTION_BUSCAR_PERSONA, MSG_CAPTION_BUSCAR_PERSONA, MB_YESNO) = IDYES) then begin
            CodigoPersonaActual := f.Persona.CodigoPersona;
            CargarDatosPersona(CodigoPersonaActual);
		end;
	end;   *)
	f.Release;
end;

procedure TFormSucursalesBancarias.CargarDatosPersona(CodigoPersona: integer);
begin
    ObtenerDatosPersona.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
    if not OpenTables([ObtenerDatosPersona]) then Exit;

    try
        with ObtenerDatosPersona do begin
            //Cargar Datos Personales
            CargarPersoneria(cbPersoneria, Trim(FieldByName('Personeria').AsString));
            cbPersoneria.OnChange(cbPersoneria);
            txtNombre.Text			    := Trim(FieldByName('Nombre').AsString);
            txtApellido.Text  		    := Trim(FieldByName('Apellido').AsString);
            txtApellidoMaterno.Text  	:= Trim(FieldByName('ApellidoMaterno').AsString);

            CargarPaises(DMConnections.BaseCAC, cbLugarNacimiento, FieldByName('LugarNacimiento').AsString);
            dtFechaNacimiento.Date	    := FieldByName('FechaNacimiento').AsDateTime;

            if Trim(FieldByName('Personeria').AsString) = PERSONERIA_FISICA then
                CargarSexos(cbSexo, Trim(FieldByName('Sexo').AsString))
            else if Trim(FieldByName('Personeria').AsString) = PERSONERIA_JURIDICA then
                CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Trim(FieldByName('CodigoSituacionIVA').AsString));
(* todo:Leonardo
            CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, Trim(FieldByName('CodigoDocumento').AsString));

            mcTipoNumeroDocumento.MaskText	:= Trim(FieldByName('NumeroDocumento').AsString);
*)
//            CargarActividadesPersona(DMConnections.BaseCAC, cbActividades,  FieldByName('CodigoActividad').AsInteger);
            chkActivo.Checked		:= FieldByName('Activo').AsBoolean;

            // Cargamos los domicilios
            CargarDomiciliosPersona(FieldByName('CodigoPersona').AsInteger, 0);

            // Cargamos los Medios de Comunicacion
            CargarMediosComunicacionPersona( FieldByName('CodigoPersona').AsInteger, 0);

            //txtCUIT.Text			:= FieldByName('CUIT').AsString;
            CodigoPersonaActual     := FieldByName('CodigoPersona').AsInteger;
            CodigoDomicilioEntrega  := FieldByName('CodigoDomicilioEntrega').AsInteger;
        end;
        ObtenerDatosPersona.Close;
    except
        on E: Exception do begin
            ObtenerDatosPersona.Close;
        end;
    end;
end;


procedure TFormSucursalesBancarias.cbBancosChange(Sender: TObject);
begin
    FCodigoBanco := Ival(StrRight(cbBancos.text, 10));
    SucursalesBancarias.Filter := 'CodigoBanco = ' + intToStr(FCodigoBanco);
    SucursalesBancarias.Filtered := true;
    CodigoPersonaActual := FCodigoBanco;
    alEntidadesBancarias.Reload;
end;

procedure TFormSucursalesBancarias.mcTipoNumeroDocumentoKeyPress(
  Sender: TObject; var Key: Char);
begin
     if not (Key  in ['0'..'9','K','k', #8]) then Key := #0;
end;

end.
