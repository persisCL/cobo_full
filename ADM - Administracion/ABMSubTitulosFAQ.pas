{********************************** File Header ********************************
File Name   : ABMSubTitulosFAQ
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 08-Mar-2004
Language    : ES-AR
Description : ABM en Maestro de Subt�tulos FAQs

Autor       :   CQuezadaI
Fecha       :   17 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se limpia el Caption del Panel1 en el dfm
*******************************************************************************}

unit ABMSubTitulosFAQ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaProcs,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, validate, Util, ADODB, DPSControls,
  FileCtrl;

type
  TfrmABMSubTitulosFAQ = class(TForm)
	SubTitulosFAQ: TADOTable;
	qry_MaxSubTitulo: TADOQuery;
    Panel1: TPanel;
    GroupB: TPanel;
	Label1: TLabel;
    Label15: TLabel;
    txt_Descripcion: TEdit;
	txt_CodigoSubTitulo: TNumericEdit;
    Panel2: TPanel;
    Notebook: TNotebook;
	panABM: TPanel;
	abmSubTitulos: TAbmToolbar;
	dblSubTitulos: TAbmList;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
	procedure BtnCancelarClick(Sender: TObject);
	procedure dblSubTitulosClick(Sender: TObject);
	procedure dblSubTitulosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure dblSubTitulosEdit(Sender: TObject);
	procedure dblSubTitulosRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure dblSubTitulosDelete(Sender: TObject);
	procedure dblSubTitulosInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function dblSubTitulosProcess(Tabla: TDataSet; var Texto: String): Boolean;
  private
	{ Private declarations }
	EdicionExterna : Boolean;
//	FDirIMagenes: ANSIString;
	procedure Limpiar_Campos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializar(CodigoSubTitulo: Integer = 0): Boolean;
  end;

var
  frmABMSubTitulosFAQ: TfrmABMSubTitulosFAQ;

implementation

uses RStrings;

{$R *.DFM}

function TfrmABMSubTitulosFAQ.Inicializar(CodigoSubTitulo: Integer = 0): Boolean;
begin
	Result := False;
	if not OpenTables([SubTitulosFAQ]) then exit;

	Notebook.PageIndex := 0;

	EdicionExterna := CodigoSubTitulo <> 0;
	If EdicionExterna Then Begin
		Constraints.MaxHeight := Constraints.MinHeight;

		dblSubTitulos.Table.Filtered := True;
		dblSubTitulos.Table.Filter := 'CodigoSubTitulo = ' + IntToStr (CodigoSubTitulo);

		panABM.Visible := False;
		dblSubTitulosClick(dblSubTitulos);
		dblSubTitulosEdit(dblSubTitulos);
	end
	Else dblSubTitulos.Reload;

	Result := True;
end;

procedure TfrmABMSubTitulosFAQ.BtnCancelarClick(Sender: TObject);
begin
	if EdicionExterna then begin
		Notebook.PageIndex := 0;
		BtnSalirClick(Sender);
	end
	else Volver_Campos;
end;

procedure TfrmABMSubTitulosFAQ.dblSubTitulosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
		txt_CodigoSubTitulo.Value	:= FieldByName('CodigoSubTitulo').AsInteger;
		txt_Descripcion.text	:= Trim(FieldByName('Descripcion').AsString);
	end;
end;

procedure TfrmABMSubTitulosFAQ.dblSubTitulosDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoSubTitulo').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TfrmABMSubTitulosFAQ.dblSubTitulosEdit(Sender: TObject);
begin
	dblSubTitulos.Enabled	:= False;
	dblSubTitulos.Estado    := modi;
	Notebook.PageIndex 		:= 1;
	groupb.Enabled     		:= True;

	txt_CodigoSubTitulo.Enabled:= False;
	if panABM.Visible then txt_Descripcion.setFocus;
end;

procedure TfrmABMSubTitulosFAQ.dblSubTitulosRefresh(Sender: TObject);
begin
	 if dblSubTitulos.Empty then Limpiar_Campos();
end;

procedure TfrmABMSubTitulosFAQ.Limpiar_Campos();
begin
	txt_CodigoSubTitulo.Clear;
	txt_Descripcion.Clear;
end;

procedure TfrmABMSubTitulosFAQ.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

procedure TfrmABMSubTitulosFAQ.dblSubTitulosDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[FLD_SUBTITULO]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[FLD_SUBTITULO]), Format(MSG_ERROR_ELIMINAR_DEPENDENCIA,[FLD_SUBTITULO]), Format(MSG_CAPTION_ELIMINAR,[FLD_SUBTITULO]), MB_ICONSTOP);
			end;
		end;
		dblSubTitulos.Reload;
	end;
	dblSubTitulos.Estado     := Normal;
	dblSubTitulos.Enabled    := True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TfrmABMSubTitulosFAQ.dblSubTitulosInsert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	dblSubTitulos.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoSubTitulo.Enabled := False;
	txt_Descripcion.SetFocus;
end;

procedure TfrmABMSubTitulosFAQ.BtnAceptarClick(Sender: TObject);
begin
	if not ValidateControls([txt_Descripcion],
				[trim(txt_Descripcion.text) <> ''],
				MSG_CAPTION_ACTUALIZAR,
				[FLD_DESCRIPCION]) then exit;
 	Screen.Cursor := crHourGlass;
	With dblSubTitulos.Table do begin
		Try
			if dblSubTitulos.Estado = Alta then begin
				Append;
				qry_MaxSubTitulo.Open;
				txt_CodigoSubTitulo.Value := qry_MaxSubTitulo.FieldByNAme('CodigoSubTitulo').AsInteger + 1;
				qry_MaxSubTitulo.Close;
			end else begin
				Edit;
			end;
			FieldByName('CodigoSubTitulo').AsInteger 	:= Trunc(txt_CodigoSubTitulo.Value);
			if Trim(txt_Descripcion.text) = '' then
				FieldByName('Descripcion').Clear
			else
				FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_SUBTITULO]), E.message, Format (MSG_CAPTION_ACTUALIZAR,[FLD_SUBTITULO]), MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor 	   := crDefault;

	if EdicionExterna then begin
		Notebook.PageIndex := 0;
		BtnSalirClick(Sender);
	end
	else Volver_Campos;
end;

procedure TfrmABMSubTitulosFAQ.FormShow(Sender: TObject);
begin
   	dblSubTitulos.Reload;
end;

procedure TfrmABMSubTitulosFAQ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TfrmABMSubTitulosFAQ.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TfrmABMSubTitulosFAQ.Volver_Campos;
begin
	dblSubTitulos.Estado     := Normal;
	dblSubTitulos.Enabled    := True;
    dblSubTitulos.Reload;
	dblSubTitulos.SetFocus;
	txt_CodigoSubTitulo.Enabled := True;
	Notebook.PageIndex := 0;
	groupb.Enabled     := False;
end;

function TfrmABMSubTitulosFAQ.dblSubTitulosProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Texto := Tabla.FieldByName('CodigoSubTitulo').AsString + ' ' +
	  Trim(Tabla.FieldByName('Descripcion').AsString);
	Result := True;
end;

end.
