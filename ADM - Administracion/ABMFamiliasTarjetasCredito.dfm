object FormABMFamiliasTarjetasCredito: TFormABMFamiliasTarjetasCredito
  Left = 120
  Top = 165
  Caption = 'ABM de Familias de Tarjetas de Cr'#233'dito'
  ClientHeight = 420
  ClientWidth = 717
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 717
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 717
    Height = 216
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'117'#0'C'#243'digo Familia Tarjetas'
      #0'64'#0'Descripci'#243'n')
    HScrollBar = True
    RefreshTime = 100
    Table = ObtenerFamiliasTarjetasCredito
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 249
    Width = 717
    Height = 132
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    ExplicitTop = 250
    object Label1: TLabel
      Left = 15
      Top = 46
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = Txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Txt_Descripcion: TEdit
      Left = 134
      Top = 41
      Width = 387
      Height = 21
      Hint = 'Descripci'#243'n de la familia de Tarjetas'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 200
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 381
    Width = 717
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 520
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object ObtenerFamiliasTarjetasCredito: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexFieldNames = 'CodigoFamiliaTarjetaCredito'
    TableName = 'FamiliasTarjetasCredito'
    Left = 242
    Top = 110
    object ObtenerFamiliasTarjetasCreditoCodigoFamiliaTarjetaCredito: TAutoIncField
      FieldName = 'CodigoFamiliaTarjetaCredito'
      ReadOnly = True
    end
    object ObtenerFamiliasTarjetasCreditoDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 200
    end
  end
end
