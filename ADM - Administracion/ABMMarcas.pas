{-----------------------------------------------------------------------------
 File Name: 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura


Firma       :   SS_1147_MBE_20140813
Description :   Se agrega que valide que la marca no exista, al agregar o
                modificar una marca.

Firma       :   SS_1147_MCA_20150303
Description :   se elimina boton que permitia ingresar los modelos de los vehiculos.                 
-----------------------------------------------------------------------------}

unit ABMMarcas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, AbmModelos, ADODB, DPSControls, Peatypes;

type
  TFormMarcas = class(TForm)
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    Label15: TLabel;
    txt_CodigoMarca: TNumericEdit;
    qry_MaxMarca: TADOQuery;
    Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
    Panel3: TPanel;
    Marcas: TADOTable;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    //procedure btn_ModelosClick(Sender: TObject);                              //SS_1147_MCA_20150303
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa(MDIChild: Boolean): Boolean;

  end;

var
  FormMarcas: TFormMarcas;

implementation

uses DMConnection;

resourcestring
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Marcas';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';

{$R *.DFM}

function TFormMarcas.Inicializa(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([Marcas]) then exit;
    Marcas.Filter := 'Descripcion <> ''' + QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_OTRA_MARCA()') + '''';
    Marcas.Filtered := true;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	Result := True;
end;

procedure TFormMarcas.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormMarcas.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	    txt_CodigoMarca.Value	:= FieldByName('CodigoMarca').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormMarcas.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoMarca').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFormMarcas.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    txt_CodigoMarca.Enabled	:= False;
    txt_Descripcion.setFocus;
end;

procedure TFormMarcas.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then begin
        //btn_Modelos.Enabled := False;                                         //SS_1147_MCA_20150303
     	Limpiar_Campos();
     //end else begin                                                           //SS_1147_MCA_20150303
        //btn_Modelos.Enabled := True;                                          //SS_1147_MCA_20150303
     end;
end;

procedure TFormMarcas.Limpiar_Campos();
begin
	txt_CodigoMarca.Clear;
	txt_Descripcion.Clear;
end;

procedure TFormMarcas.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormMarcas.DBList1Delete(Sender: TObject);

    function ExistenVehiculosRelaccionados: Boolean;
    //Devuelve True si existen vehiculos relacionados con la marca actual
    begin
        Result := StrtoBool(QueryGetValue(DMConnections.BaseCAC,
                                          'select dbo.MarcaConVehiculos(''' + txt_CodigoMarca.Text + ''')'));
    end;

resourcestring
    MSG_ELIMINARMARCA         = '�Est� seguro de querer eliminar la Marca seleccionada?';
    CAPTION_ELIMINARMARCA     = 'Eliminar Marcas';
    MSG_DELETE_ERROR          = 'No se pudo eliminar la Marca seleccionada';
    MSG_ERROR_RELATED_RECORDS = 'No se pudo eliminar la Marca seleccionada porque existen Veh�culos relacionados';
begin
    if (ExistenVehiculosRelaccionados) then begin
        MsgBox(MSG_ERROR_RELATED_RECORDS, CAPTION_ELIMINARMARCA, MB_ICONSTOP);
        DbList1.Estado     := Normal;
        DbList1.Enabled    := True;
        Exit;
    end;

	Screen.Cursor := crHourGlass;
    try
        If MsgBox(MSG_ELIMINARMARCA, CAPTION_ELIMINARMARCA, MB_YESNO + MB_ICONQUESTION) =
            IDYES then begin
            try
                (Sender AS TDbList).Table.Delete;
            Except
                On E: Exception do begin
                    (Sender AS TDbList).Table.Cancel;
                    MsgBoxErr(MSG_DELETE_ERROR, E.Message, CAPTION_ELIMINARMARCA, MB_ICONSTOP);
                    Raise;
                end;
            end;
            DbList1.Reload;
        end;
        DbList1.Estado     := Normal;
        DbList1.Enabled    := True;
        Notebook.PageIndex := 0;
    finally
	    Screen.Cursor := crDefault;
    end;
end;

procedure TFormMarcas.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoMarca.Enabled := False;
	txt_Descripcion.SetFocus;
end;

procedure TFormMarcas.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ACTUALIZARMARCA     = 'No se pudieron actualizar los datos de la Marca seleccionada.';
    CAPTION_ACTUALIZARMARCA = 'Actualizar Marca';
    MSG_YA_EXISTE           = 'Ya existe una marca con la misma descripci�n';                                               //SS_1147_MBE_20140813
    MSG_SQL_EXISTE_INSERT   = 'SELECT 1 FROM VehiculosMarcas (NOLOCK) WHERE Descripcion = ''%s'' ';                         //SS_1147_MBE_20140813
    MSG_SQL_EXISTE_UPDATE   = 'SELECT 1 FROM VehiculosMarcas (NOLOCK) WHERE Descripcion = ''%s'' AND CodigoMarca <> %d';    //SS_1147_MBE_20140813
var                                                                                                                         //SS_1147_MBE_20140813
    vExisteInt : Integer;                                                                                                   //SS_1147_MBE_20140813
begin
    if not ValidateControls([txt_Descripcion],
                [trim(txt_Descripcion.text) <> ''],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION]) then exit;

    if DBList1.Estado = Alta then begin                                                                                     //SS_1147_MBE_20140813
        vExisteInt := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_EXISTE_INSERT, [txt_Descripcion.Text]));       //SS_1147_MBE_20140813
    end                                                                                                                     //SS_1147_MBE_20140813
    else begin                                                                                                              //SS_1147_MBE_20140813
        vExisteInt := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_EXISTE_UPDATE, [txt_Descripcion.Text, txt_CodigoMarca.ValueInt]));       //SS_1147_MBE_20140813
    end;                                                                                                                    //SS_1147_MBE_20140813
                                                                                                                            //SS_1147_MBE_20140813
    if vExisteInt > 0 then begin                                                                                            //SS_1147_MBE_20140813
        MsgBoxBalloon(MSG_YA_EXISTE, Caption, MB_ICONEXCLAMATION, txt_Descripcion);                                         //SS_1147_MBE_20140813
        Exit;                                                                                                               //SS_1147_MBE_20140813
    end;                                                                                                                    //SS_1147_MBE_20140813
    
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                qry_MaxMarca.Open;
                txt_CodigoMarca.Value	:=  qry_MaxMarca.FieldByNAme('CodigoMarca').AsInteger + 1;
				qry_MaxMarca.Close;
			end else begin
            	Edit;
            end;
			FieldByName('CodigoMarca').AsInteger 	:= Trunc(txt_CodigoMarca.Value);
			if Trim(txt_Descripcion.text) = '' then
            	FieldByName('Descripcion').Clear
            else
				FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZARMARCA, E.message, CAPTION_ACTUALIZARMARCA, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormMarcas.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormMarcas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormMarcas.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormMarcas.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
    txt_CodigoMarca.Enabled := True;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

//procedure TFormMarcas.btn_ModelosClick(Sender: TObject);                      //SS_1147_MCA_20150303
//var                                                                           //SS_1147_MCA_20150303
//	f: TFormModelos;                                                            //SS_1147_MCA_20150303
//begin                                                                         //SS_1147_MCA_20150303
//	Application.CreateForm(TFormModelos, f);                                    //SS_1147_MCA_20150303
//    if f.Inicializa(Marcas.FieldByName('CodigoMarca').AsInteger,              //SS_1147_MCA_20150303
//      Marcas.FieldByName('Descripcion').AsString, False) then  f.ShowModal;   //SS_1147_MCA_20150303
//    f.Release;                                                                //SS_1147_MCA_20150303
//end;                                                                          //SS_1147_MCA_20150303

end.
