object FABMPos: TFABMPos
  Left = 123
  Top = 122
  Caption = 'Mantenimiento de POS (Terminales de Cobro TransBank)'
  ClientHeight = 442
  ClientWidth = 624
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 624
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object lb_Pos: TAbmList
    Left = 0
    Top = 33
    Width = 624
    Height = 274
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'51'#0'N'#186' POS  '
      #0'157'#0'C'#243'digo Comercio                       '
      #0'230'#0'Descripci'#243'n (Numero de Serie)                          '
      
        #0'307'#0'Asignado                                                   ' +
        '                                  '
      #0'8'#0)
    HScrollBar = True
    RefreshTime = 100
    Table = tbl_Pos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lb_PosClick
    OnDrawItem = lb_PosDrawItem
    OnRefresh = lb_PosRefresh
    OnInsert = lb_PosInsert
    OnDelete = lb_PosDelete
    OnEdit = lb_PosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 307
    Width = 624
    Height = 96
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object lbl_NumeroPos: TLabel
      Left = 10
      Top = 12
      Width = 48
      Height = 13
      Caption = 'N'#186' POS:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Descripcion: TLabel
      Left = 11
      Top = 68
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_CodigoComercio: TLabel
      Left = 11
      Top = 40
      Width = 99
      Height = 13
      Caption = 'C'#243'digo comercio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_NumeroPos: TNumericEdit
      Left = 115
      Top = 8
      Width = 105
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 0
      OnKeyPress = txt_NumeroPosKeyPress
    end
    object txt_Descripcion: TEdit
      Left = 115
      Top = 64
      Width = 388
      Height = 21
      Color = 16444382
      MaxLength = 255
      TabOrder = 2
    end
    object txt_CodigoComercio: TEdit
      Left = 115
      Top = 36
      Width = 388
      Height = 21
      Color = 16444382
      MaxLength = 50
      TabOrder = 1
    end
  end
  object pnl_Botones: TPanel
    Left = 0
    Top = 403
    Width = 624
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 427
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object btn_Aceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = btn_AceptarClick
        end
        object btn_Cancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btn_CancelarClick
        end
      end
    end
  end
  object ds_Pos: TDataSource
    DataSet = tbl_Pos
    Left = 12
    Top = 60
  end
  object tbl_Pos: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'POS'
    Left = 48
    Top = 60
  end
  object sp_ActualizarPos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarPos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPos'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroPosNuevo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoComercio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Asignado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 48
    Top = 92
  end
  object sp_EliminarPos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarPos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPos'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 84
    Top = 92
  end
  object sp_InsertarPos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarPos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPos'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoComercio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 12
    Top = 92
  end
end
