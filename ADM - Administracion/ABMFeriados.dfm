object FormFeriados: TFormFeriados
  Left = 195
  Top = 144
  Caption = 'Mantenimiento de D'#237'as Feriados'
  ClientHeight = 477
  ClientWidth = 664
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 670
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlEdicion: TPanel
    Left = 0
    Top = 41
    Width = 664
    Height = 90
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object lblDescripcion: TLabel
      Left = 13
      Top = 61
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = tedDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFechaFeriado: TLabel
      Left = 13
      Top = 35
      Width = 44
      Height = 13
      Caption = '&Fecha: '
      FocusControl = deFechaFeriado
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblShow_Inserta_Modifica: TLabel
      Left = 325
      Top = 6
      Width = 212
      Height = 24
      Caption = 'lblShow_Inserta_Modifica'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clOlive
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object tedDescripcion: TEdit
      Left = 102
      Top = 57
      Width = 335
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 1
    end
    object deFechaFeriado: TDateEdit
      Left = 102
      Top = 31
      Width = 90
      Height = 21
      Hint = 
        'Seleccione un d'#237'a del calendario pulsando sobre el bot'#243'n del cos' +
        'tado '
      AutoSelect = False
      Color = 16444382
      TabOrder = 0
      AllowEmpty = False
      Date = 36526.000000000000000000
    end
    object BtnAceptar: TButton
      Left = 464
      Top = 52
      Width = 79
      Height = 26
      Caption = '&Aceptar'
      TabOrder = 2
      OnClick = BtnAceptarClick
    end
    object BtnCancelar: TButton
      Left = 564
      Top = 52
      Width = 79
      Height = 26
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 3
      OnClick = BtnCancelarClick
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 438
    Width = 664
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      664
      39)
    object lblTotalRegistros: TLabel
      Left = 32
      Top = 10
      Width = 69
      Height = 13
      Caption = 'Total registros:'
    end
    object lblShowTotalRegistros: TLabel
      Left = 136
      Top = 10
      Width = 105
      Height = 13
      Caption = 'lblShowTotalRegistros'
    end
    object BtnSalir: TButton
      Left = 582
      Top = 5
      Width = 79
      Height = 26
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 0
      OnClick = BtnSalirClick
    end
  end
  object dblListaDiasFeriados: TDBListEx
    Left = 0
    Top = 131
    Width = 664
    Height = 307
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        MinWidth = 150
        MaxWidth = 150
        Header.Caption = 'FechaFeriado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaFeriado'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 350
        MinWidth = 350
        MaxWidth = 350
        Header.Caption = 'Descripcion'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
      end>
    DataSource = dsPlanTarifarioDiasFeriados_Obtener
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
    OnClick = dblListaDiasFeriadosClick
  end
  object pnlCabecera: TPanel
    Left = 0
    Top = 0
    Width = 664
    Height = 41
    Align = alTop
    TabOrder = 3
    object sbtActualizaDiaFeriado: TSpeedButton
      Left = 463
      Top = 13
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      OnClick = sbtActualizaDiaFeriadoClick
    end
    object sbtAgregaDiaFeriado: TSpeedButton
      Left = 424
      Top = 13
      Width = 23
      Height = 22
      Hint = 'Inserta nuevo registro'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = sbtAgregaDiaFeriadoClick
    end
    object sbtEliminaDiaFeriado: TSpeedButton
      Left = 499
      Top = 13
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = sbtEliminaDiaFeriadoClick
    end
    object Label2: TLabel
      Left = 28
      Top = 14
      Width = 77
      Height = 13
      Caption = 'Seleccione a'#241'o:'
    end
    object vcbAnio: TVariantComboBox
      Left = 112
      Top = 11
      Width = 113
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = vcbAnioChange
      Items = <>
    end
  end
  object spPlanTarifarioDiasFeriados_Obtener: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'PlanTarifarioDiasFeriados_Obtener'
    Parameters = <>
    Left = 80
    Top = 272
  end
  object spPlanTarifarioDiasFeriados_Agregar: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'PlanTarifarioDiasFeriados_Agregar'
    Parameters = <>
    Left = 80
    Top = 304
  end
  object spPlanTarifarioDiasFeriados_Actualizar: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'PlanTarifarioDiasFeriados_Actualizar'
    Parameters = <>
    Left = 80
    Top = 336
  end
  object spPlanTarifarioDiasFeriados_Eliminar: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'PlanTarifarioDiasFeriados_Eliminar'
    Parameters = <>
    Left = 80
    Top = 368
  end
  object spPlanTarifarioDiasFeriados_Obtener_Anio: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'PlanTarifarioDiasFeriados_Obtener_Anio'
    Parameters = <>
    Left = 80
    Top = 240
  end
  object dsPlanTarifarioDiasFeriados_Obtener: TDataSource
    DataSet = spPlanTarifarioDiasFeriados_Obtener
    Left = 120
    Top = 272
  end
end
