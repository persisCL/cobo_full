object frmPatronBusqueda: TfrmPatronBusqueda
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Buscar...'
  ClientHeight = 135
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 32
    Top = 16
    Width = 149
    Height = 13
    Caption = 'Ingrese patr'#243'n de b'#250'squeda...'
  end
  object edBuscar: TEdit
    Left = 29
    Top = 48
    Width = 305
    Height = 21
    TabOrder = 0
  end
  object btnBuscar: TButton
    Left = 144
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 1
    OnClick = btnBuscarClick
  end
end
