{-----------------------------------------------------------------------------
 File Name: ABMDenominacionesMoneda
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}

unit ABMDenominacionesMoneda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  ADODB, DPSControls, DMConnection;

type
  TFormDenominacionesMoneda = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    DenominacionesMoneda: TADOTable;
    txt_CodigoDenominacion: TNumericEdit;
    Notebook: TNotebook;
    txt_ValorMonedaLocal: TNumericEdit;
    Label2: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos();
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormDenominacionesMoneda: TFormDenominacionesMoneda;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '¿Está seguro de querer eliminar esta Denominación de Moneda?';
    MSG_DELETE_ERROR		= 'No se puede eliminar esta Denominación de Moneda porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION 		= 'Eliminar Denominación de Moneda';
    MSG_ACTUALIZAR_ERROR	= 'No se pudo actualizar la Denominación de Moneda.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Denominación de Moneda';

{$R *.DFM}

function TFormDenominacionesMoneda.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([DenominacionesMoneda]) then
		Result := False
	else begin
       	Notebook.PageIndex := 0;
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormDenominacionesMoneda.BtnCancelarClick(Sender: TObject);
begin
   	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFormDenominacionesMoneda.DBList1Click(Sender: TObject);
begin
	with DenominacionesMoneda do begin
		txt_CodigoDenominacion.Value	:= FieldByName('CodigoDenominacionMoneda').AsInteger;
		txt_Descripcion.text			:= FieldByName('Descripcion').AsString;
        txt_ValorMonedaLocal.Value		:= FieldByName('ValorMonedaLocal').AsFloat;
	end;
end;

procedure TFormDenominacionesMoneda.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormDenominacionesMoneda.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    txt_CodigoDenominacion.Enabled	:= False;
	txt_CodigoDenominacion.Color	:= clBtnFace;
    txt_Descripcion.setFocus;
end;

procedure TFormDenominacionesMoneda.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormDenominacionesMoneda.Limpiar_Campos();
begin
	txt_CodigoDenominacion.Clear;
	txt_Descripcion.Clear;
    txt_ValorMonedaLocal.Clear;
end;

procedure TFormDenominacionesMoneda.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormDenominacionesMoneda.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			DenominacionesMoneda.Delete;
		Except
			On E: Exception do begin
				DenominacionesMoneda.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormDenominacionesMoneda.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoDenominacion.Enabled	:= False;
	txt_CodigoDenominacion.Color	:= clBtnFace;
	txt_Descripcion.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: None
  Return Value: Sender: TObject

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormDenominacionesMoneda.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_DESCRIPCION = 'Falta completar la descripción';
    MSG_VALOR = 'Falta ingresar el valor en moneda local';
begin
    if not ValidateControls(
        [txt_Descripcion, txt_ValorMonedaLocal],
        [Trim(txt_Descripcion.text) <> '',
        trim(txt_ValorMonedaLocal.text) <> ''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_DESCRIPCION, MSG_VALOR]) then exit;

 	Screen.Cursor := crHourGlass;
	With DenominacionesMoneda do begin
		Try
			if DbList1.Estado = Alta then begin
				Append;
                FieldByName('CodigoDenominacionMoneda').value	:= QueryGetValueInt(Connection,
                	'SELECT  ISNULL(MAX(CodigoDenominacionMoneda), 0) + 1 FROM DenominacionesMoneda WITH (NOLOCK) ');
			end else
				Edit;
			FieldByName('Descripcion').AsString  				:= Trim(txt_Descripcion.text);
            FieldByName('ValorMonedaLocal').value				:= txt_ValorMonedaLocal.Value;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    groupb.Enabled     := False;
  	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	DbList1.SetFocus;
    txt_CodigoDenominacion.Enabled	:= True;
	txt_CodigoDenominacion.Color	:= clWindow;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormDenominacionesMoneda.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormDenominacionesMoneda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormDenominacionesMoneda.BtnSalirClick(Sender: TObject);
begin
     close;
end;

end.
