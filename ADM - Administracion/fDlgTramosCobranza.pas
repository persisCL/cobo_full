unit fDlgTramosCobranza;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, PeaProcs;

type
  TfrmEdicionTramosCobranza = class(TForm)
    btnAceptarTramo: TButton;
    btnCancelarTramo: TButton;
    Label1: TLabel;
    txtInicioTramo: TEdit;
    Label4: TLabel;
    Label2: TLabel;
    txtPorcentajeTramo: TEdit;
    Label3: TLabel;
    Bevel1: TBevel;
    txtFinTramo: TEdit;
    txtMontoTramo: TEdit;
    Label5: TLabel;
    Label6: TLabel;
//    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);                     //TASK_115_JMA_20170316
//    procedure txtPorcentajeTramoKeyPress(Sender: TObject; var Key: Char);                 //TASK_115_JMA_20170316
    procedure txtFinTramoKeyPress(Sender: TObject; var Key: Char);
    procedure txtFinTramoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);         //TASK_115_JMA_20170316
    procedure FormShow(Sender: TObject);                                                    //TASK_115_JMA_20170316
    procedure btnAceptarTramoClick(Sender: TObject);                                        //TASK_115_JMA_20170316
  private
    { Private declarations }
  public
    { Public declarations }
	{INICIO		: 20160909 CFU
    function Inicializar(Monto: Integer = 0; Porcentaje: Double = 0.00): Boolean;
	}
    function Inicializar(MontoInicial: Integer = 0; MontoFinal: Integer = 0; MontoTramo: Integer = 0; Porcentaje: Double = 0.00; EsUltimo: Boolean = False): Boolean;   //TASK_115_JMA_20170316
	//TERMINO	: 20160909 CFU
  end;

var
  frmEdicionTramosCobranza: TfrmEdicionTramosCobranza;

implementation

{$R *.dfm}

{ TForm1 }

{INICIO		: 20160909 CFU
function TfrmEdicionTramosCobranza.Inicializar(Monto: Integer = 0; Porcentaje: Double = 0.00): Boolean;
}

function TfrmEdicionTramosCobranza.Inicializar(MontoInicial: Integer = 0; MontoFinal: Integer = 0; MontoTramo: Integer = 0; Porcentaje: Double = 0.00; EsUltimo: Boolean = False): Boolean;  //TASK_115_JMA_20170316
//TERMINO	: 20160909 CFU
begin
    txtInicioTramo.Text     := IntToStr(MontoInicial);
    //INICIO	: 20160909 CFU
    txtFinTramo.Text 		:= IntToStr(MontoFinal);
    txtMontoTramo.Text 		:= IntToStr(MontoTramo);
    //TERMINO	: 20160909 CFU
    txtPorcentajeTramo.Text := FormatFloat('0.00',Porcentaje);

    if not EsUltimo then
        txtFinTramo.Tag := 1;

    Result := True;
end;

procedure TfrmEdicionTramosCobranza.txtFinTramoKeyPress(Sender: TObject; var Key: Char); //TASK_115_JMA_20170316
begin
    if not (Key  in ['0'..'9', #8]) then
        Key := #0;
end;

{INICIO: TASK_115_JMA_20170316
procedure TfrmEdicionTramosCobranza.txtPorcentajeTramoKeyPress(Sender: TObject; var Key: Char);
var
    SepDec: char;
begin
    SepDec := DecimalSeparator;
    if Key in ['.',','] then Key := SepDec;
    if not (Key  in ['0'..'9', #8, SepDec]) then
        Key := #0;
end;

procedure TfrmEdicionTramosCobranza.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    function ValidarInicio: Boolean;
    var
        i: Integer;
    begin
        Result := False;
        try
            i := StrToInt(txtInicioTramo.Text);
            Result := (i >= 0);   // el monto inicial PUEDE ser Cero.
        except
        end;
    end;
    function ValidarPorcentaje: Boolean;
    var
    f: Double;
    begin
        Result := False;
        try 
            f := StrToFloat(txtPorcentajeTramo.Text);
            Result := (f >= 0.01) and (f <= 9999.99); // El porcentaje NO Puede ser Cero.
        except
        end;
    end;
resourcestring
    ERROR_CAPTION       = 'El valor ingresado no es v�lido';
    ERROR_TRAMO         = 'El valor del tramo debe ser mayor o igual a cero.';
    ERROR_PORCENTAJE    = 'El valor del porcentaje debe ser mayor que cero y menor que 9999,99%';
begin
    //if ModalResult = mrOk then
    	{INICIO		: 20160909 CFU
        if not ValidateControls( [txtInicioTramo, txtPorcentajeTramo],
          [ValidarInicio, ValidarPorcentaje ],caption, [ERROR_TRAMO, ERROR_PORCENTAJE]) then begin
        //
        if not ValidateControls( [txtInicioTramo, txtFinTramo, txtMontoTramo, txtPorcentajeTramo],
          [ValidarInicio, ValidarInicio, ValidarInicio, ValidarPorcentaje ],caption, [ERROR_TRAMO, ERROR_PORCENTAJE]) then begin
        //TERMINO	: 20160909 CFU
            ModalResult := mrNone;
            CanClose := False;
        end;
end;
TERMINO: TASK_115_JMA_20170316}

{INICIO: TASK_115_JMA_20170316}

procedure TfrmEdicionTramosCobranza.FormShow(Sender: TObject);
begin
    if txtFinTramo.Tag = 0 then
        txtFinTramo.SetFocus
    else
    begin
        txtFinTramo.Enabled := False;
        txtPorcentajeTramo.SetFocus;
    end;
end;

procedure TfrmEdicionTramosCobranza.txtFinTramoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
    montoInicio, montoFin: integer;
begin
    if (Length(txtFinTramo.Text) > 0) and (StrToInt(txtFinTramo.Text) >= 0) then
    begin
        montoInicio := StrToInt(txtInicioTramo.Text);
        montoFin := StrToInt(txtFinTramo.Text);
        if montoFin = 0 then
            txtMontoTramo.Text := '0'
        else if montoFin > montoInicio then
            txtMontoTramo.Text := IntToStr(montoFin - montoInicio + 1)
        else
            txtMontoTramo.Clear;
    end;
end;

procedure TfrmEdicionTramosCobranza.btnAceptarTramoClick(Sender: TObject);
    function ValidarTramo: Boolean;
    var
        i, f: integer;
    begin
        Result := False;
        i := StrToInt(txtInicioTramo.Text);
        f := StrToInt(txtFinTramo.Text);
        try
            Result := (((i = 0) and (f > 0)) or ((i > 0) and  ((f = 0) or (f > i))));
        except
        end;
    end;
    function ValidarPorcentaje: Boolean;
    var
    f: Double;
    begin
        Result := False;
        try
            f := StrToFloat(txtPorcentajeTramo.Text);
            Result := (f >= 0.01) and (f <= 9999.99); // El porcentaje NO Puede ser Cero.
        except
        end;
    end;
resourcestring
    ERROR_CAPTION       = 'El valor ingresado no es v�lido';
    ERROR_TRAMO         = 'El valor del campo fin tramo debe ser mayor al campo monto inicial o igual a cero para el �ltimo tramo.';
    ERROR_PORCENTAJE    = 'El valor del porcentaje debe ser mayor que cero y menor que 9999,99%';
begin
    if not ValidateControls( [txtFinTramo, txtPorcentajeTramo],
                                [ValidarTramo, ValidarPorcentaje],
                                caption,
                                [ERROR_TRAMO, ERROR_PORCENTAJE]) then begin
        ModalResult := mrNone;                        
        Exit;
    end;
    ModalResult := mrOk;
end;
{TERMINO: TASK_115_JMA_20170316}

end.
