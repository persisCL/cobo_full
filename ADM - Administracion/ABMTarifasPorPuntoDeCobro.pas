unit ABMTarifasPorPuntoDeCobro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DmiCtrls, validate, Dateedit, ComCtrls, Buttons, ExtCtrls,
  Grids, Db, DBTables, UtilDb, Util, UtilProc, ActHorario, TimeEdit,
  DBClient, ADODB, peaprocsCN, DPSControls, UtilTarifas, RStrings,StrUtils,
  ConstParametrosGenerales, ListBoxEx, DBListEx, VariantComboBox,
{INICIO: TASK_109_JMA_20170215}
  //BoxFechaActivacionVersionPlanTarifario,                                                                 
  Provider,                                                                                                 
  ABMPlanTarifarioVersionesTarifasTiposPesosPorKilometro,FrmPlanTarifarioVersionesCrear,
  DBGrids,                                                                                                      
  CategoriasClases, Concesionarias, PeaProcs, Variants, PlanesTarifarios, PuntosCobro, Categoria, BandasHorarias, IPC,               
  FrmEditarTarifaPuntoCobro, frmHabilitarPlanTarifario, FrmFecha, FrmPlanTarifarioGaps;
{TERMINO: TASK_109_JMA_20170215}

type
  TConjunto = set of char;

  TFormTarifasPorPuntoDeCobro = class(TForm)
    pnlBOTTOM: TPanel;
{INICIO: TASK_109_JMA_20170215
	dblListaPlanTarifarioPuntoCobro: TDBListEx;
    PageControl: TPageControl;
    Tab_Horarios: TTabSheet;
    pnlVisualizacionEdicion: TPanel;
    pnlEdicion1: TPanel;
    lblPuntoCobro: TLabel;
    lblCategoria: TLabel;
    lblDiaTipo: TLabel;
    pnlPaintBoxHorario: TPanel;
    PaintBoxHorario: TPaintBox;
    Panel5: TPanel;
    lbl_24_00: TLabel;
    lbl_00_00: TLabel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    pnlEdicion2: TPanel;
    btnInserta: TSpeedButton;
    btnElimina: TSpeedButton;
    btnModifica: TSpeedButton;
    btnSalir: TButton;
    btnCancelar: TButton;
    btnAceptar: TButton;
    lblHoraDesde: TLabel;
    lblHoraHasta: TLabel;
    tedHoraDesde: TTimeEdit;
    tedHoraHasta: TTimeEdit;
    nedImporte: TNumericEdit;
    lblImporte: TLabel;
    lblCodigoTipoTarifa: TLabel;
    GroupBox_Hora_Desde_Hasta: TGroupBox;
TERMINO: TASK_109_JMA_20170215}
    pnlTOP: TPanel;
{INICIO: TASK_109_JMA_20170215
	chbHabilitado: TCheckBox;
    lblImporteCalculado: TLabel;
    lblShow_Inserta_Modifica: TLabel;
    btnDesHabilitar: TButton;
TERMINO: TASK_109_JMA_20170215}
    lblTotalRegistros: TLabel;
    lblShowTotalRegistros: TLabel;
	//btnExpToExcel: TButton;				//TASK_109_JMA_20170215
    sdGuardarCSV: TSaveDialog;
{INICIO: TASK_109_JMA_20170215
	spPlanTarifarioTarifasPuntosCobro_Obtener: TADOStoredProc;
    dsPlanTarifarioTarifasPuntosCobro_Obtener: TDataSource;
    spCategoriaClases_Obtener: TADOStoredProc;
    spConcesionaria_Obtener: TADOStoredProc;
    spCategorias_Obtener: TADOStoredProc;
    spPlanTarifarioVersionesConcesionaria_Obtener: TADOStoredProc;
    spPlanTarifarioVersionesTarifasTipos_Obtener: TADOStoredProc;
    spPlanTarifarioPuntosCobroConcesionaria_Obtener: TADOStoredProc;
    spPlanTarifarioVersionesTarifasTipos_Crear: TADOStoredProc;
    spPlanTarifarioVersiones_FechaActivacion_Existe: TADOStoredProc;
    spPlanTarifarioVersiones_Actualizar: TADOStoredProc;
    spPlanTarifarioVersiones_Agregar: TADOStoredProc;
    spPlanTarifarioVersiones_Copiar: TADOStoredProc;
}    
	pnlTarifas: TPanel;
    pnlDetalleTarifa: TPanel;
    dblListaPlanTarifarioPuntoCobro: TDBListEx;
    pnlOpcionesDetalle: TPanel;
    lblPuntoCobro: TLabel;
    lblDiaTipo: TLabel;
    btnInserta: TSpeedButton;
    btnElimina: TSpeedButton;
    btnModifica: TSpeedButton;
    pnlEdicion1: TPanel;
    cbbPuntoCobro: TVariantComboBox;
    cbbTipoDia: TVariantComboBox;
    pnlPaintBoxHorario: TPanel;
    PaintBoxHorario: TPaintBox;
    pnl2: TPanel;
    pnl3: TPanel;
    pnl1: TPanel;
    lbl_24_00: TLabel;
    lbl_00_00: TLabel;
    pnlConcesionaria: TPanel;
    pnlCategorias: TPanel;
    lblCategoriaClases: TLabel;
    cbbClaseCategorias: TVariantComboBox;
{TERMINO: TASK_109_JMA_20170215}
    btnCrearVersion: TButton;
    btnGenerarPlan: TButton;     //ELIMINAR
{INICIO: TASK_109_JMA_20170215}
    btnExpToExcel: TButton;
    lblConcesionaria: TLabel;
    cbbConcesionaria: TVariantComboBox;
    cbbCategorias: TVariantComboBox;
    lblCategorias: TLabel;
    dblPlanesVersiones: TDBListEx;
    btnEditarPlan: TButton;
    nbGuardar: TNotebook;
    btnSalir: TButton;
    btnGuardarPlan: TButton;
    btnCancelarPlan: TButton;
    cbbTipoTarifa: TVariantComboBox;
    lblTipoTarifa: TLabel;
{TERMINO: TASK_109_JMA_20170215}
    btnEliminarPlan: TButton;
    pbSinAsignar: TPaintBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
{INICIO: TASK_109_JMA_20170215}
    procedure cbbCategoriaClase_Changed(Sender: TObject);
    procedure cbb_PuntoCobro_Categoria_TipoDia_TipoTarifa_Changed(Sender: TObject);
    procedure btnCrearVersionClick(Sender: TObject);
    procedure btnCopiarPlanClick(Sender: TObject);
    procedure btnEditarPlanClick(Sender: TObject);
    procedure btnCancelarPlanClick(Sender: TObject);
    procedure btnGuardarPlanClick(Sender: TObject);
    procedure PaintBoxHorario_Paint(Sender: TDataset);         //TASK_109_JMA_20170215
    procedure btnEliminaClick(Sender: TObject);
    procedure btnModificaClick(Sender: TObject);
    procedure btnInsertaClick(Sender: TObject);
    procedure dblPlanesVersionesLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnSalirClick(Sender: TObject);
    procedure btnEliminarPlanClick(Sender: TObject);
    procedure btnExpToExcelClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dblPlanesVersionesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblListaPlanTarifarioPuntoCobroDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure cbbConcesionaria_Change(Sender: TObject);
{TERMINO: TASK_109_JMA_20170215}
{INICIO: TASK_109_JMA_20170215
    procedure ActualizarLista(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure pnlPaintBoxHorarioResize(Sender: TObject);
    procedure PaintBoxHorarioPaint(Sender: TObject);
    procedure btnEliminaClick(Sender: TObject);
    procedure btnInsertaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure vcbVersionesChange(Sender: TObject);
    procedure vcbConsecionariaChange(Sender: TObject);
    procedure vcbClaseCategoriaChange(Sender: TObject); //TASK_17_FSI
    procedure btnModificaClick(Sender: TObject);
    procedure dblListaPlanTarifarioPuntoCobroDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblListaPlanTarifarioPuntoCobroClick(Sender: TObject);
    procedure chbHabilitadoClick(Sender: TObject);
    procedure nedImporteChange(Sender: TObject);
    procedure btnDesHabilitarClick(Sender: TObject);
    procedure btnExpToExcelClick(Sender: TObject);
    procedure btnCrearVersionClick(Sender: TObject);
    procedure modalCrearVersion(_Copia: Boolean);
    procedure btnGenerarPlanClick(Sender: TObject);
TERMINO: TASK_109_JMA_20170215}
  private
    { Private declarations }
{INICIO: TASK_109_JMA_20170215
     FEdicion : byte;		 0 = browse, 1 = Nuevo, 2 = Edici�n

    //Es el id del registro que esta seleccionado o posicionado el cursor en el DBListEx
    Fid_Tarifa: String;
    FversionChange: Boolean;

    FSelectedIndexdbl: Integer;

    Function ValidarDatos: Boolean;
    //procedure VolverCampos;
    function PlanTarifarioVersion_Actualizar: Boolean;

    procedure Concesionaria_Obtener;
    procedure PlanTarifarioVersionesConcesionaria_Obtener(CodigoConcesionaria: Integer);
    procedure PlanTarifarioPuntosCobroConcesionaria_Obtener(CodigoConcesionaria: Integer);
    procedure Categoria_Obtener(ID_CategoriasClases: Integer);
    procedure CategoriaClase_Obtener;
    procedure PlanTarifarioDiasTipos_Obtener;
    procedure PlanTarifarioTarifasTipos_Obtener;
    procedure PlanTarifarioTarifasPuntosCobro_Obtener;
    procedure ActivarBotones(EsActivar : boolean; Grupo : byte);
    procedure LimpiaCampos;
    procedure cargaDataOfDbListEx(Sender: TObject);
	procedure ExportarToCSV;

}

    FMulticoncesionHabilitada: Boolean;
    FFechaActual: TDateTime;
    FCodigoCategoriaInterurbana: Integer;

    oPlanes: TPlanTarifarioVersion;
    oConcesionarias: TConcesionaria;
    oClasesCategorias: TCategoriaClase;
    oCategorias: TCategoria;
    oTiposDias: TTarifasDiasTipos;
    oTiposTarifas: TTarifasTipos;
    oPuntosCobros: TPuntoCobro;
    fGaps: TfrmPlanesTarifariosGaps;

    procedure CargarComboConcesionarias();
    procedure CargarComboCategoriasClases();
    procedure CargarGrillaPlanesVersiones();
    procedure CargarComboPuntosCobro();
    procedure CargarComboTiposTarifas();
    procedure CargarGrillaPlanTarifarioPuntoCobro();
    procedure CargarComboCategorias();
    procedure dblPlanesVersiones_Changed(Sender: TDataSet);
    procedure CargarComboTiposDias();
    procedure ActivarControles(activar: boolean);
    procedure EditarCrearPlan(accion: Byte);
    procedure CrearPlan(Concesionaria: TVariantComboBox; CategoriaClase: TVariantComboBox; FechaActivacion: TDateTime);
    procedure CrearNuevo(Concesionaria: TVariantComboBox; CategoriaClase: TVariantComboBox; FechaActivacion: TDateTime;CodigoEsquema : Integer);
    procedure CrearCopia(Concesionaria: TVariantComboBox; CategoriaClase: TVariantComboBox; FechaActivacion: TDateTime);
    procedure EditarTarifaPuntoCobro(accion:Byte);
    procedure PermitirEdicion(accion: Boolean);
    procedure Habilitar();
    procedure FechaMOP();
    procedure ExportarToCSV;
{TERMINO: TASK_109_JMA_20170215}
  public
    { Public declarations }
    Function Inicializa(): Boolean;
    Function CargaTarifaTipoPesosXKM(_vcbConcesionariaValue, _vcbVersionesValue: Integer): TADOStoredProc;
  end;


var
  FormTarifasPorPuntoDeCobro: TFormTarifasPorPuntoDeCobro;
  Conjunto : TConjunto;

  iDPlanTarifarioVersion: Int64;


implementation

Uses
	DMConnection, DateUtils, XSBuiltIns;

ResourceString

	MSG_CAPTION_VALIDAR				      = 'Validar datos ingresados';

  MSG_FECHA						            = ' - Fecha: %s';

  MSG_ERROR_FECHA_INCORRECTA		  = 'La Fecha de Activaci�n es incorrecta o  anterior a la fecha actual';
  MSG_ERROR_IMPORTE				        = 'El importe DayPass debe ser mayor a $0,00';
  MSG_ERROR_HORA_DESDE_HASTA      = 'La hora desde debe ser menor a la hora hasta';
  MSG_ERROR_HORA_HASTA_DESDE      = 'La hora hasta debe ser mayor a la hora desde';
	MSG_ERROR_TARIFATIPO		        = 'Falta seleccionar un tipo de tarifa';
  MSG_ERROR_PUNTOCOBRO            = 'Falta seleccionar un punto de cobro';
  MSG_ERROR_CLASECATEGORIA        = 'Falta seleccionar una clase de categoria';
  MSG_ERROR_CATEGORIA             = 'Falta seleccionar una categoria';
  MSG_ERROR_TIPODIA               = 'Falta seleccionar el tipo d�a';

  STR_TARIFA                      = 'Tarifa';

  MSG_SELECCIONE                  = ' Seleccione ';
  MSG_TODOS                       = ' Todos ';
  MSG_TODAS                       = ' Todas ';

  MSG_AGREGA_OK                     = 'Tarifa agregada con �xito';
  MSG_AGREGA_CAPTION                = 'Agregar Tarifas';
  MSG_AGREGA_ERROR                  = 'Error al agregar tarifa';
  MSG_AGREGA_ERROR_FECHA_ACTIVACION = 'La fecha de activaci�n debeb se mayor a la actual';
  MSG_AGREGA_ERROR_TRASLAPE         = 'Tarifa ingresada se traslapa con una existente';

  MSG_ACTUALIZAR_CAPTION   		      = 'Actualizar Tarifas';
  MSG_ACTUALIZAR_OK                 = 'Tarifa actualizada con �xito';
  MSG_ACTUALIZAR_ERROR              = 'Error al actualizar tarifa';

  MSG_HABILITADO_ERROR            = 'Debe deshabilitar antes de agregar o modificar';
  MSG_HABILITADO_CAPTION          = 'Habilitado';

  MSG_ELIMINAR_OK                 = 'Tarifa eliminada con �xito';
  MSG_ELIMINAR_ERROR              = 'Error al eliminar tarifa';

  MSG_ACTUALIZAFECHAVERSION_OK      = 'Fecha actualizada ok';
  MSG_ACTUALIZAFECHAVERSION_CAPTION = 'Habilitando Versi�n';
  MSG_ACTUALIZAFECHAVERSION_ERROR   = 'Error ';

  MSG_TRASLAPE_TARIFA                 = 'La tarifa se traslapa con el horario de otra';

const
    cEdicionBrowse		= 0;
    cEdicionInsertar	= 1;
    cEdicionEditar		= 2;
    cGrupoToolbar		  = 1;		//botones Nuevo, Editar, Eliminar, Buscar
    cGrupoSalir			  = 2;    //bot�n Salir
    cGrupoEdicion		  = 3;		//Botones Aceptar y Cancelar
    cGrupoBrowse		  = 4;		//botones Editar y Eliminar

{$R *.DFM}

{$REGION 'CodigoComentado TASK_109_JMA_20170215'}
{INICIO: TASK_109_JMA_20170215
procedure TFormTarifasPorPuntoDeCobro.ActivarBotones;
begin
	case Grupo of
    	cGrupoToolbar :
        	begin
        		btnInserta.Enabled  := EsActivar;
        		btnElimina.Enabled  := EsActivar;
            btnModifica.Enabled := EsActivar;
        	end;

    	cGrupoSalir :
        	begin
            btnSalir.Enabled := EsActivar;
        	end;

      cGrupoEdicion :
        	begin
        		btnAceptar.Enabled	:= EsActivar;
        		btnCancelar.Enabled := EsActivar;
            pnlEdicion2.Enabled	:= EsActivar;
            pnlEdicion2.Visible  := EsActivar;

            if EsActivar and (FEdicion <> cEdicionEditar) then PlanTarifarioTarifasTipos_Obtener;

            chbHabilitado.Enabled := not EsActivar;

            vcbConcesionarias.Enabled := not EsActivar;
            vcbClaseCategorias.Enabled := not EsActivar;
            vcbVersiones.Enabled := not EsActivar;

        	end;

      cGrupoBrowse :
        	begin
            btnElimina.Enabled := EsActivar;
          end;
    end;

end;

procedure TFormTarifasPorPuntoDeCobro.LimpiaCampos;
begin
    tedHoraDesde.Time := Time;
    tedHoraHasta.Time := Time;
    nedImporte.Value := 0;
    vcbPuntoCobro.ItemIndex := 0;
    vcbPuntoCobro.Refresh;
    vcbCategoria.ItemIndex := 0;
    vcbCategoria.Refresh;
    vcbCodigoDiaTipo.ItemIndex := 0;
    vcbCodigoDiaTipo.Refresh;
    vcbCodigoTarifaTipo.ItemIndex := 0;
    vcbCodigoTarifaTipo.Refresh;
end;

procedure TFormTarifasPorPuntoDeCobro.nedImporteChange(Sender: TObject);
begin
  lblImporteCalculado.Caption := FormatFloat( '0.000', (nedImporte.Value / 1000) );
  lblImporteCalculado.Refresh;
end;

function TFormTarifasPorPuntoDeCobro.Inicializa: Boolean;
Var
	S: TSize;
begin
    S := GetFormClientSize(Application.MainForm);
	  SetBounds(0, 0, S.cx, S.cy);

    Conjunto := ['0'..'9', '.', ','];

    FEdicion := cEdicionBrowse;
    ActivarBotones(False, cGrupoToolbar);
    ActivarBotones(True, cGrupoSalir);
    ActivarBotones(False,  cGrupoEdicion);

    Concesionaria_Obtener;
    CategoriaClase_Obtener;
    PlanTarifarioTarifasTipos_Obtener;

    chbHabilitado.Enabled := false;

    Result := True;
end;

procedure TFormTarifasPorPuntoDeCobro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormTarifasPorPuntoDeCobro.ActualizarLista(Sender: TObject);
begin

  FSelectedIndexdbl := 0;
  PlanTarifarioTarifasPuntosCobro_Obtener;
  // Repintar
  PaintBoxHorario.Invalidate;

  if (FEdicion = cEdicionEditar) then begin
    if spPlanTarifarioTarifasPuntosCobro_Obtener.Locate('ID_Tarifa', Fid_Tarifa, []) then begin
      FSelectedIndexdbl := dblListaPlanTarifarioPuntoCobro.SelectedIndex;
    end;
  end;
end;

procedure TFormTarifasPorPuntoDeCobro.PageControlChange(Sender: TObject);
begin
	ActualizarLista(nil);
end;

procedure TFormTarifasPorPuntoDeCobro.btnAceptarClick(Sender: TObject);
var
  spPlanTarifarioTarifasPuntosCobro_Agregar: TADOStoredProc;
  spPlanTarifarioTarifasPuntosCobro_Actualizar: TADOStoredProc;

begin
	btnAceptar.Enabled := False;

  if not ValidarDatos then begin
		btnAceptar.Enabled := True;
		Exit;
  end;

  Screen.Cursor := crHourGlass;

  if FEdicion = cEdicionInsertar then begin
  	try
	  	DMConnections.BaseBO_Rating.BeginTrans;
      spPlanTarifarioTarifasPuntosCobro_Agregar := TADOStoredProc.Create(Self);
      spPlanTarifarioTarifasPuntosCobro_Agregar.Close;
      with spPlanTarifarioTarifasPuntosCobro_Agregar do begin
        Connection := DMConnections.BaseBO_Rating;
        ProcedureName := 'PlanTarifarioTarifasPuntosCobro_Agregar';
        Parameters.Refresh;
        Parameters.ParamByName('@NumeroPuntoCobro').Value := vcbPuntoCobro.Value;
        Parameters.ParamByName('@CodigoCategoriasClases').Value := vcbClaseCategorias.Value;
        Parameters.ParamByName('@Categoria').Value := vcbCategoria.Value;
        Parameters.ParamByName('@CodigoTarifaTipo').Value := vcbCodigoTarifaTipo.Value;
        Parameters.ParamByName('@CodigoDiaTipo').Value := vcbCodigoDiaTipo.Value;
        Parameters.ParamByName('@HoraDesde').Value := tedHoraDesde.Text;
        Parameters.ParamByName('@HoraHasta').Value := tedHoraHasta.Text;
        Parameters.ParamByName('@Importe').Value := nedImporte.Value;
        Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := iDPlanTarifarioVersion;
        Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        ExecProc;

        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
        begin
          DMConnections.BaseBO_Rating.CommitTrans;
          MsgBox(MSG_AGREGA_OK, MSG_AGREGA_CAPTION);
        end else begin
          if (Parameters.ParamByName('@RETURN_VALUE').Value = -2) then begin
            raise Exception.Create(MSG_AGREGA_ERROR_TRASLAPE);
          end else begin
          raise exception.Create(MSG_AGREGA_ERROR);
        end;
        end;

      end;
      spPlanTarifarioTarifasPuntosCobro_Agregar.Close;
      spPlanTarifarioTarifasPuntosCobro_Agregar.Free;

    except
	    On E: EDataBaseError do
      begin
        if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
        MsgBox(MSG_AGREGA_ERROR + ': ' + E.Message, MSG_AGREGA_CAPTION);
	  	end;
      On E: Exception do
      begin
        if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
        MsgBox(E.Message, MSG_AGREGA_CAPTION);
      end;
    end;

  end else if FEdicion = cEdicionEditar	then begin

  	try
		  DMConnections.BaseBO_Rating.BeginTrans;
      spPlanTarifarioTarifasPuntosCobro_Actualizar := TADOStoredProc.Create(Self);
      spPlanTarifarioTarifasPuntosCobro_Actualizar.Close;
      with spPlanTarifarioTarifasPuntosCobro_Actualizar do begin
        Connection := DMConnections.BaseBO_Rating;
        ProcedureName := 'PlanTarifarioTarifasPuntosCobro_Actualizar';
        Parameters.Refresh;
        Parameters.ParamByName('@ID_Tarifa').Value := Fid_Tarifa;
        Parameters.ParamByName('@NumeroPuntoCobro').Value := vcbPuntoCobro.Value;
        Parameters.ParamByName('@CodigoCategoriasClases').Value := vcbClaseCategorias.Value;
        Parameters.ParamByName('@Categoria').Value := vcbCategoria.Value;
        Parameters.ParamByName('@CodigoTarifaTipo').Value := vcbCodigoTarifaTipo.Value;
        Parameters.ParamByName('@CodigoDiaTipo').Value := vcbCodigoDiaTipo.Value;
        Parameters.ParamByName('@HoraDesde').Value := tedHoraDesde.Text;
        Parameters.ParamByName('@HoraHasta').Value := tedHoraHasta.Text;
        Parameters.ParamByName('@Importe').Value := nedImporte.Value;
        Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := iDPlanTarifarioVersion;
        Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        ExecProc;

        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
        begin
          DMConnections.BaseBO_Rating.CommitTrans;
          MsgBox(MSG_ACTUALIZAR_OK, MSG_ACTUALIZAR_CAPTION);
        end else begin
          if (Parameters.ParamByName('@RETURN_VALUE').Value = -2) then begin
             raise exception.Create(MSG_TRASLAPE_TARIFA);
          end else begin
          raise exception.Create(MSG_ACTUALIZAR_ERROR);
        end;
        end;

      end;
      spPlanTarifarioTarifasPuntosCobro_Actualizar.Close;
      spPlanTarifarioTarifasPuntosCobro_Actualizar.Free;

    except
	    On E: EDataBaseError do
      begin
        if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
        MsgBox(MSG_ACTUALIZAR_ERROR + ': ' + E.Message, MSG_ACTUALIZAR_CAPTION);
		  end;
      On E: Exception do
      begin
        if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
        MsgBox(MSG_ACTUALIZAR_ERROR + ': ' + E.Message, MSG_ACTUALIZAR_CAPTION);
      end;
    end;
  end;

  Screen.Cursor := crDefault;

  ActivarBotones(True, cGrupoToolbar);
  ActivarBotones(True, cGrupoSalir);
  ActivarBotones(False,  cGrupoEdicion);
  ActualizarLista(nil);
  FEdicion := cEdicionBrowse;
end;

procedure TFormTarifasPorPuntoDeCobro.btnCancelarClick(Sender: TObject);
begin
    FSelectedIndexdbl := 0;
    FEdicion := cEdicionBrowse;
    ActivarBotones(True, cGrupoToolbar);
    ActivarBotones(True, cGrupoSalir);
    ActivarBotones(False,  cGrupoEdicion);
end;
 

procedure TFormTarifasPorPuntoDeCobro.btnCrearVersionClick(Sender: TObject);
begin
     modalCrearVersion(False);
end;

// para generar una copia del plan que se estaba mirando.
procedure TFormTarifasPorPuntoDeCobro.btnGenerarPlanClick(Sender: TObject);
begin
  // Validar Parametros <> ''
  if (vcbConcesionarias.ItemIndex > 0) then begin

     if (vcbClaseCategorias.ItemIndex > 0) then begin

      if (vcbVersiones.ItemIndex > 0) then begin
            //enviar los datos a copiar.
            modalCrearVersion(True);
      end else
      begin
          MsgBox('Debe seleccionar una Versi�n a copiar ', 'Atenci�n');
       end;

    end else
    begin
        MsgBox('Debe seleccionar una Clase de Categorias ', 'Atenci�n');
     end;

  end
  else
  begin
    MsgBox('Debe seleccionar una Concesionaria ', 'Atenci�n');
  end;
end;

//Modal de creacion de version.
procedure TFormTarifasPorPuntoDeCobro.modalCrearVersion(_Copia: Boolean);
var
  formPlanTarifarioVersionesCrear: TFormPlanTarifarioVersionesCrear;
  dtFechaActivacion_OLD: TDateTime;
begin

    //Inicializar fechaOLD
    dtFechaActivacion_OLD := dt_FechaActivacion.Date;

    Application.CreateForm(TFormPlanTarifarioVersionesCrear, formPlanTarifarioVersionesCrear);

    if formPlanTarifarioVersionesCrear.Inicializa(vcbConcesionarias.ItemIndex,vcbClaseCategorias.ItemIndex,dt_FechaActivacion.Date,vcbVersiones.Value,_Copia)then
    begin

        if formPlanTarifarioVersionesCrear.ShowModal = mrOK then begin

          try
            dt_FechaActivacion.Date := dtfrmFechaActivacion;
            vcbConcesionarias.ItemIndex:= _ConcesionariaIndex ;
            vcbClaseCategorias.ItemIndex:= _CategoriaClaseIndex;
            try
              if not ValidateControlsCN([dt_FechaActivacion],
                        [(dt_FechaActivacion.Date > Date) and (not dt_FechaActivacion.IsEmpty) ],
                        MSG_CAPTION_VALIDAR,
                        [MSG_ERROR_FECHA_INCORRECTA ], Self) then
              begin
                dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                exit;
              end;
              //Enviar a Insertar!!

               if _GenerarCopia then  begin

                 with spPlanTarifarioVersiones_Copiar do
                  begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@ID_PlanTarifarioVersion_Original').Value := ID_PlanTarifarioVersionOLD;
                    Parameters.ParamByName('@CodigoConcesionaria').Value := _ConcesionariaValue;
                    Parameters.ParamByName('@ID_CategoriasClases').Value := _CategoriaClaseValue;
                    Parameters.ParamByName('@FechaActivacion').Value := dt_FechaActivacion.Date;
                    Parameters.ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
                    Parameters.ParamByName('@ID_INT').Value := 0;
                    ExecProc;

                      if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
                      begin
                          MsgBox('Copia de Plan Tarifario fue realizada satisfactoriamente');
                      end
                      else if (Parameters.ParamByName('@RETURN_VALUE').Value =   -2) then
                      begin
                          MsgBox(Parameters.ParamByName('@ErrorDescription').Value);
                      end
                      else
                      begin
                          MsgBox('Problemas al crear la copia del Plan Tarifario ', 'Atenci�n');
                      end;
                  end;

               end
               else
               begin
                 with spPlanTarifarioVersiones_Agregar do
                  begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoConcesionaria').Value := _ConcesionariaValue;
                    Parameters.ParamByName('@ID_CategoriasClases').Value := _CategoriaClaseValue;
                    Parameters.ParamByName('@FechaActivacion').Value := dtfrmFechaActivacion;
                    Parameters.ParamByName('@Habilitado').Value := 0; //al habilitar se crea el plan Tarifario acorde.
                    Parameters.ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
                    Parameters.ParamByName('@ID_INT').Value := 0;
                    ExecProc;

                      if (Parameters.ParamByName('@RETURN_VALUE').Value <> 0) then
                      begin
                          MsgBox('Problemas al crear la nueva versi�n el Plan Tarifario ', 'Atenci�n');
                      end
                      else  MsgBox('Creaci�n de la versi�n del Plan Tarifario realizada satisfactoriamente');
                  end;
               end;

            except

              On E: EDataBaseError do
              begin
                dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                MsgBox(E.Message, 'Error BDD');
              end;
              On E: Exception do
              begin
                dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                MsgBox(E.Message, 'Error');
              end;
            end;

          finally

            if (formPlanTarifarioVersionesCrear <> nil) then formPlanTarifarioVersionesCrear.Release;

          end;  
        end; 
    end;

    Screen.Cursor := crDefault;  
    ActivarBotones(True, cGrupoToolbar);
    ActivarBotones(True, cGrupoSalir);
    ActivarBotones(False,  cGrupoEdicion);
   // ActualizarLista(nil);
    //Se cargar las versiones, que coinciden con la concesionaria//
    PlanTarifarioVersionesConcesionaria_Obtener(_ConcesionariaValue);
    Categoria_Obtener(_CategoriaClaseValue);
end;


procedure TFormTarifasPorPuntoDeCobro.btnDesHabilitarClick(Sender: TObject);
begin
    // aqui desabilitare el plantarifario simpre que la fecha no sea anterio o igual a la actual
    chbHabilitado.Checked := False;
    if PlanTarifarioVersion_Actualizar then begin
      chbHabilitado.Enabled := True;
      //btnDesHabilitar.Visible := False;
      MsgBox('DesHabilitacion realizada con �xito');
    end;
end;

function TFormTarifasPorPuntoDeCobro.ValidarDatos: Boolean;

begin
  Result := False;

  if not ValidateControlsCN(
                [dt_FechaActivacion,
                  tedHoraDesde,
                  tedHoraHasta,
                  nedImporte,
                  vcbCodigoTarifaTipo,
                  vcbPuntoCobro,
                  vcbClaseCategorias,
                  vcbCategoria,
                  vcbCodigoDiaTipo
                ],
                [((dt_FechaActivacion.Date > Date) or (dt_FechaActivacion.IsEmpty)),
                (tedHoraDesde.Time < tedHoraHasta.Time),
                  (tedHoraDesde.Time < tedHoraHasta.Time),
                nedImporte.Value > 0,
                  vcbCodigoTarifaTipo.ItemIndex > 0,
                  vcbPuntoCobro.ItemIndex > 0,
                  vcbClaseCategorias.ItemIndex > 0,
                  vcbCategoria.ItemIndex > 0,
                  vcbCodigoDiaTipo.ItemIndex > 0
                ],
                MSG_CAPTION_VALIDAR,
                [MSG_ERROR_FECHA_INCORRECTA,
                  MSG_ERROR_HORA_DESDE_HASTA,
                  MSG_ERROR_HORA_HASTA_DESDE,
                  MSG_ERROR_IMPORTE,
                  MSG_ERROR_TARIFATIPO,
                  MSG_ERROR_PUNTOCOBRO,
                  MSG_ERROR_CLASECATEGORIA,
                  MSG_ERROR_CATEGORIA,
                  MSG_ERROR_TIPODIA
                ], Self) then exit;

  Result := True;
end;

procedure TFormTarifasPorPuntoDeCobro.btnInsertaClick(Sender: TObject);
begin
    FSelectedIndexdbl := 0;
    if not ValidateControlsCN([dt_FechaActivacion, chbHabilitado],
                  [((dt_FechaActivacion.Date > Date) or (dt_FechaActivacion.IsEmpty)),
                    chbHabilitado.Checked = False],
                  MSG_AGREGA_CAPTION,
                  [MSG_AGREGA_ERROR_FECHA_ACTIVACION, MSG_HABILITADO_ERROR], Self) then exit;

    FEdicion := cEdicionInsertar;
    lblShow_Inserta_Modifica.Caption := 'Agregando Tarifa';

    ActivarBotones(False, cGrupoToolbar);
    ActivarBotones(False, cGrupoSalir);
    ActivarBotones(True,  cGrupoEdicion);
    LimpiaCampos();
    vcbPuntoCobro.SetFocus;
end;

procedure TFormTarifasPorPuntoDeCobro.btnModificaClick(Sender: TObject);
begin
  if not ValidateControlsCN([dt_FechaActivacion, chbHabilitado],
                  [((dt_FechaActivacion.Date > Date) or (dt_FechaActivacion.IsEmpty)),
                    chbHabilitado.Checked = False],
                  MSG_AGREGA_CAPTION,
                  [MSG_AGREGA_ERROR_FECHA_ACTIVACION, MSG_HABILITADO_ERROR], Self) then exit;

    FEdicion := cEdicionEditar;
    lblShow_Inserta_Modifica.Caption := 'Modificando Tarifa';
    //se rescata los datos del registro que tiene el foco en la grilla
    cargaDataOfDbListEx(dblListaPlanTarifarioPuntoCobro);
    //
    ActivarBotones(False, cGrupoToolbar);
    ActivarBotones(False, cGrupoSalir);
    ActivarBotones(True,  cGrupoEdicion);
    vcbPuntoCobro.SetFocus;

end;

procedure TFormTarifasPorPuntoDeCobro.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormTarifasPorPuntoDeCobro.pnlPaintBoxHorarioResize(Sender: TObject);
begin
	PaintBoxHorario.Invalidate;
end;

procedure TFormTarifasPorPuntoDeCobro.PaintBoxHorarioPaint(Sender: TObject);
Var
	horarioDesde: TDateTime;
  horarioHasta: TDateTime;
	posicionDesde, posicionHasta, porcentajeHoraDesde, porcentajeHoraHasta : integer;

begin
  try
	PaintBoxHorario.Canvas.Pen.Style := psClear;
	PaintBoxHorario.Canvas.Rectangle(0, 0, PaintBoxHorario.Width, PaintBoxHorario.Height);

  if spPlanTarifarioTarifasPuntosCobro_Obtener.State <> dsInactive then begin
      spPlanTarifarioTarifasPuntosCobro_Obtener.DisableControls;
    with spPlanTarifarioTarifasPuntosCobro_Obtener do begin
      First;
      while not EOF do begin
        horarioDesde := FieldByName('HoraDesde').AsDateTime * 24;
        horarioHasta := FieldByName('HoraHasta').AsDateTime * 24;

        porcentajeHoraDesde := Round(horarioDesde / 24 * 100);
        porcentajeHoraHasta := Round(horarioHasta / 24 * 100);

        posicionDesde := Round(porcentajeHoraDesde * PaintBoxHorario.Width /100);
        posicionHasta := Round(porcentajeHoraHasta * PaintBoxHorario.Width /100);

        if FieldByName('CodigoTarifaTipo').AsString = 'TBFP' then
          PaintBoxHorario.Canvas.Brush.Color := pnl_color_Normal_TBFP.Color
        else if FieldByName('CodigoTarifaTipo').AsString = 'TBP' then
          PaintBoxHorario.Canvas.Brush.Color := pnl_color_Punta_TBP.Color
        else begin
          PaintBoxHorario.Canvas.Brush.Color := pnl_color_Congestion_TS.Color
        end;

        PaintBoxHorario.Canvas.Rectangle(posicionDesde, 0, posicionHasta ,
        PaintBoxHorario.Height);

        Next;
      end;
    end;
      spPlanTarifarioTarifasPuntosCobro_Obtener.First;
      spPlanTarifarioTarifasPuntosCobro_Obtener.EnableControls;
      if (FSelectedIndexdbl > 0) then begin
        spPlanTarifarioTarifasPuntosCobro_Obtener.Locate('ID_Tarifa', Fid_Tarifa, []);
      end;
  end;

  PaintBoxHorario.Canvas.Pen.Style := psSolid;
  PaintBoxHorario.Canvas.Brush.Style := bsClear;
	PaintBoxHorario.Canvas.Rectangle(0, 0, PaintBoxHorario.Width,PaintBoxHorario.Height);
  except
			on E: Exception do begin
				MsgBox( E.Message, 'Error al dibujar franja horaria' );
      end;
		end
end;

procedure TFormTarifasPorPuntoDeCobro.vcbConsecionariaChange(Sender: TObject);
begin
    vcbVersiones.Clear;
  if (vcbConcesionarias.ItemIndex > 0) then begin
    //Se cargar las versiones, que coinciden con la concesionaria//
    PlanTarifarioVersionesConcesionaria_Obtener(vcbConcesionarias.Value);
  end else begin
    vcbClaseCategorias.Clear;
    vcbVersiones.Clear;
    vcbPuntoCobro.Clear;
    vcbCategoria.Clear;
    vcbCodigoDiaTipo.Clear;
    dt_FechaActivacion.Clear;
    chbHabilitado.Checked := False;
    chbHabilitado.Enabled := False;

  end;
end;

//TASK_17_FSI
procedure TFormTarifasPorPuntoDeCobro.vcbClaseCategoriaChange(Sender: TObject);
begin
  vcbVersiones.Clear;
  if (vcbConcesionarias.ItemIndex > 0) then begin
    //Se cargar las versiones, que coinciden con la concesionaria//
    PlanTarifarioVersionesConcesionaria_Obtener(vcbConcesionarias.Value);
    Categoria_Obtener(vcbClaseCategorias.Value);
  end else begin
    vcbVersiones.Clear;
    vcbPuntoCobro.Clear;
    vcbCategoria.Clear;
    vcbCodigoDiaTipo.Clear;
    dt_FechaActivacion.Clear;
    chbHabilitado.Checked := False;
    chbHabilitado.Enabled := False;

  end;
end;

procedure TFormTarifasPorPuntoDeCobro.vcbVersionesChange(Sender: TObject);

begin
  FversionChange := True;
    if (vcbVersiones.Items.Count > 0) then begin
      if (vcbVersiones.ItemIndex > 0) then begin
        spPlanTarifarioVersionesConcesionaria_Obtener.Open;
        if spPlanTarifarioVersionesConcesionaria_Obtener.Locate('ID_PlanTarifarioVersion', vcbVersiones.Value, [loCaseInsensitive])  then
        begin
	  	    dt_FechaActivacion.Date	:= spPlanTarifarioVersionesConcesionaria_Obtener.FieldByName('FechaActivacion').AsDateTime;
          iDPlanTarifarioVersion    := spPlanTarifarioVersionesConcesionaria_Obtener.FieldByName('ID_PlanTarifarioVersion').AsInteger;
          chbHabilitado.Checked     := spPlanTarifarioVersionesConcesionaria_Obtener.FieldByName('Habilitado').AsBoolean;

          chbHabilitado.Enabled := not chbHabilitado.Checked;

          ActivarBotones((((dt_FechaActivacion.Date) > Date) or (dt_FechaActivacion.IsEmpty)) and (not chbHabilitado.Checked), cGrupoToolbar);

          //Si aun no se activa (fecha de activacion mayor a la actual) se puede eliminar
          btnElimina.Enabled := (((dt_FechaActivacion.Date) > Date) or (dt_FechaActivacion.IsEmpty)) and (chbHabilitado.Checked = False);

        end
        else
        begin
      	  dt_FechaActivacion.Date		:= Date;
          iDPlanTarifarioVersion    := 0;
        end;
        spPlanTarifarioVersionesConcesionaria_Obtener.Close;

      end else begin
        dt_FechaActivacion.Date := NullDate;
        iDPlanTarifarioVersion  := 0;
        ActivarBotones(False, cGrupoToolbar);
        chbHabilitado.Enabled := False;
        //btnDesHabilitar.Visible := False;
      end;

      PlanTarifarioPuntosCobroConcesionaria_Obtener(vcbConcesionarias.Value);
      Categoria_Obtener(vcbClaseCategorias.Value);
      PlanTarifarioDiasTipos_Obtener;
      ActualizarLista(nil);

    end else if (vcbConcesionarias.itemindex > 0) and (vcbVersiones.Items.Count = 1) then begin
      MsgBox('No existen versiones para la Concesionaria seleccionada');
      vcbConcesionarias.SetFocus;
      LimpiaCampos();
      ActualizarLista(nil);
      chbHabilitado.Enabled := False;
    end;
  FversionChange := False;
end;

procedure TFormTarifasPorPuntoDeCobro.btnEliminaClick(Sender: TObject);
var
  spPlanTarifarioVersionesConcesionaria_Eliminar: TADOStoredProc;
begin
	Screen.Cursor := crHourGlass;
  cargaDataOfDbListEx(dblListaPlanTarifarioPuntoCobro);
	if MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_TARIFA]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
    try
      DMConnections.BaseBO_Rating.BeginTrans;
      spPlanTarifarioVersionesConcesionaria_Eliminar := TADOStoredProc.Create(nil);
      spPlanTarifarioVersionesConcesionaria_Eliminar.Close;
      with spPlanTarifarioVersionesConcesionaria_Eliminar do begin
        Connection := DMConnections.BaseBO_Rating;
        ProcedureName := 'PlanTarifarioTarifasPuntosCobro_Eliminar';
        Parameters.CreateParameter('@ID_Tarifa', ftInteger, pdInput, 0, 0);
        Parameters.CreateParameter('@Usuario', ftString, pdInput, 20, 0);
        Parameters.CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 10, 0);
        Parameters.Refresh;
        Parameters.ParamByName('@ID_Tarifa').Value := Fid_Tarifa;
        Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        ExecProc;

        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
        begin
          DMConnections.BaseBO_Rating.CommitTrans;
          MsgBox(MSG_ELIMINAR_OK, Format(MSG_CAPTION_ELIMINAR,[STR_TARIFA]));
        end
        else
        begin
          raise exception.Create(MSG_ELIMINAR_ERROR);
        end;
      end;

		except
			on E: Exception do begin
        if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_TARIFA]), E.Message, Format(MSG_CAPTION_ELIMINAR,[STR_TARIFA]), MB_ICONSTOP);
      end;
		end
	end;
	Screen.Cursor := crDefault;
  FEdicion := cEdicionBrowse;
  ActivarBotones(True, cGrupoToolbar);
  ActivarBotones(True, cGrupoSalir);
  ActivarBotones(False,  cGrupoEdicion);
  ActualizarLista(nil);
end;

procedure TFormTarifasPorPuntoDeCobro.btnExpToExcelClick(Sender: TObject);
begin
//aqui exportare a excel
     ExportarToCSV;
end;

procedure TFormTarifasPorPuntoDeCobro.PlanTarifarioVersionesConcesionaria_Obtener;
var
    lEof: Boolean;
    sFechaActivacion: String;
begin
    lEof := False;


    vcbVersiones.Items.Clear;
    vcbVersiones.Items.Add(PadR(MSG_SELECCIONE,200,' '), 0);
    vcbVersiones.ItemIndex := 0;

    with spPlanTarifarioVersionesConcesionaria_Obtener do begin
      try
        Close;
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
        Parameters.ParamByName('@ID_CategoriasClases').Value := vcbClaseCategorias.Value;
//        if Active then Close;
        Open;

        lEof := not EOF;
        if not EOF then begin
          while (not EOF) do begin
              sFechaActivacion := FieldByName('FechaActivacion').AsString;
              if (sFechaActivacion <> '') then begin
                sFechaActivacion := FormatDateTime('dd/mm/yyyy', FieldByName('FechaActivacion').AsDateTime);
                //sFechaActivacion := FormatDateTime('dd/mm/yyyy', Date);
              end;

              vcbVersiones.Items.Add(
                    PadR(
                        	PadR(IntToStr(FieldByName('CodigoVersion').AsInteger), 10, ' ') +
                          Format( MSG_FECHA, [sFechaActivacion]), 200, ' ') +
                          Istr(FieldByName('CodigoVersion').AsInteger, 10),
                          FieldByName('ID_PlanTarifarioVersion').AsInteger
                    );
          	Next;
          end;
        end else begin
          //vcbVersiones.Clear;
          vcbVersiones.Items.Clear;
          vcbVersiones.Items.Add(PadR(MSG_SELECCIONE,200,' '), 0);
          vcbVersiones.ItemIndex := 0;
        end;
      except
        	On E: Exception do begin
            	MsgBox(E.message, 'Error', MB_ICONSTOP);
            end;
        end;

     // spPlanTarifarioVersionesConcesionaria_Obtener.Close;
    //  spPlanTarifarioVersionesConcesionaria_Obtener.Free;
    end;

    if lEof then begin
      vcbVersiones.ItemIndex := 0;
      if spPlanTarifarioVersionesConcesionaria_Obtener.Locate('ID_PlanTarifarioVersion', vcbVersiones.Value, [loCaseInsensitive])  then begin
		    dt_FechaActivacion.Date		:= spPlanTarifarioVersionesConcesionaria_Obtener.FieldByName('FechaActivacion').AsDateTime;
        chbHabilitado.Checked		:= spPlanTarifarioVersionesConcesionaria_Obtener.FieldByName('Habilitado').AsBoolean;
      end else begin
        dt_FechaActivacion.Date	:= Date;
        chbHabilitado.Checked   := False;
      end;
    end;
    spPlanTarifarioVersionesConcesionaria_Obtener.Close;
    vcbVersiones.OnChange(vcbVersiones);
end;

procedure TFormTarifasPorPuntoDeCobro.Concesionaria_Obtener;
var
  lEof: Boolean;
begin
  vcbConcesionarias.Clear;
  vcbConcesionarias.Items.Add( MSG_SELECCIONE, '0');
  with spConcesionaria_Obtener do begin

    Parameters.CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 10, 0);
    Parameters.Refresh;

    if Active then Close;
      Open;
    lEof := not EOF;
    if not EOF then begin
      while not EOF do begin
        vcbConcesionarias.Items.Add(
                FieldByName('Descripcion').AsString,
                FieldByName('CodigoConcesionaria').AsInteger );
        Next;
      end;
    end else begin
      vcbConcesionarias.Clear;
    end;
  end;
  spConcesionaria_Obtener.Close;
  spConcesionaria_Obtener.Free;
  if lEof then vcbConcesionarias.ItemIndex := 0;
end;

//TASK_17_FSI
procedure TFormTarifasPorPuntoDeCobro.CategoriaClase_Obtener;
var
  lEof: Boolean;
begin
  vcbClaseCategorias.Clear;
  vcbClaseCategorias.Items.Add(MSG_SELECCIONE, '0');

  with spCategoriaClases_Obtener do begin
    Parameters.Refresh;

    if Active then Close;
      Open;

    lEof := not EOF;
    if not EOF then begin
      while not EOF do begin
        vcbClaseCategorias.Items.Add(
                  FieldByName('Descripcion').AsString,
                  FieldByName('ID_CategoriasClases').AsInteger );
        Next;
      end;
    end else begin
      vcbClaseCategorias.Clear;
    end;
  end;
  spCategoriaClases_Obtener.Close;
  //spCategoriaClases_Obtener.Free;
  if lEof then vcbClaseCategorias.ItemIndex := 0;
end;
//TASK_17_FSI  se actualiza la conexion
procedure TFormTarifasPorPuntoDeCobro.Categoria_Obtener;
var
  lEof: Boolean;
begin
    //SHOWMESSAGE();
    //MsgBox(IntToStr(ID_CategoriasClases), 'Atenci�n ID_CategoriasClases');
    vcbCategoria.Items.Clear;
    vcbCategoria.Items.Add( MSG_TODAS, '0');  

    with spCategorias_Obtener do begin
      Parameters.Refresh;
      Parameters.ParamByName('@ID_CategoriasClases').Value := ID_CategoriasClases;

      if Active then Close;
      Open;

      lEof := not EOF;
      if not EOF then begin
        while not EOF do begin
          vcbCategoria.Items.Add(
                   FieldByName('Descripcion').AsString,
                   FieldByName('CodigoCategoria').AsInteger );  //TASK_028_FSI_20161214
         Next;
        end;
      end else begin
      vcbCategoria.Clear;
    end;
  end;
  spCategorias_Obtener.Close;
  //spCategorias_Obtener.Free;
  if lEof then vcbCategoria.ItemIndex := 0;
end;

procedure TFormTarifasPorPuntoDeCobro.dblListaPlanTarifarioPuntoCobroClick(Sender: TObject);
begin
  //si el dataset no esta activo salgo
  if (Sender as TDBListEx).DataSource.Dataset.Active = false then exit;

  cargaDataOfDbListEx(Sender);

end;

procedure TFormTarifasPorPuntoDeCobro.cargaDataOfDbListEx(Sender: TObject);
var
  nI: Integer;
  lFound: Boolean;
begin
  Fid_Tarifa := (Sender as TDBListEx).DataSource.DataSet.FieldByName('ID_Tarifa').Value;

  if ((FEdicion = cEdicionEditar) or (FEdicion = cEdicionInsertar)) then begin

    nI := 0;
    lFound := True;
    while lFound do begin
      if nI < vcbPuntoCobro.Items.Count then begin
        vcbPuntoCobro.ItemIndex := nI;
        if vcbPuntoCobro.Value = (Sender as TDBListEx).DataSource.Dataset.FieldByName('NumeroPuntoCobro').Value then begin
          lFound := False;
          vcbPuntoCobro.Refresh;
        end;
        nI := nI + 1;
      end else begin
        lFound := False;
      end;
    end;

    //Agregar combo ClaseCategoria
    vcbClaseCategorias.ItemIndex := (Sender as TDBListEx).DataSource.Dataset.FieldByName('CodigoCategoriasClases').Value;
    vcbClaseCategorias.Refresh;


    vcbCategoria.ItemIndex := (Sender as TDBListEx).DataSource.Dataset.FieldByName('Categoria').Value;
    vcbCategoria.Refresh;

    nI := 0;
    lFound := True;
    while lFound do begin
      if nI < vcbCodigoDiaTipo.Items.Count then begin
        vcbCodigoDiaTipo.ItemIndex := nI;
        if vcbCodigoDiaTipo.Value = (Sender as TDBListEx).DataSource.Dataset.FieldByName('CodigoDiaTipo').Value then begin
          lFound := False;
          vcbCodigoDiaTipo.Refresh;
        end;
        nI := nI + 1;
      end else begin
        lFound := False;
      end;
    end;

    nI := 0;
    lFound := True;
    while lFound do begin
      if nI < vcbCodigoTarifaTipo.Items.Count then begin
        vcbCodigoTarifaTipo.ItemIndex := nI;
        if vcbCodigoTarifaTipo.Value = (Sender as TDBListEx).DataSource.Dataset.FieldByName('CodigoTarifaTipo').Value then begin
          lFound := False;
          vcbCodigoTarifaTipo.Refresh;
        end;
        nI := nI + 1;
      end else begin
        lFound := False;
      end;
    end;

    tedHoraDesde.Time := StrToTime((Sender as TDBListEx).DataSource.DataSet.FieldByName('HoraDesde').Value);
    tedHoraHasta.Time := StrToTime((Sender as TDBListEx).DataSource.DataSet.FieldByName('HoraHasta').Value);

    nedImporte.Value := (Sender as TDBListEx).DataSource.DataSet.FieldByName('Importe').Value;
  end;
end;

procedure TFormTarifasPorPuntoDeCobro.dblListaPlanTarifarioPuntoCobroDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
  try
  if spPlanTarifarioTarifasPuntosCobro_Obtener.RecNo > -1 then begin

  with Sender do begin
    if Column.Header.Caption = '' then begin
      Canvas.FillRect(Rect);
      if (spPlanTarifarioTarifasPuntosCobro_Obtener.FieldByName('CodigoTarifaTipo').AsString = 'TBFP') then
        Canvas.Brush.Color := pnl_color_Normal_TBFP.Color
      else if (spPlanTarifarioTarifasPuntosCobro_Obtener.FieldByName('CodigoTarifaTipo').AsString = 'TBP') then
        Canvas.Brush.Color := pnl_color_Punta_TBP.Color
        else if (spPlanTarifarioTarifasPuntosCobro_Obtener.FieldByName('CodigoTarifaTipo').AsString = 'TS') then
        Canvas.Brush.Color := pnl_color_Congestion_TS.Color
        else begin
          Canvas.Brush.Color := pnl_color_SinAsignar.Color
      end;
    end;
    if Column.Header.Caption = 'Importe' then begin
      Text := FormatFloat( '0.000', (StrToInt(Text) / 1000) );
      Canvas.FillRect(Rect);
    end;
  end;
  end;
  except
    On E: Exception do begin
      MsgBox(E.message, 'Error al pintar columna tipo dia', MB_ICONSTOP);
      MsgBox(IntToStr(spPlanTarifarioTarifasPuntosCobro_Obtener.RecNo));
      end;
  end;

end;

procedure TFormTarifasPorPuntoDeCobro.PlanTarifarioPuntosCobroConcesionaria_Obtener;
var
//    spPlanTarifarioPuntosCobroConcesionaria_Obtener: TADOStoredProc;
    lEof: Boolean;
begin
  vcbPuntoCobro.Clear;
  vcbPuntoCobro.Items.Add( MSG_TODOS, '0');

  with spPlanTarifarioPuntosCobroConcesionaria_Obtener do begin

    //Parameters.CreateParameter('@CodigoConcesionaria', ftInteger, pdInput, 10, 0);
    //Parameters.CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 10, 0);
    Parameters.Refresh;
    Parameters.ParamByName('@CodigoConcesionaria').Value := CodigoConcesionaria;
    if Active then Close;
      Open;
    lEof := not EOF;
    if not EOF then begin
      while not EOF do begin
        vcbPuntoCobro.Items.Add(
                  FieldByName('Descripcion').AsString,
                  FieldByName('NumeroPuntoCobro').AsInteger );
        Next;
      end;
    end else begin
      vcbPuntoCobro.Clear;
    end;
  end;
  spPlanTarifarioPuntosCobroConcesionaria_Obtener.Close;
//  spPlanTarifarioPuntosCobroConcesionaria_Obtener.Free;
  if lEof then vcbPuntoCobro.ItemIndex := 0;
end;



procedure TFormTarifasPorPuntoDeCobro.chbHabilitadoClick(Sender: TObject);
var
  fBoxFechaActivacionVersionPlanTarifario: TFrmFechaActivacionVersionPlanTarifario;
  fFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro: TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro;
  lOkPesosXKM: Boolean;
  dtFechaActivacion_OLD: TDateTime;
begin

  if (FversionChange = false) then begin
    if chbHabilitado.Checked = True then begin

      ActivarBotones( not chbHabilitado.Checked, cGrupoToolbar);

      try
        dtFechaActivacion_OLD := dt_FechaActivacion.Date;

        try
            //llenamos el objeto AdostoredProc que contendra los pesos x kimlometro
            spPlanTarifarioVersionesTarifasTipos_Obtener := CargaTarifaTipoPesosXKM(vcbConcesionarias.Value, vcbVersiones.Value);

            //Revisamos si tiene datos, sino se insertan las tarifas asociadas en con precios Null
            if spPlanTarifarioVersionesTarifasTipos_Obtener.RecordCount = 0 then
            begin
                with spPlanTarifarioVersionesTarifasTipos_Crear do begin

                  Parameters.Refresh;
                  Parameters.ParamByName('@ID_Concesionaria').Value := vcbConcesionarias.Value;
                  Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := vcbVersiones.Value;
                  Parameters.ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
                  //Parameters.ParamByName('@ErrorDescription').Value := nil;
                  ExecProc;

                  if (Parameters.ParamByName('@RETURN_VALUE').Value <> 0) then
                    begin
                      MsgBox('Problemas con la carga de TARIFAS VERSIONES! ', 'Atenci�n');
                    end
                  else
                    begin
                       //llenamos el objeto AdostoredProc que contendra los pesos x kimlometro
                       spPlanTarifarioVersionesTarifasTipos_Obtener := CargaTarifaTipoPesosXKM(vcbConcesionarias.Value, vcbVersiones.Value);
                    end;
                  end;
            end;


          Application.CreateForm(TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro, fFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro);

          if fFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro.Inicializa(vcbConcesionarias.Value, vcbVersiones.Value, spPlanTarifarioVersionesTarifasTipos_Obtener) then begin

            if fFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro.ShowModal = mrOK then begin

              //se revisar si los pesos por kilometros son mayor a cero, para continuar
              spPlanTarifarioVersionesTarifasTipos_Obtener := CargaTarifaTipoPesosXKM(vcbConcesionarias.Value, vcbVersiones.Value);;
              lOkPesosXKM := False;
              while not spPlanTarifarioVersionesTarifasTipos_Obtener.Eof do begin
                if spPlanTarifarioVersionesTarifasTipos_Obtener.FieldByName('PesosPorKilometro').AsFloat > 0 then begin
                  lOkPesosXKM:=True;
                end else begin
                  lOkPesosXKM := False;
                end;
                spPlanTarifarioVersionesTarifasTipos_Obtener.Next;
              end;
              if lOkPesosXKM then begin

                Application.CreateForm(TFrmFechaActivacionVersionPlanTarifario, fBoxFechaActivacionVersionPlanTarifario);

                  if fBoxFechaActivacionVersionPlanTarifario.Inicializa(dt_FechaActivacion.Date) then begin

                    if fBoxFechaActivacionVersionPlanTarifario.ShowModal = mrOK then begin
                      dt_FechaActivacion.Date := dtfrmBoxFechaActivacionFecha;

                       if not ValidateControlsCN([dt_FechaActivacion],
                                  [(dt_FechaActivacion.Date > Date) and (not dt_FechaActivacion.IsEmpty) ],
                                  MSG_CAPTION_VALIDAR,
                                  [MSG_ERROR_FECHA_INCORRECTA ], Self) then
                        begin
                          chbHabilitado.Checked := False;
                          dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                          exit;
                        end;

                        //Validando que la Fecha y la concesionaria no existan

                        with spPlanTarifarioVersiones_FechaActivacion_Existe do begin
                          if Active then Close;
                          Parameters.Refresh;
                          Parameters.ParamByName('@CodigoConcesionaria').Value := vcbConcesionarias.Value;
                          Parameters.ParamByName('@ID_CategoriasClases').Value := vcbClaseCategorias.Value;
                          Parameters.ParamByName('@FechaActivacion').Value := dt_FechaActivacion.Date;
                          Parameters.ParamByName('@RecordCount').Value := 0;

                          ExecProc;

                          if (Parameters.ParamByName('@RecordCount').Value = 0) then
                          begin
                            if PlanTarifarioVersion_Actualizar then begin
                              MsgBox('Habilitacion de Tarifa realizada satisfactoriamente');
                              chbHabilitado.Enabled := False;
                              ActivarBotones(False,  cGrupoToolbar);
                            end;
                          end else begin
                            chbHabilitado.Checked := False;
                            dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                            MsgBox('Fecha activaci�n existe, habilitaci�n cancelada', 'Atenci�n');
                          end;
                        end;

                      end
                    else begin
                      chbHabilitado.Checked := False;
                      dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                      MsgBox('Habilitaci�n cancelada por el usuario','Atenci�n');
                    end;
                  end
                  else begin
                    chbHabilitado.Checked := False;
                    dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                    MsgBox('Error al inicializar Fecha de habilitacion. Habilitacion cancelada','Atenci�n');
                  end;
                end else begin
                chbHabilitado.Checked := False;
                dt_FechaActivacion.Date := dtFechaActivacion_OLD;
                MsgBox('Falta ingresar valor peso por kilometro. Habilitaci�n cancelada','Atenci�n');
              end;
            end
            else begin
              chbHabilitado.Checked := False;
              dt_FechaActivacion.Date := dtFechaActivacion_OLD;

              MsgBox('Habilitaci�n cancelada por el usuario','Atenci�n');
            end;
          end
          else begin
            chbHabilitado.Checked := False;
            dt_FechaActivacion.Date := dtFechaActivacion_OLD;
            MsgBox('Error al inicializar actualizacion de pesos x kilometros. Habilitacion cancelada','Atenci�n');
          end;
        except
          On E: EDataBaseError do
          begin
            chbHabilitado.Checked := False;
            dt_FechaActivacion.Date := dtFechaActivacion_OLD;
            MsgBox(E.Message, 'Error BDD');
          end;
          On E: Exception do
          begin
            chbHabilitado.Checked := False;
            dt_FechaActivacion.Date := dtFechaActivacion_OLD;
            MsgBox(E.Message, 'Error');
          end;
        end;
      finally
        ActivarBotones((not chbHabilitado.Checked), cGrupoToolbar);
        if (fFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro <> nil) then fFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro.Release;
        if (fBoxFechaActivacionVersionPlanTarifario <> nil) then fBoxFechaActivacionVersionPlanTarifario.Release;
      end;
    end;
  end;

end;

function TFormTarifasPorPuntoDeCobro.PlanTarifarioVersion_Actualizar: Boolean;
var
  spPlanTarifarioTarifasInfoPaneles_Crear: TADOStoredProc;
  vcbVersionesOld: Integer;
begin
  //actualizando la fecha de la versi�n y la habilitacion
  Result := True;
  try
    DMConnections.BaseBO_Rating.BeginTrans;
    // Obteniendo el plan habilitado actual de la concesionaria en cuestion
    with spPlanTarifarioVersionesConcesionaria_Obtener do begin
      Parameters.Refresh;
      Parameters.ParamByName('@CodigoConcesionaria').Value := vcbConcesionarias.Value;
      Parameters.ParamByName('@ID_CategoriasClases').Value := vcbClaseCategorias.Value;
      Parameters.ParamByName('@UltimoHabilitado').Value := 1;
      if Active then Close;
      Open;

      if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
      begin
        //prm_Id_Version_Activa := FieldByName('CodigoVersion').AsInteger;
      end else begin
        raise exception.Create('Problemas al obtener el Plan Tarifario Habilitado Activo');
      end;
    end;


    spPlanTarifarioVersionesTarifasTipos_Obtener := CargaTarifaTipoPesosXKM(vcbConcesionarias.Value, vcbVersiones.Value);;

    if spPlanTarifarioVersionesTarifasTipos_Obtener.IsEmpty then begin
      raise Exception.Create('Problemas al obtener las tarifa tipo de la versi�n, avise al administrador del sistema ');
    end;

    with spPlanTarifarioVersiones_Actualizar do
    begin
      Parameters.Refresh;
      Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := iDPlanTarifarioVersion;
      Parameters.ParamByName('@CodigoConcesionaria').Value := vcbConcesionarias.Value;
      Parameters.ParamByName('@CodigoVersion').Value := AnsiLeftStr(vcbVersiones.Text,10);
      Parameters.ParamByName('@ID_CategoriasClases').Value := vcbClaseCategorias.Value;
      Parameters.ParamByName('@FechaActivacion').Value := dt_FechaActivacion.Date;
      Parameters.ParamByName('@Habilitado').Value := chbHabilitado.Checked;
      Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
      ExecProc;

        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then begin

            spPlanTarifarioTarifasInfoPaneles_Crear := TADOStoredProc.Create(nil);
            with spPlanTarifarioTarifasInfoPaneles_Crear do begin
              Connection := DMConnections.BaseBO_Rating;
              ProcedureName := 'PlanTarifarioTarifasInfoPaneles_Crear';
              Parameters.Refresh;
              Parameters.ParamByName('@IDPlanTarifarioVersion').Value := iDPlanTarifarioVersion;
              Parameters.ParamByName('@FechaDesde').Value := dt_FechaActivacion.Date;
              ExecProc;

              if (Parameters.ParamByName('@RETURN_VALUE').Value <> 0) then
              begin
                MsgBox('Problemas con la carga de informaci�n para los paneles! ', 'Atenci�n');
              end;
            end;
            spPlanTarifarioTarifasInfoPaneles_Crear.Close;
            spPlanTarifarioTarifasInfoPaneles_Crear.Free;

          DMConnections.BaseBO_Rating.CommitTrans;
          vcbVersionesOld := vcbVersiones.ItemIndex;
          PlanTarifarioVersionesConcesionaria_Obtener(vcbConcesionarias.Value);
          vcbVersiones.ItemIndex := vcbVersionesOld;
          vcbVersiones.OnChange(vcbVersiones);

        end
        else begin

          raise exception.Create(MSG_ACTUALIZAFECHAVERSION_ERROR);

        end;
    end;

    spPlanTarifarioVersiones_Actualizar.Close;
    spPlanTarifarioVersiones_Actualizar.Free;

  except
    On E: EDataBaseError do
    begin
      Result := False;
      chbHabilitado.Checked := False;

      if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;

      if spPlanTarifarioVersiones_Actualizar.Active then spPlanTarifarioVersiones_Actualizar.Close;
      spPlanTarifarioVersiones_Actualizar.Free;

      MsgBox(MSG_ACTUALIZAFECHAVERSION_ERROR + ': ' + E.Message, MSG_ACTUALIZAFECHAVERSION_CAPTION);
    end;
    On E: Exception do
    begin
      Result := False;
                chbHabilitado.Checked := False;

      if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;

      if spPlanTarifarioVersiones_Actualizar.Active then spPlanTarifarioVersiones_Actualizar.Close;
      spPlanTarifarioVersiones_Actualizar.Free;

      MsgBox(MSG_ACTUALIZAFECHAVERSION_ERROR + ': ' + E.Message, MSG_ACTUALIZAFECHAVERSION_CAPTION);
    end;

  end;
end;

procedure TFormTarifasPorPuntoDeCobro.PlanTarifarioDiasTipos_Obtener;
var
  spPlanTarifarioDiasTipos_Obtener: TADOStoredProc;
  lEOF: Boolean;
begin
  vcbCodigoDiaTipo.Clear;
  vcbCodigoDiaTipo.Items.Add(MSG_TODOS, 'Todas');
  spPlanTarifarioDiasTipos_Obtener := TADOStoredProc.Create(Self);
  spPlanTarifarioDiasTipos_Obtener.Close;
  with spPlanTarifarioDiasTipos_Obtener do begin
    Connection := DMConnections.BaseBO_Rating;
    ProcedureName := 'PlanTarifarioDiasTipos_Obtener';
    Parameters.Refresh;
    if Active then Close;
      Open;
    lEOF := not EOF;
    if not EOF then begin
      while not EOF do begin
          vcbCodigoDiaTipo.Items.Add(
                FieldByName('Descripcion').AsString,
                FieldByName('CodigoDiaTipo').AsString );
        Next;
      end;
    end else begin
      vcbCodigoDiaTipo.Clear;
    end;
  end;
  spPlanTarifarioDiasTipos_Obtener.Close;
  spPlanTarifarioDiasTipos_Obtener.Free;
  if lEOF then vcbCodigoDiaTipo.ItemIndex := 0;

end;

procedure TFormTarifasPorPuntoDeCobro.PlanTarifarioTarifasPuntosCobro_Obtener;
begin
  try
    if iDPlanTarifarioVersion > 0 then begin
      spPlanTarifarioTarifasPuntosCobro_Obtener.DisableControls;
    with spPlanTarifarioTarifasPuntosCobro_Obtener do begin

        if Active then Close;
        Parameters.Refresh;
        Parameters.ParamByName('@NumeroPuntoCobro').Value := vcbPuntoCobro.Value;
        Parameters.ParamByName('@CodigoCategoriasClases').Value := vcbClaseCategorias.Value;
        Parameters.ParamByName('@Categoria').Value := vcbCategoria.Value;

          If vcbCodigoDiaTipo.Value <> 'Todas' then begin
              Parameters.ParamByName('@CodigoDiaTipo').Value := vcbCodigoDiaTipo.Value;
          end;

        Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := iDPlanTarifarioVersion;

        Open;

          if not Eof then
            lblShowTotalRegistros.Caption := IntToStr(RecordCount)
          else lblShowTotalRegistros.Caption := '0';
    end;

      dblListaPlanTarifarioPuntoCobro.Refresh;
      dblListaPlanTarifarioPuntoCobro.Show;
      spPlanTarifarioTarifasPuntosCobro_Obtener.EnableControls;
      if (FEdicion = cEdicionEditar) then begin
        dblListaPlanTarifarioPuntoCobro.SelectedIndex := FSelectedIndexdbl;
      end;
    end else begin
      spPlanTarifarioTarifasPuntosCobro_Obtener.Close;
    end;
  except
    On E: Exception do begin
      MsgBox(E.message, 'Error', MB_ICONSTOP);
      end;
  end;
end;

procedure TFormTarifasPorPuntoDeCobro.PlanTarifarioTarifasTipos_Obtener;
var
  spPlanTarifarioTarifasTipos_Obtener: TADOStoredProc;
  lEOF: Boolean;
begin
  vcbCodigoTarifaTipo.Clear;
  vcbCodigoTarifaTipo.Items.Add(MSG_SELECCIONE, 'Nuevo');
  spPlanTarifarioTarifasTipos_Obtener := TADOStoredProc.Create(Self);
  spPlanTarifarioTarifasTipos_Obtener.Close;
  with spPlanTarifarioTarifasTipos_Obtener do begin
    Connection := DMConnections.BaseBO_Rating;
    ProcedureName := 'PlanTarifarioTarifasTipos_Obtener';
    Parameters.Refresh;
    if Active then Close;
      Open;
    lEOF := not EOF;
    if not EOF then begin
      while not EOF do begin
        vcbCodigoTarifaTipo.Items.Add(
                FieldByName('Descripcion').AsString,
                FieldByName('CodigoTarifaTipo').AsString);

        if FieldByName('CodigoTarifaTipo').AsString = 'TBFP' then begin
          if FieldByName('Color').AsString = '' then begin
            MsgBox('Falta definir color para el tipo tarifa TBFP','Atenci�n');
          end else begin
          pnl_color_Normal_TBFP.Color := StrToInt('$'+ FieldByName('Color').AsString);
        end;
        end;

        if FieldByName('CodigoTarifaTipo').AsString = 'TBP' then begin
          if FieldByName('Color').AsString = '' then begin
            MsgBox('Falta definir color para el tipo tarifa TBP','Atenci�n');
          end else begin
          pnl_color_Punta_TBP.Color := StrToInt('$'+ FieldByName('Color').AsString);
        end;
        end;

        if FieldByName('CodigoTarifaTipo').AsString = 'TS' then begin
          if FieldByName('Color').AsString = '' then begin
            MsgBox('Falta definir color para el tipo tarifa TS','Atenci�n');
          end else begin
          pnl_color_Congestion_TS.Color := StrToInt('$'+ FieldByName('Color').AsString);
        end;
        end;

        Next;
      end;
    end else begin
      vcbCodigoTarifaTipo.Clear;
    end;
  end;
  spPlanTarifarioTarifasTipos_Obtener.Close;
  spPlanTarifarioTarifasTipos_Obtener.Free;
  if lEOF then vcbCodigoTarifaTipo.ItemIndex := 0;

end;
}
{$ENDREGION}
Function TFormTarifasPorPuntoDeCobro.CargaTarifaTipoPesosXKM(_vcbConcesionariaValue, _vcbVersionesValue: Integer): TADOStoredProc;
var
  _spPlanTarifarioVersionesTarifasTipos_Obtener: TADOStoredProc;
begin
  _spPlanTarifarioVersionesTarifasTipos_Obtener := TADOStoredProc.Create(nil);
  _spPlanTarifarioVersionesTarifasTipos_Obtener.Close;

  if (_vcbConcesionariaValue > 0) and (_vcbVersionesValue > 0) then begin

    with _spPlanTarifarioVersionesTarifasTipos_Obtener do begin
        Connection := DMConnections.BaseBO_Rating;
        ProcedureName := 'PlanTarifarioVersionesTarifasTipos_Obtener';
        Parameters.Refresh;

        Parameters.ParamByName('@ID_Concesionaria').Value := _vcbConcesionariaValue;
        Parameters.ParamByName('@ID_PlanTarifarioVersion').Value := _vcbVersionesValue;
        if Active then Close;
      Open;
    end;
  end;
  Result := _spPlanTarifarioVersionesTarifasTipos_Obtener;
end;

procedure TFormTarifasPorPuntoDeCobro.ExportarToCSV; //(dbl_Grid: TDBListEx; BDD: String);
resourcestring
    MSG_TITLE   = 'Exportar CSV';
    MSG_ERROR   = 'ERROR';
    MSG_SUCCESS = 'El archivo %s fu� creado exitosamente';

    MSG_EOF = 'No existen datos';
    MSG_TITLE_EMPTY   = 'Atenci�n';

const
    cCamposCOP = '';
    cCamposCAC = '';
    DELIMITADOR_EXCEL = '"';
    SEPARADOR_EXCEL = ';';

    function FormatearCampoExcel(Campo: TField): string;
    begin
        result := '';
        //if Campo.IsNull then exit;                          //TASK_109_JMA_20170215
        if not Assigned(Campo) then exit;                     //TASK_109_JMA_20170215
        case Campo.DataType of
            ftUnknown, ftString, ftFixedChar, ftWideString, ftBytes, ftVarBytes, ftLargeint, ftVariant, ftGuid:
                result := result + DELIMITADOR_EXCEL + stringreplace(Campo.AsString, DELIMITADOR_EXCEL, DELIMITADOR_EXCEL + DELIMITADOR_EXCEL, [rfReplaceAll]) + DELIMITADOR_EXCEL;

            ftSmallint, ftInteger, ftWord, ftAutoInc:
                result := result + Campo.AsString;

            ftBoolean:
                result := result + Campo.AsString;

            ftFloat, ftCurrency, ftBCD, ftFMTBcd:
                result := result + Campo.AsString;

            ftDate:
                result := result + FormatDateTime('dd/mm/yyyy', Campo.AsDateTime);

            ftTime:
                result := result + FormatDateTime('hh:nn:ss', Campo.AsDateTime);

            ftDateTime, ftTimeStamp:
                begin
                    if trunc(Campo.AsDateTime) = 0 then begin
                        // es una hora
                        result := result + FormatDateTime('hh:nn:ss', Campo.AsDateTime);
                    end else if frac(Campo.AsDateTime) = 0 then begin
                        // es una fecha
                        result := result + FormatDateTime('dd/mm/yyyy', Campo.AsDateTime);
                    end else begin
                        // es una fecha completa
                        result := result + FormatDateTime('dd/mm/yyyy hh:nn:ss', Campo.AsDateTime);
                    end;
                end;

            ftBlob, ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob, ftInterface, ftIDispatch:
                result := result + 'Tipo de campo no soportado';
        end;
    end;

    // Dataset := es el conjunto de datos a exportar
    // lnewFileBuffer := Si es TRUE crea un nuevo FileBuffer, si es FALSE concatena el FileBuffer
    // FileBuffer := es el FileBuffer que contendra la data a exportar
    function ExportToCsv(Dataset: TDataset; lnewFileBuffer: Boolean; var FileBuffer: string; var Error: string; cCampos: String): boolean;
    var
        i: integer;
    begin
        result := false;
        try
            if lnewFileBuffer then FileBuffer := '' else FileBuffer := FileBuffer + CRLF;

            with Dataset do begin
                // Pongo las cabeceras
                for i := 0 to FieldCount - 1 do begin
                  if cCampos <> '' then begin
                    if Pos(Fields[i].FieldName,cCampos) > 0 then begin
                        FileBuffer := FileBuffer + AnsiQuotedStr(Fields[i].FieldName, DELIMITADOR_EXCEL) + SEPARADOR_EXCEL;
                    end;
                  end else begin
                    FileBuffer := FileBuffer + AnsiQuotedStr(Fields[i].FieldName, DELIMITADOR_EXCEL) + SEPARADOR_EXCEL;
                  end;
                end;
                //FileBuffer := FileBuffer + AnsiQuotedStr(Fields[FieldCount - 1].FieldName, DELIMITADOR_EXCEL) + CRLF;
                FileBuffer := FileBuffer + CRLF;

                // Insertamos los datos
                First;
                while not eof do begin
                    for i := 0 to FieldCount - 1 do begin
                        if cCampos <> '' then begin
                          if Pos(Fields[i].FieldName, cCampos) > 0 then begin
                            FileBuffer := FileBuffer + FormatearCampoExcel(Fields[i]) + SEPARADOR_EXCEL;
                          end;
                        end else begin
                          FileBuffer := FileBuffer + FormatearCampoExcel(Fields[i]) + SEPARADOR_EXCEL;
                        end;
                    end;
                    //FileBuffer := FileBuffer + FormatearCampoExcel(Fields[FieldCount - 1]) + CRLF;
                    FileBuffer := FileBuffer + CRLF;
                    next;
                end;
            end;
            result := true;
        except
            on e: exception do begin
                Error := e.message;
            end;
        end;
    end;

var
    FileBuffer, Error:  String;
begin

      if not dblListaPlanTarifarioPuntoCobro.DataSource.Dataset.IsEmpty then
      begin
        //spPlanTarifarioTarifasPuntosCobro_Obtener.DisableControls;                    //TASK_109_JMA_20170215
        oPlanes.oTarifas.ClientDataSet.DisableControls;   //TASK_109_JMA_20170215


          if ExportToCsv(dblListaPlanTarifarioPuntoCobro.DataSource.DataSet, False, FileBuffer, Error, '') then begin
{INICIO: TASK_109_JMA_20170215
            sdGuardarCSV.FileName := 'PlanTarifarioTPC_' + Trim(String(vcbConcesionarias.Value))+'_'+ Trim(AnsiLeftStr(vcbVersiones.Text,10))+'_' + StringReplace(DateTimeToStr(Now),':','-',[rfReplaceAll]);
}
            sdGuardarCSV.FileName := 'PlanTarifarioTPC_' + IntToStr(cbbConcesionaria.Value)+'_'+ IntToStr(oPlanes.CodigoVersion)+'_' + StringReplace(StringReplace(DateTimeToStr(Now),':','-',[rfReplaceAll]),'/','-',[rfReplaceAll]);
{TERMINO: TASK_109_JMA_20170215}
            if sdGuardarCSV.Execute then begin
                StringToFile(FileBuffer, sdGuardarCSV.FileName);
                MsgBox(Format(MSG_SUCCESS, [sdGuardarCSV.FileName]), MSG_TITLE, MB_ICONINFORMATION);
            end;
          end;

      end else begin
        MsgBox(Format(MSG_EOF, [sdGuardarCSV.FileName]), MSG_TITLE_EMPTY, MB_ICONINFORMATION);
      end;

      oPlanes.oTarifas.ClientDataSet.EnableControls;      //TASK_109_JMA_20170215
      //spPlanTarifarioTarifasPuntosCobro_Obtener.EnableControls;                       //TASK_109_JMA_20170215
end;


{INICIO: TASK_109_JMA_20170215}
function TFormTarifasPorPuntoDeCobro.Inicializa: Boolean;
Var
    col: TDBListExColumn;
    CodigoConcesionaria: Byte;
begin

    oPlanes:= TPlanTarifarioVersion.Create;
    oPlanes.oTarifas := TPlanTarifarioTarifasPuntosCobro.Create;
    FMulticoncesionHabilitada := ObtenerParametroGeneralMulticoncesionHabilitada();
    CodigoConcesionaria := ObtenerCodigoConcesionariaNativa(DMConnections.BaseCAC);
    FFechaActual := NowBase(DMConnections.BaseCAC);

    if FMulticoncesionHabilitada then
    begin
        pnlConcesionaria.Visible := True;
        col := dblPlanesVersiones.Columns.Insert(0);
        col.Width := 200;
        col.Header.Caption := 'Concesionaria';
        col.Header.Alignment := taCenter;
        col.FieldName := 'Concesionaria';
    end;

    CargarComboConcesionarias();
    CargarComboCategoriasClases();
    CargarComboTiposDias();

    cbbConcesionaria.Value := CodigoConcesionaria;
    cbbConcesionaria_Change(nil);//Esto para que se cree el objeto puntos de cobro

    pbSinAsignar.Canvas.Brush.Color := clWhite;
    PaintBoxHorario.Canvas.Rectangle(0, 0, pbSinAsignar.Width, pbSinAsignar.Height);
    pbSinAsignar.Canvas.TextOut(0,0, 'Sin Asignar');

    Result := True;
end;

procedure TFormTarifasPorPuntoDeCobro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if (nbGuardar.ActivePage = 'PagGuardar') then
    begin
        if Application.MessageBox(PChar('�Desea salir sin guardar los cambios?'),'Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) <> mrYes then
        begin
           Action := caNone;
           exit
        end;
    end;

    if Assigned(dblPlanesVersiones.DataSource) then
        dblPlanesVersiones.DataSource.Free;
    if Assigned(dblListaPlanTarifarioPuntoCobro.DataSource) then
        dblListaPlanTarifarioPuntoCobro.DataSource.Free;
    FreeAndNil(oPlanes);
    FreeAndNil(oCategorias);
    FreeAndNil(oConcesionarias);
    FreeAndNil(oClasesCategorias);
    FreeAndNil(oTiposDias);
    FreeAndNil(oTiposTarifas);
    FreeAndNil(oPuntosCobros);
    FreeAndNil(fGaps);
    Action := caFree;

end;

procedure TFormTarifasPorPuntoDeCobro.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{$REGION 'Carga y Filtro'}

procedure TFormTarifasPorPuntoDeCobro.CargarComboConcesionarias();
begin
    cbbConcesionaria.Enabled := False;
    cbbConcesionaria.Items.Clear;

    try
        try
            oConcesionarias := TConcesionaria.Create;
            oConcesionarias.Obtener();
            oConcesionarias.ClientDataSet.First;
            while not oConcesionarias.ClientDataSet.Eof do
            begin
                cbbConcesionaria.Items.InsertItem(oConcesionarias.Descripcion, oConcesionarias.CodigoConcesionaria);
                oConcesionarias.ClientDataSet.Next;
            end;     
        except
            on e:Exception do
                MsgBoxErr('Error cargando Combo Concesionarias', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        if cbbConcesionaria.Items.Count > 0 then
            cbbConcesionaria.ItemIndex:= 0;
        cbbConcesionaria.Enabled := True;
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.CargarComboCategoriasClases();
begin
    cbbClaseCategorias.Enabled := False;
    cbbClaseCategorias.Items.Clear;

    try
        try
            oClasesCategorias:= TCategoriaClase.Create;
            oClasesCategorias.Obtener();
            oClasesCategorias.ClientDataSet.First;
            While not oClasesCategorias.ClientDataSet.Eof do
            begin
                cbbClaseCategorias.Items.InsertItem(oClasesCategorias.Descripcion, oClasesCategorias.Id_CategoriasClases);
                if UpperCase(oClasesCategorias.Descripcion) = 'INTERURBANA' then
                    FCodigoCategoriaInterurbana := oClasesCategorias.Id_CategoriasClases;
                oClasesCategorias.ClientDataSet.Next;
            end;
            
        except
            on e:Exception do
                MsgBoxErr('Error cargando Combo Categorias Clases', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        cbbClaseCategorias.ItemIndex:= 0;
        cbbClaseCategorias.Enabled := True;
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.CargarComboTiposDias();
begin
    cbbTipoDia.Enabled := False;
    cbbTipoDia.Items.Clear;
    cbbTipoDia.Items.InsertItem('Todos', '');
    try
        try
            
            oTiposDias:= TTarifasDiasTipos.Create();
            oTiposDias.Obtener();
            oTiposDias.ClientDataSet.First;
            while not oTiposDias.ClientDataSet.Eof do
            begin
                cbbTipoDia.Items.InsertItem(oTiposDias.Descripcion, oTiposDias.CodigoDiaTipo);
                oTiposDias.ClientDataSet.Next;
            end;

        except
            on e:Exception do
                MsgBoxErr('Error cargando Tipos de Dias', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        cbbTipoDia.ItemIndex:= 0;
        cbbTipoDia.Enabled := True;
    end;

end;


procedure TFormTarifasPorPuntoDeCobro.cbbConcesionaria_Change(Sender: TObject);
begin
    try
        if oPuntosCobros = nil then
            oPuntosCobros := TPuntoCobro.Create;
        oPuntosCobros.Obtener_from_BO_Raiting(cbbConcesionaria.Value);
        cbbCategoriaClase_Changed(nil);
    except
        on e: Exception do
            MsgBoxErr('Error cargando puntos de cobro', e.Message, 'Error de carga', MB_ICONSTOP);
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.cbbCategoriaClase_Changed(Sender: TObject);
begin
    CargarComboPuntosCobro();
    CargarComboCategorias();
    CargarComboTiposTarifas();
    CargarGrillaPlanesVersiones();
end;

procedure TFormTarifasPorPuntoDeCobro.CargarComboPuntosCobro();
begin

    cbbPuntoCobro.Enabled := False;
    try
        try

            cbbPuntoCobro.Items.Clear;
            cbbPuntoCobro.Items.InsertItem('Todos', 0);

            oPuntosCobros.ClientDataSet.filter := ' IDCategoriaClase = ' + QuotedStr(IntToStr(cbbClaseCategorias.Value));
            oPuntosCobros.ClientDataSet.First;
            while not oPuntosCobros.ClientDataSet.Eof do
            begin
                cbbPuntoCobro.Items.InsertItem(oPuntosCobros.Descripcion, oPuntosCobros.NumeroPuntoCobro);
                oPuntosCobros.ClientDataSet.Next;
            end;

        except
            on e:Exception do
                MsgBoxErr('Error cargando Puntos de Cobro', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        if cbbPuntoCobro.Items.Count > 0 then
            cbbPuntoCobro.ItemIndex:= 0;
        cbbPuntoCobro.Enabled := True;
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.CargarComboCategorias();
begin

    cbbCategorias.Enabled := False;
    try
        try
            if oCategorias = nil then
            begin
                oCategorias:= TCategoria.Create();
                oCategorias.ObtenerCategorias(0, '', 0);
            end;

            cbbCategorias.Items.Clear;
            cbbCategorias.Items.InsertItem('Todos', 0);

            oCategorias.ClientDataSet.Filter := ' IDCategoriaClase = ' + QuotedStr(IntToStr(cbbClaseCategorias.Value));
            oCategorias.ClientDataSet.First;
            while not oCategorias.ClientDataSet.Eof do
            begin
                cbbCategorias.Items.InsertItem(oCategorias.Descripcion, oCategorias.CodigoCategoria);
                oCategorias.ClientDataSet.Next;
            end;
                            
        except
            on e:Exception do
                MsgBoxErr('Error cargando Categorias', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        if cbbCategorias.Items.Count > 0 then
            cbbCategorias.ItemIndex := 0;
        cbbCategorias.Enabled := True;
    end;
    
end;

procedure TFormTarifasPorPuntoDeCobro.CargarComboTiposTarifas();
begin
    cbbTipoTarifa.Enabled := False;

    try
        try

            if oTiposTarifas = nil then
            begin
                oTiposTarifas:= TTarifasTipos.Create();
                oTiposTarifas.Obtener(True, Null);
            end;

            cbbTipoTarifa.Items.Clear;           
            oTiposTarifas.ClientDataSet.Filter := ' Id_CategoriasClases = ' + QuotedStr(InttoStr(cbbClaseCategorias.value));

            if (cbbClaseCategorias.value <> FCodigoCategoriaInterurbana) then
                cbbTipoTarifa.Items.InsertItem('Todos', '');

            oTiposTarifas.ClientDataSet.First;
            while not oTiposTarifas.ClientDataSet.Eof do
            begin
                cbbTipoTarifa.Items.InsertItem(oTiposTarifas.Descripcion, oTiposTarifas.CodigoTarifaTipo);
                oTiposTarifas.ClientDataSet.Next;
            end;

        except
            on e:Exception do
                MsgBoxErr('Error cargando Tipos de Tarifas', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        if cbbTipoTarifa.Items.Count > 0 then
            cbbTipoTarifa.ItemIndex:= 0;
        cbbTipoTarifa.Enabled := True;
    end;
        
end;

procedure TFormTarifasPorPuntoDeCobro.CargarGrillaPlanesVersiones();
begin
    if Assigned(dblPlanesVersiones.DataSource) then
       dblPlanesVersiones.DataSource.Free;

    if cbbConcesionaria.Enabled and cbbClaseCategorias.Enabled then        //Si no estoy cargando los combos
    begin

        dblPlanesVersiones.Enabled := False;

        try
            try
                dblPlanesVersiones.DataSource := TDataSource.Create(nil);
                dblPlanesVersiones.DataSource.DataSet := oPlanes.Obtener(cbbConcesionaria.Value, cbbClaseCategorias.Value);
                oPlanes.ClientDataSet.AfterScroll:= dblPlanesVersiones_Changed;
            except
                on e:Exception do
                    MsgBoxErr('Error cargando Concesionarias', e.Message, 'Error de carga', MB_ICONSTOP);
            end;
        finally
            dblPlanesVersiones.Enabled := True;
            if oPlanes.ClientDataSet.RecordCount > 0 then
                oPlanes.ClientDataSet.First;
        end;  
        
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.dblPlanesVersionesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.Header.Caption = 'Habilitado' then
    begin
        if Text = 'True' then
            Text := 'Activado'
        else
            Text := 'Activar';
    end
    else if Column.Header.Caption = 'Fecha Autorizaci�n MOP' then
    begin
        if Text = '' then
            Text := 'Ingresar'
    end;
end;



procedure TFormTarifasPorPuntoDeCobro.dblPlanesVersiones_Changed(Sender: TDataSet);
begin
    if dblPlanesVersiones.Enabled then                          //Si no estoy cargando el dblist
    begin
        if Assigned(oPlanes.ClientDataSet) and (oPlanes.ClientDataSet.RecordCount>0) then
        begin
            if (oPlanes.FechaActivacion < FFechaActual) and oPlanes.Habilitado then
                PermitirEdicion(False)
            else
                PermitirEdicion(True);

            if not oPlanes.Habilitado then
                btnEliminarPlan.Enabled := True
            else
                btnEliminarPlan.Enabled := False;
        end;
                       
        CargarGrillaPlanTarifarioPuntoCobro();

    end;
end;

procedure TFormTarifasPorPuntoDeCobro.dblListaPlanTarifarioPuntoCobroDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if oPlanes.oTarifas.ClientDataSet.Fields[0].Tag = 0 then
        dblListaPlanTarifarioPuntoCobro.Canvas.font.color := clWindowText
    else
        dblListaPlanTarifarioPuntoCobro.Canvas.font.color := clRed;
end;

procedure TFormTarifasPorPuntoDeCobro.PermitirEdicion(accion: Boolean);
begin
    btnInserta.Enabled := accion;
    btnElimina.Enabled := accion;
    btnModifica.Enabled := accion;
    btnEditarPlan.Enabled := accion;
end;

procedure TFormTarifasPorPuntoDeCobro.CargarGrillaPlanTarifarioPuntoCobro();
begin
    dblListaPlanTarifarioPuntoCobro.Enabled := False;

    if Assigned(dblListaPlanTarifarioPuntoCobro.DataSource) then
       dblListaPlanTarifarioPuntoCobro.DataSource.Free;

    try
        if (oPlanes.ClientDataSet.RecordCount>0) then
        begin

            try
                dblListaPlanTarifarioPuntoCobro.DataSource := TDataSource.Create(nil);
                dblListaPlanTarifarioPuntoCobro.DataSource.DataSet := oPlanes.oTarifas.Obtener(oPlanes.ID_PlanTarifarioVersion, 0, 0, oPlanes.ID_CategoriasClases, '');
                oPlanes.oTarifas.ClientDataSet.First;
                oPlanes.oTarifas.ClientDataSet.Filter := 'Estado <> ' + QuotedStr('3');
                oPlanes.oTarifas.ClientDataSet.AfterScroll := PaintBoxHorario_Paint;
            except
                on e:Exception do
                    MsgBoxErr('Error cargando Planes Puntos de Cobro', e.Message, 'Error de carga', MB_ICONSTOP);
            end;

        end;
    finally
        dblListaPlanTarifarioPuntoCobro.Enabled := True;
        lblShowTotalRegistros.Caption := IntToStr(oPlanes.oTarifas.ClientDataSet.RecordCount);
//Esto para que no filtre al cambiar los indices-------
        cbbTipoDia.Enabled := False;
        cbbTipoDia.ItemIndex := 0;
        cbbTipoDia.Enabled := True; 
 //-----------------------------------------------------
        PaintBoxHorario_Paint(nil);
    end;
    
end;

procedure TFormTarifasPorPuntoDeCobro.cbb_PuntoCobro_Categoria_TipoDia_TipoTarifa_Changed(Sender: TObject);
var
    filtro: string;
begin
    if Assigned(oPlanes.oTarifas.ClientDataSet)
        and cbbPuntoCobro.Enabled and cbbCategorias.Enabled and cbbTipoDia.Enabled and cbbTipoTarifa.Enabled then
    begin
        filtro := 'Estado <> ' + QuotedStr('3');        //Estado distinto de eliminado
        if cbbPuntoCobro.Value <> 0 then
        begin
            filtro := filtro + ' and NumeroPuntoCobro = ' + QuotedStr(string(cbbPuntoCobro.Value));
        end;

        if cbbCategorias.Value <> 0 then
        begin
            if filtro <> '' then
                filtro := filtro + ' and ';
             filtro := filtro + ' Categoria = ' + QuotedStr(string(cbbCategorias.Value));
        end;

        if cbbTipoDia.Value <> '' then
        begin
            if filtro <> '' then
                filtro := filtro + ' and ';
            filtro := filtro + ' CodigoDiaTipo = ' + QuotedStr(cbbTipoDia.Value);
        end;

        if cbbTipoTarifa.Value <> '' then
        begin
            if filtro <> '' then
                filtro := filtro + ' and ';
            filtro := filtro + ' CodigoTarifaTipo = ' + QuotedStr(cbbTipoTarifa.Value);
        end;

        oPlanes.oTarifas.ClientDataSet.Filter := filtro;
        lblShowTotalRegistros.Caption := IntToStr(oPlanes.oTarifas.ClientDataSet.RecordCount);
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.FormResize(Sender: TObject);
begin
    PaintBoxHorario.Invalidate;
end;

procedure TFormTarifasPorPuntoDeCobro.PaintBoxHorario_Paint(Sender: TDataSet);
Var
	horarioDesde: TDateTime;
  horarioHasta: TDateTime;
	posicionDesde, posicionHasta, porcentajeHoraDesde, porcentajeHoraHasta : integer;
    m, td: string;
    npc, cat, i: Integer;
    bm: TBookmark;
begin

    PaintBoxHorario.Canvas.Pen.Style := psClear;
    PaintBoxHorario.Canvas.Brush.Style := bsClear;
    PaintBoxHorario.Canvas.Brush.Color := clWindow;
    PaintBoxHorario.Canvas.Rectangle(0, 0, PaintBoxHorario.Width,PaintBoxHorario.Height);

    if not oPlanes.oTarifas.ClientDataSet.ControlsDisabled
        and (oPlanes.oTarifas.ClientDataSet.RecordCount > 0) then
    begin
        try
            try 

                if oPlanes.oTarifas.ClientDataSet.State <> dsInactive then
                begin

                    oPlanes.oTarifas.ClientDataSet.DisableControls;
                    oPlanes.oTarifas.ActivarAfterScroll(False);
                    m:= oPlanes.oTarifas.ClientDataSet.Bookmark;

                    npc := oPlanes.oTarifas.NumeroPuntoCobro;
                    cat := oPlanes.oTarifas.Categoria;
                    td := oPlanes.oTarifas.CodigoDiaTipo;

                    for I := 1 to 2 do                //TBFP siempre debe dibujarse primero
                    begin
                        oPlanes.oTarifas.ClientDataSet.First;
                        while not oPlanes.oTarifas.ClientDataSet.EOF do
                        begin
                    
                            if (npc = oPlanes.oTarifas.NumeroPuntoCobro)
                                and (cat = oPlanes.oTarifas.Categoria)
                                and (td = oPlanes.oTarifas.CodigoDiaTipo) then
                            begin
                                horarioDesde := StrToTime(oPlanes.oTarifas.HoraDesde) * 24;
                                horarioHasta := StrToTime(oPlanes.oTarifas.HoraHasta) * 24;

                                porcentajeHoraDesde := Round(horarioDesde / 24 * 100);
                                porcentajeHoraHasta := Round(horarioHasta / 24 * 100);

                                posicionDesde := Round(porcentajeHoraDesde * PaintBoxHorario.Width /100);
                                posicionHasta := Round(porcentajeHoraHasta * PaintBoxHorario.Width /100);

                                PaintBoxHorario.Canvas.Brush.Color := clWhite;

                                if ((i=1) and ((oPlanes.oTarifas.HoraDesde = '00:00') and (oPlanes.oTarifas.HoraHasta = '23:59')))
                                    or
                                    ((i<>1) and not ((oPlanes.oTarifas.HoraDesde = '00:00') and (oPlanes.oTarifas.HoraHasta = '23:59'))) then
                                begin
                                    bm := oTiposTarifas.BuscarEnClientDataSet('CodigoTarifaTipo', oPlanes.oTarifas.CodigoTarifaTipo);
                                    if oTiposTarifas.ClientDataSet.BookmarkValid(bm) then
                                    begin
                                        oTiposTarifas.ClientDataSet.GotoBookmark(bm);
                                        PaintBoxHorario.Canvas.Brush.Color := StrToInt('$'+ oTiposTarifas.Color);

                                        if PaintBoxHorario.Canvas.Brush.Color <> clWhite then
                                            PaintBoxHorario.Canvas.Rectangle(posicionDesde, 0, posicionHasta, PaintBoxHorario.Height);

                                        PaintBoxHorario.Canvas.TextOut(posicionDesde, 0, oTiposTarifas.CodigoTarifaTipo)
                                    end;
                                end;

                            end;

                            oPlanes.oTarifas.ClientDataSet.Next;
                        end;
                    end;
                end;      

            except
                on E: Exception do begin
                    MsgBox( E.Message, 'Error al dibujar franja horaria' );
                end;
            end
        finally
            oPlanes.oTarifas.ClientDataSet.Bookmark := m;
            oPlanes.oTarifas.ClientDataSet.EnableControls;
            oPlanes.oTarifas.ActivarAfterScroll(True);
            if Assigned(bm) and oTiposTarifas.ClientDataSet.BookmarkValid(bm) then
                oTiposTarifas.ClientDataSet.FreeBookmark(bm);
        end;
    end;
end;

{$ENDREGION}


{$REGION 'Edici�n de Planes y Versiones'}
procedure TFormTarifasPorPuntoDeCobro.btnCrearVersionClick(Sender: TObject);
begin
    EditarCrearPlan(1);           //Nuevo
end;

procedure TFormTarifasPorPuntoDeCobro.btnCopiarPlanClick(Sender: TObject);
begin
    EditarCrearPlan(2);            //Copiar
end;

procedure TFormTarifasPorPuntoDeCobro.btnEditarPlanClick(Sender: TObject);
begin
    if oPlanes.ClientDataSet.RecordCount > 0 then
    begin
        if (oPlanes.FechaActivacion <= FFechaActual) and oPlanes.Habilitado then
            MsgBox('No se puede editar planes ya iniciados')
        else
            EditarCrearPlan(3);    //Modificar
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.btnEliminarPlanClick(Sender: TObject);
begin
    if MsgBox('�Confirma eliminar el Plan: ' + IntToStr(oPlanes.CodigoVersion) + '?', 'Atenci�n!!!', MB_YESNO) = mrYes then
    begin

        try
            oPlanes.Eliminar(oPlanes.ID_PlanTarifarioVersion, UsuarioSistema);
            CargarGrillaPlanesVersiones();
        except
            on e: Exception do
                MsgBoxErr('Error eliminando plan', e.Message, 'Error', MB_ICONSTOP);
        end;

    end;
end;



procedure TFormTarifasPorPuntoDeCobro.btnExpToExcelClick(Sender: TObject);
begin
     ExportarToCSV();
end;

procedure TFormTarifasPorPuntoDeCobro.EditarCrearPlan(accion: Byte);
var
  formPTV: TFormPlanTarifarioVersionesCrear;
begin

    dblPlanesVersiones.Enabled := False;

    try

        Application.CreateForm(TFormPlanTarifarioVersionesCrear, formPTV);

        if formPTV.Inicializa(FMulticoncesionHabilitada, cbbConcesionaria.Value, oConcesionarias, oClasesCategorias, oPlanes, accion, FFechaActual) then
        begin

            if formPTV.ShowModal = mrOK then begin

                if accion = 1 then              //Nuevo
                begin
                    CrearNuevo(formPTV.cbbConcesionaria, formPTV.cbbClaseCategorias, formPTV.edtFechaActivacion.Date, formPTV.cbbEsquema.value);
                end
                else if accion = 2 then          //Copiar
                begin
                    CrearCopia(formPTV.cbbConcesionaria, formPTV.cbbClaseCategorias, formPTV.edtFechaActivacion.Date);
                end
                else                             //Modificar
                begin

                    oPlanes.oTarifas.ActivarAfterScroll(False);

                    if oPlanes.Actualizar(oPlanes.ID_PlanTarifarioVersion,
                        formPTV.edtFechaActivacion.Date,
                        oPlanes.oTarifas,
                        UsuarioSistema) then
                    begin
                        oPlanes.FechaActivacion := formPTV.edtFechaActivacion.Date;
                        oPlanes.ClientDataSet.Post;
                    end;

                end;

            end;
        end;

    finally
        oPlanes.oTarifas.ActivarAfterScroll(True);
        FreeAndNil(formPTV);
        dblPlanesVersiones.Enabled := True;
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.CrearPlan(Concesionaria: TVariantComboBox; CategoriaClase: TVariantComboBox; FechaActivacion: TDateTime);
begin
    oPlanes.ClientDataSet.Append;
    oPlanes.ID_PlanTarifarioVersion := 0;
    oPlanes.CodigoConcesionaria := Concesionaria.Value;
    oPlanes.Concesionaria := Concesionaria.Text;
    oPlanes.ID_CategoriasClases := CategoriaClase.Value;
    oPlanes.CategoriaClase := CategoriaClase.Text;
    oPlanes.Habilitado := False;
    oPlanes.FechaActivacion := FechaActivacion;
    oPlanes.UsuarioCreacion := UsuarioSistema;
    oPlanes.FechaHoraCreacion := FFechaActual;
end;

procedure TFormTarifasPorPuntoDeCobro.CrearNuevo(Concesionaria: TVariantComboBox; CategoriaClase: TVariantComboBox; FechaActivacion: TDateTime; CodigoEsquema: Integer);
var
    oBandasHorarias: TBandaHoraria;
    oPuntosCobro: TPuntoCobro;
    oIPC : TIPC;
    i: Integer;
begin
     if cbbClaseCategorias.Value <> CategoriaClase.value then
        cbbClaseCategorias.Value := CategoriaClase.value;
     cbbCategoriaClase_Changed(nil);

     try
         oPlanes.ActivarAfterScroll(false);

         CrearPlan(Concesionaria, CategoriaClase, FechaActivacion);
         FreeAndNil(oPlanes.oTarifas.ClientDataSet);

         oPlanes.oTarifas.Obtener(-1); //Esto me crea el ClientDataSet Vacio pues no hay registros con -1

         if CodigoEsquema > 0 then
         begin
            try
                oIPC:= TIPC.Create;
                oIPC.Obtener(Year(oPlanes.FechaActivacion));

                if oIPC.ClientDataSet.RecordCount = 0 then
                    MsgBox('No se pudo generar las tarifas automaticamente porque el a�o de activaci�n no tiene IPC calculado')
                else
                begin
                    oIPC.ClientDataSet.First;

                    for i:=0 to oIPC.ClientDataSet.Fields.Count -1 do
                    begin
                        if (oIPC.ClientDataSet.Fields[i].FieldName <> 'Anio') and (oIPC.ClientDataSet.Fields[i].FieldName <> 'IPC') then
                        begin
                            if oPlanes.ClientDataSet.FieldByName(oIPC.ClientDataSet.Fields[i].FieldName) <> nil then
                               oPlanes.ClientDataSet.FieldByName(oIPC.ClientDataSet.Fields[i].FieldName).Value := oIPC.ClientDataSet.Fields[i].Value;
                        end;
                    end;

                    oBandasHorarias:= TBandaHoraria.Create;
                    oBandasHorarias.Obtener(CodigoEsquema);

                    oPuntosCobro:= TPuntoCobro.Create;
                    oPuntosCobro.Obtener_from_BO_Raiting(Concesionaria.value);
                    oPuntosCobro.ClientDataSet.Filter := 'IDCategoriaClase = ' + QuotedStr(IntToStr(CategoriaClase.Value));

                    oCategorias.ClientDataSet.Filter := ' IDCategoriaClase = ' + QuotedStr(IntToStr(CategoriaClase.Value));

                    oPlanes.oTarifas.ActivarAfterScroll(False);

                    oPuntosCobro.ClientDataSet.First;
                    while not oPuntosCobro.ClientDataSet.Eof do
                    begin

                        oCategorias.ClientDataSet.First;
                        while not oCategorias.ClientDataSet.Eof do
                        begin

                            oBandasHorarias.ClientDataSet.First;
                            while not oBandasHorarias.ClientDataSet.Eof do
                            begin

                                oPlanes.oTarifas.ClientDataSet.Append;
                                oPlanes.oTarifas.ID_Tarifa := 0;
                                oPlanes.oTarifas.ID_PlanTarifarioVersion := 0;
                                oPlanes.oTarifas.NumeroPuntoCobro := oPuntosCobro.NumeroPuntoCobro;
                                oPlanes.oTarifas.DescripcionPuntoCobro := oPuntosCobro.Descripcion;
                                oPlanes.oTarifas.Categoria := oCategorias.CodigoCategoria;
                                oPlanes.oTarifas.DescripcionCategoria := oCategorias.Descripcion;
                                oPlanes.oTarifas.CodigoCategoriasClases := CategoriaClase.Value;
                                oPlanes.oTarifas.CodigoTarifaTipo := oBandasHorarias.CodigoTarifaTipo;
                                oPlanes.oTarifas.DescripcionTarifaTipo := oBandasHorarias.TipoTarifaDescripcion;
                                oPlanes.oTarifas.CodigoDiaTipo := oBandasHorarias.CodigoDiaTipo;
                                oPlanes.oTarifas.DescripcionDiaTipo := oBandasHorarias.TipoDiaDescripcion;
                                oPlanes.oTarifas.HoraDesde := oBandasHorarias.HoraDesde;
                                oPlanes.oTarifas.HoraHasta := oBandasHorarias.HoraHasta;

                                if oPlanes.Id_CategoriasClases = FCodigoCategoriaInterurbana then
                                    oPlanes.oTarifas.Importe := oIPC.TBI
                                else
                                    oPlanes.oTarifas.Importe := oIPC.ClientDataSet.FieldByName(oBandasHorarias.CodigoTarifaTipo).Value * oPuntosCobro.Kms * oCategorias.FactorTarifa;

                                oPlanes.oTarifas.Estado := 1;     //Agregado
                                oPlanes.oTarifas.ClientDataSet.Post;

                                oBandasHorarias.ClientDataSet.Next;

                            end;

                            oCategorias.ClientDataSet.Next;

                        end;

                        oPuntosCobro.ClientDataSet.Next;
                    end;

                end;

            finally
                FreeAndNil(oBandasHorarias);
                FreeAndNil(oPuntosCobro);
                FreeAndNil(oIPC);
                ActivarControles(false);
                PermitirEdicion(True);
                oPlanes.oTarifas.ActivarAfterScroll(True);
            end;
         end;
     finally
         oPlanes.ActivarAfterScroll(True);
         dblListaPlanTarifarioPuntoCobro.DataSource.DataSet := oPlanes.oTarifas.ClientDataSet;
     end;

end;

procedure TFormTarifasPorPuntoDeCobro.CrearCopia(Concesionaria: TVariantComboBox; CategoriaClase: TVariantComboBox; FechaActivacion: TDateTime);
var
    temp: TClientDataSet;
begin

    try
        oPlanes.oTarifas.ActivarAfterScroll(False);
        CrearPlan(Concesionaria, CategoriaClase, FechaActivacion);

        temp := oPlanes.oTarifas.CopiarClientDataSet();
        FreeAndNil(oPlanes.oTarifas.ClientDataSet);
        oPlanes.oTarifas.ClientDataSet := temp;
        temp := nil;

        dblListaPlanTarifarioPuntoCobro.DataSource.Free;
        dblListaPlanTarifarioPuntoCobro.DataSource := TDataSource.Create(nil);
        oPlanes.oTarifas.ClientDataSet.First;
        while not oPlanes.oTarifas.ClientDataSet.eof do
        begin
            oPlanes.oTarifas.Estado := 1;     //Seteo a 1 todos los registros copia para poder agregarlos
            oPlanes.oTarifas.ClientDataSet.Next;
        end;
        oPlanes.oTarifas.ClientDataSet.Filter := 'Estado <> ' + QuotedStr('3');
        dblListaPlanTarifarioPuntoCobro.DataSource.DataSet := oPlanes.oTarifas.ClientDataSet;

        ActivarControles(false);
    finally
        oPlanes.oTarifas.ActivarAfterScroll(True);
        PaintBoxHorario_Paint(nil);
    end;

end;

procedure TFormTarifasPorPuntoDeCobro.ActivarControles(activar: boolean);
begin
    pnlCategorias.Enabled := activar;
    pnlTarifas.Enabled := activar;

    if activar then
        nbGuardar.PageIndex := 0
    else
        nbGuardar.PageIndex := 1;

end;

procedure TFormTarifasPorPuntoDeCobro.btnCancelarPlanClick(Sender: TObject);
begin
    if MsgBox('�Desea cancelar los cambios?', 'Cancelar', MB_YESNO) = mrYes then
    begin
        CargarGrillaPlanesVersiones();
        ActivarControles(True);
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.btnGuardarPlanClick(Sender: TObject);
var
    m: string;
begin
    try
        try
            oPlanes.oTarifas.ActivarAfterScroll(False);
            m:= oPlanes.ClientDataSet.Bookmark;
            if oPlanes.ID_PlanTarifarioVersion = 0 then  //Nuevo o Copia
            begin
                 oPlanes.Agregar(oPlanes.CodigoConcesionaria,
                                                    oPlanes.ID_CategoriasClases,
                                                    oPlanes.FechaActivacion,
                                                    oPlanes.oTarifas,
                                                    UsuarioSistema);
            end
            else          //Modificacion
            begin
                  oPlanes.Actualizar(oPlanes.ID_PlanTarifarioVersion,
                                                    oPlanes.FechaActivacion,
                                                    oPlanes.oTarifas,
                                                    UsuarioSistema);
            end;
            CargarGrillaPlanesVersiones;
            ActivarControles(True);
        except
            on e: Exception do
            begin
                MsgBoxErr('Error guardando cambios', e.Message, 'Error', MB_ICONSTOP);
            end;
        end;
    finally
        oPlanes.oTarifas.ActivarAfterScroll(True);
        oPlanes.ClientDataSet.Bookmark:= m;
    end;

end;



procedure TFormTarifasPorPuntoDeCobro.btnInsertaClick(Sender: TObject);
begin
    EditarTarifaPuntoCobro(1);
end;

procedure TFormTarifasPorPuntoDeCobro.btnModificaClick(Sender: TObject);
begin
    EditarTarifaPuntoCobro(2);
end;

procedure TFormTarifasPorPuntoDeCobro.EditarTarifaPuntoCobro(accion:Byte);
var
    formPunto: TFormEditarTarifaPuntoCobro;
begin
    try
        Application.CreateForm(TFormEditarTarifaPuntoCobro, formPunto);
        
        if formPunto.Inicializar(oPlanes, oCategorias, oPuntosCobros, oTiposDias, oTiposTarifas, FCodigoCategoriaInterurbana, accion) then
        begin
           if formPunto.ShowModal = mrOK then
           begin
               
               if accion = 1 then                 //Agregado
               begin
                   oPlanes.oTarifas.ActivarAfterScroll(False);
                   oPlanes.oTarifas.ClientDataSet.Append;
                   oPlanes.oTarifas.Estado := 1;
                   oPlanes.oTarifas.ID_PlanTarifarioVersion := oPlanes.ID_PlanTarifarioVersion;
                   oPlanes.oTarifas.NumeroPuntoCobro := formPunto.cbbPuntosCobro.Value;
                   oPlanes.oTarifas.DescripcionPuntoCobro := formPunto.cbbPuntosCobro.Text;
                   oPlanes.oTarifas.CodigoCategoriasClases := oPlanes.ID_CategoriasClases;
               end
               else
               begin                              //Modificado
                    if oPlanes.oTarifas.Estado <> 1 then       //Si no es nuevo lo cambio a modificado
                        oPlanes.oTarifas.Estado := 2;
               end;

               oPlanes.oTarifas.Categoria := formPunto.cbbCategoria.Value;
               oPlanes.oTarifas.DescripcionCategoria := formPunto.cbbCategoria.Text;
               oPlanes.oTarifas.CodigoDiaTipo := formPunto.cbbTipoDia.Value;
               oPlanes.oTarifas.DescripcionDiaTipo := formPunto.cbbTipoDia.Text;
               oPlanes.oTarifas.CodigoTarifaTipo := formPunto.cbbTipoTarifa.Value;
               oPlanes.oTarifas.DescripcionTarifaTipo := formPunto.cbbTipoTarifa.Text;
               oPlanes.oTarifas.HoraDesde := formPunto.edtHoraDesde.Text;
               oPlanes.oTarifas.HoraHasta := formPunto.edtHoraHasta.Text;
               oPlanes.oTarifas.Importe := StrToFloat(Trim(formPunto.txtImporte.Text));
               oPlanes.oTarifas.ClientDataSet.Post;

               ActivarControles(false);
               
           end;
        end;
    finally
        FreeAndNil(formPunto);
        oPlanes.oTarifas.ActivarAfterScroll(True);
        PaintBoxHorario_Paint(nil);
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.btnEliminaClick(Sender: TObject);
begin
    if oPlanes.oTarifas.ClientDataSet.RecordCount > 0 then
    begin
        if MsgBox('�Confirma eliminar la tarifa seleccionada?', 'Eliminar Tariafa', MB_YESNO) = mrYes then
        begin
            oPlanes.oTarifas.ActivarAfterScroll(False);
            if oPlanes.oTarifas.Estado = 1 then     //Si es nuevo lo elimino
                oPlanes.oTarifas.ClientDataSet.Delete
            else                                                                 //Si es cargado o modificado cambio el estado
            begin
                oPlanes.oTarifas.Estado := 3;
                oPlanes.oTarifas.ClientDataSet.Post;
            end;
            ActivarControles(False);
            oPlanes.oTarifas.ActivarAfterScroll(True);
            PaintBoxHorario_Paint(nil);
        end;
    end;
end;


procedure TFormTarifasPorPuntoDeCobro.dblPlanesVersionesLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    if (Column.Header.Caption = 'Habilitado') then
    begin
        if (oPlanes.FechaActivacion > FFechaActual) then
            Habilitar()
        else
        begin
          if oPlanes.Habilitado then
            MsgBox('El plan ya se encuentra en uso')
          else
            MsgBox('El plan esta vencido, cambie la fecha del plan a futuro');
        end;
    end
    else if Column.Header.Caption = 'Fecha Autorizaci�n MOP' then
        FechaMOP();   
end;

procedure TFormTarifasPorPuntoDeCobro.Habilitar();
var
  formHPT: TFormHabilitarPlanTarifario;
  m: string;
  gaps : TClientDataSet;
  i, j : Integer;
begin
    try
        try

            gaps := oPlanes.oTarifas.ValidarTarifas(oPlanes.oTarifas, oPlanes.ID_CategoriasClases);
            if gaps.RecordCount > 0 then
            begin
                if Not Assigned(fGaps) then
                    Application.CreateForm(TfrmPlanesTarifariosGaps, fgaps);
                fgaps.Inicializar(gaps);
                fGaps.FormStyle := fsStayOnTop;
                fgaps.Show;
                Exit;
            end;
            

            Application.CreateForm(TFormHabilitarPlanTarifario, formHPT);

            if formHPT.Inicializar(oPlanes, FFechaActual) then
            begin

                if formHPT.ShowModal = mrOK then begin
                    m := oPlanes.ClientDataSet.Bookmark;
                    if formHPT.cbbHabilitar.ItemIndex = 0 then
                    begin
                        if MsgBox('�Confirma habilitar el plan?', 'Confirmaci�n', MB_YESNO) = mrYes then
                        begin
                          oPlanes.HabilitarPlan(oPlanes.ID_PlanTarifarioVersion, UsuarioSistema, True);

                        end;
                    end
                    else
                    begin
                        if MsgBox('�Confirma deshabilitar el plan?', 'Confirmaci�n', MB_YESNO) = mrYes then
                        begin
                          oPlanes.HabilitarPlan(oPlanes.ID_PlanTarifarioVersion, UsuarioSistema, False);
                        end;
                    end;
                    CargarGrillaPlanesVersiones;
                    oPlanes.ClientDataSet.Bookmark := m;
                end;

            end;
        except
            on e: Exception do
               MsgBox(e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        oPlanes.oTarifas.ClientDataSet.EnableControls;
        oPlanes.oTarifas.ActivarAfterScroll(True);
        FreeAndNil(formHPT);
    end;
end;

procedure TFormTarifasPorPuntoDeCobro.FechaMOP;
var
    formFecha: TformFecha;
    m: string;
begin
    try
        try

            Application.CreateForm(TformFecha, formFecha);

            if formFecha.Inicializar('Fecha Autorizaci�n MOP', FFechaActual) then
            begin

                if formFecha.ShowModal = mrOK then begin
                   m := oPlanes.ClientDataSet.Bookmark;
                   oPlanes.ActualizarFechaMOP(oPlanes.ID_PlanTarifarioVersion, formFecha.edtFecha.Date, UsuarioSistema);
                   CargarGrillaPlanesVersiones;
                   oPlanes.ClientDataSet.Bookmark := m;
                end;

            end;
        except
            on e: Exception do
               MsgBox(e.Message, 'Error', MB_ICONSTOP);
        end;
    finally
        FreeAndNil(formFecha);
    end;
end;

{$ENDREGION}

{TERMINO: TASK_109_JMA_20170215}
end.
