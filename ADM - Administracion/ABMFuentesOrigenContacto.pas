{-----------------------------------------------------------------------------
 File Name:ABMFuentesOrigenContacto 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

Autor       :   CQuezadaI
Fecha       :   17 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se modifica en el DFM el valor de IndexFieldName
                desde "descripcion" a "OrdenMostrar"
-----------------------------------------------------------------------------}

unit ABMFuentesOrigenContacto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls,RStrings;

type
  TFrmFuentesOrigenContacto = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    FuentesOrigenContacto: TADOTable;
    txt_CodigoFuenteOrigenContacto: TNumericEdit;
    Notebook: TNotebook;
    Label2: TLabel;
    txt_OrdenMostrar: TNumericEdit;
    FuentesOrigenContactoCodigoFuenteOrigenContacto: TWordField;
    FuentesOrigenContactoDescripcion: TStringField;
    FuentesOrigenContactoOrdenMostrar: TWordField;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FrmFuentesOrigenContacto: TFrmFuentesOrigenContacto;

implementation


resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar esta Acci�n?';
    MSG_DELETE_ERROR		= 'No se puede eliminar esta Acci�n porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION		= 'Eliminar Acci�n';
    MSG_ACTUALIZAR_ERROR  	= 'No se pudieron actualizar los datos de la Acci�n.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Acci�n';


{$R *.DFM}

procedure TFrmFuentesOrigenContacto.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
end;

function TFrmFuentesOrigenContacto.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([FuentesOrigenContacto]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
    Notebook.PageIndex:=0;
end;

procedure TFrmFuentesOrigenContacto.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFrmFuentesOrigenContacto.DBList1Click(Sender: TObject);
begin
	with FuentesOrigenContacto do begin
		txt_CodigoFuenteOrigenContacto.Value	:= FieldByName('CodigoFuenteOrigenContacto').AsInteger;
		txt_Descripcion.text	:= Trim(FieldByName('Descripcion').AsString);
        txt_OrdenMostrar.value := FieldByName('OrdenMostrar').AsInteger;
	end;
end;

procedure TFrmFuentesOrigenContacto.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFrmFuentesOrigenContacto.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

{-----------------------------------------------------------------------------
  Function Name: DBList1Insert
  Author:
  Date Created:  /  /
  Description:
  Parameters: None
  Return Value: Sender: TObject

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFrmFuentesOrigenContacto.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    txt_CodigoFuenteOrigenContacto.value := QueryGetValueInt(DMConnections.BaseCAC,
                	'Select ISNULL(MAX(CodigoFuenteOrigenContacto), 0) + 1 FROM FuentesOrigenContacto  WITH (NOLOCK) ');
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFrmFuentesOrigenContacto.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFrmFuentesOrigenContacto.Limpiar_Campos;
begin
	txt_CodigoFuenteOrigenContacto.Clear;
	txt_Descripcion.Clear;
    txt_OrdenMostrar.Clear;
end;

procedure TFrmFuentesOrigenContacto.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFrmFuentesOrigenContacto.DBList1Delete(Sender: TObject);
resourcestring
    MSG_ERROR_ELIMINAR = 'No se puede eliminar la Fuente de Contacto seleccionada porque hay datos que dependen de ella';
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(format(MSG_QUESTION_BAJA_ESTA,[FLD_FUENTE_ORIGEN_CONTACTO]), format(MSG_ADMINISTRACION,[FLD_FUENTE_ORIGEN_CONTACTO]), MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			FuentesOrigenContacto.Delete;
		Except
			On E: Exception do begin
				FuentesOrigenContacto.Cancel;
				MsgBoxErr(MSG_ERROR_ELIMINAR, e.message, format(MSG_ADMINISTRACION,[FLD_FUENTE_ORIGEN_CONTACTO]), MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;

procedure TFrmFuentesOrigenContacto.BtnAceptarClick(Sender: TObject);
begin

    if not(ValidateControls([txt_Descripcion,txt_OrdenMostrar],
                            [trim(txt_Descripcion.Text)<>'',(txt_OrdenMostrar.Value > 0) and (txt_OrdenMostrar.value<=255)],
                            format(MSG_ADMINISTRACION,[FLD_FUENTE_ORIGEN_CONTACTO]),
                            [format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION]),format(MSG_VALIDAR_DEBE_EL,[FLD_ORDEN_MOSTRAR])]
                            )) then exit;

 	Screen.Cursor := crHourGlass;
	With FuentesOrigenContacto do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                FieldByName('CodigoFuenteOrigenContacto').value		:= txt_CodigoFuenteOrigenContacto.value;
			end else Edit;
			FieldByName('Descripcion').AsString			:= txt_Descripcion.text;
			FieldByName('OrdenMostrar').AsInteger	:= txt_OrdenMostrar.ValueInt;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[FLD_FUENTE_ORIGEN_CONTACTO]), E.message, format(MSG_ADMINISTRACION,[FLD_FUENTE_ORIGEN_CONTACTO]), MB_ICONSTOP);
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFrmFuentesOrigenContacto.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFrmFuentesOrigenContacto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFrmFuentesOrigenContacto.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFrmFuentesOrigenContacto.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_Descripcion.SetFocus;
end;

end.
