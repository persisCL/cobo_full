unit ABMPaises;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask, ComCtrls, PeaProcs, ADODB, DPSControls;

type
  TFormPaises = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    Descripcion: TEdit;
    CodigoPais: TEdit;
    Paises: TADOTable;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos();
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormPaises: TFormPaises;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Pa�s?';
    MSG_DELETE_ERROR		= 'No se puede eliminar este Pa�s porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Pa�s';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Pa�s';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Pa�s';

{$R *.DFM}

function TFormPaises.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([Paises]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormPaises.BtnCancelarClick(Sender: TObject);
begin
   	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled := False;
end;

procedure TFormPaises.DBList1Click(Sender: TObject);
begin
	 with paises do begin
		  CodigoPais.Text 		:= FieldByName('CodigoPais').AsString;
		  Descripcion.text 	    := FieldByName('Descripcion').AsString;
     end;
end;

procedure TFormPaises.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormPaises.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    codigoPais.setFocus;
end;

procedure TFormPaises.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormPaises.Limpiar_Campos();
begin
	CodigoPais.Clear;
	Descripcion.Clear;
end;

procedure TFormPaises.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormPaises.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION,	MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			Paises.Delete;
		Except
			On E: Exception do begin
				Paises.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormPaises.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
	CodigoPais.SetFocus;
end;

procedure TFormPaises.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With Paises do begin
		Try
			if DbList1.Estado = Alta then
				Append
			else
				Edit;
			FieldByName('CodigoPais').AsString 	 := CodigoPais.Text;
			FieldByName('Descripcion').AsString  := Descripcion.text;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    groupb.Enabled     := False;
  	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	DbList1.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormPaises.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormPaises.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormPaises.BtnSalirClick(Sender: TObject);
begin
     close;
end;

end.
