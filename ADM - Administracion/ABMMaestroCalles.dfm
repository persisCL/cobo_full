object FormCalles: TFormCalles
  Left = 251
  Top = 79
  Width = 668
  Height = 557
  Caption = 'Mantenimiento de Calles'
  Color = clBtnFace
  Constraints.MinHeight = 512
  Constraints.MinWidth = 668
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ATCalle: TAbmToolbar
    Left = 0
    Top = 0
    Width = 660
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = ATCalleClose
  end
  object pnl_BotonesGeneral: TPanel
    Left = 0
    Top = 491
    Width = 660
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 463
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 322
    Width = 660
    Height = 169
    ActivePage = tsTramos
    Align = alBottom
    TabIndex = 1
    TabOrder = 2
    object tsGeneral: TTabSheet
      Caption = 'Datos Generales'
      object Label2: TLabel
        Left = 10
        Top = 16
        Width = 72
        Height = 13
        Caption = '&Descripci'#243'n:'
        FocusControl = txt_descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_descripcion: TEdit
        Left = 115
        Top = 12
        Width = 422
        Height = 21
        Hint = 'Nombre de la calle'
        CharCase = ecUpperCase
        Color = 16444382
        MaxLength = 60
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object tsTramos: TTabSheet
      Caption = 'Rangos de Numeraci'#243'n'
      ImageIndex = 1
      DesignSize = (
        652
        141)
      object dblTramos: TDBListEx
        Left = 4
        Top = 5
        Width = 549
        Height = 124
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 70
            Header.Caption = 'Desde'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroDesde'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 70
            Header.Caption = 'Hasta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroHasta'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 50
            Header.Caption = 'CAD'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'CAD'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 110
            Header.Caption = 'C'#243'digo Postal Par'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'CodigoPostalPar'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 110
            Header.Caption = 'C'#243'digo Postal Impar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'COdigoPostalImpar'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 50
            Header.Caption = 'Latitud'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Latitud'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'Longitud'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Longitud'
          end>
        DataSource = dsTramos
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDblClick = dblTramosDblClick
      end
      object btnInsertar: TDPSButton
        Left = 565
        Top = 4
        Width = 79
        Height = 26
        Anchors = [akRight]
        Caption = '<< &Insertar'
        TabOrder = 1
        OnClick = btnInsertarClick
      end
      object btnEditar: TDPSButton
        Left = 565
        Top = 36
        Width = 79
        Height = 26
        Anchors = [akRight]
        Caption = '<> &Editar'
        TabOrder = 2
        OnClick = btnEditarClick
      end
      object btnBorrar: TDPSButton
        Left = 565
        Top = 68
        Width = 79
        Height = 26
        Anchors = [akRight]
        Caption = '>> &Borrar'
        TabOrder = 3
        OnClick = btnBorrarClick
      end
    end
    object tsAlias: TTabSheet
      Caption = 'Alias'
      ImageIndex = 2
      DesignSize = (
        652
        141)
      object dbl_Alias: TDBListEx
        Left = 4
        Top = 5
        Width = 552
        Height = 124
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Alias'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Alias'
          end>
        DataSource = ds_Alias
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDblClick = dbl_AliasDblClick
      end
      object btn_InsertarAlia: TDPSButton
        Left = 569
        Top = 4
        Width = 79
        Height = 26
        Anchors = [akTop, akRight]
        Caption = '<< &Insertar'
        TabOrder = 1
        OnClick = btn_InsertarAliaClick
      end
      object btn_EditarAlia: TDPSButton
        Left = 569
        Top = 36
        Width = 79
        Height = 26
        Anchors = [akTop, akRight]
        Caption = '<> &Editar'
        TabOrder = 2
        OnClick = btn_EditarAliaClick
      end
      object btn_BorrarAlia: TDPSButton
        Left = 569
        Top = 68
        Width = 79
        Height = 26
        Anchors = [akTop, akRight]
        Caption = '>> &Borrar'
        TabOrder = 3
        OnClick = btn_BorrarAliaClick
      end
    end
  end
  object Dbl_Calles: TAbmList
    Left = 0
    Top = 65
    Width = 660
    Height = 257
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      
        #0'262'#0'Descripci'#243'n                                                ' +
        '                  ')
    HScrollBar = True
    RefreshTime = 100
    Table = ObtenerCalles
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = Dbl_CallesClick
    OnProcess = ALTramosProcess
    OnDrawItem = Dbl_CallesDrawItem
    OnRefresh = Dbl_CallesRefresh
    OnInsert = Dbl_CallesInsert
    OnDelete = Dbl_CallesDelete
    OnEdit = Dbl_CallesEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = ATCalle
  end
  object Panel1: TPanel
    Left = 0
    Top = 33
    Width = 660
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object Label1: TLabel
      Left = 10
      Top = 10
      Width = 45
      Height = 13
      Caption = '&Region:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 322
      Top = 10
      Width = 50
      Height = 13
      Caption = '&Comuna:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cb_Region: TComboBox
      Left = 64
      Top = 6
      Width = 241
      Height = 21
      Hint = 'Tipo de calle'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      MaxLength = 4
      TabOrder = 0
      OnChange = cb_RegionChange
    end
    object cbComuna: TComboBox
      Left = 379
      Top = 6
      Width = 262
      Height = 21
      Hint = 'Tipo de calle'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      MaxLength = 4
      TabOrder = 1
      OnChange = cbComunaChange
    end
  end
  object dsTramos: TDataSource
    DataSet = cdsTramos
    Left = 76
    Top = 224
  end
  object cdsTramos: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'cdsTramosIndex2'
        Fields = 'numerodesde'
      end>
    IndexFieldNames = 'numerodesde'
    Params = <
      item
        DataType = ftInteger
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end
      item
        DataType = ftInteger
        Name = '@CodigoPersona'
        ParamType = ptInput
      end
      item
        DataType = ftWord
        Name = '@CodigoTipoOrigenDato'
        ParamType = ptInput
      end>
    ProviderName = 'dspTramos'
    StoreDefs = True
    AfterOpen = cdsTramosAfterOpen
    AfterClose = cdsTramosAfterOpen
    AfterPost = cdsTramosAfterOpen
    AfterDelete = cdsTramosAfterOpen
    Left = 112
    Top = 224
    object cdsTramosCodigoSegmento: TIntegerField
      FieldName = 'CodigoSegmento'
    end
    object cdsTramosCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsTramosCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsTramosCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
    object cdsTramosCodigoCalle: TIntegerField
      FieldName = 'CodigoCalle'
    end
    object cdsTramosCAD: TStringField
      FieldName = 'CAD'
      FixedChar = True
      Size = 2
    end
    object cdsTramosNumeroDesde: TIntegerField
      FieldName = 'NumeroDesde'
    end
    object cdsTramosNumeroHasta: TIntegerField
      FieldName = 'NumeroHasta'
    end
    object cdsTramosCodigoPostalPar: TStringField
      FieldName = 'CodigoPostalPar'
      FixedChar = True
      Size = 7
    end
    object cdsTramosCodigoPostalImpar: TStringField
      FieldName = 'CodigoPostalImpar'
      FixedChar = True
      Size = 7
    end
    object cdsTramosLatitud: TFloatField
      FieldName = 'Latitud'
    end
    object cdsTramosLongitud: TFloatField
      FieldName = 'Longitud'
    end
    object cdsTramosCodigoSegmentoInterfase: TIntegerField
      FieldName = 'CodigoSegmentoInterfase'
    end
  end
  object dspTramos: TDataSetProvider
    DataSet = ObtenerTramosCalle
    Constraints = True
    Left = 144
    Top = 224
  end
  object ObtenerCalles: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCalles;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@NumeroCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@SoloCallesPropias'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 112
    Top = 192
  end
  object ActualizarCalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarCalle;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CallePropia'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 352
    Top = 248
  end
  object qry_ActualizarTramo: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoCalle'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'Inserto'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoRegion'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoPais'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoComuna'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoCiudad'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoComunaAnt'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoRegionAnt'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoPaisAnt'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoCiudadAnt'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'NumeroDesde'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'NumeroHasta'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoOriginalGEO'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @CodigoCalle INT'
      'DECLARE @CodigoRegion CHAR(3)'
      'DECLARE @CodigoPais CHAR(3)'
      'DECLARE @CodigoComuna CHAR(3)'
      'DECLARE @CodigoCiudad CHAR(3)'
      ''
      'DECLARE @CodigoComunaAnt CHAR(3)'
      'DECLARE @CodigoRegionAnt CHAR(3)'
      'DECLARE @CodigoPaisAnt CHAR(3)'
      'DECLARE @CodigoCiudadAnt CHAR(3)'
      ''
      'DECLARE @NumeroDesde INT'
      'DECLARE @NumeroHasta INT'
      'DECLARE @CodigoOriginalGEO CHAR(10)'
      ''
      'DECLARE @Inserto BIT'
      ''
      'SET @CodigoCalle = :CodigoCalle'
      'SET @Inserto = :Inserto'
      'SET @CodigoRegion = :CodigoRegion'
      'SET @CodigoPais = :CodigoPais'
      'SET @CodigoComuna = :CodigoComuna'
      'SET @CodigoCiudad = :CodigoCiudad'
      ''
      'SET @CodigoComunaAnt = :CodigoComunaAnt'
      'SET @CodigoRegionAnt = :CodigoRegionAnt'
      'SET @CodigoPaisAnt = :CodigoPaisAnt'
      'SET @CodigoCiudadAnt = :CodigoCiudadAnt'
      ''
      'SET @NumeroDesde = :NumeroDesde'
      'SET @NumeroHasta = :NumeroHasta'
      'SET @CodigoOriginalGEO = :CodigoOriginalGEO'
      ''
      ''
      'IF @Inserto = 0 BEGIN'
      
        #9'UPDATE MaestroCallesTramos SET CodigoPais=@CodigoPais, CodigoRe' +
        'gion=@CodigoRegion, CodigoComuna=@CodigoComuna,'
      
        #9#9#9#9#9'CodigoCiudad=@CodigoCiudad, NumeroDesde=@NumeroDesde, Numer' +
        'oHasta=@NumeroHasta,'
      #9#9#9#9#9'CodigoOriginalGEO=@CodigoOriginalGEO'
      
        #9'WHERE CodigoCalle=@CodigoCalle AND CodigoPais LIKE @CodigoPaisA' +
        'nt AND CodigoRegion LIKE @CodigoRegionAnt'
      
        #9#9'AND CodigoComuna LIKE @CodigoComunaAnt AND CodigoCiudad LIKE @' +
        'CodigoCiudadAnt'
      'END ELSE BEGIN'
      
        #9'INSERT MaestroCallesTramos (CodigoCalle, CodigoPais, CodigoRegi' +
        'on, CodigoComuna, CodigoCiudad, NumeroDesde,'
      #9#9#9#9'    NumeroHasta, CodigoOriginalGEO)'
      
        #9'VALUES (@CodigoCalle, @CodigoPais, @CodigoRegion, @CodigoComuna' +
        ', @CodigoCiudad, @NumeroDesde,'
      #9#9'@NumeroHasta, @CodigoOriginalGEO)'
      ''
      'END'
      '')
    Left = 512
    Top = 232
  end
  object qry_Actualizar: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoCalle'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'Alias'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoTipoCalle'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'Inserto'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @CodigoCalle INT'
      'DECLARE @Descripcion VARCHAR(100)'
      'DECLARE @Alias VARCHAR(50)'
      'DECLARE @CodigoTipoCalle TINYINT'
      'DECLARE @Inserto BIT'
      ''
      'SET @CodigoCalle = :CodigoCalle'
      'SET @Descripcion = :Descripcion'
      'SET @Alias = :Alias'
      'SET @CodigoTipoCalle = :CodigoTipoCalle'
      'SET @Inserto = :Inserto'
      ''
      'IF @Inserto = 0 BEGIN'
      
        #9'UPDATE MaestroCalles SET Descripcion=@Descripcion, Alias=@Alias' +
        ', CodigoTipoCalle=@CodigoTipoCalle'
      #9'WHERE CodigoCalle=@CodigoCalle'
      'END ELSE BEGIN'
      
        #9'INSERT MaestroCalles (CodigoCalle, Descripcion, Alias, CodigoTi' +
        'poCalle)'
      #9'VALUES (@CodigoCalle, @Descripcion ,@Alias ,@CodigoTipoCalle)'
      'END'
      '')
    Left = 480
    Top = 232
  end
  object ActualizarTramo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTramo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CAD'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroDesde'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroHasta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOriginalGEO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostalPar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 7
        Value = Null
      end
      item
        Name = '@CodigoPostalImpar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 7
        Value = Null
      end
      item
        Name = '@Latitud'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@Longitud'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end>
    Left = 384
    Top = 248
  end
  object EliminarCalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarCalle'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 552
    Top = 104
  end
  object ObtenerTramosCalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTramosCalle;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftSmallint
        Direction = pdReturnValue
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        DataType = ftWord
        Size = -1
        Value = Null
      end>
    Left = 40
    Top = 224
  end
  object EliminarTramosCalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTramosCalle'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 584
    Top = 104
  end
  object cds_Alias: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'cds_AliasIndex1'
        Expression = 'alias'
        Options = [ixExpression]
      end>
    IndexName = 'cds_AliasIndex1'
    Params = <>
    ProviderName = 'dsp_Alias'
    StoreDefs = True
    AfterOpen = cds_AliasAfterOpen
    AfterClose = cds_AliasAfterOpen
    AfterPost = cds_AliasAfterOpen
    AfterDelete = cds_AliasAfterOpen
    Left = 112
    Top = 264
    object cds_AliasCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cds_AliasCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cds_AliasCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
    object cds_AliasCodigoCalle: TIntegerField
      FieldName = 'CodigoCalle'
    end
    object cds_AliasAlias: TStringField
      FieldName = 'Alias'
      Size = 100
    end
  end
  object ds_Alias: TDataSource
    DataSet = cds_Alias
    Left = 80
    Top = 264
  end
  object ObtenerAliasCalles: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerAliasCalles;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 264
  end
  object dsp_Alias: TDataSetProvider
    DataSet = ObtenerAliasCalles
    Constraints = True
    Left = 144
    Top = 264
  end
  object ActualizarAliasCalles: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarAliasCalles;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Alias'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 416
    Top = 248
  end
end
