{-----------------------------------------------------------------------------
 File Name: ABMMotivosTransitoNoFacturable.pas
 Author:    ndonadio
 Date Created: 26/05/2005
 Language: ES-AR
 Description: Mantiene la lista de Motivos de Transitos NO Facturables.

 Revision : 1
  Date: 19/02/2009
  Author: mpiazza
  Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
    los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
unit ABMMotivosTransitoNoFacturable;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Util,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Abm_obj, DmiCtrls, ComCtrls, DB, ADODB,
  DbList, DMConnection, UtilProc, StrUtils, UtilDB, RStrings, PeaProcs;

type
  TfrmABMMotivosTransitosNoFacturables = class(TForm)
    AbmToolBar: TAbmToolbar;
    pnl_Bottom: TPanel;
    nb_Botones: TNotebook;
    dbl_Motivos: TAbmList;
    pnl_Datos: TPanel;
    Ldescripcion: TLabel;
    lbl_CodigoMotivo: TLabel;
    txt_Descripcion: TEdit;
    txt_CodigoMotivo: TNumericEdit;
    ds_MotivosTransitoNoFacturable: TDataSource;
    tbl_MotivosTransitoNoFacturable: TADOTable;
    pnl_HintDeshabilitado: TPanel;
    lbl_HintDeshabilitado: TLabel;
    btn_Salir: TButton;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure dbl_MotivosDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbl_MotivosClick(Sender: TObject);
    procedure dbl_MotivosEdit(Sender: TObject);
    procedure dbl_MotivosInsert(Sender: TObject);
    procedure dbl_MotivosDelete(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
    procedure AbmToolBarClose(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
   private
    { Private declarations }
    procedure LimpiarCampos;
    procedure HabilitarCampos(Habilitado: Boolean);
    procedure HabilitarModo(NoEditable: Boolean);
    function ExisteMismaDescripcion:Boolean;
    function Existe_ID:Boolean;
    function CodigoReferenciado:Boolean;

   public
    { Public declarations }
    function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  frmABMMotivosTransitosNoFacturables: TfrmABMMotivosTransitosNoFacturables;

implementation

{$R *.dfm}

function TfrmABMMotivosTransitosNoFacturables.Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
	Result := False;

	if MDIChild then begin
        FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	nb_Botones.PageIndex := 0;

   	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([tbl_MotivosTransitoNoFacturable]) then Exit;

    // Limpiar controles de ingreso de datos.
    LimpiarCampos;
    // Dehabilita los campos para que no sean editables...
    HabilitarCampos(False);

    KeyPreview := True;
	dbl_Motivos.Reload;
	Result := True;
end;



procedure TfrmABMMotivosTransitosNoFacturables.dbl_MotivosDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	with Sender.Canvas do begin
    	FillRect(Rect);
        (* Si est� dado de Baja L�gica mostrarlo en Rojo*)
	    if Tabla.FieldByName('Interno').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clSilver
            else
                Sender.Canvas.Font.Color := clGray;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end;

     	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoMotivoTransitoNoFacturable').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
    end;
end;

procedure TfrmABMMotivosTransitosNoFacturables.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmABMMotivosTransitosNoFacturables.LimpiarCampos;
begin
    txt_Descripcion.Clear;
    txt_CodigoMotivo.Clear;
end;

procedure TfrmABMMotivosTransitosNoFacturables.HabilitarCampos(Habilitado: Boolean);
begin
	dbl_Motivos.Enabled    		:= NOT Habilitado;

//    if Habilitado then nb_Botones.PageIndex := 1
//    else nb_Botones.PageIndex := 0;
	  nb_Botones.PageIndex 		:= iif(Habilitado, 1, 0);
    pnl_Datos.Enabled           := Habilitado;
    txt_Descripcion.Enabled := Habilitado;
    txt_CodigoMotivo.Enabled := Habilitado;

    if Habilitado then begin
        txt_Descripcion.SetFocus;
    end
    else begin
        dbl_Motivos.Estado := Normal;
    end


end;

procedure TfrmABMMotivosTransitosNoFacturables.dbl_MotivosClick(
  Sender: TObject);
begin
    txt_Descripcion.Text := tbl_MotivosTransitoNoFacturable.FieldByName('Descripcion').AsString;
    txt_CodigoMotivo.ValueInt :=  tbl_MotivosTransitoNoFacturable.FieldByName('CodigoMotivoTransitoNoFacturable').AsInteger ;
    HabilitarModo(tbl_MotivosTransitoNoFacturable.FieldByName('Interno').asBoolean );
end;

procedure TfrmABMMotivosTransitosNoFacturables.HabilitarModo(NoEditable: Boolean);
begin
    if NoEditable then begin
        abmToolbar.Habilitados := [btAlta, btSalir];
        dbl_Motivos.Access := [accAlta];
    end
    else begin
        abmToolbar.Habilitados := [btAlta, btBaja, btModi, btSalir];
        dbl_Motivos.Access := [accAlta, accBaja, accModi];
    end;
end;

procedure TfrmABMMotivosTransitosNoFacturables.dbl_MotivosEdit(
  Sender: TObject);
begin
	HabilitarCampos(True);
    dbl_Motivos.Estado := Modi;
end;

procedure TfrmABMMotivosTransitosNoFacturables.dbl_MotivosInsert(
  Sender: TObject);
begin
	HabilitarCampos(True);
    dbl_Motivos.Estado := Alta;
    txt_Descripcion.Clear;
    txt_CodigoMotivo.Clear;

end;

procedure TfrmABMMotivosTransitosNoFacturables.dbl_MotivosDelete(Sender: TObject);
resourcestring
    MSG_DELETE_CONFIRMATION = '�Desea eliminar el motivo de Tr�nsito No Facturable '+ CR+LF
                             +' " %s " ?';
    MSG_CANNOT_DELETE       = 'No se puede eliminar un Motivo que ya utilizado.';

begin
	HabilitarCampos(False);
    dbl_Motivos.Estado := Baja;
    if ( MsgBox(Format( MSG_DELETE_CONFIRMATION,[tbl_MotivosTransitoNoFacturable.FieldByName('Descripcion').asString])
               ,'Confirme Eliminaci�n',MB_ICONQUESTION+MB_YESNO)
               = mrYes ) then begin
        try
            tbl_MotivosTransitoNoFacturable.Delete;
        except
            ShowMessage(MSG_CANNOT_DELETE);
        end;
    end;
    dbl_Motivos.Estado := Normal;
end;

procedure TfrmABMMotivosTransitosNoFacturables.btn_AceptarClick(
  Sender: TObject);
resourcestring
    MSG_DESCRIPTION_ALREADY_EXISTS  = 'La descripcion ya existe con otro C�digo';
    MSG_ID_ALREADY_EXISTS           = 'El C�digo ingresado ya existe';
    MSG_INVALIDA_ID                 = 'El c�digo ingresado no es v�lido';
    MSG_INVALID_DESCRIPTION         = 'Debe ingresar una descripci�n';
    MSG_CANNOT_CHANGE_USED_ID       = 'No puede modificar un C�digo que ha sido utilizado';
begin

    if NOT( ValidateControls([txt_Descripcion,txt_CodigoMotivo,
                              txt_CodigoMotivo, txt_Descripcion,txt_CodigoMotivo] ,
                              [NOT ExisteMismaDescripcion, NOT Existe_ID, (txt_CodigoMotivo.ValueInt >= 0) ,
                              TRIM(txt_Descripcion.Text) <> '', NOT CodigoReferenciado]  ,
                             'Datos Inv�lidos',
                             [  MSG_DESCRIPTION_ALREADY_EXISTS,MSG_ID_ALREADY_EXISTS ,
                                MSG_INVALIDA_ID,MSG_INVALID_DESCRIPTION, MSG_CANNOT_CHANGE_USED_ID
                                ]
                             )) then begin
            Exit;
        end;

    if (dbl_Motivos.Estado = Modi) then begin
    // Modificacion
        tbl_MotivosTransitoNOFacturable.Edit;
        tbl_MotivosTransitoNOFacturable.FieldByName('CodigoMotivoTransitoNoFacturable').Value := txt_codigoMotivo.ValueInt;
        tbl_MotivosTransitoNOFacturable.FieldByName('Descripcion').Value := txt_Descripcion.Text;
        tbl_MotivosTransitoNOFacturable.Post;

        dbl_Motivos.Reload;
    end
    else begin
    // Alta
        tbl_MotivosTransitoNOFacturable.AppendRecord([trim(txt_CodigoMotivo.text), txt_Descripcion.Text,False]);
        dbl_Motivos.Reload;
    end;

    HabilitarCampos(False);

end;

function TfrmABMMotivosTransitosNoFacturables.ExisteMismaDescripcion:Boolean;
begin
    Result := False;
    if (    dbl_Motivos.Estado = Alta) OR (
            (dbl_Motivos.Estado = Modi) and
            (Trim(txt_Descripcion.Text) <> Trim(dbl_Motivos.Table.FieldByName('Descripcion').AsString))
        ) then begin
            Result := dbl_Motivos.Table.Locate('Descripcion', txt_Descripcion.Text, []) ;
            end;
end;

function TfrmABMMotivosTransitosNoFacturables.Existe_ID:Boolean;
begin
    Result := False;
    if (dbl_Motivos.Estado = Alta)
        OR
        ( (dbl_Motivos.Estado = Modi) and
          (txt_CodigoMotivo.ValueInt <> dbl_Motivos.Table.FieldByName('CodigoMotivoTransitoNoFacturable').AsInteger)
        ) then begin
            Result := dbl_Motivos.Table.Locate('CodigoMotivoTransitoNoFacturable', txt_CodigoMotivo.ValueInt, []) ;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CodigoReferenciado
  Author:
  Date Created:  /  /
  Description:
  Parameters: --
  Return Value: Boolean

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TfrmABMMotivosTransitosNoFacturables.CodigoReferenciado:Boolean;
var
    a,b,c : integer;
begin
// if Cambio el Codigo
// and el codigo original figura en donde sea que tenga que figurar
// then no se puede cambiar
    Result := False;
    if (dbl_Motivos.Estado = Modi) and
       (txt_CodigoMOtivo.ValueInt <> dbl_Motivos.Table.FieldByName('CodigoMotivoTransitoNoFacturable').AsInteger) then begin

            a := QueryGetValueInt(DMConnections.BaseCAC,Format(
                            'SELECT ISNULL((SELECT 1 FROM Anomalias  WITH (NOLOCK) WHERE CodigoMotivoTransitoNoFacturable = %d '+
                            ' GROUP BY CodigoMotivoTransitoNoFacturable),0)',[dbl_Motivos.Table.FieldByName('CodigoMotivoTransitoNoFacturable').AsInteger]));

            b := QueryGetValueInt(DMConnections.BaseCAC,Format(
                            'SELECT ISNULL((SELECT 1 FROM CuentasNoFacturables  WITH (NOLOCK) WHERE CodigoMotivoTransitoNoFacturable = %d '+
                            ' GROUP BY CodigoMotivoTransitoNoFacturable),0)',[dbl_Motivos.Table.FieldByName('CodigoMotivoTransitoNoFacturable').AsInteger]));

            c := QueryGetValueInt(DMConnections.BaseCAC,Format(
                            'SELECT ISNULL((SELECT 1 FROM PatentesNoFacturables  WITH (NOLOCK) WHERE CodigoMotivoTransitoNoFacturable = %d '+
                            ' GROUP BY CodigoMotivoTransitoNoFacturable),0)',[dbl_Motivos.Table.FieldByName('CodigoMotivoTransitoNoFacturable').AsInteger]));


            Result := ((a+b+c) > 0)
    end;

end;

procedure TfrmABMMotivosTransitosNoFacturables.btn_SalirClick(
  Sender: TObject);
begin
    Close;
end;

procedure TfrmABMMotivosTransitosNoFacturables.AbmToolBarClose(
  Sender: TObject);
begin
    Close;
end;

procedure TfrmABMMotivosTransitosNoFacturables.btn_CancelarClick(
  Sender: TObject);
begin
    dbl_Motivos.Estado      := Normal;
	dbl_Motivos.Enabled    	:= True;
	dbl_Motivos.SetFocus;
	nb_Botones.PageIndex 	:= 0;
    pnl_Datos.Enabled       := False;
	dbl_Motivos.Reload;
end;

end.
