
unit frmABMInfraRegulParam;

interface

uses
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants, VariantComboBox,
  SysUtilsCN, PeaProcsCN;

type
  TABMInfraRegulParamForm = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    AbmToolbar: TAbmToolbar;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    ListaParametros: TAbmList;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    spObtenerInfraccionesRegularizacionParametros: TADOStoredProc;
    dsObtenerInfraccionesRegularizacionParametros: TDataSource;
    spActualizarInfraccionesRegularizacionParametros: TADOStoredProc;
    spEliminarInfraccionesRegularizacionParametros: TADOStoredProc;
    vcbConcesionarias: TVariantComboBox;
    nedtDiasRegul: TNumericEdit;
    Label1: TLabel;
    nedtDiasRegulNOFacturable: TNumericEdit;
    procedure FormShow(Sender: TObject);
   	procedure ListaParametrosRefresh(Sender: TObject);
   	function  ListaParametrosProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaParametrosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaParametrosClick(Sender: TObject);
	procedure ListaParametrosInsert(Sender: TObject);
	procedure ListaParametrosEdit(Sender: TObject);
	procedure ListaParametrosDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
    function ExisteConcesionaria(pCodigoCOncesionaria: Integer): Boolean;
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;

var
  ABMInfraRegulParamForm: TABMInfraRegulParamForm;

resourcestring
	STR_CAPTION	= 'Infracciones Parámetros Regularización';


implementation

{$R *.DFM}

function TABMInfraRegulParamForm.Inicializar:Boolean;
Var
	S: TSize;
begin
    try
        Result := False;
        FormStyle := fsMDIChild;
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
        if not OpenTables([spObtenerInfraccionesRegularizacionParametros]) then raise EErrorExceptionCN.Create('Inicializar ' + STR_CAPTION, 'NO se pudo abrir la tabla de Parámetros de Regularización');
        CargarComboConcesionarias(DMConnections.BaseCAC, vcbConcesionarias, True);
        Notebook.PageIndex := 0;
        ListaParametros.Reload;
        Result := True;
    except
        on e:Exception do begin
            ShowMsgBoxCN(e, Self);
        end;
    end;
end;


procedure TABMInfraRegulParamForm.LimpiarCampos;
begin
	vcbConcesionarias.ItemIndex := 0;
    nedtDiasRegul.Value := 0;
    nedtDiasRegulNOFacturable.Value := 0;
end;

procedure TABMInfraRegulParamForm.FormShow(Sender: TObject);
begin
	ListaParametros.Reload;
end;

procedure TABMInfraRegulParamForm.ListaParametrosRefresh(Sender: TObject);
begin
	 if ListaParametros.Empty then LimpiarCampos;
end;

procedure TABMInfraRegulParamForm.Volver_Campos;
begin
	ListaParametros.Estado := Normal;
	ListaParametros.Enabled:= True;

	ActiveControl             := ListaParametros;
	Notebook.PageIndex        := 0;
	groupb.Enabled            := False;
    vcbConcesionarias.Enabled := True;
	ListaParametros.Reload;
end;

procedure TABMInfraRegulParamForm.ListaParametrosDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('Concesionaria').AsString));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('DiasRegul').AsString));
		TextOut(Cols[2], Rect.Top, Trim(Tabla.FieldbyName('DiasRegulNOFacturable').AsString));
	end;
end;

procedure TABMInfraRegulParamForm.ListaParametrosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
        vcbConcesionarias.ItemIndex     := vcbConcesionarias.Items.IndexOfValue(FieldByName('CodigoConcesionaria').AsInteger);
        nedtDiasRegul.Value             := FieldByName('DiasRegul').AsInteger;
        nedtDiasRegulNOFacturable.Value := FieldByName('DiasRegulNOFacturable').AsInteger;
	end;
end;

function TABMInfraRegulParamForm.ListaParametrosProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

procedure TABMInfraRegulParamForm.ListaParametrosInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled            := True;
	ListaParametros.Enabled   := False;
	Notebook.PageIndex        := 1;
    vcbConcesionarias.Enabled := True;
	ActiveControl             := vcbConcesionarias;
end;

procedure TABMInfraRegulParamForm.ListaParametrosEdit(Sender: TObject);
begin
	ListaParametros.Enabled:= False;
	ListaParametros.Estado := modi;

	Notebook.PageIndex        := 1;
	groupb.Enabled            := True;
    vcbConcesionarias.Enabled := False;

	ActiveControl := nedtDiasRegul;
end;

procedure TABMInfraRegulParamForm.ListaParametrosDelete(Sender: TObject);
resourcestring
  MSG_DELETE_CAPTION = 'Eliminar Parámetros Regularización';
  MSG_DELETE_ERROR   = 'Se produjo un Error al eliminar los Parámetros de Regularización para la COncesionaria Seleccionada.';
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_CAPTION]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with spEliminarInfraccionesRegularizacionParametros do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoConcesionaria').Value := spObtenerInfraccionesRegularizacionParametros.FieldbyName('CodigoConcesionaria').AsInteger;
                ExecProc;
                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                    MsgBox(MSG_DELETE_ERROR, MSG_DELETE_CAPTION, MB_ICONSTOP);
                close;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_CAPTION]), MB_ICONSTOP);
			end
		end
	end;

	ListaParametros.Reload;
	ListaParametros.Estado := Normal;
	ListaParametros.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TABMInfraRegulParamForm.BtnAceptarClick(Sender: TObject);

begin
	if not ValidateControls(
        [vcbConcesionarias, vcbConcesionarias, nedtDiasRegul, nedtDiasRegulNOFacturable],
        [((not vcbConcesionarias.Enabled) or (not ExisteConcesionaria(vcbConcesionarias.Value))), (vcbConcesionarias.ItemIndex > 0), (nedtDiasRegul.Value >= 0), (nedtDiasRegulNOFacturable.Value >= 0)],
	    Format(MSG_CAPTION_ACTUALIZAR,[STR_CAPTION]),
	    ['La Concesionaria ya está dada de Alta', 'Debe Seleccionar una Concesionaria', 'El valor debe ser igual o mayor a Cero', 'El valor debe ser igual o mayor a Cero']) then begin
		Exit;
	end;

	with ListaParametros do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarInfraccionesRegularizacionParametros, Parameters do begin
                    Parameters.Refresh;
					ParamByName('@CodigoConcesionaria').Value   := vcbConcesionarias.Value;
					ParamByName('@DiasRegul').Value             := nedtDiasRegul.Value;
					ParamByName('@DiasRegulNOFacturable').Value := nedtDiasRegulNOFacturable.Value;
					ParamByName('@Usuario').Value               := UsuarioSistema;

					ExecProc;
				end;
			except
				On E: Exception do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_CAPTION]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TABMInfraRegulParamForm.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TABMInfraRegulParamForm.AbmToolbarClose(Sender: TObject);
begin
	 close;
end;

procedure TABMInfraRegulParamForm.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TABMInfraRegulParamForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

function TABMInfraRegulParamForm.ExisteConcesionaria(pCodigoCOncesionaria: Integer): Boolean;
    const
        cSQL = 'SELECT 1 FROM InfraccionesRegularizacionParametros (NOLOCK) WHERE CodigoConcesionaria = %d';
begin
    try
        Result := QueryGetValueInt(DMConnections.BaseCAC, Format(cSQL, [pCodigoCOncesionaria])) = 1;
    except
        on e:Exception do begin
            Result := False;
            ShowMsgBoxCN(e, Self);
        end;
    end;
end;

end.

