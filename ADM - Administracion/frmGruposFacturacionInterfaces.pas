{-----------------------------------------------------------------------------
 Unit Name: ABMGruposFacturacionInterfaces
 Author: Lgisuk
 Description: ABM de Grupos de Facturacion de Interfaces
-----------------------------------------------------------------------------}
unit frmGruposFacturacionInterfaces;

interface

uses
  //Grupos Facturacion Interfaces
  DMConnection,
  VariantComboBox,
  Util,
  UtilDB,
  UtilProc,
  Abm_obj,
  Db,
  DBTables,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList,   OleCtrls, DmiCtrls, Mask,  ComCtrls,
  PeaProcs, validate, Dateedit, ADODB,DPSControls;

type
  TfrmGruposFacturacionInterfaces = class(TForm)
    AbmToolbar: TAbmToolbar;
    DBList: TAbmList;
    GroupB: TPanel;
    LDia: TLabel;
    PBotones: TPanel;
    PAbajo: TPanel;
    Notebook: TNotebook;
    GruposFacturacionInterfaces: TADOTable;
    Lgrupo: TLabel;
    edDiaFacturacion: TNumericEdit;
    cbGruposFacturacion: TVariantComboBox;
    spObtenerGruposFacturacion: TADOStoredProc;
    LInterface: TLabel;
    cbinterface: TVariantComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBListClick(Sender: TObject);
    procedure DBListDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBListEdit(Sender: TObject);
    procedure DBListRefresh(Sender: TObject);
    procedure AbmToolbarClose(Sender: TObject);
    procedure DBListDelete(Sender: TObject);
    procedure DBListInsert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure Volver_Campos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

implementation

resourcestring
	MSG_DELETE_QUESTION	    = '�Est� seguro de querer eliminar la asignaci�n de Grupo de Facturaci�n?';
    MSG_DELETE_CAPTION 	    = 'Eliminar asignaci�n de Grupo de Facturaci�n';
    MSG_DELETE_ERROR        = 'La Asignaci�n de Grupo de Facturaci�n no se pudo eliminar';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos de asignaci�n de Grupo de Facturaci�n.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar asignaci�n de Grupo de Facturaci�n';

{$R *.DFM}

{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:  Lgisuk
  Date Created: 27/06/2005
  Description: Inicializacion de este formulario
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmGruposFacturacionInterfaces.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;
	if not OpenTables([GruposFacturacionInterfaces, spObtenerGruposFacturacion]) then Exit;
    cbGruposFacturacion.Items.Clear;
    While not spObtenerGruposFacturacion.Eof do begin
        cbGruposFacturacion.Items.Add(  spObtenerGruposFacturacion.FieldByName('Descripcion').AsString,
                                        spObtenerGruposFacturacion.FieldByName('CodigoGrupoFacturacion').AsInteger );
		spObtenerGruposFacturacion.Next;
    end;
    spObtenerGruposFacturacion.Close;
    Notebook.PageIndex := 0;
	Result := True;
	DbList.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author: Lgisuk
  Date Created: 27/06/2005
  Description: hace un reload de la lista
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.FormShow(Sender: TObject);
begin
   	DBList.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: Limpiar_Campos
  Author: Lgisuk
  Date Created: 27/06/2005
  Description: limpia los campos
  Parameters:
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.Limpiar_Campos();
begin
	edDiaFacturacion.Clear;
    cbinterface.ItemIndex := -1;
	cbGruposFacturacion.ItemIndex := -1;
end;

{-----------------------------------------------------------------------------
  Function Name: Volver_Campos
  Author:  Lgisuk
  Date Created: 27/06/2005
  Description: retorna al form al estado normal
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.Volver_Campos;
begin
	DbList.Estado     := Normal;
	DbList.Enabled    := True;
    DBList.Reload;
	DbList.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListClick
  Author:  Lgisuk
  Date Created: 27/06/2005
  Description:  obtengo los datos de un grupo de facturacion
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.DBListClick(Sender: TObject);
begin
	 with GruposFacturacionInterfaces do begin
          cbInterface.Value         := FieldByName('OrigenMandato').AsString;
		  edDiaFacturacion.ValueInt := FieldByName('DiaFacturacion').AsInteger;
          cbGruposFacturacion.Value := FieldByName('CodigoGrupoFacturacion').AsInteger;
     end;
end;
                            
{-----------------------------------------------------------------------------
  Function Name: DBListDrawItem
  Author:  Lgisuk
  Date Created: 27/06/2005
  Description: dibujo los registros en la grilla
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.DBListDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);

    //obtengo el nombre de la interfaz
    Function ObtenerNombreInterfaz(OrigenMandato:string):string;
    resourcestring
        STR_PRESTO    =  'Presto';
        STR_FALABELLA =  'Falabella';
    begin
        if OrigenMandato = 'P' then Result:='Presto';
        if OrigenMandato = 'F' then Result:='Falabella';
    end;

begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        TextOut(Cols[0], Rect.Top, ' ' + ObtenerNombreInterfaz(Tabla.FieldByName('OrigenMandato').AsString));
      	TextOut(Cols[1], Rect.Top, Tabla.FieldByName('DiaFacturacion').AsString);
        TextOut(Cols[2], Rect.Top, QueryGetValue( DMCOnnections.BaseCAC, Format( 'SELECT dbo.ObtenerDescripcionGrupoFacturacion(%d)', [Tabla.FieldByName('CodigoGrupoFacturacion').AsInteger])));
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListRefresh
  Author: Lgisuk
  Date Created: 27/06/2005
  Description: si la lista esta vacia limpio los campos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.DBListRefresh(Sender: TObject);
begin
	 if DBList.Empty then Limpiar_Campos();
end;

{-----------------------------------------------------------------------------
  Function Name: DBListEdit
  Author: lgisuk
  Date Created: 27/06/2005
  Description: Permito Editar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.DBListEdit(Sender: TObject);
begin
	DbList.Enabled    			:= False;
    dblist.Estado     			:= modi;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
	cbinterface.Enabled 		:= False;
    edDiaFacturacion.Enabled 	:= False;
    cbGruposFacturacion.Enabled	:= True;
    cbGruposFacturacion.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListInsert
  Author: Lgisuk
  Date Created: 27/06/2005
  Description:  Permito Insertar un nuevo Registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.DBListInsert(Sender: TObject);
begin
    groupb.Enabled := True;
	Limpiar_Campos;
	DbList.Enabled				:= False;
	Notebook.PageIndex 			:= 1;
	cbinterface.Enabled			:= True;
    edDiaFacturacion.Enabled 	:= True;
	cbGruposFacturacion.Enabled := True;
    cbInterface.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDelete
  Author:  lgisuk
  Date Created: 27/06/2005
  Description:  permito eliminar un registro existente
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.DBListDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			GruposFacturacionInterfaces.Delete;
		Except
			On E: Exception do begin
				GruposFacturacionInterfaces.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:  lgisuk
  Date Created: 27/06/2005
  Description: Permite cancelar la operacion
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:  lgisuk
  Date Created: 27/06/2005
  Description: Permite Agregar o Actualizar un grupo de facturaci�n
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_INVOICE_INTERFACE               = 'Debe indicar a que interface pertence el dia y grupo de facturaci�n!';
    MSG_INVOICE_DAY_MUST_HAVE_A_VALUE   = 'El d�a de facturaci�n no puede quedar en blanco';
    MSG_INVALID_INVOICE_DAY             = 'El d�a de facturaci�n es inv�lido';
    MSG_INVOICE_DAY_ALREADY_EXISTS      = 'El d�a de facturaci�n ya est� ingresado';
    MSG_INVOICE_GROUP_MUST_HAVE_A_VALUE = 'El grupo de facturaci�n no puede quedar en blanco';
    STR_ERROR                           = 'Error';
begin

    //Verifico que este cargada la interfase a la que
    //pertenece el dia y grupo de facturaci�n
    if cbinterface.Value = '' then begin
        MsgBoxBalloon(MSG_INVOICE_INTERFACE, STR_ERROR, MB_ICONSTOP,  cbinterface);
        cbinterface.SetFocus;
        exit;
    end;

    if not ValidateControls ([  edDiaFacturacion,
                                edDiaFacturacion,
                                edDiaFacturacion,
                                cbGruposFacturacion],
						    [   Trim(edDiaFacturacion.Text) <> '',
                                (edDiaFacturacion.ValueInt >= 1) and (edDiaFacturacion.ValueInt <= 31),
                                (DbList.Estado = Modi) Or (QueryGetValue(DMConnections.BaseCAC,
                                    Format('SELECT dbo.ExisteGrupoFacturacionInterfases('''+CBInterface.Value+''','+'%d)', [edDiaFacturacion.ValueInt])) = 'False'),
						        cbGruposFacturacion.ItemIndex <> -1],
						   Caption,
						   [MSG_INVOICE_DAY_MUST_HAVE_A_VALUE,
                            MSG_INVALID_INVOICE_DAY,
                            MSG_INVOICE_DAY_ALREADY_EXISTS,
                            MSG_INVOICE_GROUP_MUST_HAVE_A_VALUE]) then Exit;



 	Screen.Cursor := crHourGlass;
	With GruposFacturacionInterfaces do begin
		Try
            //Seleciono si inserto o edito seg�n el estado
			if DbList.Estado = Alta then Append else Edit;
            //Asigno los valores
            Fieldbyname('OrigenMandato').AsString           := cbInterface.Value;
			FieldByName('DiaFacturacion').AsInteger         := edDiaFacturacion.ValueInt;
			FieldByName('CodigoGrupoFacturacion').AsInteger := cbGruposFacturacion.Value;
            //Guardo los cambios
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:  lgisuk
  Date Created: 27/06/2005
  Description: permito cerrar el form desde el ToolBar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.AbmToolbarClose(Sender: TObject);
begin
     close;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author: lgisuk
  Date Created: 27/06/2005
  Description: permito salir del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.BtnSalirClick(Sender: TObject);
begin
     close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author: lgisuk
  Date Created: 27/06/2005
  Description: lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmGruposFacturacionInterfaces.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

end.
