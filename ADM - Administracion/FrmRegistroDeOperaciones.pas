{********************************** File Header *********************************
File Name   : FrmRegistroDeOperaciones.pas
Author      : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created: 02/05/2003
Language    : ES-AR
Description : Formulario principal del registro de logs.
********************************************************************************}
unit FrmRegistroDeOperaciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, Grids, DBGrids, DB, ADODB,
  ToolWin, ExtCtrls, UtilProc, DPSAbout, ImgList, ActnMenus,
  ActnCtrls, StdActns, ShellApi, ppBands, ppCache, ppClass,
  ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppDB, ppDBPipe, ppCtrls,
  ppPrnabl, ppVar, Util, DPSGrid, CollPnl, StdCtrls, DBCtrls, ppParameter;

type
  TSistemas = class(TComponent)
    private Codigo : byte;
  end;

type
  TFiltro = record
    CodigoCategoria: ShortInt;
    CodigoModulo: ShortInt;
    CodigoAccion: Integer;
    FechaDesde: TDateTime;
    FechaHasta: TDateTime;
    CodigoSeveridad: ShortInt;
    CodigoUsuario: AnsiString;
    Resultado: ShortInt;
  end;

type
  TFormRegistroDeOperaciones = class(TForm)
    DataSourceRegistroOperaciones: TDataSource;
    ObtenerRegistroOperaciones: TADOStoredProc;
    ImageList: TImageList;
    ToolBar: TToolBar;
    RBInterface: TRBInterface;
    ppDBPipeline: TppDBPipeline;
    ImageListRegistroOperaciones: TImageList;
    pnlEventos: TPanel;
    grdRegistroOperaciones: TDPSGrid;
    tbnExportar: TToolButton;
    TVSistemas: TTreeView;
    ObtenerSistemas: TADOStoredProc;
    rptRegistroOperaciones: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLine1: TppLine;
    lbl_usuario: TppLabel;
    lbl_titulo: TppLabel;
    DetailBand: TppDetailBand;
    ppDBText1: TppDBText;
    ppFooterBand1: TppFooterBand;
    lbl_pagina: TppSystemVariable;
    ppLabel1: TppLabel;
    Grupo: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    tbnBuscar: TToolButton;
    tbnFiltrar: TToolButton;
    tbnDepurar: TToolButton;
    EliminarRegistroOperaciones: TADOStoredProc;
    SaveDialog: TSaveDialog;
    tbnVer: TToolButton;
    txtDetalle: TDBMemo;
    ppLabel3: TppLabel;
    ppLabel2: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    tbnSalir: TToolButton;
    procedure FormResize(Sender: TObject);
    procedure grdRegistroOperacionesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DataSourceRegistroOperacionesDataChange(Sender: TObject; Field: TField);
    procedure TVSistemasChange(Sender: TObject; Node: TTreeNode);
    procedure tbnFiltrarClick(Sender: TObject);
    procedure tbnVerClick(Sender: TObject);
    procedure tbnDepurarClick(Sender: TObject);
    procedure tbnExportarClick(Sender: TObject);
    procedure tbnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tbnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure CargarEventos;
    procedure ConfigurarToolbar;
  public
    { Public declarations }
    Filtro: TFiltro;
    function Inicializar: Boolean;
  end;

var
  FormRegistroDeOperaciones: TFormRegistroDeOperaciones;

implementation

uses DMConnection, frmRegistroDeOperacionesBuscar, frmRegistroDeOperacionesFiltrar;

{$R *.dfm}

procedure TFormRegistroDeOperaciones.FormResize(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	// Ajustamos el tama�o de la �ltima columna para que no se vea la barra de
     // desplazamiento horizontal.
	grdRegistroOperaciones.Columns[6].Width := grdRegistroOperaciones.Width - (grdRegistroOperaciones.Columns[0].Width + grdRegistroOperaciones.Columns[1].Width + grdRegistroOperaciones.Columns[2].Width + grdRegistroOperaciones.Columns[3].Width + grdRegistroOperaciones.Columns[4].Width + grdRegistroOperaciones.Columns[5].Width);
	try
          if (Round(grdRegistroOperaciones.Height / 33) < DataSourceRegistroOperaciones.DataSet.RecordCount) then begin
			grdRegistroOperaciones.Columns[6].Width := (grdRegistroOperaciones.Columns[6].Width - 17);
	     end;
     except
     end;
     Screen.Cursor := crDefault;
end;

procedure TFormRegistroDeOperaciones.grdRegistroOperacionesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
	bmp: TBitmap;
begin
	if not ((DataSourceRegistroOperaciones.DataSet.Bof = True) and (DataSourceRegistroOperaciones.DataSet.Eof = True)) then begin
          // Colocamos el icono que identifica al tipo de evento en la grilla.
		with (Sender as TDBGrid) do begin
               if (Column.Index = 0) then begin
			     canvas.FillRect(rect);
				bmp := TBitMap.Create;
				case DataSourceRegistroOperaciones.DataSet.FieldByName('CodigoSeveridad').AsInteger of
    	        	          0: ImageListRegistroOperaciones.GetBitmap(0, bmp);
        	      	     1: ImageListRegistroOperaciones.GetBitmap(1, bmp);
            	  	     2: ImageListRegistroOperaciones.GetBitmap(2, bmp);
              		     3: ImageListRegistroOperaciones.GetBitmap(3, bmp);
	                    4: ImageListRegistroOperaciones.GetBitmap(4, bmp);
    	               end;
				if (Rect.Right <= bmp.Width) then begin
					canvas.StretchDraw(Rect, bmp)
    	               end
				else begin
	            	     canvas.Draw(Rect.left + ((Rect.Right - Rect.left) - bmp.Width) div 2, Rect.top, bmp);
                    end;
				bmp.Free;
			end;
		end;
    end;
end;

procedure TFormRegistroDeOperaciones.DataSourceRegistroOperacionesDataChange(Sender: TObject;
  Field: TField);
begin
	ConfigurarToolbar;
end;

procedure TFormRegistroDeOperaciones.TVSistemasChange(Sender: TObject;
  Node: TTreeNode);
begin
     CargarEventos;
end;

procedure TFormRegistroDeOperaciones.tbnFiltrarClick(Sender: TObject);
var
	f: TFormRegistroDeOperacionesFiltrar;
begin
	Screen.Cursor := crDefault;
	Application.CreateForm(TFormRegistroDeOperacionesFiltrar, f);
     if (TVSistemas.Selected.IsFirstNode = True) then begin
          f.CodigoSistema := 0;
     end
     else begin
          f.CodigoSistema := TSistemas(TVSistemas.Selected.Data).Codigo;
     end;
     f.cbCategoriasChange(f.cbCategorias);
	// Filtramos los registros seg�n la selecci�n que halla hecho el usuario.
     f.cbCategorias.ItemIndex := Filtro.CodigoCategoria;
     f.cbModulos.ItemIndex := Filtro.CodigoModulo;
     f.cbAcciones.ItemIndex := Filtro.CodigoAccion;
     f.DateEditFechaDesde.Date := Filtro.FechaDesde;
     f.DateEditFechaHasta.Date := Filtro.FechaHasta;
     f.cbSeveridades.ItemIndex := Filtro.CodigoSeveridad;
     f.cbUsuarios.Value := Filtro.CodigoUsuario;
     f.cbResultados.ItemIndex := Filtro.Resultado;
   	if (f.ShowModal = mrOk) then begin
          Screen.Cursor := crHourGlass;
          Filtro.CodigoCategoria := f.cbCategorias.ItemIndex;
          Filtro.CodigoModulo := f.cbModulos.ItemIndex;
          Filtro.CodigoAccion := f.cbAcciones.ItemIndex;
          Filtro.FechaDesde := f.DateEditFechaDesde.Date;
          Filtro.FechaHasta := f.DateEditFechaHasta.Date;
          Filtro.CodigoSeveridad := f.cbSeveridades.ItemIndex;
          Filtro.CodigoUsuario := f.cbUsuarios.Value;
          Filtro.Resultado := f.cbResultados.ItemIndex;
	     CargarEventos;
   	end;
     Screen.Cursor := crHourGlass;
  	f.Release;
	Screen.Cursor := crDefault;
end;

procedure TFormRegistroDeOperaciones.tbnVerClick(Sender: TObject);
begin
     Screen.Cursor := crHourGlass;
     // Generamos el reporte con todos los eventos desplegados en pantalla.
	ObtenerRegistroOperaciones.DisableControls;
	Screen.Cursor := crDefault;
	RBInterface.Execute(True);
	ObtenerRegistroOperaciones.EnableControls;
end;

procedure TFormRegistroDeOperaciones.tbnDepurarClick(Sender: TObject);
var
     ErrorAlEliminar : boolean;
resourcestring
     MSG_ERROR_ELIMINAR_EVENTO = 'No se ha podido eliminar el evento.';
     MSG_PREGUNTA_ELIMINAR_DATOS = '�Est� seguro que desea eliminar los datos?';
begin
     if (MessageDlg(MSG_PREGUNTA_ELIMINAR_DATOS, mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
          Screen.Cursor := crHourGlass;
	     ObtenerRegistroOperaciones.DisableControls;
          ErrorAlEliminar := False;
          DataSourceRegistroOperaciones.DataSet.First;
          while not ((DataSourceRegistroOperaciones.DataSet.Eof = True) or (ErrorAlEliminar = True)) do
          begin
               try
                    EliminarRegistroOperaciones.Parameters.ParamByName('@CodigoSistema').Value := DataSourceRegistroOperaciones.DataSet.FieldByName('CodigoSistema').AsInteger;
                    EliminarRegistroOperaciones.Parameters.ParamByName('@IDRegistroOperacion').Value := DataSourceRegistroOperaciones.DataSet.FieldByName('IDRegistroOperacion').AsInteger;
                    EliminarRegistroOperaciones.ExecProc;
               except
              	     on e:exception do begin
                         ErrorAlEliminar := True;
	     	          Screen.Cursor := crDefault;
	                    MsgBoxErr(MSG_ERROR_ELIMINAR_EVENTO, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
                    end;
               end;
               DataSourceRegistroOperaciones.DataSet.Next;
          end;
	     ObtenerRegistroOperaciones.EnableControls;
          CargarEventos;
          Screen.Cursor := crDefault;
     end;
end;

procedure TFormRegistroDeOperaciones.tbnExportarClick(Sender: TObject);
resourcestring
     MSG_ERROR_EXPORTAR = 'No se ha podido generar el archivo de exportaci�n.';
var
     F: TextFile;
     Linea, NombreArchivo: AnsiString;
begin
     if (SaveDialog.Execute = True) then begin
          Screen.Cursor := crHourGlass;
     	ObtenerRegistroOperaciones.DisableControls;
          NombreArchivo := SaveDialog.FileName;
          AssignFile(F, NombreArchivo);
          try
               ReWrite(F);
               Linea := 'CodigoSeveridad;Sistema;Accion;PuestoDeTrabajo;FechaHora;CodigoUsuario;Evento';
               WriteLn(F, Linea);
               while not (DataSourceRegistroOperaciones.DataSet.Eof = True) do begin
                    Linea := DataSourceRegistroOperaciones.DataSet.FieldByName('CodigoSeveridad').AsString + ';' +
                             Trim(DataSourceRegistroOperaciones.DataSet.FieldByName('Sistema').AsString) + ';' +
                             Trim(DataSourceRegistroOperaciones.DataSet.FieldByName('Accion').AsString) + ';' +
                             Trim(DataSourceRegistroOperaciones.DataSet.FieldByName('PuestoDeTrabajo').AsString) + ';' +
                             Trim(DataSourceRegistroOperaciones.DataSet.FieldByName('FechaHora').AsString) + ';' +
                             Trim(DataSourceRegistroOperaciones.DataSet.FieldByName('CodigoUsuario').AsString) + ';' +
                             Trim(DataSourceRegistroOperaciones.DataSet.FieldByName('Evento').AsString);
                    WriteLn(F, Linea);
                    DataSourceRegistroOperaciones.DataSet.Next;
               end;
          except
         	     on e:exception do begin
	     	     Screen.Cursor := crDefault;
	               MsgBoxErr(MSG_ERROR_EXPORTAR, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
               end;
          end;
          CLoseFile(F);
     	ObtenerRegistroOperaciones.EnableControls;
          Screen.Cursor := crDefault;
     end;
end;

procedure TFormRegistroDeOperaciones.tbnBuscarClick(Sender: TObject);
resourcestring
     MSG_ERROR_TIPO_DATO_CAMPO    = 'El tipo de dato del campo es incorrecto.';
     MSG_ERROR_TIPO_DATO_FECHA    = 'El dato ingresado no es de tipo fecha.';
     MSG_ERROR_TIPO_DATO_NUMERICO = 'El dato ingresado no es de tipo num�rico.';
     MSG_ERROR_DATO_NO_ENCONTRADO = 'El tipo de dato del campo es incorrecto.';
     MSG_ERROR_CONDICION_BUSQUEDA = 'La condici�n de b�squeda es incorrecta.';
var
	f: TFormRegistroDeOperacionesBuscar;
     miBookmark: TBookmark;
begin
	Screen.Cursor := crHourGlass;
	// Buscamos un registro en particular seg�n la selecci�n que halla hecho el usuario.
     miBookmark := DataSourceRegistroOperaciones.DataSet.GetBookmark;
	Application.CreateForm(TFormRegistroDeOperacionesBuscar, f);
     f.cbCampos.ItemIndex := 0;
	f.cbDirecciones.ItemIndex := 1;
     f.cbComienzos.ItemIndex := 1;
	Screen.Cursor := crDefault;
   	if (f.ShowModal = mrOk) then begin
		case DataSourceRegistroOperaciones.DataSet.FieldByName(f.cbCampos.Value).DataType of
               ftString, ftMemo, ftFmtMemo, ftFixedChar, ftWideString:
            	     if (f.cbCondiciones.Text = '=') then begin
                         ObtenerRegistroOperaciones.Recordset.Find(f.cbCampos.Value + f.cbCondiciones.Text + '''' + f.txtValor.Text + '''', 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ObtenerRegistroOperaciones.Recordset.Bookmark));
                    end
                    else if (f.cbCondiciones.Text = 'PARECIDO') then begin
				     ObtenerRegistroOperaciones.Recordset.Find(f.cbCampos.Value + ' LIKE ''*' + f.txtValor.Text + '*''', 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ObtenerRegistroOperaciones.Recordset.Bookmark));
			     end
                    else begin
				     Screen.Cursor := crDefault;
                         MsgBox(MSG_ERROR_CONDICION_BUSQUEDA, Application.Title, MB_ICONINFORMATION + MB_OK);
                         Exit;
                    end;
               ftDate, ftTime, ftDateTime, ftTimeStamp:
                    if (IsValidDate(f.txtValor.Text) = true) then begin
                         if ((f.cbCondiciones.Text = '=') or (f.cbCondiciones.Text = '>') or (f.cbCondiciones.Text = '<')) then begin
        	        	          ObtenerRegistroOperaciones.Recordset.Find(f.cbCampos.Value + f.cbCondiciones.Text + '#' + f.txtValor.Text + '#', 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ObtenerRegistroOperaciones.Recordset.Bookmark));
                         end
                         else begin
                              Screen.Cursor := crDefault;
                	          MsgBox(MSG_ERROR_CONDICION_BUSQUEDA, Application.Title, MB_ICONINFORMATION + MB_OK);
                              Exit;
                         end;
                    end
                    else begin
                         Screen.Cursor := crDefault;
               	     MsgBox(MSG_ERROR_TIPO_DATO_FECHA, Application.Title, MB_ICONINFORMATION + MB_OK);
                         Exit;
                    end;
               ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD, ftAutoInc, ftFMTBcd:
                    try
                         f.txtValor.Text := IntToStr(StrToInt(f.txtValor.Text));
	            	     if ((f.cbCondiciones.Text = '=') or (f.cbCondiciones.Text = '>') or (f.cbCondiciones.Text = '<')) then begin
        	        	          ObtenerRegistroOperaciones.Recordset.Find(f.cbCampos.Value + f.cbCondiciones.Text + ' ' + f.txtValor.Text, 0, f.cbDirecciones.Value, iif((f.cbComienzos.Text = 'Principio'), 1, ObtenerRegistroOperaciones.Recordset.Bookmark));
                         end
    	                    else begin
				          Screen.Cursor := crDefault;
                	          MsgBox(MSG_ERROR_CONDICION_BUSQUEDA, Application.Title, MB_ICONINFORMATION + MB_OK);
                              Exit;
                         end;
                    except
			          Screen.Cursor := crDefault;
               	     MsgBox(MSG_ERROR_TIPO_DATO_NUMERICO, Application.Title, MB_ICONINFORMATION + MB_OK);
                         Exit;
                    end;
        	     else
			     Screen.Cursor := crDefault;
                    MsgBox(MSG_ERROR_TIPO_DATO_CAMPO, Application.Title, MB_ICONINFORMATION + MB_OK);
                    Exit;
        	     end;
		// Se posiciona en el registro buscado si fue encontrado o en el registro
          // inicial si no fue as�.
		if (ObtenerRegistroOperaciones.Recordset.EOF = true) then begin
    	          DataSourceRegistroOperaciones.DataSet.GotoBookmark(miBookmark);
			Screen.Cursor := crDefault;
	   	     MsgBox(MSG_ERROR_DATO_NO_ENCONTRADO, Application.Title, MB_ICONINFORMATION + MB_OK);
    	          Screen.Cursor := crHourGlass;
	     end
          else begin
	          DataSourceRegistroOperaciones.DataSet.Resync([rmExact]);
    	     end;
     end;
     f.Release;
	Screen.Cursor := crDefault;
end;

procedure TFormRegistroDeOperaciones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormRegistroDeOperaciones.tbnSalirClick(Sender: TObject);
begin
     Close;
end;

{******************************** Function Header *******************************
Function Name: Inicializar
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 02/05/2003
Description  : Realiza la inicializaci�n de todas las variables y la
               configuraci�n visual del sistema seg�n la �ltima selecci�n del
               usuario.
Parameters   : N/A
Return Value : True:  El sistema se inicializ� correctamente.
               False: El sistema no se inicializ� correctamente.
********************************************************************************}
function TFormRegistroDeOperaciones.Inicializar: Boolean;
var
     S: TSize;
     Sistemas : TSistemas;
resourcestring
     MSG_ERROR_CARGAR_FORMULARIO = 'No se ha podido realizar la carga del formulario.';
begin
	Screen.Cursor := crHourGlass;
     // Asigna el tama�o de la ventana.
   	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	// Cargamos la lista de sistemas en el TreeView.
     try
          ObtenerSistemas.Open;
		while not (ObtenerSistemas.Eof = True) do begin
               Sistemas := TSistemas.Create(FormRegistroDeOperaciones);
               Sistemas.Codigo := ObtenerSistemas.FieldByName('CodigoSistema').AsInteger;
               TVSistemas.Items.AddChildObject(TVSistemas.Items[0], Trim(ObtenerSistemas.FieldByName('Descripcion').AsString), Sistemas);
        	     ObtenerSistemas.Next;
		end;
          ObtenerSistemas.Close;
          TVSistemas.Items[0].Expand(True);
          TVSistemas.Items[0].Selected := True;
     except
    	     on e:exception do begin
               Screen.Cursor := crDefault;
	          MsgBoxErr(MSG_ERROR_CARGAR_FORMULARIO, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
          end;
     end;
     Filtro.CodigoCategoria := 0;
     Filtro.CodigoModulo := 0;
     Filtro.CodigoAccion := 0;
     Filtro.FechaDesde := (Date - 1);
     Filtro.FechaHasta := Date;
     Filtro.CodigoSeveridad := 0;
     Filtro.CodigoUsuario := '';
     Filtro.Resultado := 0;
	CargarEventos;
     Result := True;
     Screen.Cursor := crDefault;
end;

{******************************** Function Header *******************************
Function Name: CargarEventos
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 02/05/2003
Description  : Busca en la base de datos los tipos de eventos seleccionados.
Parameters   : N/A
Return Value : N/A
********************************************************************************}
procedure TFormRegistroDeOperaciones.CargarEventos();
resourcestring
     MSG_ERROR_CARGAR_EVENTOS = 'No se han podido cargar los eventos.';
begin
     try
          ObtenerRegistroOperaciones.Close;
          if (TVSistemas.Selected.IsFirstNode = True) then begin
               ObtenerRegistroOperaciones.Parameters.ParamByName('@CodigoSistema').Value := 0;
          end
          else begin
               ObtenerRegistroOperaciones.Parameters.ParamByName('@CodigoSistema').Value := TSistemas(TVSistemas.Selected.Data).Codigo;
          end;
          ObtenerRegistroOperaciones.Parameters.ParamByName('@CodigoModulo').Value := Filtro.CodigoModulo;
          ObtenerRegistroOperaciones.Parameters.ParamByName('@CodigoAccion').Value := Filtro.CodigoAccion;
          ObtenerRegistroOperaciones.Parameters.ParamByName('@FechaDesde').Value := Filtro.FechaDesde;
          ObtenerRegistroOperaciones.Parameters.ParamByName('@FechaHasta').Value := Filtro.FechaHasta;
          ObtenerRegistroOperaciones.Parameters.ParamByName('@CodigoSeveridad').Value := Filtro.CodigoSeveridad;
          ObtenerRegistroOperaciones.Parameters.ParamByName('@CodigoUsuario').Value := Filtro.CodigoUsuario;
          ObtenerRegistroOperaciones.Parameters.ParamByName('@Resultado').Value := Filtro.Resultado;
		ObtenerRegistroOperaciones.Open;
          ConfigurarToolbar;
     except
    	     on e:exception do begin
		     Screen.Cursor := crDefault;
	          MsgBoxErr(MSG_ERROR_CARGAR_EVENTOS, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
          end;
     end;
     FormResize(Self);
end;

{******************************** Function Header *******************************
Function Name: ConfigurarToolbar
Author       : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created : 02/05/2003
Description  : Habilita o deshabilita los menues y los botones de la barra de
               herramientas seg�n el tipo de operaci�n realizada.
Parameters   : N/A
Return Value : N/A
********************************************************************************}
procedure TFormRegistroDeOperaciones.ConfigurarToolbar();
begin
     if not ((DataSourceRegistroOperaciones.DataSet.bof = True) and (DataSourceRegistroOperaciones.DataSet.Eof = True)) then begin
          tbnBuscar.Enabled := True;
          tbnExportar.Enabled := True;
          tbnDepurar.Enabled := True;
          tbnVer.Enabled := True;
     end
     else begin
          tbnBuscar.Enabled := False;
          tbnExportar.Enabled := False;
          tbnDepurar.Enabled := False;
          tbnVer.Enabled := False;
     end;
     tbnBuscar.Enabled := tbnBuscar.Enabled;
end;

end.
