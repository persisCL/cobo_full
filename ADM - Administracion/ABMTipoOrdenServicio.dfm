object FormTiposOrdenServicio: TFormTiposOrdenServicio
  Left = 144
  Top = 141
  Width = 696
  Height = 480
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  ActiveControl = Lista
  Caption = 'Tipos de Orden de Servicio'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  ShowHint = True
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 414
    Width = 688
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 366
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TDPSButton
            Left = 110
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TDPSButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TDPSButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object Lista: TAbmList
    Left = 0
    Top = 33
    Width = 688
    Height = 259
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'44'#0'C'#243'digo '
      
        #0'223'#0'Descripci'#243'n                                                ' +
        '     '
      #0'164'#0'Categoria                                     '
      #0'184'#0'Motivo                                                '
      #0'69'#0'Solo Clientes'
      #0'110'#0'Permite programaci'#243'n')
    HScrollBar = True
    RefreshTime = 10
    Table = Workflows
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaClick
    OnProcess = ListaProcess
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
    OnInsert = ListaInsert
    OnDelete = ListaDelete
    OnEdit = ListaEdit
    Estado = Normal
    ToolBar = AbmToolbar1
    Access = [accAlta, accBaja, accModi]
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 688
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbar1Close
  end
  object GroupB: TPanel
    Left = 0
    Top = 292
    Width = 688
    Height = 122
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 3
    object Label1: TLabel
      Left = 15
      Top = 15
      Width = 59
      Height = 13
      Caption = '&Descripci'#243'n:'
    end
    object Label3: TLabel
      Left = 15
      Top = 37
      Width = 50
      Height = 13
      Caption = '&Categor'#237'a:'
    end
    object Label4: TLabel
      Left = 15
      Top = 60
      Width = 96
      Height = 13
      Caption = '&Motivo de Contacto:'
    end
    object Label2: TLabel
      Left = 15
      Top = 81
      Width = 88
      Height = 13
      Caption = '&S'#243'lo para Clientes:'
    end
    object Label5: TLabel
      Left = 14
      Top = 100
      Width = 105
      Height = 13
      Caption = '&Permite programaci'#243'n:'
    end
    object txt_Descripcion: TEdit
      Left = 124
      Top = 9
      Width = 429
      Height = 21
      MaxLength = 60
      TabOrder = 0
    end
    object cbMotivosContacto: TVariantComboBox
      Left = 124
      Top = 56
      Width = 257
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object cbCategoriaOrdenServicio: TVariantComboBox
      Left = 124
      Top = 32
      Width = 257
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
    object chkAplicaSoloClientes: TCheckBox
      Left = 124
      Top = 80
      Width = 97
      Height = 17
      TabOrder = 3
    end
    object chkCalendario: TCheckBox
      Left = 124
      Top = 99
      Width = 97
      Height = 17
      TabOrder = 4
    end
  end
  object Panel3: TPanel
    Left = 172
    Top = 1
    Width = 185
    Height = 32
    BevelOuter = bvNone
    TabOrder = 4
    object btnEditarWorkflow: TSpeedButton
      Left = 3
      Top = 4
      Width = 26
      Height = 25
      Hint = 'Dise'#241'ar Workflow'
      Flat = True
      Glyph.Data = {
        C6050000424DC605000000000000360400002800000014000000140000000100
        08000000000090010000C40E0000C40E00000001000000000000273435002E34
        31003235390021364000213B4600223A44003A3645003B3946003C3946003C39
        47003D3947003D3A47003E3A47003E3B47003F3B48003A4647003B4748003D47
        49003E4649002543650020426C0020637800423F4D0042454A0045484C004745
        5200484552004846530049465300494753004A4753004B475400484B5700494B
        57004B485400484C5100494C50004A4D52004A4D53004B4E53004C4854004C4F
        53004B514E00465557004F5353004B5D5E004E595D0051544D00535250005154
        580054575B0056585C00515E62005D5F63005E74780069487B006A497C006862
        5A006F735B0062636E0064676C0066696F006A6C6F00656772006C6F74006177
        7B006F7173007475770071727E0077787A0018569F0018718C0078518C007B53
        8F00777B8000777B81007A7D820000C4F80061878300658F8A009B907F008789
        8B008D8F910094979D0095989D00999B9F00999C9F009B9C9F00809FA3009EA0
        A1009EA1A7009FA2A7009EA3AA00ABB18500A0A2A500A1A3A500A3A4A600A4A5
        A700A3A5A900A4A6A900A6A7A900A7A8AB00A7A9AC00A7AAAE00A7ADB500ABAD
        B100A8ADB500B4B7BD00B4B9C000B7BCC200BABDC300B8BEC700B9BEC60093D3
        C800BBC0C800C3A9CF00C5AAD100C7ACD300CEB2DA00CEB3DA00C0C1C300C5CA
        D100C8CCD200D0D1D300D0D3D700D3D5D600D3D7DC00D4D7DC00D6D9DF00D8D9
        D900DADBDE00DBDCDF00D9DCE000DBDEE000DBDFE500DEDEE000DCDFE500DFE1
        E300DDE0E500E6D8EC00E2E2E300E4E5E700E3E4E800E2E7EE00E5E7E900E7E7
        E900E5EAEF00EAEBEC00E1E8F200E3E9F200E5EBF300E7EAF000E7ECF400E8EC
        F200EAEDF200E8EDF500EAEFF500ECEFF300EEEFF000EDF0F200EDF0F300EEF1
        F300EDF0F600EEF2F800F1F4F600F2F5F700F0F3F800F2F5F900F4F7F900F7F8
        FB00F8FAFC00FBFBFC00FCFDFE00000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000002600F6BF28009C8120006900E8000078
        00000378FC005600B100007809000000A4000078EE00F6BF0100000000000000
        000026A20A006900000069002600F6BFF200B24D0A00F4079E0026A2960042C7
        5F00000000000000000026A2000016160000E705000026A2000016162600FE00
        2A00E4641600260226004D4F07002A0000000200E7000000000026A21600E705
        AF00290000000F000000F4079E00B2C79A00770400001616770042FC17000C00
        940056005C000E00E4005900C40000000100F6BF00005800A80096095A0026A2
        000000008E00E464960026023E004D4FDF00010082000000E700A487E2007704
        960000009E00648DDE0026A22600E705E000A3138700061A1600484848484848
        4848484848484848484848484848484900000000000000000000000000000000
        48484849009495969B9C88868A84A58E90919E0048484849009495969B9C4C31
        32299F122B2E660048484849009495969B9C4059810218007158650048484849
        009495969B7A174252259F0F4F4165004848484900949596793D6B53545B9F55
        56577B0048484849006F72703D9CA2A3A6A7A882898C8D004848484900131503
        989CA2A3A6A7A82C2F3045004848484900464D0423252627272929015D504300
        48484849001447058F9297999A9DA12A3A3943004848484900686A5C4B9CA2A3
        A6A7A8695E5F780048484849009495966C4A6E5A5B67A06263647D0048484849
        009495969B6D243E51279F104E36660048484849009495969B9C3C5981021800
        7158650048484849009495969B9C4C3335299F112D3466004848484900949596
        9B9C807E7F7CA4838587930048484849009495969B9CA2A3A6A7A8A9AAABAC00
        4848484800000000000000000000000000000000484848484848484848484848
        48484848484848484848}
      OnClick = btnEditarWorkflowClick
    end
  end
  object Workflows: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT A.*, '
      'SoloClientes = '
      '      CASE A.AplicaSoloClientes'
      '         WHEN 0 THEN '#39'No'#39
      '         WHEN 1 THEN '#39'Si'#39
      '         ELSE '#39'-'#39
      '      END,'
      'PermiteProgramacion ='
      '      CASE A.Calendario'
      '         WHEN 0 THEN '#39'No'#39
      '         WHEN 1 THEN '#39'Si'#39
      '         ELSE '#39'-'#39
      '      END,'
      'B.Descripcion as DescCategoria,'
      
        'DescMotivo = dbo.ObtenerDescripcionMotivoContacto(a.MotivoContac' +
        'to),'
      'A.AplicaSoloClientes'
      'FROM  Workflows A LEFT OUTER JOIN CategoriasOrdenServicio B'
      #9'ON A.CategoriaOrdenServicio = B.CodigoCategoriaOrdenServicio')
    Left = 321
    Top = 72
  end
  object TblCategoriasOrdenServicio: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'CategoriasOrdenServicio'
    Left = 320
    Top = 112
  end
  object dsCategoriasOrdenServicio: TDataSource
    DataSet = TblCategoriasOrdenServicio
    Left = 288
    Top = 112
  end
  object dsWorkfows: TDataSource
    DataSet = Workflows
    Left = 288
    Top = 72
  end
  object MotivosContacto: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'MotivosContacto'
    Left = 320
    Top = 152
  end
  object dsMotivosContactos: TDataSource
    DataSet = MotivosContacto
    Left = 288
    Top = 152
  end
  object ActualizarTipoOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTipoOrdenServicio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CategoriaOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MotivoContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@VersionActual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AplicaSoloClientes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Calendario'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoWorkFlow'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 220
  end
  object EliminarTipoOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTipoOrdenServicio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoWorkflow'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 324
    Top = 220
  end
end
