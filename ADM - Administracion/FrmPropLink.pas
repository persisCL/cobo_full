unit FrmPropLink;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, WorkflowComp, DmiCtrls, DB, ADODB, Util, UtilDB,
  Evaluate, OPScripts, UtilProc, DPSControls;

type
  TFormPropLink = class(TForm)
	Label1: TLabel;
	txt_condiciones: TMemo;
    Button1: TDPSButton;
    Button2: TDPSButton;
	procedure Button1Click(Sender: TObject);
  private
	{ Private declarations }
	function  CheckCondition: Boolean;
	function  GetConditions: AnsiString;
    procedure SetConditions(const Value: AnsiString);
  public
	{ Public declarations }
	function Inicializa: Boolean;
	property Conditions: AnsiString Read GetConditions Write SetConditions;
  end;

var
  FormPropLink: TFormPropLink;

implementation

uses DMConnection;

{$R *.dfm}

{ TFormPropLink }

function TFormPropLink.Inicializa: Boolean;
begin
	Result := True;
end;

procedure TFormPropLink.Button1Click(Sender: TObject);
begin
	if not CheckCondition then begin
		txt_condiciones.SetFocus;
		Exit;
	end;
	ModalResult := mrOk
end;

function TFormPropLink.CheckCondition: Boolean;
resourcestring
	MSG_ERROR_EXPRESION_BOOLEANA	= 'La expresión no es de tipo booleano';
    MSG_VALIDAR_CAPTION				= 'Condición';


Var
	V: Variant;
	Eval: TExpressionEvaluator;
begin
	if Trim(txt_condiciones.Text) = '' then begin
		Result := True;
		Exit;
	end;
	Eval := CrearEvaluadorOrdenServicio(DMConnections.BaseCAC, -1);
	try
		V := Eval.Evaluate(txt_condiciones.text);
		if VarType(V) <> varBoolean then raise exception.create(MSG_ERROR_EXPRESION_BOOLEANA);
		Result := True;
	except
		on e: exception do begin
			MsgBox(e.message, MSG_VALIDAR_CAPTION, MB_ICONSTOP);
			Result := False;
		end;
	end;
	Eval.Free;
end;

function TFormPropLink.GetConditions: AnsiString;
begin
	Result := Trim(txt_condiciones.text);
end;

procedure TFormPropLink.SetConditions(const Value: AnsiString);
begin
	txt_condiciones.text := Trim(Value);
end;

end.
