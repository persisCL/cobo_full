unit ABMTitulosFAQ;

interface

uses
  {$WARN SYMBOL_PLATFORM OFF}
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, DbList, Abm_obj, DmiCtrls, StdCtrls, DPSControls,
  ExtCtrls, utildb, PeaProcs,
  UtilProc, OleCtrls,  Mask,  ComCtrls, validate, Util,
  FileCtrl;
  {$WARN SYMBOL_PLATFORM ON}

type
  TfrmABMTitulosFAQ = class(TForm)
    panEdicion: TPanel;
    Panel2: TPanel;
    Notebook: TNotebook;
	GroupB: TPanel;
	Label1: TLabel;
	Label15: TLabel;
	txt_Descripcion: TEdit;
	txt_CodigoTitulo: TNumericEdit;
	panABM: TPanel;
	abmTitulos: TAbmToolbar;
	dblTitulos: TAbmList;
	TitulosFAQ: TADOTable;
	qry_MaxTitulo: TADOQuery;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
	function Inicializar(CodigoTitulo: Integer = 0): Boolean;
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure abmTitulosClose(Sender: TObject);
    procedure dblTitulosClick(Sender: TObject);
	procedure dblTitulosDelete(Sender: TObject);
    procedure dblTitulosDrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure dblTitulosEdit(Sender: TObject);
	procedure dblTitulosInsert(Sender: TObject);
	function dblTitulosProcess(Tabla: TDataSet;
	  var Texto: String): Boolean;
	procedure dblTitulosRefresh(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	EdicionExterna : Boolean;
	procedure Limpiar_Campos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
  end;

var
  frmABMTitulosFAQ: TfrmABMTitulosFAQ;

implementation

uses
	RStrings, DMConnection, ConstParametrosGenerales;

{$R *.dfm}

function TfrmABMTitulosFAQ.Inicializar(CodigoTitulo: Integer = 0): Boolean;
begin
	Result := False;
	if not OpenTables([TitulosFAQ]) then exit;

	Notebook.PageIndex := 0;

	EdicionExterna := CodigoTitulo <> 0;
	If EdicionExterna Then Begin
		Constraints.MaxHeight := Constraints.MinHeight;

		dblTitulos.Table.Filtered := True;
		dblTitulos.Table.Filter := 'CodigoTitulo = ' + IntToStr (CodigoTitulo);

		panABM.Visible := False;
		dblTitulosClick(dblTitulos);
		dblTitulosEdit(dblTitulos);
	End
	Else dblTitulos.Reload;

	Result := True;
end;

procedure TfrmABMTitulosFAQ.BtnAceptarClick(Sender: TObject);
begin
	if not ValidateControls([txt_Descripcion],
				[trim(txt_Descripcion.text) <> ''],
				MSG_CAPTION_ACTUALIZAR,
				[FLD_DESCRIPCION]) then exit;
	Screen.Cursor := crHourGlass;
	With dblTitulos.Table do begin
		Try
			if dblTitulos.Estado = Alta then begin
				Append;
				qry_MaxTitulo.Open;
				txt_CodigoTitulo.Value := qry_MaxTitulo.FieldByNAme('CodigoTitulo').AsInteger + 1;
				qry_MaxTitulo.Close;
			end else begin
				Edit;
			end;
			FieldByName('CodigoTitulo').AsInteger 	:= Trunc(txt_CodigoTitulo.Value);
			if Trim(txt_Descripcion.text) = '' then
				FieldByName('Descripcion').Clear
			else
				FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_TITULO]), E.message, Format (MSG_CAPTION_ACTUALIZAR,[FLD_TITULO]), MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor := crDefault;

	if EdicionExterna then begin
		Notebook.PageIndex := 0;
		BtnSalirClick(Sender);
	end
	else Volver_Campos;
end;

procedure TfrmABMTitulosFAQ.BtnCancelarClick(Sender: TObject);
begin
	if EdicionExterna then begin
		Notebook.PageIndex := 0;
		BtnSalirClick(Sender);
	end
	else Volver_Campos;
end;

procedure TfrmABMTitulosFAQ.BtnSalirClick(Sender: TObject);
begin
	close;
end;

procedure TfrmABMTitulosFAQ.abmTitulosClose(Sender: TObject);
begin
	 close;
end;

procedure TfrmABMTitulosFAQ.dblTitulosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
		txt_CodigoTitulo.Value	:= FieldByName('CodigoTitulo').AsInteger;
		txt_Descripcion.text	:= Trim(FieldByName('Descripcion').AsString);
	end;
end;

procedure TfrmABMTitulosFAQ.dblTitulosDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[FLD_TITULO]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[FLD_TITULO]), Format(MSG_ERROR_ELIMINAR_DEPENDENCIA,[FLD_TITULO]), Format(MSG_CAPTION_ELIMINAR,[FLD_TITULO]), MB_ICONSTOP);
			end;
		end;
		dblTitulos.Reload;
	end;
	dblTitulos.Estado     := Normal;
	dblTitulos.Enabled    := True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TfrmABMTitulosFAQ.dblTitulosDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoTitulo').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TfrmABMTitulosFAQ.dblTitulosEdit(Sender: TObject);
begin
	dblTitulos.Enabled 	:= False;
	dblTitulos.Estado  	:= modi;
	Notebook.PageIndex 	:= 1;
	groupb.Enabled     	:= True;

	txt_CodigoTitulo.Enabled:= False;
	if panABM.Visible then txt_Descripcion.setFocus;
end;

procedure TfrmABMTitulosFAQ.dblTitulosInsert(Sender: TObject);
begin
	groupb.Enabled     := True;
	Limpiar_Campos;
	dblTitulos.Enabled    := False;
	Notebook.PageIndex := 1;
	txt_CodigoTitulo.Enabled := False;
	txt_Descripcion.SetFocus;
end;

function TfrmABMTitulosFAQ.dblTitulosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Texto := Tabla.FieldByName('CodigoTitulo').AsString + ' ' +
			  Trim(Tabla.FieldByName('Descripcion').AsString);
	Result := True;
end;

procedure TfrmABMTitulosFAQ.dblTitulosRefresh(Sender: TObject);
begin
	 if dblTitulos.Empty then Limpiar_Campos();
end;

procedure TfrmABMTitulosFAQ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	action := caFree;
end;

procedure TfrmABMTitulosFAQ.Limpiar_Campos();
begin
	txt_CodigoTitulo.Clear;
	txt_Descripcion.Clear;
end;

procedure TfrmABMTitulosFAQ.Volver_Campos;
begin
	dblTitulos.Estado     := Normal;
	dblTitulos.Enabled    := True;
	dblTitulos.Reload;
	dblTitulos.SetFocus;
	txt_CodigoTitulo.Enabled := True;
	Notebook.PageIndex := 0;
	groupb.Enabled     := False;
end;

end.
