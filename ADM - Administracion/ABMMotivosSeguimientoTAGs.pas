unit ABMMotivosSeguimientoTAGs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, ExtCtrls, ComCtrls, ToolWin, ImgList, DB,
  ADODB, DBClient, Provider, DMConnection, UtilProc, Util;

type
  TfrmMotivosSeguimientoTAGs = class(TForm)
    ilImagenes: TImageList;
    pnl1: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btn3: TToolButton;
    btnBuscar: TToolButton;
    pnlInferior: TPanel;
    nb1: TNotebook;
    btnSalir1: TButton;
    btnCancelar: TButton;
    btnGuardar: TButton;
    pnl4: TPanel;
    lbl1: TLabel;
    lbl3: TLabel;
    edtCodigoMotivo: TEdit;
    edtDescripcion: TEdit;
    dbgrdMotivosSeguimiento: TDBGrid;
    spMotivosSeguimiento_DELETE: TADOStoredProc;
    spMotivosSeguimiento_UPDATE: TADOStoredProc;
    spMotivosSeguimiento_INSERT: TADOStoredProc;
    spMotivosSeguimiento_SELECT: TADOStoredProc;
    dtstprMotivosSeguimiento: TDataSetProvider;
    cdsMotivosSeguimiento: TClientDataSet;
    dsMotivosSeguimiento: TDataSource;
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnSalir1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cdsMotivosSeguimientoAfterScroll(DataSet: TDataSet);
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
    Accion: Integer;
    FGuardandoDatos: Boolean;

    procedure Limpiar_Campos;
    procedure CargarRegistros;
    procedure HabilitarDeshabilitarControles(Estado: Boolean);
    procedure CargarDatosRegistro;
  public
    { Public declarations }   
    function Inicializar: Boolean;
  end;

var
  frmMotivosSeguimientoTAGs: TfrmMotivosSeguimientoTAGs;

implementation

{$R *.dfm}

function TfrmMotivosSeguimientoTAGs.Inicializar: Boolean;
var
    sp: TADOStoredProc;
begin
    Result := True;
    Screen.Cursor := crHourGlass;

    Accion := 0;

    sp := TADOStoredProc.Create(Self);
    try
        try
            Limpiar_Campos;
            CargarRegistros;
            HabilitarDeshabilitarControles(False);
        except
            on e: Exception do begin
                MsgBoxErr('Error inicializando el formulario', e.Message, 'Error', MB_ICONERROR);
                Result := False;
            end;
        end;
    finally
        sp.Free;
        Screen.Cursor := crDefault;
    end;
end;

procedure TfrmMotivosSeguimientoTAGs.btnAgregarClick(Sender: TObject);
begin
    Accion := 1;
    Limpiar_Campos;
    HabilitarDeshabilitarControles(True);
    edtDescripcion.SetFocus;
end;

procedure TfrmMotivosSeguimientoTAGs.btnCancelarClick(Sender: TObject);
begin
    Accion := 0;
    Limpiar_Campos;
    HabilitarDeshabilitarControles(False);     
    CargarDatosRegistro;
end;

procedure TfrmMotivosSeguimientoTAGs.btnEditarClick(Sender: TObject);
begin
    Accion := 2;
    HabilitarDeshabilitarControles(True);
    edtDescripcion.SetFocus;
end;

procedure TfrmMotivosSeguimientoTAGs.btnEliminarClick(Sender: TObject);
resourcestring
    MSG_TITLE = 'Eliminar Motivo';
    MSG_CONFIRM = '�Desea eliminar el Motivo seleccionado?';
    MSG_ERROR = 'Error eliminando el Motivo';
    MSG_ELIMINADO = 'El Motivo ha sido eliminado';
begin
    if MsgBox(MSG_CONFIRM, MSG_TITLE, MB_YESNO+MB_ICONQUESTION) = ID_NO then
        Exit;

    try
        spMotivosSeguimiento_DELETE.Parameters.Refresh;
        spMotivosSeguimiento_DELETE.Parameters.ParamByName('@CodigoMotivo').Value := cdsMotivosSeguimiento.FieldByName('CodigoMotivo').AsInteger;
        spMotivosSeguimiento_DELETE.ExecProc;

        if spMotivosSeguimiento_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
            raise Exception.Create(spMotivosSeguimiento_DELETE.Parameters.ParamByName('@ErrorDescription').Value);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;

    MsgBox(MSG_ELIMINADO, MSG_TITLE, MB_ICONINFORMATION);

    CargarRegistros;
end;

procedure TfrmMotivosSeguimientoTAGs.CargarRegistros;
begin
    spMotivosSeguimiento_SELECT.Parameters.Refresh;
    spMotivosSeguimiento_SELECT.ExecProc;

    if spMotivosSeguimiento_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
        raise Exception.Create(spMotivosSeguimiento_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

    cdsMotivosSeguimiento.Data := dtstprMotivosSeguimiento.Data;
end;

procedure TfrmMotivosSeguimientoTAGs.cdsMotivosSeguimientoAfterScroll(
  DataSet: TDataSet);
begin
    if FGuardandoDatos then Exit;

    CargarDatosRegistro;
end;

procedure TfrmMotivosSeguimientoTAGs.CargarDatosRegistro;
begin
    edtCodigoMotivo.Text := cdsMotivosSeguimiento.FieldByName('CodigoMotivo').AsString;
    edtDescripcion.Text := cdsMotivosSeguimiento.FieldByName('Descripcion').AsString;
end;

procedure TfrmMotivosSeguimientoTAGs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmMotivosSeguimientoTAGs.HabilitarDeshabilitarControles(Estado: Boolean);
begin
    btnSalir.Enabled        := not Estado;
    btnAgregar.Enabled      := not Estado;
    btnEliminar.Enabled     := (not Estado) and (cdsMotivosSeguimiento.RecordCount > 0);
    btnEditar.Enabled       := (not Estado) and (cdsMotivosSeguimiento.RecordCount > 0);
    dbgrdMotivosSeguimiento.Enabled    := (not Estado) and (cdsMotivosSeguimiento.RecordCount > 0);

    edtDescripcion.Enabled  := Estado;

    if Estado then
        nb1.ActivePage := 'pgAltaModi'
    else
        nb1.ActivePage := 'pgSalir';
end;

procedure TfrmMotivosSeguimientoTAGs.Limpiar_Campos;
begin
    edtCodigoMotivo.Text := EmptyStr;
    edtDescripcion.Text := EmptyStr;
end;

procedure TfrmMotivosSeguimientoTAGs.btnSalir1Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmMotivosSeguimientoTAGs.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmMotivosSeguimientoTAGs.btnGuardarClick(Sender: TObject);
resourcestring
    MSG_FALTA_DESC = 'Debe ingresar una descripci�n';   
    MSG_ERROR_INSERT = 'Error al insertar motivo';
    MSG_ERROR_UPDATE = 'Error al actualizar motivo';
var
    I: Integer;
begin
    if edtDescripcion.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_FALTA_DESC, 'Error', MB_ICONSTOP, edtDescripcion);
        Exit;
    end;

    try
        FGuardandoDatos := True;

        if Accion = 1 then begin
            try
                spMotivosSeguimiento_INSERT.Parameters.Refresh;
                spMotivosSeguimiento_INSERT.Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
                spMotivosSeguimiento_INSERT.Parameters.ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
                spMotivosSeguimiento_INSERT.ExecProc;

                if (spMotivosSeguimiento_INSERT.Parameters.ParamByName('@RETURN_VALUE').Value < 0) then
                    raise Exception.Create(spMotivosSeguimiento_INSERT.Parameters.ParamByName('@ErrorDescription').Value);

                edtCodigoMotivo.Text := spMotivosSeguimiento_INSERT.Parameters.ParamByName('@CodigoMotivo').Value;

                cdsMotivosSeguimiento.Append;

                for I := 0 to cdsMotivosSeguimiento.Fields.Count - 1 do
                    cdsMotivosSeguimiento.Fields[I].ReadOnly := False;

                cdsMotivosSeguimiento.FieldByName('CodigoMotivo').Value := StrToInt(edtCodigoMotivo.Text);
                cdsMotivosSeguimiento.FieldByName('Descripcion').Value := edtDescripcion.Text;
                cdsMotivosSeguimiento.FieldByName('UsuarioCreacion').Value := UsuarioSistema;
                cdsMotivosSeguimiento.FieldByName('FechaHoraCreacion').Value := Now;
                cdsMotivosSeguimiento.Post;
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_INSERT, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;       

        if Accion = 2 then begin
            try
                spMotivosSeguimiento_UPDATE.Parameters.Refresh;                                                   
                spMotivosSeguimiento_UPDATE.Parameters.ParamByName('@CodigoMotivo').Value := StrToInt(edtCodigoMotivo .Text);
                spMotivosSeguimiento_UPDATE.Parameters.ParamByName('@Descripcion').Value := edtDescripcion.Text;
                spMotivosSeguimiento_UPDATE.Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
                spMotivosSeguimiento_UPDATE.ExecProc;

                if (spMotivosSeguimiento_UPDATE.Parameters.ParamByName('@RETURN_VALUE').Value < 0) then
                    raise Exception.Create(spMotivosSeguimiento_UPDATE.Parameters.ParamByName('@ErrorDescription').Value);

                cdsMotivosSeguimiento.Edit;
                cdsMotivosSeguimiento.FieldByName('CodigoMotivo').Value := StrToInt(edtCodigoMotivo.Text);
                cdsMotivosSeguimiento.FieldByName('Descripcion').Value := edtDescripcion.Text;
                cdsMotivosSeguimiento.FieldByName('UsuarioModificacion').Value := UsuarioSistema;
                cdsMotivosSeguimiento.FieldByName('FechaHoraModificacion').Value := Now;
                cdsMotivosSeguimiento.Post;
            except
                on e: Exception do begin
                    MsgBoxErr(MSG_ERROR_UPDATE, e.Message, 'Error', MB_ICONERROR);
                    Exit;
                end;
            end;
        end;
    finally
        FGuardandoDatos := False;
    end;

    Accion := 0;
    HabilitarDeshabilitarControles(False);
end;

end.
