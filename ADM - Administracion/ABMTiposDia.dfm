object FormTiposDia: TFormTiposDia
  Left = 239
  Top = 215
  Width = 600
  Height = 400
  Caption = 'Mantenimiento de Tipos de D'#237'a'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 234
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'50'#0'D'#237'a        '
      #0'181'#0'Descripci'#243'n                                       ')
    HScrollBar = True
    RefreshTime = 100
    Table = TiposDia
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Estado = Normal
    ToolBar = AbmToolbar1
    Access = [accAlta, accBaja, accModi]
  end
  object GroupB: TPanel
    Left = 0
    Top = 267
    Width = 592
    Height = 67
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 15
      Top = 41
      Width = 59
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
    end
    object Label15: TLabel
      Left = 15
      Top = 13
      Width = 45
      Height = 13
      Caption = '&Tipo D'#237'a:'
      FocusControl = txt_TipoDia
    end
    object txt_Descripcion: TEdit
      Left = 92
      Top = 37
      Width = 297
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 1
    end
    object txt_TipoDia: TEdit
      Left = 92
      Top = 9
      Width = 33
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 2
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 334
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 395
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object TiposDia: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'dbo.TiposDia'
    Left = 204
    Top = 80
  end
end
