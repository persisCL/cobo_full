object FormRegistroDeOperacionesBuscar: TFormRegistroDeOperacionesBuscar
  Left = 256
  Top = 111
  BorderStyle = bsDialog
  Caption = 'Buscar'
  ClientHeight = 256
  ClientWidth = 368
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBuscar: TPanel
    Left = 4
    Top = 8
    Width = 357
    Height = 209
    BevelOuter = bvLowered
    TabOrder = 0
    object labCampo: TLabel
      Left = 16
      Top = 16
      Width = 36
      Height = 13
      Caption = 'Campo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labCondicion: TLabel
      Left = 16
      Top = 56
      Width = 50
      Height = 13
      Caption = 'Condici'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labValor: TLabel
      Left = 16
      Top = 96
      Width = 27
      Height = 13
      Caption = 'Valor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labDireccion: TLabel
      Left = 16
      Top = 136
      Width = 48
      Height = 13
      Caption = 'Direcci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object labComienzo: TLabel
      Left = 16
      Top = 176
      Width = 34
      Height = 13
      Caption = 'Desde:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object cbCondiciones: TComboBox
      Left = 104
      Top = 56
      Width = 81
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        ''
        '='
        '>'
        '<'
        'PARECIDO')
    end
    object cbCampos: TVariantComboBox
      Left = 104
      Top = 16
      Width = 241
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <
        item
          Value = Null
        end
        item
          Caption = 'Sistema'
          Value = 'Sistema'
        end
        item
          Caption = 'Modulo'
          Value = 'Modulo'
        end
        item
          Caption = 'Accion'
          Value = 'Accion'
        end
        item
          Caption = 'Puesto de Trabajo'
          Value = 'PuestoDeTrabajo'
        end
        item
          Caption = 'Fecha / Hora'
          Value = 'FechaHora'
        end
        item
          Caption = 'Usuario'
          Value = 'CodigoUsuario'
        end>
    end
    object cbComienzos: TComboBox
      Left = 104
      Top = 176
      Width = 241
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 4
      Items.Strings = (
        ''
        'Posici'#243'n actual'
        'Principio')
    end
    object cbDirecciones: TVariantComboBox
      Left = 104
      Top = 136
      Width = 241
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 3
      Items = <
        item
          Value = 1
        end
        item
          Caption = 'Adelante'
          Value = 1
        end
        item
          Caption = 'Atr'#225's'
          Value = 255
        end>
    end
    object txtValor: TEdit
      Left = 104
      Top = 96
      Width = 241
      Height = 21
      TabOrder = 2
    end
  end
  object btnAceptar: TButton
    Left = 206
    Top = 224
    Width = 75
    Height = 25
    Hint = 'Aceptar'
    Caption = '&Aceptar'
    Default = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 286
    Top = 224
    Width = 75
    Height = 25
    Hint = 'Cancelar'
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
  end
end
