object FormABMTiposComprobantesLibrosContables: TFormABMTiposComprobantesLibrosContables
  Left = 120
  Top = 165
  Caption = 'ABM de Tipos de Comprobante por Libro Contable'
  ClientHeight = 366
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 162
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'164'#0'Tipo de Comprobante'
      #0'76'#0'Libro Contable')
    HScrollBar = True
    RefreshTime = 100
    Table = TiposComprobantesLibrosContables
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 195
    Width = 592
    Height = 132
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 15
      Top = 38
      Width = 126
      Height = 13
      Caption = '&Tipo de Comprobante:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 15
      Top = 78
      Width = 87
      Height = 13
      Caption = '&Libro Contable:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CbTipoComprobante: TVariantComboBox
      Left = 147
      Top = 35
      Width = 214
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
    object CbLibroContable: TVariantComboBox
      Left = 147
      Top = 70
      Width = 214
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 327
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 395
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object TiposComprobantesLibrosContables: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    IndexName = 'PK_TiposComprobantesLibrosContables'
    TableName = 'TiposComprobantesLibrosContables'
    Left = 242
    Top = 110
    object TiposComprobantesLibrosContablesTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object TiposComprobantesLibrosContablesIdLibroContable: TIntegerField
      FieldName = 'IdLibroContable'
    end
    object TiposComprobantesLibrosContablesUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
      Size = 50
    end
    object TiposComprobantesLibrosContablesFechaModificacion: TDateTimeField
      FieldName = 'FechaModificacion'
    end
    object TiposComprobantesLibrosContablesUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      Size = 50
    end
    object TiposComprobantesLibrosContablesFechaCreacion: TDateTimeField
      FieldName = 'FechaCreacion'
    end
  end
  object QryLibrosContables: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT MLC.idLibroContable, MLC.Descripcion'
      'FROM MaestroLibrosContables MLC (NOLOCK) '
      '')
    Left = 408
    Top = 152
  end
  object QryTiposComprobantes: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT TC.TipoComprobante, TC.Descripcion'
      'FROM TiposComprobantes TC (NOLOCK)')
    Left = 440
    Top = 152
  end
end
