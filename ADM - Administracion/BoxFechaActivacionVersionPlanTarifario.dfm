object FrmFechaActivacionVersionPlanTarifario: TFrmFechaActivacionVersionPlanTarifario
  Left = 0
  Top = 0
  Caption = 'Habilita versi'#243'n'
  ClientHeight = 137
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 30
    Top = 37
    Width = 138
    Height = 13
    Caption = 'Ingrese fecha de habilitaci'#243'n'
  end
  object dtFechaActivacion: TDateEdit
    Left = 189
    Top = 34
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    Date = -693594.000000000000000000
  end
  object btnAcepta: TButton
    Left = 174
    Top = 87
    Width = 75
    Height = 25
    Caption = '&Acepta'
    TabOrder = 1
    OnClick = btnAceptaClick
  end
  object btnCancela: TButton
    Left = 62
    Top = 87
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancela'
    ModalResult = 2
    TabOrder = 2
  end
end
