{-----------------------------------------------------------------------------
 File Name: AbmUsuarios
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

Revision : 2
Date: 13/05/2016
Author: JLO
Description:  Importado desde VNE y sacado todo el codigo en duro.

Etiqueta	:	20160623 MGO
Descripci�n	:	Se agrega listado de alarmas

-----------------------------------------------------------------------------}
unit AbmUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DbList, UtilProc, StdCtrls, Abm_obj, Buttons, Db, DBTables,
  UtilDB, FrmDatosUsuario, ComCtrls, FrmDatosGrupo, Util, Frm_AgregarPermiso,
  ADODB, DMConnection, DPSControls, Peaprocs,  Variants, Grids, PeaTypes,
  DBGrids, OleServer, AcroPDFLib_TLB;

type
  TFormAbmUsuarios = class(TForm)
    PageControl: TPageControl;
	tab_usuarios: TTabSheet;
    tab_Permisos: TTabSheet;
    Panel2: TPanel;
    ListaUsuarios: TDBList;
    Panel5: TPanel;
    ListaPermisos: TDBList;
    Panel6: TPanel;
    Label4: TLabel;
    Panel7: TPanel;
    ListaFunciones: TDBList;
    Panel8: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    cb_Sistema: TComboBox;
    Panel10: TPanel;
    Panel4: TPanel;
    Bevel1: TBevel;
    btn_close: TSpeedButton;
    btn_Insert: TSpeedButton;
    btn_Delete: TSpeedButton;
    btn_Edit: TSpeedButton;
    btn_print: TSpeedButton;
    img_usuario: TImage;
    img_grupo: TImage;
    img_ok: TImage;
    img_can: TImage;
    Usuarios: TADOTable;
    Grupos: TADOTable;
    ObtenerPermisosFuncion: TADOStoredProc;
    tmrInicializarBusqueda: TTimer;
    Button1: TButton;
    btn_quitarpermiso: TButton;
    Splitter2: TSplitter;
    Splitter1: TSplitter;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Bevel2: TBevel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    ListaGrupos: TDBList;
    ListaUsuarioGrupos: TDBList;
    spGruposUsuarios: TADOStoredProc;
    spSistemas: TADOStoredProc;
    spFunciones: TADOStoredProc;
    spDELPermisosUsuarios: TADOStoredProc;
    AdobeSPOpenDocuments1: TAdobeSPOpenDocuments;
    spDELGruposUsuarios: TADOStoredProc;
    spDELUsuariosSistemas: TADOStoredProc;
    spDELPermisosGrupos: TADOStoredProc;
    spDELGruposUsuarios_Grupo: TADOStoredProc;
    spDELGruposSistemas: TADOStoredProc;
    spQuitarPermisosGrupo: TADOStoredProc;
    spQuitarPermisosUsuario: TADOStoredProc;
    ListaAlertas: TDBList;										// 20160623 MGO
    spADM_UsuariosSistemasEnvioAlarmas_SELECT: TADOStoredProc;	// 20160623 MGO
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ListaUsuariosDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure ListaGruposDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure btn_closeClick(Sender: TObject);
    procedure ListaUsuariosEdit(Sender: TObject);
    procedure ListaUsuariosInsert(Sender: TObject);
    procedure btn_InsertClick(Sender: TObject);
    procedure btn_DeleteClick(Sender: TObject);
    procedure btn_EditClick(Sender: TObject);
    procedure ListaUsuariosDelete(Sender: TObject);
    procedure ListaGruposEdit(Sender: TObject);
    procedure ListaGruposInsert(Sender: TObject);
    procedure ListaGruposDelete(Sender: TObject);
    procedure cb_SistemaClick(Sender: TObject);
    procedure ListaFuncionesDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure ListaPermisosDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure ListaPermisosRefresh(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_quitarpermisoClick(Sender: TObject);
    procedure UsuariosAfterOpen(DataSet: TDataSet);
    procedure GruposAfterOpen(DataSet: TDataSet);
    procedure ListaUsuariosKeyPress(Sender: TObject; var Key: Char);
    procedure ListaGruposKeyPress(Sender: TObject; var Key: Char);
    procedure tmrInicializarBusquedaTimer(Sender: TObject);
    procedure ListaFuncionesClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure ListaUsuariosClick(Sender: TObject);
    procedure ListaAlertasDrawItem(Sender: TDBList; Tabla: TDataSet;	// 20160623 MGO
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);		// 20160623 MGO
  private
	{ Private declarations }
    FContenidoBuscar: String;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FormAbmUsuarios: TFormAbmUsuarios;

implementation
resourcestring
	MSG_DELETE_QUESTION_USUARIO	= '�Est� seguro de que desea quitar el usuario %s %s %s (%s)?';
    MSG_DELETE_ERROR_USUARIO	= 'Hay informaci�n de auditor�a vinculada a este usuario, por lo cual no puede eliminarse.';
    MSG_DELETE_CAPTION_USUARIO 	= 'Quitar usuario';
	MSG_DELETE_QUESTION_GRUPO	= '�Est� seguro de que desea quitar el grupo %s (%s)?';
    MSG_DELETE_CAPTION_GRUPO 	= 'Quitar Grupo';
    MSG_DELETE_QUESTION_PERMISO = '�Est� seguro de que desea quitar el permiso a %s?'; //JLO 20160523
    MSG_DELETE_CAPTION_PERMISO  = 'Quitar Permiso';  //JLO 20160523
    MSG_GRUPO_SISTEMA_DELETE    = 'El Grupo especificado es un grupo del sistema y no puede ser eliminado.';
    MSG_GRUPO_SISTEMA_EDIT      = 'El Grupo especificado es un grupo del sistema y no puede ser modificado.';
    MNU_CAPTION_ARCHIVO         = 'Archivo';
    MNU_CAPTION_VER             = 'Ver';
    MNU_CAPTION_VENTANA         = 'Ventana';
    MNU_CAPTION_AYUDA           = 'Ayuda';

{$R *.DFM}

{ TFormAbmUsuarios }

function TFormAbmUsuarios.Inicializa: Boolean;

begin

	PageControl.ActivePage := tab_Usuarios;

    Result := OpenTables([Usuarios, Grupos, spSistemas]);

	if not Result then Exit;

 	ListaUsuarios.Reload;
	ListaGrupos.Reload;
	while not spSistemas.Eof do begin
		cb_Sistema.Items.Add (spSistemas.FieldByName('Descripcion').AsString +
		                      Space(200) +
                          spSistemas.FieldByName('CodigoSistema').AsString);
		spSistemas.Next;
	end;
	spSistemas.Close;
	cb_Sistema.ItemIndex := 0;
	cb_Sistema.OnClick(cb_Sistema);

    FContenidoBuscar := EmptyStr;
end;

procedure TFormAbmUsuarios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormAbmUsuarios.ListaUsuariosDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
Var
	w, h: Integer;
begin
	With Sender.Canvas do begin
		FillRect(Rect);

        if Tabla.FieldByName('Deshabilitado').AsBoolean then begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end else begin
            if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end;

		TextOut(Cols[1], Rect.Top + 1, tabla.FieldByName('CodigoUsuario').AsString);
		TextOut(Cols[2], Rect.Top + 1, tabla.FieldByName('Nombre').AsString);
		TextOut(Cols[3], Rect.Top + 1, tabla.FieldByName('Apellido').AsString);
		TextOut(Cols[4], Rect.Top + 1, tabla.FieldByName('ApellidoMaterno').AsString);
		w := img_usuario.Picture.Bitmap.width;
		h := img_usuario.Picture.Bitmap.height;
		BrushCopy (Classes.Rect(Cols[0], Rect.Top, Cols[0] + w, Rect.Top + h),
               img_usuario.Picture.Bitmap, Classes.Rect(0, 0, w, h), clRed);
	end;
end;

procedure TFormAbmUsuarios.ListaGruposDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
Var
	w, h: Integer;
begin
	With Sender.Canvas, tabla do begin
		FillRect(Rect);
		TextOut(Cols[1], Rect.Top + 1, FieldByName('CodigoGrupo').AsString);
		TextOut(Cols[2], Rect.Top + 1, FieldByName('Descripcion').AsString);
		w := img_grupo.Picture.Bitmap.width;
		h := img_grupo.Picture.Bitmap.height;
		BrushCopy (Classes.Rect(Cols[0], Rect.Top, Cols[0] + w, Rect.Top + h),
               img_grupo.Picture.Bitmap, Classes.Rect(0, 0, w, h), clRed);
	end;
end;

procedure TFormAbmUsuarios.btn_closeClick(Sender: TObject);
begin
	Close;
end;

procedure TFormAbmUsuarios.ListaUsuariosEdit(Sender: TObject);
Var
	f: TFormDatosUsuario;
begin
	Application.CreateForm(TFormDatosUsuario, f);
	if f.Inicializa(Usuarios.FieldByName('CodigoUsuario').AsString) and
     (f.ShowModal = mrOk) then ListaUsuarios.Reload;
	f.Release;
end;

procedure TFormAbmUsuarios.ListaUsuariosInsert(Sender: TObject);
Var
	f: TFormDatosUsuario;
begin
	Application.CreateForm(TFormDatosUsuario, f);
	if f.Inicializa('') and (f.ShowModal = mrOk) then ListaUsuarios.Reload;
	f.Release;
end;

procedure TFormAbmUsuarios.btn_InsertClick(Sender: TObject);
begin
	//if ActiveControl = ListaUsuarios then ListaUsuarios.OnInsert(ListaUsuarios);
	//if ActiveControl = ListaGrupos then ListaGrupos.OnInsert(ListaGrupos);
    ListaUsuarios.OnInsert(ListaUsuarios);
end;

procedure TFormAbmUsuarios.btn_DeleteClick(Sender: TObject);
begin
//	if ActiveControl = ListaUsuarios then ListaUsuarios.OnDelete(ListaUsuarios);
//	if ActiveControl = ListaGrupos then ListaGrupos.OnDelete(ListaGrupos);
    ListaUsuarios.OnDelete(ListaUsuarios);
end;

procedure TFormAbmUsuarios.btn_EditClick(Sender: TObject);
begin
//	if ActiveControl = ListaUsuarios then ListaUsuarios.OnEdit(ListaUsuarios);
//	if ActiveControl = ListaGrupos then ListaGrupos.OnEdit(ListaGrupos);
    ListaUsuarios.OnEdit(ListaUsuarios);
end;

procedure TFormAbmUsuarios.ListaUsuariosClick(Sender: TObject);
begin
    //if qry_GruposUsuario.Active then qry_GruposUsuario.Close;
    { INICIO : 20160623 MGO
     if spGruposUsuarios.Active then spGruposUsuarios.Close;
    } // FIN : 20160623 MGO

    if Usuarios.Active then begin
        // INICIO : 20160623 MGO
        spGruposUsuarios.Close;
    	spADM_UsuariosSistemasEnvioAlarmas_SELECT.Close;
        // FIN : 20160623 MGO
        if not Usuarios.IsEmpty then begin
            spGruposUsuarios.Parameters.ParamByName('@CodigoUsuario').Value :=  Usuarios.FieldByName('CodigoUsuario').AsVariant;
            spGruposUsuarios.Open;

            // INICIO : 20160623 MGO
            spADM_UsuariosSistemasEnvioAlarmas_SELECT.Parameters.ParamByName('@Usuario').Value := Usuarios.FieldByName('CodigoUsuario').AsVariant;
            spADM_UsuariosSistemasEnvioAlarmas_SELECT.Open;   
            // FIN : 20160623 MGO
        end;
    end;

    ListaUsuarioGrupos.ReLoad;
    ListaAlertas.ReLoad;    // 20160623 MGO
end;

procedure TFormAbmUsuarios.ListaUsuariosDelete(Sender: TObject);
Var
	CodUsuario: AnsiString;
begin
	CodUsuario := TrimRight(Usuarios.FieldByName('CodigoUsuario').AsString);
	if MsgBox (Format (MSG_DELETE_QUESTION_USUARIO,[Trim(Usuarios.FieldByName('Nombre').AsString),
                                                  Trim(Usuarios.FieldByName('Apellido').AsString),
                                                  Trim(Usuarios.FieldByName('ApellidoMaterno').AsString),
                                                  CodUsuario]),
	  	               MSG_DELETE_CAPTION_USUARIO, MB_ICONQUESTION or MB_YESNO) = IDYES
  then begin
    //Usuarios.Connection.BeginTrans;
    DMConnections.BaseBO_Master.BeginTrans;
		try
            //JLO 20160513 INICIO
			// Le quitamos los permisos individuales
            spDELPermisosUsuarios.Parameters.ParamByName('@CodigoUsuario').Value := CodUsuario;
            spDELPermisosUsuarios.ExecProc;

            {
			QueryExecute (DMConnections.BaseBO_Master, 'DELETE FROM PermisosUsuario WHERE ' +
                                                       'CodigoUsuario = ''' + CodUsuario + '''');
            }

			// Le quitamos los grupos
            spDELGruposUsuarios.Parameters.ParamByName('@CodigoUsuario').Value := CodUsuario;
            spDELGruposUsuarios.ExecProc;

            {
			QueryExecute (DMConnections.BaseBO_Master, 'DELETE FROM GruposUsuario WHERE ' +
                                         'CodigoUsuario = ''' + CodUsuario + '''');
            }

			// Quitamos el usuario
            spDELUsuariosSistemas.Parameters.ParamByName('@CodigoUsuario').Value := CodUsuario;
            spDELUsuariosSistemas.ExecProc;

            {
			QueryExecute (DMConnections.BaseBO_Master, 'DELETE FROM UsuariosSistemas WHERE ' +
                                         'CodigoUsuario = ''' + CodUsuario + '''');
             }

            DMConnections.BaseBO_Master.CommitTrans;
			ListaUsuarios.Reload;
            //JLO 20160513 TERMINO
		except

			DMConnections.BaseBO_Master.RollbackTrans;
			MsgBox( MSG_DELETE_ERROR_USUARIO, MSG_DELETE_CAPTION_USUARIO, MB_ICONSTOP)

		end;
	end;
end;

procedure TFormAbmUsuarios.ListaGruposEdit(Sender: TObject);
Var
	f: TFormDatosGrupo;
begin
    if (Trim(Grupos.FieldByName('CodigoGrupo').AsString) = USUARIO_TODOS) or
       (Trim(Grupos.FieldByName('CodigoGrupo').AsString) = USUARIO_ADMINISTRADORES)
    then begin
        MsgBox(MSG_GRUPO_SISTEMA_EDIT, self.Caption, MB_ICONSTOP);
        Exit;
    end;

	Application.CreateForm(TFormDatosGrupo, f);
	if f.Inicializa(Grupos.FieldByName('CodigoGrupo').AsString) and
	  (f.ShowModal = mrOk) then ListaGrupos.Reload;
	f.Release;
end;

procedure TFormAbmUsuarios.ListaGruposInsert(Sender: TObject);
Var
	f: TFormDatosGrupo;
begin
	Application.CreateForm(TFormDatosGrupo, f);
	if f.Inicializa('') and (f.ShowModal = mrOk) then ListaGrupos.Reload;
	f.Release;
end;

procedure TFormAbmUsuarios.ListaGruposDelete(Sender: TObject);
Var
	CodGrupo: AnsiString;
begin

    if (Trim(Grupos.FieldByName('CodigoGrupo').AsString) = USUARIO_TODOS) or
       (Trim(Grupos.FieldByName('CodigoGrupo').AsString) = USUARIO_ADMINISTRADORES) then begin
        MsgBox(MSG_GRUPO_SISTEMA_DELETE, self.Caption, MB_ICONSTOP);
        Exit;
    end;

	CodGrupo := TrimRight(Grupos.FieldByName('CodigoGrupo').AsString);
	if MsgBox (Format (MSG_DELETE_QUESTION_GRUPO,
                     [CodGrupo, Trim(Grupos.FieldByName('Descripcion').AsString)]),
             MSG_DELETE_CAPTION_GRUPO, MB_ICONQUESTION or MB_YESNO) = IDYES  then begin
    //JLO 20160526 INICIO
        //JLO 20160513 INICIO
    DMConnections.BaseBO_Master.BeginTrans;
		try
		// Le quitamos los permisos
        spDELPermisosGrupos.Parameters.ParamByName('@CodigoGrupo').Value := CodGrupo;
        spDELPermisosGrupos.ExecProc;

        {
		QueryExecute (Grupos.Connection, 'DELETE FROM PermisosGrupo WHERE ' +
                                     'CodigoGrupo = ''' + CodGrupo + '''');
        }

		// Le quitamos los usuarios
        spDELGruposUsuarios_Grupo.Parameters.ParamByName('@CodigoGrupo').Value := CodGrupo;
        spDELGruposUsuarios_Grupo.ExecProc;


        {
		QueryExecute (Grupos.Connection, 'DELETE FROM GruposUsuario WHERE ' +
                                     'CodigoGrupo = ''' + CodGrupo + '''');
        }


		// Quitamos el grupo
        spDELGruposSistemas.Parameters.ParamByName('@CodigoGrupo').Value := CodGrupo;
        spDELGruposSistemas.ExecProc;
        {
		QueryExecute (Grupos.Connection, 'DELETE FROM GruposSistemas WHERE ' +
                                     'CodigoGrupo = ''' + CodGrupo + '''');
        }

        DMConnections.BaseBO_Master.CommitTrans;
		ListaGrupos.Reload
		except

			DMConnections.BaseBO_Master.RollbackTrans;
			MsgBox( MSG_GRUPO_SISTEMA_DELETE, MSG_DELETE_CAPTION_GRUPO, MB_ICONSTOP)

		end;
            //JLO 20160513 TERMINO
        //JLO 20160526 TERMINO
	end;
end;

procedure TFormAbmUsuarios.cb_SistemaClick(Sender: TObject);
Var
	CodSistema: AnsiString;
begin
	CodSistema := Trim(StrRight(cb_Sistema.Text, 50));
	spFunciones.Close;
	spFunciones.Parameters.ParamByName('@CodSistema').Value := CodSistema;
	spFunciones.Open;
    spFunciones.First;

(* Da permisos a los administradores para todos los menues y a TODOS para los especificados, resuelto por trigger
    while not qry_Funciones.Eof do begin
        //Agregar Administrador en esta linea
        QueryExecute(DMConnections.BaseCAC,
            'ActualizarPermisosGrupo ''' + USUARIO_ADMINISTRADORES + ''', ''' +
            CodSistema + ''', ''' + trim(qry_Funciones.FieldByName('Funcion').AsString) + ''', ' +
            '1' + ',' + '0');

        if (LimpiarCaracteresLabel(trim(qry_Funciones.FieldByName('Descripcion').AsString)) = MNU_CAPTION_ARCHIVO) or
           (LimpiarCaracteresLabel(trim(qry_Funciones.FieldByName('Descripcion').AsString)) = MNU_CAPTION_VER) or
           (LimpiarCaracteresLabel(trim(qry_Funciones.FieldByName('Descripcion').AsString)) = MNU_CAPTION_VENTANA) or
           (LimpiarCaracteresLabel(trim(qry_Funciones.FieldByName('Descripcion').AsString)) = MNU_CAPTION_AYUDA) then begin
            AgregarTodos:= True;
            //Agregar grupo Todos
            QueryExecute(DMConnections.BaseCAC,
				  'ActualizarPermisosGrupo ''' + USUARIO_TODOS + ''', ''' +
                  CodSistema + ''', ''' + trim(qry_Funciones.FieldByName('Funcion').AsString) + ''', ' +
				  '1' + ',' + '0');
        end
        else begin
            if (AgregarTodos) and (qry_Funciones.FieldByName('Nivel').AsInteger <> 1) then begin
                //Agregar grupo Todos
                QueryExecute(DMConnections.BaseCAC,
				     'ActualizarPermisosGrupo ''' + USUARIO_TODOS + ''', ''' +
                     CodSistema + ''', ''' + trim(qry_Funciones.FieldByName('Funcion').AsString) + ''', ' +
                     '1' + ',' + '0');
            end
            else AgregarTodos := False;
        end;
        qry_Funciones.Next;
    end;
*)
    spFunciones.First;
	ListaFunciones.Reload;

	ObtenerPermisosFuncion.Close;

    if not spFunciones.IsEmpty then begin
        ObtenerPermisosFuncion.Parameters.ParamByName('@CodigoSistema').Value :=
          spFunciones.FieldByname('CodigoSistema').AsString;
        ObtenerPermisosFuncion.Parameters.ParamByName('@Funcion').Value :=
          spFunciones.FieldByname('Funcion').AsString;
        ObtenerPermisosFuncion.Open;
    end;
    ListaPermisos.ReLoad;
end;

// INICIO : 20160623 MGO
procedure TFormAbmUsuarios.ListaAlertasDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
Var
	w, h: Integer;
    Descripcion: String;
begin
	With Sender.Canvas, tabla do begin
        Descripcion := FieldByName('Descripcion').AsString;
        if (FieldByName('CodigoSistema').AsInteger > 0) and (FieldByName('CodigoSistemaModulo').AsInteger > 0) then
            Descripcion := '    ' + Descripcion;
        

		FillRect(Rect);
		TextOut(Cols[1], Rect.Top + 1, Descripcion);
		TextOut(Cols[2], Rect.Top + 1, iif(FieldByName('EnvioEventos').AsBoolean, 'SI', 'NO'));
		TextOut(Cols[3], Rect.Top + 1, iif(FieldByName('EnvioAlertas').AsBoolean, 'SI', 'NO'));
	end;
end;
// FIN : 20160623 MGO

procedure TFormAbmUsuarios.ListaFuncionesClick(Sender: TObject);
begin
	// Mostramos los usuarios / grupos que tienen acceso a este �tem
	ObtenerPermisosFuncion.Close;
	ObtenerPermisosFuncion.Parameters.ParamByName('@CodigoSistema').Value :=
	  spFunciones.FieldByname('CodigoSistema').AsString;
	ObtenerPermisosFuncion.Parameters.ParamByName('@Funcion').Value :=
	  spFunciones.FieldByname('Funcion').AsString;
	ObtenerPermisosFuncion.Open;

	ListaPermisos.Reload;
end;

procedure TFormAbmUsuarios.ListaFuncionesDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	with Sender.Canvas, Tabla do begin
		FillRect(Rect);
        TextOut (Cols[0] + FieldByName('Level').AsInteger * 15,
             Rect.Top + 1, EliminarCaracterDeCadena(FieldByName('Descripcion').AsString, COMODIN_CHAR));
	end;
end;

procedure TFormAbmUsuarios.ListaPermisosDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
Var
	Bmp: TBitmap;
	w, h: Integer;
begin
	With Sender.Canvas, tabla do begin
		FillRect(Rect);
		TextOut(Cols[1], Rect.Top + 1, FieldByName('CodigoGrupoUsuario').AsString);
		TextOut(Cols[2], Rect.Top + 1, FieldByName('Descripcion').AsString);
		if FieldByName('EsGrupo').AsBoolean
      then Bmp := img_grupo.Picture.Bitmap
		  else Bmp := img_usuario.Picture.Bitmap;
		w := Bmp.width;
		h := Bmp.height;
		BrushCopy(Classes.Rect(Cols[0], Rect.Top, Cols[0] + w, Rect.Top + h),
		  Bmp, Classes.Rect(0, 0, w, h), clRed);
		if FieldByName('Permiso').AsBoolean
      then Bmp := img_ok.Picture.Bitmap
		  else Bmp := img_can.Picture.Bitmap;
		w := Bmp.width;
		h := Bmp.height;
		BrushCopy (Classes.Rect(Cols[3] + 5, Rect.Top, Cols[3] + w + 5, Rect.Top + h),
               Bmp, Classes.Rect(0, 0, w, h), clRed);
	end;
end;

procedure TFormAbmUsuarios.ListaPermisosRefresh(Sender: TObject);
begin
	btn_quitarpermiso.Enabled 	:= not ListaPermisos.Empty;
    Button1.Enabled				:= not ListaFunciones.Empty;
end;

procedure TFormAbmUsuarios.Button1Click(Sender: TObject);
Var
	f: TFormAgregarPermiso;
begin
	// Mostramos un form con la lista de usuarios y grupos a agregar
	Application.CreateForm(TFormAgregarPermiso, f);
	if f.Inicializa (spFunciones.FieldByname('CodigoSistema').AsString,
                   spFunciones.FieldByname('Funcion').AsString) and
    (f.ShowModal = mrOk)
  then begin
		ObtenerPermisosFuncion.Close;
		ObtenerPermisosFuncion.Open;
		ListaPermisos.Reload;
	end;
	f.Release;

    if spFunciones.FieldByname('CodigoSistema').AsInteger=SYS_ADMINISTRACION then // modulo de administracion
    begin
        CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_ADMINISTRACION);
        HabilitarPermisosMenu(Application.MainForm.Menu);
    end;
end;

procedure TFormAbmUsuarios.btn_quitarpermisoClick(Sender: TObject);
Var
	Codigo, CodigoSistema, Funcion: AnsiString;
begin
	Funcion := spFunciones.FieldByname('Funcion').AsString;
	CodigoSistema := spFunciones.FieldByname('CodigoSistema').AsString;
	Codigo := ObtenerPermisosFuncion.FieldByName('CodigoGrupoUsuario').AsString;
    // JLO 20160523 INICIO
    if MsgBox (Format (MSG_DELETE_QUESTION_PERMISO,[Trim(Codigo)]),
	  	               MSG_DELETE_CAPTION_PERMISO, MB_ICONQUESTION or MB_YESNO) = IDYES
    then begin
        //JLO 20160513 INICIO
        if ObtenerPermisosFuncion.FieldByName('EsGrupo').AsBoolean then begin
            if //(Trim(Grupos.FieldByName('CodigoGrupo').AsString) = USUARIO_TODOS) or
                UpperCase(Trim(Codigo)) = UpperCase(USUARIO_ADMINISTRADORES) then begin
                MsgBox(MSG_GRUPO_SISTEMA_DELETE, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            try
                spQuitarPermisosGrupo.Parameters.ParamByName('@CodigoGrupo').Value := Codigo;
                spQuitarPermisosGrupo.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;
                spQuitarPermisosGrupo.Parameters.ParamByName('@Funcion').Value := Funcion;
                spQuitarPermisosGrupo.ExecProc;
             except

            end;
            {
            QueryExecute (DMConnections.BaseBO_Master, 'EXEC QuitarPermisosGrupo ''' + Codigo + ''' , ' +
                                 CodigoSistema + ' , ''' + Funcion + '''');
            }
        end
        else begin
            try
                spQuitarPermisosUsuario.Parameters.ParamByName('@CodigoGrupo').Value := Codigo;
                spQuitarPermisosUsuario.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;
                spQuitarPermisosUsuario.Parameters.ParamByName('@Funcion').Value := Funcion;
                spQuitarPermisosUsuario.ExecProc;
            except

            end;
            {
            QueryExecute (DMConnections.BaseBO_Master, 'EXEC QuitarPermisosUsuario ''' + Codigo + ''' , ' +
                                     CodigoSistema + ' , ''' + Funcion + '''');
            }
        end;
        //JLO 20160513 TERMINO
        ObtenerPermisosFuncion.Close;
        ObtenerPermisosFuncion.Open;
        ListaPermisos.Reload;

        if spFunciones.FieldByname('CodigoSistema').AsInteger=SYS_ADMINISTRACION then // modulo de administracion
        begin
            CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_ADMINISTRACION);
            HabilitarPermisosMenu(Application.MainForm.Menu);
        end;
    end;
    //JLO 20160523 TERMINO

end;

procedure TFormAbmUsuarios.UsuariosAfterOpen(DataSet: TDataSet);
begin
    Usuarios.Sort:='CodigoUsuario';
end;

procedure TFormAbmUsuarios.GruposAfterOpen(DataSet: TDataSet);
begin
    Grupos.Sort := 'CodigoGrupo';
end;

procedure TFormAbmUsuarios.ListaUsuariosKeyPress(Sender: TObject;
  var Key: Char);
begin
    tmrInicializarBusqueda.Enabled := False;
    FContenidoBuscar := FContenidoBuscar + Key;
    if Usuarios.Locate('CodigoUsuario', FContenidoBuscar, [loPartialKey, loCaseInsensitive]) then begin
        ListaUsuarios.ReLoad;
        tmrInicializarBusqueda.Enabled := True;
    end
    else
        FContenidoBuscar := EmptyStr;
end;

procedure TFormAbmUsuarios.SpeedButton2Click(Sender: TObject);
begin
	ListaGrupos.OnInsert(ListaGrupos);
end;

procedure TFormAbmUsuarios.SpeedButton3Click(Sender: TObject);
begin
	ListaGrupos.OnDelete(ListaGrupos);
end;

procedure TFormAbmUsuarios.SpeedButton4Click(Sender: TObject);
begin
	ListaGrupos.OnEdit(ListaGrupos);
end;

procedure TFormAbmUsuarios.ListaGruposKeyPress(Sender: TObject;
  var Key: Char);
begin
    tmrInicializarBusqueda.Enabled := False;
    FContenidoBuscar := FContenidoBuscar + Key;
    if Grupos.Locate('CodigoGrupo', FContenidoBuscar, [loPartialKey, loCaseInsensitive]) then begin
        ListaGrupos.ReLoad;
        tmrInicializarBusqueda.Enabled := True;
    end
    else
        FContenidoBuscar := EmptyStr;
end;

procedure TFormAbmUsuarios.tmrInicializarBusquedaTimer(Sender: TObject);
begin
    tmrInicializarBusqueda.Enabled := False;
    FContenidoBuscar := EmptyStr;
end;

end.
