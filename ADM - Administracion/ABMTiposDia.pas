unit ABMTiposDia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DPSControls;

type
  TFormTiposDia = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    TiposDia: TADOTable;
    txt_TipoDia: TEdit;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormTiposDia: TFormTiposDia;

implementation
resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar este Tipo de D�a?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Tipo de D�a porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Tipo de D�a';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Tipo de D�a.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Tipo de D�a';

{$R *.DFM}

function TFormTiposDia.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;
	if not OpenTables([TiposDia]) then exit;
	DbList1.Reload;
    Notebook.PageIndex := 0;
   	Result := True;
end;

procedure TFormTiposDia.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormTiposDia.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
		txt_tipoDia.Text		:= FieldByName('TipoDia').AsString;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormTiposDia.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('TipoDia').AsString));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFormTiposDia.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    txt_TipoDia.SetFocus;
end;

procedure TFormTiposDia.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormTiposDia.Limpiar_Campos();
begin
	txt_TipoDia.Clear;
	txt_Descripcion.Clear;
end;

procedure TFormTiposDia.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTiposDia.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormTiposDia.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_TipoDia.SetFocus;
end;

procedure TFormTiposDia.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then Append
			else Edit;
			FieldByName('TipoDia').AsString			:= Trim(txt_TipoDia.text);
			FieldByName('Descripcion').AsString		:= Trim(txt_Descripcion.text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormTiposDia.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormTiposDia.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTiposDia.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormTiposDia.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

end.
