{*******************************************************************************

Etiqueta    :   TASK_128_MGO_20170131
Descripci�n :   Se obtienen valores del Par�metro en vez de la tabla entera
                Se guarda nuevo valor en variable p�blica en vez de hacer update

*******************************************************************************}

unit frmEditarVal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, ExtCtrls, StdCtrls, Util, UtilDB, UtilProc, Buttons,
  PeaProcs, constParametrosGenerales, StrUtils;                                 // TASK_128_MGO_20170131

type
  TfrmValueEdit = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    txtValor: TEdit;
    Label1: TLabel;
    lblDescri: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure txtValorChange(Sender: TObject);
  private
    //FClass: String[1];                                                        // TASK_128_MGO_20170131
    FName: String;
    FType: String[1];
    //FTabla: TADOTable;                                                        // TASK_128_MGO_20170131
    //FRecNum: Integer;                                                         // TASK_128_MGO_20170131
  public
    FValor: String;                                                             // TASK_128_MGO_20170131
    //function Inicializar(Tabla: TADOTable; RecNum: Integer): Boolean;                 // TASK_128_MGO_20170131
    function Inicializar(Nombre, Descripcion, TipoParametro, Valor: string): Boolean;   // TASK_128_MGO_20170131
  end;

var
  frmValueEdit: TfrmValueEdit;

implementation

{$R *.dfm}

{ TfrmValueEdit }

{ INICIO : TASK_128_MGO_20170131
function TfrmValueEdit.Inicializar(Tabla: TADOTable; RecNum: Integer): Boolean;
begin
    lblDescri.Caption := Tabla.FieldByName('Descripcion').AsString;
    FClass := Tabla.FieldByName('ClaseParametro').AsString;
    FName := Trim(Tabla.FieldByName('Nombre').AsString);
    FType := Trim(Tabla.FieldByName('TipoParametro').AsString);
    txtValor.Text := Trim(Tabla.FieldByName('Valor').AsString);
    FTabla := Tabla;
    FRecNum:= RecNum;
    Result := True;
end;
}
function TfrmValueEdit.Inicializar(Nombre, Descripcion, TipoParametro, Valor: string): Boolean;
resourcestring
    MSG_TITLE = 'Editar valor Par�metro General';
    MSG_ERROR = 'Ha habido un error al inicializar el formulario';
begin
    Result := True;
    try
        lblDescri.Caption := Trim(Descripcion);
        FType := Trim(TipoParametro);
        FName := Trim(Nombre);
        FValor := Trim(Valor);   
        txtValor.Text := Trim(Valor);
    except
        on e: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR, e.Message, MSG_TITLE, MB_ICONERROR);
        end;
    end;
end;
// FIN : TASK_128_MGO_20170131

procedure TfrmValueEdit.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_INVALID_DATE = 'El valor ingresado no es una fecha valida';
    MSG_INVALID_NUMBER = 'El valor ingresado no es num�rico';                   // TASK_128_MGO_20170131
    MSG_INVALID_INT = 'El valor ingresado no es un entero';                     // TASK_128_MGO_20170131
    MSG_VALUE = 'Valor No V�lido';
    MSG_UPDATE = 'Error al actualizar';
    MSG_ERROR = 'Error';
var                                                                             // TASK_128_MGO_20170131
    ValorDecimal: Double;                                                       // TASK_128_MGO_20170131
    ValorEntero: Integer;
    Code: Integer;                                                              // TASK_128_MGO_20170131
begin
    try
        if FType = TP_FECHA then begin
            if  not ValidateControls([txtValor], [(IsValidDate(txtValor.text))],
              MSG_VALUE,[MSG_INVALID_DATE]) then begin
                Exit;
            end else begin
                txtValor.Text := FormatDateTime('dd/mm/yyyy', StrToDateTime(txtValor.text));
            end;
        end;

        // INICIO : TASK_128_MGO_20170131
        if FType = TP_NUMERICO then begin                    
            ValorDecimal := 0;
            txtValor.Text := ReplaceStr(txtValor.Text,',','.');
            Val(txtValor.Text, ValorDecimal, Code);
            if Code <> 0 then begin
                MsgBoxBalloon(MSG_INVALID_NUMBER, MSG_VALUE, MB_ICONSTOP, txtValor);
                Exit;
            end;
        end;

        if FType = TP_ENTERO then begin
            ValorEntero := 0;
            txtValor.Text := ReplaceStr(txtValor.Text,',','.');
            Val(txtValor.Text, ValorEntero, Code);
            if Code <> 0 then begin
                MsgBoxBalloon(MSG_INVALID_INT, MSG_VALUE, MB_ICONSTOP, txtValor);
                Exit;
            end;
        end;

        FValor := txtValor.Text;
        
        {
        FTabla.Close;
        QueryExecute(FTabla.Connection,
        'UPDATE PARAMETROSGENERALES SET VALOR =' + QuotedStr(Trim(txtValor.Text)) +
        'WHERE CLASEPARAMETRO = ' + QuotedStr(FClass) + ' AND ' +
        'NOMBRE = ' + QuotedStr(FName));
        FTabla.Open;
        FTabla.MoveBy(FRecNum - 1);
        } // FIN : TASK_128_MGO_20170131
        ModalResult := mrOK;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, MSG_UPDATE, 0);
            //ModalResult := mrOK;                                              // TASK_128_MGO_20170131
        end;
    end;
end;

procedure TfrmValueEdit.Label3Click(Sender: TObject);
var
    Ubica: String;
begin
    Ubica := BrowseForFolder('Elija una carpeta');
    if Ubica <> '' then txtValor.Text := Ubica;
end;

// INICIO : TASK_128_MGO_20170131
procedure TfrmValueEdit.txtValorChange(Sender: TObject);
begin
    FValor := txtValor.Text;
end;
// FIN : TASK_128_MGO_20170131

end.
