object FormComunas: TFormComunas
  Left = 191
  Top = 177
  Caption = 'Mantenimiento de Comunas'
  ClientHeight = 402
  ClientWidth = 788
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 788
    Height = 33
    Habilitados = [btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 788
    Height = 215
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'201'#0'Pa'#237's'
      #0'187'#0'Regi'#243'n'
      #0'164'#0'Comuna                                       '
      #0'41'#0'Ciudad')
    HScrollBar = True
    RefreshTime = 100
    Table = Comunas
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 248
    Width = 788
    Height = 115
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label2: TLabel
      Left = 35
      Top = 63
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 35
      Top = 38
      Width = 44
      Height = 13
      Caption = '&C'#243'digo:'
      FocusControl = txt_CodigoComuna
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 35
      Top = 14
      Width = 31
      Height = 13
      Caption = '&Pa'#237's:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 363
      Top = 14
      Width = 45
      Height = 13
      Caption = '&Regi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 363
      Top = 64
      Width = 83
      Height = 13
      Caption = 'C'#243'digo &Postal:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel
      Left = 363
      Top = 38
      Width = 44
      Height = 13
      Caption = 'Ci&udad:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_descripcion: TEdit
      Left = 111
      Top = 59
      Width = 240
      Height = 21
      Hint = 'Nombre de la Comuna'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnKeyPress = txt_descripcionKeyPress
    end
    object txt_CodigoComuna: TEdit
      Left = 111
      Top = 34
      Width = 54
      Height = 21
      Hint = 'C'#243'digo de la Comuna'
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = False
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object cb_pais: TComboBox
      Left = 423
      Top = 60
      Width = 20
      Height = 21
      Hint = 'Pa'#237's'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 0
      MaxLength = 4
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = False
    end
    object cb_regiones: TComboBox
      Left = 450
      Top = 60
      Width = 18
      Height = 21
      Hint = 'Regi'#243'n de la Comuna'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 0
      MaxLength = 4
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Visible = False
    end
    object txt_CP: TNumericEdit
      Left = 391
      Top = 60
      Width = 25
      Height = 21
      Hint = 'C'#243'digo postal de la comuna'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      Visible = False
    end
    object txtCiudad: TEdit
      Left = 455
      Top = 34
      Width = 240
      Height = 21
      Hint = 'Ciudad de la Comuna'
      Color = 16444382
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
    end
    object txtPais: TEdit
      Left = 111
      Top = 10
      Width = 240
      Height = 21
      Hint = 'Nombre de la Comuna'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnKeyPress = txt_descripcionKeyPress
    end
    object txtRegion: TEdit
      Left = 455
      Top = 10
      Width = 240
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnKeyPress = txt_descripcionKeyPress
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 363
    Width = 788
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 591
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object BuscaTablaPaises: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = qry_Paises
    Left = 544
    Top = 121
  end
  object BuscaTablaRegiones: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = qry_Regiones
    Left = 644
    Top = 121
  end
  object qry_Paises: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'select * from paises WITH (NOLOCK) ')
    Left = 544
    Top = 64
  end
  object qry_Regiones: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    Filtered = True
    Parameters = <
      item
        Name = 'CodigoPais'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      'CodigoPais, CodigoRegion, Descripcion'
      'FROM Regiones  WITH (NOLOCK) '
      'WHERE '
      '(:CodigoPais = Regiones.CodigoPais)')
    Left = 645
    Top = 64
  end
  object Comunas: TADOTable
    Connection = DMConnections.BaseCAC
    IndexName = 'PK_Comunas'
    TableName = 'Comunas'
    Left = 437
    Top = 64
  end
end
