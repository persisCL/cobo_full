object FormModulos: TFormModulos
  Left = 220
  Top = 144
  Width = 656
  Height = 411
  Caption = 'Consulta de M'#243'dulos de Sistemas'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 648
    Height = 33
    Habilitados = [btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 648
    Height = 207
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'92'#0'C'#243'digo                 '
      
        #0'280'#0'Descripci'#243'n                                                ' +
        '                        '
      #0'100'#0'Categor'#237'a               ')
    HScrollBar = True
    RefreshTime = 100
    Table = Modulos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 240
    Width = 648
    Height = 98
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 19
      Top = 41
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 19
      Top = 67
      Width = 124
      Height = 13
      Caption = '&Categor'#237'a de M'#243'dulo:'
      FocusControl = cb_CategoriasModulo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 20
      Top = 15
      Width = 44
      Height = 13
      Caption = '&C'#243'digo:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 152
      Top = 36
      Width = 305
      Height = 21
      Color = 16444382
      MaxLength = 40
      TabOrder = 0
    end
    object cb_CategoriasModulo: TComboBox
      Left = 152
      Top = 63
      Width = 305
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
    end
    object neCodigoModulo: TNumericEdit
      Left = 152
      Top = 10
      Width = 107
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 4
      ReadOnly = True
      TabOrder = 2
      Decimals = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 338
    Width = 648
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 451
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Modulos: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Modulos'
    Left = 242
    Top = 110
  end
  object qry_MaxCodigoModulo: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      
        'SELECT ISNULL(MAX(CodigoModulo),0) AS CodigoModulo FROM MODULOS ' +
        ' WITH (NOLOCK) ')
    Left = 329
    Top = 110
  end
end
