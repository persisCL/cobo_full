object frmABMBandasHorarias: TfrmABMBandasHorarias
  Left = 0
  Top = 0
  Caption = 'Administrar Bandas Horarias'
  ClientHeight = 684
  ClientWidth = 960
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlSuperior: TPanel
    Left = 0
    Top = 0
    Width = 960
    Height = 226
    Align = alTop
    TabOrder = 0
    object dbgrdEsquemas: TDBGrid
      Left = 1
      Top = 42
      Width = 958
      Height = 149
      Align = alClient
      DataSource = dsEsquemas
      Enabled = False
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDrawColumnCell = dbgrdEsquemasDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'CodigoEsquema'
          Title.Caption = 'ID de Esquema'
          Width = 85
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descripcion'
          Title.Caption = 'Descripci'#243'n'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CategoriasClasesDesc'
          Title.Caption = 'Clase Categor'#237'a'
          Width = 115
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CompletoDesc'
          Title.Caption = 'Completo'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UsuarioCreacion'
          Title.Caption = 'Usuario Creador'
          Width = 85
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FechaHoraCreacion'
          Title.Caption = 'Fecha Creaci'#243'n'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UsuarioModificacion'
          Title.Caption = 'Usuario Modificador'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FechaHoraModificacion'
          Title.Caption = 'Fecha Modificaci'#243'n'
          Width = 110
          Visible = True
        end>
    end
    object pnl1: TPanel
      Left = 1
      Top = 1
      Width = 958
      Height = 41
      Align = alTop
      TabOrder = 0
      object tlbBotonera: TToolBar
        Left = 9
        Top = 6
        Width = 176
        Height = 29
        Align = alNone
        ButtonHeight = 25
        Caption = 'tlbBotonera'
        Images = ilImagenes
        TabOrder = 0
        object btnSalir: TToolButton
          Left = 0
          Top = 0
          Hint = 'Salir'
          Caption = 'btnSalir'
          ImageIndex = 0
          ParentShowHint = False
          ShowHint = True
          OnClick = btnSalirClick
        end
        object btn1: TToolButton
          Left = 23
          Top = 0
          Width = 8
          Caption = 'btn1'
          ImageIndex = 1
          Style = tbsSeparator
          Visible = False
        end
        object btnAgregar: TToolButton
          Left = 31
          Top = 0
          Hint = 'Agregar tipo de convenio'
          Caption = 'btnAgregar'
          ImageIndex = 1
          ParentShowHint = False
          ShowHint = True
          OnClick = btnAgregarClick
        end
        object btnEliminar: TToolButton
          Left = 54
          Top = 0
          Hint = 'Eliminar/Deshabilitar tipo de convenio'
          Caption = 'btnEliminar'
          ImageIndex = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnEliminarClick
        end
        object btnEditar: TToolButton
          Left = 77
          Top = 0
          Hint = 'Modificar tipo de convenio'
          Caption = 'btnEditar'
          ImageIndex = 3
          ParentShowHint = False
          ShowHint = True
          OnClick = btnEditarClick
        end
        object btn2: TToolButton
          Left = 100
          Top = 0
          Width = 8
          Caption = 'btn2'
          ImageIndex = 4
          Style = tbsSeparator
          Visible = False
        end
        object btnImprimir: TToolButton
          Left = 108
          Top = 0
          Caption = 'btnImprimir'
          Enabled = False
          ImageIndex = 4
        end
        object btn3: TToolButton
          Left = 131
          Top = 0
          Width = 8
          Caption = 'btn3'
          ImageIndex = 5
          Style = tbsSeparator
          Visible = False
        end
        object btnBuscar: TToolButton
          Left = 139
          Top = 0
          Caption = 'btnBuscar'
          Enabled = False
          ImageIndex = 5
        end
      end
    end
    object pnl3: TPanel
      Left = 1
      Top = 191
      Width = 958
      Height = 34
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object lbl2: TLabel
        Left = 16
        Top = 10
        Width = 76
        Height = 13
        Caption = 'ID de Esquema:'
      end
      object lbl3: TLabel
        Left = 180
        Top = 10
        Width = 58
        Height = 13
        Caption = 'Descripci'#243'n:'
      end
      object lbl7: TLabel
        Left = 486
        Top = 10
        Width = 95
        Height = 13
        Caption = 'Clase de Categor'#237'a:'
      end
      object edtCodigoEsquema: TEdit
        Left = 98
        Top = 7
        Width = 51
        Height = 21
        Color = clInactiveCaption
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaptionText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object edtDescripcion: TEdit
        Left = 244
        Top = 7
        Width = 211
        Height = 21
        TabOrder = 1
      end
      object vcbCategoriaClase: TVariantComboBox
        Left = 587
        Top = 7
        Width = 145
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 2
        Items = <>
      end
    end
  end
  object pnlMedio: TPanel
    Left = 0
    Top = 226
    Width = 960
    Height = 420
    Align = alClient
    TabOrder = 1
    object pnl2: TPanel
      Left = 1
      Top = 1
      Width = 958
      Height = 35
      Align = alTop
      TabOrder = 0
      object lbl1: TLabel
        Left = 91
        Top = 10
        Width = 92
        Height = 13
        Caption = 'Bandas Horarias'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl9: TLabel
        Left = 416
        Top = 4
        Width = 461
        Height = 26
        Caption = 
          'Ingrese todas las franjas horarias para cada Tipo de D'#237'a para fo' +
          'rmar un Esquema Completo.'#13#10'De estar Incompleto, el Esquema no po' +
          'dr'#225' ser utilizado por Puntos de Cobro y Planes Tarifarios.'
        WordWrap = True
      end
      object tlb1: TToolBar
        Left = 9
        Top = 4
        Width = 82
        Height = 25
        Align = alNone
        Caption = 'tlbBotonera'
        Images = ilImagenes
        TabOrder = 0
        object btnAgregarHorario: TToolButton
          Left = 0
          Top = 0
          Hint = 'Agregar tipo de convenio'
          Caption = 'btnAgregar'
          ImageIndex = 1
          ParentShowHint = False
          ShowHint = True
          OnClick = btnAgregarHorarioClick
        end
        object btnEliminarHorario: TToolButton
          Left = 23
          Top = 0
          Hint = 'Eliminar/Deshabilitar tipo de convenio'
          Caption = 'btnEliminar'
          ImageIndex = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnEliminarHorarioClick
        end
        object btnEditarHorario: TToolButton
          Left = 46
          Top = 0
          Hint = 'Modificar tipo de convenio'
          Caption = 'btnEditar'
          ImageIndex = 3
          ParentShowHint = False
          ShowHint = True
          OnClick = btnEditarHorarioClick
        end
      end
    end
    object dbgrdBandasHorarias: TDBGrid
      Left = 1
      Top = 36
      Width = 958
      Height = 304
      Align = alClient
      DataSource = dsBandasHorarias
      Enabled = False
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'TipoDiaDescripcion'
          Title.Caption = 'Tipo D'#237'a'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HoraDesde'
          Title.Caption = 'Desde'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HoraHasta'
          Title.Caption = 'Hasta'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoTarifaDescripcion'
          Title.Caption = 'Tarifa'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UsuarioCreacion'
          Title.Caption = 'Usuario Creador'
          Width = 85
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FechaHoraCreacion'
          Title.Caption = 'Fecha Creaci'#243'n'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UsuarioModificacion'
          Title.Caption = 'Usuario Modificador'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FechaHoraModificacion'
          Title.Caption = 'Fecha Modificaci'#243'n'
          Width = 110
          Visible = True
        end>
    end
    object pnl4: TPanel
      Left = 1
      Top = 340
      Width = 958
      Height = 79
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object lbl4: TLabel
        Left = 13
        Top = 14
        Width = 42
        Height = 13
        Caption = 'Tipo D'#237'a:'
      end
      object lbl5: TLabel
        Left = 21
        Top = 48
        Width = 34
        Height = 13
        Caption = 'Desde:'
      end
      object lbl6: TLabel
        Left = 152
        Top = 48
        Width = 32
        Height = 13
        Caption = 'Hasta:'
      end
      object lbl8: TLabel
        Left = 290
        Top = 48
        Width = 32
        Height = 13
        Caption = 'Tarifa:'
      end
      object vcbTipoDia: TVariantComboBox
        Left = 61
        Top = 11
        Width = 188
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = vcbTipoDiaChange
        Items = <>
      end
      object vcbTarifa: TVariantComboBox
        Left = 328
        Top = 45
        Width = 169
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 3
        Items = <>
      end
      object tedtDesde: TTimeEdit
        Left = 61
        Top = 45
        Width = 57
        Height = 21
        AutoSelect = False
        Enabled = False
        TabOrder = 1
        AllowEmpty = False
        ShowSeconds = False
      end
      object tedtHasta: TTimeEdit
        Left = 190
        Top = 45
        Width = 59
        Height = 21
        AutoSelect = False
        TabOrder = 2
        AllowEmpty = False
        ShowSeconds = False
      end
      object btnAceptar: TButton
        Left = 560
        Top = 43
        Width = 75
        Height = 25
        Caption = '&Aceptar'
        TabOrder = 4
        OnClick = btnAceptarClick
      end
      object btnCancelarHorario: TButton
        Left = 641
        Top = 43
        Width = 75
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 5
        OnClick = btnCancelarHorarioClick
      end
    end
  end
  object pnlInferior: TPanel
    Left = 0
    Top = 646
    Width = 960
    Height = 38
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      960
      38)
    object nb1: TNotebook
      Left = 690
      Top = 1
      Width = 266
      Height = 34
      Anchors = [akRight, akBottom]
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'pgSalir'
        object btnSalir1: TButton
          Left = 184
          Top = 4
          Width = 75
          Height = 25
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btnSalir1Click
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'pgAltaModi'
        DesignSize = (
          266
          34)
        object btnCancelar: TButton
          Left = 184
          Top = 4
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = 'Ca&ncelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
        object btnGuardar: TButton
          Left = 64
          Top = 4
          Width = 114
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = '&Guardar Esquema'
          Default = True
          TabOrder = 0
          OnClick = btnGuardarClick
        end
      end
    end
  end
  object ilImagenes: TImageList
    Left = 704
    Top = 65528
    Bitmap = {
      494C010106000800880010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF000000FF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000008080
      800000000000000000000000000000000000FFFF0000FFFF0000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000008080800000000000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000C0C0C000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFF0000FFFF0000FFFF0000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      800000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      800000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000000000000000000000FF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      800000008000000080000000800000008000000080000000800000FFFF000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF00000000000000000000FFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      80000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF00000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      80000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF00000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF008001FFFF000000000000FFFD00000000
      0000FFF8000000000000FFF1000000000000F023000000000000E78700000000
      0000CF0F0000000000009FA700000000E007BFD700000000E007BFF700000000
      E007AFF700000000E007AFF700000000E00F87E700000000E01FC1CF00000000
      E03FE79F00000000E07FF03F00000000C007FFFFFFFFFC00C007E7F8FFF8FC00
      C007E7F8FFF82000C00781FF81FF0000C00781FC81FC0000C007E7FCFFFC0000
      C007E7FFFFFF0000C007FFFCFFFC0000C007FEFCF7FC0000C007FE7FE7FF0000
      C00780138013E000C00780138013F800C007FE7FE7FFF000C007FEF8F7F8E001
      C007FFF8FFF8C403C007FFFFFFFFEC0700000000000000000000000000000000
      000000000000}
  end
  object dsEsquemas: TDataSource
    DataSet = cdsEsquemas
    Left = 376
    Top = 96
  end
  object dsBandasHorarias: TDataSource
    DataSet = cdsBandasHorarias
    Left = 464
    Top = 407
  end
  object cdsEsquemas: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoEsquema'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Completo'
        DataType = ftBoolean
      end
      item
        Name = 'CompletoDesc'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'UsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioModificacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraModificacion'
        DataType = ftDateTime
      end
      item
        Name = 'Id_CategoriasClases'
        DataType = ftInteger
      end
      item
        Name = 'CategoriasClasesDesc'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dtstprEsquemas'
    StoreDefs = True
    AfterScroll = cdsEsquemasAfterScroll
    Left = 288
    Top = 96
  end
  object cdsBandasHorarias: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoBandaHoraria'
        DataType = ftInteger
      end
      item
        Name = 'CodigoEsquema'
        DataType = ftInteger
      end
      item
        Name = 'CodigoDiaTipo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TipoDiaDescripcion'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'HoraDesde'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'HoraHasta'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoTarifaTipo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TipoTarifaDescripcion'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'UsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioModificacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraModificacion'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dtstprBandasHorarias'
    StoreDefs = True
    AfterScroll = cdsBandasHorariasAfterScroll
    Left = 352
    Top = 407
  end
  object dtstprEsquemas: TDataSetProvider
    DataSet = spADM_Esquemas_SELECT
    Constraints = False
    Left = 192
    Top = 96
  end
  object dtstprBandasHorarias: TDataSetProvider
    DataSet = spADM_BandasHorarias_SELECT
    Left = 224
    Top = 407
  end
  object spADM_Esquemas_SELECT: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_Esquemas_SELECT'
    Parameters = <>
    Left = 72
    Top = 96
    object intgrfldADM_Esquemas_SELECTCodigoEsquema: TIntegerField
      FieldName = 'CodigoEsquema'
    end
    object strngfldADM_Esquemas_SELECTDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 255
    end
    object blnfldADM_Esquemas_SELECTCompleto: TBooleanField
      FieldName = 'Completo'
    end
    object strngfldADM_Esquemas_SELECTCompletoDesc: TStringField
      FieldName = 'CompletoDesc'
      Size = 2
    end
    object strngfldADM_Esquemas_SELECTUsuarioCreador: TStringField
      FieldName = 'UsuarioCreacion'
    end
    object dtmfldADM_Esquemas_SELECTFechaCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object strngfldADM_Esquemas_SELECTUsuarioModificador: TStringField
      FieldName = 'UsuarioModificacion'
    end
    object dtmfldADM_Esquemas_SELECTFechaModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
    object procADM_Esquemas_SELECTId_CategoriasClases: TIntegerField
      FieldName = 'Id_CategoriasClases'
    end
    object strngfldADM_Esquemas_SELECTCategoriasClasesDesc: TStringField
      FieldName = 'CategoriasClasesDesc'
      Size = 50
    end
  end
  object spADM_BandasHorarias_SELECT: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_BandasHorarias_SELECT'
    Parameters = <>
    Left = 72
    Top = 407
    object intgrfldADM_BandasHorarias_SELECTCodigoBandaHoraria: TIntegerField
      FieldName = 'CodigoBandaHoraria'
    end
    object intgrfldADM_BandasHorarias_SELECTCodigoEsquema: TIntegerField
      FieldName = 'CodigoEsquema'
    end
    object strngfldADM_BandasHorarias_SELECTCodigoDiaTipo: TStringField
      FieldName = 'CodigoDiaTipo'
    end
    object strngfldADM_BandasHorarias_SELECTTipoDiaDescripcion: TStringField
      DisplayWidth = 60
      FieldName = 'TipoDiaDescripcion'
      Size = 60
    end
    object strngfldADM_BandasHorarias_SELECTHoraDesde: TStringField
      FieldName = 'HoraDesde'
      Size = 5
    end
    object strngfldADM_BandasHorarias_SELECTHoraHasta: TStringField
      FieldName = 'HoraHasta'
      Size = 5
    end
    object strngfldADM_BandasHorarias_SELECTCodigoTarifaTipo: TStringField
      FieldName = 'CodigoTarifaTipo'
    end
    object strngfldADM_BandasHorarias_SELECTTarifa: TStringField
      DisplayWidth = 60
      FieldName = 'TipoTarifaDescripcion'
      Size = 60
    end
    object strngfldADM_BandasHorarias_SELECTUsuarioCreador: TStringField
      FieldName = 'UsuarioCreacion'
    end
    object dtmfldADM_BandasHorarias_SELECTFechaCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object strngfldADM_BandasHorarias_SELECTUsuarioModificador: TStringField
      FieldName = 'UsuarioModificacion'
    end
    object dtmfldADM_BandasHorarias_SELECTFechaModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
  end
  object spADM_Esquemas_INSERT: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_Esquemas_INSERT'
    Parameters = <>
    Left = 824
    Top = 48
  end
  object spADM_Esquemas_UPDATE: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_Esquemas_UPDATE'
    Parameters = <>
    Left = 824
    Top = 96
  end
  object spADM_Esquemas_DELETE: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_Esquemas_DELETE'
    Parameters = <>
    Left = 824
    Top = 144
  end
  object spADM_BandasHorarias_UPDATE: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_BandasHorarias_UPDATE'
    Parameters = <>
    Left = 816
    Top = 392
  end
  object spADM_BandasHorarias_INSERT: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_BandasHorarias_INSERT'
    Parameters = <>
    Left = 816
    Top = 344
  end
  object spADM_BandasHorarias_DELETE: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'ADM_BandasHorarias_DELETE'
    Parameters = <>
    Left = 816
    Top = 440
  end
end
