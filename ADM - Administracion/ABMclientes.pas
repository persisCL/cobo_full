unit ABMclientes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB,
  DPSControls;

type
  TFormTiposCliente = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    Descripcion: TEdit;
    tipoCliente: TEdit;
    Clientes: TADOTable;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    Label2: TLabel;
    MemoComentarios: TMemo;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos();
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormTiposCliente: TFormTiposCliente;

implementation
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudo actualizar el Tipo de Cliente.';
    MSG_ACTUALIZAR_CAPTION 	= 'Eliminar Tipo de Cliente';
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Tipo de Cliente?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar el Tipo de Cliente porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Tipo de Cliente';

{$R *.DFM}

function TFormTiposCliente.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([clientes]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormTiposCliente.BtnCancelarClick(Sender: TObject);
begin
   	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFormTiposCliente.DBList1Click(Sender: TObject);
begin
	 with clientes do begin
		  tipoCliente.Text 		:= FieldByName('tipoCliente').AsString;
		  Descripcion.text 	    := FieldByName('Descripcion').AsString;
          MemoComentarios.Text  := FieldByName('Comentarios').AsString;
     end;
end;

procedure TFormTiposCliente.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormTiposCliente.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    TipoCliente.setFocus;
end;

procedure TFormTiposCliente.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormTiposCliente.Limpiar_Campos();
begin
	tipoCliente.Clear;
	Descripcion.Clear;
    MemoComentarios.Clear;
end;

procedure TFormTiposCliente.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTiposCliente.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			Clientes.Delete;
		Except
			On E: Exception do begin
				Clientes.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormTiposCliente.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
	tipoCliente.SetFocus;
end;

procedure TFormTiposCliente.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With Clientes do begin
		Try
			if DbList1.Estado = Alta then begin
				Append;

            end
			else begin
				Edit;
            end;
			FieldByName('tipoCliente').AsString  := tipoCliente.Text;
			FieldByName('Descripcion').AsString  := Descripcion.text;
            FieldByName('Comentarios').AsString  := MemoComentarios.text;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    groupb.Enabled     := False;
  	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	DbList1.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormTiposCliente.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormTiposCliente.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTiposCliente.BtnSalirClick(Sender: TObject);
begin
     close;
end;

end.
