object FormMaestroPersonal: TFormMaestroPersonal
  Left = 188
  Top = 88
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Usuarios'
  ClientHeight = 699
  ClientWidth = 972
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlInferior: TPanel
    Left = 0
    Top = 661
    Width = 972
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 534
    ExplicitWidth = 817
    object Label3: TLabel
      Left = 35
      Top = 13
      Width = 63
      Height = 13
      Caption = 'Desactivado.'
    end
    object Notebook: TNotebook
      Left = 775
      Top = 0
      Width = 197
      Height = 38
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      ExplicitLeft = 620
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 74
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 107
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
    object Panel1: TPanel
      Left = 16
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 972
    Height = 35
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
    ExplicitWidth = 817
  end
  object alMaestroPersonal: TAbmList
    Left = 0
    Top = 35
    Width = 972
    Height = 282
    TabStop = True
    TabOrder = 2
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = 12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'119'#0'C'#243'digo'
      #0'143'#0'Apellido'
      #0'45'#0'Nombre')
    HScrollBar = True
    Table = MaestroPersonal
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alMaestroPersonalClick
    OnKeyPress = alMaestroPersonalKeyPress
    OnDrawItem = alMaestroPersonalDrawItem
    OnRefresh = alMaestroPersonalRefresh
    OnInsert = alMaestroPersonalInsert
    OnDelete = alMaestroPersonalDelete
    OnEdit = alMaestroPersonalEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
    ExplicitWidth = 817
  end
  object pcDatosPersonal: TPageControl
    Left = 0
    Top = 317
    Width = 972
    Height = 344
    ActivePage = tsCliente
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 3
    ExplicitWidth = 817
    ExplicitHeight = 217
    object tsCliente: TTabSheet
      Caption = 'Identificaci'#243'n'
      ExplicitWidth = 809
      ExplicitHeight = 189
      object pnlDatosPersonal: TPanel
        Left = 0
        Top = 0
        Width = 964
        Height = 316
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 809
        ExplicitHeight = 189
        inline FrePersona: TFramePersona
          Left = 0
          Top = 12
          Width = 809
          Height = 193
          TabOrder = 0
          TabStop = True
          ExplicitTop = 12
          ExplicitWidth = 809
          ExplicitHeight = 193
          inherited Label2: TLabel
            Width = 74
            ExplicitWidth = 74
          end
          inherited Label10: TLabel
            Width = 47
            Visible = False
            ExplicitWidth = 47
          end
          inherited lblApellidoMaterno: TLabel
            Width = 82
            ExplicitWidth = 82
          end
          inherited Label11: TLabel
            Left = 3
            Top = 164
            Visible = False
            ExplicitLeft = 3
            ExplicitTop = 164
          end
          inherited lblFechaNacCreacion: TLabel
            Left = 409
            Top = 113
            Width = 89
            ExplicitLeft = 409
            ExplicitTop = 113
            ExplicitWidth = 89
          end
          inherited lblMail: TLabel
            Width = 95
            Visible = False
            ExplicitWidth = 95
          end
          inherited cbActividades: TComboBox
            Visible = False
          end
          inherited btn_Buscar: TBitBtn
            OnClick = FrePersonabtn_BuscarClick
          end
          inherited txtFechaNacimiento: TDateEdit
            Left = 524
            Top = 109
            ExplicitLeft = 524
            ExplicitTop = 109
          end
          inherited cbSituacionIVA: TComboBox
            Left = 118
            Top = 159
            Visible = False
            ExplicitLeft = 118
            ExplicitTop = 159
          end
          inherited txt_Documento: TEdit
            OnChange = FrePersonatxt_DocumentoChange
          end
          inherited txt_email: TEdit
            Visible = False
          end
        end
        object PNL_Chk_Borrar: TPanel
          Left = 653
          Top = 124
          Width = 144
          Height = 17
          BevelOuter = bvNone
          TabOrder = 1
          object chkActivo: TCheckBox
            Left = 5
            Top = 0
            Width = 61
            Height = 17
            Alignment = taLeftJustify
            Caption = '&Activo'
            TabOrder = 0
          end
        end
      end
    end
    object tsDomicilio: TTabSheet
      Caption = 'Domicilio'
      ImageIndex = 1
      ExplicitWidth = 809
      ExplicitHeight = 189
      object pnlDomicilio: TPanel
        Left = 0
        Top = 0
        Width = 964
        Height = 316
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 809
        ExplicitHeight = 189
        DesignSize = (
          964
          316)
        inline FreDomicilios: TFrameListDomicilios
          Left = 5
          Top = 5
          Width = 959
          Height = 315
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 5
          ExplicitTop = 5
          ExplicitWidth = 804
          ExplicitHeight = 188
          DesignSize = (
            959
            315)
          inherited dblDomicilios: TDBListEx
            Left = -1
            Width = 959
            Height = 274
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 0
                Header.Caption = 'Direcci'#243'n'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'MS Sans Serif'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DireccionCompleta'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 0
                Header.Caption = 'Domicilio de Entrega'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -12
                Header.Font.Name = 'MS Sans Serif'
                Header.Font.Style = []
                IsLink = False
              end>
            ExplicitLeft = -1
            ExplicitWidth = 959
            ExplicitHeight = 274
          end
          inherited btnAgregarDomicilio: TButton
            Left = 724
            Top = 279
            OnClick = FreDomiciliosbtnAgregarDomicilioClick
            ExplicitLeft = 724
            ExplicitTop = 279
          end
          inherited btnEditarDomicilio: TButton
            Left = 799
            Top = 279
            ExplicitLeft = 799
            ExplicitTop = 279
          end
          inherited btnEliminarDomicilio: TButton
            Left = 874
            Top = 279
            ExplicitLeft = 874
            ExplicitTop = 279
          end
          inherited cdsDomicilios: TClientDataSet
            IndexDefs = <
              item
                Name = 'DireccionCompleta'
              end>
          end
        end
      end
    end
    object tsContacto: TTabSheet
      Caption = 'Contacto'
      ImageIndex = 2
      ExplicitWidth = 809
      ExplicitHeight = 189
      object pnlMediosContacto: TPanel
        Left = 0
        Top = 0
        Width = 964
        Height = 316
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 809
        ExplicitHeight = 189
        DesignSize = (
          964
          316)
        inline FreMediosContactos: TFrameListMediosContactos
          Left = 5
          Top = 0
          Width = 959
          Height = 316
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 5
          ExplicitWidth = 804
          ExplicitHeight = 189
          DesignSize = (
            959
            316)
          inherited dblMedios: TDBListEx
            Width = 957
            Height = 276
            Columns = <
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 0
                Header.Caption = 'Tipo'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'DescriTipoMedioContacto'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 0
                Header.Caption = 'Medio de Contacto'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Valor'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 0
                Header.Caption = 'Detalle'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                IsLink = False
                FieldName = 'Detalle'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 0
                Header.Caption = 'Hora desde'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'HorarioDesde'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Width = 0
                Header.Caption = 'Hora hasta'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = []
                Header.Alignment = taCenter
                IsLink = False
                FieldName = 'HorarioHasta'
              end>
            ExplicitWidth = 957
            ExplicitHeight = 276
          end
          inherited btnAgregarMedioComunicacion: TButton
            Left = 733
            Top = 288
            ExplicitLeft = 733
            ExplicitTop = 288
          end
          inherited btnEditarMedioComunicacion: TButton
            Left = 808
            Top = 288
            ExplicitLeft = 808
            ExplicitTop = 288
          end
          inherited btnEliminarMedioComunicacion: TButton
            Left = 882
            Top = 288
            ExplicitLeft = 882
            ExplicitTop = 288
          end
          inherited cdsMediosComunicacionBorrados: TClientDataSet
            Top = 168
          end
        end
      end
    end
    object tsSueldos: TTabSheet
      Caption = 'Datos Empleado'
      ImageIndex = 3
      object pnlSueldos: TPanel
        Left = 0
        Top = 0
        Width = 964
        Height = 316
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 70
          Width = 64
          Height = 13
          Caption = 'Sueldo Bruto:'
        end
        object Label20: TLabel
          Left = 7
          Top = 44
          Width = 75
          Height = 13
          Caption = 'Tipo de Sueldo:'
        end
        object Label21: TLabel
          Left = 8
          Top = 93
          Width = 62
          Height = 13
          Caption = 'Sueldo Neto:'
        end
        object Label19: TLabel
          Left = 9
          Top = 18
          Width = 61
          Height = 13
          Caption = 'Categor'#237'a:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txtSueldoBruto: TNumericEdit
          Left = 132
          Top = 62
          Width = 121
          Height = 21
          TabOrder = 2
          Decimals = 2
        end
        object txtSueldoNeto: TNumericEdit
          Left = 132
          Top = 87
          Width = 121
          Height = 21
          TabOrder = 3
          Decimals = 2
        end
        object cbTiposSueldo: TComboBox
          Left = 132
          Top = 37
          Width = 189
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
        end
        object cbCategoria: TComboBox
          Left = 132
          Top = 12
          Width = 278
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
        end
        object chkActivo2: TCheckBox
          Left = 8
          Top = 110
          Width = 136
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Activo:'
          TabOrder = 4
        end
      end
    end
  end
  object MaestroPersonal: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltReadOnly
    IndexFieldNames = 'Apellido;ApellidoMaterno;Nombre'
    TableName = 'VW_MaestroPersonal'
    Left = 220
  end
  object ActualizarDatosMaestroPersonal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosMaestroPersonal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCategoriaEmpleado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoSueldo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@SueldoBruto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SueldoNeto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 248
  end
  object ActualizarMedioComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Anexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@IndiceMedioComunicacion'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end>
    Left = 340
    Top = 4
  end
  object EliminarMaestroPersonal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarMaestroPersonal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Deshabilitar'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 372
  end
  object EliminarDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ListaDomicilios'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 428
  end
  object ActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoDomicilio'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@DescripcionCiudad'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 482
    Top = 4
  end
  object ActualizarDomicilioEntregaPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioEntregaPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 550
    Top = 4
  end
  object ActualizarDomicilioRelacionado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioRelacionado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 514
    Top = 4
  end
  object ActualizarDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Giro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ApellidoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaternoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NombreContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@SexoContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@EmailContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Password'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Pregunta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 280
    Top = 65532
  end
  object ObtenerDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 313
    Top = 4
  end
  object EliminarTodosDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTodosDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 456
  end
  object tmrInicializarBusqueda: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmrInicializarBusquedaTimer
    Left = 400
    Top = 3
  end
  object tmrValidarRUT: TTimer
    Enabled = False
    OnTimer = tmrValidarRUTTimer
    Left = 360
    Top = 360
  end
end
