object DetalleMensajesTipoClienteForm: TDetalleMensajesTipoClienteForm
  Left = 0
  Top = 0
  Caption = 'Detalle Mensajes Tipo Cliente'
  ClientHeight = 336
  ClientWidth = 857
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBotones: TPanel
    Left = 0
    Top = 295
    Width = 857
    Height = 41
    Align = alBottom
    TabOrder = 0
    object Notebook: TNotebook
      Left = 441
      Top = 1
      Width = 415
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 297
          Top = 5
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
        object btnNuevo: TButton
          Left = 212
          Top = 5
          Width = 79
          Height = 26
          Caption = '&Nuevo'
          TabOrder = 1
          OnClick = btnNuevoClick
        end
        object btnBorrar: TButton
          Left = 127
          Top = 5
          Width = 79
          Height = 25
          Caption = '&Borrar'
          TabOrder = 2
          OnClick = btnBorrarClick
        end
        object btnEditar: TButton
          Left = 42
          Top = 5
          Width = 79
          Height = 25
          Caption = '&Editar'
          TabOrder = 3
          OnClick = btnEditarClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 11
          Top = 5
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 96
          Top = 5
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          Visible = False
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object pnlDetalleMensajeTipoCliente: TPanel
    Left = 0
    Top = 160
    Width = 857
    Height = 135
    Align = alBottom
    TabOrder = 1
    Visible = False
    object lblCodigoTipoMedioPago: TLabel
      Left = 16
      Top = -11
      Width = 41
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblDescripcion: TLabel
      Left = 16
      Top = 35
      Width = 68
      Height = 13
      Caption = 'Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCodigoConceptoPago: TLabel
      Left = 16
      Top = 65
      Width = 110
      Height = 13
      Caption = 'C'#243'digo del Sistema:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 13
      Width = 110
      Height = 13
      Caption = 'C'#243'digo Tipo Cliente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 92
      Width = 58
      Height = 13
      Caption = 'Sem'#225'foro:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigoTipoClienteMensaje: TNumericEdit
      Left = 177
      Top = -19
      Width = 57
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
      Visible = False
    end
    object txtDescripcion: TEdit
      Left = 177
      Top = 32
      Width = 408
      Height = 21
      Color = 16444382
      MaxLength = 255
      TabOrder = 1
      OnChange = txtDescripcionChange
    end
    object cbCodigoSistema: TVariantComboBox
      Left = 177
      Top = 62
      Width = 322
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      OnSelect = cbCodigoSistemaSelect
      Items = <>
    end
    object txtCodigoTipoCliente: TNumericEdit
      Left = 177
      Top = 5
      Width = 57
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 3
    end
    object txtSemaforo: TMaskEdit
      Left = 177
      Top = 89
      Width = 90
      Height = 21
      Hint = 'Sem'#225'foro'
      CharCase = ecUpperCase
      Color = clBtnFace
      MaxLength = 6
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 4
    end
  end
  object lbMensajeTipoCliente: TStringGrid
    Left = 0
    Top = 0
    Width = 857
    Height = 160
    Align = alClient
    ColCount = 6
    DefaultRowHeight = 16
    FixedCols = 0
    RowCount = 2
    GridLineWidth = 0
    TabOrder = 2
    OnDblClick = lbMensajeTipoClienteDblClick
    ColWidths = (
      86
      136
      73
      70
      186
      279)
  end
  object cdColores: TColorDialog
    Left = 384
    Top = 152
  end
  object qrySistemas: TADOQuery
    Connection = DMConnections.BaseBO_Master
    Parameters = <>
    SQL.Strings = (
      'ObtenerSistemas')
    Left = 224
    Top = 120
  end
end
