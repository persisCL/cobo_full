object FormTiposDayPass: TFormTiposDayPass
  Left = 184
  Top = 111
  Caption = 'Tarifas de Pases Diarios'
  ClientHeight = 544
  ClientWidth = 761
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 761
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object gbTarifasDayPass: TPanel
    Left = 0
    Top = 342
    Width = 761
    Height = 163
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 1
    object Label2: TLabel
      Left = 29
      Top = 38
      Width = 89
      Height = 13
      Caption = '&Codigo Material'
      FocusControl = txt_CodigoMaterial
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 29
      Top = 86
      Width = 100
      Height = 13
      Caption = '&Fecha Activaci'#243'n'
      FocusControl = deFechaActivacion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 30
      Top = 135
      Width = 63
      Height = 13
      Caption = 'Categor'#237'as'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 29
      Top = 13
      Width = 48
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_codigo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 30
      Top = 109
      Width = 43
      Height = 13
      Caption = '&Importe'
      FocusControl = neImporte
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 30
      Top = 61
      Width = 26
      Height = 13
      Caption = '&Tipo'
      FocusControl = cbTipos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblUltimaFechaActivacion: TLabel
      Left = 259
      Top = 86
      Width = 119
      Height = 13
      Caption = 'lblUltimaFechaActivacion'
      Visible = False
    end
    object txt_CodigoMaterial: TEdit
      Left = 149
      Top = 34
      Width = 100
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 4
      TabOrder = 1
    end
    object txt_codigo: TNumericEdit
      Left = 149
      Top = 9
      Width = 100
      Height = 21
      Color = clBtnFace
      Enabled = False
      MaxLength = 3
      TabOrder = 0
    end
    object deFechaActivacion: TDateEdit
      Left = 149
      Top = 81
      Width = 100
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 3
      Date = -693594.000000000000000000
    end
    object neImporte: TNumericEdit
      Left = 149
      Top = 105
      Width = 100
      Height = 21
      Color = 16444382
      TabOrder = 4
    end
    object cbTipos: TVariantComboBox
      Left = 149
      Top = 57
      Width = 101
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object dblucbCategorias: TDBLookupComboBox
      Left = 149
      Top = 132
      Width = 588
      Height = 21
      KeyField = 'Categoria'
      ListField = 'DescComentario'
      ListSource = dsObtenerCategorias
      TabOrder = 5
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 505
    Width = 761
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 564
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 31
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object alTarifasDayPass: TAbmList
    Left = 0
    Top = 33
    Width = 761
    Height = 309
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'81'#0'Codigo Material'
      #0'77'#0'Tipo de Pase  '
      #0'97'#0'Fecha Activaci'#243'n  '
      #0'115'#0'Importe Pase Diario     '
      #0'55'#0'Categor'#237'a')
    HScrollBar = True
    RefreshTime = 300
    Table = TarifasDayPass
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alTarifasDayPassClick
    OnDrawItem = alTarifasDayPassDrawItem
    OnRefresh = alTarifasDayPassRefresh
    OnInsert = alTarifasDayPassInsert
    OnDelete = alTarifasDayPassDelete
    OnEdit = alTarifasDayPassEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object spActualizarTarifaDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTarifaDayPass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarifaDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMaterial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@TipoDayPass'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@FechaActivacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 264
    Top = 72
  end
  object TarifasDayPass: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'TarifasDayPass'
    Left = 82
    Top = 72
  end
  object spEliminarCategoriasDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarCategoriasDayPass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarifaDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 264
    Top = 124
  end
  object spObtenerCategoriasTarifaDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCategoriasTarifaDayPass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarifaDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 120
  end
  object spAgregarCategoriaDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarCategoriaDayPass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarifaDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCategoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 264
    Top = 172
  end
  object spEliminarTarifaDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTarifaDayPass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarifaDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 384
    Top = 72
  end
  object dsObtenerCategorias: TDataSource
    DataSet = spObtenerCategorias
    Left = 608
    Top = 80
  end
  object spObtenerCategorias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    OnCalcFields = spObtenerCategoriasCalcFields
    ProcedureName = 'ObtenerCategorias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 664
    Top = 80
    object spObtenerCategoriasDescComentario: TStringField
      FieldKind = fkCalculated
      FieldName = 'DescComentario'
      Size = 100
      Calculated = True
    end
    object spObtenerCategoriasCategoria: TWordField
      FieldName = 'Categoria'
    end
    object spObtenerCategoriasDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 60
    end
    object spObtenerCategoriasComentarios: TWideMemoField
      FieldName = 'Comentarios'
      BlobType = ftWideMemo
    end
  end
  object spObtenerUltimaFechaActivacionTarifaDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerUltimaFechaActivacionTarifaDayPass'
    Parameters = <>
    Left = 384
    Top = 120
    object spObtenerUltimaFechaActivacionTarifaDayPassUltimaFechaActivacion: TDateTimeField
      FieldName = 'UltimaFechaActivacion'
      ReadOnly = True
    end
  end
end
