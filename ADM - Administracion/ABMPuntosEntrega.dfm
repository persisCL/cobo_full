object FormABMPuntosEntrega: TFormABMPuntosEntrega
  Left = 106
  Top = 125
  Caption = 'Mantenimiento de Puntos de Entrega'
  ClientHeight = 483
  ClientWidth = 769
  Color = clBtnFace
  Constraints.MinHeight = 512
  Constraints.MinWidth = 668
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 304
    Top = 160
    Width = 151
    Height = 13
    Caption = 'Por Favor, Aguarde un Instante '
  end
  object ATPuntoEntrega: TAbmToolbar
    Left = 0
    Top = 0
    Width = 769
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = ATPuntoEntregaClose
  end
  object pnl_BotonesGeneral: TPanel
    Left = 0
    Top = 444
    Width = 769
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 572
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 8
          Width = 79
          Height = 25
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 191
    Width = 769
    Height = 253
    ActivePage = tsPuntosVenta
    Align = alBottom
    TabOrder = 2
    object tsGeneral: TTabSheet
      Caption = 'Datos &Generales'
      object Label2: TLabel
        Left = 10
        Top = 41
        Width = 72
        Height = 13
        Caption = '&Descripci'#243'n:'
        FocusControl = txt_descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 10
        Top = 17
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label9: TLabel
        Left = 10
        Top = 180
        Width = 53
        Height = 13
        Caption = 'Supervisor:'
      end
      object Label6: TLabel
        Left = 10
        Top = 91
        Width = 95
        Height = 13
        Caption = 'Almac'#233'n proveedor:'
      end
      object Label14: TLabel
        Left = 265
        Top = 91
        Width = 94
        Height = 13
        Caption = 'Almac'#233'n recepci'#243'n:'
      end
      object Label16: TLabel
        Left = 10
        Top = 133
        Width = 89
        Height = 13
        Caption = 'Destino de ventas:'
      end
      object Label17: TLabel
        Left = 265
        Top = 133
        Width = 89
        Height = 13
        Caption = 'Destino comodato:'
      end
      object Label5: TLabel
        Left = 523
        Top = 91
        Width = 99
        Height = 13
        Caption = 'Almac'#233'n devoluci'#243'n:'
      end
      object LFuenteReclamo: TLabel
        Left = 519
        Top = 130
        Width = 98
        Height = 13
        Caption = 'Fuente del Reclamo:'
      end
      object txt_descripcion: TEdit
        Left = 115
        Top = 37
        Width = 318
        Height = 21
        Hint = 'Descripci'#243'n'
        CharCase = ecUpperCase
        Color = 16444382
        Enabled = False
        MaxLength = 60
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object txtCodigoPuntoEntrega: TNumericEdit
        Left = 115
        Top = 12
        Width = 105
        Height = 21
        TabStop = False
        Color = clBtnFace
        MaxLength = 3
        ReadOnly = True
        TabOrder = 0
      end
      object cbActivo: TCheckBox
        Left = 8
        Top = 205
        Width = 117
        Height = 15
        Alignment = taLeftJustify
        Caption = 'Activo'
        TabOrder = 2
      end
      object cbDefault: TCheckBox
        Left = 217
        Top = 205
        Width = 113
        Height = 15
        Alignment = taLeftJustify
        Caption = 'Default'
        TabOrder = 3
      end
      object bteSupervisor: TBuscaTabEdit
        Left = 80
        Top = 176
        Width = 417
        Height = 21
        Enabled = True
        MaxLength = 200
        TabOrder = 4
        OnKeyPress = bteSupervisorKeyPress
        EditorStyle = bteTextEdit
        BuscaTabla = BuscaTablaSupervisor
      end
      object bteAlmProveedor: TBuscaTabEdit
        Left = 8
        Top = 106
        Width = 233
        Height = 21
        Color = 16444382
        Enabled = True
        MaxLength = 50
        ReadOnly = True
        TabOrder = 5
        EditorStyle = bteTextEdit
        BuscaTabla = BuscaTablaAlmProveedor
      end
      object bteAlmRecepcion: TBuscaTabEdit
        Left = 264
        Top = 106
        Width = 233
        Height = 21
        Color = 16444382
        Enabled = True
        MaxLength = 50
        ReadOnly = True
        TabOrder = 6
        EditorStyle = bteTextEdit
        BuscaTabla = BuscaTablaAlmRecepcion
      end
      object bteAlmDevolucion: TBuscaTabEdit
        Left = 520
        Top = 106
        Width = 233
        Height = 21
        Color = 16444382
        Enabled = True
        MaxLength = 50
        ReadOnly = True
        TabOrder = 7
        EditorStyle = bteTextEdit
        BuscaTabla = BuscaTablaAlmDevolucion
      end
      object bteDestinoVentas: TBuscaTabEdit
        Left = 8
        Top = 146
        Width = 233
        Height = 21
        Color = 16444382
        Enabled = True
        MaxLength = 50
        ReadOnly = True
        TabOrder = 8
        EditorStyle = bteTextEdit
        BuscaTabla = BuscaTablaDestinoVentas
      end
      object bteDestinoComodato: TBuscaTabEdit
        Left = 264
        Top = 146
        Width = 233
        Height = 21
        Color = 16444382
        Enabled = True
        MaxLength = 50
        ReadOnly = True
        TabOrder = 9
        EditorStyle = bteTextEdit
        BuscaTabla = BuscaTablaDestinoComodato
      end
      object cbFuenteReclamo: TVariantComboBox
        Left = 518
        Top = 145
        Width = 234
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 10
        Items = <>
      end
    end
    object tsPuntosVenta: TTabSheet
      Caption = 'Puntos de &Venta'
      ImageIndex = 2
      DesignSize = (
        761
        225)
      object dblPuntosVenta: TDBListEx
        Left = 4
        Top = 9
        Width = 541
        Height = 205
        Anchors = [akLeft, akTop, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'C'#243'digo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescCodigo'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 300
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'POSNET'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NumeroPos'
          end>
        DataSource = dsPuntosVenta
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDblClick = dblPuntosVentaDblClick
      end
      object btnAgregarPuntoVenta: TButton
        Left = 556
        Top = 11
        Width = 91
        Height = 26
        Anchors = [akLeft]
        Caption = 'Ag&regar'
        TabOrder = 1
        OnClick = btnAgregarPuntoVentaClick
      end
      object btnEditarPuntoVenta: TButton
        Left = 556
        Top = 40
        Width = 91
        Height = 26
        Anchors = [akLeft]
        Caption = '&Editar'
        TabOrder = 2
        OnClick = btnEditarPuntoVentaClick
      end
      object btnBorrarPuntoVenta: TButton
        Left = 556
        Top = 73
        Width = 91
        Height = 26
        Anchors = [akLeft]
        Caption = '&Borrar'
        TabOrder = 3
        OnClick = btnBorrarPuntoVentaClick
      end
    end
    object tsParametrosStock: TTabSheet
      Caption = 'Parametros &Stock'
      ImageIndex = 3
      object Label13: TLabel
        Left = 607
        Top = 12
        Width = 122
        Height = 13
        Caption = 'Per'#237'odo reposici'#243'n (d'#237'as):'
      end
      object dbgParamStock: TDBGrid
        Left = 9
        Top = 9
        Width = 571
        Height = 211
        DataSource = dsParamStock
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = dbgParamStockKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Descripcion'
            ReadOnly = True
            Title.Caption = 'Categor'#237'a'
            Width = 244
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'StockDeseado'
            Title.Alignment = taRightJustify
            Title.Caption = 'Stock Deseado'
            Width = 100
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'StockMinimo'
            Title.Alignment = taRightJustify
            Title.Caption = 'Stock M'#237'nimo'
            Width = 100
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'ConsumoEstimado'
            Title.Alignment = taRightJustify
            Title.Caption = 'Consumo Estimado'
            Width = 100
            Visible = True
          end>
      end
      object txtPeriodoReposicion: TNumericEdit
        Left = 606
        Top = 26
        Width = 105
        Height = 21
        Hint = 'Per'#237'odo de reposici'#243'n'
        MaxLength = 5
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
  end
  object dblPuntosEntrega: TAbmList
    Left = 0
    Top = 33
    Width = 769
    Height = 158
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'98'#0'C'#243'digo                   '
      
        #0'382'#0'Descripci'#243'n                                                ' +
        '                                                          '
      #0'42'#0'Default')
    HScrollBar = True
    RefreshTime = 100
    Table = ObtenerPuntosEntrega
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblPuntosEntregaClick
    OnDrawItem = dblPuntosEntregaDrawItem
    OnRefresh = dblPuntosEntregaRefresh
    OnInsert = dblPuntosEntregaInsert
    OnDelete = dblPuntosEntregaDelete
    OnEdit = dblPuntosEntregaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = ATPuntoEntrega
  end
  object ObtenerPuntosEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerPuntosEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 226
    Top = 104
  end
  object ActualizarPuntoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarPuntosEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoAlmacenRecepcion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenProveedor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestinoVentas'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestinoComodato'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrdenMostrar'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PuntoEntregaDefault'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@PeriodoReposicion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuarioSupervisor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDevolucion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 487
    Top = 233
  end
  object EliminarPuntoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarPuntoEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 255
    Top = 104
  end
  object dsPuntosVenta: TDataSource
    DataSet = cdsPuntosVenta
    Left = 235
    Top = 234
  end
  object ObtenerPuntosVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerPuntosVenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 482
    Top = 265
  end
  object cdsPuntosVenta: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoPuntoEntrega'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPuntoVenta'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescCodigo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroPOS'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 267
    Top = 234
  end
  object ActualizarPuntoVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarPuntosVenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPOS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 521
    Top = 232
  end
  object AlmacenesValidos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 255
    Top = 74
  end
  object qryGenerarAlmacenes: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'DescripcionPE'
        Size = -1
        Value = Null
      end
      item
        Name = 'CodigoAlmacen'
        Direction = pdOutput
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @CodigoBodega int'
      'DECLARE @CodigoAlmacen int'
      'DECLARE @Descripcion char(50)'
      ''
      'SELECT @CodigoBodega = 1'
      ''
      'SELECT @Descripcion = '#39'DEP.'#39' + :DescripcionPE '
      ''
      
        'Exec ActualizarMaestroAlmacenes  @Descripcion, @CodigoBodega, 1,' +
        ' NULL, NULL, @CodigoAlmacen OUTPUT'
      ''
      'INSERT INTO almacenesorigenoperacion values(3,@CodigoAlmacen)'
      'INSERT INTO almacenesorigenoperacion values(4,@CodigoAlmacen)'
      'INSERT INTO almacenesorigenoperacion values(5,@CodigoAlmacen)'
      'INSERT INTO almacenesorigenoperacion values(6,@CodigoAlmacen)'
      'INSERT INTO almacenesdestinooperacion values(9,@CodigoAlmacen)'
      ''
      'SET :CodigoAlmacen =  @CodigoAlmacen '
      '')
    Left = 226
    Top = 74
  end
  object dsParamStock: TDataSource
    DataSet = cdsParamStock
    Left = 381
    Top = 444
  end
  object spObtenerParametrosStockPuntoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerParametrosStockPuntoEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 447
    Top = 444
    object spObtenerParametrosStockPuntoEntregaCodigoPuntoEntrega: TIntegerField
      FieldName = 'CodigoPuntoEntrega'
    end
    object spObtenerParametrosStockPuntoEntregaCategoria: TWordField
      FieldName = 'Categoria'
    end
    object spObtenerParametrosStockPuntoEntregaStockDeseado: TIntegerField
      FieldName = 'StockDeseado'
    end
    object spObtenerParametrosStockPuntoEntregaStockMinimo: TIntegerField
      FieldName = 'StockMinimo'
    end
    object spObtenerParametrosStockPuntoEntregaConsumoEstimado: TIntegerField
      FieldName = 'ConsumoEstimado'
    end
    object spObtenerParametrosStockPuntoEntregaDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 60
    end
  end
  object spActualizarParametrosStockPuntoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarParametrosStockPuntoEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@StockDeseado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@StockMinimo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConsumoEstimado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 486
    Top = 444
  end
  object cdsParamStock: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoPuntoEntrega'
        DataType = ftInteger
      end
      item
        Name = 'Categoria'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'StockDeseado'
        DataType = ftInteger
      end
      item
        Name = 'StockMinimo'
        DataType = ftInteger
      end
      item
        Name = 'ConsumoEstimado'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 414
    Top = 444
  end
  object BuscaTablaSupervisor: TBuscaTabla
    Caption = 'Seleccionar Usuario Supervisor'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier'
    Font.Style = []
    HelpContext = 0
    Dataset = qrySupervisor
    OnSelect = BuscaTablaSupervisorSelect
    Left = 504
    Top = 400
  end
  object qrySupervisor: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT CodigoUsuario ,dbo.ArmarNombrePersona('#39#39', Apellido, Apell' +
        'idoMaterno, Nombre) as Nombre'
      'FROM vw_UsuariosSistemas')
    Left = 536
    Top = 400
    object qrySupervisorCodigoUsuario: TStringField
      FieldName = 'CodigoUsuario'
      FixedChar = True
    end
    object qrySupervisorCOLUMN1: TStringField
      FieldName = 'Nombre'
      ReadOnly = True
      Size = 200
    end
  end
  object qryAlmProveedor: TADOQuery
    Parameters = <>
    Left = 160
    Top = 304
  end
  object BuscaTablaAlmProveedor: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryAlmProveedor
    OnSelect = BuscaTablaAlmProveedorSelect
    Left = 128
    Top = 304
  end
  object BuscaTablaAlmRecepcion: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryAlmRecepcion
    OnSelect = BuscaTablaAlmRecepcionSelect
    Left = 368
    Top = 304
  end
  object qryAlmRecepcion: TADOQuery
    Parameters = <>
    Left = 400
    Top = 304
  end
  object BuscaTablaAlmDevolucion: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryAlmDevolucion
    OnSelect = BuscaTablaAlmDevolucionSelect
    Left = 632
    Top = 304
  end
  object qryAlmDevolucion: TADOQuery
    Parameters = <>
    Left = 664
    Top = 304
  end
  object BuscaTablaDestinoVentas: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryDestinoVentas
    OnSelect = BuscaTablaDestinoVentasSelect
    Left = 128
    Top = 360
  end
  object qryDestinoVentas: TADOQuery
    Parameters = <>
    Left = 160
    Top = 360
  end
  object BuscaTablaDestinoComodato: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = qryDestinoComodato
    OnSelect = BuscaTablaDestinoComodatoSelect
    Left = 376
    Top = 360
  end
  object qryDestinoComodato: TADOQuery
    Parameters = <>
    Left = 408
    Top = 360
  end
  object spBorrarPuntoVenta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BorrarPuntoVenta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 315
    Top = 453
  end
end
