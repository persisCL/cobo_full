object FormTarjetasCreditoTipo: TFormTarjetasCreditoTipo
  Left = 140
  Top = 110
  Caption = 'Mantenimiento de Tipos de Tarjetas de Cr'#233'dito'
  ClientHeight = 537
  ClientWidth = 761
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 761
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object alTarjetaCreditoTipo: TAbmList
    Left = 0
    Top = 33
    Width = 761
    Height = 213
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'109'#0'Tarjeta de cr'#233'dito      '
      #0'109'#0'Descripci'#243'n               '
      #0'86'#0'Caracter inicial   '
      #0'88'#0'Longitud banda  '
      #0'74'#0'Valida prefijo  '
      #0'44'#0'Activo  '
      #0'112'#0'C'#243'digo Familia Tarjeta')
    HScrollBar = True
    RefreshTime = 300
    Table = TarjetasCreditoTipos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = alTarjetaCreditoTipoClick
    OnDrawItem = alTarjetaCreditoTipoDrawItem
    OnRefresh = alTarjetaCreditoTipoRefresh
    OnInsert = alTarjetaCreditoTipoInsert
    OnDelete = alTarjetaCreditoTipoDelete
    OnEdit = alTarjetaCreditoTipoEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object gbTarjetaCreditoTipo: TPanel
    Left = 0
    Top = 246
    Width = 761
    Height = 252
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label2: TLabel
      Left = 29
      Top = 38
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 29
      Top = 63
      Width = 118
      Height = 13
      Caption = '&Longitud de &Prefijos:'
      FocusControl = txt_caracterinicialbanda
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 232
      Top = 63
      Width = 112
      Height = 13
      Caption = '&Longitud de Banda:'
      FocusControl = txt_longitudbanda
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 29
      Top = 90
      Width = 47
      Height = 13
      Caption = 'Prefi&jos:'
      FocusControl = lb_prefijos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 29
      Top = 13
      Width = 48
      Height = 13
      Caption = '&C'#243'digo: '
      FocusControl = txt_codigo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 30
      Top = 184
      Width = 112
      Height = 13
      Caption = '&Familia de Tarjetas:'
      FocusControl = cbFamiliaTarjetas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 149
      Top = 34
      Width = 318
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 30
      TabOrder = 1
    end
    object txt_caracterinicialbanda: TNumericEdit
      Left = 149
      Top = 59
      Width = 27
      Height = 21
      Color = 16444382
      MaxLength = 2
      TabOrder = 2
      OnKeyPress = txt_caracterinicialbandaKeyPress
    end
    object txt_longitudbanda: TNumericEdit
      Left = 350
      Top = 59
      Width = 27
      Height = 21
      Color = 16444382
      MaxLength = 2
      TabOrder = 3
      OnKeyPress = txt_caracterinicialbandaKeyPress
    end
    object lb_prefijos: TListBox
      Left = 149
      Top = 84
      Width = 230
      Height = 85
      ItemHeight = 13
      TabOrder = 4
    end
    object chkActivo: TCheckBox
      Left = 26
      Top = 226
      Width = 131
      Height = 17
      Alignment = taLeftJustify
      Caption = '&Activo:'
      TabOrder = 8
    end
    object txt_codigo: TNumericEdit
      Left = 149
      Top = 9
      Width = 66
      Height = 21
      Color = clBtnFace
      Enabled = False
      MaxLength = 3
      TabOrder = 0
    end
    object cbValidarPrefijo: TCheckBox
      Left = 26
      Top = 203
      Width = 131
      Height = 17
      Alignment = taLeftJustify
      Caption = '&Validar Prefijo'
      TabOrder = 7
    end
    object btnAgregarPrefijo: TButton
      Left = 386
      Top = 88
      Width = 80
      Height = 26
      Caption = '<< A&gregar'
      Enabled = False
      TabOrder = 5
      OnClick = btnAgregarPrefijoClick
    end
    object btnQuitarPrefijo: TButton
      Left = 386
      Top = 116
      Width = 80
      Height = 26
      Caption = '>> &Quitar'
      Enabled = False
      TabOrder = 6
      OnClick = btnQuitarPrefijoClick
    end
    object cbFamiliaTarjetas: TVariantComboBox
      Left = 149
      Top = 176
      Width = 230
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 9
      Items = <>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 498
    Width = 761
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Label3: TLabel
      Left = 35
      Top = 13
      Width = 63
      Height = 13
      Caption = 'Desactivado.'
    end
    object Notebook: TNotebook
      Left = 564
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 31
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
    object Panel1: TPanel
      Left = 16
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
  end
  object ActualizarDatosTarjetaCreditoTipos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosTarjetaCreditoTipos;1'
    Parameters = <>
    Left = 360
    Top = 80
  end
  object TarjetasCreditoTipos: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'TarjetasCreditoTipos'
    Left = 154
    Top = 104
  end
  object EliminarTarjetasCreditoTipos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTarjetasCreditoTipos'
    Parameters = <>
    Left = 480
    Top = 100
  end
  object qry_prefijos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoTipoTarjetaCredito'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM'
      '  TarjetasCreditoPrefijos  WITH (NOLOCK) '
      'WHERE'
      '  CodigoTipoTarjetaCredito = :CodigoTipoTarjetaCredito')
    Left = 428
    Top = 180
  end
  object QryFamiliasTarjetas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM FamiliasTarjetasCredito (NOLOCK)')
    Left = 560
    Top = 160
  end
end
