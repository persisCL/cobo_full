object FDescripcionesPuntosCobro: TFDescripcionesPuntosCobro
  Left = 155
  Top = 109
  Caption = 'Mantenimiento de Descripciones de Puntos de Cobro'
  ClientHeight = 598
  ClientWidth = 738
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 738
    Height = 33
    Habilitados = [btAlta, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object lb_PuntosCobro: TAbmList
    Left = 0
    Top = 33
    Width = 738
    Height = 274
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'103'#0'N'#186' Punto de Cobro  '
      #0'64'#0'Domain ID  '
      #0'58'#0'TSMC ID  '
      
        #0'277'#0'Descripci'#243'n                                                ' +
        '                       '
      #0'85'#0'C'#243'digo Eje Vial  '
      #0'50'#0'Sentido  '
      #0'99'#0'Progresiva Metros  '
      #0'42'#0'TS ID  '
      #0'128'#0'C'#243'digo PC Concesionaria'
      #0'46'#0'Kms      '
      #0'141'#0'Concesionaria                      '
      #0'137'#0'Punto Cobro Concesionaria')
    HScrollBar = True
    RefreshTime = 100
    Table = tbl_PuntosCobro
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lb_PuntosCobroClick
    OnDrawItem = lb_PuntosCobroDrawItem
    OnRefresh = lb_PuntosCobroRefresh
    OnInsert = lb_PuntosCobroInsert
    OnEdit = lb_PuntosCobroEdit
    Access = [accAlta, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 307
    Width = 738
    Height = 252
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    ExplicitLeft = -32
    object lbl_Descripcion: TLabel
      Left = 54
      Top = 45
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 102
      Height = 13
      Caption = 'Numero Punto Cobro:'
    end
    object Label2: TLabel
      Left = 205
      Top = 16
      Width = 53
      Height = 13
      Caption = 'Domain ID:'
    end
    object Label3: TLabel
      Left = 354
      Top = 16
      Width = 44
      Height = 13
      Caption = 'TSMCID:'
    end
    object Label4: TLabel
      Left = 56
      Top = 224
      Width = 70
      Height = 13
      Caption = 'Concesionaria:'
    end
    object Label5: TLabel
      Left = 87
      Top = 105
      Width = 39
      Height = 13
      Caption = 'Sentido:'
    end
    object Label6: TLabel
      Left = 354
      Top = 224
      Width = 132
      Height = 13
      Caption = 'Punto Cobro Concesionaria:'
    end
    object Label7: TLabel
      Left = 374
      Top = 164
      Width = 23
      Height = 13
      Caption = 'Kms:'
    end
    object Label8: TLabel
      Left = 52
      Top = 75
      Width = 74
      Height = 13
      Caption = 'Codigo Eje Vial:'
    end
    object Label9: TLabel
      Left = 56
      Top = 164
      Width = 70
      Height = 13
      Caption = 'Prefijo Imagen:'
    end
    object Label10: TLabel
      Left = 297
      Top = 75
      Width = 101
      Height = 13
      Caption = 'Punto Cobro Anterior:'
    end
    object Label11: TLabel
      Left = 316
      Top = 105
      Width = 82
      Height = 13
      Caption = 'Cantidad Carriles:'
    end
    object Label13: TLabel
      Left = 83
      Top = 194
      Width = 43
      Height = 13
      Caption = 'Juzgado:'
    end
    object Label14: TLabel
      Left = 274
      Top = 134
      Width = 123
      Height = 13
      Caption = 'Codigo PC Concesionaria:'
    end
    object Label15: TLabel
      Left = 498
      Top = 16
      Width = 28
      Height = 13
      Caption = 'TSID:'
    end
    object Label16: TLabel
      Left = 38
      Top = 134
      Width = 88
      Height = 13
      Caption = 'Progresiva Metros:'
    end
    object lblEsItaliano: TLabel
      Left = 477
      Top = 45
      Width = 49
      Height = 13
      Caption = 'Es Italiano'
    end
    object edtDescripcion: TEdit
      Left = 132
      Top = 41
      Width = 336
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 4
    end
    object neNumeroPuntoCobro: TNumericEdit
      Left = 132
      Top = 12
      Width = 53
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 0
    end
    object neDomain: TNumericEdit
      Left = 264
      Top = 12
      Width = 59
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 1
    end
    object neTSMCID: TNumericEdit
      Left = 404
      Top = 12
      Width = 64
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 2
    end
    object neEjeVial: TNumericEdit
      Left = 132
      Top = 71
      Width = 53
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 5
    end
    object edtSentido: TEdit
      Left = 132
      Top = 101
      Width = 27
      Height = 21
      Color = 16444382
      MaxLength = 1
      TabOrder = 7
      Text = 'edtSentido'
    end
    object neProgresivaMetros: TNumericEdit
      Left = 132
      Top = 130
      Width = 73
      Height = 21
      Color = 16444382
      TabOrder = 9
      Decimals = 1
    end
    object neTSID: TNumericEdit
      Left = 532
      Top = 12
      Width = 49
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 3
    end
    object edtPrefijoImagen: TEdit
      Left = 132
      Top = 163
      Width = 121
      Height = 21
      TabOrder = 11
      Text = 'edtPrefijoImagen'
    end
    object nePuntoCobroAnterior: TNumericEdit
      Left = 403
      Top = 71
      Width = 65
      Height = 21
      MaxLength = 3
      TabOrder = 6
    end
    object neCarriles: TNumericEdit
      Left = 403
      Top = 101
      Width = 65
      Height = 21
      MaxLength = 3
      TabOrder = 8
    end
    object vcbJuzgado: TVariantComboBox
      Left = 132
      Top = 190
      Width = 196
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 14
      Items = <>
    end
    object nePCConcesionaria: TNumericEdit
      Left = 402
      Top = 130
      Width = 66
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 10
    end
    object neKms: TNumericEdit
      Left = 402
      Top = 160
      Width = 66
      Height = 21
      Color = 16444382
      TabOrder = 12
      Decimals = 2
    end
    object chkSentido: TCheckBox
      Left = 384
      Top = 192
      Width = 142
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Sentido Ultimo P'#243'rtico:'
      TabOrder = 13
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 132
      Top = 220
      Width = 194
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 15
      Items = <>
    end
    object neNroPtoCobroCC: TNumericEdit
      Left = 496
      Top = 220
      Width = 121
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 16
    end
    object cbEsItaliano: TVariantComboBox
      Left = 532
      Top = 41
      Width = 49
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 17
      Items = <
        item
          Caption = 'Si'
          Value = True
        end
        item
          Caption = 'No'
          Value = False
        end>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 559
    Width = 738
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 541
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object tbl_PuntosCobro: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'PuntosCobro'
    Left = 40
    Top = 60
  end
  object spActualizarPuntoCobro: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarPuntoCobro;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPuntoCobro'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@DomainID'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TSMCID'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoEjeVial'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Sentido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@ProgresivaMetros'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 1
        Precision = 8
        Value = Null
      end
      item
        Name = '@TSID'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PrefijoImagen'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@PuntoCobroAnterior'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CantidadCarriles'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@UltimoPorticoSentido'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoJuzgado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPCConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Kms'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 8
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroPuntoCobroConcesionaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@EsItaliano'
        DataType = ftBoolean
        Value = False
      end>
    Left = 40
    Top = 96
  end
  object ds_PuntosCobro: TDataSource
    DataSet = tbl_PuntosCobro
    Left = 8
    Top = 60
  end
  object spObtenerConcesionarias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConcesionarias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 88
    Top = 96
  end
  object spObtenerJuzgados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerJuzgados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 120
    Top = 96
  end
end
