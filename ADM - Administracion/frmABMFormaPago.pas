{********************************** Unit Header ********************************
File Name : frmABMFormaPago.pas
Author : pdominguez
Date Created: 18/06/2009
Language : ES-AR
Description : ABM Formas de Pago
        - Se crea el ABM por el requerimiento de la SS 809.
        - Se usa como base el ABM de Bodegas.

Revision : 1
    Author : pdominguez
    Date   : 13/08/2009
    Description : SS 809
        - Se modificaron/a�adieron los siguientes objetos:
            ListaFormasPago: TABMList
                se a�adieron 2 columnas nuevas para visualizar los nuevos campos
            edtCodigoEntradaUsuario: TEdit,
            cbCobroAnticipado: TCheckBox,
            spActualizaFormasPago: TADOStoredProc
                se a�adieron los par�metros @CodigoEntradaUsuario y @AceptaCobroAnticipado
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            LimpiarCampos,
            ListaFormasPagoDrawItem,
            ListaFormasPagoClick,
            BtnAceptarClick

Revision : 2
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se modificaron/a�adieron los siguientes objetos:
            ListaFormasPago: TABMList
                se a�adieron 3 columnas nuevas para visualizar los nuevos campos
            vcbPantallas: TVariantComboBox,
            nedtCodigoBanco: TEdit,
            nedtCodigoTarjeta: TEdit
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializa
            LimpiarCampos
            ListaFormasPagoDrawItem
            ListaFormasPagoClick
            BtnAceptarClick
*******************************************************************************}
unit frmABMFormaPago;

interface

uses
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants, VariantComboBox;

type
  TABMFormaPagoForm = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    tblFormasPago: TADOTable;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    spActualizaFormasPago: TADOStoredProc;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    ListaFormasPago: TAbmList;
    dsFormasPago: TDataSource;
    txtDescripcion: TEdit;
    spEliminarFormaPago: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    cbCobroAnticipado: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    edtCodigoEntradaUsuario: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    nedtCodigoBanco: TNumericEdit;
    nedtCodigoTarjeta: TNumericEdit;
    Label5: TLabel;
    vcbPantallas: TVariantComboBox;
    procedure FormShow(Sender: TObject);
   	procedure ListaFormasPagoRefresh(Sender: TObject);
   	function  ListaFormasPagoProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaFormasPagoDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaFormasPagoClick(Sender: TObject);
	procedure ListaFormasPagoInsert(Sender: TObject);
	procedure ListaFormasPagoEdit(Sender: TObject);
	procedure ListaFormasPagoDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  ABMFormaPagoForm: TABMFormaPagoForm;

resourcestring
	STR_MAESTRO_FORMAS	= 'Maestro de Formas de Pago';


implementation

{$R *.DFM}

{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: Boolean

  Revision : 1
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se a�ade la carga del Combo de Pantallas.
-----------------------------------------------------------------------------}
function TABMFormaPagoForm.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
    // Rev. 1 (SS 809)
    with vcbPantallas.Items do begin
        Add('Sin Pantalla Asociada',-1);
        Add(CONST_PAGE_DESC_EFECTIVO,CONST_PAGE_EFECTIVO);
        Add(CONST_PAGE_DESC_CHEQUE, CONST_PAGE_CHEQUE);
        Add(CONST_PAGE_DESC_VALE_VISTA, CONST_PAGE_VALE_VISTA);
        Add(CONST_PAGE_DESC_DEBITO_CUENTA, CONST_PAGE_DEBITO_CUENTA_PAC);
        Add(CONST_PAGE_DESC_PAGARE, CONST_PAGE_PAGARE);
        Add(CONST_PAGE_DESC_TARJETA_CREDITO, CONST_PAGE_TARJETA_CREDITO);
        Add(CONST_PAGE_DESC_TARJETA_DEBITO, CONST_PAGE_TARJETA_DEBITO);
    end;
    // Fin Rev. 1 (SS 809)
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblFormasPago]) then exit;
	Notebook.PageIndex := 0;
	ListaFormasPago.Reload;
    // Rev. 1 (SS 809)
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarCampos
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: None

  Revision : 1
      Author : pdominguez
      Date   : 13/08/2009
      Description : SS 809
        - Se a�aden los objetos nuevos edtCodigoEntradaUsuario y
        cbCobroAnticipado.

  Revision : 2
      Author : pdominguez
      Date   : 17/09/2009
      Description : SS 809
        - Se a�aden los objetos nuevos nedtCodigoBanco y nedtCodigoTarjeta.
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
    // Rev. 1 (SS 809)
    edtCodigoEntradaUsuario.Clear;
    cbCobroAnticipado.Checked := False;
    // Fin Rev. 1 (SS 809)
    // Rev. 2 (SS 809)
    nedtCodigoBanco.Clear;
    nedtCodigoTarjeta.Clear;
    // Fin Rev. 2 (SS 809)
end;


{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author:
  Date Created:
  Description: Actualizo la grilla
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.FormShow(Sender: TObject);
begin
	ListaFormasPago.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaFormasPagoRefresh
  Author:
  Date Created:
  Description: lipio los campos si la tabla esta vacia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.ListaFormasPagoRefresh(Sender: TObject);
begin
	 if ListaFormasPago.Empty then LimpiarCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: Volver_Campos
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.Volver_Campos;
begin
	ListaFormasPago.Estado := Normal;
	ListaFormasPago.Enabled:= True;

	ActiveControl       := ListaFormasPago;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaFormasPago.Reload;
end;


{-----------------------------------------------------------------------------
  Function Name: ListaFormasPagoDrawItem
  Author:
  Date Created:
  Description: Muestro los Registros
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None

  Revision : 1
    Author : pdominguez
    Date   : 13/08/2009
    Description : SS 809
        - Se a�adi� la visualizaci�n de los campos nuevos.

  Revision : 2
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se a�adi� la visualizaci�n de los campos nuevos.
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.ListaFormasPagoDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoFormaPago').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
        // Rev. 1 (SS 809)
		TextOut(Cols[2], Rect.Top, Trim(Tabla.FieldbyName('CodigoEntradaUsuario').AsString));
        if Tabla.FieldbyName('AceptaCobroAnticipado').AsBoolean then
    		TextOut(Cols[3], Rect.Top, 'S�')
        else TextOut(Cols[3], Rect.Top, 'No');
        // Fin Rev. 1 (SS 809)
        // Rev. 2 (SS 809)
        if vcbPantallas.Items.IndexOfValue(FieldByname('CodigoPantalla').AsInteger) = -1 then begin
            Font.Color := clRed;
            Font.Style := Font.Style + [fsBold];
            TextOut(Cols[4], Rect.Top, vcbPantallas.Items[0].Caption);
            Font.Color := clBlack;
            Font.Style := Font.Style - [fsBold];
        end
        else TextOut(Cols[4], Rect.Top,vcbPantallas.Items[vcbPantallas.Items.IndexOfValue(FieldByname('CodigoPantalla').AsInteger)].Caption);
		TextOut(Cols[5], Rect.Top, Trim(Tabla.FieldbyName('CodigoBanco').AsString));
		TextOut(Cols[6], Rect.Top, Trim(Tabla.FieldbyName('CodigoTarjeta').AsString));
        // Fin Rev. 2 (SS 809)
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaFormasPagoClick
  Author:
  Date Created:
  Description: Selecciono un campo
  Parameters: Sender: TObject
  Return Value: None

  Revision : 1
    Author : pdominguez
    Date   : 13/08/2009
    Description : SS 809
        - Se a�adi� la asignaci�n de los valores de los campos nuevos.

  Revision : 2
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se a�adi� la asignaci�n de los valores de los campos nuevos.
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.ListaFormasPagoClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		:= FieldByName('CodigoFormaPago').AsInteger;
	   txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
        // Rev. 1 (SS 809)
       edtCodigoEntradaUsuario.Text := FieldByname('CodigoEntradaUsuario').AsString;
       cbCobroAnticipado.Checked := FieldbyName('AceptaCobroAnticipado').AsBoolean;
       // Fin Rev. 1 (SS 809)
        // Rev. 2 (SS 809)
        if vcbPantallas.Items.IndexOfValue(FieldByname('CodigoPantalla').AsInteger) = -1 then
            vcbPantallas.ItemIndex := 0
       else
            vcbPantallas.ItemIndex := vcbPantallas.Items.IndexOfValue(FieldByname('CodigoPantalla').AsInteger);
       nedtCodigoBanco.Value := FieldByname('CodigoBanco').AsInteger;
       nedtCodigoTarjeta.Value := FieldByname('CodigoTarjeta').AsInteger;
       // Fin Rev. 2 (SS 809)
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaFormasPagoProcess
  Author:
  Date Created:
  Description:
  Parameters: Tabla: TDataSet;var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TABMFormaPagoForm.ListaFormasPagoProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaFormasPagoInsert
  Author:
  Date Created:
  Description: limpio los campos para que ingrese un nuevo registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.ListaFormasPagoInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          := True;
	ListaFormasPago.Enabled := False;
	Notebook.PageIndex      := 1;
	ActiveControl           := txtDescripcion;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaFormasPagoEdit
  Author:
  Date Created:
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.ListaFormasPagoEdit(Sender: TObject);
begin
	ListaFormasPago.Enabled:= False;
	ListaFormasPago.Estado := modi;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion
end;

{-----------------------------------------------------------------------------
  Function Name: ListaFormasPagoDelete
  Author:
  Date Created:
  Description: elimino un item
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.ListaFormasPagoDelete(Sender: TObject);
resourcestring
  MSG_EXISTEN_CANALES = 'Existen Canales de Pago asociados a esta Forma.';
  MSG_DELETE_CAPTION  = 'Eliminar Forma de Pago.';
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_FORMAS]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with spEliminarFormaPago do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoFormaPago').Value := tblFormasPago.FieldbyName('CodigoFormaPago').Value;
                Parameters.ParamByName('@Resultado').Value := 0;
                ExecProc;
                if Parameters.ParamByName('@Resultado').Value = 1 then
                    MsgBox(MSG_EXISTEN_CANALES, MSG_DELETE_CAPTION, MB_ICONSTOP);
                close;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_FORMAS]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_FORMAS]), MB_ICONSTOP);
			end
		end
	end;

	ListaFormasPago.Reload;
	ListaFormasPago.Estado := Normal;
	ListaFormasPago.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: Acepto los cambios
  Parameters: Sender: TObject
  Return Value: None

  Revision : 1
    Author : pdominguez
    Date   : 13/08/2009
    Description : SS 809
        - Se a�ade la asignaci�n del valor a los par�metros nuevos
        @CodigoEntradaUsuario y @AceptaCobroAnticipado del SP spActualizaFormasPago.

  Revision : 2
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se a�ade la asignaci�n del valor a los par�metros nuevos
        @CodigoPantalla, @CodigoBanco y @CodigoTarjeta del SP spActualizaFormasPago.
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.BtnAceptarClick(Sender: TObject);

begin
	if not ValidateControls([txtDescripcion],
	  [(Trim(txtDescripcion.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_FORMAS]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
		Exit;
	end;

	with ListaFormasPago do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizaFormasPago, Parameters do begin
                    Parameters.Refresh;
					ParamByName('@CodigoFormaPago').Value := iif(txtCodigo.ValueInt <> 0,txtCodigo.ValueInt,Null);
					ParamByName('@Descripcion').Value := Trim(txtDescripcion.Text);
                    // Rev. 1 (SS 809)
                    ParamByName('@CodigoEntradaUsuario').Value := Trim(edtCodigoEntradaUsuario.Text);
                    ParamByName('@AceptaCobroAnticipado').Value := cbCobroAnticipado.Checked;
                    // Fin Rev. 1 (SS 809)
                    // Rev. 2 (SS 809)
					ParamByName('@CodigoPantalla').Value := iif(vcbPantallas.Value <> -1,vcbPantallas.Value,Null);
					ParamByName('@CodigoBanco').Value := iif(nedtCodigoBanco.ValueInt <> 0,nedtCodigoBanco.ValueInt,Null);
					ParamByName('@CodigoTarjeta').Value := iif(nedtCodigoTarjeta.ValueInt <> 0,nedtCodigoTarjeta.ValueInt,Null);
                    // Fin Rev. 2 (SS 809)
                    //INICIO:	20160714 CFU
                    try
						ExecProc;
                    except
                    	on E:Exception do begin
                            MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_FORMAS]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_FORMAS]), MB_ICONSTOP);
                    	end;
                    end;
                    //TERMINO:	20160714 CFU
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_FORMAS]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_FORMAS]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:
  Date Created:
  Description: permito cancelar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:
  Date Created:
  Description: salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.AbmToolbarClose(Sender: TObject);
begin
	 close;
end;


{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:
  Date Created:
  Description:  salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:
  Date Created:
  Description: lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TABMFormaPagoForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;




end.

