{--------------------------------------------------------------------------

Author      :
Date        :
Decription  :

Firma       :   SS_1147_MBE_20140819
Description :   Se modifica el funcionamiento del ABM.

---------------------------------------------------------------------------}
unit ABMColumnasLibrosContables;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls,RStrings, Validate, DateEdit;

type
  TFormABMColumnasLibrosContables = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    LibrosContablesColumnas: TADOTable;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    Txt_Descripcion: TEdit;
    LibrosContablesColumnasIdLibroContable: TIntegerField;
    LibrosContablesColumnasIdColumnaLibroContable: TIntegerField;
    LibrosContablesColumnasDescripcion: TStringField;
    LibrosContablesColumnasUsuarioModificacion: TStringField;
    LibrosContablesColumnasFechaModificacion: TDateTimeField;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

  
resourcestring
    FLD_LIBROS_CONTABLES = 'Columna Libro Contable';


var
  FormABMColumnasLibrosContables: TFormABMColumnasLibrosContables;

implementation

{$R *.DFM}

procedure TFormABMColumnasLibrosContables.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
end;

function TFormABMColumnasLibrosContables.Inicializa: boolean;
begin
    CenterForm(Self);
	if not OpenTables([LibrosContablesColumnas]) then
		Result := False
	else begin
    	Notebook.PageIndex := 0;
		Result := True;
        VolverCampos;
		DbList1.Reload;
	end;
end;

procedure TFormABMColumnasLibrosContables.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormABMColumnasLibrosContables.DBList1Click(Sender: TObject);
begin
	with LibrosContablesColumnas do begin
        Txt_descripcion.Text := FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormABMColumnasLibrosContables.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('IdLibroContable').AsString);
      	TextOut(Cols[1], Rect.Top, Tabla.FieldByName('IdColumnaLibroContable').AsString);
        TextOut(Cols[2], Rect.Top, Tabla.FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormABMColumnasLibrosContables.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormABMColumnasLibrosContables.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormABMColumnasLibrosContables.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormABMColumnasLibrosContables.Limpiar_Campos;
begin
	Txt_descripcion.Clear;
end;

procedure TFormABMColumnasLibrosContables.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormABMColumnasLibrosContables.BtnAceptarClick(Sender: TObject);
    function CombinacionCargada(Combinacion:String;Posicion:Integer=-1):Boolean;
    var
        pos:TBookmark;
    begin
        result:=False;
        with LibrosContablesColumnas do begin
            pos:=GetBookmark;
            First;
            while not(eof) and (result=False) do begin
                if (Posicion<>RecNo) and (uppercase(FieldByName('Descripcion').AsString)=uppercase(Combinacion)) then
                        Result:=True;
                next;
            end;
            GotoBookmark(pos);
        end;
    end;

begin
    Txt_Descripcion.Text:=Txt_Descripcion.Text;
    if not ValidateControls([Txt_Descripcion],
                [trim(Txt_Descripcion.Text) <> ''],
                format(MSG_CAPTION_GESTION,[FLD_LIBROS_CONTABLES]),
                [format(MSG_VALIDAR_DEBE_LA,[FLD_LIBROS_CONTABLES]),
                format(MSG_VALIDAR_DEBE_EL,[FLD_DIGITO]),
                format(MSG_VALIDAR_DEBE_EL,[FLD_DIGITO])]) then exit;

    if CombinacionCargada(trim(Txt_Descripcion.Text),iif(DbList1.Estado = Alta,-1,LibrosContablesColumnas.RecNo)) then begin
        MsgBoxBalloon(format(MSG_ERROR_DUPLICADO_EL,[FLD_LIBROS_CONTABLES]),format(MSG_CAPTION_GESTION,[FLD_LIBROS_CONTABLES]),MB_ICONSTOP,Txt_Descripcion);
        Exit;
    end;

 	Screen.Cursor := crHourGlass;
	With LibrosContablesColumnas do begin
		Try
			if DbList1.Estado = Alta then begin                                                  //SS_1147_MBE_20140819
                Append;
                FieldByName('UsuarioCreacion').Value:= UsuarioSistema;                           //SS_1147_MBE_20140819
                FieldByName('FechaCreacion').Value:= NowBase(DMConnections.BaseCAC);             //SS_1147_MBE_20140819
            end                                                                                  //SS_1147_MBE_20140819
			else Edit;

            FieldByName('Descripcion').Value:= txt_Descripcion.text;
            FieldByName('UsuarioModificacion').Value:= UsuarioSistema;
            FieldByName('FechaModificacion').Value:= NowBase(DMConnections.BaseCAC);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[FLD_LIBROS_CONTABLES]), E.message, format(MSG_CAPTION_GESTION,[FLD_LIBROS_CONTABLES]), MB_ICONSTOP);
                Screen.Cursor	:= crDefault;
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormABMColumnasLibrosContables.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormABMColumnasLibrosContables.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormABMColumnasLibrosContables.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormABMColumnasLibrosContables.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    Txt_Descripcion.SetFocus;
end;

end.
