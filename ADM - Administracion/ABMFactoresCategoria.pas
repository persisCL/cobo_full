unit ABMFactoresCategoria;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcsCN, validate,
  Dateedit, Util, ADODB,DPSControls, Provider, DBClient, ListBoxEx, DBListEx,
  VariantComboBox, RStrings;

type
  TFormFactoresCategoria = class(TForm)
    pnlEdicion: TPanel;
    lblDescripcion: TLabel;
    lblIDCategoriaUnico: TLabel;
    pnlBotones: TPanel;
    txtDescripcion: TEdit;
    lblShow_Inserta_Modifica: TLabel;
    nbBotones: TNotebook;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    lblIDCategoria: TLabel;
    lblCategoriaPortico: TLabel;
    lblFactorMultiplicacion: TLabel;
    txtIDCategoriaUnico: TEdit;
    txtIdCategoria: TEdit;
    txtCategoriaPortico: TEdit;
    txtFactorMultiplicacion: TEdit;
    btnEditar: TSpeedButton;
    dblFactoresCategoria: TDBListEx;
    dsFactores: TDataSource;

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure dblFactoresCategoriaClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);

  private
    { Private declarations }


    procedure LimpiarCampos();
    procedure PermitirEdicion(Activar : boolean);
    procedure CargarFactores();

  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormFactoresCategoria: TFormFactoresCategoria;
  spObtenerFactores, spActualizarFactores: TADOStoredProc;
  
implementation

Uses
  DMConnection;

resourcestring
  MSG_CAPTION_VALIDAR               = 'Validar datos ingresados';
  MSG_FACTOR_VACIO                  = 'Ingrese Factor deCategor�a de Veh�culos';

  MSG_SELECCIONE                    = ' Seleccione ';

  MSG_TITULO_ERROR                  = 'Error de ejecuci�n';
  MSG_ACTUALIZAR_CAPTION   		      = 'Actualizar Categor�ade Veh�culo';
  MSG_ACTUALIZAR_OK                 = 'Categor�a de Veh�culo actualizado con �xito';
  MSG_ACTUALIZAR_ERROR              = 'Error al actualizar la Categor�a de Veh�culo';

{$R *.DFM}

function TFormFactoresCategoria.Inicializa: boolean;
begin

    PermitirEdicion(false);
    CargarFactores();

    lblShow_Inserta_Modifica.Caption := '';
    dblFactoresCategoria.SetFocus;

    Result := True;
end;


procedure TFormFactoresCategoria.CargarFactores();
begin
    if not Assigned(spObtenerFactores) then
    begin
        spObtenerFactores := TADOStoredProc.Create(nil);
        spObtenerFactores.Connection := DMConnections.BaseCAC;
        spObtenerFactores.ProcedureName := 'ObtenerCategorias';
        spObtenerFactores.Parameters.Refresh();
    end;

    dsFactores.DataSet := spObtenerFactores;
    spObtenerFactores.Open();

end;




procedure TFormFactoresCategoria.dblFactoresCategoriaClick(Sender: TObject);
begin
    if spObtenerFactores.RecordCount > 0 then begin
        txtIDCategoriaUnico.Text := (Sender as TDBListEx).DataSource.DataSet.FieldByName('IdCategoriaUnico').Value;
        txtDescripcion.Text := (Sender as TDBListEx).DataSource.DataSet.FieldByName('Descripcion').Value;
        txtIdCategoria.text := (Sender as TDBListEx).DataSource.DataSet.FieldByName('CodigoCategoria').Value;
        txtCategoriaPortico.text := (Sender as TDBListEx).DataSource.DataSet.FieldByName('CategoriaPortico').Value;
        txtFactorMultiplicacion.text := (Sender as TDBListEx).DataSource.DataSet.FieldByName('FactorTarifa').Value;
    end;
end;


procedure TFormFactoresCategoria.btnAceptarClick(Sender: TObject);
begin
    btnAceptar.Enabled := False;
    btnCancelar.Enabled := False;

    if not ValidateControlsCN([txtFactorMultiplicacion],
                            [TRIM(txtFactorMultiplicacion.Text) <> ''],
                            MSG_CAPTION_VALIDAR,
                            [MSG_FACTOR_VACIO],
                            Self) then begin
            btnAceptar.Enabled := True;
        Exit;
    end;

    Screen.Cursor := crHourGlass;

    if not Assigned(spActualizarFactores) then
    begin
        spActualizarFactores := TADOStoredProc.Create(nil);
        spActualizarFactores.Connection := DMConnections.BaseCAC;
        spActualizarFactores.ProcedureName := '';
        spActualizarFactores.Parameters.Refresh();
    end;

    spActualizarFactores.Parameters.ParamByName('CodigoCategoria').Value := txtIdCategoria.Text;
    spActualizarFactores.Parameters.ParamByName('FactorTarifa').Value := txtFactorMultiplicacion.Text;
    spActualizarFactores.ExecProc;

    if spActualizarFactores.Parameters.ParamByName('RETURN_VALUE').Value <> 0 then
    begin
        MsgBoxErr(MSG_ACTUALIZAR_ERROR,
                    spActualizarFactores.Parameters.ParamByName('ErrorDescription').Value,
                    MSG_TITULO_ERROR,
                    MB_ICONSTOP);
    end
    else
    begin
        MsgBox(MSG_ACTUALIZAR_OK, MSG_ACTUALIZAR_CAPTION, MB_OK);
    end;

    PermitirEdicion(false);
    LimpiarCampos();

    btnAceptar.Enabled := True;
    btnCancelar.Enabled := True;
end;

procedure TFormFactoresCategoria.btnCancelarClick(Sender: TObject);
begin
    PermitirEdicion(False);
    LimpiarCampos();
end;

procedure TFormFactoresCategoria.LimpiarCampos();
begin
	txtIDCategoriaUnico.Clear();
    txtDescripcion.Clear();
    txtIdCategoria.Clear();
    txtCategoriaPortico.Clear();
    txtFactorMultiplicacion.Clear();
end;

procedure TFormFactoresCategoria.btnEditarClick(Sender: TObject);
begin
    if Assigned(spObtenerFactores) and (spObtenerFactores.RecordCount > 0) then
        PermitirEdicion(True);
end;

procedure TFormFactoresCategoria.PermitirEdicion(Activar: Boolean);
begin
    pnlBotones.Enabled := not Activar;
    dblFactoresCategoria.Enabled := not Activar;
    txtFactorMultiplicacion.Enabled := Activar;
    nbBotones.PageIndex := 0;
    if Activar then
        nbBotones.PageIndex := 1;
end;


procedure TFormFactoresCategoria.btnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormFactoresCategoria.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    FreeAndNil(spObtenerFactores);
    FreeAndNil(spActualizarFactores);
end;

end.
