object FABMEntidades: TFABMEntidades
  Left = 107
  Top = 114
  Width = 825
  Height = 605
  Caption = 'Mantenimiento de Entidades'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 432
    Width = 817
    Height = 100
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Lcodigo: TLabel
      Left = 10
      Top = 11
      Width = 44
      Height = 13
      Caption = 'C'#243'digo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ldescripcion: TLabel
      Left = 11
      Top = 38
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ltipo: TLabel
      Left = 11
      Top = 68
      Width = 30
      Height = 13
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 86
      Top = 8
      Width = 105
      Height = 21
      Color = 16444382
      Enabled = False
      TabOrder = 0
      Decimals = 0
    end
    object txtDescripcion: TEdit
      Left = 87
      Top = 36
      Width = 388
      Height = 21
      Color = 16444382
      TabOrder = 1
    end
    object txttipo: TEdit
      Left = 87
      Top = 66
      Width = 58
      Height = 21
      Color = 16444382
      MaxLength = 1
      TabOrder = 2
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 532
    Width = 817
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 620
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 817
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object ListaEntidades: TAbmList
    Left = 0
    Top = 33
    Width = 817
    Height = 399
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'74'#0'C'#243'digo           '
      
        #0'358'#0'Descripci'#243'n                                                ' +
        '                                                  '
      #0'29'#0'Tipo')
    HScrollBar = True
    RefreshTime = 100
    Table = tblEntidades
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaEntidadesClick
    OnProcess = ListaEntidadesProcess
    OnDrawItem = ListaEntidadesDrawItem
    OnRefresh = ListaEntidadesRefresh
    OnInsert = ListaEntidadesInsert
    OnDelete = ListaEntidadesDelete
    OnEdit = ListaEntidadesEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object tblEntidades: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'Entidades'
    Left = 404
    Top = 81
  end
  object spActualizarEntidades: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEntidad;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoEntidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 476
    Top = 80
  end
  object dsEntidades: TDataSource
    DataSet = tblEntidades
    Left = 438
    Top = 80
  end
  object EliminarEntidad: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarEntidad'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEntidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Resultado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 512
    Top = 80
  end
end
