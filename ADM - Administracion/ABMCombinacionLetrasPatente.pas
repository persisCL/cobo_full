{***************************************************************************************
Revision 1:
  Author: mbecerra
  Date:  13-Mayo-2008
  Description:  Se hicieron las siguientes modificaciones:
  				* Se elimin� el objeto TAbmToolBar, y se reemplaz� por un Panel y
                  los correspondientes TSpeedButtons
                * Se habilit� el evento KeyPress de la DBList1, para que se posicione
                  en el primer registro que comience con la letra ingresada.
            	* Se agregaron dos botones: Filtrar y deshacer Filtrar. El primero aplica
                  un Filtro sobre la lista desplegada. El segundo anula dicho Filtro.
                * Se modificaron los mensajes de aviso al usuario


  Revision 2:
  Author: mbecerra
  Date: 30-Julio-2008
  Description:  Se hicieron las siguientes modificaciones.
            	* Se quit� el objeto TABMList y se reemplaz� por una DBGrid.
                  La raz�n es que la b�squeda por teclas funciona en Canelo, pero no en Pino.
                * Se modific� la funci�n CombinacionCargada pues recorr�a el objeto
                  TADOTable para saber si la combinaci�n ingresada se repet�a.
                  Se renombr� a CombinacionYaExiste, que es m�s claro.


  Revision 3: 
  Date: 19/02/2009
  Author: mpiazza
  Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
    los bloqueos de tablas en la lectura

***************************************************************************************}
unit ABMCombinacionLetrasPatente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls,RStrings, Validate, DateEdit, Menus, Grids, DBGrids, Variants,
  ComCtrls, Mask, DBCtrls;

type
  TFormABMCombinacionLetrasPatente = class(TForm)
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    ObtenerCombinacionLetrasValidas: TADOTable;
    Notebook: TNotebook;
    Label2: TLabel;
    Label4: TLabel;
    ObtenerCombinacionLetrasValidasCombinacionValida: TStringField;
    ObtenerCombinacionLetrasValidasDescripcion: TStringField;
    ObtenerCombinacionLetrasValidasTipoCombinacion: TStringField;
    GBDigitoVerificador: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ObtenerCombinacionLetrasValidasD7: TWordField;
    ObtenerCombinacionLetrasValidasD6: TWordField;
    ObtenerCombinacionLetrasValidasD5: TWordField;
    Label3: TLabel;
    deFecha: TDateEdit;
    ObtenerCombinacionLetrasValidasFechaActualizacion: TDateTimeField;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    pnlBotones: TPanel;
    btnTSalir: TSpeedButton;
    btnTInsertar: TSpeedButton;
    btnTEliminar: TSpeedButton;
    btnTEditar: TSpeedButton;
    btnTFiltrar: TSpeedButton;
    btnTDesFiltrar: TSpeedButton;
    VerificarLetraPatenteNueva: TADOQuery;
    tmerResetBuffer: TTimer;
    dsCombinacion: TDataSource;
    DBGrid1: TDBGrid;
    edtCombinacion: TEdit;
    edtDesc: TEdit;
    edtTipo: TEdit;
    edtDv7: TEdit;
    edtDv6: TEdit;
    edtDv5: TEdit;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure edtDv7KeyPress(Sender: TObject;
      var Key: Char);
    procedure btnTSalirClick(Sender: TObject);
    procedure btnTEliminarClick(Sender: TObject);
    procedure btnTEditarClick(Sender: TObject);
    procedure btnTFiltrarClick(Sender: TObject);
    procedure btnTDesFiltrarClick(Sender: TObject);
    procedure tmerResetBufferTimer(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure btnTInsertarClick(Sender: TObject);
    procedure dsCombinacionDataChange(Sender: TObject; Field: TField);

  private
    { Private declarations }
    FBuffer : string;
    FCuentaTimer : byte;
    FFechaHoy : TDateTime;
    FCombinacionActual : string;  {es la combinaci�n que est� siendo editada}
    FEstado : TDataSetState;               
    procedure VolverCampos;
    procedure HabilitarCampos;
    procedure LimpiarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormABMCombinacionLetrasPatente: TFormABMCombinacionLetrasPatente;

implementation

{$R *.DFM}

procedure TFormABMCombinacionLetrasPatente.LimpiarCampos;
begin
    edtCombinacion.Text := '';
    edtDv7.Text  := '';
    edtDv6.Text  := '';
    edtDv5.Text  := '';
    edtDesc.Text := '';
    edtTipo.Text := '';
    deFecha.Date := 0;
end;

procedure TFormABMCombinacionLetrasPatente.VolverCampos;
begin
	Notebook.PageIndex 	:= 0;
	groupb.Enabled     	:= False;
    pnlBotones.Enabled  := True;
    DBGrid1.SetFocus;
    FEstado := dsBrowse;
end;

function TFormABMCombinacionLetrasPatente.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([ObtenerCombinacionLetrasValidas]) then
		Result := False
	else begin
    	FFechaHoy := QueryGetValueDateTime(DMConnections.BaseCAC, 'SELECT GETDATE()');
    	Notebook.PageIndex := 0;
		Result := True;
        VolverCampos();
        dsCombinacionDataChange(nil, nil);
	end;
end;

procedure TFormABMCombinacionLetrasPatente.BtnCancelarClick(Sender: TObject);
begin
	VolverCampos();
end;

procedure TFormABMCombinacionLetrasPatente.DBGrid1KeyPress(Sender: TObject;
  var Key: Char);
begin
	if (FEstado = dsBrowse) and (Key in ['A'..'Z', 'a'..'z']) then begin
    	tmerResetBuffer.Enabled := True;
        FCuentaTimer := 0;
        FBuffer := FBuffer + Key;
        DBGrid1.DataSource.DataSet.Locate('CombinacionValida', FBuffer, [loPartialKey]);
    end;
end;

procedure TFormABMCombinacionLetrasPatente.dsCombinacionDataChange(
  Sender: TObject; Field: TField);
begin
	if (FEstado = dsBrowse) and (Field = nil) then begin
    	edtCombinacion.Text := Trim(ObtenerCombinacionLetrasValidas.FieldByName('CombinacionValida').AsString);
        edtDv7.Text         := Trim(ObtenerCombinacionLetrasValidas.FieldByName('D7').AsString);
        edtDv6.Text         := Trim(ObtenerCombinacionLetrasValidas.FieldByName('D6').AsString);
        edtDv5.Text         := Trim(ObtenerCombinacionLetrasValidas.FieldByName('D5').AsString);
        edtDesc.Text        := Trim(ObtenerCombinacionLetrasValidas.FieldByName('Descripcion').AsString);
        edtTipo.Text        := Trim(ObtenerCombinacionLetrasValidas.FieldByName('TipoCombinacion').AsString);
        deFecha.Date        := ObtenerCombinacionLetrasValidas.FieldByName('FechaActualizacion').AsDateTime;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: None
  Return Value: Sender: TObject

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}

procedure TFormABMCombinacionLetrasPatente.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_COMBINACION_NUEVA = 'La combinaci�n utilizada contiene una letra no v�lida';
    MSG_ERROR_LONGITUD = 'La combinaci�n utilizada solo tiene 3 caracteres';

    {Revision 2
    function CombinacionCargada(Combinacion:String;Posicion:Integer=-1):Boolean;
    var
        pos:TBookmark;
    begin
        result:=False;
        with ObtenerCombinacionLetrasValidas do begin
            pos:=GetBookmark;
            First;
            while not(eof) and (result=False) do begin
                if (Posicion<>RecNo) and (uppercase(FieldByName('CombinacionValida').AsString)=uppercase(Combinacion)) then
                        Result:=True;
                next;
            end;
            GotoBookmark(pos);
        end;
    end;}

    function CombinacionYaExiste( Combinacion : string ) : boolean;
    var
        Sql, CombinacionActual : string;
    begin
        {busca que la combinaci�n ingresada no exista}
        Sql := 'SELECT TOP 1 CombinacionValida FROM CombinacionLetrasValidas  WITH (NOLOCK) WHERE CombinacionValida = ''' + Combinacion + '''';
        if FEstado = dsEdit then begin
			if Combinacion = FCombinacionActual then begin
           	{si se est� editando y la combinaci�n no ha cambiado, entonces excluirla de la consulta}
            	Sql := Sql + ' AND CombinacionValida <> ''' + FCombinacionActual + '''';
            end;
        end;

        CombinacionActual := QueryGetValue(DMConnections.BaseCAC, Sql);
        Result := (CombinacionActual <> '');

    end;
    
    function VerificarLetrasNuevaPatente(Combinacion:String):Boolean;
    var
        i:integer;
    begin
        Result:=True;
        If VerificarLetraPatenteNueva.Active then VerificarLetraPatenteNueva.Active := False;
        for i := 1 to Length(Combinacion) do begin
            VerificarLetraPatenteNueva.Parameters.ParamByName('Letra').Value := Combinacion[i];
            VerificarLetraPatenteNueva.Active := True;
            if VerificarLetraPatenteNueva.IsEmpty then begin
                Result := False;
            end;
            VerificarLetraPatenteNueva.Active := False;
        end;
    end;


begin
    if length(edtCombinacion.Text) = 3 then begin
        MsgBoxBalloon(MSG_ERROR_LONGITUD,format(MSG_CAPTION_GESTION,[FLD_COMBINACION_PATENTE]),MB_ICONSTOP,edtCombinacion);
        Exit;
    end;

    if not ValidateControls([edtCombinacion, edtDv7, edtDv6, edtDv5, edtDesc, edtTipo, deFecha ],
			                [Trim(edtCombinacion.Text) <> '', Trim(edtDv7.Text) <> '', Trim(edtDv6.Text) <> '',
                			 Trim(edtDv5.Text) <> '', Trim(edtDv6.Text) <> '', Trim(edtTipo.Text)<>'', deFecha.Date > 0],
                			Format(MSG_CAPTION_GESTION,[FLD_COMBINACION_PATENTE]),
                			[Format(MSG_VALIDAR_DEBE_LA,[FLD_COMBINACION_PATENTE]),
                			 Format(MSG_VALIDAR_DEBE_EL,[FLD_DIGITO]),
                			 Format(MSG_VALIDAR_DEBE_EL,[FLD_DIGITO]),
                			 Format(MSG_VALIDAR_DEBE_EL,[FLD_DIGITO]),
                			 Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION]),
                             Format(MSG_VALIDAR_DEBE_EL,[FLD_TOP_COMBINACION]),
                             Format(MSG_VALIDAR_DEBE_LA,[FLD_FECHA]) ]) then Exit;

    {Revision 2
    if CombinacionCargada(	Trim(dbeCombinacion.Text),
    						iif(ObtenerCombinacionLetrasValidas.State <> dsEdit, -1 , ObtenerCombinacionLetrasValidas.RecNo)) then begin}
    if CombinacionYaExiste(Trim(edtCombinacion.Text)) then begin
        MsgBoxBalloon(format(MSG_ERROR_DUPLICADO,[FLD_COMBINACION_PATENTE]),format(MSG_CAPTION_GESTION,[FLD_COMBINACION_PATENTE]),MB_ICONSTOP,edtCombinacion);
        Exit;
    end;

    if Length(edtCombinacion.Text) = 4 then begin
        if not VerificarLetrasNuevaPatente(edtCombinacion.Text) then begin
            MsgBoxBalloon(MSG_ERROR_COMBINACION_NUEVA,format(MSG_CAPTION_GESTION,[FLD_COMBINACION_PATENTE]),MB_ICONSTOP, edtCombinacion);
            Exit;
        end;
    end;

 	Screen.Cursor := crHourGlass;
    Try
    	if FEstado = dsInsert then ObtenerCombinacionLetrasValidas.Append
        else ObtenerCombinacionLetrasValidas.Edit;

        ObtenerCombinacionLetrasValidas.FieldByName('CombinacionValida').AsString := edtCombinacion.Text;
        ObtenerCombinacionLetrasValidas.FieldByName('D7').AsString := edtDv7.Text;
        ObtenerCombinacionLetrasValidas.FieldByName('D6').AsString := edtDv6.Text;
        ObtenerCombinacionLetrasValidas.FieldByName('D5').AsString := edtDv5.Text;
        ObtenerCombinacionLetrasValidas.FieldByName('Descripcion').AsString := edtDesc.Text;
        ObtenerCombinacionLetrasValidas.FieldByName('TipoCombinacion').AsString := edtTipo.Text;
        ObtenerCombinacionLetrasValidas.FieldByName('FechaActualizacion').AsDateTime := deFecha.Date;
        ObtenerCombinacionLetrasValidas.Post;
    except on E: EDataBaseError do begin
				ObtenerCombinacionLetrasValidas.Cancel;
				MsgBoxErr(format(MSG_ERROR_ACTUALIZAR,[FLD_COMBINACION_PATENTE]), E.message, format(MSG_CAPTION_GESTION,[FLD_COMBINACION_PATENTE]), MB_ICONSTOP);
                Screen.Cursor	:= crDefault;
                Exit;
    		end;
    end;

    VolverCampos();
	Screen.Cursor	:= crDefault;
end;

procedure TFormABMCombinacionLetrasPatente.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Action := caFree;
end;

procedure TFormABMCombinacionLetrasPatente.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormABMCombinacionLetrasPatente.btnTDesFiltrarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
    ObtenerCombinacionLetrasValidas.Filtered := False;
    Screen.Cursor := crDefault;
end;

procedure TFormABMCombinacionLetrasPatente.btnTEditarClick(Sender: TObject);
begin
	FCombinacionActual := ObtenerCombinacionLetrasValidas.FieldByName('CombinacionValida').AsString;
    FEstado := dsEdit;
    HabilitarCampos();
end;

procedure TFormABMCombinacionLetrasPatente.btnTEliminarClick(Sender: TObject);
resourcestring
    MSG_ESTA_SEGURO = 'Est� seguro de eliminar esta combinaci�n?';
begin
    if (not ObtenerCombinacionLetrasValidas.Eof) and
       (ObtenerCombinacionLetrasValidas.State = dsBrowse) and
       (not ObtenerCombinacionLetrasValidas.Fields[0].IsNull) and
       (MsgBox(MSG_ESTA_SEGURO, Caption, MB_ICONQUESTION + MB_YESNO) = IDYES) then
        ObtenerCombinacionLetrasValidas.Delete;
    
end;

procedure TFormABMCombinacionLetrasPatente.btnTFiltrarClick(Sender: TObject);
resourcestring
	DEBE_INGRESAR = 'Debe ingresar 2 caracteres como m�nimo';
    NO_EXISTE_COMB = 'No existe la combinaci�n %s';
    SOLO_SE_PERMITEN = 'S�lo se permite ingresar letras para la combinaci�n a buscar';
var
	TextoABuscar : String;
    i : Integer;
    Continuar, MostrarVentana : boolean;
begin

	TextoABuscar := '';
    MostrarVentana := True;
    while MostrarVentana do begin

		if InputQuery(Caption, 'Combinaci�n a filtrar : ', TextoABuscar) then begin
    		TextoABuscar := UpperCase(Trim(TextoABuscar));
    		if Length(TextoABuscar) >= 2 then begin
          		Continuar := True;
          		i := 1;
          		while Continuar and (i <= Length(TextoABuscar)) do begin
          			Continuar := (TextoABuscar[i] in ['A'..'Z']);
            		Inc(i);
          		end;

          		if Continuar then begin
                	Screen.Cursor := crHourGlass;
            		ObtenerCombinacionLetrasValidas.Filter := 'CombinacionValida LIKE ''' +  TextoABuscar + '%'' ';
            		ObtenerCombinacionLetrasValidas.Sort := 'CombinacionValida ASC';
            		ObtenerCombinacionLetrasValidas.Filtered := True;
                    if ObtenerCombinacionLetrasValidas.RecordCount = 0 then begin
                        MsgBox(Format(NO_EXISTE_COMB, [TextoABuscar]), Caption, MB_OK + MB_ICONEXCLAMATION);
                    end
                    else begin
                    	MostrarVentana := False;
                    end;
                    
                    Screen.Cursor := crDefault;
          		end
          		else
            		MsgBox(SOLO_SE_PERMITEN, Caption, MB_OK + MB_ICONEXCLAMATION);
        	end
        	else
        		MsgBox(DEBE_INGRESAR, Caption, MB_OK + MB_ICONEXCLAMATION);
        end
    	else
    		MostrarVentana := False;
	end; {while}


end;

procedure TFormABMCombinacionLetrasPatente.btnTSalirClick(Sender: TObject);
begin
	BtnSalirClick(nil);
end;

procedure TFormABMCombinacionLetrasPatente.HabilitarCampos;
begin
	pnlBotones.Enabled  := False;
	Notebook.PageIndex 	:= 1;
    groupb.Enabled     	:= True;
	edtCombinacion.SetFocus;
end;

procedure TFormABMCombinacionLetrasPatente.tmerResetBufferTimer(
  Sender: TObject);
begin
    Inc(FCuentaTimer);
    if FCuentaTimer >= 3 then begin
    	tmerResetBuffer.Enabled := False;
        FBuffer := '';
    end;

end;

procedure TFormABMCombinacionLetrasPatente.edtDv7KeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9', Char(VK_BACK)]) then
        Key := #0;
end;



procedure TFormABMCombinacionLetrasPatente.btnTInsertarClick(Sender: TObject);
begin
    FEstado := dsInsert;
    HabilitarCampos();
    LimpiarCampos();
    deFecha.Date := FFechaHoy;
end;

end.
