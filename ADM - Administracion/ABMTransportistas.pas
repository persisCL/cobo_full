{-----------------------------------------------------------------------------
 File Name: ABMTransportistas
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ABMTransportistas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaTypes,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, ListBoxEx, DBListEx, Variants,
  RStrings, VariantComboBox;

type
  TFABMTransportes = class(TForm)
	GroupB: TPanel;
	Panel2: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
	Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
	Label2: TLabel;
    Label3: TLabel;
    spActualizarTransportistas: TADOStoredProc;
	Label7: TLabel;
	Label10: TLabel;
    ListaTransportistas: TAbmList;
    dsTransportistas: TDataSource;
    txtDescripcion: TEdit;
    Label1: TLabel;
    cbAlmacenes: TVariantComboBox;
    Transportistas: TADOQuery;
    EliminarTransportista: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
	procedure BtnCancelarClick(Sender: TObject);
	procedure ListaTransportistasClick(Sender: TObject);
	procedure ListaTransportistasDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaTransportistasEdit(Sender: TObject);
	procedure ListaTransportistasRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure ListaTransportistasDelete(Sender: TObject);
	procedure ListaTransportistasInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function ListaTransportistasProcess(Tabla: TDataSet; var Texto: String): Boolean;
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMTransportes: TFABMTransportes;

implementation

{$R *.DFM}

function TFABMTransportes.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([Transportistas]) then exit;

    {INICIO:	20160725 CFU
	CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, 0, true);
    }
	CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, '', 0, true);
    //TERMINO:	20160725 CFU
    Notebook.PageIndex := 0;

	ListaTransportistas.Reload;
	Result := True;
end;

procedure TFABMTransportes.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TFABMTransportes.ListaTransportistasClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
		txtCodigo.Value		:= FieldByName('CodigoTransportista').AsInteger;
	   	txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
        {INICIO:	20160725 CFU
		CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, FieldByname('AlmacenAsociado').AsInteger, true);
        }
        CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, '', FieldByname('AlmacenAsociado').AsInteger, true);
        //TERMINO:	20160725 CFU
	end;
end;

procedure TFABMTransportes.ListaTransportistasDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoTransportista').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFABMTransportes.ListaTransportistasEdit(Sender: TObject);
begin
	ListaTransportistas.Enabled:= False;
	ListaTransportistas.Estado := modi;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion
end;

procedure TFABMTransportes.ListaTransportistasRefresh(Sender: TObject);
begin
	 if ListaTransportistas.Empty then LimpiarCampos;
end;

procedure TFABMTransportes.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
end;

procedure TFABMTransportes.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

procedure TFABMTransportes.ListaTransportistasDelete(Sender: TObject);
resourcestring
 MSG_EXISTEN_GUIASDESPACHO = 'Existen Gu�as de Despacho asociadas con este transportista.';
 MSG_DELETE_CAPTION		   = 'Eliminar Maestro de Transportistas';
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_TRANSPORTES]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with EliminarTransportista do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoTransportista').Value := Transportistas.FieldbyName('CodigoTransportista').Value;
                Parameters.ParamByName('@Resultado').Value := 0;
                ExecProc;
                if Parameters.ParamByName('@Resultado').Value = 1 then
                    MsgBox(MSG_EXISTEN_GUIASDESPACHO, MSG_DELETE_CAPTION, MB_ICONSTOP);
                close;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_TRANSPORTES]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_TRANSPORTES]), MB_ICONSTOP);
			end
		end
	end;

	ListaTransportistas.Reload;
	ListaTransportistas.Estado := Normal;
	ListaTransportistas.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFABMTransportes.ListaTransportistasInsert(Sender: TObject);
begin
	LimpiarCampos;
    {INICIO:	20160725 CFU
	CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, 0, true);
    }
    CargarAlmacenes(DMConnections.BaseCAC, cbAlmacenes, '', 0, true);
    //TERMINO:	20160725 CFU
    groupb.Enabled          	:= True;
	ListaTransportistas.Enabled := False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDescripcion;
end;

procedure TFABMTransportes.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CAMPO_DEPOSITO = 'Debe completar un Almac�n.';


begin



	if not ValidateControls([txtDescripcion, cbAlmacenes ],
	  [(Trim(txtDescripcion.Text) <> ''), cbAlmacenes.itemindex > 0],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_TRANSPORTES]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION]), MSG_CAMPO_DEPOSITO ]) then begin
 		Exit;
	end;



	with ListaTransportistas do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarTransportistas, Parameters do begin
					ParamByName('@CodigoTransportista').Value 	:= txtCodigo.ValueInt;
					ParamByName('@Descripcion').Value 			:= Trim(txtDescripcion.Text);
					ParamByName('@AlmacenAsociado').Value 		:= cbAlmacenes.Items[cbAlmacenes.ItemIndex].Value;
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_TRANSPORTES]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_TRANSPORTES]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TFABMTransportes.FormShow(Sender: TObject);
begin
	ListaTransportistas.Reload;
end;

procedure TFABMTransportes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TFABMTransportes.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

procedure TFABMTransportes.Volver_Campos;
begin
	ListaTransportistas.Estado := Normal;
	ListaTransportistas.Enabled:= True;

	ActiveControl       := ListaTransportistas;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaTransportistas.Reload;
end;

function TFABMTransportes.ListaTransportistasProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Result := True;
end;


end.

