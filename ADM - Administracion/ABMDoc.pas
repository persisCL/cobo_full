unit AbmDoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB, DMConnection,
  DPSControls;

type
  TFormDocs = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    Documentos: TADOTable;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    txt_Descripcion: TEdit;
    txt_Documento: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
    chk_default: TCheckBox;
    txtMascara: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    txtCaracter: TEdit;
    chkDigitoVerificador: TCheckBox;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormDocs  : TFormDocs;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Tipo de Documento?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Tipo de Documento porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Tipo de Documento';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Tipo de Documento.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Tipo de Documento';

{$R *.DFM}

function TFormDocs.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([Documentos]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;
end;

procedure TFormDocs.Limpiar_Campos();
begin
	txt_Documento.Clear;
	txt_Descripcion.Clear;
end;

function TFormDocs.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	 Result := True;
	 Texto :=  PadR(Trim(Tabla.FieldByName('CodigoDocumento').AsString), 4, ' ' ) + ' '+
			  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormDocs.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFormDocs.ListaInsert(Sender: TObject);
begin
	Screen.Cursor      := crHourGlass;
	Lista.Estado     := Alta;
	Limpiar_Campos;
	GroupB.Enabled     := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txt_Documento.SetFocus;
	Screen.Cursor      := crDefault;
end;

procedure TFormDocs.ListaEdit(Sender: TObject);
begin
	Screen.Cursor      := crHourGlass;
	Lista.Estado     := Modi;
	GroupB.Enabled     := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txt_Documento.SetFocus;
	Screen.Cursor      := crDefault;
end;

procedure TFormDocs.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			Documentos.Delete;
		Except
			On E: EDataBaseError do begin
				Documentos.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormDocs.ListaClick(Sender: TObject);
begin
	 with Documentos do begin
		  txt_Documento.Text	:= Trim(FieldByName('CodigoDocumento').AsString);
		  txt_Descripcion.text	:= Trim(FieldByName('Descripcion').AsString);
		  chk_default.checked	:= FieldByName('DocumentoDefault').AsBoolean;
          chkDigitoVerificador.Checked := FieldByname('ValidarNroDoc').asBoolean;
          txtMascara.Text		:= ParseParambyNumber(Trim(FieldByName('Mascara').AsString), 1, ';');
          txtCaracter.Text		:= ParseParambyNumber(Trim(FieldByName('Mascara').AsString), 3, ';');
	 end;
end;

procedure TFormDocs.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormDocs.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormDocs.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('CodigoDocumento').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormDocs.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormDocs.BtnAceptarClick(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	With Documentos do begin
		Try
			if Lista.Estado = Alta then Append else Edit;
			if chk_default.checked then QueryExecute(Documentos.Connection,
            	'UPDATE TiposDocumento SET DocumentoDefault = 0');
			FieldByName('CodigoDocumento').AsString   := Trim(txt_Documento.Text);
			FieldByName('Descripcion').AsString 	  := Trim(txt_Descripcion.Text);
			FieldByName('DocumentoDefault').AsBoolean := chk_default.checked;
            FieldByName('ValidarNroDoc').AsBoolean    := chkDigitoVerificador.checked;
            FieldByName('Mascara').AsString 	  	  := Trim(txtMascara.Text) + ';0;' + Trim(txtCaracter.Text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado     	:= Normal;
	Lista.Enabled    	:= True;
	GroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormDocs.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormDocs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormDocs.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
