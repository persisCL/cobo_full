{********************************** File Header *********************************
File Name   : FrmRegistroDeOperacionesFiltrar.pas
Author      : Patricio Diego Cingolani <padici@dpsautomation.com>
Date Created: 02/05/2005
Language    : ES-AR
Description : Formulario que despliega las opciones de filtrado de los eventos
              desplegados en pantalla.
********************************************************************************}
unit FrmRegistroDeOperacionesFiltrar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DB, ADODB, UtilProc, Util, ExtCtrls,
  Validate, DateEdit;

type
  TFormRegistroDeOperacionesFiltrar = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    ObtenerCategoriasModulos: TADOStoredProc;
    pnlFiltrar: TPanel;
    labCategoria: TLabel;
    labAccion: TLabel;
    labModulo: TLabel;
    cbModulos: TVariantComboBox;
    labSeveridad: TLabel;
    labUsuario: TLabel;
    labResultado: TLabel;
    cbCategorias: TVariantComboBox;
    cbAcciones: TVariantComboBox;
    cbSeveridades: TVariantComboBox;
    cbUsuarios: TVariantComboBox;
    cbResultados: TVariantComboBox;
    DateEditFechaDesde: TDateEdit;
    labFechaDesde: TLabel;
    DateEditFechaHasta: TDateEdit;
    labFechaHasta: TLabel;
    ObtenerModulos: TADOStoredProc;
    ObtenerAcciones: TADOStoredProc;
    ObtenerUsuariosSistemas: TADOStoredProc;
    btnRestaurar: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbCategoriasChange(Sender: TObject);
    procedure btnRestaurarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    CodigoSistema: shortint;
  end;

var
  FormRegistroDeOperacionesFiltrar: TFormRegistroDeOperacionesFiltrar;

implementation

uses DMConnection;

{$R *.dfm}

procedure TFormRegistroDeOperacionesFiltrar.FormClose(Sender: TObject;
	var Action: TCloseAction);
begin
	Action := caHide;
end;

procedure TFormRegistroDeOperacionesFiltrar.cbCategoriasChange(
  Sender: TObject);
resourcestring
     MSG_ERROR_CARGAR_MODULOS = 'No se ha podido realizar la carga de m�dulos.';
begin
	// Cargamos la lista de m�dulos en el combo.
     cbModulos.Items.BeginUpdate;
     cbModulos.Clear;
     cbModulos.Items.Add('Todos', 0);
     try
          ObtenerModulos.Parameters.ParamByName('@CodigoCategoria').Value := iif((cbCategorias.ItemIndex > 0), cbCategorias.ItemIndex, 0);
          ObtenerModulos.Open;
          while not (ObtenerModulos.Eof = True) do begin
	          cbModulos.Items.Add(ObtenerModulos.FieldByName('Descripcion').AsString, ObtenerModulos.FieldByName('CodigoModulo').AsString);
        	     ObtenerModulos.Next;
		end;
          ObtenerModulos.Close;
     except
    	     on e:exception do begin
	          Screen.Cursor := crDefault;
	          MsgBoxErr(MSG_ERROR_CARGAR_MODULOS, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
          end;
     end;
     cbModulos.Items.EndUpdate;
     cbModulos.ItemIndex := 0;
end;

procedure TFormRegistroDeOperacionesFiltrar.btnRestaurarClick(
  Sender: TObject);
begin
     // Coloca el valor por defecto de cada control.
     cbCategorias.ItemIndex := 0;
     cbModulos.ItemIndex := 0;
     cbAcciones.ItemIndex := 0;
     DateEditFechaDesde.Date := (Date - 1);
     DateEditFechaHasta.Date := Date;
     cbSeveridades.ItemIndex := 0;
     cbUsuarios.ItemIndex := 0;
     cbResultados.ItemIndex := 0;
end;

procedure TFormRegistroDeOperacionesFiltrar.FormCreate(Sender: TObject);
resourcestring
     MSG_ERROR_CARGAR_ACCIONES = 'No se ha podido realizar la carga de acciones.';
     MSG_ERROR_CARGAR_USUARIOS = 'No se ha podido realizar la carga de usuarios.';
     MSG_ERROR_CARGAR_CATEGORIAS = 'No se ha podido realizar la carga de categor�as.';
begin
	// Cargamos la lista de categor�as en el combo.
     cbCategorias.Items.BeginUpdate;
     cbCategorias.Items.Add('Todas', 0);
     try
          ObtenerCategoriasModulos.Open;
          while not (ObtenerCategoriasModulos.Eof = True) do begin
	          cbCategorias.Items.Add(ObtenerCategoriasModulos.FieldByName('Descripcion').AsString, ObtenerCategoriasModulos.FieldByName('CodigoCategoria').AsString);
        	     ObtenerCategoriasModulos.Next;
		end;
          ObtenerCategoriasModulos.Close;
     except
    	     on e:exception do begin
	          Screen.Cursor := crDefault;
	          MsgBoxErr(MSG_ERROR_CARGAR_CATEGORIAS, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
          	Screen.Cursor := crHourGlass;
          end;
     end;
     cbCategorias.Items.EndUpdate;
	// Cargamos la lista de acciones en el combo.
     cbAcciones.Items.BeginUpdate;
     cbAcciones.Items.Add('Todas', 0);
     try
          ObtenerAcciones.Open;
          while not (ObtenerAcciones.Eof = True) do begin
	          cbAcciones.Items.Add(ObtenerAcciones.FieldByName('Descripcion').AsString, ObtenerAcciones.FieldByName('CodigoAccion').AsString);
        	     ObtenerAcciones.Next;
		end;
          ObtenerAcciones.Close;
     except
    	     on e:exception do begin
	          Screen.Cursor := crDefault;
	          MsgBoxErr(MSG_ERROR_CARGAR_ACCIONES, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
          	Screen.Cursor := crHourGlass;
          end;
     end;
     cbAcciones.Items.EndUpdate;
	// Cargamos la lista de usuarios en el combo.
     cbUsuarios.Items.BeginUpdate;
     cbUsuarios.Items.Add('Todos', '');
     try
          ObtenerUsuariosSistemas.Open;
          while not (ObtenerUsuariosSistemas.Eof = True) do begin
	          cbUsuarios.Items.Add(ObtenerUsuariosSistemas.FieldByName('CodigoUsuario').AsString, ObtenerUsuariosSistemas.FieldByName('CodigoUsuario').AsString);
        	     ObtenerUsuariosSistemas.Next;
		end;
          ObtenerUsuariosSistemas.Close;
     except
    	     on e:exception do begin
	          Screen.Cursor := crDefault;
	          MsgBoxErr(MSG_ERROR_CARGAR_USUARIOS, e.Message, Application.Title, MB_ICONSTOP + MB_OK);
          end;
     end;
     cbUsuarios.Items.EndUpdate;
end;

procedure TFormRegistroDeOperacionesFiltrar.btnAceptarClick(
  Sender: TObject);
resourcestring
     MSG_ERROR_ACEPTAR = 'Debe completar el campo para poder continuar.';
begin
	if (cbCategorias.Value = Null) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbCategorias);
     end
	else if (cbModulos.Value = Null) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbModulos);
     end
     else if (cbAcciones.Value = Null) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbAcciones);
     end
     else if (DateEditFechaDesde.Date = NullDate) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, DateEditFechaDesde);
     end
     else if (DateEditFechaHasta.Date = NullDate) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, DateEditFechaHasta);
     end
     else if (cbSeveridades.Value = Null) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbSeveridades);
     end
     else if (cbUsuarios.Value = Null) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbUsuarios);
     end
     else if (cbResultados.Value = Null) then begin
          MsgBoxBalloon(MSG_ERROR_ACEPTAR, Application.Title, MB_ICONINFORMATION + MB_OK, cbResultados);
     end
     else begin
          Close;
          ModalResult := mrOk;
     end;
end;

end.
