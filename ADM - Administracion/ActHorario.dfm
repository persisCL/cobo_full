object FormActualizarHorario: TFormActualizarHorario
  Left = 502
  Top = 188
  BorderStyle = bsDialog
  Caption = 'Actualizar Horario'
  ClientHeight = 109
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 46
    Width = 61
    Height = 13
    Caption = 'Tipo Horario:'
  end
  object Label2: TLabel
    Left = 12
    Top = 18
    Width = 54
    Height = 13
    Caption = 'Hora Inicio:'
  end
  object Bevel1: TBevel
    Left = 4
    Top = 3
    Width = 306
    Height = 71
  end
  object cb_TiposHorario: TComboBox
    Left = 92
    Top = 41
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = cb_TiposHorarioClick
    OnClick = cb_TiposHorarioClick
  end
  object dt_HoraInicio: TTimeEdit
    Left = 92
    Top = 14
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    OnChange = cb_TiposHorarioClick
    AllowEmpty = False
    ShowSeconds = False
  end
  object btn_Aceptar: TDPSButton
    Left = 156
    Top = 80
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 2
  end
  object Button2: TDPSButton
    Left = 236
    Top = 80
    Caption = '&Cancelar'
    TabOrder = 3
    OnClick = Button2Click
  end
  object qry_TiposHorario: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM TIPOSHORARIO WITH (NOLOCK) ')
    Left = 252
    Top = 16
  end
end
