unit FrmNumerosTerminal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, DBGrids, DMConnection, DB,
  ADODB, UtilDb, Util, Utilproc, DmiCtrls, ExtCtrls, PeaProcs,
  DPSControls;

type
  TFormNumerosTerminal = class(TForm)
    ds_CodigosComercio: TDataSource;
    GrillaNumerosTerminal: TStringGrid;
    ObtenerNumerosTerminal: TADOStoredProc;
    ActualizarDatosNumeroTerminal: TADOStoredProc;
    Label1: TLabel;
    Label3: TLabel;
    cb_Tarjetas: TComboBox;
    Panel1: TPanel;
    Notebook1: TNotebook;
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    Label4: TLabel;
    cb_AplicacionesVenta: TComboBox;
    Label2: TLabel;
    cb_OperadorLogistico: TComboBox;
    Label5: TLabel;
    cb_LugarDeVenta: TComboBox;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure GrillaNumerosTerminalSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GrillaNumerosTerminalEnter(Sender: TObject);
    procedure GrillaNumerosTerminalKeyPress(Sender: TObject; var Key: Char);
    procedure ActualizarGrilla(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cb_OperadorLogisticoChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperadorLogistico: integer;
    FCodigoLugarDeVenta: integer;
  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean; CodigoOperadorLogistico: integer = 0;
                        CodigoLugarDeVenta: integer = 0): boolean;
    procedure LimpiarGrilla;
  end;

var
  FormNumerosTerminal: TFormNumerosTerminal;

implementation

{$R *.dfm}

{ TFormLiquidacion }
var
  Conjunto : set of char;

function TFormNumerosTerminal.Inicializa(MDIChild: Boolean; CodigoOperadorLogistico: integer = 0;
                                         CodigoLugarDeVenta: integer = 0): boolean;
resourcestring
    TARJETA_DE_CREDITO = 'Tarjeta de cr�dito';
    NUMERO_PUESTO      = 'N�mero de puesto';
    NUMERO_TERMINAL    = 'N�mero de Terminal';
    NO_HAY_OPERADORES  = 'No hay Operadores Log�sticos cargados en el Sistema';
    NO_HAY_TARJETAS    = 'No existen tarjetas de cr�dito cargadas en el sistema';
    NO_HAY_SISTEMAS    = 'No existen Sistemas cargados';
    TODAS              = 'TODAS';
    ERROR_CONFIGURACION_INICIAL = 'No se pudo cargar la configuraci�n inicial';
    INICIALIZAR_CARGA  = 'Inicializar la Carga de C�digos de Comercio';

Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    FCodigoOperadorLogistico := CodigoOperadorLogistico;
    FCodigoLugarDeVenta := CodigoLugarDeVenta;

	Conjunto :=  [#8, #9, #13, '0'..'9', Char(VK_BACK), Char(VK_SPACE), Char(VK_DELETE)];

    With GrillaNumerosTerminal do begin
		Cells[0,0] := TARJETA_DE_CREDITO;
		Cells[1,0] := NUMERO_PUESTO;
		Cells[2,0] := NUMERO_TERMINAL;
    end;

    result := False;
    try
        CargarOperadoresLogisticos(DMConnections.BaseCAC, cb_OperadorLogistico);
		if cb_OperadorLogistico.Items.Count < 1 then
        	raise Exception.Create(NO_HAY_OPERADORES);
        if cb_OperadorLogistico.ItemIndex = -1 then
            cb_OperadorLogistico.ItemIndex := 0;
        CargarLugaresDeVenta(DMConnections.BaseCAC,cb_LugarDeVenta,
                            StrToInt(StrRight(cb_OperadorLogistico.Text, 20)));
        cb_LugarDeVenta.ItemIndex := 0;

        //Cargar los sistemas de venta por eso se manda true
        CargarSistemas(DMConnections.BaseCAC,cb_AplicacionesVenta, true);
		if cb_AplicacionesVenta.Items.Count < 1 then
	        raise Exception.Create(NO_HAY_SISTEMAS);

        CargarTarjetasCredito(DMConnections.BaseCAC,cb_Tarjetas);
		if cb_Tarjetas.Items.Count < 1 then
        	raise Exception.Create(NO_HAY_TARJETAS);

        cb_Tarjetas.Items.Insert(0, TODAS);
        cb_Tarjetas.ItemIndex := 0;
        cb_AplicacionesVenta.ItemIndex := 0;

        if FCodigoOperadorLogistico <> 0 then cb_OperadorLogistico.Enabled := False;
        if FCodigoLugarDeVenta <> 0 then cb_LugarDeVenta.Enabled := false;
        ActualizarGrilla(cb_LugarDeVenta);
        Result := True;

    except
		On E: exception do begin
        	MsgBoxErr(ERROR_CONFIGURACION_INICIAL,
            e.Message, INICIALIZAR_CARGA, MB_ICONSTOP);
        end;
    end;
end;

procedure TFormNumerosTerminal.btn_AceptarClick(Sender: TObject);
ResourceString
    MSG_ACTUALIZAR_NUMERO_TERMINAL_CAPTION  = 'Actualizar N�meros de Terminal';
    MSG_ACTUALIZAR_NUMERO_TERMINAL_QUESTION = '�Desea guardar los datos de los N�meros de Terminal?';
    MSG_ACTUALIZAR_NUMERO_TERMINAL_ERROR    = 'No se pudieron almacenar los datos del N�mero de Terminal';

Var
	i: integer;
    TodoOk: boolean;
    Texto: AnSiString;
begin
	TodoOK := False;
	if MsgBox(MSG_ACTUALIZAR_NUMERO_TERMINAL_QUESTION, MSG_ACTUALIZAR_NUMERO_TERMINAL_CAPTION, MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) <> IDYES then Exit;

	Screen.Cursor := crHourGlass;
	// Recorro toda la grilla y guardo los Numeros de Terminal.
    try

    	DMConnections.BaseCAC.BeginTrans;
		//Grabo los nuevos N�meros de terminal.
        for i := 1 to GrillaNumerosTerminal.RowCount - 1 do begin
        	if Trim(GrillaNumerosTerminal.Cells[2, i]) <> '' then begin

	            try
					with ActualizarDatosNumeroTerminal.Parameters do begin
                        ParamByName('@CodigoLugarDeVenta').Value := Ival(strRight(cb_LugarDeVenta.Text, 20));
                     	ParamByName('@CodigoPuntoVenta').Value 	 := Ival(StrRight(GrillaNumerosTerminal.Cells[1,i], 10));
                        ParamByName('@CodigoSistema').Value		 := Ival(strRight(cb_AplicacionesVenta.Text, 10));
                        ParamByName('@CodigoTarjeta').Value 	 := Ival(StrRight(GrillaNumerosTerminal.Cells[0,i], 10));
                        ParamByName('@NumeroTerminal').Value 	 := Trim(GrillaNumerosTerminal.Cells[2,i]);
                     end;
                     ActualizarDatosNumeroTerminal.ExecProc;
                     ActualizarDatosNumeroTerminal.Close;
                except
                     on E: Exception do begin
                        MsgBoxErr(MSG_ACTUALIZAR_NUMERO_TERMINAL_ERROR, e.message,MSG_ACTUALIZAR_NUMERO_TERMINAL_CAPTION, MB_ICONSTOP);
                        ActualizarDatosNumeroTerminal.Close;
                        Exit;
                     end;
                end;

            end else begin

                //Primero Borro todo lo que haya.
                try
                    Texto := 'DELETE FROM NumerosTerminal ' +
                        'WHERE CodigoLugarDeVenta = ' + IntToStr(Ival(strRight(cb_LugarDeVenta.Text, 10))) +
						'  AND CodigoSistema = ' + IntToStr(Ival(StrRight(cb_AplicacionesVenta.Text, 10)))+
						'  AND CodigoTarjeta = ' + IntToStr(Ival(StrRight(GrillaNumerosTerminal.Cells[0,i], 10))) +
						'  AND CodigoPuntoVenta = ' + IntToStr(Ival(StrRight(GrillaNumerosTerminal.Cells[1,i], 10)));
                    QueryExecute(DMConnections.BaseCAC, Texto);
                except
                    On E: Exception do begin
                        MsgBoxErr(MSG_ACTUALIZAR_NUMERO_TERMINAL_ERROR, e.Message, MSG_ACTUALIZAR_NUMERO_TERMINAL_CAPTION, MB_ICONSTOP);
                        Exit;
                    end;
                end;
            end;
        end;
        TodoOK := True;

	finally
        if TodoOK then begin
        	DMConnections.BaseCAC.CommitTrans;
            ModalResult := mrOk;
        end
        else DMConnections.BaseCAC.RollbackTrans;
      	Screen.Cursor := crDefault;
    end;
	Screen.Cursor := crDefault;
end;

procedure TFormNumerosTerminal.btn_CancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormNumerosTerminal.GrillaNumerosTerminalSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
	if ACol <> 2 then CanSelect := False;
end;

procedure TFormNumerosTerminal.GrillaNumerosTerminalEnter(Sender: TObject);
var
	CanClose : Boolean;
begin
	GrillaNumerosTerminal.Row := 1;
    GrillaNumerosTerminal.Col := 2;
	GrillaNumerosTerminal.OnSelectCell(GrillaNumerosTerminal, 2, 1, CanClose);
end;

procedure TFormNumerosTerminal.GrillaNumerosTerminalKeyPress(Sender: TObject; var Key: Char);
begin
	if  not (Key in Conjunto) then Key := #0;
end;


procedure TFormNumerosTerminal.ActualizarGrilla(Sender: TObject);
var
	i: integer;
begin
	ObtenerNumerosTerminal.Close;
	with ObtenerNumerosTerminal.Parameters do begin
        ParamByName('@CodigoLugarDeVenta').Value	    := Ival(StrRight(cb_LugarDeVenta.Text, 10));
    	ParamByName('@CodigoSistema').Value		        := Ival(StrRight(cb_AplicacionesVenta.Text, 10));
		ParamByName('@CodigoTarjeta').Value		        := iif(cb_Tarjetas.ItemIndex = 0, null, Ival(StrRight(cb_Tarjetas.Text, 10)));
    end;
	OpenTables([ObtenerNumerosTerminal]);
    LimpiarGrilla;
    GrillaNumerosTerminal.rowCount :=  iif( (ObtenerNumerosTerminal.RecordCount = 0), 2, ObtenerNumerosTerminal.RecordCount + 1);
    ObtenerNumerosTerminal.First;
    i:= 1;
    While not ObtenerNumerosTerminal.Eof do begin
        GrillaNumerosTerminal.Cells[0,i] := PadR(Trim(ObtenerNumerosTerminal.FieldByName('DescriTarjeta').AsString), 300, ' ') + Istr(ObtenerNumerosTerminal.FieldByName('CodigoTarjeta').AsInteger);
        GrillaNumerosTerminal.Cells[1,i] := PadR(Trim(ObtenerNumerosTerminal.FieldByName('DescriPuntoVenta').AsString), 300, ' ') + Istr(ObtenerNumerosTerminal.FieldByName('CodigoPuntoVenta').AsInteger);
        GrillaNumerosTerminal.Cells[2,i] := Trim(ObtenerNumerosTerminal.FieldByName('NumeroTerminal').AsString);
        ObtenerNumerosTerminal.Next;
		inc(i);
    end;
    btn_Aceptar.Enabled := ObtenerNumerosTerminal.RecordCount > 0;
    ObtenerNumerosTerminal.Close;
//    GrillaNumerosTerminal.SetFocus;
end;

procedure TFormNumerosTerminal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    cb_OperadorLogistico.Enabled := true;
    cb_LugarDeVenta.Enabled := true;
    Action := caFree;
end;

procedure TFormNumerosTerminal.LimpiarGrilla;
var
	i: integer;
begin
    for i := 1 to GrillaNumerosTerminal.RowCount - 1 do begin
        GrillaNumerosTerminal.Cells[0, i] := '';
        GrillaNumerosTerminal.Cells[1, i] := '';
        GrillaNumerosTerminal.Cells[2, i] := '';
    end;
end;

procedure TFormNumerosTerminal.cb_OperadorLogisticoChange(Sender: TObject);
begin
    CargarLugaresDeVenta(DMConnections.BaseCAC, cb_LugarDeVenta,
                         StrToInt(StrRight(cb_OperadorLogistico.Text, 20)));
    cb_LugarDeVenta.ItemIndex := 0;                         
    ActualizarGrilla(cb_LugarDeVenta);
end;

end.
