object FormCodigosAreaTelefono: TFormCodigosAreaTelefono
  Left = 283
  Top = 136
  Caption = 'Mantenimiento de C'#243'digos de '#193'rea'
  ClientHeight = 390
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object dblCodigosArea: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 175
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'117'#0'C'#243'digo Area                 '
      #0'199'#0'Descripci'#243'n                                             '
      #0'136'#0'Rango Telefonico Desde   '
      #0'124'#0'Rango Telefonico Hasta'
      #0'75'#0'C'#243'digo Pa'#237's   '
      #0'60'#0'Tipo L'#237'nea')
    HScrollBar = True
    RefreshTime = 100
    Table = CodigosAreaTelefono
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = dblCodigosAreaClick
    OnDrawItem = dblCodigosAreaDrawItem
    OnRefresh = dblCodigosAreaRefresh
    OnInsert = dblCodigosAreaInsert
    OnDelete = dblCodigosAreaDelete
    OnEdit = dblCodigosAreaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object GroupB: TPanel
    Left = 0
    Top = 208
    Width = 592
    Height = 143
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object lblDescripcion: TLabel
      Left = 31
      Top = 37
      Width = 59
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblCodigoArea: TLabel
      Left = 16
      Top = 11
      Width = 74
      Height = 13
      Caption = '&C'#243'digo Area:'
      FocusControl = txtCodigoArea
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 21
      Top = 62
      Width = 69
      Height = 13
      Caption = '&Rango Desde:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 207
      Top = 62
      Width = 66
      Height = 13
      Caption = 'Rango &Hasta:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblTipo: TLabel
      Left = 35
      Top = 111
      Width = 55
      Height = 13
      Caption = '&Tipo L'#237'nea:'
      FocusControl = vcbTiposTelefono
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblCodigoPais: TLabel
      Left = 29
      Top = 86
      Width = 61
      Height = 13
      Caption = 'C'#243'digo &Pa'#237's:'
      FocusControl = txt_CodigoPais
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtDescripcion: TEdit
      Left = 98
      Top = 34
      Width = 289
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 40
      TabOrder = 1
    end
    object txtCodigoArea: TNumericEdit
      Left = 98
      Top = 8
      Width = 69
      Height = 21
      Color = 16444382
      MaxLength = 10
      TabOrder = 0
    end
    object txt_RangoDesde: TNumericEdit
      Left = 98
      Top = 59
      Width = 100
      Height = 21
      MaxLength = 10
      TabOrder = 2
    end
    object txt_RangoHasta: TNumericEdit
      Left = 279
      Top = 61
      Width = 100
      Height = 21
      MaxLength = 10
      TabOrder = 3
    end
    object vcbTiposTelefono: TVariantComboBox
      Left = 98
      Top = 108
      Width = 145
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 5
      Items = <
        item
          Caption = 'Celular'
          Value = 'C'
        end
        item
          Caption = 'L'#237'nea Fija'
          Value = 'F'
        end>
    end
    object txt_CodigoPais: TNumericEdit
      Left = 98
      Top = 83
      Width = 69
      Height = 21
      MaxLength = 10
      TabOrder = 4
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 351
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 395
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object CodigosAreaTelefono: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'CodigosAreaTelefono'
    Left = 242
    Top = 110
  end
end
