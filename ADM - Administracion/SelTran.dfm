object FormSeleccionTransitos: TFormSeleccionTransitos
  Left = 242
  Top = 227
  BorderStyle = bsDialog
  Caption = 'y'
  ClientHeight = 143
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 24
    Width = 65
    Height = 13
    Caption = 'Fecha desde:'
  end
  object Label2: TLabel
    Left = 12
    Top = 52
    Width = 82
    Height = 13
    Caption = 'Puntos de Cobro:'
  end
  object Label3: TLabel
    Left = 12
    Top = 84
    Width = 50
    Height = 13
    Caption = 'Categor'#237'a:'
  end
  object Label4: TLabel
    Left = 216
    Top = 24
    Width = 29
    Height = 13
    Caption = 'hasta:'
  end
  object btn_Aceptar: TDPSButton
    Left = 205
    Top = 111
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 3
  end
  object btn_Cancelar: TDPSButton
    Left = 285
    Top = 111
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 5
  end
  object cb_PuntosDeCobro: TComboBox
    Left = 96
    Top = 48
    Width = 263
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object cb_Categorias: TComboBox
    Left = 96
    Top = 76
    Width = 263
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
  end
  object dt_FechaDesde: TDateEdit
    Left = 96
    Top = 20
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    OnChange = ValidarFecha
    OnClick = ValidarFecha
    AllowEmpty = False
    Date = -693594.000000000000000000
  end
  object dt_FechaHasta: TDateEdit
    Left = 269
    Top = 20
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 1
    OnChange = ValidarFecha
    OnClick = ValidarFecha
    AllowEmpty = False
    Date = -693594.000000000000000000
  end
  object qry_PuntosDeCobro: TQuery
    DatabaseName = 'BaseGestion'
    SQL.Strings = (
      'SELECT NumeroPuntoCobro, Descripcion  FROM PUNTOSCOBRO')
    Left = 28
    Top = 100
  end
  object Query2: TQuery
    Left = 72
    Top = 100
  end
end
