unit ABMConvenioClienteTipo;

interface

uses
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
   Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
   ToolWin, ComCtrls, UtilProc, Util;

resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';

type
  TfrmABMConvenioClienteTipo = class(TForm)
    Grid: TDBGrid;
    pnlSuperior: TPanel;
    Botonera: TToolBar;
    btnSalir: TToolButton;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    pnlControles: TPanel;
    lbl01: TLabel;
    Label1: TLabel;
    txtDescripcion: TEdit;
    txtCodigoTipoCliente: TEdit;
    pnlInferior: TPanel;
    btnGuardar: TButton;
    btnCancelar: TButton;
    spSelect: TADOStoredProc;
    spSelectCodigoTipoCliente: TIntegerField;
    spSelectDescripcion1: TStringField;
    spSelectUsuarioCreacion: TStringField;
    spSelectFechaCreacion: TDateTimeField;
    spSelectUsuarioModificacion: TStringField;
    spSelectFechaModificacion: TDateTimeField;
    dsGrid: TDataSource;
    Imagenes: TImageList;
    ImageList1: TImageList;
    spInsert: TADOStoredProc;
    spDelete: TADOStoredProc;
    spUpdate: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
    procedure spSelectCalcFields(DataSet: TDataSet);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spSelectAfterScroll(DataSet: TDataSet);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
  private
    { Private declarations }
    CantidadRegistros : integer;
    Posicion          : TBookmark;
    Accion            : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno           : Integer;
    MensajeError      : string;
  public
    { Public declarations }
  end;

var
  frmABMConvenioClienteTipo: TfrmABMConvenioClienteTipo;

implementation

{$R *.dfm}
function TfrmABMConvenioClienteTipo.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    Position := poMainFormCenter;
	Result := True;
end;

function TfrmABMConvenioClienteTipo.TraeRegistros;
begin
  try
    spSelect.Close;
    spSelect.Open;
    Retorno := spSelect.Parameters.ParamByName('@RETURN_VALUE').Value;
    if Retorno <> 0 then begin
      MensajeError := spSelect.Parameters.ParamByName('@ErrorDescription').Value;
      Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
    end;
    CantidadRegistros := spSelect.RecordCount;
    try
      spSelect.GotoBookmark(Posicion);
    except
    end;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
    end;
  end;
end;

procedure TfrmABMConvenioClienteTipo.btnAgregarClick(Sender: TObject);
begin
  HabilitaBotones('000000110');
//  chkHabilitado.Checked := True;
  with txtCodigoTipoCliente do begin
      Enabled := True;
      Text    := '';
      SetFocus;
  end;
  with txtDescripcion do begin
      Enabled := True;
      Text    := '';
  end;
  Accion := 1;
end;

procedure TfrmABMConvenioClienteTipo.btnCancelarClick(Sender: TObject);
begin
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
  try
    spSelect.GotoBookmark(Posicion);
  except
  end;
//  with chkHabilitado do begin
//    Enabled := False;
//    Checked := spSelect.FieldByName('Habilitado').AsBoolean
//  end;

  with txtDescripcion do begin
    Enabled := False;
    Text    := spSelect.FieldByName('Descripcion').AsString; 
  end;

  with txtCodigoTipoCliente do begin
    Enabled := False;
    Text    := spSelect.FieldByName('CodigoTipoCliente').AsString;
  end;

end;

procedure TfrmABMConvenioClienteTipo.btnEditarClick(Sender: TObject);
begin
  HabilitaBotones('000000110');
  //chkHabilitado.Enabled := True;
  with txtDescripcion do begin
      Enabled := True;
  end;
  with txtCodigoTipoCliente do begin
      Enabled := True;
      SetFocus;
  end;
  Accion := 3;
end;

procedure TfrmABMConvenioClienteTipo.btnEliminarClick(Sender: TObject);
begin
  HabilitaBotones('000000000');
  Accion := 2;
  try
    Posicion := spSelect.GetBookmark;
  except
  end;
  if Application.MessageBox(PChar('Est� seguro de eliminar este Tipo de Cliente? - ' + spSelect.FieldByName('Descripcion').AsString),'Pregunta',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    with spDelete do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoTipoCliente').Value  := spSelect.FieldByName('CodigoTipoCliente').AsInteger;
        Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
        try
          ExecProc;
          Retorno := spDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := spDelete.Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
    TraeRegistros;
    if CantidadRegistros > 0 then
      HabilitaBotones('111100001')
    else
      HabilitaBotones('110000001');
end;

procedure TfrmABMConvenioClienteTipo.btnGuardarClick(Sender: TObject);
begin
  if Length(txtDescripcion.Text) = 0 then begin
    Application.MessageBox('Ingrese la Descripci�n del Tipo de Cliente','Problema', MB_ICONEXCLAMATION);
    Exit;
  end;

  try
    StrToInt(txtCodigoTipoCliente.Text);
  except
    Application.MessageBox('Ingrese el C�digo del Tipo de Cliente','Problema', MB_ICONEXCLAMATION);
    Exit;
  end;


  try
    Posicion := spSelect.GetBookmark;
  except
  end;

  if Accion = 1 then begin
    with spInsert do begin
        Parameters.Refresh;
        Parameters.ParamByName('@Descripcion').Value        := txtDescripcion.Text;
        Parameters.ParamByName('@CodigoTipoCliente').Value := StrToInt(txtCodigoTipoCliente.Text);
        Parameters.ParamByName('@UsuarioCreacion').Value    := UsuarioSistema;
        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
  end;

  if Accion = 3 then begin
    with spUpdate do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoTipoCliente').Value     := spSelect.FieldByName('CodigoTipoCliente').Value;
        Parameters.ParamByName('@CodigoTipoClienteNew').Value  := StrToInt(txtCodigoTipoCliente.Text);
        Parameters.ParamByName('@Descripcion').Value            := txtDescripcion.Text;
//        Parameters.ParamByName('@Habilitado').Value             := chkHabilitado.Checked;
        Parameters.ParamByName('@UsuarioModificacion').Value    := UsuarioSistema;
        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
            MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
            MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
          end;
        except
          on E : Exception do begin
            MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
          end;
        end;
    end;
  end;

  txtDescripcion.Enabled := False;
  txtCodigoTipoCliente.Enabled := False;
//  chkHabilitado.Enabled := False;
  TraeRegistros;
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMConvenioClienteTipo.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMConvenioClienteTipo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Application.MessageBox('Confirma que desea salir?','Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    Action := caFree
  else
    Action := caNone;
end;

procedure TfrmABMConvenioClienteTipo.FormCreate(Sender: TObject);
begin
  TraeRegistros;
  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMConvenioClienteTipo.HabilitaBotones(Botones : string);
begin
  btnSalir.Enabled      := Botones[1] = '1';
  btnAgregar.Enabled    := Botones[2] = '1';
  btnEliminar.Enabled   := Botones[3] = '1';
  btnEditar.Enabled     := Botones[4] = '1';
  btnImprimir.Enabled   := Botones[5] = '1';
  btnBuscar.Enabled     := Botones[6] = '1';
  btnGuardar.Enabled    := Botones[7] = '1';
  btnCancelar.Enabled   := Botones[8] = '1';
  Grid.Enabled          := Botones[9] = '1';
end;

procedure TfrmABMConvenioClienteTipo.spSelectAfterScroll(DataSet: TDataSet);
begin
  txtDescripcion.Text    := DataSet.FieldByName('Descripcion').AsString;
  txtCodigoTipoCliente.Text := DataSet.FieldByName('CodigoTipoCliente').AsString;
  //chkHabilitado.Checked := DataSet.FieldByName('Habilitado').AsBoolean;
end;

procedure TfrmABMConvenioClienteTipo.spSelectCalcFields(DataSet: TDataSet);
begin
//  if DataSet.FieldByName('Habilitado').AsBoolean then
//    DataSet.FieldByName('Habilitado2').AsString := 'S�'
//  else
//    DataSet.FieldByName('Habilitado2').AsString := 'No'
end;

end.
