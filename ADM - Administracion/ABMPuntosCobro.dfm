object frmABMPuntosCobro: TfrmABMPuntosCobro
  Left = 0
  Top = 0
  Caption = 'ABM Domain'
  ClientHeight = 435
  ClientWidth = 884
  Color = clBtnFace
  Constraints.MaxHeight = 475
  Constraints.MaxWidth = 940
  Constraints.MinHeight = 440
  Constraints.MinWidth = 900
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TDBGrid
    Left = 0
    Top = 41
    Width = 884
    Height = 201
    Align = alClient
    DataSource = dsGrid
    Enabled = False
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NumeroPuntoCobro'
        Title.Caption = 'N'#250'mero Punto Cobro'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Caption = 'Descripci'#243'n'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UsuarioCreacion'
        Title.Caption = 'User Creador'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FechaHoraCreacion'
        Title.Caption = 'Fecha Creaci'#243'n'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UsuarioModificacion'
        Title.Caption = 'User Modificador'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FechaHoraModificacion'
        Title.Caption = 'Fecha Modificaci'#243'n'
        Width = 110
        Visible = True
      end>
  end
  object pnlSuperior: TPanel
    Left = 0
    Top = 0
    Width = 884
    Height = 41
    Align = alTop
    TabOrder = 1
    object Botonera: TToolBar
      Left = 9
      Top = 6
      Width = 176
      Height = 29
      Align = alNone
      ButtonHeight = 25
      Caption = 'Botonera'
      Images = ImageList1
      TabOrder = 0
      object btnSalir: TToolButton
        Left = 0
        Top = 0
        Hint = 'Salir'
        Caption = 'btnSalir'
        ImageIndex = 0
        ParentShowHint = False
        ShowHint = True
        OnClick = btnSalirClick
      end
      object ToolButton2: TToolButton
        Left = 23
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 1
        Style = tbsSeparator
        Visible = False
      end
      object btnAgregar: TToolButton
        Left = 31
        Top = 0
        Hint = 'Agregar tipo de convenio'
        Caption = 'btnAgregar'
        ImageIndex = 1
        ParentShowHint = False
        ShowHint = True
        OnClick = btnAgregarClick
      end
      object btnEliminar: TToolButton
        Left = 54
        Top = 0
        Hint = 'Eliminar/Deshabilitar tipo de convenio'
        Caption = 'btnEliminar'
        ImageIndex = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnEliminarClick
      end
      object btnEditar: TToolButton
        Left = 77
        Top = 0
        Hint = 'Modificar tipo de convenio'
        Caption = 'btnEditar'
        ImageIndex = 3
        ParentShowHint = False
        ShowHint = True
        OnClick = btnEditarClick
      end
      object ToolButton6: TToolButton
        Left = 100
        Top = 0
        Width = 8
        Caption = 'ToolButton6'
        ImageIndex = 4
        Style = tbsSeparator
        Visible = False
      end
      object btnImprimir: TToolButton
        Left = 108
        Top = 0
        Caption = 'btnImprimir'
        Enabled = False
        ImageIndex = 4
      end
      object ToolButton8: TToolButton
        Left = 131
        Top = 0
        Width = 8
        Caption = 'ToolButton8'
        ImageIndex = 5
        Style = tbsSeparator
        Visible = False
      end
      object btnBuscar: TToolButton
        Left = 139
        Top = 0
        Caption = 'btnBuscar'
        Enabled = False
        ImageIndex = 5
      end
    end
  end
  object pnlControles: TPanel
    Left = 0
    Top = 242
    Width = 884
    Height = 156
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    object pg01: TPageControl
      Left = 1
      Top = 1
      Width = 882
      Height = 154
      ActivePage = tabGeneral
      Align = alClient
      MultiLine = True
      TabOrder = 0
      object tabGeneral: TTabSheet
        Caption = 'General'
        ImageIndex = 3
        object lbl01: TLabel
          Left = 50
          Top = 68
          Width = 115
          Height = 13
          Caption = 'N'#250'mero Punto de Cobro'
        end
        object lbl02: TLabel
          Left = 10
          Top = 41
          Width = 155
          Height = 13
          Caption = 'Controlador Punto Cobro / Plaza'
        end
        object lbl03: TLabel
          Left = 111
          Top = 95
          Width = 54
          Height = 13
          Caption = 'Descripci'#243'n'
        end
        object lbl04: TLabel
          Left = 609
          Top = 14
          Width = 34
          Height = 13
          Caption = 'Eje Vial'
        end
        object lbl05: TLabel
          Left = 607
          Top = 41
          Width = 36
          Height = 13
          Caption = 'Sentido'
        end
        object lbl06: TLabel
          Left = 258
          Top = 68
          Width = 47
          Height = 13
          Caption = 'TSID / V'#237'a'
        end
        object lbl6: TLabel
          Left = 82
          Top = 14
          Width = 83
          Height = 13
          Caption = 'Tipo Punto Cobro'
        end
        object lbl4: TLabel
          Left = 562
          Top = 68
          Width = 81
          Height = 13
          Caption = 'Esquema Horario'
        end
        object lbl8: TLabel
          Left = 593
          Top = 95
          Width = 50
          Height = 13
          Caption = 'Proveedor'
        end
        object edNumeroPunto: TDBEdit
          Left = 171
          Top = 65
          Width = 50
          Height = 21
          DataField = 'NumeroPuntoCobro'
          DataSource = dsGrid
          TabOrder = 2
        end
        object edDomainTSMCID: TDBLookupComboBox
          Left = 171
          Top = 38
          Width = 361
          Height = 21
          DataField = 'TSMCKEY'
          DataSource = dsGrid
          KeyField = 'llave'
          ListField = 'Descripcion'
          ListSource = dsTSMC
          TabOrder = 1
        end
        object edDescripcion: TDBEdit
          Left = 171
          Top = 92
          Width = 361
          Height = 21
          DataField = 'Descripcion'
          DataSource = dsGrid
          TabOrder = 4
        end
        object edEjeVial: TDBLookupComboBox
          Left = 649
          Top = 11
          Width = 190
          Height = 21
          DataField = 'CodigoEjeVial'
          DataSource = dsGrid
          KeyField = 'CodigoEjeVial'
          ListField = 'Descripcion'
          ListSource = dsEjesViales
          TabOrder = 5
        end
        object edSentido: TDBLookupComboBox
          Left = 649
          Top = 38
          Width = 190
          Height = 21
          DataField = 'Sentido'
          DataSource = dsGrid
          KeyField = 'Sentido'
          ListField = 'Descripcion'
          ListSource = dsSentido
          TabOrder = 6
        end
        object edTSMCID: TDBEdit
          Left = 311
          Top = 65
          Width = 50
          Height = 21
          DataField = 'TSID'
          DataSource = dsGrid
          TabOrder = 3
        end
        object edCodigoTipoPuntoCobro: TDBLookupComboBox
          Left = 171
          Top = 11
          Width = 190
          Height = 21
          DataField = 'CodigoTipoPuntoCobro'
          DataSource = dsGrid
          KeyField = 'CodigoTipoPuntoCobro'
          ListField = 'Descripcion'
          ListSource = dsTipos
          TabOrder = 0
        end
        object edCodigoEsquema: TDBLookupComboBox
          Left = 649
          Top = 65
          Width = 190
          Height = 21
          DataField = 'CodigoEsquema'
          DataSource = dsGrid
          KeyField = 'CodigoEsquema'
          ListField = 'Descripcion'
          ListSource = dsEsquemas
          TabOrder = 7
        end
        object edProveedor: TDBLookupComboBox
          Left = 649
          Top = 92
          Width = 190
          Height = 21
          DataField = 'Proveedor'
          DataSource = dsGrid
          KeyField = 'ID'
          ListField = 'Nombre'
          ListSource = dsProveedores
          TabOrder = 8
        end
      end
      object tsCarriles: TTabSheet
        Caption = 'Carriles'
        ImageIndex = 2
        object lbl14: TLabel
          Left = 19
          Top = 22
          Width = 97
          Height = 13
          Caption = 'Cantidad de Carriles'
        end
        object bvl1: TBevel
          Left = 288
          Top = 3
          Width = 257
          Height = 118
        end
        object lbl1: TLabel
          Left = 343
          Top = 16
          Width = 30
          Height = 13
          Caption = 'Orden'
        end
        object lbl2: TLabel
          Left = 302
          Top = 70
          Width = 71
          Height = 13
          Caption = 'Posici'#243'n Desde'
        end
        object lbl3: TLabel
          Left = 304
          Top = 97
          Width = 69
          Height = 13
          Caption = 'Posici'#243'n Hasta'
        end
        object lbl5: TLabel
          Left = 319
          Top = 43
          Width = 54
          Height = 13
          Caption = 'Descripci'#243'n'
        end
        object dbedtCantidadCarriles: TDBEdit
          Left = 122
          Top = 19
          Width = 50
          Height = 21
          DataField = 'CantidadCarriles'
          DataSource = dsGrid
          TabOrder = 0
        end
        object dbgrdBandasHorarias: TDBGrid
          Left = 551
          Top = 0
          Width = 323
          Height = 126
          Align = alRight
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataSource = dsCarriles
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 7
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Orden'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descripcion'
              Title.Caption = 'Descripci'#243'n'
              Width = 123
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PosicionDesde'
              Title.Caption = 'Pos. Desde'
              Width = 61
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PosicionHasta'
              Title.Caption = 'Pos. Hasta'
              Width = 60
              Visible = True
            end>
        end
        object edtOrden: TEdit
          Left = 379
          Top = 13
          Width = 56
          Height = 21
          Color = clInactiveCaption
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object txtPosicionDesde: TNumericEdit
          Left = 379
          Top = 67
          Width = 56
          Height = 21
          TabOrder = 4
          BlankWhenZero = False
        end
        object txtPosicionHasta: TNumericEdit
          Left = 379
          Top = 94
          Width = 56
          Height = 21
          TabOrder = 5
          BlankWhenZero = False
        end
        object btnGenerarCarriles: TButton
          Left = 186
          Top = 17
          Width = 75
          Height = 25
          Caption = 'Generar'
          TabOrder = 1
          OnClick = btnGenerarCarrilesClick
        end
        object btnAplicarCarril: TButton
          Left = 461
          Top = 92
          Width = 75
          Height = 25
          Caption = 'Aplicar'
          TabOrder = 6
          OnClick = btnAplicarCarrilClick
        end
        object edtDescripcionCarril: TEdit
          Left = 379
          Top = 40
          Width = 157
          Height = 21
          TabOrder = 3
        end
      end
      object tabPag02: TTabSheet
        Caption = 'MLFF'
        object lbl07: TLabel
          Left = 65
          Top = 15
          Width = 70
          Height = 13
          Caption = 'Prefijo Imagen'
        end
        object lbl08: TLabel
          Left = 20
          Top = 41
          Width = 117
          Height = 13
          Caption = 'Punto de Cobro Anterior'
        end
        object lbl12: TLabel
          Left = 559
          Top = 41
          Width = 76
          Height = 13
          Caption = 'C'#243'digo Juzgado'
        end
        object lbl13: TLabel
          Left = 616
          Top = 67
          Width = 19
          Height = 13
          Caption = 'Kms'
        end
        object lbl7: TLabel
          Left = 516
          Top = 14
          Width = 119
          Height = 13
          Caption = 'C'#243'digo PC Concesionaria'
        end
        object edPrefijoImagen: TDBEdit
          Left = 143
          Top = 12
          Width = 161
          Height = 21
          DataField = 'PrefijoImagen'
          DataSource = dsGrid
          TabOrder = 0
        end
        object edPuntoCobroAnterior: TDBLookupComboBox
          Left = 143
          Top = 38
          Width = 282
          Height = 21
          DataField = 'PuntoCobroAnterior'
          DataSource = dsGrid
          KeyField = 'NumeroPuntoCobro'
          ListField = 'Descripcion'
          ListSource = dsPuntoCobroAnt
          TabOrder = 1
        end
        object edUltimoPortico: TDBCheckBox
          Left = 27
          Top = 66
          Width = 128
          Height = 17
          Alignment = taLeftJustify
          Caption = #218'ltimo P'#243'rtico Sentido'
          DataField = 'UltimoPorticoSentido'
          DataSource = dsGrid
          TabOrder = 2
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object edCodigoJuzgado: TDBEdit
          Left = 641
          Top = 38
          Width = 50
          Height = 21
          DataField = 'CodigoJuzgado'
          DataSource = dsGrid
          TabOrder = 4
        end
        object edKms: TDBEdit
          Left = 641
          Top = 64
          Width = 50
          Height = 21
          DataField = 'Kms'
          DataSource = dsGrid
          TabOrder = 5
        end
        object edCodigoPCConcesionaria: TDBEdit
          Left = 641
          Top = 11
          Width = 50
          Height = 21
          DataField = 'CodigoPCConcesionaria'
          DataSource = dsGrid
          TabOrder = 3
        end
      end
    end
  end
  object pnlInferior: TPanel
    Left = 0
    Top = 398
    Width = 884
    Height = 37
    Align = alBottom
    TabOrder = 3
    object btnGuardar: TButton
      Left = 719
      Top = 5
      Width = 75
      Height = 25
      Caption = 'Guardar'
      TabOrder = 0
      OnClick = btnGuardarClick
    end
    object btnCancelar: TButton
      Left = 800
      Top = 5
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object procSelect: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = 'ADM_PuntosCobro_SELECT;1'
    Parameters = <>
    Left = 808
    Top = 184
    object procSelectNumeroPuntoCobro: TWordField
      FieldName = 'NumeroPuntoCobro'
    end
    object procSelectDomainID: TSmallintField
      FieldName = 'DomainID'
    end
    object procSelectTSMCID: TWordField
      FieldName = 'TSMCID'
    end
    object procSelectDescripcion: TStringField
      FieldName = 'Descripcion'
      FixedChar = True
      Size = 60
    end
    object procSelectCodigoEjeVial: TWordField
      FieldName = 'CodigoEjeVial'
    end
    object procSelectCodigoEsquema: TIntegerField
      FieldName = 'CodigoEsquema'
    end
    object procSelectSentido: TStringField
      FieldName = 'Sentido'
      FixedChar = True
      Size = 1
    end
    object procSelectTSID: TWordField
      FieldName = 'TSID'
    end
    object procSelectPrefijoImagen: TStringField
      FieldName = 'PrefijoImagen'
      Size = 50
    end
    object procSelectPuntoCobroAnterior: TWordField
      FieldName = 'PuntoCobroAnterior'
    end
    object procSelectCantidadCarriles: TWordField
      FieldName = 'CantidadCarriles'
    end
    object procSelectUltimoPorticoSentido: TBooleanField
      FieldName = 'UltimoPorticoSentido'
    end
    object wrdfldSelectCodigoPCConcesionaria: TWordField
      FieldName = 'CodigoPCConcesionaria'
    end
    object procSelectCodigoJuzgado: TIntegerField
      FieldName = 'CodigoJuzgado'
    end
    object procSelectKms: TBCDField
      FieldName = 'Kms'
      Precision = 8
    end
    object procSelectUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
    end
    object procSelectFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object procSelectUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
    end
    object procSelectFechaHoraModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
    object procSelectEsMLFF: TBooleanField
      FieldName = 'EsMLFF'
    end
    object cdsPDSelectClaseCategoria: TIntegerField
      FieldName = 'IdCategoriaClase'
    end
    object smlntfldSelectCodigoTipoPuntoCobro: TSmallintField
      FieldName = 'CodigoTipoPuntoCobro'
    end
    object procSelectProveedor: TIntegerField
      FieldName = 'Proveedor'
    end
  end
  object dsGrid: TDataSource
    DataSet = CDS
    Left = 704
    Top = 88
  end
  object ImageList1: TImageList
    Left = 832
    Top = 72
    Bitmap = {
      494C010106000800E00010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF000000FF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000008080
      800000000000000000000000000000000000FFFF0000FFFF0000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000008080800000000000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000C0C0C000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFF0000FFFF0000FFFF0000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      800000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      800000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000000000000000000000FF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      800000008000000080000000800000008000000080000000800000FFFF000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF00000000000000000000FFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      80000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF00000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      80000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF00000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF008001FFFF000000000000FFFD00000000
      0000FFF8000000000000FFF1000000000000F023000000000000E78700000000
      0000CF0F0000000000009FA700000000E007BFD700000000E007BFF700000000
      E007AFF700000000E007AFF700000000E00F87E700000000E01FC1CF00000000
      E03FE79F00000000E07FF03F00000000C007FFFFFFFFFC00C007E7F8FFF8FC00
      C007E7F8FFF82000C00781FF81FF0000C00781FC81FC0000C007E7FCFFFC0000
      C007E7FFFFFF0000C007FFFCFFFC0000C007FEFCF7FC0000C007FE7FE7FF0000
      C00780138013E000C00780138013F800C007FE7FE7FFF000C007FEF8F7F8E001
      C007FFF8FFF8C403C007FFFFFFFFEC0700000000000000000000000000000000
      000000000000}
  end
  object procInsert: TADOStoredProc
    ProcedureName = 'ADM_PuntosCobro_INSERT'
    Parameters = <>
    Left = 808
    Top = 136
  end
  object procDelete: TADOStoredProc
    ProcedureName = 'ADM_PuntosCobro_DELETE'
    Parameters = <>
    Left = 752
    Top = 184
  end
  object procUpdate: TADOStoredProc
    ProcedureName = 'ADM_PuntosCobro_UPDATE'
    Parameters = <>
    Left = 752
    Top = 136
  end
  object CDS: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroPuntoCobro'
        DataType = ftSmallint
      end
      item
        Name = 'DomainID'
        DataType = ftSmallint
      end
      item
        Name = 'TSMCID'
        DataType = ftSmallint
      end
      item
        Name = 'Descripcion'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CodigoEsquema'
        DataType = ftInteger
      end
      item
        Name = 'Sentido'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TSID'
        DataType = ftSmallint
      end
      item
        Name = 'PrefijoImagen'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PuntoCobroAnterior'
        DataType = ftSmallint
      end
      item
        Name = 'CantidadCarriles'
        DataType = ftSmallint
      end
      item
        Name = 'UltimoPorticoSentido'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoPCConcesionaria'
        DataType = ftInteger
      end
      item
        Name = 'CodigoJuzgado'
        DataType = ftInteger
      end
      item
        Name = 'Kms'
        DataType = ftBCD
        Precision = 8
        Size = 4
      end
      item
        Name = 'UsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioModificacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHoraModificacion'
        DataType = ftDateTime
      end
      item
        Name = 'EsMLFF'
        DataType = ftBoolean
      end
      item
        Name = 'IdCategoriaClase'
        DataType = ftInteger
      end
      item
        Name = 'TSMCKEY'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoEjeVial'
        DataType = ftWord
      end
      item
        Name = 'CodigoTipoPuntoCobro'
        DataType = ftInteger
      end
      item
        Name = 'Proveedor'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    ReadOnly = True
    StoreDefs = True
    AfterScroll = CDSAfterScroll
    Left = 752
    Top = 88
    Data = {
      8D0200009619E0BD0100000018000000180000000000030000008D02104E756D
      65726F50756E746F436F62726F020001000000000008446F6D61696E49440200
      0100000000000654534D43494402000100000000000B4465736372697063696F
      6E0100490020000100055749445448020002003C000D436F6469676F45737175
      656D6104000100000000000753656E7469646F01004900200001000557494454
      48020002000100045453494402000100000000000D50726566696A6F496D6167
      656E01004900000001000557494454480200020032001250756E746F436F6272
      6F416E746572696F7202000100000000001043616E746964616443617272696C
      6573020001000000000014556C74696D6F506F727469636F53656E7469646F02
      0003000000000015436F6469676F5043436F6E636573696F6E61726961040001
      00000000000D436F6469676F4A757A6761646F0400010000000000034B6D7306
      0005000000020008444543494D414C5302000200040005574944544802000200
      08000F5573756172696F4372656163696F6E0100490000000100055749445448
      020002001400114665636861486F72614372656163696F6E0800080000000000
      135573756172696F4D6F64696669636163696F6E010049000000010005574944
      5448020002001400154665636861486F72614D6F64696669636163696F6E0800
      0800000000000645734D4C4646020003000000000010496443617465676F7269
      61436C61736504000100000000000754534D434B455901004900020001000557
      49445448020002000A000D436F6469676F456A655669616C0200020000000000
      14436F6469676F5469706F50756E746F436F62726F0400010000000000095072
      6F766565646F7204000100000000000000}
  end
  object qryTSMC: TADOQuery
    CursorType = ctStatic
    DataSource = dsGrid
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      
        #9'RIGHT(REPLICATE('#39'0'#39',5) + CAST(a.DomainID as varchar(10)),5) + R' +
        'IGHT(REPLICATE('#39'0'#39',5) + CAST(a.TSMCID as varchar(10)), 5) as lla' +
        've, '
      #9'a.TSMCID, '
      #9'a.DomainID, '
      
        #9'LTRIM(RTRIM(a.Descripcion)) + '#39' - Dom: '#39' + b.Descripcion + ISNU' +
        'LL('#39' - Clase: '#39' + C.Descripcion, '#39#39') AS Descripcion'
      #9',*'
      #9'FROM TSMCs A WITH (NOLOCK)'
      #9#9'INNER JOIN Domain B WITH (NOLOCK)'
      #9#9#9'ON A.DomainID = B.DomainID'
      #9#9'LEFT JOIN CategoriasClases C WITH (NOLOCK)'
      #9#9#9'ON'#9'A.ID_CategoriasClases = C.ID_CategoriasClases'
      '  ORDER BY LLAVE')
    Left = 451
    Top = 88
  end
  object dsTSMC: TDataSource
    DataSet = qryTSMC
    Left = 451
    Top = 136
  end
  object qryPBL: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select CodigoVersionPBL,Descripcion from BO_OBO..VersionesPBL wi' +
        'th (nolock)')
    Left = 491
    Top = 88
  end
  object dsPBL: TDataSource
    AutoEdit = False
    DataSet = qryPBL
    Left = 491
    Top = 136
  end
  object qryEjesViales: TADOQuery
    CursorType = ctStatic
    DataSource = dsGrid
    Parameters = <>
    SQL.Strings = (
      
        'SELECT CodigoEjeVial, Descripcion FROM BO_OBO..EjesViales WITH (' +
        'NOLOCK) ')
    Left = 547
    Top = 88
  end
  object dsEjesViales: TDataSource
    AutoEdit = False
    DataSet = qryEjesViales
    Left = 547
    Top = 136
  end
  object qrySentido: TADOQuery
    CursorType = ctStatic
    DataSource = dsGrid
    Parameters = <>
    SQL.Strings = (
      'SELECT Sentido, Descripcion FROM BO_OBO..Sentidos WITH (NOLOCK) ')
    Left = 395
    Top = 88
  end
  object dsSentido: TDataSource
    DataSet = qrySentido
    Left = 395
    Top = 136
  end
  object qryPuntoCobroAnt: TADOQuery
    CursorType = ctStatic
    DataSource = dsGrid
    Parameters = <>
    SQL.Strings = (
      
        'SELECT NumeroPuntoCobro, Descripcion FROM BO_OBO..PuntosCobro WI' +
        'TH (NOLOCK)')
    Left = 627
    Top = 88
  end
  object dsPuntoCobroAnt: TDataSource
    AutoEdit = False
    DataSet = qryPuntoCobroAnt
    Left = 627
    Top = 136
  end
  object cdsCarriles: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dtstprCarriles'
    AfterScroll = cdsCarrilesAfterScroll
    Left = 16
    Top = 128
  end
  object dtstprCarriles: TDataSetProvider
    DataSet = spADM_Carril_SELECT
    Constraints = False
    Left = 72
    Top = 96
  end
  object spADM_Carril_SELECT: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_Carril_SELECT'
    Parameters = <>
    Left = 16
    Top = 80
  end
  object qryEsquemas: TADOQuery
    Connection = DMConnections.BaseBO_Rating
    CursorType = ctStatic
    DataSource = dsGrid
    Parameters = <>
    SQL.Strings = (
      'SELECT CodigoEsquema, Descripcion '
      'FROM BO_Rating..Esquemas WITH (NOLOCK)'
      'WHERE Completo = 1')
    Left = 331
    Top = 88
  end
  object dsEsquemas: TDataSource
    DataSet = qryEsquemas
    Left = 331
    Top = 136
  end
  object dsCarriles: TDataSource
    DataSet = cdsCarriles
    Left = 72
    Top = 144
  end
  object dsTipos: TDataSource
    DataSet = qryTipos
    OnDataChange = dsTiposDataChange
    Left = 267
    Top = 136
  end
  object qryTipos: TADOQuery
    Connection = DMConnections.BaseBO_Master
    CursorType = ctStatic
    DataSource = dsGrid
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      #9'CodigoTipoPuntoCobro'
      #9',Descripcion'
      'FROM PuntosCobroTipos (NOLOCK)')
    Left = 267
    Top = 88
  end
  object qryProveedores: TADOQuery
    Connection = DMConnections.BaseBO_Master
    CursorType = ctStatic
    DataSource = dsGrid
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      #9'ID'
      #9',Nombre'
      'FROM PuntosCobroProveedor (NOLOCK)')
    Left = 200
    Top = 88
  end
  object dsProveedores: TDataSource
    DataSet = qryProveedores
    Left = 200
    Top = 136
  end
end
