object FormFactoresCategoria: TFormFactoresCategoria
  Left = 195
  Top = 144
  Caption = 'Factores por Categoria de Vehiculos'
  ClientHeight = 516
  ClientWidth = 974
  Color = clBtnFace
  Constraints.MaxWidth = 990
  Constraints.MinHeight = 480
  Constraints.MinWidth = 670
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlEdicion: TPanel
    Left = 0
    Top = 352
    Width = 974
    Height = 164
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 0
    object lblDescripcion: TLabel
      Left = 13
      Top = 46
      Width = 72
      Height = 13
      Caption = 'Descripci'#243'n:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblIDCategoriaUnico: TLabel
      Left = 13
      Top = 19
      Width = 192
      Height = 13
      Caption = 'ID Categor'#237'a '#218'nica de Veh'#237'culo: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblIDCategoria: TLabel
      Left = 13
      Top = 73
      Width = 133
      Height = 13
      Caption = 'ID Categor'#237'a Veh'#237'culo:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCategoriaPortico: TLabel
      Left = 13
      Top = 100
      Width = 105
      Height = 13
      Caption = 'Categor'#237'a P'#243'rtico:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFactorMultiplicacion: TLabel
      Left = 13
      Top = 127
      Width = 141
      Height = 13
      Caption = 'Factor de Multiplicaci'#243'n:'
      FocusControl = txtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtDescripcion: TEdit
      Left = 211
      Top = 42
      Width = 335
      Height = 21
      Color = 16444382
      Enabled = False
      MaxLength = 60
      TabOrder = 1
    end
    object nbBotones: TNotebook
      Left = 752
      Top = 110
      Width = 209
      Height = 43
      TabOrder = 5
      object TPage
        Left = 0
        Top = 0
        Caption = 'PagSalir'
        object btnSalir: TButton
          Left = 128
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'PagModi'
        object btnAceptar: TButton
          Left = 47
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Aceptar'
          TabOrder = 0
          OnClick = btnAceptarClick
        end
        object btnCancelar: TButton
          Left = 128
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
      end
    end
    object txtIDCategoriaUnico: TEdit
      Left = 211
      Top = 15
      Width = 134
      Height = 21
      Color = 16444382
      Enabled = False
      MaxLength = 60
      TabOrder = 0
    end
    object txtIdCategoria: TEdit
      Left = 211
      Top = 69
      Width = 134
      Height = 21
      Color = 16444382
      Enabled = False
      MaxLength = 60
      TabOrder = 2
    end
    object txtCategoriaPortico: TEdit
      Left = 211
      Top = 96
      Width = 335
      Height = 21
      Color = 16444382
      Enabled = False
      MaxLength = 60
      TabOrder = 3
    end
    object txtFactorMultiplicacion: TEdit
      Left = 211
      Top = 123
      Width = 335
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 4
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 0
    Width = 974
    Height = 39
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object lblShow_Inserta_Modifica: TLabel
      Left = 381
      Top = 9
      Width = 212
      Height = 24
      Caption = 'lblShow_Inserta_Modifica'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clOlive
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object btnEditar: TSpeedButton
      Left = 13
      Top = 11
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      OnClick = btnEditarClick
    end
  end
  object dblFactoresCategoria: TDBListEx
    Left = 0
    Top = 39
    Width = 974
    Height = 313
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        MinWidth = 150
        MaxWidth = 150
        Header.Caption = 'ID Categor'#237'a Unica de Veh'#237'culos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Categoria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 350
        MinWidth = 350
        MaxWidth = 350
        Header.Caption = 'Descripci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'ID Categor'#237'a de Veh'#237'culos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Categoria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Categor'#237'a P'#243'rtico'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Categoria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Factor de Multiplicaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Categoria'
      end>
    Constraints.MaxWidth = 974
    DataSource = dsFactores
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
    OnClick = dblFactoresCategoriaClick
  end
  object dsFactores: TDataSource
    Left = 784
    Top = 120
  end
end
