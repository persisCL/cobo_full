unit ABMCategoriasModulos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB,  DMConnection, Util,
  DPSControls;

type
  TFormCategoriasModulos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    CategoriasModulo: TADOTable;
    txt_CodigoCategoriaModulo: TNumericEdit;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    EsConsulta: boolean;
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormCategoriasModulos: TFormCategoriasModulos;

implementation
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos de la Categor�a.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Categor�a';
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar esta Categor�a?';
    MSG_DELETE_ERROR		= 'No se puede eliminar esta Categor�a porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION 		= 'Eliminar Categor�a';

{$R *.DFM}

procedure TFormCategoriasModulos.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txt_CodigoCategoriaModulo.Enabled	:= True;
end;

function TFormCategoriasModulos.Inicializa: boolean;
Var
	S: TSize;
begin
    //Como no es un ABM, lo dejo as�... el dia de ma�ana si se quiere usar
    //como ABM ver qu� se hace con esta variable y listo (por ahora es privada, puede ser parametro)
    EsConsulta := true;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([CategoriasModulo]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
    if EsConsulta then begin
        txt_Descripcion.Color := txt_CodigoCategoriaModulo.Color;
        AbmToolbar1.Habilitados := [btSalir, btBuscar];
    end
    else
        AbmToolbar1.Habilitados := [btSalir, btBuscar, btAlta, btBaja, btModi];
end;

procedure TFormCategoriasModulos.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormCategoriasModulos.DBList1Click(Sender: TObject);
begin
	with CategoriasModulo do begin
		txt_CodigoCategoriaModulo.Value	:= FieldByName('CodigoCategoria').AsInteger;
		txt_Descripcion.text			:= FieldByName('Descripcion').AsString;
	end;
end;

procedure TFormCategoriasModulos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormCategoriasModulos.DBList1Edit(Sender: TObject);
begin
    if EsConsulta then begin
        //Si no se agrega esto, deshabilita la puertita de salir y la lupa de buscar
        DBList1.Estado := Normal;
    end
    else begin
	    HabilitarCampos;
	    dblist1.Estado := modi;
    end;
end;

procedure TFormCategoriasModulos.DBList1Insert(Sender: TObject);
begin
    if EsConsulta then begin
        //Si no se agrega esto, deshabilita la puertita de salir y la lupa de buscar
        DBList1.Estado := Normal;
    end
    else begin
        Limpiar_Campos;
        HabilitarCampos;
        dblist1.Estado     := Alta;
    end;
end;

procedure TFormCategoriasModulos.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormCategoriasModulos.Limpiar_Campos;
begin
	txt_CodigoCategoriaModulo.Clear;
	txt_Descripcion.Clear;
end;

procedure TFormCategoriasModulos.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormCategoriasModulos.DBList1Delete(Sender: TObject);
begin
    if EsConsulta then begin
        //Si no se agrega esto, deshabilita la puertita de salir y la lupa de buscar
        DBList1.Estado := Normal;
        exit;
    end;
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			CategoriasModulo.Delete;
		Except
			On E: Exception do begin
				CategoriasModulo.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;

procedure TFormCategoriasModulos.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With CategoriasModulo do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                txt_CodigoCategoriaModulo.value := QueryGetValueInt(DMConnections.BaseCAC,
                	'Select ISNULL(MAX(CodigoCategoria), 0) + 1 FROM CategoriasModulo');
                FieldByName('CodigoCategoria').value	:= txt_codigoCategoriaModulo.value;
			end else Edit;
			FieldByName('Descripcion').AsString			:= txt_Descripcion.text;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormCategoriasModulos.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormCategoriasModulos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormCategoriasModulos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormCategoriasModulos.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_CodigoCategoriaModulo.Enabled := False;
    txt_Descripcion.SetFocus;
end;

end.
