{-----------------------------------------------------------------------------
 Unit Name: ABMTasasInteres
 Author:    ndonadio
 Purpose:   ABM de Tasas de Interes Anual
 History:   versi�n anterior de lgisuk.

    Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

 Rev.: SS_963_PDO_20110428
 Descripci�n: Se cambia el t�tulo del ABM y la etiqueta de la tasa para indicar
    que es NO Reajustable.


Firma       : SS_1147_MCA_20150422
Descripcion : se activa boton salir de la barra de menuu
-----------------------------------------------------------------------------}

unit ABMTasasInteres;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilDB, DMConnection, DB, Validate, DateEdit, StdCtrls,
  DmiCtrls, ExtCtrls, DbList, Abm_obj, ADODB, PeaProcs,Util, UtilProc;

  
type
  TFAbmTasasInteres = class(TForm)
    AbmToolbar: TAbmToolbar;
    AbmList: TAbmList;
    pnlEditing: TPanel;
    LPorcentaje: TLabel;
    LfechadeInicio: TLabel;
    LTasadeInteresAnual: TLabel;
    neTasaInteres: TNumericEdit;
    deFechaInicio: TDateEdit;
    tblTasasInteres: TADOTable;
    tblTasasInteresFechaInicio: TDateTimeField;
    tblTasasInteresTasaInteresAnual: TBCDField;
    pnlButtons: TPanel;
    Notebook: TNotebook;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
   procedure btnSalirClick(Sender: TObject);
    procedure abmListClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure abmListInsert(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure abmListDelete(Sender: TObject);
    procedure abmListEdit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure abmListDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure abmListRefresh(Sender: TObject);
    procedure AbmToolbarClose(Sender: TObject);
  private
    { Private declarations }
    FFechaInteresVigente: TDateTime;
    FInicializando: Boolean;
    procedure CambiarAModo(Modo: integer);
    function ObtenerFechaInteresVigente:TDateTime;
  public
    { Public declarations }
    function inicializar:boolean;
  end;

const
    CONSULTA  = 0; // indica que la ventana esta en modo normal
    EDICION = 1; // indica que la ventana esta en modo edicion

var
  FFAbmTasasInteres: TFAbmTasasInteres;

implementation

{$R *.dfm}

function TFAbmTasasInteres.Inicializar;
resourcestring
    ERROR_MSG = 'Error de Inicializacion';
Var
	S: TSize;
begin
    FInicializando := True;
try
    FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    CenterForm(Self);
    try
        FFechaInteresVigente :=  ObtenerFechaInteresVigente;
        tblTasasInteres.Open;
        // Cambiamos a modo consulta a mano
        // para evitar la llamada al reload y el onClick (que estaria dehabilitado)
        // y se lo llama mas abajo.... en el finally
            deFechaInicio.Enabled := False;
            neTasaInteres.Enabled := False;
            abmList.Enabled := True;
            Notebook.ActivePage := 'Salir';

        abmList.Reload;
        Result := True;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_MSG,e.Message,'ERROR',MB_ICONSTOP) ;
            Result := False;
        end;
    end;
finally
     FInicializando := False;
     abmListclick(self);
end;
end;

procedure TFAbmTasasInteres.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFAbmTasasInteres.abmListClick(Sender: TObject);
begin
if NOT FInicializando then begin

        deFechaInicio.Date := tblTasasInteres.FieldByName('FechaInicio').AsDateTime;
        
        //INICIO	: 20160616 CFU
        //neTasaInteres.Value := (tblTasasInteres.FieldByName('TasaInteresAnual').AsInteger);
        neTasaInteres.Value := (tblTasasInteres.FieldByName('TasaInteresAnual').AsFloat);
		// TERMINO 	: 20160616 CFU
        
        if (deFechaInicio.Date > FFechaInteresVigente) then begin
            // Estoy en un registro que es una tasa futura. La puedo eliminar o editar.
                abmList.Access := [accAlta, accBaja, accModi] ;                 //SS_1147_MCA_20150422
                abmToolbar.Habilitados := [btAlta, btBaja, btModi, btSalir];   //SS_1147_MCA_20150422
        end
        else begin
            // Estoy en una tasa vigente o historica. No puedo modificar ni editar.
            abmList.Access := [accAlta];
            abmToolbar.Habilitados := [btAlta, btSalir];
        end;
end;
end;


procedure TFAbmTasasInteres.CambiarAModo(Modo: integer);
begin
    if (Modo = CONSULTA) then begin
        deFechaInicio.Enabled := False;
        neTasaInteres.Enabled := False;
        abmList.Enabled := True;
        Notebook.ActivePage := 'Salir';
        abmList.Estado := Normal;
        abmList.Reload;
        abmListClick(Self);
    end
    else begin
        // Estoy en modo edicion. La ABMList esta deshabilitada. Solo puedo trabajar sobre
        // los Edit y los botones Aceptar y Cancelar.
        if abmList.Estado = Alta then begin
            deFechaInicio.Date := NowBase(DMConnections.BaseCAC);
            neTasaInteres.Value := 0;
        end;
        deFechaInicio.Enabled := True;
        neTasaInteres.Enabled := True;
        abmToolbar.Habilitados := [];
        abmList.Access := [] ;
        abmList.Enabled := False;
        Notebook.ActivePage := 'Editing';
        neTasaInteres.SetFocus ;
    end;

end;


procedure TFAbmTasasInteres.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
       CanClose := NOT ( neTasaInteres.Enabled );
end;

procedure TFAbmTasasInteres.abmListInsert(Sender: TObject);
begin
    CambiarAModo(EDICION);
end;

procedure TFAbmTasasInteres.btnCancelarClick(Sender: TObject);
begin
    // Estaba editando y decido cancelar
    tblTasasInteres.Cancel ;
    CambiarAModo(CONSULTA);
end;

procedure TFAbmTasasInteres.btnAceptarClick(Sender: TObject);
resourcestring
    ERROR_INSERT    =   'No se ha insertado ning�n registro';
    ERROR_MODIFY    =   'No se ha modificado el registro';
    ERROR_TITLE     =   'ERROR';
    MSG_OVERWRITE    =   'Ya existe un tasa de interes cargada para esa fecha. Desea modificarla?';
    TITLE_OVERWRITE =   'Confirme sobreescritura';
var
    sErrorCaption: String;
    fFechaActual : TDateTime;
begin
    try
        // Fijo la fache actual (toma la fecha del server, con mlisegundos...)
        fFechaActual := NowBase(DMConnections.BaseCAC);
        // Valido los controles
        if ValidateControls([deFechaInicio, neTasaInteres],
           [(deFechaInicio.Date >= TRUNC(FFechaActual)),
            ((neTasaInteres.Value > 0 ) AND (neTasaInteres.Value <= 100 ))],
            'Datos Incorrectos',
           ['La Fecha m�nima es la actual',
            'La tasa de interes debe ser positiva y menor o igual a 100%']) then begin
                    // Si los datos son validos
                    if abmList.Estado = Alta then begin
                        // Antes de aceptar el alta, debo controlar
                        // que la fecha ingresada no concuerde con otro registro...
                        if (tblTasasInteres.Locate( 'FechaInicio',deFechaInicio.Date,[])) AND (deFechaInicio.Date > FFechaActual) then begin
                            // Si existe pregunto si quiere sobreescribir...
                            if MsgBox(MSG_OVERWRITE,TITLE_OVERWRITE,MB_ICONQUESTION+MB_YESNO) = mrYes then begin
                                sErrorCaption := ERROR_MODIFY;
                                tblTasasInteres.Edit;
                            end
                            else begin
                            // Si no quiere, salgo...
                                CambiarAModo(CONSULTA);
                                Exit;
                            end;
                        end
                        else begin
                        // Si estaba haciendo un alta, sigo...
                        sErrorCaption := ERROR_INSERT;
                        tblTasasInteres.Insert  ;
                        end;
                    end
                    else begin
                        // Sino estaba haciendo una modificacion
                        sErrorCaption := ERROR_MODIFY;
                        tblTasasInteres.Edit;
                    end;
                    tblTasasInteres.FieldByName('FechaInicio').value := iif( (TRUNC(deFechaInicio.Date) = TRUNC(FFechaActual) ),
                                                    FFechaActual,deFechaInicio.Date);
                    tblTasasInteres.FieldByName('TasaInteresAnual').Value := neTasaInteres.Value;
                    // Blanqueo los errores de la connection
                    tblTasasInteres.Connection.Errors.Clear;
                    // Guardo los cambios
                    tblTasasInteres.Post;
                    // Actualizo la fecha de Interes Vigente
                    FFechaInteresVigente := ObtenerFechaInteresVigente;
                    CambiarAModo(CONSULTA);
        end else begin
                // Reseteo los valores porque no fueron validados...
                if (neTasaInteres.Value > 100) then
                    neTasaInteres.Value := 0;
                if deFechaInicio.Date < FFechaActual then
                    deFechaInicio.Date := FFechaActual;
        end;

    except
        // Se produjo un error....
        on e:exception do begin
            MsgBoxErr(sErrorCaption,e.Message,ERROR_TITLE,MB_ICONSTOP);
            if tblTasasInteres.State <> dsBrowse then tblTasasInteres.Cancel;
            CambiarAModo(CONSULTA);
        end;
    end;
end;

procedure TFAbmTasasInteres.abmListDelete(Sender: TObject);
resourcestring
    ERROR_DELETE = 'Error al intentar eliminar el registro';
    MSG_DELETE =   'Desea eliminar la Tasa de Interes del %s vigente a partir del %s ';
    TITLE_DELETE = 'Confirme Eliminaci�n';
begin
    try
        try
            // Solicito al usuario confirmacion de lo que quiere borrar
        	//INICIO	: 20160616 CFU
//            if (MsgBox(Format(MSG_DELETE,[ (floattostr((tblTasasInteres.FieldByName('TasaInteresAnual').asInteger)) + '%')
//                , tblTasasInteres.FieldByName('FechaInicio').asString ]),TITLE_DELETE,MB_ICONQUESTION+MB_YESNO) = mrYes) then begin
            if (MsgBox(Format(MSG_DELETE,[ (floattostrf((tblTasasInteres.FieldByName('TasaInteresAnual').AsFloat), ffNumber, 18, 2) + '%')
                , tblTasasInteres.FieldByName('FechaInicio').asString ]),TITLE_DELETE,MB_ICONQUESTION+MB_YESNO) = mrYes) then begin
			// TERMINO 	: 20160616 CFU

                tblTasasInteres.DeleteRecords(arCurrent);
                abmList.Repaint;
                abmListClick(Sender);
            end;
        except
            // Se produjo un error!
            on e:exception do begin
                MsgBoxErr(ERROR_DELETE,e.Message,'ERROR',MB_ICONSTOP);
            end;
        end;
    finally
        // Cambio a modo consulta (por las dudas)
        CambiarAModo(CONSULTA);
    end;
end;



{-----------------------------------------------------------------------------
  Function Name: ObtenerFechaInteresVigente
  Author:
  Date Created:  /  /
  Description:
  Parameters: N/A
  Return Value: TDateTime

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
Function TFAbmTasasInteres.ObtenerFechaInteresVigente:TDateTime;
var
    sFecha: TDateTime;
    MyQry: TADOQuery;
begin
    try
        // Obtengo la fecha de Interes Vigente.

        // Hago esto con un Query porque la funcion QueryGetValueDateTime recupera
        // la fecha como cadena y lo convierte a DateTime... al convertir a cadena
        // pierde los decimales de los segundos... entonces no sirve para comparar...

        //Creo el Query
        MyQry := TADOQuery.Create(nil);
        MyQry.Connection := DMConnections.BaseCAC;
        MyQry.SQL.Text := 'SELECT MAX(FechaInicio) as FechaMax FROM TasasInteres  WITH (NOLOCK) WHERE FechaInicio <= GetDate()';
        //Lo abro...
        MyQry.Open ;

        // Si la tabla esta vacia, devuelvo una fecha antigua...
        if MyQry.IsEmpty  then  sFecha := StrToDateTime('01/01/1900')
        else sFecha := MyQry.FieldByName('FechaMax').AsDateTime;
    except
        // Si falla... devuelvo un fecha antigua...
        sFecha := StrToDateTime('01/01/1900')
    end;
    Result := sFecha;
end;

procedure TFAbmTasasInteres.abmListEdit(Sender: TObject);
begin
// Entro en Modo Edicion
   CambiarAModo(EDICION);
end;

procedure TFAbmTasasInteres.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
// Cierro el formulario
    Action := caFree;
end;

procedure TFAbmTasasInteres.abmListDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
    // Decido como mostrar los datos en la tabla...
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        if  (Tabla.FieldByName('FechaInicio').AsDateTime = FFechaInteresVigente) then begin
            Font.Style := [];
            Font.Color := clRed;

        end;
        if  (Tabla.FieldByName('FechaInicio').AsDateTime > FFechaInteresVigente) then begin
            Font.Style := [];
            Font.Color := clWindowText;
        end;
        if  (Tabla.FieldByName('FechaInicio').AsDateTime < FFechaInteresVigente) then begin
            Font.Style := [];
            Font.Color := clGrayText;
        end;
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('FechaInicio').AsString);
        TextOut(Cols[1], Rect.Top, ((Format('%f',[(Tabla.FieldByName('TasaInteresAnual').AsFloat)] )+ ' %' )));

	end;

end;

procedure TFAbmTasasInteres.abmListRefresh(Sender: TObject);
begin
	if tblTasasInteres.IsEmpty then begin
        deFechaInicio.Date := Date;
        neTasaInteres.Value := 0;
    end;
end;

procedure TFAbmTasasInteres.AbmToolbarClose(Sender: TObject);                   //SS_1147_MCA_20150422
begin                                                                           //SS_1147_MCA_20150422
    Close;                                                                      //SS_1147_MCA_20150422
end;

end.
