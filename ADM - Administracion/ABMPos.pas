{-----------------------------------------------------------------------------
 Unit Name: ABMPos
 Author:    ggomez
 Purpose: Form para hacer ABM de registros sobre la tabla POS.
    * Se permiten modificar los campos NumeroPos, CodigoComercio y Descripcion.
    * Ninguno de los tres campos puede ser vac�o.
    * NumeroPos no puede ser repetido.
 History:

 Revision 1
 	Author:
    Date: 19/05/2005
    Description: Al actualizar los datos de un POS asignar al campo
    	Asignado el mismo valor que ten�a el registro al iniciar la
        modificaci�n, debido a que desde este form no se permite modificar ese
        dato.


Revision : 2
  Date: 19/02/2009
  Author: mpiazza
  Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
    los bloqueos de tablas en la lectura


-----------------------------------------------------------------------------}

unit ABMPos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DmiCtrls, DbList, Abm_obj, DB,
  ADODB,
  DMConnection, PeaProcs, Util, UtilProc, RStrings, StrUtils, UtilDB, Mask;

type
  TFABMPos = class(TForm)
    AbmToolbar: TAbmToolbar;
    lb_Pos: TAbmList;
    pnl_Datos: TPanel;
    lbl_NumeroPos: TLabel;
    lbl_Descripcion: TLabel;
    txt_NumeroPos: TNumericEdit;
    txt_Descripcion: TEdit;
    pnl_Botones: TPanel;
    Notebook: TNotebook;
    lbl_CodigoComercio: TLabel;
    txt_CodigoComercio: TEdit;
    ds_Pos: TDataSource;
    tbl_Pos: TADOTable;
    sp_ActualizarPos: TADOStoredProc;
    sp_EliminarPos: TADOStoredProc;
    sp_InsertarPos: TADOStoredProc;
    btn_Salir: TButton;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_SalirClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure AbmToolbarClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lb_PosClick(Sender: TObject);
    procedure lb_PosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure lb_PosEdit(Sender: TObject);
    procedure lb_PosRefresh(Sender: TObject);
    procedure lb_PosDelete(Sender: TObject);
    procedure lb_PosInsert(Sender: TObject);
    procedure txt_NumeroPosKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
	procedure LimpiarCampos;
	Procedure VolverCampos;
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  FABMPos: TFABMPos;

resourcestring
	STR_MAESTRO_POS = 'Maestro de POS';

implementation

{$R *.dfm}

procedure TFABMPos.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFABMPos.btn_CancelarClick(Sender: TObject);
begin
	VolverCampos;
end;

procedure TFABMPos.btn_AceptarClick(Sender: TObject);
resourcestring
    FLD_NUMERO_POS = 'N� de POS';
    FLD_CODIGO_COMERCIO = 'C�digo Comercio';
    MSG_NUMERO_POS_EXISTENTE = 'El N� de POS ya existe.';
    //INICIO	: 20160620 CFU    
    MSG_ERROR_TIPO_DATO	= 'El N�mero de Pos debe ser num�rico';
    MSG_NUMERO_FUERA_DE_RANGO = 'El N�mero de POS debe estar entre 1 y 255';
    //TERMINO	: 20160620
    CAPTION_TITULO = 'Actualizar POS';
begin
	//INICIO	: 20160620 CFU
    if (txt_NumeroPos.ValueInt < 1 ) or (txt_NumeroPos.ValueInt > 255) then begin
    	MsgBox(MSG_NUMERO_FUERA_DE_RANGO, CAPTION_TITULO, MB_ICONSTOP);
        txt_NumeroPos.SetFocus;
        Exit;
    end;
    //TERMINO	: 20160620 CFU
    
	if not ValidateControls([txt_NumeroPos, txt_CodigoComercio,txt_Descripcion],
            [txt_NumeroPos.Text <> EmptyStr,
            (Trim(txt_CodigoComercio.Text) <> EmptyStr),
            (Trim(txt_Descripcion.Text) <> EmptyStr)],
	        Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_POS]),
            [Format(MSG_VALIDAR_DEBE_EL, [FLD_NUMERO_POS]),
            Format(MSG_VALIDAR_DEBE_EL, [FLD_CODIGO_COMERCIO]),
            Format(MSG_VALIDAR_DEBE_LA, [FLD_DESCRIPCION])]) then begin
		Exit;
	end;

    
    (* Si estoy insertando, controlar que el NumeroPos NO exista en la tabla. *)
    if (lb_Pos.Estado = Alta) then begin
        //if lb_Pos.Table.Locate('NumeroPos', txt_NumeroPos.ValueInt, []) then begin
        try
            if lb_Pos.Table.Locate('NumeroPos', txt_NumeroPos.Text, []) then begin
                MsgBox(MSG_NUMERO_POS_EXISTENTE, CAPTION_TITULO, MB_ICONSTOP);
                Exit;
            end;
        except
            MsgBox(MSG_ERROR_TIPO_DATO, CAPTION_TITULO, MB_ICONSTOP);
            Exit;
        end;
    end;

    (* Si estoy modificando y el NumeroPosNuevo es distinto al valor original,
        controlar que el NumeroPos NO exista en la tabla. *)
    if (lb_Pos.Estado = Modi)
            and (txt_NumeroPos.ValueInt <> lb_Pos.Table.FieldByName('NumeroPos').AsInteger)
        then begin
            if lb_Pos.Table.Locate('NumeroPos', txt_NumeroPos.ValueInt, []) then begin
                MsgBox(MSG_NUMERO_POS_EXISTENTE, CAPTION_TITULO, MB_ICONSTOP);
                Exit;
            end;
    end;

	with lb_Pos do begin
		Screen.Cursor := crHourGlass;
		try
			try
                case Estado of
                    Alta:   begin
                                with sp_InsertarPos, Parameters do begin
                                    ParamByName('@NumeroPos').Value := txt_NumeroPos.ValueInt;
                                    ParamByName('@CodigoComercio').Value := Trim(txt_CodigoComercio.Text);
                                    ParamByName('@Descripcion').Value := Trim(txt_Descripcion.Text);
                                    ExecProc;
                                end;
                            end;
                    Modi:   begin
                                with sp_ActualizarPos, Parameters do begin
                                    ParamByName('@NumeroPos').Value := Table.FieldByName('NumeroPos').AsInteger;
                                    ParamByName('@NumeroPosNuevo').Value := txt_NumeroPos.ValueInt;
                                    ParamByName('@CodigoComercio').Value := Trim(txt_CodigoComercio.Text);
                                    ParamByName('@Descripcion').Value := Trim(txt_Descripcion.Text);
                                    ParamByName('@Asignado').Value := Table.FieldByName('Asignado').Value;
                                    ExecProc;
                                end;
                            end;
                end; // case
			except
				on E: Exception do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR, [STR_MAESTRO_POS]),
                        E.Message, Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_POS]),
                        MB_ICONSTOP);
				end;
			end;
		finally
            VolverCampos;
			Screen.Cursor := crDefault;
            Reload;
		end;
	end; // with
end;

procedure TFABMPos.AbmToolbarClose(Sender: TObject);
begin
    Close;
end;

procedure TFABMPos.FormShow(Sender: TObject);
begin
	lb_Pos.Reload;
end;

procedure TFABMPos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 Action := caFree;
end;

procedure TFABMPos.LimpiarCampos;
begin
	txt_NumeroPos.Clear;
    txt_CodigoComercio.Clear;
	txt_Descripcion.Clear;
end;

//INICIO  : 20160614 CFU
procedure TFABMPos.txt_NumeroPosKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key in ['0'..'9', #8]) then Key :=#0;
end;
//FIN	: 20160614 CFU

procedure TFABMPos.VolverCampos;
begin
	lb_Pos.Estado       := Normal;
	lb_Pos.Enabled      := True;
	ActiveControl       := lb_Pos;
	Notebook.PageIndex  := 0;
	pnl_Datos.Enabled   := False;
	lb_Pos.Reload;
end;

function TFABMPos.Inicializar(txtCaption: String;
  MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
	Result := False;

	if MDIChild then begin
        FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	Notebook.PageIndex := 0;

   	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([tbl_Pos]) then Exit;

    // Limpiar controles de ingreso de datos.
    LimpiarCampos;

    KeyPreview := True;
	lb_Pos.Reload;
	Result := True;
end;

procedure TFABMPos.lb_PosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txt_NumeroPos.Value      := FieldByName('NumeroPos').AsInteger;
	   txt_CodigoComercio.Text  := FieldByName('CodigoComercio').AsString;
	   txt_Descripcion.Text     := FieldByname('Descripcion').AsString;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: lb_PosDrawItem
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TDBList; Tabla: TDataSet;
            Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFABMPos.lb_PosDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
resourcestring
    MSG_UBICACION = 'Este POS se encuentra en el ';
    MSG_NO = 'No';
const
    TxtSQL = 'SELECT PUNTOSVENTA.DESCRIPCION FROM PUNTOSVENTA  WITH (NOLOCK) ' +
      ' INNER JOIN POS  WITH (NOLOCK)  ON POS.NUMEROPOS = PUNTOSVENTA.NUMEROPOS AND POS.NUMEROPOS = %d';
var
    Asignado: String;

begin
	with Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(FieldbyName('NumeroPos').AsInteger, 3));
		TextOut(Cols[1], Rect.Top, Trim(FieldbyName('CodigoComercio').AsString));
		TextOut(Cols[2], Rect.Top, Trim(FieldbyName('Descripcion').AsString));
        Asignado := QueryGetValue(DMConnections.BaseCAC, Format(TxtSQL, [Tabla.FieldByName('NumeroPos').AsInteger]));
        if Trim(Asignado) = '' then Asignado := MSG_NO else Asignado := MSG_UBICACION  + Asignado;
        TextOut(Cols[3], Rect.Top, Asignado);
	end;
end;

procedure TFABMPos.lb_PosEdit(Sender: TObject);
begin
	lb_Pos.Enabled      := False;
	lb_Pos.Estado       := Modi;
	Notebook.PageIndex  := 1;
	pnl_Datos.Enabled   := True;
	ActiveControl       := txt_NumeroPos;
end;

procedure TFABMPos.lb_PosRefresh(Sender: TObject);
begin
	 if lb_Pos.Empty then LimpiarCampos;
end;

procedure TFABMPos.lb_PosDelete(Sender: TObject);
resourcestring
    MSG_DATOS_ASOCIADOS   = 'Existen datos asociados al POS. No se permite la eliminaci�n.';
    MSG_DELETE_CAPTION    = 'Eliminar POS';
begin
	Screen.Cursor := crHourGlass;

    (* Mostrar mensaje de confirmaci�n de la eliminaci�n. *)
	if MsgBox(Format(MSG_QUESTION_ELIMINAR, [STR_MAESTRO_POS]), STR_CONFIRMACION,
            MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with sp_EliminarPos do begin
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroPos').Value := lb_Pos.Table.FieldbyName('NumeroPos').Value;
                Parameters.ParamByName('@Resultado').Value := 0;
                ExecProc;
            end;
		except
			on E: Exception do begin
                (* Si Resultado es distinto de 0 (cero), es porque ocurri� un
                error en la eliminaci�n.
                547: Es el error que ocurre si se viola una Foreign Key. *)
                if sp_EliminarPos.Parameters.ParamByName('@Resultado').Value = 547 then
    				MsgBoxErr(MSG_DATOS_ASOCIADOS,
                        E.Message, Format(MSG_CAPTION_ELIMINAR, [STR_MAESTRO_POS]),
                        MB_ICONSTOP)
                else
    				MsgBoxErr(Format(MSG_ERROR_ELIMINAR, [STR_MAESTRO_POS]),
                        E.Message, Format(MSG_CAPTION_ELIMINAR, [STR_MAESTRO_POS]),
                        MB_ICONSTOP);
			end;
		end;
	end;
	lb_Pos.Reload;
	lb_Pos.Estado := Normal;
	lb_Pos.Enabled := True;
	Notebook.PageIndex := 0;
	Screen.Cursor := crDefault;
end;

procedure TFABMPos.lb_PosInsert(Sender: TObject);
begin
	LimpiarCampos;
    pnl_Datos.Enabled   := True;
	lb_Pos.Enabled      := False;
	Notebook.PageIndex  := 1;
	ActiveControl       := txt_NumeroPos;
end;

end.
