object FormTSMC: TFormTSMC
  Left = 203
  Top = 159
  Caption = 'Mantenimiento de TSMC'
  ClientHeight = 425
  ClientWidth = 692
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 692
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 692
    Height = 196
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'99'#0'Concesionaria        '
      #0'64'#0'Domain ID  '
      #0'58'#0'TSMC ID  '
      #0'142'#0'Descripci'#243'n                          '
      #0'96'#0'Direcci'#243'n IP          '
      #0'42'#0'Port 1  '
      #0'42'#0'Port 2  '
      #0'42'#0'Port 3  '
      #0'50'#0'UserID   '
      #0'89'#0'FTPServer          '
      #0'104'#0'FTPUser                  ')
    HScrollBar = True
    RefreshTime = 100
    Table = qry_TSMC
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 229
    Width = 692
    Height = 157
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      692
      157)
    object PageControl: TPageControl
      Left = 7
      Top = 8
      Width = 689
      Height = 145
      ActivePage = TabOpciones
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      object TabGeneral: TTabSheet
        Caption = '&General'
        Enabled = False
        object Label2: TLabel
          Left = 26
          Top = 14
          Width = 85
          Height = 13
          Caption = '&Concesionaria:'
          FocusControl = cbConcesionarias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 26
          Top = 40
          Width = 64
          Height = 13
          Caption = '&Domain ID:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 26
          Top = 94
          Width = 72
          Height = 13
          Caption = '&Descripci'#243'n:'
          FocusControl = txt_Descripcion
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 26
          Top = 67
          Width = 56
          Height = 13
          Caption = '&TSMC ID:'
          FocusControl = txt_TSMC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cbConcesionarias: TComboBox
          Left = 119
          Top = 9
          Width = 297
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbConcesionariasChange
        end
        object txt_Descripcion: TEdit
          Left = 119
          Top = 88
          Width = 297
          Height = 21
          Color = 16444382
          MaxLength = 60
          TabOrder = 3
        end
        object txt_TSMC: TNumericEdit
          Left = 119
          Top = 60
          Width = 105
          Height = 21
          Color = 16444382
          MaxLength = 2
          TabOrder = 2
        end
        object txt_Domain: TNumericEdit
          Left = 119
          Top = 36
          Width = 105
          Height = 21
          Color = 16444382
          MaxLength = 2
          TabOrder = 1
        end
      end
      object TabConexion: TTabSheet
        Caption = '&Conexion'
        Enabled = False
        ImageIndex = 1
        object Label5: TLabel
          Left = 26
          Top = 14
          Width = 75
          Height = 13
          Caption = '&Direcci'#243'n IP:'
          FocusControl = cbConcesionarias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 26
          Top = 42
          Width = 35
          Height = 13
          Caption = 'Port &1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 26
          Top = 70
          Width = 35
          Height = 13
          Caption = 'Port &2'
          FocusControl = txt_Descripcion
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 26
          Top = 98
          Width = 35
          Height = 13
          Caption = 'Port &3'
          FocusControl = txt_port3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txt_port3: TNumericEdit
          Left = 119
          Top = 92
          Width = 105
          Height = 21
          Color = 16444382
          MaxLength = 6
          TabOrder = 3
        end
        object txt_port1: TNumericEdit
          Left = 119
          Top = 36
          Width = 105
          Height = 21
          Color = 16444382
          MaxLength = 6
          TabOrder = 1
        end
        object txt_port2: TNumericEdit
          Left = 119
          Top = 64
          Width = 105
          Height = 21
          Color = 16444382
          MaxLength = 6
          TabOrder = 2
        end
        object txt_ip: TMaskEdit
          Left = 120
          Top = 8
          Width = 103
          Height = 21
          Color = 16444382
          EditMask = '999.999.999.999;1; '
          MaxLength = 15
          TabOrder = 0
          Text = '   .   .   .   '
        end
      end
      object TabFTP: TTabSheet
        Caption = 'FTP'
        Enabled = False
        ImageIndex = 2
        object Label9: TLabel
          Left = 26
          Top = 14
          Width = 28
          Height = 13
          Caption = 'FTP '
          FocusControl = cbConcesionarias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 26
          Top = 42
          Width = 48
          Height = 13
          Caption = '&Usuario:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 26
          Top = 70
          Width = 59
          Height = 13
          Caption = '&Password:'
          FocusControl = txt_Descripcion
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 26
          Top = 98
          Width = 24
          Height = 13
          Caption = 'Por&t'
          FocusControl = txt_TSMC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label14: TLabel
          Left = 298
          Top = 70
          Width = 110
          Height = 13
          Caption = '&Verificar Password:'
          FocusControl = txt_Descripcion
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txt_ftpUser: TEdit
          Left = 111
          Top = 36
          Width = 121
          Height = 21
          Color = 16444382
          MaxLength = 20
          TabOrder = 1
        end
        object txt_ftpIP: TMaskEdit
          Left = 112
          Top = 8
          Width = 121
          Height = 21
          Color = 16444382
          EditMask = '999.999.999.999;1; '
          MaxLength = 15
          TabOrder = 0
          Text = '   .   .   .   '
        end
        object txt_ftpPort: TNumericEdit
          Left = 111
          Top = 92
          Width = 121
          Height = 21
          Color = 16444382
          MaxLength = 6
          TabOrder = 2
        end
        object txt_FTPPassword: TMaskEdit
          Left = 112
          Top = 64
          Width = 119
          Height = 21
          Color = 16444382
          MaxLength = 20
          PasswordChar = '*'
          TabOrder = 3
        end
        object txt_ftppasswordVerificar: TMaskEdit
          Left = 416
          Top = 64
          Width = 121
          Height = 21
          Color = 16444382
          MaxLength = 20
          PasswordChar = '*'
          TabOrder = 4
        end
      end
      object TabOpciones: TTabSheet
        Caption = 'Opciones'
        ImageIndex = 3
        object Label16: TLabel
          Left = 26
          Top = 14
          Width = 107
          Height = 13
          Caption = 'Path de Imagenes:'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label13: TLabel
          Left = 26
          Top = 42
          Width = 94
          Height = 13
          Caption = '&Nombre de Usuario:'
          FocusControl = cbConcesionarias
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object txt_path: TEdit
          Left = 152
          Top = 9
          Width = 382
          Height = 21
          Color = 16444382
          MaxLength = 128
          TabOrder = 0
          Text = 'txt_path'
        end
        object txt_user: TEdit
          Left = 152
          Top = 36
          Width = 153
          Height = 21
          Color = clWhite
          MaxLength = 20
          TabOrder = 1
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 386
    Width = 692
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 495
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object qry_TSMC: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM '
      'TSMCs WITH(NOLOCK)')
    Left = 311
    Top = 91
  end
end
