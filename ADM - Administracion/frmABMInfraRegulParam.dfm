object ABMInfraRegulParamForm: TABMInfraRegulParamForm
  Left = 106
  Top = 61
  Caption = 'Infracciones Par'#225'metros Regularizaci'#243'n'
  ClientHeight = 571
  ClientWidth = 817
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupB: TPanel
    Left = 0
    Top = 427
    Width = 817
    Height = 105
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    ExplicitTop = 408
    object Lcodigo: TLabel
      Left = 148
      Top = 25
      Width = 85
      Height = 13
      Caption = 'Concesionaria:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Ldescripcion: TLabel
      Left = 113
      Top = 48
      Width = 120
      Height = 13
      Caption = 'D'#237'as Regularizaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 23
      Top = 71
      Width = 210
      Height = 13
      Caption = 'D'#237'as Regularizaci'#243'n NO Facturable::'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 239
      Top = 22
      Width = 258
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
    object nedtDiasRegul: TNumericEdit
      Left = 239
      Top = 45
      Width = 58
      Height = 21
      Color = 16444382
      TabOrder = 1
      BlankWhenZero = False
    end
    object nedtDiasRegulNOFacturable: TNumericEdit
      Left = 239
      Top = 68
      Width = 58
      Height = 21
      Color = 16444382
      TabOrder = 2
      BlankWhenZero = False
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 532
    Width = 817
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 620
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 817
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbarClose
  end
  object ListaParametros: TAbmList
    Left = 0
    Top = 33
    Width = 817
    Height = 394
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      
        #0'237'#0'Concesionaria                                              ' +
        '        '
      #0'110'#0'D'#237'as Regularizaci'#243'n  '
      #0'185'#0'D'#237'as Regularizaci'#243'n NO Facturable   '
      #0'11'#0' ')
    HScrollBar = True
    RefreshTime = 100
    Table = spObtenerInfraccionesRegularizacionParametros
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaParametrosClick
    OnProcess = ListaParametrosProcess
    OnDrawItem = ListaParametrosDrawItem
    OnRefresh = ListaParametrosRefresh
    OnInsert = ListaParametrosInsert
    OnDelete = ListaParametrosDelete
    OnEdit = ListaParametrosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
    ExplicitHeight = 432
  end
  object spObtenerInfraccionesRegularizacionParametros: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerInfraccionesRegularizacionParametros'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 440
    Top = 256
  end
  object dsObtenerInfraccionesRegularizacionParametros: TDataSource
    DataSet = spObtenerInfraccionesRegularizacionParametros
    Left = 456
    Top = 280
  end
  object spActualizarInfraccionesRegularizacionParametros: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesRegularizacionParametros'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@DiasRegul'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
      end
      item
        Name = '@DiasRegulNOFacturable'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
      end>
    Left = 592
    Top = 96
  end
  object spEliminarInfraccionesRegularizacionParametros: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarInfraccionesRegularizacionParametros'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end>
    Left = 624
    Top = 152
  end
end
