object FormTramosCalles: TFormTramosCalles
  Left = 319
  Top = 206
  BorderStyle = bsDialog
  Caption = 'Tramo de Calle'
  ClientHeight = 273
  ClientWidth = 232
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    232
    273)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 8
    Top = 8
    Width = 216
    Height = 217
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Datos del Tramo'
    TabOrder = 0
    object Label11: TLabel
      Left = 8
      Top = 134
      Width = 87
      Height = 13
      Caption = '&Codigo Postal Par:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 18
      Width = 41
      Height = 13
      Caption = '&Desde:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 47
      Width = 38
      Height = 13
      Caption = '&Hasta:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 105
      Width = 97
      Height = 13
      Caption = '&Codigo Postal Impar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 76
      Width = 30
      Height = 13
      Caption = '&CAD:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 190
      Width = 44
      Height = 13
      Caption = 'L&ongitud:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 8
      Top = 161
      Width = 35
      Height = 13
      Caption = '&Latitud:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtCodigoPostalPAR: TEdit
      Left = 119
      Top = 130
      Width = 74
      Height = 21
      Hint = 'C'#243'digo postal par'
      MaxLength = 7
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object txt_NumeroDesde: TNumericEdit
      Left = 119
      Top = 14
      Width = 74
      Height = 21
      Hint = 'Numeraci'#243'n del tramo "desde"'
      Color = 16444382
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Decimals = 0
    end
    object txt_NumeroHasta: TNumericEdit
      Left = 119
      Top = 43
      Width = 74
      Height = 21
      Hint = 'Numeraci'#243'n del tramo "hasta"'
      Color = 16444382
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Decimals = 0
    end
    object txtCodigoPostalImpar: TEdit
      Left = 119
      Top = 101
      Width = 74
      Height = 21
      Hint = 'C'#243'digo postal impar'
      MaxLength = 7
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object cbCAD: TComboBox
      Left = 120
      Top = 72
      Width = 73
      Height = 21
      Hint = 'CAD'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      ItemIndex = 0
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = '0'
      Items.Strings = (
        '0'
        '1')
    end
    object txt_latitud: TNumericEdit
      Left = 119
      Top = 156
      Width = 74
      Height = 21
      TabOrder = 5
      Decimals = 0
    end
    object txt_longitud: TNumericEdit
      Left = 119
      Top = 182
      Width = 74
      Height = 21
      TabOrder = 6
      Decimals = 0
    end
  end
  object BtnAceptar: TDPSButton
    Left = 58
    Top = 239
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = BtnAceptarClick
  end
  object BtnCancelar: TDPSButton
    Left = 145
    Top = 239
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = BtnCancelarClick
  end
end
