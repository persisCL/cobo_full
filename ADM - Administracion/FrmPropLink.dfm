object FormPropLink: TFormPropLink
  Left = 258
  Top = 233
  BorderStyle = bsDialog
  Caption = 'Propiedades de la secuencia'
  ClientHeight = 216
  ClientWidth = 435
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 61
    Height = 13
    Caption = 'Condiciones:'
  end
  object txt_condiciones: TMemo
    Left = 8
    Top = 25
    Width = 420
    Height = 152
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Button1: TDPSButton
    Left = 264
    Top = 184
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TDPSButton
    Left = 352
    Top = 184
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
end
