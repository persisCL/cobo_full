unit ParamGen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DMConnection,
  StdCtrls, validate, TimeEdit, Db, DBTables, UtilProc, UtilDB, ExtCtrls, Variants,
  Util, CheckLst, ComCtrls, ADODB, DmiCtrls, Buttons, PeaProcs, DPSControls,
  ListBoxEx, DBListEx, frmEditarVal, VariantComboBox, ConstParametrosGenerales;

type
  TFormParam = class(TForm)
    Parametros: TADOTable;
    PageControl: TPageControl;
    TabSheet1: TTabSheet;
    ButtonCancelar: TDPSButton;
    ButtonAceptar: TDPSButton;
    OpenDialog: TOpenDialog;
    Lista: TDBListEx;
    DataSource1: TDataSource;
    Generales: TADOTable;
    GeneralesClaseParametro: TStringField;
    GeneralesNombre: TStringField;
    GeneralesDescripcion: TStringField;
    GeneralesValor: TStringField;
    GeneralesTipoParametro: TStringField;
    GeneralesValorDefault: TStringField;
    Panel1: TPanel;
    Label1: TLabel;
    cbFiltros: TVariantComboBox;
    procedure ButtonCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonAceptarClick(Sender: TObject);
    procedure ListaLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure cbFiltrosChange(Sender: TObject);
    procedure ListaDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure ListaColumns0HeaderClick(Sender: TObject);
    procedure ListaColumns1HeaderClick(Sender: TObject);
    procedure ListaColumns2HeaderClick(Sender: TObject);
    procedure ListaColumns3HeaderClick(Sender: TObject);
  private
	function CargarClasesParametros: Boolean;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormParam: TFormParam;

implementation

{$R *.DFM}

function TFormParam.Inicializa: boolean;
begin
	CenterForm(Self);
	PageControl.TabIndex := 0;
    PageControl.ActivePageIndex := 0;
    Result := CargarClasesParametros and OpenTables([Parametros, Generales]);
end;

procedure TFormParam.ButtonCancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormParam.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Parametros.Close;
	Action := caFree;
end;

procedure TFormParam.ButtonAceptarClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormParam.ListaLinkClick
  Author:    gcasais
  Date:      14-Oct-2004
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn
  Result:    None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
var
    f: TfrmValueEdit;
begin
     Application.CreateForm(TfrmValueEdit, f);
     if f.Inicializar(Generales, Generales.RecNo) then f.ShowModal;
     f.Release;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormParam.CargarClasesParametros
  Author:    gcasais
  Date:      21-Ene-2005
  Arguments: None
  Result:    Boolean
-----------------------------------------------------------------------------}

function TFormParam.CargarClasesParametros: Boolean;
resourcestring
    CLASE_FACTURACION = 'Facturación';
    CLASE_GENERAL = 'General';
    CLASE_INTERFACE = 'Interfaces';
    CLASE_MAIL = 'Correo Electronico';
    CLASE_REPORTE = 'Reportes';
    CLASE_SOLICITUD = 'Solicitudes';
    CLASE_VALIDACION = 'Validación';
    CLASE_COBRANZAS = 'Cobranzas';
    MSG_TODOS = 'Todos los Sistemas';
var
    Qry: TADOQuery;
    Clase: Char;
begin
    Result := False;
    Qry:= TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    Qry.SQL.Add('SELECT distinct ClaseParametro As Clase From ParametrosGenerales Order By Clase');
    try
        Qry.Open;
        if Qry.IsEmpty then exit;
        cbFiltros.Items.InsertItem(MSG_TODOS, '1');
        while not Qry.Eof do begin
            Clase := Qry.FieldByName('Clase').AsString[1];
            case Clase of
                'F': cbFiltros.Items.InsertItem(CLASE_FACTURACION, Clase);
                'G': cbFiltros.Items.InsertItem(CLASE_GENERAL, Clase);
                'I': cbFiltros.Items.InsertItem(CLASE_INTERFACE, Clase);
                'M': cbFiltros.Items.InsertItem(CLASE_MAIL, Clase);
                'R': cbFiltros.Items.InsertItem(CLASE_REPORTE, Clase);
                'S': cbFiltros.Items.InsertItem(CLASE_SOLICITUD, Clase);
                'V': cbFiltros.Items.InsertItem(CLASE_VALIDACION, Clase);
                'C': cbFiltros.Items.InsertItem(CLASE_COBRANZAS, Clase);
            end;
            Qry.Next;
        end;
        cbFiltros.ItemIndex := 0;
        Result := True;
    finally
        FreeAndNil(Qry);
    end;
end;

procedure TFormParam.cbFiltrosChange(Sender: TObject);
begin
    if cbFiltros.Value = '1' then begin
        Generales.Filtered := False;
        Exit;
    end;
    Generales.Filtered := False;
    Generales.Filter := 'CLASEPARAMETRO = ' + QuotedStr(cbFiltros.Value);
    Generales.Filtered := True;
end;
{-----------------------------------------------------------------------------
  Procedure: TFormParam.ListaDrawText
  Author:    gcasais
  Date:      20-Ene-2005
  Arguments: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect:
  TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean
  Result:    None
-----------------------------------------------------------------------------}

procedure TFormParam.ListaDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
resourcestring
    MSG_PARAM_TYPE_INTEGER = 'Valor Entero';
    MSG_PARAM_TYPE_TEXT =    'Texto';
    MSG_PARAM_TYPE_DATETIME = 'Fecha/Hora';
    MSG_PARAM_TYPE_NUMERIC = 'Valor Numérico';
begin
    if Column.FieldName = 'TipoParametro' then begin
        if TRIM(Text) = TP_ENTERO then  Text :=  MSG_PARAM_TYPE_INTEGER
        else begin
            if TRIM(Text) = TP_TEXTO then Text :=  MSG_PARAM_TYPE_TEXT
            else begin
                if TRIM(Text) = TP_FECHA then Text :=  MSG_PARAM_TYPE_DATETIME
                   else Text := MSG_PARAM_TYPE_NUMERIC;
            end;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ListaColumns0HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns0HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'Descripcion DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'Descripcion ASC';
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ListaColumns1HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns1HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'Valor DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'Valor ASC';
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ListaColumns2HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns2HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'ValorDefault DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'ValorDefault ASC';
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: ListaColumns3HeaderClick
  Author:    gcasais
  Date Created: 01/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormParam.ListaColumns3HeaderClick(Sender: TObject);
begin
    if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        Generales.Sort := 'TipoParametro DESC';
    end else begin TDBListExColumn(sender).Sorting := csAscending;
        Generales.Sort := 'TipoParametro ASC';
    end;
end;
end.
