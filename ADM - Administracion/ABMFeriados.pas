unit ABMFeriados;
{
Firma       : PCL00200-EBA-20131121
Description :
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcsCN, validate,
  Dateedit, Util, ADODB,DPSControls, Provider, DBClient, ListBoxEx, DBListEx,
  VariantComboBox, RStrings;

type
  TFormFeriados = class(TForm)
    pnlEdicion: TPanel;
    lblDescripcion: TLabel;
    lblFechaFeriado: TLabel;
    pnlBottom: TPanel;
    tedDescripcion: TEdit;
    deFechaFeriado: TDateEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    spPlanTarifarioDiasFeriados_Obtener: TADOStoredProc;
    spPlanTarifarioDiasFeriados_Agregar: TADOStoredProc;
    spPlanTarifarioDiasFeriados_Actualizar: TADOStoredProc;
    dblListaDiasFeriados: TDBListEx;
    spPlanTarifarioDiasFeriados_Eliminar: TADOStoredProc;
    sbtAgregaDiaFeriado: TSpeedButton;
    sbtActualizaDiaFeriado: TSpeedButton;
    sbtEliminaDiaFeriado: TSpeedButton;
    lblTotalRegistros: TLabel;
    lblShowTotalRegistros: TLabel;
    pnlCabecera: TPanel;
    vcbAnio: TVariantComboBox;
    Label2: TLabel;
    spPlanTarifarioDiasFeriados_Obtener_Anio: TADOStoredProc;
    dsPlanTarifarioDiasFeriados_Obtener: TDataSource;
    lblShow_Inserta_Modifica: TLabel;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure sbtAgregaDiaFeriadoClick(Sender: TObject);
    procedure sbtActualizaDiaFeriadoClick(Sender: TObject);
    procedure sbtEliminaDiaFeriadoClick(Sender: TObject);
    procedure dblListaDiasFeriadosClick(Sender: TObject);
    procedure vcbAnioChange(Sender: TObject);

  private
    { Private declarations }
    FEdicion : byte;		{ 0 = browse, 1 = Nuevo, 2 = Edici�n}

    FFechaDiaFeriado: TDateTime;
    FDescripcion: string;

    FFechaFeriado: String;
    FSelectedIndexdbl: Integer;

    procedure Limpiar_Campos;
//    procedure Volver_Campos;
//    procedure CambiaIndice(pIndice: string);


    procedure ActivarBotones(EsActivar : boolean; Grupo : byte);
    procedure CargarAnios;
    procedure PlanTarifarioDiasFeriados_Obtener;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormFeriados: TFormFeriados;

const
    cFechaFeriado = 'FechaFeriado';
    cFechaFeriadoDESC = 'FechaFeriadoDESC';
    cDescripcion = 'Descripcion';
    cDescripcionDESC = 'DescripcionDESC';

implementation

Uses
  DMConnection;

resourcestring
	MSG_CAPTION_VALIDAR				        = 'Validar datos ingresados';

  MSG_SELECCIONE                    = ' Seleccione ';

  STR_DIAFERIADO                    = 'D�a feriado';

  MSG_ERROR_FECHA_INCORRECTA		    = 'La fecha es incorrecta, no debe ser igual o anterior a la fecha actual';
  MSG_ERROR_ANIO_INCORRECTO         = 'Solo se pueden ingresar dias feriado de los a�os permitidos, ver "Seleccione a�o"';
  MSG_ERROR_DESCRIPCION_VACIA       = 'Ingrese una descripcion';                    //TASK_103_JMA_20160126

  MSG_AGREGA_OK                     = 'D�a feriado agregado con �xito';
  MSG_AGREGA_CAPTION                = 'Agregar dia feriado';
  MSG_AGREGA_ERROR                  = 'Error al agregar d�a feriado';
  MSG_AGREGA_ERROR_FECHA_ACTIVACION = 'La fecha de activaci�n debe se mayor a la actual';
  MSG_AGREGA_EXISTE                 = 'La fecha ingresada ya existe';              //TASK_113_JMA_20170313

  MSG_ACTUALIZAR_CAPTION   		      = 'Actualizar d�a feriado';
  MSG_ACTUALIZAR_OK                 = 'D�a feriado actualizado con �xito';
  MSG_ACTUALIZAR_ERROR              = 'Error al actualizar d�a feriado';
  MSG_ACTUALIZAR_NOK                = 'S�lo se pueden modificar d�as futuros';

  MSG_ELIMINAR_OK                 = 'D�a feriado eliminada con �xito';
  MSG_ELIMINAR_ERROR              = 'Error al eliminar d�a feriado';
  MSG_ELIMINAR_NOK                = 'D�a no puede ser eliminado';

const
    cEdicionBrowse		= 0;
    cEdicionInsertar	= 1;
    cEdicionEditar		= 2;
    cGrupoToolbar		  = 1;		//botones Nuevo, Editar, Eliminar, Buscar
    cGrupoSalir			  = 2;    //bot�n Salir
    cGrupoEdicion		  = 3;		//Botones Aceptar y Cancelar
    cGrupoBrowse		  = 4;		//botones Editar y Eliminar

{$R *.DFM}

function TFormFeriados.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

  FEdicion := cEdicionBrowse;

  ActivarBotones(True, cGrupoSalir);
  ActivarBotones(False,  cGrupoEdicion);

  deFechaFeriado.Date := Now;

  CargarAnios;
  vcbAnio.OnChange(vcbAnio);

  dblListaDiasFeriados.SetFocus;

	Result := True;

end;

procedure TFormFeriados.CargarAnios;
var
  lEof: Boolean;
  nI: Integer;
  lFound: Boolean;
begin
  //cargando el combo con los a�os existentes y el siguiente del actual
  vcbAnio.Clear;
  //vcbAnio.Items.Add(MSG_SELECCIONE, 0);

  with spPlanTarifarioDiasFeriados_Obtener_Anio do begin
    if Active then Close;
    Open;
    lEof := not EOF;
    while not EOF do begin
      vcbAnio.Items.Add(
             FieldByName('Anio').AsString,
             FieldByName('Anio').AsInteger );
        Next;
    end;
  end;
  spPlanTarifarioDiasFeriados_Obtener_Anio.Close;
  //spPlanTarifarioDiasFeriados_Obtener_Anio.Free;

  if lEof then begin
    nI := 0;
    lFound := True;
    while lFound do begin
      if nI < vcbAnio.Items.Count then begin
        vcbAnio.ItemIndex := nI;
        if vcbAnio.Value = Year(Now) then begin
          lFound := False;
          vcbAnio.Refresh;
        end;
        nI := nI + 1;
      end else begin
        lFound := False;
              end;
          end;
     end;

end;

procedure TFormFeriados.BtnCancelarClick(Sender: TObject);
begin
    FEdicion := cEdicionBrowse;
    vcbAnio.Enabled := True;
    ActivarBotones(True, cGrupoToolbar);
    ActivarBotones(True, cGrupoSalir);
    ActivarBotones(False,  cGrupoEdicion);
    dblListaDiasFeriados.Enabled := True;

end;

procedure TFormFeriados.Limpiar_Campos();
begin
	deFechaFeriado.Clear;
	tedDescripcion.Clear;
end;

procedure TFormFeriados.sbtActualizaDiaFeriadoClick(Sender: TObject);
begin

  vcbAnio.Enabled := False;

  if (FFechaDiaFeriado <= Date) then begin
    MsgBox(MSG_ACTUALIZAR_NOK, Format(MSG_CAPTION_ACTUALIZAR,[STR_DIAFERIADO]));
    vcbAnio.Enabled := True;
		Exit;
  end;

  //Aqui actualizo el dia feriado
  FEdicion := cEdicionEditar;
  lblShow_Inserta_Modifica.Caption := 'Modificando d�a feriado';

  FSelectedIndexdbl := dblListaDiasFeriados.SelectedIndex;

  ActivarBotones(False, cGrupoToolbar);
  ActivarBotones(False, cGrupoSalir);
  ActivarBotones(True,  cGrupoEdicion);

  deFechaFeriado.Enabled := False;
  dblListaDiasFeriados.Enabled := False;
  deFechaFeriado.Date := FFechaDiaFeriado;
  tedDescripcion.Text := FDescripcion;
  tedDescripcion.SetFocus;

end;

procedure TFormFeriados.sbtAgregaDiaFeriadoClick(Sender: TObject);
begin
  //aqui agrega un dia feriado
  FEdicion := cEdicionInsertar;
  lblShow_Inserta_Modifica.Caption := 'Agregando d�a feriado';

  Limpiar_Campos;
  deFechaFeriado.Enabled := True;

  if vcbAnio.Value > Year(Now) then begin
     deFechaFeriado.Date := StrToDate('01-01-'+Inttostr(vcbAnio.Value));
  end else begin
    deFechaFeriado.Date := Now;
        end;

  ActivarBotones(False, cGrupoToolbar);
  ActivarBotones(True,  cGrupoEdicion);
  vcbAnio.Enabled := False;
  deFechaFeriado.SetFocus;
end;

procedure TFormFeriados.sbtEliminaDiaFeriadoClick(Sender: TObject);
begin
  // aqui elimino un dia feriado

  btnAceptar.Enabled := False;
  vcbAnio.Enabled := False;

  if (FFechaDiaFeriado <= Date) then begin
    MsgBox(MSG_ELIMINAR_NOK, Format(MSG_CAPTION_ELIMINAR,[STR_DIAFERIADO]));
		btnAceptar.Enabled := True;
    vcbAnio.Enabled := True;
		Exit;
  end;

	Screen.Cursor := crHourGlass;
	if MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_DIAFERIADO]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
      DMConnections.BaseBO_Rating.BeginTrans;
      with spPlanTarifarioDiasFeriados_Eliminar do begin
        Parameters.CreateParameter('@FechaFeriado', ftSmallint, pdInput, 2, 0);
        Parameters.CreateParameter('@Descripcion', ftString, pdInput, 60, 0);
        Parameters.CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 10, 0);
        Parameters.Refresh;
        Parameters.ParamByName('@FechaFeriado').Value := FFechaDiaFeriado;
        Parameters.ParamByName('@Descripcion').Value := FDescripcion;
        ExecProc;

        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
        begin
          DMConnections.BaseBO_Rating.CommitTrans;
          MsgBox(MSG_ELIMINAR_OK, Format(MSG_CAPTION_ELIMINAR,[STR_DIAFERIADO]));
        end
        else
        begin
          raise exception.Create(MSG_ELIMINAR_ERROR);
        end;
        PlanTarifarioDiasFeriados_Obtener;
      end;
		Except
			On E: Exception do begin
				if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_DIAFERIADO]), E.Message, Format(MSG_CAPTION_ELIMINAR,[STR_DIAFERIADO]), MB_ICONSTOP);
			end;
		end;
	end;
	//CargarAnios;
  vcbAnio.Enabled := True;
	Screen.Cursor := crDefault;
end;

procedure TFormFeriados.vcbAnioChange(Sender: TObject);
begin
  PlanTarifarioDiasFeriados_Obtener;
end;

procedure TFormFeriados.BtnAceptarClick(Sender: TObject);
var											//TASK_113_JMA_20170313
    ok : Boolean;                           //TASK_113_JMA_20170313
begin
    ok:= False;                              //TASK_113_JMA_20170313
  btnAceptar.Enabled := False;

  if not ValidateControlsCN(
                [deFechaFeriado, deFechaFeriado, tedDescripcion],       //TASK_103_JMA_20160126
                [((deFechaFeriado.Date > Date) or (deFechaFeriado.IsEmpty)), (year(deFechaFeriado.Date) <= (Year(Now)+1)), Trim(tedDescripcion.Text) <> ''],  //TASK_103_JMA_20160126
                MSG_CAPTION_VALIDAR,
                [MSG_ERROR_FECHA_INCORRECTA, MSG_ERROR_ANIO_INCORRECTO, MSG_ERROR_DESCRIPCION_VACIA], Self) then begin      //TASK_103_JMA_20160126
		btnAceptar.Enabled := True;
		Exit;
  end;

 	Screen.Cursor := crHourGlass;
  if FEdicion = cEdicionInsertar then begin
    if spPlanTarifarioDiasFeriados_Obtener.Locate('FechaFeriado', deFechaFeriado.Text, []) then begin
      MsgBox(MSG_AGREGA_EXISTE, MSG_AGREGA_CAPTION);
      btnAceptar.Enabled := True;
      Screen.Cursor := crDefault;
      deFechaFeriado.SetFocus;
      Exit;
    end else begin

    try
        DMConnections.BaseBO_Rating.BeginTrans;
        with spPlanTarifarioDiasFeriados_Agregar do begin
          Close;
          Parameters.CreateParameter('@FechaFeriado', ftSmallint, pdInput, 2, 0);
          Parameters.CreateParameter('@Descripcion', ftString, pdInput, 60, 0);
          Parameters.CreateParameter('@UsuarioCreacion', ftString, pdInput, 20, 0);
          Parameters.CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 10, 0);
          Parameters.Refresh;
          Parameters.ParamByName('@FechaFeriado').Value := deFechaFeriado.Date;
          Parameters.ParamByName('@Descripcion').Value := tedDescripcion.Text;
          Parameters.ParamByName('@UsuarioCreacion').Value := UsuarioSistema;
          ExecProc;

          if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
          begin
            DMConnections.BaseBO_Rating.CommitTrans;
            MsgBox(MSG_AGREGA_OK, MSG_AGREGA_CAPTION);
          end else begin
            raise exception.Create(MSG_AGREGA_ERROR);
          end;
        end;
        vcbAnioChange(vcbAnio);
        PlanTarifarioDiasFeriados_Obtener;
        ok := True;                 //TASK_113_JMA_20170313
      except
        On E: EDataBaseError do
        begin
          if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
          MsgBox(MSG_AGREGA_ERROR + ': ' + E.Message, MSG_AGREGA_CAPTION);
        end;
        On E: Exception do
        begin
          if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
          MsgBox(E.Message, MSG_AGREGA_CAPTION);
        end;
      end;
    end;
  end else if FEdicion = cEdicionEditar	then begin
        try
      DMConnections.BaseBO_Rating.BeginTrans;
      with spPlanTarifarioDiasFeriados_Actualizar do begin
        Close;
        Parameters.CreateParameter('@FechaFeriado', ftSmallint, pdInput, 2, 0);
        Parameters.CreateParameter('@Descripcion', ftString, pdInput, 60, 0);
        Parameters.CreateParameter('@UsuarioModificacion', ftString, pdInput, 20, 0);
        Parameters.CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 10, 0);
        Parameters.Refresh;
        Parameters.ParamByName('@FechaFeriado').Value := FFechaDiaFeriado;
        Parameters.ParamByName('@Descripcion').Value := tedDescripcion.Text;
        Parameters.ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
        ExecProc;

        if (Parameters.ParamByName('@RETURN_VALUE').Value = 0) then
        begin
          DMConnections.BaseBO_Rating.CommitTrans;
          MsgBox(MSG_ACTUALIZAR_OK, MSG_ACTUALIZAR_CAPTION);
        end else begin
          raise exception.Create(MSG_ACTUALIZAR_ERROR);
            end;
      end;
      PlanTarifarioDiasFeriados_Obtener;
      ok := True;                       //TASK_113_JMA_20170313
        except
	    On E: EDataBaseError do
      begin
        if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
        MsgBox(MSG_ACTUALIZAR_ERROR + ': ' + E.Message, MSG_ACTUALIZAR_CAPTION);
		  end;
      On E: Exception do
      begin
        if DMConnections.BaseBO_Rating.InTransaction then DMConnections.BaseBO_Rating.RollbackTrans;
        MsgBox(MSG_ACTUALIZAR_ERROR + ': ' + E.Message, MSG_ACTUALIZAR_CAPTION);
      end;
            end;
        end;

    	Screen.Cursor 	   := crDefault;
{INICIO: TASK_113_JMA_20170313
  if (FEdicion = cEdicionEditar) then begin
    if spPlanTarifarioDiasFeriados_Obtener.Locate('FechaFeriado', FFechaFeriado, []) then begin
      FSelectedIndexdbl := dblListaDiasFeriados.SelectedIndex;
    end;
  end;
  if FEdicion <> cEdicionInsertar then begin
    FEdicion := cEdicionBrowse;
    ActivarBotones(True, cGrupoToolbar);
    ActivarBotones(True, cGrupoSalir);
    ActivarBotones(False,  cGrupoEdicion);
    dblListaDiasFeriados.Enabled := True;
    dblListaDiasFeriados.OnClick(dblListaDiasFeriados);
    vcbAnio.Enabled := True;
  end else begin
    btnAceptar.Enabled := True;
    tedDescripcion.Text := '';
    deFechaFeriado.SetFocus;
    end;
}
    if OK then
    begin
    
        if (FEdicion = cEdicionEditar) then begin
            if spPlanTarifarioDiasFeriados_Obtener.Locate('FechaFeriado', FFechaFeriado, []) then 
                FSelectedIndexdbl := dblListaDiasFeriados.SelectedIndex;
        end;        
    
        ActivarBotones(True, cGrupoToolbar);
        ActivarBotones(True, cGrupoSalir);
        ActivarBotones(False,  cGrupoEdicion);
        dblListaDiasFeriados.Enabled := True;
        dblListaDiasFeriados.OnClick(dblListaDiasFeriados);
        vcbAnio.Enabled := True;        
        FEdicion := cEdicionBrowse;
    end;
{TERMINO: TASK_113_JMA_20170313}
end;

procedure TFormFeriados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormFeriados.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormFeriados.dblListaDiasFeriadosClick(Sender: TObject);
begin

  if spPlanTarifarioDiasFeriados_Obtener.RecordCount > 0 then begin

    FFechaDiaFeriado := (Sender as TDBListEx).DataSource.DataSet.FieldByName('FechaFeriado').Value;
    FDescripcion := (Sender as TDBListEx).DataSource.DataSet.FieldByName('Descripcion').Value;

    FFechaFeriado := (Sender as TDBListEx).DataSource.DataSet.FieldByName('FechaFeriado').Value;
  end;

//  deFechaFeriado.Date := FFechaDiaFeriado;
//  tedDescripcion.Text := FDescripcion;

end;

procedure TFormFeriados.ActivarBotones;
begin
	case Grupo of
    	cGrupoToolbar :
        	begin
        		sbtAgregaDiaFeriado.Enabled  := EsActivar;
        		sbtEliminaDiaFeriado.Enabled  := EsActivar;
            sbtActualizaDiaFeriado.Enabled := EsActivar;
        	end;

    	cGrupoSalir :
        	begin
            //btnSalir.Enabled := EsActivar;
            //Notebook.PageIndex := 0;
        	end;

      cGrupoEdicion :
        	begin
            //Notebook.PageIndex := 1;
        		btnAceptar.Enabled	:= EsActivar;
        		btnCancelar.Enabled := EsActivar;
            pnlEdicion.Visible  := EsActivar;
            pnlEdicion.Enabled	:= EsActivar;

            //if EsActivar and (FEdicion <> cEdicionEditar) then spPlanTarifarioDiasFeriados_Obtener;

        	end;

      cGrupoBrowse :
        	begin
            sbtEliminaDiaFeriado.Enabled := EsActivar;
          end;
    end;

end;

procedure TFormFeriados.PlanTarifarioDiasFeriados_Obtener;
begin
  try
    if spPlanTarifarioDiasFeriados_Obtener.Active then spPlanTarifarioDiasFeriados_Obtener.Close;

      spPlanTarifarioDiasFeriados_Obtener.DisableControls;
      with spPlanTarifarioDiasFeriados_Obtener do begin
          Close;
          Parameters.Clear;
          Parameters.CreateParameter('@Anio', ftInteger, pdInput, 10, 0);
          Parameters.Refresh;
          Parameters.ParamByName('@Anio').Value := vcbAnio.Value;
          Open;

          if not Eof then begin
            lblShowTotalRegistros.Caption := IntToStr(RecordCount);
            ActivarBotones(True, cGrupoToolbar);
          end else begin
            lblShowTotalRegistros.Caption := '0';
            sbtActualizaDiaFeriado.Enabled := False;
            sbtEliminaDiaFeriado.Enabled := False;
          end;
      end;

      dblListaDiasFeriados.Refresh;
      dblListaDiasFeriados.Show;
      spPlanTarifarioDiasFeriados_Obtener.EnableControls;
      if (FEdicion = cEdicionBrowse) and (not spPlanTarifarioDiasFeriados_Obtener.Eof) then begin
        dblListaDiasFeriados.OnClick(dblListaDiasFeriados);
      end;
  except
    On E: Exception do begin
      MsgBox(E.message, 'Error', MB_ICONSTOP);
      end;
  end;
end;



end.
