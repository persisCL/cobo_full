{********************************** File Header ********************************
File Name   : MainForm.pas
Author      :
Date Created:
Language    : ES-AR
Description : Admin

Revision 1:
Date: 07-03-2008
Author: lcanteros
Description: Se agrega la funcionalidad del ABM Tipos de Clientes

Revision 2:
Date: 08-05-2008
Author: dAllegretti
Description:  Se agrega la funcionalidad del ABM Tarifas Infractores.
              Se agrega la funcionalidad de seguridad (Acceso restingido por permisos).

Revision 3:
Author : pdominguez
Date   : 18/06/2009
Description : SS 809
        - Se a�ade en el men� principal, dentro del apartado Tablas, un nuevo
        submenu "Medios y Formas Pago", conteniendo los ABM de "Formas de Pago"
        y "Tipos de Medio de Pago".

Revision 4
Author:			mbecerra
Date:			18-Junio-2010
Description:    (Ref Fase 2)
            	Se a�ade el men� de Concesionarias, para el ABM de Concesionarias
                y Estacionamientos

Revision 	: 5
Author		: Nelson Droguett Sierra
Date		: 22-Junio-2010
Description	: (Ref Fase 2)
              Se a�ade el men� de Sub Tipos Ordenes de Servicios, para el ABM de
              SubTipos de Ordenes de Servicio

Revision    : 6
Author      : mbecerra
Date        : 01-Febrero-2011
Description :       (Ref Fase 2)
                Se agrega el ABM de Clientes a Facturar Otras Concesionarias
                
Firma       : SS-1006-NDR-20120712
Description : Se crea el ABM Motivos Inhabilitacion

Firma       : SS-1006-GVI-20120718
Description : Se crea el ABM Motivos Restriccion Adhesion PA

Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)

Firma       :   SS_925_MBE_20131223
Description :   Se cambia la ventana de Acerca de.

Autor       :   CQuezadaI
Fecha       :   21 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se deshabilita el men� CrearnuevacontraseadeBasedeDatos1 ya que no se usa
                y est� deshabilitada por c�digo.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de menues segun concesionaria nativa

Firma       : SS_1436_NDR_20151230
Descripcion : Dejar un registro en RegistroOperaciones cuando el usuario sale del sistema  o cuando trata de ingresar con password erronea

Firma		: TASK_082_MGO_20161109
Descripci�n : Se agrega ABM Patentes Conocidas
*******************************************************************************}
unit MainForm;

interface

uses
  //Admintraci�n
  DMConnection,
  Navigator,
  Util,
  UtilDB,
  UtilProc,
  ParamGen,
  PeaTypes,
  PeaProcs,
  RStrings,
  CACPROCS,
  ConstParametrosGenerales,
  ABMBodegas,
  ABMEntidades,
  ABMVehiculosTipos,
  ABMModelosDeMarcas,
  ABMTarjetasCreditoTipos,
  ABMFuentesSolicitud,
  ABMMensajes,
  ABMCombinacionLetrasPatente,
  ABMLetrasDigitoVerificador,
  ABMFuentesOrigenContacto,
  ABMMaestroMensajesComprobantes,
  ABMTiposOrdenServicio,
  //Rev.5 / 22-Junio-2010 / Nelson Droguett Sierra
  ABMSubTiposOrdenServicio,
  ABMCheckList,
  ABMMarcas,
  ABMBancos,
  ABMTransportistas,
  ABMPos,
  ABMIva,
  ABMDeclaracionesConvenios,
  ABMPuntosEntrega,
  ABMMaestroAlmacenes,
  ABMPreguntas,
  ABMComunas,
  ABMUsuarios,
  ABMPreguntasFAQ,
  AbmRegiones,
  ABMFeriados,
  ABMTasasInteres,
  FrmEntidadesBancarias,
  frmMaestroPersonal,
  FrmDescripcionesPuntosCobro,
  FrmTiposDayPass,
  frmGruposFacturacionInterfaces,
  FrmCodigosAreaTelefono,
  ArbolFAQ,
  MotivosDevolucionComp,
  DpsAbout,
  VersionesSW,
  ABMMotivosPatenteNoDetectada,
  ABMMotivosTAGNoDetectado,
  ABMMotivosCancelacionRecibos,
  ABMDenominacionesMoneda,
  ABMMotivosTransitoNoFacturable,
  ABMTramosSerbanc,
  ABMToleranciasDaypass,
  CustomAssistance,
  frmProveedoresDayPass,
  frmABMTipoMedioPago,
  frmABMFormaPago,
  frmABMConcesionarias,			//REV.4
  frmABMClientesAFacturarOtrasConcesionarias,
  frmAcercaDe,                                          //SS_925_MBE_20131223
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ImgList, ComCtrls, Menus, Login, DBTables, CambioPassword,
  Db,StdCtrls, ADODB, FrmWorkload, Buttons,  jpeg, ABMPersonasMedioPagoDup,
  XPMan,
    ABMFamiliasTarjetasCredito,
    ABMMaestroLibrosContables,
    ABMTiposComprobante,
    ABMColumnasLibrosContables,
    ABMTiposComprobantesLibrosContables,
    ABMConceptosMovimientosLibrosContables,
    FrmAbmContextMarks,
    frmABMInfraRegulParam,
    ABMTarifasPorPuntoDeCobro, //TASK17_FSI
  ABMVersionesPBL,          // TASK_006_RME_20160503
  ABMMaestroAlarmas,        // TASK_006_RME_20160503
  ABMEjesViales,            // TASK_006_RME_20160503
  ABMSentidos,              // TASK_006_RME_20160503
  ABMPuntosCobroDemoras,    // TASK_006_RME_20160503
  ABMPuntosCobro,           // TASK_006_RME_20160503
  ABMTSMCs,                 // TASK_006_RME_20160503
  ABMDomain,                // TASK_006_RME_20160503
  ABMTipoConvenio,          // TASK_004_RME_20160418
  ABMConvenioClienteTipo, LMDPNGImage,    //TASK_040_ECA_20160628
  ABMBandasHorarias,        // TASK_111_MGO_20170110
  ABMApartadosEvento,       // TASK_124_MGO_20170125
  ABMPatentesConocidas,
  ABMIPC,
  ABMMotivosSeguimientoTAGs,// TASK_131_MGO_20170203
  ABMBDReferenciaVehiculos, // TASK_138_MGO_20170214
  ABMTiposCategoria,        //TASK_094_JMA_20160111
  ABMTiposDeCasos,			//TASK_094_CFU_20170202
  {20170221 CFU TASK_095_CFU SE ELIMINA ABM SUBTIPOS DE CASOS
  ABMSubTiposDeCasos,		//TASK_095_CFU_20170202}
  ABMOrigenDeCasos,			//TASK_096_CFU_20170202
  ABMAreasAtencionDeCasos,	//TASK_097_CFU_20170202
  ABMEstadosDeCasos,		//TASK_098_CFU_20170202
  FrameDatosVehiculos;      //TASK_108_JMA_20170214
type
  TPrincipal = class(TForm)
    PageImagenes: TImageList;
    MainMenu: TMainMenu;
	mnu_salir: TMenuItem;
	mnu_ventana: TMenuItem;
    mnu_cascada: TMenuItem;
    mnu_mosaico: TMenuItem;
    N2: TMenuItem;
	mnu_siguiente: TMenuItem;
    mnu_anterior: TMenuItem;
    StatusBar: TStatusBar;
	mnu_tablas: TMenuItem;
    mnu_mantenimiento: TMenuItem;
	mnu_paramgenerales: TMenuItem;
	N1: TMenuItem;
    mnu_cambiopassword: TMenuItem;
	mnu_ayuda: TMenuItem;
    mnu_acercade: TMenuItem;
    CrearnuevacontraseadeBasedeDatos1: TMenuItem;
    N9: TMenuItem;
    mnu_mantbase: TMenuItem;
	mnu_parametros: TMenuItem;
    N10: TMenuItem;
    mnu_usuarios: TMenuItem;
    mnu_Bancos: TMenuItem;
	mnu_Comunas: TMenuItem;
    mnu_Regiones: TMenuItem;
    mnu_entesrecaud: TMenuItem;
    mnu_paisesregiones: TMenuItem;
    img_Fondo: TImage;
    MenuImagenes: TImageList;
    mnu_ver: TMenuItem;
    mnu_IconBar: TMenuItem;
    mnu_vehiculos: TMenuItem;
	mnu_MarcasVehiculo: TMenuItem;
	mnu_TiposVehiculos: TMenuItem;
    mnu_Informes: TMenuItem;
    mnu_TarifasdeDayPass: TMenuItem;
    N5: TMenuItem;
	mnu_TarjetasCreditoTipos: TMenuItem;
    mnu_ABMFuenteSolicitud: TMenuItem;
    mnu_GestionMensajes: TMenuItem;
    ArboldeFAQs1: TMenuItem;
	mnu_Preguntas: TMenuItem;
    MantenimientodeUsuarios1: TMenuItem;
	mnu_CodigosArea: TMenuItem;
    mnu_CombinacioValidaPatente: TMenuItem;
    mnu_FuentesOrigenDatos: TMenuItem;
    mnu_Archivo: TMenuItem;
    mnuPuntosEntrega: TMenuItem;
    mnu_Maestrodealmacenes: TMenuItem;
    mnu_Bodegas: TMenuItem;
    mnu_Transportistas: TMenuItem;
    MaestroMensajesComprobantes1: TMenuItem;
    mnu_ListaChequeo: TMenuItem;
    DeclaracionesenConvenios1: TMenuItem;
    MotivosdedevolucindelCorreo1: TMenuItem;
    mnuPos: TMenuItem;
    mnuIVA: TMenuItem;
    mnu_DiasFeriados: TMenuItem;
    mnuGruposFacturacionInterfaces: TMenuItem;
    Entidades1: TMenuItem;
    MediosdePagoDuplicados1: TMenuItem;
    VersionesdeSoftwaredelSistemaCentral: TMenuItem;
    mnu_MotivosPatenteNoDetectada: TMenuItem;
    mnu_MotivosTelevaNoDetectado: TMenuItem;
    mnu_DenominacionesMonedas: TMenuItem;
    mnu_MotivosCancelacionRecibos: TMenuItem;
    MotivosTrnsitoNoFacturable1: TMenuItem;
    mnu_BarraDeNavegacion: TMenuItem;
    mnu_BarraDeHotLinks: TMenuItem;
    XPManifest1: TXPManifest;
    TramosdeGastosdeCobranzaSerbanc: TMenuItem;
    ToleranciasBHTU1: TMenuItem;
    mnuClientesNQ: TMenuItem;
	Mnu_LetrasDigitoVerificador: TMenuItem;
	mnu_FamiliasTarjetasCredito: TMenuItem;
	MnuLibros: TMenuItem;
    MnuLibrosContables: TMenuItem;
    MnuTiposComprobantes: TMenuItem;
    MnuColumnasLibrosContables: TMenuItem;
    MnuTiposComprobantesLibrosContables: TMenuItem;
    MnuConceptosMovimientosLibrosContables: TMenuItem;
    mnu_ABMTipoMedioPagoForm: TMenuItem;
    mnu_ContextMarks: TMenuItem;
    MantenimientodeTiposdeClientes1: TMenuItem;
	mnu_TarifasInfractores: TMenuItem;
    mnuProveedoresDayPass: TMenuItem;
	FormasdePago1: TMenuItem;
    iposdeMediosdePago1: TMenuItem;
    mnuConcesionarias: TMenuItem;
    mnuClientesaFacturarOtrasConcesionarias: TMenuItem;
    InfraccionesParmetrosRegularizacin1: TMenuItem;
    //mnuABMTiposConvenio1: TMenuItem;                  //TASK_069_GLE_20170420
    smnuPuntosdeCobro: TMenuItem;
    abmDomains: TMenuItem;
    abmTSMCs: TMenuItem;
    abmPuntosdeCobro: TMenuItem;
    abmPuntosdeCobroDemoras: TMenuItem;
    abmSentidos: TMenuItem;
    abmEjesViales: TMenuItem;
    abmMaestroAlarmas: TMenuItem;
    abmVersionesPBL: TMenuItem;
    mnu_PlanTarifario: TMenuItem;
    mnu_AMBTarifaPuntodeCobro: TMenuItem;
    Image1: TImage;
    Image2: TImage;
    mnuABMConvenioClienteTipo: TMenuItem;
    mnuPatentesConocidas: TMenuItem;
    mnu_BandasHorarias: TMenuItem;
    mnu_IPC: TMenuItem;
    mnu_FactoresporCategoria: TMenuItem;
    mnu_MotivosSeguimientoTAGs: TMenuItem;
    mnuEstadosDeLosCasos: TMenuItem;
    mnuOrigenDeCasos: TMenuItem;
    mnuAreasDeAtencionDeCasos: TMenuItem;
    mnuTiposDeCasos: TMenuItem;                               //TASK_082_MGO_20161109
	mnu_RefVehiculos: TMenuItem;	// TASK_138_MGO_20170214
    procedure mnuClientesNQClick(Sender: TObject);
    procedure ToleranciasBHTU1Click(Sender: TObject);
    procedure TramosdeGastosdeCobranzaSerbancClick(Sender: TObject);
	procedure btn_salirClick(Sender: TObject);
	procedure mnu_paramgeneralesClick(Sender: TObject);
    procedure CascadaMosaicoAnteriorSiguiente(Sender: TObject);
    procedure mnu_cambiopasswordClick(Sender: TObject);
    procedure mnu_ventanaClick(Sender: TObject);
    procedure mnu_acercadeClick(Sender: TObject);
    procedure CrearnuevacontraseadeBasedeDatos1Click(Sender: TObject);
	procedure mnu_usuariosClick(Sender: TObject);
	procedure mnu_BancosClick(Sender: TObject);
    procedure mnu_ComunasClick(Sender: TObject);
    procedure mnu_RegionesClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
	procedure VerWorkload1Click(Sender: TObject);
	procedure mnu_IconBarClick(Sender: TObject);
    procedure mnu_TiposVehiculosClick(Sender: TObject);
	procedure mnu_MarcasVehiculoClick(Sender: TObject);
    procedure mnu_ModelosVehiculosClick(Sender: TObject);
    procedure mnu_TarjetasCreditoTiposClick(Sender: TObject);
	procedure ArboldeFAQs1Click(Sender: TObject);
	procedure mnu_PreguntasClick(Sender: TObject);
    procedure MantenimientodeUsuarios1Click(Sender: TObject);
    procedure mnu_GestionMensajesClick(Sender: TObject);
    procedure mnu_ABMFuenteSolicitudClick(Sender: TObject);
    procedure mnu_CodigosAreaClick(Sender: TObject);
    procedure mnu_CombinacioValidaPatenteClick(Sender: TObject);
    procedure mnu_FuentesOrigenDatosClick(Sender: TObject);
    procedure mnuPuntosEntregaClick(Sender: TObject);
    procedure mnu_MaestrodealmacenesClick(Sender: TObject);
    procedure mnu_BodegasClick(Sender: TObject);
    procedure mnu_TransportistasClick(Sender: TObject);
    procedure mnu_TarifasdeDayPassClick(Sender: TObject);
    procedure MaestroMensajesComprobantes1Click(Sender: TObject);
    {INICIO	: 20170221 CFU SE REEMPLAZA POR ABM TIPOS DE CASOS
    procedure iposOrdenServicio1Click(Sender: TObject);}
    procedure mnu_ListaChequeoClick(Sender: TObject);
    procedure DeclaracionesenConvenios1Click(Sender: TObject);
    procedure MotivosdedevolucindelCorreo1Click(Sender: TObject);
    procedure mnuDescripcionesDePuntosDeCobroClick(Sender: TObject);
    procedure mnuPosClick(Sender: TObject);
    procedure mnuIVAClick(Sender: TObject);
    procedure mnu_DiasFeriadosClick(Sender: TObject);
    procedure mnuTasasInteresClick(Sender: TObject);
    procedure mnuGruposFacturacionInterfacesClick(Sender: TObject);
    procedure Entidades1Click(Sender: TObject);
    procedure MediosdePagoDuplicados1Click(Sender: TObject);
    procedure VersionesdeSoftwaredelSistemaCentralClick(Sender: TObject);
    procedure mnu_MotivosPatenteNoDetectadaClick(Sender: TObject);
    procedure mnu_MotivosTelevaNoDetectadoClick(Sender: TObject);
    procedure mnu_DenominacionesMonedasClick(Sender: TObject);
    procedure mnu_MotivosCancelacionRecibosClick(Sender: TObject);
    procedure MotivosTrnsitoNoFacturable1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnu_BarraDeNavegacionClick(Sender: TObject);
    procedure mnu_BarraDeHotLinksClick(Sender: TObject);
    procedure Mnu_LetrasDigitoVerificadorClick(Sender: TObject);
    procedure mnu_FamiliasTarjetasCreditoClick(Sender: TObject);
    procedure MnuLibrosContablesClick(Sender: TObject);
    procedure MnuTiposComprobantesClick(Sender: TObject);
    procedure MnuColumnasLibrosContablesClick(Sender: TObject);
    procedure MnuTiposComprobantesLibrosContablesClick(Sender: TObject);
    procedure MnuConceptosMovimientosLibrosContablesClick(Sender: TObject);
	procedure mnu_ContextMarksClick(Sender: TObject);
    procedure MantenimientodeTiposdeClientes1Click(Sender: TObject);
    procedure mnu_TarifasInfractoresClick(Sender: TObject);
    procedure mnuProveedoresDayPassClick(Sender: TObject);
    procedure FormasdePago1Click(Sender: TObject);
    procedure iposdeMediosdePago1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mnuConcesionariasClick(Sender: TObject);
    //Rev.5 / 22-Junio-2010 / Nelson Droguett Sierra ---------------------------------
    procedure mniSubTiposOrdenServicioClick(Sender: TObject);
    procedure mnuClientesaFacturarOtrasConcesionariasClick(Sender: TObject);
    procedure InfraccionesParmetrosRegularizacin1Click(Sender: TObject);
    procedure mnuMotivosInhabilitacionClick(Sender: TObject);                   //SS-1006-NDR-20120712
    procedure mnuMotivosRestriccinAdhesinPAClick(Sender: TObject);
    //procedure mnuABMTiposConvenio1Click(Sender: TObject);                   // TASK_004_RME_20160418  //TASK_069_GLE_20170420
    procedure abmDomainsClick(Sender: TObject);                             // TASK_006_RME_20160503
    procedure abmTSMCsClick(Sender: TObject);                               // TASK_006_RME_20160503
    procedure abmPuntosdeCobroClick(Sender: TObject);                       // TASK_006_RME_20160503
    procedure abmPuntosdeCobroDemorasClick(Sender: TObject);                // TASK_006_RME_20160503
    procedure abmSentidosClick(Sender: TObject);                            // TASK_006_RME_20160503
    procedure abmEjesVialesClick(Sender: TObject);                          // TASK_006_RME_20160503
    procedure abmMaestroAlarmasClick(Sender: TObject);                      // TASK_006_RME_20160503
    procedure abmVersionesPBLClick(Sender: TObject);
    procedure mnu_AMBTarifaPuntodeCobroClick(Sender: TObject);
    // TASK_006_RME_20160503
    procedure mnuABMConvenioClienteTipoClick(Sender: TObject);
    procedure mnuPatentesConocidasClick(Sender: TObject);
    procedure mnu_BandasHorariasClick(Sender: TObject);
    procedure btntestClick(Sender: TObject);
    procedure mnu_IPCClick(Sender: TObject);
    procedure btncategoriasClick(Sender: TObject);
    procedure mnu_FactoresporCategoriaClick(Sender: TObject);
    procedure mnu_ApartadoEventoClick(Sender: TObject);
    procedure mnu_MotivosSeguimientoTAGsClick(Sender: TObject);
    procedure mnuEstadosDeLosCasosClick(Sender: TObject);
    procedure mnuOrigenDeCasosClick(Sender: TObject);
    procedure mnuAreasDeAtencionDeCasosClick(Sender: TObject);
    {20170221 CFU TASK_095_CFU SE ELIMINA ABM SUBTIPOS DE CASOS
    procedure mnuSubTiposDeCasosClick(Sender: TObject);}
    procedure mnuTiposDeCasosClick(Sender: TObject);                  //TASK_082_MGO_20161109
	procedure mnu_RefVehiculosClick(Sender: TObject);	// TASK_138_MGO_20170214
  private
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                //SS_1147_NDR_20141216
	{ Private declarations }
    procedure RefrescarBarraDeEstado;
    procedure RefrescarEstadosIconBar;
    procedure InicializarIconBar;
    procedure GuardarUserSettings;
  public
	{ Public declarations }
	function Inicializa: Boolean;
    function CambiarEventoMenu(Menu: TMenu): Boolean;                           //SS_1147_NDR_20141216
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);             //SS_1147_NDR_20141216
  end;

var
	Principal: TPrincipal;

implementation

uses ABMClientesNQ, frmAbmTiposClientes, frmABMTarifasInfractores,ABMMotivosInhabilitacion, //SS-1006-NDR-20120712
ABMMotivosRestriccionAdhesion, Frm_Startup;                                  //SS-1006-GVI-20120718

resourcestring
    PAGINA_MANTENIMIENTO    = 'Mantenimiento';
    PAGINA_TABLAS           = 'Tablas';
	MSG_TABLAS              = 'Tablas';
    MSG_HINT_TABLAS         = 'Mantenimiento de Tablas del Sistema';
    MSG_TARIFAS             = 'Tarifas';
	MSG_PAISES              = 'Paises';
	MSG_REGIONES            = 'Regiones';
	MSG_COMUNAS             = 'Comunas';
    MSG_CALLES              = 'Calles';
    MSG_TIPOS_VEHICULO      = 'Tipos de Veh�culos';
    MSG_MARCAS_VEHICULO     = 'Marcas de Veh�culos';
	MSG_MODELOS_VEHICULO    = 'Modelos de Veh�culos';
	MSG_COLORES_VEHICULO    = 'Colores de Veh�culos';
    MSG_COMBINACIONES_PATENTE   = 'Combinaciones V�lidas de Patente';
    MSG_FAQ_TITULOS         = 'T�tulos de FAQs';
    MSG_FAQ_SUBTITULOS      = 'Subt�tulos de FAQs';
    MSG_FAQ_PREGUNTAS       = 'Preguntas de FAQs';
    MSG_FAQ_ARBOL           = '�rbol de FAQs';
    MSG_GESTION_PREGUNTAS_ENCUESTA      = 'Preguntas de Encuesta';
    MSG_MANTENIMIENTO_FUENTE_SOLICITUD  = 'Fuentes de Solicitud';
    MSG_GESTION_MENSAJES        = 'Mensajes';
    MSG_PARAMETROS_SOLICITUD    = 'Par�metros de la solicitud';
    MSG_CODIGO_AREA             = 'C�digos de �rea';

	MSG_INFORMES        = 'Informes';
	MSG_HINT_INFORMES   = 'Creaci�n y ejecuci�n de Informes';
	MSG_INF_TRANSITOS_CATEGORIA = 'Tr�nsitos por Categoria';
	MSG_INF_TRANSITOS_HORARIO	= 'Tr�nsitos por Tipo de Horario';

	MSG_MANTENIMIENTO       = 'Mantenimiento';
    MSG_HINT_MANTENIMIENTO  = 'Mantenimiento de Par�metros del Sistema';
	MSG_ADMINISTRACION_USUARIOS = 'Usuarios';
	MSG_CAMBIO_CONTRASENIA  = 'Cambiar Contrase�a';
	MSG_PARAMETROS          = 'Par�metros del Sistema';

	MSG_TAREAS      = 'Tareas';
	MSG_HINT_TAREAS = 'Administraci�n de Tareas';

	MSG_TIPOSOS     = 'Tipos de Orden de Servicio';
	MSG_HINT_TIPOSO =  'Creaci�n de �rdenes de Servicio';
	MSG_NUEVA_OS    = 'Nueva';

	MSG_AREAS           = '�reas';
	MSG_GRUPOSTAREAS    = 'Grupos de Tareas';
	MSG_WORKFLOWS       = 'Workflows';
	MSG_ORDENESSERVICIO = '�rdenes de Servicio';
//Revision 2
  MSG_TARIFAS_INFRACTORES = 'Seguridad - Boleto Infractor';
  MSG_TARIFAS_INFRACTORES_MODIFICAR = 'Modificar - Boleto Infractor';
  MSG_TARIFAS_INFRACTORES_ELIMINAR = 'Eliminar - Boleto Infractor';

 const FuncionesAdicionales: Array[1..3] of TPermisoAdicional = (
      (Funcion: 'Seguridad_TarifasInfractores'; Descripcion: MSG_TARIFAS_INFRACTORES_MODIFICAR; Nivel: 1),
      (Funcion: 'Modificar_TarifasInfractores'; Descripcion: MSG_TARIFAS_INFRACTORES_MODIFICAR; Nivel: 2),
      (Funcion: 'Eliminar_TarifasInfractores'; Descripcion: MSG_TARIFAS_INFRACTORES_ELIMINAR; Nivel: 2)    );

{$R *.DFM}
{$I ..\comunes\Ambiente.inc}

procedure TPrincipal.btncategoriasClick(Sender: TObject);
var
    f: TABMCategorias;
begin
    if FindFormOrCreate(TABMCategorias, f) then
        f.Show
    else begin
        if not f.Inicializa then f.Release;
    end;
end;


procedure TPrincipal.btntestClick(Sender: TObject);
var
    f: TfrmABMBandasHorarias;
begin
    if FindFormOrCreate(TfrmABMBandasHorarias, f) then
        f.Show
    else begin
        if not f.Inicializar then f.Release;
    end;
end;


procedure TPrincipal.btn_salirClick(Sender: TObject);
begin
	Close;
end;

procedure TPrincipal.InfraccionesParmetrosRegularizacin1Click(Sender: TObject);
var
    f: TABMInfraRegulParamForm;
begin
    if FindFormOrCreate(TABMInfraRegulParamForm, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;
end;

function TPrincipal.Inicializa: Boolean;
Var
	f: TLoginForm;
    ok: boolean;
begin
    Result := False;
	Application.Title   := SYS_ADMINISTRACION_TITLE;
    SistemaActual       := SYS_ADMINISTRACION;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    CambiarEventoMenu(MainMenu);                                                                            //SS_1147_NDR_20141216

    try
        Application.CreateForm(TLoginForm, f);
        if f.Inicializar(DMConnections.BaseCAC, SYS_ADMINISTRACION) and (f.ShowModal = mrOk) then begin
			UsuarioSistema := f.CodigoUsuario;

            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
            //    Exit;
            //end;
            //
            //ok := True;
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
            ////Revision 2
            //    ok := GenerateMenuFile(Menu, SYS_ADMINISTRACION, DMConnections.BaseCAC, FuncionesAdicionales);
            //end;

            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, Menu, FuncionesAdicionales) then begin
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            ok := True;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------

            Result := False;
            if ok then begin
                CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_ADMINISTRACION);
                HabilitarPermisosMenu(Menu);

                CargarInformes(DMConnections.BaseCAC, SYS_ADMINISTRACION, MainMenu);
                RefrescarBarraDeEstado;
                CambiarEventoMenu(MainMenu);                                                                            //SS_1147_NDR_20141216
                Result := True;
                mnu_ABMFuenteSolicitud.Visible:=False; //TASK_027_ECA_20160608
            end;

            if Result then InicializarIconBar;
        end
        else                                                                    //SS_1436_NDR_20151230
        begin                                                                   //SS_1436_NDR_20151230
            UsuarioSistema := f.CodigoUsuario;                                  //SS_1436_NDR_20151230
        end;                                                                    //SS_1436_NDR_20151230
    finally
        f.Free;
        if Assigned(FormStartup) then FreeAndNil(FormStartup);
    end;
end;

procedure TPrincipal.RefrescarBarraDeEstado;
var
	UltimoPanel: Integer;
begin
	StatusBar.Panels.Items[0].Text  := Format(MSG_USER, [UsuarioSistema]);
    StatusBar.Panels[0].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[0].text));

	StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, [InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')]));
	StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));

    UltimoPanel := StatusBar.Panels.Count-1;
    StatusBar.Panels[UltimoPanel].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[UltimoPanel].text));
end;

procedure TPrincipal.mnu_paramgeneralesClick(Sender: TObject);
var
	f: TFormParam;
begin
	Application.CreateForm(TFormParam, f);
	if f.Inicializar then f.ShowModal;
	f.Release;
end;

procedure TPrincipal.CascadaMosaicoAnteriorSiguiente(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

procedure TPrincipal.mnu_cambiopasswordClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnections.BaseCAC, UsuarioSistema) then f.ShowModal;
	f.Release;
end;


procedure TPrincipal.mnu_ventanaClick(Sender: TObject);
begin
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

// *************************** Informes **************************************//
{******************************** Function Header ******************************
Function Name: mnu_acercadeClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision :
    Author : ggomez
    Date : 29/01/2007
    Description : En la llamada f.Inicializa, agregu� el segundo par�metro ''
        debido a que cambi� el perfil de la funci�n en las Utiles.
*******************************************************************************}
procedure TPrincipal.mnu_acercadeClick(Sender: TObject);
{                                                               SS_925_MBE_20131223
resourcestring
	MSG_CAPTION = 'Cliente de Administraci�n';

Var
	f: TAboutFrm;
    a: TfrmLocalAssistance;
}
begin
{                                                               SS_925_MBE_20131223
    Application.CreateForm(TfrmLocalAssistance, a);
    try
        if a.Inicializar then begin
            Application.CreateForm(TAboutFrm, f);
            if f.Inicializa(MSG_CAPTION, '', '', a) then f.ShowModal;
            f.Release;
        end;
    finally
        a.Release;
    end; // finally
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

procedure TPrincipal.mnu_AMBTarifaPuntodeCobroClick(Sender: TObject);
var
  f: TFormTarifasPorPuntoDeCobro;
begin
    if FindFormOrCreate(TFormTarifasPorPuntoDeCobro, f) then begin
      f.show;
    end else begin
      if f.Inicializa then f.Show else f.Release;
    end;
end;

// INICIO : TASK_124_MGO_20170125
procedure TPrincipal.mnu_ApartadoEventoClick(Sender: TObject);
var
    f: TfrmApartadosEvento;
begin
    if FindFormOrCreate(TfrmApartadosEvento, f) then
        f.Show
    else begin
        if f.Inicializar then f.Show else f.Release;
    end;
end;
// FIN : TASK_124_MGO_20170125

procedure TPrincipal.CrearnuevacontraseadeBasedeDatos1Click(Sender: TObject);
resourcestring
	MSG_QUESTION = 'A continuaci�n se crear� una nueva contrase�a de Base de Datos. La nueva contrase�a ser� generada al azar.' +
    	CRLF + 'Desea continuar?';
	MSG_ACCION	= 'Cambio de contrase�a de Base de Datos';
    MSG_DETALLE_ERROR = 'No se ha podido cambiar la contrase�a de la Base de Datos';
    MSG_NO_IMPLEMENTADO = 'No implementado';

Var
	DescriError: AnsiString;
begin
	if MsgBox( MSG_QUESTION, MSG_ACCION,
	  MB_YESNO or MB_ICONWARNING) <> IDYES then Exit;
//	if ChangeDBSecurity(BaseGestion, DescriError) then begin
//		MsgBox('La nueva contrase�a de la Base de Datos se ha generado ' +
//		  'correctamente.', 'Cambio de contrase�a de Base de Datos',
//		  MB_ICONINFORMATION);
//		BaseGestion.Close;
//	end else begin
		DescriError := MSG_NO_IMPLEMENTADO;
		MsgBoxErr( MSG_DETALLE_ERROR, DescriError, MSG_ACCION, MB_ICONSTOP);
//	end;
end;


procedure TPrincipal.mnu_usuariosClick(Sender: TObject);
Var
	f: TFormAbmUsuarios;
begin
	if FindFormOrCreate(TFormAbmUsuarios, f) then
    	f.Show
    else begin
    	if not f.Inicializa then f.Release;
    end;
end;

procedure TPrincipal.mnu_BancosClick(Sender: TObject);
var
	f: TFormBancos;
begin
    if FindFormOrCreate(TFormBancos, f) then
        f.Show
    else begin
        if not f.Inicializar then f.Release;
    end;
end;

// INICIO : TASK_111_MGO_20170110
procedure TPrincipal.mnu_BandasHorariasClick(Sender: TObject);
var
    f: TfrmABMBandasHorarias;
begin
    if FindFormOrCreate(TfrmABMBandasHorarias, f) then
        f.Show
    else begin
        if not f.Inicializar then f.Release;
    end;
end;
// FIN : TASK_111_MGO_20170110

procedure TPrincipal.mnu_ComunasClick(Sender: TObject);
var
	f: TFormComunas;
begin
    if FindFormOrCreate(TFormComunas, f) then
        f.Show
    else begin
        if not f.Inicializa(True) then f.Release;
    end;
end;

{******************************** Function Header ******************************
Function Name: mnu_ContextMarksClick
Author : lgisuk
Date Created : 05/12/2005
Description :  Abre el form para administrar los context marks...
Parameters : Sender: TObject;
Return Value : None
*******************************************************************************}
procedure TPrincipal.mnu_ContextMarksClick(Sender: TObject);
var
    f: TFAbmContextMarks;
begin
    if FindFormOrCreate(TFAbmContextMarks, f) then begin
        f.Show;
    end else begin
       if f.Inicializar(f.Caption, True) then f.Show else f.Release;
    end;
end;

// INICIO : TASK_138_MGO_20170214
procedure TPrincipal.mnu_RefVehiculosClick(Sender: TObject);
var
    f: TfrmABMBDReferenciaVehiculos;
begin
    if FindFormOrCreate(TfrmABMBDReferenciaVehiculos, f) then
        f.Show
    else begin
        if not f.Inicializar then f.Release;
    end;
end;
// FIN : TASK_138_MGO_20170214

procedure TPrincipal.mnu_RegionesClick(Sender: TObject);
var
	f: TFormRegiones;
begin
	if FindFormOrCreate(TFormRegiones, f) then
		f.Show
	else begin
		if not f.Inicializa(True) then f.Release;
	end;
end;

procedure TPrincipal.FormPaint(Sender: TObject);
begin
  //	DrawMDIBackground(img_Fondo.Picture.Graphic);
end;

procedure TPrincipal.VerWorkload1Click(Sender: TObject);
Var
	f: TFormWorkload;
begin
	if findFormOrCreate(TFormWorkload, f) then
		f.Show
	else begin
		if not f.Inicializa then f.Release;
	end;
end;

procedure TPrincipal.mnu_IconBarClick(Sender: TObject);
begin
	GNavigator.iconBarVisible := not GNavigator.iconBarVisible;
    mnu_IconBar.Checked		 := GNavigator.iconBarVisible;
end;

procedure TPrincipal.mnu_IPCClick(Sender: TObject);
var
	f: TFormIPC;
begin
	if FindFormOrCreate(TFormIPC, f) then
		f.Show
	else begin
		if f.Inicializa then f.Show else f.Release;
	end;
end;

procedure TPrincipal.Mnu_LetrasDigitoVerificadorClick(Sender: TObject);
var
	f: TFormABMLetrasDigitoVerificador;
begin
	if FindFormOrCreate(TFormABMLetrasDigitoVerificador, f) then
		f.Show
	else begin
		if f.Inicializa then f.Show else f.Release;
	end;
end;

procedure TPrincipal.mnu_TiposVehiculosClick(Sender: TObject);
var
	f: TFormVehiculosTipos;
begin
	if FindFormOrCreate(TFormVehiculosTipos, f) then
		f.Show
	else begin
		if f.Inicializa then f.Show else f.Release;
	end;
end;

procedure TPrincipal.mnu_MarcasVehiculoClick(Sender: TObject);
var
	f: TFormMarcas;
begin
	if FindFormOrCreate(TFormMarcas, f) then
		f.Show
	else begin
		if f.Inicializa(True) then f.Show else f.Release;
	end;
end;

procedure TPrincipal.mnu_ModelosVehiculosClick(Sender: TObject);
var
    f: TFormModelosDeMarcas;
begin
	if FindFormOrCreate(TFormModelosDeMarcas, f) then
		f.Show
	else begin
		if not f.Inicializa(True) then f.Release;
	end;
end;

procedure TPrincipal.mnu_TarjetasCreditoTiposClick(Sender: TObject);
var
	f: TFormTarjetasCreditoTipo;
begin
	if FindFormOrCreate(TFormTarjetasCreditoTipo, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmDomainsClick(Sender: TObject);
var
  f : TfrmABMDomain;
begin

  if FindFormOrCreate(TfrmABMDomain, f) then
        f.Show
   else if not f.Inicializar(True) then
       f.Release;
end;
//Fin TASK_006_RME_20160503


//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmEjesVialesClick(Sender: TObject);
var
	f: TfrmABMEjesViales;
begin

	if FindFormOrCreate(TfrmABMEjesViales, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;

end;
//Fin TASK_006_RME_20160503

//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmMaestroAlarmasClick(Sender: TObject);
var
	f: TfrmABMMaestroAlarmas;
begin

	if FindFormOrCreate(TfrmABMMaestroAlarmas, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;

end;
//Fin TASK_006_RME_20160503


//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmPuntosdeCobroClick(Sender: TObject);
var
	f: TfrmABMPuntosCobro;
begin

	if FindFormOrCreate(TfrmABMPuntosCobro, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;

end;
//Fin TASK_006_RME_20160503


//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmPuntosdeCobroDemorasClick(Sender: TObject);
var
	f: TfrmABMPuntosCobroDemoras;
begin

	if FindFormOrCreate(TfrmABMPuntosCobroDemoras, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;

end;
//Fin TASK_006_RME_20160503

//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmSentidosClick(Sender: TObject);
var
	f: TfrmABMSentidos;
begin

	if FindFormOrCreate(TfrmABMSentidos, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;

end;
//Fin TASK_006_RME_20160503


//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmTSMCsClick(Sender: TObject);
var
	f: TfrmABMTSMCs;
begin

	if FindFormOrCreate(TfrmABMTSMCs, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;
end;
//Fin TASK_006_RME_20160503

//Inicio TASK_006_RME_20160503
procedure TPrincipal.abmVersionesPBLClick(Sender: TObject);
var
	f: TfrmABMVersionesPBL;
begin
	if FindFormOrCreate(TfrmABMVersionesPBL, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;

end;
//Fin TASK_006_RME_20160503

procedure TPrincipal.ArboldeFAQs1Click(Sender: TObject);
var
	f: TfrmArbolFAQ;
begin
	if FindFormOrCreate(TfrmArbolFAQ, f) then
  		f.Show
	else begin
	  	if not f.Inicializar(True) then f.Release;
	end;
end;


procedure TPrincipal.mnu_PreguntasClick(Sender: TObject);
var
	f:TFormPreguntas;
begin
	if FindFormOrCreate(TFormPreguntas, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;
{-----------------------------------------------------------------------------
  Procedure: MantenimientodeTiposdeClientes1Click
  Author:    lcanteros
  Date:      6-Mar-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Inicia el form de tipos de clientes
-----------------------------------------------------------------------------}
procedure TPrincipal.MantenimientodeTiposdeClientes1Click(Sender: TObject);
var
	f: TABMTiposClientesForm;
begin
	if FindFormOrCreate(TABMTiposClientesForm, f) then
		f.Show
	else begin
        //TASK_002_AUN_20170309-CU.COBO.ADM.COB.103
		if not f.Inicializar(true, false) then f.Release;
	end;

end;

procedure TPrincipal.MantenimientodeUsuarios1Click(Sender: TObject);
var
	f: TFormMaestroPersonal;
begin
	if FindFormOrCreate(TFormMaestroPersonal, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TPrincipal.mnu_GestionMensajesClick(Sender: TObject);
var
	f: TFormABMMensajes;
begin
	if FindFormOrCreate(TFormABMMensajes, f) then
		f.Show
	else begin
		if not f.Inicializa then f.Release;
	end;
end;


//INICIO	: TASK_094_CFU_20170202
procedure TPrincipal.mnuTiposDeCasosClick(Sender: TObject);
var
	f: TFABMTiposDeCasos;
begin
	if FindFormOrCreate(TFABMTiposDeCasos, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;
//FIN	: TASK_094_CFU_20170202

{20170221 CFU TASK_095_CFU SE ELIMINA ABM SUBTIPOS DE CASOS
//INICIO:	TASK_095_CFU_20170202
procedure TPrincipal.mnuSubTiposDeCasosClick(Sender: TObject);
var
	f: TFABMSubTiposDeCasos;
begin
	if FindFormOrCreate(TFABMSubTiposDeCasos, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;
//FIN:		TASK_095_CFU_20170202
20170221 CFU TASK_095_CFU SE ELIMINA ABM SUBTIPOS DE CASOS}

//INICIO:	TASK_096_CFU_20170202
procedure TPrincipal.mnuOrigenDeCasosClick(Sender: TObject);
var
	f: TFABMOrigenDeCasos;
begin
	if FindFormOrCreate(TFABMOrigenDeCasos, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;
//FIN:	TASK_096_CFU_20170202

//INICIO	: TASK_097_CFU_20170202
procedure TPrincipal.mnuAreasDeAtencionDeCasosClick(Sender: TObject);
var
	f: TFABMAreasAtencionDeCasos;
begin
	if FindFormOrCreate(TFABMAreasAtencionDeCasos, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;
//FIN:	TASK_097_CFU_20170202

//INICIO:	TASK_098_CFU_20170202
procedure TPrincipal.mnuEstadosDeLosCasosClick(Sender: TObject);
var
	f: TFABMEstadosDeCasos;
begin
	if FindFormOrCreate(TFABMEstadosDeCasos, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;
//FIN:	TASK_098_CFU_20170202

procedure TPrincipal.mnu_ABMFuenteSolicitudClick(Sender: TObject);
var
	f: TFormFuentesSolicitud;
begin
	if FindFormOrCreate(TFormFuentesSolicitud, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;
end;

procedure TPrincipal.mnu_CodigosAreaClick(Sender: TObject);
var
	f: TFormCodigosAreaTelefono;
begin
	if FindFormOrCreate(TFormCodigosAreaTelefono, f) then
		f.Show
	else begin
		if not f.InicializaR then f.Release;
	end;
end;

procedure TPrincipal.mnu_CombinacioValidaPatenteClick(Sender: TObject);
var
	f: TFormABMCombinacionLetrasPatente;
begin
	if FindFormOrCreate(TFormABMCombinacionLetrasPatente, f) then
		f.Show
	else begin
		if f.Inicializa then f.Show else f.Release;
	end;
end;

{INICIO: TASK_094_JMA_20160111}
procedure TPrincipal.mnu_FactoresporCategoriaClick(Sender: TObject);
var
	f: TABMCategorias;
begin
	if FindFormOrCreate(TABMCategorias, f) then
		f.Show
	else begin
		if not f.Inicializa then f.Release;
	end;
end;
{TERMINO: TASK_094_JMA_20160111}

procedure TPrincipal.mnu_FamiliasTarjetasCreditoClick(Sender: TObject);
var
	f: TFormABMFamiliasTarjetasCredito;
begin
	if FindFormOrCreate(TFormABMFamiliasTarjetasCredito, f) then
		f.Show
	else begin
		if not f.Inicializa then f.Release;
	end;
end;

procedure TPrincipal.mnu_FuentesOrigenDatosClick(Sender: TObject);
var
	f: TFrmFuentesOrigenContacto;
begin
	if FindFormOrCreate(TFrmFuentesOrigenContacto, f) then
		f.Show
	else begin
		if f.Inicializa then f.Show else f.Release;
	end;
end;

procedure TPrincipal.mnuPuntosEntregaClick(Sender: TObject);
var
	f: TFormABMPuntosEntrega;
begin
	if FindFormOrCreate(TFormABMPuntosEntrega, f) then begin
        f.Inicializar('Mantenimiento de Puntos de Entrega', True);
    end
	else begin
		if not f.Inicializar('Mantenimiento de Puntos de Entrega', True) then f.Release;
	end;
end;

procedure TPrincipal.mnu_MaestrodealmacenesClick(Sender: TObject);
var
	f: TFormABMMaestroAlmacenes;
begin
	if FindFormOrCreate(TFormABMMaestroAlmacenes, f) then begin
        f.Inicializa(mnu_Maestrodealmacenes.Caption, True);
    end
	else begin
		if not f.Inicializa(mnu_Maestrodealmacenes.Caption, True) then f.Release;
	end;
end;

procedure TPrincipal.mnu_BodegasClick(Sender: TObject);
var
	f: TFABMBodegas;
begin
	if FindFormOrCreate(TFABMBodegas, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;

procedure TPrincipal.mnu_TransportistasClick(Sender: TObject);
var
	f: TFABMTransportes;
begin
	if FindFormOrCreate(TFABMTransportes, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;

procedure TPrincipal.mnu_TarifasdeDayPassClick(Sender: TObject);
var
	f: TFormTiposDayPass;
begin
	if FindFormOrCreate(TFormTiposDayPass, f) then
		f.Show
	else begin
		if f.Inicializar(True) then f.Show else f.Release;
	end;
end;

procedure TPrincipal.mnu_TarifasInfractoresClick(Sender: TObject);
var
    f: TABMTarifasInfractoresForm;
begin
    if FindFormOrCreate(TABMTarifasInfractoresForm, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;
end;

procedure TPrincipal.MaestroMensajesComprobantes1Click(Sender: TObject);
var
    f: TFormABMMaestroMensajesComprobantes;
begin
    if FindFormOrCreate(TFormABMMaestroMensajesComprobantes, f) then
		f.Show
	else begin
		if f.Inicializar(True) then f.Show else f.Release;
	end;
end;

//INICIO: TASK_069_GLE_20170420 Elimina Opci�n de Men�
{
//Inicio TASK_004_RME_20160418
procedure TPrincipal.mnuABMTiposConvenio1Click(Sender: TObject);
var
  f : TfrmABMTipoConvenio;
begin

  if FindFormOrCreate(TfrmABMTipoConvenio, f) then
        f.Show
   else if not f.Inicializar(True) then
       frmABMTipoConvenio.Release;

end;
// Fin TASK_004_RME_20160418
}
//TERMINO: TASK_069_GLE_20170420

procedure TPrincipal.iposdeMediosdePago1Click(Sender: TObject);
var
	f: TABMTipoMedioPagoForm;
begin
	if FindFormOrCreate(TABMTipoMedioPagoForm, f) then
		f.Show
	else begin
		if not f.Inicializar then f.Release;
	end;

end;

{INICIO	: 20170221 CFU SE REEMPLAZA POR ABM TIPOS DE CASOS
procedure TPrincipal.iposOrdenServicio1Click(Sender: TObject);
var
    f: TFormABMTiposOrdenServicio;
begin
    if FindFormOrCreate(TFormABMTiposOrdenServicio, f) then
		f.Show
	else begin
		if f.Inicializar(True) then f.Show else f.Release;
	end;
end;
TERMINO : 20170221 CFU SE REEMPLAZA POR ABM TIPOS DE CASOS}

procedure TPrincipal.mnu_ListaChequeoClick(Sender: TObject);
var
    f: TFrmCheckList;
begin
	if FindFormOrCreate(TFrmCheckList, f) then
		f.Show
	else begin
		if f.Inicializar then f.Show else f.Release;
	end;
end;

procedure TPrincipal.DeclaracionesenConvenios1Click(Sender: TObject);
var
    f: TFABMDeclaracionesConvenios;
begin
    if FindFormOrCreate(TFABMDeclaracionesConvenios, f) then begin
        f.Show;
    end else begin
       if f.Inicializar(f.Caption, True) then f.Show else f.Release;
    end;
end;

procedure TPrincipal.MotivosdedevolucindelCorreo1Click(Sender: TObject);
var
    f: TfrmMotivosDevComp;
begin
    if FindFormOrCreate(TfrmMotivosDevComp, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;
end;

procedure TPrincipal.mnuConcesionariasClick(Sender: TObject);
var
	f : TABMConcesionariasForm;
begin

    if FindFormOrCreate(TABMConcesionariasForm, f) then begin
        f.Show;
    end else begin
       if f.Inicializar() then f.Show else f.Release;
    end;
end;

procedure TPrincipal.mnuDescripcionesDePuntosDeCobroClick(Sender: TObject);
//var
//    f: TFDescripcionesPuntosCobro;
begin
//    if FindFormOrCreate(TFDescripcionesPuntosCobro, f) then begin
//        f.Show;        
//    end else begin
//       if f.Inicializar(f.Caption, True) then f.Show else f.Release;
//    end;
end;

// Inicio Bloque : SS-1006-NDR-20120712
procedure TPrincipal.mnuMotivosInhabilitacionClick(Sender: TObject);
var
    f: TFormMotivosInhabilitacion;
begin
    if FindFormOrCreate(TFormMotivosInhabilitacion, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;
// Fin Bloque : SS-1006-NDR-20120712

// Inicio Bloque : SS-1006-GVI-20120718
procedure TPrincipal.mnuMotivosRestriccinAdhesinPAClick(Sender: TObject);
var
    f: TFormMotivosRestriccionAdhesion;
begin
     if FindFormOrCreate(TFormMotivosRestriccionAdhesion, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;

// Fin Bloque : SS-1006-GVI-20120718

// INICIO : TASK_082_MGO_20161109
procedure TPrincipal.mnuPatentesConocidasClick(Sender: TObject);
var
    f: TFormABMPatentesConocidas;
begin
    if FindFormOrCreate(TFormABMPatentesConocidas, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;
// FIN : TASK_082_MGO_20161109

procedure TPrincipal.mnuPosClick(Sender: TObject);
var
    f: TFABMPos;
begin
    if FindFormOrCreate(TFABMPos, f) then begin
        f.Show;
    end else begin
       if f.Inicializar(f.Caption, True) then f.Show else f.Release;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: mnuProveedoresDayPassClick
  Author:    mbecerra
  Date Created: 26-Agosto-2008
  Description: invoca al Form de Proveedores DayPass
-----------------------------------------------------------------------------}
procedure TPrincipal.mnuProveedoresDayPassClick(Sender: TObject);
resourcestring
    MSG_PD_CAPTION = 'Administraci�n de Proveedores DayPass';

var
	f : TProveedoresDayPassForm;
begin
    if FindFormOrCreate(TProveedoresDayPassForm, f) then f.Show
    else begin
    	if f.Inicializa(MSG_PD_CAPTION) then f.Show
    	else f.Release;
    end;

end;

procedure TPrincipal.mnuIVAClick(Sender: TObject);
var
    f: TForm_ABM_IVA;
begin
    if FindFormOrCreate(TForm_ABM_IVA,f) then
        f.Show
    else  if  f.inicializar then f.Show else f.Release;
end;

{-----------------------------------------------------------------------------
  Function Name: MnuLibrosContables
  Author:    FSandi
  Date Created: 19-06-2007
  Description: ABM de Libros Contables
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.MnuLibrosContablesClick(Sender: TObject);
var
    f: TFormABMMaestroLibrosContables;
begin
    if FindFormOrCreate(TFormABMMaestroLibrosContables, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;

procedure TPrincipal.mnu_DiasFeriadosClick(Sender: TObject);
var
    f: TFormFeriados;
begin
    if FindFormOrCreate(TFormFeriados, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnuTasasInteresClick
  Author:    lgisuk
  Date Created: 25/02/2005
  Description: ABM Tasas de Interes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.mnuTasasInteresClick(Sender: TObject);
var
    f: TFAbmTasasInteres;
begin
    if FindFormOrCreate(TFAbmTasasInteres, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MnuTiposComprobantes
  Author:    FSandi
  Date Created: 19-06-2007
  Description: ABM Tipos de Comprobantes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.MnuTiposComprobantesClick(Sender: TObject);
var
    f: TFormTiposComprobante;
begin
    if FindFormOrCreate(TFormTiposComprobante, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MnuLibrosContables
  Author:    FSandi
  Date Created: 19-06-2007
  Description: ABM de Libros Contables
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.MnuTiposComprobantesLibrosContablesClick(Sender: TObject);
var
    f: TFormABMTiposComprobantesLibrosContables;
begin
    if FindFormOrCreate(TFormABMTiposComprobantesLibrosContables, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;



{-----------------------------------------------------------------------------
  Function Name: mnuGruposFacturacionInterfacesClick
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: A/B/M Grupos de Facturaci�n de Interfaces
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.mnuGruposFacturacionInterfacesClick(Sender: TObject);
var
    f: TfrmGruposFacturacionInterfaces;
begin
    if FindFormOrCreate(TfrmGruposFacturacionInterfaces, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Entidades1Click
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: A/B/M Entidades
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.Entidades1Click(Sender: TObject);
var
	f: TFABMEntidades;
begin
	if FindFormOrCreate(TFABMEntidades, f) then begin
        f.Inicializa;
    end
	else begin
		if not f.Inicializa then f.Release;
	end;
end;


procedure TPrincipal.MediosdePagoDuplicados1Click(Sender: TObject);
var
    f: TFABMPersonasMedioPagoDup;
begin
    if FindFormOrCreate(TFABMPersonasMedioPagoDup, f) then begin
        f.Show;
    end else begin
        if f.Inicializa then f.Show else f.Release;
    end;
end;

procedure TPrincipal.VersionesdeSoftwaredelSistemaCentralClick(
  Sender: TObject);
var
    f: TfrmVersionesSoftware;
begin
    if FindFormOrCreate(TfrmVersionesSoftware, f) then begin
        f.Show;
    end else begin
        if f.Inicializa then f.Show else f.Release;
    end;
end;

procedure TPrincipal.mnu_MotivosPatenteNoDetectadaClick(Sender: TObject);
var
    f: TFormABMMotivosPatenteNoDetectada;
begin
    if FindFormOrCreate(TFormABMMotivosPatenteNoDetectada, f) then begin
        f.Show;
    end else begin
       if f.Inicializar(f.Caption, True) then f.Show else f.Release;
    end;
end;

// INICIO : TASK_131_MGO_20170203
procedure TPrincipal.mnu_MotivosSeguimientoTAGsClick(Sender: TObject);
var
    f: TfrmMotivosSeguimientoTAGs;
begin
    if FindFormOrCreate(TfrmMotivosSeguimientoTAGs, f) then
        f.Show
    else begin
        if f.Inicializar then f.Show else f.Release;
    end;
end;
// FIN : TASK_131_MGO_20170203

procedure TPrincipal.mnu_MotivosTelevaNoDetectadoClick(Sender: TObject);
var
    f: TFormABMMotivosTAGNoDetectado;
begin
    if FindFormOrCreate(TFormABMMotivosTAGNoDetectado, f) then begin
        f.Show;
    end else begin
       if f.Inicializar(f.Caption, True) then f.Show else f.Release;
    end;
end;

procedure TPrincipal.mnu_DenominacionesMonedasClick(Sender: TObject);
var
    f: TFormDenominacionesMoneda;
begin
    if FindFormOrCreate(TFormDenominacionesMoneda, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;

procedure TPrincipal.mnu_MotivosCancelacionRecibosClick(Sender: TObject);
var
    F: TFormABMMotivosCancelacionRecibos;
begin
    if FindFormOrCreate(TFormABMMotivosCancelacionRecibos, F) then begin
        F.Show;
    end else begin
       if F.Inicializa then F.Show else F.Release;
    end;
end;

procedure TPrincipal.MotivosTrnsitoNoFacturable1Click(Sender: TObject);
var
    f: TfrmABMMotivosTransitosNoFacturables;
begin
    if FindFormOrCreate(TfrmABMMotivosTransitosNoFacturables, f) then begin
        f.Show;
    end else begin
       if f.Inicializar(MotivosTrnsitoNoFacturable1.Caption, True) then f.Show else f.Release;
    end;

end;

{-----------------------------------------------------------------------------
  Procedure: TPrincipal.InicializarIconBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: None
  Result:    None
  Description: Inicializa la barra de �conos.
-----------------------------------------------------------------------------}
procedure TPrincipal.InicializarIconBar;
var
    Bmp: TBitmap;
    NavigatorHasPages : Boolean;
begin
    //------------------------------------------------------------------------
	// Navegador
    //------------------------------------------------------------------------
	Bmp                         := TBitmap.Create;
	GNavigator                  := TNavigator.Create(Self);

    GNavigator.ShowBrowserBar       := GetUserSettingShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;

    GNavigator.ShowHotLinkBar   := GetUserSettingShowHotLinksBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;

    GNavigator.IconBarVisible   := False;
    NavigatorHasPages           := False;

	//----------------------------------------------------------
	// Agregamos las p�ginas e �conos HABILITADAS al toolbar
	//----------------------------------------------------------

  	// Mantenimiento
    if ExisteAcceso('mnu_usuarios') or ExisteAcceso('mnu_cambiopassword') then begin

        GNavigator.AddPage(PAGINA_MANTENIMIENTO, MSG_MANTENIMIENTO, MSG_HINT_MANTENIMIENTO);
    	NavigatorHasPages := True;

        if ExisteAcceso('mnu_usuarios') then begin
            PageImagenes.GetBitmap(6, Bmp);
            GNavigator.AddIcon(PAGINA_MANTENIMIENTO, MSG_ADMINISTRACION_USUARIOS, EmptyStr, Bmp, mnu_usuarios.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('mnu_cambiopassword') then begin
            PageImagenes.GetBitmap(7, Bmp);
            GNavigator.AddIcon(PAGINA_MANTENIMIENTO, MSG_CAMBIO_CONTRASENIA, EmptyStr, Bmp, mnu_cambiopassword.OnClick);
            Bmp.Assign(nil);
        end;
    end;

	// Tablas
    if ExisteAcceso('mnu_Regiones') or ExisteAcceso('mnu_Comunas')
            or ExisteAcceso('mnu_MarcasVehiculo') then begin

        GNavigator.AddPage(PAGINA_TABLAS, MSG_TABLAS, MSG_HINT_TABLAS);
    	NavigatorHasPages := True;

        // SUB-MENU VEHICULOS
        if ExisteAcceso('mnu_TiposVehiculos') then begin
            PageImagenes.GetBitmap(26, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_TIPOS_VEHICULO, EmptyStr, Bmp, mnu_TiposVehiculos.OnClick);
            Bmp.Assign(nil);
        end;
        if ExisteAcceso('mnu_MarcasVehiculo') then begin
            PageImagenes.GetBitmap(21, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_MARCAS_VEHICULO, EmptyStr, Bmp, mnu_MarcasVehiculo.OnClick);
            Bmp.Assign(nil);
        end;
        if ExisteAcceso('mnu_CombinacioValidaPatente') then begin
            PageImagenes.GetBitmap(18, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_COMBINACIONES_PATENTE, EmptyStr, Bmp, mnu_CombinacioValidaPatente.OnClick);
            Bmp.Assign(nil);
        end;

        // SUB-MENU DIVISION POLITICA CALLES
        if ExisteAcceso('mnu_Regiones') then begin
            PageImagenes.GetBitmap(11, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_REGIONES, EmptyStr, Bmp, mnu_Regiones.OnClick);
            Bmp.Assign(nil);
        end;
        if ExisteAcceso('mnu_Comunas') then begin
            PageImagenes.GetBitmap(12, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_COMUNAS, EmptyStr, Bmp, mnu_Comunas.OnClick);
            Bmp.Assign(nil);
        end;

		// Arbol FAQs
        if ExisteAcceso('ArboldeFAQs1') then begin
            PageImagenes.GetBitmap(19, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_FAQ_ARBOL, EmptyStr, Bmp, ArboldeFAQs1.OnClick);
            Bmp.Assign(nil);
        end;

        // ITEM PREGUNTAS
        if ExisteAcceso('mnu_ABMFuenteSolicitud') then begin
            PageImagenes.GetBitmap(15, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_MANTENIMIENTO_FUENTE_SOLICITUD, EmptyStr, Bmp, mnu_ABMFuenteSolicitud.OnClick);
            Bmp.Assign(nil);
        end;

        // SUB-MENU CONFIGURACION GENERAL DE SOLICITUD
        if ExisteAcceso('mnu_GestionMensajes') then begin
            PageImagenes.GetBitmap(20, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_GESTION_MENSAJES, EmptyStr, Bmp, mnu_GestionMensajes.OnClick);
            Bmp.Assign(nil);
        end;
        if ExisteAcceso('mnu_CodigosArea') then begin
            PageImagenes.GetBitmap(17, Bmp);
            GNavigator.AddIcon(PAGINA_TABLAS, MSG_CODIGO_AREA, EmptyStr, Bmp, mnu_CodigosArea.OnClick);
            Bmp.Assign(nil);
        end;
    end;


	if NavigatorHasPages then GNavigator.IconBarActivePage := 0;

	Bmp.Free;
	Update;

    RefrescarEstadosIconBar;

    GNavigator.IconBarVisible   := GetUserSettingShowIconBar;
    mnu_IconBar.Checked		    := GNavigator.IconBarVisible;

    GNavigator.ConnectionCOP    := DMConnections.BaseCOP;
    GNavigator.ConnectionCAC    := DMConnections.BaseCAC;
end;

procedure TPrincipal.RefrescarEstadosIconBar;
begin
    if (GNavigator = Nil) then Exit;

    GNavigator.SetIconEnabled(PAGINA_MANTENIMIENTO, MSG_ADMINISTRACION_USUARIOS, mnu_usuarios.Enabled);
    GNavigator.SetIconEnabled(PAGINA_MANTENIMIENTO, MSG_CAMBIO_CONTRASENIA, mnu_cambiopassword.Enabled);

    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_TIPOS_VEHICULO, mnu_TiposVehiculos.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_MARCAS_VEHICULO, mnu_MarcasVehiculo.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_COMBINACIONES_PATENTE, mnu_CombinacioValidaPatente.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_REGIONES, mnu_Regiones.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_COMUNAS, mnu_Comunas.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_FAQ_ARBOL, ArboldeFAQs1.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_MANTENIMIENTO_FUENTE_SOLICITUD, mnu_ABMFuenteSolicitud.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_GESTION_MENSAJES, mnu_GestionMensajes.Enabled);
    GNavigator.SetIconEnabled(PAGINA_TABLAS, MSG_CODIGO_AREA, mnu_CodigosArea.Enabled);
end;

procedure TPrincipal.FormasdePago1Click(Sender: TObject);
var
    f: TABMFormaPagoForm;
begin

	if FindFormOrCreate(TABMFormaPagoForm, f) then
		f.Show
	else begin
		if not f.Inicializa then f.Release;
	end;

end;

procedure TPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
resourcestring                                                                                    //SS_1436_NDR_20151230
	MSG_REGISTRO_CAPTION	= 'Error al Grabar Registro de Operaciones';                              //SS_1436_NDR_20151230
var                                                                                               //SS_1436_NDR_20151230
  DescriError: array[0..255] of char;                                                             //SS_1436_NDR_20151230
begin
    (* Guardar las opciones de configuraci�n del usuario. *)
    GuardarUserSettings;
    if ( CrearRegistroOperaciones(                                                                //SS_1436_NDR_20151230
            DMCOnnections.BaseCAC,                                                                //SS_1436_NDR_20151230
            SYS_ADMINISTRACION,                                                                   //SS_1436_NDR_20151230
            RO_MOD_LOGOUT,                                                                        //SS_1436_NDR_20151230
            RO_AC_LOGOUT,                                                                         //SS_1436_NDR_20151230
            0,                                                                                    //SS_1436_NDR_20151230
            GetMachineName,                                                                       //SS_1436_NDR_20151230
            UsuarioSistema,                                                                       //SS_1436_NDR_20151230
            'LOGOUT DEL SISTEMA',                                                                 //SS_1436_NDR_20151230
            0,0,0,0,                                                                              //SS_1436_NDR_20151230
            DescriError) = -1 ) then                                                              //SS_1436_NDR_20151230
    begin                                                                                         //SS_1436_NDR_20151230
      MsgBoxErr(MSG_ERROR_ACCESO, StrPas(DescriError), MSG_REGISTRO_CAPTION, MB_ICONSTOP);        //SS_1436_NDR_20151230
    end;                                                                                          //SS_1436_NDR_20151230
end;

procedure TPrincipal.FormCreate(Sender: TObject);
begin
	{$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
//        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

procedure TPrincipal.mnu_BarraDeNavegacionClick(Sender: TObject);
begin
	GNavigator.ShowBrowserBar       := not GNavigator.ShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;
end;

procedure TPrincipal.mnu_BarraDeHotLinksClick(Sender: TObject);
begin
	GNavigator.ShowHotLinkBar   := not GNavigator.ShowHotLinkBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.GuardarUserSettings
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: None
  Result:    None
  Description: Guarda las opciones de configuraci�n para el usuario logueado
    en el Sistema Operativo.
-----------------------------------------------------------------------------}
procedure TPrincipal.GuardarUserSettings;
begin
    SetUserSettingShowIconBar(mnu_IconBar.Checked);
    SetUserSettingShowBrowserBar(mnu_BarraDeNavegacion.Checked);
    SetUserSettingShowHotLinksBar(mnu_BarraDeHotLinks.Checked);
end;

{******************************** Function Header ******************************
Function Name: ramosdeGastosdeCobranzaSerbanc1Click
Author : ndonadio
Date Created : 02/09/2005
Description : Abre el ABM de tramos de gastos de cobranza de serbanc
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TPrincipal.TramosdeGastosdeCobranzaSerbancClick(Sender: TObject);
var
    f: TfrmABMTramosGastosSerbanc;
begin
    if FindFormOrCreate(TfrmABMTramosGastosSerbanc, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;


end;

{******************************** Function Header ******************************
Function Name: ToleranciasBHTU1Click
Author : ndonadio
Date Created : 15/09/2005
Description :    abre el form para el ABM de Tolerancias de los BHTU
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TPrincipal.ToleranciasBHTU1Click(Sender: TObject);
var
    f: TfrmABMToleranciasDaypass;
begin
    if FindFormOrCreate(TfrmABMToleranciasDaypass, f) then begin
        f.Show;
    end else begin
       if f.Inicializar( ToleranciasBHTU1.Caption )then f.Show else f.Release;
    end;

end;


//Rev.5 / 22-Junio-2010 / Nelson Droguett Sierra ---------------------------------
procedure TPrincipal.mniSubTiposOrdenServicioClick(Sender: TObject);
var
    f: TFormABMSubTiposOrdenServicio;
begin
    if FindFormOrCreate(TFormABMSubTiposOrdenServicio, f) then
		f.Show
	else begin
		if f.Inicializar(True) then f.Show else f.Release;
	end;
end;



procedure TPrincipal.mnuClientesaFacturarOtrasConcesionariasClick(
  Sender: TObject);
begin
    if FindFormOrCreate(TABMClientesAFacturarOtrasConcesionariasForm, ABMClientesAFacturarOtrasConcesionariasForm) then begin
        ABMClientesAFacturarOtrasConcesionariasForm.Show;
    end
    else if not ABMClientesAFacturarOtrasConcesionariasForm.Inicializar() then ABMClientesAFacturarOtrasConcesionariasForm.Release;

end;

procedure TPrincipal.mnuClientesNQClick(Sender: TObject);
var
    f: TfrmABMClientesNQ;
begin
    if FindFormOrCreate(TfrmABMClientesNQ, f) then begin
        f.Show;
    end else begin
       if f.Inicializar then f.Show else f.Release;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: MnuLibrosContables
  Author:    FSandi
  Date Created: 19-06-2007
  Description: ABM de Libros Contables
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.MnuColumnasLibrosContablesClick(Sender: TObject);
var
    f: TFormABMColumnasLibrosContables;
begin
  if FindFormOrCreate(TFormABMColumnasLibrosContables, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MnuLibrosContables
  Author:    FSandi
  Date Created: 19-06-2007
  Description: ABM de Libros Contables
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TPrincipal.MnuConceptosMovimientosLibrosContablesClick(
  Sender: TObject);
var
    f: TFormABMConceptosMovimientosLibrosContables;
begin
    if FindFormOrCreate(TFormABMConceptosMovimientosLibrosContables, f) then begin
        f.Show;
    end else begin
       if f.Inicializa then f.Show else f.Release;
    end;
end;

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TPrincipal.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
  with ACanvas do
  begin
    S := TMenuItem(Sender).Caption;
    if Selected then
    begin
        Brush.Color := FColorMenuSel;
        Font.Color := FColorFontSel;
    end
    else
    begin
        if TMenuItem(Sender).Enabled then
        begin
            Brush.Color := FColorMenu;
            Font.Color := FColorFont;
            Font.Style:=[fsBold];
        end
        else
        begin
            Brush.Color := FColorMenu;
            Font.Color := clGrayText;
            Font.Style:=[];
        end;
    end;


    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 6 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

    if S='-' then
    begin
      with ACanvas do
      begin
        ACanvas.MoveTo(ARect.Left,ARect.top + 4);
        ACanvas.Pen.Color := FColorFont;
        ACanvas.LineTo(ARect.Right, ARect.top + 4 );
      end;
    end;

  end;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TPrincipal.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;

//INICIO: TASK_040_ECA_20160628
procedure TPrincipal.mnuABMConvenioClienteTipoClick(Sender: TObject);
var
  f : TfrmABMConvenioClienteTipo;
begin

  if FindFormOrCreate(TfrmABMConvenioClienteTipo, f) then
        f.Show
   else if not f.Inicializar(True) then
       frmABMConvenioClienteTipo.Release;
end;
//FIN: TASK_040_ECA_20160628
end.

