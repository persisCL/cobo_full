
{********************************** Unit Header ********************************
File Name : ABMToleranciasDaypass.pas
Author : ndonadio
Date Created: 14/09/2005
Language : ES-AR
Description : ABM que carga las tolerancias para definir el DIA BHTU para cada
            concesionaria. Con soporte para versionado segun fecha de activacion.
            Las de las otras concesionarias son para hacer el clearing de BHTU.

Autor       :   CQuezadaI
Fecha       :   17 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se env�a al par�metro @FechaActivacionActual el valor del campo deFechaActivacion
                ya que la fecha que se enviaba no era correcta.
*******************************************************************************}
unit ABMToleranciasDaypass;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  Util, UtilProc, OleCtrls, DmiCtrls, ComCtrls, PeaProcs, DMConnection,
  ADODB, variants, DPSControls, PeaTypes, Validate, DateEdit, VariantComboBox;

type
  TfrmABMToleranciasDaypass = class(TForm)
    AbmToolbar1: TAbmToolbar;
    gbEdicion: TPanel;
    deFechaActivacion: TDateEdit;
    Panel2: TPanel;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    alTolerancias: TAbmList;
    Label1: TLabel;
    Label2: TLabel;
    neInferior: TNumericEdit;
    neSuperior: TNumericEdit;
    UpDownSuperior: TUpDown;
    UpDownInferior: TUpDown;
    Label3: TLabel;
    Label4: TLabel;
    cbConcesionaria: TVariantComboBox;
    spObtenerConcesionarias: TADOStoredProc;
    spObtenerListadoToleranciasDiaDaypass: TADOStoredProc;
    Label5: TLabel;
    Label6: TLabel;
    spActualizarToleranciaBHTU: TADOStoredProc;
    spObtenerFechasToleranciaBHTUVigente: TADOStoredProc;
    spEliminarToleranciaBHTU: TADOStoredProc;
    procedure alToleranciasClick(Sender: TObject);
    procedure alToleranciasDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure alToleranciasDelete(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure alToleranciasInsert(Sender: TObject);
    procedure alToleranciasEdit(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FInicializando          : Boolean;
    FRecNoActual            : Integer;
    FConcesionariaActual    : Integer;
    FFechaActivacionActual  : TDateTime;
    function FechaActivacionVigente(CodConcesionaria: Integer) : TDateTime;
    function CargarConcesionarias(var Error: AnsiString): boolean;
    function CargarTolerancias(var Error: AnsiString): boolean;
    function CargarVigentes(var Error: AnsiString): boolean;
    function ValidarFechaActivacion: Boolean;
    procedure HabilitarBotones(Valor: Boolean; Deleting: Boolean = False);
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

var
  frmABMToleranciasDaypass: TfrmABMToleranciasDaypass;

implementation

{$R *.dfm}

{ TForm1 }

{******************************** Function Header ******************************
Function Name:  Inicializar
Author : ndonadio
Date Created : 14/09/2005
Description : Inicializa el formulario...
Parameters : Not available
Return Value : Not available
*******************************************************************************}
function TfrmABMToleranciasDaypass.Inicializar(Titulo: AnsiString): boolean;
resourcestring
    ERROR_AL_INICIALIZAR = 'No se puede inicializar el formulario.';
var
    s: TSize;
    descError: AnsiString;
begin
    Result := False;
    FInicializando := True;
    FRecNoActual := 0;
    Caption := Titulo;
    //CenterForm(Self);
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    // cargo las fechas de vigencia para c/conc.
    if not CargarVigentes(descError) then begin
        MsgBoxErr(ERROR_AL_INICIALIZAR, descError, Caption, MB_ICONERROR);
        Exit;
    end;
    // cargo las concesionarias
    if not CargarConcesionarias(descError) then begin
        MsgBoxErr(ERROR_AL_INICIALIZAR, descError, Caption, MB_ICONERROR);
        Exit;
    end;
    // levanto la lista de tolerancias
    if not CargarTolerancias(descError) then begin
        MsgBoxErr(ERROR_AL_INICIALIZAR, descError, Caption, MB_ICONERROR);
        Exit;
    end;
    alTolerancias.Reload;
    FInicializando := False;
    HabilitarBotones(False);
    Result := True;
end;


{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 14/09/2005
Description : Proc. que habilita o no los botones, combos, etc.
Parameters : Valor: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.HabilitarBotones(Valor: Boolean; Deleting: Boolean = False);
begin
    gbEdicion.Enabled := Valor;
    alTolerancias.Enabled := NOT Valor;
    if Valor then begin
        FRecNoActual := spObtenerListadoToleranciasDiaDaypass.RecNo;
        // Habilito lo que tengo que habilitar si puedo.
        if Not Deleting then
            Notebook.ActivePage := 'PageModi';
    end
    else begin
        // Deshabilito todo...
        alTolerancias.Estado := Normal;
        if Not Deleting then
            Notebook.ActivePage := 'PageSalir';
        spObtenerListadoToleranciasDiaDaypass.DisableControls;
        spObtenerListadoToleranciasDiaDaypass.First;
        spObtenerListadoToleranciasDiaDaypass.EnableControls;
        spObtenerListadoToleranciasDiaDaypass.MoveBy(FRecNoActual);

    end;
end;

{******************************** Function Header ******************************
Function Name: CargarConcesionarias
Author : ndonadio
Date Created : 14/09/2005
Description : Carga la lista de concesionarias en el combo...
Parameters : var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmABMToleranciasDaypass.CargarConcesionarias(var Error: AnsiString): boolean;
resourcestring
    ERROR_NO_HAY_CONCESIONES    = 'No hay concesionarias definidas.';
    ERROR_NO_SE_CARGO_COMBO     = 'Fall� la carga del combo de concesionarias.';
var
    Concesionaria: AnsiString;
    Codigo:          Integer;
begin
    Result := False;
    try
        try
            // vacio el combo
            cbConcesionaria.Clear;
            // Obtengo las concesionarias
            spObtenerConcesionarias.Open;
            // Si no hay ninguna, levanto una excepcion, para manejar los errores todos en el mismo lugar...
            if spObtenerConcesionarias.IsEmpty then raise exception.Create(ERROR_NO_HAY_CONCESIONES);
            While not spObtenerConcesionarias.EOF do begin
                Concesionaria := spObtenerConcesionarias.FieldByName('Descripcion').asString;
                Codigo        := spObtenerConcesionarias.FieldByName('CodigoConcesionaria').asInteger;
                cbConcesionaria.Items.Add(Concesionaria, Codigo) ;
                spObtenerConcesionarias.Next;
            end;
            if cbConcesionaria.Items.Count = 0 then raise exception.create(ERROR_NO_SE_CARGO_COMBO);
            Result := True;
        except
            // Si surgi� un error, cargo la var Error y salgo...
            on e:exception do begin
                Error := e.Message;
                Exit;
            end;
        end;
    finally
        spObtenerConcesionarias.Close;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarTolerancias
Author : ndonadio
Date Created : 14/09/2005
Description : funcion que cierra y vuelve a abrir el Sp de carga de
                las tolerancias.
Parameters : var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmABMToleranciasDaypass.CargarTolerancias(var Error: AnsiString): boolean;
begin
    Result := False;
    spObtenerListadoToleranciasDiaDaypass.Close;
    try
        spObtenerListadoToleranciasDiaDaypass.Open;
        alTolerancias.Refresh;
        alToleranciasClick(Self);
        Result := True;
    except
        on e:Exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnSalirClick
Author : ndonadio
Date Created : 14/09/2005
Description : Al cerrarse el form
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 14/09/2005
Description :  Sale del form
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: alToleranciasEdit
Author : ndonadio
Date Created : 14/09/2005
Description :  Al querer salir...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := NOT gbEdicion.Enabled ;
end;

{******************************** Function Header ******************************
Function Name: alToleranciasEdit
Author : ndonadio
Date Created : 14/09/2005
Description :   Al editer las tolerancias
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.alToleranciasEdit(Sender: TObject);
begin
    FConcesionariaActual   := spObtenerListadoToleranciasDiaDaypass.FieldByName('CodigoConcesionaria').asInteger;
    FFechaActivacionActual  := spObtenerListadoToleranciasDiaDaypass.FieldByName('FechaActivacion').asDateTime;
    HabilitarBotones(True);
end;

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author : ndonadio
Date Created : 14/09/2005
Description : Al insertar una tolerancia
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.alToleranciasInsert(Sender: TObject);
begin
    cbConcesionaria.ItemIndex := 0;
    deFechaActivacion.Date := NowBase(DMConnections.BaseCAC) + 1;
    HabilitarBotones(True);
end;

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author : ndonadio
Date Created : 14/09/2005
Description : Acepta los cambios editados en el registro...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.BtnAceptarClick(Sender: TObject);
resourcestring
    ERROR_UPDATING_DATA     = 'Error al actualizar la informaci�n';
    ERROR_LOADING_DATA      = 'No se pueden cargar las tolerancias.';
    ERROR_CONCESIONARIA     = 'Debe seleccionar una Concesionaria.';
    ERROR_INVALID_TOLERANCE = 'El valor de tolerancia debe estar entre 0 y 1,440';
    ERROR_DATE_INVALID      = 'La fecha de activaci�n debe ser mayor a la fecha de vigencia actual ' + CRLF + 'y mayor o igual a la fecha de hoy.';
var
    descError: AnsiString;
begin
    if not ValidateControls([cbConcesionaria, deFechaActivacion, neInferior, neSuperior],
                        [(cbConcesionaria.ItemIndex >= 0),
                         (ValidarFechaActivacion),
                         (neInferior.ValueInt <= 1440) AND (neInferior.ValueInt >= 0 ),
                         (neSuperior.ValueInt <= 1440) AND (neSuperior.ValueInt >= 0 )],
                        Caption,[ERROR_CONCESIONARIA, ERROR_DATE_INVALID, ERROR_INVALID_TOLERANCE, ERROR_INVALID_TOLERANCE]) then Exit;

    Cursor := crHourGlass;
    try
        try
            with spActualizarToleranciaBHTU do begin
                Parameters.Refresh;
                if alTolerancias.Estado = Modi then begin
                    Parameters.ParamByName('@FechaActivacionActual').Value := FFechaActivacionActual;
                    Parameters.ParamByName('@ConcesionariaActual').Value := FConcesionariaActual;
                end
                else begin
                    //Parameters.ParamByName('@FechaActivacionActual').Value := StrToDate('01/01/1980');    // SS_1147_CQU_20140714
                    Parameters.ParamByName('@FechaActivacionActual').Value := deFechaActivacion.Date;       // SS_1147_CQU_20140714
                    Parameters.ParamByName('@ConcesionariaActual').Value := NULL;
                end;
                Parameters.ParamByName('@Concesionaria').Value := cbConcesionaria.Items[cbConcesionaria.ItemIndex].Value ;
                Parameters.ParamByName('@FechaActivacion').Value := deFechaActivacion.Date;
                Parameters.ParamByName('@ToleranciaInferior').Value := neInferior.ValueInt;
                Parameters.ParamByName('@ToleranciaSuperior').Value := neSuperior.ValueInt;
                Parameters.ParamByName('@Error').Value := '';
                ExecProc;
                if Parameters.ParamByName('@RETURN_VALUE').Value = -1 then begin
                    HabilitarBotones(False);
                    descError :=  Parameters.ParamByName('@Error').Value;
                    MsgBox(descError, Caption, MB_ICONERROR);
                    Exit;
                end;
                if Parameters.ParamByName('@RETURN_VALUE').Value = -2 then begin
                    HabilitarBotones(False);
                    descError :=  Parameters.ParamByName('@Error').Value;
                    MsgBoxErr( ERROR_UPDATING_DATA, descError, Caption, MB_ICONERROR);
                    Exit;
                end;
            end;

            if  (not CargarVigentes(descError)) or
                (not CargarTolerancias(descError)) then begin
                        MsgBoxErr(ERROR_LOADING_DATA, descError, Caption, MB_ICONERROR);
                        Close;
            end;
            HabilitarBotones(False);
        except
            on e:exception do begin
                MsgBoxErr( ERROR_UPDATING_DATA, e.Message, Caption, MB_ICONERROR);
                Close;
            end;
        end;
    finally
        Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnCancelarClick
Author : ndonadio
Date Created : 14/09/2005
Description :    Cancela el agregado o edicion de un registro
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.BtnCancelarClick(Sender: TObject);
begin
    HabilitarBotones(False);
end;

{******************************** Function Header ******************************
Function Name: alToleranciasDelete
Author : ndonadio
Date Created : 15/09/2005
Description : Elimina la entrada en el tabla de tolerancias...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmABMToleranciasDaypass.alToleranciasDelete(Sender: TObject);
resourcestring
    ERROR_DELETING              = 'Error al eliminar el registro';
    ERROR_NOT_EXIST             = 'La Tolerancia que desa eliminar No Existe';
    ERROR_IS_ACTIVE_OR_PAST     = 'La Tolerancia que quiere eliminar no se puede elliminar por ser la Tolerancia Activa o anterior a esta.';
    ERROR_DELETING_DESC         = 'Se ha producido un error al intentar eliminar el registro de la Tolerancia.';
    ERROR_LOADING_DATA          = 'No se pueden cargar las tolerancias.';

    MSG_DELETE_CONFIRMATION     = 'Desea eliminar los par�metros de tolerancia para la Concesionaria %s,' + CRLF + 'con vigencia a partir del d�a %s';
const
    FORMATO_FECHA   = 'dddd, dd/mm/aaaa';
var
    descError:      AnsiString;
    Confirmacion:   AnsiString;
begin
    // solicito al usuario confirmaci�n de lo que quiere eliminar
    Confirmacion := Format( MSG_DELETE_CONFIRMATION, [TRIM(spObtenerListadoToleranciasDiaDaypass.FieldByName('DescripcionConcesionaria').AsString),
      FormatDateTime(FORMATO_FECHA, spObtenerListadoToleranciasDiaDaypass.FieldByName('FechaActivacion').asDateTime)]);

    if MsgBox(Confirmacion, Caption, MB_ICONQUESTION+MB_YESNO) <> mrYes then Exit;

    // Ya confirmo, intento eliminar
    Cursor := crHourglass;
    HabilitarBotones(True, True);
    try
        try
            spEliminarToleranciaBHTU.Parameters.ParamByName('@CodigoConcesionaria').Value := spObtenerListadoToleranciasDiaDaypass.FieldByName('CodigoConcesionaria').asInteger;
            spEliminarToleranciaBHTU.Parameters.ParamByName('@FechaActivacion').Value := spObtenerListadoToleranciasDiaDaypass.FieldByName('FechaActivacion').asDateTime;
            spEliminarToleranciaBHTU.ExecProc;
            Case spEliminarToleranciaBHTU.Parameters.ParamByName('@RETURN_VALUE').Value of
                -1: begin
                        MsgBoxErr(ERROR_DELETING, ERROR_NOT_EXIST, Caption, MB_ICONERROR);
                        Exit;
                    end;
                -2: begin
                        MsgBoxErr(ERROR_DELETING, ERROR_IS_ACTIVE_OR_PAST, Caption, MB_ICONERROR);
                        Exit;
                    end;
                -3: begin
                        MsgBoxErr(ERROR_DELETING, ERROR_DELETING_DESC, Caption, MB_ICONERROR);
                        Exit;
                    end;
            end;
            if  (not CargarVigentes(descError))  or
                (not CargarTolerancias(descError))then begin
                        MsgBoxErr(ERROR_LOADING_DATA, descError, Caption, MB_ICONERROR);
                        Close;
            end;
        except
            on e:exception do begin
                MsgBoxErr(ERROR_DELETING, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        HabilitarBotones(False, True);
//        alToleranciasClick(Self);
        Cursor := crDefault;
    end;
end;

procedure TfrmABMToleranciasDaypass.alToleranciasDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
    // Decido como mostrar los datos en la tabla...
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        if  (Tabla.FieldByName('FechaActivacion').AsDateTime = FechaActivacionVigente(Tabla.FieldByName('CodigoConcesionaria').asInteger)) then begin
            Font.Style := [];
            Font.Color := clRed;

        end;
        if  (Tabla.FieldByName('FechaActivacion').AsDateTime > FechaActivacionVigente(Tabla.FieldByName('CodigoConcesionaria').asInteger)) then begin
            Font.Style := [];
            Font.Color := clWindowText;
        end;
        if  (Tabla.FieldByName('FechaActivacion').AsDateTime < FechaActivacionVigente(Tabla.FieldByName('CodigoConcesionaria').asInteger)) then begin
            Font.Style := [];
            Font.Color := clGrayText;
        end;
        TextOut(Cols[0], Rect.Top, Tabla.FieldByName('DescripcionConcesionaria').AsString);
      	TextOut(Cols[1], Rect.Top, Tabla.FieldByName('FechaActivacion').AsString);

	end;
end;

procedure TfrmABMToleranciasDaypass.alToleranciasClick(Sender: TObject);
begin
    if NOT FInicializando then begin

        deFechaActivacion.Date  := spObtenerListadoToleranciasDiaDaypass.FieldByName('FechaActivacion').AsDateTime;
        neSuperior.ValueInt     := spObtenerListadoToleranciasDiaDaypass.FieldByName('ToleranciaSuperior').AsInteger;
        neInferior.ValueInt     := spObtenerListadoToleranciasDiaDaypass.FieldByName('ToleranciaInferior').AsInteger;
        cbConcesionaria.ItemIndex := cbConcesionaria.Items.IndexOfValue(spObtenerListadoToleranciasDiaDaypass.FieldByName('CodigoConcesionaria').asInteger);

        if (deFechaActivacion.Date > FechaActivacionVigente(spObtenerListadoToleranciasDiaDaypass.FieldByName('CodigoConcesionaria').asInteger)) then begin
            // Estoy en un registro que es una tasa futura. La puedo eliminar o editar.
                alTolerancias.Access := [accAlta, accBaja, accModi] ;
                abmToolbar1.Habilitados := [btAlta, btBaja, btModi];
        end
        else begin
            // Estoy en una tasa vigente o historica. No puedo modificar ni editar.
            alTolerancias.Access := [accAlta];
            abmToolbar1.Habilitados := [btAlta];
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarVigentes
Author : ndonadio
Date Created : 15/09/2005
Description :  Carga las tarifas vigentes para cada concesionaria...
Parameters : var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmABMToleranciasDaypass.CargarVigentes(var Error: AnsiString): boolean;
begin
    Result := False;
    try
        spObtenerFechasToleranciaBHTUVigente.Close;
        spObtenerFechasToleranciaBHTUVigente.Open;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: FechaActivacionVigente
Author : ndonadio
Date Created : 15/09/2005
Description : Me devuelve la Feha de Activacion vigente para una concesionaria dada
Parameters : CodConcesionaria: Integer
Return Value : TDateTime
*******************************************************************************}
function TfrmABMToleranciasDaypass.FechaActivacionVigente(CodConcesionaria: Integer): TDateTime;
begin
    spObtenerFechasToleranciaBHTUVigente.Locate('CodigoConcesionaria', CodCOncesionaria,[loCaseInsensitive]);
    if  not spObtenerFechasToleranciaBHTUVigente.Eof then result := spObtenerFechasToleranciaBHTUVigente.FieldByName('FechaActivacionVigente').asDateTime
    else Result := NowBase(DMConnections.BaseCAC);
end;

function TfrmABMToleranciasDaypass.ValidarFechaActivacion: Boolean;
begin
    Result := ( deFechaActivacion.Date >= NowBase(DMConnections.BaseCAC) ) AND
              ( deFechaActivacion.Date > FechaActivacionVigente(cbConcesionaria.Items[cbConcesionaria.ItemIndex].Value));
end;

end.
