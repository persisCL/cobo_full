{-------------------------------------------------------------------

Author      :
Date        :
Description :

Firma       :   SS_1147_MBE_20140818
Description :   Se agregan los campos Es Fiscal y Se usa en Saldo

Firma       :   SS_1233_NDR_20141224
Descripcion :   Se agrega bit para campo PermiteSaltosFolio

Firma		:   201160613 CFU
Descripci�n :   Se inhabilita bot�n imprimir seg�n lo indicado en CU.COBO.ADM.0111 - Tipos Comprobantes
--------------------------------------------------------------------}
unit ABMTiposComprobante;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB,DMConnection,
  DPSControls, VariantComboBox, TiposComprobantes, ListBoxEx, DBListEx, ImgList,
  ComCtrls, ToolWin;

type
  TFormTiposComprobante = class(TForm)
	Panel2: TPanel;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    txtDescripcion: TEdit;
    txtTipoComprobante: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    lblSigno: TLabel;
    cbSigno: TVariantComboBox;
    chkSaldo: TCheckBox;
    chkFiscal: TCheckBox;
    chkPermiteSaltosFolio: TCheckBox;
    dblTiposComprobantes: TDBListEx;
    pnlBotones: TPanel;
    pnlSuperior: TPanel;
    tlbBotonera: TToolBar;
    btn4: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btn3: TToolButton;
    btnBuscar: TToolButton;
    ilImagenes: TImageList;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
{INICIO: TASK_117_JMA_20170320
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
TERMINO: TASK_117_JMA_20170320}
	procedure AbmToolbar1Close(Sender: TObject);
{INICIO: TASK_117_JMA_20170320
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
TERMINO: TASK_117_JMA_20170320}
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);

  private
	{ Private declarations }
    //INICIO: TASK_117_JMA_20170320
    FPuedeEditar: Boolean;
    oTipos : TTipoComprobante;    
     
    procedure CargarTiposComprobantes();
    procedure dblTiposComprobantes_Scrolled(DataSet: TDataSet);
    procedure ActivarControles(Activar: Boolean);
    //TERMINO: TASK_117_JMA_20170320
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializar : boolean;
  end;

var
  FormTiposComprobante  : TFormTiposComprobante;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Tipo de Comprobante?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Tipo de Comprobante porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Tipo de Comprobante';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Tipo de Comprobante.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Tipo de Comprobante';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';

{$R *.DFM}

function TFormTiposComprobante.Inicializar: boolean;
Var
	S: TSize;
begin
{INICIO: TASK_117_JMA_20170320
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([TiposComprobantes]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;
}

    FPuedeEditar := ExisteAcceso('tipos_comprobantes_administrar');

    if not FPuedeEditar then
    begin
        BtnAceptar.Enabled := False;
        btnEditar.Enabled := False;
        btnEliminar.Enabled := False;
    end;        

    CargarTiposComprobantes();
{TERMINO:TASK_117_JMA_20170320}

    Notebook.PageIndex := 0;
end;

procedure TFormTiposComprobante.Limpiar_Campos();
begin
	txtTipoComprobante.Clear;
	txtDescripcion.Clear;
    chkSaldo.Checked := False;                                           //SS_1147_MBE_20140818
    chkFiscal.Checked := False;                                          //SS_1147_MBE_20140818
    cbSigno.ItemIndex := -1;                                             //SS_1147_MBE_20140818
    chkPermiteSaltosFolio.Checked := False;                              //SS_1233_NDR_20141224
end;

function TFormTiposComprobante.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
	Texto :=  PadR(Trim(Tabla.FieldByName('TipoComprobante').AsString), 4, ' ' ) + ' '+
	  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormTiposComprobante.BtSalirClick(Sender: TObject);
begin
	Close;
end;

{INICIO: TASK_117_JMA_20170320
procedure TFormTiposComprobante.ListaInsert(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Alta;
	Limpiar_Campos;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txtTipoComprobante.Enabled := True;
	txtTipoComprobante.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormTiposComprobante.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txtTipoComprobante.Enabled := False;
	Screen.Cursor    := crDefault;
	txtDescripcion.SetFocus;

end;

procedure TFormTiposComprobante.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			TiposComprobantes.Delete;
		Except
			On E: EDataBaseError do begin
				TiposComprobantes.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormTiposComprobante.ListaClick(Sender: TObject);
var
  Codigo,                                                                           //SS_1147_MBE_20140818
  index: integer;
begin
	 with TiposComprobantes do begin
        txtTipoComprobante.Text	:= Trim(FieldByName('TipoComprobante').AsString);
        txtDescripcion.text	:= Trim(FieldByName('Descripcion').AsString);
//        for index := 0 to cbSigno.Items.Count -1 do                               //SS_1147_MBE_20140818
//            if FieldByName('Signo').AsString = cbSigno.Items[index].Value then    //SS_1147_MBE_20140818
//                cbSigno.ItemIndex := index;                                       //SS_1147_MBE_20140818

        for index := 0 to cbSigno.Items.Count - 1 do begin                          //SS_1147_MBE_20140818
            Codigo := cbSigno.Items[index].Value;                                   //SS_1147_MBE_20140818
            if FieldByName('Signo').AsInteger = Codigo then begin                   //SS_1147_MBE_20140818
                cbSigno.ItemIndex := index;                                         //SS_1147_MBE_20140818
                Break;                                                              //SS_1147_MBE_20140818
            end;                                                                    //SS_1147_MBE_20140818

        end;                                                                        //SS_1147_MBE_20140818

        chkSaldo.Checked := FieldByName('SeUsaEnSaldo').AsBoolean;                  //SS_1147_MBE_20140818
        chkFiscal.Checked := FieldByName('EsFiscal').AsBoolean;                     //SS_1147_MBE_20140818
        chkPermiteSaltosFolio.Checked := FieldByName('PermiteSaltosFolio').AsBoolean;//SS_1233_NDR_20141224
	 end;
end;

procedure TFormTiposComprobante.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;
TERMINO: TASK_117_JMA_20170320}

procedure TFormTiposComprobante.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

{INICIO: TASK_117_JMA_20170320
procedure TFormTiposComprobante.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('TipoComprobante').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormTiposComprobante.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormTiposComprobante.BtnAceptarClick(Sender: TObject);
resourcestring                                                                                                          //SS_1147_MBE_20140818
    MSG_TIPO        = 'Debe ingresar el Tipo de Comprobante (2 caracteres)';                                            //SS_1147_MBE_20140818
    MSG_SIGNO       = 'Debe indicar Signo del comprobante';                                                             //SS_1147_MBE_20140818
    MSG_SQL_INSERT  = 'SELECT 1 FROM TiposComprobantes (NOLOCK) WHERE TipoComprobante = ''%s'' ';                       //SS_1147_MBE_20140818
    MSG_YA_EXISTE   = 'Ya existe este Tipo Comprobante';                                                                //SS_1147_MBE_20140818

var                                                                                                                     //SS_1147_MBE_20140818
    vSQL : string;                                                                                                      //SS_1147_MBE_20140818
    vExiste : Integer;                                                                                                  //SS_1147_MBE_20140818
begin
    if not ValidateControls([txtTipoComprobante, txtDescripcion, cbSigno],                                              //SS_1147_MBE_20140818
                [Trim(txtTipoComprobante.Text) <> '', (txtDescripcion.text) <> '', cbSigno.ItemIndex >= 0],             //SS_1147_MBE_20140818
                MSG_ACTUALIZAR_CAPTION,
                [MSG_TIPO, MSG_DESCRIPCION, MSG_SIGNO]) then exit;                                                      //SS_1147_MBE_20140818

    //verificar que no se duplique el tipo de comprobante                                                               //SS_1147_MBE_20140818
    if Lista.Estado = Alta then begin                                                                                   //SS_1147_MBE_20140818
        vSQL := Format(MSG_SQL_INSERT, [txtTipoComprobante.Text]);                                                      //SS_1147_MBE_20140818
        vExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);                                                       //SS_1147_MBE_20140818
        if vExiste = 1 then begin                                                                                       //SS_1147_MBE_20140818
            MsgBoxBalloon(MSG_YA_EXISTE, Caption, MB_ICONEXCLAMATION, txtTipoComprobante);                              //SS_1147_MBE_20140818
            Exit;                                                                                                       //SS_1147_MBE_20140818
        end;                                                                                                            //SS_1147_MBE_20140818
    end;                                                                                                                //SS_1147_MBE_20140818


	Screen.Cursor := crHourGlass;
	With TiposComprobantes do begin
		Try
			if Lista.Estado = Alta then Append else Edit;

			FieldByName('TipoComprobante').AsString   := Trim(txtTipoComprobante.Text);
			FieldByName('Descripcion').AsString 	  := Trim(txtDescripcion.Text);
//            FieldByName('Signo').AsString             := cbSigno.Value;                 //SS_1147_MBE_20140818
            FieldByName('Signo').AsInteger            := cbSigno.Value ;                  //SS_1147_MBE_20140818
            FieldByName('SeUsaEnSaldo').AsBoolean     := chkSaldo.Checked;                //SS_1147_MBE_20140818
            FieldByName('EsFiscal').AsBoolean         := chkFiscal.Checked;               //SS_1147_MBE_20140818

            if Lista.Estado = Alta then begin
              FieldByName('UsuarioCreacion').AsString 	  := UsuarioSistema;
              FieldByName('FechaHoraCreacion').AsDateTime := NowBase(TiposComprobantes.Connection);
            end;

			FieldByName('UsuarioActualizacion').AsString 	  := UsuarioSistema;
			FieldByName('FechaHoraActualizacion').AsDateTime := NowBase(TiposComprobantes.Connection);
			FieldByName('PermiteSaltosFolio').AsBoolean := chkPermiteSaltosFolio.Checked;       //SS_1233_NDR_20141224

			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado     	:= Normal;
	Lista.Enabled    	:= True;
	GroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormTiposComprobante.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;
TERMINO: TASK_117_JMA_20170320}

procedure TFormTiposComprobante.FormClose(Sender: TObject; var Action: TCloseAction);
begin
{INICIO: TASK_117_JMA_20170320}
    FreeAndNil(oTipos);
    if Assigned(dblTiposComprobantes.DataSource) then
        dblTiposComprobantes.DataSource.Free;
{TERMINO: TASK_117_JMA_20170320}
    Action := caFree;
end;

procedure TFormTiposComprobante.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

{INICIO: TASK_117_JMA_20170320}
procedure TFormTiposComprobante.CargarTiposComprobantes();
begin
    try
        if Assigned(dblTiposComprobantes.DataSource) then
             dblTiposComprobantes.DataSource.Free;
        dblTiposComprobantes.DataSource := TDataSource.Create(nil);

        if not Assigned(oTipos) then
            oTipos := TTipoComprobante.Create();

        dblTiposComprobantes.DataSource.DataSet := oTipos.Obtener;
        otipos.ClientDataSet.AfterScroll := dblTiposComprobantes_Scrolled;
        if oTipos.ClientDataSet.RecordCount > 0 then
            dblTiposComprobantes_Scrolled(nil);
    except
        on e: Exception do
            MsgBoxErr('Error cargando Tipos de Comprobantes', e.Message, 'Error de Carga', MB_ICONERROR);
    end;
end;

procedure TFormTiposComprobante.dblTiposComprobantes_Scrolled(DataSet: TDataSet);
begin
    if Assigned(oTipos) and Assigned(oTipos.ClientDataSet) and (oTipos.ClientDataSet.RecordCount > 0) then
    begin
        txtTipoComprobante.Text := oTipos.TipoComprobante;
        txtDescripcion.Text := oTipos.Descripcion;
        cbSigno.Value := oTipos.Signo;
        chkSaldo.Checked := oTipos.SeUsaEnSaldo;
        chkFiscal.Checked := oTipos.EsFiscal;
        chkPermiteSaltosFolio.Checked := oTipos.PermiteSaltosFolio;
    end;
end;

procedure TFormTiposComprobante.ActivarControles(Activar: Boolean);
begin   

    if dblTiposComprobantes.Tag = 1 then
        txtTipoComprobante.Enabled := Activar;
    if not activar then
        dblTiposComprobantes.Tag := 0;

    txtDescripcion.Enabled := Activar;
    cbSigno.Enabled := Activar;
    chkSaldo.Enabled := Activar;
    chkFiscal.Enabled := Activar;
    chkPermiteSaltosFolio.Enabled := Activar;

    dblTiposComprobantes.Enabled := not Activar;
    pnlBotones.Enabled := not Activar;
    if activar then
        Notebook.PageIndex := 1
    else
        Notebook.PageIndex := 0;             
end;

procedure TFormTiposComprobante.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_TIPO        = 'Debe ingresar el Tipo de Comprobante (2 caracteres)';
    MSG_SIGNO       = 'Debe indicar Signo del comprobante';
    MSG_SQL_INSERT  = 'SELECT 1 FROM TiposComprobantes (NOLOCK) WHERE TipoComprobante = ''%s'' ';
    MSG_YA_EXISTE   = 'Ya existe este Tipo Comprobante';

var
    vSQL : string;
    vExiste : Integer;
    tipoComprobanteIngresado : string;
    bm: TBookmark;
begin
    if not ValidateControls([txtTipoComprobante, txtDescripcion, cbSigno],
                [Trim(txtTipoComprobante.Text) <> '', Trim(txtDescripcion.text) <> '', cbSigno.ItemIndex >= 0],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_TIPO, MSG_DESCRIPCION, MSG_SIGNO]) then
                Exit;

    if dblTiposComprobantes.Tag = 1 then begin             //ALTA
        //verificar que no se duplique el tipo de comprobante
        vSQL := Format(MSG_SQL_INSERT, [txtTipoComprobante.Text]);
        vExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
        if vExiste = 1 then begin
            MsgBoxBalloon(MSG_YA_EXISTE, Caption, MB_ICONEXCLAMATION, txtTipoComprobante);
            Exit;
        end;
    end;

    try
        try
            Screen.Cursor := crHourGlass;
            
            if dblTiposComprobantes.Tag = 1 then              //ALTA
                oTipos.Agregar(txtTipoComprobante.Text, txtDescripcion.Text, cbSigno.Value, chkSaldo.Checked, UsuarioSistema, chkFiscal.Checked, chkPermiteSaltosFolio.Checked)
            else
                oTipos.Modificar(txtTipoComprobante.Text, txtDescripcion.Text, cbSigno.Value, chkSaldo.Checked, UsuarioSistema, chkFiscal.Checked, chkPermiteSaltosFolio.Checked);

        except
            on e: Exception do
            begin
                if dblTiposComprobantes.Tag = 1 then
                    MsgBoxErr('Error agregando tipo de comprobante', e.Message, 'Error', MB_ICONERROR)
                else
                    MsgBoxErr('Error modificando tipo de comprobante', e.Message, 'Error', MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;

    tipoComprobanteIngresado := txtTipoComprobante.Text;
    ActivarControles(False);
    CargarTiposComprobantes();
    try
        bm := oTipos.BuscarEnClientDataSet('TipoComprobante', tipoComprobanteIngresado);
        oTipos.ClientDataSet.GotoBookmark(bm);
    finally
        if oTipos.ClientDataSet.BookmarkValid(bm) then
            oTipos.ClientDataSet.FreeBookmark(bm);
    end;


end;

procedure TFormTiposComprobante.BtnCancelarClick(Sender: TObject);
begin
    ActivarControles(False);
    dblTiposComprobantes_Scrolled(nil);
end;

procedure TFormTiposComprobante.btnAgregarClick(Sender: TObject);
begin
    dblTiposComprobantes.Tag := 1;
    ActivarControles(True);
    Limpiar_Campos;
    txtTipoComprobante.SetFocus;
end;

procedure TFormTiposComprobante.btnEditarClick(Sender: TObject);
begin
    dblTiposComprobantes.Tag := 2;
    ActivarControles(True);       
    txtDescripcion.SetFocus;
end;

procedure TFormTiposComprobante.btnEliminarClick(Sender: TObject);
begin
    if Assigned(oTipos) and Assigned(oTipos.ClientDataSet) and (oTipos.ClientDataSet.RecordCount > 0) then
    begin
        If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) = IDYES then begin
            try
                try
                    Screen.Cursor := crHourGlass;
                    oTipos.Eliminar(oTipos.TipoComprobante, UsuarioSistema);
                    CargarTiposComprobantes();
                Except
                    On E: EDataBaseError do begin
                        MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                    end;
                end;
            finally
                Screen.Cursor := crDefault;
            end;
        end;
    end;
end;

{TERMINO: TASK_117_JMA_20170320}

end.
