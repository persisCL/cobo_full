unit ABMTiposOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, AbmModelos, ADODB, DPSControls, Peatypes;

type
  TFormABMTiposOrdenServicio = class(TForm)
    GroupB: TPanel;
    Label2: TLabel;
    txt_descripcion: TMemo;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    DBList1: TAbmList;
    btn_Mensajes: TSpeedButton;
    Panel3: TPanel;
    AbmToolbar1: TAbmToolbar;
    ObtenerTiposOrdenServicio: TADOStoredProc;
    InsertarTiposOrdenServicio: TADOStoredProc;
    BorrarTiposOrdenServicio: TADOStoredProc;
    ModificarTiposOrdenServicio: TADOStoredProc;
    CB_PrioridadPorDefecto: TComboBox;
    Label1: TLabel;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure AbmToolbar1Close(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AltaTiposOrdenServicio(Descripcion:String;EsReclaro,EsGeneral:Boolean;PrioridadPorDefecto:Integer);
    procedure BajaTiposOrdenServicio(ID:Integer);
    procedure ModificacionTiposOrdenServicio(ID:Integer;Descripcion:String;EsReclaro,EsGeneral:Boolean;
    PrioridadPorDefecto:Integer);
    procedure Limpiar_Campos();
    function Inicializar(MDIChild: Boolean): Boolean;
    procedure Volver_Campos;
    function damePrioridad:SHORT;
  end;

var
  FormABMTiposOrdenServicio: TFormABMTiposOrdenServicio;

implementation

uses DMConnection;

resourcestring
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Tipo de Orden de Servicio';
    MSG_TEXTO               = 'Debe ingresar el Texto';
{$R *.dfm}

procedure TFormABMTiposOrdenServicio.AbmToolbar1Close(Sender: TObject);
begin
     Close;
end;

function TFormABMTiposOrdenServicio.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([ObtenerTiposOrdenServicio]) then exit;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	Result := True;
end;

procedure TFormABMTiposOrdenServicio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action:=caFree;
end;

procedure TFormABMTiposOrdenServicio.DBList1DrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do
    begin
		FillRect(Rect);
    TextOut(Cols[0], Rect.Top, Istr(FieldbyName('PriodidadPorDefecto').AsInteger, 3));
		TextOut(Cols[1], Rect.Top, Trim(FieldbyName('Descripcion').AsString));
	end;
end;

procedure TFormABMTiposOrdenServicio.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFormABMTiposOrdenServicio.Limpiar_Campos();
begin
    txt_descripcion.Clear;
    CB_PrioridadPorDefecto.ItemIndex:=0;
end;

procedure TFormABMTiposOrdenServicio.BtnCancelarClick(Sender: TObject);
begin
    Volver_Campos;
end;

function TFormABMTiposOrdenServicio.damePrioridad:SHORT;
begin
    damePrioridad:=CB_PrioridadPorDefecto.ItemIndex+1;
end;


procedure TFormABMTiposOrdenServicio.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then
     begin
     	Limpiar_Campos();
     end
end;

procedure TFormABMTiposOrdenServicio.AltaTiposOrdenServicio(Descripcion:String;EsReclaro,EsGeneral:Boolean;
    PrioridadPorDefecto:Integer);
begin
   with InsertarTiposOrdenServicio.Parameters do
   begin
        ParamByName('@Descripcion').Value   :=Descripcion;
        ParamByName('@EsReclamo').Value :=EsReclaro;
        ParamByName('@EsGeneral').Value :=EsGeneral;
        ParamByName('@PriodidadPorDefecto').Value:=PrioridadPorDefecto;
   end;
   InsertarTiposOrdenServicio.ExecProc;
end;


procedure TFormABMTiposOrdenServicio.ModificacionTiposOrdenServicio(ID:Integer;Descripcion:String;EsReclaro,EsGeneral:Boolean;
    PrioridadPorDefecto:Integer);
begin
   with ModificarTiposOrdenServicio.Parameters do
   begin
        ParamByName('@TipoOrdenServicio').Value:=ID;
        ParamByName('@Descripcion').Value:=Descripcion;
        ParamByName('@EsReclamo').Value:=EsReclaro;
        ParamByName('@EsGeneral').Value:=EsGeneral;
        ParamByName('@PriodidadPorDefecto').Value:=PrioridadPorDefecto;
   end;
   ModificarTiposOrdenServicio.ExecProc;
end;

procedure TFormABMTiposOrdenServicio.BajaTiposOrdenServicio(ID:Integer);
begin
   BorrarTiposOrdenServicio.Parameters.ParamByName('@Id').Value:=Id;
   BorrarTiposOrdenServicio.ExecProc;
end;

procedure TFormABMTiposOrdenServicio.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ACTUALIZARTpOrdSrv = 'No se pudieron actualizar los datos del Tipo de Orden de Servicio seleccionado.';
    CAPTION_ACTUALIZARTpOrdSrv = 'Actualizar Tipo de Orden de Servicio';
begin
    if not ValidateControls([txt_descripcion],
                [trim(txt_descripcion.text) <> ''],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_TEXTO]) then exit;

 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then begin
                AltaTiposOrdenServicio(Trim(txt_descripcion.Lines.Text),True,True,damePrioridad);
			end else begin
                ModificacionTiposOrdenServicio(FieldByName('TipoOrdenServicio').asInteger,
                 Trim(txt_descripcion.Lines.Text),True,True,damePrioridad);
            end;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZARTpOrdSrv, E.message, CAPTION_ACTUALIZARTpOrdSrv, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormABMTiposOrdenServicio.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormABMTiposOrdenServicio.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
    DbList1.Estado     := Alta;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
	txt_descripcion.SetFocus;
end;
//TASK_053_FSI_201703 : se agregan filtro de editar o no.
procedure TFormABMTiposOrdenServicio.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    txt_descripcion.setFocus;
end;

procedure TFormABMTiposOrdenServicio.DBList1Delete(Sender: TObject);
resourcestring
    MSG_ELIMINARTpOrdSrv = '�Est� seguro de querer eliminar el Tipo de Orden de Servico seleccionado?';
    MSG_ERRORELIMINARTpOrdSrv = 'No se pudo eliminar el Tipo de Orden de Servico seleccionado?.';
    CAPTION_ELIMINARTpOrdSrv = 'Eliminar Tipo de Orden de Servico';
var
    primerRg:Boolean;
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_ELIMINARTpOrdSrv, CAPTION_ELIMINARTpOrdSrv, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
            primerRg:=ObtenerTiposOrdenServicio.Bof;
            BajaTiposOrdenServicio(ObtenerTiposOrdenServicio.FieldbyName('TipoOrdenServicio').Value);
            if primerRg then
                begin
                    ObtenerTiposOrdenServicio.Close;
                    ObtenerTiposOrdenServicio.Open;
                end
                else
                    ObtenerTiposOrdenServicio.MoveBy(-1);
            
		Except
			On E: Exception do begin
				MsgBoxErr(MSG_ERRORELIMINARTpOrdSrv, e.message, CAPTION_ELIMINARTpOrdSrv, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormABMTiposOrdenServicio.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
		txt_descripcion.text	:= FieldByName('Descripcion').AsString;
    CB_PrioridadPorDefecto.ItemIndex    := FieldByName('PriodidadPorDefecto').AsInteger - 1;
    //TASK_052_FSI_201703 :Los elementos NO Editables, deben estar bloqueados.
     txt_descripcion.Enabled         := FieldbyName('Editable').Value;
     CB_PrioridadPorDefecto.Enabled    := FieldbyName('Editable').Value;
     CB_PrioridadPorDefecto.Enabled         := FieldbyName('Editable').Value;
	end;
end;

end.
