unit ABMTSMC;


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls;

type
  TFormTSMC = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Panel2: TPanel;
    Notebook: TNotebook;
    qry_TSMC: TADOQuery;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    PageControl: TPageControl;
    TabGeneral: TTabSheet;
    cbConcesionarias: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    txt_Descripcion: TEdit;
    txt_TSMC: TNumericEdit;
    Label4: TLabel;
    txt_Domain: TNumericEdit;
    TabConexion: TTabSheet;
    TabFTP: TTabSheet;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    txt_port3: TNumericEdit;
    txt_port1: TNumericEdit;
    txt_port2: TNumericEdit;
    txt_ip: TMaskEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    txt_ftpUser: TEdit;
    txt_ftpIP: TMaskEdit;
    txt_ftpPort: TNumericEdit;
    Label14: TLabel;
    TabOpciones: TTabSheet;
    txt_path: TEdit;
    Label16: TLabel;
    txt_user: TEdit;
    Label13: TLabel;
    txt_FTPPassword: TMaskEdit;
    txt_ftppasswordVerificar: TMaskEdit;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cbConcesionariasChange(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
    function PuedeGrabar: boolean;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormTSMC: TFormTSMC;

implementation
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del TSCM.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar TSMC';

    MSG_IP                  = 'Debe indicar una direcci�n IP del TSMC valida.';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n.';
    MSG_PORT1               = 'Debe indicar el n�mero del puerto nro 1.';
    MSG_PORT2               = 'Debe indicar el n�mero del puerto nro 2.';
    MSG_PORT3               = 'Debe indicar el n�mero del puerto nro 3.';
    MSG_FTPIP               = 'Debe indicar una direcci�n IP del servidor FTP valida.';
    MSG_FTPUSER             = 'Debe indicar el usuario del servidor FTP.';
    MSG_FTPPASSWORD         = 'Debe indicar el password del servidor FTP.';
    MSG_FTPPORT             = 'Debe indicar el n�mero de puerto del servidor FTP.';
    MSG_PATH                = 'Debe indicar el path de la imagenes.';
    MSG_FTPPASSWORD_ERROR   = 'La verificaci�n del password no coincide con el original';
    MSG_FTPPASSWORD_CARACTER_ERROR = 'El password tiene caracteres inv�lidos';
{$R *.DFM}

function TFormTSMC.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Result := False;

	if not OpenTables([qry_TSMC]) then exit;

    CargarConcesionarias(DMConnections.BaseCAC, cbConcesionarias, 0);
    cbConcesionarias.ItemIndex := 0;

    PageControl.ActivePage := TabGeneral;
    Notebook.PageIndex := 0;
    DBList1.Reload;
   	Result := True;
end;

procedure TFormTSMC.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormTSMC.DBList1Click(Sender: TObject);
var
	i: integer;
begin
	with (Sender AS TDbList).Table do begin

        cbConcesionarias.ItemIndex := -1;
        for i := 0 to cbConcesionarias.Items.Count - 1 do begin
        	if Ival(StrRight(cbConcesionarias.Items[i], 20)) =  FieldByName('CodigoConcesionaria').AsInteger then begin
            	cbConcesionarias.ItemIndex := i;
            	Break;
            end;
        end;

        txt_Domain.Value:=FieldByName('DomainID').Value;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
        txt_TSMC.Value		:= FieldByName('TSMCID').Value;
        txt_ip.Text		:= FieldByName('DireccionIP').Value;
        txt_port1.Value:=FieldByName('Port1').Value;
        txt_port2.Value:=FieldByName('Port2').Value;
        txt_port3.Value:=FieldByName('Port3').Value;
        txt_user.Text:=FieldByName('userid').AsString;
        txt_ftpIP.Text:=FieldByName('ftpServer').AsString;
        txt_ftpUser.Text:=FieldByName('ftpuser').AsString;
        txt_ftpPort.Value:=FieldByName('ftpport').Value;
        txt_ftppassword.Text:=Trim(FieldByName('FTPPassword').AsString);
        txt_ftppasswordVerificar.Text:=Trim(FieldByName('FTPPassword').AsString);
        txt_path.Text:=Trim(FieldByName('ImagePath').AsString);
end;

end;

procedure TFormTSMC.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, qry_TSMC do begin
		FillRect(Rect);

        //Concesionaria
        TextOut(Cols[0], Rect.Top,
          Trim(QueryGetValue(qry_TSMC.Connection,
            'SELECT Descripcion FROM Concesionarias WHERE CodigoConcesionaria = '
            + FieldByName('CodigoConcesionaria').AsString)));

		TextOut(Cols[1], Rect.Top, Tabla.FieldbyName('DomainID').AsString);
		TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('TSMCID').AsString);
		TextOut(Cols[3], Rect.Top, Tabla.FieldbyName('Descripcion').AsString);
        TextOut(Cols[4], Rect.Top, Tabla.FieldbyName('DireccionIP').AsString);
        TextOut(Cols[5], Rect.Top, Tabla.FieldbyName('Port1').AsString);
        TextOut(Cols[6], Rect.Top, Tabla.FieldbyName('Port2').AsString);
        TextOut(Cols[7], Rect.Top, Tabla.FieldbyName('Port3').AsString);
        TextOut(Cols[8], Rect.Top, Tabla.FieldbyName('UserID').AsString);
        TextOut(Cols[9], Rect.Top, Tabla.FieldbyName('FTPServer').AsString);
        TextOut(Cols[10], Rect.Top, Tabla.FieldbyName('FTPUser').AsString);
	end;
end;

procedure TFormTSMC.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
	TabGeneral.Enabled := True;
	TabConexion.Enabled := True;
	TabFTP.Enabled := True;
    TabOpciones.Enabled:=True;
	PageControl.ActivePage := TabGeneral;
    txt_Descripcion.SetFocus;

    // No se puede editar los primary key
    cbConcesionarias.Enabled:=false;
    txt_Domain.Enabled:=false;
    txt_TSMC.Enabled:=False;

end;

procedure TFormTSMC.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormTSMC.Limpiar_Campos();
begin
        txt_Domain.Clear;
		txt_Descripcion.Clear;
        txt_TSMC.Clear;
        txt_ip.Clear;
        txt_port1.Clear;
        txt_port2.Clear;
        txt_port3.Clear;
        txt_user.Clear;
        txt_ftpIP.Clear;
        txt_ftpUser.Clear;
        txt_ftpPort.Clear;
        txt_ftppassword.Clear;
        txt_ftppasswordVerificar.Clear;
        txt_path.Clear;
end;

procedure TFormTSMC.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormTSMC.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
    if PuedeGrabar then begin
        With DbList1.Table do begin
            Try
                Edit;

                FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);
                FieldByName('direccionIP').Value     := Trim(copy(txt_ip.text,1,3))+'.'+Trim(copy(txt_ip.text,5,3))+'.'+Trim(copy(txt_ip.text,9,3))+'.'+Trim(copy(txt_ip.text,13,3));
                FieldByName('Port1').Value   	     := Trim(txt_port1.text);
                FieldByName('Port2').Value   	     := Trim(txt_port2.text);
                FieldByName('Port3').Value   	     := Trim(txt_port3.text);
                FieldByName('FTPServer').Value   	 := Trim(copy(txt_FTPip.text,1,3))+'.'+Trim(copy(txt_FTPip.text,5,3))+'.'+Trim(copy(txt_FTPip.text,9,3))+'.'+Trim(copy(txt_FTPip.text,13,3));
                FieldByName('FTPUser').Value   	     := Trim(txt_ftpUser.text);
                FieldByName('FTPPassword').Value   	 := Trim(txt_FTPPassword.text);
                FieldByName('FTPPort').Value   	     := Trim(txt_ftpPort.text);
                FieldByName('ImagePath').Value   	 := Trim(txt_path.text);
                FieldByName('UserId').Value   	     := Trim(txt_user.text);

                Post;
            except
                On E: EDataBaseError do begin
                    Cancel;
                    MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                end;
            end;
        end;
        Volver_Campos;
    end;
	Screen.Cursor := crDefault;
end;

procedure TFormTSMC.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormTSMC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormTSMC.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormTSMC.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
    PageControl.ActivePage:=TabGeneral;
    //groupb.Enabled     := False;

	TabGeneral.Enabled := false;
	TabConexion.Enabled := false;
	TabFTP.Enabled := false;
    TabOpciones.Enabled:=False;

end;

procedure TFormTSMC.cbConcesionariasChange(Sender: TObject);
begin
    CargarSectoresConcesionaria(DMConnections.BaseCAC, cbConcesionarias,
                                Ival(StrRight(cbConcesionarias.Text, 20)));
end;

function TFormTSMC.PuedeGrabar: boolean;
var
    i:integer;
begin
    result:=True;

    if not ValidateControls([txt_Descripcion,
                            txt_ip,
                            txt_port1,
                            txt_port2,
                            txt_port3,
                            txt_ftpIP,
                            txt_ftpUser,
                            txt_ftppassword,
                            txt_ftpPort,
                            txt_path],
                           [trim(txt_Descripcion.text) <> '',
                            (Trim(copy(txt_ip.text,1,3)) <>'') and (Trim(copy(txt_ip.text,5,3))<>'') and (Trim(copy(txt_ip.text,9,3))+'.'+Trim(copy(txt_ip.text,13,3))<>''),
                            trim(txt_port1.text) <> '',
                            trim(txt_port2.text) <> '',
                            trim(txt_port3.text) <> '',
                            (Trim(copy(txt_ftpip.text,1,3)) <>'') and (Trim(copy(txt_ftpip.text,5,3))<>'') and (Trim(copy(txt_ftpip.text,9,3))+'.'+Trim(copy(txt_ftpip.text,13,3))<>''),
                            trim(txt_ftpUser.text) <> '',
                            trim(txt_ftpPassword.text) <> '',
                            trim(txt_ftpPort.text) <> '',
                            trim(txt_path.text) <> '' ],
                            MSG_ACTUALIZAR_CAPTION,
                           [MSG_DESCRIPCION,
                            MSG_IP,
                            MSG_PORT1,
                            MSG_PORT2,
                            MSG_PORT3,
                            MSG_FTPIP,
                            MSG_FTPUSER,
                            MSG_FTPPASSWORD,
                            MSG_FTPPORT,
                            MSG_PATH ]) then begin result:=false; exit; end;

    //los unicos caracteres validos son A-Z,a-z,0-9

    for i:=1 to length(txt_FTPPassword.Text) do begin
        if not (
           ((ord(txt_FTPPassword.Text[i])>=ord('A')) and (ord(txt_FTPPassword.Text[i])<=ord('Z'))) or
           ((ord(txt_FTPPassword.Text[i])>=ord('a')) and (ord(txt_FTPPassword.Text[i])<=ord('z'))) or
           ((ord(txt_FTPPassword.Text[i])>=ord('0')) and (ord(txt_FTPPassword.Text[i])<=ord('9'))) ) then begin

            MsgBox( MSG_FTPPASSWORD_CARACTER_ERROR,MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
            result:=false;
            exit;
        end;
    end;


    if trim(txt_ftppassword.Text)<>trim(txt_ftppasswordVerificar.Text) then begin
        MsgBox( MSG_FTPPASSWORD_ERROR,MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
        result:=false;
        exit;
    end;


end;


end.
