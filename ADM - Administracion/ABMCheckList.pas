unit ABMCheckList;


{
Firma       :   SS_1147_MCA_20150304
Descripcion :   se agrega la funcionalidad salir a la barra de herramientas.
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DbList, Abm_obj, DB, ADODB, UtilProc, UtilDB, PeaProcs;

type
  TFrmCheckList = class(TForm)
    AbmToolbar1: TAbmToolbar;
    dblChecklist: TAbmList;
    GroupB: TPanel;
    Label15: TLabel;
    Label3: TLabel;
    txtCodigo: TEdit;
    txtdescripcion: TEdit;
    Panel2: TPanel;
    Panel1: TPanel;
    tblChecklist: TADOTable;
    tblChecklistCodigoCheckList: TIntegerField;
    tblChecklistDescripcion: TStringField;
    Notebook: TNotebook;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    qryMaxCheckList: TADOQuery;
    qryVerificarCheckList: TADOQuery;
    procedure dblChecklistDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dblChecklistEdit(Sender: TObject);
    procedure dblChecklistClick(Sender: TObject);
    procedure dblChecklistRefresh(Sender: TObject);
    procedure LimpiarCampos;
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblChecklistDelete(Sender: TObject);
    procedure dblChecklistInsert(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);

  private
    { Private declarations }
  public
    function inicializar: boolean;
  end;

var
  FrmCheckList: TFrmCheckList;

implementation
resourcestring
    MSG_DESCRIPCION         = 'Debe especificar una descripci�n.';
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el �tem seleccionado?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar el �tem seleccionado.';
    MSG_DELETE_FK_ERROR		= 'El �tem tiene asociado convenios';
    MSG_DELETE_CAPTION		= 'Eliminar Item';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del �tem seleccionado.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Item';

{$R *.dfm}

{ TFrmCheckList }

function TFrmCheckList.inicializar: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblChecklist]) then
		Result := False
	else begin
		Result := True;
		dblChecklist.Reload;
	end;
end;

procedure TFrmCheckList.dblChecklistDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
var
    i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to tabla.FieldCount-1 do
       		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFrmCheckList.dblChecklistEdit(Sender: TObject);
begin
	Notebook.PageIndex := 1;
	dblCheckList.Enabled     := False;
    dblCheckList.Estado      := modi;
    groupb.Enabled          := True;
    txtdescripcion.setFocus;
end;

procedure TFrmCheckList.dblChecklistClick(Sender: TObject);
begin
	 with tblCheckList do begin
          txtCodigo.Text:= FieldByName('CodigoChecklist').AsString;
		  txtdescripcion.Text 	:= Trim(FieldByName('Descripcion').AsString);
     end;
end;

procedure TFrmCheckList.dblChecklistRefresh(Sender: TObject);
begin
	 if dblCheckList.Empty then LimpiarCampos;
end;

procedure TFrmCheckList.LimpiarCampos;
begin
	txtdescripcion.Clear;
	txtCodigo.Clear;
end;


procedure TFrmCheckList.AbmToolbar1Close(Sender: TObject);
begin
    Close;                                                                      //SS_1147_MCA_20150304
end;

procedure TFrmCheckList.BtnAceptarClick(Sender: TObject);
begin
    if not (ValidateControls([txtdescripcion],
        [trim(txtdescripcion.Text)<>''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_DESCRIPCION]) ) then exit;

 	Screen.Cursor := crHourGlass;
	With dblChecklist.Table do begin
		Try
			if dblChecklist.Estado = Alta then begin
            	Append;
                qryMaxCheckList.Open;
                txtCodigo.Text	:=  inttostr(qryMaxCheckList.FieldByNAme('CodigoChecklist').AsInteger + 1);
				qryMaxCheckList.Close;
			end else begin
            	Edit;
            end;
            FieldByName('CodigoChecklist').AsInteger 	:= strtoint(txtCodigo.Text);
			if Trim(txtDescripcion.text) = '' then
            	FieldByName('Descripcion').Clear
            else
				FieldByName('Descripcion').AsString	 := Trim(txtDescripcion.text);

			Post;
		except
			On E: Exception do begin
                Screen.Cursor := crDefault;
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	LimpiarCampos;
    dblChecklist.Reload;
	dblChecklist.Estado     := Normal;
	dblChecklist.Enabled    := True;
 	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
	Screen.Cursor := crDefault;
end;

procedure TFrmCheckList.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFrmCheckList.dblChecklistDelete(Sender: TObject);
var
    i: integer;
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
(*
             qryVerificarCheckList.Parameters.ParamByName('CodigoCheckList').Value :=  tblChecklist.FieldByName('CodigoCheckList').AsInteger;
             qryVerificarCheckList.Open;
             if qryVerificarCheckList.RecordCount > 0 then begin
                MsgBoxErr(MSG_DELETE_ERROR, MSG_DELETE_FK_ERROR, MSG_DELETE_CAPTION, MB_ICONSTOP);
             end else
			    (Sender AS TDbList).Table.Delete;
             qryVerificarCheckList.Close;
*)
            tblChecklist.Connection.Errors.Clear;
            (Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
                i:= 0;
                while (i <= tblChecklist.Connection.Errors.Count - 1) and (tblChecklist.Connection.Errors.Item[i].NativeError <> 547) (*Error de FK*) do begin
                    ShowMessage(inttostr(tblChecklist.Connection.Errors.Item[i].NativeError));
                    inc(i);
                end;
                if (i <= tblChecklist.Connection.Errors.Count - 1) then
                    MsgBoxErr(MSG_DELETE_ERROR, MSG_DELETE_FK_ERROR, MSG_DELETE_CAPTION, MB_ICONSTOP)
                else
    				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		dblChecklist.Reload;
	end;
	dblChecklist.Estado     := Normal;
	dblChecklist.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFrmCheckList.dblChecklistInsert(Sender: TObject);
begin
    groupb.Enabled     := True;
	LimpiarCampos;
	dblChecklist.Enabled    := False;
	Notebook.PageIndex := 1;
	txtDescripcion.SetFocus;
end;

procedure TFrmCheckList.BtnCancelarClick(Sender: TObject);
begin
	dblChecklist.Estado     := Normal;
	dblChecklist.Enabled    := True;
    dblChecklist.Reload;
	dblChecklist.SetFocus;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

procedure TFrmCheckList.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

end.
