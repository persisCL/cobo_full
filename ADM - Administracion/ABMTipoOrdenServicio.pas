unit ABMTipoOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, Abm_obj, DmiCtrls, PeaProcs, ADODB,CheckLst,
  DBCtrls, VariantComboBox, variants, DPSControls, peatypes, Workflowdsgn;

type
  TFormTiposOrdenServicio = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    GroupB: TPanel;
    Label1: TLabel;
    txt_Descripcion: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
    Workflows: TADOQuery;
    Label3: TLabel;
    TblCategoriasOrdenServicio: TADOTable;
    dsCategoriasOrdenServicio: TDataSource;
    dsWorkfows: TDataSource;
    Label4: TLabel;
    MotivosContacto: TADOTable;
    dsMotivosContactos: TDataSource;
    cbMotivosContacto: TVariantComboBox;
    Panel3: TPanel;
    btnEditarWorkflow: TSpeedButton;
    cbCategoriaOrdenServicio: TVariantComboBox;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    ActualizarTipoOrdenServicio: TADOStoredProc;
    chkAplicaSoloClientes: TCheckBox;
    Label2: TLabel;
    EliminarTipoOrdenServicio: TADOStoredProc;
    Label5: TLabel;
    chkCalendario: TCheckBox;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
    procedure btnEditarWorkflowClick(Sender: TObject);
    procedure ListaInsert(Sender: TObject);
    procedure ListaDelete(Sender: TObject);

  private
	{ Private declarations }
	procedure LimpiarCampos;
	procedure VolverCampos;
  public
	{ Public declarations }
	Function Inicializa: boolean;
  end;

var
  FormTiposOrdenServicio  : TFormTiposOrdenServicio;



implementation

uses DMConnection;



{$R *.DFM}


resourcestring
    MSG_MOTIVO_CONTACTO = '<Ninguno>';

procedure TFormTiposOrdenServicio.VolverCampos;
begin
	Lista.Estado     			:= Normal;
	GroupB.Enabled   			:= False;
	Lista.Enabled    			:= True;
	Notebook.PageIndex			:= 0;
    btnEditarWorkflow.Enabled	:= True;
	txt_Descripcion.Color 		:= $00FAEBDE;
    txt_Descripcion.Enabled 	:= False;
	cbCategoriaOrdenServicio.Enabled	:= True;
    cbMotivosContacto.Enabled 			:= True;
	Lista.Reload;
    Screen.Cursor 				:= crDefault;
    Lista.SetFocus;
end;

function TFormTiposOrdenServicio.Inicializa: boolean;
Var
	S: TSize;
begin
	Result := False;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([Workflows, TblCategoriasOrdenServicio, MotivosContacto]) then Exit;

	CargarMotivosContacto(DMCOnnections.BaseCAC, cbMotivosContacto, True, 0);
    CargarCategoriasOrdenServicio(DMCOnnections.BaseCAC, cbCategoriaOrdenServicio, True, 0);

	VolverCampos;

	Result := True;
end;

procedure TFormTiposOrdenServicio.LimpiarCampos;
begin
	txt_Descripcion.Clear;
end;

function TFormTiposOrdenServicio.ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
begin
	 Result := True;
	 Texto := Tabla.FieldByName('CodigoWorkflow').AsString + ' ' +
	   Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormTiposOrdenServicio.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFormTiposOrdenServicio.ListaEdit(Sender: TObject);
resourceString
	MSG_EDITAR_TIPO_OS_CAPTION = 'Editar Tipo de �rden de Servicio';
	MSG_EDITAR_TIPO_OS_ERROR   = 'No se pueden editar los Tipos de �rdenes de Servicio definidos por el Sistema';
begin
	btnEditarWorkFlow.Enabled 	:= False;
    if Workflows.FieldByName('CodigoWorkflow').AsInteger <= MAX_OS_DEFINIDAS then begin
		MsgBox(MSG_EDITAR_TIPO_OS_ERROR,MSG_EDITAR_TIPO_OS_CAPTION, MB_ICONSTOP);
        VolverCampos;
    	Exit;
    end;
	Lista.Estado     			:= Modi;
	GroupB.Enabled   			:= True;
	Lista.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    txt_Descripcion.Color		:= $00FAEBDE;
	cbCategoriaOrdenServicio.SetFocus;
end;

procedure TFormTiposOrdenServicio.ListaClick(Sender: TObject);
begin
	txt_Descripcion.text := Trim(Workflows.FieldByName('Descripcion').AsString);

	//CargarMotivosContacto(DMCOnnections.BaseCAC, cbMotivosContacto, True, Workflows.FieldByName('MotivoContacto').asInteger);
    //CargarCategoriasOrdenServicio(DMCOnnections.BaseCAC, cbCategoriaOrdenServicio, True, Workflows.FieldByName('CategoriaOrdenServicio').asInteger);    
    MarcarItemComboVariant(cbMotivosContacto, Workflows.FieldByName('MotivoContacto').asInteger);
    MarcarItemComboVariant(cbCategoriaOrdenServicio, Workflows.FieldByName('CategoriaOrdenServicio').asInteger);
    
	chkAplicaSoloClientes.Checked := Workflows.FieldByName('AplicaSoloClientes').AsBoolean;
    chkCalendario.Checked         := Workflows.FieldByName('Calendario').AsBoolean;
end;

procedure TFormTiposOrdenServicio.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then LimpiarCampos;
end;

procedure TFormTiposOrdenServicio.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormTiposOrdenServicio.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0] + 2, Rect.Top, FieldByName('CodigoWorkflow').AsString);
		TextOut(Cols[1] + 2, Rect.Top, FieldByName('Descripcion').AsString);
        TextOut(Cols[2] + 2, Rect.Top, FieldByName('DescCategoria').AsString);
        TextOut(Cols[3] + 2, Rect.Top, FieldByName('DescMotivo').AsString);
        TextOut(Cols[4] + 2, Rect.Top, FieldByName('SoloClientes').AsString);
        TextOut(Cols[5] + 2, Rect.Top, FieldByName('PermiteProgramacion').AsString);
	end;
end;

procedure TFormTiposOrdenServicio.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Tipo de Orden de Servicio.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Tipo de Orden de Servicio';
var
	TipoOSActual: TBookmark;
    EstadoLista: TAbmState;
begin

	TipoOSActual := Lista.Table.GetBookmark;

	try
        Screen.Cursor := crHourGlass;
        try
            With ActualizarTipoOrdenServicio do begin
                Parameters.ParamByName('@Descripcion').Value 			:= Trim(txt_Descripcion.Text);
                Parameters.ParamByName('@CategoriaOrdenServicio').Value := iif( cbCategoriaOrdenServicio.Value < 1, null, cbCategoriaOrdenServicio.Value);
                Parameters.ParamByName('@MotivoContacto').Value 		:= iif( cbMotivosContacto.Value < 1, null, cbMotivosContacto.Value);
                Parameters.ParamByName('@VersionActual').Value 			:= iif(Lista.Estado = Alta, 1, Lista.Table.FieldByName('VersionActual').AsInteger);
                Parameters.ParamByName('@AplicaSoloClientes').Value 	:= chkAplicaSoloClientes.Checked;
                Parameters.ParamByName('@Calendario').Value 	        := chkCalendario.Checked;
                Parameters.ParamByName('@CodigoWorkFlow').Value 		:= iif(Lista.Estado = Alta, null, Lista.Table.FieldByName('CodigoWorkflow').AsInteger);
            end;
            ActualizarTipoOrdenServicio.ExecProc;
            ActualizarTipoOrdenServicio.Close;
        except
            On E: exception do begin
                MsgBoxErr(MSG_ACTUALIZAR_ERROR, e.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                ActualizarTipoOrdenServicio.Close;
                Exit;
                Screen.Cursor := crDefault;
            end;
        end;

        // Listo
        EstadoLista := Lista.Estado;

        if EstadoLista = Modi then begin
        	Lista.Table.GotoBookmark(TipoOsActual);
        end else begin
	        Lista.Table.Close;
	        Lista.Table.Open;
         	Lista.Table.DisableControls;
            Lista.Table.Last;
            Lista.Table.EnableControls;
        end;
	finally
    	Lista.Table.FreeBookmark(TipoOSActual);
    end;
	VolverCampos;
end;

procedure TFormTiposOrdenServicio.BtnCancelarClick(Sender: TObject);
begin
	VolverCampos;
end;

procedure TFormTiposOrdenServicio.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormTiposOrdenServicio.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormTiposOrdenServicio.btnEditarWorkflowClick(Sender: TObject);
resourcestring
	MSG_CAPTION = 'Editor de Workflows para la �rden de Servicio: %s';

Var
	f: TWorkflowDesigner;
begin
	Application.CreateForm(TWorkflowDesigner, f);
    f.Caption := Format(MSG_CAPTION, [Trim(Workflows.fieldByName('Descripcion').AsString)]);
	if f.Inicializa(Workflows.fieldByName('CodigoWorkflow').AsInteger) then f.ShowModal;
	f.Release;
end;

procedure TFormTiposOrdenServicio.ListaInsert(Sender: TObject);
begin
	LimpiarCampos;
	Lista.Estado     := Alta;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
    btnEditarWorkflow.Enabled := False;

	CargarMotivosContacto(DMCOnnections.BaseCAC, cbMotivosContacto, True, 6);
    CargarCategoriasOrdenServicio(DMCOnnections.BaseCAC, cbCategoriaOrdenServicio, True, CAT_OS_MANTENIMIENTO);
    cbMotivosContacto.Enabled := False;
	cbCategoriaOrdenServicio.Enabled := False;

	txt_Descripcion.Color 	:= $00FAEBDE;
    txt_Descripcion.Enabled := True;
    txt_Descripcion.SetFocus;
end;



procedure TFormTiposOrdenServicio.ListaDelete(Sender: TObject);
resourceString
	MSG_ELIMINAR_TIPO_OS_CAPTION 		= 'Eliminar Tipo de �rden de Servicio';
	MSG_ELIMINAR_TIPO_OS_ERROR   		= 'No se pueden eliminar los Tipos de �rdenes de Servicio definidos por el Sistema';
    MSG_ELIMINAR_TIPO_ERROR_REFERENCIAS	= 'No se pudieron eliminar las tablas vinculadas al Tipo de �rde de Servicio';

	MSG_ELIMINAR_TIPO_OS_ERROR_OS   =
    	'No se pueden eliminar el Tipo de �rde de Servicio, ' + #13#10 +
    	'porque hay �rdenes de Servicio relacionadas';
var
	Resultado, FCodigo: integer;
    TipoOSActual: TBookmark;
begin
    FCodigo			:= -1;
	//Borro un Tipo de Orden de Servicio si es de las que cre� el cliente.
    // Las que son provistas por el sistema no pueden ser modificadas.
    if Lista.Table.FieldByName('CodigoWorkflow').AsInteger <= MAX_OS_DEFINIDAS then begin
		MsgBox(MSG_ELIMINAR_TIPO_OS_ERROR,MSG_ELIMINAR_TIPO_OS_CAPTION, MB_ICONSTOP);
    	Exit;
    end;

    //Elimino
	TipoOSActual := Lista.Table.GetBookmark;
    try
        screen.Cursor	:= crHourGlass;
        Resultado 		:= -1;

        try
        	FCodigo := Workflows.fieldByName('CodigoWorkflow').AsInteger;
            with EliminarTipoOrdenServicio do begin
                Parameters.ParamByName('@CodigoWorkflow').Value := Workflows.fieldByName('CodigoWorkflow').AsInteger;
                ExecProc;
                Resultado := Parameters.ParamByName('@RETURN_VALUE').Value;
            end;
            EliminarTipoOrdenServicio.Close;
            case Resultado of
                -1: MsgBox( MSG_ELIMINAR_TIPO_ERROR_REFERENCIAS, MSG_ELIMINAR_TIPO_OS_CAPTION, MB_ICONSTOP);
                -2: MsgBox( MSG_ELIMINAR_TIPO_OS_ERROR_OS, MSG_ELIMINAR_TIPO_OS_CAPTION, MB_ICONSTOP);
            end;

        except
            On E: Exception do begin
                EliminarTipoOrdenServicio.Close;
                if Resultado = -1 then MsgBox( MSG_ELIMINAR_TIPO_ERROR_REFERENCIAS, MSG_ELIMINAR_TIPO_OS_CAPTION, MB_ICONSTOP);
            end;
        end;
        Screen.Cursor := crDefault;

        if Resultado = 0 then begin
            //Me paro en el registro anterior
            Lista.Table.Close;
            Lista.Table.Open;
        	Lista.Table.DisableControls;
            Lista.Table.First;
            While not Lista.Table.Eof do begin
                if (FCodigo = Lista.Table.FieldByName('CodigoWorkflow').AsInteger) or
                (Lista.Table.FieldByName('CodigoWorkflow').AsInteger > FCodigo) then break;
            	Lista.Table.Next;
            end;
        	Lista.Table.EnableControls;
        end else begin
            //Me paro en el registro que estaba porque no pas� nada.
            Lista.Table.GotoBookmark(TipoOSActual);
        end;

	finally
	   	Lista.Table.FreeBookmark( TipoOSActual);
	end;
    VolverCampos;
end;



end.
