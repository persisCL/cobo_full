object FormPreguntaSolicitud: TFormPreguntaSolicitud
  Left = 304
  Top = 242
  BorderStyle = bsDialog
  Caption = 'Fuentes de Solicitud'
  ClientHeight = 137
  ClientWidth = 413
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl_BotonesGeneral: TPanel
    Left = 0
    Top = 98
    Width = 413
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object BtnAceptar: TButton
      Left = 240
      Top = 7
      Width = 79
      Height = 26
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = BtnAceptarClick
    end
    object BtnCancelar: TButton
      Left = 327
      Top = 7
      Width = 79
      Height = 26
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = BtnCancelarClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 8
    Width = 409
    Height = 89
    Caption = 'Fuente de Solicitud de la Pregunta'
    TabOrder = 1
    object Label1: TLabel
      Left = 10
      Top = 59
      Width = 75
      Height = 13
      Caption = '&Probabilidad:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 27
      Width = 72
      Height = 13
      Caption = '&Descripcion:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cb_FuenteSolicitud: TComboBox
      Left = 91
      Top = 23
      Width = 294
      Height = 21
      Hint = 'Tipo de Pregunta'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      MaxLength = 4
      TabOrder = 0
    end
    object txt_Proba: TNumericEdit
      Left = 91
      Top = 52
      Width = 42
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 1
      Decimals = 0
      Value = 50.000000000000000000
    end
  end
end
