{-----------------------------------------------------------------------------
 Unit Name: ABMMotivosTAGNoDetectado
 Author:    ggomez
 Purpose: Permitir gestionar los motivos por los cuales un TAG no fue
    detectado.
    S�lo permite hacer Modificaciones sobre los datos de la tabla
    MotivosTAGNoDetectado.
 History:
-----------------------------------------------------------------------------}
unit ABMMotivosTAGNoDetectado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Abm_obj, DmiCtrls, ComCtrls,
  DbList, DB, ADODB,
  DMConnection, UtilProc, StrUtils, UtilDB, RStrings, PeaProcs;

type
  TFormABMMotivosTAGNoDetectado = class(TForm)
    AbmToolBar: TAbmToolbar;
    pnl_Bottom: TPanel;
    lbl_HintDeshabilitado: TLabel;
    nb_Botones: TNotebook;
    pnl_HintDeshabilitado: TPanel;
    dbl_Motivos: TAbmList;
    pnl_Datos: TPanel;
    Ldescripcion: TLabel;
    txt_Descripcion: TEdit;
    lbl_CodigoMotivo: TLabel;
    txt_CodigoMotivo: TNumericEdit;
    chb_Habilitado: TCheckBox;
    ds_MotivosTAGNoDetectado: TDataSource;
    tbl_MotivosTAGNoDetectado: TADOTable;
    sp_ActualizarMotivoTAGNoDetectado: TADOStoredProc;
    btn_Salir: TButton;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure dbl_MotivosClick(Sender: TObject);
    procedure dbl_MotivosDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dbl_MotivosEdit(Sender: TObject);
    procedure dbl_MotivosRefresh(Sender: TObject);
    procedure AbmToolBarClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarCampos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  FormABMMotivosTAGNoDetectado: TFormABMMotivosTAGNoDetectado;

resourcestring
	STR_MAESTRO_MOTIVOSTAGNODETECTADO = 'Maestro de Motivos de Telev�a No Detectado';

implementation

{$R *.dfm}

{ TFormABMMotivosTAGNoDetectado }

procedure TFormABMMotivosTAGNoDetectado.HabilitarCampos;
begin
	dbl_Motivos.Enabled    		:= False;
	nb_Botones.PageIndex 		:= 1;
    pnl_Datos.Enabled           := True;
    txt_CodigoMotivo.Enabled    := False;
    txt_Descripcion.SetFocus;
end;

function TFormABMMotivosTAGNoDetectado.Inicializar(txtCaption: String;
    MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
	Result := False;

	if MDIChild then begin
        FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	nb_Botones.PageIndex := 0;

   	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([tbl_MotivosTAGNoDetectado]) then Exit;

    // Limpiar controles de ingreso de datos.
    LimpiarCampos;

    KeyPreview := True;
	dbl_Motivos.Reload;
	Result := True;
end;

procedure TFormABMMotivosTAGNoDetectado.LimpiarCampos;
begin
	txt_CodigoMotivo.Clear;
	txt_Descripcion.Clear;
    chb_Habilitado.Checked := False;
end;

procedure TFormABMMotivosTAGNoDetectado.VolverCampos;
begin
	dbl_Motivos.Estado      := Normal;
	dbl_Motivos.Enabled    	:= True;
	dbl_Motivos.SetFocus;
	nb_Botones.PageIndex 	:= 0;
    pnl_Datos.Enabled       := False;
	dbl_Motivos.Reload;
end;

procedure TFormABMMotivosTAGNoDetectado.btn_CancelarClick(
  Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormABMMotivosTAGNoDetectado.dbl_MotivosClick(
  Sender: TObject);
begin
	with tbl_MotivosTAGNoDetectado do begin
		txt_CodigoMotivo.Value	 := FieldByName('CodigoMotivoTAGNoDetectado').AsInteger;
		txt_Descripcion.Text := FieldByName('Descripcion').AsString;
        chb_Habilitado.Checked := not FieldByName('BajaLogica').AsBoolean;
	end;
end;

procedure TFormABMMotivosTAGNoDetectado.dbl_MotivosDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	with Sender.Canvas do begin
    	FillRect(Rect);
        (* Si est� dado de Baja L�gica mostrarlo en Rojo*)
	    if Tabla.FieldByName('BajaLogica').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end;

      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoMotivoTAGNoDetectado').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
	end;
end;

procedure TFormABMMotivosTAGNoDetectado.dbl_MotivosEdit(
  Sender: TObject);
begin
	HabilitarCampos;
    dbl_Motivos.Estado := Modi;
end;

procedure TFormABMMotivosTAGNoDetectado.dbl_MotivosRefresh(
  Sender: TObject);
begin
	if dbl_Motivos.Empty then LimpiarCampos;
end;

procedure TFormABMMotivosTAGNoDetectado.AbmToolBarClose(
  Sender: TObject);
begin
    Close;
end;

procedure TFormABMMotivosTAGNoDetectado.FormShow(Sender: TObject);
begin
    dbl_Motivos.Reload;
end;

procedure TFormABMMotivosTAGNoDetectado.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Action := caFree;
end;

procedure TFormABMMotivosTAGNoDetectado.btn_AceptarClick(
  Sender: TObject);
resourcestring
    MSG_DESCRIPCION_EXISTENTE = 'La descripci�n ya existe para otro motivo.';
    CAPTION_TITULO = 'Actualizar Motivo';
begin
	if not ValidateControls([txt_Descripcion],
            [(Trim(txt_Descripcion.Text) <> EmptyStr)],
	        Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_MOTIVOSTAGNODETECTADO]),
            [Format(MSG_VALIDAR_DEBE_LA, [FLD_DESCRIPCION])]) then begin
		Exit;
	end; // if

    (* Si estoy modificando y la Descripci�n es distinta al valor original,
        controlar que la Descripci�n NO exista en la tabla. *)
    if (dbl_Motivos.Estado = Modi)
            and (Trim(txt_Descripcion.Text) <> Trim(dbl_Motivos.Table.FieldByName('Descripcion').AsString))
        then begin
            if dbl_Motivos.Table.Locate('Descripcion', txt_Descripcion.Text, []) then begin
                MsgBox(MSG_DESCRIPCION_EXISTENTE, CAPTION_TITULO, MB_ICONSTOP);
                Exit;
            end;
    end; // if

	with dbl_Motivos do begin
		Screen.Cursor := crHourGlass;
		try
			try
                case Estado of
                    Modi:   begin
                                with sp_ActualizarMotivoTAGNoDetectado, Parameters do begin
                                    ParamByName('@CodigoMotivo').Value := Table.FieldByName('CodigoMotivoTAGNoDetectado').AsInteger;
                                    ParamByName('@Descripcion').Value := Trim(txt_Descripcion.Text);
                                    ParamByName('@BajaLogica').Value := not chb_Habilitado.Checked;
                                    ExecProc;
                                end;
                            end;
                end; // case
			except
				on E: Exception do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR, [STR_MAESTRO_MOTIVOSTAGNODETECTADO]),
                        E.Message, Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_MOTIVOSTAGNODETECTADO]),
                        MB_ICONSTOP);
				end;
			end;
		finally
            VolverCampos;
			Screen.Cursor := crDefault;
            Reload;
		end;
	end; // with
end;

procedure TFormABMMotivosTAGNoDetectado.btn_SalirClick(
  Sender: TObject);
begin
     Close;
end;

end.
