{********************************** File Header ********************************
File Name   : FrmMaestroAlmacenes.pas
Author      : pmarrone
Date Created: 26/02/2004
Language    : ES-AR
Description : Esta Unit realiza la administración del Maestro de Almacenes.
*******************************************************************************}

unit FrmLugaresVenta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, Abm_obj, OleCtrls, DmiCtrls, Grids,DmConnection, ADODB,
  Variants, ComCtrls, PeaProcs, Validate, BuscaTab, MaskCombo,
  DPSControls, Peatypes, TimeEdit, Provider, DBClient, ListBoxEx, DBListEx,
  FrmEditarDomicilio, FrmEditarMedioComunicacion, DBGrids, BuscaClientes, ABMPuntosVenta;

type
  TFormLugaresVenta = class(TForm)
    pnlInferior: TPanel;
	AbmToolbar1: TAbmToolbar;
    Notebook: TNotebook;
    alLugaresDeVenta: TAbmList;
    tblLugaresDeVenta: TADOTable;
    pcDatosPersonal: TPageControl;
    tsCliente: TTabSheet;
    tsDomicilio: TTabSheet;
    tsContacto: TTabSheet;
    pnlDatosPersonal: TPanel;
    lblRazonSocial: TLabel;
    txtRazonSocial: TEdit;
    lblNombreDeFantasia: TLabel;
    txtNombreDeFantasia: TEdit;
    mcTipoNumeroDocumento: TMaskCombo;
    TipoNumeroDocumento: TLabel;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    txtContacto: TEdit;
    lblContacto: TLabel;
    cbSituacionIVA: TComboBox;
    ObtenerMediosComunicacionLugarDeVenta: TADOStoredProc;
    ActualizarDatosLugarDeVenta: TADOStoredProc;
    Label11: TLabel;
    pnlDomicilio: TPanel;
    dblDomicilios: TDBListEx;
    btnAgregarDomicilio: TDPSButton;
    btnEditarDomicilio: TDPSButton;
    btnEliminarDomicilio: TDPSButton;
    QueryTemp: TADOQuery;
    dsMediosComunicacion: TDataSource;
    cdsMediosComunicacion: TClientDataSet;
    dspMediosComunicacion: TDataSetProvider;
    pnlMediosContacto: TPanel;
    dblMediosComunicacion: TDBListEx;
    btnAgregarMedioComunicacion: TDPSButton;
    btnEditarMedioComunicacion: TDPSButton;
    btnEliminarMedioComunicacion: TDPSButton;
    EliminarLugarDeVenta: TADOStoredProc;
    qryOperadoresLogisticos: TADOQuery;
    btOperadoresLogisticos: TBuscaTabla;
    Panel3: TPanel;
    Label5: TLabel;
    ObtenerDomiciliosLugarDeVenta: TADOStoredProc;
    dsDomicilios: TDataSource;
    cdsDomicilios: TClientDataSet;
    dspDomicilios: TDataSetProvider;
    ActualizarMedioComunicacionLugarDeVenta: TADOStoredProc;
    ObtenerDatosLugarDeVenta: TADOStoredProc;
    EliminarDomiciliosLugarDeVenta: TADOStoredProc;
    EliminarTodosDomiciliosLugarDeVenta: TADOStoredProc;
    ActualizarDomicilioLugarDeVenta: TADOStoredProc;
    ActualizarDomicilioEntregaLugarDeVenta: TADOStoredProc;
    ActualizarDomicilioRelacionadoLugarDeVenta: TADOStoredProc;
    Label1: TLabel;
    bteOperadoresLogisticos: TBuscaTabEdit;
    chkActivo: TCheckBox;
    Panel1: TPanel;
    btnPuntosVenta: TSpeedButton;
	procedure AbmToolbar1Close(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnCancelarClick(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
    procedure alLugaresDeVentaClick(Sender: TObject);
    procedure alLugaresDeVentaDelete(Sender: TObject);
    procedure alLugaresDeVentaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure alLugaresDeVentaEdit(Sender: TObject);
    procedure alLugaresDeVentaInsert(Sender: TObject);
    procedure alLugaresDeVentaRefresh(Sender: TObject);
    procedure IrAlInicio(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Agregar(Sender: TObject);
    procedure btnEliminarDomicilioClick(Sender: TObject);
    procedure btnEditarDomicilioClick(Sender: TObject);
    procedure dblDomiciliosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblMediosComunicacionDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnEliminarMedioComunicacionClick(Sender: TObject);
    procedure btnAgregarMedioComunicacionClick(Sender: TObject);
    procedure btnEditarMedioComunicacionClick(Sender: TObject);
    function btOperadoresLogisticosProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure btOperadoresLogisticosSelect(Sender: TObject;
      Tabla: TDataSet);
    procedure mcTipoNumeroDocumentoChange(Sender: TObject);
    procedure btnPuntosVentaClick(Sender: TObject);

  private
	{ Private declarations }
    procedure HabilitarCamposEdicion(habilitar : Boolean);
	procedure Limpiar_Campos;
	procedure Volver_Campos;
    procedure HabilitarBotonesDomicilios;
    procedure HabilitarBotonesMediosComunicacion;
    procedure CargarMediosComunicacionPersona(CodigoAlmacen: integer; TipoOrigenDato: integer);
    procedure CargarDomiciliosPersona(CodigoAlmacen: integer; TipoOrigenDato: integer);
  public
	{ Public declarations }
	Function Inicializar(MDIChild: Boolean = True): Boolean;

  end;

var
    FormLugaresVenta:    TFormLugaresVenta;
    CodigoAlmacenActual:    integer;
    RecNoDomicilioEntrega:  integer;
    CodigoDomicilioEntrega: integer;

implementation
{$R *.DFM}

uses RStrings;

procedure TFormLugaresVenta.CargarMediosComunicacionPersona(CodigoAlmacen: integer; TipoOrigenDato: integer);
begin
    //Primero intento abrir el Stored Procedure
    ObtenerMediosComunicacionLugarDeVenta.Close;
    ObtenerMediosComunicacionLugarDeVenta.Parameters.ParamByName('@CodigoLugarDeVenta').Value         := CodigoAlmacen;
    ObtenerMediosComunicacionLugarDeVenta.Parameters.ParamByName('@CodigoTipoOrigenDato').Value  := iif(TipoOrigenDato < 1, null, TipoOrigenDato);
    if not OpenTables([ObtenerMediosComunicacionLugarDeVenta]) then Exit;
    cdsMediosComunicacion.Data := dspMediosComunicacion.Data;
    ObtenerMediosComunicacionLugarDeVenta.Close;
    HabilitarBotonesMediosComunicacion;
end;


procedure TFormLugaresVenta.HabilitarBotonesDomicilios;
begin
    btnEditarDomicilio.Enabled      := iif( cdsDomicilios.Active, cdsDomicilios.RecordCount > 0, False);
    btnEliminarDomicilio.Enabled    := btnEditarDOmicilio.Enabled;
end;

procedure TFormLugaresVenta.HabilitarBotonesMediosComunicacion;
begin
    btnEditarMedioComunicacion.Enabled      := iif( cdsMediosComunicacion.Active, cdsMediosComunicacion.RecordCount > 0, False);
    btnEliminarMedioComunicacion.Enabled    := btnEditarMedioComunicacion.Enabled;
end;

procedure TFormLugaresVenta.CargarDomiciliosPersona(CodigoAlmacen: integer; TipoOrigenDato: integer);
begin
    //Primero intento abrir el Stored Procedure
    RecNoDomicilioEntrega := -1;
    ObtenerDomiciliosLugarDeVenta.Close;
    ObtenerDomiciliosLugarDeVenta.Parameters.ParamByName('@CodigoLugarDeVenta').Value         := CodigoAlmacen;
    ObtenerDomiciliosLugarDeVenta.Parameters.ParamByName('@CodigoTipoOrigenDato').Value  := iif(TipoOrigenDato < 1, null, TipoOrigenDato);
    if not OpenTables([ObtenerDomiciliosLugarDeVenta]) then Exit;
    while not ObtenerDomiciliosLugarDeVenta.Eof do begin
        if ObtenerDomiciliosLugarDeVenta.FieldByName('DomicilioEntrega').AsBoolean then begin
            RecNoDomicilioEntrega := ObtenerDomiciliosLugarDeVenta.RecNo;
            break;
        end;
        ObtenerDomiciliosLugarDeVenta.Next;
    end;
    ObtenerDomiciliosLugarDeVenta.First;
    cdsDomicilios.Data := dspDomicilios.Data;
    ObtenerDomiciliosLugarDeVenta.Close;
    HabilitarBotonesDomicilios;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.Volver_Campos
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Vuelve al estado Inicial los controles del Form
********************************************************************************}
procedure TFormLugaresVenta.Volver_Campos ;
begin
    CodigoAlmacenActual := -1;
	alLugaresDeVenta.Estado := Normal;
    HabilitarCamposEdicion(False);
	Notebook.PageIndex		:= 0;
    alLugaresDeVenta.Reload;
    alLugaresDeVenta.Refresh;
    alLugaresDeVenta.SetFocus;
    btnPuntosVenta.Enabled := True;
    Screen.Cursor := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.Inicializar
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    MDIChild: Boolean = True
  Return Value:  Boolean
  Description:   Inicializa el Form de Maestro de Almacenes. Primero determina el
    	formato en que se va a mostrar la pantalla según el parámetro MDIChild y
    	luego intenta abrir la tabla "MaestroAlmacenes"
********************************************************************************}
function TFormLugaresVenta.Inicializar(MDIChild: Boolean = True): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;

    if not OpenTables([tblLugaresDeVenta, qryOperadoresLogisticos]) then Exit;
    CargarTipoSituacionIVA(DMConnections.BaseCAC, cbSituacionIVA);
	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento);
    pcDatosPersonal.ActivePageIndex := 0;
    cdsDomicilios.CreateDataSet;
    Volver_Campos;
	Result := True;
end;



{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.Limpiar_Campos
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    None
  Return Value:  None
  Description:   Limpia los valores que figuran en los controles.
********************************************************************************}
procedure TFormLugaresVenta.Limpiar_Campos;
begin
    //Datos De la Persona
    txtNombreDeFantasia.Clear;
    txtRazonSocial.Clear;
    txtContacto.Clear;
    mcTipoNumeroDocumento.MaskText  := '';
    cbSituacionIVA.ItemIndex        := 0;
    chkActivo.Checked	            := False;
    bteOperadoresLogisticos.Text    := '';
    CargarDomiciliosPersona(-1, 0);
    CargarMediosComunicacionPersona(-1, 0);

end;


procedure TFormLugaresVenta.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;


procedure TFormLugaresVenta.BtnSalirClick(Sender: TObject);
begin
	Close;
end;


procedure TFormLugaresVenta.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	Action := caFree;
end;


procedure TFormLugaresVenta.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;






{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.BtnAceptarClick
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Inserta o Modifica los datos de un Almacen utilizando el
  	procedimiento almacenado "ActualizarMaestroAlmacen".
    Antes de Realizar dichas operaciones verifica que esten completos los
    compos considerados obligatorios.
********************************************************************************}
procedure TFormLugaresVenta.BtnAceptarClick(Sender: TObject);
var
    TodoOk: Boolean;
    CodigosEliminar, CodigosNoEliminar: AnsiString;
    ListaDomicilios: TStringList;
    i, FCodigoDomicilio: integer;
begin

	if (Trim(txtNombreDeFantasia.Text) = '') then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon(Format(MSG_VALIDAR_DETALLE,[LBL_PERSONA_NOMBRE_FANTASIA, STR_LUGARES_VENTA]), Format(MSG_CAPTION_VALIDAR,[STR_LUGARES_VENTA]), MB_ICONSTOP, txtNombreDeFantasia);
    	Exit;
    end;

	if (Trim(bteOperadoresLogisticos.Text) = '') then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon(Format(MSG_VALIDAR_DETALLE,[STR_OPERADOR_LOGISTICO, STR_LUGARES_VENTA]), Format(MSG_CAPTION_VALIDAR,[STR_LUGARES_VENTA]), MB_ICONSTOP, bteOperadoresLogisticos);
    	Exit;
    end;

    if  ((Trim(mcTipoNumeroDocumento.MaskText) <> '') and not mcTipoNUmeroDocumento.ValidateMask()) then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon(Format(MSG_VALIDAR_DETALLE,[FLD_CODIGO_DOCUMENTO,STR_LUGARES_VENTA]), Format(MSG_CAPTION_VALIDAR,[STR_LUGARES_VENTA]), MB_ICONSTOP, mcTipoNumeroDocumento);
    	Exit;
    end;

    if ((Trim(mcTipoNumeroDocumento.MaskText) <> '') and (txtRazonSocial.Text = '')) then begin
    	pcDatosPersonal.ActivePageIndex := 0;
		MsgBoxBalloon(Format(MSG_VALIDAR_DETALLE,[LBL_PERSONA_RAZON_SOCIAL, STR_LUGARES_VENTA]), Format(MSG_CAPTION_VALIDAR,[STR_LUGARES_VENTA]), MB_ICONSTOP, txtRazonSocial);
    	Exit;
    end;

    if (dblDomicilios.DataSource.DataSet.RecordCount = 0) then begin
    	pcDatosPersonal.ActivePageIndex := 1;
		MsgBoxBalloon(Format(MSG_VALIDAR_CANTIDAD_DATOS,[FLD_DOMICILIO]), Format(MSG_CAPTION_VALIDAR,[STR_LUGARES_VENTA]), MB_ICONSTOP, dblDomicilios);
    	Exit;
    end;

	if (dblMediosComunicacion.DataSource.DataSet.RecordCount = 0) then begin
    	pcDatosPersonal.ActivePageIndex := 2;
		MsgBoxBalloon(Format(MSG_VALIDAR_CANTIDAD_DATOS,[FLD_MEDIO_CONTACTO]), Format(MSG_CAPTION_VALIDAR,[STR_LUGARES_VENTA]), MB_ICONSTOP, dblMediosComunicacion);
    	Exit;
    end;

    TodoOK          := False;
    ListaDomicilios := TStringList.Create;

	//HASTA ACA TODO BIEN
	Screen.Cursor   := crHourGlass;
    DMConnections.BaseCAC.BeginTrans;
    try

        try
            //Ahora grabo los datos propios del almacen

            ActualizarDatosLugarDeVenta.Close;
            With ActualizarDatosLugarDeVenta.Parameters do begin

                ParamByName('@NombreDeFantasia').Value		:= iif(Trim(txtNombreDeFantasia.Text)= '', null, Trim(txtNombreDeFantasia.Text));
                ParamByName('@RazonSocial').Value			:= iif(Trim(txtRazonSocial.Text)= '', null, Trim(txtRazonSocial.Text));

                ParamByName('@Contacto').Value	            := iif(Trim(txtContacto.Text)= '', null, Trim(txtContacto.Text));
                ParamByName('@CodigoDocumento').Value	    := Trim(StrRight(mcTipoNumeroDocumento.ComboText, 10));
                ParamByName('@NumeroDocumento').Value	    := iif(Trim(mcTipoNumeroDocumento.MaskText)= '', null, Trim(mcTipoNumeroDocumento.MaskText));

                ParamByName('@CodigoSituacionIVA').Value    := iif( cbSituacionIVA.ItemIndex < 0, null,  Trim(StrRight( cbSituacionIVA.Text, 2)));
                ParamByName('@CodigoLugarDeVenta').Value 	    := CodigoAlmacenActual;
                ParamByName('@Activo').Value 	            := chkActivo.Checked;
                ParamByName('@CodigoLugarDeVenta').Value             := CodigoAlmacenActual;

                ParamByName('@CodigoOperadorLogistico').Value   := Ival( StrRight( Trim(bteOperadoresLogisticos.Text), 10));
                ParamByName('@Activo').Value                    := chkActivo.Checked;
            end;
            ActualizarDatosLugarDeVenta.ExecProc;
            CodigoAlmacenActual :=  ActualizarDatosLugarDeVenta.Parameters.ParamByName('@CodigoLugarDeVenta').Value;
            ActualizarDatosLugarDeVenta.Close;
        except
            On E: Exception do begin
                ActualizarDatosLugarDeVenta.Close;
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_LUGARES_VENTA]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_LUGARES_VENTA]), MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;

        //Ahora grabo los domicilios
        //  Borrar todos los domicilios que figuran en la base que ya no pertenecen al cliente.
        if alLugaresDeVenta.Estado = Modi then begin

            cdsDomicilios.First;
            CodigosNoEliminar := '';
            while not cdsDomicilios.Eof do begin
                if (dblDomicilios.DataSource.DataSet.FieldByName('CodigoDomicilio').AsInteger > 0) then begin
                    CodigosNoEliminar := CodigosNoEliminar + ',' + Trim(IntToStr(dblDomicilios.DataSource.DataSet.FieldByName('CodigoDomicilio').AsInteger));
                end;
                cdsDomicilios.Next;
            end;
            CodigosNoEliminar := Trim(Copy(CodigosNoEliminar, 2, length(CodigosNoEliminar)));
            CodigosNoEliminar := '(' + CodigosNoEliminar + ')';
            if CodigosNoEliminar <> '()' then begin
                QueryTemp.Close;
                QueryTemp.SQL.Text :=
                    'SELECT CodigoDomicilio FROM DomiciliosLugaresDeVenta ' +
                    ' WHERE CodigoLugarDeVenta = ' + IntToStr(CodigoAlmacenActual) +
                    ' AND CodigoDomicilio not in ' + CodigosNoEliminar;
                QueryTemp.open;

                if QueryTemp.RecordCount > 0 then begin
                    CodigosEliminar := '';
                    While not QueryTemp.Eof do begin
                        CodigosEliminar := CodigosEliminar + ',' + Trim(IntToStr(QueryTemp.FieldByName('CodigoDomicilio').AsInteger));
                        QueryTemp.Next;
                    end;
                    CodigosEliminar := Trim(Copy(CodigosEliminar, 2, length(CodigosEliminar)));

                    try
                        with EliminarDomiciliosLugarDeVenta.Parameters do begin
                            ParamByName('@ListaDomicilios').Value := CodigosEliminar;
                        end;
                        EliminarDomiciliosLugarDeVenta.ExecProc;
                        EliminarDomiciliosLugarDeVenta.Close;
                    except
                        On E: Exception do begin
                            EliminarDomiciliosLugarDeVenta.Close;
                            MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_LUGARES_VENTA]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_LUGARES_VENTA]), MB_ICONSTOP);
                            Screen.Cursor := crDefault;
                            Exit;
                        end;
                    end;
                end;
                QueryTemp.Close;
            end else begin
                try
                    EliminarTodosDomiciliosLugarDeVenta.Parameters.ParamByName('@CodigoLugarDeVenta').Value := CodigoAlmacenActual;
                    EliminarTodosDomiciliosLugarDeVenta.ExecProc;
                    EliminarTodosDomiciliosLugarDeVenta.Close;
                except
                    On E: Exception do begin
                        EliminarTodosDomiciliosLugarDeVenta.Close;
                        MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_LUGARES_VENTA]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_LUGARES_VENTA]), MB_ICONSTOP);
                        Screen.Cursor := crDefault;
                        Exit;
                    end;
                end;
            end;
        end;

        //Ahora grabo los domicilios que quedaron
        CodigoDomicilioEntrega := -1;
        cdsDomicilios.First;
        While not cdsDomicilios.Eof do begin
            try
                with ActualizarDomicilioLugarDeVenta.Parameters do begin
                	ParamByName('@CodigoLugarDeVenta').Value         := CodigoAlmacenActual;
                	ParamByName('@CodigoTipoOrigenDato').Value  := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
                	ParamByName('@CodigoTipoEdificacion').Value := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
					ParamByName('@CodigoCalle').Value           := iif( cdsDomicilios.FieldByName('CodigoCalle').Asinteger < 1, null, cdsDomicilios.FieldByName('CodigoCalle').Asinteger);
					ParamByName('@CalleDesnormalizada').Value   := iif( Trim(cdsDomicilios.FieldByName('CalleDesnormalizada').AsString) = '', null, Trim(cdsDomicilios.FieldByName('CalleDesnormalizada').AsString));
					ParamByName('@Numero').Value                := cdsDomicilios.FieldByName('Numero').Asinteger;
					ParamByName('@Piso').Value                  := iif( cdsDomicilios.FieldByName('piso').Asinteger < 1, null, cdsDomicilios.FieldByName('piso').Asinteger);;
					ParamByName('@Depto').Value                 := iif( Trim(cdsDomicilios.FieldByName('Dpto').AsString) = '', null, Trim(cdsDomicilios.FieldByName('Dpto').AsString));
					ParamByName('@CodigoPostal').Value          := iif( Trim(cdsDomicilios.FieldByName('CodigoPostal').AsString) = '', null, Trim(cdsDomicilios.FieldByName('CodigoPostal').AsString));
					ParamByName('@Detalle').Value               := iif( Trim(cdsDomicilios.FieldByName('Detalle').AsString) = '', null, Trim(cdsDomicilios.FieldByName('Detalle').AsString));
                	ParamByName('@CodigoDomicilio').Value       := iif(cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger < 1, null, cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger);
                end;
                ActualizarDomicilioLugarDeVenta.ExecProc;

                ListaDomicilios.Add(IntToStr(cdsDomicilios.RecNo) + Space(200) + IntToStr(ActualizarDomicilioLugarDeVenta.Parameters.ParamByName('@CodigoDomicilio').Value));

                if RecNoDomicilioEntrega = cdsDomicilios.RecNo then CodigoDomicilioEntrega :=  ActualizarDomicilioLugarDeVenta.Parameters.ParamByName('@CodigoDomicilio').Value;
                ActualizarDomicilioLugarDeVenta.Close;
            except
                On E: Exception do begin
                    ActualizarDomicilioLugarDeVenta.Close;
                    MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_DOMICILIO]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_DOMICILIO]), MB_ICONSTOP);
                    Screen.Cursor := crDefault;
                    Exit;
                end;
            end;
            cdsDomicilios.Next;
        end;
        //Fin Domicilios.

        //Actualizo en los datos de la persona el Codigo de Domicilio de Entrega.
        try
            with ActualizarDomicilioEntregaLugarDeVenta.Parameters do begin
                ParamByName('@CodigoLugarDeVenta').Value             := CodigoAlmacenActual;
                ParamByName('@CodigoDomicilioEntrega').Value    := iif(CodigoDomicilioEntrega < 1, null, CodigoDomicilioEntrega);
            end;
            ActualizarDomicilioEntregaLugarDeVenta.ExecProc;
            ActualizarDomicilioEntregaLugarDeVenta.Close;
        except
            On E: Exception do begin
                ActualizarDomicilioEntregaLugarDeVenta.Close;
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_DOMICILIO]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_DOMICILIO]), MB_ICONSTOP);
                Screen.Cursor := crDefault;
                Exit;
            end;
        end;


        //Ahora grabo los Medios de Contacto
        QueryExecute( DMConnections.BaseCAC, 'DELETE FROM MediosComunicacionLugaresDeVenta WHERE CodigoLugarDeVenta = ' + IntToStr(CodigoAlmacenActual));

        cdsMediosComunicacion.First;
        While not cdsMediosComunicacion.Eof do begin
            try
                FCodigoDomicilio := -1;
                if (Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) = '') then begin
                    // estoy grabando un Medio de Contacto con Domicilio.
                    FCodigoDomicilio := cdsMediosComunicacion.fieldByName('CodigoDomicilio').AsInteger;
                    if (FCodigoDomicilio < 0) then begin
                        for i:= 0 to ListaDomicilios.Count -1 do begin
                            if Ival(StrLeft(Trim(ListaDomicilios.Strings[i]), 20)) = cdsMediosComunicacion.FieldByName('CodigoDomicilioRecNo').AsInteger then begin
                                FCodigoDomicilio := Ival(StrRight(Trim(ListaDomicilios.Strings[i]), 20));
                                break;
                            end;
                        end;
                    end;
                end;

                if (FCodigoDomicilio > 0) or (Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) <> '') then begin
                    with ActualizarMedioComunicacionLugarDeVenta.Parameters do begin
                        ParamByName('@CodigoLugarDeVenta').Value         := CodigoAlmacenActual;
                        ParamByName('@CodigoMedioContacto').Value   := cdsMediosComunicacion.fieldByName('CodigoMedioContacto').AsInteger;
                        ParamByName('@CodigoTipoOrigenDato').Value  := cdsMediosComunicacion.fieldByName('CodigoTipoOrigenDato').AsInteger;
                        ParamByName('@Valor').Value                 := iif( Trim(cdsMediosComunicacion.fieldByName('Valor').AsString) = '', null, Trim(cdsMediosComunicacion.fieldByName('Valor').AsString));
                        ParamByName('@CodigoDomicilio').Value       := iif( FCodigoDomicilio < 1, null, FCodigoDomicilio);
                        ParamByName('@HorarioDesde').Value          := iif( cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime <= nulldate, null, cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime);
                        ParamByName('@HorarioHasta').Value          := iif( cdsMediosComunicacion.fieldByName('HorarioDesde').AsDateTime <= nulldate, null, cdsMediosComunicacion.fieldByName('HorarioHasta').AsDateTime);
                        ParamByName('@Observaciones').Value         := iif( Trim(cdsMediosComunicacion.fieldByName('Observaciones').AsString) = '', null, Trim(cdsMediosComunicacion.fieldByName('Observaciones').AsString));
//                        ParamByName('@Activo').Value                := cdsMediosComunicacion.fieldByName('Activo').AsBoolean;
                    end;
                    ActualizarMedioComunicacionLugarDeVenta.ExecProc;
                    ActualizarMedioComunicacionLugarDeVenta.Close;
                end;
            except
                on E: exception do begin
                    MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_MEDIO_CONTACTO]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
                    ActualizarMedioComunicacionLugarDeVenta.Close;
                    Screen.Cursor := crDefault;
                    Exit;
                end;
            end;
            cdsMediosComunicacion.Next;
        end;

        CodigoAlmacenActual := -1;

        TodoOK := True;
    finally
        if TodoOK then DMConnections.BaseCAC.CommitTrans
        else DMConnections.BaseCAC.RollbackTrans;
        FreeAndNil(ListaDomicilios);
    end;
	Volver_Campos;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.alMaestroAlmacenesClick
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Completa los campos en pantalla utilizando la información del registro
    actual de la tabla "MaestroAlmacenes"
********************************************************************************}
procedure TFormLugaresVenta.alLugaresDeVentaClick(Sender: TObject);
//var
//	i: integer;
begin

    if tblLugaresDeVenta.FieldByName('CodigoLugarDeVenta').AsInteger = CodigoAlmacenActual then Exit;

    tblLugaresDeVenta.DisableControls;

	with tblLugaresDeVenta do begin

        //Cargar Datos Personales

		txtNombreDeFantasia.Text		:= Trim(FieldByName('NombreDeFantasia').AsString);
		txtRazonSocial.Text  		    := Trim(FieldByName('RazonSocial').AsString);
        txtContacto.Text := Trim(FieldByNAme('Contacto').AsString);

       	CargarTipoSItuacionIVA(DMConnections.BaseCAC, cbSituacionIVA, Trim(FieldByName('CodigoSituacionIVA').AsString));

       	CargarTiposDocumento(DMConnections.BaseCAC, mcTipoNumeroDocumento, Trim(FieldByName('CodigoDocumento').AsString));

		mcTipoNumeroDocumento.MaskText	:= Trim(FieldByName('NumeroDocumento').AsString);

        chkActivo.Checked		:= FieldByName('Activo').AsBoolean;

        // Cargamos los domicilios
        CargarDomiciliosPersona(FieldByName('CodigoLugarDeVenta').AsInteger, 0);

        // Cargamos los Medios de Comunicacion
        CargarMediosComunicacionPersona( FieldByName('CodigoLugarDeVenta').AsInteger, 0);

        //Cargar Datos del Lugar De Venta
        bteOperadoresLogisticos.Text := Trim(FieldByNAme('RazonSocialOperador').AsString) + space(200) + IntToStr(FieldByName('CodigoOperadorLogistico').AsInteger);

	end;
    tblLugaresDeVenta.EnableControls;
    CodigoAlmacenActual := tblLugaresDeVenta.FieldByName('CodigoLugarDeVenta').AsInteger;
end;

{******************************** Function Header *******************************
  Function Name: TFormMaestroPersonal.alMaestroPersonalDelete
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Elimina un registro de la tabla "MaestroAlmacenes".
    Primero intenta eliminarlo, en caso de no ser posible debido a la integridad
    referencial, modifica el estado del registro marcándolo como inactivo
    mediante la ejecución de un query.
********************************************************************************}
procedure TFormLugaresVenta.alLugaresDeVentaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
    btnPuntosVenta.Enabled := False;

    If MsgBox(Format(MSG_QUESTION_BAJA,[STR_LUGARES_VENTA]), Format(MSG_CAPTION_BAJA,[STR_LUGARES_VENTA]), MB_YESNO) = IDYES then begin
        try
            DMConnections.BaseCAC.BeginTrans;
            EliminarLugarDeVenta.Parameters.ParamByName('@CodigoLugarDeVenta').Value := tblLugaresDeVenta.FieldByName('CodigoLugarDeVenta').AsInteger;
            EliminarLugarDeVenta.ExecProc;
            EliminarLugarDeVenta.Close;
            DMConnections.BaseCAC.CommitTrans;
        Except
            On E: exception do begin
                MsgBoxErr(Format(MSG_ERROR_BAJA,[STR_LUGARES_VENTA]), E.message, Format(MSG_CAPTION_BAJA,[STR_LUGARES_VENTA]), MB_ICONSTOP);
                EliminarLugarDeVenta.Close;
                DMConnections.BaseCAC.RollbackTrans;
            end;
        end;
    end;
	Volver_Campos;

    btnPuntosVenta.Enabled := true;
	Screen.Cursor      := crDefault;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.alMaestroAlmacenesDrawItem
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TDBList; Tabla: TDataSet; Rect: TRect;
  				 State: TOwnerDrawState; Cols: TColPositions
  Return Value:  None
  Description:   Dibuja el la grilla los valores del registro actual de la tabla
  				 "MaestroAlmacenes"
********************************************************************************}
procedure TFormLugaresVenta.alLugaresDeVentaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas  do begin
		FillRect(Rect);
        if Tabla.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;

        with Tabla do begin
//            TextOut(Cols[0], Rect.Top, IStr(FieldByName('CodigoLugarDeVenta').AsInteger, 10));
            TextOut(Cols[0], Rect.Top, Trim(FieldByName('NombreDeFantasia').AsString));
            TextOut(Cols[1], Rect.Top, Trim(FieldByName('RazonSocial').AsString));
            TextOut(Cols[2], Rect.Top, Trim(FieldByName('RazonSocialOperador').AsString));
		end;
	end;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.alMaestroAlmacenesEdit
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la Modificación de un registro.
********************************************************************************}
procedure TFormLugaresVenta.alLugaresDeVentaEdit(Sender: TObject);
begin
    HabilitarCamposEdicion(True);
    alLugaresDeVenta.Estado   	:= modi;
    pcDatosPErsonal.ActivePage 	:= tsCliente;
    btnPuntosVenta.Enabled      := False;
	txtNombreDeFantasia.SetFocus;
end;


{******************************** Function Header *******************************
  Function Name: TFormMaestroAlmacenes.alMaestroAlmacenesInsert
  Author:        Marrone, Pablo <pmarrone@dpsautomation.com>
  Date Created:  04-Sep-2003
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Prepara los controles para la inserción de un registro.
********************************************************************************}
procedure TFormLugaresVenta.alLugaresDeVentaInsert(Sender: TObject);
begin
	Limpiar_Campos;
    CodigoAlmacenActual := -1;
    HabilitarCamposEdicion(True);
    alLugaresDeVenta.Estado    := alta;
    chkActivo.Checked           := True;
    pcDatosPersonal.ActivePage  := tsCliente;
    btnPuntosVenta.Enabled      := False;
	txtNombreDeFantasia.SetFocus;
end;

procedure TFormLugaresVenta.alLugaresDeVentaRefresh(Sender: TObject);
begin
	if alLugaresDeVenta.Empty then Limpiar_Campos;
end;

procedure TFormLugaresVenta.HabilitarCamposEdicion(habilitar: Boolean);
begin
	alLugaresDeVenta.Enabled	    := not Habilitar;
    pcDatosPersonal.Enabled	        := True;
    pnlDatosPersonal.Enabled        := Habilitar;
    pnlDomicilio.Enabled            := Habilitar;
	pnlMediosContacto.Enabled       := Habilitar;
	Notebook.PageIndex 		        := ord(habilitar);
end;


procedure TFormLugaresVenta.IrAlInicio(Sender: TObject);
begin
	bteOperadoresLogisticos.SelStart := 0;
end;

procedure TFormLugaresVenta.SpeedButton1Click(Sender: TObject);
begin
	bteOperadoresLogisticos.Text := '';
end;

procedure TFormLugaresVenta.FormShow(Sender: TObject);
begin
    CodigoAlmacenActual := -1;
end;

procedure TFormLugaresVenta.Agregar(Sender: TObject);
var
	f: TFormEditarDomicilio;
begin
(*
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar() and (f.ShowModal = mrOK) then begin
        try
            with cdsDomicilios do begin
                Append;
                FieldByName('CodigoDomicilio').AsInteger        := -1;
                FieldByName('CodigoLugarDeVenta').AsInteger     := CodigoAlmacenActual;
                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoDomicilio;
                FieldByName('DescriTipoOrigenDato').AsString    := f.DescriTipoDomicilio;
                FieldByName('CodigoCalle').AsInteger            := f.CodigoCalle;
                FieldByName('DescriCalle').AsString             := f.DescripcionCalle;
                FieldByName('Numero').AsInteger                 := f.Numero;
                FieldByName('Piso').AsInteger                   := f.Piso;
                FieldByName('Dpto').AsString                    := f.Depto;
                FieldByName('CalleDesnormalizada').AsString     := f.DescripcionCalle;
                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('CodigoPostal').AsString            := f.CodigoPostal;
                FieldByName('CodigoPais').AsString              := f.Pais;
                FieldByName('CodigoRegion').AsString            := f.Region;
                FieldByName('CodigoComuna').AsString            := f.Comuna;
                FieldByName('DescriPais').AsString              := f.DescriPais;
                FieldByName('DescriRegion').AsString            := f.DescriRegion;
                FieldByName('DescriComuna').AsString            := f.DescriComuna;
                FieldByName('DireccionCompleta').AsString       := f.DireccionCompleta;
                FieldByName('CodigoTipoEdificacion').AsInteger  := f.TipoEdificacion;
                FieldByName('DescriTipoEdificacion').AsString   := f.DescriTipoEdificacion;
                FieldByName('DomicilioEntrega').AsBoolean       := f.DomicilioEntrega;
                FieldByName('Normalizado').AsInteger            := Ord(f.CodigoCalle > 0);
                FieldByName('CalleRelacionadaUno').AsInteger        := f.CalleRelacionadaUno;
                FieldByName('NumeroCalleRelacionadaUno').AsInteger  := f.NumeroCalleRelacionadaUno;
                FieldByName('CalleRelacionadaDos').AsInteger        := f.CalleRelacionadaDos;
                FieldByName('NumeroCalleRelacionadaDos').AsInteger  := f.NumeroCalleRelacionadaDos;
                FieldByName('DomicilioCompleto').AsString           := f.DomicilioCompleto;
                Post;
                if f.DomicilioEntrega then RecNoDomicilioEntrega   := dsDomicilios.DataSet.RecNo;
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR, E.message, Format(MSG_CAPTION_AGREGAR,[FLD_DOMICILIO]), MB_ICONSTOP);
                dsDomicilios.DataSet.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesDomicilios;
    *)
end;

procedure TFormLugaresVenta.btnEliminarDomicilioClick(Sender: TObject);
begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(Format(MSG_QUESTION_BAJA,[FLD_DOMICILIO]), Format(MSG_CAPTION_ELIMINAR,[FLD_DOMICILIO]), MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			dsDomicilios.DataSet.Delete;
		Except
			On E: Exception do begin
				dsDomicilios.DataSet.Cancel;
				MsgBoxErr(Format(MSG_ERROR_EDITAR,[FLD_DOMICILIO]), e.message, Format(MSG_CAPTION_ELIMINAR,[FLD_DOMICILIO]), MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
    HabilitarBotonesDomicilios;    
end;

procedure TFormLugaresVenta.btnEditarDomicilioClick(Sender: TObject);
var
	f: TFormEditarDomicilio;
    i: integer;
begin
(*
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar(
        dsDomicilios.DataSet.FieldByName('CodigoDomicilio').AsInteger,
        dsDomicilios.DataSet.FieldByName('CodigoTipoOrigenDato').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('CodigoPais').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoRegion').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoComuna').AsString),
        dsDomicilios.DataSet.FieldByName('CodigoCalle').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('CalleDesnormalizada').AsString),
        dsDomicilios.DataSet.FieldByName('Numero').AsString,
        dsDomicilios.DataSet.FieldByName('Piso').AsInteger,
        Trim(dsDomicilios.DataSet.FieldByName('Dpto').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('Detalle').AsString),
        Trim(dsDomicilios.DataSet.FieldByName('CodigoPostal').AsString),
        dsDomicilios.DataSet.FieldByName('CodigoTipoEdificacion').AsInteger,
        iif(dsDomicilios.DataSet.RecNo = RecNoDomicilioEntrega, true, false),
        dsDomicilios.DataSet.FieldByName('CalleRelacionadaUno').AsInteger,
        dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaUno').AsInteger,
        dsDomicilios.DataSet.FieldByName('CalleRelacionadaDos').AsInteger,
        dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaDos').AsInteger
        ) and (f.ShowModal = mrOK) then begin
        try
            with cdsDomicilios do begin
                for i := 0 to cdsDomicilios.FieldCount -1 do cdsDomicilios.FieldDefs[i].Attributes  := [];
                Edit;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('CodigoLugarDeVenta').AsInteger          := CodigoAlmacenActual;
                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoDomicilio;
                FieldByName('DescriTipoOrigenDato').AsString    := f.DescriTipoDomicilio;
                FieldByName('CodigoCalle').AsInteger            := f.CodigoCalle;
                FieldByName('DescriCalle').AsString             := f.DescripcionCalle;
                FieldByName('Numero').AsInteger                 := f.Numero;
                FieldByName('Piso').AsInteger                   := f.Piso;
                FieldByName('Dpto').AsString                    := f.Depto;
                FieldByName('CalleDesnormalizada').AsString     := f.DescripcionCalle;
                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('CodigoPostal').AsString            := f.CodigoPostal;
                FieldByName('CodigoPais').AsString              := f.Pais;
                FieldByName('CodigoRegion').AsString            := f.Region;
                FieldByName('CodigoComuna').AsString            := f.Comuna;
                FieldByName('DescriPais').AsString              := f.DescriPais;
                FieldByName('DescriRegion').AsString            := f.DescriRegion;
                FieldByName('DescriComuna').AsString            := f.DescriComuna;
                FieldByName('DireccionCompleta').AsString       := f.DireccionCompleta;
                FieldByName('CodigoTipoEdificacion').AsInteger  := f.TipoEdificacion;
                FieldByName('DescriTipoEdificacion').AsString   := f.DescriTipoEdificacion;
                FieldByName('DomicilioEntrega').AsBoolean       := f.DomicilioEntrega;
                FieldByName('Normalizado').AsInteger            := Ord(f.CodigoCalle > 0);
                FieldByName('CalleRelacionadaUno').AsInteger        := f.CalleRelacionadaUno;
                FieldByName('NumeroCalleRelacionadaUno').AsInteger  := f.NumeroCalleRelacionadaUno;
                FieldByName('CalleRelacionadaDos').AsInteger        := f.CalleRelacionadaDos;
                FieldByName('NumeroCalleRelacionadaDos').AsInteger  := f.NumeroCalleRelacionadaDos;
                FieldByName('DomicilioCompleto').AsString           := f.DomicilioCompleto;                
                Post;
                if f.DomicilioEntrega then RecNoDomicilioEntrega   := cdsDomicilios.RecNo;
            end;
        except
            On E: Exception do begin
                MsgBoxErr(Format(MSG_CAPTION_EDITAR,[FLD_DOMICILIO]), E.message, Format(MSG_CAPTION_EDITAR,[FLD_DOMICILIO]), MB_ICONSTOP);
                cdsDomicilios.Cancel;
            end;
        end;
    end;
    f.Release;
    HabilitarBotonesDomicilios;
    *)
end;


procedure TFormLugaresVenta.dblDomiciliosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
{	With Sender.Canvas do begin
		FillRect(Rect);
        if dblDomicilios.DataSource.DataSet.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;
	end;
}
    if (Column.Index = 4) and (RecNoDomicilioEntrega = dsDomicilios.DataSet.RecNo) then Text := 'Si';

end;

procedure TFormLugaresVenta.dblMediosComunicacionDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
{
	With Sender.Canvas do begin
		FillRect(Rect);
        if dblMediosComunicacion.DataSource.DataSet.FieldByName('Activo').AsBoolean then begin
        	if odSelected in State then
				Sender.Canvas.Font.Color := clWindow
			else
	        	Sender.Canvas.Font.Color := clWindowText;
		end else begin
			if odSelected in State then
				Sender.Canvas.Font.Color := clYellow
			else
	        	Sender.Canvas.Font.Color := clRed;
		end;
	end;
}
    if Column.Index = 3 then Text := Formatdatetime('hh:nn', dblMediosComunicacion.DataSource.DataSet.FieldByName('HorarioDesde').AsDateTime);
    if Column.Index = 4 then Text := Formatdatetime('hh:nn', dblMediosComunicacion.DataSource.DataSet.FieldByName('HorarioHasta').AsDateTime);
end;

procedure TFormLugaresVenta.btnEliminarMedioComunicacionClick(
  Sender: TObject);
begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(Format(MSG_QUESTION_BAJA,[FLD_MEDIO_CONTACTO]), Format(MSG_CAPTION_ELIMINAR,[FLD_MEDIO_CONTACTO]), MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			dsMediosComunicacion.DataSet.Delete;
		Except
			On E: Exception do begin
				dsMediosComunicacion.DataSet.Cancel;
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[FLD_MEDIO_CONTACTO]), e.message, Format(MSG_CAPTION_ELIMINAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
    HabilitarBotonesMediosComunicacion;
end;

procedure TFormLugaresVenta.btnAgregarMedioComunicacionClick(
  Sender: TObject);
var
	f: TFormEditarMedioComunicacion;
begin
    Application.CreateForm(TFormEditarMedioComunicacion, f);
    cdsDomicilios.First;
    {
    While not cdsDomicilios.Eof do begin
        with f.Tabla do begin
            Append;
            FieldByName('CodigoDomicilio').AsInteger        := cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger;
            FieldByName('CodigoPersona').AsInteger          := cdsDomicilios.FieldByName('CodigoLugarDeVenta').AsInteger;
            FieldByName('CodigoTipoOrigenDato').AsInteger   := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
            FieldByName('DescriTipoOrigenDato').AsString    := cdsDomicilios.FieldByName('DescriTipoOrigenDato').AsString;
            FieldByName('CodigoTipoCalle').AsInteger        := cdsDomicilios.FieldByName('CodigoTipoCalle').AsInteger;
            FieldByName('CodigoCalle').AsInteger            := cdsDomicilios.FieldByName('CodigoCalle').AsInteger;
            FieldByName('DescriCalle').AsString             := cdsDomicilios.FieldByName('DescriCalle').AsString;
            FieldByName('Numero').AsInteger                 := cdsDomicilios.FieldByName('Numero').AsInteger;
            FieldByName('Piso').AsInteger                   := cdsDomicilios.FieldByName('Piso').AsInteger;
            FieldByName('Dpto').AsString                    := cdsDomicilios.FieldByName('Dpto').AsString;
            FieldByName('CalleDesnormalizada').AsString     := cdsDomicilios.FieldByName('CalleDesnormalizada').AsString;
            FieldByName('Detalle').AsString                 := cdsDomicilios.FieldByName('Detalle').AsString;
            FieldByName('CodigoPostal').AsString            := cdsDomicilios.FieldByName('CodigoPostal').AsString;
            FieldByName('CodigoPais').AsString              := cdsDomicilios.FieldByName('CodigoPais').AsString;
            FieldByName('CodigoRegion').AsString            := cdsDomicilios.FieldByName('CodigoRegion').AsString;
            FieldByName('CodigoComuna').AsString            := cdsDomicilios.FieldByName('CodigoComuna').AsString;
            FieldByName('CodigoCiudad').AsString            := cdsDomicilios.FieldByName('CodigoCiudad').AsString;
            FieldByName('DescriPais').AsString              := cdsDomicilios.FieldByName('DescriPais').AsString;
            FieldByName('DescriRegion').AsString            := cdsDomicilios.FieldByName('DescriRegion').AsString;
            FieldByName('DescriComuna').AsString            := cdsDomicilios.FieldByName('DescriComuna').AsString;
            FieldByName('DescriCiudad').AsString            := cdsDomicilios.FieldByName('DescriCiudad').AsString;
            FieldByName('DireccionCompleta').AsString       := cdsDomicilios.FieldByName('DireccionCompleta').AsString;
            FieldByName('CodigoTipoEdificacion').AsInteger  := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
            FieldByName('DescriTipoEdificacion').AsString   := cdsDomicilios.FieldByName('DescriTipoEdificacion').AsString;
            FieldByName('DomicilioEntrega').AsBoolean       := cdsDomicilios.FieldByName('DomicilioEntrega').AsBoolean;
            FieldByName('Normalizado').AsInteger            := cdsDomicilios.FieldByName('Normalizado').AsInteger;
            FieldByName('CalleRelacionadaUno').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaUno').AsInteger;
            FieldByName('NumeroCalleRelacionadaUno').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaUno').AsInteger;
            FieldByName('CalleRelacionadaDos').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaDos').AsInteger;
            FieldByName('NumeroCalleRelacionadaDos').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaDos').AsInteger;
            Post;
        end;
        cdsDomicilios.Next
    end;

    if f.Inicializar(1, 1, 1, '', 0, 0, '', '', 0) and (f.ShowModal = mrOK) then begin
        try
            with dsMediosComunicacion.DataSet do begin
                Append;
//                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoOrigenDato;
//                FieldByName('CodigoMedioContacto').AsInteger    := f.MedioContacto;
//                FieldByName('DescriMedioContacto').AsString     := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MediosContacto WHERE CodigoMedioContacto = ' + IntToStr(f.MedioContacto)));
//                FieldByName('TipoMedio').AsString               := f.TipoMedio;
//                FieldByName('DescriTipoOrigenDato').AsString    := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TiposOrigenDatos WHERE CodigoTipoOrigenDato = ' + IntToStr(f.TipoOrigenDato)));
                FieldByName('Valor').AsString                   := f.Valor;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('DescriDomicilio').AsString         := f.DescriDomicilio;
//                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('HorarioDesde').AsDateTime          := f.HoraDesde;
                FieldByName('HorarioHasta').AsDateTime          := f.HoraHasta;
                FieldByName('Observaciones').AsString           := f.Observaciones;
                FieldByName('CodigoDomicilioRecNo').AsInteger   := f.CodigoDomicilioRecNo;
                Post;
            end;
        except
            On E: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_AGREGAR,[FLD_MEDIO_CONTACTO]), E.message, Format(MSG_CAPTION_AGREGAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
                dsMediosComunicacion.DataSet.Cancel;
            end;
        end;
    end;
    }
    f.Release;
    HabilitarBotonesMediosComunicacion;
end;

procedure TFormLugaresVenta.btnEditarMedioComunicacionClick(
  Sender: TObject);
var
	f: TFormEditarMedioComunicacion;
begin
    Application.CreateForm(TFormEditarMedioComunicacion, f);
    cdsDomicilios.First;
    {
    While not cdsDomicilios.Eof do begin
        f.Tabla.Open;
        with f.Tabla do begin
            Append;
            FieldByName('CodigoDomicilio').AsInteger        := cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger;
            FieldByName('CodigoPersona').AsInteger          := cdsDomicilios.FieldByName('CodigoLugarDeVenta').AsInteger;
            FieldByName('CodigoTipoOrigenDato').AsInteger   := cdsDomicilios.FieldByName('CodigoTipoOrigenDato').AsInteger;
            FieldByName('DescriTipoOrigenDato').AsString    := cdsDomicilios.FieldByName('DescriTipoOrigenDato').AsString;
            FieldByName('CodigoTipoCalle').AsInteger        := cdsDomicilios.FieldByName('CodigoTipoCalle').AsInteger;
            FieldByName('CodigoCalle').AsInteger            := cdsDomicilios.FieldByName('CodigoCalle').AsInteger;
            FieldByName('DescriCalle').AsString             := cdsDomicilios.FieldByName('DescriCalle').AsString;
            FieldByName('Numero').AsInteger                 := cdsDomicilios.FieldByName('Numero').AsInteger;
            FieldByName('Piso').AsInteger                   := cdsDomicilios.FieldByName('Piso').AsInteger;
            FieldByName('Dpto').AsString                    := cdsDomicilios.FieldByName('Dpto').AsString;
            FieldByName('CalleDesnormalizada').AsString     := cdsDomicilios.FieldByName('CalleDesnormalizada').AsString;
            FieldByName('Detalle').AsString                 := cdsDomicilios.FieldByName('Detalle').AsString;
            FieldByName('CodigoPostal').AsString            := cdsDomicilios.FieldByName('CodigoPostal').AsString;
            FieldByName('CodigoPais').AsString              := cdsDomicilios.FieldByName('CodigoPais').AsString;
            FieldByName('CodigoRegion').AsString            := cdsDomicilios.FieldByName('CodigoRegion').AsString;
            FieldByName('CodigoComuna').AsString            := cdsDomicilios.FieldByName('CodigoComuna').AsString;
            FieldByName('CodigoCiudad').AsString            := cdsDomicilios.FieldByName('CodigoCiudad').AsString;
            FieldByName('DescriPais').AsString              := cdsDomicilios.FieldByName('DescriPais').AsString;
            FieldByName('DescriRegion').AsString            := cdsDomicilios.FieldByName('DescriRegion').AsString;
            FieldByName('DescriComuna').AsString            := cdsDomicilios.FieldByName('DescriComuna').AsString;
            FieldByName('DescriCiudad').AsString            := cdsDomicilios.FieldByName('DescriCiudad').AsString;
            FieldByName('DireccionCompleta').AsString       := cdsDomicilios.FieldByName('DireccionCompleta').AsString;
            FieldByName('CodigoTipoEdificacion').AsInteger  := cdsDomicilios.FieldByName('CodigoTipoEdificacion').AsInteger;
            FieldByName('DescriTipoEdificacion').AsString   := cdsDomicilios.FieldByName('DescriTipoEdificacion').AsString;
            FieldByName('DomicilioEntrega').AsBoolean       := cdsDomicilios.FieldByName('DomicilioEntrega').AsBoolean;
            FieldByName('Normalizado').AsInteger            := cdsDomicilios.FieldByName('Normalizado').AsInteger;
            FieldByName('CalleRelacionadaUno').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaUno').AsInteger;
            FieldByName('NumeroCalleRelacionadaUno').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaUno').AsInteger;
            FieldByName('CalleRelacionadaDos').AsInteger        := cdsDomicilios.FieldByName('CalleRelacionadaDos').AsInteger;
            FieldByName('NumeroCalleRelacionadaDos').AsInteger  := cdsDomicilios.FieldByName('NumeroCalleRelacionadaDos').AsInteger;
            Post;
        end;
        cdsDomicilios.Next
    end;

    if f.Inicializar(
        dsMediosComunicacion.DataSet.FieldByName('CodigoTipoOrigenDato').AsInteger,
        dsMediosComunicacion.DataSet.FieldByName('CodigoMedioContacto').AsInteger,
        1,
        dsMediosComunicacion.DataSet.FieldByName('Valor').AsString,
        dsMediosComunicacion.DataSet.FieldByName('HorarioDesde').AsDateTime,
        dsMediosComunicacion.DataSet.FieldByName('HorarioHasta').AsDateTime,
        dsMediosComunicacion.DataSet.FieldByName('Observaciones').AsString,
        '',
        dsMediosComunicacion.DataSet.FieldByName('CodigoDomicilio').AsInteger) and (f.ShowModal = mrOK) then begin
        try
            with dsMediosComunicacion.DataSet do begin
                Edit;
//                FieldByName('CodigoTipoOrigenDato').AsInteger   := f.TipoOrigenDato;
//                FieldByName('CodigoMedioContacto').AsInteger    := f.MedioContacto;
//                FieldByName('DescriMedioContacto').AsString     := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM MediosContacto WHERE CodigoMedioContacto = ' + IntToStr(f.MedioContacto)));
//                FieldByName('TipoMedio').AsString               := f.TipoMedio;
//                FieldByName('DescriTipoOrigenDato').AsString    := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TiposOrigenDatos WHERE CodigoTipoOrigenDato = ' + IntToStr(f.TipoOrigenDato)));
                FieldByName('Valor').AsString                   := f.Valor;
                FieldByName('CodigoDomicilio').AsInteger        := f.CodigoDomicilio;
                FieldByName('DescriDomicilio').AsString         := f.DescriDomicilio;
//                FieldByName('Detalle').AsString                 := f.Detalle;
                FieldByName('HorarioDesde').AsDateTime          := f.HoraDesde;
                FieldByName('HorarioHasta').AsDateTime          := f.HoraHasta;
                FieldByName('Observaciones').AsString           := f.Observaciones;
                FieldByName('CodigoDomicilioRecNo').AsInteger   := f.CodigoDomicilioRecNo;
                Post;
            end;
        except
            On E: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_MEDIO_CONTACTO]), E.message, Format(MSG_CAPTION_EDITAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
                dsMediosComunicacion.DataSet.Cancel;
            end;
        end;
    end;
    }
    f.Release;
    HabilitarBotonesMediosComunicacion;
end;

function TFormLugaresVenta.btOperadoresLogisticosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    Texto   :=
        Trim(Tabla.FieldByName('RazonSocial').AsString) + Space(200) +
        Trim(Tabla.FieldByName('CodigoOperadorLogistico').AsString);
    Result  := True;
end;

procedure TFormLugaresVenta.btOperadoresLogisticosSelect(Sender: TObject;
  Tabla: TDataSet);
begin
	bteOperadoresLogisticos.Text :=
        Trim(Tabla.FieldByName('RazonSocial').AsString) + Space(200) +
        Trim(Tabla.FieldByName('CodigoOperadorLogistico').AsString);
end;

procedure TFormLugaresVenta.mcTipoNumeroDocumentoChange(
  Sender: TObject);
begin
    if (mcTipoNumeroDocumento.MaskText <> '')  then txtRazonSocial.Color := $00FAEBDE
    else txtRazonSocial.Color := clWindow;
end;

procedure TFormLugaresVenta.btnPuntosVentaClick(Sender: TObject);
var
	f: TFormPuntosVenta;
begin
    Application.CreateForm(TFormPuntosVenta, f);
	if f.Inicializar(False, tblLugaresDeVenta.FieldByName('CodigoLugarDeVenta').AsInteger, tblLugaresDeVenta.FieldByName('CodigoOperadorLogistico').AsInteger) then  f.ShowModal;
    f.Release;
end;

end.
