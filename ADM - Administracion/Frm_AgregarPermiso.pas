{-----------------------------------------------------------------------------
 File Name: Frm_AgregarPermiso
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit Frm_AgregarPermiso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DbList, Db, DBTables, ComCtrls, Util, UtilDB, ADODB,
  DMCOnnection, DPSControls,RStrings, UtilProc;

type
  TFormAgregarPermiso = class(TForm)
	img_usuario: TImage;
	img_grupo: TImage;
	Bevel1: TBevel;
    rb_conpermiso: TRadioButton;
	RadioButton2: TRadioButton;
    Panel1: TPanel;
    lb_usuarios: TListBox;
    ObtenerUsuariosGruposSinFuncion: TADOStoredProc;
    Header1: THeader;
    Shape1: TShape;
    btn_Aceptar: TButton;
    Button2: TButton;
    procedure lb_usuariosDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure btn_AceptarClick(Sender: TObject);
    procedure lb_usuariosDblClick(Sender: TObject);
    procedure lb_usuariosKeyPress(Sender: TObject; var Key: Char);
  private
	{ Private declarations }
	FSistema, FFuncion: AnsiString;
  public
	{ Public declarations }
	function Inicializa(CodigoSistema, Funcion: AnsiString): Boolean;
  end;

var
  FormAgregarPermiso: TFormAgregarPermiso;

implementation

{$R *.DFM}

{ TFormAgregarPermiso }

function TFormAgregarPermiso.Inicializa(CodigoSistema,
  Funcion: AnsiString): Boolean;
begin
	FSistema := CodigoSistema;
	FFuncion := Funcion;
	ObtenerUsuariosGruposSinFuncion.Parameters.ParamByName('@CodigoSistema').Value := FSistema;
	ObtenerUsuariosGruposSinFuncion.Parameters.ParamByName('@Funcion').Value := FFuncion;
	Result := OpenTables([ObtenerUsuariosGruposSinFuncion]);
	if not Result then Exit;
	While not ObtenerUsuariosGruposSinFuncion.Eof do begin
		lb_usuarios.items.add(
		  IntToStr(Integer(ObtenerUsuariosGruposSinFuncion.FieldByName('EsGrupo').AsBoolean)) + CR +
		  ObtenerUsuariosGruposSinFuncion.FieldByName('CodigoGrupoUsuario').AsString + CR +
		  ObtenerUsuariosGruposSinFuncion.FieldByName('Descripcion').AsString);
		ObtenerUsuariosGruposSinFuncion.Next;
	end;
	ObtenerUsuariosGruposSinFuncion.Close;
end;

procedure TFormAgregarPermiso.lb_usuariosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
Var
	Bmp: TBitmap;
	w, h: Integer;
	Item: AnsiString;
begin
	with lb_usuarios.Canvas do begin
		FillRect(Rect);
		Item := lb_usuarios.Items[Index];
		if Copy(Item, 1, 1) = '1' then Bmp := img_grupo.Picture.Bitmap
		  else Bmp := img_usuario.Picture.Bitmap;
		w := Bmp.width;
		h := Bmp.height;
		BrushCopy(Classes.Rect(Rect.Left, Rect.Top, Rect.Left + w, Rect.Top + h),
		  Bmp, Classes.Rect(0, 0, w, h), clRed);
		TextOut(Rect.Left + 25, Rect.Top + 1, ParseParamByNumber(Item, 2, CR));
		TextOut(Rect.Left + 175, Rect.Top + 1, ParseParamByNumber(Item, 3, CR));
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: btn_AceptarClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormAgregarPermiso.btn_AceptarClick(Sender: TObject);
Var
	SoloHaciaArriba,i: Integer;
	EsGrupo: Boolean;
	Item, Permiso: AnsiString;
begin
	// Vamos agregando los �tems que est�n seleccionados.
	if rb_conpermiso.checked then Permiso := '1' else Permiso := '0';
	for i := 0 to lb_usuarios.items.count - 1 do begin
		if lb_usuarios.Selected[i] then begin
			Item := lb_usuarios.items[i];
			EsGrupo := (Copy(Item, 1, 1) = '1');
			Item := ParseParamByNumber(Item, 2, CR);

            SoloHaciaArriba:=0;
            if StrToInt('0'+trim(QueryGetValue(DMConnections.BaseBO_Master,format('SELECT Level FROM FuncionesSistemas  WITH (NOLOCK) WHERE CodigoSistema=%s AND Funcion = ''%s''',[FSistema,FFuncion]))))=1 then begin
                if MsgBox(MSG_QUESTION_PERMISOS_PADRE, Caption, MB_YESNO or MB_ICONQUESTION) = IDYES then
                    SoloHaciaArriba:=0
                else
                    SoloHaciaArriba:=1;
            end;


			if EsGrupo then begin
				// Agregamos este Grupo
				QueryExecute(ObtenerUsuariosGruposSinFuncion.Connection,
				  'ActualizarPermisosGrupo ''' + Item + ''', ''' +
                  FSistema + ''', ''' + FFuncion + ''', ' +
				  Permiso + ',' + inttostr(SoloHaciaArriba) );
			end else begin
				// Agregamos este Usuario
				QueryExecute(ObtenerUsuariosGruposSinFuncion.Connection,
				  'ActualizarPermisosUsuario ''' + Item + ''', ''' +
                  FSistema + ''', ''' + FFuncion + ''', ' +
				  Permiso + ',' +inttostr(SoloHaciaArriba) );
			end;
		end;
	end;
	ModalResult := mrOk;
end;

procedure TFormAgregarPermiso.lb_usuariosDblClick(Sender: TObject);
begin
    if btn_Aceptar.Enabled then btn_Aceptar.Click;
end;

procedure TFormAgregarPermiso.lb_usuariosKeyPress(Sender: TObject;
  var Key: Char);
var
    i : Integer;
    fin : Boolean;
begin
    fin := False;
    i := lb_usuarios.ItemIndex;
    while (i + 1 <= lb_usuarios.Count) and not(fin) do
        if UpperCase(ParseParamByNumber(lb_usuarios.Items[i] ,2 ,CR )[1]) <> UpperCase(Key) then Inc(i)
        else fin := True;

    if fin then begin
        lb_usuarios.Selected[lb_usuarios.ItemIndex] := False;
        lb_usuarios.Selected[i] := True;
    end;
end;

end.



