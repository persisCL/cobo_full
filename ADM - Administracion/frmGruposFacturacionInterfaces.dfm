object frmGruposFacturacionInterfaces: TfrmGruposFacturacionInterfaces
  Left = 195
  Top = 144
  Width = 600
  Height = 422
  Caption = 'Grupos de Facturaci'#243'n de Interfaces'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 592
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object DBList: TAbmList
    Left = 0
    Top = 33
    Width = 592
    Height = 231
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'122'#0'Interface                        '
      #0'148'#0'D'#237'a Facturaci'#243'n                     '
      #0'204'#0'Grupo Facturaci'#243'n                                    ')
    HScrollBar = True
    RefreshTime = 100
    Table = GruposFacturacionInterfaces
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBListClick
    OnDrawItem = DBListDrawItem
    OnRefresh = DBListRefresh
    OnInsert = DBListInsert
    OnDelete = DBListDelete
    OnEdit = DBListEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object GroupB: TPanel
    Left = 0
    Top = 264
    Width = 592
    Height = 85
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object LDia: TLabel
      Left = 13
      Top = 35
      Width = 97
      Height = 13
      Caption = '&D'#237'a Facturaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Lgrupo: TLabel
      Left = 13
      Top = 61
      Width = 110
      Height = 13
      Caption = '&Grupo Facturaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LInterface: TLabel
      Left = 14
      Top = 9
      Width = 56
      Height = 13
      Caption = '&Interface:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edDiaFacturacion: TNumericEdit
      Left = 130
      Top = 31
      Width = 145
      Height = 21
      TabOrder = 1
      Decimals = 0
    end
    object cbGruposFacturacion: TVariantComboBox
      Left = 130
      Top = 58
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object cbinterface: TVariantComboBox
      Left = 130
      Top = 5
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <
        item
          Caption = 'Ninguno'
        end
        item
          Caption = 'Presto'
          Value = 'P'
        end
        item
          Caption = 'Falabella'
          Value = 'F'
        end>
    end
  end
  object PBotones: TPanel
    Left = 0
    Top = 349
    Width = 592
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object PAbajo: TPanel
      Left = 270
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 2
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
    end
  end
  object GruposFacturacionInterfaces: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'GruposFacturacionInterfases'
    Left = 234
    Top = 102
  end
  object spObtenerGruposFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerGruposFacturacion'
    Parameters = <>
    Left = 272
    Top = 102
  end
end
