object FormAbmUsuarios: TFormAbmUsuarios
  Left = 246
  Top = 166
  ActiveControl = ListaGrupos
  Caption = 'Gesti'#243'n de Grupos, Usuarios y Permisos'
  ClientHeight = 683
  ClientWidth = 1216
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 1216
    Height = 683
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = ' Grupos '
      ImageIndex = 2
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1208
        Height = 34
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Bevel2: TBevel
          Left = 0
          Top = 32
          Width = 1208
          Height = 2
          Align = alBottom
          Shape = bsTopLine
          ExplicitWidth = 716
        end
        object SpeedButton1: TSpeedButton
          Left = 5
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Cerrar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
            03333377777777777F333301111111110333337F333333337F33330111111111
            0333337F333333337F333301111111110333337F333333337F33330111111111
            0333337F333333337F333301111111110333337F333333337F33330111111111
            0333337F3333333F7F333301111111B10333337F333333737F33330111111111
            0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
            0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
            0333337F377777337F333301111111110333337F333333337F33330111111111
            0333337FFFFFFFFF7F3333000000000003333377777777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_closeClick
        end
        object SpeedButton2: TSpeedButton
          Left = 35
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Agregar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton2Click
        end
        object SpeedButton3: TSpeedButton
          Left = 60
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Eliminar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 85
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Modificar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton4Click
        end
        object SpeedButton5: TSpeedButton
          Left = 116
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Imprimir'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            00033FFFFFFFFFFFFFFF0888888888888880777777777777777F088888888888
            8880777777777777777F0000000000000000FFFFFFFFFFFFFFFF0F8F8F8F8F8F
            8F80777777777777777F08F8F8F8F8F8F9F0777777777777777F0F8F8F8F8F8F
            8F807777777777777F7F0000000000000000777777777777777F3330FFFFFFFF
            03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
            03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
            33333337F3FF7F3733333330F08F0F0333333337F7737F7333333330FFFF0033
            33333337FFFF7733333333300000033333333337777773333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          Visible = False
        end
        object Image1: TImage
          Left = 224
          Top = 8
          Width = 17
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D617042010000424D420100000000000076000000280000001100
            0000110000000100040000000000CC000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF009999999999999999900000009999EEE00EE00EE9900000009990EE0EE00E
            0E0990000000999900EE08F0009990000000999990008F809999900000009999
            9908F000099990000000999990008F8F80999000000099999000F8F800999000
            0000999900008F8F80999000000099900008F8F8F80990000000999000000F8C
            80999000000099900000F8F0099990000000999000078F8F0999900000009990
            000007F099999000000099990000000009999000000099999000000099999000
            0000999999999999999990000000}
          Visible = False
        end
        object Image2: TImage
          Left = 248
          Top = 8
          Width = 21
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D617042010000424D420100000000000076000000280000001500
            0000110000000100040000000000CC000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00990000000000009999999000990888888888809999999000990FFFFFFFFF
            F099999990009990000000000DDD09909000999088888888DDF0E70990009990
            8CCCCCBB0F0070999000999084ECC3B0F00070099000999084ECBB0F70077770
            900099908444BB0FFFF07770900099908888BB0FFF807777000099990000BB0F
            FFFF0700900099999990BBB0F7007770900099999990BBB0FFF0070090009999
            99990BBB0000000090009999999990BBBBB00009900099999999990000099999
            9000999999999999999999999000}
          Visible = False
        end
        object Image3: TImage
          Left = 280
          Top = 8
          Width = 16
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D6170FE000000424DFE0000000000000076000000280000001000
            000011000000010004000000000088000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF0099999999999999999999999999999999999999A99999999999999AA99999
            99999999AAAA99999999999AAAAA9999999999AAA9AAA99999999AA9999AA999
            999999999999AA999999999999999A9999999999999999A9999999999999999A
            9999999999999999A9999999999999999A9999999999999999A9999999999999
            99999999999999999999}
          Visible = False
        end
        object Image4: TImage
          Left = 302
          Top = 8
          Width = 16
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D6170FE000000424DFE0000000000000076000000280000001000
            000011000000010004000000000088000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00999999999999999999999999999999999919999999999199999199999999
            1199999119999991199999991199991199999999111991119999999991111119
            9999999999111199999999999111119999999999111111199999991111991111
            9999991119999111199991119999991111999111999999111199999999999999
            99999999999999999999}
          Visible = False
        end
      end
      object ListaGrupos: TDBList
        Left = 0
        Top = 34
        Width = 1208
        Height = 621
        TabStop = True
        TabOrder = 1
        Align = alClient
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWindowText
        HeaderFont.Height = -11
        HeaderFont.Name = 'MS Sans Serif'
        HeaderFont.Style = []
        SubTitulos.Sections = (
          #0'23'#0
          #0'147'#0'Grupo'
          #0'64'#0'Descripci'#243'n')
        Table = Grupos
        Style = lbOwnerDrawFixed
        OnKeyPress = ListaGruposKeyPress
        OnDrawItem = ListaGruposDrawItem
        OnInsert = ListaGruposInsert
        OnDelete = ListaGruposDelete
        OnEdit = ListaGruposEdit
      end
    end
    object tab_usuarios: TTabSheet
      Caption = ' Usuarios '
      object Panel2: TPanel
        Left = 765
        Top = 34
        Width = 443
        Height = 621
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Splitter1: TSplitter
          Left = 0
          Top = 313
          Height = 0
          ExplicitLeft = 16
          ExplicitTop = 240
          ExplicitHeight = 100
        end
        object ListaUsuarioGrupos: TDBList
          Left = 0
          Top = 0
          Width = 443
          Height = 313
          TabStop = True
          TabOrder = 0
          Align = alTop
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clWindowText
          HeaderFont.Height = -11
          HeaderFont.Name = 'MS Sans Serif'
          HeaderFont.Style = []
          SubTitulos.Sections = (
            #0'23'#0
            #0'37'#0'Grupo'
            #0'64'#0'Descripci'#243'n')
          Table = spGruposUsuarios
          Style = lbOwnerDrawFixed
          OnDrawItem = ListaGruposDrawItem
        end
        object ListaAlertas: TDBList
          Left = 0
          Top = 313
          Width = 443
          Height = 308
          TabStop = True
          TabOrder = 1
          Align = alBottom
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clWindowText
          HeaderFont.Height = -11
          HeaderFont.Name = 'MS Sans Serif'
          HeaderFont.Style = []
          SubTitulos.Sections = (
            #0'23'#0
            
              #0'271'#0'M'#243'dulo                                                     ' +
              '                       '
            #0'47'#0'Eventos'
            #0'40'#0'Alertas')
          Table = spADM_UsuariosSistemasEnvioAlarmas_SELECT
          Style = lbOwnerDrawFixed
          OnDrawItem = ListaAlertasDrawItem
        end
      end
      object ListaUsuarios: TDBList
        Left = 0
        Top = 34
        Width = 765
        Height = 621
        TabStop = True
        TabOrder = 0
        Align = alClient
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWindowText
        HeaderFont.Height = -11
        HeaderFont.Name = 'MS Sans Serif'
        HeaderFont.Style = []
        SubTitulos.Sections = (
          #0'22'#0
          #0'125'#0'Usuario                           '
          #0'171'#0'Nombre                                          '
          #0'171'#0'Apellido                                          '
          #0'171'#0'Apellido Materno                            ')
        Table = Usuarios
        Style = lbOwnerDrawFixed
        OnClick = ListaUsuariosClick
        OnKeyPress = ListaUsuariosKeyPress
        OnDrawItem = ListaUsuariosDrawItem
        OnInsert = ListaUsuariosInsert
        OnDelete = ListaUsuariosDelete
        OnEdit = ListaUsuariosEdit
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1208
        Height = 34
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Bevel1: TBevel
          Left = 0
          Top = 32
          Width = 1208
          Height = 2
          Align = alBottom
          Shape = bsTopLine
          ExplicitWidth = 716
        end
        object btn_close: TSpeedButton
          Left = 5
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Cerrar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
            03333377777777777F333301111111110333337F333333337F33330111111111
            0333337F333333337F333301111111110333337F333333337F33330111111111
            0333337F333333337F333301111111110333337F333333337F33330111111111
            0333337F3333333F7F333301111111B10333337F333333737F33330111111111
            0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
            0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
            0333337F377777337F333301111111110333337F333333337F33330111111111
            0333337FFFFFFFFF7F3333000000000003333377777777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_closeClick
        end
        object btn_Insert: TSpeedButton
          Left = 35
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Agregar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_InsertClick
        end
        object btn_Delete: TSpeedButton
          Left = 60
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Eliminar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_DeleteClick
        end
        object btn_Edit: TSpeedButton
          Left = 85
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Modificar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_EditClick
        end
        object btn_print: TSpeedButton
          Left = 116
          Top = 5
          Width = 25
          Height = 24
          Hint = 'Imprimir'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            00033FFFFFFFFFFFFFFF0888888888888880777777777777777F088888888888
            8880777777777777777F0000000000000000FFFFFFFFFFFFFFFF0F8F8F8F8F8F
            8F80777777777777777F08F8F8F8F8F8F9F0777777777777777F0F8F8F8F8F8F
            8F807777777777777F7F0000000000000000777777777777777F3330FFFFFFFF
            03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
            03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
            33333337F3FF7F3733333330F08F0F0333333337F7737F7333333330FFFF0033
            33333337FFFF7733333333300000033333333337777773333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          Visible = False
        end
        object img_usuario: TImage
          Left = 224
          Top = 8
          Width = 17
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D617042010000424D420100000000000076000000280000001100
            0000110000000100040000000000CC000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF009999999999999999900000009999EEE00EE00EE9900000009990EE0EE00E
            0E0990000000999900EE08F0009990000000999990008F809999900000009999
            9908F000099990000000999990008F8F80999000000099999000F8F800999000
            0000999900008F8F80999000000099900008F8F8F80990000000999000000F8C
            80999000000099900000F8F0099990000000999000078F8F0999900000009990
            000007F099999000000099990000000009999000000099999000000099999000
            0000999999999999999990000000}
          Visible = False
        end
        object img_grupo: TImage
          Left = 248
          Top = 8
          Width = 21
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D617042010000424D420100000000000076000000280000001500
            0000110000000100040000000000CC000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00990000000000009999999000990888888888809999999000990FFFFFFFFF
            F099999990009990000000000DDD09909000999088888888DDF0E70990009990
            8CCCCCBB0F0070999000999084ECC3B0F00070099000999084ECBB0F70077770
            900099908444BB0FFFF07770900099908888BB0FFF807777000099990000BB0F
            FFFF0700900099999990BBB0F7007770900099999990BBB0FFF0070090009999
            99990BBB0000000090009999999990BBBBB00009900099999999990000099999
            9000999999999999999999999000}
          Visible = False
        end
        object img_ok: TImage
          Left = 280
          Top = 8
          Width = 16
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D6170FE000000424DFE0000000000000076000000280000001000
            000011000000010004000000000088000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF0099999999999999999999999999999999999999A99999999999999AA99999
            99999999AAAA99999999999AAAAA9999999999AAA9AAA99999999AA9999AA999
            999999999999AA999999999999999A9999999999999999A9999999999999999A
            9999999999999999A9999999999999999A9999999999999999A9999999999999
            99999999999999999999}
          Visible = False
        end
        object img_can: TImage
          Left = 302
          Top = 8
          Width = 16
          Height = 17
          AutoSize = True
          Picture.Data = {
            07544269746D6170FE000000424DFE0000000000000076000000280000001000
            000011000000010004000000000088000000C40E0000C40E0000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00999999999999999999999999999999999919999999999199999199999999
            1199999119999991199999991199991199999999111991119999999991111119
            9999999999111199999999999111119999999999111111199999991111991111
            9999991119999111199991119999991111999111999999111199999999999999
            99999999999999999999}
          Visible = False
        end
      end
    end
    object tab_Permisos: TTabSheet
      Caption = ' Permisos '
      ImageIndex = 1
      object Splitter2: TSplitter
        Left = 361
        Top = 0
        Height = 655
        ExplicitLeft = 352
        ExplicitTop = 160
        ExplicitHeight = 100
      end
      object Panel5: TPanel
        Left = 364
        Top = 0
        Width = 844
        Height = 655
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel5'
        TabOrder = 0
        object ListaPermisos: TDBList
          Left = 0
          Top = 54
          Width = 844
          Height = 569
          TabStop = True
          TabOrder = 0
          Align = alClient
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clWindowText
          HeaderFont.Height = -11
          HeaderFont.Name = 'MS Sans Serif'
          HeaderFont.Style = []
          SubTitulos.Sections = (
            #0'25'#0
            #0'156'#0'Usuario/Grupo                          '
            
              #0'234'#0'Nombre                                                     ' +
              '          '
            #0'45'#0'Permiso')
          RefreshTime = 0
          Table = ObtenerPermisosFuncion
          Style = lbOwnerDrawFixed
          OnDrawItem = ListaPermisosDrawItem
          OnRefresh = ListaPermisosRefresh
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 844
          Height = 54
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label4: TLabel
            Left = 4
            Top = 39
            Width = 270
            Height = 13
            Caption = 'Grupos y Usuarios con permisos para la funci'#243'n indicada:'
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 623
          Width = 844
          Height = 32
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object Button1: TButton
            Left = 0
            Top = 4
            Width = 75
            Height = 25
            Caption = '&Agregar'
            TabOrder = 0
            OnClick = Button1Click
          end
          object btn_quitarpermiso: TButton
            Left = 81
            Top = 6
            Width = 75
            Height = 25
            Caption = '&Quitar'
            TabOrder = 1
            OnClick = btn_quitarpermisoClick
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 361
        Height = 655
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel5'
        TabOrder = 1
        object ListaFunciones: TDBList
          Left = 0
          Top = 54
          Width = 361
          Height = 601
          TabStop = True
          TabOrder = 0
          Align = alClient
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clWindowText
          HeaderFont.Height = -11
          HeaderFont.Name = 'MS Sans Serif'
          HeaderFont.Style = []
          RefreshTime = 0
          Table = spFunciones
          Style = lbOwnerDrawFixed
          OnClick = ListaFuncionesClick
          OnDrawItem = ListaFuncionesDrawItem
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 361
          Height = 54
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label5: TLabel
            Left = 9
            Top = 12
            Width = 52
            Height = 13
            Caption = 'Aplicaci'#243'n:'
          end
          object Label6: TLabel
            Left = 3
            Top = 39
            Width = 52
            Height = 13
            Caption = 'Funciones:'
          end
          object cb_Sistema: TComboBox
            Left = 66
            Top = 7
            Width = 281
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnClick = cb_SistemaClick
          end
        end
      end
    end
  end
  object Usuarios: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = UsuariosAfterOpen
    TableName = 'VW_UsuariosSistemas'
    Left = 499
    Top = 170
  end
  object Grupos: TADOTable
    Connection = DMConnections.BaseBO_Master
    AfterOpen = GruposAfterOpen
    TableName = 'GruposSistemas'
    Left = 443
    Top = 170
  end
  object ObtenerPermisosFuncion: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ObtenerPermisosFuncion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Funcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end>
    Left = 683
    Top = 98
  end
  object tmrInicializarBusqueda: TTimer
    Enabled = False
    Interval = 5000000
    OnTimer = tmrInicializarBusquedaTimer
    Left = 688
    Top = 27
  end
  object spGruposUsuarios: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposUsuarios_SELECT'
    Parameters = <
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 680
    Top = 152
  end
  object spSistemas: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_SistemasModulos_Aplicaciones_SELECT'
    Parameters = <>
    Left = 680
    Top = 208
  end
  object spFunciones: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_FuncionesSistemas_SELECT'
    Parameters = <
      item
        Name = '@CodSistema'
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 680
    Top = 264
  end
  object spDELPermisosUsuarios: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_PermisosUsuarios_DELETE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 224
    Top = 384
  end
  object AdobeSPOpenDocuments1: TAdobeSPOpenDocuments
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 680
    Top = 336
  end
  object spDELGruposUsuarios: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposUsuarios_DELETE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 224
    Top = 288
  end
  object spDELUsuariosSistemas: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_UsuariosSistemas_DELETE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 224
    Top = 336
  end
  object spDELPermisosGrupos: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_PermisosGrupos_DELETE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 112
    Top = 288
  end
  object spDELGruposUsuarios_Grupo: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposUsuarios_Grupo_DELETE'
    Parameters = <
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 104
    Top = 336
  end
  object spDELGruposSistemas: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_GruposSistemas_DELETE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 104
    Top = 384
  end
  object spQuitarPermisosGrupo: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'QuitarPermisosGrupo'
    Parameters = <
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@Funcion'
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 648
    Top = 512
  end
  object spQuitarPermisosUsuario: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'QuitarPermisosUsuario'
    Parameters = <
      item
        Name = '@CodigoGrupo'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@Funcion'
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 648
    Top = 560
  end
  object spADM_UsuariosSistemasEnvioAlarmas_SELECT: TADOStoredProc
    Connection = DMConnections.BaseBO_Master
    ProcedureName = 'ADM_UsuariosSistemasEnvioAlarmas_SELECT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Value = 0
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4000
        Value = Null
      end>
    Left = 928
    Top = 480
  end
end
