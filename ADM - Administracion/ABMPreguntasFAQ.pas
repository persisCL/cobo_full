{********************************** File Header ********************************
File Name   : ABMPreguntasFAQ
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 08-Mar-2004
Language    : ES-AR
Description : ABM en Maestro de Preguntas de FAQs

Autor       :   CQuezadaI
Fecha       :   17 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se agrega Try-Finally.
                Se agrega Transacccion al momento de querer eliminar.
                Se agrega el evento de cierre a la barra de botones.
*******************************************************************************}

unit ABMPreguntasFAQ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaProcs,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, Util, ADODB, Variants,
  DPSControls, EBEditors, VariantComboBox,
  FileCtrl;

type
  TfrmABMPreguntasFAQ = class(TForm)
	PreguntasFAQ: TADOTable;
	qry_MaxPregunta: TADOQuery;
    panABM: TPanel;
    abmPreguntas: TAbmToolbar;
	dblPreguntas: TAbmList;
    panEdicion: TPanel;
	GroupB: TPanel;
    Label15: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    Imagen: TImage;
    Label3: TLabel;
	txt_CodigoPregunta: TNumericEdit;
    txt_Descripcion: TEdit;
    cbArchivos: TComboBox;
    cbImagenes: TComboBox;
    memRespuesta: TEBEditor;
    Panel2: TPanel;
    Notebook: TNotebook;
    cbFuentesSolicitud: TComboBox;
    rgFiltro: TRadioGroup;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    btnDuplicar: TButton;
	procedure BtnCancelarClick(Sender: TObject);
	procedure dblPreguntasClick(Sender: TObject);
	procedure dblPreguntasDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure dblPreguntasEdit(Sender: TObject);
	procedure dblPreguntasRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure dblPreguntasDelete(Sender: TObject);
	procedure dblPreguntasInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function dblPreguntasProcess(Tabla: TDataSet; var Texto: String): Boolean;
	procedure cbImagenesChange(Sender: TObject);
    procedure btnDuplicarClick(Sender: TObject);
    procedure rgFiltroClick(Sender: TObject);
  private
	{ Private declarations }
	EdicionExterna : Boolean;
	FDirIMagenes: ANSIString;
	procedure Limpiar_Campos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializar(CodigoPregunta: Integer = 0): Boolean;
  end;

var
  frmABMPreguntasFAQ: TfrmABMPreguntasFAQ;

implementation

uses RStrings, DMConnection, ConstParametrosGenerales;

{$R *.DFM}

function TfrmABMPreguntasFAQ.Inicializar(CodigoPregunta: Integer = 0): Boolean;
Var
	valor: AnsiString;
	F: TSearchRec;
begin
	Result := False;
	if not OpenTables([PreguntasFAQ]) then exit;

	cbArchivos.clear;
	if ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DOCUMENTOS_FAQ, Valor) then begin
		// crea lista de Archivos Asociados para FAQ
		cbArchivos.items.add(SELECCIONAR);
		if findFirst(GoodDir(Valor) + '*.*', $00000001, f) = 0 then begin
			repeat
				cbArchivos.items.add(f.Name);
			until FindNext(f) <> 0;
			FindClose(f);
		end;
        cbArchivos.ItemIndex := 0;
	end;

	cbImagenes.Clear;
	if ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_PREGUNTAS, Valor) then begin
		cbImagenes.items.add(SELECCIONAR);
		if findFirst(GoodDir(Valor)   + '*.*', $00000001, f) = 0 then begin
			repeat
				cbImagenes.items.add(f.Name);
			until FindNext(f) <> 0;
			FindClose(f);
		end;
	end;

    CargarFuentesSolicitud(DMCOnnections.BaseCAC, cbFuentesSolicitud, 0, true, false, '');
	Notebook.PageIndex := 0;

	dblPreguntas.Table.Filtered := True;
    dblPreguntas.Table.Filter := '';

	EdicionExterna := CodigoPregunta <> 0;
	If EdicionExterna Then Begin
		Constraints.MaxHeight := Constraints.MinHeight;
		btnDuplicar.Visible := False;

		dblPreguntas.Table.Filter := 'CodigoPregunta = ' + IntToStr (CodigoPregunta);

		panABM.Visible := False;
		dblPreguntasClick(dblPreguntas);
		dblPreguntasEdit(dblPreguntas);
	End
	else dblPreguntas.Reload;

	Result := True;
end;

procedure TfrmABMPreguntasFAQ.BtnCancelarClick(Sender: TObject);
begin
	if EdicionExterna then begin
		Notebook.PageIndex := 0;
		BtnSalirClick(Sender);
	end
	else Volver_Campos;
end;

procedure TfrmABMPreguntasFAQ.dblPreguntasClick(Sender: TObject);

begin
	with (Sender AS TDbList).Table do begin
		txt_CodigoPregunta.Value	:= FieldByName('CodigoPregunta').AsInteger;
		txt_Descripcion.text		:= Trim(FieldByName('Descripcion').AsString);

		cbArchivos.ItemIndex := IndexCombo(cbArchivos, Trim (FieldByName('ArchivoAsociado').AsString));
		cbImagenes.ItemIndex := IndexCombo(cbImagenes, Trim (FieldByName('ImagenAsociada').AsString));

		memRespuesta.RTFText := FieldByName('Respuesta').AsString
	end;

	cbImagenesChange(Sender);
end;

procedure TfrmABMPreguntasFAQ.dblPreguntasDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoPregunta').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

procedure TfrmABMPreguntasFAQ.dblPreguntasEdit(Sender: TObject);
var
	valor : ansiString;
	F: TSearchRec;
begin
	dblPreguntas.Enabled    		:= False;
	dblPreguntas.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
	groupb.Enabled     		:= True;

	(* Cargamos las listas de archivos actualizada *)
	cbArchivos.Clear;
	if ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DOCUMENTOS_FAQ, Valor) then begin
        cbArchivos.items.add(SELECCIONAR);
		if findFirst(GoodDir(Valor) + '*.*', $00000001, f) = 0 then begin
			repeat
				cbArchivos.items.add(f.Name);
			until FindNext(f) <> 0;
			FindClose(f);
		end;
		cbArchivos.ItemIndex := 0;
	end;

	cbImagenes.Clear;
	if ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_PREGUNTAS, Valor) then begin
		FDirIMagenes := Valor;
        cbImagenes.items.add(SELECCIONAR);
		if findFirst(GoodDir(Valor) + '*.*', $00000001, f) = 0 then begin
			repeat
				cbImagenes.items.add(f.Name);
			until FindNext(f) <> 0;
			FindClose(f);
		end;
	end;

	cbArchivos.ItemIndex := IndexCombo(cbArchivos, Trim (PreguntasFAQ.FieldByName('ArchivoAsociado').AsString));
	cbImagenes.ItemIndex := IndexCombo(cbImagenes, Trim (PreguntasFAQ.FieldByName('ImagenAsociada').AsString));

	txt_CodigoPregunta.Enabled:= False;
	if panABM.Visible then txt_Descripcion.setFocus;
end;

procedure TfrmABMPreguntasFAQ.dblPreguntasRefresh(Sender: TObject);
begin
	if dblPreguntas.Empty then Limpiar_Campos();
end;

procedure TfrmABMPreguntasFAQ.Limpiar_Campos();
begin
	txt_CodigoPregunta.Clear;
	txt_Descripcion.Clear;
	cbArchivos.ItemIndex := 0;
	cbImagenes.ItemIndex := 0;
	memRespuesta.RTFText := '';
end;

procedure TfrmABMPreguntasFAQ.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

procedure TfrmABMPreguntasFAQ.dblPreguntasDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[FLD_PREGUNTA]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        try                                                                                                                                                                                 // SS_1147_CQU_20140714
            DMConnections.BaseCAC.BeginTrans;                                                                                                                                               // SS_1147_CQU_20140714
            try                                                                                                                                                                             // SS_1147_CQU_20140714
                (Sender AS TDbList).Table.Delete;                                                                                                                                           // SS_1147_CQU_20140714
            Except                                                                                                                                                                          // SS_1147_CQU_20140714
                On E: Exception do begin                                                                                                                                                    // SS_1147_CQU_20140714
                    (Sender AS TDbList).Table.Cancel;                                                                                                                                       // SS_1147_CQU_20140714
                    MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[FLD_PREGUNTA]), Format(MSG_ERROR_ELIMINAR_DEPENDENCIA,[FLD_PREGUNTA]), Format(MSG_CAPTION_ELIMINAR,[FLD_PREGUNTA]), MB_ICONSTOP);  // SS_1147_CQU_20140714
                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                                                                                        // SS_1147_CQU_20140714
                end;                                                                                                                                                                        // SS_1147_CQU_20140714
            end;                                                                                                                                                                            // SS_1147_CQU_20140714
        finally                                                                                                                                                                             // SS_1147_CQU_20140714
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;                                                                                                  // SS_1147_CQU_20140714
            dblPreguntas.Reload;                                                                                                                                                            // SS_1147_CQU_20140714
        end;                                                                                                                                                                                // SS_1147_CQU_20140714
	end;
	dblPreguntas.Estado     := Normal;
	dblPreguntas.Enabled    := True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TfrmABMPreguntasFAQ.dblPreguntasInsert(Sender: TObject);
begin
	groupb.Enabled     := True;
	Limpiar_Campos;
	dblPreguntas.Enabled    := False;
	Notebook.PageIndex := 1;
	txt_CodigoPregunta.Enabled := False;
	txt_Descripcion.SetFocus;
end;

procedure TfrmABMPreguntasFAQ.BtnAceptarClick(Sender: TObject);
begin
   if not ValidateControls(
		[txt_descripcion],
		[trim(txt_descripcion.text) <> ''],
		MSG_CAPTION_ACTUALIZAR,
		[FLD_DESCRIPCION]) then exit;

	Screen.Cursor := crHourGlass;
	With dblPreguntas.Table do begin
		Try
			if dblPreguntas.Estado = Alta then begin
				Append;
				qry_MaxPregunta.Open;
				txt_CodigoPregunta.Value := qry_MaxPregunta.FieldByNAme('CodigoPregunta').AsInteger + 1;
				qry_MaxPregunta.Close;
			end else begin
				Edit;
			end;
			FieldByName('CodigoPregunta').AsInteger	:= Trunc(txt_CodigoPregunta.Value);
			if Trim(txt_Descripcion.text) = '' then
				FieldByName('Descripcion').Clear
			else
				FieldByName('Descripcion').AsString	:= Trim(txt_Descripcion.text);
			FieldByName('ArchivoAsociado').AsString	:= Trim(cbArchivos.Text);
			FieldByName('ImagenAsociada').AsString	:= Trim(cbImagenes.Text);
			FieldByName('FechaHoraUltimaActualizacion').AsDateTime	:= Now;
			FieldByName('Respuesta').AsString := '';
			FieldByName('Respuesta').AsString := memRespuesta.RTFText;

			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_PREGUNTA]), E.message, Format (MSG_CAPTION_ACTUALIZAR,[FLD_PREGUNTA]), MB_ICONSTOP);
			end;
		end;
	end;
	Screen.Cursor 	   := crDefault;

	if EdicionExterna then begin
		Notebook.PageIndex := 0;
		BtnSalirClick(Sender);
	end
	else Volver_Campos;
end;

procedure TfrmABMPreguntasFAQ.FormShow(Sender: TObject);
begin
	dblPreguntas.Reload;
end;

procedure TfrmABMPreguntasFAQ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;

procedure TfrmABMPreguntasFAQ.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TfrmABMPreguntasFAQ.Volver_Campos;
begin
	dblPreguntas.Estado     := Normal;
	dblPreguntas.Enabled    := True;
	dblPreguntas.Reload;
	dblPreguntas.SetFocus;
	txt_CodigoPregunta.Enabled := True;
	Notebook.PageIndex := 0;
	groupb.Enabled     := False;
end;

function TfrmABMPreguntasFAQ.dblPreguntasProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Texto := Tabla.FieldByName('CodigoPregunta').AsString + ' ' +
			  Trim(Tabla.FieldByName('Descripcion').AsString);
	Result := True;
end;

procedure TfrmABMPreguntasFAQ.cbImagenesChange(Sender: TObject);
begin
	try
		Imagen.Visible := cbImagenes.Text <> '';
		if FileExists (DIR_IMAGENES_PREGUNTAS + cbImagenes.Text) then
			Imagen.Picture.LoadFromFile(FDirIMagenes + cbImagenes.Text);
	except
	end;
end;

procedure TfrmABMPreguntasFAQ.btnDuplicarClick(Sender: TObject);
begin
	dblPreguntas.Estado := Alta;
	Limpiar_Campos;

	with dblPreguntas.Table do begin
		txt_Descripcion.text := Trim(FieldByName('Descripcion').AsString);
		cbArchivos.ItemIndex := IndexCombo(cbArchivos, Trim (FieldByName('ArchivoAsociado').AsString));
		cbImagenes.ItemIndex := IndexCombo(cbImagenes, Trim (FieldByName('ImagenAsociada').AsString));
		memRespuesta.RTFText := FieldByName('Respuesta').AsString
	end;

	BtnAceptarClick(dblPreguntas);
end;

procedure TfrmABMPreguntasFAQ.rgFiltroClick(Sender: TObject);
begin
    if rgfiltro.itemindex = 0 then begin
    	abmPreguntas.Habilitados:= [btAlta, btBaja, btModi, btSalir, btBuscar];
    	PreguntasFAQ.Filter := ''
    end
    else begin
	    abmPreguntas.Habilitados:= [btBaja, btModi, btSalir, btBuscar];
    	PreguntasFAQ.Filter := 'CodigoFuenteSolicitud = ' + Trim(Copy(cbFuentesSolicitud.Text, 201, 10));
    end;

    dblPreguntas.Reload
end;

end.

