object FormTramos: TFormTramos
  Left = 222
  Top = 171
  Width = 708
  Height = 432
  Caption = 'Mantenimiento de Tramos'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefaultPosOnly
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 700
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 700
    Height = 199
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'117'#0'Concesionaria              '
      #0'47'#0'C'#243'digo  '
      #0'295'#0'Descripci'#243'n                                       '
      #0'117'#0'Sector                          '
      #0'82'#0'Longitud en Km')
    HScrollBar = True
    RefreshTime = 100
    Table = Tramos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 232
    Width = 700
    Height = 134
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 26
      Top = 85
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = txt_Descripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 26
      Top = 12
      Width = 69
      Height = 13
      Caption = 'C'#243'digo Tramo:'
    end
    object Label3: TLabel
      Left = 26
      Top = 61
      Width = 42
      Height = 13
      Caption = '&Sector:'
      FocusControl = cb_SectoresPorEje
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 26
      Top = 110
      Width = 54
      Height = 13
      Caption = '&Longitud:'
      FocusControl = txt_Longitud
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 230
      Top = 110
      Width = 22
      Height = 13
      Caption = 'Km.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 26
      Top = 37
      Width = 85
      Height = 13
      Caption = '&Concesionaria:'
      FocusControl = cbConcesionarias
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 119
      Top = 81
      Width = 297
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 3
    end
    object txt_CodigoTramo: TNumericEdit
      Left = 119
      Top = 8
      Width = 105
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
      Decimals = 0
    end
    object cb_SectoresPorEje: TComboBox
      Left = 119
      Top = 57
      Width = 297
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
    end
    object txt_Longitud: TNumericEdit
      Left = 119
      Top = 106
      Width = 105
      Height = 21
      Color = 16444382
      MaxLength = 10
      TabOrder = 4
      Decimals = 2
    end
    object cbConcesionarias: TComboBox
      Left = 119
      Top = 33
      Width = 297
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbConcesionariasChange
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 366
    Width = 700
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 503
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Tramos: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Tramos'
    Left = 250
    Top = 91
  end
  object qry_Sectores: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM '
      'SECTORES WITH(NOLOCK)')
    Left = 279
    Top = 91
  end
  object qry_MaxTramo: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT ISNULL(MAX(CodigoTramo),0) AS CodigoTramo FROM '
      'TRAMOS')
    Left = 308
    Top = 91
  end
end
