object FormPuntoCobro: TFormPuntoCobro
  Left = 299
  Top = 149
  Width = 681
  Height = 537
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Puntos de Cobro'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 464
    Width = 673
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Notebook: TNotebook
      Left = 477
      Top = 0
      Width = 196
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 109
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 6
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 673
    Height = 280
    TabStop = True
    TabOrder = 0
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'99'#0'Concesionaria        '
      #0'62'#0'C'#243'digo       '
      #0'142'#0'Descripci'#243'n                          '
      #0'142'#0'Eje Vial                                 '
      #0'89'#0'Sentido               '
      #0'57'#0'KM           '
      #0'48'#0'Carriles  '
      #0'82'#0'Ultimo Portico   '
      #0'103'#0'Codigo Punto Cobro')
    HScrollBar = True
    RefreshTime = 100
    Table = qryPuntosCobro
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnProcess = DBList1Process
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 673
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object PageControl: TPageControl
    Left = 0
    Top = 313
    Width = 673
    Height = 151
    ActivePage = Tab_General
    Align = alBottom
    TabIndex = 0
    TabOrder = 3
    object Tab_General: TTabSheet
      Caption = 'General'
      Enabled = False
      object Label1: TLabel
        Left = 10
        Top = 66
        Width = 72
        Height = 13
        Caption = '&Descripci'#243'n:'
        FocusControl = txt_descripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 10
        Top = 38
        Width = 48
        Height = 13
        Caption = '&C'#243'digo: '
        FocusControl = txt_PuntoDeCobro
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 10
        Top = 95
        Width = 109
        Height = 13
        Caption = 'C'#243'digo de Eje &Vial:'
        FocusControl = cb_EjesViales
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 10
        Top = 10
        Width = 85
        Height = 13
        Caption = 'C&oncesionaria:'
        FocusControl = cbConcesionaria
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_descripcion: TEdit
        Left = 138
        Top = 62
        Width = 323
        Height = 21
        Color = 16444382
        MaxLength = 60
        TabOrder = 2
      end
      object cb_EjesViales: TComboBox
        Left = 138
        Top = 91
        Width = 325
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 3
      end
      object txt_PuntoDeCobro: TNumericEdit
        Left = 138
        Top = 34
        Width = 121
        Height = 21
        Color = 16444382
        MaxLength = 4
        TabOrder = 1
        Decimals = 0
      end
      object cbConcesionaria: TComboBox
        Left = 138
        Top = 6
        Width = 223
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbConcesionariaChange
      end
    end
    object tab_vias: TTabSheet
      Caption = 'Secuencia'
      Enabled = False
      ImageIndex = 2
      object Label6: TLabel
        Left = 8
        Top = 95
        Width = 96
        Height = 13
        Caption = 'Cantidad de carriles:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 8
        Top = 11
        Width = 48
        Height = 13
        Caption = '&Sentido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 8
        Top = 66
        Width = 46
        Height = 13
        Caption = '&Kil'#243'metro:'
        FocusControl = txt_km
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 8
        Top = 38
        Width = 136
        Height = 13
        Caption = '&C'#243'digo Punto de Cobro:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_cantidadCarriles: TNumericEdit
        Left = 151
        Top = 91
        Width = 63
        Height = 21
        MaxLength = 2
        TabOrder = 3
        Decimals = 0
      end
      object CB_ultimoPorticoSentido: TCheckBox
        Left = 304
        Top = 95
        Width = 198
        Height = 13
        Caption = 'Es el '#250'ltimo portico en ese sentido'
        TabOrder = 4
      end
      object CB_sentidoEstacion: TComboBox
        Left = 151
        Top = 7
        Width = 113
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 0
        Items.Strings = (
          'Ascendente'
          'Descendente')
      end
      object txt_km: TNumericEdit
        Left = 151
        Top = 62
        Width = 63
        Height = 21
        TabOrder = 2
        Decimals = 2
      end
      object txt_CodigoPCConcesionaria: TNumericEdit
        Left = 151
        Top = 34
        Width = 63
        Height = 21
        Color = 16444382
        MaxLength = 2
        TabOrder = 1
        Decimals = 0
      end
    end
    object Tab_Tramos: TTabSheet
      Caption = 'Tramos'
      ImageIndex = 3
      object chk_Tramos: TCheckListBox
        Left = 0
        Top = 0
        Width = 665
        Height = 123
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object Tab_Opciones: TTabSheet
      Caption = 'Opciones'
      ImageIndex = 4
      object Label7: TLabel
        Left = 8
        Top = 12
        Width = 257
        Height = 13
        Caption = 'Paths de fotograf'#237'as de tr'#225'nsitos pendientes:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label8: TLabel
        Left = 8
        Top = 41
        Width = 176
        Height = 13
        Caption = 'Paths de fotograf'#237'as de viajes:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label5: TLabel
        Left = 10
        Top = 97
        Width = 134
        Height = 13
        Caption = 'Ultima ejecuci'#243'n del Loader:'
      end
      object Label2: TLabel
        Left = 8
        Top = 69
        Width = 39
        Height = 13
        Caption = 'TSMC:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_PathFotosViajes: TEdit
        Left = 265
        Top = 37
        Width = 382
        Height = 21
        Color = 16444382
        TabOrder = 1
        Text = 'txt_PathFotosViajes'
      end
      object txt_PathFotosPendientes: TEdit
        Left = 265
        Top = 8
        Width = 382
        Height = 21
        Color = 16444382
        TabOrder = 0
        Text = 'txt_PathFotosPendientes'
      end
      object deLoader: TDateEdit
        Left = 265
        Top = 93
        Width = 97
        Height = 21
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
      object cbTSMC: TComboBox
        Left = 265
        Top = 66
        Width = 153
        Height = 19
        Style = csOwnerDrawFixed
        Color = 16444382
        ItemHeight = 13
        TabOrder = 3
      end
    end
  end
  object PuntosCobro: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'PuntosCobro'
    Left = 307
    Top = 160
  end
  object qry_EjesViales: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM EJESVIALES WITH (NOLOCK)')
    Left = 180
    Top = 160
  end
  object ObtenerTramosPorPuntoDeCobro: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTramosPorPuntoDeCobro'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPuntoCobro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 236
    Top = 160
  end
  object EliminarTramosPorPuntoDeCobro: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarTramosPorPuntoDeCobro'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPuntoCobro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 272
    Top = 160
  end
  object TramosPuntosCobro: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'dbo.TramosPuntosCobro'
    Left = 340
    Top = 160
  end
  object qryPuntosCobro: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT PuntosCobro.*, Concesionarias.Descripcion AS Concesionari' +
        'a, EjesViales.Descripcion AS EjeVial'
      '  FROM PuntosCobro, Concesionarias, EjesViales'
      
        ' WHERE PuntosCobro.codigoCOncesionaria = Concesionarias.CodigoCo' +
        'ncesionaria'
      '   AND PuntosCobro.CodigoEjeVial = EjesViales.CodigoEjeVial')
    Left = 108
    Top = 160
  end
  object qryTSMC: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoConcesionaria'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM TSMCs'
      ' WHERE CodigoConcesionaria = :CodigoConcesionaria')
    Left = 144
    Top = 160
  end
end
