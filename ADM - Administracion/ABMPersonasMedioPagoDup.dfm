object FABMPersonasMedioPagoDup: TFABMPersonasMedioPagoDup
  Left = 149
  Top = 114
  Width = 825
  Height = 605
  Caption = 'Mantenimiento de RUTs Autorizados para Medio Pago Duplicado'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 9
    Top = 20
    Width = 43
    Height = 13
    Caption = 'N'#250'mero :'
  end
  object Label3: TLabel
    Left = 9
    Top = 49
    Width = 36
    Height = 13
    Caption = 'Fecha :'
  end
  object GroupB: TPanel
    Left = 0
    Top = 492
    Width = 817
    Height = 40
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Label10: TLabel
      Left = 11
      Top = 14
      Width = 116
      Height = 13
      Caption = '&Numero Documento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtDocumento: TEdit
      Left = 130
      Top = 10
      Width = 135
      Height = 21
      Color = 16444382
      MaxLength = 9
      TabOrder = 0
      OnKeyPress = txtDocumentoKeyPress
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 532
    Width = 817
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 620
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 176
    Top = 2
    Width = 104
    Height = 27
    BevelOuter = bvNone
    TabOrder = 2
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 817
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbar1Close
  end
  object Lista: TAbmList
    Left = 0
    Top = 33
    Width = 817
    Height = 459
    TabStop = True
    TabOrder = 4
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'103'#0'N'#250'mero Documento')
    HScrollBar = True
    RefreshTime = 100
    Table = tblPersonasMPDup
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaClick
    OnProcess = ListaProcess
    OnDrawItem = ListaDrawItem
    OnRefresh = ListaRefresh
    OnInsert = ListaInsert
    OnDelete = ListaDelete
    OnEdit = ListaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object tblPersonasMPDup: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'PersonasMedioPagoDuplicados'
    Left = 588
    Top = 73
  end
  object dsPersonasMPDup: TDataSource
    DataSet = tblPersonasMPDup
    Left = 558
    Top = 74
  end
end
