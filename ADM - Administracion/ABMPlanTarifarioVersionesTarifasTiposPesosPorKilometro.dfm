object FrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro: TFrmPlanTarifarioVersionesTarifaTipoPesosPorKilometro
  Left = 0
  Top = 0
  Caption = 'Plan Tarifario - Versiones - TarifaTipo - Pesos Por Kilometro'
  ClientHeight = 302
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 261
    Width = 555
    Height = 41
    Align = alBottom
    TabOrder = 0
    ExplicitLeft = 1
    ExplicitTop = 266
    object btnCancela: TButton
      Left = 328
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Siguiente'
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancelar: TButton
      Left = 424
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 81
    Width = 555
    Height = 180
    Align = alClient
    Caption = 'Panel3'
    TabOrder = 1
    ExplicitWidth = 528
    ExplicitHeight = 175
    object dbePlanTarifarioVersionesTarifasTipos_Obtener: TDBListEx
      Left = 1
      Top = 1
      Width = 553
      Height = 178
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          MinWidth = 100
          MaxWidth = 100
          Header.Caption = 'Versi'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -13
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'ID_PlanTarifarioVersionesTarifasTipos'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 150
          MaxWidth = 150
          Header.Caption = 'Tarifa Tipo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -13
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'CodigoTarifaTipo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 150
          MaxWidth = 150
          Header.Caption = 'Pesos x KM'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -13
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'PesosPorKilometro'
        end>
      DataSource = dsPlanTarifarioVersionesTarifasTipos_Obtener
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnClick = dbePlanTarifarioVersionesTarifasTipos_ObtenerClick
      ExplicitWidth = 526
      ExplicitHeight = 173
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 555
    Height = 81
    Align = alTop
    TabOrder = 2
    ExplicitWidth = 528
    object lblPesosXKM: TLabel
      Left = 8
      Top = 54
      Width = 58
      Height = 13
      Caption = 'Pesos x KM:'
    end
    object lblConcesionaria: TLabel
      Left = 40
      Top = 16
      Width = 104
      Height = 19
      Caption = 'Concesionaria:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblShowConcesionaria: TLabel
      Left = 150
      Top = 21
      Width = 103
      Height = 13
      Caption = 'lblShowConcesionaria'
    end
    object lblShowPesosXKM: TLabel
      Left = 168
      Top = 54
      Width = 82
      Height = 13
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = '0'
      ParentBiDiMode = False
      Transparent = True
    end
    object btnIngresaModificaPesosXKM: TButton
      Left = 328
      Top = 49
      Width = 201
      Height = 25
      Caption = '&Ingresa/Modifica Pesos Por Kilometro'
      TabOrder = 0
      OnClick = btnIngresaModificaPesosXKMClick
    end
    object nedPesosXKM: TNumericEdit
      Left = 81
      Top = 54
      Width = 81
      Height = 21
      TabOrder = 1
      OnChange = nedPesosXKMChange
    end
  end
  object dsPlanTarifarioVersionesTarifasTipos_Obtener: TDataSource
    DataSet = spPlanTarifarioVersionesTarifasTipos_Obtener
    Left = 24
    Top = 264
  end
  object spPlanTarifarioVersionesTarifasTipos_Obtener: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'PlanTarifarioVersionesTarifasTipos_Obtener;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@ID_Concesionaria'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
      end
      item
        Name = '@ID_PlanTarifarioVersion_Habilitar'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4000
      end>
    Left = 64
    Top = 264
  end
  object spPlanTarifarioVersionesTarifasTipos_PesosXKM_Actualizar: TADOStoredProc
    Connection = DMConnections.BaseBO_Rating
    ProcedureName = 'PlanTarifarioVersionesTarifasTipos_PesosXKM_Actualizar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@ID_PlanTarifarioVersionesTarifasTipos'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
      end
      item
        Name = '@ID_PlanTarifarioVersion'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@PesosPorKilometro'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4000
      end>
    Left = 96
    Top = 264
  end
end
