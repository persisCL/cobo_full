object FABMDeclaracionesConvenios: TFABMDeclaracionesConvenios
  Left = 118
  Top = 121
  Width = 640
  Height = 480
  Caption = 'Mantenimiento de Declaraciones de Convenios'
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 632
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object Panel2: TPanel
    Left = 0
    Top = 407
    Width = 632
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 435
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object btn_Salir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btn_SalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object pnl_Datos: TPanel
    Left = 0
    Top = 314
    Width = 632
    Height = 93
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object lbl_Texto: TLabel
      Left = 11
      Top = 59
      Width = 37
      Height = 13
      Caption = '&Texto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 11
      Top = 20
      Width = 99
      Height = 13
      Caption = 'Tipo de &Mensaje:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Texto: TEdit
      Left = 119
      Top = 55
      Width = 606
      Height = 21
      Color = 16444382
      MaxLength = 255
      TabOrder = 1
    end
    object cbTipoMensaje: TComboBox
      Left = 120
      Top = 16
      Width = 145
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Alta'
        'Modificaci'#243'n'
        'P'#233'rdida'
        'Estado Actual')
    end
  end
  object lb_DeclaracionesConvenios: TAbmList
    Left = 0
    Top = 33
    Width = 632
    Height = 281
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'117'#0'Tipo de Mensaje          '
      
        #0'431'#0'Texto                                                      ' +
        '                                                                ' +
        '              ')
    HScrollBar = True
    RefreshTime = 100
    Table = tblDeclaracionesConvenios
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lb_DeclaracionesConveniosClick
    OnDrawItem = lb_DeclaracionesConveniosDrawItem
    OnRefresh = lb_DeclaracionesConveniosRefresh
    OnInsert = lb_DeclaracionesConveniosInsert
    OnDelete = lb_DeclaracionesConveniosDelete
    OnEdit = lb_DeclaracionesConveniosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object dsDeclaracionesConvenios: TDataSource
    DataSet = tblDeclaracionesConvenios
    Left = 15
    Top = 61
  end
  object tblDeclaracionesConvenios: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'DeclaracionesConvenios'
    Left = 48
    Top = 61
    object tblDeclaracionesConveniosTipoMensaje: TStringField
      DisplayLabel = 'TipoMensaje '
      FieldName = 'TipoMensaje'
      FixedChar = True
      Size = 1
    end
    object tblDeclaracionesConveniosTexto: TStringField
      FieldName = 'Texto'
      Size = 255
    end
  end
end
