{-----------------------------------------------------------------------------
 File Name: AbmIVA 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

Etiqueta    :   20160608 MGO
Descripci�n :   Se valida que la fecha no exista en otro registro al modificar
-----------------------------------------------------------------------------}


unit AbmIVA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilDB, DMConnection, DB, Validate, DateEdit, StdCtrls,
  DmiCtrls, ExtCtrls, DbList, Abm_obj, ADODB, PeaProcs,Util, UtilProc;

type
  TForm_ABM_IVA = class(TForm)
    tblIVA: TADOTable;
    pnlButtons: TPanel;
    pnlEditing: TPanel;
    AbmToolbar1: TAbmToolbar;
    AbmList1: TAbmList;
    Notebook1: TNotebook;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    neTasaIVA: TNumericEdit;
    deFechaInicio: TDateEdit;
    tblIVAFechaInicio: TDateTimeField;
    tblIVATasaIVA: TIntegerField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;

    procedure btnSalirClick(Sender: TObject);
    procedure AbmList1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure AbmList1Insert(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure AbmList1Delete(Sender: TObject);
    procedure AbmList1Edit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AbmList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure AbmList1Refresh(Sender: TObject);
  private
    { Private declarations }
    FFechaIVAVigente: TDateTime;
    FInicializando: Boolean;
    procedure CambiarAModo(Modo: integer);
    function ObtenerFechaIVAVigente:TDateTime;
  public
    { Public declarations }
    function inicializar:boolean;
  end;

const
    CONSULTA  = 0; // indica que la ventana esta en modo normal
    EDICION = 1; // indica que la ventana esta en modo edicion

var
  Form_ABM_IVA: TForm_ABM_IVA;

implementation

{$R *.dfm}

function TForm_ABM_IVA.Inicializar;
resourcestring
    ERROR_MSG = 'Error de Inicializacion';
Var
	S: TSize;
begin
    FInicializando := True;
try
    FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    CenterForm(Self);
    try
        FFechaIVAVigente :=  ObtenerFechaIVAVigente;
        tblIVA.Open;
        // Cambiamos a modo consulta a mano
        // para evitar la llamada al reload y el onClick (que estaria dehabilitado)
        // y se lo llama mas abajo.... en el finally
            deFechaInicio.Enabled := False;
            neTasaIva.Enabled := False;
            abmList1.Enabled := True;
            Notebook1.ActivePage := 'Salir';

        abmList1.Reload;
        Result := True;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_MSG,e.Message,'ERROR',MB_ICONSTOP) ;
            Result := False;
        end;
    end;
finally
     FInicializando := False;
     abmList1click(self);
end;
end;

procedure TForm_ABM_IVA.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TForm_ABM_IVA.AbmList1Click(Sender: TObject);
begin
if NOT FInicializando then begin

        deFechaInicio.Date := tblIVA.FieldByName('FechaInicio').AsDateTime;
        neTasaIVA.Value := (tblIVA.FieldByName('TasaIVA').AsInteger / 100 );

        if (deFechaInicio.Date > FFechaIVAVigente) then begin
            // Estoy en un registro que es una tasa futura. La puedo eliminar o editar.
                abmList1.Access := [accAlta, accBaja, accModi] ;
                abmToolbar1.Habilitados := [btAlta, btBaja, btModi];
        end
        else begin
            // Estoy en una tasa vigente o historica. No puedo modificar ni editar.
            abmList1.Access := [accAlta];
            abmToolbar1.Habilitados := [btAlta];
        end;
end;
end;


procedure TForm_ABM_IVA.CambiarAModo(Modo: integer);
begin
    if (Modo = CONSULTA) then begin
        deFechaInicio.Enabled := False;
        neTasaIva.Enabled := False;
        abmList1.Enabled := True;
        Notebook1.ActivePage := 'Salir';
        abmList1.Estado := Normal;
        abmList1.Reload;
        abmList1Click(Self);
    end
    else begin
        // Estoy en modo edicion. La ABMList esta deshabilitada. Solo puedo trabajar sobre
        // los Edit y los botones Aceptar y Cancelar.
        if abmList1.Estado = Alta then begin
            deFechaInicio.Date := NowBase(DMConnections.BaseCAC);
            neTasaIVA.Value := 0;
        end;
        deFechaInicio.Enabled := True;
        neTasaIva.Enabled := True;
        abmToolbar1.Habilitados := [];
        abmList1.Access := [] ;
        abmList1.Enabled := False;
        Notebook1.ActivePage := 'Editing';
        neTasaIVa.SetFocus ;
    end;

end;


procedure TForm_ABM_IVA.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
       CanClose := NOT ( neTasaIVA.Enabled );
end;

procedure TForm_ABM_IVA.AbmList1Insert(Sender: TObject);
begin
    CambiarAModo(EDICION);
end;

procedure TForm_ABM_IVA.btnCancelarClick(Sender: TObject);
begin
    // Estaba editando y decido cancelar
    tblIVA.Cancel ;
    CambiarAModo(CONSULTA);
end;

procedure TForm_ABM_IVA.btnAceptarClick(Sender: TObject);
resourcestring
    ERROR_INSERT    =   'No se ha insertado ning�n registro';
    ERROR_MODIFY    =   'No se ha modificado el registro';
    ERROR_TITLE     =   'ERROR';
    MSG_OVERWRITE    =   'Ya existe un tasa de IVA cargada para esa fecha. Desea modificarla?';
    TITLE_OVERWRITE =   'Confirme sobre escritura';
    ERR_EXISTE_FECHA =  'La fecha ingresada ya existe en otra tasa de IVA.'; // 20160608 MGO
var
    sErrorCaption: String;
    fFechaActual : TDateTime;
begin
    try
        // Fijo la fache actual (toma la fecha del server, con mlisegundos...)
        fFechaActual := NowBase(DMConnections.BaseCAC);
        // Valido los controles
        if ValidateControls([deFechaInicio, neTasaIVA],
           [(deFechaInicio.Date >= TRUNC(FFechaActual)),
            ((neTasaIVA.Value > 0) AND (neTasaIVA.Value <= 100 ))],
            'Datos Incorrectos',
           ['La Fecha m�nima es la actual',
            'La tasa de IVA debe ser positiva y menor o igual a 100%']) then begin
                    // Si los datos son validos
                    if abmList1.Estado = Alta then begin
                        // Antes de aceptar el alta, debo controlar
                        // que la fecha ingresada no concuerde con otro registro...

                        if (tblIVA.Locate( 'FechaInicio',deFechaInicio.Date,[])) AND (deFechaInicio.Date > FFechaActual) then begin
                            // Si existe pregunto si quiere sobreescribir...
                            if MsgBox(MSG_OVERWRITE,TITLE_OVERWRITE,MB_ICONQUESTION+MB_YESNO) = mrYes then begin
                                sErrorCaption := ERROR_MODIFY;
                                tblIVA.Edit;
                            end
                            else begin
                            // Si no quiere, salgo...
                                CambiarAModo(CONSULTA);
                                Exit;
                            end;
                        end
                        else begin
                        // Si estaba haciendo un alta, sigo...
                        sErrorCaption := ERROR_INSERT;
                        tblIVA.Insert  ;
                        end;
                    end
                    else begin
                        // Sino estaba haciendo una modificacion
                        sErrorCaption := ERROR_MODIFY;

						// INICIO : 20160608 MGO
		                // si la fecha se modific� y ya existe en el listado...
		                if (deFechaInicio.Date <> tblIVA.FieldByName('FechaInicio').AsDateTime)
		                    and tblIVA.Locate( 'FechaInicio',deFechaInicio.Date,[]) then begin
		                    // muestro un error indicando que la fecha ya existe
		                    MsgBox(ERR_EXISTE_FECHA, ERROR_TITLE, MB_ICONEXCLAMATION+MB_OK);
		                    CambiarAModo(CONSULTA);
		                    Exit;
		                end;
		                // FIN : 20160608 MGO

                        tblIVA.Edit;
                    end;
                    tblIVA.FieldByName('FechaInicio').value := iif( (TRUNC(deFechaInicio.Date) = TRUNC(FFechaActual) ),
                                                    FFechaActual,deFechaInicio.Date);
                    tblIVA.FieldByName('TasaIVA').Value := ROUND(neTasaIVA.Value *100);
                    // Blanqueo los errores de la connection
                    tblIVA.Connection.Errors.Clear;
                    // Guardo los cambios
                    tblIVA.Post;
                    // Actualizo la fecha de IVA Vigente
                    FFechaIVAVigente := ObtenerFechaIVAVigente;
                    CambiarAModo(CONSULTA);
        end else begin
                // Reseteo los valores porque no fueron validados...
                if (neTasaIVA.Value > 100) then
                    neTasaIVA.Value := 0;
                if deFechaInicio.Date < FFechaActual then
                    deFechaInicio.Date := FFechaActual;
        end;

    except
        // Se produjo un error....
        on e:exception do begin
            MsgBoxErr(sErrorCaption,e.Message,ERROR_TITLE,MB_ICONSTOP);
            if tblIVA.State <> dsBrowse then tblIVA.Cancel;
            CambiarAModo(CONSULTA);
        end;
    end;
end;

procedure TForm_ABM_IVA.AbmList1Delete(Sender: TObject);
resourcestring
    ERROR_DELETE = 'Error al intentar eliminar el registro';
    MSG_DELETE =   'Desea eliminar la Tasa de IVA del %s vigente a partir del %s ';
    TITLE_DELETE = 'Confirme Eliminaci�n';
begin
    try
        try
            // Solicito al usuario confirmacion de lo que quiere borrar
            if (MsgBox(Format(MSG_DELETE,[ (floatToStr((tblIVA.FieldByName('TasaIVA').asInteger) / 100) + '%')
                , tblIVA.FieldByName('FechaInicio').asString ]),TITLE_DELETE,MB_ICONQUESTION+MB_YESNO) = mrYes) then begin

                tblIVA.DeleteRecords(arCurrent);
                abmList1.Repaint;
                abmList1Click(Sender);
            end;
        except
            // Se produjo un error!
            on e:exception do begin
                MsgBoxErr(ERROR_DELETE,e.Message,'ERROR',MB_ICONSTOP);
            end;
        end;
    finally
        // Cambio a modo consulta (por las dudas)
        CambiarAModo(CONSULTA);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerFechaIVAVigente
  Author:
  Date Created:  /  /
  Description:
  Parameters: N/A
  Return Value: TDateTime

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
Function TForm_ABM_IVA.ObtenerFechaIVAVigente:TDateTime;
var
    sFecha: TDateTime;
    MyQry: TADOQuery;
begin
    try
        // Obtengo la fecha de IVA Vigente.

        // Hago esto con un Query porque la funcion QueryGetValueDateTime recupera
        // la fecha como cadena y lo convierte a DateTime... al convertir a cadena
        // pierde los decimales de los segundos... entonces no sirve para comparar...

        //Creo el Query
        MyQry := TADOQuery.Create(nil);
        MyQry.Connection := DMConnections.BaseCAC;
        MyQry.SQL.Text := 'SELECT MAX(FechaInicio) as FechaMax FROM IVA  WITH (NOLOCK) WHERE FechaInicio <= GetDate()';
        //Lo abro...
        MyQry.Open ;

        // Si la tabla esta vacia, devuelvo una fecha antigua...
        if MyQry.IsEmpty  then  sFecha := StrToDateTime('01/01/1900')
        else sFecha := MyQry.FieldByName('FechaMax').AsDateTime;
    except
        // Si falla... devuelvo un fecha antigua...
        sFecha := StrToDateTime('01/01/1900')
    end;
    Result := sFecha;
end;

procedure TForm_ABM_IVA.AbmList1Edit(Sender: TObject);
begin
// Entro en Modo Edicion
   CambiarAModo(EDICION);
end;

procedure TForm_ABM_IVA.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
// Cierro el formulario
    Action := caFree;
end;

procedure TForm_ABM_IVA.AbmList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
    // Decido como mostrar los datos en la tabla...
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        if  (Tabla.FieldByName('FechaInicio').AsDateTime = FFechaIVAVigente) then begin
            Font.Style := [];
            Font.Color := clRed;

        end;
        if  (Tabla.FieldByName('FechaInicio').AsDateTime > FFechaIVAVigente) then begin
            Font.Style := [];
            Font.Color := clWindowText;
        end;
        if  (Tabla.FieldByName('FechaInicio').AsDateTime < FFechaIVAVigente) then begin
            Font.Style := [];
            Font.Color := clGrayText;
        end;
      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('FechaInicio').AsString);
        TextOut(Cols[1], Rect.Top, ((Format('%f',[(Tabla.FieldByName('TasaIVA').AsInteger / 100)] )+ ' %' )));

	end;

end;

procedure TForm_ABM_IVA.AbmList1Refresh(Sender: TObject);
begin
	if tblIVA.IsEmpty then begin
        deFechaInicio.Date := Date;
        neTasaIVA.Value := 0;
    end;
end;

end.
