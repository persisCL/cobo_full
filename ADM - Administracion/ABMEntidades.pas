{-----------------------------------------------------------------------------
 Unit Name: ABMEntidades
 Author:  lgisuk
 Description: ABM de Entidades
-----------------------------------------------------------------------------}
unit ABMEntidades;

interface

uses
  //ABM Entidades
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants;

type
  TFABMEntidades = class(TForm)
	GroupB: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    Lcodigo: TLabel;
    Ldescripcion: TLabel;
    txtDescripcion: TEdit;
    tblEntidades: TADOTable;
    dsEntidades: TDataSource;
    spActualizarEntidades: TADOStoredProc;
    EliminarEntidad: TADOStoredProc;
    ListaEntidades: TAbmList;
    Ltipo: TLabel;
    txttipo: TEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure FormShow(Sender: TObject);
   	procedure ListaEntidadesRefresh(Sender: TObject);
   	function  ListaEntidadesProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure ListaEntidadesDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaEntidadesClick(Sender: TObject);
	procedure ListaEntidadesInsert(Sender: TObject);
	procedure ListaEntidadesEdit(Sender: TObject);
	procedure ListaEntidadesDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

var
  FABMEntidades: TFABMEntidades;

resourcestring
	STR_MAESTRO_Entidades	= 'Maestro de Entidades';


implementation

{$R *.DFM}

{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFABMEntidades.Inicializa:Boolean;
Var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([tblEntidades]) then exit;
	Notebook.PageIndex := 0;
	ListaEntidades.Reload;
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarCampos
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.LimpiarCampos;
begin
	txtCodigo.Clear;
	txtDescripcion.Clear;
    txttipo.clear;
end;


{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author:
  Date Created:
  Description: Actualizo la grilla
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.FormShow(Sender: TObject);
begin
	ListaEntidades.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEntidadesRefresh
  Author:
  Date Created:
  Description: lipio los campos si la tabla esta vacia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.ListaEntidadesRefresh(Sender: TObject);
begin
	 if ListaEntidades.Empty then LimpiarCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: Volver_Campos
  Author:
  Date Created:
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.Volver_Campos;
begin
	ListaEntidades.Estado := Normal;
	ListaEntidades.Enabled:= True;

	ActiveControl       := ListaEntidades;
	Notebook.PageIndex  := 0;
	groupb.Enabled      := False;
	ListaEntidades.Reload;
end;


{-----------------------------------------------------------------------------
  Function Name: ListaEntidadesDrawItem
  Author:
  Date Created:
  Description: Muestro los Registros
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.ListaEntidadesDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('CodigoEntidad').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
        TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('Tipo').AsString);
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEntidadesClick
  Author:
  Date Created:
  Description: Selecciono un campo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.ListaEntidadesClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		:= FieldByName('CodigoEntidad').AsInteger;
	   txtDescripcion.Text	:= FieldByname('Descripcion').AsString;
       txtTipo.Text	        := FieldByname('Tipo').AsString;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEntidadesProcess
  Author:
  Date Created:
  Description:
  Parameters: Tabla: TDataSet;var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFABMEntidades.ListaEntidadesProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEntidadesInsert
  Author:
  Date Created:
  Description: limpio los campos para que ingrese un nuevo registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.ListaEntidadesInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled          	:= True;
	ListaEntidades.Enabled := False;
	Notebook.PageIndex      	:= 1;
	ActiveControl           	:= txtDescripcion;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEntidadesEdit
  Author:
  Date Created:
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.ListaEntidadesEdit(Sender: TObject);
begin
	ListaEntidades.Enabled:= False;
	ListaEntidades.Estado := modi;

	Notebook.PageIndex := 1;
	groupb.Enabled     := True;

	ActiveControl:= txtDescripcion
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEntidadesDelete
  Author:
  Date Created:
  Description: elimino un item
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.ListaEntidadesDelete(Sender: TObject);
resourcestring
  MSG_EXISTEN_ALMACEN = 'Existen Almacen asociados a esta Entidad.';
  MSG_DELETE_CAPTION		   = 'Eliminar Entidad.';
begin
	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_MAESTRO_Entidades]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with EliminarEntidad do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoEntidad').Value := tblEntidades.FieldbyName('CodigoEntidad').Value;
                Parameters.ParamByName('@Resultado').Value := 0;
                ExecProc;
                if Parameters.ParamByName('@Resultado').Value = 1 then
                    MsgBox(MSG_EXISTEN_ALMACEN, MSG_DELETE_CAPTION, MB_ICONSTOP);
                close;
            end;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_MAESTRO_Entidades]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_MAESTRO_Entidades]), MB_ICONSTOP);
			end
		end
	end;

	ListaEntidades.Reload;
	ListaEntidades.Estado := Normal;
	ListaEntidades.Enabled:= True;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: Acepto los cambios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.BtnAceptarClick(Sender: TObject);

begin
	if not ValidateControls([txtDescripcion],
	  [(Trim(txtDescripcion.Text) <> '')],
	  Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_Entidades]),
	  [Format(MSG_VALIDAR_DEBE_LA,[FLD_DESCRIPCION])]) then begin
		Exit;
	end;

    if not((txttipo.Text = 'I') or (txttipo.Text = 'E')) then begin
        msgbox('Tipo No valido!');
        exit;
    end;

	with ListaEntidades do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarEntidades, Parameters do begin
                    refresh;
					ParamByName('@CodigoEntidad').Value := txtCodigo.ValueInt;
					ParamByName('@Descripcion').Value := Trim(txtDescripcion.Text);
                    ParamByName('@tipo').Value := txttipo.Text;
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_MAESTRO_Entidades]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_MAESTRO_Entidades]), MB_ICONSTOP);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:
  Date Created:
  Description: permito cancelar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:
  Date Created:
  Description: salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.AbmToolbarClose(Sender: TObject);
begin
	 close;
end;


{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:
  Date Created:
  Description:  salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.BtnSalirClick(Sender: TObject);
begin
	 close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:
  Date Created:
  Description: lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFABMEntidades.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 action := caFree;
end;




end.

