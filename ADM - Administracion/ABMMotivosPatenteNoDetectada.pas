{-----------------------------------------------------------------------------
 Unit Name: ABMMotivosPatenteNoDetectada
 Author:    ggomez
 Purpose: Permitir gestionar los motivos por los cuales una patente no fue
    detectada.
    S�lo permite hacer Modificaciones sobre los datos de la tabla
    MotivosPatenteNoDetectada.
 History:
-----------------------------------------------------------------------------}


unit ABMMotivosPatenteNoDetectada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Abm_obj, DmiCtrls, ComCtrls,
  DbList, DB, ADODB,
  DMConnection, UtilProc, StrUtils, UtilDB, RStrings, PeaProcs;

type
  TFormABMMotivosPatenteNoDetectada = class(TForm)
    AbmToolBar: TAbmToolbar;
    pnl_Bottom: TPanel;
    lbl_HintDeshabilitado: TLabel;
    nb_Botones: TNotebook;
    pnl_HintDeshabilitado: TPanel;
    dbl_Motivos: TAbmList;
    pnl_Datos: TPanel;
    Ldescripcion: TLabel;
    txt_Descripcion: TEdit;
    lbl_CodigoMotivo: TLabel;
    txt_CodigoMotivo: TNumericEdit;
    chb_UtilizarEnCambioDePatente: TCheckBox;
    chb_UtilizarEnPatenteNoIdentificable: TCheckBox;
    chb_Habilitado: TCheckBox;
    ds_MotivosPatenteNoDetectada: TDataSource;
    tbl_MotivosPatenteNoDetectada: TADOTable;
    sp_ActualizarMotivoPatenteNoDetectada: TADOStoredProc;
    btn_Salir: TButton;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure dbl_MotivosClick(Sender: TObject);
    procedure dbl_MotivosDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure dbl_MotivosEdit(Sender: TObject);
    procedure dbl_MotivosRefresh(Sender: TObject);
    procedure AbmToolBarClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimpiarCampos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: Boolean): Boolean;
  end;

var
  FormABMMotivosPatenteNoDetectada: TFormABMMotivosPatenteNoDetectada;

resourcestring
	STR_MAESTRO_MOTIVOSPATENTENODETECTADA = 'Maestro de Motivos de Patente No Detectada';

implementation

{$R *.dfm}

{ TFormABMMotivosPatenteNoDetectada }

procedure TFormABMMotivosPatenteNoDetectada.HabilitarCampos;
begin
	dbl_Motivos.Enabled    		:= False;
	nb_Botones.PageIndex 		:= 1;
    pnl_Datos.Enabled           := True;
    txt_CodigoMotivo.Enabled    := False;
    txt_Descripcion.SetFocus;
end;

function TFormABMMotivosPatenteNoDetectada.Inicializar(txtCaption: String;
    MDIChild: Boolean): Boolean;
var
	S: TSize;
begin
	Result := False;

	if MDIChild then begin
        FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	nb_Botones.PageIndex := 0;

   	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	if not OpenTables([tbl_MotivosPatenteNoDetectada]) then Exit;

    // Limpiar controles de ingreso de datos.
    LimpiarCampos;

    KeyPreview := True;
	dbl_Motivos.Reload;
   	nb_Botones.PageIndex := 0;
	Result := True;
end;

procedure TFormABMMotivosPatenteNoDetectada.LimpiarCampos;
begin
	txt_CodigoMotivo.Clear;
	txt_Descripcion.Clear;
    chb_Habilitado.Checked := False;
    chb_UtilizarEnCambioDePatente.Checked := False;
    chb_UtilizarEnPatenteNoIdentificable.Checked := False;
end;

procedure TFormABMMotivosPatenteNoDetectada.VolverCampos;
begin
	dbl_Motivos.Estado      := Normal;
	dbl_Motivos.Enabled    	:= True;
	dbl_Motivos.SetFocus;
	nb_Botones.PageIndex 	:= 0;
    pnl_Datos.Enabled       := False;
	dbl_Motivos.Reload;
end;

procedure TFormABMMotivosPatenteNoDetectada.btn_CancelarClick(
  Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormABMMotivosPatenteNoDetectada.dbl_MotivosClick(
  Sender: TObject);
begin
	with tbl_MotivosPatenteNoDetectada do begin
		txt_CodigoMotivo.Value	 := FieldByName('CodigoMotivoPatenteNoDetectada').AsInteger;
		txt_Descripcion.Text := FieldByName('Descripcion').AsString;
        chb_Habilitado.Checked := not FieldByName('BajaLogica').AsBoolean;
        chb_UtilizarEnCambioDePatente.Checked := FieldByName('AplicaCambioPatente').AsBoolean;
        chb_UtilizarEnPatenteNoIdentificable.Checked := FieldByName('AplicaNoIdentificable').AsBoolean;
	end;
end;

procedure TFormABMMotivosPatenteNoDetectada.dbl_MotivosDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	with Sender.Canvas do begin
    	FillRect(Rect);
        (* Si est� dado de Baja L�gica mostrarlo en Rojo*)
	    if Tabla.FieldByName('BajaLogica').AsBoolean then begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clYellow
            else
                Sender.Canvas.Font.Color := clRed;
        end else begin
            if odSelected in State then
                Sender.Canvas.Font.Color := clWindow
            else
                Sender.Canvas.Font.Color := clWindowText;
        end;

      	TextOut(Cols[0], Rect.Top, Tabla.FieldByName('CodigoMotivoPatenteNoDetectada').AsString);
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldByName('Descripcion').AsString));
	end;
end;

procedure TFormABMMotivosPatenteNoDetectada.dbl_MotivosEdit(
  Sender: TObject);
begin
	HabilitarCampos;
    dbl_Motivos.Estado := Modi;
end;

procedure TFormABMMotivosPatenteNoDetectada.dbl_MotivosRefresh(
  Sender: TObject);
begin
	if dbl_Motivos.Empty then LimpiarCampos;
end;

procedure TFormABMMotivosPatenteNoDetectada.AbmToolBarClose(
  Sender: TObject);
begin
    Close;
end;

procedure TFormABMMotivosPatenteNoDetectada.FormShow(Sender: TObject);
begin
    dbl_Motivos.Reload;
end;

procedure TFormABMMotivosPatenteNoDetectada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Action := caFree;
end;

procedure TFormABMMotivosPatenteNoDetectada.btn_AceptarClick(
  Sender: TObject);
resourcestring
    MSG_DESCRIPCION_EXISTENTE = 'La descripci�n ya existe para otro motivo.';
    CAPTION_TITULO = 'Actualizar Motivo';
begin
	if not ValidateControls([txt_Descripcion],
            [(Trim(txt_Descripcion.Text) <> EmptyStr)],
	        Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_MOTIVOSPATENTENODETECTADA]),
            [Format(MSG_VALIDAR_DEBE_LA, [FLD_DESCRIPCION])]) then begin
		Exit;
	end; // if

    (* Si estoy modificando y la Descripci�n es distinta al valor original,
        controlar que la Descripci�n NO exista en la tabla. *)
    if (dbl_Motivos.Estado = Modi)
            and (Trim(txt_Descripcion.Text) <> Trim(dbl_Motivos.Table.FieldByName('Descripcion').AsString))
        then begin
            if dbl_Motivos.Table.Locate('Descripcion', txt_Descripcion.Text, []) then begin
                MsgBox(MSG_DESCRIPCION_EXISTENTE, CAPTION_TITULO, MB_ICONSTOP);
                Exit;
            end;
    end; // if

	with dbl_Motivos do begin
		Screen.Cursor := crHourGlass;
		try
			try
                case Estado of
                    Modi:   begin
                                with sp_ActualizarMotivoPatenteNoDetectada, Parameters do begin
                                    ParamByName('@CodigoMotivo').Value := Table.FieldByName('CodigoMotivoPatenteNoDetectada').AsInteger;
                                    ParamByName('@Descripcion').Value := Trim(txt_Descripcion.Text);
                                    ParamByName('@BajaLogica').Value := not chb_Habilitado.Checked;
                                    ParamByName('@AplicaCambioPatente').Value := chb_UtilizarEnCambioDePatente.Checked;
                                    ParamByName('@AplicaNoIdentificable').Value := chb_UtilizarEnPatenteNoIdentificable.Checked;
                                    ExecProc;
                                end;
                            end;
                end; // case
			except
				on E: Exception do begin
					MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR, [STR_MAESTRO_MOTIVOSPATENTENODETECTADA]),
                        E.Message, Format(MSG_CAPTION_ACTUALIZAR, [STR_MAESTRO_MOTIVOSPATENTENODETECTADA]),
                        MB_ICONSTOP);
				end;
			end;
		finally
            VolverCampos;
			Screen.Cursor := crDefault;
            Reload;
		end;
	end; // with
end;

procedure TFormABMMotivosPatenteNoDetectada.btn_SalirClick(
  Sender: TObject);
begin
     Close;
end;

end.
