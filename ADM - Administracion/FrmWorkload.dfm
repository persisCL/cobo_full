object FormWorkload: TFormWorkload
  Left = 206
  Top = 160
  Width = 585
  Height = 478
  Caption = 'Workload'
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 480
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 383
    Width = 225
    Height = 13
    Caption = 'Tiempo medio de espera en el Call-Center (seg.)'
  end
  object Label2: TLabel
    Left = 4
    Top = 404
    Width = 201
    Height = 13
    Caption = 'Cantidad de '#243'rdenes de servicio a resolver'
  end
  object Label3: TLabel
    Left = 4
    Top = 425
    Width = 157
    Height = 13
    Caption = 'Cantidad de tr'#225'nsitos para validar'
  end
  object lbl_CC_Valor: TLabel
    Left = 246
    Top = 383
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '00:00'
  end
  object lbl_BO_Valor: TLabel
    Left = 246
    Top = 405
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0'
  end
  object lbl_VA_Valor: TLabel
    Left = 246
    Top = 427
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0'
  end
  object Label7: TLabel
    Left = 313
    Top = 360
    Width = 33
    Height = 13
    Caption = 'Umbral'
  end
  object Label8: TLabel
    Left = 246
    Top = 360
    Width = 57
    Height = 13
    Caption = 'Valor Actual'
  end
  object Label9: TLabel
    Left = 361
    Top = 360
    Width = 35
    Height = 13
    Caption = 'Desv'#237'o'
  end
  object Label10: TLabel
    Left = 414
    Top = 360
    Width = 44
    Height = 13
    Caption = 'Situaci'#243'n'
  end
  object lbl_CC_Max: TLabel
    Left = 302
    Top = 383
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '00:00'
  end
  object lbl_BO_Max: TLabel
    Left = 302
    Top = 405
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0'
  end
  object lbl_VA_Max: TLabel
    Left = 302
    Top = 427
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0'
  end
  object lbl_CC_Dev: TLabel
    Left = 350
    Top = 383
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '100%'
  end
  object lbl_BO_Dev: TLabel
    Left = 350
    Top = 405
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '100%'
  end
  object lbl_VA_Dev: TLabel
    Left = 350
    Top = 427
    Width = 43
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '100%'
  end
  object bv_separador: TBevel
    Left = 5
    Top = 376
    Width = 563
    Height = 2
  end
  object Bevel2: TBevel
    Left = 234
    Top = 356
    Width = 2
    Height = 89
  end
  object Bevel3: TBevel
    Left = 404
    Top = 355
    Width = 2
    Height = 89
  end
  object lbl_CC_Status: TLabel
    Left = 414
    Top = 383
    Width = 26
    Height = 13
    Caption = '100%'
  end
  object lbl_BO_Status: TLabel
    Left = 414
    Top = 405
    Width = 26
    Height = 13
    Caption = '100%'
  end
  object lbl_VA_Status: TLabel
    Left = 414
    Top = 427
    Width = 26
    Height = 13
    Caption = '100%'
  end
  object hg_CallCenter: THistogram
    Left = 0
    Top = 19
    Width = 571
    Height = 100
    Variables = <
      item
        Color = clYellow
      end>
  end
  object pnl_CallCenter: TPanel
    Left = 0
    Top = 0
    Width = 570
    Height = 18
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = ' Tiempo de Espera promedio en el Call-Center (segundos)'
    TabOrder = 1
  end
  object hg_Validacion: THistogram
    Left = 1
    Top = 256
    Width = 571
    Height = 100
    Variables = <
      item
        Color = clYellow
      end>
  end
  object hg_BackOffice: THistogram
    Left = 0
    Top = 136
    Width = 571
    Height = 100
    Variables = <
      item
        Color = clYellow
      end>
  end
  object pnl_BackOffice: TPanel
    Left = 0
    Top = 118
    Width = 570
    Height = 18
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = ' Cantidad de '#211'rdenes de Servicio a resolver'
    TabOrder = 2
  end
  object pnl_Validacion: TPanel
    Left = 0
    Top = 236
    Width = 570
    Height = 18
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = ' Cantidad de Tr'#225'nsitos a validar manualmente'
    TabOrder = 4
  end
  object qnt_callcenter: TPanel
    Left = 1
    Top = 20
    Width = 35
    Height = 23
    BevelOuter = bvNone
    Caption = '0'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
  object qnt_backoffice: TPanel
    Left = 1
    Top = 137
    Width = 35
    Height = 23
    BevelOuter = bvNone
    Caption = '0'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  object qnt_validacion: TPanel
    Left = 1
    Top = 257
    Width = 35
    Height = 23
    BevelOuter = bvNone
    Caption = '0'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object ObtenerWorkload: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerWorkload;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DataCallCenter'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DataBackOffice'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DataValidacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxCallCenter'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxBackOffice'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxValidacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 504
    Top = 400
  end
  object Timer: TTimer
    Enabled = False
    OnTimer = TimerTimer
    Left = 536
    Top = 400
  end
end
