object FormMotivosRestriccionAdhesion: TFormMotivosRestriccionAdhesion
  Left = 0
  Top = 0
  Caption = 'Mantenimiento Motivos Restricci'#243'n Adhesi'#243'n PA'
  ClientHeight = 539
  ClientWidth = 1007
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object abmtlbr1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 1007
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar, btImprimir]
    OnClose = abmtlbr1Close
  end
  object lstLista: TAbmList
    Left = 0
    Top = 33
    Width = 1007
    Height = 329
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'47'#0'C'#243'digo  '
      #0'181'#0'Descripci'#243'n                                       '
      #0'35'#0'Texto')
    HScrollBar = True
    RefreshTime = 10
    Table = MotivosRestriccionAdhesionPAK
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = lstListaClick
    OnDrawItem = lstListaDrawItem
    OnRefresh = lstListaRefresh
    OnInsert = lstListaInsert
    OnDelete = lstListaDelete
    OnEdit = lstListaEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = abmtlbr1
  end
  object pnl1: TPanel
    Left = 0
    Top = 362
    Width = 1007
    Height = 138
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    object lbl1: TLabel
      Left = 10
      Top = 14
      Width = 39
      Height = 13
      Caption = 'C'#243'digo: '
      FocusControl = edtCodigoMotivoRestriccion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbl2: TLabel
      Left = 10
      Top = 41
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      FocusControl = edtDescripcion
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTexto: TLabel
      Left = 10
      Top = 68
      Width = 103
      Height = 26
      Caption = 'Texto Descriptivo:'#13#10
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtCodigoMotivoRestriccion: TNumericEdit
      Left = 119
      Top = 10
      Width = 107
      Height = 21
      TabStop = False
      Color = clBtnFace
      MaxLength = 4
      ReadOnly = True
      TabOrder = 0
    end
    object edtDescripcion: TEdit
      Left = 119
      Top = 37
      Width = 530
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 1
    end
    object mmoTexto: TMemo
      Left = 119
      Top = 64
      Width = 530
      Height = 68
      Color = 16444382
      Lines.Strings = (
        'mmoTexto')
      TabOrder = 2
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 500
    Width = 1007
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object pnl3: TPanel
      Left = 685
      Top = 0
      Width = 322
      Height = 39
      Align = alRight
      Alignment = taRightJustify
      BevelOuter = bvNone
      TabOrder = 0
      object Notebook: TNotebook
        Left = 126
        Top = 6
        Width = 197
        Height = 37
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object btnBtnSalir: TButton
            Left = 116
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = btnBtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object btnBtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = btnBtnAceptarClick
          end
          object btnBtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = btnBtnCancelarClick
          end
        end
      end
    end
  end
  object MotivosRestriccionAdhesionPAK: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'MotivosRestriccionAdhesionPAK'
    Left = 316
    Top = 80
  end
end
