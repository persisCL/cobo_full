unit ABMDiaDefault;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DPSControls;

type
  TFormDiaDefault = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label15: TLabel;
    Panel2: TPanel;
    Notebook: TNotebook;
    TiposDiaDefault: TADOTable;
    txt_DiaSemana: TNumericEdit;
    Label1: TLabel;
    txt_Descripcion: TEdit;
    Label2: TLabel;
    cb_TiposDia: TComboBox;
    qry_TiposDia: TADOQuery;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormDiaDefault: TFormDiaDefault;

implementation

uses DMConnection;
resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se pudo actualizar el Tipo de D�a.';
    MSG_ACTUALIZAR_CAPTION 	= 'Actualizar Tipo de D�a';
    
{$R *.DFM}

function TFormDiaDefault.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Result := False;
	if not OpenTables([TiposDiadefault, Qry_TiposDia]) then exit;
	cb_TiposDia.Items.Clear;
	While not Qry_TiposDia.Eof do begin
		cb_TiposDia.Items.Add(
			PadR(Qry_TiposDia.FieldByName('Descripcion').AsString, 200, ' ') +
			Trim(Qry_TiposDia.FieldByName('TipoDia').AsString));
		Qry_TiposDia.Next;
    end;
    Qry_TiposDia.Close;
	DbList1.Reload;
    Notebook.PageIndex := 0;
   	Result := True;
end;

procedure TFormDiaDefault.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormDiaDefault.DBList1Click(Sender: TObject);
var
	i: integer;
begin
	with (Sender AS TDbList).Table do begin
		txt_DiaSemana.Value		:= FieldByName('DiaSemana').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
        cb_TiposDia.ItemIndex := -1;
        for i := 0 to cb_TiposDia.Items.Count - 1 do begin
            if Trim(Copy(cb_TiposDia.Items[i], 201, 2)) =  Trim(FieldByName('TipoDia').AsString) then begin
                cb_TiposDia.ItemIndex := i;
                Break;
            end;
        end;
	end;
end;

procedure TFormDiaDefault.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName('DiaSemana').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
		TextOut(Cols[2], Rect.Top, QueryGetValue(TiposDiaDefault.Connection,
                	            'Select Descripcion From TIPOSDIA WITH (NOLOCK) where TipoDia = ''' +
                                Trim(Tabla.FieldbyName('TipoDia').AsString)+ ''''));
	end;
end;

procedure TFormDiaDefault.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    := False;
    dblist1.Estado     := modi;
	Notebook.PageIndex := 1;
    groupb.Enabled     := True;
    txt_DiaSemana.Enabled  := False;
    txt_DiaSemana.ReadOnly := True;
    cb_TiposDia.SetFocus;
end;

procedure TFormDiaDefault.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormDiaDefault.Limpiar_Campos();
begin
	txt_DiaSemana.Clear;
	cb_TiposDia.ItemIndex := 0;
	txt_Descripcion.Clear;
end;

procedure TFormDiaDefault.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormDiaDefault.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			Edit;
			FieldByName('TipoDia').AsString			:= Trim(Copy(Cb_TiposDia.text, 201, 10));
			FieldByName('Descripcion').AsString		:= Trim(txt_Descripcion.text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormDiaDefault.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormDiaDefault.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormDiaDefault.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormDiaDefault.Volver_Campos;
begin
	DbList1.Estado     		:= Normal;
	DbList1.Enabled    		:= True;
    DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex 		:= 0;
    txt_DiaSemana.Enabled  	:= True;
    txt_DiaSemana.ReadOnly 	:= False;
    groupb.Enabled     := False;
end;

end.
