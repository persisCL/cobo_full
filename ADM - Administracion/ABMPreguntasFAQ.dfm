object frmABMPreguntasFAQ: TfrmABMPreguntasFAQ
  Left = 136
  Top = 60
  Width = 817
  Height = 678
  Caption = 'Mantenimiento FAQs'
  Color = clBtnFace
  Constraints.MinHeight = 380
  Constraints.MinWidth = 725
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panABM: TPanel
    Left = 0
    Top = 0
    Width = 809
    Height = 299
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object abmPreguntas: TAbmToolbar
      Left = 0
      Top = 0
      Width = 809
      Height = 34
      Hint = 'Para insertar, debe seleccionar la opcion de filtro "Todas"'
      Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
      OnClose = AbmToolbar1Close
    end
    object dblPreguntas: TAbmList
      Left = 0
      Top = 34
      Width = 809
      Height = 265
      TabStop = True
      TabOrder = 1
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      SubTitulos.Sections = (
        #0'53'#0'C'#243'digo    '
        #0'64'#0'Descripci'#243'n')
      HScrollBar = True
      RefreshTime = 100
      Table = PreguntasFAQ
      Style = lbOwnerDrawFixed
      ItemHeight = 14
      OnClick = dblPreguntasClick
      OnProcess = dblPreguntasProcess
      OnDrawItem = dblPreguntasDrawItem
      OnRefresh = dblPreguntasRefresh
      OnInsert = dblPreguntasInsert
      OnDelete = dblPreguntasDelete
      OnEdit = dblPreguntasEdit
      Access = [accAlta, accBaja, accModi]
      Estado = Normal
      ToolBar = abmPreguntas
    end
    object cbFuentesSolicitud: TComboBox
      Left = 481
      Top = 7
      Width = 217
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnSelect = rgFiltroClick
    end
    object rgFiltro: TRadioGroup
      Left = 192
      Top = 0
      Width = 283
      Height = 31
      Hint = 'Para insertar, debe seleccionar la opcion de filtro "Todas"'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Mostrar todas'
        'Filtrar para la selecci'#243'n')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = rgFiltroClick
      OnEnter = rgFiltroClick
    end
  end
  object panEdicion: TPanel
    Left = 0
    Top = 299
    Width = 809
    Height = 345
    Align = alBottom
    Constraints.MinHeight = 345
    TabOrder = 1
    object GroupB: TPanel
      Left = 1
      Top = 1
      Width = 807
      Height = 302
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      DesignSize = (
        807
        302)
      object Label15: TLabel
        Left = 18
        Top = 12
        Width = 79
        Height = 13
        Caption = 'C'#243'digo Pregunta'
      end
      object Label2: TLabel
        Left = 18
        Top = 63
        Width = 65
        Height = 13
        Caption = '&Respuesta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 18
        Top = 39
        Width = 72
        Height = 13
        Caption = '&Descripci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 18
        Top = 246
        Width = 103
        Height = 13
        Caption = 'Archivo asociado:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Imagen: TImage
        Left = 692
        Top = 141
        Width = 105
        Height = 96
        Anchors = [akRight, akBottom]
        Stretch = True
        Visible = False
      end
      object Label3: TLabel
        Left = 18
        Top = 270
        Width = 98
        Height = 13
        Caption = 'Imagen Asociada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_CodigoPregunta: TNumericEdit
        Left = 114
        Top = 9
        Width = 105
        Height = 21
        TabStop = False
        Color = clBtnFace
        MaxLength = 3
        ReadOnly = True
        TabOrder = 0
        Decimals = 0
      end
      object txt_Descripcion: TEdit
        Left = 114
        Top = 36
        Width = 682
        Height = 21
        Anchors = [akLeft, akTop, akRight, akBottom]
        Color = 16444382
        MaxLength = 255
        TabOrder = 1
        Text = 'Descripcion'
      end
      object cbArchivos: TComboBox
        Left = 123
        Top = 243
        Width = 469
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        Items.Strings = (
          'Seleccionar')
      end
      object cbImagenes: TComboBox
        Left = 123
        Top = 267
        Width = 469
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        OnChange = cbImagenesChange
        Items.Strings = (
          'Seleccionar')
      end
      object memRespuesta: TEBEditor
        Left = 18
        Top = 81
        Width = 666
        Height = 154
        Anchors = [akLeft, akTop, akRight]
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 303
      Width = 807
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Notebook: TNotebook
        Left = 610
        Top = 0
        Width = 197
        Height = 39
        Align = alRight
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'PageSalir'
          object BtnSalir: TButton
            Left = 110
            Top = 6
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Salir'
            TabOrder = 0
            OnClick = BtnSalirClick
          end
        end
        object TPage
          Left = 0
          Top = 0
          HelpContext = 1
          Caption = 'PageModi'
          object BtnAceptar: TButton
            Left = 24
            Top = 7
            Width = 79
            Height = 26
            Caption = '&Aceptar'
            Default = True
            TabOrder = 0
            OnClick = BtnAceptarClick
          end
          object BtnCancelar: TButton
            Left = 111
            Top = 7
            Width = 79
            Height = 26
            Cancel = True
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BtnCancelarClick
          end
        end
      end
      object btnDuplicar: TButton
        Left = 6
        Top = 9
        Width = 75
        Height = 25
        Caption = '&Duplicar'
        TabOrder = 1
        OnClick = btnDuplicarClick
      end
    end
  end
  object PreguntasFAQ: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Filtered = True
    TableName = 'VW_FAQPreguntasFuentes'
    Left = 219
    Top = 84
  end
  object qry_MaxPregunta: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT ISNULL(MAX(CodigoPregunta),0) AS CodigoPregunta FROM FAQP' +
        'reguntas  WITH (NOLOCK) ')
    Left = 252
    Top = 84
  end
end
