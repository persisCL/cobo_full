{-----------------------------------------------------------------------------
  Function Name: ABMModelos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ABMModelos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, VariantComboBox, Variants,
  BuscaTab;

type
  TFormModelos = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Notebook: TNotebook;
    Modelos: TADOTable;
    Label15: TLabel;
    txt_CodigoModelo: TNumericEdit;
    qry_MaxModelo: TADOQuery;
    Label2: TLabel;
    cb_TipoVehiculo: TVariantComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    CodigoMarca: integer;
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
    { Public declarations }
	function Inicializa(Marca: integer; Descripcion: AnsiString; MDIChild: Boolean): boolean;
  end;

var
  FormModelos: TFormModelos;

implementation

resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Modelo seleccionado?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar el Modelo seleccionado.';
    MSG_DELETE_CAPTION		= 'Eliminar Modelo';
    MSG_ACTUALIZAR_ERROR	= 'No se pudieron actualizar los datos del Modelo seleccionado.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Modelo';

{$R *.DFM}

function TFormModelos.Inicializa(Marca: integer; Descripcion: AnsiString; MDIChild: Boolean): boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		BorderStyle := bsDialog;
		Width := Screen.Width div 2;
		Height := Screen.Height div 2;
		CenterForm(Self);
	end;
    Result := False;
    FormModelos.Caption :=  FormModelos.Caption + ': ' + Descripcion;
    CodigoMarca	:= Marca;
    Modelos.Filter := 'CodigoMarca = ' + IntToStr(Marca);
	if not OpenTables([Modelos]) then exit;
    CargarVehiculosTipos(DMConnections.BaseCAC, cb_TipoVehiculo);
    Notebook.PageIndex := 0;
    DBList1.Reload;
   	Result := True;
end;

procedure TFormModelos.BtnCancelarClick(Sender: TObject);
begin
   	Volver_Campos;
end;

procedure TFormModelos.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	    txt_CodigoModelo.Value	:= FieldByName('CodigoModelo').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
        MarcarItemComboVariant(cb_TipoVehiculo, FieldByName('TipoVehiculo').AsInteger);
	end;
end;

{-----------------------------------------------------------------------------
  Function Name:  DBList1DrawItem
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TDBList; Tabla: TDataSet;
              Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: N/A

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormModelos.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var
    DescTipoVehiculo: AnsiString;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr( Tabla.FieldbyName('CodigoModelo').AsInteger, 10));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));

        if (Tabla.FieldByName('TipoVehiculo').AsString <> '') then
            DescTipoVehiculo := QueryGetValue(DMConnections.BaseCAC,
                                'SELECT Descripcion FROM VehiculosTipos  WITH (NOLOCK) ' +
                                'WHERE CodigoTipoVehiculo = ' +
                                Tabla.FieldByName('TipoVehiculo').AsString)
        else DescTipoVehiculo := '';
        TextOut(Cols[2], Rect.Top, DescTipoVehiculo);
	end;
end;

procedure TFormModelos.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
    groupb.Enabled     		:= True;
    txt_CodigoModelo.Enabled	:= False;
   	txt_CodigoModelo.color := clBtnFace;
    txt_Descripcion.setFocus;
end;

procedure TFormModelos.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormModelos.Limpiar_Campos();
begin
	txt_CodigoModelo.Clear;
	txt_Descripcion.Clear;
    cb_TipoVehiculo.ItemIndex := -1;
end;

procedure TFormModelos.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormModelos.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			(Sender AS TDbList).Table.Delete;
		Except
			On E: Exception do begin
				(Sender AS TDbList).Table.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
 	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormModelos.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    txt_CodigoModelo.Enabled := False;
	txt_CodigoModelo.color := clBtnFace;
	txt_Descripcion.SetFocus;
    cb_TipoVehiculo.ItemIndex := 0;
end;

procedure TFormModelos.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With DbList1.Table do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                qry_MaxModelo.Parameters.ParamByName('CodigoMarca').Value := CodigoMarca;
                qry_MaxModelo.Open;
                txt_CodigoModelo.Value	:=  qry_MaxModelo.FieldByNAme('CodigoModelo').AsInteger + 1;
				qry_MaxModelo.Close;
			end else begin
            	Edit;
            end;
            FieldByName('CodigoMarca').AsInteger 	:= CodigoMarca;
			FieldByName('CodigoModelo').AsInteger	:= Trunc(txt_CodigoModelo.Value);
			if Trim(txt_Descripcion.text) = '' then
            	FieldByName('Descripcion').Clear
            else
				FieldByName('Descripcion').AsString	 := Trim(txt_Descripcion.text);

            //El item 0 es "(Sin especificar)"
            if (cb_TipoVehiculo.ItemIndex = 0) then
                FieldByName('TipoVehiculo').Clear
            else
                FieldByName('TipoVehiculo').AsString := iif(cb_TipoVehiculo.ItemIndex = 0, null,
                                                        IntToStr(cb_TipoVehiculo.value));
			Post;
		except
			On E: Exception do begin
                Screen.Cursor := crDefault;
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Volver_Campos;
	Screen.Cursor := crDefault;
end;

procedure TFormModelos.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormModelos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormModelos.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormModelos.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
    DBList1.Reload;
	DbList1.SetFocus;
    txt_CodigoModelo.Enabled := True;
   	txt_CodigoModelo.color := clWindow;
	Notebook.PageIndex := 0;
    groupb.Enabled     := False;
end;

end.
