{-----------------------------------------------------------------------------
 Unit Name: MotivosDevolucionComp.pas
 Author:    mtraversa
 Date Created: 25/01/2005
 Language: ES-AR
 Description: Formulario para la carga de los motivos de devolucion de las
              cartas (comprobantes) por parte del correo

  Revision 1:
  Date: 19/02/2009
  Author: mpiazza
  Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
    los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}

unit MotivosDevolucionComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaTypes,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, ListBoxEx, DBListEx, Variants,
  RStrings;

type
  TfrmMotivosDevComp = class(TForm)
	GroupB: TPanel;
	Panel2: TPanel;
	Notebook: TNotebook;
    tblMotivosDevolucion: TADOTable;
	Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
	Label2: TLabel;
    Label3: TLabel;
    spActualizarDevolucionComprobantesCorreo: TADOStoredProc;
	Label10: TLabel;
    ListaMotivos: TAbmList;
    txtDetalle: TEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
	procedure BtnCancelarClick(Sender: TObject);
	procedure ListaMotivosClick(Sender: TObject);
	procedure ListaMotivosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaMotivosEdit(Sender: TObject);
	procedure ListaMotivosRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure ListaMotivosDelete(Sender: TObject);
	procedure ListaMotivosInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function ListaMotivosProcess(Tabla: TDataSet; var Texto: String): Boolean;
  private
	{ Private declarations }
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;

var
  frmMotivosDevComp: TfrmMotivosDevComp;

implementation

{$R *.DFM}

function TfrmMotivosDevComp.Inicializar:Boolean;
Var
	S: TSize;
begin
	Result := False;

	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);

	if not OpenTables([tblMotivosDevolucion]) then exit;
	Notebook.PageIndex := 0;
	ListaMotivos.Reload;

	Result := True;
end;

procedure TfrmMotivosDevComp.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TfrmMotivosDevComp.ListaMotivosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do
	   txtDetalle.Text := FieldByName('Detalle').AsString;
end;

procedure TfrmMotivosDevComp.ListaMotivosDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('Detalle').AsString));
	end;
end;

procedure TfrmMotivosDevComp.ListaMotivosEdit(Sender: TObject);
begin
	ListaMotivos.Enabled := False;
	ListaMotivos.Estado  := modi;
    Notebook.PageIndex   := 1;
	groupb.Enabled       := True;
    ActiveControl        := txtDetalle;
end;

procedure TfrmMotivosDevComp.ListaMotivosRefresh(Sender: TObject);
begin
	 if ListaMotivos.Empty then LimpiarCampos;
end;

procedure TfrmMotivosDevComp.LimpiarCampos;
begin
	txtDetalle.Clear;
end;

procedure TfrmMotivosDevComp.AbmToolbar1Close(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaMotivosDelete
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 20/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TfrmMotivosDevComp.ListaMotivosDelete(Sender: TObject);

    function MotivoUtilizado: Boolean;
    var
        Codigo: ShortString;
    begin
        //Se fija si el registro actual tiene registros relacionados en ComprobantesDevueltosCorreo

        Codigo := tblMotivosDevolucion.FieldByName('CodigoMotivoDevolucion').AsString;
        Result := (QueryGetValueInt(DMConnections.BaseCAC,
                                    'SELECT TOP 1 CodigoMotivoDevolucion FROM ComprobantesDevueltosCorreo  WITH (NOLOCK) ' +
                                    'WHERE CodigoMotivoDevolucion = ' + Codigo)) > 0;
    end;

resourcestring
    MSG_COMNFIRM_DELETE = 'Desea Eliminar El Motivo de Devolución de Cartas?';
    MSG_DELETE          = 'Eliminar';
    MSG_ERROR           = 'Error';
    MSG_CAN_NOT_DELETE  = 'No se puede eliminar el Motivo de Devolución ya que posee cartas devueltas relacionadas';
begin
    //Si el registro posee registros relacionandos en la tabla ComprobantesDevueltosCorreo no dejo eliminarlo
    if MotivoUtilizado then begin
        MsgBox(MSG_CAN_NOT_DELETE);
        ListaMotivos.Estado := Normal;
        Exit;
    end;

	Screen.Cursor := crHourGlass;

	if (MsgBox(MSG_COMNFIRM_DELETE, MSG_DELETE, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin
		try
            QueryExecute(DMConnections.BaseCAC,
                         'DELETE FROM MotivosDevolucionComprobantesCorreo ' +
                         'WHERE CodigoMotivoDevolucion = ' + IntToStr(tblMotivosDevolucion.FieldByName('CodigoMotivoDevolucion').AsInteger));
		except
			On E: Exception do
				MsgBoxErr(MSG_ERROR, E.message, MSG_ERROR, MB_ICONSTOP);
		end
	end;
	ListaMotivos.Reload;
	ListaMotivos.Estado  := Normal;
	ListaMotivos.Enabled := True;
	Notebook.PageIndex   := 0;
	Screen.Cursor        := crDefault;
end;

procedure TfrmMotivosDevComp.ListaMotivosInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled       := True;
	ListaMotivos.Enabled := False;
	Notebook.PageIndex   := 1;
	ActiveControl        := txtDetalle;
end;

procedure TfrmMotivosDevComp.BtnAceptarClick(Sender: TObject);
begin
	with ListaMotivos do begin
		Screen.Cursor := crHourGlass;
		try
			try
				with spActualizarDevolucionComprobantesCorreo, Parameters do begin
                    if ListaMotivos.Estado = Alta then
                        ParamByName('@Accion').Value := 0
                    else
                        ParamByName('@Accion').Value := 1;
                    ParamByName('@Codigo').Value := tblMotivosDevolucion.FieldByName('CodigoMotivoDevolucion').AsInteger;
                    ParamByName('@Detalle').Value := Trim(txtDetalle.Text);
					ExecProc;
				end;
			except
				On E: EDataBaseError do begin
					MsgBoxErr('Error', e.message, 'Error', MB_ICONERROR);
				end;
			end;
		finally
            Volver_Campos;
			Screen.Cursor:= crDefault;
            Reload;
		end;
	end;
end;

procedure TfrmMotivosDevComp.FormShow(Sender: TObject);
begin
	ListaMotivos.Reload;
end;

procedure TfrmMotivosDevComp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmMotivosDevComp.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TfrmMotivosDevComp.Volver_Campos;
begin
	ListaMotivos.Estado  := Normal;
	ListaMotivos.Enabled := True;
	ActiveControl        := ListaMotivos;
	Notebook.PageIndex   := 0;
	groupb.Enabled       := False;
	ListaMotivos.Reload;
end;

function TfrmMotivosDevComp.ListaMotivosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Result := True;
end;


end.

