object FrmAgregarCategoriaDayPass: TFrmAgregarCategoriaDayPass
  Left = 296
  Top = 307
  BorderStyle = bsDialog
  Caption = 'Agregar Categor'#237'a'
  ClientHeight = 183
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  DesignSize = (
    500
    183)
  PixelsPerInch = 96
  TextHeight = 13
  object dbCategoria: TDBListEx
    Left = 8
    Top = 8
    Width = 521
    Height = 137
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 70
        Header.Caption = 'Categor'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Categoria'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Descripci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Descripcion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 300
        Header.Caption = 'Detalle'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Comentarios'
      end>
    DataSource = dsObtenerCategorias
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
    OnDblClick = BtnAceptarClick
  end
  object BtnAceptar: TButton
    Left = 331
    Top = 151
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 0
    OnClick = BtnAceptarClick
  end
  object BtnCancelar: TButton
    Left = 411
    Top = 151
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object spObtenerCategorias: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCategorias'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 96
    Top = 88
  end
  object dsObtenerCategorias: TDataSource
    DataSet = spObtenerCategorias
    Left = 40
    Top = 88
  end
end
