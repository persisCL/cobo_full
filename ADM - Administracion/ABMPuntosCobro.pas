unit ABMPuntosCobro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, ExtCtrls, StdCtrls, Buttons, ImgList,
  ToolWin, ComCtrls, UtilProc, Util, DBClient, DBCtrls, Mask, Provider, StrUtils, DSIntf, XMLIntf, XMLDoc,
  DmiCtrls, VariantComboBox, UtilDB, PeaProcs;                                  // TASK_153_MGO_20170315

                   
resourcestring
	MSG_ERROR_UPDATE = 'Error Actualizando Registro';
 	MSG_ERROR_INSERT = 'Error Agregando Registro';
  	MSG_ERROR_DELETE = 'Error Eliminando Registro';
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
    HINT_SALIR       = 'Salir del ABM';
    HINT_AGREGAR     = 'Agregar Punto de Cobro';
    HINT_ELIMINAR    = 'Eliminar Punto de Cobro';
    HINT_EDITAR      = 'Editar Punto de Cobro';
    ANSW_ELIMINAR    = 'Est� seguro de eliminar este Punto de Cobro? - ';
    ANSW_SALIR       = 'Confirma que desea salir?';

    PARAMS_IGNORE    = '@TSMCKEY, @DomainTSMC, @NumeroPuntoCobroNew, @NumeroPuntoCobro, @IdCategoriaClase, @UsuarioCreacion, @FechaHoraCreacion, @UsuarioModificacion, @FechaHoraModificacion, ';   // TASK_153_MGO_20170315
    FIELDS_TINYINT   = 'NumeroPuntoCobro, CodigoEjeVial, TSID, PuntoCobroAnterior, CantidadCarriles, CodigoPCConcesionaria, ';
    FIELDS_SMALLNT   = 'DomainID, CodigoEsquema, CodigoTipoPuntoCobro';         // TASK_153_MGO_20170315

    PROCEDURE_SELECT = 'ADM_PuntosCobro_SELECT';
    PROCEDURE_INSERT = 'ADM_PuntosCobro_INSERT';
    PROCEDURE_UPDATE = 'ADM_PuntosCobro_UPDATE';
    PROCEDURE_DELETE = 'ADM_PuntosCobro_DELETE';

    CAPTION_FORM     = 'ABM de Puntos de Cobro';





type

  TClientDataSetAccess = Class(TClientDataSet);
  TfrmABMPuntosCobro = class(TForm)
    procSelect: TADOStoredProc;
    dsGrid: TDataSource;
    Grid: TDBGrid;
    pnlSuperior: TPanel;
    pnlControles: TPanel;
    pnlInferior: TPanel;
    Botonera: TToolBar;
    btnSalir: TToolButton;
    ImageList1: TImageList;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    ToolButton8: TToolButton;
    btnBuscar: TToolButton;
    btnGuardar: TButton;
    btnCancelar: TButton;
    procInsert: TADOStoredProc;
    procDelete: TADOStoredProc;
    procUpdate: TADOStoredProc;
    CDS: TClientDataSet;
    qryTSMC: TADOQuery;
    pg01: TPageControl;
    tabPag02: TTabSheet;
    tabGeneral: TTabSheet;
    dsTSMC: TDataSource;
    qryPBL: TADOQuery;
    dsPBL: TDataSource;
    lbl01: TLabel;
    lbl02: TLabel;
    lbl03: TLabel;
    lbl07: TLabel;
    lbl08: TLabel;
    edNumeroPunto: TDBEdit;
    edDomainTSMCID: TDBLookupComboBox;
    procSelectNumeroPuntoCobro: TWordField;
    procSelectDomainID: TSmallintField;
    procSelectTSMCID: TWordField;
    procSelectDescripcion: TStringField;
    procSelectCodigoEjeVial: TWordField;
    procSelectSentido: TStringField;
    procSelectTSID: TWordField;
    procSelectPrefijoImagen: TStringField;
    procSelectPuntoCobroAnterior: TWordField;
    procSelectCantidadCarriles: TWordField;
    procSelectUltimoPorticoSentido: TBooleanField;
    procSelectCodigoJuzgado: TIntegerField;
    procSelectKms: TBCDField;
    procSelectUsuarioCreacion: TStringField;
    procSelectFechaHoraCreacion: TDateTimeField;
    procSelectUsuarioModificacion: TStringField;
    procSelectFechaHoraModificacion: TDateTimeField;
    procSelectEsMLFF: TBooleanField;
    cdsPDSelectClaseCategoria: TIntegerField;
    edDescripcion: TDBEdit;
    qryEjesViales: TADOQuery;
    dsEjesViales: TDataSource;
    edEjeVial: TDBLookupComboBox;
    lbl04: TLabel;
    qrySentido: TADOQuery;
    dsSentido: TDataSource;
    edSentido: TDBLookupComboBox;
    lbl05: TLabel;
    edTSMCID: TDBEdit;
    lbl06: TLabel;
    edPrefijoImagen: TDBEdit;
    edPuntoCobroAnterior: TDBLookupComboBox;
    qryPuntoCobroAnt: TADOQuery;
    dsPuntoCobroAnt: TDataSource;
    edUltimoPortico: TDBCheckBox;
    lbl12: TLabel;
    edCodigoJuzgado: TDBEdit;
    lbl13: TLabel;
    edKms: TDBEdit;
    dbedtCantidadCarriles: TDBEdit;
    lbl14: TLabel;
    tsCarriles: TTabSheet;
    dbgrdBandasHorarias: TDBGrid;
    bvl1: TBevel;
    lbl1: TLabel;
    edtOrden: TEdit;
    lbl2: TLabel;
    txtPosicionDesde: TNumericEdit;
    lbl3: TLabel;
    txtPosicionHasta: TNumericEdit;
    btnGenerarCarriles: TButton;
    btnAplicarCarril: TButton;
    cdsCarriles: TClientDataSet;
    dtstprCarriles: TDataSetProvider;
    spADM_Carril_SELECT: TADOStoredProc;
    lbl4: TLabel;
    edCodigoEsquema: TDBLookupComboBox;
    qryEsquemas: TADOQuery;
    dsEsquemas: TDataSource;
    procSelectCodigoEsquema: TIntegerField;
    dsCarriles: TDataSource;
    edtDescripcionCarril: TEdit;
    lbl5: TLabel;
    dsTipos: TDataSource;
    lbl6: TLabel;
    edCodigoTipoPuntoCobro: TDBLookupComboBox;
    edCodigoPCConcesionaria: TDBEdit;
    lbl7: TLabel;
    smlntfldSelectCodigoTipoPuntoCobro: TSmallintField;
    wrdfldSelectCodigoPCConcesionaria: TWordField;
    qryTipos: TADOQuery;
    qryProveedores: TADOQuery;
    dsProveedores: TDataSource;
    lbl8: TLabel;
    edProveedor: TDBLookupComboBox;
    procSelectProveedor: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure HabilitaBotones(Botones : string);
    function TraeRegistros : Boolean;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    function Inicializar(MDIChild: Boolean): Boolean;
    //procedure PonerFocoEnPrimerControl;                                       // TASK_153_MGO_20170315
    procedure ValidarTextoDeControl(Sender: TField; const Text: string);
    procedure edTSMCIDClick(Sender: TObject);
    procedure HabilitarDeshabilitarControles(Estado : Boolean);
    procedure CDSAfterScroll(DataSet: TDataSet);
    function GetCamposPK : string;
    procedure CargarCarriles;                                                   // TASK_114_MGO_20170117
    procedure btnGenerarCarrilesClick(Sender: TObject);                         // TASK_114_MGO_20170117
    procedure cdsCarrilesAfterScroll(DataSet: TDataSet);                        // TASK_114_MGO_20170117
    procedure btnAplicarCarrilClick(Sender: TObject);                           // TASK_114_MGO_20170117
    //procedure edMLFFClick(Sender: TObject);                                   // TASK_153_MGO_20170315  // TASK_114_MGO_20170117
    procedure HabilitarDeshabilitarControlesMLFF(Estado: Boolean);
    procedure dsTiposDataChange(Sender: TObject; Field: TField);                // TASK_153_MGO_20170315
  private
    { Private declarations }
    CantidadRegistros   : integer;
    Posicion            : TBookmark;
    Accion              : Integer; // 1=agregar   2=eliminar  3=editar
    Retorno             : Integer;
    MensajeError        : string;
    OldNumeroPuntoCobro : integer;
    FIELDS_PK           : string;
    FLeerCarriles       : Boolean;                                              // TASK_114_MGO_20170117
    TipoPorticoMLFF     : Integer;                                              // TASK_153_MGO_20170315


    //procedure CargarCategoriasClases;                                         // TASK_153_MGO_20170315  //INICIO: TASK_100_JMA_20160124

  public
    { Public declarations }
  published

  end;

var
  frmABMPuntosCobro: TfrmABMPuntosCobro;

implementation

uses DMConnection;

{$R *.dfm}


function TfrmABMPuntosCobro.GetCamposPK : string;
var
  qry           : TADOQuery;
  XML, Salida   : string;
  DocXML        : IXMLDocument;
  Nodo          : IXMLNode;
begin

  qry := TADOQuery.Create(nil);
  qry.Connection := DMConnections.BaseBO_Master;
  qry.SQL.Text :=   'SELECT                                                                                                          ' +
                    '   CAST(                                                                                                        ' +
                    '       (SELECT COLUMN_NAME AS Nombre                                                                            ' +
                    '           FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A                                                          ' +
                    '               LEFT JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ON A.CONSTRAINT_NAME = B.CONSTRAINT_NAME  ' +
                    '           where A.TABLE_SCHEMA + ''.'' + A.TABLE_NAME = ''dbo.PuntosCobro'' and CONSTRAINT_TYPE = ''PRIMARY KEY'' ' +
                    '       FOR XML PATH(''Campo''), ROOT(''Campos''), TYPE)                                                         ' +
                    '       AS VARCHAR(8000)) AS Campos                                                                              ' ;
  qry.Open;
  XML := qry.FieldByName('Campos').AsString;
  qry.Close;
  FreeAndNil(qry);

  DocXML          := TXMLDocument.Create(nil);
  DocXML.XML.Text := XML;
  DocXML.Active   := True;

  Nodo := DocXML.DocumentElement.ChildNodes.FindNode('Campo');

  Salida := EmptyStr;

  repeat
    if Nodo <> nil then begin
      Salida := Salida  + Nodo.ChildNodes.Nodes[0].NodeValue + ', ';
      Nodo:= Nodo.NextSibling;
    end;
  until Nodo = nil;

  DocXML.Active   := False;
  FreeAndNil(Nodo);

  Result          := Salida;

end;


Procedure MakeReadWrite (Const Field :TField);
  Begin
    Field.ReadOnly := False;
    With TClientDataSetAccess (Field.DataSet As TClientDataSet) Do
      Check (DSCursor.SetProp (CURProp (4),
             Field.FieldNo));
  End;

procedure TfrmABMPuntosCobro.ValidarTextoDeControl(Sender: TField; const Text: string);
var
  NombreCampo           : string;
  Valida                : Boolean;
  ValorCampo            : LongInt;
begin

  Valida := True;

  if TField(Sender).ClassName = 'TDateTimeField'  then begin
      try
        StrToDateTime(Text);
      except
          raise Exception.Create('Error en valor del campo, reintente');
      end;
  end;

  if TField(Sender).ClassName = 'TSmallintField'  then begin
    try
      ValorCampo := StrToInt(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if ContainsText(FIELDS_TINYINT, NombreCampo) then
      if not ((ValorCampo >= 0) and (ValorCampo <= 255)) then
          Valida := False;

    if ContainsText(FIELDS_SMALLNT, NombreCampo) then
       if not ((ValorCampo >= -32768) and (ValorCampo <= 32767)) then
          Valida := False;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;

  if TField(Sender).ClassName = 'TIntegerField'  then begin
    try
      ValorCampo := StrToInt(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if not ((ValorCampo >= -32768) and (ValorCampo <= 32767)) then
       Valida := False;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;

  if TField(Sender).ClassName = 'TBCDField'  then begin
    try
      StrToFloat(Text);
    except
      raise Exception.Create('Error en valor del campo, reintente');
    end;

    if not Valida then
      raise Exception.Create('Valor del campo fuera de rango, reintente')

  end;


  TField(Sender).Value := Text;

end;

function TfrmABMPuntosCobro.Inicializar(MDIChild: Boolean): Boolean;
resourcestring                                                                                                          // TASK_153_MGO_20170315
    QRY_TIPO_PORTICO_MLFF = 'SELECT CodigoTipoPuntoCobro FROM PuntosCobroTipos (NOLOCK) WHERE Descripcion = ''MLFF''';  // TASK_153_MGO_20170315
Var
	S: TSize;
    i: Integer;	// TASK_153_MGO_20170315
begin
	if MDIChild then begin
		FormStyle := fsMDIChild;
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
    Position := poMainFormCenter;
    //CargarCategoriasClases();    // TASK_153_MGO_20170315 //TASK_100_JMA_20160124
	// INICIO : TASK_153_MGO_20170315
    TipoPorticoMLFF := QueryGetValueInt(DMConnections.BaseBO_Master, QRY_TIPO_PORTICO_MLFF, 30);
    TraeRegistros;

    CDS.First;

    for i := 0 to CDS.Fields.Count - 1 do
        CDS.Fields[i].OnSetText := ValidarTextoDeControl;


    if CantidadRegistros > 0 then
        HabilitaBotones('111100001')
    else
        HabilitaBotones('110000001');
	// FIN : TASK_153_MGO_20170315

	Result := True;
end;

{ INICIO : TASK_153_MGO_20170315
//INICIO: TASK_100_JMA_20160124
procedure TfrmABMPuntosCobro.CargarCategoriasClases;
begin
    try
        try
            if Assigned(spCategoriasClases) and spCategoriasClases.Active then
                spCategoriasClases.Close;
            spCategoriasClases.Parameters.Refresh;

            spCategoriasClases.Open;
            cdsCategoriaClase.Data:=dspCategoriasClases.Data;

        except
            on e: Exception do
            begin
                MsgBoxErr('Error en carga de tipos de categorias', e.Message, 'Error', MB_ICONERROR);
            end;
        end;
    finally

    end;
end;
//TERMINO: TASK_100_JMA_20160124
} // FIN : TASK_153_MGO_20170315

function TfrmABMPuntosCobro.TraeRegistros;
var
  Resultado : Boolean;
  i         : Integer;
begin
    Accion := 0;    // TASK_153_MGO_20170315

  try
    qryTSMC.Close;
    qryPBL.Close;
    qrySentido.Close;
    qryPuntoCobroAnt.Close;
    qryEjesViales.Close;
    qryEsquemas.Close;                                                          // TASK_114_MGO_20170117
    qryTipos.Close;                                                             // TASK_153_MGO_20170315
    qryProveedores.Close;                                                       // TASK_153_MGO_20170315

    qryTSMC.Open;
    qryPBL.Open;
    qrySentido.Open;
    qryPuntoCobroAnt.Open;
    qryEjesViales.Open;
    qryEsquemas.Open;                                                           // TASK_114_MGO_20170117
    qryTipos.Open;                                                              // TASK_153_MGO_20170315
    qryProveedores.Open;                                                        // TASK_153_MGO_20170315

    procSelect.Parameters.Refresh;
    procSelect.Open;
    Retorno := procSelect.Parameters.ParamByName('@RETURN_VALUE').Value;

    if Retorno <> 0 then begin
      MensajeError := procSelect.Parameters.ParamByName('@ErrorDescription').Value;
      Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
    end;

    CantidadRegistros := procSelect.RecordCount;

    for i := 0 to CDS.Fields.Count - 1 do
      CDS.Fields[i].ReadOnly := False;


    CDS.Active   := True;
    CDS.ReadOnly := False;
    CDS.EmptyDataSet;
    CDS.DisableControls;                                                        // TASK_114_MGO_20170117


    for i := 0 to CDS.Fields.Count - 1 do
      MakeReadWrite (CDS.FieldByName (CDS.Fields[i].FieldName));

    procSelect.First;
    while not procSelect.eof do begin
      CDS.Append;
      { INICIO : TASK_114_MGO_20170117
      for i := 0 to procSelect.Fields.Count - 1 do
        CDS.Fields[i].Value := procSelect.Fields[i].Value;
      }
      CDS.FieldByName('NumeroPuntoCobro').Value         := procSelect.FieldByName('NumeroPuntoCobro').Value;
      CDS.FieldByName('DomainID').Value                 := procSelect.FieldByName('DomainID').Value;
      CDS.FieldByName('TSMCID').Value                   := procSelect.FieldByName('TSMCID').Value;
      CDS.FieldByName('Descripcion').Value              := procSelect.FieldByName('Descripcion').Value;
      CDS.FieldByName('CodigoEjeVial').Value            := procSelect.FieldByName('CodigoEjeVial').Value;
      CDS.FieldByName('CodigoEsquema').Value            := procSelect.FieldByName('CodigoEsquema').Value;
      CDS.FieldByName('Sentido').Value                  := procSelect.FieldByName('Sentido').Value;
      CDS.FieldByName('TSID').Value                     := procSelect.FieldByName('TSID').Value;
      CDS.FieldByName('PrefijoImagen').Value            := procSelect.FieldByName('PrefijoImagen').Value;
      CDS.FieldByName('PuntoCobroAnterior').Value       := procSelect.FieldByName('PuntoCobroAnterior').Value;
      CDS.FieldByName('CantidadCarriles').Value         := procSelect.FieldByName('CantidadCarriles').Value;
      CDS.FieldByName('UltimoPorticoSentido').Value     := procSelect.FieldByName('UltimoPorticoSentido').Value;
      CDS.FieldByName('CodigoJuzgado').Value            := procSelect.FieldByName('CodigoJuzgado').Value;
      CDS.FieldByName('Kms').Value                      := procSelect.FieldByName('Kms').Value;
      CDS.FieldByName('UsuarioCreacion').Value          := procSelect.FieldByName('UsuarioCreacion').Value;
      CDS.FieldByName('FechaHoraCreacion').Value        := procSelect.FieldByName('FechaHoraCreacion').Value;
      CDS.FieldByName('UsuarioModificacion').Value      := procSelect.FieldByName('UsuarioModificacion').Value;
      CDS.FieldByName('FechaHoraModificacion').Value    := procSelect.FieldByName('FechaHoraModificacion').Value;
      CDS.FieldByName('EsMLFF').Value                   := procSelect.FieldByName('EsMLFF').Value;
      //CDS.FieldByName('EsInterurbano').Value          := procSelect.FieldByName('EsInterurbano').Value;     //TASK_100_JMA_20160124
      CDS.FieldByName('IdCategoriaClase').Value         := procSelect.FieldByName('IdCategoriaClase').Value;      //TASK_100_JMA_20160124
      CDS.FieldByName('CodigoTipoPuntoCobro').Value     := procSelect.FieldByName('CodigoTipoPuntoCobro').Value;    // TASK_153_MGO_20170315
      CDS.FieldByName('CodigoPCConcesionaria').Value    := procSelect.FieldByName('CodigoPCConcesionaria').Value;   // TASK_153_MGO_20170315  
      CDS.FieldByName('Proveedor').Value                := procSelect.FieldByName('Proveedor').Value;               // TASK_153_MGO_20170315
      // FIN : TASK_114_MGO_20170117

        CDS.FieldByName('TSMCKEY').Value  :=    RightStr('00000' + CDS.FieldByName('DomainID').AsString, 5)
                                              + RightStr('00000' + CDS.FieldByName('TSMCID').AsString, 5);

      CDS.Post;
      procSelect.Next;
    end;

    CDS.ReadOnly := True;
    CDS.EnableControls;                                                         // TASK_114_MGO_20170117
    procSelect.Close;

    try
      CDS.GotoBookmark(Posicion);
    except
      CDS.First;
    end;

    Resultado := True;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      Resultado := False;
    end;
  end;

  HabilitarDeshabilitarControles(False);

  Result := Resultado;
end;

procedure TfrmABMPuntosCobro.btnAgregarClick(Sender: TObject);
begin
  HabilitaBotones('000000110');
  try
    Posicion := CDS.GetBookmark;
  except
  end;

  CDS.ReadOnly := False;

  CDS.Append;
  CDS.Post;
  //CDS.Edit;                                                                   // TASK_114_MGO_20170117

  pg01.ActivePage := tabGeneral;

  HabilitarDeshabilitarControles(True);
  //HabilitarDeshabilitarControlesMLFF(CDS.FieldByName('EsMLFF').AsBoolean);    // TASK_153_MGO_20170315    // TASK_114_MGO_20170117
  HabilitarDeshabilitarControlesMLFF(False);                                    // TASK_153_MGO_20170315
  //PonerFocoEnPrimerControl;       // TASK_153_MGO_20170315
  pg01.ActivePageIndex := 0;        // TASK_153_MGO_20170315
  edDescripcion.SetFocus;           // TASK_153_MGO_20170315

  Accion := 1;
end;

// INICIO : TASK_114_MGO_20170117
procedure TfrmABMPuntosCobro.btnAplicarCarrilClick(Sender: TObject);
resourcestring
    MSG_DESDE_HASTA = 'La Posici�n Hasta debe ser mayor a la Posici�n Desde';
begin
    if cdsCarriles.RecordCount = 0 then Exit;

    if txtPosicionHasta.ValueInt <= txtPosicionDesde.ValueInt then begin
        MsgBoxBalloon(MSG_DESDE_HASTA, 'Error', MB_ICONSTOP, txtPosicionHasta);
        Exit;
    end;

    cdsCarriles.Edit;
    cdsCarriles.FieldByName('PosicionDesde').Value := txtPosicionDesde.ValueInt;
    cdsCarriles.FieldByName('PosicionHasta').Value := txtPosicionHasta.ValueInt;
    cdsCarriles.FieldByName('Descripcion').Value := edtDescripcionCarril.Text;         // TASK_153_MGO_20170315
    cdsCarriles.Post;
end;
// FIN : TASK_114_MGO_20170117

procedure TfrmABMPuntosCobro.btnCancelarClick(Sender: TObject);
begin
  CDS.Cancel;
  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');

end;

{ INICIO : TASK_153_MGO_20170315
procedure TfrmABMPuntosCobro.PonerFocoEnPrimerControl;
var
  i, ControlIndex, TabOrderMenor : Integer;
begin

  ControlIndex  := 99;
  TabOrderMenor := 99;

  for i := 0 to pg01.ActivePage.ControlCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox',TWinControl(pg01.ActivePage.Controls[i]).ClassType.ClassName) then
      if TWinControl(pg01.ActivePage.Controls[i]).TabOrder < TabOrderMenor then begin
        TabOrderMenor := TWinControl(pg01.ActivePage.Controls[i]).TabOrder;
        ControlIndex  := i;
      end;

  if ControlIndex < 99 then
    TWinControl(pg01.ActivePage.Controls[ControlIndex]).SetFocus;

end;
} // FIN : TASK_153_MGO_20170315

procedure TfrmABMPuntosCobro.HabilitarDeshabilitarControles(Estado : Boolean);
var
  i : Integer;
begin

   for i := 0 to ComponentCount - 1 do
    //if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox', Components[i].ClassName) then begin                // TASK_114_MGO_20170117
    if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox, TNumericEdit', Components[i].ClassName) then begin    // TASK_114_MGO_20170117

      //TWinControl(Components[i]).Enabled    := Estado;

      if Components[i].ClassName = 'TDBEdit' then
        TDBEdit(Components[i]).Enabled := Estado;				// TASK_153_MGO_20170315

      if Components[i].ClassName = 'TDBLookupComboBox' then
        TDBLookupComboBox(Components[i]).Enabled := Estado;		// TASK_153_MGO_20170315

      if Components[i].ClassName = 'TDBCheckBox' then
        TDBCheckBox(Components[i]).Enabled := Estado;			// TASK_153_MGO_20170315

      if Components[i].ClassName = 'TNumericEdit' then                          // TASK_114_MGO_20170117
        TNumericEdit(Components[i]).Enabled := Estado;          // TASK_153_MGO_20170315           // TASK_114_MGO_20170117

    end;

end;


procedure TfrmABMPuntosCobro.btnEditarClick(Sender: TObject);
var
  i : Integer;
begin

  HabilitaBotones('000000110');

  Posicion              := CDS.GetBookmark;
  OldNumeroPuntoCobro   := CDS.FieldByName('NumeroPuntoCobro').Value;
  CDS.ReadOnly          := False;

  //HabilitarDeshabilitarControles(True);	// TASK_153_MGO_20170315

  for i := 0 to CDS.Fields.Count - 1 do
    if ContainsStr(FIELDS_PK, CDS.Fields[i].FieldName + ',') then
      CDS.Fields[i].ReadOnly := True;
      
  { INICIO : TASK_114_MGO_20170117
  for i := 0 to ComponentCount - 1 do
    if ContainsText('TDBEdit, TDBLookupComboBox, TDBCheckBox', Components[i].ClassName) then begin

      if Components[i].ClassName = 'TDBEdit' then
        if ContainsText(FIELDS_PK, TDBEdit(Components[i]).DataField) then
          TDBEdit(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBLookupComboBox' then
        if ContainsText(FIELDS_PK, TDBLookupComboBox(Components[i]).DataField) then
          TDBLookupComboBox(Components[i]).ReadOnly := True;

      if Components[i].ClassName = 'TDBCheckBox' then
        if ContainsText(FIELDS_PK, TDBCheckBox(Components[i]).DataField) then
          TDBCheckBox(Components[i]).ReadOnly := True;

    end;
  }
  HabilitarDeshabilitarControles(True);                                                                     // TASK_153_MGO_20170315
  //HabilitarDeshabilitarControlesMLFF(CDS.FieldByName('EsMLFF').AsBoolean);                                // TASK_153_MGO_20170315
  HabilitarDeshabilitarControlesMLFF(CDS.FieldByName('CodigoTipoPuntoCobro').AsInteger = TipoPorticoMLFF);  // TASK_153_MGO_20170315
  // FIN : TASK_114_MGO_20170117

  CDS.Edit;

  //PonerFocoEnPrimerControl;       // TASK_153_MGO_20170315
  pg01.ActivePageIndex := 0;        // TASK_153_MGO_20170315
  edDescripcion.SetFocus;           // TASK_153_MGO_20170315

  Accion := 3;
end;

procedure TfrmABMPuntosCobro.btnEliminarClick(Sender: TObject);
begin

  HabilitaBotones('000000000');

  try
    Posicion := CDS.GetBookmark;
  except
  end;

  if Application.MessageBox(PChar(ANSW_ELIMINAR + CDS.FieldByName('Descripcion').AsString),'Pregunta',MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    with procDelete do begin
      Parameters.Refresh;
      Parameters.ParamByName('@NumeroPuntoCobro').Value  := CDS.FieldByName('NumeroPuntoCobro').Value;

      try
        ExecProc;
        Retorno := procDelete.Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
          MensajeError := procDelete.Parameters.ParamByName('@ErrorDescription').Value;
          MsgBoxErr(MSG_ERROR_DELETE, MensajeError, Caption, MB_ICONERROR);
        end;
      except
        on E : Exception do begin
          MsgBoxErr(MSG_ERROR_DELETE, E.Message, Caption, MB_ICONERROR);
        end;
      end;
  end;

  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

// INICIO : TASK_114_MGO_20170117
procedure TfrmABMPuntosCobro.btnGenerarCarrilesClick(Sender: TObject);
resourcestring
    MSG_CONFIRM = 'Si ingresa una cantidad de carriles menor a la existente, los sobrantes se eliminar�n. �Desea continuar?';
var
  I, PosIni, PosFin: Integer;
  Cant, Diff: Byte;
begin
    FLeerCarriles := True;
    cdsCarriles.DisableControls;
    try
        Cant := StrToInt(dbedtCantidadCarriles.Text);

        if Cant < cdsCarriles.RecordCount then begin
            if MsgBox(MSG_CONFIRM, 'Confirmar', MB_YESNO+MB_ICONQUESTION) = ID_NO then begin
                dbedtCantidadCarriles.Text := IntToStr(cdsCarriles.RecordCount);
                Exit;
            end;

            cdsCarriles.First;
            cdsCarriles.MoveBy(Cant);
            for I := Cant to cdsCarriles.RecordCount - 1 do begin
                cdsCarriles.Delete;
                cdsCarriles.MoveBy(1);
            end;
        end;

        if Cant > cdsCarriles.RecordCount then begin
            if cdsCarriles.RecordCount > 0 then begin
                cdsCarriles.Last;
                PosIni := cdsCarriles.FieldByName('PosicionHasta').AsInteger + 1;
            end else
                PosIni := 0;

            Diff := Cant - cdsCarriles.RecordCount;
            for I := 0 to Diff - 1 do begin
                PosFin := PosIni + 1;

                cdsCarriles.Append;
                cdsCarriles.FieldByName('Orden').Value := cdsCarriles.RecordCount + 1;
                cdsCarriles.FieldByName('PosicionDesde').Value := PosIni;
                cdsCarriles.FieldByName('PosicionHasta').Value := PosFin;
                cdsCarriles.FieldByName('Descripcion').Value := 'Carril ' + IntToStr(cdsCarriles.RecordCount + 1);  // TASK_153_MGO_20170315
                cdsCarriles.Post;

                PosIni := PosFin + 1;
            end;
        end;
    finally
        FLeerCarriles := False;
        cdsCarriles.EnableControls;
        cdsCarriles.First;
    end;
end;
// FIN : TASK_114_MGO_20170117

procedure TfrmABMPuntosCobro.btnGuardarClick(Sender: TObject);
resourcestring                                                                  // TASK_114_MGO_20170117
    MSG_CARRILES_GAP = 'Las Posici�n Desde de cada Carril debe ser consecutiva a la Posici�n Hasta del anterior';    // TASK_114_MGO_20170117
    // INICIO : TASK_153_MGO_20170315
    MSG_ERROR_TIPO = 'Debe seleccionar un Tipo';
    MSG_ERROR_TSMC = 'Debe seleccionar un Controlador de Punto de Cobro';
    MSG_ERROR_NPC = 'Debe ingresar el N�mero de Punto de Cobro';
    MSG_ERROR_DESC = 'Debe ingresar una Descripci�n';
    MSG_ERROR_EJE = 'Debe seleccionar un Eje Vial';
    MSG_ERROR_SENTIDO = 'Debe seleccionar un Sentido';
    MSG_ERROR_TSID = 'Debe ingresar el TSID / Via';
    MSG_ERROR_ESQUEMA = 'Debe seleccionar un Esquema Horario';
    MSG_ERROR_CARRILES = 'Debe generar Carriles';
    MSG_ERROR_PREFIJO = 'Debe ingresar el prefijo de las im�genes';
    MSG_ERROR_PC = 'Debe ingresar el C�digo PC Concesionaria';
    MSG_ERROR_JUZGADO = 'Debe ingresar el C�digo de Juzgado';
    MSG_ERROR_KMS = 'Debe ingresar el largo en KMs';
   // FIN : TASK_153_MGO_20170315
var
  i : integer;
  spCarriles: TADOStoredProc;
  Pos: Integer;                                                                 // TASK_114_MGO_20170117
begin
    // INICIO : TASK_153_MGO_20170315
    if not ValidateControls([edCodigoTipoPuntoCobro,
                            edDomainTSMCID,
                            edNumeroPunto,
                            edDescripcion,
                            edEjeVial,
                            edSentido,
                            edTSMCID,
                            edCodigoEsquema],
                            [CDS.FieldByName('CodigoTipoPuntoCobro').AsInteger > 0,
                            CDS.FieldByName('TSMCKEY').AsString <> '',
                            edNumeroPunto.Text <> '',
                            edDescripcion.Text <> '',
                            CDS.FieldByName('CodigoEjeVial').AsInteger > 0,
                            CDS.FieldByName('Sentido').AsString <> '',
                            edTSMCID.Text <> '',
                            CDS.FieldByName('CodigoEsquema').AsInteger > 0],
                            'Error',
                            [MSG_ERROR_TIPO,
                            MSG_ERROR_TSMC,
                            MSG_ERROR_NPC,
                            MSG_ERROR_DESC,
                            MSG_ERROR_EJE,
                            MSG_ERROR_SENTIDO,
                            MSG_ERROR_TSID,
                            MSG_ERROR_ESQUEMA]) then
        Exit;
    

    if CDS.FieldByName('CodigoTipoPuntoCobro').AsInteger = TipoPorticoMLFF then begin
        if not ValidateControls([dbedtCantidadCarriles,
                                edPrefijoImagen,
                                edCodigoPCConcesionaria,
                                edCodigoJuzgado,
                                edKms],
                                [cdsCarriles.RecordCount > 0,
                                edPrefijoImagen.Text <> '',
                                edCodigoPCConcesionaria.Text <> '',
                                edCodigoJuzgado.Text <> '',
                                edKms.Text <> ''],
                                'Error',
                                [MSG_ERROR_CARRILES,
                                MSG_ERROR_PREFIJO,
                                MSG_ERROR_PC,
                                MSG_ERROR_JUZGADO,
                                MSG_ERROR_KMS]) then
            Exit;
    end;
    // FIN : TASK_153_MGO_20170315

    // INICIO : TASK_114_MGO_20170117
    if cdsCarriles.RecordCount > 0 then
    try
        FLeerCarriles := True;
        cdsCarriles.DisableControls;
        cdsCarriles.First;
        Pos := cdsCarriles.FieldByName('PosicionHasta').AsInteger;
        cdsCarriles.Next;
        while not cdsCarriles.Eof do begin
            if (cdsCarriles.FieldByName('PosicionDesde').AsInteger - Pos) <> 1 then begin
                MsgBox(MSG_CARRILES_GAP, 'Error', MB_ICONSTOP);
                Exit;
            end;
            Pos := cdsCarriles.FieldByName('PosicionHasta').AsInteger;
            cdsCarriles.Next;
        end;
    finally
        FLeerCarriles := False;
        cdsCarriles.EnableControls;
    end;
    // FIN : TASK_114_MGO_20170117

  try
    CDS.Edit;                                                                   // TASK_114_MGO_20170117
    for i := 0 to CDS.Fields.Count - 1 do
      MakeReadWrite (CDS.FieldByName (CDS.Fields[i].FieldName));

    CDS.FieldByName('TSMCID').Value       := StrToInt(RightStr(CDS.FieldByName('TSMCKEY').Value,5));
    CDS.FieldByName('DomainID').Value     := StrToInt(LeftStr (CDS.FieldByName('TSMCKEY').Value,5));

    CDS.Post;
  except
    on E : Exception do begin
      MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
      //TraeRegistros;		                                                    // TASK_114_MGO_20170117
      Exit;
    end;
  end;

  dbedtCantidadCarriles.Text := IntToStr(cdsCarriles.RecordCount);              // TASK_114_MGO_20170117

  if Accion = 1 then begin
    with procInsert do begin
        Parameters.Refresh;
        for i:=0 to  CDS.Fields.Count - 1 do
           if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName + ',') then
             Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;
        Parameters.ParamByName('@UsuarioCreacion').Value        := UsuarioSistema;
        Parameters.ParamByName('@NumeroPuntoCobro').Value       := CDS.FieldByName('NumeroPuntoCobro').Value;

        try
          ExecProc;
          Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
          if Retorno <> 0 then begin
             MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
             MsgBoxErr(MSG_ERROR_INSERT, MensajeError, Caption, MB_ICONERROR);
             Exit;                                                              // TASK_114_MGO_20170117
           end;
          except
            on E : Exception do begin
             MsgBoxErr(MSG_ERROR_INSERT, E.Message, Caption, MB_ICONERROR);
             Exit;                                                              // TASK_114_MGO_20170117
            end;
        end;

    end;
  end;

  if Accion = 3 then begin

    with procUpdate do begin
      Parameters.Refresh;
      for i:=0 to  CDS.Fields.Count - 1 do
        if NOT ContainsStr(PARAMS_IGNORE, '@' + CDS.Fields[i].FieldName + ',') then
          Parameters.ParamByName('@' + CDS.Fields[i].FieldName).Value := CDS.Fields[i].Value;

      Parameters.ParamByName('@UsuarioModificacion').Value  := UsuarioSistema;
      Parameters.ParamByName('@NumeroPuntoCobroNew').Value  := CDS.FieldByName('NumeroPuntoCobro').Value;
      Parameters.ParamByName('@NumeroPuntoCobro').Value     := OldNumeroPuntoCobro;

      try
        ExecProc;
        Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno <> 0 then begin
          MensajeError := Parameters.ParamByName('@ErrorDescription').Value;
          MsgBoxErr(MSG_ERROR_UPDATE, MensajeError, Caption, MB_ICONERROR);
          Exit;                                                                 // TASK_114_MGO_20170117
        end;
      except
        on E : Exception do begin
          MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
          Exit;                                                                 // TASK_114_MGO_20170117
        end;
      end;

    end;
  end;

    // INICIO : TASK_114_MGO_20170117
    spCarriles := TADOStoredProc.Create(Self);
    try                                      
        FLeerCarriles := True;
        cdsCarriles.DisableControls;
        try
            spCarriles.Connection := DMConnections.BaseBO_Master;
            spCarriles.CommandTimeout := 30;
            spCarriles.ProcedureName := 'ADM_Carril_DELETE';
            spCarriles.Parameters.Refresh;
            spCarriles.Parameters.ParamByName('@NumeroPuntoCobro').Value := CDS.FieldByName('NumeroPuntoCobro').Value;
            spCarriles.ExecProc;

            if spCarriles.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spCarriles.Parameters.ParamByName('@ErrorDescription').Value);

            spCarriles.ProcedureName := 'ADM_Carril_INSERT';
            spCarriles.Parameters.Refresh;
            cdsCarriles.First;
            while not cdsCarriles.Eof do begin
                spCarriles.Parameters.ParamByName('@NumeroPuntoCobro').Value := CDS.FieldByName('NumeroPuntoCobro').Value;
                spCarriles.Parameters.ParamByName('@Orden').Value := cdsCarriles.FieldByName('Orden').Value;
                spCarriles.Parameters.ParamByName('@PosicionDesde').Value := cdsCarriles.FieldByName('PosicionDesde').Value;
                spCarriles.Parameters.ParamByName('@PosicionHasta').Value := cdsCarriles.FieldByName('PosicionHasta').Value;
                spCarriles.Parameters.ParamByName('@Descripcion').Value := cdsCarriles.FieldByName('Descripcion').Value;                                // TASK_153_MGO_20170315
                spCarriles.Parameters.ParamByName('@EsExtremo').Value := IIf((cdsCarriles.FieldByName('Orden').Value = 1) or                            // TASK_153_MGO_20170315
                                                                            (cdsCarriles.FieldByName('Orden').Value = cdsCarriles.RecordCount), 1, 0);  // TASK_153_MGO_20170315
                spCarriles.ExecProc;

                if spCarriles.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                    raise Exception.Create(spCarriles.Parameters.ParamByName('@ErrorDescription').Value);

                cdsCarriles.Next;
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_UPDATE, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    finally        
        FLeerCarriles := False;
        cdsCarriles.EnableControls;
        spCarriles.Free;
    end;
    // FIN : TASK_114_MGO_20170117

  TraeRegistros;

  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
end;

procedure TfrmABMPuntosCobro.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmABMPuntosCobro.CDSAfterScroll(DataSet: TDataSet);
begin
  try
    edEjeVial.KeyValue        := null;
    edEjeVial.KeyValue        := DataSet.FieldByName('CodigoEjeVial').Value;
  except
  end;
// INICIO : TASK_114_MGO_20170117
  try
    edCodigoEsquema.KeyValue        := null;
    edCodigoEsquema.KeyValue        := DataSet.FieldByName('CodigoEsquema').Value;
  except
  end;
// FIN : TASK_114_MGO_20170117
  try
    edDomainTSMCID.KeyValue   := null;
    edDomainTSMCID.KeyValue   := DataSet.FieldByName('TSMCKEY').Value;
  except
  end;
  try
    edSentido.KeyValue        := null;
    edSentido.KeyValue        := DataSet.FieldByName('Sentido').Value;
  except
  end;

  try
    edPuntoCobroAnterior.KeyValue        := null;
    edPuntoCobroAnterior.KeyValue        := DataSet.FieldByName('PuntoCobroAnterior').Value;
  except
  end;

  //INICIO: TASK_100_JMA_20160124
  try
    edKms.Text := DataSet.FieldByName('Kms').Value;
  except
  end;
  //TERMINO: TASK_100_JMA_20160124
  CargarCarriles;                                                               // TASK_114_MGO_20170117
  HabilitarDeshabilitarControlesMLFF(False);  // TASK_153_MGO_20170315

  end;

// INICIO : TASK_114_MGO_20170117
procedure TfrmABMPuntosCobro.cdsCarrilesAfterScroll(DataSet: TDataSet);
begin
    if FLeerCarriles or (cdsCarriles.RecordCount = 0) then Exit;
    
    edtOrden.Text := cdsCarriles.FieldByName('Orden').AsString;
    txtPosicionDesde.Value := cdsCarriles.FieldByName('PosicionDesde').AsInteger;
    txtPosicionHasta.Value := cdsCarriles.FieldByName('PosicionHasta').AsInteger;
    edtDescripcionCarril.Text := cdsCarriles.FieldByName('Descripcion').AsString;   // TASK_153_MGO_20170315
end;

// INICIO : TASK_153_MGO_20170315
procedure TfrmABMPuntosCobro.dsTiposDataChange(Sender: TObject; Field: TField);
begin
    if Accion > 0 then
        HabilitarDeshabilitarControlesMLFF(CDS.FieldByName('CodigoTipoPuntoCobro').AsInteger = TipoPorticoMLFF);
end;
// FIN : TASK_153_MGO_20170315

procedure TfrmABMPuntosCobro.CargarCarriles;
resourcestring
    MSG_ERROR = 'Error obteniendo Carriles';
begin                       
    FLeerCarriles := True;
    cdsCarriles.DisableControls;
    try
        try
            spADM_Carril_SELECT.Parameters.Refresh;
            spADM_Carril_SELECT.Parameters.ParamByName('@NumeroPuntoCobro').Value := CDS.FieldByName('NumeroPuntoCobro').AsInteger;
            spADM_Carril_SELECT.Open;

            if spADM_Carril_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spADM_Carril_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsCarriles.Data := dtstprCarriles.Data;
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR, e.Message, 'Error', MB_ICONERROR);
        end;
    finally
        spADM_Carril_SELECT.Close;
        FLeerCarriles := False;
        cdsCarriles.EnableControls;
    end;
    cdsCarriles.First;
end;

{ INICIO : TASK_153_MGO_20170315
procedure TfrmABMPuntosCobro.edMLFFClick(Sender: TObject);
begin
    HabilitarDeshabilitarControlesMLFF(edMLFF.Checked);
end;
} // FIN : TASK_153_MGO_20170315

procedure TfrmABMPuntosCobro.HabilitarDeshabilitarControlesMLFF(Estado: Boolean);
begin
    btnGenerarCarriles.Enabled := Estado;
    btnAplicarCarril.Enabled := Estado;
    dbedtCantidadCarriles.Enabled := Estado;
    txtPosicionDesde.Enabled := Estado;
    txtPosicionHasta.Enabled := Estado;
    edPrefijoImagen.Enabled := Estado;
    edPuntoCobroAnterior.Enabled := Estado;
    edUltimoPortico.Enabled := Estado;
    // INICIO : TASK_153_MGO_20170315  
    edtDescripcionCarril.Enabled := Estado;
    edCodigoPCConcesionaria.Enabled := Estado;
    edCodigoJuzgado.Enabled := Estado;
    edKms.Enabled := Estado;
    // FIN : TASK_153_MGO_20170315
end;    
// FIN : TASK_114_MGO_20170117

procedure TfrmABMPuntosCobro.edTSMCIDClick(Sender: TObject);
begin
//   CDS.FieldByName('DomainID').Value := qryTSMC.FieldByName('DomainID').Value;
end;

procedure TfrmABMPuntosCobro.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
{ INICIO : TASK_153_MGO_20170315
  if Application.MessageBox(PChar(ANSW_SALIR),'Pregunta', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrYes then
    Action := caFree
  else
    Action := caNone;
}
    Action := caFree;
// FIN : TASK_153_MGO_20170315
end;

procedure TfrmABMPuntosCobro.FormCreate(Sender: TObject);
var
  i         : Integer;
  SP_Aux    : TADOStoredProc;
begin

  Caption           := CAPTION_FORM;
  btnSalir.Hint     := HINT_SALIR;
  btnAgregar.Hint   := HINT_AGREGAR;
  btnEliminar.Hint  := HINT_ELIMINAR;
  btnEditar.Hint    := HINT_EDITAR;

  procSelect.Close;
  procInsert.Close;
  procUpdate.Close;
  procDelete.Close;

  procSelect.ProcedureName      := PROCEDURE_SELECT;
  procInsert.ProcedureName      := PROCEDURE_INSERT;
  procUpdate.ProcedureName      := PROCEDURE_UPDATE;
  procDelete.ProcedureName      := PROCEDURE_DELETE;

  procSelect.Connection         := DMConnections.BaseBO_Master;
  procInsert.Connection         := DMConnections.BaseBO_Master;
  procUpdate.Connection         := DMConnections.BaseBO_Master;
  procDelete.Connection         := DMConnections.BaseBO_Master;

  qryPBL.Close;
  qryTSMC.Close;
  qryEjesViales.Close;
  qrySentido.Close;
  qryPuntoCobroAnt.Close;
  qryEsquemas.Close;                                                            // TASK_114_MGO_20170117
  qryTipos.Close;                                                               // TASK_153_MGO_20170315  
  qryProveedores.Close;                                                         // TASK_153_MGO_20170315

  qryPBL.Connection             := DMConnections.BaseBO_Master;					// TASK_114_MGO_20170117
  qryTSMC.Connection            := DMConnections.BaseBO_Master;					// TASK_114_MGO_20170117
  qryEjesViales.Connection      := DMConnections.BaseBO_Master;                 // TASK_114_MGO_20170117
  qrySentido.Connection         := DMConnections.BaseBO_Master;					// TASK_114_MGO_20170117
  qryPuntoCobroAnt.Connection   := DMConnections.BaseBO_Master;					// TASK_114_MGO_20170117


  pg01.ActivePage := tabGeneral;

  FIELDS_PK := GetCamposPK;
{ INICIO : TASK_153_MGO_20170315
  TraeRegistros;

  CDS.First;

  for i := 0 to CDS.Fields.Count - 1 do
    CDS.Fields[i].OnSetText := ValidarTextoDeControl;


  if CantidadRegistros > 0 then
    HabilitaBotones('111100001')
  else
    HabilitaBotones('110000001');
} // FIN : TASK_153_MGO_20170315
end;

procedure TfrmABMPuntosCobro.HabilitaBotones(Botones : string);
begin
  btnSalir.Enabled      := Botones[1] = '1';
  btnAgregar.Enabled    := Botones[2] = '1';
  btnEliminar.Enabled   := Botones[3] = '1';
  btnEditar.Enabled     := Botones[4] = '1';
  btnImprimir.Enabled   := Botones[5] = '1';
  btnBuscar.Enabled     := Botones[6] = '1';
  btnGuardar.Enabled    := Botones[7] = '1';
  btnCancelar.Enabled   := Botones[8] = '1';
  Grid.Enabled          := Botones[9] = '1';

  btnGenerarCarriles.Enabled := btnGuardar.Enabled;
  btnAplicarCarril.Enabled := btnGuardar.Enabled;
end;

end.



