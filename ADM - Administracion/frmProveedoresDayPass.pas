{***************************************************************************************
            	frmProveedoresDayPass

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Mantenedor de los Proveedores DayPass.


  31-Agosto-2009
    	1.-		Se agregan los campos Giro y Personer�a a la tabla y al ABM

Autor       :   CQuezadaI
Fecha       :   18 - Julio - 2014
Firma       :   SS_1147_CQU_20140714
Descripcion :   Se modifica el dfm cambiando el valor de la propiedad Hint por la que corresponde
***************************************************************************************}

unit frmProveedoresDayPass;

interface

uses
	DMConnection,
	Utilproc,
    UtilDB,
    PeaProcs,
    PeaTypes,
    Util,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBTables, StdCtrls, ExtCtrls, Validate, DateEdit, Grids, DBGrids,
  ADODB, Buttons, DmiCtrls;

type
  TProveedoresDayPassForm = class(TForm)
    pnlBotones: TPanel;
    btnTSalir: TSpeedButton;
    btnTInsertar: TSpeedButton;
    btnTEliminar: TSpeedButton;
    btnTEditar: TSpeedButton;
    tblProveedoresDayPass: TADOTable;
    dsProveedores: TDataSource;
    DBGrid1: TDBGrid;
    PAbajo: TPanel;
    Notebook: TNotebook;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    GroupB: TPanel;
    lblCodigo: TLabel;
    lblCodigoPostal: TLabel;
    lblNumero: TLabel;
    lblCalle: TLabel;
    lblRegion: TLabel;
    lblDetalle: TLabel;
    lblDocumento: TLabel;
    lblComuna: TLabel;
    edtCodigo: TNumericEdit;
    edtCodigoPostal: TEdit;
    edtNumero: TEdit;
    cbRegion: TComboBox;
    edtDetalle: TEdit;
    edtDocumento: TEdit;
    cbComuna: TComboBox;
    edtCalle: TEdit;
    Label2: TLabel;
    edtDescripcion: TEdit;
    Label1: TLabel;
    edtGiro: TEdit;
    Label3: TLabel;
    rbtnJuridica: TRadioButton;
    rbtnNatural: TRadioButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsProveedoresDataChange(Sender: TObject; Field: TField);
    procedure btnTSalirClick(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnTEditarClick(Sender: TObject);
    procedure cbRegionChange(Sender: TObject);
    procedure btnTInsertarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure btnTEliminarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FEstado : TDataSetState;
    FPersoneriaJuridica,
    FPersoneriaNatural : string;
  public
    { Public declarations }
    function Inicializa(sCaption : string): boolean;
    procedure HabilitarCampos(Habilitar : boolean);
    procedure LimpiarCampos;
    procedure BuscarRegion(CodigoRegion : string);
  end;

var
  ProveedoresDayPassForm: TProveedoresDayPassForm;

implementation

{$R *.dfm}

{***************************************************************************************
            	Inicializa

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Inicializa el mantenedor de los Proveedores DayPass.

***************************************************************************************}
function TProveedoresDayPassForm.Inicializa;
resourcestring
	MSG_ERROR = 'Error al inicializar';

    MSG_SQL_JURIDIC	= 'select dbo.CONST_PERSONERIA_JURIDICA()';
    MSG_SQL_NATURAL	= 'select dbo.CONST_PERSONERIA_NATURAL()';
    
begin
	Result := False;
	try
    	Caption := sCaption;
        CenterForm(Self);
        CargarRegiones(DMConnections.BaseCAC, cbRegion, PAIS_CHILE, REGION_SANTIAGO);
        FPersoneriaJuridica := QueryGetValue(DMConnections.BaseCAC, MSG_SQL_JURIDIC);
        FPersoneriaNatural  := QueryGetValue(DMConnections.BaseCAC, MSG_SQL_NATURAL);
        tblProveedoresDayPass.Open;
        HabilitarCampos(False);
        dsProveedores.OnDataChange(nil, nil);
    	Result := True;
    except on e:exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

{***************************************************************************************
            	LimpiarCampos

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Limpia todos los campos de edici�n

***************************************************************************************}
procedure TProveedoresDayPassForm.LimpiarCampos;
begin
	edtCodigo.Value		:= 0;
    edtDescripcion.Text := '';
    edtDocumento.Text	:= '';
    edtCalle.Text		:= '';
    edtNumero.Text		:= '';
    edtCodigoPostal.Text := '';
    edtDetalle.Text		:= '';
    edtGiro.Text		:= '';
	rbtnJuridica.Checked := False;
    rbtnNatural.Checked := False;
    cbComuna.ItemIndex	:= -1;
    cbRegion.ItemIndex	:= -1;

end;

{***************************************************************************************
            	BuscarRegion

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Busca una regi�n en la ComboBox

***************************************************************************************}
procedure TProveedoresDayPassForm.BuscarRegion;
var
	Region : string;
    i : integer;
begin
	i := 0;
    while i < cbRegion.Items.Count  do begin
        Region := Trim(StrRight(cbRegion.Items[i], 200));
        if Region = CodigoRegion then begin
        	cbRegion.ItemIndex := i;
            i := cbRegion.Items.Count;
        end;

        Inc(i);
    end;

end;

{***************************************************************************************
            	HabilitarCampos

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Habilita o deshabilita los Edit y los botones para ingreso o
  				visualizaci�n de datos

***************************************************************************************}
procedure TProveedoresDayPassForm.HabilitarCampos;
begin
    GroupB.Enabled     := Habilitar;
    pnlBotones.Enabled := not Habilitar;
    if Habilitar then begin
    	Notebook.PageIndex := 1;
        edtDescripcion.SetFocus;
    end
    else begin
    	Notebook.PageIndex := 0;
        FEstado := dsBrowse;
        DBGrid1.SetFocus;
    end;


end;

{***************************************************************************************
            	btnAceptarClick

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Graba los cambios.

***************************************************************************************}
procedure TProveedoresDayPassForm.BtnAceptarClick(Sender: TObject);
resourcestring
	MSG_PROVEE	= ' del Proveedor DayPass';
    MSG_NOHA	= 'No ha ingresado ';
    MSG_DESCRI	= 'la descripci�n';
    MSG_RUT		= 'el Rut';
    MSG_CALLE	= 'la Calle';
    MSG_NUMERO	= 'el N�mero';
    MSG_REGION	= 'la Regi�n';
    MSG_COMUNA	= 'la Comuna';
    MSG_GIRO	= 'el Giro';
    MSG_PERSON	= 'la Personer�a';
    MSG_ERROR	= 'Ocurri� un error al grabar la informaci�n del Proveedor DayPass';
    MSG_TODOOK	= 'La informaci�n ha sido grabada';

begin
    if not ValidateControls([edtDescripcion, edtDocumento, edtGiro, edtCalle,
    						 edtNumero, rbtnJuridica, cbRegion, cbComuna ],
    						[edtDescripcion.Text <> '', edtDocumento.Text <> '',
                             Trim(edtGiro.Text) <> '', edtCalle.Text <> '', edtNumero.Text <> '',
                             rbtnJuridica.Checked or rbtnNatural.Checked,
                             cbRegion.Text <> '', cbComuna.Text <> ''],
                             Caption,
                            [MSG_NOHA + MSG_DESCRI + MSG_PROVEE,
                             MSG_NOHA + MSG_RUT + MSG_PROVEE,
                             MSG_NOHA + MSG_GIRO + MSG_PROVEE,
                             MSG_NOHA + MSG_CALLE + MSG_PROVEE,
                             MSG_NOHA + MSG_NUMERO + MSG_PROVEE,
                             MSG_NOHA + MSG_PERSON + MSG_PROVEE,
                             MSG_NOHA + MSG_REGION + MSG_PROVEE,
                             MSG_NOHA + MSG_COMUNA + MSG_PROVEE ]) then Exit;


    try
    	if FEstado = dsInsert then tblProveedoresDayPass.Append
    	else tblProveedoresDayPass.Edit;

        tblProveedoresDayPass.FieldByName('Descripcion').AsString		:= edtDescripcion.Text;
        tblProveedoresDayPass.FieldByName('NumeroDocumento').AsString	:= edtDocumento.Text;
        tblProveedoresDayPass.FieldByName('Calle').AsString				:= edtCalle.Text;
        tblProveedoresDayPass.FieldByName('Numero').AsString			:= edtNumero.Text;
        tblProveedoresDayPass.FieldByName('CodigoPostal').AsString		:= edtCodigoPostal.Text;
        tblProveedoresDayPass.FieldByName('DetalleDomicilio').AsString	:= edtDetalle.Text;
        tblProveedoresDayPass.FieldByName('CodigoRegion').AsString		:= Trim(StrRight(cbRegion.Text,200));
    	tblProveedoresDayPass.FieldByName('CodigoComuna').AsString		:= Trim(StrRight(cbComuna.Text,200));
        tblProveedoresDayPass.FieldByName('Giro').AsString				:= Trim(edtGiro.Text);
        if rbtnJuridica.Checked then tblProveedoresDayPass.FieldByName('Personeria').AsString := FPersoneriaJuridica
        else tblProveedoresDayPass.FieldByName('Personeria').AsString := FPersoneriaNatural;

        tblProveedoresDayPass.Post;
        MsgBox(MSG_TODOOK, Caption, MB_ICONINFORMATION);
        HabilitarCampos(False);
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
            if tblProveedoresDayPass.State in [dsEdit, dsInsert] then begin
            	tblProveedoresDayPass.Cancel;
            end;

    	end;

    end;


end;
{***************************************************************************************
            	btnCancelarClick

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Cancela la inserci�n o la edici�n de datos

***************************************************************************************}
procedure TProveedoresDayPassForm.BtnCancelarClick(Sender: TObject);
begin
    HabilitarCampos(False);
end;

{***************************************************************************************
            	btnSalirClick

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Cierra esta ventana

***************************************************************************************}
procedure TProveedoresDayPassForm.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

{***************************************************************************************
            	btnEditarClick

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Inicia la edici�n de un registro existente

***************************************************************************************}
procedure TProveedoresDayPassForm.btnTEditarClick(Sender: TObject);
begin
    if not tblProveedoresDayPass.Fields[0].IsNull then begin
    	FEstado := dsEdit;
    	HabilitarCampos(True);
    end;
end;

{***************************************************************************************
            	btnEliminarClick

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Elimina el registro actual

***************************************************************************************}
procedure TProveedoresDayPassForm.btnTEliminarClick(Sender: TObject);
resourcestring
	MSG_ELIMINAR = 'Est� seguro de eliminar este proveedor?';
    MSG_ERROR    = 'No se pudo eliminar el proveedor DayPass';

begin
	if	(not tblProveedoresDayPass.Eof) and
    	(not tblProveedoresDayPass.Fields[0].IsNull) and
        (tblProveedoresDayPass.State = dsBrowse) and
    	(MsgBox(MSG_ELIMINAR,Caption, MB_ICONQUESTION + MB_YESNO) = ID_YES) then begin
        try
    		tblProveedoresDayPass.Delete;
        except on e:exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
        	end;

        end;
    end;

end;

{***************************************************************************************
            	btnInsertarClick

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Inicia la Inserci�n de un registro nuevo

***************************************************************************************}
procedure TProveedoresDayPassForm.btnTInsertarClick(Sender: TObject);
begin
    FEstado := dsInsert;
    HabilitarCampos(True);
    LimpiarCampos();
end;

{***************************************************************************************
            	btnTSalirClick

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Invoca al evento OnClick del bot�n btnSalir

***************************************************************************************}
procedure TProveedoresDayPassForm.btnTSalirClick(Sender: TObject);
begin
	BtnSalir.Click;
end;

{***************************************************************************************
            	cbRegionChange

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Obtiene el c�digo de la regi�n seleccinada en la combobox y busca las comunas
            	asociadas a esa regi�n.

***************************************************************************************}
procedure TProveedoresDayPassForm.cbRegionChange(Sender: TObject);
var
	Region, Comuna : string;
begin
    if FEstado in [dsEdit, dsBrowse] then Comuna := Trim(tblProveedoresDayPass.FieldByName('CodigoComuna').AsString)
    else Comuna := '';

	Region := Trim(StrRight(cbRegion.Text,200));
    CargarComunas(DMConnections.BaseCAC, cbComuna, PAIS_CHILE, Region, Comuna, False, False);
    if Comuna = '' then cbComuna.ItemIndex := -1;
    

end;

{***************************************************************************************
            	dsProveedoresDataChange

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Despliega los datos del registro actual.

***************************************************************************************}
procedure TProveedoresDayPassForm.dsProveedoresDataChange(Sender: TObject;
  Field: TField);
begin
	if (FEstado = dsBrowse) and (Field = nil) then begin
    	with tblProveedoresDayPass do begin
        	edtCodigo.ValueInt	:= FieldByName('CodigoProveedorDayPass').AsInteger;
        	edtDescripcion.Text	:= FieldByName('Descripcion').AsString;
            edtDocumento.Text	:= FieldByName('NumeroDocumento').AsString;
            edtCalle.Text		:= FieldByName('Calle').AsString;
            edtNumero.Text		:= FieldByName('Numero').AsString;
            edtCodigoPostal.Text:= FieldByName('CodigoPostal').AsString;
            edtDetalle.Text		:= FieldByName('DetalleDomicilio').AsString;
            edtGiro.Text		:= FieldByName('Giro').AsString;
            if FieldByName('Personeria').AsString = FPersoneriaJuridica then rbtnJuridica.Checked := True
            else if FieldByName('Personeria').AsString = FPersoneriaNatural then rbtnNatural.Checked := True
            else begin
            	rbtnJuridica.Checked := False;
                rbtnNatural.Checked := False;
            end;
            
            if not FieldByName('CodigoRegion').IsNull then begin
				BuscarRegion(FieldByName('CodigoRegion').AsString);
				cbRegion.OnChange(cbRegion);
            end
            else begin
            	cbRegion.ItemIndex := -1;
                cbComuna.Clear;
            end;
        end;

    end;

end;

{***************************************************************************************
            	FormClose

  Author: mbecerra
  Date:  26-Agosto-2008
  Description:  Fuerza a cerrar el formulario MDIChild

***************************************************************************************}
procedure TProveedoresDayPassForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;


end.
