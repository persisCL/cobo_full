unit FrmCodigosComercio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, DBGrids, DMConnection, DB,
  ADODB, UtilDb, Util, Utilproc, DmiCtrls, ExtCtrls, PeaProcs,
  DPSControls;

type
  TFormCodigosComercio = class(TForm)
    ds_CodigosComercio: TDataSource;
    GrillaCodigosComercio: TStringGrid;
    ObtenerCodigosComercio: TADOStoredProc;
    ActualizarCodigoComercio: TADOStoredProc;
    Label1: TLabel;
    cb_OperadorLogistico: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    cb_Tarjetas: TComboBox;
    Panel1: TPanel;
    Label4: TLabel;
    cb_AplicacionesVenta: TComboBox;
    Label5: TLabel;
    cb_LugarDeVenta: TComboBox;
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure GrillaCodigosComercioSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GrillaCodigosComercioEnter(Sender: TObject);
    procedure GrillaCodigosComercioKeyPress(Sender: TObject; var Key: Char);
    procedure ActualizarGrilla(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cb_OperadorLogisticoChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperadorLogistico: integer;
    FCodigoLugarDeVenta: integer;

  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean; CodigoOperadorLogistico: integer = 0; CodigoLugarDeVenta: integer = 0): boolean;
    procedure LimpiarGrilla;
  end;

var
  FormCodigosComercio: TFormCodigosComercio;

implementation

{$R *.dfm}

{ TFormLiquidacion }
var
  Conjunto : set of char;

function TFormCodigosComercio.Inicializa(MDIChild: Boolean;
                                CodigoOperadorLogistico: integer = 0;
                                CodigoLugarDeVenta: integer = 0): boolean;
resourcestring
    TARJETA_DE_CREDITO = 'Tarjeta de cr�dito';
    CODIGO_DE_COMERCIO = 'C�digo de comercio';
    NO_HAY_OPERADORES  = 'No hay Operadores Log�sticos cargados en el Sistema';
    NO_HAY_TARJETAS    = 'No existen tarjetas de cr�dito cargadas en el sistema';
    NO_HAY_SISTEMAS    = 'No existen Sistemas cargados';
    TODAS              = 'TODAS';
    ERROR_CONFIGURACION_INICIAL = 'No se pudo cargar la configuraci�n inicial';
    INICIALIZAR_CARGA  = 'Inicializar la Carga de C�digos de Comercio';
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

    FCodigoOperadorLogistico := CodigoOperadorLogistico;
    FCodigoLugarDeVenta := CodigoLugarDeVenta;

	Conjunto := [#8, #9, #13, '0'..'9', Char(VK_BACK), Char(VK_SPACE), Char(VK_DELETE)];

    GrillaCodigosComercio.Cells[0,0] := TARJETA_DE_CREDITO;
    GrillaCodigosComercio.Cells[1,0] := CODIGO_DE_COMERCIO;
    Result:= False;

    try
        CargarOperadoresLogisticos(DMConnections.BaseCAC, cb_OperadorLogistico);
		if cb_OperadorLogistico.Items.Count < 1 then raise Exception.Create(NO_HAY_OPERADORES);
        if cb_OperadorLogistico.ItemIndex = -1 then cb_OperadorLogistico.ItemIndex := 0;
        CargarLugaresDeVenta(DMConnections.BaseCAC,cb_LugarDeVenta, StrToInt(StrRight(cb_OperadorLogistico.Text, 20)));
        cb_LugarDeVenta.ItemIndex := 0;
        CargarTarjetasCredito(DMConnections.BaseCAC,cb_Tarjetas);
		if cb_Tarjetas.Items.Count < 1 then	raise Exception.Create(NO_HAY_TARJETAS);

        //Cargar los sistemas de venta por eso se manda true
        CargarSistemas(DMConnections.BaseCAC,cb_AplicacionesVenta, true);

		if cb_AplicacionesVenta.Items.Count < 1 then raise Exception.Create(NO_HAY_SISTEMAS);
        cb_Tarjetas.Items.Insert(0, TODAS);
        cb_Tarjetas.ItemIndex := 0;
        cb_AplicacionesVenta.ItemIndex := 0;

        if FCodigoOperadorLogistico <> 0 then cb_OperadorLogistico.Enabled := False;
        if FCodigoLugarDeVenta <> 0 then cb_LugarDeVenta.Enabled := false;
        ActualizarGrilla(cb_LugarDeVenta);
        Result := True;
    except
		On E: exception do begin
        	MsgBoxErr(ERROR_CONFIGURACION_INICIAL,
            e.Message, INICIALIZAR_CARGA, MB_ICONSTOP);
        end;
    end;
end;

procedure TFormCodigosComercio.btn_AceptarClick(Sender: TObject);
resourcestring
    DESEA_GUARDAR = '�Desea guardar los datos de los C�digos de Comercio?';
    CODIGOS_COMERCIO = 'C�digos de Comercio';
    ERROR_ELIMINAR = 'No se pudieron eliminar los C�digos de Comercio';
    ACTUALIZAR_CODIGOS_COMERCIO = 'Actualizar C�digos de Comercio';
    ERROR_ALMACENAR = 'No se pudieron almacenar los datos del C�digo de Comercio';
Var
	i: integer;
    TodoOk: boolean;
    Texto: AnsiString;
begin
	TodoOK := False;
	if MsgBox(DESEA_GUARDAR,
	  CODIGOS_COMERCIO, MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) <> IDYES then Exit;

	Screen.Cursor := crHourGlass;

	// Recorro toda la grilla y guardo los C�digos de Comercio.
    try

    	DMConnections.BaseCAC.BeginTrans;

		//Grabo los nuevos C�digos de Comercio.
        for i := 1 to GrillaCodigosComercio.RowCount - 1 do begin
            try
                if Trim(GrillaCodigosComercio.Cells[1, i]) <> '' then begin
                   with ActualizarCodigoComercio do begin
                      Parameters.ParamByName('@CodigoLugarDeVenta').Value	    := Ival(strRight(cb_LugarDeVenta.Text, 20));
                      Parameters.ParamByName('@CodigoSistema').Value 	:= Trim(strRight(cb_AplicacionesVenta.Text, 40));
                      Parameters.ParamByName('@CodigoTarjeta').Value 	:= Ival(StrRight(GrillaCodigosComercio.Cells[0,i], 10));
                      Parameters.ParamByName('@CodigoComercio').Value 	:= Trim(GrillaCodigosComercio.Cells[1,i]);
                   end;
                   ActualizarCodigoComercio.ExecProc;
                   ActualizarCodigoComercio.Close;

                end else begin
                    try
              	        //Si esta vac�o lo borro.
                        Texto :=
                                'DELETE FROM CodigosComercio WHERE ' +
                                ' CodigoLugarDeVenta = ' + IntToStr(Ival(strRight(cb_LugarDeVenta.Text, 10))) +
                                ' AND CodigoSistema = ''' + Trim(StrRight(cb_AplicacionesVenta.Text, 40))+ ''''   +
                                ' AND CodigoTarjeta = ' + IntToStr(Ival(StrRight(GrillaCodigosComercio.Cells[0,i], 10)));
                        QueryExecute(DMConnections.BaseCAC, Texto);
                    except
                        On E: Exception do begin
                            MsgBoxErr(ERROR_ELIMINAR, e.Message, ACTUALIZAR_CODIGOS_COMERCIO, MB_ICONSTOP);
                            Exit;
                        end;
                    end;
                end;
            except
                 on E: Exception do begin
                    MsgBoxErr(ERROR_ALMACENAR, e.message, ACTUALIZAR_CODIGOS_COMERCIO, MB_ICONSTOP);
                    ActualizarCodigoComercio.Close;
                    Exit;
                 end;
            end;
        end;
        TodoOK := True;

	finally
        if TodoOK then begin
        	DMConnections.BaseCAC.CommitTrans;
            ModalResult := mrOk;
        end
        else DMConnections.BaseCAC.RollbackTrans;
        Screen.Cursor := crDefault;
    end;
	Screen.Cursor := crDefault;
end;

procedure TFormCodigosComercio.btn_CancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormCodigosComercio.GrillaCodigosComercioSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
	if ACol <> 1 then CanSelect := False;
end;

procedure TFormCodigosComercio.GrillaCodigosComercioEnter(Sender: TObject);
begin
	GrillaCodigosComercio.Row := 1;
	GrillaCodigosComercio.Col := 1;
end;

procedure TFormCodigosComercio.GrillaCodigosComercioKeyPress(Sender: TObject; var Key: Char);
begin
	if  not (Key in Conjunto) then Key := #0;
end;


procedure TFormCodigosComercio.ActualizarGrilla(Sender: TObject);
var
	i: integer;
begin
	ObtenerCodigosComercio.Close;
	ObtenerCodigosComercio.Parameters.ParamByName('@CodigoOperadorLogistico').Value	:= Ival(StrRight(cb_OperadorLogistico.Text, 20));
	ObtenerCodigosComercio.Parameters.ParamByName('@CodigoLugarDeVenta').Value	:= Ival(StrRight(cb_LugarDeVenta.Text, 20));
	ObtenerCodigosComercio.Parameters.ParamByName('@CodigoSistema').Value	:= Trim(StrRight(cb_AplicacionesVenta.Text, 40));
	ObtenerCodigosComercio.Parameters.ParamByName('@CodigoTarjeta').Value	:= iif(cb_Tarjetas.ItemIndex = 0, null, Ival(StrRight(cb_Tarjetas.Text, 10)));
	OpenTables([ObtenerCodigosComercio]);
    LimpiarGrilla;
    GrillaCodigosComercio.rowCount :=  iif( (ObtenerCodigosComercio.RecordCount = 0), 2,ObtenerCodigosComercio.RecordCount + 1);
    ObtenerCodigosComercio.First;
    i:= 1;
    While not ObtenerCodigosComercio.Eof do begin
        GrillaCodigosComercio.Cells[0,i] := PadR(Trim(ObtenerCodigosComercio.FieldByName('DescriTarjeta').AsString), 300, ' ')
                                               + Istr(ObtenerCodigosComercio.FieldByName('CodigoTarjeta').AsInteger);
        GrillaCodigosComercio.Cells[1,i] := Trim(ObtenerCodigosComercio.FieldByName('CodigoComercio').AsString);
        ObtenerCodigosComercio.Next;
		inc(i);
    end;
    btn_Aceptar.Enabled := ObtenerCodigosComercio.RecordCount > 0;
    ObtenerCodigosComercio.Close;
end;

procedure TFormCodigosComercio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    cb_OperadorLogistico.Enabled := True;
    cb_LugarDeVenta.Enabled := True;
    Action := caFree;
end;

procedure TFormCodigosComercio.LimpiarGrilla;
var
	i: integer;
begin
    for i := 1 to GrillaCodigosComercio.RowCount - 1 do begin
        GrillaCodigosComercio.Cells[0, i] := '';
        GrillaCodigosComercio.Cells[1, i] := '';
    end;
end;

procedure TFormCodigosComercio.cb_OperadorLogisticoChange(Sender: TObject);
begin
    CargarLugaresDeVenta(DMConnections.BaseCAC, cb_LugarDeVenta,
                         StrToInt(StrRight(cb_OperadorLogistico.Text, 20)));
    cb_LugarDeVenta.ItemIndex := 0;
    ActualizarGrilla(cb_LugarDeVenta);
end;

end.
