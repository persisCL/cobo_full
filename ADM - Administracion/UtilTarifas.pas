{-----------------------------------------------------------------------------
 File Name: UtilTarifas
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 20/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit UtilTarifas;

interface

Uses
	Classes, Windows, Messages, SysUtils, PeaTypes, Menus, Util, UtilProc, DB,
	DBTables, UtilDB, Graphics, WinSock, Forms, Controls, JPeg,
	ADODB, StdCtrls, Variants, ComCtrls, StrUtils, dialogs, DMConnection, Math,
    VariantComboBox, DateUtils, RStrings;


procedure CargarTramosConcesionaria(Conn: TADOConnection; Combo: TComboBox; CodigoConcesionaria: integer; CodigoTramo: Integer = 0);

implementation

{-----------------------------------------------------------------------------
  Function Name:
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 20/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTramosConcesionaria(Conn: TADOConnection; Combo: TComboBox; CodigoConcesionaria: integer; CodigoTramo: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text :=
        ' SELECT * FROM TRAMOS WITH (NOLOCK) ';
        //' WHERE CodigoConcesionaria = ' + IntToStr(CodigoConcesionaria);

		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Trim(Qry.FieldByName('Descripcion').AsString) +
			    Space(500) + Qry.FieldByName('CodigoTramo').AsString);
			if Qry.FieldByName('CodigoTramo').AsInteger = CodigoTramo then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


end.
