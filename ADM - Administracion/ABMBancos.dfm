object FormBancos: TFormBancos
  Left = 113
  Top = 70
  Caption = 'Mantenimiento de Bancos'
  ClientHeight = 534
  ClientWidth = 738
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 738
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 738
    Height = 266
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'86'#0'C'#243'digo               '
      #0'187'#0'Descripci'#243'n                                         ')
    HScrollBar = True
    RefreshTime = 100
    Table = Bancos
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupB: TPanel
    Left = 0
    Top = 299
    Width = 738
    Height = 196
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      738
      196)
    object Label1: TLabel
      Left = 15
      Top = 14
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object PcBancos: TPageControl
      Left = 1
      Top = 1
      Width = 744
      Height = 195
      ActivePage = TsDatosBanco
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      object TsDatosBanco: TTabSheet
        Caption = 'Identificaci'#243'n'
        Enabled = False
        object Label15: TLabel
          Left = 9
          Top = 12
          Width = 48
          Height = 13
          Caption = '&C'#243'digo: '
          FocusControl = txt_codigo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 9
          Top = 38
          Width = 72
          Height = 13
          Caption = '&Descripci'#243'n:'
          FocusControl = txt_Descripcion
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 9
          Top = 64
          Width = 75
          Height = 13
          Caption = 'C'#243'&digo SBIF:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object txt_Descripcion: TEdit
          Left = 103
          Top = 34
          Width = 319
          Height = 21
          Hint = 'Descripci'#243'n del Tipo de Veh'#237'culo'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 50
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object cb_Activo: TCheckBox
          Left = 8
          Top = 148
          Width = 145
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Ac&tivo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object txt_codigo: TNumericEdit
          Left = 103
          Top = 8
          Width = 66
          Height = 21
          Color = clBtnFace
          Enabled = False
          MaxLength = 3
          TabOrder = 0
        end
        object neCodSBEI: TNumericEdit
          Left = 103
          Top = 60
          Width = 121
          Height = 21
          Color = 16444382
          MaxLength = 4
          TabOrder = 2
        end
        object chb_HabilitadoPagoVentanilla: TCheckBox
          Left = 8
          Top = 86
          Width = 145
          Height = 17
          Hint = 'Indica si se aceptan cheques del banco para el pago en caja.'
          Alignment = taLeftJustify
          Caption = 'Pago &Ventanilla'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object chb_HabilitadoEmisorTC: TCheckBox
          Left = 8
          Top = 107
          Width = 145
          Height = 17
          Hint = 'Indica si es emisor de Tarjetas de Cr'#233'dito.'
          Alignment = taLeftJustify
          Caption = 'Emisor Tarjeta Cr'#233'dito'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
        end
        object chb_HabilitadoEmisorPAC: TCheckBox
          Left = 8
          Top = 128
          Width = 145
          Height = 17
          Hint = 'Indica si est'#225' habilitado para firmar convenios PAC.'
          Alignment = taLeftJustify
          Caption = 'Convenios PAC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
        end
      end
      object TsCuentas: TTabSheet
        Caption = 'Cuentas'
        Enabled = False
        ImageIndex = 1
        DesignSize = (
          736
          167)
        object Cb_Cuentas: TVariantCheckListBox
          Left = 5
          Top = 8
          Width = 722
          Height = 153
          Anchors = [akLeft, akTop, akRight, akBottom]
          ItemHeight = 13
          Items = <>
          TabOrder = 0
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 495
    Width = 738
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Label3: TLabel
      Left = 35
      Top = 13
      Width = 63
      Height = 13
      Caption = 'Desactivado.'
    end
    object Notebook: TNotebook
      Left = 541
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 25
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
    object Panel1: TPanel
      Left = 16
      Top = 12
      Width = 12
      Height = 14
      BevelOuter = bvNone
      Color = clRed
      TabOrder = 1
    end
  end
  object Bancos: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'Bancos'
    Left = 242
    Top = 110
  end
  object ActualizarCuentasBancariasxBancos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarCuentasBancariasxBancos'
    Parameters = <>
    Left = 328
    Top = 152
  end
  object spActualizarEmisoresTarjetaCredito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEmisoresTarjetaCredito'
    Parameters = <>
    Left = 470
    Top = 107
  end
end
