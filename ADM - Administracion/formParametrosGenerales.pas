{********************************** File Header ********************************
File Name   : FormParametrosSolicitud
Author      : Malisia, Fabio <fmalisia@dpsautomation.com>
Date Created: 17-Mar-2004
Language    : ES-AR
Description : Permite configurar los parametros generales de una clase dada
*******************************************************************************}

unit FormParametrosGenerales;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaProcs,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, validate, Util, ADODB, DPSControls,
  DateEdit, ConstParametrosGenerales;

type
  TfrmParametrosGenerales = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    eDescripcion: TEdit;
	Notebook: TNotebook;
	Label15: TLabel;
	BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    QryParametrosSolicitud: TADOQuery;
    Label2: TLabel;
    nbValores: TNotebook;
    neValor: TNumericEdit;
    deValor: TDateEdit;
    neNumericoValorDefault: TNumericEdit;
    neNumericoValor: TNumericEdit;
    neValorDefault: TNumericEdit;
    deValorDefault: TDateEdit;
    eValor: TEdit;
    eValorDefault: TEdit;
    Label3: TLabel;
    eNombre: TEdit;
    ActualizarParametroGeneral: TADOStoredProc;
    btnDefault: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnDefaultClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializar(CaptionForm: string; ClaseParametros: Char): Boolean;
  end;

var
  frmParametrosGenerales: TfrmParametrosGenerales;

implementation

uses RStrings, DMConnection;

{$R *.DFM}

function TfrmParametrosGenerales.Inicializar(CaptionForm: string; ClaseParametros: Char): Boolean;
Var
	S: TSize;
begin
    self.Caption := CaptionForm;
    S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Result := False;
    QryParametrosSolicitud.Parameters.paramByName('ClaseParametro').value := ClaseParametros;
	if not OpenTables([QryParametrosSolicitud]) then exit;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	Result := True;
end;

procedure TfrmParametrosGenerales.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

{******************************** Function Header *******************************
  Function Name: TfrmParametrosGenerales.DBList1Click
  Author:        Malisia, Fabio Nazareno <fmalisia@dpsautomation.com>
  Date Created:  18-Mar-2004
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Permite asignar los valores de la tabla en los controles de edici�n.
********************************************************************************}
procedure TfrmParametrosGenerales.DBList1Click(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
		eNombre.text    	:= FieldByName('Nombre').AsString;
		eDescripcion.text	:= Trim(FieldByName('Descripcion').AsString);
        case FieldByName('TipoParametro').AsString[1] of
            TP_FECHA:
                begin
                    nbValores.ActivePage := 'Fecha';
                    try
                        deValorDefault.Date := StrToDateTime(FieldByName('ValorDefault').AsString);
                        deValor.date := StrToDateTime(FieldByName('Valor').AsString);
                    except
                    end;
                end;
            TP_NUMERICO:
                begin
                    nbValores.ActivePage := 'Numerico';
                    try
                        neNumericoValorDefault.value := strToFloat(FieldByName('ValorDefault').AsString);
                        neNumericoValor.value := strToFloat(FieldByName('Valor').AsString);
                    except
                    end;
                end;
            TP_ENTERO:
                begin
                    nbValores.ActivePage := 'Entero';
                    try
                        neValorDefault.valueint := strToInt(FieldByName('ValorDefault').AsString);
                        neValor.valueint := strToInt(FieldByName('Valor').AsString);
                    except
                    end;
                end;
            TP_TEXTO:
                begin
                    nbValores.ActivePage := 'Texto';
                    eValorDefault.Text := Trim(FieldByName('ValorDefault').AsString);
                    eValor.Text := Trim(FieldByName('Valor').AsString);
                end;
        end;
	end;
end;

{******************************** Function Header *******************************
  Function Name: TfrmParametrosGenerales.DBList1DrawItem
  Author:        Malisia, Fabio Nazareno <fmalisia@dpsautomation.com>
  Date Created:  18-Mar-2004
  Parameters:    Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value:  None
  Description:   Permite visualizar cada registro de la lista.
********************************************************************************}
procedure TfrmParametrosGenerales.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
resourcestring
    DTP_FECHA = 'Fecha';
    DTP_NUMERICO = 'N�merico';
    DPT_ENTERO = 'Entero';
    DPT_TEXTO = 'Texto';


begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(Tabla.FieldbyName('Nombre').AsString));
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Valor').AsString));
        case Tabla.FieldbyName('TipoParametro').AsString[1] of
            TP_FECHA:
                begin
                    TextOut(Cols[2], Rect.Top, DTP_FECHA);
                end;
            TP_NUMERICO:
                begin
                    TextOut(Cols[2], Rect.Top, DTP_NUMERICO);
                end;
            TP_ENTERO:
                begin
                    TextOut(Cols[2], Rect.Top, DPT_ENTERO);
                end;
            TP_TEXTO:
                begin
                    TextOut(Cols[2], Rect.Top, DPT_TEXTO);
                end;
        end;
        TextOut(Cols[3], Rect.Top, Trim(Tabla.FieldbyName('ValorDefault').AsString));
	end;
end;

procedure TfrmParametrosGenerales.DBList1Edit(Sender: TObject);
begin
	DbList1.Enabled    		:= False;
    dblist1.Estado     		:= modi;
	Notebook.PageIndex 		:= 1;
	groupb.Enabled     		:= True;
    case dblist1.table.FieldByName('TipoParametro').AsString[1] of
        TP_FECHA:
            begin
                nbValores.ActivePage := 'Fecha';
                deValor.SetFocus;
            end;
        TP_NUMERICO:
            begin
                nbValores.ActivePage := 'Numerico';
                neNumericoValor.SetFocus;
            end;
        TP_ENTERO:
            begin
                nbValores.ActivePage := 'Entero';
                neValor.SetFocus;
            end;
        TP_TEXTO:
            begin
                nbValores.ActivePage := 'Texto';
                eValor.SetFocus;
            end;
    end;
end;

procedure TfrmParametrosGenerales.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TfrmParametrosGenerales.Limpiar_Campos();
begin
    deValor.date := nulldate;
    neNumericoValor.value := 0.0;
    neValor.valueint := 0;
    eValor.Text := '';
end;

procedure TfrmParametrosGenerales.AbmToolbar1Close(Sender: TObject);
begin
	 close;
end;

{******************************** Function Header *******************************
  Function Name: TfrmParametrosGenerales.BtnAceptarClick
  Author:        Malisia, Fabio Nazareno <fmalisia@dpsautomation.com>
  Date Created:  18-Mar-2004
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Acepta los cambios realizados.
********************************************************************************}
procedure TfrmParametrosGenerales.BtnAceptarClick(Sender: TObject);
var FLD_VALOR: ansistring;
begin
    Screen.Cursor := crHourGlass;
    try
        ActualizarParametroGeneral.Parameters.ParamByName('@Nombre').value := trim(dbList1.Table.fieldByName('Nombre').asString);
        case dbList1.Table.FieldByName('TipoParametro').AsString[1] of
            TP_FECHA:
                begin
                    try
                        ActualizarParametroGeneral.Parameters.ParamByName('@ValorParametro').value := DateTimeToStr(deValor.date);
                    except
                    end;
                end;
            TP_NUMERICO:
                begin
                    try
                        ActualizarParametroGeneral.Parameters.ParamByName('@ValorParametro').value := FloatToStr(neNumericoValor.value);
                    except
                    end;
                end;
            TP_ENTERO:
                begin
                    try
                        ActualizarParametroGeneral.Parameters.ParamByName('@ValorParametro').value := IntToStr(neValor.valueint);
                    except
                    end;
                end;
            TP_TEXTO:
                begin
                    ActualizarParametroGeneral.Parameters.ParamByName('@ValorParametro').value := eValor.text;
                end;
        end;
        ActualizarParametroGeneral.ExecProc;
    except
        On E: Exception do begin
            MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[FLD_VALOR]), E.message, Format (MSG_CAPTION_ACTUALIZAR,[FLD_VALOR]), MB_ICONSTOP);
        end;
    end;
	Volver_Campos;
	Screen.Cursor 	   := crDefault;
end;

procedure TfrmParametrosGenerales.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TfrmParametrosGenerales.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	action := caFree;
end;

procedure TfrmParametrosGenerales.BtnSalirClick(Sender: TObject);
begin
	close;
end;

procedure TfrmParametrosGenerales.Volver_Campos;
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	DBList1.Reload;
	DbList1.SetFocus;
	Notebook.PageIndex := 0;
	groupb.Enabled     := False;
end;

{******************************** Function Header *******************************
  Function Name: TfrmParametrosGenerales.btnDefaultClick
  Author:        Malisia, Fabio Nazareno <fmalisia@dpsautomation.com>
  Date Created:  18-Mar-2004
  Parameters:    Sender: TObject
  Return Value:  None
  Description:   Asigna al valor el valor por default.
********************************************************************************}
procedure TfrmParametrosGenerales.btnDefaultClick(Sender: TObject);
begin
    case dblist1.Table.FieldByName('TipoParametro').AsString[1] of
        TP_FECHA: deValor.date := deValorDefault.Date;
        TP_NUMERICO: neNumericoValor.value := neNumericoValorDefault.value;
        TP_ENTERO: neValor.valueint := neValorDefault.valueint;
        TP_TEXTO: eValor.Text := eValorDefault.Text;
    end;
end;

end.
