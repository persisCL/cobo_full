unit AbmPuntoCobro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util,
  UtilProc, UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, ComCtrls, PeaProcs,
  CheckLst, ADODB, validate, strUtils, Variants, DMConnection,
  DPSControls;

type
  TFormPuntoCobro = class(TForm)
	Panel2: TPanel;
	DBList1: TAbmList;
	AbmToolbar1: TAbmToolbar;
    PuntosCobro: TADOTable;
    PageControl: TPageControl;
    Tab_General: TTabSheet;
    Label1: TLabel;
    Label15: TLabel;
    txt_descripcion: TEdit;
    tab_vias: TTabSheet;
    Label6: TLabel;
    txt_cantidadCarriles: TNumericEdit;
    Label12: TLabel;
	CB_ultimoPorticoSentido: TCheckBox;
	CB_sentidoEstacion: TComboBox;
    Label9: TLabel;
    txt_km: TNumericEdit;
    Label10: TLabel;
    Tab_Tramos: TTabSheet;
    Notebook: TNotebook;
	chk_Tramos: TCheckListBox;
    cb_EjesViales: TComboBox;
    qry_EjesViales: TADOQuery;
    ObtenerTramosPorPuntoDeCobro: TADOStoredProc;
    txt_PuntoDeCobro: TNumericEdit;
    EliminarTramosPorPuntoDeCobro: TADOStoredProc;
    TramosPuntosCobro: TADOTable;
    Tab_Opciones: TTabSheet;
    txt_PathFotosViajes: TEdit;
    txt_PathFotosPendientes: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    cbConcesionaria: TComboBox;
    Label14: TLabel;
    qryPuntosCobro: TADOQuery;
    Label5: TLabel;
    deLoader: TDateEdit;
    cbTSMC: TComboBox;
    Label2: TLabel;
    qryTSMC: TADOQuery;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    Label3: TLabel;
    txt_CodigoPCConcesionaria: TNumericEdit;

	function  DBList1Process(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure DBList1Edit(Sender: TObject);
	procedure DBList1Delete(Sender: TObject);
	procedure DBList1Click(Sender: TObject);
	procedure DBList1Refresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure FormShow(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cbConcesionariaChange(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
    procedure CargarTramos;
    function SeleccionoAlgunTramo: boolean;
    function PuedeGrabar: boolean;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

  resourcestring
    MSG_PUNTO_COBRO = 'Debe indicar el codigo del punto de cobro';
    MSG_DESCRIPCION = 'Debe indicar la descripci�n';
    MSG_EJE_VIAL = 'Debe indicar el eje vial';
    MSG_SENTIDO_ESTACION = 'Debe indicar el sentido';
    MSG_PATH_FOTOS_VIAJES = 'Debe indicar el path de fotograf�as de viajes';
    MSG_PATH_FOTOS_PENDIENTES = 'Debe indicar el path de fotograf�as pendientes';
    MSG_TRAMOS = 'Debe seleccionar por lo menos un tramo.';
    MSG_TSMC = 'Debe ingresar TSMC.';
    MSG_PCCONCESIONARIA ='Debe indicar un codigo de punto de cobro.';
    MSG_PORTICO = 'Se supero el n�mero maximo de porticos.';
    MSG_PCCONCESIONARIA_EXISTE = 'Debe indicar un codigo de punto de cobro que no exista';
    CONST_MAX_PORTICO = '3';

var
  FormPuntoCobro  : TFormPuntoCobro;

implementation

resourcestring
	MSG_YES								= 'Si';
    MSG_ASCENDENTE						= 'Ascendente';
    MSG_DESCENDENTE						= 'Descendente';
	//Puntos de Cobro
    MSG_ACTUALIZAR_CAPTION_PUNTOS_COBRO	= 'Actualizar Punto de Cobro';
	MSG_ACTUALIZAR_ERROR_PUNTOS_COBRO	= 'No se pudieron actualizar los datos del Punto de Cobro.';
	MSG_DELETE_QUESTION_PUNTOS_COBRO	= '�Est� seguro de querer eliminar este Punto de Cobro?';
    MSG_DELETE_ERROR_PUNTOS_COBRO		= 'No se puede eliminar el Punto de Cobro porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION_PUNTOS_COBRO 	= 'Eliminar Punto de Cobro';

    MSG_VALIDAR_DATOS_PUNTOS_COBRO		= 'El C�digo del Punto de Cobro debe ser mayor que cero.';

    //tramos
	MSG_DELETE_ERROR_TRAMOS  			= 'No se pudieron eliminar los Tramos del Punto de Cobro.';
    MSG_DELETE_CAPTION_TRAMOS			= 'Eliminar Tramos';
    MSG_ACTUALIZAR_CAPTION_TRAMOS 	  	= 'Actualizar Tramos del Punto de Cobro';
	MSG_ACTUALIZAR_ERROR_TRAMOS 		= 'No se pudieron actualizar los datos del Tramo del Punto de Cobro.';


{$R *.DFM}

function TFormPuntoCobro.Inicializa: boolean;
Var
	S: TSize;
	i: Integer;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Result := False;
	if not OpenTables([QryPuntosCobro, TramosPuntosCobro, Qry_EjesViales]) then Exit;
	cargarConcesionarias(DMConnections.BaseCAC, cbConcesionaria,
	  QryPuntosCobro.FieldByName('CodigoConcesionaria').asInteger);
	cb_EjesViales.Items.Clear;
	While not qry_EjesViales.Eof do begin
		cb_EjesViales.Items.Add(
		  qry_EjesViales.FieldByName('Descripcion').AsString + Space(201) +
		  qry_EjesViales.FieldByName('CodigoEjeVial').AsString);
		qry_EjesViales.Next;
	end;
	Qry_EjesViales.Close;
	cbConcesionariaChange(cbConcesionaria);
	PageControl.ActivePage := Tab_General;
	notebook.PageIndex := 0;
	DbList1.Reload;
	for i:= 0 to chk_Tramos.Items.Count - 1 do begin
		Chk_Tramos.ItemEnabled[i] := False;
	end;
	Tab_General.Enabled := False;
	Tab_Vias.Enabled 	:= False;
	Tab_Opciones.Enabled := False;
	Result := True;
end;

procedure TFormPuntoCobro.Limpiar_Campos();
begin
	txt_PuntoDeCobro.Clear;
	txt_Descripcion.Clear;
	txt_PathFotosPendientes.clear;
	txt_PathFotosViajes.clear;
	cb_EjesViales.itemIndex := 0;
	txt_CantidadCarriles.Clear;
    txt_CodigoPCConcesionaria.clear;
	txt_km.Clear;
	txt_CantidadCarriles.clear;
	deLoader.Clear;
	cb_sentidoEstacion.itemIndex := 0;
	cb_ultimoPorticoSentido.checked := false;
	txt_km.Clear;
end;

function TFormPuntoCobro.DBList1Process(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	 Result := True;
	 Texto := PADR(Trim(Tabla.FieldByName('NumeroPuntoCobro').AsString), 4, ' ' ) + ' '+
			  trim(PADR(Tabla.FieldByName('Descripcion').AsString, 30, ' ')) + ' ' +
              trim(PADR(Tabla.FieldByName('Concesionaria').AsString, 30, ' '));
//			  PADR(Tabla.FieldByName('DireccionIP').AsString, 20, ' ') + ' '+
//			  PADR(Tabla.FieldByName('CantidadViasA').AsString, 7, ' ') + ' '+
//			  PADR(Tabla.FieldByName('CantidadViasD').AsString, 7, ' ');
end;

procedure TFormPuntoCobro.BtSalirClick(Sender: TObject);
begin
	 Close;
end;


procedure TFormPuntoCobro.DBList1Edit(Sender: TObject);
var
	i: integer;
begin
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
	PageControl.ActivePage := Tab_General;
	Tab_General.Enabled := True;
	Tab_Vias.Enabled := True;
	Tab_Opciones.Enabled := True;
	txt_PuntoDeCobro.enabled := true;
    txt_descripcion.SetFocus;
	deloader.enabled := false;
    For i:= 0 to chk_Tramos.Items.Count - 1 do begin
		Chk_Tramos.ItemEnabled[i] := True;
    end;

    // No se puede editar la concesionaria ni el codigo del punto de Cobro
    cbConcesionaria.Enabled:=false;
    txt_PuntoDeCobro.Enabled:=false;

end;

procedure TFormPuntoCobro.DBList1Delete(Sender: TObject);
begin
{	Screen.Cursor := crHourGlass;
	// Hola
	// Hay que elimiar todos los Datos asociados
	// Cuando se borra un Punto de Cobro

	If MsgBox( MSG_DELETE_QUESTION_PUNTOS_COBRO, MSG_DELETE_CAPTION_PUNTOS_COBRO,
    	 MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		PuntosCobro.open;
		try
			if PuntosCobro.locate('CodigoConcesionaria;NumeroPuntoCobro',
			  VarArrayOf([QryPuntosCobro.FieldByName('CodigoConcesionaria').asInteger,
			  QryPuntosCobro.FieldByName('NumeroPuntoCobro').asInteger]), []) then
				PuntosCobro.Delete;
		Except
			On E: Exception do begin
				PuntosCobro.Cancel;
				MsgBoxErr( MSG_DELETE_ERROR_PUNTOS_COBRO, e.message, MSG_DELETE_CAPTION_PUNTOS_COBRO, MB_ICONSTOP);
			end;
		end;
		PuntosCobro.close;
		DbList1.Reload;
	end;
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	Tab_General.Enabled := False;
	Tab_Vias.Enabled := False;
	Tab_Opciones.Enabled := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;}
end;

procedure TFormPuntoCobro.DBList1Click(Sender: TObject);
var
	i: integer;
begin
	with DBList1.Table do begin
		// Cargamos los datos generales
		for i := 0 to cbConcesionaria.Items.Count - 1 do begin
			if IVal(RightStr(cbConcesionaria.Items[i], 10)) =  FieldByName('CodigoConcesionaria').AsInteger then begin
				cbConcesionaria.ItemIndex := i;
				Break;
			end;
		end;
		txt_PuntoDeCobro.Value	 	  := FieldByName('NumeroPuntoCobro').AsInteger;
		txt_Descripcion.text 		  := FieldByName('Descripcion').AsString;
		cb_EjesViales.ItemIndex := -1;
		for i := 0 to cb_EjesViales.Items.Count - 1 do begin
			if IVal(RightStr(cb_EjesViales.Items[i], 10)) =  FieldByName('CodigoEjeVial').AsInteger then begin
				cb_EjesViales.ItemIndex := i;
				Break;
			end;
		end;

		// Cargamos los datos de distribuci�n
		if FieldByName('Sentido').AsString = 'D' then
			cb_sentidoestacion.itemindex := 1 else cb_sentidoestacion.itemindex := 0;
        txt_CodigoPCConcesionaria.Value  := FieldByName('CodigoPCConcesionaria').AsInteger;
		txt_CantidadCarriles.Value	     := FieldByName('CantidadCarriles').AsInteger;
		txt_km.Value                     := FieldByName('km').AsFloat;
		cb_ultimoPorticoSentido.checked  := FieldByName('ultimoPorticoSentido').AsBoolean;

		// Cargamos la configuraci�n
		txt_PathFotosPendientes.text  := trim(FieldByName('PathFotosPendientes').asString);
		txt_PathFotosViajes.text	  := trim(FieldByName('PathFotosViajes').asString);
		deloader.date                 := FieldByName('UltimaEjecucionLoader').AsDateTime;

		// Cargamos las Concesionarias, segun la concesionaria cargamos los tramos
		cbConcesionariaChange(cbConcesionaria);
	end;
end;

procedure TFormPuntoCobro.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then Limpiar_Campos();
end;

procedure TFormPuntoCobro.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormPuntoCobro.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, tabla.FieldByName('Concesionaria').AsString);
		TextOut(Cols[1], Rect.Top, tabla.FieldByName('NumeroPuntoCobro').AsString);
		TextOut(Cols[2], Rect.Top, tabla.FieldByName('Descripcion').AsString);
		TextOut(Cols[3], Rect.Top, tabla.FieldByName('EjeVial').AsString);
		if tabla.fieldByName('Sentido').asString = 'A' then
			TextOut(Cols[4], Rect.Top, MSG_ASCENDENTE)
		else
			TextOut(Cols[4], Rect.Top, MSG_DESCENDENTE);
		TextOut(Cols[5], Rect.Top, tabla.FieldByName('KM').AsString);
		TextOut(Cols[6], Rect.Top, tabla.FieldByName('CantidadCarriles').AsString);
        TextOut(Cols[8], Rect.Top, tabla.FieldByName('CodigoPCConcesionaria').AsString);
		if tabla.FieldByName('ultimoPorticoSentido').asBoolean then begin
			font.Color := clRed;
			TextOut(Cols[7], Rect.Top, MSG_YES);
		   end
		else
		   begin
			font.Color := clblack;
			TextOut(Cols[7], Rect.Top, 'No');
		   end;

	end;
	sender.Canvas.font.color := clblack;
end;

procedure TFormPuntoCobro.FormShow(Sender: TObject);
begin
	DBList1.Reload;
end;

procedure TFormPuntoCobro.BtnAceptarClick(Sender: TObject);
var
	i: integer;
begin
 	if Trunc(txt_PuntoDeCobro.Value) < 1 then begin
		MsgBox( MSG_VALIDAR_DATOS_PUNTOS_COBRO, MSG_ACTUALIZAR_CAPTION_PUNTOS_COBRO, MB_ICONSTOP);
		txt_PuntoDeCobro.SetFocus;
		Exit;
	end;

    if not PuedeGrabar then exit;

    if not SeleccionoAlgunTramo then begin
        MsgBox( MSG_TRAMOS, MSG_ACTUALIZAR_CAPTION_PUNTOS_COBRO, MB_ICONSTOP);
        Exit;
    end;

	Screen.Cursor := crHourGlass;
	try
		DMConnections.BaseCAC.BeginTrans;
		With PuntosCobro do begin
			Try
				open;
				if DbList1.Estado = Alta then Append else begin
					if locate('CodigoConcesionaria;NumeroPuntoCobro',
						VarArrayOf([QryPuntosCobro.FieldByName('CodigoConcesionaria').asInteger,
						QryPuntosCobro.FieldByName('NumeroPuntoCobro').asInteger]), []) then
						Edit
					else
						exit;
				end;
				// Asignamos Datos Generales
				FieldByName('CodigoConcesionaria').AsInteger  := Ival(RightStr(cbConcesionaria.Text, 10));
				FieldByName('NumeroPuntoCobro').AsInteger 	  := txt_PuntoDeCobro.ValueInt;
				if Trim(txt_Descripcion.text) = '' then
					FieldByName('Descripcion').Clear
				else
					FieldByName('Descripcion').AsString	 	  := Trim(txt_Descripcion.text);
				FieldByName('CodigoEjeVial').asInteger		  := Ival(rightStr(cb_EjesViales.Text, 10));

				// Asignamos opciones
				FieldByName('PathFotosPendientes').AsString   := trim(txt_PathFotosPendientes.text);
				FieldByName('PathFotosViajes').AsString		  := trim(txt_PathFotosViajes.text);
				FieldByName('DomainID').AsInteger	  := IVal(leftStr(RightStr(cbTSMC.Text, 20), 10));
				FieldByName('TSMCID').AsInteger		  := IVal(RightStr(cbTSMC.Text, 10));

				// Asignamos Distribuci�n
				FieldByName('UltimoPorticoSentido').AsBoolean := cb_ultimoPorticoSentido.checked;
				FieldByName('KM').Asfloat 		   	          := txt_km.Value;
				FieldByName('CantidadCarriles').asInteger	  := txt_CantidadCarriles.ValueInt;
                FieldByName('CodigoPCConcesionaria').asInteger	          := txt_CodigoPCConcesionaria.ValueInt;
				if cb_sentidoestacion.itemindex = 0 then
					FieldByName('Sentido').AsString := 'A'
				else begin
					FieldByName('Sentido').AsString := 'D';
				end;

			    //FieldByName('DireccionIP').asString := '1.1.1.1';
				Post;
				close;
			except
				On E: Exception do begin
					Cancel;
					close;
					DMConnections.BaseCAC.RollbackTrans;
					MsgBoxErr( MSG_ACTUALIZAR_ERROR_PUNTOS_COBRO, E.message, MSG_ACTUALIZAR_CAPTION_PUNTOS_COBRO, MB_ICONSTOP);
					exit;
				end;
			end;
		end;
		// Ahora agrego o elimino los tramos del Punto de Cobro.
		// Elimino todos los tramos del punto de cobro.
		try
			EliminarTramosPorPuntoDeCobro.Parameters.ParamByName('@CodigoConcesionaria').Value := IVal(RightStr(cbConcesionaria.Text, 10));
			EliminarTramosPorPuntoDeCobro.Parameters.ParamByName('@NumeroPuntoCobro').Value := Trunc(txt_PuntoDeCobro.Value);
			EliminarTramosPorPuntoDeCobro.ExecProc;
			EliminarTramosPorPuntoDeCobro.Close;
		except
			On E: Exception do begin
				DMConnections.BaseCAC.RollbackTrans;
				MsgBoxErr( MSG_DELETE_ERROR_TRAMOS, E.message, MSG_DELETE_CAPTION_TRAMOS, MB_ICONSTOP);
				EliminarTramosPorPuntoDeCobro.Close;
				exit;
			end;
		end;
		for i:= 0 to chk_tramos.Items.Count - 1 do begin
			if chk_tramos.Checked[i] then begin
				try
					 TramosPuntosCobro.Append;
					 TramosPuntosCobro.FieldByName('CodigoConcesionaria').AsInteger	:= IVal(RightStr(cbConcesionaria.text, 10));
					 TramosPuntosCobro.FieldByName('NumeroPuntoCobro').AsInteger	:= txt_PuntoDeCobro.ValueInt;
					 TramosPuntosCobro.FieldByName('CodigoTramo').AsInteger         := IVal(StrRight(chk_tramos.Items[i], 10));
					 TramosPuntosCobro.Post;
				except
					On E: Exception do begin
						DMConnections.BaseCAC.RollbackTrans;
						MsgBoxErr(MSG_ACTUALIZAR_ERROR_TRAMOS,
                        	E.message, MSG_ACTUALIZAR_CAPTION_TRAMOS, MB_ICONSTOP);
						exit;
					end;
				end;
			end;
		end;
		DMConnections.BaseCAC.CommitTrans;
	finally
		DbList1.Estado     	:= Normal;
		DbList1.Enabled    	:= True;
		Tab_General.Enabled := False;
		Tab_Vias.Enabled 	:= False;
		Tab_Opciones.Enabled := False;
		Notebook.PageIndex 	:= 0;
		DBList1.Reload;
		DbList1.SetFocus;
		Screen.Cursor 	   := crDefault;
	end;
end;

procedure TFormPuntoCobro.BtnCancelarClick(Sender: TObject);
begin
	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	DbList1.SetFocus;
	Tab_General.Enabled := False;
	Tab_Vias.Enabled := False;
	Tab_Opciones.Enabled := False;
	Notebook.PageIndex := 0;
end;

procedure TFormPuntoCobro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormPuntoCobro.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormPuntoCobro.CargarTramos;
var i : integer;
begin
	chk_Tramos.Items.Clear;
	ObtenerTramosPorPuntoDeCobro.Close;
	ObtenerTramosPorPuntoDeCobro.Parameters.ParamByName('@CodigoConcesionaria').Value :=
	  IVal(RightStr(cbConcesionaria.Text, 10));
	ObtenerTramosPorPuntoDeCobro.Parameters.ParamByName('@NumeroPuntoCobro').Value :=
	  txt_PuntoDeCobro.ValueInt;
	if not OpenTables([ObtenerTramosPorPuntoDeCobro]) then Exit;
	if ObtenerTramosPorPuntoDeCobro.Eof then Exit;
	ObtenerTramosPorPuntoDeCobro.First;
	i := 0;
	While not ObtenerTramosPorPuntoDeCobro.Eof do begin
		chk_tramos.Items.Add(
			PadR(ObtenerTramosPorPuntoDeCobro.FieldByName('Descripcion').AsString, 600, ' ') +
			Istr(ObtenerTramosPorPuntoDeCobro.FieldByName('CodigoTramo').AsInteger, 10));
		if ObtenerTramosPorPuntoDeCobro.FieldByName('Pertenece').AsInteger > 0
			then chk_tramos.Checked[i] := True;
		inc(i);
		ObtenerTramosPorPuntoDeCobro.Next;
	end;
	ObtenerTramosPorPuntoDeCobro.Close;
	For i:= 0 to chk_Tramos.Items.Count - 1 do
		Chk_Tramos.ItemEnabled[i] := DbList1.Estado in [Alta, Modi];
end;

procedure TFormPuntoCobro.cbConcesionariaChange(Sender: TObject);
var codigo, i: integer;
begin
	// Cargar los TSMC
	try
		qryTSMC.parameters.paramByName('CodigoConcesionaria').value :=
		  RightStr(cbConcesionaria.Items[cbConcesionaria.itemIndex], 10);
		qryTSMC.open;
		i := 0;
		Codigo := -1;
		cbTSMC.clear;
		While not qryTSMC.Eof do begin
			cbTSMC.Items.Add(format('%s%s%10d%10d',[qryTSMC.FieldByName('Descripcion').AsString, Space(500),
			  qryTSMC.FieldByName('DomainID').AsInteger, qryTSMC.FieldByName('TSMCID').AsInteger]));
			if ((qryPuntosCobro.FieldByName('DomainID').asInteger = qryTSMC.FieldByName('DomainID').AsInteger) and
				(qryPuntosCobro.FieldByName('TSMCID').asInteger = qryTSMC.FieldByName('TSMCID').AsInteger)) then
				Codigo := i
			else
				inc(i);
			qryTSMC.Next;
		end;
		cbTSMC.ItemIndex := Codigo;
		CargarTramos;
	finally
		qryTSMC.Close;
	end;
end;

function TFormPuntoCobro.SeleccionoAlgunTramo: boolean;
var
    ok: boolean;
    i: integer;
begin
    ok := false;
	For i:= 0 to chk_Tramos.Items.Count - 1 do begin
        if Chk_Tramos.Checked[i] then ok := true;
    end;
    result := ok;
end;

function TFormPuntoCobro.PuedeGrabar: boolean;
var
    aux:Integer;
    aux_txt:string;
begin
    result:=True;

    if not ValidateControls(
        [txt_PuntoDeCobro, txt_Descripcion, cb_EjesViales,
        CB_sentidoEstacion, cbTSMC, txt_PathFotosPendientes, txt_PathFotosViajes, txt_CodigoPCConcesionaria],
        [Trim(txt_PuntoDeCobro.text) <> '',
        Trim(txt_Descripcion.text) <> '',
        trim(cb_EjesViales.text) <> '',
        trim(CB_sentidoEstacion.text) <> '',
        trim(cbTSMC.text) <> '',
        Trim(txt_PathFotosPendientes.text) <> '',
        Trim(txt_PathFotosViajes.text) <> '',
        Trim(txt_CodigoPCConcesionaria.Text) <> ''],
        MSG_ACTUALIZAR_CAPTION_PUNTOS_COBRO,
        [MSG_PUNTO_COBRO, MSG_DESCRIPCION, MSG_EJE_VIAL,
        MSG_SENTIDO_ESTACION, MSG_TSMC,
        MSG_PATH_FOTOS_PENDIENTES,
        MSG_PATH_FOTOS_VIAJES,
        MSG_PCCONCESIONARIA]) then begin result:=False; exit; end;


        // La cantidad de porticos no debe exeder al maximo
        aux := strtoint(QueryGetValue(PuntosCobro.Connection,
                            format('SELECT SUM(CONVERT(INT,ultimoPorticoSentido)) AS CountultimoPorticoSentido FROM puntoscobro WHERE CodigoConcesionaria = %s',[RightStr(cbConcesionaria.Text, 10)])
                    ));
        if CB_ultimoPorticoSentido.Checked then aux:=aux+1;

        if aux>strtoint(CONST_MAX_PORTICO) then begin
            MsgBox(MSG_PORTICO, MSG_ACTUALIZAR_CAPTION_PUNTOS_COBRO, MB_ICONSTOP);
            result:=False;
            exit;
        end;


        // el codigo de pc concesionaria no se puede estar duplicado
        aux_txt := trim(QueryGetValue(PuntosCobro.Connection,
                                format('SELECT CodigoPCConcesionaria FROM PuntosCobro WHERE CodigoConcesionaria= %s AND CodigoPCConcesionaria= %s',[trim(RightStr(cbConcesionaria.Text, 10)),trim(txt_CodigoPCConcesionaria.text)])));

        if aux_txt=trim(txt_CodigoPCConcesionaria.Text) then begin
            MsgBox(MSG_PCCONCESIONARIA, MSG_ACTUALIZAR_CAPTION_PUNTOS_COBRO, MB_ICONSTOP);
            result:=false;
        end;
end;

end.
