unit ABMTiposPatente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DateEdit, DB, DBTables, ExtCtrls, DbList, Util, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, PeaProcs, ADODB,DMConnection,
  DPSControls;

type
  TFormTiposPatente = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    TiposPatente: TADOTable;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    txtDescripcion: TEdit;
    txtCodigoTipoPatente: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
    chkdefault: TCheckBox;
    txtMascara: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    txtCaracter: TEdit;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;

	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);

  private
	{ Private declarations }
    procedure Limpiar_Campos;
  public
	{ Public declarations }
	Function Inicializa : boolean;
  end;

var
  FormTiposPatente  : TFormTiposPatente;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar el Tipo de Patente?';
    MSG_DELETE_ERROR		= 'No se puede eliminar el Tipo de Patente porque hay datos que dependen de �l.';
    MSG_DELETE_CAPTION 		= 'Eliminar Tipo de Patente';
    MSG_ACTUALIZAR_ERROR	= 'No se puede actualizar el Tipo de Patente.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Tipo de Patente';
    MSG_DESCRIPCION         = 'Debe ingresar la descripci�n';
    MSG_MASCARA             = 'Debe ingresar la m�scara';
    MSG_CARACTER            = 'Debe ingresar el caracter para completar';
    
{$R *.DFM}

function TFormTiposPatente.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([TiposPatente]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
	end;

end;

procedure TFormTiposPatente.Limpiar_Campos();
begin
	txtCodigoTipoPatente.Clear;
	txtDescripcion.Clear;
    chkDefault.Checked := false;
    txtMascara.Text := '';
    txtCaracter.Text := '';
end;

function TFormTiposPatente.ListaProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
	Result := True;
	Texto :=  PadR(Trim(Tabla.FieldByName('CodigoTipoPatente').AsString), 4, ' ' ) + ' '+
	  Tabla.FieldByName('Descripcion').AsString;
end;

procedure TFormTiposPatente.BtSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormTiposPatente.ListaInsert(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Alta;
	Limpiar_Campos;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txtCodigoTipoPatente.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormTiposPatente.ListaEdit(Sender: TObject);
begin
	Screen.Cursor    := crHourGlass;
	Lista.Estado     := Modi;
	GroupB.Enabled   := True;
	Lista.Enabled    := False;
	Notebook.PageIndex := 1;
	txtCodigoTipoPatente.SetFocus;
	Screen.Cursor    := crDefault;
end;

procedure TFormTiposPatente.ListaDelete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO) =
		IDYES then begin
		try
			TiposPatente.Delete;
		Except
			On E: EDataBaseError do begin
				TiposPatente.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		Lista.Reload;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Screen.Cursor      := crDefault;
end;

procedure TFormTiposPatente.ListaClick(Sender: TObject);
begin
	 with TiposPatente do begin
		  txtCodigoTipoPatente.Text	:= Trim(FieldByName('CodigoTipoPatente').AsString);
		  txtDescripcion.text	:= Trim(FieldByName('Descripcion').AsString);
		  chkdefault.checked	:= FieldByName('TipoPatenteDefault').AsBoolean;
          txtMascara.Text		:= ParseParambyNumber(Trim(FieldByName('Mascara').AsString), 1, ';');
          txtCaracter.Text		:= ParseParambyNumber(Trim(FieldByName('Mascara').AsString), 3, ';');
	 end;
end;

procedure TFormTiposPatente.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos();
end;

procedure TFormTiposPatente.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormTiposPatente.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('CodigoTipoPatente').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormTiposPatente.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

procedure TFormTiposPatente.BtnAceptarClick(Sender: TObject);
begin
    if not ValidateControls([txtDescripcion, txtMascara],
                [trim(txtDescripcion.text) <> '',
                trim(txtMascara.text) <> ''],
                MSG_ACTUALIZAR_CAPTION,
                [MSG_DESCRIPCION, MSG_MASCARA]) then exit;
	Screen.Cursor := crHourGlass;
	With TiposPatente do begin
		Try
			if Lista.Estado = Alta then Append else Edit;
			if chkdefault.checked then QueryExecute(TiposPatente.Connection,
            	'UPDATE TiposPatente SET TipoPatenteDefault = 0');
			FieldByName('CodigoTipoPatente').AsString   := Trim(txtCodigoTipoPatente.Text);
			FieldByName('Descripcion').AsString 	  := Trim(txtDescripcion.Text);
			FieldByName('TipoPatenteDefault').AsBoolean := chkdefault.checked;
            FieldByName('Mascara').AsString 	  	  := Trim(txtMascara.Text) + ';0;' + Trim(txtCaracter.Text);
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message,
                	MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
	Lista.Estado     	:= Normal;
	Lista.Enabled    	:= True;
	GroupB.Enabled		:= False;
	Notebook.PageIndex	:= 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormTiposPatente.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado     := Normal;
	Lista.Enabled    := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormTiposPatente.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormTiposPatente.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
