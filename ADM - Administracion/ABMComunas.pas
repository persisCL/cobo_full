{-----------------------------------------------------------------------------
 File Name: ABMComunas
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 19/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura



Firma       :   SS_1147_MBE_20140814
Description :   Se agrega la validaci�n de que el Insertar o Modificar una comuna, �sta
                no exista en otro registro para el mismo pa�s y regi�n.

                Se elimina la funci�n ComunaConcalles, pues s�lo busca por el c�digo de comuna
                sin considerar el c�digo de regi�n ni de pais.

                Se corrige la obtenci�n del nuevo c�digo de comuna (MAX) + 1, pues s�lo trae dos d�gitos

                Se corrige que con cada click, cargue nuevamente las combobox de pa�s y regi�n


-----------------------------------------------------------------------------}

unit ABMComunas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes;

type
  TFormComunas = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Panel2: TPanel;
    txt_descripcion: TEdit;
	Label2: TLabel;
    Label3: TLabel;
    txt_CodigoComuna: TEdit;
    Notebook: TNotebook;
    Label8: TLabel;
    Label9: TLabel;
    BuscaTablaPaises: TBuscaTabla;
    BuscaTablaRegiones: TBuscaTabla;
    qry_Paises: TADOQuery;
    qry_Regiones: TADOQuery;
    Comunas: TADOTable;
    cb_pais: TComboBox;
    cb_regiones: TComboBox;
    Label1: TLabel;
    txt_CP: TNumericEdit;
    //spActualizarRegionEnCalle: TADOStoredProc;					//TASK_111_JMA_20170306
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    Label4: TLabel;
    txtCiudad: TEdit;
    txtPais: TEdit;													//TASK_111_JMA_20170306
    txtRegion: TEdit;												//TASK_111_JMA_20170306
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cb_paisChange(Sender: TObject);
    procedure txt_descripcionKeyPress(Sender: TObject; var Key: Char);
    procedure cb_regionesChange(Sender: TObject);
  private
	{ Private declarations }

	procedure Limpiar_Campos;
  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean; CodigoPais: AnsiString = ''; CodigoRegion: AnsiString = ''): Boolean;
  end;

var
  FormComunas: TFormComunas;

implementation

resourcestring
    MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar la Comuna seleccionada?';
    MSG_DELETE_ERROR		= 'No se pudo eliminar la Comuna seleccionada porque existen datos relacionados';
    MSG_DELETE_CAPTION		= 'Eliminar Comuna';
    MSG_ACTUALIZAR_ERROR 	= 'No se pudieron actualizar los datos de la Comuna seleccionada.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Comuna';
    MSG_PAIS                = 'Debe seleccionar el pa�s';
    MSG_REGION              = 'Debe seleccionar la regi�n';
    MSG_CODIGO              = 'Debe ingresar el c�digo';
    MSG_DESCRIPCION         = 'Debe ingresar el descripci�n';

{$R *.DFM}


{--------------------------------------------------------------                                                                 //SS_1147_MBE_20140814
        IndexComboPorCodigo                                                                                                     //SS_1147_MBE_20140814
                                                                                                                                //SS_1147_MBE_20140814
Author      : mbecerra                                                                                                          //SS_1147_MBE_20140814
Date        : 12-Agosto-2014                                                                                                    //SS_1147_MBE_20140814
Description : Devuelva el valor de la posici�n en que se encuentra el registro                                                  //SS_1147_MBE_20140814
                cuyo c�digo coincide con c�digo pasado por par�metro                                                            //SS_1147_MBE_20140814
                                                                                                                                //SS_1147_MBE_20140814
                cada registro de la comboBox se asume del tipo  Glosa Space(200) Codigo                                         //SS_1147_MBE_20140814
}                                                                                                                               //SS_1147_MBE_20140814
function IndexComboPorCodigo(cb : TComboBox; Codigo : string) : Integer;                                                        //SS_1147_MBE_20140814
var                                                                                                                             //SS_1147_MBE_20140814
  Index : Integer;                                                                                                              //SS_1147_MBE_20140814
begin                                                                                                                           //SS_1147_MBE_20140814
	Index := cb.Items.Count - 1;                                                                                                //SS_1147_MBE_20140814
	While (Index <> -1) And (Trim(Copy(Cb.Items[Index], 200, Length(cb.Items[index]))) <> Codigo) do  Dec (Index);              //SS_1147_MBE_20140814
                                                                                                                                //SS_1147_MBE_20140814
	if (Index < 0) then Index := -1;                                                                                            //SS_1147_MBE_20140814
                                                                                                                                //SS_1147_MBE_20140814
    Result := Index;                                                                                                            //SS_1147_MBE_20140814
                                                                                                                                //SS_1147_MBE_20140814
end;                                                                                                                            //SS_1147_MBE_20140814

function TFormComunas.Inicializa(MDIChild: Boolean; CodigoPais: AnsiString = ''; CodigoRegion: AnsiString = ''): Boolean;
Var
	S: TSize;
begin
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
   	if (CodigoPais <> '') or (CodigoRegion <> '') then begin
		Comunas.Filtered := True;
        if (CodigoPais <> '') then Comunas.Filter := 'CodigoPais=''' + CodigoPais+ '''';
        if (CodigoPais <> '') and (CodigoRegion <> '') then Comunas.Filter := Comunas.Filter + ' and CodigoRegion = ''' + CodigoRegion+ ''''
        else if (CodigoRegion <> '') then Comunas.Filter := 'CodigoRegion = ''' + CodigoRegion+ '''';
    end;


	if not OpenTables([Comunas, qry_Paises]) then Exit;

    if CodigoPais = '' then CodigoPais := PAIS_CHILE;
    CargarPaises(DMConnections.BaseCAC,cb_pais,CodigoPais);
    CargarRegiones(DMConnections.BaseCAC,cb_regiones,CodigoPais,'');
	Result := True;
	DbList1.Reload;
  	Notebook.PageIndex := 0;
end;

procedure TFormComunas.BtnCancelarClick(Sender: TObject);
begin
   	DbList1.Estado      := Normal;
	DbList1.Enabled     := True;
    cb_pais.Enabled     := True;
    cb_regiones.Enabled := True;

	DbList1.SetFocus;
	Notebook.PageIndex := 0;
	groupb.Enabled     := False;
end;

procedure TFormComunas.DBList1Click(Sender: TObject);
begin
	With DbList1 do begin
//        CargarPaises(DMConnections.BaseCAC, cb_pais, Table.FieldByName('CodigoPais').AsString);                //SS_1147_MBE_20140814
        cb_pais.ItemIndex       := IndexComboPorCodigo(cb_pais, Table.FieldByName('CodigoPais').AsString);       //SS_1147_MBE_20140814
        txtPais.Text := cb_pais.Text;                                                                                      //TASK_111_JMA_20170306

        //La ventana no est� filtrada por pa�ses, por lo que hay que cargar todas las regiones del pa�s          //SS_1147_MBE_20140814
        //CargarRegiones(DMConnections.BaseCAC,cb_regiones,Table.FieldByName('CodigoPais').AsString,               //SS_1147_MBE_20140814           //TASK_111_JMA_20170306
        //                Table.FieldByName('CodigoRegion').AsString);                                             //SS_1147_MBE_20140814           //TASK_111_JMA_20170306
        cb_regiones.ItemIndex := IndexComboPorCodigo(cb_pais, Split(' ',cb_pais.Text)[1]);                                                          //TASK_111_JMA_20170306
        txtRegion.Text := cb_regiones.Text;                                                                                                         //TASK_111_JMA_20170306

        txt_CodigoComuna.Text   := table.FieldByName('CodigoComuna').AsString;
        txt_Descripcion.Text    := table.FieldByName('Descripcion').AsString;
        //txt_CP.Value            := StrToInt('0'+trim(table.FieldByName('CodigoPostal').AsString));               //TASK_111_JMA_20170306
        txtCiudad.Text          := table.FieldByName('Ciudad').AsString;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBList1DrawItem
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TDBList; Tabla: TDataSet; Rect: TRect;
              State: TOwnerDrawState; Cols: TColPositions
  Return Value: Sender: TObject

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}

procedure TFormComunas.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0] + 1, Rect.Top,  QueryGetValue(Comunas.Connection, 'SELECT Descripcion FROM Paises  WITH (NOLOCK) WHERE CodigoPais = ''' +  Tabla.FieldByName('CodigoPais').AsString + ''''));
		TextOut(Cols[1], Rect.Top, QueryGetValue(Comunas.Connection,
    		  'SELECT Descripcion FROM Regiones  WITH (NOLOCK) WHERE CodigoPais = ''' +
    		  Tabla.FieldByName('CodigoPais').AsString + ''' AND CodigoRegion = ''' +
    		  Tabla.FieldByName('CodigoRegion').AsString + ''''));
		TextOut(Cols[2], Rect.Top, FieldByName('Descripcion').AsString);
        //TextOut(Cols[3], Rect.Top, Trim(FieldByName('CodigoPostal').AsString));          //TASK_111_JMA_20170306
        //TextOut(Cols[4], Rect.Top, Trim(FieldByName('Ciudad').AsString));                //TASK_111_JMA_20170306
        TextOut(Cols[3], Rect.Top, Trim(FieldByName('Ciudad').AsString));                //TASK_111_JMA_20170306
	end;
end;

procedure TFormComunas.DBList1Edit(Sender: TObject);

    function ExistenCallesRelaccionadas: Boolean;
    //Devuelve True si existen vehiculos relacionados con el tipo actual
        resourcestring                                                                                           //SS_1147_MBE_20140814
            MSG_SQL_EXISTE =    'SELECT 1 FROM MaestroCalles (NOLOCK) ' +                                        //SS_1147_MBE_20140814
                                'WHERE CodigoPais = ''%s'' AND CodigoRegion = ''%s'' ' +                         //SS_1147_MBE_20140814
                                '  AND CodigoComuna = ''%s''';                                                 //SS_1147_MBE_20140814
        var                                                                                                      //SS_1147_MBE_20140814
            vExiste : Integer;                                                                                   //SS_1147_MBE_20140814
            vSQL : string;                                                                                       //SS_1147_MBE_20140814
    begin
        {                                                                                                        //SS_1147_MBE_20140814
        with Comunas do                                                                                          //SS_1147_MBE_20140814
            Result := StrtoBool(QueryGetValue(DMConnections.BaseCAC,                                             //SS_1147_MBE_20140814
                                              'select dbo.ComunaConCalle(''' +                                   //SS_1147_MBE_20140814
                                              FieldByName('CodigoPais').AsString + ''',''' +                     //SS_1147_MBE_20140814
                                              FieldByName('CodigoComuna').AsString + ''',''' +                   //SS_1147_MBE_20140814
                                              FieldByName('CodigoRegion').AsString + ''')'));                    //SS_1147_MBE_20140814
        }                                                                                                        //SS_1147_MBE_20140814

        vSQL := Format(MSG_SQL_EXISTE, [    Comunas.FieldByName('CodigoPais').AsString,                          //SS_1147_MBE_20140814
                                            Comunas.FieldByName('CodigoRegion').AsString,                        //SS_1147_MBE_20140814
                                            Comunas.FieldByName('CodigoComuna').AsString ] );                    //SS_1147_MBE_20140814
        vExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);                                                //SS_1147_MBE_20140814
        Result := (vExiste = 1);                                                                                 //SS_1147_MBE_20140814
    end;

begin
	DbList1.Enabled     := False;
    dblist1.Estado      := modi;
	Notebook.PageIndex  := 1;
    groupb.Enabled      := True;
    cb_pais.Enabled     := not ExistenCallesRelaccionadas;
    cb_regiones.Enabled := cb_pais.Enabled;
    txt_descripcion.setFocus;
end;

procedure TFormComunas.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then begin
        txtPais.Clear;              //TASK_111_JMA_20170306
        txtRegion.Clear;            //TASK_111_JMA_20170306
		txt_CodigoComuna.Clear;
		txt_Descripcion.Clear;
        //txt_CP.Clear;				//TASK_111_JMA_20170306
        txtCiudad.Clear;
	 end;
end;

procedure TFormComunas.Limpiar_Campos();
begin
	txt_CodigoComuna.Clear;
  	txt_Descripcion.Clear;
    txt_CP.Clear;
    txtCiudad.Clear;
    CargarPaises(DMConnections.BaseCAC,cb_pais,PAIS_CHILE);
    CargarRegiones(DMConnections.BaseCAC,cb_regiones,PAIS_CHILE, REGION_SANTIAGO);
end;

procedure TFormComunas.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TFormComunas.DBList1Delete(Sender: TObject);

    function ExistenCallesRelacionadas: Boolean;
    //Devuelve True si existen calles relacionadas con la comuna actual
        resourcestring                                                                                           //SS_1147_MBE_20140814
            MSG_SQL_EXISTE =    'SELECT 1 FROM MaestroCalles (NOLOCK) ' +                                        //SS_1147_MBE_20140814
                                'WHERE CodigoPais = ''%s'' AND CodigoRegion = ''%s'' ' +                         //SS_1147_MBE_20140814
                                '  AND CodigoComuna = ''%s'' 0';                                                 //SS_1147_MBE_20140814
        var                                                                                                      //SS_1147_MBE_20140814
            vExiste : Integer;                                                                                   //SS_1147_MBE_20140814
            vSQL : string;                                                                                       //SS_1147_MBE_20140814
    begin
        {                                                                                                        //SS_1147_MBE_20140814
        Result := StrtoBool(QueryGetValue(DMConnections.BaseCAC,                                                 //SS_1147_MBE_20140814
                                          'select dbo.ComunaConCalles(''' + txt_CodigoComuna.Text + ''')'));     //SS_1147_MBE_20140814
        }                                                                                                        //SS_1147_MBE_20140814

        vSQL := Format(MSG_SQL_EXISTE, [    Comunas.FieldByName('CodigoPais').AsString,                          //SS_1147_MBE_20140814
                                            Comunas.FieldByName('CodigoRegion').AsString,                        //SS_1147_MBE_20140814
                                            Comunas.FieldByName('CodigoComuna').AsString ] );                    //SS_1147_MBE_20140814
        vExiste := QueryGetValueInt(DMConnections.BaseCAC, vSQL);                                                //SS_1147_MBE_20140814
        Result := (vExiste = 1);                                                                                 //SS_1147_MBE_20140814

    end;

resourcestring
    MSG_ERROR_RELATED_RECORDS = 'No se pudo eliminar la Comuna seleccionada porque existen Calles relacionadas';
begin
    if (ExistenCallesRelacionadas) then begin
        MsgBox(MSG_ERROR_RELATED_RECORDS, MSG_DELETE_CAPTION, MB_ICONSTOP);
        DbList1.Estado  := Normal;
        DbList1.Enabled := True;
        Exit;
    end;

    Screen.Cursor := crHourGlass;
    try
        If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            try
                Comunas.Delete;
            Except
                On E: Exception do begin
                    Comunas.Cancel;
                    MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                end;
            end;
            DbList1.Reload;
        end;
        DbList1.Estado  := Normal;
        DbList1.Enabled := True;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBList1Insert
  Author:
  Date Created:  /  /
  Description:
  Parameters: None
  Return Value: Sender: TObject

  Revision : 1
    Date: 19/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura



-----------------------------------------------------------------------------}
procedure TFormComunas.DBList1Insert(Sender: TObject);
begin
    groupb.Enabled     := True;
	Limpiar_Campos;
	DbList1.Enabled    := False;
	Notebook.PageIndex := 1;
    cb_pais.ItemIndex := -1;
    cb_regiones.ItemIndex := -1;
//    txt_CodigoComuna.Text:=Trim(QueryGetValue(DMConnections.BaseCAC,'SELECT MAX(CodigoComuna)+1 AS CodigoComuna From Comunas  WITH (NOLOCK) '));     //SS_1147_MBE_20140814
    txt_CodigoComuna.Text := '';                                                                             //SS_1147_MBE_20140814
	txt_descripcion.SetFocus;
end;

procedure TFormComunas.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CONFIRM_CHANGE_REGION = 'Al modificar la Regi�n de la Comuna se modificar�n tambien ' +
                                'las regiones de las Calles relacionadas.'#13#13'�Confirma continuar?';
    MSG_YA_EXISTE   = 'Ya existe una Comuna con esta Descripci�n';                                           //SS_1147_MBE_20140814
    MSG_SQL_INSERT  =   'SELECT 1 FROM Comunas (NOLOCK) WHERE CodigoPais = ''%s'' ' +                        //SS_1147_MBE_20140814
                        '   AND CodigoRegion = ''%s'' AND Descripcion = ''%s'' ';                            //SS_1147_MBE_20140814
    MSG_SQL_UPDATE  = ' AND CodigoComuna <> ''%s'' ';                                                        //SS_1147_MBE_20140814

var
    vSQL : string;                                                                                           //SS_1147_MBE_20140814
    vExiste : Boolean;                                                                                       //SS_1147_MBE_20140814
begin
    if not ValidateControls(
        [cb_pais,
        cb_regiones,
        txt_CodigoComuna,
        txt_descripcion],
        [Trim(cb_pais.text) <> '',
		Trim(cb_regiones.text) <> '',
        trim(txt_CodigoComuna.text) <> '',
        trim(txt_descripcion.text) <> ''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_PAIS,
        MSG_REGION,
        MSG_CODIGO,
        MSG_DESCRIPCION]) then exit;

    //ver que no exista otra comuna con el mismo nombre                                                      //SS_1147_MBE_20140814
    if DBList1.Estado = Alta then begin                                                                      //SS_1147_MBE_20140814
        vSQL := Format(MSG_SQL_INSERT, [StrRight(cb_pais.Text,3), StrRight(cb_regiones.Text,3), txt_Descripcion.Text]);                    //SS_1147_MBE_20140814
    end                                                                                                      //SS_1147_MBE_20140814
    else begin                                                                                               //SS_1147_MBE_20140814
        vSQL := MSG_SQL_INSERT + MSG_SQL_UPDATE;                                                             //SS_1147_MBE_20140814
        vSQL := Format(vSQL, [  Comunas.FieldByName('CodigoPais').AsString,                                  //SS_1147_MBE_20140814
                                Comunas.FieldByName('CodigoRegion').AsString,                                //SS_1147_MBE_20140814
                                Trim(txt_Descripcion.Text),                                                  //SS_1147_MBE_20140814
                                Comunas.FieldByName('CodigoComuna').AsString] );                             //SS_1147_MBE_20140814

    end;                                                                                                     //SS_1147_MBE_20140814

    vExiste := (QueryGetValueInt(DMConnections.BaseCAC, vSQL) = 1);                                          //SS_1147_MBE_20140814
    if vExiste then begin                                                                                    //SS_1147_MBE_20140814
        MsgBoxBalloon(MSG_YA_EXISTE, Caption, MB_ICONEXCLAMATION, txt_Descripcion);                          //SS_1147_MBE_20140814
        Exit;                                                                                                //SS_1147_MBE_20140814
    end;                                                                                                     //SS_1147_MBE_20140814


 	Screen.Cursor := crHourGlass;
	With Comunas do begin
		Try
			if DbList1.Estado = Alta then Append
            else Edit;
			FieldByName('CodigoPais').AsString  	     := Trim(StrRight(cb_pais.Text, 20));
            FieldByName('CodigoRegion').AsString  	     := Trim(StrRight(cb_regiones.Text, 20));
			FieldByName('Descripcion').AsString  	     := Trim(txt_descripcion.text);
			FieldByName('DescripcionInterfase').AsString := Trim(txt_descripcion.text);
			FieldByName('CodigoComuna').AsString 	     := Trim(txt_codigoComuna.Text);
            FieldByName('CodigoPostal').AsString 	     := iif(txt_CP.ValueInt=0,'',Trim(txt_CP.Text));
            FieldByName('Ciudad').AsString				 := txtCiudad.Text;
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
		end;
	end;
    groupb.Enabled     := False;
  	DbList1.Estado     := Normal;
	DbList1.Enabled    := True;
	Notebook.PageIndex := 0;
	DBList1.Reload;
	DbList1.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormComunas.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormComunas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormComunas.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormComunas.cb_paisChange(Sender: TObject);
begin
  CargarRegiones(DMConnections.BaseCAC,cb_regiones,Trim(StrRight(cb_pais.Text, 20)),'');
end;

{------------------------------------------
                cb_regionesChange

Author      : mbecerra
Date        : 14-08-2014
Description :   SS_1147_MBE_20140814
                Obtiene el siguiente c�digo de comuna en caso de ser una alta

-----------------------------------------------}
procedure TFormComunas.cb_regionesChange(Sender: TObject);
resourcestring
    MSG_SQL_CODIGO  =   'SELECT ISNULL(MAX(CodigoComuna),0) + 1 FROM Comunas WITH (NOLOCK) ' +
                        ' WHERE CodigoPais = ''%s'' AND CodigoRegion = ''%s'' ';

var
    vCodigo : Integer;
    vSQL : string;
begin
    //obtiene el siguiente c�digo de regi�n en caso de ser una alta
    if (DBList1.Estado = Alta) and (cb_pais.ItemIndex >= 0) and (cb_regiones.ItemIndex >= 0) then begin
        vSQL := Format(MSG_SQL_CODIGO, [StrRight(cb_pais.Text,3), StrRight(cb_regiones.Text,3)]);
        vCodigo := QueryGetValueInt(DMConnections.BaseCAC, vSQL);
        txt_CodigoComuna.Text := Format('%.03d', [vCodigo]);
    end;                                                                                        


end;

procedure TFormComunas.txt_descripcionKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key:=UpCase(Key);
end;

end.
