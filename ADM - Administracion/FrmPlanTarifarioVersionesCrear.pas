unit FrmPlanTarifarioVersionesCrear;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB,VariantComboBox,
  DmiCtrls,Util,DMConnection, Validate, DateEdit, peaprocsCN, UtilProc,							//TASK_109_JMA_20170215
  CategoriasClases, Concesionarias, PeaProcs, PlanesTarifarios, BandasHorarias;               	//TASK_109_JMA_20170215

type
  TFormPlanTarifarioVersionesCrear = class(TForm)
{INICIO: TASK_109_JMA_20170215}
    pnlConcesionaria: TPanel;
    cbbConcesionaria: TVariantComboBox;
{TERMINO: TASK_109_JMA_20170215}
    lblConsecionaria: TLabel;
	//vcbfrmConcesionarias: TVariantComboBox;		//TASK_109_JMA_20170215
    pnlGenerar: TPanel;								//TASK_109_JMA_20170215
    lblCategoriaClases: TLabel;
{INICIO: TASK_109_JMA_20170215
   	vcbFrmClaseCategorias: TVariantComboBox;
    lblFechaHabilitacion: TLabel;
    dtFechaActivar: TDateEdit;
    btnGuardar: TButton;
    btnCancela: TButton;
    spConcesionaria_Obtener: TADOStoredProc;
    spCategoriaClases_Obtener: TADOStoredProc;
    spPlanTarifarioVersiones_Agregar: TADOStoredProc;
    lblMensaje: TLabel;
    spPlanTarifarioVersiones_Copiar: TADOStoredProc;
    procedure btnGuardarClick(Sender: TObject);
}
	lblFechaActivacion: TLabel;
    cbbClaseCategorias: TVariantComboBox;
    edtFechaActivacion: TDateEdit;
    pnlPlantilla: TPanel;
    cbbEsquema: TVariantComboBox;
    lblEsquema: TLabel;
    pnlBotones: TPanel;
    btnGenerar: TButton;
    btnCancelar: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
    procedure cbbClaseCategoriasChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
{INICIO: TASK_109_JMA_20170215
    procedure Concesionaria_Obtener;
    procedure CategoriaClase_Obtener;
}
    oPlanTarifario : TPlanTarifarioVersion;
    oEsquema: TEsquema;
    FMulticoncesionHabilitada: Boolean;
    FPlanSeleccionado : string;

    FAccion: Byte;
    FFechaActual: TDateTime;
    procedure CargarComboConcesionarias(oConcesionaria: TConcesionaria);
    procedure CargarComboCategoriasClases(oCategoriaClase: TCategoriaClase);
    procedure CargarComboEsquemas();
{TERMINO: TASK_109_JMA_20170215}
  public
    { Public declarations }
    //Function Inicializa(_ConcesionariaIndex: Integer; _CategoriaClaseIndex: Integer; _fechaActivacion: TdateTime;ID_PlanTarifarioVersion_Original: Integer;_Copia: Boolean): Boolean; //TASK_109_JMA_20170215
    Function Inicializa(FMulticoncesion: Boolean; CodigoConcesionaria: Integer; oConcesionaria: TConcesionaria; oCategoriaClase: TCategoriaClase; oPlanTarifarioVersion: TPlanTarifarioVersion; accion: Byte; FechaActual: TDateTime): Boolean;
  end;

var
  FormPlanTarifarioVersionesCrear: TFormPlanTarifarioVersionesCrear;
  dtfrmFechaActivacion : TDateTime;
  ID_PlanTarifarioVersionOLD: Integer;
  _GenerarCopia: Boolean;
  _ConcesionariaIndex: Integer;
  _CategoriaClaseIndex: Integer;
  _ConcesionariaValue: Integer;
  _CategoriaClaseValue: Integer;
implementation

ResourceString

	MSG_CAPTION_VALIDAR				      = 'Validar datos ingresados';
  MSG_ERROR_FECHA_INCORRECTA		  = 'La Fecha de Activaci�n es incorrecta o es igual o anterior a la fecha actual';
  MSG_SELECCIONE                  = ' Seleccione ';

{$R *.dfm}
{INICIO: TASK_109_JMA_20170215
Function TFormPlanTarifarioVersionesCrear.Inicializa(_ConcesionariaIndex: Integer; _CategoriaClaseIndex: Integer; _fechaActivacion: TdateTime;ID_PlanTarifarioVersion_Original: Integer; _Copia: Boolean): Boolean;
begin

    Concesionaria_Obtener;
    CategoriaClase_Obtener;

    _GenerarCopia := _Copia;
    if _Copia then  begin

      ID_PlanTarifarioVersionOLD :=  ID_PlanTarifarioVersion_Original;
      dtFechaActivar.Date := _fechaActivacion;

      if _ConcesionariaIndex <> 0 then  vcbfrmConcesionarias.ItemIndex := _ConcesionariaIndex;
      if _CategoriaClaseIndex <> 0 then  vcbFrmClaseCategorias.ItemIndex := _CategoriaClaseIndex;

      vcbfrmConcesionarias.Enabled := False;
      vcbFrmClaseCategorias.Enabled := False;

      lblMensaje.Caption := 'Generar copia del Plan Tarifario';
    end
    else
    begin
      lblMensaje.Caption := 'Crear nueva versi�n del Plan Tarifario'
    end;
    
    Result := True;

end;

procedure TFormPlanTarifarioVersionesCrear.Concesionaria_Obtener;
var
  lEof: Boolean;
begin
  vcbfrmConcesionarias.Clear;
  vcbfrmConcesionarias.Items.Add( MSG_SELECCIONE, '0');
     with spConcesionaria_Obtener do begin

    Parameters.CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 10, 0);
    Parameters.Refresh;

    if Active then Close;
      Open;
    lEof := not EOF;
    if not EOF then begin
      while not EOF do begin
        vcbfrmConcesionarias.Items.Add(
                FieldByName('Descripcion').AsString,
                FieldByName('CodigoConcesionaria').AsInteger );
        Next;
      end;
    end else begin
      vcbfrmConcesionarias.Clear;
    end;
  end;
  spConcesionaria_Obtener.Close;
  spConcesionaria_Obtener.Free;
  if lEof then vcbfrmConcesionarias.ItemIndex := 0;
end;

procedure TFormPlanTarifarioVersionesCrear.CategoriaClase_Obtener;
var
  lEof: Boolean;
begin
  vcbFrmClaseCategorias.Clear;
  vcbFrmClaseCategorias.Items.Add( MSG_SELECCIONE, '0');

  with spCategoriaClases_Obtener do begin
    Parameters.Refresh;

    if Active then Close;
      Open;

    lEof := not EOF;
    if not EOF then begin
      while not EOF do begin
        vcbFrmClaseCategorias.Items.Add(
                  FieldByName('Descripcion').AsString,
                  FieldByName('ID_CategoriasClases').AsInteger );
        Next;
      end;
    end else begin
      vcbFrmClaseCategorias.Clear;
    end;
  end;
  spCategoriaClases_Obtener.Close;
  //spCategoriaClases_Obtener.Free;
  if lEof then vcbFrmClaseCategorias.ItemIndex := 0;
end;


//TASK_17_FSI
procedure TFormPlanTarifarioVersionesCrear.btnGenerarClick(Sender: TObject);
begin
  // Validar Parametros <> ''
  if (vcbfrmConcesionarias.ItemIndex > 0) then begin

     if (vcbFrmClaseCategorias.ItemIndex > 0) then begin

         if not dtFechaActivar.IsEmpty then begin
          if (dtFechaActivar.Date > Date) then begin

            //setear valores para enviar a Insertar
            dtfrmFechaActivacion := dtFechaActivar.Date;
            _ConcesionariaIndex:= vcbfrmConcesionarias.ItemIndex;
            _CategoriaClaseIndex:= vcbFrmClaseCategorias.ItemIndex;
            _ConcesionariaValue:= vcbfrmConcesionarias.Value;
            _CategoriaClaseValue:= vcbFrmClaseCategorias.Value;
            ModalResult := mrOK;
            
          end else begin
            MsgBox('La fecha de habilitaci�n debe ser mayor a la fecha actual', 'Atenci�n');
          end;
        end else begin
          MsgBox('Debe ingresar una fecha de habilitaci�n valida ', 'Atenci�n');
        end;
     end else begin
        MsgBox('Debe seleccionar una Clase de Categorias ', 'Atenci�n');
     end;
  end else begin
    MsgBox('Debe seleccionar una Concesionaria ', 'Atenci�n');
  end;
end;
}

Function TFormPlanTarifarioVersionesCrear.Inicializa(FMulticoncesion: Boolean; CodigoConcesionaria: Integer; oConcesionaria: TConcesionaria; oCategoriaClase: TCategoriaClase; oPlanTarifarioVersion: TPlanTarifarioVersion; accion: Byte; FechaActual: TDateTime): Boolean;
var
  I: Integer;
begin
    oPlanTarifario := oPlanTarifarioVersion;
    FPlanSeleccionado := oPlanTarifario.ClientDataSet.Bookmark;
    FMulticoncesionHabilitada := FMulticoncesion;
    FAccion:= accion;
    FFechaActual:= FechaActual;

    CargarComboConcesionarias(oConcesionaria);
    for I := 0 to cbbConcesionaria.Items.Count - 1 do
    begin
        if cbbConcesionaria.Items[i].Value = CodigoConcesionaria then
        begin
            cbbConcesionaria.ItemIndex := i;
            Break;
        end;
    end;    
    if FMulticoncesionHabilitada then
        pnlConcesionaria.Visible := True;

    CargarComboCategoriasClases(oCategoriaClase);

    CargarComboEsquemas();

    if accion = 1 then
        self.Caption := 'Nuevo Plan Tarifario'
    else
    begin
        cbbClaseCategorias.ItemIndex := cbbClaseCategorias.Items.IndexOf(oPlanTarifario.CategoriaClase);
        cbbClaseCategorias.Enabled := False;
        edtFechaActivacion.Date := oPlanTarifario.FechaActivacion;
        pnlPlantilla.Visible:= False;
        
        if accion = 2 then
            self.Caption := 'Copiar Plan Tarifario'
        else
        begin
            if FMulticoncesionHabilitada then
                cbbConcesionaria.Enabled := False;

            cbbClaseCategorias.Enabled := False;

            if (oPlanTarifario.FechaActivacion <= FFechaActual) and oPlanTarifario.Habilitado then
                edtFechaActivacion.Enabled := False;

            self.Caption := 'Editar Plan Tarifario';
        end;
    end;

    Result := True;

end;  

procedure TFormPlanTarifarioVersionesCrear.CargarComboConcesionarias(oConcesionaria: TConcesionaria);
var
    i: integer;
begin
    cbbConcesionaria.Enabled := False;
    cbbConcesionaria.Items.Clear;
    cbbConcesionaria.Items.InsertItem('Todos', 0);

    try
        try
            oConcesionaria.ClientDataSet.First;
            for i := 0 to oConcesionaria.ClientDataSet.RecordCount - 1 do
            begin
                cbbConcesionaria.Items.InsertItem(oConcesionaria.Descripcion, oConcesionaria.CodigoConcesionaria);
                oConcesionaria.ClientDataSet.Next;
            end;     
        except
            on e:Exception do
                MsgBoxErr('Error cargando Combo Concesionarias', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        cbbConcesionaria.ItemIndex:= 0;
        cbbConcesionaria.Enabled := True;
    end;
end;

procedure TFormPlanTarifarioVersionesCrear.CargarComboEsquemas();
var
    i: integer;   
begin
    cbbEsquema.Enabled := False;
    cbbEsquema.Items.Clear;
    cbbEsquema.Items.InsertItem('Sin Plantilla', 0);

    try
        try
            if oEsquema = nil then
            begin
                oEsquema := TEsquema.Create;
                oEsquema.Obtener(0, true);
            end;
            oEsquema.ClientDataSet.Filter := ' ID_CategoriasClases = ' + Chr(39) + IntToStr(cbbClaseCategorias.Value) + Chr(39);
            oEsquema.ClientDataSet.First;
            for i := 0 to oEsquema.ClientDataSet.RecordCount - 1 do
            begin
                cbbEsquema.Items.InsertItem(oEsquema.Descripcion, oEsquema.CodigoEsquema);
                oEsquema.ClientDataSet.Next;
            end;
            
        except
            on e:Exception do
                MsgBoxErr('Error cargando Combo Plantillas', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        cbbEsquema.ItemIndex:= 0;
        cbbEsquema.Enabled := True;
    end;
end;

procedure TFormPlanTarifarioVersionesCrear.cbbClaseCategoriasChange(Sender: TObject);
begin
    if cbbClaseCategorias.Enabled then    
         CargarComboEsquemas();
end;

procedure TFormPlanTarifarioVersionesCrear.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    FreeAndNil(oEsquema);
end;

procedure TFormPlanTarifarioVersionesCrear.CargarComboCategoriasClases(oCategoriaClase: TCategoriaClase);
var
    i: integer;
begin
    cbbClaseCategorias.Enabled := False;
    cbbClaseCategorias.Items.Clear;
    cbbClaseCategorias.Items.InsertItem('Todos', 0);

    try
        try
            oCategoriaClase.ClientDataSet.First;
            for i := 0 to oCategoriaClase.ClientDataSet.RecordCount - 1 do
            begin
                cbbClaseCategorias.Items.InsertItem(oCategoriaClase.Descripcion, oCategoriaClase.Id_CategoriasClases);
                oCategoriaClase.ClientDataSet.Next;
            end;
            
        except
            on e:Exception do
                MsgBoxErr('Error cargando Combo Categorias Clases', e.Message, 'Error de carga', MB_ICONSTOP);
        end;
    finally
        cbbClaseCategorias.ItemIndex:= 0;
        cbbClaseCategorias.Enabled := True;
    end;
end;


procedure TFormPlanTarifarioVersionesCrear.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TFormPlanTarifarioVersionesCrear.btnGenerarClick(Sender: TObject);
begin

    if FMulticoncesionHabilitada then
    begin
        if not ValidateControls([cbbConcesionaria], [cbbConcesionaria.ItemIndex>0],'Atenci�n', ['Seleccione una Concesionaria']) then
            Exit;
    end;

    if not ValidateControls([cbbClaseCategorias, edtFechaActivacion, edtFechaActivacion],
                     [cbbClaseCategorias.ItemIndex > 0,
                     edtFechaActivacion.Date <> NullDate,
                     edtFechaActivacion.Date > FFechaActual],
                     'Atenci�n',
                     ['Seleccione una Clase de Categoria',
                     'Indique una Fecha Activacion',
                     'Fecha Activaci�n debe ser mayor al d�a de hoy']) then
                     Exit;

    if (FAccion = 3) and (oPlanTarifario.Habilitado) then
    begin
        //Si estoy cambiando la fecha desde de un plan habilidado a una fecha menor o igual a la fecha actual, muestro un error
        if (edtFechaActivacion.Date <> oPlanTarifario.FechaActivacion) and (oPlanTarifario.FechaActivacion <= FFechaActual) then
        begin
            MsgBox('No puede modificar una Fecha de Activaci�n de un plan habilitado y cuya Fecha Desde sea menor al dia de hoy', 'Error en fechas', MB_ICONSTOP);
            Exit;
        end;
    end;

    try
        oPlanTarifario.ClientDataSet.DisableControls;
        oPlanTarifario.ClientDataSet.First;
        while not oPlanTarifario.ClientDataSet.Eof do
        begin
            if not ((FAccion = 3) and (FPlanSeleccionado = oPlanTarifario.ClientDataSet.Bookmark)) then  //Editando no verifico la fecha del plan a editar
            begin
                if (cbbClaseCategorias.Value = oPlanTarifario.ID_CategoriasClases)
                    and (cbbConcesionaria.Value = oPlanTarifario.CodigoConcesionaria)
                    and (edtFechaActivacion.Date = oPlanTarifario.FechaActivacion)
                then
                begin
                    MsgBox('Las fecha de activaci�n ingresada ya existe en el plan: ' + IntToStr(oPlanTarifario.CodigoVersion), 'Error en fechas', MB_ICONSTOP);
                    Exit;
                end;
            end;
            oPlanTarifario.ClientDataSet.Next;
        end;
    finally
        oPlanTarifario.ClientDataSet.Bookmark := FPlanSeleccionado;
        oPlanTarifario.ClientDataSet.EnableControls;
    end;

    ModalResult := mrOK;
end;

{TERMINO: TASK_109_JMA_20170215}
end.
