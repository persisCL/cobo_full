unit ABMAcciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls;

type
  TFormAcciones = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Panel2: TPanel;
    txt_Descripcion: TEdit;
    Acciones: TADOTable;
    txt_CodigoAccion: TNumericEdit;
    Notebook: TNotebook;
    cb_TiposSeveridad: TComboBox;
    Label2: TLabel;
    BtnSalir: TDPSButton;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormAcciones: TFormAcciones;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar esta Acci�n?';
    MSG_DELETE_ERROR		= 'No se puede eliminar esta Acci�n porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION		= 'Eliminar Acci�n';
    MSG_ACTUALIZAR_ERROR  	= 'No se pudieron actualizar los datos de la Acci�n.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Acci�n';


{$R *.DFM}

procedure TFormAcciones.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txt_CodigoAccion.Enabled	:= True;
end;

function TFormAcciones.Inicializa: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	if not OpenTables([Acciones]) then
		Result := False
	else begin
    	CargarTiposSeveridad(DMConnections.BaseCAC, cb_TiposSeveridad);
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormAcciones.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;

procedure TFormAcciones.DBList1Click(Sender: TObject);
begin
	with Acciones do begin
		txt_CodigoAccion.Value	:= FieldByName('CodigoAccion').AsInteger;
		txt_Descripcion.text	:= FieldByName('Descripcion').AsString;
        CargarTiposSeveridad(DMConnections.BaseCAC, cb_TiposSeveridad, FieldByName('CodigoSeveridadDefecto').AsInteger)
	end;
end;

procedure TFormAcciones.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormAcciones.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormAcciones.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormAcciones.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormAcciones.Limpiar_Campos;
begin
	txt_codigoAccion.Clear;
	txt_Descripcion.Clear;
    cb_TiposSeveridad.ItemIndex := 0;
end;

procedure TFormAcciones.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormAcciones.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			Acciones.Delete;
		Except
			On E: Exception do begin
				Acciones.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;

procedure TFormAcciones.BtnAceptarClick(Sender: TObject);
begin
 	Screen.Cursor := crHourGlass;
	With Acciones do begin
		Try
			if DbList1.Estado = Alta then begin
            	Append;
                txt_codigoAccion.value := QueryGetValueInt(DMConnections.BaseCAC,
                	'Select ISNULL(MAX(CodigoAccion), 0) + 1 FROM Acciones');
                FieldByName('CodigoAccion').value		:= txt_codigoAccion.value;
			end else Edit;
			FieldByName('Descripcion').AsString			:= txt_Descripcion.text;
			FieldByName('CodigoSeveridadDefecto').AsInteger	:= Ival(StrRight(cb_TiposSeveridad.Text, 10));
			Post;
		except
			On E: EDataBaseError do begin
				Cancel;
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                Exit;
			end;
		end;
	end;
    VolverCampos;
	Screen.Cursor	:= crDefault;
end;

procedure TFormAcciones.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormAcciones.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormAcciones.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormAcciones.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_CodigoAccion.Enabled	:= False;
    txt_Descripcion.SetFocus;
end;

end.
