object FormSectores: TFormSectores
  Left = 221
  Top = 144
  Width = 687
  Height = 420
  Caption = 'Mantenimiento de Sectores'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 679
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 679
    Height = 199
    TabStop = True
    TabOrder = 1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'171'#0'Concesionaria                                '
      #0'65'#0'C'#243'digo        '
      #0'178'#0'Descripci'#243'n                                      '
      #0'163'#0'Eje Vial                                        ')
    HScrollBar = True
    RefreshTime = 100
    Table = Sectores
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBList1Click
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnDelete = DBList1Delete
    OnEdit = DBList1Edit
    Estado = Normal
    ToolBar = AbmToolbar1
    Access = [accAlta, accBaja, accModi]
  end
  object GroupB: TPanel
    Left = 0
    Top = 232
    Width = 679
    Height = 122
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 97
      Width = 72
      Height = 13
      Caption = '&Descripci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 16
      Top = 69
      Width = 85
      Height = 13
      Caption = '&C'#243'digo Sector:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 40
      Width = 48
      Height = 13
      Caption = 'Eje &Vial:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 11
      Width = 85
      Height = 13
      Caption = 'C&oncesionaria:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txt_Descripcion: TEdit
      Left = 117
      Top = 94
      Width = 297
      Height = 21
      Color = 16444382
      MaxLength = 60
      TabOrder = 2
    end
    object txt_CodigoSector: TNumericEdit
      Left = 117
      Top = 65
      Width = 105
      Height = 21
      Color = 16444382
      MaxLength = 3
      TabOrder = 1
      Decimals = 0
    end
    object cb_EjesViales: TComboBox
      Left = 117
      Top = 36
      Width = 297
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
    end
    object cbConcesionarias: TComboBox
      Left = 117
      Top = 7
      Width = 297
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 3
      OnChange = cbConcesionariasChange
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 354
    Width = 679
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 482
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TDPSButton
          Left = 112
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TDPSButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TDPSButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Qry_EjesViales: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoConcesionaria'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '  * '
      'FROM '
      '  EjesViales WITH(NOLOCK)'
      'WHERE'
      '  CodigoConcesionaria = :CodigoConcesionaria')
    Left = 284
    Top = 80
  end
  object Sectores: TADOTable
    Connection = DMConnections.BaseCAC
    TableName = 'Sectores'
    Left = 252
    Top = 80
  end
end
